﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoiceFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.InvoiceFinderControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<asp:Panel runat="server" ID="pnlInvoiceFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Invoice Finder<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
        </h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlInvoiceFinderContent.ClientID%> div[class="finderScroll"]').offset().top - $('#<%= pnlInvoiceFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlInvoiceFinderContent.ClientID%> div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table id="finderTable" class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:Button ID="btnProcessSelected2" runat="server" Text="ADD SELECTED" OnClick="OnProcessSelectedClicked" CausesValidation="false" />
                        </div>
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add" />
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="right">
                                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Invoices" ShowAutoRefresh="False"
                                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>
                        <hr class="mb5" />
                        <script type="text/javascript">$(window).load(function () {SetControlFocus();});</script><table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                        <div class="pl10 row mb5">
                            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" CssClass="mr10" />
                            <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL" CssClass="mr10" CausesValidation="false" />
                            <asp:Button ID="btnProcessSelected" runat="server" Text="ADD SELECTED" OnClick="OnProcessSelectedClicked" CausesValidation="False" />
                            <label class="right pr10">Sort: <asp:Literal runat="server" ID="litSortOrder" Text='<%# WebApplicationConstants.Default %>' /></label>
                        </div>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstSearchResults" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="invoiceFinderScrollSection"
                SelectionCheckBoxControlId="chkSelected" SelectionUniqueRefHiddenFieldId="hidInvoiceId" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheUsersFinderTable" TableId="invoiceFinderTable" ScrollerContainerId="invoiceFinderScrollSection" ScrollOffsetControlId="pnlInvoiceFinderContent"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
            <div class="finderScroll" id="invoiceFinderScrollSection">
                <table id="invoiceFinderTable" class="stripe sm_chk">
                    <tr>
                        <th style="width: 15%;">
                            <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'invoiceFinderTable');" />
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortInvoiceNumber" CssClass="link_nounderline white"
                                CausesValidation="False" OnCommand="OnSortData">
                                            Invoice
                            </asp:LinkButton>
                        </th>
                        <th style="width: 5%;">
                            <asp:LinkButton runat="server" ID="lbtnSortInvoiceDate" CssClass="link_nounderline white"
                                CausesValidation="False" OnCommand="OnSortData">
                                            Invoice Date
                            </asp:LinkButton>
                        </th>
                        <th style="width: 5%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDueDate" CssClass="link_nounderline white"
                                CausesValidation="False" OnCommand="OnSortData">
                                            Due Date
                            </asp:LinkButton>
                        </th>
                        <th style="width: 5%;">
                            <asp:LinkButton runat="server" ID="lbtnSortPostDate" CssClass="link_nounderline white"
                                CausesValidation="False" OnCommand="OnSortData">
                                            Posted Date
                            </asp:LinkButton>
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortTypeText" CssClass="link_nounderline white"
                                CausesValidation="False" OnCommand="OnSortData">
                                            Type
                            </asp:LinkButton>
                        </th>
                        <th style="width: 40%;">Customer
                                <asp:LinkButton runat="server" ID="lbtnSortCustomerNumber" CssClass="link_nounderline white"
                                    CausesValidation="False" OnCommand="OnSortData">
                                            Number
                                </asp:LinkButton>/
                                <asp:LinkButton runat="server" ID="lbtnSortCustomerName" CssClass="link_nounderline white"
                                    CausesValidation="False" OnCommand="OnSortData">
                                            Name
                                </asp:LinkButton>
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortOriginalInvoice" CssClass="link_nounderline white"
                                CausesValidation="False" OnCommand="OnSortData">
                                            Original Inv. #
                            </asp:LinkButton>
                        </th>
                    </tr>
                    <asp:ListView ID="lstSearchResults" runat="server" ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked" Visible='<%# !EnableMultiSelection %>' CausesValidation="false" />
                                    <asp:Button ID="btnEditSelect" runat="server" Text="Edit" OnClick="OnEditSelectClicked" Visible='<%# !EnableMultiSelection && OpenForEditEnabled %>' CausesValidation="false" />
                                    <eShip:AltUniformCheckBox runat="server" ID="chkSelected" Visible='<%# EnableMultiSelection %>' OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'invoiceFinderTable', 'chkSelectAllRecords');" />
                                    <eShip:CustomHiddenField ID="hidInvoiceId" Value='<%# Eval("Id") %>' runat="server" />
                                </td>
                                <td>
                                    <%# Eval("InvoiceNumber")%>
                                </td>
                                <td>
                                    <%# Eval("InvoiceDate").FormattedShortDate()%>
                                </td>
                                <td>
                                    <%# !Eval("DueDate").ToDateTime().Equals(DateUtility.SystemEarliestDateTime) ? Eval("DueDate").FormattedShortDate() : string.Empty %>
                                </td>
                                <td>
                                    <%# !Eval("PostDate").ToDateTime().Equals(DateUtility.SystemEarliestDateTime) ? Eval("PostDate").FormattedShortDate() : string.Empty %>
                                </td>
                                <td>
                                    <%# Eval("InvoiceType").FormattedString() %>
                                </td>
                                <td>
                                    <%# Eval("CustomerNumber") %> - <%# Eval("CustomerName") %>
                                </td>
                                <td>
                                    <%# Eval("OriginalInvoiceNumber")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnSortInvoiceNumber" />
            <asp:PostBackTrigger ControlID="lbtnSortInvoiceDate" />
            <asp:PostBackTrigger ControlID="lbtnSortDueDate" />
            <asp:PostBackTrigger ControlID="lbtnSortPostDate" />
            <asp:PostBackTrigger ControlID="lbtnSortTypeText" />
            <asp:PostBackTrigger ControlID="lbtnSortCustomerNumber" />
            <asp:PostBackTrigger ControlID="lbtnSortCustomerName" />
            <asp:PostBackTrigger ControlID="lbtnSortOriginalInvoice" />
            <asp:PostBackTrigger ControlID="lstSearchResults" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<asp:Panel ID="pnlInvoiceFinderDimScreen" CssClass="dimBackground" runat="server"
    Visible="false" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
<eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
<eShip:CustomHiddenField runat="server" ID="hidEditSelected" />
<eShip:CustomHiddenField runat="server" ID="hidOpenForEditEnabled" />
<eShip:CustomHiddenField runat="server" ID="hidInvoiceFinderEnableMultiSelection" />
<eShip:CustomHiddenField runat="server" ID="hidFilterForInvoiceType" />
<eShip:CustomHiddenField runat="server" ID="hidInvoiceToExclude" />
<eShip:CustomHiddenField runat="server" ID="hidSortAscending" />
<eShip:CustomHiddenField runat="server" ID="hidSortField" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />