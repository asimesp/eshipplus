﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class PaymentRefundOrVoidControl : MemberControlBase, IPaymentRefundOrVoidView
    {
        protected const string Refundable = "Refundable";
        protected const string Voidable = "Voidable";
        protected const string CannotVoidOrRefund = "Cannot Void/Refund";

        public new bool Visible
        {
            get { return pnlPaymentRefund.Visible; }
            set
            {
                base.Visible = value;
                pnlPaymentRefund.Visible = value;
                pnlPaymentRefundDimScreen.Visible = value;
            }
        }


        private bool _lockFailed;

        public event EventHandler Close;
        public event EventHandler PaymentRefundProcessed;
        public event EventHandler<ViewEventArgs<List<ValidationMessage>>> ProcessingMessages;

        public event EventHandler<ViewEventArgs<Shipment, List<MiscReceipt>>> RefundOrVoidMiscReceiptsForShipment;
        public event EventHandler<ViewEventArgs<LoadOrder, List<MiscReceipt>>> RefundOrVoidMiscReceiptsForLoadOrder;
        public event EventHandler<ViewEventArgs<Shipment, List<MiscReceipt>>> LockShipmentAndMiscReceipts;
        public event EventHandler<ViewEventArgs<LoadOrder, List<MiscReceipt>>> LockLoadOrderAndMiscReceipts;
        public event EventHandler<ViewEventArgs<List<MiscReceipt>>>  UnlockMiscReceipts;

        private string GetMiscReceiptStatusToDisplay(MiscReceipt miscReceipt)
        {
            if (miscReceipt.PaymentGatewayType == PaymentGatewayType.Check || miscReceipt.PaymentGatewayType == PaymentGatewayType.NotApplicable)
                return Refundable;

            if (miscReceipt.PaymentGatewayType != miscReceipt.Tenant.PaymentGatewayType)
                return CannotVoidOrRefund;

            try
            {
                switch (miscReceipt.GetTransactionStatusViaPaymentGateway())
                {
                    case TransactionStatus.AuthorizedPendingCapture:
                    case TransactionStatus.CapturedPendingSettlement:
                        return miscReceipt.AmountLeftToBeAppliedOrRefunded(ActiveUser.TenantId) < miscReceipt.AmountPaid ? CannotVoidOrRefund : Voidable;
                    case TransactionStatus.SettledSuccessfully:
                        return Refundable;
                    default:
                        return CannotVoidOrRefund;
                }
            }
            catch (Exception)
            {
                return CannotVoidOrRefund;
            }
        }


        public void Clear()
        {
            hidShipmentId.Value = string.Empty;
            hidLoadOrderId.Value = string.Empty;
            rptMiscReceipts.DataSource = new List<object>();
            rptMiscReceipts.DataBind();
        }
        
        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void FailedLock()
        {
            _lockFailed = true;
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> msgs)
        {
            if (ProcessingMessages != null)
                ProcessingMessages(this, new ViewEventArgs<List<ValidationMessage>>(msgs.ToList()));
        }
        
        public void DisplayMiscReceipts(IEnumerable<MiscReceipt> miscReceipts)
        {
            rptMiscReceipts.DataSource = miscReceipts
                .Select(mr => new
                {
                    mr.Id,
                    mr.ShipmentId,
                    mr.LoadOrderId,
                    ShipmentNumber = mr.ShipmentId != default(long) ? mr.Shipment.ShipmentNumber : string.Empty,
                    LoadOrderNumber = mr.LoadOrderId != default(long) ? mr.LoadOrder.LoadOrderNumber : string.Empty,
                    mr.User.Username,
                    mr.UserId,
                    AppliableAmount = mr.AmountLeftToBeAppliedOrRefunded(ActiveUser.TenantId),
                    mr.PaymentDate,
                    mr.GatewayTransactionId,
                    mr.PaymentGatewayType,
                    Status = GetMiscReceiptStatusToDisplay(mr),
                });
            rptMiscReceipts.DataBind();
        }
        

        public void LoadLoadOrder(LoadOrder loadOrder)
        {
            if (loadOrder.IsNew)
            {
                if (ProcessingMessages != null)
                    ProcessingMessages(this, new ViewEventArgs<List<ValidationMessage>>(new List<ValidationMessage> { ValidationMessage.Error("Cannot refund or void misc receipts on an unsaved load order.") }));
                return;
            }

            var miscReceipts = new MiscReceiptSearch().FetchMiscReceiptsRelatedToLoadOrder(loadOrder.Id, ActiveUser.TenantId).Where(mr => !mr.Reversal);

            if (LockLoadOrderAndMiscReceipts != null)
                LockLoadOrderAndMiscReceipts(this, new ViewEventArgs<LoadOrder, List<MiscReceipt>>(loadOrder, miscReceipts.ToList()));

            if (_lockFailed)
            {
                Visible = false;
                return;
            }

            hidLoadOrderId.Value = loadOrder.Id.GetString();
            hidShipmentId.Value = string.Empty;
            litHeaderShipmentOrLoadOrderNumber.Text = string.Format(" {0} {1}", loadOrder.EntityName().FormattedString(), loadOrder.LoadOrderNumber);

            DisplayMiscReceipts(miscReceipts);
        }

        public void LoadShipment(Shipment shipment)
        {
            if (shipment.IsNew)
            {
                if (ProcessingMessages != null)
                    ProcessingMessages(this, new ViewEventArgs<List<ValidationMessage>>(new List<ValidationMessage> { ValidationMessage.Error("Cannot refund or void misc receipts on an unsaved shipment.") }));
                return;
            }

            var miscReceipts = new MiscReceiptSearch().FetchMiscReceiptsRelatedToShipment(shipment.Id, ActiveUser.TenantId).Where(mr => !mr.Reversal);

            if(LockShipmentAndMiscReceipts != null)
                LockShipmentAndMiscReceipts(this, new ViewEventArgs<Shipment, List<MiscReceipt>>(shipment, miscReceipts.ToList()));

            if (_lockFailed)
            {
                Visible = false;
                return;
            }

            hidLoadOrderId.Value = string.Empty;
            hidShipmentId.Value = shipment.Id.GetString();
            litHeaderShipmentOrLoadOrderNumber.Text = string.Format(" {0} {1}", shipment.EntityName().FormattedString(), shipment.ShipmentNumber);

            DisplayMiscReceipts(miscReceipts);
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new PaymentRefundOrVoidHandler(this).Initialize();
        }
        

        protected void OnClosePaymentRefundClicked(object sender, EventArgs e)
        {
            var miscReceipts = rptMiscReceipts
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new MiscReceipt(i.FindControl("hidMiscReceiptId").ToCustomHiddenField().Value.ToLong()))
                .ToList();

            if(UnlockMiscReceipts != null)
                UnlockMiscReceipts(this, new ViewEventArgs<List<MiscReceipt>>(miscReceipts));

            Clear();

            if (Close != null)
                Close(this, new EventArgs());
        }

        protected void OnRefundVoidMiscReceiptsClicked(object sender, EventArgs e)
        {
            var refundReceipts = rptMiscReceipts
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToAltUniformCheckBox().Checked)
                .Select(i =>
                    {
                        var miscReceipt = new MiscReceipt(i.FindControl("hidMiscReceiptId").ToCustomHiddenField().Value.ToLong());
                        var amountToRefund = i.FindControl("txtAmountToRefund").ToTextBox().Text.ToDecimal();

                        return new MiscReceipt
                            {
                                ShipmentId = i.FindControl("hidShipmentId").ToCustomHiddenField().Value.ToLong(),
                                LoadOrderId = i.FindControl("hidLoadOrderId").ToCustomHiddenField().Value.ToLong(),
                                AmountPaid = amountToRefund,
                                TenantId = ActiveUser.TenantId,
                                OriginalMiscReceipt = miscReceipt,
                                Reversal = true,
                                NameOnCard = miscReceipt.NameOnCard,
                                CustomerId = miscReceipt.CustomerId,
                                PaymentDate = DateTime.Now,
                                GatewayTransactionId = string.Empty,
                                UserId = ActiveUser.Id,
                                PaymentGatewayType = miscReceipt.PaymentGatewayType,
                                MiscReceiptApplications = new List<MiscReceiptApplication>(),
                                PaymentProfileId = string.Empty
                            };
                    }).ToList();

            if (hidShipmentId.Value.ToLong() != default(long) && RefundOrVoidMiscReceiptsForShipment != null)
                RefundOrVoidMiscReceiptsForShipment(this, new ViewEventArgs<Shipment, List<MiscReceipt>>(new Shipment(hidShipmentId.Value.ToLong()), refundReceipts));

            if (hidLoadOrderId.Value.ToLong() != default(long) && RefundOrVoidMiscReceiptsForLoadOrder != null)
                RefundOrVoidMiscReceiptsForLoadOrder(this, new ViewEventArgs<LoadOrder, List<MiscReceipt>>(new LoadOrder(hidLoadOrderId.Value.ToLong()), refundReceipts));

            if(UnlockMiscReceipts != null)
                UnlockMiscReceipts(this, new ViewEventArgs<List<MiscReceipt>>(refundReceipts.Select(mr => mr.OriginalMiscReceipt).ToList()));

            if(PaymentRefundProcessed != null)
                PaymentRefundProcessed(this, new EventArgs());
        }
    }
}