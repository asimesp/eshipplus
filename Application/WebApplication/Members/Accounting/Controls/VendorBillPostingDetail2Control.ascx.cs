﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class VendorBillPostingDetail2Control : MemberControlBase
    {
        public bool Selected { get { return chkSelection.Checked; } }

        public bool EnableMultiSelect { get; set; }

        public int ItemIndex
        {
            get { return hidItemIndex.Value.ToInt(); }
            set { hidItemIndex.Value = value.ToString(); }
        }

        public void LoadVendorBill(VendorBillDashboardDto vendorBill)
        {
            hidVendorBillId.Value = vendorBill.Id.ToString();
            litDocumentNumber.Text = vendorBill.DocumentNumber;
            litVendor.Text = string.Format("{0} - {1}", vendorBill.VendorNumber, vendorBill.VendorName);
            var vendor = new VendorSearch().FetchVendorByNumber(vendorBill.VendorNumber, ActiveUser.TenantId);
            hypVendor.NavigateUrl = string.Format("{0}?{1}={2}", VendorView.PageAddress, WebApplicationConstants.TransferNumber, vendor.Id.GetString().UrlTextEncrypt());

            litDocumentDate.Text = vendorBill.DocumentDate.FormattedShortDate();
            litDateCreated.Text = vendorBill.DateCreated.FormattedShortDate();
            litAddress.Text = string.Format("{0}{1}{2}{3}{4}, {5} {6} {7}",
                                            vendorBill.LocationStreet1,
                                            WebApplicationConstants.HtmlBreak, vendorBill.LocationStreet2,
                                            string.IsNullOrEmpty(vendorBill.LocationStreet2)
                                                ? string.Empty
                                                : WebApplicationConstants.HtmlBreak,
                                            vendorBill.LocationCity, vendorBill.LocationState, vendorBill.LocationPostalCode,
                                            vendorBill.LocationCountryName);
            litType.Text = vendorBill.BillType.ToString();
            litUser.Text = vendorBill.Username;

            var hasAccessToUser = ActiveUser.HasAccessTo(ViewCode.User);
            litUser.Text = vendorBill.Username;

            if (hasAccessToUser)
            {
                var user = new UserSearch().FetchUserByUsernameAndAccessCode(vendorBill.Username, ActiveUser.Tenant.Code);
                hypUser.Visible = true;
                hypUser.NavigateUrl = string.Format("{0}?{1}={2}", UserView.PageAddress, WebApplicationConstants.TransferNumber, user.Id.GetString().UrlTextEncrypt());
            }

            litAmountDue.Text = vendorBill.AmountDue.ToString("c2");

            var httpAddr = Request.ResolveSiteRootWithHttp();
            litServiceTickets.Text = ActiveUser.HasAccessTo(ViewCode.ServiceTicket)
                                               ? ServiceTicketView.BuildServiceTicketLinks(vendorBill.ServiceTickets).Replace("~", httpAddr)
                                               : vendorBill.ServiceTickets;
            litShipments.Text = ActiveUser.HasAccessTo(ViewCode.Shipment)
                                               ? ShipmentView.BuildShipmentLinks(vendorBill.Shipments).Replace("~", httpAddr)
                                               : vendorBill.Shipments;
            hypVendorBillView.NavigateUrl = string.Format("{0}?{1}={2}", VendorBillView.PageAddress, WebApplicationConstants.TransferNumber, vendorBill.Id.GetString().UrlTextEncrypt());
        }
    }
}