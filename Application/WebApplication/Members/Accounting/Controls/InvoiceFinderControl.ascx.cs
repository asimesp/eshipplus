﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class InvoiceFinderControl : MemberControlBase, IInvoiceFinderView
    {
        private SearchField SortByField
        {
            get { return AccountingSearchFields.InvoiceSortFields.FirstOrDefault(c => c.Name == hidSortField.Value); }
        }

        public new bool Visible
        {
            get { return pnlInvoiceFinderContent.Visible; }
            set
            {
                base.Visible = true;
                pnlInvoiceFinderContent.Visible = value;
                pnlInvoiceFinderDimScreen.Visible = value;
            }
        }

        public bool OpenForEdit
        {
            get { return hidEditSelected.Value.ToBoolean(); }
        }

        public bool OpenForEditEnabled
        {
            get { return hidOpenForEditEnabled.Value.ToBoolean(); }
            set { hidOpenForEditEnabled.Value = value.ToString(); }
        }

        public bool EnableMultiSelection
        {
            get { return hidInvoiceFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidInvoiceFinderEnableMultiSelection.Value = value.ToString();
                btnProcessSelected.Visible = value;
                btnProcessSelected2.Visible = value;
                upcseDataUpdate.PreserveRecordSelection = value;
                chkSelectAllRecords.Visible = value;
            }
        }

        public bool ShowInvoiceTypeOnly
        {
            set { hidFilterForInvoiceType.Value = value.ToString(); }
        }

        public long ExcludeInvoiceId
        {
            set { hidInvoiceToExclude.Value = value.ToString(); }
        }

        public long CustomerId { set { hidCustomerId.Value = value.ToString(); } }

        public event EventHandler<ViewEventArgs<InvoiceViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<Invoice>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<Invoice>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<InvoiceDashboardDto> invoiceDtos)
        {
            var filtered = hidCustomerId.Value.ToLong() != default(long)
                                    ? invoiceDtos
                                        .Where(c => c.CustomerId == hidCustomerId.Value.ToLong())
                                        .ToList()
                                    : invoiceDtos;

            filtered = hidFilterForInvoiceType.Value.ToBoolean()
                        ? filtered.Where(i => i.InvoiceType == InvoiceType.Invoice).ToList()
                        : filtered;

            var id = hidInvoiceToExclude.Value.ToLong();
            var invoicesToDisplay = filtered.Where(i => i.Id != id).ToList();
            litRecordCount.Text = invoicesToDisplay.BuildRecordCount();
            upcseDataUpdate.DataSource = invoicesToDisplay;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString();
            DisplaySearchResult(new List<InvoiceDashboardDto>());
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = hidSortAscending.Value.ToBoolean(),
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            var sortAscending = hidSortAscending.Value.ToBoolean();

            litSortOrder.SetSortDisplay(field, hidSortAscending.Value.ToBoolean());
            DoSearch(new InvoiceViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = sortAscending
            });

        }
        
        private void DoSearch(InvoiceViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<InvoiceViewSearchCriteria>(criteria));
        }



        private void ProcessItemSelection(long invoiceId)
        {
            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<Invoice>(new Invoice(invoiceId, false)));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new InvoiceFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;
            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;

            //link up sort buttons
            lbtnSortInvoiceNumber.CommandName = AccountingSearchFields.InvoiceNumber.Name;
            lbtnSortInvoiceDate.CommandName = AccountingSearchFields.InvoiceDate.Name;
            lbtnSortDueDate.CommandName = AccountingSearchFields.DueDate.Name;
            lbtnSortPostDate.CommandName = AccountingSearchFields.PostDate.Name;
            lbtnSortTypeText.CommandName = AccountingSearchFields.InvoiceTypeText.Name;
            lbtnSortCustomerNumber.CommandName = AccountingSearchFields.CustomerNumber.Name;
            lbtnSortCustomerName.CommandName = AccountingSearchFields.CustomerName.Name;
            lbtnSortOriginalInvoice.CommandName = AccountingSearchFields.OriginalInvoiceNumber.Name;


            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                             ? profile.Columns
                             : AccountingSearchFields.DefaultInvoices.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            hidSortAscending.Value = (profile != null && profile.SortAscending).GetString();

            if (profile != null && profile.SortBy != null)
                hidSortField.Value = profile.SortBy.Name;

            litSortOrder.SetSortDisplay(SortByField, hidSortAscending.Value.ToBoolean());
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = false.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnProcessSelectedClicked(object sender, EventArgs e)
        {
            var invoices = (from item in lstSearchResults.Items
                            let hidden = item.FindControl("hidInvoiceId").ToCustomHiddenField()
                            let checkBox = item.FindControl("chkSelected").ToAltUniformCheckBox()
                            where checkBox != null && checkBox.Checked
                            select new Invoice(hidden.Value.ToLong()))
                .Where(u => u.KeyLoaded)
                .ToList();

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<Invoice>>(invoices));
        }

        protected void OnEditSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = true.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.Invoices.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AccountingSearchFields.Invoices);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            hidSortAscending.Value = e.Argument.SortAscending.GetString();
            hidSortField.Value = e.Argument.SortBy == null ? string.Empty : e.Argument.SortBy.Name;

            litSortOrder.SetSortDisplay(SortByField, hidSortAscending.Value.ToBoolean());
        }



        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = AccountingSearchFields.InvoiceSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;

            if (hidSortField.Value == field.Name && hidSortAscending.Value.ToBoolean())
            {
                hidSortAscending.Value = false.GetString();
            }
            else if (hidSortField.Value == field.Name && !hidSortAscending.Value.ToBoolean())
            {
                hidSortAscending.Value = true.GetString();
            }

            hidSortField.Value = field.Name;
            litSortOrder.SetSortDisplay(field, hidSortAscending.Value.ToBoolean());
            DoSearchPreProcessingThenSearch(field);
        }
    }
}