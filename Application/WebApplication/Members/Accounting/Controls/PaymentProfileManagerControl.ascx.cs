﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class PaymentProfileManagerControl : MemberControlBase, IPaymentProfileManagerView
    {
        private const string AddOrUpdatePaymentProfileFlag = "UPP";

        public List<PaymentGatewayPaymentProfile> PaymentProfiles
        {
            set
            {
                rptPaymentProfiles.DataSource = value;
                rptPaymentProfiles.DataBind();
            }
        }

        public event EventHandler<ViewEventArgs<List<ValidationMessage>>> ProcessingMessages;

        public event EventHandler<ViewEventArgs<long>> LoadPaymentProfiles;
        public event EventHandler<ViewEventArgs<long, string>> DeletePaymentProfile;
        public event EventHandler<ViewEventArgs<long, PaymentGatewayPaymentProfile>> AddOrUpdatePaymentProfile;


        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);

        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == AddOrUpdatePaymentProfileFlag)
            {
                LoadPaymentProfile(new PaymentGatewayPaymentProfile());
                pnlModifyPaymentProfile.Visible = false;
                pnlPaymentProfileManagerControlDimScreen.Visible = false;
                hidFlag.Value = string.Empty;
            }

            if (ProcessingMessages != null)
                ProcessingMessages(this, new ViewEventArgs<List<ValidationMessage>>(messages.ToList()));
        }

        public void LoadCustomerToManagePaymentProfiles(Customer customer)
        {
            if (customer == null)
            {
                btnAddPaymentProfile.Visible = false;
                PaymentProfiles = new List<PaymentGatewayPaymentProfile>();
                return;
            }

            hidCustomerId.Value = customer.Id.GetString();
            if (!string.IsNullOrEmpty(customer.PaymentGatewayKey))
            {
                btnAddPaymentProfile.Visible = true;
                if(LoadPaymentProfiles != null)
                    LoadPaymentProfiles(this, new ViewEventArgs<long>(customer.Id));
            }
            else
            {
                btnAddPaymentProfile.Visible = false;
                DisplayMessages(new[] { ValidationMessage.Error("The selected customer account is not setup to handle credit card payments.") });
                PaymentProfiles = new List<PaymentGatewayPaymentProfile>();
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new PaymentProfileManagerHandler(this).Initialize();

            if (IsPostBack) return;

            var months = DateUtility.BuildMonthList().Select(m => new ViewListItem(m, m)).ToList();
            months.Insert(0, new ViewListItem("XX", string.Empty));
            ddlPaymentProfileCardExpirationMonth.DataSource = months;
            ddlPaymentProfileCardExpirationMonth.DataBind();

            var years = DateUtility.BuildPresentAndFutureYearsList(WebApplicationConstants.PaymentGatewayExpirationDateYearsFromNow).Select(m => new ViewListItem(m, m)).ToList();
            years.Insert(0, new ViewListItem("XX", string.Empty));
            ddlPaymentProfileCardExpirationYear.DataSource = years;
            ddlPaymentProfileCardExpirationYear.DataBind();
        }


        private void LoadPaymentProfile(PaymentGatewayPaymentProfile profile)
        {
            txtPaymentProfileCardNumber.SetTextBoxEnabled(string.IsNullOrEmpty(profile.PaymentProfileId));
            txtPaymentProfileSecurityCode.SetTextBoxEnabled(string.IsNullOrEmpty(profile.PaymentProfileId));
            ddlPaymentProfileCardExpirationMonth.Enabled = string.IsNullOrEmpty(profile.PaymentProfileId);
            ddlPaymentProfileCardExpirationYear.Enabled = string.IsNullOrEmpty(profile.PaymentProfileId);

            hidEditPaymentProfileId.Value = profile.PaymentProfileId;
            txtPaymentProfileCardNumber.Text = profile.CreditCardNumber;
            ddlPaymentProfileCardExpirationMonth.SelectedValue = ddlPaymentProfileCardExpirationMonth.ContainsValue(profile.ExpirationDateMonth)
                    ? profile.ExpirationDateMonth
                    : string.Empty;
            ddlPaymentProfileCardExpirationYear.SelectedValue = ddlPaymentProfileCardExpirationYear.ContainsValue(profile.ExpirationDateYear)
                    ? profile.ExpirationDateYear
                    : string.Empty;
            txtPaymentProfileSecurityCode.Text = string.Empty;
            txtPaymentProfileBillingFirstName.Text = profile.BillingFirstName;
            txtPaymentProfileBillingLastName.Text = profile.BillingLastName;
            txtPaymentProfileBillingStreet.Text = profile.BillingStreet;
            txtPaymentProfileBillingCity.Text = profile.BillingCity;
            txtPaymentProfileBillingState.Text = profile.BillingState;
            txtPaymentProfileBillingZip.Text = profile.BillingZip;

            var country = (ProcessorVars.RegistryCache.Countries.Values.FirstOrDefault(c => c.Code == profile.BillingCountry) ?? new Country());
            ddlPaymentProfileBillingCountry.SelectedValue = country.Id.GetString();
        }


        protected void OnEditPaymentProfileClicked(object sender, ImageClickEventArgs e)
        {
            var control = (Control)sender;
            LoadPaymentProfile(new PaymentGatewayPaymentProfile
            {
                PaymentProfileId = control.FindControl("hidProfileId").ToCustomHiddenField().Value,
                CreditCardNumber = control.FindControl("litPaymentProfileCreditCardNumber").ToLiteral().Text,
                ExpirationDateMonth = control.FindControl("litPaymentProfileExpirationDateMonth").ToLiteral().Text,
                ExpirationDateYear = control.FindControl("litPaymentProfileExpirationDateYear").ToLiteral().Text,
                BillingFirstName = control.FindControl("litPaymentProfileBillingFirstName").ToLiteral().Text,
                BillingLastName = control.FindControl("litPaymentProfileBillingLastName").ToLiteral().Text,
                BillingStreet = control.FindControl("litPaymentProfileBillingStreet").ToLiteral().Text,
                BillingCity = control.FindControl("litPaymentProfileBillingCity").ToLiteral().Text,
                BillingState = control.FindControl("litPaymentProfileBillingState").ToLiteral().Text,
                BillingZip = control.FindControl("litPaymentProfileBillingZip").ToLiteral().Text,
                BillingCountry = control.FindControl("litPaymentProfileBillingCountry").ToLiteral().Text,
                SecurityCode = string.Empty
            });

            pnlModifyPaymentProfile.Visible = true;
            pnlPaymentProfileManagerControlDimScreen.Visible = true;
        }

        protected void OnDeletePaymentProfileClicked(object sender, ImageClickEventArgs e)
        {
            var profileId = ((Control)sender).FindControl("hidProfileId").ToCustomHiddenField().Value;

            if (DeletePaymentProfile != null)
                DeletePaymentProfile(this, new ViewEventArgs<long, string>(hidCustomerId.Value.ToLong(), profileId));
        }


        protected void OnCloseEditPaymentProfileClicked(object sender, EventArgs e)
        {
            pnlModifyPaymentProfile.Visible = false;
            pnlPaymentProfileManagerControlDimScreen.Visible = false;
        }

        protected void OnEditPaymentProfileDoneClicked(object sender, EventArgs e)
        {
            var paymentProfile = new PaymentGatewayPaymentProfile
            {
                PaymentProfileId = hidEditPaymentProfileId.Value,
                CreditCardNumber = txtPaymentProfileCardNumber.Text,
                ExpirationDateMonth = ddlPaymentProfileCardExpirationMonth.SelectedValue,
                ExpirationDateYear = ddlPaymentProfileCardExpirationYear.SelectedValue,
                BillingFirstName = txtPaymentProfileBillingFirstName.Text,
                BillingLastName = txtPaymentProfileBillingLastName.Text,
                BillingStreet = txtPaymentProfileBillingStreet.Text,
                BillingCity = txtPaymentProfileBillingCity.Text,
                BillingState = txtPaymentProfileBillingState.Text,
                BillingZip = txtPaymentProfileBillingZip.Text,
                BillingCountry = new Country(ddlPaymentProfileBillingCountry.SelectedValue.ToLong()).Code,
                SecurityCode = txtPaymentProfileSecurityCode.Text
            };

            hidFlag.Value = AddOrUpdatePaymentProfileFlag;
            if (AddOrUpdatePaymentProfile != null)
                AddOrUpdatePaymentProfile(this, new ViewEventArgs<long, PaymentGatewayPaymentProfile>(hidCustomerId.Value.ToLong(), paymentProfile));
        }

        protected void OnAddPaymentProfileClicked(object sender, EventArgs e)
        {
            LoadPaymentProfile(new PaymentGatewayPaymentProfile());
            pnlModifyPaymentProfile.Visible = true;
            pnlPaymentProfileManagerControlDimScreen.Visible = true;
        }
    }
}