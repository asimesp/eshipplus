﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class PaymentEntryControl : MemberControlBaseWithControlStore, IPaymentEntryView
    {
        private const string SaveFlag = "S";

        private const string ShipmentStoreKey = "SSK";
        private const string LoadOrderStoreKey = "LOK";

        private const string PopupWidth500Class = " popupControlOverW500";

        public bool CanModifyPaymentAmount
        {
            get
            {
                switch (PaymentGatewayType)
                {
                    case PaymentGatewayType.NotApplicable:
                        case PaymentGatewayType.Check:
                        return txtNonCreditCardAmountPaid.Enabled;
                    default:
                        return txtAmountPaid.Enabled;
                }
            }
            set
            {
                txtAmountPaid.SetTextBoxEnabled(value);
                txtNonCreditCardAmountPaid.SetTextBoxEnabled(value);
            }
        }

        public new bool Visible
        {
            get { return pnlPaymentEntry.Visible; }
            set
            {
                base.Visible = value;
                pnlPaymentEntry.Visible = value;
                pnlPaymentEntryDimScreen.Visible = value;
            }
        }

        public PaymentGatewayType PaymentGatewayType
        {
            get { return string.IsNullOrEmpty(hidPaymentGatewayType.Value) ? PaymentGatewayType.NotApplicable : hidPaymentGatewayType.Value.ToEnum<PaymentGatewayType>(); }
            set
            {
                hidPaymentGatewayType.Value = value.ToString();
                switch (value)
                {
                    case PaymentGatewayType.NotApplicable:
                        pnlNonCreditCardPayment.Visible = true;
                        pnlCheckInformation.Visible = false;
                        pnlCreditCardPayment.Visible = false;
                        pnlPaymentProfiles.Visible = false;
                        if (!pnlPaymentEntry.CssClass.Contains(PopupWidth500Class)) pnlPaymentEntry.CssClass += PopupWidth500Class;
                        break;
                    case PaymentGatewayType.Check:
                        pnlNonCreditCardPayment.Visible = true;
                        pnlCheckInformation.Visible = true;
                        pnlCreditCardPayment.Visible = false;
                        pnlPaymentProfiles.Visible = false;
                        if (!pnlPaymentEntry.CssClass.Contains(PopupWidth500Class)) pnlPaymentEntry.CssClass += PopupWidth500Class;
                        break;
                    default:
                        pnlNonCreditCardPayment.Visible = false;
                        pnlCheckInformation.Visible = false;
                        pnlCreditCardPayment.Visible = true;
                        pnlPaymentProfiles.Visible = ActiveUser.HasAccessTo(ViewCode.CanSeePaymentProfilesForCustomer);
                        pnlPaymentEntry.CssClass = pnlPaymentEntry.CssClass.Replace(PopupWidth500Class, string.Empty);
                        break;
                }
            }
        }


        private LoadOrder StoredLoadOrder
        {
            get
            {
                return ControlStore.ContainsKey(LoadOrderStoreKey)
                           ? ControlStore[LoadOrderStoreKey] as LoadOrder
                           : null;
            }
            set
            {
                if (!ControlStore.ContainsKey(LoadOrderStoreKey)) ControlStore.Add(LoadOrderStoreKey, value);
                else ControlStore[LoadOrderStoreKey] = value;
            }
        }

        private Shipment StoredShipment
        {
            get
            {
                return ControlStore.ContainsKey(ShipmentStoreKey)
                           ? ControlStore[ShipmentStoreKey] as Shipment
                           : null;
            }
            set
            {
                if (!ControlStore.ContainsKey(ShipmentStoreKey)) ControlStore.Add(ShipmentStoreKey, value);
                else ControlStore[ShipmentStoreKey] = value;
            }
        }


        public Dictionary<string, string> PaymentProfiles
        {
            set
            {
                var values = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).OrderBy(t => t.Text).ToList();
                values.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, string.Empty));

                ddlSavedCreditCards.DataSource = values;
                ddlSavedCreditCards.DataBind();
            }
        }


        public event EventHandler Close;
        public event EventHandler PaymentSuccessfullyProcessed;
        public event EventHandler<ViewEventArgs<List<ValidationMessage>>> ProcessingMessages;

        public event EventHandler<ViewEventArgs<PaymentInformation>> CustomAuthorizeAndCapturePayment;
        public event EventHandler<ViewEventArgs<MiscReceipt, PaymentInformation>> AuthorizeAndCapturePayment;
        public event EventHandler<ViewEventArgs<long, string>> DeletePaymentProfile;
        public event EventHandler<ViewEventArgs<long, string>> LoadPaymentProfile;
        public event EventHandler<ViewEventArgs<long>> LoadPaymentProfiles;
        public event EventHandler<ViewEventArgs<MiscReceipt>> SaveMiscReceipt;


        public void Clear()
        {
            ddlSavedCreditCards.SelectedValue = string.Empty;
            txtCardNumber.Text = string.Empty;
            txtCheckNumber.Text = string.Empty;

            if (ddlCardExpirationMonth.ContainsValue(DateTime.Now.Month.ToString()))
                ddlCardExpirationMonth.SelectedValue = DateTime.Now.Month.ToString();

            var firstDateOfCurrentYear = new DateTime(DateTime.Now.Year, 1, 1);
            if (ddlCardExpirationYear.ContainsValue(firstDateOfCurrentYear.Year.ToString()))
                ddlCardExpirationYear.SelectedValue = firstDateOfCurrentYear.Year.ToString();

            txtNameOnCard.Text = string.Empty;
            txtCardSecurityCode.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtBillingCity.Text = string.Empty;
            txtBillingPostalCode.Text = string.Empty;
            txtBillingStreet.Text = string.Empty;
            txtBillingState.Text = string.Empty;
	        ddlBillingCountry.SelectedValue = default(long).GetString();
            txtAmountPaid.Text = string.Empty;

            DisplayPaymentProfile(new PaymentGatewayPaymentProfile());
        }


        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> msgs)
        {
            if (ProcessingMessages != null)
                ProcessingMessages(this, new ViewEventArgs<List<ValidationMessage>>(msgs.ToList()));

            if(!msgs.HasErrors() && !msgs.HasWarnings() && hidFlag.Value == SaveFlag && PaymentSuccessfullyProcessed != null)
                PaymentSuccessfullyProcessed(this, new EventArgs());
        }

        public void DisplayPaymentProfile(PaymentGatewayPaymentProfile profile)
        {
            if (!string.IsNullOrEmpty(profile.PaymentProfileId))
            {
                txtCardNumber.Text = profile.CreditCardNumber;
                txtCardNumber.SetTextBoxEnabled(false);
                txtCardSecurityCode.Text = string.Empty;
                trCardSecurityCode.Visible = false;
                trCardExpirationDate.Visible = false;
                txtFirstName.Text = profile.BillingFirstName;
                txtFirstName.SetTextBoxEnabled(false);
                txtLastName.Text = profile.BillingLastName;
                txtLastName.SetTextBoxEnabled(false);
                txtBillingStreet.Text = profile.BillingStreet;
                txtBillingStreet.SetTextBoxEnabled(false);
                txtBillingCity.Text = profile.BillingCity;
                txtBillingCity.SetTextBoxEnabled(false);
                txtBillingState.Text = profile.BillingState;
                txtBillingState.SetTextBoxEnabled(false);
                txtBillingPostalCode.Text = profile.BillingZip;
                txtBillingPostalCode.SetTextBoxEnabled(false);

                var country = ProcessorVars.RegistryCache.Countries.Values.FirstOrDefault(c => c.Code == profile.BillingCountry);
                if (country != null)
                {
                    ddlBillingCountry.SelectedValue  = country.Id.GetString();
                    ddlBillingCountry.Enabled = false;
                }

            }
            else
            {
                txtCardNumber.Text = string.Empty;
                txtCardNumber.SetTextBoxEnabled(true);
                txtCardSecurityCode.Text = string.Empty;
                trCardSecurityCode.Visible = true;
                trCardExpirationDate.Visible = true;
                txtFirstName.Text = string.Empty;
                txtFirstName.SetTextBoxEnabled(true);
                txtLastName.Text = string.Empty;
                txtLastName.SetTextBoxEnabled(true);
                txtBillingStreet.Text = string.Empty;
                txtBillingStreet.SetTextBoxEnabled(true);
                txtBillingCity.Text = string.Empty;
                txtBillingCity.SetTextBoxEnabled(true);
                txtBillingState.Text = string.Empty;
                txtBillingState.SetTextBoxEnabled(true);
                txtBillingPostalCode.Text = string.Empty;
                txtBillingPostalCode.SetTextBoxEnabled(true);
				ddlBillingCountry.SelectedValue = default(long).GetString();
                ddlBillingCountry.Enabled = true;
            }
        }

        public void LoadShipmentToPayFor(Shipment shipment)
        {
            StoredShipment = shipment;

            // if loading a shipment, must set load order to null
            StoredLoadOrder = null;

            var customer = shipment != null && shipment.Customer != null ? shipment.Customer : new Customer();

            pnlDemoAccount.Visible = customer.RateAndScheduleInDemo;
            pnlCreditCardPayment.Enabled = !customer.RateAndScheduleInDemo;
            pnlNonCreditCardPayment.Enabled = !customer.RateAndScheduleInDemo;
            if (customer.RateAndScheduleInDemo) return;
            
            hidCustomer.Value = customer.Id.GetString();
            if ((string.IsNullOrEmpty(customer.PaymentGatewayKey) || !customer.CanPayByCreditCard) && PaymentGatewayType != PaymentGatewayType.NotApplicable && PaymentGatewayType != PaymentGatewayType.Check)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Cannot process credit card payments for this shipment's customer. Please check the customer setup.") });
                Visible = false;
                return;
            }

            litTitle.Text = string.Format("For Shipment {0}", shipment != null ? shipment.ShipmentNumber : string.Empty);

            var amountToPay = shipment != null ? shipment.Charges.Sum(c => c.AmountDue).ToString("n2") : string.Empty;
            txtAmountPaid.Text = amountToPay;
            txtNonCreditCardAmountPaid.Text = amountToPay;

            var location = customer != null && customer.Locations.Any(l => l.MainBillToLocation)
                               ? customer.Locations.First(l => l.MainBillToLocation)
                               : new CustomerLocation();
            txtBillingCity.Text = location.City;
            txtBillingPostalCode.Text = location.PostalCode;
            txtBillingState.Text = location.State;
            txtBillingStreet.Text = string.Format("{0} {1}", location.Street1, location.Street2);
            ddlBillingCountry.SelectedValue = location.CountryId.GetString();
            hidCustomer.Value = customer.Id.GetString();
            if (PaymentGatewayType != PaymentGatewayType.NotApplicable && PaymentGatewayType != PaymentGatewayType.Check
                && ActiveUser.HasAccessTo(ViewCode.CanSeePaymentProfilesForCustomer) && LoadPaymentProfiles != null)
                LoadPaymentProfiles(this, new ViewEventArgs<long>(customer.Id));
        }

        public void LoadLoadOrderToBePaidFor(LoadOrder loadOrder)
        {
            StoredLoadOrder = loadOrder;

            // if loading a load order, must set shipment to null
            StoredShipment = null;

            var customer = loadOrder != null && loadOrder.Customer != null ? loadOrder.Customer : new Customer();

            pnlDemoAccount.Visible = customer.RateAndScheduleInDemo;
            pnlCreditCardPayment.Enabled = !customer.RateAndScheduleInDemo;
            pnlNonCreditCardPayment.Enabled = !customer.RateAndScheduleInDemo;
            if (customer.RateAndScheduleInDemo) return;
            
            hidCustomer.Value = customer.Id.GetString();
            if ((string.IsNullOrEmpty(customer.PaymentGatewayKey) || !customer.CanPayByCreditCard) && PaymentGatewayType != PaymentGatewayType.NotApplicable && PaymentGatewayType != PaymentGatewayType.Check)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Cannot process credit card payments for this load order's customer. Please check the customer setup.") });
                Visible = false;
                return;
            }

            litTitle.Text = string.Format("For Load Order {0}", loadOrder != null ? loadOrder.LoadOrderNumber : string.Empty);

            var amountToPay = loadOrder != null ? loadOrder.Charges.Sum(c => c.AmountDue).ToString("n2") : string.Empty;
            txtAmountPaid.Text = amountToPay;
            txtNonCreditCardAmountPaid.Text = amountToPay;

            var location = customer != null && customer.Locations.Any(l => l.MainBillToLocation) 
                               ? customer.Locations.First(l => l.MainBillToLocation)
                               : new CustomerLocation();
            txtBillingCity.Text = location.City;
            txtBillingPostalCode.Text = location.PostalCode;
            txtBillingState.Text = location.State;
            txtBillingStreet.Text = string.Format("{0} {1}", location.Street1, location.Street2);
			ddlBillingCountry.SelectedValue = location.CountryId.GetString();
            if (PaymentGatewayType != PaymentGatewayType.NotApplicable && PaymentGatewayType != PaymentGatewayType.Check
                && ActiveUser.HasAccessTo(ViewCode.CanSeePaymentProfilesForCustomer) && LoadPaymentProfiles != null)
                LoadPaymentProfiles(this, new ViewEventArgs<long>(customer.Id));
        }

        public void LoadInvoicesToBePaidFor(List<Invoice> invoices)
        {
            if (invoices == null || !invoices.Any())
            {
                DisplayMessages(new []{ValidationMessage.Error("No invoices were selected to be paid for.")});
                return;
            }

            if (invoices.Any(i => !i.Posted))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Cannot pay for invoices that are not yet posted.") });
                return;
            }

            if (invoices.Any(i => i.InvoiceType == InvoiceType.Credit))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Cannot pay for invoices that are of type 'Credit'.") });
                return;
            }

            var customer = invoices.First().CustomerLocation.Customer;

            pnlDemoAccount.Visible = customer.RateAndScheduleInDemo;
            pnlCreditCardPayment.Enabled = !customer.RateAndScheduleInDemo;
            pnlNonCreditCardPayment.Enabled = !customer.RateAndScheduleInDemo;
            if (customer.RateAndScheduleInDemo) return;

            hidCustomer.Value = customer.Id.GetString();
            if ((string.IsNullOrEmpty(customer.PaymentGatewayKey) || !customer.CanPayByCreditCard) && PaymentGatewayType != PaymentGatewayType.NotApplicable && PaymentGatewayType != PaymentGatewayType.Check)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Cannot process credit card payments for this shipment's customer. Please check the customer setup.") });
                Visible = false;
                return;
            }

            var title = string.Format("For Invoices {0}", invoices.Select(i => i.InvoiceNumber).ToArray().CommaJoin());
            if (title.Length > 100) title = string.Format("{0}<abbr title='{1}'>...</abbr>", title.Substring(0, 100), invoices.Select(i => i.InvoiceNumber).ToArray().CommaJoin());
            litTitle.Text = title;

            var amountToPay = (invoices.Sum(i => i.Details.Sum(d => d.AmountDue)) - invoices.Sum(i => i.PaidAmount)).ToString();
            txtAmountPaid.Text = amountToPay;
            txtNonCreditCardAmountPaid.Text = amountToPay;

            var location = customer.Locations.Any(l => l.MainBillToLocation)
                               ? customer.Locations.First(l => l.MainBillToLocation)
                               : new CustomerLocation();
            txtBillingCity.Text = location.City;
            txtBillingPostalCode.Text = location.PostalCode;
            txtBillingState.Text = location.State;
            txtBillingStreet.Text = string.Format("{0} {1}", location.Street1, location.Street2);
            ddlBillingCountry.SelectedValue = location.CountryId.GetString();
            hidCustomer.Value = customer.Id.GetString();
            if (PaymentGatewayType != PaymentGatewayType.NotApplicable && PaymentGatewayType != PaymentGatewayType.Check && ActiveUser.HasAccessTo(ViewCode.CanSeePaymentProfilesForCustomer) && LoadPaymentProfiles != null)
                LoadPaymentProfiles(this, new ViewEventArgs<long>(customer.Id));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new PaymentEntryHandler(this).Initialize();

            if (IsPostBack) return;

            ddlCardExpirationMonth.DataSource = DateUtility.BuildMonthList().Select(m => new ViewListItem(m, m));
            ddlCardExpirationMonth.DataBind();

            if (ddlCardExpirationMonth.ContainsValue(DateTime.Now.Month.ToString()))
                ddlCardExpirationMonth.SelectedValue = DateTime.Now.Month.ToString();

            ddlCardExpirationYear.DataSource = DateUtility.BuildPresentAndFutureYearsList(WebApplicationConstants.PaymentGatewayExpirationDateYearsFromNow).Select(m => new ViewListItem(m, m));
            ddlCardExpirationYear.DataBind();
        }


        protected void OnPayClicked(object sender, EventArgs e)
        {
            var shipment = StoredShipment;
            var loadOrder = StoredLoadOrder;
            var customer = new Customer(hidCustomer.Value.ToLong());

            var paymentInfo = new PaymentInformation
                {
                    Description = shipment != null ?  shipment.ShipmentNumber :  loadOrder != null ? loadOrder.LoadOrderNumber : string.Empty,
                    BillingAddress = txtBillingStreet.Text,
                    BillingCity = txtBillingCity.Text,
                    BillingCountry = new Country(ddlBillingCountry.SelectedValue.ToLong()).Code,
                    BillingFirstName = txtFirstName.Text,
                    BillingLastName = txtLastName.Text,
                    BillingState = txtBillingState.Text,
                    BillingZip = txtBillingPostalCode.Text,
                    CardNumber = txtCardNumber.Text,
                    ChargeAmount = txtAmountPaid.Text.ToDecimal(),
                    CustomerPaymentProfileId = ddlSavedCreditCards.SelectedValue,
                    CustomerPaymentGatewayKey = customer.PaymentGatewayKey,
                    CustomerNumber = customer.CustomerNumber,
                    ExpMonth = ddlCardExpirationMonth.SelectedValue.ToInt(),
                    ExpYear = ddlCardExpirationYear.SelectedValue.ToInt(),
                    SecurityCode = txtCardSecurityCode.Text,
                    CheckNumber = txtCheckNumber.Text,
                    NameOnCard = txtNameOnCard.Text
                };

            if (CustomAuthorizeAndCapturePayment != null)
            {
                CustomAuthorizeAndCapturePayment(this, new ViewEventArgs<PaymentInformation>(paymentInfo));
                return;
            }

            var miscReceipt = new MiscReceipt
            {
                AmountPaid = paymentInfo.ChargeAmount,
                PaymentDate = DateTime.Now,
                CustomerId = customer.Id,
                PaymentGatewayType = PaymentGatewayType,
                ShipmentId = shipment != null ? shipment.Id : default(long),
                LoadOrderId = loadOrder != null ? loadOrder.Id : default(long),
                TenantId = customer.TenantId,
                Reversal = false,
                UserId = ActiveUser.Id,
                GatewayTransactionId = PaymentGatewayType == PaymentGatewayType.Check ? paymentInfo.CheckNumber : string.Empty,
                MiscReceiptApplications = new List<MiscReceiptApplication>(),
                NameOnCard = paymentInfo.NameOnCard,
                PaymentProfileId = ddlSavedCreditCards.SelectedValue
            };

            hidFlag.Value = SaveFlag;

            if (AuthorizeAndCapturePayment != null)
                AuthorizeAndCapturePayment(this, new ViewEventArgs<MiscReceipt, PaymentInformation>(miscReceipt, paymentInfo));
        }

        protected void OnNonCreditCardPayClicked(object sender, EventArgs e)
        {
            var shipment = StoredShipment;
            var loadOrder = StoredLoadOrder;

            var miscReceipt = new MiscReceipt
            {
                AmountPaid = txtNonCreditCardAmountPaid.Text.ToDecimal(),
                PaymentDate = DateTime.Now,
                CustomerId = hidCustomer.Value.ToLong(),
                PaymentGatewayType = PaymentGatewayType,
                ShipmentId = shipment != null ? shipment.Id : default(long),
                LoadOrderId = loadOrder != null ? loadOrder.Id : default(long),
                TenantId = ActiveUser.TenantId,
                Reversal = false,
                UserId = ActiveUser.Id,
                GatewayTransactionId = PaymentGatewayType == PaymentGatewayType.Check ? txtCheckNumber.Text : string.Empty,
                MiscReceiptApplications = new List<MiscReceiptApplication>(),
                NameOnCard = string.Empty,
                PaymentProfileId = string.Empty
            };

            hidFlag.Value = SaveFlag;

            if (SaveMiscReceipt != null)
                SaveMiscReceipt(this, new ViewEventArgs<MiscReceipt>(miscReceipt));
        }

        protected void OnCloseClicked(object sender, EventArgs e)
        {
            Clear();

            if (Close != null)
                Close(this, new EventArgs());
        }


        protected void OnSavedCreditCardsSelectedIndexChanged(object sender, EventArgs e)
        {
            if (LoadPaymentProfile != null)
                LoadPaymentProfile(this, new ViewEventArgs<long, string>(hidCustomer.Value.ToLong(), ddlSavedCreditCards.SelectedValue));
        }

        protected void OnManagePaymentProfilesClicked(object sender, EventArgs e)
        {
            pnlManagePaymentProfiles.Visible = true;
            pnlManagePaymentProfilesDimScreen.Visible = true;

            var customer = new Customer(hidCustomer.Value.ToLong());
            litPaymentProfileCustomerTitle.Text = string.Format("{0} - {1}", customer.CustomerNumber, customer.Name);

            ppmPaymentProfileManager.LoadCustomerToManagePaymentProfiles(customer);
        }

        protected void OnCloseManagePaymentProfilesClicked(object sender, ImageClickEventArgs e)
        {
            pnlManagePaymentProfiles.Visible = false;
            pnlManagePaymentProfilesDimScreen.Visible = false;
            
            if (PaymentGatewayType != PaymentGatewayType.NotApplicable && PaymentGatewayType != PaymentGatewayType.Check && ActiveUser.HasAccessTo(ViewCode.CanSeePaymentProfilesForCustomer) && LoadPaymentProfiles != null)
                LoadPaymentProfiles(this, new ViewEventArgs<long>(hidCustomer.Value.ToLong()));

            DisplayPaymentProfile(new PaymentGatewayPaymentProfile());
        }

        protected void OnPaymentProfileManagerProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
        {
            DisplayMessages(e.Argument);
        }
    }
}