﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls {
    
    
    public partial class PaymentRefundOrVoidControl {
        
        /// <summary>
        /// pnlPaymentRefund control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlPaymentRefund;
        
        /// <summary>
        /// litHeaderShipmentOrLoadOrderNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litHeaderShipmentOrLoadOrderNumber;
        
        /// <summary>
        /// ibtnClosePaymentRefund control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ibtnClosePaymentRefund;
        
        /// <summary>
        /// chkSelectAllRecords control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.AltUniformCheckBox chkSelectAllRecords;
        
        /// <summary>
        /// rptMiscReceipts control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater rptMiscReceipts;
        
        /// <summary>
        /// btnRefundVoidMiscReceipts control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnRefundVoidMiscReceipts;
        
        /// <summary>
        /// btnClosePaymentRefund control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnClosePaymentRefund;
        
        /// <summary>
        /// pnlPaymentRefundDimScreen control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlPaymentRefundDimScreen;
        
        /// <summary>
        /// hidLoadOrderId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidLoadOrderId;
        
        /// <summary>
        /// hidShipmentId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidShipmentId;
    }
}
