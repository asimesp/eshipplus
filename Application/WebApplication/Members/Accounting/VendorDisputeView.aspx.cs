﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Notifications;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class VendorDisputeView : MemberPageBaseWithPageStore, IVendorDisputeView
    {
        private const string DisputeComponentsHeader = "Dispute Components";

        private const string ResolveArgs = "Resolve";

        private const string SaveFlag = "S";
        private const string DeleteFlag = "D";

        public static string PageAddress
        {
            get { return "~/Members/Accounting/VendorDisputeView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.VendorDispute; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<VendorDispute>> Save;
        public event EventHandler<ViewEventArgs<VendorDispute>> Delete;
        public event EventHandler<ViewEventArgs<VendorDispute>> Resolve;
        public event EventHandler<ViewEventArgs<VendorDispute>> Lock;
        public event EventHandler<ViewEventArgs<VendorDispute>> UnLock;
	    public event EventHandler<ViewEventArgs<VendorDispute>> LoadAuditLog;

	    public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<VendorDispute>(new VendorDispute(hidVendorDisputeId.Value.ToLong(), false)));

                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }
        public void OnVendorDisputeResolvedComplete(VendorDispute dispute)
        {
	        var charges =
		        dispute.VendorDisputeDetails.Select(
			        dd =>
			        new ChargeSearch().FetchChargesByTypeAndId(dd.ChargeLineId.ToString(), dd.ComponentNumber,
			                                                   dd.ChargeLineIdType == ChargeLineType.ServiceTicket
				                                                   ? ChargeType.ServiceTicket
				                                                   : ChargeType.Shipment)).ToList();


	        this.NotifyVendorDisputeResolved(charges, dispute);
        }

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
		{
			auditLogs.Load(logs);
		}

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(VendorDispute vendorDispute)
        {
            LoadVendorDispute(vendorDispute);

            SetEditStatus(false);

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<VendorDispute>(vendorDispute));
        }

        private List<ToolbarMoreAction> ToolbarMoreActions(bool enabled)
        {
            var actions = new List<ToolbarMoreAction>();

            var resolve = new ToolbarMoreAction
            {
                CommandArgs = ResolveArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Mark Dispute as Resolved",
                NavigationUrl = string.Empty
            };

            if (enabled)
            {
                actions.Add(resolve);
            }

            memberToolBar.ShowMore = actions.Any();

            return actions;
        }

        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlDisputeComponents.Enabled = enabled;

            memberToolBar.EnableSave = enabled;
            memberToolBar.EnableImport = enabled;
            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
            hidEditStatus.Value = enabled.ToString();
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(enabled));
        }

        private void FillResolvedContactInformation(VendorDispute vendorDispute)
        {
            lnkResolvedUserPhone.InnerHtml = vendorDispute.ResolvedByUser != null ? vendorDispute.ResolvedByUser.Phone : ActiveUser.Phone;
            lnkResolvedUserPhone.HRef = string.Format("tel:{0}", vendorDispute.ResolvedByUser != null ? vendorDispute.ResolvedByUser.Phone : ActiveUser.Phone);
            lnkResolvedUserMobile.InnerHtml = vendorDispute.ResolvedByUser != null ? vendorDispute.ResolvedByUser.Mobile : ActiveUser.Mobile;
            lnkResolvedUserMobile.HRef = string.Format("tel:{0}", vendorDispute.ResolvedByUser != null ? vendorDispute.ResolvedByUser.Mobile : ActiveUser.Mobile);
            lnkResolvedUserFax.InnerHtml = vendorDispute.ResolvedByUser != null ? vendorDispute.ResolvedByUser.Fax : ActiveUser.Fax;
            lnkResolvedUserFax.HRef = string.Format("tel:{0}", vendorDispute.ResolvedByUser != null ? vendorDispute.ResolvedByUser.Fax : ActiveUser.Fax);
            lnkResolvedUserEmail.InnerHtml = vendorDispute.ResolvedByUser != null ? vendorDispute.ResolvedByUser.Email : ActiveUser.Email;
            lnkResolvedUserEmail.HRef = string.Format("mailto:{0}", vendorDispute.ResolvedByUser != null ? vendorDispute.ResolvedByUser.Email : ActiveUser.Email);
        }

        private void LoadVendorDispute(VendorDispute vendorDispute)
        {
            var isNew = vendorDispute.IsNew;
            hidVendorDisputeId.Value = vendorDispute.Id.ToString();
            pnlResolvedAlert.Visible = vendorDispute.IsResolved;

            txtDisputeReferenceNumber.Text = isNew ? string.Empty : vendorDispute.DisputerReferenceNumber;
            txtDateCreated.Text = isNew ? DateTime.Now.FormattedShortDate() : vendorDispute.DateCreated.FormattedShortDate();
            txtResolutionDate.Text = isNew || vendorDispute.ResolutionDate.IsSystemEarliestDate() ? string.Empty : vendorDispute.ResolutionDate.FormattedShortDate();
            txtCreatedBy.Text = vendorDispute.CreatedByUser != null ? vendorDispute.CreatedByUser.Username : string.Empty;
            txtResolvedBy.Text = vendorDispute.ResolvedByUser == null ? string.Empty : vendorDispute.ResolvedByUser.Username;
            txtResolutionComments.Text = vendorDispute.ResolutionComments;
	        txtDispuetComments.Text = vendorDispute.DisputeComments;

            hidVendorId.Value = vendorDispute.VendorId.ToString();
            txtVendorNumber.Text = vendorDispute.Vendor != null ? vendorDispute.Vendor.VendorNumber : string.Empty;
            txtVendorName.Text = vendorDispute.Vendor != null ? vendorDispute.Vendor.Name : string.Empty;

            lnkUserPhone.InnerHtml = vendorDispute.CreatedByUser != null ? vendorDispute.CreatedByUser.Phone : string.Empty;
            lnkUserPhone.HRef = string.Format("tel:{0}", vendorDispute.CreatedByUser != null ? vendorDispute.CreatedByUser.Phone : string.Empty);
            lnkUserMobile.InnerHtml = vendorDispute.CreatedByUser != null ? vendorDispute.CreatedByUser.Mobile : string.Empty;
            lnkUserMobile.HRef = string.Format("tel:{0}", vendorDispute.CreatedByUser != null ? vendorDispute.CreatedByUser.Mobile : string.Empty);
            lnkUserFax.InnerHtml = vendorDispute.CreatedByUser != null ? vendorDispute.CreatedByUser.Fax : string.Empty;
            lnkUserFax.HRef = string.Format("tel:{0}", vendorDispute.CreatedByUser != null ? vendorDispute.CreatedByUser.Fax : string.Empty);
            lnkUserEmail.InnerHtml = vendorDispute.CreatedByUser != null ? vendorDispute.CreatedByUser.Email : string.Empty;
            lnkUserEmail.HRef = string.Format("mailto:{0}", vendorDispute.CreatedByUser != null ? vendorDispute.CreatedByUser.Email : string.Empty);

            if (vendorDispute.IsResolved)
            {
                FillResolvedContactInformation(vendorDispute);
            }

            //Dispute Components
            lstDisputeComponents.DataSource = vendorDispute.VendorDisputeDetails;
            lstDisputeComponents.DataBind();
            tabDisputeComponents.HeaderText = vendorDispute.VendorDisputeDetails.BuildTabCount(DisputeComponentsHeader);
			
        }

        private VendorDispute UpdateVendorDispute()
        {
            var vendorDisputeId = hidVendorDisputeId.Value.ToLong();
            var vendorDispute = new VendorDispute(vendorDisputeId, vendorDisputeId != default(long));

            vendorDispute.ResolutionComments = txtResolutionComments.ToTextBox().Text;
            vendorDispute.IsResolved = !string.IsNullOrEmpty(txtResolvedBy.Text);
	        vendorDispute.DisputeComments = txtDispuetComments.ToTextBox().Text;
            if (vendorDispute.IsResolved)
            {
                vendorDispute.ResolutionDate = DateTime.Now;
                vendorDispute.ResolvedByUser = ActiveUser;

                FillResolvedContactInformation(vendorDispute);
            }

            //Update From View
            UpdateDisputeCoponentsFromView(vendorDispute);

            return vendorDispute;
        }

        private void UpdateDisputeCoponentsFromView(VendorDispute vendorDispute)
        {
            var vendorComponents = lstDisputeComponents.Items
                .Select(i =>
                {
                    var id = (i.FindControl("hidDisputeComponentId")).ToCustomHiddenField().Value.ToLong();
                    return new VendorDisputeDetail(id, id != default(long))
                    {
                        OriginalChargeAmount = i.FindControl("txtOriginalChargeAmount").ToTextBox().Text.ToDecimal(),
                        RevisedChargeAmount = i.FindControl("txtRevisedChargeAmount").ToTextBox().Text.ToDecimal(),
                    };
                })
                .ToList();

            vendorDispute.VendorDisputeDetails = vendorComponents;
        }

        protected void MarkAsResolved()
        {
            txtResolvedBy.Text = ActiveUser.Username;
            txtResolutionDate.Text = DateTime.Now.FormattedShortDate();

            var vendorDisputeId = hidVendorDisputeId.Value.ToLong();
            var vendorDispute = new VendorDispute(vendorDisputeId, vendorDisputeId != default(long));

            if (Lock == null || vendorDispute.IsNew || vendorDispute.IsResolved) return;

            FillResolvedContactInformation(vendorDispute);
            hidFlag.Value = SaveFlag;

            vendorDispute = UpdateVendorDispute();

            if (Resolve != null)
                Resolve(this, new ViewEventArgs<VendorDispute>(vendorDispute));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<VendorDispute>(vendorDispute));
        }

        private void ProcessTransferredRequest(VendorDispute vendorDispute, bool disableEdit = false)
        {
            if (vendorDispute == null) return;

			if (vendorDispute.IsNew || vendorDispute.VendorId == default(long))
			{
				DisplayMessages(new[] { ValidationMessage.Error("Vendor dispute does not exist.") });
				return;
			}

            LoadVendorDispute(vendorDispute);

            if (Lock != null && !vendorDispute.IsNew && Access.Modify && !disableEdit)
            {
                SetEditStatus(true);
                Lock(this, new ViewEventArgs<VendorDispute>(vendorDispute));
            }
            else SetEditStatus(false);

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<VendorDispute>(vendorDispute));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorDisputeHandler(this).Initialize();

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));

            if (IsPostBack) return;

            var vendorDispute = new VendorDispute(default(long));
            LoadVendorDispute(vendorDispute);

            SetEditStatus(false);

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<VendorDispute>(vendorDispute));

            litErrorMessages.Text = string.Empty;

            if (Session[WebApplicationConstants.TransferDisputeVendorId] != null)
            {
                var disableEdit = Session[WebApplicationConstants.DisableEditModeOnTransfer].ToBoolean();

                ProcessTransferredRequest(new VendorDispute(Session[WebApplicationConstants.TransferDisputeVendorId].ToLong()), disableEdit);
                Session[WebApplicationConstants.TransferDisputeVendorId] = null;
                Session[WebApplicationConstants.DisableEditModeOnTransfer] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferDisputeVendorId]))
                ProcessTransferredRequest(new VendorDispute(Request.QueryString[WebApplicationConstants.TransferDisputeVendorId].UrlTextDecrypt().ToLong()), true);


            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new VendorDisputeSearch().FetchVendorDisputeByDisputerReferenceNumber(Request.QueryString[WebApplicationConstants.TransferNumber].GetString(), ActiveUser.TenantId), true);

			
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var vendorDispute = new VendorDispute(hidVendorDisputeId.Value.ToLong(), false);
			if(vendorDispute.IsNew || vendorDispute.VendorId == default(long)) return;
            if (Lock == null || vendorDispute.IsNew || vendorDispute.IsResolved) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<VendorDispute>(vendorDispute));

            LoadVendorDispute(vendorDispute);

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<VendorDispute>(vendorDispute));
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var vendorDispute = UpdateVendorDispute();
            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<VendorDispute>(vendorDispute));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<VendorDispute>(vendorDispute));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var vendorDispute = new VendorDispute(hidVendorDisputeId.Value.ToLong(), false);
			if (vendorDispute.IsNew || vendorDispute.VendorId == default(long)) return;
            hidFlag.Value = DeleteFlag;

            if (Delete != null)
                Delete(this, new ViewEventArgs<VendorDispute>(vendorDispute));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<VendorDispute>(vendorDispute));

            vendorDisputeFinder.Reset();
        }

        protected void OnToolbarUnlockClicked(object sender, EventArgs e)
        {
            var vendorDispute = new VendorDispute(hidVendorDisputeId.Value.ToLong(), false);
            if (UnLock != null && !vendorDispute.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<VendorDispute>(vendorDispute));
            }

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<VendorDispute>(vendorDispute));
        }

        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case ResolveArgs:
                    MarkAsResolved();
                    break;
            }
        }

        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            vendorDisputeFinder.Visible = true;
        }

        protected void OnVendorDisputeFinderItemSelected(object sender, ViewEventArgs<VendorDispute> e)
        {
            if (hidVendorDisputeId.Value.ToLong() != default(long))
            {
                var oldVendorDispute = new VendorDispute(hidVendorDisputeId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<VendorDispute>(oldVendorDispute));
            }

            var vendorDispute = e.Argument;

            LoadVendorDispute(vendorDispute);

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<VendorDispute>(vendorDispute));

            vendorDisputeFinder.Visible = false;

            if (vendorDisputeFinder.OpenForEdit && Lock != null && !vendorDispute.IsResolved)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<VendorDispute>(vendorDispute));
            }
            else
            {
                SetEditStatus(false);
            }

            litErrorMessages.Text = string.Empty;
        }

        protected void OnVendorDisputeFinderItemCancelled(object sender, EventArgs e)
        {
            vendorDisputeFinder.Visible = false;
        }
    }
}