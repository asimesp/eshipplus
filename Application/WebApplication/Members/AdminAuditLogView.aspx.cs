﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    public partial class AdminAuditLogView : AdminMemberPageBase, IAdminAuditLogView 
    {
        private const string ExportFlag = "ExportFlag";

        protected const int DescriptionCharacterLimit = 1100;

        private string ResultFlag
        {
            get { return ViewState["ResultFlag"].GetString(); }
            set { ViewState["ResultFlag"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/AdminAuditLogView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.AdminAuditLog; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.UtilitiesBlue; }
        }

        public event EventHandler<ViewEventArgs<AdminAuditLogSearchCriteria>> Search;


        public void DisplaySearchResult(List<AuditLogsViewSearchDto> adminAuditLogs)
        {
            switch (ResultFlag)
            {
                case ExportFlag:
                    DoExport(adminAuditLogs);
                    break;
                default:
                    foreach (var adminAuditLog in adminAuditLogs)
                        adminAuditLog.Description = adminAuditLog.Description.Length > DescriptionCharacterLimit
                                                   ? adminAuditLog.Description.Substring(0, DescriptionCharacterLimit)
                                                   : adminAuditLog.Description;
                    cseDataUpdate.DataSource = adminAuditLogs;
                    cseDataUpdate.DataBind();
                    litRecordCount.Text = adminAuditLogs.BuildRecordCount();
                    break;
            }
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak,
                                             messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        
        private void DoExport(IEnumerable<AuditLogsViewSearchDto> auditLogs)
        {
            foreach (var auditLog in auditLogs)
                auditLog.Description = auditLog.Description.Replace(WebApplicationConstants.Tab, string.Empty);

            var q = auditLogs.Select(a => a.ToString().Replace(Environment.NewLine, string.Empty)).ToList();
            q.Insert(0, AuditLogsViewSearchDto.Header());
            Response.Export(q.ToArray().NewLineJoin());
        }

        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new AdminAuditLogSearchCriteria
                {
                    Parameters = GetCurrentRunParameters(false)
                });
        }

        private void DoSearch(AdminAuditLogSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<AdminAuditLogSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new AdminAuditLogHandler(this);
            handler.Initialize();

            if (IsPostBack) return;

            lstFilterParameters.DataSource = CoreSearchFields.DefaultAdminAuditLogs.Select(f => f.ToParameterColumn()).ToList();
            lstFilterParameters.DataBind();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            cseDataUpdate.ResetLoadCount();
            ResultFlag = string.Empty;
            DoSearchPreProcessingThenSearch();
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            ResultFlag = ExportFlag;
            DoSearchPreProcessingThenSearch();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters
                .Items
                .Select(i => ((ReportRunParameterControl) i.FindControl("reportRunParameterControl"))
                                 .RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = CoreSearchFields.AdminAuditLogs.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }

        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(CoreSearchFields.AdminAuditLogs);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnGoToRecord(object sender, EventArgs e)
        {
            this.GoToRecord2(hidGoToEntityCode.Value, hidGoToEntityId.Value);
        }

        [ScriptMethod]
        [WebMethod]
        public static object RetrieveEntityDetails(string entityCode, string entityId)
        {
            return AuditLogView.RetrieveEntityDetails(entityCode, entityId);
        }

        [ScriptMethod]
        [WebMethod]
        public static object RetrieveFullDescription(string id)
        {
            var auditLog = new AdminAuditLog(id.ToLong());
            return auditLog.KeyLoaded ? auditLog.Description.Replace(Environment.NewLine, "<br/>") : "Admin Audit Log record does not exist";
        }
    }
}