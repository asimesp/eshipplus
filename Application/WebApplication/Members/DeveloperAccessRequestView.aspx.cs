﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    public partial class DeveloperAccessRequestView : MemberPageBase, IDeveloperAccessRequestView
    {
        private const string SupportFlag = "SF";

        public static string PageAddress
        {
            get { return "~/Members/DeveloperAccessRequestView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.DeveloperAccessRequest; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.UtilitiesBlue; }
        }

        private bool ClosePopUp
        {
            get { return ViewState["ClosePopUp"].ToBoolean(); }
            set { ViewState["ClosePopUp"] = value; }
        }

        public List<string> SupportAreas
        {
            set
            {
                ddlCategory.DataSource = value;
                ddlCategory.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Save;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            ClosePopUp = !messages.HasErrors() || messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new DeveloperAccessRequestHandler(this);
            handler.Initialize();

            var siteRoot = Request.ResolveSiteRootWithHttp();
            litPartnerAuthenticationSiteRoot.Text = siteRoot;
            litRequestURLSiteRoot.Text = siteRoot;
            litRetrievePasswordSiteRoot.Text = siteRoot;
            litDocumentAccessLinkSiteRoot.Text = siteRoot;

            var defaultCustomer = ActiveUser.DefaultCustomer;
            var request = new DeveloperAccessRequestSearch().FetchRequest(defaultCustomer.Id, ActiveUser.TenantId);

            pnlNoAccess.Visible = request == null;

            if (request != null)
            {
                prodAccessRequest.Visible = request.ProductionAccess;
                prodAccessRequest2.Visible = request.ProductionAccess;
                pnlGrantedAccess.Visible = request.AccessGranted;
                pnlPendingAccess.Visible = !request.AccessGranted;

                if (request.AccessGranted && !string.IsNullOrEmpty(request.TestInformation))
                {
                    TestAccessDetails.Visible = true;
                    litTestInfo.Text = request.TestInformation.ReplaceNewLineWithHtmlBreak();

                }

                lbtnRequestProductionAccess.Visible = !request.ProductionAccess;

                if (request.ProductionAccess && defaultCustomer.Communication != null && defaultCustomer.Communication.EnableWebServiceAPIAccess)
                {
                    litGuidInfo.Text = string.Format(
                        "<div style='display:inline-block;width:400px;text-indent:2em;'>Customer: <span style='font-weight:normal;'>{0} - {1}</span></div> Connect Guid: <span style='font-weight:normal;'>{2}</span><br/>",
                        defaultCustomer.CustomerNumber,
                        defaultCustomer.Name,
                        defaultCustomer.Communication.ConnectGuid);

                    litDocumentAccessKeys.Text = string.Format(
	                    "<div style='display:inline-block;width:400px;text-indent:2em;'>Customer: <span style='font-weight:normal;'>{0} - {1}</span></div> Document Access Key: <span style='font-weight:normal;'>{2}</span><br/>",
	                    defaultCustomer.CustomerNumber,
	                    defaultCustomer.Name,
	                    defaultCustomer.Communication.DocumentLinkAccessKey);

                    var shipAs = ActiveUser.RetrieveShipAsCustomers();

                    foreach (var customer in shipAs.Where(c => c.Communication != null && c.Communication.EnableWebServiceAPIAccess))
                    {
                        litGuidInfo.Text += string.Format("<div style='display:inline-block;width:400px;text-indent:2em;'>Customer: <span style='font-weight:normal;'>{0} - {1}</span></div> Connect Guid: <span style='font-weight:normal;'>{2}</span><br/>",
                            customer.CustomerNumber, customer.Name, customer.Communication.ConnectGuid);
                    }

                    litGuidInfo.Text += WebApplicationConstants.HtmlBreak;

                    foreach (var customer in shipAs.Where(c => c.Communication != null && c.Communication.DocumentLinkAccessKey != Guid.Empty))
                    {
	                    litDocumentAccessKeys.Text += string.Format("<div style='display:inline-block;width:400px;text-indent:2em;'>Customer: <span style='font-weight:normal;'>{0} - {1}</span></div> Document Access Key: <span style='font-weight:normal;'>{2}</span><br/>",
	                                                                customer.CustomerNumber, customer.Name, customer.Communication.DocumentLinkAccessKey);
                    }

                    litDocumentAccessKeys.Text += WebApplicationConstants.HtmlBreak;
                }
            }

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
            if (ClosePopUp) ClosePopup();
        }

        private void ClosePopup()
        {
            if (pnlDimScreen.Visible) pnlDimScreen.Visible = false;
            if (pnlEmail.Visible) pnlEmail.Visible = false;
            if (pnlRequestAccess.Visible) pnlRequestAccess.Visible = false;
            hidEmailFlag.Value = null;
        }

        private void OpenEmailPanel()
        {
            if (hidEmailFlag.Value == SupportFlag)
            {
                pnlSupport.Visible = true;
                litEmailTitle.Text = "DEVELOPER SUPPORT EMAIL";
            }
            else
            {
                pnlSupport.Visible = false;
                litEmailTitle.Text = "UPDATE DEVELOPER ACCESS";
            }

            pnlDimScreen.Visible = true;
            pnlEmail.Visible = true;
        }

        protected void OnRequestAccessClick(object sender, EventArgs e)
        {
            txtUserName.Text = string.Format("{0} {1}", ActiveUser.FirstName, ActiveUser.LastName);
            txtCustomer.Text = string.Format("{0} - {1}", ActiveUser.DefaultCustomer.CustomerNumber, ActiveUser.DefaultCustomer.Name);
            pnlRequestAccess.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnSaveDeveloperAccessRequestClick(object sender, EventArgs e)
        {
            var request = new DeveloperAccessRequest
            {
                TenantId = ActiveUser.TenantId,
                ContactName = txtContactName.Text,
                ContactEmail = txtContactEmail.Text.StripSpacesFromEmails(),
                DateCreated = DateTime.Now,
                Customer = ActiveUser.DefaultCustomer
            };

            if (Save != null)
                Save(this, new ViewEventArgs<DeveloperAccessRequest>(request));

            this.NotifyDeveloperAccessRequest(request);
        }

        protected void OnCloseDeveloperAccessRequestClicked(object sender, EventArgs e)
        {
            ClosePopup();
        }


        protected void OnUpdateRequestClick(object sender, EventArgs e)
        {
            OpenEmailPanel();
        }

        protected void OnRequestDeveloperSupportClicked(object sender, EventArgs e)
        {
            hidEmailFlag.Value = SupportFlag;
            OpenEmailPanel();
        }

        protected void OnSendAccessRequestEmailClick(object sender, EventArgs e)
        {
            var request = new DeveloperAccessRequestSearch().FetchRequest(ActiveUser.DefaultCustomer.Id, ActiveUser.TenantId);
            var message = ediAccountRequestUpdate.Content;

            if (hidEmailFlag.Value == SupportFlag)
                this.NotifyDeveloperAccessSupportRequest(request, message, ddlCategory.SelectedValue);
            else
                this.NotifyDeveloperAccessRequestUpdated(request, message);

            ClosePopup();
        }

        protected void OnCloseAccessRequestEmailClicked(object sender, EventArgs e)
        {
            ClosePopup();
        }


        protected void OnRequestProductionAccessClicked(object sender, EventArgs e)
        {
            var request = new DeveloperAccessRequestSearch().FetchRequest(ActiveUser.DefaultCustomer.Id,
                                                                          ActiveUser.TenantId);
            this.NotifyProductionAccessRequest(request);

            DisplayMessages(new[] { ValidationMessage.Information("A Production access request has been sent") });
        }
    }
}
