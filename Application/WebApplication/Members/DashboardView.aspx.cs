﻿using System;
using System.Linq;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public partial class DashboardView : MemberPageBase
	{
		public override ViewCode PageCode
		{
			get { return ViewCode.Dashboard; }
		}

		public override string SetPageIconImage
		{
			set { }
		}

		public static string PageAddress
		{
			get { return "~/Members/DashboardView.aspx"; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return;

            var operations = DashboardItemConfiguration.Operations2(ActiveUser.Tenant)
				.Where(p => ActiveUser.HasAccessTo(p.IconName))
				.ToList();
			lstOperationsDashboard.DataSource = operations;
			lstOperationsDashboard.DataBind();

			var businessIntelligence = DashboardItemConfiguration.BusinessIntelligence2(ActiveUser.Tenant)
                .Where(p => ActiveUser.HasAccessTo(p.IconName))
				.ToList();
			lstBusinessIntelligenceDashboard.DataSource = businessIntelligence;
			lstBusinessIntelligenceDashboard.DataBind();

			var utilities = DashboardItemConfiguration.Utilities2(ActiveUser.Tenant)
                .Where(p => ActiveUser.HasAccessTo(p.IconName))
				.ToList();
			lstUtilitiesDashboard.DataSource = utilities;
			lstUtilitiesDashboard.DataBind();

			var registry = DashboardItemConfiguration.Registry2(ActiveUser.Tenant)
                .Where(p => ActiveUser.HasAccessTo(p.IconName))
				.ToList();
			lstRegistryDashboard.DataSource = registry;
			lstRegistryDashboard.DataBind();

			var rating = DashboardItemConfiguration.Rating2(ActiveUser.Tenant)
                .Where(p => ActiveUser.HasAccessTo(p.IconName))
				.ToList();
			lstRatingDashboard.DataSource = rating;
			lstRatingDashboard.DataBind();

			var connect = DashboardItemConfiguration.Connect2(ActiveUser.Tenant)
                .Where(p => ActiveUser.HasAccessTo(p.IconName))
				.ToList();
			lstConnectDashboard.DataSource = connect;
			lstConnectDashboard.DataBind();

			var finance = DashboardItemConfiguration.Accounting2(ActiveUser.Tenant)
                .Where(p => ActiveUser.HasAccessTo(p.IconName))
				.ToList();
			lstFinanceDashboard.DataSource = finance;
			lstFinanceDashboard.DataBind();

			var administration = DashboardItemConfiguration.Administration2(ActiveUser.Tenant)
                .Where(p => ActiveUser.HasAccessTo(p.IconName))
				.ToList();
			administration.AddRange(DashboardItemConfiguration.SuperAdmin2().Where(p => ActiveUser.HasAccessTo(p.IconName)));
			lstAdministrationDashboard.DataSource = administration;
			lstAdministrationDashboard.DataBind();


			pnlGroup1.Visible = businessIntelligence.Any() || connect.Any() || administration.Any();
			pnlGroup2.Visible = rating.Any() || utilities.Any();
		}
	}
}