﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Views;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class SortableExtender : UserControl
	{
		private const string EventArgument = "__EVENTARGUMENT";
		private const string EventTarget = "__EVENTTARGET";

		public string DragHandleElementId
		{
			set { hidDragHandlerId.Value = value; }
		}

		public string SortableElementWrapperId
		{
			set { hidSortableElementWrapperId.Value = value; }
		}

		public string SortableElement
		{
			set { hidSortableElementIdChildGroup.Value = value; }
		}

		public string OnUpdateRunFunction
		{
			protected get { return hidOnUpdateFunction.Value; }
			set { hidOnUpdateFunction.Value = value; }
		}

		public string UpdatePanelToExtendId
		{
			set { hidUpdatePanelToExtendControlId.Value = value; }
		}

		protected string UpdatePanelClientId
		{
			get { return Parent.FindControl(hidUpdatePanelToExtendControlId.Value).ClientID; }
		}

		public bool Enabled
		{
			get { return hidEnabled.Value.ToBoolean(); }
			set { hidEnabled.Value = value.ToString(); }
		}


		public event EventHandler<ViewEventArgs<List<int>>> ItemsReordered; 

		protected void Page_Load(object sender, EventArgs e)
		{
			if (ItemsReordered == null || Request.Params.Get(EventTarget) != UpdatePanelClientId) return;

			var args = Request.Params.Get(EventArgument);
			if (string.IsNullOrEmpty(args)) return;

			var indices = args.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).Select(arg => arg.ToInt()).ToList();
			ItemsReordered(this, new ViewEventArgs<List<int>>(indices));
		}
	}
}