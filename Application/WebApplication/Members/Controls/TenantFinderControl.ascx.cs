﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
    public partial class TenantFinderControl : MemberControlBase, ITenantFinderView
    {
        public new bool Visible
        {
            get { return pnlTenantFinderContent.Visible; }
            set
            {
                base.Visible = true;
                pnlTenantFinderContent.Visible = value;
                pnlTenantFinderDimScreen.Visible = value;
            }
        }

        public bool OpenForEdit { get { return hidEditSelected.Value.ToBoolean(); } }

        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<Tenant>> ItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<Tenant> tenants)
        {
            cseDataUpdate.DataSource = tenants.OrderBy(t => t.Name).ToList();
            cseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void Reset()
        {
            DisplaySearchResult(new List<Tenant>());
        }


        private void DoSearch(List<ParameterColumn> columns)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(columns));
        }


        private void ProcessItemSelection(long tenantId)
        {
            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<Tenant>(new Tenant(tenantId, false)));
        }

        private void ProcessSelectionCancelled()
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new TenantFinderHandler(this).Initialize();


            if (IsPostBack) return;

            lstFilterParameters.DataSource = AdminSearchFields.DefaultTenants.Select(f => f.ToParameterColumn()).ToList();
            lstFilterParameters.DataBind();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = false.ToString();
            cseDataUpdate.ResetLoadCount();
            DoSearch(GetCurrentRunParameters());
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = false.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidTenantId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnEditSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = true.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidTenantId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            ProcessSelectionCancelled();
        }


        private List<ParameterColumn> GetCurrentRunParameters()
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter()).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AdminSearchFields.Tenants.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters();

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AdminSearchFields.Tenants);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters();
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }
    }
}