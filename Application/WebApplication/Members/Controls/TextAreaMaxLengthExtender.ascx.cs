﻿using System;
using System.Web.UI;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class TextAreaMaxLengthExtender : UserControl
	{
		public string TargetControlId
		{
			set
			{
				hidTargetControlId.Value = value;
			}
		}

		public int MaxLength
		{
			set { hidMaxLength.Value = value.GetString(); }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(hidTargetControlId.Value))
			{
			    var control = Parent.FindControl(hidTargetControlId.Value).ToTextBox();
				hidTargetControlClientlId.Value = control.ClientID;

                var functionCall = string.Format("CheckAndDisplayTextAreaLength(this, {0}, '{1}');", hidMaxLength.Value, lbCharCount.ClientID);

                if (string.IsNullOrEmpty(control.Attributes["onkeyup"]) || control.Attributes["onkeyup"] != functionCall)
                    control.Attributes["onkeyup"] += functionCall;
			}
		}
	}
}