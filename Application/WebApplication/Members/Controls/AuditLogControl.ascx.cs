﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using LogisticsPlus.Eship.Processor.Dto.Core;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class AuditLogControl : UserControl
	{
		public new void Load(IEnumerable<AuditLogsViewSearchDto> auditLogs)
		{
			lstAuditLogs.DataSource = auditLogs;
			lstAuditLogs.DataBind();
		}

		public void Page_Load(object sender, EventArgs args)
		{
		}
	}
}