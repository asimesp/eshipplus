﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class UserAuthentication : UserControl, ILoginView
	{
		private const string Msg = "msg";

		private string PartnerAccessCode
		{
			get
			{
				return Request.Form.AllKeys.Any() && Request.Form.AllKeys.Contains(WebApplicationConstants.AccessCode)
						  ? Request.Form[WebApplicationConstants.AccessCode]
						  : (Request.QueryString[WebApplicationConstants.AccessCode] ?? string.Empty);
			}
		}
		private string PartnerUsername
		{
			get
			{
				return Request.Form.AllKeys.Any() && Request.Form.AllKeys.Contains(WebApplicationConstants.Username)
						  ? Request.Form[WebApplicationConstants.Username]
						  : (Request.QueryString[WebApplicationConstants.Username] ?? string.Empty);
			}
		}
		private string PartnerPassword
		{
			get
			{
				return Request.Form.AllKeys.Any() && Request.Form.AllKeys.Contains(WebApplicationConstants.Password)
							  ? Request.Form[WebApplicationConstants.Password]
							  : (Request.QueryString[WebApplicationConstants.Password] ?? string.Empty);
			}
		}
		private string PartnerReturnUrl
		{
			get
			{
				var url = Request.Form.AllKeys.Any() && Request.Form.AllKeys.Contains(WebApplicationConstants.ReturnUrl)
							? Request.Form[WebApplicationConstants.ReturnUrl]
							: (Request.QueryString[WebApplicationConstants.ReturnUrl] ?? string.Empty);
				return string.IsNullOrEmpty(url) ? null : url;
			}
		}
		private string PartnerHomeUrl
		{
			get
			{
				var url = Request.Form.AllKeys.Any() && Request.Form.AllKeys.Contains(WebApplicationConstants.HomeUrl)
							? Request.Form[WebApplicationConstants.HomeUrl]
							: (Request.QueryString[WebApplicationConstants.HomeUrl] ?? string.Empty);
				return string.IsNullOrEmpty(url) ? null : url;
			}
		}
		private string PartnerAuthenticationErrorReturnUrl
		{
			get
			{
				var url = Request.Form.AllKeys.Any() &&
						  Request.Form.AllKeys.Contains(WebApplicationConstants.AuthenticationErrorReturnUrl)
							  ? Request.Form[WebApplicationConstants.AuthenticationErrorReturnUrl]
							  : (Request.QueryString[WebApplicationConstants.AuthenticationErrorReturnUrl] ?? string.Empty);
				return string.IsNullOrEmpty(url) ? null : url;
			}
		}

		private bool PartnerActionIndicatorPresent
		{
			get { return !string.IsNullOrEmpty(PartnerActionKey); }
		}
		private string PartnerActionKey
		{
			get { return Request.QueryString[WebApplicationConstants.PartnerActionCodeIndicator]; }
		}

		public bool HideLogo
		{
			set { pnlLogo.Visible = !value; }
		}
		public bool HideFooter
		{
			set { pnlFooter.Visible = false; }
		}

		public string AccessCode { get { return hidAccessCode.Value; } }
		public bool AdminAuthentication { get { return txtUsername.Text.StartsWith(WebApplicationConstants.SuperAdminLoginPrefix); } }
		public string UserName
		{
			get
			{
				return string.IsNullOrEmpty(txtUsername.Text)
						? string.Empty
						: txtUsername.Text.Replace(WebApplicationConstants.SuperAdminLoginPrefix, string.Empty);
			}
		}
		public string Password { get { return txtPassword.Text; } }

        public string Domain
        {
            get { return WebApplicationSettings.Domain; }
        }

	    public AuthenticationType AuthenticationType
	    {
	        get { return ddlAutenticationType.SelectedValue.ToInt().ToEnum<AuthenticationType>(); }
	    }

	    public event EventHandler AuthenticateUser;
		public event EventHandler RetrieveUserPassword;
		public event EventHandler<ViewEventArgs<string>> PasswordChange;

		public event EventHandler<ViewEventArgs<User>> SuccessfulLogin;

		public void NotifyInActiveAccount()
		{
			const string error = "This user client's account is inactive.";
			if (RedirectIfPartnerLogin(error)) return;
			DisplayMessages(new[] { ValidationMessage.Error(error) });
		}

		public void NotifyDisabledUser()
		{
			const string error = "This user account is disabled.";
			if (RedirectIfPartnerLogin(error)) return;
			DisplayMessages(new[] { ValidationMessage.Error(error) });
		}

		public void NotifyUserNotFound()
		{
			const string error = "User not found.";
			if (RedirectIfPartnerLogin(error)) return;
			DisplayMessages(new[] { ValidationMessage.Error(error) });
		}

		public void NotifyInvalidPassword()
		{
			const string error = "Invalid password.";
			if (RedirectIfPartnerLogin(error)) return;
			DisplayMessages(new[] { ValidationMessage.Error(error) });
		}

		public void NotifyLoginAttemptsExceeded()
		{
			const string error = "The maximum number of login attempts has been reached/exceeded. Please contact your application administrator.";
			if (RedirectIfPartnerLogin(error)) return;
			DisplayMessages(new[] { ValidationMessage.Error(error) });
		}

		public void NotifySuccesfulLogin(User user)
		{
			if (SuccessfulLogin != null)
				SuccessfulLogin(this, new ViewEventArgs<User>(user));
		}

		public void NotifySuccesfulLogin(AdminUser adminUser)
		{
			var user = new User
			{
				Username = adminUser.Username,
				FirstName = adminUser.FirstName,
				LastName = adminUser.LastName,
				Email = adminUser.Email,
				Enabled = adminUser.Enabled,
				Password = adminUser.Password,
			};
			foreach (var p in adminUser.AdminUserPermissions)
			{
				var permission = new UserPermission
				{
					User = user,
					Code = p.Code,
					Description = p.Description,
					Grant = p.Grant,
					Deny = p.Deny,
					Remove = p.Remove,
					Modify = p.Modify,
					DeleteApplicable = p.DeleteApplicable,
					ModifyApplicable = p.ModifyApplicable
				};
				user.UserPermissions.Add(permission);
			}
			Session[WebApplicationConstants.IsSuperUserLogin] = true;
			Session[WebApplicationConstants.ActiveSuperUser] = adminUser;
			user.SetAuthenticatedUser(Request.QueryString[WebApplicationConstants.RedirectUrlPrefix] ?? DashboardView.PageAddress);
		}



		public void ProcessPasswordRetrieval(string password, string userEmail)
		{
			DisplayMessages(!Request.SendPasswordRecoveryEmail(userEmail, password)
								? new[]
			                	  	{
			                	  		ValidationMessage.Error(
			                	  			"Error sending password to user email.  Please contact your application administrator.")
			                	  	}
								: new[] { ValidationMessage.Information("Password sent to user email on file.") });
		}

		public void ProcessPasswordReset()
		{
			pnlPwdReset.Visible = true;
			pnlLogin.Visible = false;
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			litMessages.Text = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			pnlErrors.Visible = true;
		}

		private bool RedirectIfPartnerLogin(string message)
		{
			var returnUrl = Session[WebApplicationConstants.AuthenticationErrorReturnUrl].GetString();
			if (!string.IsNullOrEmpty(returnUrl))
			{
				var url = !string.IsNullOrEmpty(message) ? string.Format("{0}?{1}={2}", returnUrl, Msg, message) : returnUrl;
				Response.Redirect(url);
				return true;
			}
			return false;
		}

		private void Authenticate()
		{
			if (AuthenticateUser != null)
				AuthenticateUser(this, new EventArgs());
		}

		private void RetrievePassword()
		{
			if (RetrieveUserPassword != null)
				RetrieveUserPassword(this, new EventArgs());
		}


		private void LoadLoginCss()
		{
			Page.Header.Controls.Add(new LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/css/login2.css") + "\" />"));
		}




	    protected void Page_Load(object sender, EventArgs e)
	    {
	        LoadLoginCss();
	        new LoginHandler(this).Initialize();

	        if (IsPostBack) return;

            ddlAutenticationType.DataSource = ProcessorUtilities.GetAll<AuthenticationType>();
            ddlAutenticationType.DataBind();

	        if (PartnerActionIndicatorPresent && PartnerActionKey == WebApplicationConstants.PartnerForgotPasswordKey)
	        {
	            //set values
	            hidAccessCode.Value = PartnerAccessCode;
	            txtUsername.Text = PartnerUsername;

	            RetrievePassword();
	        }
	        else if (!string.IsNullOrEmpty(PartnerUsername))
	        {
	            // set return url
	            Session[WebApplicationConstants.PartnerReturnUrlKey] = PartnerReturnUrl;
	            Session[WebApplicationConstants.PartnerHomeUrlKey] = PartnerHomeUrl;
	            Session[WebApplicationConstants.AuthenticationErrorReturnUrl] = PartnerAuthenticationErrorReturnUrl;

	            Response.PersistPartnerAuthenticationErrorReturnUrl(PartnerAuthenticationErrorReturnUrl);
	            Response.PersistPartnerHomeUrl(PartnerHomeUrl);
	            Response.PersistPartnerReturnUrl(PartnerReturnUrl);

	            // set values
	            hidAccessCode.Value = PartnerAccessCode;
	            txtUsername.Text = PartnerUsername;
	            txtPassword.Text = PartnerPassword;

	            // Authenticate
	            Authenticate();
	        }
	    }

	    protected void OnLoginClicked(object sender, EventArgs e)
		{
			Authenticate();
		}

		protected void OnRetrievePasswordClicked(object sender, EventArgs e)
		{
			RetrievePassword();
		}

		protected void OnOkayResetPasswordClicked(object sender, EventArgs e)
		{
			if ((String.IsNullOrEmpty(txtNewPassword.Text) || String.IsNullOrEmpty(txtConfirmPassword.Text)) || (txtNewPassword.Text != txtConfirmPassword.Text))
			{
				DisplayMessages(new[] { ValidationMessage.Error("Passwords do not match.") });
				return;
			}

			if (PasswordChange != null)
				PasswordChange(this, new ViewEventArgs<string>(txtConfirmPassword.Text));
		}

		protected void OnCancelResetPasswordClicked(object sender, EventArgs e)
		{
			DisplayMessages(new[] { ValidationMessage.Error("Password has not been changed.") });

			pnlPwdReset.Visible = false;
			pnlLogin.Visible = true;
		}

		
	}
}