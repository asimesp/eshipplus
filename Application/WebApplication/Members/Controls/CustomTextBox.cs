﻿using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public class CustomTextBox : TextBox
	{
		private const string CustomTextBoxTypeKey = "CTBTK";

		public CustomTextBoxType Type
		{
			get
			{
				return string.IsNullOrEmpty(ViewState[CustomTextBoxTypeKey].GetString())
						   ? CustomTextBoxType.NotSet
						   : ViewState[CustomTextBoxTypeKey].GetString().ToEnum<CustomTextBoxType>();
			}
			set { ViewState[CustomTextBoxTypeKey] = value.ToString(); }
		}

		protected override void OnLoad(System.EventArgs e)
		{
			switch (Type)
			{
				case CustomTextBoxType.Date:
					Attributes.Add("eShipDateInput", "true");
					break;
				case CustomTextBoxType.FloatingPointNumbers:
					Attributes.Add("floatingpointnumbersonly", "true");
					break;
				case CustomTextBoxType.NegativeNumbers:
					Attributes.Add("negativenumbersonly", "true");
					break;
				case CustomTextBoxType.NumbersOnly:
					Attributes.Add("numbersonly", "true");
					break;
				case CustomTextBoxType.ReadOnly:
					Attributes.Add("eShipReadOnly", "true");
					break;
				case CustomTextBoxType.Time:
                    Attributes.Add("eShipTimeInput", "true");
					break;
			}
			base.OnLoad(e);
		}
	}
}