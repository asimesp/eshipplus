﻿namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public enum HandleButtonsClientSide
	{
		None = 0,
		Ok,
		YesNo,
		YesNoCancel,
		All
	}

}