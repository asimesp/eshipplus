﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AltUniformCheckBox.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.AltUniformCheckBox" %>

<asp:Panel CssClass="checker" runat="server" ID="chkControlDiv">
    <span class=''>
        <script type="text/javascript">
            $(document).ready(function() {
                SetAltUniformCheckBoxClickedStatus(document.getElementById('<%= chkControl.ClientID %>'));
            });
        </script>
        <asp:CheckBox runat="server" ID="chkControl" onclick="SetAltUniformCheckBoxClickedStatus(this);" />
    </span>
</asp:Panel>
