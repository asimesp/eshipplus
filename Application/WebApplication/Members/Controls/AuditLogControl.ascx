﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuditLogControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.AuditLogControl" %>
<asp:Panel runat="server" ID="pnlAuditLog" Style="overflow-x: scroll; overflow-y: auto;">
    
    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAuditLogControlTable" TableId="auditLogControlTable" IgnoreHeaderBackgroundFill="True" />

    <table id="auditLogControlTable" class="stripe">
        <tr>
            <th style="width: 65%;">Description</th>
            <th style="width: 15%;">User</th>
            <th style="width: 20%;">Log Date/Time</th>
        </tr>
        <asp:ListView ID="lstAuditLogs" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>

                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />

            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td class="text-left">
                        <%# Eval("Description") %>
                    </td>
                    <td class="text-left top">
                        <%# Eval("UserName") %>
                    </td>
                    <td class="text-left top">
                        <%# Eval("LogDateTime") %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </table>
</asp:Panel>
