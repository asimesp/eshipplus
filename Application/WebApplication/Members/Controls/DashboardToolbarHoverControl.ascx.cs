﻿using System;
using System.Linq;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class DashboardToolbarHoverControl : MemberControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var operations = DashboardItemConfiguration.Operations2(ActiveUser.Tenant).Where(p => ActiveUser.HasAccessTo(p.IconName));
			lstOperations.DataSource = operations;
			lstOperations.DataBind();
			lstOperations.Visible = operations.Any();

            var busIntelligence = DashboardItemConfiguration.BusinessIntelligence2(ActiveUser.Tenant).Where(p => ActiveUser.HasAccessTo(p.IconName));
			lstBusinessIntelligence.DataSource = busIntelligence;
			lstBusinessIntelligence.DataBind();
			lstBusinessIntelligence.Visible = busIntelligence.Any();

            var registry = DashboardItemConfiguration.Registry2(ActiveUser.Tenant).Where(p => ActiveUser.HasAccessTo(p.IconName));
			lstRegistry.DataSource = registry;
			lstRegistry.DataBind();
			lstRegistry.Visible = registry.Any();

            var utilities = DashboardItemConfiguration.Utilities2(ActiveUser.Tenant).Where(p => ActiveUser.HasAccessTo(p.IconName));
			lstUtilities.DataSource = utilities;
			lstUtilities.DataBind();
			lstUtilities.Visible = utilities.Any();

            var admin = DashboardItemConfiguration.Administration2(ActiveUser.Tenant).Where(p => ActiveUser.HasAccessTo(p.IconName)).ToList();
            admin.AddRange(DashboardItemConfiguration.SuperAdmin2().Where(p => ActiveUser.HasAccessTo(p.IconName)));
			lstAdministration.DataSource = admin;
			lstAdministration.DataBind();
			lstAdministration.Visible = admin.Any();

            var rating = DashboardItemConfiguration.Rating2(ActiveUser.Tenant).Where(p => ActiveUser.HasAccessTo(p.IconName));
			lstRating.DataSource = rating;
			lstRating.DataBind();
			lstRating.Visible = rating.Any();

            var connect = DashboardItemConfiguration.Connect2(ActiveUser.Tenant).Where(p => ActiveUser.HasAccessTo(p.IconName));
			lstConnect.DataSource = connect;
			lstConnect.DataBind();
			lstConnect.Visible = connect.Any();

            var finance = DashboardItemConfiguration.Accounting2(ActiveUser.Tenant).Where(p => ActiveUser.HasAccessTo(p.IconName));
			lstFinance.DataSource = finance;
			lstFinance.DataBind();
			lstFinance.Visible = finance.Any();

			pnlGroup1.Visible = busIntelligence.Any() || connect.Any() || admin.Any();
			pnlGroup2.Visible = rating.Any() || utilities.Any();
		}
	}
}