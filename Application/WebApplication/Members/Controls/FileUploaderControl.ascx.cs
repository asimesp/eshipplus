﻿using System;
using LogisticsPlus.Eship.Processor.Views;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
    public partial class FileUploaderControl : MemberControlBase
    {
        public string Title { set { litTitle.Text = value; } }
        public string Instructions { set { litInstructions.Text = value; } }

        public new bool Visible
        {
            get { return pnlFileUpload.Visible; }
            set
            {
                base.Visible = true;
                pnlFileUpload.Visible = value;
                pnlFileUploaderDimScreen.Visible = value;
            }
        }

        public event EventHandler<FileUploaderEventArgs> FileUploaded;
        public event EventHandler<ViewEventArgs<string>> FileUploadError;
        public event EventHandler FileUploadCancelled;

        protected void OnUploadClicked(object sender, EventArgs e)
        {
            if (fupFileUpload.HasFile)
            {
                if (FileUploaded != null)
                    FileUploaded(this, new FileUploaderEventArgs(fupFileUpload.FileContent, fupFileUpload.FileName));
            }
            else
            {
                if (FileUploadError != null)
                    FileUploadError(this, new ViewEventArgs<string>("A file must be selected for upload!"));
            }
        }

        protected void OnCancelClicked(object sender, EventArgs e)
        {
            if (FileUploadCancelled != null)
                FileUploadCancelled(this, new EventArgs());
        }
    }
}