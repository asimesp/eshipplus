﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class PaginationExtender : MemberControlBaseWithControlStore
	{
		private MemberPageBaseWithPageStore _parentPage;

		private MemberPageBaseWithPageStore ParentPage
		{
			get { return _parentPage ?? (_parentPage = Page as MemberPageBaseWithPageStore); }
		}

		private IList StoredData
		{
			get
			{
				return UseParentDataStore
						   ? (ParentPage.PageStore[ParentDataStoreKey] as IList ?? new List<Object>())
						   : (ControlStore[ClientID] as IList ?? new List<object>());
			}
			set
			{
				if (UseParentDataStore)
					ParentPage.PageStore[ParentDataStoreKey] = value;
				else
					ControlStore[ClientID] = value;
			}
		}

		public int PageSize
		{
			get { return hidPageSize.Value.ToInt(); }
			set { hidPageSize.Value = value.GetString(); }
		}

		/// <summary>
		/// Actual page number and not 0 based index
		/// </summary>
		public int CurrentPage
		{
			get
			{
				var i = ddlPageNumber.SelectedValue.ToInt();
				return i < 1 ? 1 : i;
			}
		}

		public string TargetControlId
		{
			get { return hidTargetControlId.Value; }
			set { hidTargetControlId.Value = value; }
		}

		public string UnCheckControlIdOnPageChange
		{
			get { return hidUnCheckControlIdOnPageChange.Value; }
			set { hidUnCheckControlIdOnPageChange.Value = value; }
		}

		public bool UseParentDataStore
		{
			get { return hidUseParentPageDataStore.Value.ToBoolean(); }
			set { hidUseParentPageDataStore.Value = value.GetString(); }
		}

		public string ParentDataStoreKey
		{
			get { return hidParentPageDataStoreKey.Value; }
			set { hidParentPageDataStoreKey.Value = value; }
		}

		/// <summary>
		/// Fires as page is changing.  If page change is originates from page dropdown list, CurrentPage value will already be changed!
		/// </summary>
		public event EventHandler PageChanging;

		private void LoadPage(int pageNumber)
		{
			var control = Parent.FindControl(TargetControlId);
			if (control == null) return;

			var data = StoredData;
			var pages = data.Cast<Object>().ToList().ToCollectionPages(PageSize);

			// set dropdown list & page counts
			if (pages.Any())
			{
				lbPageCount.Text = pages.Count.GetString();
				ddlPageNumber.DataSource = pages
					.Select((i, k) => new ViewListItem((k + 1).GetString(), (k + 1).GetString()))
					.ToList();
				ddlPageNumber.DataBind();
				if (pageNumber < 1) pageNumber = 1;
				if (pageNumber > pages.Count) pageNumber = pages.Count;
				ddlPageNumber.SelectedValue = pageNumber.GetString();
			}
			else
			{
				lbPageCount.Text = 1.GetString();
				ddlPageNumber.DataSource = new List<ViewListItem> { new ViewListItem(1.GetString(), 1.GetString()) };
				ddlPageNumber.DataBind();
				ddlPageNumber.SelectedValue = 1.GetString();
			}

			// bind target control
			var index = pageNumber - 1;
			var dataToBind = 0 <= index && index < pages.Count ? pages[index] : new List<object>();
			var lst = control.ToListView();
			if (lst != null)
			{
				lst.DataSource = dataToBind;
				lst.DataBind();
			}
			var rpt = control.ToRepeater();
			if (rpt != null)
			{
				rpt.DataSource = dataToBind;
				rpt.DataBind();
			}

			// button visibilities
			ibtnPrevious.Visible = pages.Count > 1 && pageNumber != 1;
			ibtnNext.Visible = pages.Count > 1 && pageNumber != pages.Count;

			// uncheck check all on page change if control exists
			if (string.IsNullOrEmpty(UnCheckControlIdOnPageChange)) return;
			var altUniChk = Parent.FindControl(UnCheckControlIdOnPageChange).ToAltUniformCheckBox();
			if (altUniChk != null) altUniChk.Checked = false;
			var chk = Parent.FindControl(UnCheckControlIdOnPageChange).ToCheckBox();
			if (chk != null) chk.Checked = false;
		}


		public void LoadPageWithRecordIndex(int index)
		{
			LoadPage(Math.Ceiling(index.ToDouble() / PageSize).ToInt());
		}

		public void LoadData<T>(List<T> data, int pageNumber = 1)
		{
			StoredData = data;
			LoadPage(pageNumber);
		}

		public List<T> GetData<T>()
		{
			return StoredData.Count > 0 ? StoredData.Cast<T>().ToList() : new List<T>();
		}



		protected void Page_Load(object sender, EventArgs e)
		{
		}


		protected void OnPreviousClicked(object sender, ImageClickEventArgs e)
		{
			var page = ddlPageNumber.SelectedValue.ToInt() - 1;
			if (ddlPageNumber.ContainsValue(page.GetString()))
			{
				if (PageChanging != null)
					PageChanging(this, new EventArgs());

				LoadPage(page);
			}
		}

		protected void OnNextClicked(object sender, ImageClickEventArgs e)
		{
			var page = ddlPageNumber.SelectedValue.ToInt() + 1;
			if (ddlPageNumber.ContainsValue(page.GetString()))
			{
				if (PageChanging != null)
					PageChanging(this, new EventArgs());

				LoadPage(page);
			}
		}

		protected void OnPageNumberSelectedIndexChanged(object sender, EventArgs e)
		{
			// drop down list already has page number we're going to
			if (PageChanging != null)
				PageChanging(this, new EventArgs());

			LoadPage(ddlPageNumber.SelectedValue.ToInt());
		}
	}
}