﻿namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public enum MessageButton
	{
		Ok = 0,
		YesNo,
		YesNoCancel,
	}
}
