﻿using System;
using System.Web.UI;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
    public partial class MessageBoxControl : UserControl
    {
        public string Title
        {
            get { return title.Text; }
            set { title.Text = value; }
        }

        public string Message
        {
            get { return message.Text; }
            set { message.Text = value; }
        }

        public MessageButton Button
        {
            set
            {
                btnOkay.Visible = value == MessageButton.Ok;
                btnYes.Visible = value == MessageButton.YesNo || value == MessageButton.YesNoCancel;
                btnNo.Visible = value == MessageButton.YesNo || value == MessageButton.YesNoCancel;
                btnCancel.Visible = value == MessageButton.YesNoCancel;

                if (btnOkay.Visible) pnlMessages.DefaultButton = btnOkay.ID;
                else if (btnYes.Visible) pnlMessages.DefaultButton = btnYes.ID;

                var mb = hidHandleButtonsClientSide.Value.ToInt().ToEnum<HandleButtonsClientSide>();
                switch (mb)
                {
                    case Members.Controls.HandleButtonsClientSide.Ok:
                        hidHandleClientSide.Value = (value == MessageButton.Ok).ToString();
                        break;
                    case Members.Controls.HandleButtonsClientSide.YesNo:
                    case Members.Controls.HandleButtonsClientSide.YesNoCancel:
                        hidHandleClientSide.Value = (value == MessageButton.YesNo || value == MessageButton.YesNoCancel).ToString();
                        break;
                    case Members.Controls.HandleButtonsClientSide.All:
                        hidHandleClientSide.Value = true.ToString();
                        break;
                    default:
                        hidHandleClientSide.Value = false.ToString();
                        break;
                }
            }
        }

        public MessageIcon Icon
        {
            set
            {
                imgError.Visible = value == MessageIcon.Error;
                imgInformation.Visible = value == MessageIcon.Information;
                imgWarning.Visible = value == MessageIcon.Warning;
                imgQuestion.Visible = value == MessageIcon.Question;
            }
        }

        public HandleButtonsClientSide HandleButtonsClientSide
        {
            set { hidHandleButtonsClientSide.Value = value.ToInt().ToString(); }
        }

        public new bool Visible
        {
            get { return pnlMessages.Visible; }
            set
            {
                if (hidHandleClientSide.Value.ToBoolean())
                {
                    pnlMessages.Attributes.Add("style", string.Format("display: {0}", value ? "block" : "none"));
                    pnlPopUpDimScreen.Attributes.Add("style", string.Format("display: {0}", value ? "block" : "none"));
                    base.Visible = true;
                    pnlMessages.Visible = true;
                    pnlPopUpDimScreen.Visible = true;

                }
                else
                {
                    pnlMessages.Attributes.Add("style", "display: block;");
                    pnlPopUpDimScreen.Attributes.Add("style", "display: block;");
                }

                base.Visible = true;
                pnlMessages.Visible = value;
                pnlPopUpDimScreen.Visible = value;
            }
        }


        public string OkayClientScript
        {
            set { if (!btnOkay.OnClientClick.Contains(value)) btnOkay.OnClientClick += value; }
        }

        public string YesClientScript
        {
            set { if (!btnYes.OnClientClick.Contains(value)) btnYes.OnClientClick += value; }
        }

        public string NoClientScript
        {
            set { if (!btnNo.OnClientClick.Contains(value)) btnNo.OnClientClick += value; }
        }

        public string CancelClientScript
        {
            set { if (!btnCancel.OnClientClick.Contains(value)) btnCancel.OnClientClick += value; }
        }

        public event EventHandler Okay;
        public event EventHandler Yes;
        public event EventHandler No;
        public event EventHandler Cancel;

        protected void OnOkayClicked(object sender, EventArgs e)
        {
            if (Okay != null)
                Okay(this, new EventArgs());
        }

        protected void OnYesClicked(object sender, EventArgs e)
        {
            if (Yes != null)
                Yes(this, new EventArgs());
        }

        protected void OnNoClicked(object sender, EventArgs e)
        {
            if (No != null)
                No(this, new EventArgs());
        }

        protected void OnCancelClicked(object sender, EventArgs e)
        {
            if (Cancel != null)
                Cancel(this, new EventArgs());
        }

        protected override void OnLoad(EventArgs e)
        {
            if (hidHandleClientSide.Value.ToBoolean())
            {
                pnlMessages.Attributes.Add("style", "display: none;");
                pnlPopUpDimScreen.Attributes.Add("style", "display: none;");
            }
            base.OnLoad(e);
        }
    }
}