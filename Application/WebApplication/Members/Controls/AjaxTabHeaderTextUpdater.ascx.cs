﻿using System;
using System.Web.UI;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class AjaxTabHeaderTextUpdater : UserControl
	{
		public void SetForUpdate(string tabHeaderIdEndsWith, string tabHeaderText)
		{
			hidTabPanelIdEndsWith.Value = tabHeaderIdEndsWith;
			hidTabPanelHeaderText.Value = tabHeaderText.StripTags();
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}