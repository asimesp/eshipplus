﻿using System;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
    [Serializable]
    public class DeveloperAccessRequestFinderObj
    {
        public long Id { get; set; }

        public string Customer { get; set; }

        public string ContactName { get; set; }

        public string ContactEmail { get; set; }

        public string DateCreated { get; set; }
    }
}