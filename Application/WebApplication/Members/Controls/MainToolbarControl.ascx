﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainToolbarControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.MainToolbarControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Register Src="DashboardToolbarHoverControl.ascx" TagName="DashboardToolbarHover" TagPrefix="uc1" %>

<script type="text/javascript">
    $(document).ready(function () {
        $('#<%= hypDashboard.ClientID %>').on('mouseover mouseenter', function() {
            $("#hoverContainer").show();
            $("#hoverContainerHelp").hide();
            $("#hoverContainerMore").hide();
        });
        $('#<%= hypDashboard.ClientID %>').parent().parent().mouseleave(function() {
            $("#hoverContainer").hide();
        });

        $('#<%= lnkMore.ClientID %>').on('mouseover mouseenter', function() {
            $("#hoverContainerMore").show();
            $("#hoverContainer").hide();
            $("#hoverContainerHelp").hide();
            
            adjustHoverMenu('moreHoverMenu');
        });
        $('#<%= lnkMore.ClientID %>').parent().parent().mouseleave(function() {
            $("#hoverContainerMore").hide();
        });

        $('#<%= lnkHelp.ClientID %>').on('mouseover mouseenter', function() {
            $("#hoverContainerHelp").show();
            $("#hoverContainerMore").hide();
            $("#hoverContainer").hide();

            adjustHoverMenu('HelpHoverMenu');
        });
        $('#<%= lnkHelp.ClientID %>').parent().parent().mouseleave(function() {
             $("#hoverContainerHelp").hide();
        });
        

        $('#<%= lnkNew.ClientID %>').on('mouseover mouseenter', function () { hideAllHovers(); });
        $('#<%= lnkEdit.ClientID %>').on('mouseover mouseenter', function () { hideAllHovers(); });
        $('#<%= lnkUnlock.ClientID %>').on('mouseover mouseenter', function () { hideAllHovers(); });
        $('#<%= lnkSave.ClientID %>').on('mouseover mouseenter', function () { hideAllHovers(); });
        $('#<%= lnkDelete.ClientID %>').on('mouseover mouseenter', function () { hideAllHovers(); });
        $('#<%= lnkFind.ClientID %>').on('mouseover mouseenter', function () { hideAllHovers(); });
        $('#<%= lnkImport.ClientID %>').on('mouseover mouseenter', function () { hideAllHovers(); });
        $('#<%= lnkExport.ClientID %>').on('mouseover mouseenter', function () { hideAllHovers(); });
        
        function hideAllHovers() {
            $("#hoverContainerHelp").hide();
            $("#hoverContainerMore").hide();
            $("#hoverContainer").hide();
        }
        
        function adjustHoverMenu(cid) {
            var sideBuffters = ($(window).width() - jsHelper.PageWidth) / 2;
            var rightBounds = jsHelper.PageWidth + sideBuffters;
            var controlWidth = $(jsHelper.AddHashTag(cid)).width();
            var controlParentWidth = $(jsHelper.AddHashTag(cid)).parent().width();
            var left = jsHelper.PageX - controlParentWidth + 20;
            
            if (jsHelper.PageX + controlWidth > rightBounds) {
                left = jsHelper.PageX - controlWidth + controlParentWidth + 20; // account for 20 pixel margin-left
            }

            $(jsHelper.AddHashTag(cid)).css("left", left);
            $(jsHelper.AddHashTag(cid)).css("top", jsHelper.PageHeaderHeight);
            $(jsHelper.AddHashTag(cid)).css("position", "Fixed");
        }

    });
</script>

<div id="toolbar">
    <table style="width: 100%;">
        <tr>
            <td>
                <ul>
                    <li>
                        <asp:HyperLink CssClass="toolbaricons" ID="hypDashboard" NavigateUrl="~/Members/DashboardView.aspx"
                            runat="server" Visible="true">
                            <asp:Image ID="imgDashboard" ImageUrl="~/images/dashboard.png" runat="server" ToolTip="Dashboard"
                                Width="24px" Height="23px" />Dashboard
                        </asp:HyperLink>
                        <div id="hoverContainer" style="display: none;">
                            <uc1:DashboardToolbarHover ID="dashboardHover" runat="server" Visible="true" />
                        </div>
                    </li>
                    <li>
                        <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkNew" OnClick="OnNewClicked"
                            runat="server" Visible="false">
                            <asp:Image ID="imgNew" ImageUrl="~/images/icons2/new.png" runat="server" ToolTip="New" Width="20px"
                                Height="23px" />New
                        </asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkEdit" OnClick="OnEditClicked"
                            runat="server" Visible="false">
                            <asp:Image ID="imgEdit" ImageUrl="~/images/icons2/edit.png" Width="23" Height="23" runat="server"
                                ToolTip="Edit" />Edit
                        </asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkUnlock" OnClick="OnUnlockClicked"
                            runat="server" Visible="false">
                            <asp:Image ID="Image4" ImageUrl="~/images/icons2/unlock.png" Width="25" Height="25" runat="server"
                                ToolTip="Release Record Lock" />Unlock
                        </asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkSave" OnClick="OnSaveClicked"
                            OnClientClick="ShowProcessingDivBlock();" runat="server" Visible="false">
                            <asp:Image ID="imgSave" ImageUrl="~/images/icons2/save.png" Width="22" Height="23" runat="server"
                                ToolTip="Save" />Save
                        </asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkDelete" OnClick="OnDeleteClicked"
                            runat="server" Visible="false">
                            <asp:Image ID="imgDelete" ImageUrl="~/images/icons2/delete.png" Width="17" Height="23" runat="server"
                                ToolTip="Delete" />Delete
                        </asp:LinkButton>
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="lnkDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </li>
                    <li>
                        <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkFind" OnClick="OnFindClicked"
                            runat="server" Visible="false">
                            <asp:Image ID="imgFind" ImageUrl="~/images/icons2/find.png" Width="24px" Height="23px" runat="server"
                                ToolTip="Find" />Find
                        </asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkImport" OnClick="OnImportClicked"
                            runat="server" Visible="false">
                            <asp:Image ID="imgImport" ImageUrl="~/images/icons2/import.png" Width="24px" runat="server"
                                ToolTip="Import" />Import
                        </asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkExport" OnClick="OnExportClicked"
                            runat="server" Visible="false">
                            <asp:Image ID="imgExport" ImageUrl="~/images/icons2/export.png" Width="24px" runat="server"
                                ToolTip="Export" />Export
                        </asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkMore" OnClick="OnMoreClicked"
                            runat="server" Visible="false">
                            <asp:Image ID="imgMore" ImageUrl="~/images/icons2/more.png" Width="23px" Height="23px" runat="server"
                                ToolTip="More" />More
                        </asp:LinkButton>


                        <asp:Repeater runat="server" ID="rptMoreActions">
                            <HeaderTemplate>
                                <div id="hoverContainerMore" style="display: none;" class="hoverContainer clearfix">
                                    <div id="moreHoverMenu" class="moreHoverMenu clearfix">
                                        <div class="column2 full">
                                            <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li class="menuDropdown">
                                    <asp:HyperLink runat="server" ID="hypDashboardItem" NavigateUrl='<%# Eval("NavigationUrl") %>'
                                        CssClass="link_nounderline" Visible='<%# Eval("IsLink").ToBoolean() %>' ToolTip='<%# Eval("Name") %>'
                                        Target='<%# Eval("OpenInNewWindow").ToBoolean() ? "_blank" : "_self" %>'>
                                        <span class="menuDropDownImgSpan">
                                            <asp:Image runat="server" Height="15px" ID="imgDashboardItem" ImageUrl='<%# Eval("ImageUrl") %>' />
                                        </span>
                                        <span class="white">
                                            <asp:Literal runat="server" ID="litDashboardItem" Text='<%# Eval("Name") %>' />
                                        </span>
                                    </asp:HyperLink>
                                    <asp:LinkButton CausesValidation="false" ID="lnkCommand" OnClick="OnCommandClicked"
                                        CssClass="link_nounderline" OnClientClick='<%# !Eval("ConfirmCommand").ToBoolean() && Eval("EnableProcessingDiv").ToBoolean() ? "ShowProcessingDivBlock();" : string.Empty %>'
                                        runat="server" Visible='<%# Eval("IsCommand").ToBoolean() %>'>
                                        <span class="menuDropDownImgSpan">
                                            <asp:Image ID="Image1" ImageUrl='<%# Eval("ImageUrl") %>' Height="15px" runat="server"
                                                ToolTip='<%# Eval("Name") %>' />
                                        </span><span class="white">
                                            <%# Eval("Name") %></span>
                                    </asp:LinkButton>
                                    <eShip:CustomHiddenField runat="server" ID="hidCommandArgs" Value='<%# Eval("CommandArgs") %>' />
                                    <ajax:ConfirmButtonExtender runat="server" ID="cbeCommand" TargetControlID="lnkCommand"
                                        Enabled='<%# Eval("ConfirmCommand").ToBoolean() %>' ConfirmText='<%# Eval("ConfirmationMessage") %>' />
                                    <hr style='<%# string.Format("display: {0}", Eval("IsSeperator").ToBoolean() ? "block": "none") %>'
                                        class="menuSeperator" />
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                                              </div>
                            </div>
                        </div>
                            </FooterTemplate>
                        </asp:Repeater>

                    </li>
                    <li>
                        <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkHelp" runat="server"
                            Visible="true">
                            <asp:Image ID="Image1" ImageUrl="~/images/icons2/help.png" Width="11px" runat="server" ToolTip="Help" />Help
                        </asp:LinkButton>
                        <div id="hoverContainerHelp" style="display: none;" class="hoverContainer clearfix">
                            <div id="HelpHoverMenu" class="moreHoverMenu clearfix">
                                <div class="column2 full">
                                    <ul>
                                        <asp:Repeater runat="server" ID="rptTemplates">
                                            <ItemTemplate>
                                                <li class="menuDropdown">
                                                    <div class="menuDropDownImgSpan left text-center">
                                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons2/export.png" Height="15px" />
                                                    </div>
                                                    <div class="left">
                                                        <asp:LinkButton runat="server" ID="lnkDownloadTemplate" CausesValidation="False"
                                                            CssClass="link_nounderline" Text='<%# Eval("DisplayName") %>' OnClick="OnDownloadTemplateClicked" />
                                                        <eShip:CustomHiddenField runat="server" ID="hidTemplateName" Value='<%# Eval("Path") %>' />
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </li>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <li>
                                                    <hr class="menuSeperator" />
                                                </li>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <li class="menuDropdown">
                                            <div class="menuDropDownImgSpan left text-center">
                                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons2/connect.png" Height="15px"
                                                    CssClass="ml4" />
                                            </div>
                                            <div class="left">
                                                <asp:HyperLink runat="server" ID="HyperLink1" Visible="true" NavigateUrl="~/Members/SupportEmailView.aspx"
                                                    Text="Support" Target="_blank" CssClass="link_nounderline" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li class="menuDropdown" runat="server" id="liUserGuide">
                                            <div class="menuDropDownImgSpan left text-center">
                                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons2/help.png" Height="15px"
                                                    Style="margin-left: 4px;" />
                                            </div>
                                            <div class="left">
                                                <asp:HyperLink runat="server" ID="hypUserGuide" Target="_blank" Text="User Guide" CssClass="link_nounderline" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </td>
            <td class="right">
                <div class="userinfo text-right white">
                    <asp:LinkButton CausesValidation="false" CssClass="toolbaricons" ID="lnkSignOut"
                        OnClick="OnSignOutClicked" runat="server" Visible="true">
                        <asp:Image ID="imgSignOut" ImageUrl="~/images/icons2/signout.png" Width="24px" runat="server"
                            ToolTip="Sign Out" />
                        Sign Out
                    </asp:LinkButton>
                </div>
            </td>
        </tr>
    </table>
</div>
