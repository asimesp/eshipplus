﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecordIdentityDisplayControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.RecordIdentityDisplayControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<script type="text/javascript">
    $(document).ready(function () {
        var control = $("[id$='" + $(jsHelper.AddHashTag('<%= hidTargetControlId.ClientID %>')).val() + "']")[0];
        var clientidDiv = '<%= ClientID %>_div';

        $(control).change(function() {
            if ($(this).is('select')) {
                switch ($(jsHelper.AddHashTag($(this).prop('id') + ' option:selected')).text()) {
                    case '<%= WebApplicationConstants.ChooseOne %>':
                    case '<%= WebApplicationConstants.NotApplicable %>':
                    case '<%= WebApplicationConstants.NotApplicableShort %>':
                    case '<%= WebApplicationConstants.NotAvailable %>':
                        SetText(clientidDiv, jsHelper.EmptyString);
                        break;
                    default:
                        SetText(clientidDiv, $(jsHelper.AddHashTag($(this).prop('id') + ' option:selected')).text());
                }

            } else {
                SetText(clientidDiv, $(this).val());
            }
        });

        // set on load of document
        if ($(control).is('select')) {
            switch ($(jsHelper.AddHashTag($(control).prop('id') + ' option:selected')).text()) {
                case '<%= WebApplicationConstants.ChooseOne %>':
                case '<%= WebApplicationConstants.NotApplicable %>':
                case '<%= WebApplicationConstants.NotApplicableShort %>':
                case '<%= WebApplicationConstants.NotAvailable %>':
                    SetText(clientidDiv, jsHelper.EmptyString);
                    break;
                default:
                    SetText(clientidDiv, $(jsHelper.AddHashTag($(control).prop('id') + ' option:selected')).text());
            }
        } else {
            SetText(clientidDiv, $(control).val());
        }
    });
</script>

<div class="pageHeaderRecDesc" id='<%= ClientID %>_div'></div>

<eShip:CustomHiddenField runat="server" ID="hidTargetControlId" />
