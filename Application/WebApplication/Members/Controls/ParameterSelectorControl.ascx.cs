﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
    public partial class ParameterSelectorControl : UserControl
    {
        public new bool Visible
        {
            get { return pnlParameterSelector.Visible; }
            set
            {
                base.Visible = value;
                pnlParameterSelector.Visible = value;
                pnlParameterSelectorDimScreen.Visible = value;
            }
        }

        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Add;
        public event EventHandler Close;


        public void LoadParameterSelector(List<SearchField> fields)
        {
            var columns = fields
                .Select(f => new
                {
                    Name = f.DisplayName,
                    f.DataType,
                    ReadOnly = false,
                    Operator = f.DataType.SearchDefaultParameter()
                })
                .ToList();

            chkParameterSelectorSelectAll.Checked = false;
            lstAvailableParameters.DataSource = columns.OrderBy(p => p.Name);
            lstAvailableParameters.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnParameterAddDoneClicked(object sender, EventArgs e)
        {
            var parameters = lstAvailableParameters
                .Items
                .Select(i => new
                {
                    Name = i.FindControl("litParameterName").ToLiteral().Text,
                    DataType = i.FindControl("hidParameterDataType").ToCustomHiddenField().Value,
                    ReadOnly = i.FindControl("hidParameterReadOnly").ToCustomHiddenField().Value,
                    Operator = i.FindControl("hidParameterOperator").ToCustomHiddenField().Value,
                    Selection = i.FindControl("chkSelection").ToAltUniformCheckBox().Checked
                })
                .Where(p => p.Selection)
                .Select(p => new ParameterColumn
                {
                    ReportColumnName = p.Name,
                    DefaultValue = string.Empty,
                    ReadOnly = false,
                    Operator = p.Operator.ToEnum<Operator>()
                })
                .ToList();

            if (Add != null)
                Add(this, new ViewEventArgs<List<ParameterColumn>>(parameters));

            lstAvailableParameters.DataSource = new List<ParameterColumn>();
            lstAvailableParameters.DataBind();
        }

        protected void OnParameterAddCloseClicked(object sender, EventArgs e)
        {
            lstAvailableParameters.DataSource = new List<ParameterColumn>();
            lstAvailableParameters.DataBind();

            if (Close != null)
                Close(this, new EventArgs());
        }
    }
}