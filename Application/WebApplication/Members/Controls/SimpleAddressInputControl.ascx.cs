﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class SimpleAddressInputControl : MemberControlBase
	{
		public string CssClass
		{
			set
			{
				txtCity.CssClass = value;
				txtPostalCode.CssClass = value;
				txtState.CssClass = value;
				txtStreet1.CssClass = value;
				txtStreet2.CssClass = value;
				ddlCountries.CssClass = value;
			}
		}

		public void LoadAddress<T>(T location) where T : Location
		{
			if (location == null) return;

			txtStreet1.Text = location.Street1;
			txtStreet2.Text = location.Street2;
			txtCity.Text = location.City;
			txtState.Text = location.State;
			txtPostalCode.Text = location.PostalCode;
		    ddlCountries.SelectedValue = location.CountryId.GetString();

		    if (ddlCountries.SelectedValue.ToLong() == default(long))
		        ddlCountries.SelectedValue = ActiveUser.Tenant.DefaultCountryId.GetString();
		}

		public void RetrieveAddress<T>(T location) where T : Location
		{
			if (location == null) return;

			location.Street1 = txtStreet1.Text;
			location.Street2 = txtStreet2.Text;
			location.City = txtCity.Text;
			location.State = txtState.Text;
			location.PostalCode = txtPostalCode.Text;
			location.CountryId = ddlCountries.SelectedValue.ToInt();
		}

		public void ClearFields()
		{
			txtStreet1.Text = string.Empty;
			txtStreet2.Text = string.Empty;
			txtCity.Text = string.Empty;
			txtState.Text = string.Empty;
			txtPostalCode.Text = string.Empty;
		}
	}
}