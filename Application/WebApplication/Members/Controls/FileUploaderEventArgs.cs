﻿using System;
using System.IO;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public class FileUploaderEventArgs : EventArgs
	{
        private readonly Stream _stream;
		private StreamReader _reader;
		private readonly string _filename;


        public Stream Stream { get { return _stream; } }
        public StreamReader Reader { get { return _reader ?? (_reader = new StreamReader(_stream)); } }
		public string Filename { get { return _filename; } }


        public FileUploaderEventArgs(Stream stream, string filename)
        {
            _stream = stream;
            _filename = filename;
        }
	}
}
