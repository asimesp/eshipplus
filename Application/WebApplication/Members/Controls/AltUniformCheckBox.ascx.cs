﻿using System;
using System.Web.UI;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class AltUniformCheckBox : UserControl
	{
		public bool Checked
		{
			get { return chkControl.Checked; }
			set { chkControl.Checked = value; }
		}

		public new string ClientID
		{
			get { return chkControl.ClientID; }
		}

	    public bool Enabled
	    {
            get { return chkControl.Enabled; }
            set{ chkControl.Enabled = value;}
	    }

		public bool AutoPostBack
		{
			get { return chkControl.AutoPostBack; }
			set { chkControl.AutoPostBack = value; }
		}

	    public string ToolTip
	    {
            get { return chkControl.ToolTip; }
            set { chkControl.ToolTip = value; }
	    }

		public string OnClientSideClicked
		{
			set { if (!chkControl.Attributes["onclick"].Contains(value)) chkControl.Attributes["onclick"] += value; }
		}

		public event EventHandler CheckedChanged;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (AutoPostBack)
			{
				chkControl.CheckedChanged += delegate
					{
						if (CheckedChanged != null)
							CheckedChanged(this, new EventArgs());
					};
			}
		}
	}
}