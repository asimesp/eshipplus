﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class MainToolbarControl : MemberControlBase
	{
		public bool ShowDashboard { set { hypDashboard.Visible = value; } }
		public bool ShowNew { set { lnkNew.Visible = value; } }
		public bool ShowEdit { set { lnkEdit.Visible = value; } }
		public bool ShowUnlock { set { lnkUnlock.Visible = value; } }
		public bool ShowSave { set { lnkSave.Visible = value; } }
		public bool ShowDelete { set { lnkDelete.Visible = value; } }
		public bool ShowFind { set { lnkFind.Visible = value; } }
		public bool ShowImport { set { lnkImport.Visible = value; } }
		public bool ShowExport { set { lnkExport.Visible = value; } }
		public bool ShowMore { set { lnkMore.Visible = value; } }

		public bool EnableSave
		{
			set
			{
				lnkSave.Enabled = value;
				lnkSave.OnClientClick = value ? "ShowProcessingDivBlock();" : string.Empty;
			}
		}

		public bool EnableImport { set { lnkImport.Enabled = value; } }

		public event EventHandler Dashboard;
		public event EventHandler New;
		public event EventHandler Save;
		public event EventHandler Delete;
		public event EventHandler Find;
		public event EventHandler Import;
		public event EventHandler Export;
		public event EventHandler Edit;
		public event EventHandler Unlock;
		public event EventHandler More;
		public event EventHandler<ViewEventArgs<string>> Command;

		public void LoadAdditionalActions(List<ToolbarMoreAction> actions)
		{
			rptMoreActions.DataSource = actions;
			rptMoreActions.DataBind();
		}

		public void LoadTemplates(List<string> templateFiles)
		{
			var data = templateFiles
				.Select(f =>
				{
					var path = f.AppendImportExportFolder();
					var name = Server.GetFileName(path).Replace(Server.GetFileExtension(path), string.Empty);
					return new
					{
						Path = path,
						DisplayName = name.FormattedString()
					};
				});
			rptTemplates.DataSource = data;
			rptTemplates.DataBind();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		    if (IsPostBack) return;

            var viewCodeString = Page as MemberPageBase == null ? string.Empty : ((MemberPageBase) Page).PageCode.ToString();
            
            if (File.Exists(Server.MapPath(string.Format("{0}{1}.pdf", WebApplicationSettings.UserGuidesFolder, viewCodeString))))
                hypUserGuide.NavigateUrl = string.Format("{0}?{1}={2}",UserGuideView.PageAddress, WebApplicationConstants.TransferNumber, viewCodeString.UrlTextEncrypt());
            else
                liUserGuide.Visible = false;
		}

		protected void OnDashboardClicked(object sender, EventArgs e)
		{
			if (Dashboard != null)
				Dashboard(this, new EventArgs());
		}

		protected void OnNewClicked(object sender, EventArgs e)
		{
			if (New != null)
				New(this, new EventArgs());
		}

		protected void OnSaveClicked(object sender, EventArgs e)
		{
			if (Save != null)
				Save(this, new EventArgs());
		}

		protected void OnFindClicked(object sender, EventArgs e)
		{
			if (Find != null)
				Find(this, new EventArgs());
		}

		protected void OnImportClicked(object sender, EventArgs e)
		{
			if (Import != null)
				Import(this, new EventArgs());
		}

		protected void OnExportClicked(object sender, EventArgs e)
		{
			if (Export != null)
				Export(this, new EventArgs());
		}

		protected void OnDeleteClicked(object sender, EventArgs e)
		{
			if (Delete != null)
				Delete(this, new EventArgs());
		}

		protected void OnEditClicked(object sender, EventArgs e)
		{
			if (Edit != null)
				Edit(this, new EventArgs());
		}

		protected void OnUnlockClicked(object sender, EventArgs e)
		{
			if (Unlock != null)
				Unlock(this, new EventArgs());
		}

		protected void OnMoreClicked(object sender, EventArgs e)
		{
			if (More != null)
				More(this, new EventArgs());
		}

		protected void OnSignOutClicked(object sender, EventArgs e)
		{
			var returnUrl = string.IsNullOrEmpty(Session[WebApplicationConstants.PartnerReturnUrlKey].GetString())
								? _Default.PageAddress
								: Session[WebApplicationConstants.PartnerReturnUrlKey].GetString();

			// clean up
			Session[WebApplicationConstants.PartnerReturnUrlKey] = string.Empty;
			Session[WebApplicationConstants.PartnerHomeUrlKey] = string.Empty;
			Session[WebApplicationConstants.AuthenticationErrorReturnUrl] = string.Empty;

			ActiveUser.RemoveAuthenticatedUser(returnUrl);
		}

		protected void OnCommandClicked(object sender, EventArgs e)
		{
			var button = sender as LinkButton;
			if (button == null) return;
			var hidden = button.Parent.FindControl("hidCommandArgs").ToCustomHiddenField();

			if (Command != null && hidden != null)
				Command(this, new ViewEventArgs<string>(hidden.Value));
		}


		protected void OnDownloadTemplateClicked(object sender, EventArgs e)
		{
			var path = ((LinkButton)sender).Parent.FindControl("hidTemplateName").ToCustomHiddenField().Value;
			Response.Export(Server.ReadFromFile(path));
		}
	}
}