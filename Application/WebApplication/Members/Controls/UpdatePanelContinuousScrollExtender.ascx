﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdatePanelContinuousScrollExtender.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.UpdatePanelContinuousScrollExtender" %>


<script type="text/javascript">
    $(document).ready(function () {


        if (jsHelper.IsNullOrEmpty('<%= ScrollableDivId %>')) BindWindowScroll();
        else BindDivScroll();


        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {

            if (!jsHelper.IsNullOrEmpty('<%= ScrollableDivId %>')) {
                BindDivScroll();
                ResetDivScrollTop();
            }

            $('#updateDivProcessing').fadeOut();

        });


    });

    function BindWindowScroll() {
        $(window).scroll(function () {
            if ($(document).height() <= $(window).scrollTop() + $(window).height() && !jsHelper.IsTrue($(jsHelper.AddHashTag('<%= hidAllRecordsLoaded.ClientID %>')).val())) {
                $('#updateDivProcessing').fadeIn();
                $('#updateDivProcessing').css('top', $(window).height() - $('#updateDivProcessing').height());
                window.__doPostBack('<%= UpdatePanelClientId %>', '<%= ClientID %>');
            }
        });
    }

    function BindDivScroll() {
        $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).scroll(function () {
            if (($(jsHelper.AddHashTag('<%= ScrollableDivId %>')).prop('scrollHeight') - $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).scrollTop() == $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).height()) && (Number('<%= DataSource != null ? DataSource.Cast<object>().Count() : 0 %>') > Number($(jsHelper.AddHashTag('<%= hidCurrentlyLoadedRecords.ClientID %>')).val()))) {
                $('#updateDivProcessing').fadeIn();
                $('#updateDivProcessing').css('top', $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).offset().top + $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).height() - $('#updateDivProcessing').height() - $(window).scrollTop());
                $('#updateDivProcessing').css('left', $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).css('left'));
                $('#updateDivProcessing').css('width', $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).width());
                $(jsHelper.AddHashTag('<%= hidElementScrollTop.ClientID %>')).val($(jsHelper.AddHashTag('<%= ScrollableDivId %>')).scrollTop());
                window.__doPostBack('<%= UpdatePanelClientId %>', '<%= ClientID %>');
            }
        });

    }

    function ResetDivScrollTop() {
        $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).scrollTop(Number(($(jsHelper.AddHashTag('<%= hidElementScrollTop.ClientID %>')).val())));
    }

</script>

<div class="dimBackground" id="updateDivProcessing" style="display: none; cursor: wait; height: 35px !important;">
    <div align="center" style="color: white; font-style: italic; font-weight: bold; font-size: 1.5em; margin: auto; display: block; padding-top: 5px;">
        Loading more records ...
    </div>
</div>

<asp:HiddenField runat="server" ID="hidElementScrollTop" Value="0" />
<asp:HiddenField runat="server" ID="hidCurrentlyLoadedRecords" />
<asp:HiddenField runat="server" ID="hidAllRecordsLoaded" Value="True"/>

<asp:HiddenField runat="server" ID="hidBindableControlId" />
<asp:HiddenField runat="server" ID="hidUpdatePanelToExtendControlId" />
<asp:HiddenField runat="server" ID="hidScrollableDivId" />


<asp:HiddenField runat="server" ID="hidCheckBoxControlId"/>
<asp:HiddenField runat="server" ID="hidUniqueRecordRefControlId"/>
<asp:HiddenField runat="server" ID="hidPreserveRecordSelectionId"/>
