﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageBoxControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.MessageBoxControl" %>

<asp:Panel ID="pnlMessages" runat="server" Visible="false" CssClass="popupControlOver trans-background no-border">
    <table id="messageTable">
        <tr>
            <td class="p_m0" colspan="2">
                <div class="popheader">
                    <h4>
                        <asp:Literal runat="server" Text="MESSAGES" ID="title" />
                    </h4>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-center middle">
                <asp:Image ID="imgInformation" ImageUrl="~/images/icons/messageIcons/information.png"
                    Width="32" Height="32" runat="server" />
                <asp:Image ID="imgError" ImageUrl="~/images/icons/messageIcons/error.png" Width="32"
                    Height="32" runat="server" />
                <asp:Image ID="imgWarning" ImageUrl="~/images/icons/messageIcons/warning.png" Width="32"
                    Height="32" runat="server" />
                <asp:Image ID="imgQuestion" ImageUrl="~/images/icons/messageIcons/question.png" Width="32"
                    Height="32" runat="server" />
            </td>
            <td>
                <asp:Literal ID="message" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="text-center" colspan="2">
                <script type="text/javascript">
                    $(window).load(function() {
                        var okBtn = $(jsHelper.AddHashTag('<%= btnOkay.ClientID %>'));
                        if (okBtn) {
                            okBtn.focus();
                            return;
                        }
                        okBtn = $(jsHelper.AddHashTag('<%= btnYes.ClientID %>'));
                            if (okBtn) {
                                okBtn.focus();
                            }
                    });

                    $(document).ready(function () {

                        $('#<%= btnOkay.ClientID %>').click(function (e) {
                            if (jsHelper.IsTrue($('#<%= hidHandleClientSide.ClientID %>').val())) {
                                HidePanels();
                                SetControlFocus();
                                e.preventDefault();
                            }
                        });

                        $('#<%= btnYes.ClientID %>').click(function (e) {
                            if (jsHelper.IsTrue($('#<%= hidHandleClientSide.ClientID %>').val())) {
                                HidePanels();
                                SetControlFocus();
                                e.preventDefault();
                            }
                        });

                        $('#<%= btnNo.ClientID %>').click(function (e) {
                            if (jsHelper.IsTrue($('#<%= hidHandleClientSide.ClientID %>').val())) {
                                HidePanels();
                                SetControlFocus();
                                e.preventDefault();
                            }
                        });

                        $('#<%= btnCancel.ClientID %>').click(function (e) {
                            if (jsHelper.IsTrue($('#<%= hidHandleClientSide.ClientID %>').val())) {
                                HidePanels();
                                SetControlFocus();
                                e.preventDefault();
                            }
                        });

                        function HidePanels() {
                            $('#<%= pnlMessages.ClientID %>').hide();
                            $('#<%= pnlPopUpDimScreen.ClientID %>').hide();
                        }
                    });
                </script>

                <asp:Button ID="btnOkay" Text="OK" OnClick="OnOkayClicked" runat="server" Visible="true"
                    CausesValidation="false" />
                <asp:Button ID="btnYes" Text="Yes" OnClick="OnYesClicked" runat="server" Visible="false"
                    CausesValidation="false" />
                <asp:Button ID="btnNo" Text="No" OnClick="OnNoClicked" runat="server" Visible="false"
                    CausesValidation="false" />
                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCancelClicked" runat="server"
                    Visible="false" CausesValidation="false" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlPopUpDimScreen" CssClass="dimBackgroundControlOver" runat="server" Visible="false" />
<eShip:CustomHiddenField runat="server" ID="hidHandleClientSide" />
<eShip:CustomHiddenField runat="server" ID="hidHandleButtonsClientSide" />
