﻿namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public enum MessageIcon
	{
		Error = 0,
		Information,
		Warning,
		Question
	}
}
