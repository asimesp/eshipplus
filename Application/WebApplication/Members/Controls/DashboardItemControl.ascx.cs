﻿using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class DashboardItemControl : MemberControlBase
	{
		public string IconName { get; set; }

		public string IconUrl { get; set; }

		public ViewCode ViewCode { get; set; }
    
        public bool OpenInNewWindow { get; set; }
    }
}