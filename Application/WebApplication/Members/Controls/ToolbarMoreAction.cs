﻿namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public class ToolbarMoreAction
	{
		public string NavigationUrl { get; set; }
		public string ImageUrl { get; set; }
		public string Name { get; set; }

		public string CommandArgs { get; set; }
		
		public bool ConfirmCommand { get; set; }
		public string CustomConfirmationMessage { get; set; }

		public string ConfirmationMessage
		{
			get
			{
				return string.IsNullOrEmpty(CustomConfirmationMessage)
				       	? string.Format("Are you sure you want to {0}?", Name)
				       	: CustomConfirmationMessage;
			}
		}

		public bool IsLink { get; set; }
		public bool IsCommand { get { return !IsLink && !IsSeperator; } }
		public bool IsSeperator { get; set; }

		public bool OpenInNewWindow { get; set; }

		public bool EnableProcessingDiv { get; set; }


		public ToolbarMoreAction()
		{
			NavigationUrl = string.Empty;
			ImageUrl = string.Empty;
			Name = string.Empty;
			CommandArgs = string.Empty;
			ConfirmCommand = false;
			CustomConfirmationMessage = string.Empty;
			IsLink = false;
			IsSeperator = false;
			OpenInNewWindow = false;
			EnableProcessingDiv = false;
		}
	}
}
