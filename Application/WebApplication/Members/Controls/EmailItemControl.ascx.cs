﻿using System;
using System.Web.UI;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
    public partial class EmailItemControl : UserControl
    {
        public bool ShowBcc
        {
            set { pnlBcc.Visible = value; }
        }

        public bool ShowCc
        {
            set { pnlCc.Visible = value; }
        }

        public string Attachments
        {
            set { litAttachment.Text = value; }
        }

        public string To
        {
            get { return txtTo.Text.StripSpacesFromEmails(); }
            set { txtTo.Text = value; }
        }

        public string Cc
        {
            get { return txtCc.Text.StripSpacesFromEmails(); }
            set { txtCc.Text = value; }
        }

        public string Bcc
        {
            get { return txtBcc.Text.StripSpacesFromEmails(); }
            set { txtBcc.Text = value; }
        }

        public string Body
        {
            get { return body.Content; }
            set { body.Content = value; }
        }

        public string Subject
        {
            get { return txtSubject.Text; }
            set { txtSubject.Text = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}