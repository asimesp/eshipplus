﻿using System;
using System.Web.UI.WebControls;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public class CustomHiddenField : TextBox
	{
	    public string Value
	    {
            get { return base.Text; }
            set { base.Text = value; }
	    }

		public new string Text
		{
			get { throw new NotSupportedException("Inproper use of control.  Property is disabled.  Use Value instead!"); }
			set { throw new NotSupportedException("Inproper use of control.  Property is disabled.  Use Value instead!"); }
		}

		protected override void OnLoad(EventArgs e)
		{
            Attributes.Add("style", "display: none;");

			base.OnLoad(e);
		}
	}
}