﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardItemControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.DashboardItemControl" %>
<li>
    <asp:HyperLink runat="server" ID="hypDashboardItem" NavigateUrl='<%# IconUrl %>' Target=<%# OpenInNewWindow ? "_blank" : "_self" %>>
        <asp:Literal runat="server" ID="litDashboardItem" Text='<%# !string.IsNullOrEmpty(IconName) ? IconName : "&nbsp;" %>' />
    </asp:HyperLink>
</li>
