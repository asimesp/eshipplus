﻿namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public enum WildcardCriteriaOptionsDefault
	{
		Exact,
		BeginsWith,
		EndsWith,
		Contains
	}
}
