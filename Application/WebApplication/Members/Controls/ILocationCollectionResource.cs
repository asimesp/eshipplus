﻿using System.Collections.Generic;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public interface ILocationCollectionResource
	{
		List<ViewListItem> ContactTypeCollection { get; }
		List<ViewListItem> CountryCollection { get; }

	}
}
