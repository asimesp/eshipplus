﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.UI;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
    public partial class DocumentDisplayControl : MemberControlBase
    {
        protected const string PdfFlag = "P";
        protected const string HtmlFlag = "H";

        public string Title
        {
            get { return litTitle.Text; }
            set
            {
                litTitle.Text = value;
                litEmailTitle.Text = string.Format("Send {0}", value);
            }
        }

        public new bool Visible
        {
            get { return pnlDocumentViewer.Visible; }
            set
            {
                base.Visible = true;
                pnlDocumentViewer.Visible = value;
                pnlDocumentDisplayDimScreen.Visible = value;
            }
        }

        public bool EnableEmailBcc
        {
            set { emailItems.ShowBcc = value; }
        }

        public bool EnalbeEmailCc
        {
            set { emailItems.ShowCc = value; }
        }

        public event EventHandler Close;

        public void DisplayHtml(string htmlContent)
        {
            var displayHtml = htmlContent.RetrieveWrapperSection(BaseFields.BodyWrapperStart, BaseFields.BodyWrapperEnd);
            if (!string.IsNullOrEmpty(displayHtml)) htmlContent = htmlContent.Replace(displayHtml, string.Empty);
            hidSaveHtmlWrapper.Value = htmlContent.StripTags();
            contentDiv.InnerHtml = displayHtml;
        }

        private void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private string RetrieveDisplayHtml()
        {
            var html = hidSaveHtmlWrapper.Value
                .ReplaceTags()
                .Replace(BaseFields.BodyWrapperStart, contentDiv.InnerHtml)
                .Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            return html;
        }

        private string RetrieveDisplayHtml(string htmlInjection)
        {
            var html = hidSaveHtmlWrapper.Value
                .ReplaceTags()
                .Replace(BaseFields.BodyWrapperStart, string.Format("{0}{1}{1}{2}", htmlInjection, WebApplicationConstants.HtmlBreak, contentDiv.InnerHtml))
                .Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            return html;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnbtnHtmlClicked(object sender, ImageClickEventArgs e)
        {
            var title = string.Format("eShipPlus_TMS_{0}_{1}", WebApplicationSettings.Version, Title);
            Response.Export(RetrieveDisplayHtml(), string.Format("{0}.html", title.Replace(" ", "_")));
        }

        protected void OnbtnPdfClicked(object sender, ImageClickEventArgs e)
        {
            var title = string.Format("eShipPlus_TMS_{0}_{1}", WebApplicationSettings.Version, Title);
            Response.Export(RetrieveDisplayHtml().ToPdf(title), string.Format("{0}.pdf", title.Replace(" ", "_")));
        }

        protected void OnCloseClicked(object sender, ImageClickEventArgs e)
        {
            if (Close != null)
                Close(this, new EventArgs());
        }


        protected void OnSendClicked(object sender, EventArgs e)
        {
            try
            {
                var seperators = WebApplicationUtilities.Seperators();
                var tos = emailItems.To
                    .Split(seperators, StringSplitOptions.RemoveEmptyEntries)
                    .Distinct()
                    .ToList();
                var ccs = emailItems.Cc
                    .Split(seperators, StringSplitOptions.RemoveEmptyEntries)
                    .Distinct()
                    .ToList();
                var bccs = emailItems.Bcc
                    .Split(seperators, StringSplitOptions.RemoveEmptyEntries)
                    .Distinct()
                    .ToList();
                var title = string.Format("eShipPlus_TMS_{0}_{1}", WebApplicationSettings.Version, Title);
                var subject = emailItems.Subject;

                if (!string.IsNullOrEmpty(ActiveUser.Email) && !tos.Contains(ActiveUser.Email) && !ccs.Contains(ActiveUser.Email) && !bccs.Contains(ActiveUser.Email))
                    tos.Add(ActiveUser.Email);

                if (!tos.Any()) tos.Add(WebApplicationSettings.DoNotReplyEmail);

                switch (hidHtmlPdfEmail.Value)
                {
                    case PdfFlag:
                        tos.QueueEmail(ccs, bccs, emailItems.Body, subject,
                                      new[]
                                        {
                                            new Attachment(new MemoryStream(RetrieveDisplayHtml().ToPdf(title)),
                                                           string.Format("{0}.pdf", title))
                                        }, ActiveUser.TenantId);
                        break;
                    default:
                        tos.QueueEmail(ccs, bccs, RetrieveDisplayHtml(emailItems.Body), subject, new Attachment[0], ActiveUser.TenantId);
                        break;
                }

                pnlEmail.Attributes["style"] = "display: none"; // hides email panel
            }
            catch (Exception ex)
            {
                DisplayMessages(new[] { ValidationMessage.Error("An error occurred sending your email: {0}{0} {1}", WebApplicationConstants.HtmlBreak, ex.Message) });
            }
        }
    }
}