﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public class CachedObjectDropDownList : DropDownList
	{
		private const string DefaultValueKey = "DVK";
		private const string ChooseOneKey = "COK";
		private const string NotApplicableKey = "NAK";
		private const string NotApplicableLongKey = "NALK";
		private const string CachedObjectDropDownTypeKey = "CODTK";

		private readonly ListItem _unknownItem = new ListItem("- UNKNOWN ITEM -", UnknownValue);

		private long _filterAccountBucketId = default(long);


		public const string UnknownValue = "#UNKNOWN#";


		private static User ActiveUser
		{
			get { return HttpContext.Current.RetrieveAuthenticatedUser() ?? new User(); }
		}

		public new Object DataSource
		{
			get
			{
				throw new NotSupportedException("Incorrect use of control.  Control self loads its data!");
			}
			set
			{
				throw new NotSupportedException("Incorrect use of control.  Control self loads its data!");
			}
		}


		public string DefaultValue
		{
			// we want the null value if not set 
			get { return ViewState[DefaultValueKey] as string; }
			set { ViewState[DefaultValueKey] = value; }
		}

		public bool EnableChooseOne
		{
			get { return ViewState[ChooseOneKey].ToBoolean(); }
			set { ViewState[ChooseOneKey] = value.ToString(); }
		}

		public bool EnableNotApplicable
		{
			get { return ViewState[NotApplicableKey].ToBoolean(); }
			set { ViewState[NotApplicableKey] = value.ToString(); }
		}

		public bool EnableNotApplicableLong
		{
			get { return ViewState[NotApplicableLongKey].ToBoolean(); }
			set { ViewState[NotApplicableLongKey] = value.ToString(); }
		}

		/// <summary>
		///		Set value of control.
		///		NOTE: When Type == AccountBucketUnits, Set value will fail.  Use SetSelectedAccountBucketUnit method instead!
		/// </summary>
		public new string SelectedValue
		{
			get { return base.SelectedValue; }
			set
			{
				base.SelectedValue = LoadDdlAndReturnValueToSet(value);
			}
		}

		public CachedObjectDropDownListType Type
		{
			get
			{
				return string.IsNullOrEmpty(ViewState[CachedObjectDropDownTypeKey].GetString())
						   ? CachedObjectDropDownListType.NotSet
						   : ViewState[CachedObjectDropDownTypeKey].GetString().ToEnum<CachedObjectDropDownListType>();
			}
			set { ViewState[CachedObjectDropDownTypeKey] = value.ToString(); }
		}

		/*
		 *	This method is specifically for filtering account bucket units based on filtered account bucket id.
		 *	Units are always related to a particular account bucket.
		 */
		public void SetSelectedAccountBucketUnit(long id, long filterAccountBucketId, bool defaultValueIsUnknown = false)
		{
			_filterAccountBucketId = filterAccountBucketId;
			SelectedValue = id.GetString();
		}


		/*
		 *	This method will evaluate if a selected value needs added to the dropdown list values.
		 *	If value is not found, then value return is unknownValue else it will be selected value.
		 */
		private string LoadDdlAndReturnValueToSet(string selectedValue = "")
		{
			// handle selected value against possible default value
			if (selectedValue == DefaultValue) selectedValue = string.Empty;

			var data = new List<ListItem>();

			switch (Type)
			{
				case CachedObjectDropDownListType.AccountBuckets:
					var abs = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.AccountBuckets
						.Select(i => new ListItem(string.Format("{0} - {1}", i.Code, i.Description), i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(abs);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						selectedValue = UnknownValue;
						if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
					}
					break;

				case CachedObjectDropDownListType.AccountBucketUnits:
					var abus = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.AccountBucketUnits
						.Where(abu => abu.AccountBucketId == _filterAccountBucketId && abu.Active)
						.Select(i => new ListItem(i.Name, i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(abus);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						var id = selectedValue.ToLong();
						var abu = ProcessorVars.RegistryCache[ActiveUser.TenantId].AccountBucketUnits.FirstOrDefault(i => i.Id == id);
						if (abu != null) data.Add(new ListItem(abu.Description, abu.Id.GetString()));
						if (abu == null)
						{
							selectedValue = UnknownValue;
							if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
						}
					}
					break;

				case CachedObjectDropDownListType.ContactTypes:
					var cts = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.ContactTypes
						.Select(i => new ListItem(i.Code, i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(cts);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						selectedValue = UnknownValue;
						if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
					}
					break;

				case CachedObjectDropDownListType.Countries:
					var cs = ProcessorVars
						.RegistryCache
						.Countries
						.Values
						.OrderByDescending(t => t.SortWeight)
						.ThenBy(t => t.Name)
						.Select(i => new ListItem(i.Name, i.Id.GetString()))
						.ToList();
					data.AddRange(cs);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						selectedValue = UnknownValue;
						if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
					}
					break;

				case CachedObjectDropDownListType.ChargeCodes:
					var ccs = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.ChargeCodes
						.Where(c => c.Active)
						.Select(i => new ListItem(i.FormattedString(), i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(ccs);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						var id = selectedValue.ToLong();
						var cc = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.FirstOrDefault(i => i.Id == id);
						if (cc != null) data.Add(new ListItem(cc.FormattedString(), cc.Id.GetString()));
						if (cc == null)
						{
							selectedValue = UnknownValue;
							if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
						}
					}
					break;

				case CachedObjectDropDownListType.DocumentTag:
					var dts = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.DocumentTags
						.Where(c => ActiveUser.TenantEmployee || c.NonEmployeeVisible)
						.Select(i => new ListItem(i.FormattedString(), i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(dts);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						var id = selectedValue.ToLong();
						var dt = ProcessorVars.RegistryCache[ActiveUser.TenantId].DocumentTags.FirstOrDefault(i => i.Id == id);
						if (dt != null) data.Add(new ListItem(dt.FormattedString(), dt.Id.GetString()));
						if (dt == null)
						{
							selectedValue = UnknownValue;
							if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
						}

					}
					break;

				case CachedObjectDropDownListType.EquipmentType:
					var ets = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.EquipmentTypes
						.Where(c => c.Active)
						.Select(i => new ListItem(i.FormattedString(), i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(ets);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						var id = selectedValue.ToLong();
						var et = ProcessorVars.RegistryCache[ActiveUser.TenantId].EquipmentTypes.FirstOrDefault(i => i.Id == id);
						if (et != null) data.Add(new ListItem(et.FormattedString(), et.Id.GetString()));
						if (et == null)
						{
							selectedValue = UnknownValue;
							if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
						}
					}
					break;

				case CachedObjectDropDownListType.FailureCodes:
					var fcs = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.FailureCodes
						.Where(c => c.Active)
						.Select(i => new ListItem(i.FormattedString(), i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(fcs);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						var id = selectedValue.ToLong();
						var fc = ProcessorVars.RegistryCache[ActiveUser.TenantId].FailureCodes.FirstOrDefault(i => i.Id == id);
						if (fc != null) data.Add(new ListItem(fc.FormattedString(), fc.Id.GetString()));
						if (fc == null)
						{
							selectedValue = UnknownValue;
							if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
						}
					}
					break;

				case CachedObjectDropDownListType.InsuranceTypes:
					var its = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.InsuranceTypes
						.Select(i => new ListItem(i.FormattedString(), i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(its);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						selectedValue = UnknownValue;
						if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
					}
					break;

				case CachedObjectDropDownListType.MileageSources:
					var mss = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.MileageSources
						.Select(i => new ListItem(i.FormattedString(), i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(mss);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						selectedValue = UnknownValue;
						if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
					}
					break;

				case CachedObjectDropDownListType.PackageType:
					var pts = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.PackageTypes
						.OrderByDescending(t => t.SortWeight)
						.ThenBy(t => t.TypeName)
						.Where(c => c.Active)
						.Select(i => new ListItem(i.TypeName, i.Id.GetString()))
						.ToList();
					data.AddRange(pts);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						var id = selectedValue.ToLong();
						var pt = ProcessorVars.RegistryCache[ActiveUser.TenantId].PackageTypes.FirstOrDefault(i => i.Id == id);
						if (pt != null) data.Add(new ListItem(pt.TypeName, pt.Id.GetString()));
						if (pt == null)
						{
							selectedValue = UnknownValue;
							if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
						}
					}
					break;

				case CachedObjectDropDownListType.Prefixes:
					var ps = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.Prefixes
						.Select(i => new ListItem(string.Format("{0} - {1}", i.Code, i.Description), i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(ps);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						selectedValue = UnknownValue;
						if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
					}
					break;

				case CachedObjectDropDownListType.Services:
					var ss = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.Services
						.Select(i => new ListItem(i.FormattedString(), i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(ss);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						selectedValue = UnknownValue;
						if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
					}
					break;

				case CachedObjectDropDownListType.ShipmentPriority:
					var sps = ProcessorVars
						.RegistryCache[ActiveUser.TenantId]
						.ShipmentPriorities
						.Where(c => c.Active)
						.Select(i => new ListItem(i.FormattedString(), i.Id.GetString()))
						.OrderBy(i => i.Text)
						.ToList();
					data.AddRange(sps);
					if (!string.IsNullOrEmpty(selectedValue) && data.All(i => i.Value != selectedValue))
					{
						var id = selectedValue.ToLong();
						var sp = ProcessorVars.RegistryCache[ActiveUser.TenantId].ShipmentPriorities.FirstOrDefault(i => i.Id == id);
						if (sp != null) data.Add(new ListItem(sp.FormattedString(), sp.Id.GetString()));
						if (sp == null)
						{
							selectedValue = UnknownValue;
							if (data.All(i => i.Value != UnknownValue)) data.Add(_unknownItem);
						}
					}
					break;
			}


			if (EnableChooseOne) data.Insert(0, new ListItem(WebApplicationConstants.ChooseOne, string.Empty));
			else if (EnableNotApplicable) data.Insert(0, new ListItem(WebApplicationConstants.NotApplicableShort, string.Empty));
			else if (EnableNotApplicableLong) data.Insert(0, new ListItem(WebApplicationConstants.NotApplicable, string.Empty));

			base.Items.Clear();
			base.Items.AddRange(data.ToArray());

			return selectedValue;
		}

		protected override void OnLoad(EventArgs e)
		{
			if (Page.IsPostBack || ActiveUser.TenantId == default(long))
			{
				base.OnLoad(e);
				return;
			}
            // If items are empty, load
            if(base.Items.Count == 0)
                LoadDdlAndReturnValueToSet();

			base.OnLoad(e);
		}

		public new void DataBind()
		{
			throw new NotSupportedException("Incorrect use of control.  Control self loads its data!");
		}
	}
}