﻿namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public enum CachedObjectDropDownListType
	{
		// NOTE: Keep A - Z
		AccountBuckets,
		AccountBucketUnits,
		ContactTypes,
		Countries,
		ChargeCodes,
		DocumentTag,
		EquipmentType,
		FailureCodes,
		InsuranceTypes,
		MileageSources,
		NotSet,
		PackageType,
		Prefixes,
		Services,
		ShipmentPriority,
	}
}