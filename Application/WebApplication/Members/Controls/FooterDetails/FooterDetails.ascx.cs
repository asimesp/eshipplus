﻿using System;
using System.Web.UI;
using LogisticsPlus.Eship.WebApplication.Public;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls.FooterDetails
{
	public partial class FooterDetails : UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return;

			hypToC.NavigateUrl = ToC.PageAddress;
			litVersion.Text = WebApplicationSettings.Version;
		}
	}
}