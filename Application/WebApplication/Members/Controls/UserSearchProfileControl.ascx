﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserSearchProfileControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.UserSearchProfileControl" %>

<label class="pr10">
    Search Profile Options:
</label>

<asp:Button runat="server" ID="btnSaveProfileAsDefault" CausesValidation="False" Text="    Add    " />
<asp:Button runat="server" ID="btnUpdateProfile" CausesValidation="False" Text="Update" Visible="False" OnClick="OnUpdateProfileClicked" />
<asp:Button runat="server" ID="btnDeleteCurrentProfile" CausesValidation="False" OnClick="OnDeleteProfileClicked" Text="Delete" CssClass="mr5" />

<asp:DropDownList runat="server" ID="ddlSearchProfiles" DataValueField="Value" DataTextField="Text" AutoPostBack="True" CausesValidation="False"
     OnSelectedIndexChanged="OnSearchProfilesSelectedIndexChanged" CssClass="middle w200" />

<asp:DropDownList runat="server" ID="ddlUpdateInterval" DataValueField="Value" DataTextField="Text" AutoPostBack="True" CausesValidation="False" Visible="False"
    OnSelectedIndexChanged="OnUpdateIntervalSelectedIndexChanged" CssClass="middle w150" />

<script type="text/javascript">
    $(document).ready(function () {
        $(jsHelper.AddHashTag('<%= btnSearchProfileClose.ClientID %>')).click(function (e) {
            e.preventDefault();
            $(jsHelper.AddHashTag('profileNameDiv<%= ClientID %>')).hide('slow');
            $(jsHelper.AddHashTag('<%= pnlDimScreenJS.ClientID %>')).hide();
        });

        $(jsHelper.AddHashTag('<%= btnSaveProfileAsDefault.ClientID %>')).click(function (e) {
            $(jsHelper.AddHashTag('profileNameDiv<%= ClientID %>')).show('slow');
            $(jsHelper.AddHashTag('<%= pnlDimScreenJS.ClientID %>')).show();
	        jsHelper.SetEmpty('<%= txtSearchProfileName.ClientID %>');
	        e.preventDefault();
        });
    });
</script>

<div id="profileNameDiv<%= ClientID %>" class="popupControlOver popupControlOverW500 inherit-left inherit-top" style="display: none;">
    <div class="popheader">
        <h4>Search Default Profile Option(s)</h4>
    </div>
    <table class="poptable">
        <tr>
            <td class="text-right"><label class="upper">Profile Name:</label>
            </td>
            <td class="text-left pt10 pb20">
                <eShip:CustomTextBox runat="server" ID="txtSearchProfileName" CssClass="w230" />
                <asp:Button runat="server" ID="btnSaveSearchProfile" Text="   Save   " CausesValidation="False" CssClass="ml10" OnClick="OnSaveProfileClicked" />
                <asp:Button runat="server" ID="btnSearchProfileClose" Text="Cancel" CausesValidation="False" />
            </td>
        </tr>
    </table>
</div>

<asp:Panel ID="pnlDimScreenJS" CssClass="dimBackgroundControlOver" runat="server" Style="display: none;" />
<eShip:CustomHiddenField runat="server" ID="hidType" />
