﻿using System;
using System.Web.UI;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls.FreeHeaderExtender
{
	public partial class TableFreezeHeaderExtender : UserControl
	{
		public string ScrollerContainerId
		{
			protected get
			{
				var control = Parent == null ? null : Parent.FindControl(hidScrollerContainerId.Value);
				return control == null ? hidScrollerContainerId.Value : control.ClientID;
			}
			set { hidScrollerContainerId.Value = value; }
		}

		public string ScrollOffsetControlId
		{
			protected get
			{
				var control = Parent == null ? null : Parent.FindControl(hidScrollOffsetControlId.Value);
				return control == null ? hidScrollOffsetControlId.Value : control.ClientID;
			}
			set { hidScrollOffsetControlId.Value = value; }
		}

		public bool IgnoreHeaderBackgroundFill
		{
			protected get { return hidDoNotFillHeaderBackgroup.Value.ToBoolean(); }
			set { hidDoNotFillHeaderBackgroup.Value = value.GetString(); }
		}

		public string TableId
		{
			protected get
			{
				var control = Parent == null ? null : Parent.FindControl(hidTableId.Value);
				return control == null ? hidTableId.Value : control.ClientID;
			}
			set { hidTableId.Value = value; }
		}

		public string HeaderZIndex
		{
			protected get { return hidZindex.Value; }
			set { hidZindex.Value = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}