﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationListingInputControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.InputControls.LocationListingInputControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>

<asp:Panel ID="pnlLocationListingInputControl" runat="server">
    <div class="rowgroup mb0">
        <div class="col_1_3 vlinedarkright">
            <h5 class="dark">
                <asp:ImageButton runat="server" ID="ibtnDeleteAddress" Text="Delete Location" OnClick="OnDeleteAddressClicked" ImageUrl="~/images/icons2/deleteX.png" Height="20px" ToolTip="Delete Location"
                    CausesValidation="False" OnClientClick="ShowProcessingDivBlock();" />
                Address
            </h5>
            <div class="rowgroup">
                <div class="row mb10">
                    <asp:Panel runat="server" ID="pnlPrimary" Visible="false" CssClass="fieldgroup">
                        <eShip:AltUniformCheckBox runat="server" ID="chkPrimary" />
                        <label>Primary</label>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlBillToRemitTo" Visible="false">
                        <div class="fieldgroup">
                            <eShip:AltUniformCheckBox runat="server" ID="chkBillToRemitTo" />
                            <label>
                                <asp:Literal runat="server" ID="litBillToRemitTo" /></label>
                        </div>
                        <div class="fieldgroup">
                            <eShip:AltUniformCheckBox runat="server" ID="chkBillToRemitToMain" />
                            <label>
                                <asp:Literal runat="server" ID="litBillToRemitToMain" /></label>
                        </div>
                    </asp:Panel>
                </div>
                <asp:Panel runat="server" ID="pnlDescription" Visible="False" CssClass="row">
                    <div class="fieldgroup">
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $(jsHelper.AddHashTag('<%= txtDescription.ClientID %>')).change(function () {
                                    var txt = $(this).val();
                                    if (txt.length == 0) return;
                                    var idx = txt.indexOf('<%= AutoCompleteUtility.AutoCompleteSeperator %>');
                                    if (idx != -1 && txt[0] == '<%= AutoCompleteUtility.Equal %>') {
                                        setTimeout('__doPostBack(\'<%= txtDescription.ClientID %>\')', 0);
                                    }
                                });
                            });

                        </script>
                        <label class="wlabel">Name/Description</label>
                        <eShip:CustomTextBox ID="txtDescription" runat="server" MaxLength="50" CssClass="w200" OnTextChanged="OnDescriptionTextChanged" />
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlAppointmentDateTime" Visible="False" CssClass="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Appointment Date</label>
                        <eShip:CustomTextBox runat="server" ID="txtAppointmentDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                    </div>
                    <div class="fieldgroup">
                        <label class="wlabel">Appointment Time</label>
                        <eShip:CustomTextBox runat="server" ID="fftTime" CssClass="w55" Type="Time" placeholder="99:99" />
                    </div>
                </asp:Panel>

                <eShip:SimpleAddressInputControl ID="saiAddressInput" runat="server" />

                <asp:Panel runat="server" ID="pnlMilesFromPreceedingStop" Visible="False" CssClass="row">
                    <div class="fieldgroup mt10">
                        <label class="wlabel">
                            Miles From Previous Location/Stop
                        </label>
                        <eShip:CustomTextBox runat="server" ID="txtMilesFromPreviousStop" CssClass="w100" Type="FloatingPointNumbers" />
                    </div>
                </asp:Panel>
                
                 <asp:Panel runat="server" ID="pnlLocationCode" Visible="False" CssClass="row">
                    <div class="fieldgroup mt10">
                        <label class="wlabel">
                            Location Code
                        </label>
                        <eShip:CustomTextBox runat="server" ID="txtLocationCode" CssClass="w200 disabled" Type="ReadOnly" />
                    </div>
                </asp:Panel>

            </div>
        </div>
        <div class="col_2_3 bbox">


            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <h5 class="dark mb10">
                        <asp:ImageButton runat="server" ID="btnAddContact" Text="Add Contact" OnClick="OnAddContactClicked" ImageUrl="~/images/icons2/add_button.png" Height="20px" ToolTip="Add Contacts"
                            OnClientClick="ShowProgressNotice(true);" CausesValidation="False" />
                        Contacts
                    </h5>
                    <asp:ListView ID="lstContacts" runat="server" ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <table class="stripe">
                                <tr>
                                    <th style="width: 3%;">&nbsp;</th>
                                    <th style="width: 20%;">Name
                                    </th>
                                    <th style="width: 20%;">Phone
                                    </th>
                                    <th style="width: 20%;">Mobile
                                    </th>
                                    <th style="width: 20%;">Fax
                                    </th>
                                    <th style="width: 10%; text-align: center;">Primary
                                    </th>
                                    <th style="width: 7%;">Action
                                    </th>
                                </tr>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="top">
                                    <%# Container.DataItemIndex+1 %>.
                                </td>
                                <td class="text-left">
                                    <eShip:CustomTextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' CssClass="w130" />
                                    <eShip:CustomHiddenField ID="hidContactId" runat="server" Value='<%# Eval("Id") %>' />
                                    <eShip:CustomHiddenField runat="server" ID="hidContactIndex" Value='<%# Container.DataItemIndex %>' />
                                </td>
                                <td class="text-left">
                                    <eShip:CustomTextBox ID="txtPhone" runat="server" Text='<%# Eval("Phone") %>' CssClass="w100" />
                                </td>
                                <td class="text-left">
                                    <eShip:CustomTextBox ID="txtMobile" runat="server" Text='<%# Eval("Mobile") %>' CssClass="w100" />
                                </td>
                                <td class="text-left">
                                    <eShip:CustomTextBox ID="txtFax" runat="server" Text='<%# Eval("Fax") %>' CssClass="w100" />
                                </td>
                                <td class="text-center">
                                    <eShip:AltUniformCheckBox runat="server" ID="chkPrimary" Checked='<%# Eval("Primary") %>' CssClass="jQueryUniform" />
                                </td>
                                <td class="text-center top">
                                    <asp:ImageButton ID="ibtnDelete" ToolTip="Delete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProgressNotice(true);"
                                        CausesValidation="false" OnClick="OnContactDeleteClicked" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" class="hidden">&nbsp;</td>
                            </tr>
                            <tr class="bottom_shadow">
                                <td>&nbsp;</td>
                                <td colspan="6" class="top">
                                    <div class="rowgroup mb5">
                                        <div class="row">
                                            <div class="fieldgroup mr20 vlinedarkright">
                                                <label class="wlabel">Contact Type</label>
                                                <eShip:CachedObjectDropDownList Type="ContactTypes" ID="ddlContactTypes" runat="server" CssClass="w200" 
                                                    EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("ContactTypeId") %>' />
                                                <label class="wlabel">Email</label>
                                                <eShip:CustomTextBox ID="txtEmail" runat="server" Text='<%# Eval("Email") %>' CssClass="w200" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">
                                                    Comment<eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtComments" MaxLength="200" />
                                                </label>
                                                <eShip:CustomTextBox ID="txtComments" runat="server" CssClass="w310" TextMode="MultiLine" Rows="5" Text='<%# Eval("Comment") %>' />
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:Panel runat="server" ID="pnlSpecialInsGeneralInfoDirection" Visible="False" CssClass="rowgroup">
                <div class="row mt20">
                    <div class="fieldgroup mr10">
                        <label class="wlabel">
                            Special Instructions
                                <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleSpecialInstructions" MaxLength="500" TargetControlId="txtSpecialInstructions" />
                        </label>
                        <eShip:CustomTextBox ID="txtSpecialInstructions" runat="server" TextMode="MultiLine" CssClass="w650" />
                    </div>
                    <div class="fieldgroup mr10">
                        <label class="wlabel">
                            General Info
                                <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleGeneralInfo" TargetControlId="txtGeneralInfo" MaxLength="2000" />
                        </label>
                        <eShip:CustomTextBox ID="txtGeneralInfo" runat="server" TextMode="MultiLine" CssClass="w650" />
                    </div>
                    <div class="fieldgroup">
                        <label class="wlabel">
                            Directions
                                <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDirection" TargetControlId="txtDirection" MaxLength="2000" />
                        </label>
                        <eShip:CustomTextBox ID="txtDirection" runat="server" TextMode="MultiLine" CssClass="w650" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Panel>
<eShip:CustomHiddenField runat="server" ID="hidLocationId" />
<eShip:CustomHiddenField runat="server" ID="hidLocationItemIndex" />
<eShip:CustomHiddenField runat="server" ID="hidUseInstructions" />
