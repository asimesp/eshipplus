﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls.InputControls
{
	public partial class LocationListingInputControl : MemberControlBase
	{
		public const string UseOriginInstructions = "O";
		public const string UseDestinationInstructions = "D";

		public new bool Visible
		{
			get { return pnlLocationListingInputControl.Visible; }
			set
			{
				base.Visible = true;
				pnlLocationListingInputControl.Visible = value;
			}
		}

		public int LocationItemIndex
		{
			get { return RetrieveLocatinItemIndex(); }
			set { SetLocationItemIndex(value); }
		}

	    public string UseInstructions
		{
			get { return hidUseInstructions.Value; }
			set { hidUseInstructions.Value = value; }
		}

		public event EventHandler<ViewEventArgs<int>> DeleteLocation;

		public void LoadLocation<T>(T location) where T : Location
		{

			if (location as CustomerLocation != null) LoadCustomerLocation(location as CustomerLocation);
			if (location as VendorLocation != null) LoadVendorLocation(location as VendorLocation);
			if (location as ShipmentLocation != null) LoadShipmentLocation(location as ShipmentLocation);
			if (location as LoadOrderLocation != null) LoadLoadOrderLocation(location as LoadOrderLocation);
			if (location as PendingVendorLocation != null) LoadPendingVendorLocation(location as PendingVendorLocation);
		}


		public long RetrieveLocationId()
		{
			return hidLocationId.Value.ToLong();
		}

		public int RetrieveLocatinItemIndex()
		{
			return hidLocationItemIndex.Value.ToInt();
		}

		public void SetLocationItemIndex(int value)
		{
			hidLocationItemIndex.Value = value.ToString();
		}

		public void UpdateLocation<T>(T location) where T : Location
		{
			saiAddressInput.RetrieveAddress(location);

			if (location as CustomerLocation != null) UpdateCustomerLocation(location as CustomerLocation);
			if (location as VendorLocation != null) UpdateVendorLocation(location as VendorLocation);
			if (location as ShipmentLocation != null) UpdateShipmentLocation(location as ShipmentLocation);
			if (location as LoadOrderLocation != null) UpdateLoadOrderLocation(location as LoadOrderLocation);
			if (location as PendingVendorLocation != null) UpdatePendingVendorLocation(location as PendingVendorLocation);
		}

		private List<T> RetrieveLocationContacts<T>(IDictionary<long, T> contactsToUpdate) where T : Contact, new()
		{
			var vContacts = lstContacts.Items
				.Select(item => new
				{
					Id = item.FindControl("hidContactId").ToCustomHiddenField().Value.ToLong(),
					Comment = item.FindControl("txtComments").ToTextBox().Text,
					ContactTypeId = item.FindControl("ddlContactTypes").ToCachedObjectDropDownList().SelectedValue.ToLong(),
					Email = item.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
					Fax = item.FindControl("txtFax").ToTextBox().Text,
					Mobile = item.FindControl("txtMobile").ToTextBox().Text,
					Name = item.FindControl("txtName").ToTextBox().Text,
					Phone = item.FindControl("txtPhone").ToTextBox().Text,
					Primary = item.FindControl("chkPrimary").ToAltUniformCheckBox().Checked
				})
				.ToList();

			var contacts = new List<T>();
			for (var i = vContacts.Count - 1; i > -1; i--)
				if (contactsToUpdate.ContainsKey(vContacts[i].Id))
				{
					contactsToUpdate[vContacts[i].Id].TakeSnapShot(); // prior to updating

					contactsToUpdate[vContacts[i].Id].Comment = vContacts[i].Comment;
					contactsToUpdate[vContacts[i].Id].ContactTypeId = vContacts[i].ContactTypeId;
					contactsToUpdate[vContacts[i].Id].Email = vContacts[i].Email;
					contactsToUpdate[vContacts[i].Id].Fax = vContacts[i].Fax;
					contactsToUpdate[vContacts[i].Id].Mobile = vContacts[i].Mobile;
					contactsToUpdate[vContacts[i].Id].Name = vContacts[i].Name;
					contactsToUpdate[vContacts[i].Id].Phone = vContacts[i].Phone;
					contactsToUpdate[vContacts[i].Id].Primary = vContacts[i].Primary;

					contacts.Add(contactsToUpdate[vContacts[i].Id]); // add updated contact to list to return

					vContacts.RemoveAt(i); // remove from list of view contacts
				}
			// add new contacts
			contacts.AddRange(vContacts.Select(c => new T
			{
				Comment = c.Comment,
				ContactTypeId = c.ContactTypeId,
				Email = c.Email,
				Fax = c.Fax,
				Mobile = c.Mobile,
				Name = c.Name,
				Phone = c.Phone,
				Primary = c.Primary,
			}));
			return contacts;
		}


		private void UpdateVendorLocation(VendorLocation location)
		{
			location.Primary = chkPrimary.Checked;
			location.RemitToLocation = chkBillToRemitTo.Checked;
			location.MainRemitToLocation = chkBillToRemitToMain.Checked;
			location.LocationNumber = txtLocationCode.Text;

			location.Contacts = RetrieveLocationContacts(location.Contacts.ToDictionary(c => c.Id, c => c));

			// set location reference for contact
			foreach (var contact in location.Contacts.Where(contact => contact.IsNew))
			{
				contact.TenantId = location.TenantId;
				contact.VendorLocation = location;
			}
		}

		private void UpdateCustomerLocation(CustomerLocation location)
		{
			location.Primary = chkPrimary.Checked;
			location.BillToLocation = chkBillToRemitTo.Checked;
			location.MainBillToLocation = chkBillToRemitToMain.Checked;
			location.LocationNumber = txtLocationCode.Text;

			location.Contacts = RetrieveLocationContacts(location.Contacts.ToDictionary(c => c.Id, c => c));

			// set location reference for contact
			foreach (var contact in location.Contacts.Where(contact => contact.IsNew))
			{
				contact.TenantId = location.TenantId;
				contact.CustomerLocation = location;
			}
		}

		private void UpdateShipmentLocation(ShipmentLocation location)
		{
			location.Description = txtDescription.Text;
			location.SpecialInstructions = txtSpecialInstructions.Text;
			location.Direction = txtDirection.Text;
			location.GeneralInfo = txtGeneralInfo.Text;
			location.MilesFromPreceedingStop = txtMilesFromPreviousStop.Text.ToDecimal();
			location.StopOrder = LocationItemIndex + 1;
			location.Contacts = RetrieveLocationContacts(location.Contacts.ToDictionary(c => c.Id, c => c));

			location.AppointmentDateTime = string.IsNullOrEmpty(txtAppointmentDate.Text)
									? DateUtility.SystemEarliestDateTime
									: txtAppointmentDate.Text.ToDateTime();

			if (!string.IsNullOrEmpty(fftTime.Text) && fftTime.Text.IsValidFreeFormTimeString() && location.AppointmentDateTime != DateUtility.SystemEarliestDateTime)
				location.AppointmentDateTime = location.AppointmentDateTime.SetTime(fftTime.Text);

			// set location reference for contact
			foreach (var contact in location.Contacts.Where(contact => contact.IsNew))
			{
				contact.TenantId = location.TenantId;
				contact.Location = location;
			}
		}

		private void UpdateLoadOrderLocation(LoadOrderLocation location)
		{
			location.Description = txtDescription.Text;
			location.SpecialInstructions = txtSpecialInstructions.Text;
			location.Direction = txtDirection.Text;
			location.GeneralInfo = txtGeneralInfo.Text;
			location.MilesFromPreceedingStop = txtMilesFromPreviousStop.Text.ToDecimal();
			location.StopOrder = LocationItemIndex + 1;
			location.Contacts = RetrieveLocationContacts(location.Contacts.ToDictionary(c => c.Id, c => c));

			location.AppointmentDateTime = string.IsNullOrEmpty(txtAppointmentDate.Text)
											? DateUtility.SystemEarliestDateTime
											: txtAppointmentDate.Text.ToDateTime();

			if (!string.IsNullOrEmpty(fftTime.Text) && fftTime.Text.IsValidFreeFormTimeString() && location.AppointmentDateTime != DateUtility.SystemEarliestDateTime)
				location.AppointmentDateTime = location.AppointmentDateTime.SetTime(fftTime.Text);

			// set location reference for contact
			foreach (var contact in location.Contacts.Where(contact => contact.IsNew))
			{
				contact.TenantId = location.TenantId;
				contact.Location = location;
			}
		}

		private void UpdatePendingVendorLocation(PendingVendorLocation location)
		{
			location.Primary = chkPrimary.Checked;
			location.RemitToLocation = chkBillToRemitTo.Checked;
			location.MainRemitToLocation = chkBillToRemitToMain.Checked;
			location.LocationNumber = txtLocationCode.Text;

			location.Contacts = RetrieveLocationContacts(location.Contacts.ToDictionary(c => c.Id, c => c));

			// set location reference for contact
			foreach (var contact in location.Contacts.Where(contact => contact.IsNew))
			{
				contact.TenantId = location.TenantId;
				contact.PendingVendorLocation = location;
			}
		}



		private void LoadVendorLocation(VendorLocation location)
		{
			pnlPrimary.Visible = true;
			pnlBillToRemitTo.Visible = true;
			pnlLocationCode.Visible = true;
			litBillToRemitTo.Text = "Remit To:";
			litBillToRemitToMain.Text = "<abbr Title='In case of multiple remit to addresses, this is main remit to address.'>Main</abbr>";

			hidLocationId.Value = location.Id.ToString();

			saiAddressInput.LoadAddress(location);
			chkPrimary.Checked = location.Primary;
			chkBillToRemitTo.Checked = location.RemitToLocation;
			chkBillToRemitToMain.Checked = location.MainRemitToLocation;
			txtLocationCode.Text = location.LocationNumber;

			lstContacts.DataSource = location.Contacts.OrderBy(c => c.Name);
			lstContacts.DataBind();
		}

		private void LoadCustomerLocation(CustomerLocation location)
		{
			pnlPrimary.Visible = true;
			pnlBillToRemitTo.Visible = true;
			pnlLocationCode.Visible = true;
			litBillToRemitTo.Text = "Billing";
			litBillToRemitToMain.Text = "<abbr Title='In case of multiple billing addresses, this is main billing address.'>Main Billing</abbr>";

			hidLocationId.Value = location.Id.ToString();

			saiAddressInput.LoadAddress(location);
			chkPrimary.Checked = location.Primary;
			chkBillToRemitTo.Checked = location.BillToLocation;
			chkBillToRemitToMain.Checked = location.MainBillToLocation;
			txtLocationCode.Text = location.LocationNumber;

			lstContacts.DataSource = location.Contacts.OrderBy(c => c.Name);
			lstContacts.DataBind();
		}

		private void LoadShipmentLocation(ShipmentLocation location)
		{
			pnlDescription.Visible = true;
			pnlSpecialInsGeneralInfoDirection.Visible = true;
			pnlAppointmentDateTime.Visible = true;
			pnlMilesFromPreceedingStop.Visible = true;

			hidLocationId.Value = location.Id.ToString();

			saiAddressInput.LoadAddress(location);
			txtDescription.Text = location.Description;
			txtSpecialInstructions.Text = location.SpecialInstructions;
			txtDirection.Text = location.Direction;
			txtGeneralInfo.Text = location.GeneralInfo;
			txtMilesFromPreviousStop.Text = location.MilesFromPreceedingStop.ToString("f4");

			txtAppointmentDate.Text = location.AppointmentDateTime.IsSystemEarliestDate()
										? string.Empty
										: location.AppointmentDateTime.FormattedShortDate();

			fftTime.Text = location.AppointmentDateTime == DateUtility.SystemEarliestDateTime ? string.Empty : location.AppointmentDateTime.GetTime();

			lstContacts.DataSource = location.Contacts.OrderBy(c => c.Name);
			lstContacts.DataBind();
		}

		private void LoadLoadOrderLocation(LoadOrderLocation location)
		{
			pnlDescription.Visible = true;
			pnlSpecialInsGeneralInfoDirection.Visible = true;
			pnlAppointmentDateTime.Visible = true;
			pnlMilesFromPreceedingStop.Visible = true;

			hidLocationId.Value = location.Id.ToString();

			saiAddressInput.LoadAddress(location);
			txtDescription.Text = location.Description;
			txtSpecialInstructions.Text = location.SpecialInstructions;
			txtDirection.Text = location.Direction;
			txtGeneralInfo.Text = location.GeneralInfo;
			txtMilesFromPreviousStop.Text = location.MilesFromPreceedingStop.ToString("f4");

			txtAppointmentDate.Text = location.AppointmentDateTime.IsSystemEarliestDate()
										? string.Empty
										: location.AppointmentDateTime.FormattedShortDate();
			fftTime.Text = location.AppointmentDateTime == DateUtility.SystemEarliestDateTime ? string.Empty : location.AppointmentDateTime.GetTime();

			lstContacts.DataSource = location.Contacts.OrderBy(c => c.Name);
			lstContacts.DataBind();
		}

		private void LoadPendingVendorLocation(PendingVendorLocation location)
		{
			pnlPrimary.Visible = true;
			pnlBillToRemitTo.Visible = true;
			pnlLocationCode.Visible = true;
			litBillToRemitTo.Text = "Remit To";
			litBillToRemitToMain.Text = "<abbr Title='In case of multiple remit to addresses, this is main remit to address.'>Main Remit To</abbr>";

			hidLocationId.Value = location.Id.ToString();

			saiAddressInput.LoadAddress(location);
			chkPrimary.Checked = location.Primary;
			chkBillToRemitTo.Checked = location.RemitToLocation;
			chkBillToRemitToMain.Checked = location.MainRemitToLocation;
			txtLocationCode.Text = location.LocationNumber;

			lstContacts.DataSource = location.Contacts.OrderBy(c => c.Name);
			lstContacts.DataBind();
		}


		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected void OnDeleteAddressClicked(object sender, ImageClickEventArgs e)
		{
			if (DeleteLocation != null)
				DeleteLocation(this, new ViewEventArgs<int>(RetrieveLocatinItemIndex()));
		}



		protected void OnAddContactClicked(object sender, EventArgs e)
		{
			var vContacts = lstContacts.Items
				.Select(item => new
				{
					Id = item.FindControl("hidContactId").ToCustomHiddenField().Value.ToLong(),
					Comment = item.FindControl("txtComments").ToTextBox().Text,
					ContactTypeId = item.FindControl("ddlContactTypes").ToCachedObjectDropDownList().SelectedValue.ToLong(),
					Email = item.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
					Fax = item.FindControl("txtFax").ToTextBox().Text,
					Mobile = item.FindControl("txtMobile").ToTextBox().Text,
					Name = item.FindControl("txtName").ToTextBox().Text,
					Phone = item.FindControl("txtPhone").ToTextBox().Text,
					Primary = item.FindControl("chkPrimary").ToAltUniformCheckBox().Checked
				})
				.ToList();

			vContacts.Add(new
			{
				Id = default(long),
				Comment = string.Empty,
				ContactTypeId = default(long),
				Email = string.Empty,
				Fax = string.Empty,
				Mobile = string.Empty,
				Name = string.Empty,
				Phone = string.Empty,
				Primary = false
			});

			lstContacts.DataSource = vContacts;
			lstContacts.DataBind();
		}

		protected void OnContactDeleteClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;
			var rowItemIndex = button.FindControl("hidContactIndex").ToCustomHiddenField().Value.ToLong();

			var vContacts = lstContacts
				.Items
				.Select(item => new
					{
						Id = item.FindControl("hidContactId").ToCustomHiddenField().Value.ToLong(),
						Comment = item.FindControl("txtComments").ToTextBox().Text,
						ContactTypeId = item.FindControl("ddlContactTypes").ToCachedObjectDropDownList().SelectedValue.ToLong(),
						Email = item.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
						Fax = item.FindControl("txtFax").ToTextBox().Text,
						Mobile = item.FindControl("txtMobile").ToTextBox().Text,
						Name = item.FindControl("txtName").ToTextBox().Text,
						Phone = item.FindControl("txtPhone").ToTextBox().Text,
						Primary = item.FindControl("chkPrimary").ToAltUniformCheckBox().Checked,
						rowItemIndex = item.FindControl("hidContactIndex").ToCustomHiddenField().Value.ToLong()
					})
				.Where(c => c.rowItemIndex != rowItemIndex)
				.ToList();

			lstContacts.DataSource = vContacts;
			lstContacts.DataBind();
		}



		protected void OnDescriptionTextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtDescription.Text)) return;

			var key = txtDescription.Text.Substring(1).RetrieveSearchCodeFromAutoCompleteString().ToLong();
			var address = new AddressBook(key);

			if (!address.KeyLoaded) return;

			saiAddressInput.LoadAddress(address);

			txtDescription.Text = address.Description;
			txtGeneralInfo.Text = address.GeneralInfo;
			txtDirection.Text = address.Direction;
			lstContacts.DataSource = address.Contacts.OrderBy(c => c.Name);
			txtSpecialInstructions.Text = UseInstructions == UseOriginInstructions
											  ? address.OriginSpecialInstruction
											  : UseInstructions == UseDestinationInstructions
													? address.DestinationSpecialInstruction
													: string.Empty;
			lstContacts.DataBind();
		}


		
	}
}