﻿namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public enum CustomTextBoxType
	{
		// Please keep A - Z
		Date,
		FloatingPointNumbers,
		NegativeNumbers,
		NotSet,
		NumbersOnly,
		ReadOnly,
		Time,
	}
}