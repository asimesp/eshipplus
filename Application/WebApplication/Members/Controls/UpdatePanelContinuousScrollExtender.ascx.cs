﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
	public partial class UpdatePanelContinuousScrollExtender : MemberControlBaseWithControlStore
	{
		private const string ListViewBindFlag = "L";
		private const string RepeaterBingFlag = "R";
		private const string Data = "DataSource";

		public const int DisplayRecordCount = 50;

		public string UpdatePanelToExtendId
		{
			set { hidUpdatePanelToExtendControlId.Value = value; }
		}

		public string BindableControlId
		{
			set { hidBindableControlId.Value = value; }
		}

		public string ScrollableDivId
		{
			protected get { return hidScrollableDivId.Value; }
			set { hidScrollableDivId.Value = value; }
		}

		public ICollection DataSource
		{
			protected get
			{
                if (!ControlStore.ContainsKey(DataSourceKey)) ControlStore[DataSourceKey] = null;
                return ControlStore[DataSourceKey] as ICollection;
			}
            set { ControlStore[DataSourceKey] = value; }
		}

		public bool PreserveRecordSelection
		{
			get { return hidPreserveRecordSelectionId.Value.ToBoolean(); }
			set { hidPreserveRecordSelectionId.Value = value.GetString(); }
		}

		public string SelectionCheckBoxControlId
		{
			set { hidCheckBoxControlId.Value = value; }
		}

		public string SelectionUniqueRefHiddenFieldId
		{
			set { hidUniqueRecordRefControlId.Value = value; }
		}


		protected string UpdatePanelClientId
		{
			get
			{
				return Parent.FindControl(hidUpdatePanelToExtendControlId.Value).ClientID;		
			}
		}

		private int RecordsToLoad
		{
			get { return hidCurrentlyLoadedRecords.Value.ToInt(); }
			set { hidCurrentlyLoadedRecords.Value = value.GetString(); }
		}

	    private string DataSourceKey
	    {
            get { return string.Format("{0}{1}", Data, ClientID); }
	    }

		public void ResetLoadCount()
		{
			RecordsToLoad = DisplayRecordCount;

            //uncheck all checkboxes when resetting and preserving record selection
		    if (!PreserveRecordSelection) return;
            var control = Parent.FindControl(hidBindableControlId.Value);
            var flag = string.Empty;
		    var type = control.GetType();
            if (type == typeof(Repeater)) flag = RepeaterBingFlag;
            else if (type == typeof(ListView)) flag = ListViewBindFlag;

            switch (flag)
            {
                case RepeaterBingFlag:
                    foreach (var item in control.ToRepeater().Items.Cast<RepeaterItem>())
                        item.FindControl(hidCheckBoxControlId.Value).ToCheckBox().Checked = false;
                    break;
                case ListViewBindFlag:
                    foreach (var item in control.ToListView().Items)
                        item.FindControl(hidCheckBoxControlId.Value).ToCheckBox().Checked = false;
                    break;
            }
		}


		public new void DataBind()
		{
			var control = Parent.FindControl(hidBindableControlId.Value);
			var flag = string.Empty;
			Dictionary<string, bool> selectedRecords = null;
			

			if (PreserveRecordSelection)
			{
				if(string.IsNullOrEmpty(hidUniqueRecordRefControlId.Value))
					throw new Exception("Missing record unique reference hidden field control id");

				if(string.IsNullOrEmpty(hidCheckBoxControlId.Value))
					throw new Exception("Missing record selection checkbox control id");

				var type = control.GetType();
				if (type == typeof (Repeater)) flag = RepeaterBingFlag;
				else if (type == typeof (ListView)) flag = ListViewBindFlag;

				switch (flag)
				{
					case RepeaterBingFlag:
						selectedRecords = control.ToRepeater().Items
						                         .Cast<RepeaterItem>()
						                         .Select(i => new
							                         {
								                         Key = i.FindControl(hidUniqueRecordRefControlId.Value).ToHiddenField().Value,
								                         Selected = i.FindControl(hidCheckBoxControlId.Value).ToCheckBox().Checked
							                         })
						                         .Where(i => i.Selected)
						                         .ToDictionary(i => i.Key, i => i.Selected);
						break;
					case ListViewBindFlag:
						selectedRecords = control.ToListView().Items
						                         .Select(i => new
							                         {
								                         Key = i.FindControl(hidUniqueRecordRefControlId.Value).ToHiddenField().Value,
								                         Selected = i.FindControl(hidCheckBoxControlId.Value).ToCheckBox().Checked
							                         })
						                         .Where(i => i.Selected)
						                         .ToDictionary(i => i.Key, i => i.Selected);
						break;
				}
			}

			if (RecordsToLoad == 0) RecordsToLoad = DisplayRecordCount;
			control.SetPropertyValue("DataSource", DataSource.Cast<object>().Take(RecordsToLoad));
			control.DataBind();
			hidAllRecordsLoaded.Value = (DataSource.Cast<object>().Count() <= RecordsToLoad).GetString();

			base.DataBind();


			// restore preserve (if applicable)
			if (PreserveRecordSelection && selectedRecords!= null)
			{
				switch (flag)
				{
					case RepeaterBingFlag:
						foreach (var item in control.ToRepeater().Items.Cast<RepeaterItem>())
							item.FindControl(hidCheckBoxControlId.Value).ToCheckBox().Checked =
								selectedRecords.ContainsKey(item.FindControl(hidUniqueRecordRefControlId.Value).ToHiddenField().Value);
						break;
					case ListViewBindFlag:
						foreach (var item in control.ToListView().Items)
							item.FindControl(hidCheckBoxControlId.Value).ToCheckBox().Checked =
								selectedRecords.ContainsKey(item.FindControl(hidUniqueRecordRefControlId.Value).ToHiddenField().Value);
						break;
				}
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Params.Get("__EVENTARGUMENT") == ClientID && DataSource != null && DataSource.Cast<object>().Count() >= RecordsToLoad)
			{
				RecordsToLoad += DisplayRecordCount;
				DataBind();
			}
		}
	}
}