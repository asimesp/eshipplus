﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Controls
{
    /// <summary>
    /// Usage: implementing control must set GetProfileItems;  Return value of delegated method cannot be null!
    /// </summary>
    public partial class UserSearchProfileControl : MemberControlBase
    {
        public struct ProfileItems
        {
            public SearchField SortBy { get; set; }
            public bool SortAscending { get; set; }
            public List<ParameterColumn> Columns { get; set; }
        }

        public delegate ProfileItems GetProfileItemsDelegate();

        public SearchDefaultsItemType Type
        {
            protected get { return hidType.Value.ToInt().ToEnum<SearchDefaultsItemType>(); }
            set
            {
                hidType.Value = value.ToInt().GetString();
                LoadDropdownProfiles(value);
            }
        }
        
        public bool ShowAutoRefresh { set { ddlUpdateInterval.Visible = value; } }


        public event EventHandler<ViewEventArgs<int>> AutoRefreshChanged;
        public event EventHandler<ViewEventArgs<string>> ProcessingError;
        public event EventHandler<ViewEventArgs<SearchDefaultsItem>> SearchProfileSelected;


        public GetProfileItemsDelegate GetProfileItems;


        public SearchDefaultsItem RetrieveDefaultProfileForTypeSet(bool setToDefault = false)
        {
	        var @default = SearchDefaults == null ? null : SearchDefaults.Default(Type);
	        if (setToDefault) SetSelectedProfile(@default);
	        return @default;
        }

	    public void SetSelectedProfile(SearchDefaultsItem item = null)
        {
            var profileName = item == null ? string.Empty : item.Name;
            btnUpdateProfile.Visible = (profileName != string.Empty);
	        if (ddlSearchProfiles.ContainsValue(profileName)) ddlSearchProfiles.SelectedValue = profileName;
        }


        private void LoadDropdownProfiles(SearchDefaultsItemType value)
        {
            var profiles = SearchDefaults == null
                            ? new List<ViewListItem>()
                            : SearchDefaults.List(value).Select(i => new ViewListItem(i.Name, i.Name)).ToList();
            profiles.Insert(0, new ViewListItem(WebApplicationConstants.ChooseSearchProfile, string.Empty));
            ddlSearchProfiles.DataSource = profiles;
            ddlSearchProfiles.DataBind();
            ddlSearchProfiles.SelectedValue = string.Empty;
            btnUpdateProfile.Visible = false;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            var internals = new List<ViewListItem>
			                	{
			                		new ViewListItem("No Auto Refresh", default(int).GetString()),
			                		new ViewListItem("Auto Refresh: 30 sec.", "30000"),
			                		new ViewListItem("Auto Refresh: 60 sec.", "60000"),
			                		new ViewListItem("Auto Refresh: 3 mins.", WebApplicationConstants.DefaultRefreshInterval),
			                		new ViewListItem("Auto Refresh: 5 mins.", "300000"),
			                		new ViewListItem("Auto Refresh: 10 mins.", "1000000"),
			                	};
            ddlUpdateInterval.DataSource = internals;
            ddlUpdateInterval.DataBind();

            ddlUpdateInterval.SelectedValue = WebApplicationConstants.DefaultRefreshInterval;
        }

        protected void OnDeleteProfileClicked(object sender, EventArgs e)
        {
            try
            {
                var profiles = SearchDefaults ?? new SearchDefaults();
                var profile = profiles.Profiles.FirstOrDefault(p => p.Name == ddlSearchProfiles.SelectedValue && p.Type == Type);
                if (profile != null) profiles.Remove(profile);

                var searchProfileDefault = SearchDefaults == null
                                            ? new SearchProfileDefault { User = ActiveUser }
                                            : new SearchProfileDefault(ActiveUser);

                searchProfileDefault.Profiles = profiles.Profiles.ToXml();
                var ex = new SearchProfileDefaultHandler().SaveSearchProfileDefaults(searchProfileDefault);
                if (ex != null) ErrorLogger.LogError(ex, Context);
                else SearchDefaults = new SearchDefaults(searchProfileDefault);


                LoadDropdownProfiles(Type);

            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
            }

        }

        protected void OnSearchProfilesSelectedIndexChanged(object sender, EventArgs e)
        {
            btnUpdateProfile.Visible = (ddlSearchProfiles.SelectedValue != string.Empty);
            if (ddlSearchProfiles.SelectedValue == string.Empty)
                return;

            try
            {
                var profiles = SearchDefaults != null
                                ? SearchDefaults.List(hidType.Value.ToEnum<SearchDefaultsItemType>())
                                : new List<SearchDefaultsItem>();
                var profile = profiles.FirstOrDefault(p => p.Name == ddlSearchProfiles.SelectedValue);

                if (SearchProfileSelected != null)
                    SearchProfileSelected(this, new ViewEventArgs<SearchDefaultsItem>(profile));
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
            }
        }

        protected void OnUpdateIntervalSelectedIndexChanged(object sender, EventArgs e)
        {
            if (AutoRefreshChanged != null)
                AutoRefreshChanged(this, new ViewEventArgs<int>(ddlUpdateInterval.SelectedValue.ToInt()));
        }

        protected void OnSaveProfileClicked(object sender, EventArgs e)
        {
            SaveProfile(txtSearchProfileName.Text);
        }

        protected void OnUpdateProfileClicked(object sender, EventArgs e)
        {
            SaveProfile(ddlSearchProfiles.SelectedValue);
        }

        private void SaveProfile(string profileName)
        {
            if (string.IsNullOrEmpty(profileName))
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>("Search Profile name cannot be empty!"));
                return;
            }

            var pi = GetProfileItems();
            var item = new SearchDefaultsItem { Columns = pi.Columns, SortAscending = pi.SortAscending, SortBy = pi.SortBy };
            try
            {
                item.Name = profileName;
                item.Type = Type;
                var profiles = SearchDefaults ?? new SearchDefaults();
                profiles.AddOrUpdate(item);

                var searchProfileDefault = SearchDefaults == null
                                               ? new SearchProfileDefault { User = ActiveUser, TenantId = ActiveUser.TenantId }
                                               : new SearchProfileDefault(ActiveUser);

                searchProfileDefault.Profiles = profiles.Profiles.ToXml();
                var ex = new SearchProfileDefaultHandler().SaveSearchProfileDefaults(searchProfileDefault);
                if (ex != null) ErrorLogger.LogError(ex, Context);
                else SearchDefaults = new SearchDefaults(searchProfileDefault);

                LoadDropdownProfiles(Type);

                if (ddlSearchProfiles.ContainsValue(item.Name)) ddlSearchProfiles.SelectedValue = item.Name;
                btnUpdateProfile.Visible = (ddlSearchProfiles.SelectedValue != string.Empty);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
            }
        }
    }
}