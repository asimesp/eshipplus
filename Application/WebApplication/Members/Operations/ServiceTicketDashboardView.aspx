﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ServiceTicketDashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.ServiceTicketDashboardView"
    EnableEventValidation="false" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                        Service Ticket Dashboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />

        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="ServiceTickets" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" ShowAutoRefresh="True" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort By:</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstServiceTicketDetails" UpdatePanelToExtendId="upDataUpdate" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheServiceTicketDashboardTable" TableId="serviceTicketDashboardTable" HeaderZIndex="2" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <div class="rowgroup">
                    <table id="serviceTicketDashboardTable" class="line2 pl2">
                        <tr>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortTicketNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							        Ticket #
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortDateCreated" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							        Date Created
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortTicketDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							        Ticket Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 28%;">
                                Customer&nbsp;
                                <asp:LinkButton runat="server" ID="lbtnSortCustomerNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							          Number
                                </asp:LinkButton>\
                                <asp:LinkButton runat="server" ID="lbtnSortCustomerName" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							          Name
                                </asp:LinkButton>
                            </th>
                            <th style="width: 20%;">
                                <asp:LinkButton runat="server" ID="lbtnSortUsername" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							        User
                                </asp:LinkButton>
                            </th>
                            <th style="width: 15%;">
                                <asp:LinkButton runat="server" ID="lbtnSortStatusText" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							        Status
                                </asp:LinkButton>
                            </th>
                            <th style="width: 7%;" class="text-center">Action</th>
                        </tr>
                        <asp:ListView runat="server" ID="lstServiceTicketDetails" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnServiceTicketDetailsItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <eShip:ServiceTicketDashboardDetailControl runat="server" ID="serviceTicketDetailControl" ItemIndex='<%# Container.DataItemIndex %>' />
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortTicketNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortDateCreated" />
                <asp:PostBackTrigger ControlID="lbtnSortTicketDate" />
                <asp:PostBackTrigger ControlID="lbtnSortCustomerNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortCustomerName" />
                <asp:PostBackTrigger ControlID="lbtnSortUsername" />
                <asp:PostBackTrigger ControlID="lbtnSortStatusText" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
</asp:Content>
