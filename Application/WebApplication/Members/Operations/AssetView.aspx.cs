﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class AssetView : MemberPageBase, IAssetView
    {
        private const string ExportFlag = "E";

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public override ViewCode PageCode { get { return ViewCode.Asset; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public static string PageAddress { get { return "~/Members/Operations/AssetView.aspx"; } }

        public Dictionary<int, string> SearchAssetTypes
        {
            set
            {
                var assetTypes = value.ToList();
                assetTypes.Insert(0, new KeyValuePair<int, string>(-1, WebApplicationConstants.NotApplicable));

                ddlAssetType.DataSource = value ?? new Dictionary<int, string>();
                ddlAssetType.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Asset>> Save;
        public event EventHandler<ViewEventArgs<Asset>> Delete;
        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<Asset>> Lock;
        public event EventHandler<ViewEventArgs<Asset>> UnLock;
        public event EventHandler<ViewEventArgs<List<Asset>>> BatchImport;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<Asset> assets)
        {
            if (assets.Count == 0)
            {
                DisplayMessages(new[] { ValidationMessage.Information("No assets found") });
            }

            litRecordCount.Text = assets.BuildRecordCount();
            lstAssets.DataSource = assets;
            lstAssets.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var lines = assets
                    .Select(a => new[]
					             	{
					             		a.AssetType.ToInt().GetString(), a.Active.GetString(), a.Description, a.AssetNumber,
					             		a.DateCreated.FormattedShortDate()
					             	}.TabJoin())
                    .ToList();
                lines.Insert(0, new[] { "Asset Type", "Active", "Description", "Asset Number", "Date Created" }.TabJoin());

                hidFlag.Value = string.Empty;

                Response.Export(lines.ToArray().NewLineJoin(), "Assets.txt");
            }
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() || messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearch(List<ParameterColumn> columns)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(columns));
        }


        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Asset>(new Asset(hidEditAssetId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtAssetNumber.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtDateCreated.Text = string.Empty;
            chkActive.Checked = false;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new AssetHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.AssetsImportTemplate });

            if (Loading != null)
                Loading(this, new EventArgs());

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : OperationsSearchFields.DefaultAssets.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();

            hidEditAssetId.Value = string.Empty;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidAssetId").ToCustomHiddenField();

            var asset = new Asset(hidden.Value.ToLong(), false);

            hidEditAssetId.Value = asset.Id.ToString();
            txtAssetNumber.Text = asset.AssetNumber;
            ddlAssetType.SelectedValue = asset.AssetType.ToInt().ToString();
            txtDescription.Text = asset.Description;
            txtDateCreated.Text = asset.DateCreated.FormattedShortDate();
            chkActive.Checked = asset.Active;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = true;

            if (Lock != null)
                Lock(this, new ViewEventArgs<Asset>(asset));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var assetId = hidEditAssetId.Value.ToLong();

            var asset = new Asset(assetId, assetId != default(long))
            {
                AssetNumber = txtAssetNumber.Text,
                AssetType = ddlAssetType.SelectedValue.ToEnum<AssetType>(),
                Active = chkActive.Checked,
                Description = txtDescription.Text,
            };

            if (asset.IsNew)
            {
                asset.TenantId = ActiveUser.TenantId;
                asset.DateCreated = DateTime.Now;
            }

            if (Save != null)
                Save(this, new ViewEventArgs<Asset>(asset));

            DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidAssetId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<Asset>(new Asset(hidden.Value.ToLong(), true)));

            DoSearch(GetCurrentRunParameters(false));
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "ASSET IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearch(GetCurrentRunParameters(false));
        }


        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 3;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<AssetType>(0));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var assets = lines
                .Select(s => new Asset
                {
                    AssetType = s[0].ToEnum<AssetType>(),
                    Active = s[1].ToBoolean(),
                    Description = s[2],
                    AssetNumber = string.Empty,
                    TenantId = ActiveUser.TenantId,
                    DateCreated = DateTime.Now
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<Asset>>(assets));
            fileUploader.Visible = false;
            DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.Assets.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.Assets);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
    }
}