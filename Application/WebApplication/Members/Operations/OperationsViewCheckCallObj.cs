﻿using System;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
	[Serializable]
	[Entity("OperationsViewCheckCallObj", ReadOnly = false, Source = EntitySource.TableView)]
	public class OperationsViewCheckCallObj : CheckCall
	{
		public new long Id
		{
			get { return base.Id; }
			set { base.Id = value; }
		}

		public string Username { get; set; }

		public OperationsViewCheckCallObj(CheckCall checkCall)
		{
			Copy(checkCall);
			Id = checkCall.Id; // must set ID here as new Id defined above hides Id in base class and is therefore not copied!
			Username = checkCall.User == null ? string.Empty : checkCall.User.Username;
		}

		public OperationsViewCheckCallObj()
		{
			
		}
	}
}