﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="EstimateRateView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.EstimateRateView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowMore="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Estimate Rate
                
            </h3>

            <div class="right ii">
                <span id="importantInformation">Important Information</span>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                if (jsHelper.IsTrue('<%= !IsPostBack %>') && jsHelper.IsTrue('<%= ActiveUser.AlwaysShowRatingNotice %>')) {
                    $("div[id$='pnlRateNotice']").show("slow"); //name of panel inside rate notice control
                    $("div[id$='pnlDimScreenJS']").show();
                } else {
                    $("div[id$='pnlRateNotice']").hide();
                    $("div[id$='pnlDimScreenJS']").hide();
                }

                $(jsHelper.AddHashTag('importantInformation')).click(function (e) {
                    e.preventDefault();
                    $("div[id$='pnlRateNotice']").show("slow");
                    $("div[id$='pnlDimScreenJS']").show();
                });
            });
        </script>

        <div class="imp_note">
            *** highlighted fields are minimum required for a rate. <span class="red">*</span> = required all modes, <span class="red">+</span> = required LTL & package,
            <span class="red">++</span> = required LTL
        </div>

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:RateNoticeControl runat="server" ID="rateNotice" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            OnlyActiveResults="True" Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:PostalCodeFinderControl runat="server" ID="postalCodeFinder" Visible="False" EnableMultiSelection="False"
            OnItemSelected="OnPostalCodeFinderItemSelected" OnSelectionCancel="OnPostalCodeFinderItemCancelled" />
        <eShip:RateSelectionDisplayControl ID="rateSelectionDisplay" runat="server" Visible="False"
            DisableRateSelection="True" EnableEnterMissingInformationButton="True" EnableRateDownload="True"
            OnRateSelectionCancelled="OnRateSelectionDisplaySelectionCancelled" OnRatesLoaded="OnRateSelectionDisplayRatesLoaded"
            OnDownloadRates="OnRateSelectionDisplayDownloadRates" />

        <asp:Panel runat="server" DefaultButton="btnEstimateRate">
            <div class="rowgroup">
                <div class="col_1_3">
                    <h5 class="mb5">General Information</h5>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Customer Account</label>
                            <eShip:CustomTextBox ID="txtCustomerNumber" runat="server" AutoPostBack="True" OnTextChanged="OnCustomerNumberEntered" CssClass="w80 requiredInput" />
                            <asp:ImageButton ID="ibtnFindCustomer" runat="server" CausesValidation="False" ImageUrl="~/images/icons2/search_dark.png" OnClick="OnCustomerSearchClicked" />
                            <eShip:CustomTextBox ID="txtCustomerName" runat="server" CssClass="w210 disabled" ReadOnly="True" />
                            <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">Shipment Date</label>
                            <asp:DropDownList runat="server" ID="ddlShipmentDate" DataValueField="Value" DataTextField="Text" />
                        </div>
                        <div class="fieldgroup_s">
                            <asp:CheckBox runat="server" ID="chkHazardousMaterials" CssClass="jQueryUniform" />
                            <label>Hazardous Materials</label>
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="pnlDeclineInsurance" Visible="false">
                        <div class="row pt10">
                            <div class="fieldgroup">
                                <asp:CheckBox runat="server" ID="chkDeclineInsurance" CssClass="jQueryUniform" />
                                <label>Decline Insurance</label>
                            </div>
                        </div>
                    </asp:Panel>

                    <h5 class="mb5">Origin</h5>
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">Postal Code</label>
                            <eShip:CustomTextBox ID="txtOriginPostalCode" runat="server" MaxLength="10" CssClass="w100 requiredInput" CausesValidation="False" />
                            <asp:ImageButton runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnOriginPostalCodeSearchClicked" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Country</label>
                            <eShip:CachedObjectDropDownList Type="Countries" ID="ddlOriginCountry" runat="server" CssClass="w180 requiredInput"  
                                EnableChooseOne="True" DefaultValue="0"  />
                        </div>
                    </div>
                    <h5 class="mb5">Destination</h5>
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">Postal Code</label>
                            <eShip:CustomTextBox ID="txtDestinationPostalCode" runat="server" MaxLength="10" CssClass="w100 requiredInput" CausesValidation="False" />
                            <asp:ImageButton runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnDestinationPostalCodeSearchClicked" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Country</label>
                            <eShip:CachedObjectDropDownList Type="Countries" ID="ddlDestinationCountry" runat="server" CssClass="w180 requiredInput"  
                                EnableChooseOne="True"  DefaultValue="0" />
                        </div>
                    </div>
                </div>
                <div class="col_2_3">
                    <h5 class="mb10">Services</h5>
                    <div class="row">
                        <ul class="twocol_list sm_chk">
                            <asp:Repeater runat="server" ID="rptServices">
                                <ItemTemplate>
                                    <li class="top">
                                        <asp:CheckBox runat="server" ID="chkSelected" CssClass="jQueryUniform" Checked='<%# Eval("Selected") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidServiceId" Value='<%# Eval("Value") %>' />
                                        <label class="fs75em lhInherit">
                                            <asp:Literal ID="litServiceName" runat="server" Text='<%# Eval("Text") %>' />
                                        </label>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
            <hr />
            <eShip:LibraryItemFinderControl runat="server" ID="libraryItemFinder" Visible="False" EnableMultiSelection="True"
                OnMultiItemSelected="OnLibraryItemFinderMultiItemSelected" OnSelectionCancel="OnLibraryItemFinderItemCancelled"
                OpenForEditEnabled="false" />
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <h5>Items</h5>
                    <div class="row mb10">
                        <div class="col_1_3 fs95em">
                            <div class="fieldgroup mr5">
                                <asp:LinkButton runat="server" ID="lnkAddItem" Text="Add Item" OnClick="OnAddItemClicked" CausesValidation="false" CssClass="add" OnClientClick="ShowProgressNotice(true);" />
                            </div>
                            <div class="fieldgroup mr5">
                                <asp:LinkButton runat="server" ID="lnkAddItemFromLibrary" Text="Add Item From Library" OnClick="OnAddLibraryItemClicked" CausesValidation="false" CssClass="add" />
                            </div>
                        </div>
                        <asp:Panel CssClass="col_1_3" runat="server" ID="pnlBypassLinearView" Visible="False">
                            <eShip:AltUniformCheckBox runat="server" ID="chkBypassLinearFootRule" />
                            <label>Bypass Linear Foot Rule</label>
                        </asp:Panel>
                        <div class="col_1_3  <%= ActiveUser.TenantEmployee ? string.Empty : "no-right-border" %>">
                            <script type="text/javascript">
                                $(window).load(function () {

                                    // reload cookie selection
                                    var pref = jsHelper.RetrieveCookie(jsHelper.RateAndScheduleInputPref);
                                    if (!jsHelper.IsNullOrEmpty(pref)) {
                                        if (pref == '<%= Units.UnitType.Metric.ToInt().ToString() %>') {
                                            jsHelper.UnCheckBox($(jsHelper.AddHashTag('<%= chkEnglish.ClientID %>')).attr('id'));
                                            jsHelper.CheckBox($(jsHelper.AddHashTag('<%= chkMetric.ClientID %>')).attr('id'));
                                        }
                                        if (pref == '<%= Units.UnitType.English.ToInt().ToString() %>') {
                                            jsHelper.UnCheckBox($(jsHelper.AddHashTag('<%= chkMetric.ClientID %>')).attr('id'));
                                            jsHelper.CheckBox($(jsHelper.AddHashTag('<%= chkEnglish.ClientID %>')).attr('id'));
                                        }
                                    }
                                    ProcessUnitsValueChanged(GetCurrentlySelectedUnits());
                                    ReloadCalculatedDensity();

                                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                        var pref = jsHelper.RetrieveCookie(jsHelper.RateAndScheduleInputPref);
                                        if (!jsHelper.IsNullOrEmpty(pref)) {
                                            if (pref == '<%= Units.UnitType.Metric.ToInt().ToString() %>') {
                                                jsHelper.UnCheckBox($(jsHelper.AddHashTag('<%= chkEnglish.ClientID %>')).attr('id'));
                                                jsHelper.CheckBox($(jsHelper.AddHashTag('<%= chkMetric.ClientID %>')).attr('id'));
                                            }
                                            if (pref == '<%= Units.UnitType.English.ToInt().ToString() %>') {
                                                jsHelper.UnCheckBox($(jsHelper.AddHashTag('<%= chkMetric.ClientID %>')).attr('id'));
                                                jsHelper.CheckBox($(jsHelper.AddHashTag('<%= chkEnglish.ClientID %>')).attr('id'));
                                            }
                                        }
                                        ProcessUnitsValueChanged(GetCurrentlySelectedUnits());
                                        ReloadCalculatedDensity();
                                    });

                                });


                                $(function () {
                                    // enforce mutual exclusion on checkboxes in ulVendorRatingImportOptions
                                    $("input[id='<%= chkMetric.ClientID %>'], input[id='<%= chkEnglish.ClientID %>']").click(function () {
                                        if (jsHelper.IsChecked($(this).attr('id'))) {
                                            $("input[id='<%= chkMetric.ClientID %>'], input[id='<%= chkEnglish.ClientID %>']").not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                                jsHelper.UnCheckBox($(this).attr('id'));
                                                SetAltUniformCheckBoxClickedStatus(this);
                                            });
                                            ReloadCalculatedDensity();
                                            SetAltUniformCheckBoxClickedStatus(this);
                                            ProcessUnitsValueChanged(GetCurrentlySelectedUnits());
                                        }
                                    });

                                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                        $("input[id='<%= chkMetric.ClientID %>'], input[id='<%= chkEnglish.ClientID %>']").click(function () {
                                            if (jsHelper.IsChecked($(this).attr('id'))) {
                                                $("input[id='<%= chkMetric.ClientID %>'], input[id='<%= chkEnglish.ClientID %>']").not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                                    jsHelper.UnCheckBox($(this).attr('id'));
                                                    SetAltUniformCheckBoxClickedStatus(this);
                                                });
                                                ReloadCalculatedDensity();
                                                SetAltUniformCheckBoxClickedStatus(this);
                                                ProcessUnitsValueChanged(GetCurrentlySelectedUnits());
                                            }
                                        });
                                    });
                                });

                                function ProcessUnitsValueChanged(units) {
                                    var ids = new ControlIds();
                                    ids.DensityWeight = "spWeight";
                                    ids.DensityLength = "spLength";
                                    ids.DensityWidth = "spWidth";
                                    ids.DensityHeight = "spHeight";
                                    ids.DensityCalc = "spDensity";
                                    SetUnitsLabel(units, ids);
                                    jsHelper.SetCookie(jsHelper.RateAndScheduleInputPref, units, jsHelper.CookieDaysToLive);
                                }

                                function ReloadCalculatedDensity() {
                                    $(jsHelper.AddHashTag('estimateRateItemsTable [id$="txtDensity"]')).each(function () {
                                        UpdateDensity(
                                            $(this).parent().parent().find('[id$="txtWeight"]').attr('id'),
                                            $(this).parent().parent().find('[id$="txtLength"]').attr('id'),
                                            $(this).parent().parent().find('[id$="txtWidth"]').attr('id'),
                                            $(this).parent().parent().find('[id$="txtHeight"]').attr('id'),
                                            $(this).parent().parent().find('[id$="txtDensity"]').attr('id'),
                                            jsHelper.EmptyString,
                                            $(this).parent().parent().find('[id$="txtQuantity"]').attr('id')
                                        );
                                    });
                                }

                                function UpdateDensity(weight, length, width, height, densityCalc, hidDensityCalc, densityQty) {

                                    var ids = new ControlIds();
                                    ids.DensityWeight = weight;
                                    ids.DensityLength = length;
                                    ids.DensityWidth = width;
                                    ids.DensityHeight = height;
                                    ids.DensityCalc = densityCalc;
                                    ids.HidDensityCalc = hidDensityCalc;
                                    ids.DensityQty = densityQty;

                                    CalculateDensity(GetCurrentlySelectedUnits(), ids);
                                }

                                function GetCurrentlySelectedUnits() {
                                    return jsHelper.IsChecked($(jsHelper.AddHashTag('<%= chkMetric.ClientID %>')).attr('id'))
                                        ? '<%= Units.UnitType.Metric.ToInt().ToString() %>'
                                        : '<%= Units.UnitType.English.ToInt().ToString() %>';
                                }
                            </script>
                            <div class="fieldgroup">
                                <label class="upper">Input Units:</label>
                            </div>

                            <div class="fieldgroup">
                                <eShip:AltUniformCheckBox runat="server" ID="chkMetric" />
                                <label>Metric</label>
                            </div>
                            <div class="fieldgroup">
                                <eShip:AltUniformCheckBox runat="server" ID="chkEnglish" Checked="True" />
                                <label>English</label>
                            </div>
                        </div>
                    </div>
                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheEstimateRateItemsTable" TableId="estimateRateItemsTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="True" />
                    <table class="stripe" id="estimateRateItemsTable">
                        <tr>
                            <th style="width: 9%;">Total Weight <span id="spWeight">(lb)</span> <span class="red">*</span>
                            </th>
                            <th style="width: 9%;">Length <span id="spLength">(in)</span> <span class="red">+</span>
                            </th>
                            <th style="width: 9%;">Width <span id="spWidth">(in)</span> <span class="red">+</span>
                            </th>
                            <th style="width: 9%;">Height <span id="spHeight">(in)</span> <span class="red">+</span>
                            </th>
                            <th style="width: 8%;">Density <span id="spDensity">lb/ft<sup>3</sup></span>
                            </th>
                            <th style="width: 5%;">Stackable
                            </th>
                            <th style="width: 7%;">
                                <abbr title="Package Quantity">Pkg. Qty.</abbr>
                                <span class="red">*</span>
                            </th>
                            <th style="width: 14%;">Package Type
                        <span class="red">*</span>
                            </th>
                            <th style="width: 8%;">Freight Class
                        <span class="red">++</span>
                            </th>
                            <th style="width: 6%;">
                                <abbr title="Said to Contain or Number of Pieces">STC</abbr>
                            </th>
                            <th style="width: 10%;">
                                <abbr title="SEE IMPORTANT INFORMATION">Value ($)</abbr>
                            </th>
                            <th style="width: 5%;">Action
                            </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstItems" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnItemsItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <eShip:CustomHiddenField runat="server" ID="hidItemIndex" Value='<%# Container.DataItemIndex %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidComment" Value='<%# Eval("Comment") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidHtsCode" Value='<%# Eval("HTSCode") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidNmfcCode" Value='<%# Eval("NMFCCode") %>' />
                                        <eShip:CustomTextBox runat="server" ID="txtWeight" Text='<%# Eval("Weight") %>' CssClass="w80 requiredInput" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtLength" Text='<%# Eval("Length") %>' CssClass="w70" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtWidth" Text='<%# Eval("Width") %>' CssClass="w70" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtHeight" Text='<%# Eval("Height") %>' CssClass="w70" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomHiddenField runat="server" ID="hidDensityValue" Value='<%# Eval("Density") %>' />
                                        <eShip:CustomTextBox runat="server" ID="txtDensity" Text='<%# Eval("Density") %>' CssClass="w55 disabled" ReadOnly="True" />
                                    </td>
                                    <td class="text-center">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkStackable" Checked='<%# Eval("IsStackable") %>' />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtQuantity" Text='<%# Eval("Quantity") %>' CssClass="w60" Type="NumbersOnly" />
                                    </td>
                                    <td>
                                        <eShip:CachedObjectDropDownList Type="PackageType" ID="ddlPackageType" runat="server" CssClass="w100"  
                                            EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("PackageTypeId") %>'  />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlFreightClass" runat="server" CssClass="w60" DataTextField="Text" DataValueField="Value" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtPieceCount" Text='<%# Eval("PieceCount") %>' CssClass="w60" Type="NumbersOnly" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtValue" Text='<%# Eval("Value") %>' CssClass="w80" Type="FloatingPointNumbers" />
                                    </td>
                                    <td class="text-center">
                                        <asp:ImageButton ID="ibtnItemDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClick="OnDeleteItemClicked" CausesValidation="false"
                                            OnClientClick="ShowProgressNotice(true);" />
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="20" class="pt0">
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label>Description:</label>
                                                <eShip:CustomTextBox runat="server" ID="txtDescription" CssClass="w360" Text='<%# Eval("Description") %>' MaxLength="200" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="lnkAddItemFromLibrary" />
                </Triggers>
            </asp:UpdatePanel>
            <hr class="dark mb10" />
            <div class="rowgroup">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:Button ID="btnEstimateRate" Text="Get Estimate Rates" runat="server" CausesValidation="False"
                            OnClick="OnGetEstimateRateClicked" OnClientClick="ShowProcessingDivBlock();" />
                        <asp:Button ID="btnGotoRateAndSchedule" Text="Continue to Rate and Schedule" runat="server"
                            OnClientClick="ShowProcessingDivBlock();" CausesValidation="False"
                            OnClick="OnGotoRateAndScheduleClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>

    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackground" runat="server" Style="display: none;" />
    <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
    <eShip:CustomHiddenField runat="server" ID="hidCustomerOutstandingBalance" />
    <eShip:CustomHiddenField runat="server" ID="hidPostalCodeSet" />
    <eShip:CustomHiddenField runat="server" ID="hidShoppedRateId" />
</asp:Content>
