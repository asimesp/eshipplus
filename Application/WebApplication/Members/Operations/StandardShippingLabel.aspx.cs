﻿using System;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
	public partial class StandardShippingLabel : MemberPageBase
	{
		public override ViewCode PageCode
		{
			get { return ViewCode.DocumentProcessorPage; }
		}

		public static string PageAddress
		{
			get { return "~/Members/Operations/StandardShippingLabel.aspx"; }
		}

		public override string SetPageIconImage { set {  } }

	    public static string GenerateStandardShippingLabelPdfLink(long shipmentId)
	    {
	        return string.Format("{0}?{1}={2}&{3}={4}",
	            PageAddress, WebApplicationConstants.ShipmentAuditId,
	            shipmentId.ToString().UrlTextEncrypt(), WebApplicationConstants.DownloadPdf, true.ToString().UrlTextEncrypt());
	    }

        protected void Page_Load(object sender, EventArgs e)
		{
			var shipmentId = !string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.ShipmentAuditId])
								  ? Request.QueryString[WebApplicationConstants.ShipmentAuditId].UrlTextDecrypt().ToLong()
								  : Session[WebApplicationConstants.ShippingLabelShipmentId].ToLong();
            

            var shipment = new Shipment(shipmentId, false);
			if (!shipment.KeyLoaded) return;

			var originContact = shipment.Origin.Contacts.FirstOrDefault(c => c.Primary);
			var destinationContact = shipment.Destination.Contacts.FirstOrDefault(c => c.Primary);
			var primaryVendor = shipment.Vendors.First(c => c.Primary);

			var standardLabel = string.Format(
                @"<table class='shippingLabel' cellspacing='0' style='background-color:white;>
					<tr valign='top'>
						<td>From:<br />
							{0}<br />
							{1}{2}<br />
							{3}<br />
							{4}<br />
							{5}<br />
						</td>
						<td>To:<br />
							{6}<br />
							{7}{8}<br />
							{9}<br />
							{10}<br />
							{11}<br />
						</td>
					</tr>
					<tr>
						<td colspan='2'>
							Carrier: {12}<br />
							BOL #: {13}
						</td>
					</tr>
					<tr>
						<td colspan='2'>
							Product Description:<br />
							{14}
						</td>
					</tr>
					<tr>
						<td colspan='2'>Internal Use Only:
							<br />
							<br />
						</td>
					</tr>
				</table>",
				shipment.Origin.Description,
				shipment.Origin.Street1,
				shipment.Origin.Street2,
				string.Format("{0}, {1} {2} {3}", shipment.Origin.City, shipment.Origin.State, shipment.Origin.PostalCode,
				              shipment.Origin.Country.Name),
				originContact != null ? originContact.Phone : string.Empty,
				originContact != null ? originContact.Name : string.Empty,
				shipment.Destination.Description,
				shipment.Destination.Street1,
				shipment.Destination.Street2,
				string.Format("{0}, {1} {2} {3}", shipment.Destination.City, shipment.Destination.State,
				              shipment.Destination.PostalCode, shipment.Destination.Country.Name),
				destinationContact != null ? destinationContact.Phone : string.Empty,
				destinationContact != null ? destinationContact.Name : string.Empty,
				primaryVendor != null ? primaryVendor.Vendor.Name : string.Empty,
				string.Format("{0}{1}", shipment.Prefix != null && !shipment.HidePrefix ? shipment.Prefix.Code : string.Empty,
				              shipment.ShipmentNumber),
				shipment.Items.Aggregate(string.Empty,
				                         (current, item) =>
				                         current +
				                         string.Format("{0} ({1}): {2}<br/>", item.PackageType.TypeName, item.Quantity,
				                                       item.Description)));

			lblStandardShippingLabel.Text = standardLabel;


		    if (!String.IsNullOrWhiteSpace(Request.QueryString[WebApplicationConstants.DownloadPdf]))
		    {
		        ProcessStandardShippingLabelPdfView(shipment);

		    }

        }

	    private void ProcessStandardShippingLabelPdfView(Shipment shipment)
	    {
	        const string cssPath = WebApplicationConstants.Member2CssUrl;
	        var absolute = cssPath.Replace("../..", WebApplicationSettings.SiteRoot);
	        var writer = new StringWriter();
	        Server.Execute(
	            string.Format("{0}?{1}={2}", PageAddress, WebApplicationConstants.ShipmentAuditId,
	                shipment.Id.ToString().UrlTextEncrypt()), writer);
	        var htmlData = writer.GetStringBuilder().ToString().Replace(cssPath, absolute);
            
	        var title = string.Format("eShipPlus_TMS_StandardShippingLabel_{0}", shipment.ShipmentNumber);
            Response.Export(htmlData.ToPdf(title), $"{title}.pdf");
	    }

    }
}