﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="RateAndScheduleConfirmationView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.RateAndScheduleConfirmationView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowMore="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" 
                AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Rate and Schedule Confirmation</h3>
        </div>

        <eShip:DocumentDisplayControl runat="server" ID="documentDisplay" Visible="false" OnClose="OnDocumentDisplayClosed" />
        <eShip:ShippingLabelGeneratorControl runat="server" ID="shippingLabelGenerator" Visible="false"
            OnClose="OnShippingLabelGeneratorCloseClicked" />

        <table class="line2 pl2">
            <asp:Panel runat="server" ID="pnlNullTransfer" Visible="False">
                <tr class="header">
                    <td>
                        <h5 class="red">TRANSFER ERROR!</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="pb40 mt20 fs120em">
                            You are getting this error because you have accessed this page directly from your
						web browser or you attempted to refresh the page after a prior process was complete.
                        </p>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlOfferedLoad" Visible="False">
                <tr class="header">
                    <td>
                        <h5 class="green">SHIPMENT LOAD REQUEST</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="pb40 mt20 fs120em">
                            Your shipment load request has been successfully submitted. Your shipment load
						    request reference number is "<asp:Literal runat="server" ID="litOfferedLoadNumber"
                                Text="00000" />". Our operations specialist will contact you shortly regarding
						    your submission.
						    <br />
                            <br />
                            Thank you.
						    <br />
                            <br />
                            <asp:LinkButton runat="server" ID="lnkOfferedLoadNumber" Text="Go To Dashboard" CssClass="blue"
                                CausesValidation="False" OnClick="OnOfferedLoadNumberClicked" />
                        </p>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlQuotedLoadSubmission" Visible="False">
                <tr class="header">
                    <td>
                        <h5 class="green">SHIPMENT QUOTE</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="pb40 mt20 fs120em">
                            You have successfully accepted the shipment quote. Your shipment quote reference
						    number is "<asp:Literal runat="server" ID="litQuoteLoadNumber" Text="00000" />". Our
						    operations specialist will contact you shortly regarding your submission.
						    <br />
                            <br />
                            Thank you.
						    <br />
                            <br />
                            <asp:LinkButton runat="server" ID="lnkViewQuotedLoad" Text="View Quote" CausesValidation="False" CssClass="blue mr10"
                                OnClick="OnViewQuoteClicked" />
                            <asp:LinkButton runat="server" ID="lnkQuotedLoadNumber" Text="Go To Dashboard" CausesValidation="False" CssClass="blue"
                                OnClick="OnQuotedLoadNumberClicked" />
                        </p>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlShipmentSubmission" Visible="False">
                <tr class="header">
                    <td>
                        <h5 class="green">SHIPMENT</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                         <p class="pb40 mt20 fs120em">
                            Your shipment has been successfully submitted. Your shipment number is "<asp:Literal
                                runat="server" ID="litShipmentNumber" Text="00000" />" . Our operations specialist
						    may need to contact you regarding your submission.
						    <br />
                            <br />
                            Thank you.
						    <br />
                            <br />
                            <asp:HyperLink runat="server" Target="_blank" Text="View Bill of Lading" ID="hypViewBol" NavigateUrl="~/Members/DocumentViewer.aspx" CssClass="blue mr10" />
						    <asp:LinkButton runat="server" ID="lnkShipmentNumber" Text="Go To Dashboard" CausesValidation="False" CssClass="blue mr10"
                                OnClick="OnShipmentNumberClicked" />
						    <asp:LinkButton runat="server" ID="lnkShippingLabels" Text="View Shipping Labels" CssClass="blue mr10"
                                CausesValidation="False" OnClick="OnShippingLabelsClicked" />
                        </p>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSystemError" Visible="False">
                <tr class="header">
                    <td>
                        <h5 class="red">SYSTEM ERROR!</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                       <p class="pb40 mt20 fs120em">
                            An error occurred while processing your request. Our IT personnel have been notified
						    and will resolve the issue. Please confirm that your prior request was received
						    by contacting our logistics specialists.
						    <br />
                                <br />
                                We apologize for any inconvenience.
						    <br />
                            <br />
                            Thank you.
                        </p>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlProcessError" Visible="False">
                <tr class="header">
                    <td>
                        <h5 class="red">PROCESS ERROR!</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="pb40 mt20 fs120em">
                            The following error(s) occurred processing your request:
						    <asp:Literal runat="server" ID="litProcessError" />
                            <br />
                            <br />
                            Please contact our operations specialist for further assistance. We apologize for
						    any inconvenience.
						    <br />
                            <br />
                            Thank you.
                        </p>
                    </td>
                </tr>
            </asp:Panel>
        </table>

        <eShip:CustomHiddenField runat="server" ID="hidTransferId" />
    </div>
</asp:Content>
