﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class EstimateRateView : MemberPageBase, IRateAndScheduleView
    {
        private const string Origin = "O";
        private const string Destination = "D";

        public static string PageAddress
        {
            get { return "~/Members/Operations/EstimateRateView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.EstimateRate; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public bool SaveOriginToAddressBook
        {
            get { return false; }
        }

        public bool SaveOriginWithServicesToAddressBook
        {
            get { return false; }
        }

        public bool SaveDestinationToAddressBook
        {
            get { return false; }
        }

        public bool SaveDestinationWithServicesToAddressBook
        {
            get { return false; }
        }

        public List<ServiceViewSearchDto> Services
        {
            set
            {
                var services = value
                    .Select(c => new
                        {
                            Selected = false, 
                            Text = string.Format(" {0}", c.Description), 
                            Value = c.Id
                        })
                    .OrderBy(v => v.Text)
                    .ToList();

                rptServices.DataSource = services;
                rptServices.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Shipment, PaymentInformation>> SaveShipment;
        public event EventHandler<ViewEventArgs<LoadOrder, PaymentInformation>> SaveLoadOrder;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;


        public void DisplayCustomer(Customer customer)
        {
            var oldCustomerId = hidCustomerId.Value; // preserve old customer id;

            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            if (!txtCustomerNumber.CssClass.Contains("inputHiglight")) txtCustomerNumber.CssClass += " inputHiglight";
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
            hidCustomerOutstandingBalance.Value = customer == null ? default(decimal).ToString() : customer.OutstandingBalance().ToString();

            // require customer reconfigurations!!!

            var customerIdSpecificFilter = customer == null ? default(long) : customer.Id;

            libraryItemFinder.CustomerIdSpecificFilter = customerIdSpecificFilter;
            libraryItemFinder.Reset();

            if (customer != null)
            {
                // if change in customer, update insurance status
                if (customer.Rating != null && oldCustomerId != hidCustomerId.Value)
                    chkDeclineInsurance.Checked = !customer.Rating.InsuranceEnabled;

                pnlDeclineInsurance.Visible = customer.Rating != null && customer.Rating.InsuranceEnabled;
            }

            ddlShipmentDate.Focus();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                      ? string.Join(WebApplicationConstants.HtmlBreak,
                                                    messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                      Select(m => m.Message).ToArray())
                                      : string.Empty;
        }

        public void Set(LoadOrder loadOrder)
        {

        }

        public void Set(ShoppedRate shoppedRate)
        {
            hidShoppedRateId.Value = shoppedRate.Id.ToString();
        }

        public void Set(Shipment shipment)
        {

        }


        private bool ValidateShipmentDateAndTime()
        {
            if (string.IsNullOrEmpty(ddlShipmentDate.SelectedValue))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Shipment date is invalid!") });
                return false;
            }

            return true;
        }

        private Shipment RetrieveShipment()
        {
            var shipment = new Shipment
            {
                User = ActiveUser,
                TenantId = ActiveUser.TenantId,
                DateCreated = DateTime.Now,
                ActualDeliveryDate = DateUtility.SystemEarliestDateTime,
                ActualPickupDate = DateUtility.SystemEarliestDateTime,
                Status = ShipmentStatus.Scheduled,
                Notes = new List<ShipmentNote>(),
                Stops = new List<ShipmentLocation>(),
                Equipments = new List<ShipmentEquipment>(),
                Documents = new List<ShipmentDocument>(),
                Assets = new List<ShipmentAsset>(),
                Vendors = new List<ShipmentVendor>(),
                AccountBuckets = new List<ShipmentAccountBucket>(),
                AutoRatingAccessorials = new List<ShipmentAutoRatingAccessorial>(),
                GeneralBolComments = string.Empty,
                CriticalBolComments = string.Empty,

                HazardousMaterial = chkHazardousMaterials.Checked,
                HazardousMaterialContactEmail = string.Empty,
                HazardousMaterialContactMobile = string.Empty,
                HazardousMaterialContactName = string.Empty,
                HazardousMaterialContactPhone = string.Empty,
                ShipperBol = string.Empty,
                DeclineInsurance = pnlDeclineInsurance.Visible && chkDeclineInsurance.Checked,

                LinearFootRuleBypassed = chkBypassLinearFootRule.Checked,
            };

            var search = new PostalCodeSearch();

            shipment.Origin = new ShipmentLocation
            {
                Shipment = shipment,
                TenantId = ActiveUser.TenantId,
                PostalCode = txtOriginPostalCode.Text,
                CountryId = ddlOriginCountry.SelectedValue.ToLong(),
                StopOrder = 0,
                Contacts = new List<ShipmentContact>()
            };
            var code = search.FetchPostalCodeByCode(shipment.Origin.PostalCode, shipment.Origin.CountryId);
            shipment.Origin.State = code.State;
            shipment.Origin.City = code.City;
            shipment.Origin.Contacts.Add(new ShipmentContact
            {
                TenantId = ActiveUser.TenantId,
                ContactTypeId = ActiveUser.Tenant.DefaultSchedulingContactTypeId,
                Location = shipment.Origin,
                Primary = true
            });
            shipment.Destination = new ShipmentLocation
            {
                Shipment = shipment,
                TenantId = ActiveUser.TenantId,
                PostalCode = txtDestinationPostalCode.Text,
                CountryId = ddlDestinationCountry.SelectedValue.ToLong(),
                StopOrder = 1,
                Contacts = new List<ShipmentContact>()
            };
            code = search.FetchPostalCodeByCode(shipment.Destination.PostalCode, shipment.Destination.CountryId);
            shipment.Destination.State = code.State;
            shipment.Destination.City = code.City;
            shipment.Destination.Contacts.Add(new ShipmentContact
            {
                TenantId = ActiveUser.TenantId,
                ContactTypeId = ActiveUser.Tenant.DefaultSchedulingContactTypeId,
                Location = shipment.Destination,
                Primary = true
            });

            shipment.ShipmentNumber = string.Empty;

            var customer = new Customer(hidCustomerId.Value.ToLong());
            shipment.Customer = customer;
            shipment.AccountBuckets.Add(new ShipmentAccountBucket
            {
                AccountBucket = customer.DefaultAccountBucket,
                Shipment = shipment,
                TenantId = ActiveUser.TenantId,
                Primary = true,
            });
            shipment.Prefix = shipment.Customer == null ? null : shipment.Customer.Prefix;
            shipment.HidePrefix = shipment.Customer != null && shipment.Customer.HidePrefix;
            shipment.DesiredPickupDate = ddlShipmentDate.SelectedValue.ToDateTime();

            shipment.EarlyPickup = TimeUtility.DefaultOpen;
            shipment.EarlyDelivery = TimeUtility.DefaultOpen;
            shipment.LatePickup = TimeUtility.DefaultClose;
            shipment.LateDelivery = TimeUtility.DefaultClose;

            shipment.DeclineInsurance = chkDeclineInsurance.Checked;

            //Services
            shipment.Services = (from RepeaterItem item in rptServices.Items
                                 where item.FindControl("chkSelected").ToCheckBox().Checked
                                 select new ShipmentService
                                 {
                                     Shipment = shipment,
                                     ServiceId = item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong(),
                                     TenantId = ActiveUser.TenantId,
                                 })
                .ToList();
            CheckForAutoServices(shipment);
            if (shipment.Services.Any(s => s.ServiceId == ActiveUser.Tenant.HazardousMaterialServiceId) && !shipment.HazardousMaterial)
                shipment.HazardousMaterial = true;

            //Items
            var items = lstItems.Items
                .Select(i =>
                {
                    var convert = chkMetric.Checked;
                    
                    var txtQty = i.FindControl("txtQuantity").ToTextBox();
                    if (txtQty.Text.ToInt() <= 0) txtQty.Text = 1.ToString();
                    
                    var txtPcs = i.FindControl("txtPieceCount").ToTextBox();
                    if (txtPcs.Text.ToInt() <= 0) txtPcs.Text = 1.ToString();
                    return new ShipmentItem
                    {
                        Pickup = shipment.Origin.StopOrder,
                        Delivery = shipment.Destination.StopOrder,
                        ActualWeight = convert ? i.FindControl("txtWeight").ToTextBox().Text.ToDecimal().KgToLb() : i.FindControl("txtWeight").ToTextBox().Text.ToDecimal(),
                        ActualLength = convert ? i.FindControl("txtLength").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtLength").ToTextBox().Text.ToDecimal(),
                        ActualWidth = convert ? i.FindControl("txtWidth").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtWidth").ToTextBox().Text.ToDecimal(),
                        ActualHeight = convert ? i.FindControl("txtHeight").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtHeight").ToTextBox().Text.ToDecimal(),
                        Quantity = txtQty.Text.ToInt(),
                        PieceCount = txtPcs.Text.ToInt(),
                        RatedFreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                        ActualFreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                        Value = i.FindControl("txtValue").ToTextBox().Text.ToDecimal(),
                        IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
                        PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                        Description = i.FindControl("txtDescription").ToTextBox().Text,
                        Comment = i.FindControl("hidComment").ToCustomHiddenField().Value,
                        HTSCode = i.FindControl("hidHtsCode").ToCustomHiddenField().Value,
                        NMFCCode = i.FindControl("hidNmfcCode").ToCustomHiddenField().Value,
                        Tenant = shipment.Tenant,
                        Shipment = shipment,
                    };
                })
                .ToList();
            shipment.Items = items;

            return shipment;
        }


        private void CheckForAutoServices(Shipment shipment)
        {
            var bcsId = string.Empty;
            var hmId = string.Empty;
            if (shipment.IsInternational() && ActiveUser.Tenant.BorderCrossingServiceId != default(long) &&
                shipment.Services.All(s => s.ServiceId != ActiveUser.Tenant.BorderCrossingServiceId))
            {
                shipment.Services.Add(new ShipmentService
                {
                    TenantId = ActiveUser.TenantId,
                    Shipment = shipment,
                    ServiceId = ActiveUser.Tenant.BorderCrossingServiceId
                });
                bcsId = ActiveUser.Tenant.BorderCrossingServiceId.ToString();

            }

            if (shipment.HazardousMaterial && ActiveUser.Tenant.HazardousMaterialServiceId != default(long) &&
                shipment.Services.All(s => s.ServiceId != ActiveUser.Tenant.HazardousMaterialServiceId))
            {
                shipment.Services.Add(new ShipmentService
                {
                    TenantId = ActiveUser.TenantId,
                    Shipment = shipment,
                    ServiceId = ActiveUser.Tenant.HazardousMaterialServiceId
                });
                hmId = ActiveUser.Tenant.HazardousMaterialServiceId.ToString();
            }

            if (!string.IsNullOrEmpty(hmId) || !string.IsNullOrEmpty(bcsId))
            {
                foreach (RepeaterItem item in rptServices.Items)
                {
                    var id = item.FindControl("hidServiceId").ToCustomHiddenField().Value;
                    if (id == hmId || id == bcsId) item.FindControl("chkSelected").ToCheckBox().Checked = true;
                }
                    
            }
        }


        private void InitialDataSetup()
        {
            // customer check
            if (!ActiveUser.TenantEmployee && !ActiveUser.UserShipAs.Any()) SingleCustomerSetup();

            // default shipment date
            var dates = new List<ViewListItem>();
            for (var i = 0; i <= ProcessorVars.AdvancedCustomerLTLScheduleLimit[ActiveUser.TenantId]; i++)
            {
                var date = DateTime.Now.AddDays(i);
                dates.Add(new ViewListItem(string.Format("{0:dddd MMM dd}", date), date.FormattedShortDate()));
            }
            dates.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, string.Empty));
            ddlShipmentDate.DataSource = dates;
            ddlShipmentDate.DataBind();
            ddlShipmentDate.SelectedValue = dates[1].Value;

            // single item
            AddSingleBlankItem();
        }

        private void SingleCustomerSetup()
        {
            if (!ActiveUser.DefaultCustomer.Active)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
                return;
            }

            DisplayCustomer(ActiveUser.DefaultCustomer);

            txtCustomerNumber.ReadOnly = true;
            ibtnFindCustomer.Enabled = false;
            if (!txtCustomerNumber.CssClass.Contains("disabled")) txtCustomerNumber.CssClass += " disabled";
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new RateAndScheduleHandler(this).Initialize();

            pnlBypassLinearView.Visible = ActiveUser.TenantEmployee;

            if (IsPostBack) return;

            txtCustomerNumber.Focus();

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            if (Loading != null)
                Loading(this, new EventArgs());

            // Set default country values
            ddlOriginCountry.SelectedValue = ActiveUser.Tenant.DefaultCountryId.GetString();
            ddlDestinationCountry.SelectedValue = ActiveUser.Tenant.DefaultCountryId.GetString();

            // New Record Setup
            InitialDataSetup();
        }



        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }


        private string CalculateDensity(decimal weight, decimal length, decimal width, decimal height, int qty)
        {
            if (length != default(long) && width != default(long) && height != default(long) && qty != default(int))
            {
                return chkEnglish.Checked
                        ? ((weight / (length.InToFt() * width.InToFt() * height.InToFt())) / qty).ToString("n2")
                        : ((weight / (length.CmToM() * width.CmToM() * height.CmToM())) / qty).ToString("n2");
            }
            return string.Empty;
        }

        protected void OnItemsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = e.Item as ListViewDataItem;
            if (item == null) return;

            var ddlFreightClass = item.FindControl("ddlFreightClass").ToDropDownList();

            var freightClasses = WebApplicationSettings.FreightClasses
                .Select(fc => new ViewListItem(fc.ToString(), fc.ToString()))
                .ToList();
            freightClasses.Insert(0, new ViewListItem(string.Empty, string.Empty));
            ddlFreightClass.DataSource = freightClasses;
            ddlFreightClass.DataBind();

            if (item.DataItem.HasGettableProperty("FreightClass"))
                ddlFreightClass.SelectedValue = item.DataItem.GetPropertyValue("FreightClass").ToString();

            var weight = e.Item.FindControl("txtWeight").ToTextBox();
            var length = e.Item.FindControl("txtLength").ToTextBox();
            var width = e.Item.FindControl("txtWidth").ToTextBox();
            var height = e.Item.FindControl("txtHeight").ToTextBox();
            var density = e.Item.FindControl("txtDensity").ToTextBox();
            var qty = e.Item.FindControl("txtQuantity").ToTextBox();

            if (weight != null && length != null && width != null && height != null && density != null && qty != null)
            {
                var wgt = weight.Text.ToDecimal();
                var l = length.Text.ToDecimal();
                var w = width.Text.ToDecimal();
                var h = height.Text.ToDecimal();
                var q = qty.Text.ToInt();


                var densityValue = CalculateDensity(wgt, l, w, h, q);
                density.Text = densityValue;

                const string format = "javascript:UpdateDensity('{0}', '{1}', '{2}', '{3}', '{4}', '', '{5}');";
                weight.Attributes.Add("onblur", string.Format(format, weight.ClientID, length.ClientID, width.ClientID, height.ClientID, density.ClientID, qty.ClientID));
                length.Attributes.Add("onblur", string.Format(format, weight.ClientID, length.ClientID, width.ClientID, height.ClientID, density.ClientID, qty.ClientID));
                width.Attributes.Add("onblur", string.Format(format, weight.ClientID, length.ClientID, width.ClientID, height.ClientID, density.ClientID, qty.ClientID));
                height.Attributes.Add("onblur", string.Format(format, weight.ClientID, length.ClientID, width.ClientID, height.ClientID, density.ClientID, qty.ClientID));
                qty.Attributes.Add("onblur", string.Format(format, weight.ClientID, length.ClientID, width.ClientID, height.ClientID, density.ClientID, qty.ClientID));
            }
        }


        private void AddSingleBlankItem()
        {
            var vItems = lstItems.Items
                .Select(i => new
                {
                    Weight = i.FindControl("txtWeight").ToTextBox().Text,
                    Length = i.FindControl("txtLength").ToTextBox().Text,
                    Width = i.FindControl("txtWidth").ToTextBox().Text,
                    Height = i.FindControl("txtHeight").ToTextBox().Text,
                    Density = i.FindControl("hidDensityValue").ToCustomHiddenField().Value,
                    IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
                    PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                    PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text,
                    Value = i.FindControl("txtValue").ToTextBox().Text,
                    Description = i.FindControl("txtDescription").ToTextBox().Text,
                    Comment = i.FindControl("hidComment").ToCustomHiddenField().Value,
                    HTSCode = i.FindControl("hidHtsCode").ToCustomHiddenField().Value,
                    NMFCCode = i.FindControl("hidNmfcCode").ToCustomHiddenField().Value,
                })
                .ToList();

            vItems.Add(new
            {
                Weight = string.Empty,
                Length = string.Empty,
                Width = string.Empty,
                Height = string.Empty,
                Density = string.Empty,
                IsStackable = false,
                Quantity = string.Empty,
                PackageTypeId = default(long),
                FreightClass = 0d,
                PieceCount = string.Empty,
                Value = string.Empty,
                Description = string.Empty,
                Comment = string.Empty,
                HTSCode = string.Empty,
                NMFCCode = string.Empty,
            });

            lstItems.DataSource = vItems;
            lstItems.DataBind();
        }

        protected void OnAddItemClicked(object sender, EventArgs e)
        {
            AddSingleBlankItem();
        }

        protected void OnDeleteItemClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var rowItemIndex = button.FindControl("hidItemIndex").ToCustomHiddenField().Value.ToLong();


            var vItems = lstItems
                .Items
                .Select(item => new
                    {
                        Weight = item.FindControl("txtWeight").ToTextBox().Text,
                        Length = item.FindControl("txtLength").ToTextBox().Text,
                        Width = item.FindControl("txtWidth").ToTextBox().Text,
                        Height = item.FindControl("txtHeight").ToTextBox().Text,
                        Density = item.FindControl("hidDensityValue").ToCustomHiddenField().Value,
                        IsStackable = item.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
                        Quantity = item.FindControl("txtQuantity").ToTextBox().Text,
                        PackageTypeId = item.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                        FreightClass = item.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                        PieceCount = item.FindControl("txtPieceCount").ToTextBox().Text,
                        Value = item.FindControl("txtValue").ToTextBox().Text,
                        Description = item.FindControl("txtDescription").ToTextBox().Text,
                        Comment = item.FindControl("hidComment").ToCustomHiddenField().Value,
                        HTSCode = item.FindControl("hidHtsCode").ToCustomHiddenField().Value,
                        NMFCCode = item.FindControl("hidNmfcCode").ToCustomHiddenField().Value,
                        rowItemIndex = item.FindControl("hidItemIndex").ToCustomHiddenField().Value.ToLong(),
                    })
                .Where(item => item.rowItemIndex != rowItemIndex)
                .ToList();

            lstItems.DataSource = vItems;
            lstItems.DataBind();

        }


        protected void OnAddLibraryItemClicked(object sender, EventArgs e)
        {
            libraryItemFinder.Visible = true;
        }

        protected void OnLibraryItemFinderMultiItemSelected(object sender, ViewEventArgs<List<LibraryItem>> e)
        {
            var items = lstItems.Items
                .Select(i => new
                {
                    Weight = i.FindControl("txtWeight").ToTextBox().Text,
                    Length = i.FindControl("txtLength").ToTextBox().Text,
                    Width = i.FindControl("txtWidth").ToTextBox().Text,
                    Height = i.FindControl("txtHeight").ToTextBox().Text,
                    Density = i.FindControl("hidDensityValue").ToCustomHiddenField().Value,
                    IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
                    PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue,
                    PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text,
                    Value = i.FindControl("txtValue").ToTextBox().Text,
                    Description = i.FindControl("txtDescription").ToTextBox().Text,
                    Comment = i.FindControl("hidComment").ToCustomHiddenField().Value,
                    HTSCode = i.FindControl("hidHtsCode").ToCustomHiddenField().Value,
                    NMFCCode = i.FindControl("hidNmfcCode").ToCustomHiddenField().Value,
                })
                .ToList();

            for (var i = items.Count - 1; i >= 0; i--)
            {
                var item = items[i];
                if (string.IsNullOrEmpty(item.Description) && string.IsNullOrEmpty(item.Weight) &&
                    string.IsNullOrEmpty(item.Length) && string.IsNullOrEmpty(item.Width) &&
                    string.IsNullOrEmpty(item.Height) && string.IsNullOrEmpty(item.Quantity) &&
                    item.PackageTypeId == default(long) && string.IsNullOrEmpty(item.FreightClass) &&
                    string.IsNullOrEmpty(item.PieceCount) && string.IsNullOrEmpty(item.NMFCCode) &&
                    string.IsNullOrEmpty(item.HTSCode) && string.IsNullOrEmpty(item.Value) &&
                    string.IsNullOrEmpty(item.Comment))
                    items.RemoveAt(i);
            }

            var itemsToAdd = e.Argument
                                .Select(t => new
                                {
                                    Weight = t.Weight.ToString(),
                                    Length = t.Length.ToString(),
                                    Width = t.Width.ToString(),
                                    Height = t.Height.ToString(),
                                    Density = CalculateDensity(t.Weight, t.Length, t.Width, t.Height, t.Quantity),
                                    t.IsStackable,
                                    Quantity = t.Quantity.ToString(),
                                    t.PackageTypeId,
                                    FreightClass = t.FreightClass.ToString(),
                                    PieceCount = t.PieceCount.ToString(),
                                    Value = 0.ToString(),
                                    t.Description,
                                    t.Comment,
                                    t.HTSCode,
                                    t.NMFCCode,
                                })
            .ToList();

            items.AddRange(itemsToAdd);

            lstItems.DataSource = items;
            lstItems.DataBind();

            libraryItemFinder.Visible = false;
        }

        protected void OnLibraryItemFinderItemCancelled(object sender, EventArgs e)
        {
            libraryItemFinder.Visible = false;
        }



        protected void OnOriginPostalCodeSearchClicked(object sender, ImageClickEventArgs e)
        {
            hidPostalCodeSet.Value = Origin;
            postalCodeFinder.Visible = true;
        }

        protected void OnDestinationPostalCodeSearchClicked(object sender, ImageClickEventArgs e)
        {
            hidPostalCodeSet.Value = Destination;
            postalCodeFinder.Visible = true;
        }

        protected void OnPostalCodeFinderItemSelected(object sender, ViewEventArgs<PostalCodeViewSearchDto> e)
        {
            switch (hidPostalCodeSet.Value)
            {
                case Origin:
                    txtOriginPostalCode.Text = e.Argument.Code;
                    ddlOriginCountry.SelectedValue = e.Argument.CountryId.ToString();
                    break;
                case Destination:
                    txtDestinationPostalCode.Text = e.Argument.Code;
                    ddlDestinationCountry.SelectedValue = e.Argument.CountryId.ToString();
                    break;
            }
            postalCodeFinder.Visible = false;
        }

        protected void OnPostalCodeFinderItemCancelled(object sender, EventArgs e)
        {
            postalCodeFinder.Visible = false;
        }


        protected void OnRateSelectionDisplaySelectionCancelled(object sender, EventArgs e)
        {
            rateSelectionDisplay.Visible = false;
        }

        protected void OnRateSelectionDisplayRatesLoaded(object sender, ViewEventArgs<ShoppedRate> e)
        {
            if (SaveShoppedRate != null)
                SaveShoppedRate(this, new ViewEventArgs<ShoppedRate>(e.Argument));
        }

        protected void OnRateSelectionDisplayDownloadRates(object sender, ViewEventArgs<bool, List<Rate>> e)
        {
            var title = string.Format("{0}_RatesDownload.pdf", Guid.NewGuid());
            Response.Export(this.GenerateRateDownload(RetrieveShipment(), e.Argument2, e.Argument).ToPdf(title), title);
        }



        protected void OnGetEstimateRateClicked(object sender, EventArgs e)
        {
            if (!ValidateShipmentDateAndTime()) return;

            var shipment = RetrieveShipment();

            rateSelectionDisplay.Visible = true;
            rateSelectionDisplay.Rate(shipment);
        }

        protected void OnGotoRateAndScheduleClicked(object sender, EventArgs e)
        {
            if (!ValidateShipmentDateAndTime()) return;

            var shipment = RetrieveShipment();

            shipment.CustomerReferences = new List<ShipmentReference>();

            Session[WebApplicationConstants.EstimateRateShipmentTransfer] = shipment;
            Response.Redirect(RateAndScheduleView.PageAddress);
        }
    }
}
