﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern;
using LogisticsPlus.Eship.Extern.Edi;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.InputControls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using EquipmentType = LogisticsPlus.Eship.Core.Registry.EquipmentType;
using Location = LogisticsPlus.Eship.Core.Location;
using Shipment = LogisticsPlus.Eship.Core.Operations.Shipment;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    /*
	 *		Truckload bidding process:
	 *		1. Use same construct as Send, Update, Cancel load tender
	 *		2. If existing bidding already completed, just follow regular send process
	 *		3. Must show notification on page that bidding is in process when applicable
	 *		4. Cannot change primary vendor when bidding is in progress [Validation should come from Processor layer.  NOtification to user on screen should let them know they should not change vendor]
	 *		5. Must cancel active bidding to change vendor [manual process by user] 
	 */

    public partial class LoadOrderView : MemberPageBaseWithPageStore, ILoadOrderView, IVendorRejectionLogProcView, ILocationCollectionResource, IDatLoadboardAssetView, ITruckloadBidView
    {
        private const string ChargesHeader = "Charges";
        private const string DocumentsHeader = "Documents";
        private const string ItemsHeader = "Items";
        private const string NotesHeader = "Notes";
        private const string StopsHeader = "Stops";


        private const string OriginStopText = "Origin";
        private const string DestinationStopText = "Destination";

        private const string RouteSummaryArgs = "RouteSummary";
        private const string ReturnArgs = "Return";
        private const string VendorLaneHistoryArgs = "VendorLaneHistory";
        private const string BolArgs = "BOL";
        private const string VendorRateAgreementArgs = "RateAgreement";
        private const string QuoteArgs = "Quote";
        private const string ShippingLabelArgs = "ShippingLabel";
        private const string DuplicateArgs = "Duplicate";
        private const string DuplicateReverseArgs = "DuplicateReverse";
        private const string ConvertToShipmentArgs = "CTS";
        private const string GoToShipmentArgs = "GTS";
        private const string SendLoadTenderNewArgs = "SendLoadTenderNewArgs";
        private const string SendLoadTenderUpdateArgs = "SendLoadTenderUpdateArgs";
        private const string CancelLoadTenderArgs = "CancelLoadTender";
        private const string SendLoadTenderEmailNewArgs = "SendLoadTenderEmailNewArgs";
        private const string CancelLoadTenderEmailNewArgs = "CancelLoadTenderEmailNewArgs";
        private const string UpdateLoadTenderEmailNewArgs = "UpdateLoadTenderEmailNewArgs";
        private const string PayForLoadOrderWithCreditCardArgs = "PayForLoadOrderWithCreditCard";
        private const string RefundCustomerPaymentsArgs = "RefundCustomerPayments";
        private const string ConvertShipmentToLoadOrderArgs = "ConvertShipmentToLoadOrder";
        private const string ConvertShipmentToLoadOrderReverseArgs = "ConvertShipmentToLoadOrderReverse";

        private const string SendLoadTenderNewFlag = "SLTN";
        private const string SendLoadTenderUpdateFlag = "SLTU";
        private const string CancelLoadTenderFlag = "CLT";

        private const string SendLoadTenderNewEmailFlag = "SLTNE";
        private const string SendLoadTenderUpdateEmailFlag = "SLTUE";
        private const string CancelLoadTenderEmailFlag = "CLTE";


        private const string OfferedCmdName = "Offered";
        private const string QuotedCmdName = "Quoted";
        private const string CancelledCmdName = "Cancelled";
        private const string CoveredCmdName = "Covered";
        private const string LostCmdName = "Lost";
        private const string AcceptedCmdName = "Accepted";

        private const string SaveFlag = "S";
        private const string DeleteFlag = "D";
        private const string VendorUpdateFlag = "V";
        private const string PrimaryVendorRejectLogFlag = "PVRLF";
        private const string NonPrimaryVendorRejectLogFlag = "NPVRLF";
        private const string CarrierCoordinatorFlag = "CarrierCoordinator";
        private const string LoadOrderCoordinatorFlag = "LoadOrderCoordinatorFlag";
        protected const string ModifyingDatPostingFlag = "MDPF";


        private const string LoadOrderChargeCodesKey = "LoadOrderChargeCodesKey";
        private const string LoadOrderNoteTypesKey = "LoadOrderNoteTypesKey";

        private const int ViewDeliveryStopOrder = 10000;

        private const string MissingShipmentRecErrMsg = "Corresponding shipment not found for current record!";

        private bool _doNotRemoveNonPrimaryVendor;
        private bool _transferToShipmentPage;
        private bool _roundTrip;
        private bool _emailTender;
        public bool RefundOrVoidMiscReceipts { get; set; }

        public bool SaveOriginToAddressBook
        {
            get { return chkSaveOrigin.Checked; }
        }

        public bool SaveDestinationToAddressBook
        {
            get { return chkSaveDestination.Checked; }
        }

        public static string PageAddress { get { return "~/Members/Operations/LoadOrderView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.LoadOrder; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; } }

        protected List<ViewListItem> ChargeCodeCollection
        {
            get { return ViewState[LoadOrderChargeCodesKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
        }

        public List<ViewListItem> ContactTypeCollection
        {
            get
            {
                var viewListItems = ProcessorVars.RegistryCache[ActiveUser.TenantId]
                    .ContactTypes
                    .OrderBy(c => c.Description)
                    .Select(t => new ViewListItem(t.Code, t.Id.ToString()))
                    .ToList();
                viewListItems.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));
                return viewListItems;
            }
        }

        protected List<ViewListItem> NoteTypeCollection
        {
            get { return PageStore[LoadOrderNoteTypesKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
        }

        public List<ViewListItem> CountryCollection
        {
            get
            {
                var viewListItems = ProcessorVars.RegistryCache.Countries
                    .Values
                    .OrderByDescending(t => t.SortWeight)
                    .ThenBy(t => t.Name)
                    .Select(c => new ViewListItem(c.Name, c.Id.ToString()))
                    .ToList();
                viewListItems.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));
                return viewListItems;
            }
        }


        public List<ServiceViewSearchDto> Services
        {
            set
            {
                var services = value
                    .OrderBy(s => s.Description)
                    .Select(c => new ViewListItem(c.FormattedString(), c.Id.ToString()))
                    .ToList();

                rptServices.DataSource = services;
                rptServices.DataBind();
            }
        }

        public List<EquipmentType> EquipmentTypes
        {
            set
            {
                var equipmentTypes = value
                    .OrderBy(e => e.TypeName)
                    .Select(c => new ViewListItem("  " + c.FormattedString(), c.Id.ToString()))
                    .ToList();

                rptEquipmentTypes.DataSource = equipmentTypes;
                rptEquipmentTypes.DataBind();
            }
        }

        public List<ViewListItem> VendorCollection
        {
            get
            {
                var viewListVendorItems = new List<ViewListItem>();

                if (lstVendors.Items.Any())
                {
                    viewListVendorItems.AddRange(lstVendors.Items.Select(c => new ViewListItem
                    {
                        Text = c.FindControl("txtName").ToTextBox().Text,
                        Value = c.FindControl("hidVendorId").ToCustomHiddenField().Value
                    }).ToList());
                }

                viewListVendorItems.Insert(0, new ViewListItem(WebApplicationConstants.AutoDefault, default(long).ToString()));

                if (hidPrimaryVendorId.Value != null)
                {
                    var primaryId = hidPrimaryVendorId.Value.ToLong();
                    var primaryVendor = new Vendor(primaryId, primaryId != default(long));

                    if (viewListVendorItems.All(w => w.Value.ToLong() != primaryVendor.Id))
                    {
                        viewListVendorItems.Insert(1, new ViewListItem { Text = primaryVendor.Name, Value = primaryVendor.Id.ToString() });
                    }
                }

                return viewListVendorItems;
            }
        }

        public Dictionary<int, string> DateUnits
        {
            set { vpscVendorPerformance.DateUnits = value; }
        }

        public Dictionary<int, string> NoteTypes
        {
            set
            {
                var types = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).OrderBy(t => t.Text).ToList();
                PageStore[LoadOrderNoteTypesKey] = types;
            }
        }

        public Dictionary<int, string> ServiceModes
        {
            set
            {
                ddlServiceMode.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                ddlServiceMode.DataBind();
            }
        }


        private Dictionary<long, string> StoredEdiMessageQueue
        {
            get
            {
                var store = PageStore.ContainsKey("EMQ") ? PageStore["EMQ"] as Dictionary<long, string> : null;
                if (store == null)
                {
                    store = new Dictionary<long, string>();
                    PageStore["EMQ"] = store;
                }
                return store;
            }
            set { PageStore["EMQ"] = value; }
        }

        private readonly List<string> _invalidEdiStatuses = new List<string> { WebApplicationConstants.EdiStatus204Cancelled, WebApplicationConstants.EdiStatusNoStatus, WebApplicationConstants.EdiStatusNotApplicable };


        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<LoadOrder>> Save;
        public event EventHandler<ViewEventArgs<LoadOrder>> Delete;
        public event EventHandler<ViewEventArgs<LoadOrder>> Lock;
        public event EventHandler<ViewEventArgs<LoadOrder>> UnLock;
        public event EventHandler<ViewEventArgs<LoadOrder>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<string>> VendorSearch;
        public event EventHandler<ViewEventArgs<LoadOrder, Shipment>> SaveConvertToShipment;
        public event EventHandler<ViewEventArgs<VendorRejectionLog>> LogVendorRejection;
        public event EventHandler<ViewEventArgs<DatLoadboardAssetPosting, bool>> CheckDatDelete;
        public event EventHandler<ViewEventArgs<LoadOrder>> CheckDatUpdate;
        public event EventHandler<ViewEventArgs<DatAssetSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<TruckloadBid, LoadOrder>> TruckloadBidSave;
        public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidDelete;
        public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidLock;
        public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidUnLock;
        public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidLoadAuditLog;



        public void DisplaySearchResult(List<DatLoadboardAssetPosting> assets)
        {
            // Method not supported for Load order view
        }

        public void DatRemovalError()
        {
            // Method not supported for Load order view
        }

        public void FinalizeConvertToShipment(Shipment shipment)
        {
            this.NotifyNewShipment(shipment, true);

            Session[WebApplicationConstants.TransferShipmentId] = shipment.Id;
            _transferToShipmentPage = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && hidFlag.Value == SaveFlag)
            {
                // clean up file cleared out!
                if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
                {
                    Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                    hidFilesToDelete.Value = string.Empty;
                }

                var loadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong(), false);

                //Send out edi 204s if any
                if (StoredEdiMessageQueue.Any())
                {
                    var truckloadBidMsg = ProcessLoadTenderMessageQueue(loadOrder);
                    var msgs = messages.ToList();
                    msgs.AddRange(truckloadBidMsg);
                    messages = msgs;
                    var currentVendor = new Vendor(hidPrimaryVendorId.Value.ToLong());
                    UpdateEdiStatus(currentVendor.VendorNumber, loadOrder.LoadOrderNumber, currentVendor.CanSendLoadTender(loadOrder.Status));
                }

                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<LoadOrder>(loadOrder));
                memberToolBar.ShowUnlock = false;
                SetEditStatus(false);
                hidFlag.Value = null;
            }

            if (hidFlag.Value == PrimaryVendorRejectLogFlag)
            {
                hidFlag.Value = null;
                if (!messages.HasErrors()) DisplayVendor(null);
            }

            if (hidFlag.Value == NonPrimaryVendorRejectLogFlag)
            {
                hidFlag.Value = null;
                _doNotRemoveNonPrimaryVendor = messages.HasErrors();
                litVendorsErrorMsg.Text = messages.HasErrors()
                                              ? string.Join(WebApplicationConstants.HtmlBreak,
                                                            messages.Where(m => m.Type == ValidateMessageType.Error)
                                                                    .OrderBy(m => m.Type)
                                                                    .Select(m => m.Message).ToArray())
                                              : string.Empty;
            }
            else litVendorsErrorMsg.Text = string.Empty;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Button = MessageButton.Ok;

            var scriptManager = ScriptManager.GetCurrent(Page);
            if (scriptManager != null && IsPostBack && scriptManager.IsInAsyncPostBack)  // disregard showing if in partial post back as message box is not in update panel
                messageBox.Visible = false;
            else
                messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }


        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            pnlDimScreen.Visible = false;
            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public List<ValidationMessage> TruckloadBidMsgs { get; set; }

        public void Set(LoadOrder loadOrder)
        {
            if (hidFlag.Value == DeleteFlag)
            {
                //If deleting record, empty out message sending queue
                StoredEdiMessageQueue = null;

                CheckDatDelete(this, new ViewEventArgs<DatLoadboardAssetPosting, bool>(new DatLoadboardAssetPosting(hidDatAssetId.Value.ToLong(), true), false));

                if (loadOrder.IsNew)
                {
                    Server.RemoveDirectory(WebApplicationSettings.LoadOrderFolder(ActiveUser.TenantId,
                                                                                  hidLoadOrderId.Value.ToLong()));
                    hidFlag.Value = string.Empty;
                }
            }
            else
            {
                CheckDatUpdate(loadOrder, new ViewEventArgs<LoadOrder>(loadOrder));

            }

            LoadLoadOrder(loadOrder);
            SetEditStatus(!loadOrder.IsNew);

        }

        public void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers = false)
        {
            var oldCustomerId = hidCustomerId.Value;	// preserve old customer id;

            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
            hidCustomerOutstandingBalance.Value = customer == null ? default(decimal).ToString() : customer.OutstandingBalance().ToString();
            hidCustomerCredit.Value = customer == null ? default(decimal).ToString() : customer.CreditLimit.ToString();
            pnlCashOnlyCustomer.Visible = customer != null && customer.IsCashOnly;

            // require customer reconfigurations!!!
            var customerIdSpecificFilter = customer == null ? default(long) : customer.Id;
            libraryItemFinder.CustomerIdSpecificFilter = customerIdSpecificFilter;
            libraryItemFinder.Reset();
            addressBookFinder.CustomerIdSpecificFilter = customerIdSpecificFilter;
            operationsAddressInputOrigin.SetCustomerIdSpecificFilter(customerIdSpecificFilter);
            operationsAddressInputDestination.SetCustomerIdSpecificFilter(customerIdSpecificFilter);

            if (hidAccountBucketId.Value.ToLong() == default(long) && customer != null)
                DisplayAccountBucket(customer.DefaultAccountBucket);

            if (hidPrefixId.Value.ToLong() == default(long) && customer != null && customer.PrefixId != default(long))
                DisplayPrefix(customer.Prefix);

            if (ddlMileageSource.SelectedValue.ToLong() == default(long) && customer != null) ddlMileageSource.SelectedValue = customer.RequiredMileageSourceId.GetString();

            // sales rep
            if (updateSalesRepAndResellers) DisplayCustomerSalesRepresentative(customer);

            if (customer != null)
            {
                // if change in customer, update insurance status
                if (customer.Rating != null)
                {
                    if (oldCustomerId != hidCustomerId.Value)
                        chkDeclineInsurance.Checked = !customer.Rating.InsuranceEnabled;

                    if (updateSalesRepAndResellers)
                        DisplayResellerAddition(customer.Rating.ResellerAddition, customer.Rating.BillReseller);

                    btnRetrieveDetails.Enabled = customer.Rating.ResellerAddition != null;
                    btnClearDetails.Enabled = customer.Rating.ResellerAddition != null;
                }
                else
                {
                    btnRetrieveDetails.Enabled = false;
                    btnClearDetails.Enabled = false;
                }

                LoadCustomerCustomFields(customer);

                // if change in customer, reset CSR link
                if (oldCustomerId != hidCustomerId.Value)
                    memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));
            }
            else
            {
                btnRetrieveDetails.Enabled = false;
                btnClearDetails.Enabled = false;
            }
        }

        public void DisplayVendor(Vendor vendor)
        {
            var oldPrimaryVendorId = hidPrimaryVendorId.Value.ToLong();

            if (hidFlag.Value == VendorUpdateFlag)
            {
                if (vendor != null &&
                    ProcessorVars.VendorInsuranceAlertThresholdDays.ContainsKey(vendor.TenantId) &&
                    vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId]))
                {
                    var msg = string.Format("{0} - {1} Cannot be primary. Reason: {2}", vendor.VendorNumber, vendor.Name,
                                            vendor.VendorInsuranceAlertText(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId]));
                    DisplayMessages(new[] { ValidationMessage.Error(msg) });
                    vendor = null;
                    hidFlag.Value = null;
                }

                if (vendor != null)
                {
                    var msg = CheckVendorConstraints(new[] { vendor });
                    if (msg.Any()) DisplayMessages(msg);
                }

                //If the old primary vendor needs to be notified, queue up a cancellation
                if ((vendor != null && vendor.Id != oldPrimaryVendorId) || vendor == null)
                {
                    var oldPrimaryVendor = new Vendor(oldPrimaryVendorId);
                    if (oldPrimaryVendor.CanSendLoadTender(hidStatus.Value.ToEnum<LoadOrderStatus>()) && (!_invalidEdiStatuses.Contains(hidEdiStatus.Value) || StoredEdiMessageQueue.ContainsKey(oldPrimaryVendor.Id)))
                    {
                        AddEdiFlagToVendorId(oldPrimaryVendor.Id, CancelLoadTenderFlag);
                    }
                }
            }

            txtVendorName.Text = vendor == null ? string.Empty : vendor.Name;
            txtVendorNumber.Text = vendor == null ? string.Empty : vendor.VendorNumber;
            hidPrimaryVendorId.Value = vendor == null ? default(long).ToString() : vendor.Id.ToString();
            ibtnLogVendorRejection.Enabled = vendor != null;
            ibtnViewVendorPerformanceStatistics.Visible = vendor != null && !vendor.IsNew;

            //reload the More toolbar actions to make sure the Edi messages can't be sent to a vendor without a com profile setup for 204 edi
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));
            UpdateEdiStatus(txtVendorNumber.Text, txtLoadOrderNumber.Text, vendor.CanSendLoadTender(hidStatus.Value.ToEnum<LoadOrderStatus>()));

            var ddlVendorsList = lstCharges.Items.Select(c => new {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField()
            }).ToList();

            if (vendor == null || !ProcessorVars.VendorInsuranceAlertThresholdDays.ContainsKey(vendor.TenantId))
            {
                ddlVendorsList.ForEach(cc =>
                {
                    cc.Vendors.Items.Clear();
                    BuildVendorsDropdownListOnChargesTab(cc.Vendors, cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId ? 0 : cc.SelectedVendorId.Value.ToLong());
                    cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId ? "0" : cc.SelectedVendorId.Value;
                });
                upPnlCharges.Update();

                pnlPrimaryVendorInsuranceAlert.Visible = false;
                litPrimaryVendorInsuranceAlert.Text = string.Empty;

                return;
            }

            pnlPrimaryVendorInsuranceAlert.Visible = vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId]);
            litPrimaryVendorInsuranceAlert.Text = vendor.VendorInsuranceAlertText(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId]);

            ddlVendorsList.ForEach(cc =>
            {
                cc.Vendors.Items.Clear();
                BuildVendorsDropdownListOnChargesTab(
                    cc.Vendors,
                    cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId && oldPrimaryVendorId != vendor.Id ? vendor.Id : cc.SelectedVendorId.Value.ToLong());
                cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId && oldPrimaryVendorId != vendor.Id ? vendor.Id.ToString() : cc.SelectedVendorId.Value;
            });
            upPnlCharges.Update();
        }

        public static string BuildLoadOrderLinks(string commaDelimitedLoadrOrderNumbers)
        {
            return commaDelimitedLoadrOrderNumbers
                .Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
                .Where(s => !string.IsNullOrEmpty(s.Trim()))
                .Select(s => string.Format("{0} <a href='{1}?{2}={0}' target='_blank' class='blue'><img src='~/images/icons2/arrow_newTab.png' alt='' width='16' align='absmiddle'/></a>", s.Trim(), PageAddress, WebApplicationConstants.TransferNumber))
                .ToArray()
                .CommaJoin();
        }



        private void DisplayCustomerSalesRepresentative(Customer customer)
        {
            var representative = customer == null ? null : customer.SalesRepresentative;

            var tier = representative == null
                           ? null
                           : representative.ApplicableTier(txtDateCreated.Text.ToDateTime(), ddlServiceMode.SelectedValue.ToEnum<ServiceMode>(), customer);

            var commPercent = tier == null ? string.Empty : tier.CommissionPercent.ToString("f4");
            var floorValue = tier == null ? string.Empty : tier.FloorValue.ToString("f4");
            var ceilingValue = tier == null ? string.Empty : tier.CeilingValue.ToString("f4");
            var addlEntityName = representative == null ? string.Empty : representative.AdditionalEntityName;
            var addlEntityCommPercent = representative == null ? string.Empty : representative.AdditionalEntityCommPercent.ToString("f4");
            DisplaySalesRepresentative(representative, commPercent, floorValue, ceilingValue, addlEntityName, addlEntityCommPercent);
        }

        private void DisplaySalesRepresentative(SalesRepresentative representative, string commPercent, string floorValue, string ceilingValue, string addlEntityName, string addlEntityCommPercent)
        {
            hidSalesRepresentativeId.Value = representative != null ? representative.Id.GetString() : string.Empty;

            var canSeeSalesRepData = ActiveUser.HasAccessTo(ViewCode.CanSeeSalesRepFinancialData);

            txtSalesRepresentativeCommissionPercent.Text = canSeeSalesRepData ? commPercent : WebApplicationConstants.OnFile;
            hidSalesRepresentativeCommissionPercent.Value = commPercent;

            txtSalesRepresentative.Text = representative != null
                                            ? string.Format("{0} - {1}", representative.SalesRepresentativeNumber,
                                                            representative.Name)
                                            : string.Empty;
            txtSalesRepresentativeAddlEntityName.Text = addlEntityName;
            txtSalesRepresentativeAddlEntityCommPercent.Text = canSeeSalesRepData ? addlEntityCommPercent : WebApplicationConstants.OnFile;
            txtSalesRepresentativeMinCommValue.Text = canSeeSalesRepData ? floorValue : WebApplicationConstants.OnFile;
            txtSalesRepresentativeMaxCommValue.Text = canSeeSalesRepData ? ceilingValue : WebApplicationConstants.OnFile;

            hidSalesRepresentativeAddlEntityCommPercent.Value = addlEntityCommPercent;
            hidSalesRepresentativeMinCommValue.Value = floorValue;
            hidSalesRepresentativeMaxCommValue.Value = ceilingValue;
        }

        private void DisplayResellerAddition(ResellerAddition reseller, bool billReseller)
        {
            if (reseller != null)
            {
                hidResellerAdditionId.Value = reseller.Id.ToString();
                litResellerAdditionName.Text = reseller.Name;
                hidBillReseller.Value = billReseller.ToString();
                litBillReseller.Text = billReseller ? "Billing: " : string.Empty;
                rptResellerAdditions.DataSource = RetrieveResellerAdditionData(reseller);
                rptResellerAdditions.DataBind();
            }
            else
            {
                hidResellerAdditionId.Value = default(long).ToString();
                litResellerAdditionName.Text = string.Empty;
                hidBillReseller.Value = false.ToString();
                litBillReseller.Text = string.Empty;
                rptResellerAdditions.DataSource = null;
                rptResellerAdditions.DataBind();
            }
        }

        private void AddServicesRequired(List<AddressBookService> services)
        {
            if (!services.Any()) return;

            var serviceIds = services.Select(s => s.ServiceId).ToList();


            var viewIds = rptServices
                .Items
                .Cast<RepeaterItem>()
                .Where(item => item.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(item => item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong())
                .ToList();


            foreach (var id in viewIds.Where(id => !serviceIds.Contains(id)))
                serviceIds.Add(id);

            DisplayServices(serviceIds);
        }

        private void DisplayAccountBucket(AccountBucket accountBucket)
        {
            txtAccountBucketCode.Text = accountBucket == null ? string.Empty : accountBucket.Code;
            txtAccountBucketDescription.Text = accountBucket == null ? string.Empty : accountBucket.Description;
            hidAccountBucketId.Value = accountBucket == null ? default(long).ToString() : accountBucket.Id.ToString();
            ddlAccountBucketUnit.SetSelectedAccountBucketUnit(default(long), accountBucket != null ? accountBucket.Id : default(long));
        }

        private void DisplayPrefix(Prefix prefix)
        {
            txtPrefixCode.Text = prefix == null ? string.Empty : prefix.Code;
            txtPrefixDescription.Text = prefix == null ? string.Empty : prefix.Description;
            hidPrefixId.Value = prefix == null ? default(long).ToString() : prefix.Id.ToString();
        }

        private void DisplayLoadOrderCoordinator(User user)
        {
            txtLoadOrderCoordinator.Text = user == null ? string.Empty : user.Username;
            txtLoadOrderCoordName.Text = user == null ? string.Empty : string.Format("{0} {1}", user.FirstName, user.LastName);
            hidLoadOrderCoordUserId.Value = user == null ? string.Empty : user.Id.ToString();
        }

        private void DisplayCarrierCoordinator(User user)
        {
            txtCarrierCoordUsername.Text = user == null ? string.Empty : user.Username;
            txtCarrierCoordName.Text = user == null ? string.Empty : string.Format("{0} {1}", user.FirstName, user.LastName);
            hidCarrierCoordUserId.Value = user == null ? string.Empty : user.Id.ToString();
        }

        private IEnumerable<ValidationMessage> CheckVendorConstraints(IEnumerable<Vendor> vendors)
        {
            var loadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong(), false);
            var messages = new List<ValidationMessage>();

            //Service Modes
            foreach (var vendor in vendors)
            {
                switch (loadOrder.ServiceMode)
                {
                    case ServiceMode.LessThanTruckload:
                        if (!vendor.HandlesLessThanTruckload)
                            messages.Add(
                                ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
                                                                        ServiceMode.LessThanTruckload.FormattedString())));
                        break;
                    case ServiceMode.Truckload:
                        if (!vendor.HandlesPartialTruckload)
                            messages.Add(
                                ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
                                                                        ServiceMode.Truckload.FormattedString())));
                        if (loadOrder.IsPartialTruckload && !vendor.HandlesPartialTruckload)
                            messages.Add(ValidationMessage.Warning(string.Format("{0} does not handle Partial Truckload shipments", vendor.Name)));
                        break;
                    case ServiceMode.Air:
                        if (!vendor.HandlesAir)
                            messages.Add(
                                ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
                                                                        ServiceMode.Air.FormattedString())));
                        break;
                    case ServiceMode.Rail:
                        if (!vendor.HandlesRail)
                            messages.Add(
                                ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
                                                                        ServiceMode.Rail.FormattedString())));
                        break;
                    case ServiceMode.SmallPackage:
                        if (!vendor.HandlesSmallPack)
                            messages.Add(
                                ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
                                                                        ServiceMode.SmallPackage.FormattedString())));
                        break;
                }

                //Services
                var vendorServices = vendor.Services.Select(s => s.ServiceId).ToList();
                var localVendor = vendor;
                messages.AddRange(from service in loadOrder.Services
                                  where !vendorServices.Contains(service.ServiceId)
                                  select ValidationMessage.Warning(string.Format("{0} does not provide the service: {1}", localVendor.Name, service.Service.Description)));

                //Equipment
                var vendorEquipment = vendor.Equipments.Select((e => e.EquipmentTypeId)).ToList();
                messages.AddRange(from equipment in loadOrder.Equipments
                                  where !vendorEquipment.Contains(equipment.EquipmentTypeId)
                                  select ValidationMessage.Warning(string.Format("{0} does not provide equipment type: {1}", localVendor.Name, equipment.EquipmentType.FormattedString())));
            }
            return messages;
        }

        private List<ToolbarMoreAction> ToolbarMoreActions(bool enabled)
        {
            var brDashboard = new ToolbarMoreAction
            {
                CommandArgs = ReturnArgs,
                ImageUrl = IconLinks.Operations,
                Name = ViewCode.ShipmentDashboard.FormattedString(),
                IsLink = false,
                NavigationUrl = string.Empty
            };
            var duplicate = new ToolbarMoreAction
            {
                CommandArgs = DuplicateArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Duplicate Load Order",
                NavigationUrl = string.Empty
            };

            var duplicateReverse = new ToolbarMoreAction
            {
                CommandArgs = DuplicateArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Duplicate Load Order (Round Trip)",
                NavigationUrl = string.Empty
            };

            var vendorLaneHistory = new ToolbarMoreAction
            {
                CommandArgs = VendorLaneHistoryArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Vendor Lane Buy/Sell",
                NavigationUrl = string.Empty
            };
            var bol = new ToolbarMoreAction
            {
                CommandArgs = BolArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Generate Bill of Lading",
                NavigationUrl = string.Empty
            };
            var csr = new ToolbarMoreAction
            {
                CommandArgs = BolArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = true,
                Name = "Customer Service Rep(s)",
                NavigationUrl = CustCSRView.PageAddress + "?" + WebApplicationConstants.CsrCustId + "=" + hidCustomerId.Value,
                OpenInNewWindow = true
            };

            var rateAgreement = new ToolbarMoreAction
            {
                CommandArgs = VendorRateAgreementArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Generate Vendor Rate Agreement",
                NavigationUrl = string.Empty
            };

            var quote = new ToolbarMoreAction
            {
                CommandArgs = QuoteArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Generate Quote",
                NavigationUrl = string.Empty
            };

            var shippingLabel = new ToolbarMoreAction
            {
                CommandArgs = ShippingLabelArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Generate Shipping Labels",
                NavigationUrl = string.Empty
            };

            var generateRouteSummary = new ToolbarMoreAction
            {
                CommandArgs = RouteSummaryArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Generate Route Summary",
                NavigationUrl = string.Empty
            };
            var convertToShipment = new ToolbarMoreAction
            {
                CommandArgs = ConvertToShipmentArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Convert To Shipment",
                NavigationUrl = string.Empty
            };
            var goToShipment = new ToolbarMoreAction
            {
                CommandArgs = GoToShipmentArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Go To Shipment",
                NavigationUrl = string.Empty
            };

            var sendLoadTenderNew = new ToolbarMoreAction
            {
                CommandArgs = SendLoadTenderNewArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Send Load Tender(New)",
                NavigationUrl = string.Empty
            };

            var sendLoadTenderUpdate = new ToolbarMoreAction
            {
                CommandArgs = SendLoadTenderUpdateArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Send Load Tender(Update)",
                NavigationUrl = string.Empty
            };

            var cancelLoadTender = new ToolbarMoreAction
            {
                CommandArgs = CancelLoadTenderArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Cancel Pending Load Tender",
                NavigationUrl = string.Empty
            };

            var payForLoadOrderWithCreditCard = new ToolbarMoreAction
            {
                CommandArgs = PayForLoadOrderWithCreditCardArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Collect Customer Payment",
                NavigationUrl = string.Empty
            };

            var refundCustomerPayments = new ToolbarMoreAction
            {
                CommandArgs = RefundCustomerPaymentsArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Refund Customer Payments",
                NavigationUrl = string.Empty
            };

            var convertShipmentToLoadOrder = new ToolbarMoreAction
            {
                CommandArgs = ConvertShipmentToLoadOrderArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Convert Shipment To Load Order",
                NavigationUrl = string.Empty
            };

            var convertShipmentToLoadOrderReverse = new ToolbarMoreAction
            {
                CommandArgs = ConvertShipmentToLoadOrderReverseArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Convert Shipment To Load Order (Round Trip)",
                NavigationUrl = string.Empty
            };

            var sendLoadTenderByEmail = new ToolbarMoreAction
            {
                CommandArgs = SendLoadTenderEmailNewArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Initiate Bidding By Email(New)",
                NavigationUrl = string.Empty
            };

            var cancelLoadTenderByEmail = new ToolbarMoreAction
            {
                CommandArgs = CancelLoadTenderEmailNewArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Cancel Bidding By Email",
                NavigationUrl = string.Empty
            };
            var updateLoadTenderByEmail = new ToolbarMoreAction
            {
                CommandArgs = UpdateLoadTenderEmailNewArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Update Bidding By Email",
                NavigationUrl = string.Empty
            };

            var seperator = new ToolbarMoreAction { IsSeperator = true };
            var notNew = hidLoadOrderId.Value.ToLong() != default(long);
            var actions = new List<ToolbarMoreAction>();
            var status = string.IsNullOrEmpty(hidStatus.Value) ? LoadOrderStatus.Offered : hidStatus.Value.ToEnum<LoadOrderStatus>();

            if (Access.Modify && notNew)
            {
                actions.Add(duplicate);
                actions.Add(duplicateReverse);
                if (ActiveUser.HasAccessToModify(ViewCode.Shipment) && enabled && status != LoadOrderStatus.Shipment)
                    actions.Add(convertToShipment);

            }

            if (ActiveUser.HasAccessTo(ViewCode.Shipment))
            {
                actions.Add(convertShipmentToLoadOrder);
                actions.Add(convertShipmentToLoadOrderReverse);
            }

            if (notNew)
            {
                if (actions.Any()) actions.Add(seperator);
                if (!enabled) actions.Add(rateAgreement);
                if (!enabled) actions.Add(quote);
                actions.Add(generateRouteSummary);



                // options only if load has been converted to a shipment.
                if (status == LoadOrderStatus.Shipment)
                {
                    actions.Add(shippingLabel);
                    actions.Add(bol);
                    actions.Add(goToShipment);
                }
            }

            if (enabled)
            {
                if (actions.Any()) actions.Add(seperator);
                actions.Add(vendorLaneHistory);
            }

            if (ActiveUser.HasAccessTo(ViewCode.Customer) && ActiveUser.TenantEmployee && notNew)
            {
                if (actions.Any()) actions.Add(seperator);
                actions.Add(csr);
            }

            if (actions.Any()) actions.Add(seperator);

            if (hidLoadOrderId.Value.ToLong() != default(long) && Access.Modify && txtStatus.Text.ToEnum<LoadOrderStatus>() != LoadOrderStatus.Shipment)
            {
                if (ActiveUser.HasAccessTo(ViewCode.CanCollectPaymentFromCustomer) && new Customer(hidCustomerId.Value.ToLong()).CanPayByCreditCard
                     && txtStatus.Text.ToEnum<LoadOrderStatus>() != LoadOrderStatus.Cancelled && txtStatus.Text.ToEnum<LoadOrderStatus>() != LoadOrderStatus.Lost)
                    actions.Add(payForLoadOrderWithCreditCard);

                if (ActiveUser.HasAccessTo(ViewCode.CanRefundPaymentFromCustomer) && hidMiscReceiptsRelatedToLoadOrderExist.Value.ToBoolean())
                    actions.Add(refundCustomerPayments);
            }

            actions.Add(brDashboard);

            var primaryVendorId = hidPrimaryVendorId.Value.ToLong();
            if (primaryVendorId != default(long) && enabled)
            {
                var primaryVendor = new Vendor(primaryVendorId);
                actions.Add(seperator);
                if (primaryVendor.CanSendLoadTender(status))
                {
                    var loadTenderDto = new XmlConnectSearch()
                        .FetchOutBoundLoadTenderByVendShipIdNumAndDocType(primaryVendor.VendorNumber, txtLoadOrderNumber.Text,
                                                                          ActiveUser.TenantId, ActiveUser.Id);

                    if (loadTenderDto == null) //If there wasn't a 204 found, allow sending a new 204
                    {
                        actions.Add(sendLoadTenderNew);
                    }
                    else //If the 204 exists, allow cancellation or updates
                    {
                        actions.Add(cancelLoadTender);
                        actions.Add(sendLoadTenderUpdate);
                    }
                }
                else
                {
                    var vendorEmail = primaryVendor.Communication != null
                                          ? primaryVendor.Communication.Notifications
                                                         .Where(
                                                             n => n.Milestone == Milestone.TruckloadBidNotification && n.Enabled)
                                                         .Select(n => n.Email).ToList()
                                          : new List<string>();
                    var bid =
                        new TruckloadBidSearch().FetchTruckloadBidByLoadOrderIdAndStatus(hidLoadOrderId.Value.ToLong(),
                                                                                         TruckloadBidStatus.Processing.ToInt(),
                                                                                         ActiveUser.TenantId);
                    if (vendorEmail.Count > 0)
                    {
                        if (bid == null) actions.Add(sendLoadTenderByEmail);
                        else
                        {
                            actions.Add(cancelLoadTenderByEmail);
                            actions.Add(updateLoadTenderByEmail);
                        }


                    }

                }
            }

            return actions;
        }

        private void SetEditStatus(bool enabled)
        {
            hidEditStatus.Value = enabled.ToString();

            var status = string.IsNullOrEmpty(hidStatus.Value) ? LoadOrderStatus.Offered : hidStatus.Value.ToEnum<LoadOrderStatus>();
            var canceled = status == LoadOrderStatus.Cancelled;
            var lost = status == LoadOrderStatus.Lost;
            var shipment = status == LoadOrderStatus.Shipment;

            var open = enabled && !canceled && !lost;

            imgCustomerSearch.Enabled = open;
            txtCustomerNumber.ReadOnly = !open;

            pnlDetails.Enabled = open;
            pnlAdditionalDetails.Enabled = open;
            pnlReferences.Enabled = open;
            pnlVendors.Enabled = open;
            pnlAccountBuckets.Enabled = open;
            pnlItems.Enabled = open;
            pnlCharges.Enabled = open;
            pnlNotes.Enabled = open;
            pnlDocuments.Enabled = open && hidLoadOrderId.Value.ToLong() != default(long);
            pnlStops.Enabled = open;

            memberToolBar.EnableSave = enabled;
            memberToolBar.EnableImport = open;

            lbtnOffered.Enabled = !shipment && enabled;
            lbtnQuoted.Enabled = !shipment && enabled;
            lbtnAccepted.Enabled = !shipment && enabled;
            lbtnCancelled.Enabled = !shipment && enabled;
            lbtnLost.Enabled = !shipment && enabled;
            lbtnCovered.Enabled = !shipment && enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

            // must do this here to load correct set of extensible actions
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(enabled));
        }

        private void BindTimeDropDowns()
        {
            var timeList = TimeUtility.BuildTimeList();

            ddlEarlyPickup.DataSource = timeList;
            ddlEarlyPickup.DataBind();

            ddlLatePickup.DataSource = timeList;
            ddlLatePickup.DataBind();

            ddlEarlyDelivery.DataSource = timeList;
            ddlEarlyDelivery.DataBind();

            ddlLateDelivery.DataSource = timeList;
            ddlLateDelivery.DataBind();
        }

        private static List<object> RetrieveResellerAdditionData(ResellerAddition resellerAddition)
        {
            var freight = new
            {
                Header = "Freight",
                Type = resellerAddition.LineHaulType.FormattedString(),
                Value = resellerAddition.LineHaulValue.ToString("f4"),
                Percent = resellerAddition.LineHaulPercentage.ToString("f4"),
                UseLower = resellerAddition.UseLineHaulMinimum
            };

            var fuel = new
            {
                Header = "Fuel",
                Type = resellerAddition.FuelType.FormattedString(),
                Value = resellerAddition.FuelValue.ToString("f4"),
                Percent = resellerAddition.FuelPercentage.ToString("f4"),
                UseLower = resellerAddition.UseFuelMinimum
            };

            var accessorial = new
            {
                Header = "Accessorial",
                Type = resellerAddition.AccessorialType.FormattedString(),
                Value = resellerAddition.AccessorialValue.ToString("f4"),
                Percent = resellerAddition.AccessorialPercentage.ToString("f4"),
                UseLower = resellerAddition.UseAccessorialMinimum
            };

            var service = new
            {
                Header = "Service",
                Type = resellerAddition.FuelType.FormattedString(),
                Value = resellerAddition.FuelValue.ToString("f4"),
                Percent = resellerAddition.FuelPercentage.ToString("f4"),
                UseLower = resellerAddition.UseFuelMinimum
            };

            var list = new List<object> { freight, fuel, accessorial, service };
            return list;
        }

        private List<LoadOrderLocation> RetrieveViewStops(LoadOrder loadOrder)
        {
            var locations = new List<LoadOrderLocation>();
            foreach (var item in lstStops.Items)
            {
                var control = item.FindControl("llicLocation").ToLocationListingInputControl();
                if (control == null) continue;
                var locationId = control.RetrieveLocationId();
                var location = new LoadOrderLocation(locationId, locationId != default(long));
                if (location.IsNew)
                {
                    location.TenantId = loadOrder.TenantId;
                    location.LoadOrder = loadOrder;
                }
                control.UpdateLocation(location);

                locations.Add(location);
            }
            return locations;
        }

        private List<LoadOrderService> RetreiveLoadOrderServices(LoadOrder loadOrder)
        {
            var services = (rptServices
               .Items
               .Cast<RepeaterItem>()
               .Where(item => item.FindControl("chkSelected").ToCheckBox().Checked)
               .Select(item => new LoadOrderService
               {
                   LoadOrder = loadOrder,
                   ServiceId = item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong(),
                   TenantId = ActiveUser.TenantId,
               }))
               .ToList();

            return services;
        }

        private List<LoadOrderEquipment> RetrieveLoadOrderEquipments(LoadOrder loadOrder)
        {
            var equipments = (rptEquipmentTypes
                .Items
                .Cast<RepeaterItem>()
                .Where(item => item.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(item => new LoadOrderEquipment
                {
                    LoadOrder = loadOrder,
                    EquipmentTypeId = item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong(),
                    TenantId = ActiveUser.TenantId,
                }))
                .ToList();

            return equipments;
        }

        private LoadOrder UpdateLoadOrder()
        {
            var id = hidLoadOrderId.Value.ToLong();
            var loadOrder = new LoadOrder(id, id != default(long));

            var editStatus = hidEditStatus.Value.ToBoolean();
            if (!editStatus) return loadOrder;

            if (loadOrder.IsNew)
            {
                loadOrder.TenantId = ActiveUser.TenantId;
                loadOrder.Origin = new LoadOrderLocation { LoadOrder = loadOrder, TenantId = ActiveUser.TenantId, };
                loadOrder.Destination = new LoadOrderLocation { LoadOrder = loadOrder, TenantId = ActiveUser.TenantId, };
                loadOrder.User = ActiveUser;
                loadOrder.DateCreated = DateTime.Now;
            }

            // load collections
            loadOrder.LoadCollections();
            if (loadOrder.Origin != null)
                loadOrder.Origin.LoadCollections();
            if (loadOrder.Destination != null)
                loadOrder.Destination.LoadCollections();

            //details
            loadOrder.Customer = new Customer(hidCustomerId.Value.ToLong());

            var oldStatus = loadOrder.Status;
            if (loadOrder.Status != LoadOrderStatus.Shipment)
                loadOrder.Status = txtStatus.Text.ToEnum<LoadOrderStatus>();

            RefundOrVoidMiscReceipts = oldStatus != LoadOrderStatus.Cancelled && loadOrder.Status == LoadOrderStatus.Cancelled;

            loadOrder.PrefixId = hidPrefixId.Value.ToLong();
            loadOrder.HidePrefix = chkHidePrefix.Checked;

            loadOrder.ShipmentPriorityId = ddlPriority.SelectedValue.ToLong();

            loadOrder.LoadOrderNumber = txtLoadOrderNumber.Text;
            loadOrder.ServiceMode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>();
            loadOrder.IsPartialTruckload = chkIsPartialTruckLoad.Checked;
            loadOrder.PurchaseOrderNumber = txtPurchaseOrderNumber.Text;
            loadOrder.ShipperReference = txtShipperReference.Text;
            loadOrder.ShipperBol = txtShipperBol.Text;
            loadOrder.DesiredPickupDate = txtDesiredPickupDate.Text.ToDateTime();

            loadOrder.EarlyPickup = ddlEarlyPickup.SelectedValue;
            loadOrder.LatePickup = ddlLatePickup.SelectedValue;
            loadOrder.EstimatedDeliveryDate = txtEstimatedDeliveryDate.Text.ToDateTime();

            loadOrder.EarlyDelivery = ddlEarlyDelivery.SelectedValue;
            loadOrder.LateDelivery = ddlLateDelivery.SelectedValue;
            loadOrder.Mileage = txtMileage.Text.ToDecimal();

            loadOrder.EmptyMileage = txtEmptyMileage.Text.ToDecimal();
            loadOrder.MileageSourceId = ddlMileageSource.SelectedValue.ToLong();

            loadOrder.MiscField1 = txtMiscField1.Text;
            loadOrder.MiscField2 = txtMiscField2.Text;

            loadOrder.DriverName = txtDriverName.Text;
            loadOrder.DriverPhoneNumber = txtDriverPhoneNumber.Text;
            loadOrder.DriverTrailerNumber = txtDriverTrailerNumber.Text;

            loadOrder.Services = RetreiveLoadOrderServices(loadOrder);
            loadOrder.Equipments = RetrieveLoadOrderEquipments(loadOrder);

            loadOrder.Description = txtDescription.Text;
            loadOrder.NotAvailableToLoadboards = chkHideFromLoadboards.Checked;

            loadOrder.SalesRepresentativeId = hidSalesRepresentativeId.Value.ToLong();
            loadOrder.SalesRepresentativeCommissionPercent = loadOrder.SalesRepresentativeId == default(long)
                                                                ? "0.0000".ToDecimal()
                                                                : hidSalesRepresentativeCommissionPercent.Value.ToDecimal();
            loadOrder.SalesRepAddlEntityCommPercent = loadOrder.SalesRepresentativeId == default(long)
                                                                ? "0.0000".ToDecimal()
                                                                : hidSalesRepresentativeAddlEntityCommPercent.Value.ToDecimal();
            loadOrder.SalesRepAddlEntityName = loadOrder.SalesRepresentativeId == default(long)
                                                   ? string.Empty
                                                   : txtSalesRepresentativeAddlEntityName.Text;
            loadOrder.SalesRepMinCommValue = loadOrder.SalesRepresentativeId == default(long)
                                                                ? "0.0000".ToDecimal()
                                                                : hidSalesRepresentativeMinCommValue.Value.ToDecimal();
            loadOrder.SalesRepMaxCommValue = loadOrder.SalesRepresentativeId == default(long)
                                                                ? "0.0000".ToDecimal()
                                                                : hidSalesRepresentativeMaxCommValue.Value.ToDecimal();

            DisplayServicesAndEquipmentTypes(loadOrder);

            //Add. Details
            loadOrder.GeneralBolComments = txtGeneralBolComments.Text;
            loadOrder.CriticalBolComments = txtCriticalBolComments.Text;

            loadOrder.HazardousMaterial = chkHazardousMaterial.Checked;
            loadOrder.HazardousMaterialContactName = txtHazardousContactName.Text;
            loadOrder.HazardousMaterialContactPhone = txtHazardousContactPhone.Text;
            loadOrder.HazardousMaterialContactMobile = txtHazardousContactMobile.Text;
            loadOrder.HazardousMaterialContactEmail = txtHazardousContactEmail.Text.StripSpacesFromEmails();



            // stops
            loadOrder.Stops = RetrieveViewStops(loadOrder);

            if (loadOrder.Origin != null)
            {
                loadOrder.Origin.TakeSnapShot();
                operationsAddressInputOrigin.RetrieveAddress(loadOrder.Origin);
                loadOrder.Origin.StopOrder = 0;
            }
            if (loadOrder.Destination != null)
            {
                loadOrder.Destination.TakeSnapShot();
                operationsAddressInputDestination.RetrieveAddress(loadOrder.Destination);
                loadOrder.Destination.StopOrder = loadOrder.Stops.Count + 1;
            }

            // must call retrieve stops prior to calling UpdateItems ....
            UpdateItemsFromView(loadOrder);
            UpdateReferencesFromView(loadOrder);
            UpdateAccountBucketsFromView(loadOrder);
            UpdateNotesFromView(loadOrder);
            UpdateVendorsFromView(loadOrder);
            UpdateDocumentsFromView(loadOrder);

            // Ensure hazmat is set on load order if any items are set as hazmat
            if (loadOrder.Items.Any(i => i.HazardousMaterial) && !loadOrder.HazardousMaterial)
            {
                chkHazardousMaterial.Checked = true;
                loadOrder.HazardousMaterial = true;
            }

            CheckRequiredHazMatService(loadOrder);


            //Charges
            UpdateChargesFromView(loadOrder);
            loadOrder.DeclineInsurance = chkDeclineInsurance.Checked;

            //accountBucketUnit
            loadOrder.AccountBucketUnitId = hidAccountBucketUnitId.Value.ToLong();

            // Reseller Addition
            loadOrder.ResellerAdditionId = hidResellerAdditionId.Value.ToLong();
            loadOrder.BillReseller = hidBillReseller.Value.ToBoolean();

            // coordinators
            loadOrder.CarrierCoordinatorId = hidCarrierCoordUserId.Value.ToLong();
            loadOrder.LoadOrderCoordinatorId = hidLoadOrderCoordUserId.Value.ToLong();

            //handle stop order; remember the ViewDeliveryStopOrder set for destination!!! in load method
            var finalStopOrder = loadOrder.Stops.Count + 1;
            foreach (var item in loadOrder.Items.Where(item => item.Delivery == ViewDeliveryStopOrder))
            {
                item.Delivery = finalStopOrder;
            }

            // job
            loadOrder.JobId = hidJobId.Value.ToLong();
            loadOrder.JobStep = txtJobStep.Text.ToInt();

            return loadOrder;
        }

        private void CheckRequiredHazMatService(LoadOrder loadOrder)
        {
            var hazMatServiceId = ActiveUser.Tenant.HazardousMaterialServiceId;

            if (hazMatServiceId != default(long) && loadOrder.Services.Any(s => s.ServiceId == hazMatServiceId) &&
                !loadOrder.HazardousMaterial)
            {
                loadOrder.HazardousMaterial = true;
            }

            else if (hazMatServiceId != default(long) && loadOrder.HazardousMaterial &&
                     loadOrder.Services.All(s => s.ServiceId != hazMatServiceId))
            {
                loadOrder.Services.Add(new LoadOrderService
                {
                    LoadOrder = loadOrder,
                    TenantId = ActiveUser.TenantId,
                    ServiceId = hazMatServiceId
                });
            }
        }

        private void UpdateItemsFromView(LoadOrder loadOrder)
        {
            var items = lstItems
                .Items
                .Select(i =>
                {
                    var id = i.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong();
                    var txtQuantity = i.FindControl("txtQuantity").ToTextBox();
                    if (txtQuantity.Text.ToInt() <= 0) txtQuantity.Text = 1.ToString();
                    var delivery = i.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt();
                    return new LoadOrderItem(id, id != default(long))
                    {
                        Pickup = i.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
                        Delivery = delivery == ViewDeliveryStopOrder ? loadOrder.Stops.Count + 1 : delivery,
                        Weight = i.FindControl("txtWeight").ToTextBox().Text.ToDecimal(),
                        Length = i.FindControl("txtLength").ToTextBox().Text.ToDecimal(),
                        Width = i.FindControl("txtWidth").ToTextBox().Text.ToDecimal(),
                        Height = i.FindControl("txtHeight").ToTextBox().Text.ToDecimal(),
                        Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                        PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text.ToInt(),
                        Description = i.FindControl("txtDescription").ToTextBox().Text,
                        Comment = i.FindControl("txtComment").ToTextBox().Text,
                        FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                        Value = i.FindControl("txtItemValue").ToTextBox().Text.ToDecimal(),
                        IsStackable = i.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                        HazardousMaterial = i.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
                        PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                        NMFCCode = i.FindControl("txtNMFCCode").ToTextBox().Text,
                        HTSCode = i.FindControl("txtHTSCode").ToTextBox().Text,
                        Tenant = loadOrder.Tenant,
                        LoadOrder = loadOrder,
                    };
                })
                .ToList();
            loadOrder.Items = items;
        }

        private void UpdateReferencesFromView(LoadOrder loadOrder)
        {
            var references = lstReferences.Items
                .Select(i =>
                {
                    var id = i.FindControl("hidReferenceId").ToCustomHiddenField().Value.ToLong();
                    return new LoadOrderReference(id, id != default(long))
                    {
                        Name = i.FindControl("txtName").ToTextBox().Text,
                        Value = i.FindControl("txtValue").ToTextBox().Text,
                        DisplayOnOrigin = i.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
                        DisplayOnDestination = i.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
                        Required = i.FindControl("hidRequired").ToCustomHiddenField().Value.ToBoolean(),
                        LoadOrder = loadOrder,
                        Tenant = loadOrder.Tenant
                    };
                })
                .ToList();
            loadOrder.CustomerReferences = references;
        }

        private void UpdateChargesFromView(LoadOrder loadOrder)
        {
            var charges = lstCharges.Items
                .Select(i =>
                {
                    var id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong();
                    var txtQty = (i.FindControl("txtQuantity").ToTextBox());
                    if (txtQty.Text.ToInt() <= 0) txtQty.Text = 1.ToString();
                    return new LoadOrderCharge(id, id != default(long))
                    {
                        Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                        UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                        UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                        UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                        Comment = i.FindControl("txtComment").ToTextBox().Text,
                        ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                        LoadOrder = loadOrder,
                        Tenant = loadOrder.Tenant,
                        VendorId = i.FindControl("ddlVendors").ToDropDownList().SelectedValue.ToLong()
                    };
                })
                .ToList();

            loadOrder.Charges = charges;
        }

        private void UpdateNotesFromView(LoadOrder loadOrder)
        {
            var notes = GetCurrentViewNotesUpdated()
                .Select(i =>
                    {
                        var note = new LoadOrderNote(i.Id, i.Id != default(long))
                        {
                            Type = i.Type,
                            Archived = i.Archived,
                            Classified = i.Classified,
                            Message = i.Message,
                            LoadOrder = loadOrder,
                            Tenant = loadOrder.Tenant,
                        };
                        if (note.IsNew) note.User = ActiveUser;
                        return note;
                    })
                .ToList();
            if (!ActiveUser.HasAccessTo(ViewCode.CanSeeArchivedNotes)) // account for not listed archived notes.
                notes.AddRange(loadOrder.Notes.Where(n => n.Archived));
            loadOrder.Notes = notes;
        }

        private void UpdateVendorsFromView(LoadOrder loadOrder)
        {
            var vendors = lstVendors.Items
                .Select(i =>
                {
                    var id = i.FindControl("hidLoadOrderVendorId").ToCustomHiddenField().Value.ToLong();
                    return new LoadOrderVendor(id, id != default(long))
                    {
                        VendorId = i.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                        LoadOrder = loadOrder,
                        TenantId = loadOrder.TenantId,
                        ProNumber = i.FindControl("txtProNumber").ToTextBox().Text,
                        Primary = false,
                    };
                })
                .ToList();

            var vendorId = hidPrimaryVendorId.Value.ToLong();
            if (vendorId != default(long))
            {
                var primaryId = hidPrimaryLoadOrderVendorId.Value.ToLong();
                vendors.Add(new LoadOrderVendor(primaryId, primaryId != default(long))
                {
                    VendorId = vendorId,
                    LoadOrder = loadOrder,
                    TenantId = loadOrder.TenantId,
                    ProNumber = txtPrimaryProNumber.Text,
                    Primary = true,
                });
            }

            loadOrder.Vendors = vendors;
        }

        private void UpdateAccountBucketsFromView(LoadOrder loadOrder)
        {
            var accountBuckets = lstAccountBuckets.Items
                .Select(i =>
                {
                    var id = i.FindControl("hidLoadOrderAccountBucketId").ToCustomHiddenField().Value.ToLong();
                    return new LoadOrderAccountBucket(id, id != default(long))
                    {
                        AccountBucketId = i.FindControl("hidAccountBucketId").ToCustomHiddenField().Value.ToLong(),
                        LoadOrder = loadOrder,
                        TenantId = loadOrder.TenantId,
                        Primary = false
                    };
                })
                .ToList();

            var primaryId = hidLoadOrderAccountBucketId.Value.ToLong();
            accountBuckets.Add(new LoadOrderAccountBucket(primaryId, primaryId != default(long))
            {
                AccountBucketId = hidAccountBucketId.Value.ToLong(),
                LoadOrder = loadOrder,
                Primary = true,
                TenantId = loadOrder.TenantId
            });

            loadOrder.AccountBuckets = accountBuckets;
        }

        private void UpdateDocumentsFromView(LoadOrder loadOrder)
        {
            var documents = lstDocuments.Items
                .Select(i =>
                {
                    var id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong();
                    return new LoadOrderDocument(id, id != default(long))
                    {
                        DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                        Name = i.FindControl("litDocumentName").ToLiteral().Text,
                        Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                        LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                        IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
                        LoadOrder = loadOrder,
                        TenantId = loadOrder.TenantId
                    };
                })
                .ToList();

            loadOrder.Documents = documents;
        }

        private void LoadLoadOrder(LoadOrder loadOrder, bool reloadLoadOrderCollections = true)
        {
            ConfigureServiceModeDisplay(loadOrder.ServiceMode);

            var isNew = loadOrder.IsNew;

            tabDocuments.Visible = !isNew; // cannot add document until shipment is saved at least once.

            if (!isNew && reloadLoadOrderCollections) loadOrder.LoadCollections();

            hidLoadOrderId.Value = loadOrder.Id.ToString();

            // necessary to clean out old references prior to attempting to load new ones.
            lstReferences.Items.Clear();
            lstReferences.DataSource = new List<object>();
            lstReferences.DataBind();
            litReferenceCount.Text = new List<object>().BuildTabCount();


            //Header
            txtLoadOrderNumber.Text = loadOrder.LoadOrderNumber;
            SetPageTitle(txtLoadOrderNumber.Text);
            txtStatus.Text = loadOrder.Status.FormattedString();
            hidStatus.Value = loadOrder.Status.ToString();
            DisplayCustomer(loadOrder.Customer);
            DisplayResellerAddition(loadOrder.ResellerAddition, loadOrder.BillReseller);

            var primaryLoadOrderVendor = loadOrder.Vendors.FirstOrDefault(v => v.Primary);
            hidPrimaryLoadOrderVendorId.Value = primaryLoadOrderVendor == null
                                                    ? default(long).ToString()
                                                    : primaryLoadOrderVendor.Id.ToString();
            var primaryVendor = primaryLoadOrderVendor == null ? null : primaryLoadOrderVendor.Vendor;
            DisplayVendor(primaryVendor);

            UpdateEdiStatus(primaryVendor == null ? string.Empty : primaryVendor.VendorNumber, loadOrder.LoadOrderNumber,
                            primaryVendor.CanSendLoadTender(loadOrder.Status));

            txtPrimaryProNumber.Text = primaryLoadOrderVendor != null ? primaryLoadOrderVendor.ProNumber : string.Empty;

            var loadOrderAccountBucket = loadOrder.AccountBuckets.FirstOrDefault(a => a.Primary);
            DisplayAccountBucket(loadOrderAccountBucket == null ? null : loadOrderAccountBucket.AccountBucket);
            ddlAccountBucketUnit.SetSelectedAccountBucketUnit(
                loadOrderAccountBucket == null ? default(long) : loadOrder.AccountBucketUnitId,
                loadOrderAccountBucket == null ? default(long) : loadOrderAccountBucket.AccountBucketId);
            hidLoadOrderAccountBucketId.Value = loadOrderAccountBucket == null
                                                    ? default(long).ToString()
                                                    : loadOrderAccountBucket.Id.ToString();

            DisplayPrefix(loadOrder.Prefix);
            chkHidePrefix.Checked = loadOrder.HidePrefix;


            ddlPriority.SelectedValue = loadOrder.ShipmentPriorityId.GetString();

            ddlServiceMode.SelectedValue = loadOrder.ServiceMode.ToInt().ToString();
            chkIsPartialTruckLoad.Checked = loadOrder.IsPartialTruckload;
            txtPurchaseOrderNumber.Text = loadOrder.PurchaseOrderNumber;
            txtShipperReference.Text = loadOrder.ShipperReference;
            txtShipperBol.Text = loadOrder.ShipperBol;
            txtDateCreated.Text = loadOrder.DateCreated.FormattedShortDate();
            txtDesiredPickupDate.Text = loadOrder.DesiredPickupDate == DateUtility.SystemEarliestDateTime
                                            ? string.Empty
                                            : loadOrder.DesiredPickupDate.FormattedShortDate();
            ddlEarlyPickup.SelectedValue = loadOrder.EarlyPickup;
            ddlLatePickup.SelectedValue = loadOrder.LatePickup;
            txtEstimatedDeliveryDate.Text = loadOrder.EstimatedDeliveryDate == DateUtility.SystemEarliestDateTime
                                                ? string.Empty
                                                : loadOrder.EstimatedDeliveryDate.FormattedShortDate();
            ddlEarlyDelivery.SelectedValue = loadOrder.EarlyDelivery;
            ddlLateDelivery.SelectedValue = loadOrder.LateDelivery;


            txtUser.Text = loadOrder.User != null ? loadOrder.User.Username : ActiveUser.Username;
            lnkUserPhone.InnerHtml = loadOrder.User != null ? loadOrder.User.Phone : ActiveUser.Phone;
            lnkUserPhone.HRef = string.Format("tel:{0}", loadOrder.User != null ? loadOrder.User.Phone : ActiveUser.Phone);
            lnkUserMobile.InnerHtml = loadOrder.User != null ? loadOrder.User.Mobile : ActiveUser.Mobile;
            lnkUserMobile.HRef = string.Format("tel:{0}", loadOrder.User != null ? loadOrder.User.Mobile : ActiveUser.Mobile);
            lnkUserFax.InnerHtml = loadOrder.User != null ? loadOrder.User.Fax : ActiveUser.Fax;
            lnkUserFax.HRef = string.Format("tel:{0}", loadOrder.User != null ? loadOrder.User.Fax : ActiveUser.Fax);
            lnkUserEmail.InnerHtml = loadOrder.User != null ? loadOrder.User.Email : ActiveUser.Email;
            lnkUserEmail.HRef = string.Format("mailto:{0}", loadOrder.User != null ? loadOrder.User.Email : ActiveUser.Email);

            txtMileage.Text = loadOrder.Mileage.ToString("f4");
            txtEmptyMileage.Text = loadOrder.EmptyMileage.ToString("f4");
            ddlMileageSource.SelectedValue = loadOrder.MileageSourceId.GetString();

            txtMiscField1.Text = loadOrder.MiscField1;
            txtMiscField2.Text = loadOrder.MiscField2;

            txtDriverName.Text = loadOrder.DriverName;
            txtDriverPhoneNumber.Text = loadOrder.DriverPhoneNumber;
            txtDriverTrailerNumber.Text = loadOrder.DriverTrailerNumber;

            txtDescription.Text = loadOrder.Description;
            chkHideFromLoadboards.Checked = loadOrder.NotAvailableToLoadboards;

            operationsAddressInputOrigin.LoadAddress(loadOrder.Origin ?? new LoadOrderLocation());
            operationsAddressInputDestination.LoadAddress(loadOrder.Destination ?? new LoadOrderLocation());

            DisplayServicesAndEquipmentTypes(loadOrder);

            //Add. Details
            txtGeneralBolComments.Text = loadOrder.GeneralBolComments;
            txtCriticalBolComments.Text = loadOrder.CriticalBolComments;



            chkHazardousMaterial.Checked = loadOrder.HazardousMaterial;
            txtHazardousContactName.Text = loadOrder.HazardousMaterialContactName;
            txtHazardousContactPhone.Text = loadOrder.HazardousMaterialContactPhone;
            txtHazardousContactMobile.Text = loadOrder.HazardousMaterialContactMobile;
            txtHazardousContactEmail.Text = loadOrder.HazardousMaterialContactEmail;

            DisplayLoadOrderCoordinator(loadOrder.LoadOrderCoordinator);
            DisplayCarrierCoordinator(loadOrder.CarrierCoordinator);

            //Items
            var nItems = loadOrder.Items
                                  .Select(i => new
                                  {
                                      i.Id,
                                      i.Pickup,
                                      Delivery = i.Delivery == (loadOrder.Stops.Count + 1) ? ViewDeliveryStopOrder : i.Delivery,
                                      i.Weight,
                                      i.Length,
                                      i.Width,
                                      i.Height,
                                      i.Quantity,
                                      i.PieceCount,
                                      i.Description,
                                      i.Comment,
                                      i.FreightClass,
                                      i.Value,
                                      i.IsStackable,
                                      i.HazardousMaterial,
                                      i.PackageTypeId,
                                      i.NMFCCode,
                                      i.HTSCode
                                  })
                                  .OrderBy(i => i.Pickup).ThenBy(i => i.Delivery)
                                  .ToList();

            lstItems.DataSource = nItems;
            lstItems.DataBind();
            tabItems.HeaderText = nItems.BuildTabCount(ItemsHeader);

            //References
            var customerReferencesFromList = lstReferences.Items
                                                          .Select(control => new
                                                          {
                                                              Id =
                                                                                 control.FindControl("hidReferenceId")
                                                                                        .ToCustomHiddenField()
                                                                                        .Value.ToLong(),
                                                              Name = control.FindControl("txtName").ToTextBox().Text,
                                                              Value = control.FindControl("txtValue").ToTextBox().Text,
                                                              DisplayOnOrigin =
                                                                                 control.FindControl("chkDisplayOnOrigin")
                                                                                        .ToAltUniformCheckBox()
                                                                                        .Checked,
                                                              DisplayOnDestination =
                                                                                 control.FindControl("chkDisplayOnDestination")
                                                                                        .ToAltUniformCheckBox()
                                                                                        .Checked,
                                                              Required =
                                                                                 control.FindControl("hidRequired")
                                                                                        .ToCustomHiddenField()
                                                                                        .Value.ToBoolean()
                                                          })
                                                          .ToList();

            var references = loadOrder.CustomerReferences
                                      .Select(
                                          control =>
                                          new
                                          {
                                              control.Id,
                                              control.Name,
                                              control.Value,
                                              control.DisplayOnOrigin,
                                              control.DisplayOnDestination,
                                              control.Required
                                          })
                                      .ToList();

            var newReferences = customerReferencesFromList.Where(c => references.All(b => b.Name != c.Name)).ToList();

            newReferences.AddRange(references);
            var referencesToBind = isNew ? references : newReferences;
            lstReferences.DataSource = referencesToBind;
            lstReferences.DataBind();
            litReferenceCount.Text = referencesToBind.BuildTabCount();

            //Vendors
            var pv = loadOrder.Vendors.FirstOrDefault(v => v.Primary);
            txtPrimaryVendor.Text = pv != null
                                        ? string.Format("{0} - {1}", pv.Vendor.VendorNumber, pv.Vendor.Name)
                                        : string.Empty;
            var vendors = loadOrder.Vendors
                                   .Where(v => !v.Primary)
                                   .Select(shv => new
                                   {
                                       shv.Id,
                                       shv.Vendor.VendorNumber,
                                       shv.Vendor.Name,
                                       shv.VendorId,
                                       shv.ProNumber,
                                       shv.Primary,
                                       InsuranceAlert =
                                                      shv.Vendor.AlertVendorInsurance(
                                                          ProcessorVars.VendorInsuranceAlertThresholdDays[shv.TenantId]),
                                       InsuranceTooltip =
                                                      shv.Vendor.VendorInsuranceAlertText(
                                                          ProcessorVars.VendorInsuranceAlertThresholdDays[shv.TenantId])
                                   })
                                   .ToList();
            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            litVendorsCount.Text = vendors.BuildTabCount();

            //Charges
            chkDeclineInsurance.Checked = loadOrder.DeclineInsurance;
            var amountPaidToLoadOrder = loadOrder.GetAmountPaidToLoadOrderViaMiscReceipts();
            txtCollectedPaymentAmount.Text = amountPaidToLoadOrder.GetString();
            hidMiscReceiptsRelatedToLoadOrderExist.Value = (amountPaidToLoadOrder > 0).ToString();

            lstCharges.DataSource = loadOrder.Charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = loadOrder.Charges.BuildTabCount(ChargesHeader);

            var totalDue = loadOrder.Charges.Sum(c => c.AmountDue);
            hidLoadedShipmentTotalDue.Value = totalDue.ToString();
            chargeStatistics.LoadCharges(loadOrder.Charges, hidResellerAdditionId.Value.ToLong());

            // sales presentative details
            var commissionPercent = loadOrder.SalesRepresentative != null
                                        ? loadOrder.SalesRepresentativeCommissionPercent.ToString("f4")
                                        : string.Empty;
            var floorValue = loadOrder.SalesRepresentative != null
                                 ? loadOrder.SalesRepMinCommValue.ToString("f4")
                                 : string.Empty;
            var ceilingValue = loadOrder.SalesRepresentative != null
                                   ? loadOrder.SalesRepMaxCommValue.ToString("f4")
                                   : string.Empty;
            var addlEntityName = loadOrder.SalesRepresentative != null ? loadOrder.SalesRepAddlEntityName : string.Empty;
            var addlEntityCommPercent = loadOrder.SalesRepresentative != null
                                            ? loadOrder.SalesRepAddlEntityCommPercent.ToString("f4")
                                            : string.Empty;
            DisplaySalesRepresentative(loadOrder.SalesRepresentative, commissionPercent, floorValue, ceilingValue,
                                       addlEntityName, addlEntityCommPercent);

            //Notes
            var notes = loadOrder.Notes.Select(i => new OperationsViewNoteObj(i)).OrderByDescending(i => i.Id).ToList();
            var notesToBind = ActiveUser.HasAccessTo(ViewCode.CanSeeArchivedNotes)
                                  ? notes
                                  : notes.Where(n => !n.Archived).ToList();
            peNotes.LoadData(notesToBind);
            tabNotes.HeaderText = notesToBind.BuildTabCount(NotesHeader);


            //Sort stop contacts with primary being first
            foreach (var stop in loadOrder.Stops)
                stop.Contacts = stop.Contacts.OrderByDescending(t => t.Primary).ToList();

            lstStops.DataSource = loadOrder.Stops.OrderBy(t => t.StopOrder).ToList();
            lstStops.DataBind();
            tabStops.HeaderText = loadOrder.Stops.BuildTabCount(StopsHeader);

            //Account Bucket
            var accountBuckets = loadOrder.AccountBuckets
                                          .Where(ab => !ab.Primary)
                                          .Select(
                                              ab =>
                                              new
                                              {
                                                  ab.Id,
                                                  ab.AccountBucketId,
                                                  AccountBucketCode = ab.AccountBucket.Code,
                                                  AccountBucketDescription = ab.AccountBucket.Description
                                              })
                                          .ToList();
            lstAccountBuckets.DataSource = accountBuckets;
            lstAccountBuckets.DataBind();
            litAccountBucketCount.Text = accountBuckets.BuildTabCount();

            //Documents
            lstDocuments.DataSource = loadOrder.Documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = loadOrder.Documents.BuildTabCount(DocumentsHeader);

            memberToolBar.ShowUnlock = loadOrder.HasUserLock(ActiveUser, loadOrder.Id);

            //DAT

            var datPosting = !loadOrder.IsNew
                                 ? new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(
                                     loadOrder.LoadOrderNumber, loadOrder.TenantId)
                                 : null;

            memberToolBar.ShowDelete = Access.Remove && (ActiveUser.HasAccessTo(ViewCode.DatLoadboard) || datPosting == null);
            pnlItems.Enabled = datPosting == null;

            // we cannot change any of these in a DAT Asset Update request, so disallow the user from changing them
            btnEditEquipment.Enabled = datPosting == null;
            pnlOriginLocation.Enabled = datPosting == null;
            pnlDestinationLocation.Enabled = datPosting == null;
            pnlHazMat.Enabled = datPosting == null;

            txtDatAssetId.Text = datPosting != null ? datPosting.AssetId : string.Empty;

            pnlDATAssetPosted.Visible = datPosting != null;
            pnlDATAssetExpired.Visible = datPosting != null && DateTime.Now >= datPosting.ExpirationDate;
	        pnlGuaranteedDeliveryServices.Visible =
		        loadOrder.Services.Any(s => s.ServiceId == ActiveUser.Tenant.GuaranteedDeliveryServiceId);

            btnModifyDatPosting.Text = datPosting != null ? "Modify DAT Posting" : "Post to DAT";

            hidDatAssetId.Value = datPosting != null ? datPosting.Id.ToString() : default(long).ToString();

            var truckLoadBidExists = hidLoadOrderId.Value.ToLong().CheckIfTrcuckloadBidExist(ActiveUser.TenantId) != null;
            pnlTruckloadBid.Visible = truckLoadBidExists;
            ddlServiceMode.Enabled = !truckLoadBidExists;
            txtVendorNumber.Enabled = !truckLoadBidExists;
            imgVendorSearch.Enabled = !truckLoadBidExists;
            ibtnLogVendorRejection.Enabled = !truckLoadBidExists;
            txtCustomerNumber.Enabled = !truckLoadBidExists;
            imgCustomerSearch.Enabled = !truckLoadBidExists;

            // Job
            var job = new Job(loadOrder.JobId);

            if (job.IsNew)
            {
                hidJobId.Value = string.Empty;
                txtJobNumber.Text = string.Empty;
                txtJobStep.Text = string.Empty;
            }
            else
            {
                hidJobId.Value = job.Id.ToString();
                txtJobNumber.Text = job.JobNumber;
                txtJobStep.Text = loadOrder.JobStep.ToString();
            }
        }

        private void UpdateEdiStatus(string vendorNumber, string shipmentNumber, bool vendorCanSendLoadTender)
        {
            if (vendorCanSendLoadTender)
            {
                var ltDto = new XmlConnectSearch()
                    .FetchOutBoundLoadTenderByVendShipIdNumAndDocType(vendorNumber, shipmentNumber, ActiveUser.TenantId, ActiveUser.Id);



                if (ltDto == null)
                {
                    txtEdiStatus.Text = WebApplicationConstants.EdiStatusNoStatus;
                }
                else
                {
                    var load = ltDto.TenderRecord.Xml
                                      .FromXml<Edi204>().Loads
                                      .First(l => l.BeginningSegment.ShipmentIdNumber == shipmentNumber);

                    if (ltDto.TenderRecordExpired && ltDto.TenderResponse == null)
                        txtEdiStatus.Text = WebApplicationConstants.EdiStatus204Expired;
                    else if (load.SetPurpose.TransactionSetPurpose == SetPurposeTransType.CA)
                        txtEdiStatus.Text = WebApplicationConstants.EdiStatus204Cancelled;
                    else if (ltDto.TenderResponse == null)
                        txtEdiStatus.Text = WebApplicationConstants.EdiStatus204Sent;
                    else if (ltDto.TenderResponse != null && ltDto.TenderResponse.Status == XmlConnectStatus.Accepted)
                        txtEdiStatus.Text = WebApplicationConstants.EdiStatus204Accepted;
                    else if (ltDto.TenderResponse != null && ltDto.TenderResponse.Status == XmlConnectStatus.Rejected)
                        txtEdiStatus.Text = WebApplicationConstants.EdiStatus204Rejected;

                }
            }
            else
            {
                txtEdiStatus.Text = WebApplicationConstants.EdiStatusNotApplicable;
            }

            hidEdiStatus.Value = txtEdiStatus.Text;
        }

        private void DisplayServicesAndEquipmentTypes(LoadOrder loadOrder)
        {
            DisplayServices(loadOrder.Services.Select(bs => bs.ServiceId));
            DisplayEquipments(loadOrder.Equipments.Select(be => be.EquipmentTypeId));
        }

        private void DisplayServices(IEnumerable<long> serviceIds)
        {
            hidServices.Value = string.Empty;

            foreach (var item in rptServices.Items.Cast<RepeaterItem>())
            {
                var check = serviceIds.Contains(item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong());
                item.FindControl("chkSelected").ToCheckBox().Checked = check;
                if (check) hidServices.Value += string.Format("{0}{1}", item.FindControl("lblServiceCode").ToLabel().Text, WebApplicationConstants.HtmlBreak);
            }

            lblServices.Text = hidServices.Value;
            hidServices.Value = hidServices.Value.StripTags();
        }

        private void DisplayEquipments(IEnumerable<long> equipmentTypeIds)
        {
            hidEquipment.Value = string.Empty;

            foreach (var item in rptEquipmentTypes.Items.Cast<RepeaterItem>())
            {
                var check = equipmentTypeIds.Contains(item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong());
                item.FindControl("chkSelected").ToCheckBox().Checked = check;
                if (check) hidEquipment.Value += string.Format("{0}{1}", item.FindControl("lblEquipmentTypeCode").ToLabel().Text, WebApplicationConstants.HtmlBreak);
            }

            lblEquipment.Text = hidEquipment.Value;
            hidEquipment.Value = hidEquipment.Value.StripTags();
        }

        private void LoadCustomerCustomFields(Customer customer)
        {
            var loadOrderReferences = lstReferences.Items
                .Select(control => new
                {
                    Id = control.FindControl("hidReferenceId").ToCustomHiddenField().Value.ToLong(),
                    Name = control.FindControl("txtName").ToTextBox().Text,
                    Value = control.FindControl("txtValue").ToTextBox().Text,
                    DisplayOnOrigin = control.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
                    DisplayOnDestination = control.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
                    Required = control.FindControl("hidRequired").ToCustomHiddenField().Value.ToBoolean()
                })
                .ToList();


            var customFieldsFromCustomer = customer.CustomFields.Where(c => loadOrderReferences.All(b => b.Name != c.Name)).ToList();

            var customerReferences = customFieldsFromCustomer
                .Select(r => new
                {
                    Id = default(long),
                    r.Name,
                    Value = string.Empty,
                    r.DisplayOnOrigin,
                    r.DisplayOnDestination,
                    r.Required
                })
                .ToList();

            loadOrderReferences.AddRange(customerReferences);

            lstReferences.DataSource = loadOrderReferences;
            lstReferences.DataBind();
            litReferenceCount.Text = loadOrderReferences.BuildTabCount();
        }

        private void ProcessTransferredRequest(LoadOrder loadOrder, bool disableEdit = false)
        {
            if (loadOrder == null) return;

            if (loadOrder.IsNew)
            {
                loadOrder.Customer = new Customer();
                loadOrder.DateCreated = DateTime.Now;
                loadOrder.EstimatedDeliveryDate = DateTime.Now.AddDays(1);
                loadOrder.DesiredPickupDate = DateTime.Now;
                loadOrder.EarlyPickup = TimeUtility.DefaultOpen;
                loadOrder.EarlyDelivery = TimeUtility.DefaultClose;
                loadOrder.LatePickup = TimeUtility.DefaultOpen;
                loadOrder.LateDelivery = TimeUtility.DefaultClose;
                loadOrder.Origin = new LoadOrderLocation();
                loadOrder.Destination = new LoadOrderLocation();
                loadOrder.Status = LoadOrderStatus.Offered;
                loadOrder.MileageSourceId = default(long);
            }
            else if (!ActiveUser.HasAccessToCustomer(loadOrder.Customer.Id)) return;

            LoadLoadOrder(loadOrder);

            if (Lock != null && !loadOrder.IsNew && Access.Modify && !disableEdit)
            {
                SetEditStatus(true);
                Lock(this, new ViewEventArgs<LoadOrder>(loadOrder));
            }
            else if (loadOrder.IsNew) SetEditStatus(true);
            else SetEditStatus(false);

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<LoadOrder>(loadOrder));
        }

        private void DuplicateLoadOrder(bool roundTrip)
        {
            var loadOrder = UpdateLoadOrder().Duplicate(roundTrip);

            LoadLoadOrder(loadOrder);

            // account for sales rep & reseller addition on customer if need be.  Reload customer this way will force refresh on sales rep & reseller additions
            if (loadOrder.Customer.Active)
                DisplayCustomer(loadOrder.Customer, true);
            else
            {
                DisplayCustomer(null, true);
                DisplayMessages(new[] { ValidationMessage.Error("Customer removed as account is not active!") });
            }

            SetEditStatus(true);

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
        }

        private void ReturnToShipmentDashboard()
        {
            var oldLoad = new LoadOrder(hidLoadOrderId.Value.ToLong(), false);

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<LoadOrder>(oldLoad));

            Response.Redirect(ShipmentDashboardView.PageAddress);
        }

        private void LoadVendorLaneBuySellData()
        {
            var origin = new Location();
            operationsAddressInputOrigin.RetrieveAddress(origin);
            var destination = new Location();
            operationsAddressInputDestination.RetrieveAddress(destination);

            var criteria = new LaneHistorySearchCriteria
            {
                DestinationCountryId = destination.CountryId,
                DestinationPostalCode = destination.PostalCode,
                OriginCountryId = origin.CountryId,
                OriginPostalCode = origin.PostalCode,
                DesiredPickupDate = txtDesiredPickupDate.Text.ToDateTime(),
                TotalWeight = lstItems.Items.Sum(control => control.FindControl("txtWeight").ToTextBox().Text.ToDecimal())
            };
            vendorLaneHistoryBuySell.LoadVendorLaneHistory(criteria);
            vendorLaneHistoryBuySell.Visible = true;
        }

        private void ConvertLoadOrderToShipment()
        {
            var loadOrder = UpdateLoadOrder();
            loadOrder.Status = LoadOrderStatus.Shipment;

            if (!CustomerCreditCheckOkay(loadOrder)) return;

            var shipment = loadOrder.ToShipment();
            shipment.User = ActiveUser;

            if (SaveConvertToShipment != null)
                SaveConvertToShipment(this, new ViewEventArgs<LoadOrder, Shipment>(loadOrder, shipment));

            // variable will be set in when handler calls finalizeConvertToShipment
            if (_transferToShipmentPage) Response.Redirect(ShipmentView.PageAddress);
        }

        private void GenerateRouteSummary()
        {
            var shipment = UpdateLoadOrder().ToShipment();
            documentDisplay.Title = string.Format("{0} RouteSummary", shipment.ShipmentNumber);
            documentDisplay.DisplayHtml(this.GenerateShipmentRouteSummary(shipment));
            documentDisplay.Visible = true;
        }

        private void GenerateQuote()
        {
            var load = UpdateLoadOrder();
            documentDisplay.Title = "Quote";
            documentDisplay.DisplayHtml(this.GenerateQuote(load));
            documentDisplay.Visible = true;
        }

        private void LoadVendorRateAgreementSelection()
        {
            var loadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong());

            var vendors = pnlAdditionalDetails.Enabled
                            ? lstVendors.Items
                                .Select(i => new
                                {
                                    Id = i.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                                    Name = i.FindControl("txtName").ToTextBox().Text
                                })
                                .ToDictionary(i => i.Id, i => i.Name)
                            : loadOrder.Vendors
                                .Where(sv => !sv.Primary)
                                .Select(i => new { Id = i.VendorId, i.Vendor.Name })
                                .ToDictionary(i => i.Id, i => i.Name);

            var pvendor = loadOrder.Vendors.FirstOrDefault(sv => sv.Primary);
            if (pvendor != null && !vendors.ContainsKey(pvendor.VendorId)) vendors.Add(pvendor.Vendor.Id, pvendor.Vendor.Name);

            var pv = loadOrder.Vendors.FirstOrDefault(sv => sv.Primary);
            var key = hidEditStatus.Value.ToBoolean()	// IE hack as ID will not render hidden field if not enabled!
                        ? hidPrimaryVendorId.Value.ToLong()
                        : pv == null ? default(long) : pv.VendorId;

            if (!vendors.ContainsKey(key) && key != default(long))
                vendors.Add(key, txtVendorName.Text);

            if (!vendors.Any())
            {
                DisplayMessages(new[] { ValidationMessage.Error("load order must have at least one vendor to generate a carrier agreement") });
                return;
            }

            // NOTE: IE hack to ensure that when in readonly mode charge code id is loaded with charge codes
            // IE will not render hidden field with value if in disabled panel
            var charges = pnlCharges.Enabled
                            ? lstCharges.Items
                                .Select(i => new Charge
                                {
                                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong()
                                })
                                .ToList()
                            : loadOrder.Charges.Cast<Charge>().ToList();



            if (vendors.Count > 1 || (pvendor == null && vendors.Any()))
            {
                vendorRateAgreement.LoadData(vendors, charges);
                vendorRateAgreement.Visible = true;
            }
            else
            {
                GenerateVendorRateAgreement(true);
            }
        }

        private void LoadShipmentLabelGenerator()
        {
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(txtLoadOrderNumber.Text, ActiveUser.TenantId);
            if (shipment == null)
            {
                DisplayMessages(new[] { ValidationMessage.Error(MissingShipmentRecErrMsg) });
                return;
            }

            shippingLabelGenerator.GenerateLabels(shipment.Id);
            shippingLabelGenerator.Visible = true;
        }

        private void GenerateBol()
        {
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(txtLoadOrderNumber.Text, ActiveUser.TenantId);
            if (shipment == null)
            {
                DisplayMessages(new[] { ValidationMessage.Error(MissingShipmentRecErrMsg) });
                return;
            }

            documentDisplay.Title = string.Format("{0} Bill of Lading", shipment.ShipmentNumber);
            documentDisplay.DisplayHtml(this.GenerateBOL(shipment));
            documentDisplay.Visible = true;
        }

        private void GotoToShipment()
        {
            var load = new LoadOrder(hidLoadOrderId.Value.ToLong());
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(load.LoadOrderNumber, ActiveUser.TenantId);

            if (shipment == null)
            {
                messageBox.Icon = MessageIcon.Question;
                messageBox.Message = @"Shipment record not found.  Record could have been deleted by another user. Do you want to create new shipment from current load record?";
                messageBox.Button = MessageButton.YesNo;
                messageBox.Visible = true;
                return;
            }

            if (hidEditStatus.Value.ToBoolean())
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<LoadOrder>(load));

            Session[WebApplicationConstants.TransferShipmentId] = shipment.Id;
            Response.Redirect(ShipmentView.PageAddress);
        }

        private void DisplayLoadTenderToBeSent(SetPurposeTransType setPurpose)
        {
            var loadOrder = UpdateLoadOrder();

            var primaryVendor = loadOrder.Vendors.First(v => v.Primary).Vendor;

            Edi204 edi204;
            _emailTender = (bool)PageStore["email"];
            switch (setPurpose)
            {
                case SetPurposeTransType.CA:
                    edi204 = loadOrder.ToEdi204ForCancellation(string.Empty);
                    hidLoadTenderDisplayFlag.Value = _emailTender ? CancelLoadTenderEmailFlag : CancelLoadTenderFlag;

                    break;
                case SetPurposeTransType.OR:
                    edi204 = loadOrder.ToEdi204(string.Empty);
                    hidLoadTenderDisplayFlag.Value = _emailTender ? SendLoadTenderNewEmailFlag : SendLoadTenderNewFlag;
                    break;
                case SetPurposeTransType.UP:
                    edi204 = loadOrder.ToEdi204ForUpdate(string.Empty);
                    hidLoadTenderDisplayFlag.Value = _emailTender ? SendLoadTenderUpdateEmailFlag : SendLoadTenderUpdateFlag;
                    break;
                default:
                    throw new NotSupportedException("Invalid Set Purpose Transaction Type");
            }

            contentDiv.InnerHtml = Server
                .GenerateLoadTender(edi204.Loads[0] ?? new Load(), primaryVendor.Name, Direction.Outbound)
                .RetrieveWrapperSection(BaseFields.BodyWrapperStart, BaseFields.BodyWrapperEnd);
            litTitle.Text = "Process Load Tender";

            pnlLoadTenderDisplay.Visible = true;
            pnlDimScreen.Visible = true;


            var isBidClosed =
               new TruckloadBidSearch().FetchTruckloadBidByLoadOrderIdAndStatus(hidLoadOrderId.Value.ToLong(),
                                                                                TruckloadBidStatus.Closed.ToInt(),
                                                                                ActiveUser.TenantId) != null;
            chkTruckLoadBid.Visible = IsPrimaryVendorPreferredVendor(loadOrder) && !isBidClosed &&
                                      (hidLoadTenderDisplayFlag.Value == (SendLoadTenderNewFlag));
        }

        private void AddEdiFlagToVendorId(long vendorId, string flag)
        {
            if (StoredEdiMessageQueue.ContainsKey(vendorId))
            {
                // negating new send with cancel
                if (StoredEdiMessageQueue[vendorId] == SendLoadTenderNewFlag && flag == CancelLoadTenderFlag)
                {
                    StoredEdiMessageQueue.Remove(vendorId);
                }
                else
                {
                    StoredEdiMessageQueue[vendorId] = flag;
                }
            }
            else
            {
                StoredEdiMessageQueue.Add(vendorId, flag);
            }
        }

        private IEnumerable<ValidationMessage> ProcessLoadTenderMessageQueue(LoadOrder loadOrder)
        {
            var truckloadBidMsgs = new List<ValidationMessage>();

            foreach (var entry in StoredEdiMessageQueue)
            {
                var truckloadBidExpirationTime = string.Empty;
                var emailPurpose = string.Empty;
                var controlNumber =
                    new AutoNumberProcessor().NextNumber(AutoNumberCode.EdiTransactionSetControlNumber, ActiveUser)
                                             .GetString();
                Edi204 edi204 = null;
                var vendor = new Vendor(entry.Key);

                //must have primary vendor be the one which we are sending the communication to
                loadOrder.Vendors = new List<LoadOrderVendor> { new LoadOrderVendor { VendorId = vendor.Id, Primary = true } };
                var bid = hidLoadOrderId.Value.ToLong().CheckIfTrcuckloadBidExist(ActiveUser.TenantId);
                switch (entry.Value)
                {
                    case CancelLoadTenderFlag:

                        if (bid != null)
                        {
                            var errorClosingBid = CloseTruckLoadBid(bid);
                            truckloadBidMsgs.AddRange(errorClosingBid);
                            if (errorClosingBid.Any()) continue;
                            pnlTruckloadBid.Visible = false;
                        }
                        edi204 = loadOrder.ToEdi204ForCancellation(controlNumber);
                        break;

                    case SendLoadTenderNewFlag:
                        if (chkTruckLoadBid.Checked)
                        {
                            truckloadBidMsgs = InitiateTruckloadBid();
                            var checkBid = hidLoadOrderId.Value.ToLong().CheckIfTrcuckloadBidExist(ActiveUser.TenantId);
                            if (checkBid == null) continue;
                            pnlTruckloadBid.Visible = true;
                            truckloadBidExpirationTime = checkBid.ExpirationTime1.ToString();
                        }
                        edi204 = loadOrder.ToEdi204(controlNumber, truckloadBidExpirationTime);
                        break;

                    case SendLoadTenderUpdateFlag:
                        if (bid != null) truckloadBidExpirationTime = UpdateTruckLoadBid(bid);
                        edi204 = loadOrder.ToEdi204ForUpdate(controlNumber, truckloadBidExpirationTime);
                        break;

                    case SendLoadTenderNewEmailFlag:

                        truckloadBidMsgs = InitiateTruckloadBid();
                        var cBid = hidLoadOrderId.Value.ToLong().CheckIfTrcuckloadBidExist(ActiveUser.TenantId);
                        if (cBid == null) continue;
                        pnlTruckloadBid.Visible = true;
                        truckloadBidExpirationTime = cBid.ExpirationTime1.ToString();
                        emailPurpose = SetPurposeTransType.OR.ToString();
                        edi204 = loadOrder.ToEdi204(controlNumber, truckloadBidExpirationTime);
                        break;

                    case SendLoadTenderUpdateEmailFlag:
                        if (bid != null) truckloadBidExpirationTime = UpdateTruckLoadBid(bid);
                        emailPurpose = SetPurposeTransType.UP.ToString();
                        edi204 = loadOrder.ToEdi204ForUpdate(controlNumber, truckloadBidExpirationTime);
                        break;

                    case CancelLoadTenderEmailFlag:

                        if (bid != null)
                        {
                            var errorClosingBid = CloseTruckLoadBid(bid);
                            truckloadBidMsgs.AddRange(errorClosingBid);
                            if (errorClosingBid.Any()) continue;
                            pnlTruckloadBid.Visible = false;
                            emailPurpose = SetPurposeTransType.CA.ToString();
                        }
                        edi204 = loadOrder.ToEdi204ForCancellation(controlNumber);

                        break;
                }


                if (vendor.CanSendLoadTender(loadOrder.Status))
                    this.SendLoadTender(edi204, vendor.Communication, truckloadBidExpirationTime);
                else
                {
                    Server.NotifyLoadTenderByEmail(edi204.Loads[0], loadOrder.Customer, ActiveUser, vendor, emailPurpose);
                }
            }
            StoredEdiMessageQueue = null;
            return truckloadBidMsgs;
        }

        private void GenerateVendorRateAgreement(bool ignoreVendorRateAgreementSelector)
        {
            var loadOrder = pnlCharges.Enabled ? UpdateLoadOrder() : new LoadOrder(hidLoadOrderId.Value.ToLong());

            if (loadOrder.ServiceMode == ServiceMode.NotApplicable)
            {
                DisplayMessages(new[] { ValidationMessage.Error("load order must have a valid service mode to generate a carrier agreement") });
                return;
            }

            var filterForSingleVendor = loadOrder.Vendors.Count > 1 || (loadOrder.Vendors.Any() && !loadOrder.Vendors.Any(v => v.Primary));
            if (!ignoreVendorRateAgreementSelector && filterForSingleVendor)
            {
                loadOrder.Vendors.RemoveAll(v => v.VendorId != vendorRateAgreement.RetrieveVendorId());
                loadOrder.Vendors[0].Primary = true; //sets only vendor as primary (for processor)

                var ids = (from idx in vendorRateAgreement.RetrieveSelectedIndices()
                           where idx < loadOrder.Charges.Count
                           select loadOrder.Charges[idx].Id).ToList();
                loadOrder.Charges.RemoveAll(c => !ids.Contains(c.Id));

                loadOrder.Charges = loadOrder.Charges.Select(c => new LoadOrderCharge
                {
                    Quantity = c.Quantity,
                    UnitBuy = c.UnitBuy,
                    UnitSell = c.UnitSell,
                    UnitDiscount = c.UnitDiscount,
                    Comment = c.Comment,
                    ChargeCodeId = c.ChargeCodeId,
                    LoadOrder = loadOrder,
                    TenantId = loadOrder.TenantId
                }).ToList();

                vendorRateAgreement.Visible = false;
            }

            // for generate carrier agreement method
            var shipment = loadOrder.ToShipment();
            shipment.User = ActiveUser;

            documentDisplay.Title = string.Format("{0} Carrier Agreement", loadOrder.LoadOrderNumber);
            documentDisplay.DisplayHtml(this.GenerateCarrierAgreement(shipment));
            documentDisplay.Visible = true;
        }

        private void ConfigureServiceModeDisplay(ServiceMode serviceMode)
        {
            var show = serviceMode == ServiceMode.Truckload || serviceMode == ServiceMode.Air;
            pnlDelivery.Visible = show;
            pnlPartialTruckload.Visible = show;
            tabStops.Visible = show;
            ddlEarlyDelivery.SelectedValue = TimeUtility.DefaultOpen; // 08:00
            ddlLateDelivery.SelectedValue = TimeUtility.DefaultClose; // 17:00
        }

        private bool CustomerCreditCheckOkay(LoadOrder loadOrder)
        {
            var charges = loadOrder.Charges.Sum(c => c.AmountDue);
            var outstanding = hidCustomerOutstandingBalance.Value.ToDecimal() - hidLoadedShipmentTotalDue.Value.ToDecimal();
            var credit = (loadOrder.Customer ?? new Customer()).CreditLimit - outstanding;

            if (charges > credit && !ActiveUser.HasAccessTo(ViewCode.BypassCustomerCreditStop))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Insufficient customer credit [{0:c2}] to cover order total [{1:c2}]", credit, charges) });
                return false;
            }

            return true;
        }


        private List<ValidationMessage> InitiateTruckloadBid()
        {
            var truckloadBidMsgs = new List<ValidationMessage>();
            var loadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong());

            var profile =
                new CustomerTLTenderProfileSearch().FetchCustomerTenderingProfileByCustomerId(loadOrder.Customer.Id,
                                                                                              ActiveUser.TenantId);
            var lane = profile != null ? profile.RetrieveMatchingLane(loadOrder) : null;

            var bid = new TruckloadBid
            {
                LoadOrder = loadOrder,
                TruckloadTenderingProfile = profile,
                BidStatus = TruckloadBidStatus.Processing,
                TenantId = ActiveUser.TenantId,
                TruckloadTenderingProfileLane = lane,
                FirstPreferredVendor = lane != null ? lane.FirstPreferredVendor : null,
                SecondPreferredVendor = lane != null ? lane.SecondPreferredVendor : null,
                ThirdPreferredVendor = lane != null ? lane.ThirdPreferredVendor : null,
                DateCreated = DateTime.Now,
                TimeLoadTendered1 = DateTime.Now,
                ExpirationTime1 = profile != null ? DateTime.Now.AddMinutes(profile.MaxWaitTime) : DateUtility.SystemEarliestDateTime,
                TimeLoadTendered2 = DateUtility.SystemEarliestDateTime,
                TimeLoadTendered3 = DateUtility.SystemEarliestDateTime,
                ExpirationTime2 = DateUtility.SystemEarliestDateTime,
                ExpirationTime3 = DateUtility.SystemEarliestDateTime,
                UserId = ActiveUser.Id,
            };

            var view = new TruckloadTenderingPrcessorImmitationView(ActiveUser, Server);
            view.SaveOrUpdateBid(bid, loadOrder);

            if (view.TruckloadBidMsgs.Any()) truckloadBidMsgs.AddRange(view.TruckloadBidMsgs);

            return truckloadBidMsgs;
        }

        private IEnumerable<ValidationMessage> CloseTruckLoadBid(TruckloadBid bid)
        {
            var truckloadBidMsgs = new List<ValidationMessage>();
            var view = new TruckloadTenderingPrcessorImmitationView(ActiveUser, Server);
            var truckloadBid = new TruckloadBid(bid.Id);
            view.LockTruckloadBid(truckloadBid);
            truckloadBid.BidStatus = TruckloadBidStatus.Closed;
            view.SaveOrUpdateBid(truckloadBid, truckloadBid.LoadOrder);
            view.UnlockTruckloadBid(truckloadBid);
            if (view.TruckloadBidMsgs.Any()) truckloadBidMsgs.AddRange(view.TruckloadBidMsgs);
            return truckloadBidMsgs;
        }

        private string UpdateTruckLoadBid(TruckloadBid bid)
        {
            var loadOrdeVendor = new LoadOrderVendor(hidPrimaryLoadOrderVendorId.Value.ToLong());
            var view = new TruckloadTenderingPrcessorImmitationView(ActiveUser, Server);
            var expirationTime = new DateTime();

            if (loadOrdeVendor.VendorId == bid.FirstPreferredVendorId)
            {
                bid.TimeLoadTendered1 = DateTime.Now;
                expirationTime = bid.ExpirationTime1 = DateTime.Now.AddMinutes(bid.TruckloadTenderingProfile.MaxWaitTime);
            }
            else if (hidPrimaryLoadOrderVendorId.Value.ToLong() == bid.SecondPreferredVendorId)
            {
                bid.TimeLoadTendered2 = DateTime.Now;
                expirationTime = bid.ExpirationTime2 = DateTime.Now.AddMinutes(bid.TruckloadTenderingProfile.MaxWaitTime);
            }
            else if (hidPrimaryLoadOrderVendorId.Value.ToLong() == bid.ThirdPreferredVendorId)
            {
                bid.TimeLoadTendered3 = DateTime.Now;
                expirationTime = bid.ExpirationTime3 = DateTime.Now.AddMinutes(bid.TruckloadTenderingProfile.MaxWaitTime);
            }
            view.SaveOrUpdateBid(bid, bid.LoadOrder);
            return expirationTime.ToString();
        }

        private bool IsPrimaryVendorPreferredVendor(LoadOrder loadOrder)
        {
            var tlProfile = new CustomerTLTenderProfileSearch().FetchCustomerTenderingProfileByCustomerId(loadOrder.Customer.Id,
                                                                                                          ActiveUser.TenantId);
            if (tlProfile != null)
            {
                var lane = tlProfile.RetrieveMatchingLane(loadOrder);
                var loadOrderVendor = new LoadOrderVendor(hidPrimaryLoadOrderVendorId.Value.ToLong());
                if (lane != null && (lane.FirstPreferredVendorId == loadOrderVendor.VendorId))
                {
                    return true;
                }
            }
            return false;

        }

	    private void CheckAndAddGuaranteedService(LoadOrder loadOrder)
	    {
		    if (ActiveUser.Tenant.GuaranteedDeliveryServiceId != default(long) &&
		        loadOrder.Services.All(s => s.ServiceId != ActiveUser.Tenant.GuaranteedDeliveryServiceId))
		    {
			    loadOrder.Services.Add(new LoadOrderService
			    {
				    TenantId = ActiveUser.TenantId,
				    LoadOrder = loadOrder,
				    ServiceId = ActiveUser.Tenant.GuaranteedDeliveryServiceId
			    });

		    }
	    }


        protected void Page_Load(object sender, EventArgs e)
        {
            new LoadOrderHandler(this).Initialize();
            new DatLoadboardAssetHandler(this).Initialize();
            new VendorRejectionLogProcHandler(this).Initialize();
            new TruckloadBidHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;

            lbtnOffered.CommandName = OfferedCmdName;
            lbtnQuoted.CommandName = QuotedCmdName;
            lbtnCancelled.CommandName = CancelledCmdName;
            lbtnCovered.CommandName = CoveredCmdName;
            lbtnLost.CommandName = LostCmdName;
            lbtnAccepted.CommandName = AcceptedCmdName;

            loadOrderFinder.OpenForEditEnabled = Access.Modify;
            _transferToShipmentPage = false;

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));

            SetPageTitle(txtLoadOrderNumber.Text);

            if (IsPostBack)
            {
                if (!string.IsNullOrEmpty(hidEquipment.Value)) lblEquipment.Text = hidEquipment.Value.ReplaceTags();
                if (!string.IsNullOrEmpty(hidServices.Value)) lblServices.Text = hidServices.Value.ReplaceTags();
                return;
            }

            pnlDat.Visible = ActiveUser.HasAccessTo(ViewCode.DatLoadboard) && ActiveUser.HasDatCredentials;

            pecPayment.PaymentGatewayType = ActiveUser.Tenant.PaymentGatewayType;

            // context key
            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });
            aceVendor.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });
            aceAccountBucketCode.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });
            acePrefixCode.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });
            aceLoadOrderCoordinator.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() }, { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() }, { AutoCompleteUtility.Keys.IsShipmentCoordinator, true.ToString() } });
            aceCarrierCoordinator.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() }, { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() }, { AutoCompleteUtility.Keys.IsCarrierCoordinator, true.ToString() } });

            BindTimeDropDowns();

            if (Loading != null)
                Loading(this, new EventArgs());

            SetEditStatus(false);

            if (Session[WebApplicationConstants.TransferLoadOrderId] != null)
            {
                var disableEdit = Session[WebApplicationConstants.DisableEditModeOnTransfer].ToBoolean();
                ProcessTransferredRequest(new LoadOrder(Session[WebApplicationConstants.TransferLoadOrderId].ToLong()), disableEdit);
                Session[WebApplicationConstants.TransferLoadOrderId] = null;
                Session[WebApplicationConstants.DisableEditModeOnTransfer] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new LoadOrderSearch().FetchLoadOrderByLoadNumber(Request.QueryString[WebApplicationConstants.TransferNumber].GetString(), ActiveUser.TenantId), true);

        }



        protected void OnLoadOrderFinderItemSelected(object sender, ViewEventArgs<LoadOrder> e)
        {
            if (hidLoadOrderId.Value.ToLong() != default(long))
            {
                var oldLoadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<LoadOrder>(oldLoadOrder));
            }

            var loadOrder = e.Argument;

            //empty out any edi flags from the page store
            StoredEdiMessageQueue = null;

            LoadLoadOrder(loadOrder);

            loadOrderFinder.Visible = false;

            if (loadOrderFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<LoadOrder>(loadOrder));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<LoadOrder>(loadOrder));

            litErrorMessages.Text = string.Empty;
        }

        protected void OnLoadOrderFinderItemCancelled(object sender, EventArgs e)
        {
            loadOrderFinder.Visible = false;
        }



        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            loadOrderFinder.Visible = true;
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var loadOrder = UpdateLoadOrder();

            if (!CustomerCreditCheckOkay(loadOrder)) return;

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<LoadOrder>(loadOrder));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<LoadOrder>(loadOrder));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var loadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong(), false);

            hidFlag.Value = DeleteFlag;

            if (Delete != null)
                Delete(this, new ViewEventArgs<LoadOrder>(loadOrder));

            loadOrderFinder.Reset();
        }

        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            operationsAddressInputOrigin.ClearFields();
            operationsAddressInputDestination.ClearFields();

            var loadOrder = new LoadOrder
            {
                Customer = new Customer(),
                DateCreated = DateTime.Now,
                Description = string.Empty,
                EstimatedDeliveryDate = DateTime.Now.AddDays(1),
                DesiredPickupDate = DateTime.Now,
                EarlyPickup = TimeUtility.DefaultOpen,
                EarlyDelivery = TimeUtility.DefaultOpen,
                LatePickup = TimeUtility.DefaultClose,
                LateDelivery = TimeUtility.DefaultClose,
                Origin = new LoadOrderLocation { CountryId = ActiveUser.Tenant.DefaultCountryId, AppointmentDateTime = DateUtility.SystemEarliestDateTime },
                Destination = new LoadOrderLocation { CountryId = ActiveUser.Tenant.DefaultCountryId, AppointmentDateTime = DateUtility.SystemEarliestDateTime },
                Status = LoadOrderStatus.Offered,
                MileageSourceId = default(long),
                ServiceMode = ServiceMode.NotApplicable
            };
            //If making new record, empty out message sending queue
            StoredEdiMessageQueue = null;

            LoadLoadOrder(loadOrder);

            lblEquipment.Text = string.Empty;
            lblServices.Text = string.Empty;

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);

            litErrorMessages.Text = string.Empty;
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var loadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong(), false);

            if (Lock == null || loadOrder.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<LoadOrder>(loadOrder));

            LoadLoadOrder(loadOrder);
            StoredEdiMessageQueue = null;
        }

        protected void OnToolbarUnlockClicked(object sender, EventArgs e)
        {
            var loadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong(), false);

            if (UnLock == null || loadOrder.IsNew) return;

            SetEditStatus(false);
            memberToolBar.ShowUnlock = false;
            UnLock(this, new ViewEventArgs<LoadOrder>(loadOrder));
        }

        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case DuplicateArgs:
                    DuplicateLoadOrder(false);
                    break;
                case DuplicateReverseArgs:
                    DuplicateLoadOrder(true);
                    break;
                case ReturnArgs:
                    ReturnToShipmentDashboard();
                    break;
                case VendorLaneHistoryArgs:
                    LoadVendorLaneBuySellData();
                    break;
                case BolArgs:
                    GenerateBol();
                    break;
                case VendorRateAgreementArgs:
                    LoadVendorRateAgreementSelection();
                    break;
                case QuoteArgs:
                    GenerateQuote();
                    break;
                case ShippingLabelArgs:
                    LoadShipmentLabelGenerator();
                    break;
                case RouteSummaryArgs:
                    GenerateRouteSummary();
                    break;
                case ConvertToShipmentArgs:
                    ConvertLoadOrderToShipment();
                    break;
                case GoToShipmentArgs:
                    GotoToShipment();
                    break;
                case SendLoadTenderNewArgs:
                    PageStore["email"] = false;
                    DisplayLoadTenderToBeSent(SetPurposeTransType.OR);
                    break;
                case SendLoadTenderUpdateArgs:
                    DisplayLoadTenderToBeSent(SetPurposeTransType.UP);
                    break;
                case CancelLoadTenderArgs:
                    DisplayLoadTenderToBeSent(SetPurposeTransType.CA);
                    break;
                case PayForLoadOrderWithCreditCardArgs:
                    pecPayment.Visible = true;
                    pecPayment.LoadLoadOrderToBePaidFor(UpdateLoadOrder());
                    break;
                case RefundCustomerPaymentsArgs:
                    prcRefundPayments.Visible = true;
                    prcRefundPayments.LoadLoadOrder(UpdateLoadOrder());
                    break;
                case ConvertShipmentToLoadOrderArgs:
                    PageStore["roundTrip"] = false;
                    shipmentFinder.Visible = true;
                    break;
                case ConvertShipmentToLoadOrderReverseArgs:
                    PageStore["roundTrip"] = true;
                    shipmentFinder.Visible = true;
                    break;
                case SendLoadTenderEmailNewArgs:
                    PageStore["email"] = true;
                    DisplayLoadTenderToBeSent(SetPurposeTransType.OR);
                    break;
                case CancelLoadTenderEmailNewArgs:
                    PageStore["email"] = true;
                    DisplayLoadTenderToBeSent(SetPurposeTransType.CA);
                    break;
                case UpdateLoadTenderEmailNewArgs:
                    PageStore["email"] = true;
                    DisplayLoadTenderToBeSent(SetPurposeTransType.UP);
                    break;

            }
        }



        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument, true);
            customerFinder.Visible = false;
        }



        protected void OnAccountBucketSearchClicked(object sender, ImageClickEventArgs e)
        {
            accountBucketFinder.Reset();
            accountBucketFinder.EnableMultiSelection = false;
            accountBucketFinder.Visible = true;
        }

        protected void OnAccountBucketFinderItemSelected(object sender, ViewEventArgs<AccountBucket> e)
        {
            DisplayAccountBucket(e.Argument);
            accountBucketFinder.Visible = false;
        }



        protected void OnPrefixSearchClicked(object sender, ImageClickEventArgs e)
        {
            prefixFinder.Visible = true;
        }

        protected void OnPrefixFinderSelectionCancelled(object sender, EventArgs e)
        {
            prefixFinder.Visible = false;
        }

        protected void OnPrefixFinderItemSelected(object sender, ViewEventArgs<Prefix> e)
        {
            DisplayPrefix(e.Argument);
            prefixFinder.Visible = false;
        }



        protected void OnVendorNumberEntered(object sender, EventArgs e)
        {
            hidFlag.Value = VendorUpdateFlag;

            if (VendorSearch != null)
                VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
        {
            vendorFinder.Reset();
            vendorFinder.EnableMultiSelection = false;
            vendorFinder.Visible = true;
        }

        protected void OnVendorFinderItemCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
        {
            hidFlag.Value = VendorUpdateFlag;
            DisplayVendor(e.Argument);
            vendorFinder.Visible = false;
        }



        protected void DisplayManageLoadOrderOnDatLoadboardClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ModifyingDatPostingFlag;
            datLoadboardAssetControl.LoadDatAssetForLoadOrder(hidLoadOrderId.Value.ToLong());
            datLoadboardAssetControl.Visible = true;
        }



        protected void OnAddItemClicked(object sender, EventArgs e)
        {
            var items = lstItems.Items
                .Select(control => new
                {
                    Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
                    Pickup = control.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
                    Delivery = control.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt(),
                    Weight = control.FindControl("txtWeight").ToTextBox().Text,
                    Length = control.FindControl("txtLength").ToTextBox().Text,
                    Width = control.FindControl("txtWidth").ToTextBox().Text,
                    Height = control.FindControl("txtHeight").ToTextBox().Text,
                    Quantity = control.FindControl("txtQuantity").ToTextBox().Text,
                    PieceCount = control.FindControl("txtPieceCount").ToTextBox().Text,
                    Description = control.FindControl("txtDescription").ToTextBox().Text,
                    Comment = control.FindControl("txtComment").ToTextBox().Text,
                    FreightClass = control.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                    Value = control.FindControl("txtItemValue").ToTextBox().Text,
                    IsStackable = control.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                    HazardousMaterial = control.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
                    PackageTypeId = control.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    NMFCCode = control.FindControl("txtNMFCCode").ToTextBox().Text,
                    HTSCode = control.FindControl("txtHTSCode").ToTextBox().Text,
                })
                .ToList();

            items.Add(new
            {
                Id = default(long),
                Pickup = default(int),
                Delivery = default(int),
                Weight = string.Empty,
                Length = string.Empty,
                Width = string.Empty,
                Height = string.Empty,
                Quantity = string.Empty,
                PieceCount = string.Empty,
                Description = string.Empty,
                Comment = string.Empty,
                FreightClass = default(double),
                Value = string.Empty,
                IsStackable = false,
                HazardousMaterial = false,
                PackageTypeId = default(long),
                NMFCCode = string.Empty,
                HTSCode = string.Empty,
            });

            lstItems.DataSource = items;
            lstItems.DataBind();
            tabItems.HeaderText = items.BuildTabCount(ItemsHeader);
            athtuTabUpdater.SetForUpdate(tabItems.ClientID, items.BuildTabCount(ItemsHeader));
            if (((Control)sender).ClientID == btnAddItemTabBottom.ClientID) btnAddItemTabBottom.Focus();
        }

        protected void OnDeleteItemClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var items = lstItems
                .Items
                .Select(control => new
                {
                    Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
                    Pickup = control.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
                    Delivery = control.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt(),
                    Weight = control.FindControl("txtWeight").ToTextBox().Text,
                    Length = control.FindControl("txtLength").ToTextBox().Text,
                    Width = control.FindControl("txtWidth").ToTextBox().Text,
                    Height = control.FindControl("txtHeight").ToTextBox().Text,
                    Quantity = control.FindControl("txtQuantity").ToTextBox().Text,
                    PieceCount = control.FindControl("txtPieceCount").ToTextBox().Text,
                    Description = control.FindControl("txtDescription").ToTextBox().Text,
                    Comment = control.FindControl("txtComment").ToTextBox().Text,
                    FreightClass = control.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                    Value = control.FindControl("txtItemValue").ToTextBox().Text,
                    IsStackable = control.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                    HazardousMaterial = control.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
                    PackageTypeId = control.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    NMFCCode = control.FindControl("txtNMFCCode").ToTextBox().Text,
                    HTSCode = control.FindControl("txtHTSCode").ToTextBox().Text,
                })
                .ToList();

            items.RemoveAt(imageButton.Parent.FindControl("hidItemIndex").ToCustomHiddenField().Value.ToInt());

            lstItems.DataSource = items;
            lstItems.DataBind();
            tabItems.HeaderText = items.BuildTabCount(ItemsHeader);
            athtuTabUpdater.SetForUpdate(tabItems.ClientID, items.BuildTabCount(ItemsHeader));
        }

        protected void OnClearItemsClicked(object sender, EventArgs e)
        {
            lstItems.DataSource = new List<object>();
            lstItems.DataBind();
            tabItems.HeaderText = new List<object>().BuildTabCount(ItemsHeader);
            athtuTabUpdater.SetForUpdate(tabItems.ClientID, new List<object>().BuildTabCount(ItemsHeader));
        }


        protected void OnLoadOrderItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var dataItem = item.DataItem;

            // Bind stops
            var ddlItemPickup = item.FindControl("ddlItemPickup").ToDropDownList();
            var ddlItemDelivery = item.FindControl("ddlItemDelivery").ToDropDownList();

            var shipment = new LoadOrder(hidLoadOrderId.Value.ToLong());

            var originStops = shipment
                .Stops
                .Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
                .ToList();
            originStops.Insert(0, new ViewListItem(OriginStopText, 0.ToString()));

            var destStops = shipment
                .Stops
                .Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
                .ToList();
            destStops.Add(new ViewListItem(DestinationStopText, ViewDeliveryStopOrder.ToString()));

            ddlItemPickup.DataSource = originStops;
            ddlItemPickup.DataBind();
            if (dataItem.HasGettableProperty("Pickup")) ddlItemPickup.SelectedValue = dataItem.GetPropertyValue("Pickup").GetString();


            ddlItemDelivery.DataSource = destStops;
            ddlItemDelivery.DataBind();
            if (dataItem.HasGettableProperty("Delivery")) ddlItemDelivery.SelectedValue = dataItem.GetPropertyValue("Delivery").GetString();

            // Bind freight classes
            var freightClasses = WebApplicationSettings.FreightClasses
                .Select(fc => new ViewListItem(fc.ToString(), fc.ToString()))
                .ToList();
            freightClasses.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicableShort, string.Empty));

            var ddlActualFreightClass = item.FindControl("ddlFreightClass").ToDropDownList();
            ddlActualFreightClass.DataSource = freightClasses;
            ddlActualFreightClass.DataBind();
            if (dataItem.HasGettableProperty("FreightClass")) ddlActualFreightClass.SelectedValue = dataItem.GetPropertyValue("FreightClass").GetString();

        }


        protected void OnSetPackageDimensionsClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var packageTypeId = imageButton.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong();

            if (packageTypeId == default(long)) return;

            var packageType = new PackageType(packageTypeId);

            // set dimensions, then recalculate density
            var txtLength = imageButton.FindControl("txtLength").ToTextBox();
            var txtWidth = imageButton.FindControl("txtWidth").ToTextBox();
            var txtHeight = imageButton.FindControl("txtHeight").ToTextBox();

            txtLength.Text = packageType.DefaultLength.ToString();
            txtWidth.Text = packageType.DefaultWidth.ToString();
            txtHeight.Text = packageType.DefaultHeight.ToString();

            var weight = imageButton.FindControl("txtWeight").ToTextBox().Text.ToDecimal();
            var length = txtLength.Text.ToDecimal();
            var width = txtWidth.Text.ToDecimal();
            var height = txtHeight.Text.ToDecimal();
            var qty = imageButton.FindControl("txtQuantity").ToTextBox().Text.ToDecimal();

            if (length != default(decimal) && width != default(decimal) && height != default(decimal) && qty != default(int))
            {
                imageButton.FindControl("txtEstimatedPcf").ToTextBox().Text = ((weight / (length.InToFt() * width.InToFt() * height.InToFt())) / qty).ToString("n2");
            }
            else
            {
                imageButton.FindControl("txtEstimatedPcf").ToTextBox().Text = string.Empty;
            }
        }


        protected void OnLoadOrderLocationServicesRequired(object sender, ViewEventArgs<List<AddressBookService>> e)
        {
            if (e.Argument.Count > 0) AddAddressBookServicesToViewServices(e.Argument);
        }

        private void AddAddressBookServicesToViewServices(IEnumerable<AddressBookService> services)
        {
            var serviceIds = services.Select(s => s.ServiceId).ToList();

            var viewIds = rptServices
                .Items
                .Cast<RepeaterItem>()
                .Where(item => item.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(item => item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong());

            foreach (var id in viewIds.Where(id => !serviceIds.Contains(id)))
                serviceIds.Add(id);

            DisplayServices(serviceIds);
        }



        protected void OnAddLibraryItemClicked(object sender, EventArgs e)
        {
            libraryItemFinder.Visible = true;
        }

        protected void OnLibraryItemFinderMultiItemSelected(object sender, ViewEventArgs<List<LibraryItem>> e)
        {
            var items = lstItems
                 .Items
                 .Select(control => new
                 {
                     Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
                     Pickup = control.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
                     Delivery = control.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt(),
                     Weight = control.FindControl("txtWeight").ToTextBox().Text,
                     Length = control.FindControl("txtLength").ToTextBox().Text,
                     Width = control.FindControl("txtWidth").ToTextBox().Text,
                     Height = control.FindControl("txtHeight").ToTextBox().Text,
                     Quantity = control.FindControl("txtQuantity").ToTextBox().Text,
                     PieceCount = control.FindControl("txtPieceCount").ToTextBox().Text,
                     Description = control.FindControl("txtDescription").ToTextBox().Text,
                     Comment = control.FindControl("txtComment").ToTextBox().Text,
                     FreightClass = control.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                     Value = control.FindControl("txtItemValue").ToTextBox().Text,
                     IsStackable = control.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                     HazardousMaterial = control.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
                     PackageTypeId = control.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                     NMFCCode = control.FindControl("txtNMFCCode").ToTextBox().Text,
                     HTSCode = control.FindControl("txtHTSCode").ToTextBox().Text,
                 })
                 .ToList();

            var itemsToAdd = e.Argument
                .Select(t => new
                {
                    Id = default(long),
                    Pickup = 0,
                    Delivery = ViewDeliveryStopOrder,
                    Weight = t.Weight.GetString(),
                    Length = t.Length.GetString(),
                    Width = t.Width.GetString(),
                    Height = t.Height.GetString(),
                    Quantity = t.Quantity.GetString(),
                    PieceCount = t.PieceCount.GetString(),
                    t.Description,
                    t.Comment,
                    t.FreightClass,
                    Value = 0.GetString(),
                    t.IsStackable,
                    t.HazardousMaterial,
                    PackageTypeId = t.PackageTypeId.ToLong(),
                    t.NMFCCode,
                    t.HTSCode,
                })
                .ToList();

            items.AddRange(itemsToAdd);

            lstItems.DataSource = items;
            lstItems.DataBind();
            tabItems.HeaderText = items.BuildTabCount(ItemsHeader);

            libraryItemFinder.Visible = false;
        }

        protected void OnLibraryItemFinderItemCancelled(object sender, EventArgs e)
        {
            libraryItemFinder.Visible = false;
        }



        protected void OnAddReferenceClicked(object sender, EventArgs e)
        {
            var references = lstReferences.Items
               .Select(control => new
               {
                   Id = control.FindControl("hidReferenceId").ToCustomHiddenField().Value.ToLong(),
                   Name = control.FindControl("txtName").ToTextBox().Text,
                   Value = control.FindControl("txtValue").ToTextBox().Text,
                   DisplayOnOrigin = control.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
                   DisplayOnDestination = control.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
                   Required = control.FindControl("hidRequired").ToCustomHiddenField().Value.ToBoolean()
               })
               .ToList();

            references.Add(new
            {
                Id = default(long),
                Name = string.Empty,
                Value = string.Empty,
                DisplayOnOrigin = false,
                DisplayOnDestination = false,
                Required = false
            });

            lstReferences.DataSource = references;
            lstReferences.DataBind();
            litReferenceCount.Text = references.BuildTabCount();

        }

        protected void OnDeleteReferenceClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var references = lstReferences.Items
               .Select(control => new
               {
                   Id = control.FindControl("hidReferenceId").ToCustomHiddenField().Value.ToLong(),
                   Name = control.FindControl("txtName").ToTextBox().Text,
                   Value = control.FindControl("txtValue").ToTextBox().Text,
                   DisplayOnOrigin = control.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
                   DisplayOnDestination = control.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
                   Required = control.FindControl("hidRequired").ToCustomHiddenField().Value.ToBoolean()
               })
               .ToList();

            references.RemoveAt(imageButton.Parent.FindControl("hidReferenceIndex").ToCustomHiddenField().Value.ToInt());

            lstReferences.DataSource = references;
            lstReferences.DataBind();
            litReferenceCount.Text = references.BuildTabCount();
        }

        protected void OnClearAllReferencesClicked(object sender, EventArgs e)
        {
            lstReferences.DataSource = new List<object>();
            lstReferences.DataBind();
            litReferenceCount.Text = new List<object>().BuildTabCount();
        }



        private void CheckForCreditAlert(decimal charges)
        {
            return; // temporarily disabled feature v4.0.18.2
            var credit = hidCustomerCredit.Value.ToDecimal();
            var outstanding = hidCustomerOutstandingBalance.Value.ToDecimal() - hidLoadedShipmentTotalDue.Value.ToDecimal();
            var alertThreshold = ProcessorVars.CustomerCreditAlertThreshold[ActiveUser.TenantId];
            pnlCreditAlert.Visible = credit.CreditAlert(outstanding + charges, alertThreshold);
        }


        protected void OnChangeVendorInCharge(object sender, EventArgs e)
        {
            lstCharges.Items.Select(c => new {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField(),
            })
            .ToList()
            .ForEach(cc =>
            {
                cc.SelectedVendorId.Value = cc.Vendors.SelectedValue;
            });

            upPnlCharges.Update();
        }


        protected void OnAddChargeClicked(object sender, EventArgs e)
        {
            var charges = lstCharges.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    //Vendors = i.FindControl("ddlVendors").ToDropDownList().SelectedValue.ToInt(),
                    VendorId = i.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value
                })
                .ToList();

            charges.Add(new
            {
                Id = default(long),
                Quantity = default(int),
                UnitBuy = default(decimal),
                UnitSell = default(decimal),
                UnitDiscount = default(decimal),
                Comment = string.Empty,
                ChargeCodeId = default(long),
                //Vendors = 0,
                VendorId = string.Empty
            });

            lstCharges.DataSource = charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, charges.BuildTabCount(ChargesHeader));
        }

        protected void OnClearChargesClicked(object sender, EventArgs e)
        {
            lstCharges.DataSource = new List<object>();
            lstCharges.DataBind();
            tabCharges.HeaderText = new List<object>().BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, new List<object>().BuildTabCount(ChargesHeader));

            chargeStatistics.LoadCharges(new List<Charge>(), hidResellerAdditionId.Value.ToLong());
            CheckForCreditAlert(0.0000m);
        }

        protected void OnDeleteChargeClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var charges = lstCharges.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    VendorId = i.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value
                })
                .ToList();

            charges.RemoveAt(imageButton.Parent.FindControl("hidChargeIndex").ToCustomHiddenField().Value.ToInt());

            lstCharges.DataSource = charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, charges.BuildTabCount(ChargesHeader));
            var chargeList = charges
                    .Select(c => new Charge
                    {
                        ChargeCodeId = c.ChargeCodeId,
                        Comment = c.Comment,
                        Quantity = c.Quantity,
                        UnitBuy = c.UnitBuy,
                        UnitSell = c.UnitSell,
                        UnitDiscount = c.UnitDiscount,
                    }).ToList();

            chargeStatistics.LoadCharges(chargeList, hidResellerAdditionId.Value.ToLong());
            CheckForCreditAlert(chargeList.Sum(c => c.AmountDue));
        }



        private void CheckServicesForAutoRating(LoadOrder loadOrder)
        {
            var bcsId = string.Empty;
            var hmId = string.Empty;
            if (loadOrder.IsInternational() && ActiveUser.Tenant.BorderCrossingServiceId != default(long) &&
                loadOrder.Services.All(s => s.ServiceId != ActiveUser.Tenant.BorderCrossingServiceId))
            {
                loadOrder.Services.Add(new LoadOrderService
                {
                    TenantId = ActiveUser.TenantId,
                    LoadOrder = loadOrder,
                    ServiceId = ActiveUser.Tenant.BorderCrossingServiceId
                });
                bcsId = ActiveUser.Tenant.BorderCrossingServiceId.ToString();

            }

            if (loadOrder.HazardousMaterial && ActiveUser.Tenant.HazardousMaterialServiceId != default(long) &&
                loadOrder.Services.All(s => s.ServiceId != ActiveUser.Tenant.HazardousMaterialServiceId))
            {
                loadOrder.Services.Add(new LoadOrderService
                {
                    TenantId = ActiveUser.TenantId,
                    LoadOrder = loadOrder,
                    ServiceId = ActiveUser.Tenant.HazardousMaterialServiceId
                });
                hmId = ActiveUser.Tenant.HazardousMaterialServiceId.ToString();
            }

            if (!string.IsNullOrEmpty(hmId) || !string.IsNullOrEmpty(bcsId))
            {
                foreach (RepeaterItem item in rptServices.Items)
                {
                    var serviceId = item.FindControl("hidServiceId").ToCustomHiddenField().Value;
                    item.FindControl("chkSelected").ToCheckBox().Checked = serviceId == hmId || serviceId == bcsId;
                }
            }
        }


        protected void OnAutoRateClicked(object sender, EventArgs e)
        {
            var loadOrder = UpdateLoadOrder();
            CheckServicesForAutoRating(loadOrder);

            //convert, and send out for rating...
            var shipment = UpdateLoadOrder().ToShipment();
            shipment.LinearFootRuleBypassed = chkBypassLinearFootRuleForRating.Checked;

            rateSelectionDisplay.Visible = true;
            rateSelectionDisplay.Rate(shipment);
        }

        protected void OnRefreshChargeStatisticsClicked(object sender, ImageClickEventArgs e)
        {
            var chargeList = lstCharges
                .Items
                .Select(i => new Charge
                {
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                })
                .ToList();

            chargeStatistics.LoadCharges(chargeList, hidResellerAdditionId.Value.ToLong());
        }

        protected void OnRateSelectionDisplaySelectionCancelled(object sender, EventArgs e)
        {
            rateSelectionDisplay.Visible = false;
        }

        protected void OnRateSelectionDisplayRateSelected(object sender, ViewEventArgs<Rate> e)
        {
            var rate = e.Argument;
            var loadOrder = UpdateLoadOrder();

            // add new charges
            loadOrder.Charges.AddRange(
                rate.Charges.Select(c => new LoadOrderCharge
                {
                    TenantId = loadOrder.TenantId,
                    LoadOrder = loadOrder,
                    ChargeCodeId = c.ChargeCodeId,
                    UnitBuy = c.UnitBuy,
                    UnitSell = c.UnitSell,
                    UnitDiscount = c.UnitDiscount,
                    Quantity = c.Quantity,
                    Comment = c.Comment
                }));

            // set charge related items
            ddlServiceMode.SelectedValue = rate.Mode.ToInt().ToString();
            loadOrder.ServiceMode = rate.Mode;
            Vendor vendor = null;
            switch (rate.Mode)
            {
                case ServiceMode.SmallPackage:
                    vendor = rate.Vendor;
                    break;
                case ServiceMode.LessThanTruckload:
                    vendor = rate.LTLSellRate.VendorRating.Vendor;
                    break;
            }
            if (vendor != null)
            {
                var primaryVendorIndex = -1;
                for (int i = 0; i < loadOrder.Vendors.Count; i++)
                    if (loadOrder.Vendors[i].Primary)
                    {
                        primaryVendorIndex = i;
                        break;
                    }
                if (primaryVendorIndex != -1) loadOrder.Vendors[primaryVendorIndex].Vendor = rate.Vendor;
                else
                    loadOrder.Vendors
                        .Add(new LoadOrderVendor
                        {
                            TenantId = loadOrder.TenantId,
                            Vendor = rate.Vendor,
                            LoadOrder = loadOrder,
                            Primary = true,
                            ProNumber = string.Empty,
                        });
            }

            // estimated delivery
            loadOrder.EstimatedDeliveryDate = e.Argument.EstimatedDeliveryDate;

            loadOrder.CriticalBolComments = rate.GetCriticalBolComments(loadOrder.EstimatedDeliveryDate);

            var sellRate = rate.LTLSellRate ?? new LTLSellRate();
            loadOrder.CriticalBolComments = sellRate.VendorRating.AppendVendorRatingCriticalCommentToShipment(loadOrder.CriticalBolComments);

            loadOrder.LinearFootRuleBypassed = chkBypassLinearFootRuleForRating.Checked;	// will still be checked upon selection

            // hide rate selction display
            rateSelectionDisplay.Visible = false;

	        if (rate.IsGuaranteed)
	        {
		        CheckAndAddGuaranteedService(loadOrder);
	        }
	             
            // re-load load order
            LoadLoadOrder(loadOrder, false);
        }



        protected void OnVendorLaneHistoryClose(object sender, EventArgs e)
        {
            vendorLaneHistoryBuySell.Visible = false;
        }

        protected void OnDocumentDisplayClosed(object sender, EventArgs e)
        {
            documentDisplay.Visible = false;
        }



        protected void OnLoadTenderDisplayCloseClicked(object sender, EventArgs e)
        {
            pnlLoadTenderDisplay.Visible = false;
            pnlDimScreen.Visible = false;
            hidLoadTenderDisplayFlag.Value = null;
        }

        protected void OnLoadTenderDisplayProcessClicked(object sender, EventArgs e)
        {
            pnlLoadTenderDisplay.Visible = false;
            pnlDimScreen.Visible = false;
            AddEdiFlagToVendorId(hidPrimaryVendorId.Value.ToLong(), hidLoadTenderDisplayFlag.Value);
            hidLoadTenderDisplayFlag.Value = null;
        }



        protected void OnVendorRateAgreementClose(object sender, EventArgs e)
        {
            vendorRateAgreement.Visible = false;
        }

        protected void OnVendorRateAgreementGenerate(object sender, EventArgs e)
        {
            GenerateVendorRateAgreement(false);
        }



        protected void OnClearResellerAdditionDetailsClicked(object sender, EventArgs e)
        {
            DisplayResellerAddition(null, false);
        }

        protected void OnRetrieveResellerAdditionDetailsClicked(object sender, EventArgs e)
        {
            var customer = new Customer(hidCustomerId.Value.ToLong());
            var rating = customer.Rating;

            DisplayResellerAddition(rating != null ? rating.ResellerAddition : null, rating != null && rating.BillReseller);
        }


        protected void OnShippingLabelGeneratorCloseClicked(object sender, EventArgs e)
        {
            shippingLabelGenerator.Visible = false;
        }

        protected void OnServiceModeSelectedIndexChanged(object sender, EventArgs e)
        {
            var serviceModeValue = ddlServiceMode.SelectedValue.ToInt();

            if (!serviceModeValue.EnumValueIsValid<ServiceMode>()) return;

            var serviceMode = serviceModeValue.ToEnum<ServiceMode>();

            //configure detail visibility based on service mode
            ConfigureServiceModeDisplay(serviceMode);

            //reset sales rep based on service mode
            DisplayCustomerSalesRepresentative(hidCustomerId.Value.ToLong() == default(long) ? null : new Customer(hidCustomerId.Value.ToLong()));
        }


        protected void OnFindLoadOrderCoordUserClicked(object sender, ImageClickEventArgs e)
        {
            hidFlag.Value = LoadOrderCoordinatorFlag;
            userFinder.FilterForShipmentCoordinator = true;
            userFinder.FilterForCarrierCoordinator = false;
            userFinder.Reset();
            userFinder.Visible = true;
        }

        protected void OnFindCarrierCoordUserClicked(object sender, ImageClickEventArgs e)
        {
            hidFlag.Value = CarrierCoordinatorFlag;
            userFinder.FilterForCarrierCoordinator = true;
            userFinder.FilterForShipmentCoordinator = false;
            userFinder.Reset();
            userFinder.Visible = true;
        }


        protected void OnUserFinderItemSelected(object sender, ViewEventArgs<User> e)
        {
            switch (hidFlag.Value)
            {
                case CarrierCoordinatorFlag:
                    DisplayCarrierCoordinator(e.Argument);
                    break;
                case LoadOrderCoordinatorFlag:
                    DisplayLoadOrderCoordinator(e.Argument);
                    break;
            }
            hidFlag.Value = string.Empty;
            userFinder.Visible = false;
        }

        protected void OnUserFinderSelectionCancelled(object sender, EventArgs e)
        {
            userFinder.Visible = false;
            hidFlag.Value = string.Empty;
        }


        private List<OperationsViewNoteObj> GetCurrentViewNotesUpdated()
        {
            var notes = peNotes.GetData<OperationsViewNoteObj>();
            var vNotes = lstNotes
                .Items
                .Select(i => new
                {
                    EditIdx = i.FindControl("hidNoteIndex").ToCustomHiddenField().Value.ToInt(),
                    Note = new OperationsViewNoteObj
                    {
                        Id = i.FindControl("hidNoteId").ToCustomHiddenField().Value.ToLong(),
                        Archived = i.FindControl("chkArchived").ToAltUniformCheckBox().Checked,
                        Classified = i.FindControl("chkClassified").ToAltUniformCheckBox().Checked,
                        IsLoadOrderNote = i.FindControl("hidLoadOrderNote").ToCustomHiddenField().Value.ToBoolean(),
                        Message = i.FindControl("txtMessage").ToTextBox().Text,
                        Type = i.FindControl("ddlNoteType").ToDropDownList().SelectedValue.ToInt().ToEnum<NoteType>(),
                        Username = i.FindControl("txtUser").ToTextBox().Text,
                    }
                });
            foreach (var vn in vNotes)
                notes[vn.EditIdx] = vn.Note;

            return notes;
        }


        protected void OnAddNoteClicked(object sender, EventArgs e)
        {
            var notes = GetCurrentViewNotesUpdated();

            notes.Insert(0, new OperationsViewNoteObj
            {
                Id = default(long),
                Type = NoteTypeCollection[0].Value.ToInt().ToEnum<NoteType>(),
                Archived = false,
                Classified = true,
                Message = string.Empty,
                Username = ActiveUser.Username,
                IsLoadOrderNote = false,
            });

            peNotes.LoadData(notes);
            tabNotes.HeaderText = notes.BuildTabCount(NotesHeader);
            athtuTabUpdater.SetForUpdate(tabNotes.ClientID, notes.BuildTabCount(NotesHeader));
        }

        protected void OnDeleteUnsavedNoteClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var notes = GetCurrentViewNotesUpdated();
            var itemIdx = imageButton.FindControl("hidNoteIndex").ToCustomHiddenField().Value.ToInt();
            notes.RemoveAt(itemIdx);
            peNotes.LoadData(notes, peNotes.CurrentPage);
            tabNotes.HeaderText = notes.BuildTabCount(NotesHeader);
            athtuTabUpdater.SetForUpdate(tabNotes.ClientID, notes.BuildTabCount(NotesHeader));
        }

        protected void OnNotesItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var ddlType = e.Item.FindControl("ddlNotetype").ToDropDownList();

            var type = e.Item.DataItem.GetPropertyValue("Type").ToInt().GetString();
            ddlType.DataSource = NoteTypeCollection;
            ddlType.DataBind();
            if (ddlType.ContainsValue(type)) ddlType.SelectedValue = type;
        }

        protected void OnPeNotesPageChanging(object sender, EventArgs e)
        {
            var notes = GetCurrentViewNotesUpdated();
            peNotes.LoadData(notes, peNotes.CurrentPage);
        }

        protected void OnAddStopFromAddressBookClicked(object sender, EventArgs e)
        {
            addressBookFinder.Visible = true;
            hidFlag.Value = OperationsAddressInputType.Stop.GetString();
        }



        private void ProcessStopMoves(IList<LoadOrderLocation> locations, int newIdx, int currentIdx)
        {
            var l1 = locations[currentIdx];
            var l2 = locations[newIdx];

            locations[currentIdx] = l2;
            locations[newIdx] = l1;

            lstStops.DataSource = locations;
            lstStops.DataBind();
            tabStops.HeaderText = locations.BuildTabCount(StopsHeader);
        }


        private void RefreshItemStops(List<LoadOrderLocation> stops)
        {
            foreach (var item in lstItems.Items)
            {
                // Bind stops
                var ddlItemPickup = item.FindControl("ddlItemPickup").ToDropDownList();
                var ddlItemDelivery = item.FindControl("ddlItemDelivery").ToDropDownList();

                var pickupValue = ddlItemPickup.SelectedValue;
                var delValue = ddlItemDelivery.SelectedValue;


                var originStops = stops
                    .OrderBy(s => s.StopOrder)
                    .Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
                    .ToList();
                originStops.Insert(0, new ViewListItem(OriginStopText, 0.ToString()));

                var destStops = stops
                    .OrderBy(s => s.StopOrder)
                    .Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
                    .ToList();
                destStops.Add(new ViewListItem(DestinationStopText, ViewDeliveryStopOrder.ToString()));

                ddlItemPickup.DataSource = originStops;
                ddlItemPickup.DataBind();
                if (ddlItemPickup.ContainsValue(pickupValue)) ddlItemPickup.SelectedValue = pickupValue;


                ddlItemDelivery.DataSource = destStops;
                ddlItemDelivery.DataBind();
                if (ddlItemDelivery.ContainsValue(delValue)) ddlItemDelivery.SelectedValue = delValue;
            }
        }


        protected void OnStopsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = e.Item as ListViewDataItem;
            if (item == null) return;
            var ic = item.FindControl("llicLocation").ToLocationListingInputControl();
            var location = item.DataItem as LoadOrderLocation;
            if (location == null) return;
            ic.LoadLocation(location);

        }


        protected void OnAddLocationClicked(object sender, EventArgs e)
        {
            var locations = RetrieveViewStops(new LoadOrder(hidLoadOrderId.Value.ToLong(), false));
            locations.Add(new LoadOrderLocation
            {
                AppointmentDateTime = DateUtility.SystemEarliestDateTime,
                StopOrder = locations.Count + 1
            });
            lstStops.DataSource = locations;
            lstStops.DataBind();
            tabStops.HeaderText = locations.BuildTabCount(StopsHeader);
            athtuTabUpdater.SetForUpdate(tabStops.ClientID, locations.BuildTabCount(StopsHeader));

            // update items stops by retrieving and reloading!
            RefreshItemStops(locations);
            upPnlItems.Update();
        }

        protected void OnDeleteLocation(object sender, ViewEventArgs<int> e)
        {
            var locations = RetrieveViewStops(new LoadOrder(hidLoadOrderId.Value.ToLong(), false));
            locations.RemoveAt(e.Argument);
            lstStops.DataSource = locations;
            lstStops.DataBind();
            tabStops.HeaderText = locations.BuildTabCount(StopsHeader);
            athtuTabUpdater.SetForUpdate(tabStops.ClientID, locations.BuildTabCount(StopsHeader));

            for (var i = 0; i < locations.Count; i++) locations[i].StopOrder = i + 1;

            // update items stops by retrieving and reloading!
            RefreshItemStops(locations);
            upPnlItems.Update();
        }


        protected void OnMoveUpClicked(object sender, EventArgs e)
        {
            var btn = (ImageButton)sender;
            var input = btn.Parent.FindControl("llicLocation").ToLocationListingInputControl();
            var index = input.RetrieveLocatinItemIndex();

            if (index - 1 < 0) return;

            var locations = RetrieveViewStops(new LoadOrder(hidLoadOrderId.Value.ToLong(), false));

            ProcessStopMoves(locations, index - 1, index);
        }

        protected void OnMoveDownClicked(object sender, EventArgs e)
        {
            var btn = (ImageButton)sender;
            var input = btn.Parent.FindControl("llicLocation").ToLocationListingInputControl();
            var index = input.RetrieveLocatinItemIndex();

            var locations = RetrieveViewStops(new LoadOrder(hidLoadOrderId.Value.ToLong(), false));

            if (index >= locations.Count - 1) return; // i.e. it's already the last one in the list

            ProcessStopMoves(locations, index + 1, index);
        }

        protected void OnAddVendorClicked(object sender, EventArgs e)
        {
            vendorFinder.Reset();
            vendorFinder.EnableMultiSelection = true;
            vendorFinder.Visible = true;
        }

        protected void OnVendorFinderMultiItemSelected(object sender, ViewEventArgs<List<Vendor>> e)
        {
            var vendors = lstVendors.Items
                .Select(c => new
                {
                    Id = c.FindControl("hidLoadOrderVendorId").ToCustomHiddenField().Value.ToLong(),
                    VendorNumber = c.FindControl("txtVendorNumber").ToTextBox().Text,
                    Name = c.FindControl("txtName").ToTextBox().Text,
                    VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                    ProNumber = c.FindControl("txtProNumber").ToTextBox().Text,
                    InsuranceAlert = c.FindControl("hidInsuranceAlert").ToCustomHiddenField().Value.ToBoolean(),
                    InsuranceTooltip = c.FindControl("hidInsuranceTooltip").ToCustomHiddenField().Value
                })
                .ToList();

            var messages = new List<ValidationMessage>();

            foreach (var vendor in e.Argument.Where(vendor => vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId])))
            {
                messages.AddRange(new[]
                                    {
                                        ValidationMessage.Error(string.Format("{0}-{1} invalid: {2}",
                                        vendor.VendorNumber,vendor.Name,vendor.VendorInsuranceAlertText(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId])))
                                    });
            }

            var newVendors = e.Argument.Where(vendor => !vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId]))
                .Select(vendor => new
                {
                    Id = default(long),
                    vendor.VendorNumber,
                    vendor.Name,
                    VendorId = vendor.Id,
                    ProNumber = string.Empty,
                    InsuranceAlert = false,
                    InsuranceTooltip = string.Empty
                })
                .Where(v => !vendors.Select(qv => qv.VendorId).Contains(v.VendorId))
                .ToList();

            vendors.AddRange(newVendors);

            if (newVendors.Any())
                messages.AddRange(CheckVendorConstraints(newVendors.Select(v => new Vendor(v.VendorId)).ToList()));

            DisplayMessages(messages);

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            litVendorsCount.Text = vendors.BuildTabCount();
            vendorFinder.Visible = false;

            var ddlNewVendors = vendors.Select(t => new ListItem { Text = t.Name, Value = t.VendorId.ToString() }).ToArray();
            lstCharges.Items.Select(c => c.FindControl("ddlVendors").ToDropDownList()).ToList().ForEach(cc => cc.Items.AddRange((ddlNewVendors.Except(cc.Items.Cast<ListItem>().ToArray())).ToArray()));
            upPnlCharges.Update();
        }

        protected void OnDeleteVendorClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var vendors = lstVendors.Items
                 .Select(c => new
                 {
                     Id = c.FindControl("hidLoadOrderVendorId").ToCustomHiddenField().Value.ToLong(),
                     VendorNumber = c.FindControl("txtVendorNumber").ToTextBox().Text,
                     Name = c.FindControl("txtName").ToTextBox().Text,
                     VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                     ProNumber = c.FindControl("txtProNumber").ToTextBox().Text,
                     InsuranceAlert = c.FindControl("hidInsuranceAlert").ToCustomHiddenField().Value.ToBoolean(),
                     InsuranceTooltip = c.FindControl("hidInsuranceTooltip").ToCustomHiddenField().Value
                 })
                 .ToList();

            vendors.RemoveAt(imageButton.Parent.FindControl("hidVendorIndex").ToCustomHiddenField().Value.ToInt());

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            litVendorsCount.Text = vendors.BuildTabCount();

            var removedVendorId = imageButton.Parent.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong();
            var ddlVendorsList = lstCharges.Items.Select(c => new {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField()
            }).ToList();

            ddlVendorsList.ForEach(cc =>
            {
                cc.Vendors.Items.Clear();
                BuildVendorsDropdownListOnChargesTab(
                    cc.Vendors,
                    cc.SelectedVendorId.Value.ToLong() == removedVendorId ? hidPrimaryVendorId.Value.ToLong() : cc.SelectedVendorId.Value.ToLong());
                cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == removedVendorId ? hidPrimaryVendorId.Value : cc.SelectedVendorId.Value;
            });
            upPnlCharges.Update();
        }

        protected void OnClearAllVendorsClicked(object sender, EventArgs e)
        {
            lstVendors.DataSource = new List<object>();
            lstVendors.DataBind();
            litVendorsCount.Text = new List<object>().BuildTabCount();

            var ddlVendorsList = lstCharges.Items.Select(c => new {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField()
            }).ToList();

            ddlVendorsList.ForEach(cc =>
            {
                cc.Vendors.Items.Clear();
                BuildVendorsDropdownListOnChargesTab(cc.Vendors, hidPrimaryVendorId.Value.ToLong());
                cc.SelectedVendorId.Value = hidPrimaryVendorId.Value.ToString();
            });
            upPnlCharges.Update();
        }

        protected void OnChargesItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;

            var ddlVendors = item.FindControl("ddlVendors").ToDropDownList();
            var selectedValue = item.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value.ToLong();

            BuildVendorsDropdownListOnChargesTab(ddlVendors, selectedValue);
        }

        private void BuildVendorsDropdownListOnChargesTab(DropDownList ddlVendors, long selectedValue)
        {
            var vendorCollection = VendorCollection;

            if (!vendorCollection.Select(s => s.Value.ToLong()).Contains(selectedValue))
            {
                var vendor = new Vendor(selectedValue);
	            vendorCollection.Add(new ViewListItem {Text = vendor.Name, Value = vendor.Id.ToString()});
            }

            ddlVendors.DataSource = vendorCollection;
            ddlVendors.SelectedValue = selectedValue == 0 ? (ddlVendors.SelectedValue.ToLong() == 0 ? hidPrimaryVendorId.Value : ddlVendors.SelectedValue) : selectedValue.ToString();
            ddlVendors.DataBind();
            upPnlCharges.Update();
        }

        private void LoadDocumentForEdit(LoadOrderDocument document)
        {
            chkDocumentIsInternal.Checked = document.IsInternal;
            txtDocumentName.Text = document.Name;
            txtDocumentDescription.Text = document.Description;
            ddlDocumentTag.SelectedValue = document.DocumentTagId.GetString();
            txtLocationPath.Text = GetLocationFileName(document.LocationPath);
            hidLocationPath.Value = document.LocationPath;
        }



        protected string GetLocationFileName(object relativePath)
        {
            return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
        }

        protected void OnAddDocumentClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadDocumentForEdit(new LoadOrderDocument());
            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnEditDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            hidEditingIndex.Value = control.FindControl("hidDocumentIndex").ToCustomHiddenField().Value;

            var document = new LoadOrderDocument
            {
                Name = control.FindControl("litDocumentName").ToLiteral().Text,
                Description = control.FindControl("litDocumentDescription").ToLiteral().Text,
                DocumentTagId = control.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                LocationPath = control.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                IsInternal = control.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
            };

            LoadDocumentForEdit(document);

            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnDeleteDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var documents = lstDocuments.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("litDocumentName").ToLiteral().Text,
                    Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                    DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                    LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                    IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
                })
                .ToList();

            var index = imageButton.Parent.FindControl("hidDocumentIndex").ToCustomHiddenField().Value.ToInt();
            hidFilesToDelete.Value += string.Format("{0};", documents[index].LocationPath);
            documents.RemoveAt(index);

            lstDocuments.DataSource = documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);
        }

        protected void OnCloseEditDocumentClicked(object sender, EventArgs eventArgs)
        {
            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditDocumentDoneClicked(object sender, EventArgs e)
        {
            var virtualPath = WebApplicationSettings.LoadOrderFolder(ActiveUser.TenantId, hidLoadOrderId.Value.ToLong());
            var physicalPath = Server.MapPath(virtualPath);

            var documents = lstDocuments.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("litDocumentName").ToLiteral().Text,
                    Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                    DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value,
                    LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                    IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
                })
                .ToList();

            var documentIndex = hidEditingIndex.Value.ToInt();

            var documentId = default(long);
            if (documentIndex != -1)
            {
                documentId = documents[documentIndex].Id;
                documents.RemoveAt(documentIndex);
            }

            var locationPath = hidLocationPath.Value; // this should actually be a hidden field holding the existing path during edit

            documents.Insert(0, new
            {
                Id = documentId,
                Name = txtDocumentName.Text,
                Description = txtDocumentDescription.Text,
                DocumentTagId = ddlDocumentTag.SelectedValue,
                LocationPath = locationPath,
                IsInternal = chkDocumentIsInternal.Checked,
            });

            if (fupLocationPath.HasFile)
            {
                // ensure directory is present
                if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

                //ensure unique filename
                var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLocationPath.FileName), true));

                // save file
                fupLocationPath.WriteToFile(physicalPath, fi.Name, Server.MapPath(documents[0].LocationPath));

                // remove updated record
                documents.RemoveAt(0);

                // re-add record with new file
                documents.Insert(0, new
                {
                    Id = documentId,
                    Name = txtDocumentName.Text,
                    Description = txtDocumentDescription.Text,
                    DocumentTagId = ddlDocumentTag.SelectedValue,
                    LocationPath = virtualPath + fi.Name,
                    IsInternal = chkDocumentIsInternal.Checked
                });
            }

            lstDocuments.DataSource = documents.OrderBy(d => d.Name);
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);

            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnLocationPathClicked(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
            Response.Export(Server.ReadFromFile(path), button.Text);
        }

        protected void OnClearAllDocumentsClicked(object sender, EventArgs e)
        {
            foreach (var item in lstDocuments.Items)
                hidFilesToDelete.Value += string.Format("{0};", item.FindControl("hidLocationPath").ToCustomHiddenField().Value);

            lstDocuments.DataSource = new List<object>();
            lstDocuments.DataBind();
            tabDocuments.HeaderText = new List<object>().BuildTabCount(DocumentsHeader);
        }



        protected void OnAddAccountBucketClicked(object sender, EventArgs e)
        {
            accountBucketFinder.Reset();
            accountBucketFinder.EnableMultiSelection = true;
            accountBucketFinder.Visible = true;
        }

        protected void OnAccountBucketFinderSelectionCancelled(object sender, EventArgs e)
        {
            accountBucketFinder.Visible = false;
        }

        protected void OnAccountBucketFinderMultiItemSelected(object sender, ViewEventArgs<List<AccountBucket>> e)
        {
            var accountBuckets = lstAccountBuckets.Items
                .Select(c => new
                {
                    Id = c.FindControl("hidLoadOrderAccountBucketId").ToCustomHiddenField().Value.ToLong(),
                    AccountBucketCode = c.FindControl("txtAccountBucketCode").ToTextBox().Text,
                    AccountBucketDescription = c.FindControl("txtAccountBucketDescription").ToTextBox().Text,
                    AccountBucketId = c.FindControl("hidAccountBucketId").ToCustomHiddenField().Value.ToLong(),
                })
                .ToList();

            var newAccountBucket = e.Argument
                .Select(accountBucket => new
                {
                    Id = default(long),
                    AccountBucketCode = accountBucket.Code,
                    AccountBucketDescription = accountBucket.Description,
                    AccountBucketId = accountBucket.Id,
                })
                .Where(v => !accountBuckets.Select(qv => qv.AccountBucketId).Contains(v.AccountBucketId));

            accountBuckets.AddRange(newAccountBucket);

            lstAccountBuckets.DataSource = accountBuckets;
            lstAccountBuckets.DataBind();
            litAccountBucketCount.Text = accountBuckets.BuildTabCount();

            accountBucketFinder.Visible = false;
        }



        protected void OnDeleteAccountBucketClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var accountBuckets = lstAccountBuckets.Items
                .Select(c => new
                {
                    Id = c.FindControl("hidLoadOrderAccountBucketId").ToCustomHiddenField().Value.ToLong(),
                    AccountBucketCode = c.FindControl("txtAccountBucketCode").ToTextBox().Text,
                    AccountBucketDescription = c.FindControl("txtAccountBucketDescription").ToTextBox().Text,
                    AccountBucketId = c.FindControl("hidAccountBucketId").ToCustomHiddenField().Value.ToLong(),
                })
                .ToList();

            accountBuckets.RemoveAt(imageButton.Parent.FindControl("hidAccountBucketIndex").ToCustomHiddenField().Value.ToInt());

            lstAccountBuckets.DataSource = accountBuckets;
            lstAccountBuckets.DataBind();
            litAccountBucketCount.Text = accountBuckets.BuildTabCount();
        }

        protected void OnCloseEditAccountBucketClicked(object sender, ImageClickEventArgs e)
        {
            accountBucketFinder.Visible = false;
        }

        protected void OnClearAllAccountBucketsClicked(object sender, EventArgs e)
        {
            lstAccountBuckets.DataSource = new List<object>();
            lstAccountBuckets.DataBind();
            litAccountBucketCount.Text = new List<object>().BuildTabCount();
        }

        protected void OnGetMilesClicked(object sender, ImageClickEventArgs e)
        {
            var ms = new MileageSource(ddlMileageSource.SelectedValue.ToLong());
            if (!ms.KeyLoaded)
            {
                txtMileage.Text = string.Empty;
                DisplayMessages(new[] { ValidationMessage.Error("Unable to load mileage source for mileage engine. Please ensure mileage source is selected.") });
                return;
            }


            var loadOrder = UpdateLoadOrder();
            var msg = loadOrder.CalculateShipmentMiles(ms.MileageEngine);
            LoadLoadOrder(loadOrder, false);
            upPnlStops.Update();

            if (msg.Any(m => m.Type == ValidateMessageType.Error)) DisplayMessages(msg);
        }


        protected void OnStatusChanged(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case OfferedCmdName:
                    txtStatus.Text = LoadOrderStatus.Offered.FormattedString();
                    break;
                case QuotedCmdName:
                    txtStatus.Text = LoadOrderStatus.Quoted.FormattedString();
                    break;
                case CancelledCmdName:
                    txtStatus.Text = LoadOrderStatus.Cancelled.FormattedString();
                    break;
                case CoveredCmdName:
                    txtStatus.Text = LoadOrderStatus.Covered.FormattedString();
                    break;
                case LostCmdName:
                    txtStatus.Text = LoadOrderStatus.Lost.FormattedString();
                    break;
                case AcceptedCmdName:
                    txtStatus.Text = LoadOrderStatus.Accepted.FormattedString();
                    break;
            }
        }



        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
            hidFlag.Value = string.Empty;
        }

        protected void OnYesProcess(object sender, EventArgs e)
        {
            // Only process using 'Yes' on message box is 'Go To Shipment'
            ConvertLoadOrderToShipment();
        }

        protected void OnNoProcess(object sender, EventArgs e)
        {
            // Only process using 'No' on message box is 'Go To Shipment'
            messageBox.Visible = false;

        }



        protected void OnLogPrimaryVendorRejectionClicked(object sender, ImageClickEventArgs e)
        {
            var load = UpdateLoadOrder();

            var log = new VendorRejectionLog
            {
                UserId = ActiveUser.Id,
                VendorId = load.Vendors.First(v => v.Primary).VendorId,
                TenantId = ActiveUser.TenantId,
                Customer = load.Customer,
                OriginPostalCode = load.Origin.PostalCode,
                OriginCity = load.Origin.City,
                OriginState = load.Origin.State,
                OriginCountryCode = load.Origin.Country == null ? string.Empty : load.Origin.Country.Code,
                DestinationPostalCode = load.Destination.PostalCode,
                DestinationCountryCode = load.Destination.Country == null ? string.Empty : load.Destination.Country.Code,
                DestinationCity = load.Destination.City,
                DestinationState = load.Destination.State,
                ShipmentNumber = load.LoadOrderNumber,
                ShipmentDateCreated = load.DateCreated,
                DateCreated = DateTime.Now,
                ServiceMode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>()
            };

            hidFlag.Value = PrimaryVendorRejectLogFlag;

            var oldPrimaryVendor = new Vendor(log.VendorId);
            if (oldPrimaryVendor.CanSendLoadTender(hidStatus.Value.ToEnum<LoadOrderStatus>()) && (!_invalidEdiStatuses.Contains(hidEdiStatus.Value) || StoredEdiMessageQueue.ContainsKey(oldPrimaryVendor.Id)))
            {
                AddEdiFlagToVendorId(oldPrimaryVendor.Id, CancelLoadTenderFlag);
            }

            txtEdiStatus.Text = WebApplicationConstants.EdiStatusNotApplicable;
            hidEdiStatus.Value = txtEdiStatus.Text;

            if (LogVendorRejection != null)
                LogVendorRejection(this, new ViewEventArgs<VendorRejectionLog>(log));
        }

        protected void OnLogNonPrimaryVendorRejection(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var vendors = lstVendors.Items
                 .Select(c => new
                 {
                     Id = c.FindControl("hidLoadOrderVendorId").ToCustomHiddenField().Value.ToLong(),
                     VendorNumber = c.FindControl("txtVendorNumber").ToTextBox().Text,
                     Name = c.FindControl("txtName").ToTextBox().Text,
                     VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                     ProNumber = c.FindControl("txtProNumber").ToTextBox().Text,
                     InsuranceAlert = c.FindControl("hidInsuranceAlert").ToCustomHiddenField().Value.ToBoolean(),
                     InsuranceTooltip = c.FindControl("hidInsuranceTooltip").ToCustomHiddenField().Value
                 })
                 .ToList();

            var index = imageButton.Parent.FindControl("hidVendorIndex").ToCustomHiddenField().Value.ToInt();

            var load = UpdateLoadOrder();

            var log = new VendorRejectionLog
            {
                UserId = ActiveUser.Id,
                VendorId = load.Vendors[index].VendorId,
                TenantId = ActiveUser.TenantId,
                Customer = load.Customer,
                OriginPostalCode = load.Origin.PostalCode,
                OriginCity = load.Origin.City,
                OriginState = load.Origin.State,
                OriginCountryCode = load.Origin.Country == null ? string.Empty : load.Origin.Country.Code,
                DestinationPostalCode = load.Destination.PostalCode,
                DestinationCountryCode = load.Destination.Country == null ? string.Empty : load.Destination.Country.Code,
                DestinationCity = load.Destination.City,
                DestinationState = load.Destination.State,
                ShipmentNumber = load.LoadOrderNumber,
                ShipmentDateCreated = load.DateCreated,
                DateCreated = DateTime.Now,
                ServiceMode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>(),
            };

            hidFlag.Value = NonPrimaryVendorRejectLogFlag;

            if (LogVendorRejection != null)
                LogVendorRejection(this, new ViewEventArgs<VendorRejectionLog>(log));

            if (_doNotRemoveNonPrimaryVendor) return;	// this will be set if there is an error in the LogVendorRejection event process

            vendors.RemoveAt(index);

            var removedVendorId = imageButton.Parent.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong();
            var ddlVendorsList = lstCharges.Items.Select(c => new {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField()
            }).ToList();

            ddlVendorsList.ForEach(cc =>
            {
                cc.Vendors.Items.Clear();
                BuildVendorsDropdownListOnChargesTab(
                    cc.Vendors,
                    cc.SelectedVendorId.Value.ToLong() == removedVendorId ? hidPrimaryVendorId.Value.ToLong() : cc.SelectedVendorId.Value.ToLong());
                cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == removedVendorId ? hidPrimaryVendorId.Value : cc.SelectedVendorId.Value;
            });
            upPnlCharges.Update();
        }


        protected void OnServicesSelectionDoneClicked(object sender, EventArgs e)
        {
            var services = rptServices
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(i => new Service(i.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong()))
                .Where(i => i.KeyLoaded)
                .ToList();

            DisplayServices(services.Select(s => s.Id));

            var serviceChargeCodes = services
                 .Select(i => i.ChargeCode)
                 .ToList();

	        var charges = lstCharges.Items
	                                .Select(i => new
		                                {
			                                Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
			                                Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
			                                UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
			                                UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
			                                UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
			                                Comment = i.FindControl("txtComment").ToTextBox().Text,
			                                ChargeCodeId =
		                                             i.FindControl("ddlChargeCode")
		                                              .ToCachedObjectDropDownList()
		                                              .SelectedValue.ToLong(),
			                                VendorId = i.FindControl("ddlVendors").ToDropDownList().SelectedValue.ToInt()
		                                })
	                                .ToList();

            var existingCodeIds = charges.Select(i => i.ChargeCodeId).ToList();
            foreach (var scc in serviceChargeCodes.Where(c => !existingCodeIds.Contains(c.Id)))
                charges.Add(new
                {
                    Id = default(long),
                    Quantity = 1,
                    UnitBuy = 0.0000m,
                    UnitSell = 0.0000m,
                    UnitDiscount = 0.0000m,
                    Comment = string.Empty,
                    ChargeCodeId = scc.Id,
                    VendorId = 0
                });

            lstCharges.DataSource = charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, charges.BuildTabCount(ChargesHeader));
            upPnlCharges.Update();
        }


        protected void OnAddressInputFindPostalCode(object sender, ViewEventArgs<string> e)
        {
            var addressinput = (OperationsAddressInputControl)sender;
            hidFlag.Value = addressinput.Type.GetString();
            postalCodeFinder.Visible = true;
            postalCodeFinder.SearchOn(e.Argument);
        }

        protected void OnAddressInputAddAddressFromAddressBook(object sender, ViewEventArgs<string> e)
        {
            var addressinput = (OperationsAddressInputControl)sender;
            hidFlag.Value = addressinput.Type.GetString();
            addressBookFinder.Visible = true;
            addressBookFinder.SearchOn(e.Argument);
        }

        protected void OnAddressBookFinderItemCancelled(object sender, EventArgs e)
        {
            addressBookFinder.Visible = false;
            addressBookFinder.Reset();
            hidFlag.Value = string.Empty;
        }

        protected void OnAddressBookFinderItemSelected(object sender, ViewEventArgs<AddressBook> e)
        {
            switch (hidFlag.Value.ToEnum<OperationsAddressInputType>())
            {
                case OperationsAddressInputType.Origin:
                    AddServicesRequired(e.Argument.Services);
                    operationsAddressInputOrigin.LoadAddress(e.Argument);
                    break;
                case OperationsAddressInputType.Destination:
                    AddServicesRequired(e.Argument.Services);
                    operationsAddressInputDestination.LoadAddress(e.Argument);
                    break;
                case OperationsAddressInputType.Stop:
                    var loadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong());
                    var location = e.Argument;
                    var stop = new LoadOrderLocation
                    {
                        City = location.City,
                        CountryId = location.CountryId,
                        Description = location.Description,
                        PostalCode = location.PostalCode,
                        State = location.State,
                        Street1 = location.Street1,
                        Street2 = location.Street2,
                        LoadOrder = loadOrder,
                        TenantId = loadOrder.TenantId,
                        SpecialInstructions = chkOriginInstruction.Checked
                                                      ? location.OriginSpecialInstruction
                                                      : chkDestinationInstruction.Checked
                                                            ? location.DestinationSpecialInstruction
                                                            : string.Empty,
                        Direction = location.Direction,
                        GeneralInfo = location.GeneralInfo,
                        AppointmentDateTime = DateUtility.SystemEarliestDateTime
                    };
                    stop.Contacts.AddRange(location.Contacts.Select(c => new LoadOrderContact
                    {
                        Location = stop,
                        Comment = c.Comment,
                        ContactTypeId = c.ContactTypeId,
                        Email = c.Email,
                        Fax = c.Fax,
                        Mobile = c.Mobile,
                        Phone = c.Phone,
                        Name = c.Name,
                        Primary = c.Primary,
                        TenantId = stop.TenantId,
                    }));

                    var stops = RetrieveViewStops(loadOrder);
                    stops.Add(stop);
                    lstStops.DataSource = stops;
                    lstStops.DataBind();
                    tabStops.HeaderText = stops.BuildTabCount(StopsHeader);
                    break;
            }

            addressBookFinder.Visible = false;
            addressBookFinder.Reset();
            hidFlag.Value = string.Empty;
        }


        protected void OnPostalCodeFinderItemCancelled(object sender, EventArgs e)
        {
            postalCodeFinder.Visible = false;
            postalCodeFinder.Reset();
            hidFlag.Value = string.Empty;
        }

        protected void OnPostalCodeFinderItemSelected(object sender, ViewEventArgs<PostalCodeViewSearchDto> e)
        {
            switch (hidFlag.Value.ToEnum<OperationsAddressInputType>())
            {
                case OperationsAddressInputType.Origin:
                    operationsAddressInputOrigin.LoadPostalCode(e.Argument);
                    break;
                case OperationsAddressInputType.Destination:
                    operationsAddressInputDestination.LoadPostalCode(e.Argument);
                    break;
            }
            postalCodeFinder.Visible = false;
            postalCodeFinder.Reset();
            hidFlag.Value = string.Empty;
        }



        protected void OnVendorPerformanceClose(object sender, EventArgs e)
        {
            vpscVendorPerformance.Visible = false;
        }

        protected void OnViewVendorPerformanceStatisticsClicked(object sender, ImageClickEventArgs e)
        {
            vpscVendorPerformance.Visible = true;
            vpscVendorPerformance.ServiceMode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>();
            vpscVendorPerformance.LoadVendorPerformanceSummary(hidPrimaryVendorId.Value.ToLong());
        }

        protected void OnViewAdditionalVendorPerformanceStatisticsClicked(object sender, ImageClickEventArgs e)
        {
            vpscVendorPerformance.Visible = true;
            vpscVendorPerformance.ServiceMode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>();
            vpscVendorPerformance.LoadVendorPerformanceSummary(((Control)sender).FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong());
        }



        protected void OnPaymentEntryCloseClicked(object sender, EventArgs e)
        {
            pecPayment.Visible = false;
        }

        protected void OnPaymentEntryProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
        {
            DisplayMessages(e.Argument);
        }

        protected void OnPaymentEntryPaymentSuccessfullyProcessed(object sender, EventArgs e)
        {
            pecPayment.Clear();
            pecPayment.Visible = false;
            var amountPaidToShipment = new LoadOrder(hidLoadOrderId.Value.ToLong()).GetAmountPaidToLoadOrderViaMiscReceipts();
            hidMiscReceiptsRelatedToLoadOrderExist.Value = (amountPaidToShipment > 0).ToString();
            txtCollectedPaymentAmount.Text = amountPaidToShipment.GetString();
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));
        }

        protected void OnPaymentRefundCloseClicked(object sender, EventArgs e)
        {
            prcRefundPayments.Visible = false;
        }

        protected void OnPaymentRefundProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
        {
            DisplayMessages(e.Argument);
        }

        protected void OnRefundPaymentsPaymentRefundProcessed(object sender, EventArgs e)
        {
            prcRefundPayments.Clear();
            prcRefundPayments.Visible = false;
            var amountPaidToLoadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong()).GetAmountPaidToLoadOrderViaMiscReceipts();
            hidMiscReceiptsRelatedToLoadOrderExist.Value = (amountPaidToLoadOrder > 0).ToString();
            txtCollectedPaymentAmount.Text = amountPaidToLoadOrder.GetString();
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));
        }



        protected void OnShipmentFinderItemSelected(object sender, ViewEventArgs<Shipment> e)
        {
            var loadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong(), false);
            if (UnLock != null && !loadOrder.IsNew)
                UnLock(this, new ViewEventArgs<LoadOrder>(loadOrder));
            _roundTrip = (bool)PageStore["roundTrip"];
            LoadLoadOrder(e.Argument.ToLoadOrder(_roundTrip));
            SetEditStatus(true);
            shipmentFinder.Visible = false;
            PageStore["roundTrip"] = null;
        }

        protected void OnShipmentFinderItemCancelled(object sender, EventArgs e)
        {
            shipmentFinder.Visible = false;
        }



        protected void OnCloseDatAssetClicked(object sender, EventArgs e)
        {
            var loadOrder = new LoadOrder(hidLoadOrderId.Value.ToLong());

            var datPosting = hidLoadOrderId.Value.ToLong() == default(long)
                                 ? null
                                 : new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(
                                     loadOrder.LoadOrderNumber, loadOrder.TenantId);

            // DAT does not allow us to change these in an Update Request, so disallow the user from changing them
            btnEditEquipment.Enabled = datPosting == null;
            pnlHazMat.Enabled = datPosting == null;
            pnlOriginLocation.Enabled = datPosting == null;
            pnlDestinationLocation.Enabled = datPosting == null;
            pnlItems.Enabled = datPosting == null;

            pnlDATAssetPosted.Visible = datPosting != null;
            pnlDATAssetExpired.Visible = datPosting != null && DateTime.Now >= datPosting.ExpirationDate;

            txtDatAssetId.Text = datPosting == null ? string.Empty : datPosting.AssetId;
            hidDatAssetId.Value = datPosting == null ? string.Empty : datPosting.Id.ToString();

            hidFlag.Value = string.Empty;
            datLoadboardAssetControl.Visible = false;
        }

        protected void OnModifyDatAssetClicked(object sender, EventArgs e)
        {
            datLoadboardAssetControl.LoadDatAssetForLoadOrder(hidLoadOrderId.Value.ToLong());
            datLoadboardAssetControl.Visible = true;
        }


        protected void OnFindJobClicked(object sender, EventArgs e)
        {
            jobFinder.Visible = true;
        }
        
        protected void OnClearJobClicked(object sender, EventArgs e)
        {
            hidJobId.Value = string.Empty;
            txtJobNumber.Text = string.Empty;
            txtJobStep.Text = string.Empty;
        }

        protected void OnJobNumberTextChanged(object sender, EventArgs e)
        {
            var jobNumber = txtJobNumber.Text;

            var job = new JobSearch().FetchJobByJobNumber(jobNumber, ActiveUser.TenantId);

            if (job == null)
            {
                hidJobId.Value = string.Empty;

                txtJobNumber.Text = string.Empty;
                txtJobStep.Text = string.Empty;
                return;
            }

            hidJobId.Value = job.Id.ToString();
            txtJobStep.Text = string.Empty;
        }

        protected void OnJobFinderItemSelected(object sender, ViewEventArgs<Job> e)
        {
            var job = e.Argument;

            hidJobId.Value = job.Id.ToString();

            txtJobNumber.Text = job.JobNumber;
            txtJobStep.Text = string.Empty;

            jobFinder.Visible = false;
        }

        protected void OnJobFinderItemCancelled(object sender, EventArgs e)
        {
            jobFinder.Visible = false;
        }
    }
}