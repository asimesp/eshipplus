﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogisticsPlus.Eship.WebApplication.Members.Operations {
    
    
    public partial class JobView {
        
        /// <summary>
        /// memberToolBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.MainToolbarControl memberToolBar;
        
        /// <summary>
        /// imgPageImageLogo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imgPageImageLogo;
        
        /// <summary>
        /// ridcJobNumberIdentity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.RecordIdentityDisplayControl ridcJobNumberIdentity;
        
        /// <summary>
        /// ridcJobCustomerNameIdentity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.RecordIdentityDisplayControl ridcJobCustomerNameIdentity;
        
        /// <summary>
        /// pnlResolvedAlert control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlResolvedAlert;
        
        /// <summary>
        /// litMessage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litMessage;
        
        /// <summary>
        /// litErrorMessages control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litErrorMessages;
        
        /// <summary>
        /// hidJobId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidJobId;
        
        /// <summary>
        /// jobFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.JobFinderControl jobFinder;
        
        /// <summary>
        /// customerFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.CustomerFinderControl customerFinder;
        
        /// <summary>
        /// shipmentFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ShipmentFinderControl shipmentFinder;
        
        /// <summary>
        /// serviceTicketFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ServiceTicketFinderControl serviceTicketFinder;
        
        /// <summary>
        /// loadOrderFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.LoadOrderFinderControl loadOrderFinder;
        
        /// <summary>
        /// tabJob control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabContainer tabJob;
        
        /// <summary>
        /// tabDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabDetails;
        
        /// <summary>
        /// pnlDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlDetails;
        
        /// <summary>
        /// txtJobNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtJobNumber;
        
        /// <summary>
        /// txtCreatedByUser control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCreatedByUser;
        
        /// <summary>
        /// hidCustomerId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidCustomerId;
        
        /// <summary>
        /// txtCustomerNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCustomerNumber;
        
        /// <summary>
        /// rfvCustomer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvCustomer;
        
        /// <summary>
        /// btnFindCustomer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton btnFindCustomer;
        
        /// <summary>
        /// txtCustomerName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCustomerName;
        
        /// <summary>
        /// aceCustomer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.AutoCompleteExtender aceCustomer;
        
        /// <summary>
        /// ddlJobStatus control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlJobStatus;
        
        /// <summary>
        /// txtDateCreated control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtDateCreated;
        
        /// <summary>
        /// txtExternalReference1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtExternalReference1;
        
        /// <summary>
        /// txtExternalReference2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtExternalReference2;
        
        /// <summary>
        /// txtEstimateCost control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtEstimateCost;
        
        /// <summary>
        /// txtActualCost control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtActualCost;
        
        /// <summary>
        /// txtDiffCost control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtDiffCost;
        
        /// <summary>
        /// txtEstimateRevenue control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtEstimateRevenue;
        
        /// <summary>
        /// txtActualRevenue control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtActualRevenue;
        
        /// <summary>
        /// txtDiffRevenue control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtDiffRevenue;
        
        /// <summary>
        /// txtEstimateProfit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtEstimateProfit;
        
        /// <summary>
        /// txtActualProfit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtActualProfit;
        
        /// <summary>
        /// txtDiffProfit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtDiffProfit;
        
        /// <summary>
        /// txtGPMEstimate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtGPMEstimate;
        
        /// <summary>
        /// txtGPMActual control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtGPMActual;
        
        /// <summary>
        /// txtTotalOutstanding control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtTotalOutstanding;
        
        /// <summary>
        /// txtInvoiceCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtInvoiceCount;
        
        /// <summary>
        /// txtCreditInvoicesCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCreditInvoicesCount;
        
        /// <summary>
        /// txtSupplementalInvoicesCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtSupplementalInvoicesCount;
        
        /// <summary>
        /// txtVendorBillsCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtVendorBillsCount;
        
        /// <summary>
        /// txtCreditVendorBillsCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCreditVendorBillsCount;
        
        /// <summary>
        /// txtShipmentCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtShipmentCount;
        
        /// <summary>
        /// txtServiceTicketCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtServiceTicketCount;
        
        /// <summary>
        /// txtLoadOrderCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtLoadOrderCount;
        
        /// <summary>
        /// tabJobComponents control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabJobComponents;
        
        /// <summary>
        /// pnlJobComponents control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlJobComponents;
        
        /// <summary>
        /// btnAddServiceTicket control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAddServiceTicket;
        
        /// <summary>
        /// btnAddShipment control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAddShipment;
        
        /// <summary>
        /// btnAddLoadOrder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAddLoadOrder;
        
        /// <summary>
        /// btnClearAll control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnClearAll;
        
        /// <summary>
        /// upJobComponents control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel upJobComponents;
        
        /// <summary>
        /// lstJobComponents control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView lstJobComponents;
        
        /// <summary>
        /// tabVendorBills control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabVendorBills;
        
        /// <summary>
        /// pnlVendorBills control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlVendorBills;
        
        /// <summary>
        /// lstVendorBillDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView lstVendorBillDetails;
        
        /// <summary>
        /// tabInvoices control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabInvoices;
        
        /// <summary>
        /// pnlInvoices control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlInvoices;
        
        /// <summary>
        /// lstInvoiceDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView lstInvoiceDetails;
        
        /// <summary>
        /// tabDocuments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabDocuments;
        
        /// <summary>
        /// pnlDocuments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlDocuments;
        
        /// <summary>
        /// btnAddDocument control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAddDocument;
        
        /// <summary>
        /// btnClearDocuments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnClearDocuments;
        
        /// <summary>
        /// lstDocuments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView lstDocuments;
        
        /// <summary>
        /// pnlEditDocument control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlEditDocument;
        
        /// <summary>
        /// ImageButton5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ImageButton5;
        
        /// <summary>
        /// txtDocumentName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtDocumentName;
        
        /// <summary>
        /// hiddenDateCreated control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hiddenDateCreated;
        
        /// <summary>
        /// tamleDocumentDescription control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.TextAreaMaxLengthExtender tamleDocumentDescription;
        
        /// <summary>
        /// txtDocumentDescription control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtDocumentDescription;
        
        /// <summary>
        /// ddlDocumentTag control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CachedObjectDropDownList ddlDocumentTag;
        
        /// <summary>
        /// hidDocumentTagId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidDocumentTagId;
        
        /// <summary>
        /// hidLocationPath control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidLocationPath;
        
        /// <summary>
        /// fupLocationPath control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload fupLocationPath;
        
        /// <summary>
        /// litLocationPath control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litLocationPath;
        
        /// <summary>
        /// tabAuditLog control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabAuditLog;
        
        /// <summary>
        /// auditLogs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.AuditLogControl auditLogs;
        
        /// <summary>
        /// pnlSelectShipments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlSelectShipments;
        
        /// <summary>
        /// lblSelectShipmentsPopupTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblSelectShipmentsPopupTitle;
        
        /// <summary>
        /// ibtnClose control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ibtnClose;
        
        /// <summary>
        /// tfheSelectShipmentsTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.FreeHeaderExtender.TableFreezeHeaderExtender tfheSelectShipmentsTable;
        
        /// <summary>
        /// chkSelectAllRecords control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.AltUniformCheckBox chkSelectAllRecords;
        
        /// <summary>
        /// lstShipments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView lstShipments;
        
        /// <summary>
        /// btnSelectShipmentsAction control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSelectShipmentsAction;
        
        /// <summary>
        /// btnEditItemClose control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnEditItemClose;
        
        /// <summary>
        /// imgWarning control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imgWarning;
        
        /// <summary>
        /// message control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal message;
        
        /// <summary>
        /// pnlDimScreen control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlDimScreen;
        
        /// <summary>
        /// messageBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.MessageBoxControl messageBox;
        
        /// <summary>
        /// upHiddens control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel upHiddens;
        
        /// <summary>
        /// hidFlag control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidFlag;
        
        /// <summary>
        /// hidEditingIndex control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidEditingIndex;
        
        /// <summary>
        /// hidFilesToDelete control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidFilesToDelete;
        
        /// <summary>
        /// hidNotificationsRequired control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidNotificationsRequired;
    }
}
