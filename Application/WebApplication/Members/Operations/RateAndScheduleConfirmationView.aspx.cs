﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using LogisticsPlus.Eship.Smc.Eva;
using Newtonsoft.Json;
using P44SDK.V4.Model;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
	public partial class RateAndScheduleConfirmationView : MemberPageBase, IRateAndScheduleConfirmationView
	{
		public static string PageAddress
		{
			get { return "~/Members/Operations/RateAndScheduleConfirmationView.aspx"; }
		}

		public override ViewCode PageCode
		{
			get { return ViewCode.RateAndSchedule; }
		}

		public override string SetPageIconImage
		{
			set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
		}

		public event EventHandler<ViewEventArgs<Shipment>> UpdateShipmentDocumentsAndTracking;
		public event EventHandler<ViewEventArgs<Shipment>> UpdateShipmentCheckCalls;
		public event EventHandler<ViewEventArgs<Shipment>> UpdateShipmentNotes;
		public event EventHandler<ViewEventArgs<Project44DispatchResponse>> SaveProject44DispatchRequestResponse;
		public event EventHandler<ViewEventArgs<SMC3TrackRequestResponse>> SaveSmc3TrackRequestResponse;
		public event EventHandler<ViewEventArgs<SMC3DispatchRequestResponse>> SaveSmc3DispatchRequestResponse;

		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;
			if (!messages.HasErrors() && !messages.HasWarnings()) return;

			pnlProcessError.Visible = true;
			litProcessError.Text = string.Join(WebApplicationConstants.HtmlBreak, messages.Select(m => m.Message).ToArray());
		}



		private void InitiateDispatch(VendorCommunication vcom, Shipment shipment, Vendor primaryVendor)
		{
			try
			{
				var dispatchRequest = vcom.GetDispatchRequest(shipment, primaryVendor);

				var evaServiceHandler = new EvaServiceHandler(WebApplicationSettings.SmcEvaSettings);
				var trackResponse = evaServiceHandler.InitiateDispatch(dispatchRequest);

				var dispatchRequestResponse = new SMC3DispatchRequestResponse
					{
						TransactionId = trackResponse.transactionID,
						ShipmentId = shipment.Id,
						RequestData = JsonConvert.SerializeObject(dispatchRequest),
						ResponseData = JsonConvert.SerializeObject(trackResponse),
						Scac = primaryVendor.Scac,
						DateCreated = DateTime.Now,
					};

				if (SaveSmc3DispatchRequestResponse != null)
					SaveSmc3DispatchRequestResponse(this, new ViewEventArgs<SMC3DispatchRequestResponse>(dispatchRequestResponse));
			}
			catch (Exception ex)
			{
				DisplayMessages(new List<ValidationMessage>
					{
						ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message)
					});
			}
		}

		private void InitiateTracking(VendorCommunication vcom, Shipment shipment, Vendor primaryVendor)
		{
			try
			{
				var trackRequest = vcom.GetTrackRequest(shipment, primaryVendor);

				var evaServiceHandler = new EvaServiceHandler(WebApplicationSettings.SmcEvaSettings);
				var trackResponse = evaServiceHandler.InitiateTracking(trackRequest);

				var trackRequestResponse = new SMC3TrackRequestResponse
				{
					TransactionId = trackResponse.transactionID,
					ShipmentId = shipment.Id,
					RequestData = JsonConvert.SerializeObject(trackRequest),
					ResponseData = JsonConvert.SerializeObject(trackResponse),
					Scac = primaryVendor.Scac,
					DateCreated = DateTime.Now,
				};

				if (SaveSmc3TrackRequestResponse != null)
					SaveSmc3TrackRequestResponse(this, new ViewEventArgs<SMC3TrackRequestResponse>(trackRequestResponse));
			}
			catch (Exception ex)
			{
				DisplayMessages(new List<ValidationMessage>
					{
						ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message)
					});
			}
		}

	    private void InitiateProject44Dispatch(VendorCommunication vcom, Shipment shipment, Vendor primaryVendor)
	    {
	        try
	        {
                
	            var p44Settings = new Project44ServiceSettings
	            {
	                Hostname = WebApplicationSettings.Project44Host,
	                Username = WebApplicationSettings.Project44Username,
	                Password = WebApplicationSettings.Project44Password,
	                PrimaryLocationId = WebApplicationSettings.Project44PrimaryLocationId
	            };

	            var p44Wrapper = new Project44Wrapper(p44Settings);
	            var dispatchResponse = p44Wrapper.DispatchShipment(shipment, primaryVendor.Scac);

	            if (dispatchResponse == null)
	            {
	                var messages = p44Wrapper.ErrorMessages.Select(e => ValidationMessage.Error(e)).ToList();
	                DisplayMessages(messages);

	                var message = string.Format("Automatic dispatch via Project 44 failed with message: {0}{0}{1}{0}{0} Please contact carrier directly!",
	                    Environment.NewLine, messages.Select(m => m.Message).ToArray().NewLineJoin());

                    // Add a note to inform operations that dispatch failed
                    // If message is greater 500 character break up in necessary number of notes

	                try
	                {
	                    var notes = new List<ShipmentNote>();
	                    var msg = string.Empty;
	                    do
	                    {
	                        if (message.Length > 500)
	                        {
	                            var positionToBreak = message.FindLastOccurance(" ", 490);// only 490 max to give room for msg #

                                msg = message.Substring(0, positionToBreak); 
	                           
	                            message = message.Substring(msg.Length);
	                        }
	                        else
	                        {
	                            msg = message;
	                            message = string.Empty;
	                        }

	                        notes.Add(new ShipmentNote
	                        {
	                            Tenant = shipment.Tenant,
	                            Type = NoteType.Critical,
	                            Archived = false,
	                            Message = msg,
	                            Shipment = shipment,
	                            Classified = true,
	                            User = ActiveUser
	                        });

                            if(notes.Count > 1)
                                for (var i = 0; i < notes.Count; i++)
                                    notes[i].Message = $"{i + 1}/{notes.Count}: {notes[i].Message}";

	                    } while (!string.IsNullOrEmpty(message));

	                    shipment.Notes.AddRange(notes);
	                }
	                catch (Exception ex)
	                {
	                    ErrorLogger.LogError(ex, Context);
	                }


	                if (UpdateShipmentNotes != null)
	                    UpdateShipmentNotes(this, new ViewEventArgs<Shipment>(shipment));

                    return;
	            }

                var trackRequestResponse = new Project44DispatchResponse
                {
	                Project44Id = dispatchResponse.Id.GetString(),
	                ShipmentId = shipment.Id,
                    UserId = ActiveUser.Id,
                    ResponseData = JsonConvert.SerializeObject(dispatchResponse),
	                Scac = primaryVendor.Scac,
	                DateCreated = DateTime.Now,
	            };

	            if (SaveProject44DispatchRequestResponse != null)
	                SaveProject44DispatchRequestResponse(this, new ViewEventArgs<Project44DispatchResponse>(trackRequestResponse));

	            if (dispatchResponse.ShipmentIdentifiers != null &&
	                dispatchResponse.ShipmentIdentifiers.Any(i => i.Type == LtlShipmentIdentifier.TypeEnum.PICKUP))
	            {
	                // add pickup check call
	                var pickupCheckCall = new CheckCall
	                {
	                    Tenant = shipment.Tenant,
	                    CallDate = DateTime.Now,
	                    CallNotes = dispatchResponse.ShipmentIdentifiers.First(i => i.Type == LtlShipmentIdentifier.TypeEnum.PICKUP).Value,
	                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
	                    EventDate = dispatchResponse.PickupDateTime.ToDateTime(),
	                    Shipment = shipment,
	                    User = ActiveUser
	                };

	                shipment.CheckCalls.Add(pickupCheckCall);


	                if (UpdateShipmentCheckCalls != null)
	                    UpdateShipmentCheckCalls(this, new ViewEventArgs<Shipment>(shipment));
                }
            }
	        catch (Exception ex)
	        {
	            DisplayMessages(new List<ValidationMessage>
	            {
	                ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message)
	            });
	        }
	    }




        private void ProcessShipmentTransfer(long id)
        {
            hidTransferId.Value = id.ToString();

            var shipment = new Shipment(id);
            litShipmentNumber.Text = shipment.ShipmentNumber;
            hypViewBol.NavigateUrl = DocumentViewer.GenerateShipmentBolLink(shipment);

            pnlShipmentSubmission.Visible = true;
            lnkShipmentNumber.Visible = ActiveUser.HasAccessTo(ViewCode.ShipmentDashboard, ViewCode.CustomerLoadDashboard);

			// check for SMC3 EVA services
	        var primaryVendor = shipment.Vendors.FirstOrDefault(v => v.Primary).Vendor;
	        var vcom = primaryVendor.Communication;

            if (vcom != null && vcom.SMC3EvaEnabled && shipment.ServiceMode == ServiceMode.LessThanTruckload)
            {
				// dispatch
				if (vcom.SMC3EvaSupportsDispatch)
				{
					InitiateDispatch(vcom, shipment, primaryVendor);
				}

				// tracking
	            if (vcom.SMC3EvaSupportsTracking)
	            {
		            InitiateTracking(vcom, shipment, primaryVendor);
	            }
            }

            if (vcom != null && vcom.Project44Enabled && shipment.ServiceMode == ServiceMode.LessThanTruckload)
            {
                //dispatch
                if (vcom.Project44DispatchEnabled)
                {
                    InitiateProject44Dispatch(vcom, shipment, primaryVendor);
                }
            }


            // small package labels etc
            if (shipment.ServiceMode == ServiceMode.SmallPackage)
            {
                var errs = this.FinalizePackageShipmentBooking(shipment);
                if (errs.Any())
                {
                    DisplayMessages(errs);
                    return;
                }

                if (shipment.Documents.Any()) lnkShippingLabels.Text = "View Shipping Labels/Documents";

                if (UpdateShipmentDocumentsAndTracking != null)
                    UpdateShipmentDocumentsAndTracking(this, new ViewEventArgs<Shipment>(shipment));
            }

            // process new shipment notification
            this.NotifyNewShipment(shipment, true);
        }

		private void ProcessQuotedLoadTransfer(long id)
		{
			hidTransferId.Value = id.ToString();

			var quotedLoad = new LoadOrder(id);
			litQuoteLoadNumber.Text = quotedLoad.LoadOrderNumber;

			pnlQuotedLoadSubmission.Visible = true;

			lnkQuotedLoadNumber.Visible = ActiveUser.HasAccessTo(ViewCode.ShipmentDashboard);

			// process new quoted load notification
			this.NotifyLoadOrderEvent(quotedLoad);
		}

		private void ProcessOfferedLoadTransfer(long id)
		{
			hidTransferId.Value = id.ToString();

			var offeredLoad = new LoadOrder(id);
			litOfferedLoadNumber.Text = offeredLoad.LoadOrderNumber;

			pnlOfferedLoad.Visible = true;

			lnkOfferedLoadNumber.Visible = ActiveUser.HasAccessTo(ViewCode.CustomerLoadDashboard);

			// process new offered load notification
			this.NotifyLoadOrderEvent(offeredLoad);
		}



		protected void Page_Load(object sender, EventArgs e)
		{
			new RateAndScheduleConfirmationHandler(this).Initialize();

			if (IsPostBack) return;

			try
			{
				if (Session[WebApplicationConstants.RateAndScheduleTransferType] == null)
				{
					pnlNullTransfer.Visible = true;
					return;
				}

				var id = Session[WebApplicationConstants.RateAndScheduleTransferId].ToLong();
				switch (Session[WebApplicationConstants.RateAndScheduleTransferType].GetString())
				{
					case WebApplicationConstants.RateAndScheduleShipmentTransfer:
						ProcessShipmentTransfer(id);
						break;
					case WebApplicationConstants.RateAndScheduleQuotedLoadTransfer:
						ProcessQuotedLoadTransfer(id);
						break;
					case WebApplicationConstants.RateAndScheduleOfferedLoadTransfer:
						ProcessOfferedLoadTransfer(id);
						break;
				}

				Session[WebApplicationConstants.RateAndScheduleTransferType] = null;
				Session[WebApplicationConstants.RateAndScheduleTransferId] = null;
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
				pnlSystemError.Visible = true;
			}
		}

		protected void OnDocumentDisplayClosed(object sender, EventArgs e)
		{
			documentDisplay.Visible = false;
		}



		protected void OnOfferedLoadNumberClicked(object sender, EventArgs e)
		{
			var offeredLoad = new LoadOrder(hidTransferId.Value.ToLong());
			if (offeredLoad.KeyLoaded)
			{
				var criteria = new SearchDefaultsItem
				{
					Columns = new List<ParameterColumn> { OperationsSearchFields.ShipmentNumber.ToParameterColumn() },
					SortAscending = true,
					SortBy = null,
					Type = SearchDefaultsItemType.Shipments
				};
				criteria.Columns[0].DefaultValue = offeredLoad.LoadOrderNumber;
				criteria.Columns[0].Operator = Operator.Equal;
				Session[WebApplicationConstants.ShipmentDashboardSearchCriteria] = criteria;
			}

			if (ActiveUser.HasAccessTo(ViewCode.ShipmentDashboard)) Response.Redirect(ShipmentDashboardView.PageAddress);
			if (ActiveUser.HasAccessTo(ViewCode.CustomerLoadDashboard)) Response.Redirect(CustomerLoadDashboardView.PageAddress);
		}

		protected void OnQuotedLoadNumberClicked(object sender, EventArgs e)
		{
			var quotedLoad = new LoadOrder(hidTransferId.Value.ToLong());
			if (quotedLoad.KeyLoaded)
			{
				var criteria = new SearchDefaultsItem
				{
					Columns = new List<ParameterColumn> { OperationsSearchFields.ShipmentNumber.ToParameterColumn() },
					SortAscending = true,
					SortBy = null,
					Type = SearchDefaultsItemType.Shipments
				};
				criteria.Columns[0].DefaultValue = quotedLoad.LoadOrderNumber;
				criteria.Columns[0].Operator = Operator.Equal;
				Session[WebApplicationConstants.ShipmentDashboardSearchCriteria] = criteria;
			}

			if (ActiveUser.HasAccessTo(ViewCode.ShipmentDashboard)) Response.Redirect(ShipmentDashboardView.PageAddress);
			if (ActiveUser.HasAccessTo(ViewCode.CustomerLoadDashboard)) Response.Redirect(CustomerLoadDashboardView.PageAddress);
		}

		protected void OnViewQuoteClicked(object sender, EventArgs e)
		{
			var qoutedLoadOrder = new LoadOrder(hidTransferId.Value.ToLong());
			documentDisplay.Title = string.Format("{0} Quote", qoutedLoadOrder.LoadOrderNumber);
			documentDisplay.DisplayHtml(this.GenerateQuote(qoutedLoadOrder));
			documentDisplay.Visible = true;
		}



		protected void OnShipmentNumberClicked(object sender, EventArgs e)
		{
			var shipment = new Shipment(hidTransferId.Value.ToLong());
			if (shipment.KeyLoaded)
			{
				var criteria = new SearchDefaultsItem
				{
					Columns = new List<ParameterColumn> { OperationsSearchFields.ShipmentNumber.ToParameterColumn() },
					SortAscending = true,
					SortBy = null,
					Type = SearchDefaultsItemType.Shipments
				};
				criteria.Columns[0].DefaultValue = shipment.ShipmentNumber;
				criteria.Columns[0].Operator = Operator.Equal;
				Session[WebApplicationConstants.ShipmentDashboardSearchCriteria] = criteria;
			}

			if (ActiveUser.HasAccessTo(ViewCode.ShipmentDashboard))
				Response.Redirect(ShipmentDashboardView.PageAddress);
			else if (ActiveUser.HasAccessTo(ViewCode.CustomerLoadDashboard))
				Response.Redirect(CustomerLoadDashboardView.PageAddress);
		}

		protected void OnViewBillOfLadingClicked(object sender, EventArgs e)
		{
			var shipment = new Shipment(hidTransferId.Value.ToLong());
			documentDisplay.Title = string.Format("{0} Bill of Lading", shipment.ShipmentNumber);
			documentDisplay.DisplayHtml(this.GenerateBOL(shipment));
			documentDisplay.Visible = true;
		}

		protected void OnShippingLabelsClicked(object sender, EventArgs e)
		{
			shippingLabelGenerator.GenerateLabels(hidTransferId.Value.ToLong());
			shippingLabelGenerator.Visible = true;
		}



		protected void OnShippingLabelGeneratorCloseClicked(object sender, EventArgs e)
		{
			shippingLabelGenerator.Visible = false;
		}
	}
}