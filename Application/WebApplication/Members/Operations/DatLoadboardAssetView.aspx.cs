﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class DatLoadboardAssetView : MemberPageBase, IDatLoadboardAssetView
    {
		private const string DeleteFlag = "D";

        private SearchField SortByField
        {
            get { return OperationsSearchFields.DatLoadboardAssets.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        public override ViewCode PageCode { get { return ViewCode.DatLoadboard; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public static string PageAddress { get { return "~/Members/Operations/DatLoadboardAssetView.aspx"; } }


        public event EventHandler<ViewEventArgs<DatAssetSearchCriteria>> Search;
		public event EventHandler<ViewEventArgs<DatLoadboardAssetPosting,bool>> CheckDatDelete;
		public event EventHandler<ViewEventArgs<LoadOrder>> CheckDatUpdate;
		

        public void DisplaySearchResult(List<DatLoadboardAssetPosting> assets)
        {
            if (assets.Count == 0)
            {
                DisplayMessages(new[] { ValidationMessage.Information("No DAT Assets found") });
            }

            var datAssets = assets.Select(a =>
                {
                    var loadOrder = new LoadOrderSearch().FetchLoadOrderByLoadNumber(a.IdNumber, a.TenantId);
                    var loadExists = loadOrder != null;
                    return new
                        {
                            a.Id,
                            a.AssetId,
                            a.IdNumber,
                            a.DateCreated,
                            a.ExpirationDate,
                            LoadExists = loadExists
                        };
                });

            litRecordCount.Text = assets.BuildRecordCount();
            lstAssets.DataSource = datAssets;
            lstAssets.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void DatRemovalError()
        {
            messageBox.Button = MessageButton.YesNo;
            DisplayMessages(new List<ValidationMessage>()
                {
                    ValidationMessage.Error("There was an error removing the posting from DAT. Would you like to force the removal of the local asset?")
                });
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new DatAssetSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(DatAssetSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<DatAssetSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new DatLoadboardAssetHandler(this).Initialize();
			
            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set sort fields
            ddlSortBy.DataSource = OperationsSearchFields.DatLoadboardAssets.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = OperationsSearchFields.DatAssetIdNumber.Name;

            lbtnSortAssetId.CommandName = OperationsSearchFields.DatAssetIdNumber.Name;
            lbtnSortLoadNumber.CommandName = OperationsSearchFields.DatLoadOrderNumber.Name;
            lbtnSortDateCreated.CommandName = OperationsSearchFields.DatDateCreated.Name;
            lbtnSortExpirationDate.CommandName = OperationsSearchFields.DatExpirationDate.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : OperationsSearchFields.DefaultDatAssets.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

        }


        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = OperationsSearchFields.DatLoadboardAssets.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }

            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;

            hidDatAssetId.Value = imageButton.Parent.FindControl("hidAssetId").ToCustomHiddenField().Value;
	        hidFlag.Value = DeleteFlag;
            if (CheckDatDelete != null)
                CheckDatDelete(this, new ViewEventArgs<DatLoadboardAssetPosting,bool>(new DatLoadboardAssetPosting(hidDatAssetId.Value.ToLong(), true),false));

            DoSearchPreProcessingThenSearch(SortByField);
        }
		
	    private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.DatLoadboardAssets.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.DatLoadboardAssets);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }

        protected void OnMessageBoxYesClicked(object sender, EventArgs e)
        {
            hidFlag.Value = DeleteFlag;
            if (CheckDatDelete != null)
                CheckDatDelete(this, new ViewEventArgs<DatLoadboardAssetPosting, bool>(new DatLoadboardAssetPosting(hidDatAssetId.Value.ToLong(), true), true));

            DoSearchPreProcessingThenSearch(SortByField);
            messageBox.Button = MessageButton.Ok;
            messageBox.Visible = false;
        }

        protected void OnMessageBoxNoClicked(object sender, EventArgs e)
        {
            hidFlag.Value = string.Empty;
            messageBox.Button = MessageButton.Ok;
            DoSearchPreProcessingThenSearch(SortByField);
            messageBox.Visible = false;
        }
    }
}