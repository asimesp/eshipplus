﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ShipmentView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.ShipmentView" EnableEventValidation="false" %>


<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true" ShowUnlock="False"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="true"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked" OnUnlock="OnUnlockClicked"
        OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Shipment</h3>
            <div class="shipmentAlertSection">
                <asp:Panel ID="pnlPickupAlert" runat="server" CssClass="flag-shipment" Visible="False">
                    Late Pickup
                </asp:Panel>
                <asp:Panel ID="pnlDeliveryAlert" runat="server" CssClass="flag-shipment" Visible="False">
                    Late Delivery
                </asp:Panel>
                <asp:Panel ID="pnlCreditAlert" runat="server" CssClass="flag-shipment" Visible="False">
                    Credit Alert
                </asp:Panel>
                <asp:Panel ID="pnlPrimaryVendorInsuranceAlert" runat="server" CssClass="flag-shipment" Visible="False">
                    <asp:Literal runat="server" ID="litPrimaryVendorInsuranceAlert" Text="Vendor Insurance" />
                </asp:Panel>
                <asp:Panel ID="pnlCheckCallsAlert" runat="server" CssClass="flag-shipment" Visible="False">
                    Missing Check Call
                </asp:Panel>
                <asp:Panel ID="pnlCreatedInError" runat="server" CssClass="flag-shipment" Visible="False">
                    Created In Error
                </asp:Panel>
                <asp:Panel ID="pnlMacroPointTracking" runat="server" CssClass="flag-shipment" Visible="False">
                    <asp:LinkButton ID="lbtnMacroPointTracking" OnClick="OnMacroPointTrackingIconClicked" runat="server" Text="[Map]" CssClass="blue" />
                    <asp:Literal runat="server" ID="litMacroPointTrackingStatus" Text="MacroPoint Status" />
                </asp:Panel>
                <asp:Panel ID="pnlGuaranteedDeliveryServices" runat="server" CssClass="flag-shipment" Visible="False">
                    Guaranteed Delivery Services
                </asp:Panel>
                <asp:Panel ID="pnlExpeditedServices" runat="server" CssClass="flag-shipment" Visible="False">
                    Expedited Delivery Services
                </asp:Panel>
                <asp:Panel ID="pnlCashOnlyCustomer" runat="server" CssClass="flag-shipment" Visible="false">
                    Cash-Only Customer
                </asp:Panel>
            </div>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="litErrorMessages" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <eShip:ShipmentFinderControl runat="server" ID="shipmentFinder" Visible="false" OnItemSelected="OnShipmentFinderItemSelected"
            OnSelectionCancel="OnShipmentFinderItemCancelled" OpenForEditEnabled="true" EnableMultiSelection="False" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            OnlyActiveResults="True" Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:PrefixFinderControl ID="prefixFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OnSelectionCancel="OnPrefixFinderSelectionCancelled" OnItemSelected="OnPrefixFinderItemSelected" />
        <eShip:LibraryItemFinderControl EnableMultiSelection="True" runat="server" ID="libraryItemFinder"
            Visible="false" OnMultiItemSelected="OnLibraryItemFinderMultiItemSelected" OnSelectionCancel="OnLibraryItemFinderItemCancelled"
            OpenForEditEnabled="false" />
        <eShip:VendorFinderControl EnableMultiSelection="true" ID="vendorFinder" OnItemSelected="OnVendorFinderItemSelected" OnMultiItemSelected="OnVendorFinderMultiItemSelected"
            OnlyActiveResults="true" OnSelectionCancel="OnVendorFinderItemCancelled" OpenForEditEnabled="false"
            runat="server" Visible="false" />
        <eShip:AccountBucketFinderControl EnableMultiSelection="true" ID="accountBucketFinder" OnItemSelected="OnAccountBucketFinderItemSelected"
            OnSelectionCancel="OnAccountBucketFinderSelectionCancelled" OnMultiItemSelected="OnAccountBucketFinderMultiItemSelected" runat="server" ShowActiveRecordsOnly="True"
            Visible="False" />
        <eShip:RateSelectionDisplayControl ID="rateSelectionDisplay" runat="server" Visible="False"
            OnRateSelectionCancelled="OnRateSelectionDisplaySelectionCancelled" OnRateSelected="OnRateSelectionDisplayRateSelected"
            OnRatesLoaded="OnRateSelectionDisplayRatesLoaded" />
        <eShip:UserFinderControl ID="userFinder" runat="server" Visible="false" EnableMultiSelection="false"
            FilterForEmployees="False" MatchNonEmployeeDeptToActiveUser="True" OpenForEditEnabled="False" OnItemSelected="OnUserFinderItemSelected"
            OnSelectionCancel="OnUserFinderSelectionCancelled" />
        <eShip:VendorLaneHistoryBuySellControl runat="server" ID="vendorLaneHistoryBuySell" Visible="False"
            OnVendorLaneHistoryClose="OnVendorLaneHistoryClose" />
        <eShip:VendorRateAgreementControl runat="server" ID="vendorRateAgreement" OnClose="OnVendorRateAgreementClose"
            OnGenerate="OnVendorRateAgreementGenerate" Visible="false" />
        <eShip:ShippingLabelGeneratorControl runat="server" ID="shippingLabelGenerator" Visible="false"
            OnClose="OnShippingLabelGeneratorCloseClicked" />
        <eShip:QuickPayOptionFinderControl runat="server" ID="quickPayOptionFinder" OnMultiItemSelected="OnQuickPayOptionMultiItemSelected"
            Visible="false" OnSelectionCancel="OnQuickPayOptionSelectionCancelled" EnableMultiSelection="true" />
        <eShip:DocumentDisplayControl runat="server" ID="documentDisplay" Visible="false" OnClose="OnDocumentDisplayClosed" />
        <eShip:MacroPointOrderInputControl runat="server" ID="macroPointOrderInput" Visible="False" OnSelectionCancel="OnMacroPointOrderInputClosed" OnSubmitOrder="OnMacroPointOrderInputSubmitOrder" />
        <eShip:MacroPointTrackingMapControl runat="server" ID="macroPointTrackingMap" OnClose="OnMacroPointTrackingMapClosed" OnRequestLocationUpdate="OnMacroPointTrackingMapRequestLocationUpdate" />
        <eShip:AddressBookFinderControl runat="server" ID="addressBookFinder" Visible="False" OnItemSelected="OnAddressBookFinderItemSelected"
            OnSelectionCancel="OnAddressBookFinderItemCancelled" OpenForEditEnabled="false" />
        <eShip:PostalCodeFinderControl runat="server" ID="postalCodeFinder" Visible="False" OnItemSelected="OnPostalCodeFinderItemSelected"
            OnSelectionCancel="OnPostalCodeFinderItemCancelled" />
        <eShip:PaymentEntryControl runat="server" ID="pecPayment" Visible="False" CanModifyPaymentAmount="True" OnClose="OnPaymentEntryCloseClicked" OnProcessingMessages="OnPaymentEntryProcessingMessages"
            OnPaymentSuccessfullyProcessed="OnPaymentEntryPaymentSuccessfullyProcessed" />
        <eShip:PaymentRefundOrVoidControl runat="server" ID="prcRefundPayments" Visible="False" OnClose="OnPaymentRefundCloseClicked" OnProcessingMessages="OnPaymentRefundProcessingMessages"
            OnPaymentRefundProcessed="OnRefundPaymentsPaymentRefundProcessed" />
        <eShip:JobFinderControl runat="server" ID="jobFinder" Visible="false" OnItemSelected="OnJobFinderItemSelected"
            OnSelectionCancel="OnJobFinderItemCancelled" OpenForEditEnabled="False" />

        <asp:UpdatePanel runat="server" UpdateMode="Always">
            <ContentTemplate>
                <eShip:VendorPerformanceSummaryControl runat="server" ID="vpscVendorPerformance" Visible="False" OnCancel="OnVendorPerformanceClose" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="vpscVendorPerformance" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" OnYes="OnMessageBoxYesClicked" OnNo="OnMessabgeBoxNoClicked" />

        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <eShip:CustomHiddenField ID="hidShipmentId" runat="server" />

        <asp:Panel ID="pnlAboveDetails" runat="server" CssClass="rowgroup" DefaultButton="imgCustomerSearch">

            <div class="row">
                <div class="fieldgroup mr20 vlinedarkright">
                    <label class="label upper blue">Shipment Number:</label>
                    <eShip:CustomTextBox ID="txtShipmentNumber" runat="server" ReadOnly="True" CssClass="w150 disabled" />
                </div>
                <div class="fieldgroup mr20 vlinedarkright">
                    <label class="label upper blue">Customer:</label>
                    <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                    <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" OnTextChanged="OnCustomerNumberEntered" AutoPostBack="True" CssClass="w110" />
                    <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                    <eShip:CustomTextBox runat="server" ID="txtCustomerName" ReadOnly="True" CssClass="w260 disabled" />
                    <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                        EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                    <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                    ResolveUrl(CustomerView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    hidCustomerId.Value.UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                            : string.Empty %>
                </div>
                <div class="fieldgroup">
                    <label class="label upper blue">Status:</label>
                    <eShip:CustomTextBox runat="server" ID="txtStatus" ReadOnly="true" CssClass="w150 disabled" />
                    <eShip:CustomHiddenField runat="server" ID="hidStatus" />
                </div>
            </div>
        </asp:Panel>

        <ajax:TabContainer ID="tabShipment" runat="server" CssClass="ajaxCustom" OnClientActiveTabChanged="HandleAddItemButtonVisibility">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel ID="pnlDetails" runat="server">
                        <div class="rowgroup">
                            <div class="row">
                                <div class="fieldgroup mr10">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#<%= txtAccountBucketCode.ClientID %>').change(function () {
                                                var ids = new ControlIds();
                                                ids.AcctBucketCode = $('#<%= txtAccountBucketCode.ClientID %>').attr('id');
                                                ids.AcctBucketDesc = $('#<%= txtAccountBucketDescription.ClientID %>').attr('id');
                                                ids.AcctBucketId = $('#<%= hidAccountBucketId.ClientID %>').attr('id');
                                                ids.AcctBucketUnits = $('#<%= ddlAccountBucketUnit.ClientID %>').attr('id');
                                                ids.AcctBucketUnitId = $('#<%= hidAccountBucketUnitId.ClientID %>').attr('id');

                                                FindActiveAcctBucket('<%= ActiveUserTenantId %>', $('#<%= txtAccountBucketCode.ClientID %>').val(), ids);
                                            });
                                        });
                                    </script>

                                    <eShip:CustomHiddenField runat="server" ID="hidAccountBucketId" Value="" />
                                    <eShip:CustomHiddenField runat="server" ID="hidShipmentAccountBucketId" Value="" />
                                    <label class="wlabel blue">Account Bucket</label>
                                    <asp:Panel ID="pnlAccountBucketSearch" runat="server" DefaultButton="imgAccountBucketSearch">
                                        <eShip:CustomTextBox runat="server" ID="txtAccountBucketCode" CssClass="w100" Type="NotSet" />
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtAccountBucketCode"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <asp:ImageButton ID="imgAccountBucketSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnAccountBucketSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtAccountBucketDescription" CssClass="w180 disabled" Type="ReadOnly" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceAccountBucketCode" TargetControlID="txtAccountBucketCode" ServiceMethod="GetAccountBucketList" ServicePath="~/Services/eShipPlusWSv4P.asmx" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" DelimiterCharacters="" Enabled="True" />
                                    </asp:Panel>
                                </div>
                                <div class="fieldgroup vlinedarkright mr10">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#<%= ddlAccountBucketUnit.ClientID %>').change(function () {
                                                $('#<%= hidAccountBucketUnitId.ClientID %>').val($('#<%= ddlAccountBucketUnit.ClientID %>').val());
                                            });
                                        });
                                    </script>
                                    <label class="wlabel blue">Account Bucket Unit</label>
                                    <asp:HiddenField runat="server" ID="hidAccountBucketUnitId" />
                                    <eShip:CachedObjectDropDownList runat="server" ID="ddlAccountBucketUnit" CssClass="w130" Type="AccountBucketUnits" EnableChooseOne="True" DefaultValue="0" EnableNotApplicable="False" EnableNotApplicableLong="False" />
                                </div>
                                <div class="fieldgroup vlinedarkright mr10">
                                    <label class="wlabel blue">Service Mode</label>
                                    <asp:DropDownList ID="ddlServiceMode" runat="server" CssClass="w160" DataTextField="Text"
                                        DataValueField="Value" AutoPostBack="True" OnSelectedIndexChanged="OnServiceModeSelectedIndexChanged" />
                                </div>
                                <div class="fieldgroup">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#<%= txtPrefixCode.ClientID %>').change(function () {
                                                var ids = new ControlIds();
                                                ids.PrefixCode = $('#<%= txtPrefixCode.ClientID %>').attr('id');
                                                ids.PrefixDesc = $('#<%= txtPrefixDescription.ClientID %>').attr('id');
                                                ids.PrefixId = $('#<%= hidPrefixId.ClientID %>').attr('id');

                                                FindActivePrefix('<%= ActiveUserTenantId %>', $('#<%= txtPrefixCode.ClientID %>').val(), ids);
                                            });
                                        });
                                    </script>

                                    <eShip:CustomHiddenField runat="server" ID="hidPrefixId" Value="" />
                                    <label class="wlabel blue">Prefix</label>
                                    <asp:Panel runat="server" DefaultButton="imgPrefixSearch">
                                        <eShip:CustomTextBox runat="server" ID="txtPrefixCode" CssClass="w100" Type="NotSet" />
                                        <asp:ImageButton ID="imgPrefixSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnPrefixSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtPrefixDescription" CssClass="w150 disabled" Type="ReadOnly" />
                                        <ajax:AutoCompleteExtender runat="server" ID="acePrefixCode" TargetControlID="txtPrefixCode" ServiceMethod="GetPrefixList" ServicePath="~/Services/eShipPlusWSv4P.asmx" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" DelimiterCharacters="" Enabled="True" />
                                    </asp:Panel>
                                </div>
                                <div class="fieldgroup_s">
                                    <asp:CheckBox runat="server" ID="chkHidePrefix" CssClass="jQueryUniform" />
                                    <label class="comlabel blue">
                                        Hide<br />
                                        Prefix</label>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="rowgroup">
                            <div class="col_1_3">
                                <h5>Service Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                var controlId = $("[id$='ibtnViewVendorPerformanceStatistics']").attr('id');
                                                if (controlId != null) jsHelper.SetDoPostBackJsLink(controlId);
                                            });
                                        </script>
                                        <label class="wlabel">
                                            Primary Vendor&nbsp;
                                            <asp:ImageButton runat="server" ID="ibtnLogVendorRejection" ImageUrl="~/images/icons2/cog.png" ImageAlign="AbsMiddle" Width="20px" CssClass="middle"
                                                OnClick="OnLogPrimaryVendorRejectionClicked" CausesValidation="False" ToolTip="Clear vendor and log vendor rejection" />
                                            <asp:ImageButton runat="server" ID="ibtnViewVendorPerformanceStatistics" ImageUrl="~/images/icons2/statsBlue.png" OnClick="OnViewVendorPerformanceStatisticsClicked"
                                                ToolTip="See Vendor Performance Statistics" Visible="False" Width="20px" />
                                            <%= hidPrimaryVendorId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='20' class='middle' /></a>", 
                                                                            ResolveUrl(VendorView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidPrimaryVendorId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <eShip:CustomHiddenField runat="server" ID="hidPrimaryVendorId" Value="" />
                                        <eShip:CustomHiddenField runat="server" ID="hidPrimaryShipmentVendorId" Value="" />
                                        <asp:Panel runat="server" DefaultButton="imgVendorSearch">
                                            <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w80" OnTextChanged="OnVendorNumberEntered"
                                                AutoPostBack="True" Type="NotSet" />
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtVendorNumber"
                                                ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                            <asp:ImageButton ID="imgVendorSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                                CausesValidation="False" OnClick="OnVendorSearchClicked" />
                                            <eShip:CustomTextBox runat="server" ID="txtVendorName" CssClass="w220 disabled" ReadOnly="True" Type="NotSet" />
                                            <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" DelimiterCharacters="" Enabled="True" />
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Vendor Pro Number</label>
                                        <eShip:CustomTextBox runat="server" ID="txtPrimaryProNumber" CssClass="w160" MaxLength="50" Type="NotSet" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Vendor Failure Code</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlPrimaryFailureCode" CssClass="w160" Type="FailureCodes" EnableNotApplicableLong="True" DefaultValue="0" EnableChooseOne="False" EnableNotApplicable="False" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Vendor Failure Comments
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtPrimaryFailureComments" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtPrimaryFailureComments" TextMode="MultiLine" CssClass="w330" Type="NotSet" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Priority</label>
                                        <eShip:CachedObjectDropDownList ID="ddlPriority" Type="ShipmentPriority" EnableChooseOne="True" DefaultValue="0" runat="server" CssClass="w160" EnableNotApplicable="False" EnableNotApplicableLong="False" />
                                    </div>
                                    <div class="fieldgroup_s">
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $(jsHelper.AddHashTag('<%= chkAutoRated.ClientID %>')).attr("onclick", "return false;");
                                                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                                    $(jsHelper.AddHashTag('<%= chkAutoRated.ClientID %>')).attr("onclick", "return false;");
                                                });
                                            });


                                        </script>
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <eShip:AltUniformCheckBox runat="server" ID="chkAutoRated"
                                                    ToolTip="Auto Rated flag will be set to false if the origin or destination postal codes change, an item is modified, the customer is changed, or the vendor is changed" />
                                                <label class="comlabel">
                                                    Auto<br />
                                                    Rated
                                                </label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="fieldgroup_s">
                                        <asp:Panel runat="server" ID="pnlPartialTruckload">
                                            <asp:CheckBox runat="server" ID="chkIsPartialTruckLoad" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Is Partial<br />
                                                TruckLoad
                                            </label>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">PO Number</label>
                                        <eShip:CustomTextBox ID="txtPurchaseOrderNumber" runat="server" CssClass="w160" MaxLength="50" Type="NotSet" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Shipper Reference</label>
                                        <eShip:CustomTextBox ID="txtShipperReference" runat="server" CssClass="w160" MaxLength="50" Type="NotSet" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Shipper BOL</label>
                                        <eShip:CustomTextBox ID="txtShipperBol" runat="server" ReadOnly="True" CssClass="w160 disabled" Type="NotSet" />
                                    </div>
                                    <div class="fieldgroup">
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                var mileage = $("#<%= txtMileage.ClientID%>").val();
                                                var emptyMileage = $("#<%= txtEmptyMileage.ClientID%>").val();

                                                $("#<%= txtTotalMiles.ClientID%>").text(+mileage + +emptyMileage);
                                            });
                                        </script>
                                        <label class="wlabel">Total Miles</label>
                                        <eShip:CustomTextBox ID="txtTotalMiles" runat="server" CssClass="w160 disabled" ReadOnly="True" Type="NotSet" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            User &nbsp;
                                            <asp:Image runat="server" ID="imgContactInformation" ToolTip="Contact Information" ImageAlign="AbsMiddle"
                                                Width="20px" ImageUrl="~/images/icons2/eye-open.png" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtUser" runat="server" ReadOnly="True" CssClass="w160 disabled" Type="NotSet" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Date Created</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDateCreated" CssClass="w160 disabled" ReadOnly="True" Type="NotSet" />
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        $(jsHelper.AddHashTag('<%= imgContactInformation.ClientID %>')).on('mouseover mouseenter', function () {
                                            $(jsHelper.AddHashTag('contactInformationDiv')).slideDown();
                                        });

                                        $(jsHelper.AddHashTag('<%= imgCloseContactInformation.ClientID %>')).click(function () {
                                            $(jsHelper.AddHashTag('contactInformationDiv')).slideUp();
                                        });

                                    });
                                </script>
                                <div class="row mt20" id="contactInformationDiv" style="display: none;">
                                    <div class="wAuto mr20 shadow">
                                        <div class="popheader">
                                            <h4>Contact Information
                                            <asp:Image ID="imgCloseContactInformation" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                                runat="server" />
                                            </h4>
                                        </div>
                                        <table class="contain">
                                            <tr>
                                                <td class="text-right p_m0" style="width: 20%">
                                                    <label class="upper blue">Phone: </label>
                                                </td>
                                                <td class="p_m0">
                                                    <label>
                                                        <a id="lnkUserPhone" runat="server" />
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right p_m0">
                                                    <label class="upper blue">Mobile: </label>
                                                </td>
                                                <td class="p_m0">
                                                    <label>
                                                        <a id="lnkUserMobile" runat="server" />
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right p_m0">
                                                    <label class="upper blue">Fax: </label>
                                                </td>
                                                <td class="p_m0">
                                                    <label>
                                                        <a id="lnkUserFax" runat="server" />
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right p_m0">
                                                    <label class="upper blue">Email: </label>
                                                </td>
                                                <td class="p_m0">
                                                    <label>
                                                        <a id="lnkUserEmail" runat="server" />
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <h5>Pickup And Delivery Dates</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel w130">Desired Pickup Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDesiredPickupDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                    </div>
                                    <div class="fieldgroup">
                                        <script type="text/javascript">
                                            function OffsetLatePickupTimes() {
                                                var ids = new ControlIds();
                                                ids.TimesOfDayWindowStart = $(jsHelper.AddHashTag('<%= ddlEarlyPickup.ClientID %>')).attr('id');
                                                ids.TimesOfDayWindowEnd = $(jsHelper.AddHashTag('<%= ddlLatePickup.ClientID %>')).attr('id');

                                                OffsetDropDownListByTimeOfDay(false, ids);
                                            }
                                        </script>
                                        <label class="wlabel shrink mr10">Earliest Pickup</label>
                                        <asp:DropDownList ID="ddlEarlyPickup" runat="server" CssClass="w75" onchange="OffsetLatePickupTimes();" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel shrink">Latest Pickup</label>
                                        <asp:DropDownList ID="ddlLatePickup" runat="server" CssClass="w75" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel w130">
                                            <abbr title="Estimated">Est.</abbr>
                                            Delivery Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtEstimatedDeliveryDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                    </div>
                                    <asp:Panel runat="server" ID="pnlDelivery">
                                        <div class="fieldgroup">
                                            <script type="text/javascript">
                                                function OffsetLateDeliveryTimes() {
                                                    var ids = new ControlIds();
                                                    ids.TimesOfDayWindowStart = $(jsHelper.AddHashTag('<%= ddlEarlyDelivery.ClientID %>')).attr('id');
                                                    ids.TimesOfDayWindowEnd = $(jsHelper.AddHashTag('<%= ddlLateDelivery.ClientID %>')).attr('id');

                                                    OffsetDropDownListByTimeOfDay(false, ids);
                                                }
                                            </script>
                                            <label class="wlabel shrink">Earliest Delivery</label>
                                            <asp:DropDownList ID="ddlEarlyDelivery" runat="server" CssClass="w75" onchange="OffsetLateDeliveryTimes();" />
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel shrink">Latest Delivery</label>
                                            <asp:DropDownList ID="ddlLateDelivery" runat="server" CssClass="w75" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Actual Pickup Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtActualPickupDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                        <eShip:CustomTextBox runat="server" ID="txtActaulPickupTime" CssClass="w55" MaxLength="5" placeholder="99:99" Type="Time" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Actual Delivery Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtActualDeliveryDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                        <eShip:CustomTextBox runat="server" ID="txtActualDeliveryTime" CssClass="w55" MaxLength="5" placeholder="99:99" Type="Time" />
                                    </div>
                                </div>
                                <div class="row mt10">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $("#<%= btnEditServices.ClientID %>").click(function (e) {
                                                e.preventDefault();
                                                ShowPanel($("#<%= pnlServiceList.ClientID %>"));
                                            });

                                            $("#<%= btnEditEquipment.ClientID %>").click(function (e) {
                                                e.preventDefault();
                                                ShowPanel($("#<%= pnlEquipmentTypeList.ClientID %>"));
                                            });

                                            $("#<%= btnEquipmentTypesDone.ClientID %>").click(function (e) {
                                                e.preventDefault();
                                                HidePanels($("#<%= pnlEquipmentTypeList.ClientID %>"));
                                            });

                                            $("#<%= btnServicesDone.ClientID %>").click(function () {
                                                HidePanels($("#<%= pnlServiceList.ClientID %>"));
                                            });

                                            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                                $("#<%= btnEditServices.ClientID %>").click(function (e) {
                                                    e.preventDefault();
                                                    ShowPanel($("#<%= pnlServiceList.ClientID %>"));
                                                });

                                                $("#<%= btnServicesDone.ClientID %>").click(function () {
                                                    HidePanels($("#<%= pnlServiceList.ClientID %>"));
                                                });
                                            });
                                        });

                                        function ShowPanel($panel) {
                                            $($panel).fadeIn('fast');
                                            $("#<%= pnlDimScreenJS.ClientID %>").fadeIn('fast');
                                        }

                                        function HidePanels($panel) {
                                            $($panel).fadeOut('fast');
                                            $("#<%= pnlDimScreenJS.ClientID %>").fadeOut('fast');
                                        }

                                        function getSelected(list, lblResults, lblValueText) {
                                            var checkboxes = $('[id$=' + list + '] input[id$="chkSelected"]');
                                            var checkedText = jsHelper.EmptyString;

                                            for (var i = 0; i < checkboxes.length; i++) {
                                                if (jsHelper.IsChecked(checkboxes[i].id)) {
                                                    checkedText += ($(jsHelper.AddHashTag(checkboxes[i].id)).parent().parent().parent().parent().find('[id$="' + lblValueText + '"]').text() + ",");
                                                }
                                            }
                                            if (!jsHelper.IsNullOrEmpty(checkedText)) checkedText = checkedText.substr(0, checkedText.length - 1);
                                            $('[id*=hid' + lblResults + '_txtHid]').val(checkedText);
                                            $('[id$=lbl' + lblResults + ']').text(checkedText);
                                        }
                                    </script>
                                    <h5>Equipment
                                        <asp:Button ID="btnEditEquipment" Text="Edit Equipment" runat="server" CausesValidation="False" CssClass="mr17 right" />
                                    </h5>
                                    <div id="equipment" class="row mb10">
                                        <eShip:CustomHiddenField ID="hidEquipment" runat="server" Value="" />
                                        <label>
                                            <asp:Label ID="lblEquipment" runat="server" />
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:UpdatePanel runat="server" ID="upPnlServices">
                                        <ContentTemplate>
                                            <h5>Services
                                            <asp:Button ID="btnEditServices" Text="Edit Services" runat="server" CausesValidation="False" CssClass="mr17 right" />
                                            </h5>
                                            <div id="services" class="row">
                                                <eShip:CustomHiddenField ID="hidServices" runat="server" />
                                                <label>
                                                    <asp:Label ID="lblServices" runat="server" />
                                                </label>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="col_1_3">
                                <div class="row">
                                    <eShip:CustomHiddenField ID="hidOriginKey" runat="server" Value="" />
                                    <eShip:OperationsAddressInputControl ID="operationsAddressInputOrigin" runat="server" Type="Origin"
                                        OnFindPostalCode="OnAddressInputFindPostalCode" OnAddAddressFromAddressBook="OnAddressInputAddAddressFromAddressBook" />
                                </div>
                                <div class="row">
                                    <asp:CheckBox runat="server" ID="chkSaveOrigin" CssClass="jQueryUniform" />
                                    <label>Save origin location to address book on shipment save</label>
                                </div>
                                <div class="row mt5" Visible="false" id="divNotifyPrimaryOrigin" runat="server">
                                    <asp:CheckBox runat="server" ID="chkNotifyPrimaryOriginContactOfShipmentUpdates" CssClass="jQueryUniform" />
                                    <label>Notify primary origin contact of shipment updates</label>
                                </div>
                            </div>
                            <div class="col_1_3 wd">
                                <div class="row">
                                    <eShip:CustomHiddenField ID="hidDestinationKey" runat="server" Value="" />
                                    <eShip:OperationsAddressInputControl ID="operationsAddressInputDestination" runat="server"
                                        Type="Destination" OnFindPostalCode="OnAddressInputFindPostalCode" OnAddAddressFromAddressBook="OnAddressInputAddAddressFromAddressBook" />
                                </div>
                                <div class="row">
                                    <asp:CheckBox runat="server" ID="chkSaveDestination" CssClass="jQueryUniform" />
                                    <label>Save destination location to address book on shipment save</label>
                                </div>
                                <div class="row mt5" Visible="false" id="divNotifyPrimaryDestination" runat="server">
                                    <asp:CheckBox runat="server" ID="chkNotifyPrimaryDestinationContactOfShipmentUpdates" CssClass="jQueryUniform" />
                                    <label>Notify primary destination contact of shipment updates</label>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlServiceList" runat="server" CssClass="popup popup-position-fixed top50" Style="display: none;">
                                <div class="popheader">
                                    <h4>Services</h4>
                                </div>
                                <div class="row finderScroll">
                                    <div class="fieldgroup pl20 pr20 pt20">
                                        <asp:Repeater runat="server" ID="rptServices">
                                            <HeaderTemplate>
                                                <ul class="twocol_list sm_chk" id="ulServices">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li>
                                                    <eShip:CustomHiddenField runat="server" ID="hidServiceId" Value='<%# Eval("Value") %>' />
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkSelected" />
                                                    <label class="lhInherit">
                                                        <asp:Label runat="server" ID="lblServiceCode" Text='<%# Eval("Text") %>' CssClass="fs85em" />
                                                    </label>
                                                </li>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="row text-right">
                                    <div class="fieldgroup pb10 pt10 right">
                                        <asp:Button ID="btnServicesDone" runat="server" Text="Done" CausesValidation="False"
                                            OnClientClick="getSelected('ulServices', 'Services', 'lblServiceCode'); ShowProcessingDivBlock();" OnClick="OnServicesSelectionDoneClicked" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Panel ID="pnlEquipmentTypeList" runat="server" CssClass="popup popupControlOverW750 popup-position-fixed top50" Style="display: none;">
                        <div class="popheader">
                            <h4>Equipment Types</h4>
                        </div>
                        <div class="row finderScroll">
                            <div class="fieldgroup pl20 pr20 pt20">
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        // enforce mutual exclusion on checkboxes in customerLoginPageSection
                                        $(jsHelper.AddHashTag('ulEquipmentTypes input:checkbox')).click(function () {
                                            if (jsHelper.IsChecked($(this).attr('id'))) {
                                                $(jsHelper.AddHashTag('ulEquipmentTypes input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                                    jsHelper.UnCheckBox($(this).attr('id'));
                                                });
                                                $.uniform.update();
                                            }
                                        });
                                    });
                                </script>
                                <asp:Repeater runat="server" ID="rptEquipmentTypes">
                                    <HeaderTemplate>
                                        <ul class="twocol_list sm_chk" id="ulEquipmentTypes">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkSelected" CssClass="jQueryUniform" />
                                            <eShip:CustomHiddenField runat="server" ID="hidEquipmentTypeId" Value='<%# Eval("Value") %>' />
                                            <label class="lhInherit">
                                                <asp:Label runat="server" ID="lblEquipmentTypeCode" Text='<%# Eval("Text") %>' CssClass="fs85em" />
                                            </label>
                                        </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="row">
                            <div class="fieldgroup pb10 pt10 right">
                                <asp:Button ID="btnEquipmentTypesDone" runat="server" Text="Done" CausesValidation="False"
                                    OnClientClick="getSelected('ulEquipmentTypes', 'Equipment', 'lblEquipmentTypeCode');" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabItems" HeaderText="Items">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlItems" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlItems" DefaultButton="btnAddItem">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkBypassLinearFootRuleForRating" />
                                        <label>Bypass Linear Foot Rule</label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddItem" Text="Add Item" CssClass="mr10" CausesValidation="false" OnClick="OnAddItemClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnAddLibraryItem" Text="Add Library Item" CssClass="mr10" CausesValidation="false" OnClick="OnAddLibraryItemClicked" />
                                        <asp:Button runat="server" ID="btnClearItems" Text="Clear Items" CausesValidation="False" OnClick="OnClearItemsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    $(window).load(function () {
                                        $(jsHelper.AddHashTag('shipmentsItemsTable [id$="txtEstimatedPcf"]')).each(function () {
                                            UpdateDensity(this);
                                        });

                                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                            $(jsHelper.AddHashTag('shipmentsItemsTable [id$="txtEstimatedPcf"]')).each(function () {
                                                UpdateDensity(this);
                                            });
                                        });
                                    });

                                    function UncheckAutorated() {
                                        jsHelper.UnCheckBox($(jsHelper.AddHashTag('<%= chkAutoRated.ClientID %>')).attr('id'));
                                        SetAltUniformCheckBoxClickedStatus($(jsHelper.AddHashTag('<%= chkAutoRated.ClientID %>')));
                                    }

                                    function UpdateDensity(control) {
                                        var ids = new ControlIds();
                                        ids.DensityWeight = $(control).parent().parent().find('[id$="txtWeight"]').attr('id');
                                        ids.DensityLength = $(control).parent().parent().find('[id$="txtLength"]').attr('id');
                                        ids.DensityWidth = $(control).parent().parent().find('[id$="txtWidth"]').attr('id');
                                        ids.DensityHeight = $(control).parent().parent().find('[id$="txtHeight"]').attr('id');
                                        ids.DensityCalc = $(control).parent().parent().find('[id$="txtEstimatedPcf"]').attr('id');
                                        ids.HidDensityCalc = jsHelper.EmptyString;
                                        ids.DensityQty = $(control).parent().parent().find('[id$="txtQuantity"]').attr('id');
                                        CalculateDensity(jsHelper.EnglishUnits, ids);
                                    }
                                </script>

                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentsItemsTable" TableId="shipmentsItemsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="shipmentsItemsTable">
                                    <tr>
                                        <th style="width: 17%;">Package Type
                                        </th>
                                        <th style="width: 7%;" class="text-center">Quantity
                                        </th>
                                        <th style="width: 9%;">Weight<abbr title="Pounds">(lb)</abbr>
                                        </th>
                                        <th style="width: 9%;">Length(in)
                                        </th>
                                        <th style="width: 9%;">Width(in)
                                        </th>
                                        <th style="width: 9%;">Height(in)
                                        </th>
                                        <th style="width: 8%;" class="text-center">
                                            <abbr title="Actual Pounds per Cubic Feet">Pcf</abbr>
                                        </th>
                                        <th style="width: 8%;" class="text-center">Stackable
                                        </th>
										<th style="width: 8%;" class="text-center">Pieces
                                        </th>
                                        <th style="width: 8%">Value
                                        </th>
										
                                        <th style="width: 4%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstItems" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnShipmentItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidItemIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidItemId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CachedObjectDropDownList runat="server" ID="ddlPackageType" CssClass="w130" Type="PackageType"
                                                        EnableChooseOne="True" DefaultValue="0" onchange="UncheckAutorated();" SelectedValue='<%# Eval("PackageTypeId") %>' />

                                                    <asp:ImageButton runat="server" ID="imgSetPackageDimensions" ImageUrl="~/images/icons2/cog.png" Width="16px"
                                                        CausesValidation="False" ToolTip="Set Package Type Dimensions" OnClick="OnSetPackageDimensionsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:CustomTextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity") %>' CssClass="w55" onblur="UpdateDensity(this);" oninput="UncheckAutorated();" Type="NumbersOnly" />
                                                </td>

                                                <td>
                                                    <eShip:CustomTextBox ID="txtWeight" runat="server" Text='<%# Eval("ActualWeight") %>' CssClass="w70" onblur="UpdateDensity(this);" oninput="UncheckAutorated();" Type="FloatingPointNumbers" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtLength" runat="server" Text='<%# Eval("ActualLength") %>' CssClass="w70" onblur="UpdateDensity(this);" oninput="UncheckAutorated();" Type="FloatingPointNumbers" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtWidth" runat="server" Text='<%# Eval("ActualWidth") %>' CssClass="w70" onblur="UpdateDensity(this);" oninput="UncheckAutorated();" Type="FloatingPointNumbers" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtHeight" runat="server" Text='<%# Eval("ActualHeight") %>' CssClass="w70" onblur="UpdateDensity(this);" oninput="UncheckAutorated();" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:CustomTextBox ID="txtEstimatedPcf" runat="server" CssClass="w55 disabled" ReadOnly="True" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkIsStackable" Checked='<%# Eval("IsStackable") %>' />
                                                </td>
												<td class="text-center">
                                                    <eShip:CustomTextBox ID="txtPieceCount" runat="server" Text='<%# Eval("PieceCount") %>' CssClass="w40" Type="NumbersOnly" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtItemValue" runat="server" Text='<%# Eval("Value") %>' CssClass="w70" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-center top" rowspan="2">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProcessingDivBlock();"
                                                        CausesValidation="false" OnClick="OnDeleteItemClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="13" class="pt0 mt0 bottom_shadow">
                                                    <div class="row mb10">
                                                        <div class="col_1_3">
                                                            <div class="fieldgroup top">
                                                                <label class="wlabel blue pb10">
                                                                    Description
                                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" MaxLength="200" TargetControlId="txtDescription" />
                                                                </label>
                                                                <eShip:CustomTextBox ID="txtDescription" runat="server" Text='<%# Eval("Description") %>' TextMode="MultiLine" CssClass="w280" />
                                                            </div>
                                                        </div>
                                                        <div class="col_1_3">
                                                            <div class="row">
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">Comment</label>
                                                                    <eShip:CustomTextBox ID="txtComment" runat="server" Text='<%# Eval("Comment") %>' CssClass="w310" MaxLength="50" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="fieldgroup mr10">
                                                                    <label class="wlabel blue">Pickup</label>
                                                                    <asp:DropDownList runat="server" ID="ddlItemPickup" DataTextField="Text" DataValueField="Value" CssClass="w100" />
                                                                </div>
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">Delivery</label>
                                                                    <asp:DropDownList runat="server" ID="ddlItemDelivery" DataTextField="Text" DataValueField="Value" CssClass="w100" />
                                                                </div> 
																<div class="fieldgroup">
                                                                <label class="wlabel blue"><abbr title="Hazardous Material">HazMat</abbr></label>
                                                                    <eShip:AltUniformCheckBox runat="server" ID="chkIsHazmat" Checked='<%# Eval("HazardousMaterial") %>' />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col_1_3">
                                                            <div class="row">
                                                                <div class="fieldgroup mr10">
                                                                    <label class="wlabel blue">NMFC Code</label>
                                                                    <eShip:CustomTextBox ID="txtNMFCCode" runat="server" Text='<%# Eval("NMFCCode") %>' CssClass="w130" />
                                                                </div>
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">HTS Code</label>
                                                                    <eShip:CustomTextBox ID="txtHTSCode" runat="server" Text='<%# Eval("HTSCode") %>' CssClass="w130" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="fieldgroup mr10">
                                                                    <label class="wlabel blue">Actual Class</label>
                                                                    <asp:DropDownList runat="server" ID="ddlActualFreightClass" DataTextField="Text" DataValueField="Value" CssClass="w130" onchange="UncheckAutorated();" />
                                                                </div>
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">Rated Class</label>
                                                                    <eShip:CustomTextBox runat="server" ID="txtRatedFreightClass" Text='<%# Eval("RatedFreightClass") %>' CssClass="w130 disabled" ReadOnly="True" />
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <script type="text/javascript">
                                    $(function () {
                                        HandleAddItemButtonVisibility();

                                        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
                                            HandleAddItemButtonVisibility();
                                        });
                                    });

                                    function HandleAddItemButtonVisibility() {
                                        if ($(document).height() > $(window).height()) {
                                            $("#divSecondAddItemButton").show();
                                        } else {
                                            $("#divSecondAddItemButton").hide();
                                        }
                                    }
                                </script>
                                <div class="row pt20" id="divSecondAddItemButton">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddItemTabBottom" Text="Add Item" CssClass="mr10" CausesValidation="false" OnClick="OnAddItemClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddLibraryItem" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>

            <ajax:TabPanel runat="server" ID="tabCharges" HeaderText="Charges">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlCharges" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:Panel runat="server" ID="pnlCharges" DefaultButton="btnAddCharge">
                                <div class="row">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            // enforce mutual exclusion on checkboxes in divAuditedOrInDispute
                                            $(jsHelper.AddHashTag('divAuditedOrInDispute input:checkbox')).click(function () {
                                                if (jsHelper.IsChecked($(this).attr('id'))) {
                                                    $(jsHelper.AddHashTag('divAuditedOrInDispute input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                                        jsHelper.UnCheckBox($(this).attr('id'));
                                                        SetAltUniformCheckBoxClickedStatus(this);
                                                    });
                                                }
                                            });

                                            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                                $(jsHelper.AddHashTag('divAuditedOrInDispute input:checkbox')).click(function () {
                                                    if (jsHelper.IsChecked($(this).attr('id'))) {
                                                        $(jsHelper.AddHashTag('divAuditedOrInDispute input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                                            jsHelper.UnCheckBox($(this).attr('id'));
                                                            SetAltUniformCheckBoxClickedStatus(this);
                                                        });
                                                    }
                                                });
                                            });
                                        });
                                    </script>

                                    <div class="fieldgroup">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkDeclineInsurance" />
                                        <label>Declined Additional Insurance</label>
                                    </div>
                                    <div class="fieldgroup" id="divAuditedOrInDispute">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkAuditedForInvoicing" />
                                        <label class="mr10 vlinedarkright pr10">Audited For Invoicing</label>
                                        <eShip:AltUniformCheckBox runat="server" ID="chkInDispute" />
                                        <label>In Dispute</label>
                                        <label>Reason:</label>
                                        <asp:DropDownList runat="server" ID="ddlInDisputeReason" DataTextField="Text" DataValueField="Value" />
                                    </div>
                                    <div class="fieldgroup right vlinedarkleft pl10">
                                        <asp:Button runat="server" ID="btnAddCharge" Text="Add Charge" CausesValidation="false" OnClick="OnAddChargeClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnAutoRate" Text="Auto Rate Shipment" CausesValidation="false" OnClick="OnAutoRateClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnClearCharges" Text="Clear Charges" CausesValidation="False" OnClick="OnClearChargesClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>
                                <div class="row mb10 mt10">
                                    <div class="fieldgroup">
                                        <label class="upper blue">Collected Payment Amount:</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCollectedPaymentAmount" CssClass="w100 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup" id="chargeStatisticsDiv">
                                        <eShip:ChargeStatistics2 ID="chargeStatistics" runat="server" />
                                        <asp:ImageButton runat="server" ID="ibtnRefreshChargeStatistics" CausesValidation="False" AlternateText="Refresh Charge Statistics" ImageUrl="~/images/icons2/cog.png"
                                            ToolTip="Refresh Charge Statistics" OnClick="OnRefreshChargeStatisticsClicked" OnClientClick="ResetChargeStatisticsColor(); ShowProcessingDivBlock();" />
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(window).load(function () {
                                        $(jsHelper.AddHashTag('shipmentChargesTable [id$="txtAmountDue"]')).each(function () {
                                            UpdateChargeInformation(this);
                                        });
                                        $('#chargeStatisticsDiv').removeClass('red');

                                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                            $(jsHelper.AddHashTag('shipmentChargesTable [id$="txtAmountDue"]')).each(function () {
                                                UpdateChargeInformation(this);
                                            });
                                            $('#chargeStatisticsDiv').removeClass('red');
                                        });
                                    });


                                    function ResetChargeStatisticsColor() {
                                        $('#chargeStatisticsDiv').removeClass('red');
                                    }

                                    function UpdateChargeInformation(control) {
                                        var unitSell = Number($(control).parent().parent().find('[id$="txtUnitSell"]').val());
                                        var unitDiscount = Number($(control).parent().parent().find('[id$="txtUnitDiscount"]').val());
                                        var qty = Number($(control).parent().parent().find('[id$="txtQuantity"]').val());
                                        if (qty < 1) qty = 1;
                                        $(control).parent().parent().find('[id$="txtAmountDue"]').val(((unitSell - unitDiscount) * qty).toFixed(4));
                                        $('#chargeStatisticsDiv').addClass('red');
                                    }

                                    function SetCheckAllShipmentCharges(control) {
                                        $("#shipmentChargesTable input:checkbox[id*='chkSelected']").each(function () {
                                            this.checked = control.checked && this.disabled == false;
                                            SetAltUniformCheckBoxClickedStatus(this);
                                        });

                                        // necessary to keep frozen header and header checkbox states the same
                                        $('[id*=' + control.id + ']').each(function () {
                                            this.checked = control.checked;
                                            SetAltUniformCheckBoxClickedStatus(this);
                                        });
                                    }
                                </script>

                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentChargesTable" TableId="shipmentChargesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="shipmentChargesTable">
                                    <tr>
                                        <th style="width: 3%;">
                                            <eShip:AltUniformCheckBox ID="chkSelectAllCharges" runat="server" OnClientSideClicked="javascript:SetCheckAllShipmentCharges(this);" />
                                        </th>
                                        <th style="width: 27%;">Charge Code</th>
                                        <th style="width: 5%;">
                                            <abbr title="Quantity">Qty</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Unit Buy
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Unit Sell
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Unit Discount
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Amount Due
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnChargesItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="top">
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkSelected" OnClientSideClicked=" SetCheckAllCheckBoxUnChecked(this, 'shipmentChargesTable', 'chkSelectAllCharges'); " />
                                                </td>
                                                <td>
                                                    <eShip:CachedObjectDropDownList runat="server" ID="ddlChargeCode" CssClass="w260" Type="ChargeCodes" EnableChooseOne="True"
                                                        DefaultValue="0" SelectedValue='<%# Eval("ChargeCodeId") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:CustomHiddenField ID="hidChargeIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidChargeId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomTextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity") %>' CssClass="w40" onblur="UpdateChargeInformation(this);" Type="NumbersOnly" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox ID="txtUnitBuy" runat="server" Text='<%# Eval("UnitBuy", "{0:f4}") %>' CssClass="w75" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox ID="txtUnitSell" runat="server" Text='<%# Eval("UnitSell", "{0:f4}") %>' CssClass="w75" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox ID="txtUnitDiscount" runat="server" Text='<%# Eval("UnitDiscount", "{0:f4}") %>' CssClass="w75" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox runat="server" ID="txtAmountDue" CssClass="w80 disabled" ReadOnly="True" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProcessingDivBlock();"
                                                        CausesValidation="false" OnClick="OnDeleteChargeClicked" Enabled='<%# Access.Modify %>' Visible='<%# (Eval("VendorBillId").ToLong() == default(long)) %>' />
                                                    <eShip:CustomHiddenField ID="hidVendorBillId" runat="server" Value='<%# Eval("VendorBillId") %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="13" class="pt0 mt0 bottom_shadow">
                                                    <div class="row mb10">
                                                        <div class="col_1_3">
                                                            <div class="fieldgroup middle">
                                                                <label class="wlabel blue">Vendor</label>
                                                                <asp:DropDownList runat="server" ID="ddlVendors" OnSelectedIndexChanged="OnChangeVendorInCharge"
                                                                    AutoPostBack="True" DataTextField="Text" DataValueField="Value" AppendDataBoundItems="true" />
                                                                <eShip:CustomHiddenField ID="hidSelectedVendorId" runat="server" Value='<%# Eval("VendorId") %>' />
                                                            </div>
                                                        </div>
                                                        <div class="col_1_3">
                                                            <div class="fieldgroup middle">
                                                                <label class="wlabel blue">Vendor Bill Number
                                                                    <%# ActiveUser.HasAccessTo(ViewCode.VendorBill) && Eval("VendorBillId").ToLong() != default(long)
                                                                        ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Bill Record'><img src={3} width='16' class='middle'/></a>", 
                                                                            ResolveUrl(VendorBillView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            Eval("VendorBillId").GetString().UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                                        : string.Empty %></label>
                                                                <eShip:CustomTextBox runat="server" ID="txtVendorBillNumber" Text='<%# Eval("VendorBillDocumentNumber") %>' CssClass="w340 disabled" ReadOnly="True" />
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col_1_3">
                                                            <div class="fieldgroup top">
                                                                <label class="wlabel blue">
                                                                    Comment
                                                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" MaxLength="100" TargetControlId="txtComment" />
                                                                </label>
                                                                <eShip:CustomTextBox ID="txtComment" runat="server" Text='<%# Eval("Comment") %>' TextMode="MultiLine" MaxLength="100" CssClass="w315" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAutoRate" />
                         </Triggers>
                    </asp:UpdatePanel>

                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabNotes" HeaderText="Notes & Check Calls">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlNoteAndCheckCalls">
                        <div class="rowgroup">
                            <div class="col_1_2 bbox">

                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>

                                        <asp:Panel runat="server" DefaultButton="ibtnAddNote" CssClass="rowgroup">
                                            <div class="row mb5">
                                                <div class="fieldgroup left">
                                                    <asp:ImageButton runat="server" ID="ibtnAddNote" Text="Add Note" OnClick="OnAddNoteClicked" ImageUrl="~/images/icons2/add_button.png"
                                                        Height="26px" ToolTip="Add Note" OnClientClick="ShowProgressNotice(true);" CausesValidation="False" />
                                                    <label class="upper blue fs120em">
                                                        Notes
                                                        <asp:Literal runat="server" ID="litNoteCount" />
                                                    </label>
                                                </div>
                                                <div class="fieldgroup right mr10">
                                                    <eShip:PaginationExtender runat="server" ID="peNotes" TargetControlId="lstNotes" PageSize="5" OnPageChanging="OnPeNotesPageChanging" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class=" wAuto pr20">
                                                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentNotesTable" TableId="shipmentNotesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                                    <table class="stripe" id="shipmentNotesTable">
                                                        <tr>
                                                            <th style="width: 20%;">Type
                                                            </th>
                                                            <th style="width: 20%;">User
                                                            </th>
                                                            <th style="width: 20%;" class="text-center">Archived
                                                            </th>
                                                            <th style="width: 20%;" class="text-center">Classified
                                                            </th>
                                                            <th style="width: 20%" class="text-center">Action
                                                            </th>
                                                        </tr>
                                                        <asp:ListView ID="lstNotes" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnNotesItemDataBound">
                                                            <LayoutTemplate>
                                                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                            </LayoutTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <eShip:CustomHiddenField ID="hidNoteIndex" runat="server" Value='<%# Container.DataItemIndex + (peNotes.PageSize*(peNotes.CurrentPage-1)) %>' />
                                                                        <eShip:CustomHiddenField ID="hidNoteId" runat="server" Value='<%# Eval("Id") %>' />
                                                                        <eShip:CustomHiddenField ID="hidLoadOrderNote" runat="server" Value='<%# Eval("IsLoadOrderNote") %>' />
                                                                        <asp:DropDownList runat="server" ID="ddlNoteType" DataValueField="Value" DataTextField="Text" CssClass="w110" />
                                                                    </td>
                                                                    <td>
                                                                        <eShip:CustomTextBox ID="txtUser" runat="server" Text='<%# Eval("Username") %>' ReadOnly="True" CssClass="w150 disabled" />
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <eShip:AltUniformCheckBox runat="server" ID="chkArchived" Checked='<%# Eval("Archived") %>' />
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <eShip:AltUniformCheckBox runat="server" ID="chkClassified" Checked='<%# Eval("Classified") %>' />
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" CausesValidation="false" OnClick="OnDeleteUnsavedNoteClicked" Enabled='<%# Access.Modify %>'
                                                                            Visible='<%# Eval("Id").ToLong() == default(long) %>' OnClientClick="ShowProgressNotice(true);" />
                                                                    </td>
                                                                </tr>
                                                                <tr class="hidden">
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr class="bottom_shadow pb5">
                                                                    <td colspan="5" class="pt0">
                                                                        <div class="row mb5">
                                                                            <div class="fieldgroup">
                                                                                <label class="wlabel blue">
                                                                                    Message
                                                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleMessage" MaxLength="500" TargetControlId="txtMessage" />
                                                                                </label>
                                                                                <eShip:CustomTextBox ID="txtMessage" runat="server" Text='<%# Eval("Message") %>' TextMode="MultiLine" CssClass='<%# Eval("Id").ToLong() == default(long) ? "w430" : "w430 disabled" %>' ReadOnly='<%# Eval("Id").ToLong() != default(long) %>' />
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </table>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>
                            <div class="col_1_2 bbox pl20 vlinedarkleft">

                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>

                                        <asp:Panel runat="server" DefaultButton="ibtnAddCheckCall" CssClass="rowgroup">
                                            <div class="row mb5">
                                                <div class="fieldgroup left">
                                                    <asp:ImageButton runat="server" ID="ibtnAddCheckCall" Text="Add Check Call" OnClick="OnAddCheckCallClicked" ImageUrl="~/images/icons2/add_button.png"
                                                        Height="26px" ToolTip="Add Check Call" OnClientClick="ShowProgressNotice(true);" CausesValidation="False" />
                                                    <label class="upper blue fs120em">
                                                        Check Calls
                                                        <asp:Literal runat="server" ID="litCheckCallCount" />
                                                    </label>
                                                </div>
                                                <div class="fieldgroup right mr10">
                                                    <eShip:PaginationExtender runat="server" ID="peCheckCalls" TargetControlId="lstCheckCalls" PageSize="5" OnPageChanging="OnPeCheckCallsPageChanging" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentCheckCallsTable" TableId="shipmentCheckCallsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                                <table class="stripe" id="shipmentCheckCallsTable">
                                                    <tr>
                                                        <th style="width: 25%;">Event Date
                                                        </th>
                                                        <th style="width: 20%;">Event Time
                                                        </th>
                                                        <th style="width: 35%;">EDI Status Code
                                                        </th>
                                                        <th style="width: 20%;" class="text-center">Action
                                                        </th>
                                                    </tr>
                                                    <asp:ListView ID="lstCheckCalls" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnCheckCallsItemDataBound">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <eShip:CustomHiddenField ID="hidCheckCallIndex" runat="server" Value='<%# Container.DataItemIndex + (peCheckCalls.PageSize*(peCheckCalls.CurrentPage-1)) %>' />
                                                                    <eShip:CustomHiddenField ID="hidCheckCallId" runat="server" Value='<%# Eval("Id") %>' />
                                                                    <eShip:CustomTextBox runat="server" ID="txtEventDate" CssClass="w80" Type="Date" placeholder="99/99/9999"
                                                                        Text='<%# Eval("EventDate").FormattedShortDateSuppressEarliestDate() %>' />
                                                                </td>
                                                                <td>
                                                                    <eShip:CustomTextBox runat="server" ID="txtEventTime" Text='<%# Eval("EventDate").ToDateTime().GetTime() %>' CssClass="w55" MaxLength="5" placeholder="99:99" Type="Time" />
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList runat="server" ID="ddlEdiCode" DataValueField="Value" DataTextField="Text" CssClass="w200" />
                                                                </td>
                                                                <td class="text-center">
                                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProgressNotice(true);"
                                                                        CausesValidation="false" OnClick="OnDeleteCheckCallClicked" Enabled='<%# Access.Modify %>' />
                                                                </td>
                                                            </tr>
                                                            <tr class="hidden">
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr class="bottom_shadow pb5">
                                                                <td colspan="5" class="pt0">
                                                                    <div class="row mb5">
                                                                        <div class="fieldgroup mr10">
                                                                            <label class="wlabel blue">
                                                                                Call Notes
                                                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tfheCallNotes" TargetControlId="txtCallNotes" MaxLength="500" />
                                                                            </label>
                                                                            <eShip:CustomTextBox ID="txtCallNotes" runat="server" Text='<%# Eval("CallNotes") %>' TextMode="MultiLine" CssClass="w330 h150" />
                                                                        </div>
                                                                        <div class="fieldgroup">
                                                                            <label class="wlabel blue">User</label>
                                                                            <eShip:CustomTextBox ID="txtUser" runat="server" Text='<%# Eval("Username") %>' CssClass="w150 disabled" ReadOnly="True" />
                                                                            <label class="wlabel blue">Call Date</label>
                                                                            <eShip:CustomTextBox runat="server" ID="txtCallDate" Text='<%# Eval("CallDate").FormattedLongDate() %>' ReadOnly="True" CssClass="w150 disabled" />
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </table>
                                            </div>
                                        </asp:Panel>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAdditionalDetails" HeaderText="Add. Details"
                Style="display: none;">
                <ContentTemplate>
                    <asp:Panel ID="pnlAdditionalDetails" runat="server">
                        <div class="rowgroup">
                            <div class="col_1_2 bbox vlinedarkright">
                                <h5>Coordinator Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $('#<%= txtShipmentCoordUsername.ClientID %>').change(function () {

                                                    var ids = new ControlIds();
                                                    ids.Username = $('#<%= txtShipmentCoordUsername.ClientID %>').attr('id');
                                                    ids.UserId = $('#<%= hidShipmentCoordUserId.ClientID %>').attr('id');
                                                    ids.UserFullName = $('#<%= txtShipmentCoordName.ClientID %>').attr('id');

                                                    FindActiveShipmentCoordinator('<%= ActiveUserTenantCode %>', '<%= ActiveUsername %>', $('#<%= txtShipmentCoordUsername.ClientID %>').val(), ids);
                                                });

                                                $('#<%= txtCarrierCoordUsername.ClientID %>').change(function () {

                                                    var ids = new ControlIds();
                                                    ids.Username = $('#<%= txtCarrierCoordUsername.ClientID %>').attr('id');
                                                    ids.UserId = $('#<%= hidCarrierCoordUserId.ClientID %>').attr('id');
                                                    ids.UserFullName = $('#<%= txtCarrierCoordName.ClientID %>').attr('id');

                                                    FindActiveCarrierCoordinator('<%= ActiveUserTenantCode %>', '<%= ActiveUsername %>', $('#<%= txtCarrierCoordUsername.ClientID %>').val(), ids);
                                                });
                                            });
                                        </script>
                                        <label class="wlabel">Shipment Coordinator</label>
                                        <eShip:CustomTextBox runat="server" ID="txtShipmentCoordUsername" MaxLength="50" CssClass="w110" />
                                        <asp:ImageButton runat="server" ID="imgShipmentCoordUsernameSearch" ToolTip="Find Shipment Coordinator" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnFindShipmentCoordUserClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtShipmentCoordName" CssClass="w280 disabled" Type="ReadOnly" />
                                        <eShip:CustomHiddenField runat="server" ID="hidShipmentCoordUserId" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceShipmentCoordinator" TargetControlID="txtShipmentCoordUsername" ServiceMethod="GetUserList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Carrier Coordinator</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCarrierCoordUsername" MaxLength="50" CssClass="w110" />
                                        <asp:ImageButton runat="server" ID="imgCarrierCoordUsernameSearch" ToolTip="Find Carrier Coordinator" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnFindCarrierCoordUserClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtCarrierCoordName" CssClass="w280 disabled" Type="ReadOnly" />
                                        <eShip:CustomHiddenField runat="server" ID="hidCarrierCoordUserId" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceCarrierCoordinator" TargetControlID="txtCarrierCoordUsername" ServiceMethod="GetUserList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>

                                <h5 class="">Mileage</h5>
                                <div class="row ">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Mileage Source</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlMileageSource" CssClass="w200" Type="MileageSources" EnableChooseOne="True" DefaultValue="0" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Empty Mileage</label>
                                        <eShip:CustomTextBox ID="txtEmptyMileage" runat="server" CssClass="w200" MaxLength="50" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtEmptyMileage"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                    <div class="fieldgroup">

                                        <asp:UpdatePanel runat="server" ID="upPnlMileage" UpdateMode="Conditional">
                                            <ContentTemplate>

                                                <label class="wlabel">Mileage</label>
                                                <eShip:CustomTextBox ID="txtMileage" runat="server" CssClass="w200" MaxLength="50" Type="FloatingPointNumbers" />
                                                <asp:ImageButton ID="btnGetMiles" runat="server" ImageUrl="~/images/icons2/cog.png" OnClientClick="ShowProcessingDivBlock();"
                                                    CausesValidation="false" ToolTip="Get Mileage" OnClick="OnGetMilesClicked" />

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>

                                <h5>Hazardous Material Contact</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkHazardousMaterial" runat="server" CssClass="jQueryUniform" />
                                        <label>Hazardous Material</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Name</label>
                                        <eShip:CustomTextBox ID="txtHazardousContactName" runat="server" CssClass="w200" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Email</label>
                                        <eShip:CustomTextBox ID="txtHazardousContactEmail" runat="server" CssClass="w200" MaxLength="100" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Phone</label>
                                        <eShip:CustomTextBox ID="txtHazardousContactPhone" runat="server" CssClass="w200" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Mobile</label>
                                        <eShip:CustomTextBox ID="txtHazardousContactMobile" runat="server" CssClass="w200" MaxLength="50" />
                                    </div>
                                </div>

                                <h5>Miscellaneous Fields</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Miscellaneous Field 1
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleMiscField1" TargetControlId="txtMiscField1" MaxLength="100" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtMiscField1" runat="server" TextMode="MultiLine" CssClass="w420" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Miscellaneous Field 2
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleMiscField2" TargetControlId="txtMiscField2" MaxLength="100" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtMiscField2" runat="server" TextMode="MultiLine" CssClass="w420" />
                                    </div>
                                </div>

                                <h5>Driver Information</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Driver Name</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDriverName" CssClass="w200" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Driver Phone Number</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDriverPhoneNumber" CssClass="w200" MaxLength="50" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Driver Truck Number</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDriverTrailerNumber" CssClass="w200" MaxLength="50" />
                                    </div>
                                </div>

                                <h5>Job Link</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">
                                            Job Number
                                            <%= txtJobNumber.Text.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Job) 
                                                            ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Job Record'><img src={3} width='20' class='middle'/></a>", 
                                                                                    ResolveUrl(JobView.PageAddress),
                                                                                    WebApplicationConstants.TransferNumber, 
                                                                                    txtJobNumber.Text,
                                                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtJobNumber" Type="NumbersOnly" CssClass="w200" MaxLength="50" OnTextChanged="OnJobNumberTextChanged"
                                            AutoPostBack="True" />
                                        <asp:ImageButton ID="btnFindJob" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="false" OnClick="OnFindJobClicked" />
                                        <asp:ImageButton ID="btnClearJob" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="false" OnClick="OnClearJobClicked" Width="20px" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Job Step</label>
                                        <eShip:CustomTextBox runat="server" ID="txtJobStep" CssClass="w70" MaxLength="50" Type="NumbersOnly" />
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2 bbox pl46">
                                <h5>Comments</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            General Comments
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleGeneralBolComments" TargetControlId="txtGeneralBolComments" MaxLength="200" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtGeneralBolComments" runat="server" TextMode="MultiLine" CssClass="w420 h150" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Critical Comments
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleCriticalBolComments" TargetControlId="txtCriticalBolComments" MaxLength="200" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtCriticalBolComments" runat="server" TextMode="MultiLine" CssClass="w420 h150" />
                                    </div>
                                </div>

                                <h5>Sales Representative Information</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Name</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeId" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentative" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Commission (%)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeCommissionPercent" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeCommissionPercent" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Minimum Commission ($)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeMinCommValue" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeMinCommValue" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Maximum Commission ($)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeMaxCommValue" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeMaxCommValue" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Additional Entity Name</label>
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeAddlEntityName" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Additional Entity Commission (%)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeAddlEntityCommPercent" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeAddlEntityCommPercent" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>

                                <asp:UpdatePanel runat="server" ID="upResellerAdditions" UpdateMode="Conditional">
                                    <ContentTemplate>

                                        <h5>Reseller Additions</h5>
                                        <div class="row mb10">
                                            <div class="fieldgroup mr20">
                                                <label class="upper blue">
                                                    <asp:Literal runat="server" ID="litBillReseller" />
                                                    <asp:Literal runat="server" ID="litResellerAdditionName" />
                                                </label>
                                            </div>
                                            <div class="fieldgroup right">
                                                <asp:Button runat="server" ID="btnRetrieveDetails" Text="Retrieve" CausesValidation="false" OnClick="OnRetrieveResellerAdditionDetailsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                                <asp:Button runat="server" ID="btnClearDetails" Text="Clear" CausesValidation="false" OnClick="OnClearResellerAdditionDetailsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                            </div>
                                        </div>
                                        <table class="stripe" id="shipmentResellerAdditionsTable">
                                            <tr>
                                                <th style="width: 20%;">Category
                                                </th>
                                                <th style="width: 15%;" class="text-center">Type
                                                </th>
                                                <th style="width: 20%;" class="text-center">Value ($)
                                                </th>
                                                <th style="width: 24%;" class="text-center">Percent (%)
                                                </th>
                                                <th style="width: 20%;" class="text-center">Use Lower
                                                </th>
                                            </tr>
                                            <asp:Repeater runat="server" ID="rptResellerAdditions">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="text-right">
                                                            <%# Eval("Header") %>
                                                        </td>
                                                        <td class="text-center">
                                                            <%# Eval("Type") %>
                                                        </td>
                                                        <td class="text-center">
                                                            <%# Eval("Value") %>
                                                        </td>
                                                        <td class="text-center">
                                                            <%# Eval("Percent") %>
                                                        </td>
                                                        <td class="text-center">
                                                            <%# Eval("UseLower").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <hr class="dark" />
                        <asp:Panel runat="server" ID="pnlRatingDetails" class="rowgroup">
                            <h5>Rating Details</h5>
                            <div class="rowgroup">
                                <div class="col_1_3">
                                    <table class="stripe">
                                        <tr>
                                            <th style="width: 70%;">
                                                <label class="upper">Field</label></th>
                                            <th style="width: 30%;">
                                                <label class="upper">Value</label></th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Original Value ($)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litOriginalValue" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Vendor Discount (%)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litVendorDiscountPercentage" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Vendor Freight Floor ($)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litVendorFreightFloor" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Vendor Freight Ceiling ($)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litVendorFreightCeiling" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Vendor Fuel (%)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litVendorFuelPercent" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Customer Freight Markup ($)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litCustomerFreightMarkup" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Customer Freight Markup (%)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litCustomerFreightMarkupPercent" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Use Lower Customer Freight Markup</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkUseLowerCustomerFreightMarkup" Enabled="false" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="blue upper">Rated Cubic Feet (ft<sup>3</sup>)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litRatedCubicFeet" /></label>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>
                                                <label class="blue upper">Rated Pounds per Cubic Feet (ft<sup>3</sup>)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litRatedPcf" /></label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="blue upper">Apply Discount</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="chkApplyDiscount" Enabled="false" CssClass="jQueryUniform" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Line Haul Profit Adjustment Ratio</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litLineHaulProfitAdjRatio" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Fuel Profit Adjustment Ratio</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litFuelProfitAdjustmentRatio" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Accessorial Profit Adjustment Ratio</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litAccessorialProfitAdjustmentRatio" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Service Profit Adj. Ratio</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litServiceProfitAdjustmentRatio" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Linear Foot Rule Bypassed</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkLinearFootRuleBypassed" Enabled="False" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Direct Point</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkDirectPoint" Enabled="false" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col_1_3">
                                    <table class="stripe">
                                        <tr>
                                            <th style="width: 70%;">
                                                <label class="upper">Field</label></th>
                                            <th style="width: 30%;">
                                                <label class="upper">Value</label></th>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="upper blue">Reseller Additional Freight Markup</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litResellerAdditionalFreightMarkup" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Reseller Additional Freight Markup Is Percent</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkResellerAdditionalFreightMarkupIsPercent" Enabled="false" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Reseller Additional Fuel Markup</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litResellerAdditionalFuelMarkup" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Reseller Additional Fuel Markup Is Percent</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkResellerAdditionalFuelMarkupIsPercent" Enabled="false" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Reseller Additional Accessorial Markup</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litResellerAdditionalAccessorialMarkup" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Reseller Additional Accessorial Markup Is Percent</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkResellerAdditionalAccessorialMarkupIsPercent" Enabled="false" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Reseller Additional Misc. Service Markup</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litResellerAdditionalServiceMarkup" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Reseller Additional Misc. Service Markup Is Percent</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkResellerAdditionalServiceMarkupIsPercent" Enabled="false" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Small Pack Type</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litSmallPackType" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Small Pack Engine</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:HiddenField runat="server" ID="hidSmallPackEngine" />
                                                    <asp:Literal runat="server" ID="litSmallPackEngine" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Is LTL Package Specific Rate</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkLTLPackageSpecificRate" Enabled="False" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Billed Weight (lb)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litBilledWeight" /></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="blue upper">Rated Weight (lb)</label>
                                            </td>
                                            <td>
                                                <label>
                                                    <asp:Literal runat="server" ID="litRatedWeight" /></label>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                                <div class="col_1_3">

                                    <table class="stripe">
                                        <tr>
                                            <th style="width: 70%;">
                                                <label class="upper">Field</label></th>
                                            <th style="width: 30%;">
                                                <label class="upper">Value</label></th>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="upper blue">Cust. name in BOL c/o</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkCareOfAdddressFormatEnabled" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Guaranteed Delivery Service</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkGuaranteedDeliveryService" CssClass="jQueryUniform" Enabled="False" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Time of Guaranteed Delivery</label>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" ID="litGuaranteedDeliveryTime" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Guaranteed Delivery Rate</label>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" ID="litGuaranteedDeliveryRate" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Guaranteed Delivery Rate Type</label>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" ID="litGuaranteedDeliveryRateType" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Expedited Delivery Service</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkExpeditedDeliveryService" CssClass="jQueryUniform" Enabled="False" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Expedited Service Rate</label>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" ID="litExpeditedServiceRate" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Expedited Service Details</label>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" ID="litExpeditedServiceDetails" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="upper blue">Quote Number</label>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" ID="litProject44QuoteNumber" />
                                            </td>
                                        </tr>
                                    </table>

                                    <div class="mb20">&nbsp;</div>

                                    <div class="row">
                                        <div class="fieldgroup mb20">
                                            <label class="wlabel blue">Origin Terminal</label>
                                            <label>
                                                <asp:Literal runat="server" ID="litOriginTerminal" /></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup mb20">
                                            <label class="wlabel blue">Destination Terminal</label>
                                            <label>
                                                <asp:Literal runat="server" ID="litDestinationTerminal" /></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="wlabel blue">Rating Override Address</label>
                                            <label>
                                                <asp:Literal runat="server" ID="litRatingOverrideAddress" /></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="rowgroup">
                                <div class="row">
                                    <label class="upper blue">Auto Rating Accessorials</label>
                                </div>
                                <table class="stripe">
                                    <tr>
                                        <th style="width: 19%;">Service Description
                                        </th>
                                        <th style="width: 8%;">Rate Type
                                        </th>
                                        <th style="width: 8%;">Rate ($)
                                        </th>
                                        <th style="width: 10%;">Buy Floor ($)
                                        </th>
                                        <th style="width: 11%;">Buy Ceiling ($)
                                        </th>
                                        <th style="width: 10%;">Sell Floor ($)
                                        </th>
                                        <th style="width: 10%;">Sell Ceiling ($)
                                        </th>
                                        <th style="width: 8%;">Markup Value ($)
                                        </th>
                                        <th style="width: 8%;">Markup (%)
                                        </th>
                                        <th style="width: 8%;" class="text-center">Use Lower Sell
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstOtherDetails" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>

                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />

                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidRatingId" Value='<%# Eval("Id") %>' />
                                                    <asp:Literal runat="server" ID="litRatingServiceDescription" Text='<%# Eval("ServiceDescription") %>' />
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidRatingRateType" Value='<%# Eval("RateType") %>' />
                                                    <asp:Literal runat="server" ID="litRatingRateType" Text='<%# Eval("RateType").FormattedString() %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litRatingRate" Text='<%# Eval("Rate") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litRatingBuyFloor" Text='<%# Eval("BuyFloor") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litRatingBuyCeiling" Text='<%# Eval("BuyCeiling") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litRatingSellFloor" Text='<%# Eval("SellFloor") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litRatingSellCeiling" Text='<%# Eval("SellCeiling") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litRatingMarkupValue" Text='<%# Eval("MarkupValue") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litRatingMarkupPercent" Text='<%# Eval("MarkupPercent") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkRatingUseLowerSell" Checked='<%# Eval("UseLowerSell") %>' Enabled="false" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabCollections" HeaderText="Misc. Items">
                <ContentTemplate>

                    <asp:UpdatePanel runat="server" ID="upReferences" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:Panel runat="server" ID="pnlReferences" DefaultButton="btnAddReference" CssClass="rowgroup">

                                <div class="row mb10">
                                    <div class="fieldgroup left">
                                        <label class="upper blue fs120em">
                                            Customer References
                                            <asp:Literal runat="server" ID="litReferenceCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddReference" Text="Add Reference" CausesValidation="false" OnClick="OnAddReferenceClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnClearReference" Text="Clear References" CausesValidation="false" OnClick="OnClearReferencesClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>
                                <table class="stripe">
                                    <tr>
                                        <th style="width: 25%;">Name
                                        </th>
                                        <th style="width: 36%;">Value
                                        </th>
                                        <th class="text-center" style="width: 12%;">Display On Origin
                                        </th>
                                        <th class="text-center" style="width: 12%;">Display On
										<abbr title="Destination">
                                            Dest.</abbr>
                                        </th>
                                        <th class="text-center" style="width: 10%;">Required
                                        </th>
                                        <th class="text-center" style="width: 5%;">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstReferences" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>

                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />

                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidReferenceIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidReferenceId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomTextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' CssClass="w260" MaxLength="50" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtValue" runat="server" Text='<%# Eval("Value") %>' CssClass="w330" MaxLength="50" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox ID="chkDisplayOnOrigin" runat="server" Checked='<%# Eval("DisplayOnOrigin") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox ID="chkDisplayOnDestination" runat="server" Checked='<%# Eval("DisplayOnDestination") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox ID="chkRequired" runat="server" Checked='<%# Eval("Required") %>' Enabled="False" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProcessingDivBlock();"
                                                        CausesValidation="false" OnClick="OnDeleteReferenceClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <hr class="dark" />
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("[id$='ibtnViewAdditionalVendorPerformanceStatistics']").each(function () {
                                jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                            });

                            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                $("[id$='ibtnViewAdditionalVendorPerformanceStatistics']").each(function () {
                                    jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                                });
                            });
                        });
                    </script>
                    <asp:UpdatePanel runat="server" ID="upVendors" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:Panel ID="pnlVendors" runat="server" DefaultButton="btnAddVendorFromFinder" CssClass="rowgroup">

                                <div class="row">
                                    <div class="fieldgroup left mb10">
                                        <label class="upper blue fs120em">
                                            Additional Vendors
                                    <asp:Literal runat="server" ID="litVendorsCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddVendorFromFinder" Text="Add Vendor" CausesValidation="false" OnClick="OnAddVendorClicked" />
                                        <asp:Button runat="server" ID="btnClearVendors" Text="Clear Vendors" CausesValidation="false" OnClick="OnClearVendorsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>

                                <div class="errorMsgLit">
                                    <asp:Literal runat="server" ID="litVendorsErrorMsg" />
                                </div>

                                <table class="stripe">
                                    <tr>
                                        <th style="width: 5%;" class="text-center">Alerts
                                        </th>
                                        <th style="width: 10%;">Vendor #
                                        </th>
                                        <th style="width: 18%;">Name
                                        </th>
                                        <th style="width: 10%;">Pro Number
                                        </th>
                                        <th style="width: 20%;">Failure Code
                                        </th>
                                        <th style="width: 21%;">Failure Comments
                                        </th>
                                        <th style="width: 16%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstVendors" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>

                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />

                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidInsuranceAlert" Value='<%# Eval("InsuranceAlert") %>' />
                                                    <eShip:CustomHiddenField runat="server" ID="hidInsuranceTooltip" Value='<%# Eval("InsuranceTooltip") %>' />
                                                    <asp:Label runat="server" CssClass="red flag" Visible='<%# Eval("InsuranceAlert").ToBoolean() %>'>
                                                <abbr title='<%# Eval("InsuranceTooltip") %>' >VI</abbr>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidVendorIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidShipmentVendorId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomHiddenField ID="hidVendorId" runat="server" Value='<%# Eval("VendorId") %>' />
                                                    <eShip:CustomTextBox ID="txtVendorNumber" runat="server" Text='<%# Eval("VendorNumber") %>' CssClass="w70 disabled" ReadOnly="True" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' CssClass="w150 disabled" ReadOnly="True" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtProNumber" runat="server" Text='<%# Eval("ProNumber") %>' CssClass="w150" MaxLength="50" />
                                                </td>
                                                <td>
                                                    <eShip:CachedObjectDropDownList runat="server" ID="ddlFailureCode" CssClass="w150" Type="FailureCodes" EnableNotApplicableLong="True"
                                                        DefaultValue="0" SelectedValue='<%# Eval("FailureCodeId") %>' />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtFailureComments" runat="server" Text='<%# Eval("FailureComments") %>' CssClass="w260" MaxLength="500" />
                                                </td>
                                                <td class="imageButtons" style="text-align: center;">
                                                    <asp:ImageButton runat="server" ID="ibtnLogVendorRejection" ImageUrl="~/images/icons2/cog.png" ToolTip="Delete line and log vendor rejection"
                                                        CausesValidation="False" OnClick="OnLogNonPrimaryVendorRejection" Enabled='<%# Access.Modify %>' OnClientClick="ShowProcessingDivBlock();"
                                                        Visible='<%# !(bool)Eval("IsRelationBetweenVendorAndVendorBill") %>' />
                                                    <asp:ImageButton runat="server" ID="ibtnViewAdditionalVendorPerformanceStatistics" ImageUrl="~/images/icons2/statsBlue.png" OnClick="OnViewAdditionalVendorPerformanceStatisticsClicked"
                                                        ToolTip="See Vendor Performance Statistics" Width="20px" OnClientClick="ShowProcessingDivBlock();" />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" ToolTip="Delete line"
                                                        CausesValidation="false" OnClick="OnDeleteVendorClicked" Enabled='<%# Access.Modify %>' OnClientClick="ShowProcessingDivBlock();"
                                                        Visible='<%# !(bool)Eval("IsRelationBetweenVendorAndVendorBill") %>' />
                                                    <eShip:CustomHiddenField ID="hidIsRelationBetweenVendorAndVendorBill" runat="server" Value='<%# Eval("IsRelationBetweenVendorAndVendorBill") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>

                            </asp:Panel>

                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddVendorFromFinder" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <hr class="dark" />

                    <asp:UpdatePanel runat="server" ID="upAccountBuckets" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:Panel ID="pnlAccountBuckets" runat="server" DefaultButton="btnAddAccountbucket" CssClass="rowgroup">

                                <div class="row mb10">
                                    <div class="fieldgroup left">
                                        <label class="upper blue fs120em">
                                            Additional Account Buckets
                                    <asp:Literal runat="server" ID="litAccountBucketCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddAccountbucket" Text="Add Account Bucket" CausesValidation="false" OnClick="OnAddAccountBucketClicked" />
                                        <asp:Button runat="server" ID="btnClearAccountbuckets" Text="Clear Account Buckets" CausesValidation="false" OnClick="OnClearAccountBucketsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>

                                <table class="stripe">
                                    <tr>
                                        <th style="width: 20%;">Code
                                        </th>
                                        <th style="width: 70%;">Description
                                        </th>
                                        <th style="width: 10%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstAccountBuckets" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>

                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />

                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidAccountBucketIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidShipmentAccountBucketId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomHiddenField ID="hidAccountBucketId" runat="server" Value='<%# Eval("AccountBucketId") %>' />
                                                    <eShip:CustomTextBox ID="txtAccountBucketCode" runat="server" Text='<%# Eval("AccountBucketCode") %>' ReadOnly="True" CssClass="w150 disabled" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtAccountBucketDescription" runat="server" Text='<%# Eval("AccountBucketDescription") %>' ReadOnly="True" CssClass="w300 disabled" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProcessingDivBlock();"
                                                        CausesValidation="false" OnClick="OnDeleteAccountBucketClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>

                            </asp:Panel>

                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddAccountbucket" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <hr class="dark" />

                    <asp:UpdatePanel runat="server" ID="upQuickPayOptions" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:Panel runat="server" ID="pnlQuickPayOptions" DefaultButton="btnAddQuickPayOption" CssClass="rowgroup">
                                <div class="row mb10">
                                    <div class="fieldgroup left">
                                        <label class="upper blue fs120em">
                                            Quick Pay Options
                                    <asp:Literal runat="server" ID="litQuickPayCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddQuickPayOption" Text="Add Quick Pay Option" CausesValidation="false" OnClick="OnAddQuickPayOptionClicked" />
                                        <asp:Button runat="server" ID="btnClearQuickPayOptions" Text="Clear Quick Pay Options" CausesValidation="false" OnClick="OnClearQuickPayOptionsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>

                                <table class="stripe">
                                    <tr>
                                        <th style="width: 70%;">Description
                                        </th>
                                        <th style="width: 20%;" class="text-right pr5">Discount Percent (%)
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstQuickPayOptions" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>

                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />

                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtDescription" runat="server" Text='<%# Eval("Description") %>' ReadOnly="True" CssClass="w300 disabled" />
                                                </td>
                                                <td class="text-right pr5">
                                                    <eShip:CustomHiddenField ID="hidQuickPayOptionIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidQuickPayOptionId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomTextBox ID="txtDiscountPercent" runat="server" Text='<%# Eval("DiscountPercent") %>' ReadOnly="True" CssClass="w150 disabled" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProcessingDivBlock();"
                                                        CausesValidation="false" OnClick="OnDeleteQuickPayOptionClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>

                                </table>
                            </asp:Panel>

                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddQuickPayOption" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <hr class="dark" />

                    <asp:UpdatePanel runat="server" ID="upAssets" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:Panel runat="server" ID="pnlAssets" DefaultButton="btnAddAsset">

                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <label class="upper blue fs120em">
                                            Assets
                                    <asp:Literal runat="server" ID="litAssetsCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup pt5 pl46">
                                        <span class="note fs75em">*** mileage engine will use origin-destination combination for mileage calculation.</span>
                                    </div>

                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" Text="Add Asset" ID="btnAddAsset" CausesValidation="false" OnClick="OnAddAssetClicked" />
                                        <asp:Button runat="server" Text="Clear Assets" ID="btnClearAsset" CausesValidation="false" OnClick="OnClearAssetsClicked" />
                                    </div>


                                </div>

                                <table class="stripe">
                                    <tr>
                                        <th style="width: 21%;">Driver Asset
                                        </th>
                                        <th style="width: 21%;">Tractor Asset
                                        </th>
                                        <th style="width: 21%;">Trailer Asset
                                        </th>
                                        <th style="width: 14%;">Miles Run
                                        </th>
                                        <th style="width: 12%;">Mileage Engine
                                        </th>
                                        <th style="width: 6%;" class="text-center">Primary
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstAssets" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnAssetsItemDataBound">
                                        <LayoutTemplate>

                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />

                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidShipmentAssetIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidShipmentAssetId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <asp:DropDownList runat="server" ID="ddlShipmentDriverAsset" DataValueField="Value" DataTextField="Text" CssClass="w180" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlShipmentTractorAsset" DataTextField="Text" DataValueField="Value" runat="server" CssClass="w180" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlShipmentTrailerAsset" DataTextField="Text" DataValueField="Value" runat="server" CssClass="w180" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtShipmentMilesRun" runat="server" Text='<%# Eval("MilesRun") %>' CssClass="w70" />
                                                    <asp:ImageButton ID="ibtnGetMiles" runat="server" ImageUrl="~/images/icons2/cog.png" CausesValidation="false" ToolTip="Get Mileage" OnClick="OnGetMilesClicked" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlMilageEngine" runat="server" DataTextField="Text" DataValueField="Value" CssClass="w150" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox ID="chkPrimary" runat="server" Checked='<%# Eval("Primary") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteAssetClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>

                                </table>
                            </asp:Panel>

                        </ContentTemplate>
                    </asp:UpdatePanel>


                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabStops" HeaderText="Stops">
                <ContentTemplate>

                    <asp:UpdatePanel runat="server" ID="upPnlStops" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlStops">
                                <div class="row mb20">
                                    <div class="fieldgroup left" id="stopsInstructionOptionDiv">

                                        <script type="text/javascript">
                                            function EnforceMutuallyStopsInstructionCheckBox(control) {

                                                $("#stopsInstructionOptionDiv").find("input[type='checkbox']").not($(control))
                                                    .each(function () {
                                                        jsHelper.UnCheckBox($(this).attr("id"));
                                                        SetAltUniformCheckBoxClickedStatus($(this));
                                                    });
                                            }

                                        </script>

                                        <eShip:AltUniformCheckBox runat="server" ID="chkOriginInstruction" Checked="true" OnClientSideClicked="EnforceMutuallyStopsInstructionCheckBox(this);" />
                                        <label class="mr20">Use origin Instruction</label>
                                        <eShip:AltUniformCheckBox runat="server" ID="chkDestinationInstruction" OnClientSideClicked="EnforceMutuallyStopsInstructionCheckBox(this);" />
                                        <label>Use destination Instruction</label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddStop" Text="Add Stop" CausesValidation="false" OnClick="OnAddLocationClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnAddStopFromAddressBook" Text="Add Stop from Address Book"
                                            CausesValidation="false" OnClick="OnAddStopFromAddressBookClicked" />
                                    </div>
                                </div>

                                <asp:ListView ID="lstStops" runat="server" OnItemDataBound="OnStopsItemDataBound"
                                    ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <table class="stripe">
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr class="bottom_shadow">
                                            <td class="pt100 text-center top">
                                                <div class="border-right pr5">
                                                    <asp:ImageButton ID="ibtnMoveUp" runat="server" ImageUrl="~/images/icons2/arrowUp.png"
                                                        OnClick="OnMoveUpClicked" CausesValidation="false" OnClientClick="ShowProcessingDivBlock();" />
                                                    <br />
                                                    <b class="mt10 mb10 fs200em"><%# Container.DataItemIndex + 1 %></b>
                                                    <br />
                                                    <asp:ImageButton ID="ibtnMoveDown" runat="server" ImageUrl="~/images/icons2/arrowDown.png"
                                                        OnClick="OnMoveDownClicked" CausesValidation="false" OnClientClick="ShowProcessingDivBlock();" />
                                                </div>
                                            </td>
                                            <td>
                                                <eShip:LocationListingInputControl ID="llicLocation" runat="server" LocationItemIndex='<%# Container.DataItemIndex %>' OnDeleteLocation="OnDeleteLocation" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddStopFromAddressBook" />
                        </Triggers>
                    </asp:UpdatePanel>

                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabDocuments" runat="server" HeaderText="Documents">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDocuments" DefaultButton="btnAddDocument">

                        <div class="row mb10">
                            <div class="fieldgroup">
                                <asp:Button runat="server" ID="btnAddLoadOrderDocuments" Text="Add Load Order Documents" Visible="False"
                                    OnClick="OnAddLoadOrderDocumentsClicked"/>
                            </div>
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnAddDocument" Text="Add Document" CausesValidation="false" OnClick="OnAddDocumentClicked" />
                                <asp:Button runat="server" ID="btnClearDocuments" Text="Clear Documents" CausesValidation="false" OnClick="OnClearDocumentsClicked" />
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("a[id$='lnkShipmentDocumentLocationPath']").each(function () {
                                    jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                                });
                            });
                        </script>
                        <table class="stripe">
                            <tr>
                                <th style="width: 5%;" class="text-center">Internal
                                </th>
                                <th style="width: 17%;">Name
                                </th>
                                <th style="width: 25%;">Description
                                </th>
                                <th style="width: 20%;">Document Tag
                                </th>
                                <th style="width: 25%;">File
                                </th>
                                <th style="width: 8%;" class="text-center">Action
                                </th>
                            </tr>
                            <asp:ListView ID="lstDocuments" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="text-center">
                                            <eShip:CustomHiddenField runat="server" ID="hidIsInternal" Value='<%# Eval("IsInternal") %>' />
                                            <%# Eval("IsInternal").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidDocumentIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                            <eShip:CustomHiddenField ID="hidDocumentId" runat="server" Value='<%# Eval("Id") %>' />
                                            <asp:Literal ID="litDocumentName" runat="server" Text='<%# Eval("Name") %>' />
                                        </td>
                                        <td>
                                            <asp:Literal ID="litDocumentDescription" runat="server" Text='<%# Eval("Description") %>' />
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidDocumentTagId" runat="server" Value='<%# Eval("DocumentTagId") %>' />
                                            <asp:Literal ID="litDocumentTag" runat="server" Text='<%# new DocumentTag(Eval("DocumentTagId").ToLong()).FormattedString() %>' />
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidLocationPath" runat="server" Value='<%# Eval("LocationPath") %>' />
                                            <asp:LinkButton runat="server" ID="lnkShipmentDocumentLocationPath" Text='<%# GetLocationFileName(Eval("LocationPath")) %>'
                                                OnClick="OnLocationPathClicked" CausesValidation="false" CssClass="blue" />
                                        </td>
                                        <td class="text-center">
                                            <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                CausesValidation="false" OnClick="OnEditDocumentClicked" Enabled='<%# Access.Modify %>' />
                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                CausesValidation="false" OnClick="OnDeleteDocumentClicked" Enabled='<%# Access.Modify %>' />
                                        </td>
                                    </tr>
	                                <tr class="hidden">
		                                <td>&nbsp;</td>
	                                </tr>
	                                <tr>
		                                <td colspan="13" class="pt0 mt0 bottom_shadow pl20">
			                                <eShip:CustomHiddenField ID="hidVendorBillId" runat="server" Value='<%# Eval("VendorBill.Id") %>' />
			                                <div class="col_1_2 ">
				                                <label class="wlabel blue">Vendor</label>
				                                <eShip:CustomTextBox runat="server" ID="txtVendor" Text='<%# Eval("VendorBill.VendorLocation.Vendor.Name")%>' CssClass="w240 disabled" ReadOnly="True" />
			                                </div>
			                                <div class="col_1_2 ">
				                                <label class="wlabel blue">Vendor Bill Number </label>
				                                <eShip:CustomTextBox runat="server" ID="txtVendorBillNumber" Text='<%#  Eval("VendorBill.DocumentNumber") %>' CssClass="w240 disabled" ReadOnly="True" />
			                                </div>
		                                </td>
	                                </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </table>

                        <asp:Panel runat="server" ID="pnlEditDocument" Visible="false" CssClass="popupControl popupControlOverW500">
                            <div class="popheader mb10">
                                <h4>Add/Modify Document</h4>
                                <asp:ImageButton ID="ibtnCloseEditDocument" ImageUrl="~/images/icons2/close.png"
                                    CssClass="close" CausesValidation="false" OnClick="OnCloseEditDocumentClicked" runat="server" />
                            </div>

                            <table class="poptable">
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Name:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtDocumentName" CssClass="w200" MaxLength="50" />
                                        <asp:CheckBox runat="server" ID="chkDocumentIsInternal" CssClass="jQueryUniform ml10" />
                                        <label>Is Internal</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Document Tag:</label>
                                    </td>
                                    <td>
                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlDocumentTag" CssClass="w300" Type="DocumentTag" EnableChooseOne="True" DefaultValue="0" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right top">
                                        <label class="upper">Description:</label>
                                        <br />
                                        <label class="lhInherit">
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDocumentDescription" MaxLength="500" TargetControlId="txtDocumentDescription" />
                                        </label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtDocumentDescription" CssClass="w300 h150" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Location Path:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomHiddenField ID="hidLocationPath" runat="server" />
                                        <asp:FileUpload runat="server" ID="fupLocationPath" CssClass="jQueryUniform" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Current File:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox ID="txtLocationPath" runat="server" ReadOnly="True" CssClass="w300 disabled" />
                                    </td>
									 <eShip:CustomHiddenField ID="hidVendorBillId" runat="server" />
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Button ID="btnEditDocumentDone" Text="Done" OnClick="OnEditDocumentDoneClicked" runat="server" CausesValidation="false" />
                                        <asp:Button ID="btnCloseEditDocument" Text="Cancel" OnClick="OnCloseEditDocumentClicked" runat="server" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>


        <asp:Panel runat="server" ID="pnlEditInvoicedShipment" Visible="False" CssClass="popup">
            <div class="popheader">
                <h4>Edit Invoiced Shipment
                </h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCloseEditInvoicedShipmentClicked" runat="server" />
            </div>

            <div class="pl10 pr10">
                <div class="rowgroup mt10">
                    <div class="col_1_2 bbox">
                        <div class="col_1_2 bbox vlinedarkright">
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel blue">Actual Pickup Date</label>
                                    <eShip:CustomTextBox runat="server" ID="txtEditActualPickupDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                </div>
                            </div>
                        </div>
                        <div class="col_1_2 bbox vlinedarkright pl20">
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel blue">Actual Delivery Date</label>
                                    <eShip:CustomTextBox runat="server" ID="txtEditActualDeliveryDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col_1_2 bbox">
                        <div class="col_1_2 bbox vlinedarkright pl20">
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel blue">Shipper Reference</label>
                                    <eShip:CustomTextBox runat="server" ID="txtReference" MaxLength="50" Width="225px"></eShip:CustomTextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col_1_2 bbox pl20">
                            <div class="row">
                                <div class="fieldgroup_s">
                                    <asp:CheckBox runat="server" ID="chkCreatedInError" Text="Created in Error" CssClass="jQueryUniform upper blue" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <table class="stripe">
                        <tr>
                            <th style="width: 30%;">Vendor Name
                            </th>
                            <th style="width: 20%;">Failure Code
                            </th>
                            <th style="width: 35%;">Comment
                            </th>
                            <th style="width: 10%;">Pro Number
                            </th>
                            <th style="width: 5%;" class="text-center">Primary
                            </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstEditInvoicedShipment">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <eShip:CustomHiddenField ID="hidShipmentVendorId" runat="server" Value='<%# Eval("Id") %>' />
                                        <%# Eval("VendorNumber") %> - <%# Eval("Name") %>
                                    </td>
                                    <td>
                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlEditFailureCodes" CssClass="w200" Type="FailureCodes" EnableNotApplicableLong="True"
                                            DefaultValue="0" SelectedValue='<%# Eval("FailureCodeId") %>' />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox MaxLength="500" ID="txtEditFailureComments" runat="server" Text='<%# Eval("FailureComments") %>' CssClass="w310" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox MaxLength="50" ID="txtProNumber" runat="server" Text='<%# Eval("ProNumber") %>' CssClass="w150" />
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("primary").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>

                <div class="rowgroup mt10">
                    <asp:Button ID="btnDoneEditInvoicedShipmentClick" Text="Update" OnClick="OnDoneEditInvoicedShipmentClick" runat="server" CausesValidation="False" />
                    <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseEditInvoicedShipmentClicked" runat="server" CausesValidation="false" />
                </div>
            </div>

        </asp:Panel>

        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
        <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackground" runat="server" Style="display: none;" />


        <asp:UpdatePanel runat="server" ID="upHiddens" UpdateMode="Always">
            <ContentTemplate>

                <eShip:CustomHiddenField runat="server" ID="hidFlag" />
                <eShip:CustomHiddenField runat="server" ID="hidCustomerOutstandingBalance" />
                <eShip:CustomHiddenField runat="server" ID="hidCustomerCredit" />
                <eShip:CustomHiddenField runat="server" ID="hidLoadedShipmentTotalDue" />
                <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
                <eShip:CustomHiddenField runat="server" ID="hidShoppedRateId" />
                <eShip:CustomHiddenField runat="server" ID="hidNotificationsRequired" />
                <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
                <eShip:CustomHiddenField runat="server" ID="hidResellerAdditionId" />
                <eShip:CustomHiddenField runat="server" ID="hidBillReseller" />
                <eShip:CustomHiddenField runat="server" ID="hidEditStatus" />
                <eShip:CustomHiddenField runat="server" ID="hidUpdateTerminalInfo" />
                <eShip:CustomHiddenField runat="server" ID="hidMiscReceiptsRelatedToShipmentExist" />
                <eShip:CustomHiddenField runat="server" ID="hidCanDispatch" />
                <eShip:CustomHiddenField runat="server" ID="hidJobId" />

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>
