﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="AssetLogView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.AssetLogView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Asset Log<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />
        <asp:Panel ID="pnlSearchPanel" runat="server">
            <table class="poptable">
                <tr>
                    <td>
                        <label class="upper blue">Asset: </label>
                        <eShip:CustomHiddenField runat="server" ID="hidAssetId" />
                        <eShip:CustomTextBox runat="server" ID="txtAssetNumber" CssClass="w75" OnTextChanged="OnAssetNumberEntered"
                            AutoPostBack="true" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvAssetNumber" ControlToValidate="txtAssetNumber"
                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                        <asp:ImageButton ID="ibtnFindAsset" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                            CausesValidation="false" OnClick="OnAssetSearchClicked" />
                        <eShip:CustomTextBox runat="server" ID="txtAssetDescription" CssClass="w170 disabled" ReadOnly="true" />
                        <span class="note green"> &#8226; Select an asset to see logs for that asset. </span>
                    </td>
                </tr>
            </table>
            <hr class="fat" />
        </asp:Panel>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAssetLogTable" TableId="assetLogTable" HeaderZIndex="2"/>
        <div class="rowgroup">
            <table class="line2 pl2" id="assetLogTable">
                <tr>
                    <th style="width: 8%;">Date Created
                    </th>
                    <th style="width: 8%;">Log Date
                    </th>
                    <th style="width: 5%;">Shipment Number
                    </th>
                    <th style="width: 21%;">Comment
                    </th>
                    <th style="width: 8%;">Mileage Engine
                    </th>
                    <th style="width: 8%;">Miles Run
                    </th>
                    <th style="width: 30%;">Address
                    </th>
                    <th style="width: 12%;" class="text-center">Action
                    </th>
                </tr>
                <asp:ListView ID="lstAssetLogs" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <eShip:CustomHiddenField ID="assetLogId" runat="server" Value='<%# Eval("Id") %>' />
                                <%# Eval("DateCreated").FormattedShortDate()%>
                            </td>
                            <td>
                                <%# Eval("LogDate").FormattedShortDate()%>
                            </td>
                            <td>
                                <%# ActiveUser.HasAccessTo(ViewCode.Shipment) && !string.IsNullOrEmpty(Eval("ShipmentNumber").GetString())
                                    ? string.Format("{0} <a href='{1}?{2}={0}' class='blue' target='_blank' title='Go To Shipment Record'><img src={3} width='16' class='middle'/></a>", 
                                        Eval("ShipmentNumber"),
                                        ResolveUrl(ShipmentView.PageAddress),
                                        WebApplicationConstants.TransferNumber, 
                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                    : Eval("ShipmentNumber") %>
                            </td>
                            <td>
                                <%# Eval("Comment") %>
                            </td>
                            <td>
                                <%# Eval("MileageEngine").ToString()%>
                            </td>
                            <td class="text-right">
                                <%# Eval("MilesRun")%>
                            </td>
                            <td>
                                <%# Eval("Street1")%>
                                <%# Eval("Street2")%>
                                <%# Eval("City")%>
                                <%# Eval("State")%>
                                <%# Eval("PostalCode")%>
                                <%# Eval("CountryCode")%>
                            </td>
                            <td class="text-center">
                                <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                    CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                    CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                                <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                                    ConfirmText="Are you sure you want to delete record?" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </div>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>Add/Modify Asset Log
                    </h4>
                    <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Date Created:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtDateCreated" CssClass="w100 disabled" ReadOnly="True" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Log Date:</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidAssetLogId" runat="server" />
                                <eShip:CustomTextBox runat="server" ID="txtLogDate" CssClass="w100" Type="Date" placeholder="99/99/9999"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Street1:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtStreet1" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Street2:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtStreet2" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">City:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtCity" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">State:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtState" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Postal Code:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtPostalCode" CssClass="w200" MaxLength="10" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Country:</label>
                            </td>
                            <td>
                                <eShip:CachedObjectDropDownList Type="Countries" ID="ddlCountries" runat="server" CssClass="w200" EnableChooseOne="True" DefaultValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Mileage Engine:</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlMilageEngine" runat="server" CssClass="w200" DataTextField="Text"
                                    DataValueField="Value" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Miles Run:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtMilesRun" CssClass="w200" MaxLength="10" runat="server" />
                                <ajax:FilteredTextBoxExtender ID="ftbeMilesRun" runat="server" TargetControlID="txtMilesRun"
                                    FilterType="Custom, Numbers" ValidChars="." />
                                <asp:ImageButton ID="ibtnGetMiles" runat="server" ImageUrl="~/images/icons2/cog.png"
                                    CausesValidation="false" ToolTip="Get Mileage" OnClick="OnGetMilesClicked" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper top">Comment:</label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleComment" TargetControlId="txtComment" MaxLength="200" />
                                </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtComment" CssClass="w200" runat="server" TextMode="MultiLine" Rows="5" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" />
    <eShip:AssetFinderControl ID="assetFinder" runat="server" OnItemSelected="OnAssetFinderItemSelected"
        OnSelectionCancel="OnAssetFinderSelectionCancelled" Visible="false" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>

