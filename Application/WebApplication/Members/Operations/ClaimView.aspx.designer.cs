﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogisticsPlus.Eship.WebApplication.Members.Operations {
    
    
    public partial class ClaimView {
        
        /// <summary>
        /// memberToolBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.MainToolbarControl memberToolBar;
        
        /// <summary>
        /// imgPageImageLogo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imgPageImageLogo;
        
        /// <summary>
        /// ridcClaimNumberIdentity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.RecordIdentityDisplayControl ridcClaimNumberIdentity;
        
        /// <summary>
        /// ridcClaimCustomerNameIdentity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.RecordIdentityDisplayControl ridcClaimCustomerNameIdentity;
        
        /// <summary>
        /// litMessage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litMessage;
        
        /// <summary>
        /// litErrorMessages control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litErrorMessages;
        
        /// <summary>
        /// claimFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ClaimFinderControl claimFinder;
        
        /// <summary>
        /// customerFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.CustomerFinderControl customerFinder;
        
        /// <summary>
        /// vendorFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.VendorFinderControl vendorFinder;
        
        /// <summary>
        /// serviceTicketFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ServiceTicketFinderControl serviceTicketFinder;
        
        /// <summary>
        /// shipmentFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ShipmentFinderControl shipmentFinder;
        
        /// <summary>
        /// athtuTabUpdater control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.AjaxTabHeaderTextUpdater athtuTabUpdater;
        
        /// <summary>
        /// hidClaimId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidClaimId;
        
        /// <summary>
        /// tabServiceTicket control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabContainer tabServiceTicket;
        
        /// <summary>
        /// tabDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabDetails;
        
        /// <summary>
        /// pnlDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlDetails;
        
        /// <summary>
        /// txtClaimNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtClaimNumber;
        
        /// <summary>
        /// txtStatus control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtStatus;
        
        /// <summary>
        /// txtDateCreated control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtDateCreated;
        
        /// <summary>
        /// txtUser control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtUser;
        
        /// <summary>
        /// hidCustomerId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidCustomerId;
        
        /// <summary>
        /// txtCustomerNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCustomerNumber;
        
        /// <summary>
        /// rfvCustomer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvCustomer;
        
        /// <summary>
        /// btnFindCustomer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton btnFindCustomer;
        
        /// <summary>
        /// txtCustomerName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCustomerName;
        
        /// <summary>
        /// aceCustomer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.AutoCompleteExtender aceCustomer;
        
        /// <summary>
        /// txtDateLpAcctToPayCarrier  control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtDateLpAcctToPayCarrier ;
        
        /// <summary>
        /// txtAcknowledged control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtAcknowledged;
        
        /// <summary>
        /// txtClaimantReferenceNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtClaimantReferenceNumber;
        
        /// <summary>
        /// txtClaimDate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtClaimDate;
        
        /// <summary>
        /// ddlClaimType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlClaimType;
        
        /// <summary>
        /// txtAmountClaimed control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtAmountClaimed;
        
        /// <summary>
        /// ddlAmountClaimedType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlAmountClaimedType;
        
        /// <summary>
        /// txtEstimatedRepairCost control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtEstimatedRepairCost;
        
        /// <summary>
        /// chkIsRepairable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.AltUniformCheckBox chkIsRepairable;
        
        /// <summary>
        /// txtCheckNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCheckNumber;
        
        /// <summary>
        /// txtCheckAmount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCheckAmount;
        
        /// <summary>
        /// tamleClaimDetailText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.TextAreaMaxLengthExtender tamleClaimDetailText;
        
        /// <summary>
        /// txtClaimDetail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtClaimDetail;
        
        /// <summary>
        /// tabVendors control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabVendors;
        
        /// <summary>
        /// upPnlVendors control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel upPnlVendors;
        
        /// <summary>
        /// pnlVendors control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlVendors;
        
        /// <summary>
        /// btnAddVendorFromFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAddVendorFromFinder;
        
        /// <summary>
        /// btnClearVendors control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnClearVendors;
        
        /// <summary>
        /// tfheClaimVendorTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.FreeHeaderExtender.TableFreezeHeaderExtender tfheClaimVendorTable;
        
        /// <summary>
        /// lstVendors control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView lstVendors;
        
        /// <summary>
        /// tabDocuments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabDocuments;
        
        /// <summary>
        /// pnlDocuments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlDocuments;
        
        /// <summary>
        /// btnAddDocument control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAddDocument;
        
        /// <summary>
        /// btnClearDocuments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnClearDocuments;
        
        /// <summary>
        /// tfheClaimDocumentTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.FreeHeaderExtender.TableFreezeHeaderExtender tfheClaimDocumentTable;
        
        /// <summary>
        /// lstDocuments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView lstDocuments;
        
        /// <summary>
        /// pnlEditDocument control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlEditDocument;
        
        /// <summary>
        /// ImageButton5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ImageButton5;
        
        /// <summary>
        /// txtDocumentName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtDocumentName;
        
        /// <summary>
        /// tamleDocumentDescription control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.TextAreaMaxLengthExtender tamleDocumentDescription;
        
        /// <summary>
        /// txtDocumentDescription control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtDocumentDescription;
        
        /// <summary>
        /// ddlDocumentTag control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CachedObjectDropDownList ddlDocumentTag;
        
        /// <summary>
        /// hidDocumentTagId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidDocumentTagId;
        
        /// <summary>
        /// hidLocationPath control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidLocationPath;
        
        /// <summary>
        /// fupLocationPath control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload fupLocationPath;
        
        /// <summary>
        /// litLocationPath control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litLocationPath;
        
        /// <summary>
        /// Button2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Button2;
        
        /// <summary>
        /// Button3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Button3;
        
        /// <summary>
        /// tabNotes control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabNotes;
        
        /// <summary>
        /// upPnlNotes control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel upPnlNotes;
        
        /// <summary>
        /// pnlNotes control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlNotes;
        
        /// <summary>
        /// peLstNotes control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.PaginationExtender peLstNotes;
        
        /// <summary>
        /// btnAddNote control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAddNote;
        
        /// <summary>
        /// tfheClaimNoteTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.FreeHeaderExtender.TableFreezeHeaderExtender tfheClaimNoteTable;
        
        /// <summary>
        /// itemPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder itemPlaceHolder;
        
        /// <summary>
        /// lstNotes control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView lstNotes;
        
        /// <summary>
        /// pnlEditNote control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlEditNote;
        
        /// <summary>
        /// ImageButton2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ImageButton2;
        
        /// <summary>
        /// ddlType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlType;
        
        /// <summary>
        /// tamleClaimNoteMessage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.TextAreaMaxLengthExtender tamleClaimNoteMessage;
        
        /// <summary>
        /// txtMessage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtMessage;
        
        /// <summary>
        /// chkArchived control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.AltUniformCheckBox chkArchived;
        
        /// <summary>
        /// chkClassified control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.AltUniformCheckBox chkClassified;
        
        /// <summary>
        /// txtReminderDate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtReminderDate;
        
        /// <summary>
        /// txtReminderTime control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtReminderTime;
        
        /// <summary>
        /// chkSendReminder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.AltUniformCheckBox chkSendReminder;
        
        /// <summary>
        /// tamleReminderEmails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.TextAreaMaxLengthExtender tamleReminderEmails;
        
        /// <summary>
        /// txtReminderEmails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtReminderEmails;
        
        /// <summary>
        /// btnEditNoteDone control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnEditNoteDone;
        
        /// <summary>
        /// btnEditNoteCancel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnEditNoteCancel;
        
        /// <summary>
        /// tabShipments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabShipments;
        
        /// <summary>
        /// upPnlShipments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel upPnlShipments;
        
        /// <summary>
        /// pnlShipments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlShipments;
        
        /// <summary>
        /// btnAddShipment control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAddShipment;
        
        /// <summary>
        /// btnClearShipments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnClearShipments;
        
        /// <summary>
        /// tfheClaimShipmentTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.FreeHeaderExtender.TableFreezeHeaderExtender tfheClaimShipmentTable;
        
        /// <summary>
        /// lstShipments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView lstShipments;
        
        /// <summary>
        /// tabServiceTickets control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabServiceTickets;
        
        /// <summary>
        /// upPnlServiceTickets control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel upPnlServiceTickets;
        
        /// <summary>
        /// pnlServiceTickets control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlServiceTickets;
        
        /// <summary>
        /// btnAddServiceTicket control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAddServiceTicket;
        
        /// <summary>
        /// btnClearServiceTickets control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnClearServiceTickets;
        
        /// <summary>
        /// tfheClaimServiceTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.FreeHeaderExtender.TableFreezeHeaderExtender tfheClaimServiceTable;
        
        /// <summary>
        /// lstServiceTickets control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView lstServiceTickets;
        
        /// <summary>
        /// tabAuditLog control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel tabAuditLog;
        
        /// <summary>
        /// auditLogs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.AuditLogControl auditLogs;
        
        /// <summary>
        /// messageBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.MessageBoxControl messageBox;
        
        /// <summary>
        /// pnlDimScreen control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlDimScreen;
        
        /// <summary>
        /// hidFlag control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidFlag;
        
        /// <summary>
        /// hidEditingIndex control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidEditingIndex;
        
        /// <summary>
        /// hidFilesToDelete control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidFilesToDelete;
        
        /// <summary>
        /// hidEditing control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidEditing;
    }
}
