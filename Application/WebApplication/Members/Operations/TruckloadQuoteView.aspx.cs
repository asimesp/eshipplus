﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;


namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class TruckloadQuoteView : MemberPageBase, IRateAndScheduleView
    {
        public static string PageAddress { get { return "~/Members/Operations/TruckloadQuoteView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.TruckloadQuote; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public List<EquipmentType> EquipmentTypes
        {
            set { quoteRequestForm.EquipmentTypes = value; }
        }

        public List<ServiceViewSearchDto> Services
        {
            set { quoteRequestForm.Services = value; }
        }

        public bool SaveOriginToAddressBook { get; protected set; }
        public bool SaveOriginWithServicesToAddressBook { get; protected set; }
        public bool SaveDestinationToAddressBook { get; protected set; }
        public bool SaveDestinationWithServicesToAddressBook { get; protected set; }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Shipment, PaymentInformation>> SaveShipment;
        public event EventHandler<ViewEventArgs<LoadOrder, PaymentInformation>> SaveLoadOrder;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;


        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayCustomer(Customer customer)
        {
            quoteRequestForm.DisplayCustomer(customer);

            // filter by customer if one has been selected
            libraryItemFinder.CustomerIdSpecificFilter = customer == null ? default(long) : customer.Id;
            libraryItemFinder.Reset();
        }

        public void Set(Shipment shipment)
        {
        }

        public void Set(LoadOrder loadOrder)
        {
            Session[WebApplicationConstants.RateAndScheduleTransferId] = loadOrder.Id;
            Session[WebApplicationConstants.RateAndScheduleTransferType] = WebApplicationConstants.RateAndScheduleOfferedLoadTransfer;
            hidTransferToConfirmation.Value = true.ToString();
        }

        public void Set(ShoppedRate shoppedRate)
        {
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new RateAndScheduleHandler(this).Initialize();

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());

            if (Session[WebApplicationConstants.TransferLoadOrderObject] != null)
            {
                quoteRequestForm.LoadLoadOrder((LoadOrder)Session[WebApplicationConstants.TransferLoadOrderObject]);
                Session[WebApplicationConstants.TransferLoadOrderObject] = null;
            }
        }



        protected void OnQuoteRequestFormError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSubmitQuoteRequestClicked(object sender, ViewEventArgs<LoadOrder> e)
        {
            if (SaveLoadOrder != null)
                SaveLoadOrder(this, new ViewEventArgs<LoadOrder, PaymentInformation>(e.Argument, null));

            if (hidTransferToConfirmation.Value.ToBoolean())
                Response.Redirect(RateAndScheduleConfirmationView.PageAddress);
        }



        protected void OnQuoteRequestFormCustomerSearch(object sender, ViewEventArgs<string> e)
        {
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(e.Argument));

        }

        protected void OnQuoteRequestFormOpenCustomerFinder(object sender, EventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }


        protected void OnQuoteRequestOpenLibraryItemFinder(object sender, EventArgs e)
        {
            libraryItemFinder.Visible = true;
        }

        protected void OnLibraryItemFinderMultiItemSelected(object sender, ViewEventArgs<List<LibraryItem>> e)
        {
            quoteRequestForm.AddLibraryItemsToItems(e.Argument);
            libraryItemFinder.Visible = false;
        }

        protected void OnLibraryItemFinderItemCancelled(object sender, EventArgs e)
        {
            libraryItemFinder.Visible = false;
        }


        protected void OnQuoteRequestFormOpenPostalCodeFinder(object sender, EventArgs e)
        {
            postalCodeFinder.Visible = true;
        }

        protected void OnPostalCodeFinderItemSelected(object sender, ViewEventArgs<PostalCodeViewSearchDto> e)
        {
            quoteRequestForm.DisplayPostalCode(e.Argument);
            postalCodeFinder.Visible = false;
        }

        protected void OnPostalCodeFinderItemCancelled(object sender, EventArgs e)
        {
            postalCodeFinder.Visible = false;
        }
    }
}