﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="DatLoadboardAssetView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.DatLoadboardAssetView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                DAT Loadboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />

        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Dat" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>

            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>

                    <div class="right">
                        <div class="fieldgroup">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <label>Sort By:</label>
                                    <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                        OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="ddlSortBy" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAssetTable" TableId="assetTable" HeaderZIndex="2" />
        <div class="rowgroup">
            <table class="line2 pl2" id="assetTable">
                <tr>
                    <th style="width: 31%;">
                        <asp:LinkButton runat='server' ID="lbtnSortAssetId" CssClass="link_nounderline blue" CausesValidation="False" 
                            OnCommand="OnSortData" Text="Asset ID"/>
                    </th>
                    <th style="width: 31%;">
                        <asp:LinkButton runat='server' ID="lbtnSortLoadNumber" CssClass="link_nounderline blue" CausesValidation="False" 
                            OnCommand="OnSortData" Text="Load Order Number"/>
                    </th>
                    <th style="width: 12%;">
                        <asp:LinkButton runat='server' ID="lbtnSortDateCreated" CssClass="link_nounderline blue" CausesValidation="False" 
                            OnCommand="OnSortData" Text="Date Created"/>
                    </th>
                    <th style="width: 12%;"
                        ><asp:LinkButton runat='server' ID="lbtnSortExpirationDate" CssClass="link_nounderline blue" CausesValidation="False" 
                            OnCommand="OnSortData" Text="Expiration Date"/>
                    </th>
                    <th style="width: 6%;">
                        Posting Expired
                    </th>
                    <th style="width: 8%;" class="text-center">Actions
                    </th>
                </tr>
                <asp:ListView ID="lstAssets" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%# Eval("AssetId") %>
                                <eShip:CustomHiddenField ID="hidAssetId" runat="server" Value='<%# Eval("Id") %>' />
                            </td>
                            <td>
                                <%# Eval("IdNumber") %>
                                <%# ActiveUser.HasAccessTo(ViewCode.LoadOrder) && Eval("LoadExists").ToBoolean()
                                        ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Load Order Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                            ResolveUrl(LoadOrderView.PageAddress),
                                            WebApplicationConstants.TransferNumber,
                                            Eval("IdNumber"),
                                            ResolveUrl("~/images/icons2/arrow_newTab.png")) 
                                        : string.Empty %>

                                <img src='<%= ResolveUrl("~/images/icons2/alert.png") %>' style='<%# Eval("LoadExists").ToBoolean() ? "display: none": string.Empty %>' width="16"
                                    align="absmiddle" title="This Load Order No Longer Exists" />
                            </td>
                            <td>
                                <%# Eval("DateCreated").FormattedShortDate()%>
                            </td>
                            <td>
                                <%# Eval("ExpirationDate").FormattedShortDate()%>
                            </td>
                            <td class="text-center">
                                <%# DateTime.Now >= Eval("ExpirationDate").ToDateTime()
                                        ? WebApplicationConstants.HtmlCheck
                                        : string.Empty
                                        %>
                            </td>
                            <td class="text-center">
                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                    ToolTip="Delete DAT Asset" CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                                <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                                    ConfirmText="Are you sure you want to delete record?" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </div>


    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess"
         OnYes="OnMessageBoxYesClicked" OnNo="OnMessageBoxNoClicked" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:CustomHiddenField runat="server" ID="hidDatAssetId" />
</asp:Content>
