﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;


namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
	public partial class LoadTenderResponseView : MemberPageBaseWithPageStore, ITruckloadBidView
	{
		public override ViewCode PageCode
		{
			get { return ViewCode.XmlConnectRecord; }
		}

		public override string SetPageIconImage
		{
			set { imgPageImageLogo.ImageUrl = IconLinks.ConnectBlue; }
		}

		public static string PageAddress { get { return "~/Members/Operations/LoadTenderResponseView.aspx"; }}

		public event EventHandler<ViewEventArgs<TruckloadBid, LoadOrder>> TruckloadBidSave;
		public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidDelete;
		public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidLock;
		public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidUnLock;
		public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidLoadAuditLog;
		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
		{
			throw new NotImplementedException();
		}

		public void FailedLock(Lock @lock)
		{
			throw new NotImplementedException();
		}

		public List<ValidationMessage> TruckloadBidMsgs { get; set; }


		protected void Page_Load(object sender, EventArgs e)
		{
			new TruckloadBidHandler(this).Initialize();

			var loadOrderNumber = Request.QueryString[WebApplicationConstants.XmlConnectRecordsShipmentIdKey];
			var loadOrder = new LoadOrderSearch().FetchLoadOrderByLoadNumber(loadOrderNumber, ActiveUser.TenantId);
			var bid = new TruckloadBidSearch().FetchTruckloadBidByLoadOrderIdAndStatus(loadOrder.Id,
																					  TruckloadBidStatus.Processing.ToInt(),
																				   ActiveUser.TenantId);

			pnlVoidResponsePage.Visible = bid == null;
			responseYes.Enabled = bid != null;
			responseNo.Enabled = bid != null;

		}



		protected void OnYesClicked(object sender, EventArgs e)
		{
			pnlDimScreen.Visible = true;
			var loadOrderNumber = Request.QueryString[WebApplicationConstants.XmlConnectRecordsShipmentIdKey];
			var vendorId = Request.QueryString[WebApplicationConstants.LoadTenderEmailResponseVendorIdKey];
			var loadOrder = new LoadOrderSearch().FetchLoadOrderByLoadNumber(loadOrderNumber, ActiveUser.TenantId);
			var bid = new TruckloadBidSearch().FetchTruckloadBidByLoadOrderIdAndStatus(loadOrder.Id,
			                                                                          TruckloadBidStatus.Processing.ToInt(),
			                                                                           ActiveUser.TenantId);
			var primaryVendor = loadOrder.Vendors.FirstOrDefault(v=>v.Primary);
			if (primaryVendor == null || primaryVendor.Id != vendorId.ToLong())
			{
				DisplayMessages(new[] { ValidationMessage.Error("This tender is expired.") });
			}
			if (bid != null)
			{
				bid.BidStatus = TruckloadBidStatus.Covered;

				if (TruckloadBidLock != null)
					TruckloadBidLock(this, new ViewEventArgs<TruckloadBid>(bid));

				if (TruckloadBidSave != null)
					TruckloadBidSave(this, new ViewEventArgs<TruckloadBid, LoadOrder>(bid, loadOrder));

				if (TruckloadBidUnLock != null)
					TruckloadBidUnLock(this, new ViewEventArgs<TruckloadBid>(bid));


				Server.NotifyOutgoingLoadTenderResponseEmail(loadOrder, ActiveUser);
				DisplayMessages(new[] { ValidationMessage.Information("Confirmation email is sent to Load Order co-ordinator.") });
			}
			else
			{
				DisplayMessages(new[] {ValidationMessage.Error("This load is already been covered.")});
			}
		}
		
		protected void OnNoClicked(object sender, EventArgs e)
		{
			pnlDimScreen.Visible = true;
			responseYes.Enabled = false;
			DisplayMessages(new[] { ValidationMessage.Information("You have rejected this load.") });
		}


		protected void OnOkayProcess(object sender, EventArgs e)
		{
			messageBox.Visible = false;
			pnlDimScreen.Visible = false;

		}
	}
}