﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PluginBase.Mileage;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;


namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class AssetLogView : MemberPageBase, IAssetLogView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public override ViewCode PageCode { get { return ViewCode.AssetLog; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public static string PageAddress { get { return "~/Members/Operations/AssetLogView.aspx"; } }

        public Dictionary<int, string> MileageEngines
        {
            set
            {
                ddlMilageEngine.DataSource = value
                    .Select(t => new ViewListItem(t.Value, t.Key.ToString()))
                    .OrderBy(t => t.Text)
                    .ToList();
                ddlMilageEngine.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<AssetLog>> Save;
        public event EventHandler<ViewEventArgs<AssetLog>> Delete;
        public event EventHandler<ViewEventArgs<AssetLog>> Lock;
        public event EventHandler<ViewEventArgs<AssetLog>> UnLock;
        public event EventHandler<ViewEventArgs<long>> Search;
        public event EventHandler<ViewEventArgs<string>> AssetSearch;

        public void DisplaySearchResult(List<AssetLogDto> assetLogs)
        {
            litRecordCount.Text = assetLogs.BuildRecordCount();
            lstAssetLogs.DataSource = assetLogs;
            lstAssetLogs.DataBind();
        }

        public void DisplayAsset(Asset asset)
        {
            txtAssetNumber.Text = asset == null ? string.Empty : asset.AssetNumber;
            txtAssetDescription.Text = asset == null ? string.Empty : asset.Description;
            hidAssetId.Value = asset == null ? default(long).ToString() : asset.Id.ToString();

            if (asset != null) DoSearch(); // auto load log on display if  asset is not null
            else DisplaySearchResult(new List<AssetLogDto>());
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() || messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<long>(hidAssetId.Value.ToLong()));
        }

        private void GeneratePopup()
        {
            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<AssetLog>(new AssetLog(hidAssetLogId.Value.ToLong(), false)));
        }

        private void LoadAssetLogForEdit(AssetLog assetLog)
        {
            hidAssetLogId.Value = assetLog.Id.ToString();

            txtDateCreated.Text = assetLog.DateCreated.FormattedShortDate();

            txtLogDate.Text = assetLog.LogDate.FormattedShortDate();
            txtStreet1.Text = assetLog.Street1;
            txtStreet2.Text = assetLog.Street2;
            txtCity.Text = assetLog.City;
            txtState.Text = assetLog.State;
            txtPostalCode.Text = assetLog.PostalCode;
            ddlCountries.SelectedValue = assetLog.CountryId.GetString();
            ddlMilageEngine.SelectedValue = assetLog.MileageEngine.ToInt().ToString();
            txtMilesRun.Text = assetLog.MilesRun.ToString();
            txtComment.Text = assetLog.Comment;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
            if (CloseEditPopUp) CloseEditPopup();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            new AssetLogHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var assetLogId = hidAssetLogId.Value.ToLong();

            var assetLog = new AssetLog(assetLogId, assetLogId != default(long))
            {
                LogDate = txtLogDate.Text.ToDateTime(),
                Street1 = txtStreet1.Text,
                Street2 = txtStreet2.Text,
                City = txtCity.Text,
                State = txtState.Text,
                PostalCode = txtPostalCode.Text,
                CountryId = ddlCountries.SelectedIndex.ToLong(),
                MileageEngine = ddlMilageEngine.SelectedValue.ToInt().ToEnum<MileageEngine>(),
                MilesRun = txtMilesRun.Text.ToDecimal(),
                Comment = txtComment.Text,
            };

            if (assetLog.IsNew)
            {
                assetLog.TenantId = ActiveUser.TenantId;
                assetLog.DateCreated = DateTime.Now;
                assetLog.AssetId = hidAssetId.Value.ToLong();
            }

            if (Save != null)
                Save(this, new ViewEventArgs<AssetLog>(assetLog));

            DoSearch();
        }

        protected void OnNewClick(object sender, EventArgs e)
        {
            if (hidAssetId.Value.ToLong() == default(long))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Please select an asset before adding a new asset log") });
                return;
            }

            GeneratePopup();
            LoadAssetLogForEdit(new AssetLog { DateCreated = DateTime.Now, LogDate = DateTime.Now });
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("assetLogId").ToCustomHiddenField();

            var assetLog = new AssetLog(hidden.Value.ToLong(), false);

            LoadAssetLogForEdit(assetLog);

            GeneratePopup();

            if (Lock != null)
                Lock(this, new ViewEventArgs<AssetLog>(assetLog));
        }

        protected void OnDeleteClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("assetLogId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<AssetLog>(new AssetLog(hidden.Value.ToLong(), true)));

            DoSearch();
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }


        protected void OnAssetNumberEntered(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAssetNumber.Text))
            {
                hidAssetId.Value = string.Empty;
                txtAssetDescription.Text = string.Empty;
                return;
            }

            DisplaySearchResult(new List<AssetLogDto>());

            if (AssetSearch != null)
                AssetSearch(this, new ViewEventArgs<string>(txtAssetNumber.Text));
        }

        protected void OnAssetSearchClicked(object sender, ImageClickEventArgs e)
        {
            assetFinder.Visible = true;
        }

        protected void OnAssetFinderSelectionCancelled(object sender, EventArgs e)
        {
            assetFinder.Visible = false;
        }

        protected void OnAssetFinderItemSelected(object sender, ViewEventArgs<Asset> e)
        {
            DisplayAsset(e.Argument);
            assetFinder.Visible = false;
        }



        protected void OnGetMilesClicked(object sender, ImageClickEventArgs e)
        {
            var engine = ddlMilageEngine.SelectedValue.ToInt().ToEnum<MileageEngine>();
            var plugin = engine.RetrieveEnginePlugin();
            if (plugin == null)
            {
                txtMilesRun.Text = string.Empty;
                DisplayMessages(new[] { ValidationMessage.Error("Unable to find implemented mileage engine for {0}.", engine) });
                return;
            }
            var search = new AssetLogSearch();
            var log = hidAssetLogId.Value.ToLong() == default(long)
                        ? search.FetchLastLogEntry(hidAssetId.Value.ToLong(), ActiveUser.TenantId)
                        : search.FetchPreviousLog(hidAssetLogId.Value.ToLong(), hidAssetId.Value.ToLong(), ActiveUser.TenantId);
            if (log == null)
            {
                txtMilesRun.Text = "0";
                return;
            }
            var p1 = new Point
            {
                Type = PointType.Address,
                PostalCode = log.PostalCode,
                Country = engine.IsApplicationDefault() ? log.CountryId.ToString() : log.CountryCode
            };
            var p2 = new Point
            {
                Type = PointType.Address,
                PostalCode = txtPostalCode.Text,
                Country = engine.IsApplicationDefault()
                            ? ddlCountries.SelectedValue
                            : new Country(ddlCountries.SelectedValue.ToLong()).Code
            };
            var miles = plugin.GetMiles(p1, p2);
            if (miles.ToInt() == -1)
            {
                txtMilesRun.Text = string.Empty;
                DisplayMessages(new[] { ValidationMessage.Error("Error retrieving miles [Message: {0}].", plugin.LastErrorMessage) });
                return;
            }
            txtMilesRun.Text = miles.ToString("n4");
        }
    }
}
