﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class ShipmentDashboardView : MemberPageBase, IShipmentDashboardView
    {
        protected const string ModifyingShipment = "MS";

        private SearchField SortByField
        {
            get { return OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        protected bool HasShipmentAccess { get; set; }

        public static string PageAddress { get { return "~/Members/Operations/ShipmentDashboardView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.ShipmentDashboard; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; } }


        // when calling LockShipment or LockLoadOrder events if acquiring lock fails, this is set to true
        public bool LockFailed { get; set; }


        public Dictionary<int, string> NoteTypes 
        { 
            set
            {
                ddlNoteType.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).OrderBy(t => t.Text).ToList();
                ddlNoteType.DataBind();
            }
        }


        public event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;
        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<LoadOrder>> LockLoadOrder;
        public event EventHandler<ViewEventArgs<LoadOrder>> UnLockLoadOrder;
        public event EventHandler<ViewEventArgs<Shipment>> LockShipment;
        public event EventHandler<ViewEventArgs<Shipment>> UnLockShipment;
        public event EventHandler<ViewEventArgs<LoadOrder>> SaveLoadOrder;
        public event EventHandler<ViewEventArgs<Shipment>> SaveShipment;

        public void DisplaySearchResult(List<ShipmentDashboardDto> shipments)
        {
            litRecordCount.Text = shipments.BuildRecordCount();
            upcseDataUpdate.DataSource = shipments;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (hidFlag.Value == ModifyingShipment)
            {
                foreach (var message in messages.Where(message => message.Type == ValidateMessageType.Warning)) message.Message = string.Format("Warning: {0}", message.Message);
                litErrorMessages.Text  = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Where(m => m.Type == ValidateMessageType.Error || m.Type == ValidateMessageType.Warning).Select(m => m.Message).ToArray());
                return;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void Set(Shipment shipment)
        {
            pnlModifyShipment.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLockShipment != null)
                UnLockShipment(this, new ViewEventArgs<Shipment>(shipment));

            DoSearchPreProcessingThenSearch(SortByField);
        }

        public void Set(LoadOrder loadOrder)
        {
            pnlModifyShipment.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLockLoadOrder != null)
                UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

            DoSearchPreProcessingThenSearch(SortByField);
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new ShipmentViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(ShipmentViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<ShipmentViewSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ShipmentDashboardHandler(this).Initialize();

            HasShipmentAccess = ActiveUser.HasAccessTo(ViewCode.Shipment);

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());

            var codes = WebApplicationUtilities.GetEdiStatusCodes();
            codes.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, string.Empty));
            ddlEdiStatusCode.DataSource = codes;
            ddlEdiStatusCode.DataBind();

            // set auto referesh
            tmRefresh.Interval = WebApplicationConstants.DefaultRefreshInterval.ToInt();

            // set sort fields
            ddlSortBy.DataSource = OperationsSearchFields.ShipmentSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = OperationsSearchFields.ShipmentNumber.Name;

            lbtnSortShipmentNumber.CommandName = OperationsSearchFields.ShipmentNumber.Name;
            lbtnSortDateCreated.CommandName = OperationsSearchFields.DateCreated.Name;
            lbtnSortDesiredPickup.CommandName = OperationsSearchFields.DesiredPickupDate.Name;
            lbtnSortActualPickup.CommandName = OperationsSearchFields.ActualPickupDate.Name;
            lbtnSortEstimatedDelivery.CommandName = OperationsSearchFields.EstimatedDeliveryDate.Name;
            lbtnSortActualDelivery.CommandName = OperationsSearchFields.ActualDeliveryDate.Name;
            lbtnSortServiceMode.CommandName = OperationsSearchFields.ServiceMode.Name;
            lbtnSortStatus.CommandName = OperationsSearchFields.Status.Name;


            //get criteria from Session
            var searchDefault = Session[WebApplicationConstants.ShipmentDashboardSearchCriteria] as SearchDefaultsItem;
            var storedCriteria = searchDefault == null
                                    ? null
                                    : new ShipmentViewSearchCriteria
                                    {
                                        ActiveUserId = ActiveUser.Id,
                                        Parameters = searchDefault.Columns,
                                        SortAscending = searchDefault.SortAscending,
                                        SortBy = searchDefault.SortBy
                                    };

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet();
            var defaultCriteria = new ShipmentViewSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = profile != null
                                ? profile.Columns
                                : OperationsSearchFields.Default.Select(f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            var criteria = storedCriteria ?? defaultCriteria;
            criteria.DisableNoRecordNotification = true;

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;

            if (storedCriteria != null)
                DoSearch(criteria);
            else
                searchProfiles.SetSelectedProfile(profile);

            Session[WebApplicationConstants.ShipmentDashboardSearchCriteria] = null;
        }


        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            Session[WebApplicationConstants.TransferShipmentId] = default(long);

            Response.Redirect(ShipmentView.PageAddress);
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnShipmentDetailsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var sh = (ShipmentDashboardDto)item.DataItem;

            if (sh == null) return;

            var detailControl = (ShipmentDashboardDetailControl)item.FindControl("shipmentDashboardDetailControl");
            detailControl.LoadShipment(sh, sh.CheckCalls);
        }


        protected void OnShippingLabelGeneratorCloseClicked(object sender, EventArgs e)
        {
            shippingLabelGenerator.Visible = false;
        }

        protected void OnGenerateShippingLabel(object sender, ViewEventArgs<DetailRecordInfo> e)
        {
            shippingLabelGenerator.GenerateLabels(e.Argument.RecordId);
            shippingLabelGenerator.Visible = true;
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.ShipmentDashboard.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.ShipmentDashboard);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }



        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }



        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }



        protected void OnTimerTick(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnShipmentDashboardDetailModifyShipment(object sender, ViewEventArgs<long, DashboardDtoType> e)
        {
            pnlModifyShipment.Visible = true;
            pnlDimScreen.Visible = true;
            trActualPickupDate.Visible = e.Argument2 == DashboardDtoType.Shipment;
            trActualDeliveryDate.Visible = e.Argument2 == DashboardDtoType.Shipment;
            pnlCreateCheckCall.Visible = e.Argument2 == DashboardDtoType.Shipment;
            litErrorMessages.Text = string.Empty;
            hidFlag.Value = ModifyingShipment;

            switch (e.Argument2)
            {
                case DashboardDtoType.LoadOrder:
                    var loadOrder = new LoadOrder(e.Argument);
                    litModifyShipmentTitle.Text = string.Format("Update Information For Load Order {0}", loadOrder.LoadOrderNumber);
                    hidEditLoadOrderId.Value = loadOrder.Id.ToString();
                    hidEditShipmentId.Value = string.Empty;

                    txtDesiredPickupDate.Text = loadOrder.DesiredPickupDate.FormattedShortDateSuppressEarliestDate();
                    txtEstimatedDeliveryDate.Text = loadOrder.EstimatedDeliveryDate.FormattedShortDateSuppressEarliestDate();
                    txtActualDeliveryDate.Text = string.Empty;
                    txtActualPickupDate.Text = string.Empty;
                    txtProNumber.Text = (loadOrder.Vendors.FirstOrDefault(v => v.Primary) ?? new LoadOrderVendor()).ProNumber;

                    
                    if (LockLoadOrder != null)
                        LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
                    break;
                case DashboardDtoType.Shipment:
                    var shipment = new Shipment(e.Argument);
                    litModifyShipmentTitle.Text = string.Format("Update Information For Shipment {0}", shipment.ShipmentNumber);
                    hidEditShipmentId.Value = shipment.Id.ToString();
                    hidEditLoadOrderId.Value = string.Empty;

                    txtActualPickupDate.Text = shipment.ActualPickupDate.FormattedShortDateSuppressEarliestDate();
                    txtDesiredPickupDate.Text = shipment.DesiredPickupDate.FormattedShortDateSuppressEarliestDate();
                    txtEstimatedDeliveryDate.Text = shipment.EstimatedDeliveryDate.FormattedShortDateSuppressEarliestDate();
                    txtActualDeliveryDate.Text = shipment.ActualDeliveryDate.FormattedShortDateSuppressEarliestDate();
                    txtProNumber.Text = (shipment.Vendors.FirstOrDefault(v => v.Primary) ?? new ShipmentVendor()).ProNumber;

                    txtCallNotes.Text = string.Empty;
                    ddlEdiStatusCode.SelectedValue = string.Empty;
                    txtEventDate.Text = DateTime.Now.FormattedShortDate();
                    txtEventTime.Text = DateTime.Now.FormattedTimeOfDay();

                   
                    if (LockShipment != null)
                        LockShipment(this, new ViewEventArgs<Shipment>(shipment));
                    break;
            }

            pnlModifyShipmentBody.Enabled = !LockFailed;
            if (LockFailed) DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
        }

        protected void OnCloseModifyShipmentClicked(object sender, EventArgs eventArgs)
        {
            hidFlag.Value = string.Empty;

            pnlModifyShipment.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLockShipment != null && hidEditShipmentId.Value.ToLong() != default(long))
                UnLockShipment(this, new ViewEventArgs<Shipment>(new Shipment(hidEditShipmentId.Value.ToLong())));

            if (UnLockLoadOrder != null && hidEditLoadOrderId.Value.ToLong() != default(long))
                UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(new LoadOrder(hidEditLoadOrderId.Value.ToLong())));
        }

        protected void OnDoneModifyShipmentClicked(object sender, EventArgs e)
        {
            if (hidEditShipmentId.Value.ToLong() != default(long))
            {
                var shipment = new Shipment(hidEditShipmentId.Value.ToLong(), true);
                shipment.LoadCollections();
                shipment.DesiredPickupDate = txtDesiredPickupDate.Text.ToDateTime();
                shipment.ActualPickupDate = txtActualPickupDate.Text.ToDateTime();
                shipment.EstimatedDeliveryDate = txtEstimatedDeliveryDate.Text.ToDateTime();
                shipment.ActualDeliveryDate = txtActualDeliveryDate.Text.ToDateTime();
                shipment.Status = shipment.InferShipmentStatus(false);

                var shipmentVendor = shipment.Vendors.FirstOrDefault(v => v.Primary);
                if (shipmentVendor != null)
                {
                    shipmentVendor.TakeSnapShot();
                    shipmentVendor.ProNumber = txtProNumber.Text;
                }

                if(!string.IsNullOrEmpty(txtCallNotes.Text))
                    shipment.CheckCalls.Add(new CheckCall
                        {
                            CallDate = DateTime.Now,
                            EventDate = txtEventDate.Text.ToDateTime().SetTime(txtEventTime.Text),
                            CallNotes = txtCallNotes.Text,
                            Shipment = shipment,
                            TenantId = shipment.TenantId,
                            EdiStatusCode = ddlEdiStatusCode.SelectedValue,
                            User = ActiveUser
                        });

                if(!string.IsNullOrEmpty(txtMessage.Text))
                    shipment.Notes.Add(new ShipmentNote
                        {
                            Archived = chkArchived.Checked,
                            Classified = chkClassified.Checked,
                            Message = txtMessage.Text,
                            Shipment = shipment,
                            TenantId = shipment.TenantId,
                            User = ActiveUser,
                            Type = ddlNoteType.SelectedValue.ToInt().ToEnum<NoteType>(),
                        });

                if (SaveShipment != null)
                    SaveShipment(this, new ViewEventArgs<Shipment>(shipment));
            }
            else if (hidEditLoadOrderId.Value.ToLong() != default(long))
            {
                var loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong(), true);
                loadOrder.LoadCollections();
                loadOrder.DesiredPickupDate = txtDesiredPickupDate.Text.ToDateTime();
                loadOrder.EstimatedDeliveryDate = txtEstimatedDeliveryDate.Text.ToDateTime();

                var loadOrderVendor = loadOrder.Vendors.FirstOrDefault(v => v.Primary);
                if (loadOrderVendor != null)
                {
                    loadOrderVendor.TakeSnapShot();
                    loadOrderVendor.ProNumber = txtProNumber.Text;
                }

                if (!string.IsNullOrEmpty(txtMessage.Text))
                    loadOrder.Notes.Add(new LoadOrderNote
                    {
                        Archived = chkArchived.Checked,
                        Classified = chkClassified.Checked,
                        Message = txtMessage.Text,
                        LoadOrder = loadOrder,
                        TenantId = loadOrder.TenantId,
                        User = ActiveUser,
                        Type = ddlNoteType.SelectedValue.ToInt().ToEnum<NoteType>(),
                    });

                if (SaveLoadOrder != null)
                    SaveLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
            }
        }
    }
}