﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class LibraryItemView : MemberPageBase, ILibraryItemView
    {
        private const string ExportFlag = "E";

        private const string SaveFlag = "S";

        public static string PageAddress { get { return "~/Members/Operations/LibraryItemView.aspx"; } }

        public override ViewCode PageCode
        { get { return ViewCode.LibraryItem; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public event EventHandler<ViewEventArgs<LibraryItem>> Save;
        public event EventHandler<ViewEventArgs<LibraryItem>> Delete;
        public event EventHandler<ViewEventArgs<LibraryItemSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<LibraryItem>> Lock;
        public event EventHandler<ViewEventArgs<LibraryItem>> UnLock;
        public event EventHandler<ViewEventArgs<List<LibraryItem>>> BatchImport;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;

        public void DisplaySearchResult(List<LibraryItem> libraryItems)
        {
            litRecordCount.Text = libraryItems.BuildRecordCount();
            lstLibraryItems.DataSource = libraryItems;
            lstLibraryItems.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var lines = libraryItems
                    .Select(a => new[]
                                     {
                                         a.Description.ToString(),
                                         a.PieceCount.ToString(),
                                         a.PackageType == null ? string.Empty : a.PackageType.TypeName.ToString(),
                                         a.Quantity.ToString(),
                                         a.Weight.ToString(),
                                         a.Length.ToString(),
                                         a.Width.ToString(),
                                         a.Height.ToString(),
										 a.IsStackable.ToString(),
                                         a.FreightClass.ToString(),
                                         a.NMFCCode.ToString(),
                                         a.HTSCode.ToString(),
                                         a.Comment.ToString(),
                                         a.HazardousMaterial.ToString()
                                     }.TabJoin())
                    .ToList();
                lines.Insert(0,
                             new[]
                                 {
                                     "Description", "Piece Count", "Package Type", "Quantity", "Weight",
                                     "Length", "Width", "Height", "Stackable", "Freight Class", "NMFC Code", "HTS Code", "Comment", "Hazmat"
                                 }.TabJoin());

                hidFlag.Value = string.Empty;

                Response.Export(lines.ToArray().NewLineJoin(), "LibraryItems.txt");
            }
        }

        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? string.Empty : customer.Id.ToString();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void LoadLibraryItem(LibraryItem libraryItem)
        {
            hidEditLibraryItemId.Value = libraryItem.Id.ToString();
            txtDescription.Text = libraryItem.Description;
            ddlPackageTypes.SelectedValue = libraryItem.PackageTypeId.GetString();
            txtWeight.Text = libraryItem.Weight.ToString();
            txtLength.Text = libraryItem.Length.ToString();
            txtHeight.Text = libraryItem.Height.ToString();
            txtWidth.Text = libraryItem.Width.ToString();
            chkIsStackable.Checked = libraryItem.IsStackable;
            chkIsHazmat.Checked = libraryItem.HazardousMaterial;
            txtNmfcCode.Text = libraryItem.NMFCCode;
            txtHtsCode.Text = libraryItem.HTSCode;
            txtComment.Text = libraryItem.Comment;
            txtPieceCount.Text = libraryItem.PieceCount.ToString();

            if (ddlFreightClass.ContainsValue(libraryItem.FreightClass.ToString()))
                ddlFreightClass.Text = libraryItem.FreightClass.ToString();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<LibraryItem>(new LibraryItem(hidEditLibraryItemId.Value.ToLong(), false)));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;
        }


        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new LibraryItemSearchCriteria
            {
                CustomerId = hidCustomerId.Value.ToLong(),
                ActiveUserId = ActiveUser.Id,
                Parameters = GetCurrentRunParameters(false)
            });
        }

        private void DoSearch(LibraryItemSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<LibraryItemSearchCriteria>(criteria));
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            hidFlag.Value = string.Empty;
            messageBox.Visible = false;

            if (!CloseEditPopUp) return;

            CloseEditPopup();

            var libraryItemId = hidEditLibraryItemId.Value.ToLong();
            var libraryItem = new LibraryItem(libraryItemId, libraryItemId != default(long));

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<LibraryItem>(libraryItem));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new LibraryItemHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            
            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.LibraryItemsImportTemplate });

            var freightClasses = WebApplicationSettings.FreightClasses
                .Select(fc => new ViewListItem(fc.ToString(), fc.ToString()))
                .ToList();
            freightClasses.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicableShort, "0"));
            ddlFreightClass.DataSource = freightClasses;
            ddlFreightClass.DataBind();

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            if (!ActiveUser.TenantEmployee && !ActiveUser.UserShipAs.Any()) DisplayCustomer(ActiveUser.DefaultCustomer);

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : OperationsSearchFields.DefaultLibraryItems.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnEditCloseClicked(object sender, EventArgs e)
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<LibraryItem>(new LibraryItem(hidEditLibraryItemId.Value.ToLong(), false)));
        }

        protected void OnSaveLibraryItemClicked(object sender, EventArgs e)
        {
            var libraryItemId = hidEditLibraryItemId.Value.ToLong();

            var libraryItem = new LibraryItem(libraryItemId, libraryItemId != default(long))
            {
                Customer = new Customer(hidCustomerId.Value.ToLong()),
                Description = txtDescription.Text,
                PieceCount = txtPieceCount.Text.ToInt(),
                PackageTypeId = ddlPackageTypes.SelectedValue.ToLong(),
                Quantity = 1,
                Weight = txtWeight.Text.ToDecimal(),
                Length = txtLength.Text.ToDecimal(),
                Height = txtHeight.Text.ToDecimal(),
                IsStackable = chkIsStackable.Checked,
                HazardousMaterial = chkIsHazmat.Checked,
                Width = txtWidth.Text.ToDecimal(),
                FreightClass = ddlFreightClass.SelectedValue.ToDouble(),
                NMFCCode = txtNmfcCode.Text,
                HTSCode = txtHtsCode.Text,
                Comment = txtComment.Text
            };

            if (libraryItem.IsNew)
            {
                libraryItem.TenantId = ActiveUser.TenantId;
            }

            if (Save != null)
                Save(this, new ViewEventArgs<LibraryItem>(libraryItem));
            hidFlag.Value = SaveFlag;

            DoSearchPreProcessingThenSearch();
        }

        protected void OnDeleteLibraryItemClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidLibraryItemId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<LibraryItem>(new LibraryItem(hidden.Value.ToLong(), true)));

            DoSearchPreProcessingThenSearch();
        }

        protected void OnNewLibraryItemClicked(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(hidCustomerId.Value))
            {
                LoadLibraryItem(new LibraryItem());
                hidEditLibraryItemId.Value = string.Empty;
                GeneratePopup("Add Library Item");
            }
            else
            {
                messageBox.Button = MessageButton.Ok;
                messageBox.Icon = MessageIcon.Error;
                messageBox.Message = "Please select a customer to create a new library item";
                messageBox.Visible = true;
            }
        }

        protected void OnEditLibraryItemClicked(object sender, ImageClickEventArgs e)
        {
            GeneratePopup("Modify Library Item");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidLibraryItemId").ToCustomHiddenField();

            var libraryItem = new LibraryItem(hidden.Value.ToLong(), false);

            LoadLibraryItem(libraryItem);

            if (Lock != null)
                Lock(this, new ViewEventArgs<LibraryItem>(libraryItem));
        }

        protected void OnSetPackageDimensionsClicked(object sender, ImageClickEventArgs e)
        {
            var packageTypeId = ddlPackageTypes.SelectedValue.ToLong();

            if (packageTypeId == default(long)) return;

            var packageType = new PackageType(packageTypeId);

            txtLength.Text = packageType.DefaultLength.ToString();
            txtWidth.Text = packageType.DefaultWidth.ToString();
            txtHeight.Text = packageType.DefaultHeight.ToString();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch();
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearchPreProcessingThenSearch();
        }


        protected void OpenLibraryItemImport(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(hidCustomerId.Value))
            {
                fileUploader.Title = "LIBRARY ITEM IMPORT";
                fileUploader.Instructions = @"**Please refer to user guide for import file format";
                fileUploader.Visible = true;
            }
            else
            {
                messageBox.Button = MessageButton.Ok;
                messageBox.Icon = MessageIcon.Error;
                messageBox.Message = "Please select a customer to batch import for";
                messageBox.Visible = true;
            }
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 13;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs = chks
                .Where(i => i.Line[0] == string.Empty)
                .Select(i => ValidationMessage.Error("Description is missing on line : {0}", i.Index))
                .ToList();
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var packageTypeSearch = new PackageTypeSearch();

            var packageTypes = ProcessorVars.RegistryCache[ActiveUser.TenantId].PackageTypes.Select(c => c.TypeName).ToList();
            msgs.AddRange(chks.CodesAreValid(1, packageTypes, new PackageType().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var freightClasses = WebApplicationSettings.FreightClasses.Select(f => f.ToString()).ToList();
            var invalidFreightClass = chks
                .Where(i => !freightClasses.Contains(i.Line[8]))
                .Select(i => ValidationMessage.Error("Invalid Freight Class on line : {0}", i.Index))
                .ToList();
            if (invalidFreightClass.Any())
            {
                invalidFreightClass.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidFreightClass);
                return;
            }

            var customer = hidCustomerId.Value.ToLong() == default(long)
                               ? null
                               : new Customer(hidCustomerId.Value.ToLong());
            var libraryItems = lines

                .Select(s => new LibraryItem
                {
                    Description = s[0].Trim(),
                    PackageTypeId = packageTypeSearch.FetchPackageTypeIdByTypeName(s[1], ActiveUser.TenantId),
                    Quantity = 1,
                    Weight = s[2].ToDecimal(),
                    Length = s[3].ToDecimal(),
                    Width = s[4].ToDecimal(),
                    Height = s[5].ToDecimal(),
                    PieceCount = s[6].ToInt(),
                    IsStackable = s[7].ToBoolean(),
                    FreightClass = s[8].ToDouble(),
                    NMFCCode = s[9],
                    HTSCode = s[10],
                    Comment = s[11],
                    HazardousMaterial = s[12].ToBoolean(),
                    Customer = customer,
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<LibraryItem>>(libraryItems));

            fileUploader.Visible = false;
            DoSearchPreProcessingThenSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }


        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCustomerNumber.Text))
            {
                hidCustomerId.Value = string.Empty;
                txtCustomerName.Text = string.Empty;
                return;
            }

            DisplaySearchResult(new List<LibraryItem>());

            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            DisplaySearchResult(new List<LibraryItem>());
            customerFinder.Visible = false;
        }
        
        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.LibraryItems.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }

        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.LibraryItems);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
    }
}