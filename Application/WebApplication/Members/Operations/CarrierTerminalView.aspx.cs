﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class CarrierTerminalView : MemberPageBase, ICarrierTerminalView
    {
        private const string ExportFlag = "E";

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public override ViewCode PageCode { get { return ViewCode.CarrierTerminal; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public static string PageAddress { get { return "~/Members/Operations/CarrierTerminalView.aspx"; } }

        public event EventHandler<ViewEventArgs<VendorTerminal>> Save;
        public event EventHandler<ViewEventArgs<VendorTerminal>> Delete;
        public event EventHandler<ViewEventArgs<VendorTerminal>> Lock;
        public event EventHandler<ViewEventArgs<VendorTerminal>> UnLock;
        public event EventHandler<ViewEventArgs<VendorTerminalSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<string>> VendorSearch;
        public event EventHandler<ViewEventArgs<List<VendorTerminal>>> BatchImport;


        public void DisplaySearchResult(List<VendorTerminal> vendorTerminals)
        {
            litRecordCount.Text = vendorTerminals.BuildRecordCount();
            upcseDataUpdate.DataSource = vendorTerminals;
            upcseDataUpdate.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var lines = vendorTerminals
                    .Select(a => new[]
					             	{
                                        a.Vendor.VendorNumber,
                                        a.Vendor.Name,
					             		a.Name,
					             		a.DateCreated.ToShortDateString(),
					             		a.ContactName,
					             		a.Comment,
					             		a.Phone,
                                        a.Mobile,
					             		a.Fax,
					             		a.Email,
					             		a.Street1,
                                        a.Street2,
                                        a.City,
                                        a.State,
                                        a.PostalCode,
                                        a.Country.Code
					             	}.TabJoin())
                    .ToList();
                lines.Insert(0,
                             new[]
				             	{
				             		"Vendor Number", "Vendor Name", "Terminal Name", "Date Created", "Contact Name",
				             		"Comment", "Phone", "Mobile","Fax","Email", "Street 1", "Street2", "City", "State",
                                    "Postal Code", "Country Code"
				             	}.TabJoin());

                hidFlag.Value = string.Empty;

                Response.Export(lines.ToArray().NewLineJoin(), "Carrier Terminals.txt");
            }
        }

        public void DisplayVendor(Vendor vendor)
        {
            txtVendorNumber.Text = vendor == null ? string.Empty : vendor.VendorNumber;
            txtVendorName.Text = vendor == null ? string.Empty : vendor.Name;
            hidEditVendorId.Value = vendor == null ? default(long).ToString() : vendor.Id.ToString();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() || messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new VendorTerminalSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
            });
        }

        private void DoSearch(VendorTerminalSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<VendorTerminalSearchCriteria>(criteria));
        }



        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEditCarrierTerminal.Visible = true;
            pnlDimScreen.Visible = pnlEditCarrierTerminal.Visible;
        }

        private void CloseEditPopup()
        {
            pnlEditCarrierTerminal.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<VendorTerminal>(new VendorTerminal(hidEditVendorTerminalId.Value.ToLong(), false)));
        }

        private void LoadCarrierTerminalForEdit(VendorTerminal vendorTerminal)
        {
            hidEditVendorTerminalId.Value = vendorTerminal.Id.ToString();
            DisplayVendor(vendorTerminal.Vendor);
            txtTerminalName.Text = vendorTerminal.Name;
            txtTerminalStreet1.Text = vendorTerminal.Street1;
            txtTerminalStreet2.Text = vendorTerminal.Street2;
            txtTerminalCity.Text = vendorTerminal.City;
            txtTerminalState.Text = vendorTerminal.State;
            txtTerminalPostalCode.Text = vendorTerminal.PostalCode;
            ddlCountries.SelectedValue = vendorTerminal.CountryId.GetString();
            txtTerminalContactName.Text = vendorTerminal.ContactName;
            txtTerminalComment.Text = vendorTerminal.Comment;
            txtTerminalPhone.Text = vendorTerminal.Phone;
            txtTerminalFax.Text = vendorTerminal.Fax;
            txtTerminalMobile.Text = vendorTerminal.Mobile;
            txtTerminalEmail.Text = vendorTerminal.Email;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            new CarrierTerminalHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            aceVendor.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });
            
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : OperationsSearchFields.DefaultVendorTerminals.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.CarrierTerminalImportTemplate });

        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var vendorTerminalId = hidEditVendorTerminalId.Value.ToLong();

            var vendorTerminal = new VendorTerminal(vendorTerminalId, vendorTerminalId != default(long))
            {
                Vendor = new Vendor(hidEditVendorId.Value.ToLong()),
                Name = txtTerminalName.Text,
                Street1 = txtTerminalStreet1.Text,
                Street2 = txtTerminalStreet2.Text,
                City = txtTerminalCity.Text,
                State = txtTerminalState.Text,
                PostalCode = txtTerminalPostalCode.Text,
                CountryId = ddlCountries.SelectedValue.ToLong(),
                ContactName = txtTerminalContactName.Text,
                Comment = txtTerminalComment.Text,
                Phone = txtTerminalPhone.Text,
                Fax = txtTerminalFax.Text,
                Email = txtTerminalEmail.Text.StripSpacesFromEmails(),
                Mobile = txtTerminalMobile.Text,
            };

            if (vendorTerminal.IsNew)
            {
                vendorTerminal.TenantId = ActiveUser.TenantId;
                vendorTerminal.DateCreated = DateTime.Now;
            }

            var isNew = vendorTerminal.IsNew;

            if (Save != null)
                Save(this, new ViewEventArgs<VendorTerminal>(vendorTerminal));

            if (!isNew) DoSearchPreProcessingThenSearch();
        }

        protected void OnNewClick(object sender, EventArgs e)
        {
            GeneratePopup("Add Carrier Terminal");
            LoadCarrierTerminalForEdit(new VendorTerminal { CountryId = ActiveUser.Tenant.DefaultCountryId });
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            GeneratePopup("Modify Carrier Terminal");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidVendorTerminalId").ToCustomHiddenField();

            var vendorTerminal = new VendorTerminal(hidden.Value.ToLong(), false);

            LoadCarrierTerminalForEdit(vendorTerminal);

            if (Lock != null)
                Lock(this, new ViewEventArgs<VendorTerminal>(vendorTerminal));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidVendorTerminalId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<VendorTerminal>(new VendorTerminal(hidden.Value.ToLong(), true)));

            DoSearchPreProcessingThenSearch();
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch();
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearchPreProcessingThenSearch();
        }
        
        protected void OnVendorNumberEntered(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtVendorNumber.Text))
            {
                hidEditVendorId.Value = string.Empty;
                txtVendorName.Text = string.Empty;
                return;
            }

            if (VendorSearch != null)
                VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
        {
            vendorFinder.Visible = true;
        }

        protected void OnVendorFinderSelectionCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
        {
            DisplayVendor(e.Argument);
            vendorFinder.Visible = false;
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "CARRIER TERMINALS IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 14;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countries = ProcessorVars.RegistryCache.Countries.Values;
            var countryCodes = countries.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(7, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var postalCodeSearch = new PostalCodeSearch();
            var postalCodes = new List<PostalCodeViewSearchDto>();
            var invalidPostalCodes = chks
                .Where(i => !string.IsNullOrEmpty(i.Line[6]))
                .Where(i =>
                {
                    var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[6], countries.First(c => c.Code == i.Line[7]).Id);
                    postalCodes.Add(code);
                    return code.Id == default(long);
                })
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidPostalCodes);
                return;
            }


            var vendorSearch = new VendorSearch();
            var vParameters = chks
                .Select(i => i.Line[0])
                .Distinct()
                .Select(n =>
                {
                    var p = AccountingSearchFields.VendorNumber.ToParameterColumn();
                    p.DefaultValue = n;
                    p.Operator = Operator.Equal;
                    return p;
                })
                .ToList();
            var vendorList = vendorSearch.FetchVendors(vParameters, ActiveUser.TenantId);
            var vendorNumbers = vendorList.Select(c => c.VendorNumber).ToList();
            msgs.AddRange(chks.CodesAreValid(0, vendorNumbers, new Vendor().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var vendorTerminals = lines
                .Select(s =>
                {
                    var postalCode = !string.IsNullOrEmpty(s[6]) ? postalCodes.First(pc => pc.Code.ToLower() == s[6].ToLower()).Code : string.Empty;
                    return new VendorTerminal
                    {
                        Vendor = vendorList.First(v => v.VendorNumber == s[0]),
                        Name = s[1],
                        DateCreated = DateTime.Now,
                        Street1 = s[2],
                        Street2 = s[3],
                        City = s[4],
                        State = s[5],
                        PostalCode = postalCode,
                        Country = countries.First(c => c.Code == s[7]),
                        ContactName = s[8],
                        Comment = s[9],
                        Phone = s[10],
                        Mobile = s[11],
                        Fax = s[12],
                        Email = s[13],
                        TenantId = ActiveUser.TenantId
                    };
                })
                .ToList();
            
            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<VendorTerminal>>(vendorTerminals));
            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.VendorTerminals.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.VendorTerminals);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }

    }
}
