﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class ShoppedRateView : MemberPageBase, IShoppedRateView
    {
        private SearchField SortByField
        {
            get { return OperationsSearchFields.ShoppedRateSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        private const string ExportFlag = "E";

        public static string PageAddress
        {
            get { return "~/Members/Operations/ShoppedRateView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.ShoppedRate; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public event EventHandler<ViewEventArgs<ShoppedRateSearchCriteria>> Search;

        public void DisplaySearchResult(List<ShoppedRateViewSearchDto> shoppedRates)
        {
            litRecordCount.Text = shoppedRates.BuildRecordCount();
            lvwShoppedRates.DataSource = shoppedRates;
            lvwShoppedRates.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var results = new List<string>();

                foreach (var rate in shoppedRates)
                {
                    var lines = new[]
                	            	{
                	            		rate.ReferenceNumber,
                	            		rate.Type.ToString(),
                	            		rate.DateCreated.FormattedLongDate(),
                	            		rate.UserName,
                	            		rate.CustomerNumber,
                	            		rate.CustomerName,
                	            		rate.VendorNumber,
                	            		rate.VendorName,
                	            		rate.OriginCity,
                	            		rate.OriginState,
                	            		rate.OriginPostalCode,
                	            		rate.OriginCountryName,
                	            		rate.DestinationCity,
                	            		rate.DestinationState,
                	            		rate.DestinationPostalCode,
                	            		rate.DestinationCountryName,
                	            		rate.NumberLineItems.ToString(),
                	            		rate.Quantity.ToString(),
                	            		rate.Weight.ToString(),
                	            		rate.HighestFreightClass.ToString(),
                	            		rate.MostSignificantFreightClass.ToString(),
                	            		rate.LowestTotalCharge.ToString(),
                	            		!rate.Services.Any()
                	            			? string.Empty
                	            			: rate.Services.Select(s => s.Description).ToArray().CommaJoin()
                	            	}.TabJoin();

                    results.Insert(0, lines);
                }
                results.Insert(0,
                               new[]
            	               	{
            	               		"Reference#", "Type", "Date Created", "User", "Customer Number", "Customer Name",
            	               		"Vendor Number", "Vendor Name",
            	               		"Origin City", "Origin State", "Origin Postal Code", "Origin Country",
            	               		"Destination City", "Destination State", "Destination Postal Code", "Destination Country",
            	               		"# Line Items", "Quantity", "Weight", "Highest Freight Class",
            	               		"Most Significant Weight Class",
            	               		"Lowest Total Charge", "Services"
            	               	}.TabJoin());

                hidFlag.Value = string.Empty;

                Response.Export(results.ToArray().NewLineJoin(), "ShoppedRates.txt");
            }
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }



        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new ShoppedRateSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(ShoppedRateSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<ShoppedRateSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ShoppedRateHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set sort fields
            ddlSortBy.DataSource = OperationsSearchFields.ShoppedRateSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = OperationsSearchFields.DateCreated.Name;

            // set sort fields
            lbtnSortReferenceNumber.CommandName = OperationsSearchFields.ReferenceNumber.Name;
            lbtnSortType.CommandName = OperationsSearchFields.TypeText.Name;
            lbtnSortDateCreated.CommandName = OperationsSearchFields.DateCreated.Name;
            lbtnSortUser.CommandName = OperationsSearchFields.Username.Name;
            lbtnSortNumberLineItems.CommandName = OperationsSearchFields.NumberLineItems.Name;
            lbnSortQuantity.CommandName = OperationsSearchFields.Quantity.Name;
            lbtnSortWeight.CommandName = OperationsSearchFields.Weight.Name;
            lbtnSortHighestFreightClass.CommandName = OperationsSearchFields.HighestFreightClass.Name;
            lbtnMostSignificantClass.CommandName = OperationsSearchFields.MostSignificantFreightClass.Name;
            lbtnLowestTotalCharge.CommandName = OperationsSearchFields.LowestTotalCharge.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : OperationsSearchFields.DefaultShoppedRates.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = profile == null || profile.SortAscending;
            rbDesc.Checked = !(profile == null || profile.SortAscending);

            if (profile != null && profile.SortBy != null && ddlSortBy.ContainsValue(profile.SortBy.Name))
                ddlSortBy.SelectedValue = profile.SortBy.Name;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnShoppedRateItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var data = item.DataItem as ShoppedRateViewSearchDto;
            var control = item.FindControl("lstShoppedRateServices").ToListView();

            if (data == null || control == null) return;

            var hypTransferLink = item.FindControl("hypTransferLink").ToHyperLink();
            var shoppedRateType = item.FindControl("litType").ToLiteral().Text.ToEnum<ShoppedRateType>();

            switch (shoppedRateType)
            {
                case ShoppedRateType.Quote:
                    hypTransferLink.NavigateUrl = string.Format("{0}?{1}={2}", LoadOrderView.PageAddress, WebApplicationConstants.TransferNumber, data.ReferenceNumber);
                    break;
                case ShoppedRateType.Shipment:
                case ShoppedRateType.WSBooking:
                    hypTransferLink.NavigateUrl = string.Format("{0}?{1}={2}", ShipmentView.PageAddress, WebApplicationConstants.TransferNumber, data.ReferenceNumber);
                    break;
            }

            control.DataSource = data.Services;
            control.DataBind();

            if (!string.IsNullOrEmpty(data.VendorNumber))
            {
                var hypVendorNumber = e.Item.FindControl("hypVendorNumber").ToHyperLink();
                var vendor = new VendorSearch().FetchVendorByNumber(data.VendorNumber, ActiveUser.TenantId);
                hypVendorNumber.NavigateUrl = string.Format("{0}?{1}={2}", VendorView.PageAddress, WebApplicationConstants.TransferNumber, vendor.Id.GetString().UrlTextEncrypt());
            }
        }


        protected bool ShowLiteralReferenceNumber(object dataItem)
        {
            var rate = dataItem as ShoppedRateViewSearchDto;
            if (rate == null) return true;

            switch (rate.Type)
            {
                case ShoppedRateType.Shipment:
                case ShoppedRateType.WSBooking:
                    return ActiveUser.HasAccessTo(ViewCode.Shipment) &&
                           (string.IsNullOrEmpty(rate.ReferenceNumber) || !rate.ShipmentExists);
                case ShoppedRateType.Quote:
                    return ActiveUser.HasAccessTo(ViewCode.LoadOrder) &&
                           (string.IsNullOrEmpty(rate.ReferenceNumber) || !rate.QuoteExists);
                default:
                    return true;
            }
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearchPreProcessingThenSearch(SortByField);
        }




        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.ShoppedRates.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.ShoppedRates);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = OperationsSearchFields.ShoppedRateSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }

            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(OperationsSearchFields.ShoppedRateSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue));
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(OperationsSearchFields.ShoppedRateSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue));
        }
    }
}
