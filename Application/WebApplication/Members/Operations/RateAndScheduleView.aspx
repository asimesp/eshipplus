﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="RateAndScheduleView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.RateAndScheduleView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowMore="false" OnCommand="OnToolbarCommandClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">

        <script type="text/javascript">
            $(document).ready(function () {

                if (jsHelper.IsTrue('<%= !IsPostBack %>') && jsHelper.IsTrue('<%= ActiveUser.AlwaysShowRatingNotice %>')) {
                    $("div[id$='pnlRateNotice']").show("slow");
                    $("div[id$='pnlDimScreenJS']").show();
                } else {
                    $("div[id$='pnlRateNotice']").hide();
                    $("div[id$='pnlDimScreenJS']").hide();
                }

                $("#importantInformation").click(function (e) {
                    e.preventDefault();
                    $("div[id$='pnlRateNotice']").show("slow");
                    $("div[id$='pnlDimScreenJS']").show();
                });

                if (jsHelper.IsTrue('<%= ScrollToBottom %>')) {
                    $(window).scrollTop($(document).height() - $(window).height());
                }
            });


        </script>

        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Rate and Schedule</h3>

            <div class="shipmentAlertSection">
                <asp:Panel ID="pnlCreditAlert" runat="server" CssClass="flag-shipment" Visible="False">
                    Insufficient Account Credit
                </asp:Panel>
            </div>

            <div class="right ii">
                <span id="importantInformation">Important Information</span>
            </div>
        </div>

        <eShip:RateNoticeControl runat="server" ID="rateNotice" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            OnlyActiveResults="True" Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:RateSelectionDisplayControl ID="rateSelectionDisplay" runat="server" Visible="False"
            EnableEnterMissingInformationButton="False" EnableRateDownload="True" EnableHightlightMissingInformationButtons="True"
            OnRateSelectionCancelled="OnRateSelectionDisplaySelectionCancelled" OnRateSelected="OnRateSelectionDisplayRateSelected"
            OnRatesLoaded="OnRateSelectionDisplayRatesLoaded" OnDownloadRates="OnRateSelectionDisplayDownloadRates" />
        <eShip:LibraryItemFinderControl runat="server" ID="libraryItemFinder" Visible="False" EnableMultiSelection="True"
            OnMultiItemSelected="OnLibraryItemFinderMultiItemSelected" OnSelectionCancel="OnLibraryItemFinderItemCancelled"
            OpenForEditEnabled="false" />
        <eShip:AddressBookFinderControl runat="server" ID="addressBookFinder" Visible="False" OnItemSelected="OnAddressBookFinderItemSelected"
            OnSelectionCancel="OnAddressBookFinderItemCancelled" OpenForEditEnabled="false" />
        <eShip:PostalCodeFinderControl runat="server" ID="postalCodeFinder" Visible="False" OnItemSelected="OnPostalCodeFinderItemSelected"
            OnSelectionCancel="OnPostalCodeFinderItemCancelled" EnableMultiSelection="False" />
        <eShip:DocumentDisplayControl runat="server" ID="documentDisplay" Visible="false" OnClose="OnDocumentDisplayClosed" />
        <eShip:PaymentEntryControl runat="server" ID="pecPayment" Visible="False" CanModifyPaymentAmount="False" OnClose="OnPaymentEntryCloseClicked" OnProcessingMessages="OnPaymentEntryProcessingMessages" 
            OnCustomAuthorizeAndCapturePayment="OnPaymentEntryCustomAuthorizeAndCapturePayment"/>

        <div class="imp_note">
            *** highlighted fields are minimum required for a rate. <span class="red">*</span> = required all modes, <span class="red">+</span> = required LTL & package,
            <span class="red">++</span> = required LTL
        </div>
    <div class="row">
        <div class="mt10 mb10 right">
            <eShip:RateAndScheduleProfilesControl ID="searchProfiles" runat="server" OnProcessingError="OnRateAndScheduleProfilesProcessingError" OnSearchProfileSelected="OnRateAndScheduleProfilesProfileSelected" />
        </div>
    </div>
    <hr class="dark" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <div class="rowgroup">
            <div class="row">
                <div class="fieldgroup mr20">
                    <label class="wlabel blue">Customer</label>
                    <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" OnTextChanged="OnCustomerNumberEntered"
                        AutoPostBack="True" CssClass="requiredInput w110" />
                    <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                        CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                    <eShip:CustomTextBox runat="server" ID="txtCustomerName" ReadOnly="True" CssClass="w280 disabled" />
                    <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                        EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                </div>
                <asp:Panel runat="server" ID="pnlDeclineInsurance" Visible="False" CssClass="fieldgroup_s pr20">
                    <asp:CheckBox runat="server" ID="chkDeclineInsurance" CssClass="jQueryUniform" />
                    <label class="comlabel blue">
                        Decline<br />
                        Insurance</label>
                </asp:Panel>
                <div class="fieldgroup vlinedarkright vlinedarkleft">
                    <label class="wlabel blue">Desired Shipment Date</label>
                    <asp:DropDownList runat="server" ID="ddlShipmentDate" DataValueField="Value" DataTextField="Text" CssClass="w200 requiredInput" />
                </div>
                <div class="fieldgroup ml20">
                    <label class="wlabel blue">Pickup Window Between</label>
                    <script type="text/javascript">
                        function OffsetPickupLateTimes() {
                            var ids = new ControlIds();
                            ids.TimesOfDayWindowStart = $(jsHelper.AddHashTag('<%= ddlReadyEarly.ClientID %>')).attr('id');
                            ids.TimesOfDayWindowEnd = $(jsHelper.AddHashTag('<%= ddlReadyLate.ClientID %>')).attr('id');

                            OffsetDropDownListByTimeOfDay(true, ids);
                        }
                    </script>
                    <asp:DropDownList runat="server" ID="ddlReadyEarly" onchange="OffsetPickupLateTimes();" CssClass="w100 requiredInput" />
                    <asp:Image runat="server" ImageUrl="~/images/icons2/amper.png" ImageAlign="AbsMiddle" CssClass="ml10 mr10" />
                    <asp:DropDownList runat="server" ID="ddlReadyLate" CssClass="w100 requiredInput" />
                </div>
            </div>
        </div>

        <hr />

        <div class="rowgroup">
            <div class="row">
                <div class="col_1_3">
                    <div class="row bottom_shadow pb5">
                        <eShip:CustomHiddenField ID="hidOriginKey" runat="server" />
                        <eShip:OperationsAddressInputControlRandS ID="operationsAddressInputOrigin" runat="server" Type="Origin"
                            OnFindPostalCode="OnAddressInputFindPostalCode" OnAddAddressFromAddressBook="OnAddressInputAddAddressFromAddressBook" />
                    </div>
                    <div class="row mt20">
                        <script type="text/javascript">
                            $(function () {
                                $(jsHelper.AddHashTag('<%= chkSaveOrigin.ClientID %>')).click(function () {
                                    jsHelper.UnCheckBox('<%= chkSaveOriginWithServices.ClientID %>');
                                    $.uniform.update();
                                });

                                $(jsHelper.AddHashTag('<%= chkSaveOriginWithServices.ClientID %>')).click(function () {
                                    jsHelper.UnCheckBox('<%= chkSaveOrigin.ClientID %>');
                                    $.uniform.update();
                                });
                            });
                        </script>

                        <ul class="twocol_list">
                            <li>
                                <asp:CheckBox runat="server" ID="chkSaveOrigin" CssClass="jQueryUniform" />
                                <label class="comlabel">
                                    Add Origin<br />
                                    to Address Book
                                </label>
                            </li>
                            <li>
                                <asp:CheckBox runat="server" ID="chkSaveOriginWithServices" CssClass="jQueryUniform" />
                                <label class="comlabel">
                                    Add Origin &amp; Services<br />
                                    to Address Book
                                </label>
                            </li>
                            <li id="liNotifyPrimaryOrigin" runat="server" Visible="false">
                                <asp:CheckBox runat="server" ID="chkNotifyPrimaryOriginContactOfShipmentUpdates" CssClass="jQueryUniform" />
                                <label class="comlabel">Notify primary origin<br /> contact of shipment<br/> updates</label>
                            </li>
                        </ul>

                    </div>
                </div>
                <div class="col_1_3">
                    <div class="row bottom_shadow pb5">
                        <eShip:CustomHiddenField ID="hidDestinationKey" runat="server" />
                        <eShip:OperationsAddressInputControlRandS ID="operationsAddressInputDestination" runat="server"
                            OnFindPostalCode="OnAddressInputFindPostalCode" OnAddAddressFromAddressBook="OnAddressInputAddAddressFromAddressBook" Type="Destination" />
                    </div>
                    <div class="row mt20">
                        <script type="text/javascript">
                            $(function () {
                                $(jsHelper.AddHashTag('<%= chkSaveDestination.ClientID %>')).click(function () {
                                    jsHelper.UnCheckBox('<%= chkSaveDestinationWithServices.ClientID %>');
                                    $.uniform.update();
                                });

                                $(jsHelper.AddHashTag('<%= chkSaveDestinationWithServices.ClientID %>')).click(function () {
                                    jsHelper.UnCheckBox('<%= chkSaveDestination.ClientID %>');
                                    $.uniform.update();
                                });
                            });
                        </script>

                        <ul class="twocol_list">
                            <li>
                                <asp:CheckBox runat="server" ID="chkSaveDestination" CssClass="jQueryUniform" />
                                <label class="comlabel">
                                    Add Destination<br />
                                    to Address Book
                                </label>
                            </li>
                            <li>
                                <asp:CheckBox runat="server" ID="chkSaveDestinationWithServices" CssClass="jQueryUniform" />
                                <label class="comlabel">
                                    Add Destination &amp;
                                    <br />
                                    Services to Address Book
                                </label>
                            </li>
                            <li id="liNotifyPrimaryDestination" runat="server"  Visible="false">
                                <asp:CheckBox runat="server" ID="chkNotifyPrimaryDestinationContactOfShipmentUpdates" CssClass="jQueryUniform" />
                                <label class="comlabel">Notify primary destination<br /> contact of shipment<br/> updates</label>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col_1_3 pr0">
                    <asp:Panel runat="server" ID="pnlDeliveryWindow" Visible="False" CssClass="row bottom_shadow pb5">
                        <div class="fieldgroup">
                            <label class="wlabel blue">Delivery Window Between</label>
                            <script type="text/javascript">
                                function OffsetDeliveryLateTimes() {
                                    var ids = new ControlIds();
                                    ids.TimesOfDayWindowStart = $(jsHelper.AddHashTag('<%= ddlDeliverEarly.ClientID %>')).attr('id');
                                    ids.TimesOfDayWindowEnd = $(jsHelper.AddHashTag('<%= ddlDeliverLate.ClientID %>')).attr('id');

                                    OffsetDropDownListByTimeOfDay(false, ids);
                                }
                            </script>
                            <asp:DropDownList runat="server" ID="ddlDeliverEarly" onchange="OffsetDeliveryLateTimes();" CssClass="w100" />
                            <asp:Image runat="server" ImageUrl="~/images/icons2/amper.png" ImageAlign="AbsMiddle" CssClass="ml10 mr10" />
                            <asp:DropDownList runat="server" ID="ddlDeliverLate" CssClass="w100" />
                        </div>
                    </asp:Panel>
                    <h5>Special Services</h5>
                    <div class="vlist mt0">
                        <asp:Repeater runat="server" ID="rptServices">
                            <HeaderTemplate>
                                <ul class="sm_chk chk_list">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <asp:CheckBox runat="server" ID="chkService" Text='<%# Eval("Text") %>' CssClass="jQueryUniform" />
                                    <eShip:CustomHiddenField runat="server" ID="hidServiceId" Value='<%# Eval("Value") %>' />
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>

        <hr />

        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h5>Items</h5>
                <div class="row">
                    <div class="col_1_3 fs95em">
                        <div class="fieldgroup mr5">
                            <asp:LinkButton runat="server" ID="lnkAddItem" OnClick="OnAddItemClicked" CausesValidation="false" CssClass="add" OnClientClick="ShowProgressNotice(true);">
                       Add Items
                            </asp:LinkButton>
                        </div>
                        <div class="fieldgroup mr5">
                            <asp:LinkButton runat="server" ID="lnkAddItemFromLibrary" Text="Add Item From Library"
                                OnClick="OnAddLibraryItemClicked" CausesValidation="false" CssClass="add" />
                        </div>
                    </div>
                    <asp:Panel ID="pnlBypassLinearFoot" runat="server" CssClass="col_1_3">
                        <eShip:AltUniformCheckBox runat="server" ID="chkBypassLinearFootRule" Visible="False" />
                        <label>Bypass Linear Foot Rule</label>
                    </asp:Panel>
                    <div class="col_1_3 <%= !ActiveUser.TenantEmployee ? "no-right-border" : string.Empty %>">
                        <script type="text/javascript">
                            $(window).load(function () {
                                // reload cookie selection
                                var pref = jsHelper.RetrieveCookie(jsHelper.RateAndScheduleInputPref);
                                if (!jsHelper.IsNullOrEmpty(pref)) {
                                    if (pref == '<%= Units.UnitType.Metric.ToInt().ToString() %>') {
                                        jsHelper.UnCheckBox($(jsHelper.AddHashTag('<%= chkEnglish.ClientID %>')).attr('id'));
                                        jsHelper.CheckBox($(jsHelper.AddHashTag('<%= chkMetric.ClientID %>')).attr('id'));
                                    }
                                    if (pref == '<%= Units.UnitType.English.ToInt().ToString() %>') {
                                        jsHelper.UnCheckBox($(jsHelper.AddHashTag('<%= chkMetric.ClientID %>')).attr('id'));
                                        jsHelper.CheckBox($(jsHelper.AddHashTag('<%= chkEnglish.ClientID %>')).attr('id'));
                                    }
                                }
                                ProcessUnitsValueChanged(GetCurrentlySelectedUnits());
                                ReloadCalculatedDensity();


                                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                    var pref = jsHelper.RetrieveCookie(jsHelper.RateAndScheduleInputPref);
                                    if (!jsHelper.IsNullOrEmpty(pref)) {
                                        if (pref == '<%= Units.UnitType.Metric.ToInt().ToString() %>') {
                                            jsHelper.UnCheckBox($(jsHelper.AddHashTag('<%= chkEnglish.ClientID %>')).attr('id'));
                                            jsHelper.CheckBox($(jsHelper.AddHashTag('<%= chkMetric.ClientID %>')).attr('id'));
                                        }
                                        if (pref == '<%= Units.UnitType.English.ToInt().ToString() %>') {
                                            jsHelper.UnCheckBox($(jsHelper.AddHashTag('<%= chkMetric.ClientID %>')).attr('id'));
                                            jsHelper.CheckBox($(jsHelper.AddHashTag('<%= chkEnglish.ClientID %>')).attr('id'));
                                        }

                                    }
                                    ProcessUnitsValueChanged(GetCurrentlySelectedUnits());
                                    ReloadCalculatedDensity();
                                });
                            });

                            $(function () {
                                // enforce mutual exclusion on checkboxes in ulVendorRatingImportOptions
                                $("input[id='<%= chkMetric.ClientID %>'], input[id='<%= chkEnglish.ClientID %>']").click(function () {
                                    if (jsHelper.IsChecked($(this).attr('id'))) {
                                        $("input[id='<%= chkMetric.ClientID %>'], input[id='<%= chkEnglish.ClientID %>']").not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                            jsHelper.UnCheckBox($(this).attr('id'));
                                            SetAltUniformCheckBoxClickedStatus(this);
                                        });
                                        SetAltUniformCheckBoxClickedStatus(this);
                                        ProcessUnitsValueChanged(GetCurrentlySelectedUnits());
                                        ReloadCalculatedDensity();
                                    }
                                });

                                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                    $("input[id='<%= chkMetric.ClientID %>'], input[id='<%= chkEnglish.ClientID %>']").click(function () {
                                        if (jsHelper.IsChecked($(this).attr('id'))) {
                                            $("input[id='<%= chkMetric.ClientID %>'], input[id='<%= chkEnglish.ClientID %>']").not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                                jsHelper.UnCheckBox($(this).attr('id'));
                                                SetAltUniformCheckBoxClickedStatus(this);
                                            });
                                            SetAltUniformCheckBoxClickedStatus(this);
                                            ProcessUnitsValueChanged(GetCurrentlySelectedUnits());
                                            ReloadCalculatedDensity();
                                        }
                                    });
                                });
                            });

                            function ProcessUnitsValueChanged(units) {
                                var ids = new ControlIds();
                                ids.DensityWeight = "spWeight";
                                ids.DensityLength = "spLength";
                                ids.DensityWidth = "spWidth";
                                ids.DensityHeight = "spHeight";
                                ids.DensityCalc = "spDensity";
                                SetUnitsLabel(units, ids);
                                jsHelper.SetCookie(jsHelper.RateAndScheduleInputPref, units, jsHelper.CookieDaysToLive);
                            }

                            function ReloadCalculatedDensity() {
                                $(jsHelper.AddHashTag('rateAndScheduleRateItemsTable [id$="txtDensity"]')).each(function () {
                                    UpdateDensity(
                                        $(this).parent().parent().find('[id$="txtWeight"]').attr('id'),
                                        $(this).parent().parent().find('[id$="txtLength"]').attr('id'),
                                        $(this).parent().parent().find('[id$="txtWidth"]').attr('id'),
                                        $(this).parent().parent().find('[id$="txtHeight"]').attr('id'),
                                        $(this).parent().parent().find('[id$="txtDensity"]').attr('id'),
                                        jsHelper.EmptyString,
                                        $(this).parent().parent().find('[id$="txtQuantity"]').attr('id')
                                    );
                                });
                            }

                            function UpdateDensity(weight, length, width, height, densityCalc, hidDensityCalc, densityQty) {

                                var ids = new ControlIds();
                                ids.DensityWeight = weight;
                                ids.DensityLength = length;
                                ids.DensityWidth = width;
                                ids.DensityHeight = height;
                                ids.DensityCalc = densityCalc;
                                ids.HidDensityCalc = hidDensityCalc;
                                ids.DensityQty = densityQty;

                                CalculateDensity(GetCurrentlySelectedUnits(), ids);
                            }

                            function GetCurrentlySelectedUnits() {
                                return jsHelper.IsChecked($(jsHelper.AddHashTag('<%= chkMetric.ClientID %>')).attr('id'))
                                    ? '<%= Units.UnitType.Metric.ToInt().ToString() %>'
                                    : '<%= Units.UnitType.English.ToInt().ToString() %>';
                            }
                        </script>
                        <div class="fieldgroup">
                            <label class="upper">Input Units:</label>
                        </div>
                        <div class="fieldgroup">
                            <eShip:AltUniformCheckBox runat="server" ID="chkMetric" Text="Metric" />
                            <label>Metric</label>
                        </div>
                        <div class="fieldgroup">
                            <eShip:AltUniformCheckBox runat="server" ID="chkEnglish" Text="English" Checked="True" />
                            <label>English</label>
                        </div>
                    </div>

                </div>
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheRateAndScheduleRateItemsTable" TableId="rateAndScheduleRateItemsTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="True" />
                <div class="row mt10">
                    <table class="stripe" id="rateAndScheduleRateItemsTable">
                        <tr class="header" style="text-align: center;">
                            <th style="width: 8%;">Total Weight <span id="spWeight">(lb)</span> <span class="red no-left-border">*</span>
                            </th>
                            <th style="width: 8%;">Length <span id="spLength">(in)</span> <span class="red">+</span>
                            </th>
                            <th style="width: 8%;">Width <span id="spWidth">(in)</span> <span class="red">+</span>
                            </th>
                            <th style="width: 8%;">Height <span id="spHeight">(in)</span> <span class="red">+</span>
                            </th>
                            <th style="width: 9%;">Density <span id="spDensity">lb/ft<sup>3</sup></span>
                            </th>
                            <th style="width: 5%;">Stackable
                            </th>
                            <th style="width: 8%;">
                                <abbr title="Package Quantity">
                                    Package Qty.</abbr>
                                <span class="red">*</span>
                            </th>
                            <th style="width: 8%;">Package<br />
                                Type <span class="red">*</span>
                            </th>
                            <th style="width: 8%;">Freight<br />
                                Class <span class="red">++</span>
                            </th>
                            <th style="width: 8%;">
                                <abbr title="Said to Contain or Number of Pieces">
                                    STC</abbr>
                            </th>
                            <th style="width: 9%;">
                                <abbr title="National Motor Freight Classification">
                                    NMFC</abbr>
                                Code
                            </th>
                            <th style="width: 8%;">
                                <abbr title="SEE IMPORTANT INFORMATION">Value ($)</abbr>
                            </th>
                            <th style="width: 5%;">Action
                            </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstItems" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnItemsItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <eShip:CustomHiddenField runat="server" ID="hidItemIndex" Value='<%# Container.DataItemIndex %>' />
                                        <eShip:CustomTextBox runat="server" ID="txtWeight" Text='<%# Eval("Weight") %>' CssClass="requiredInput w75" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtLength" Text='<%# Eval("Length") %>' CssClass="w55" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtWidth" Text='<%# Eval("Width") %>' CssClass="w55" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtHeight" Text='<%# Eval("Height") %>' CssClass="w55" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtDensity" Text='<%# Eval("Density") %>' CssClass="w55 disabled" ReadOnly="True" />
                                    </td>
                                    <td class="text-center">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkStackable" Checked='<%# Eval("IsStackable") %>' />
                                    </td>
                                    <td class="text-center">
                                        <eShip:CustomTextBox runat="server" ID="txtQuantity" Text='<%# Eval("Quantity") %>' CssClass="w40" Type="NumbersOnly" />
                                    </td>
                                    <td>
                                        <eShip:CachedObjectDropDownList Type="PackageType" ID="ddlPackageType" runat="server" CssClass="w90" 
                                            EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("PackageTypeId") %>' />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlFreightClass" runat="server" CssClass="w60" DataTextField="Text" DataValueField="Value" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtPieceCount" Text='<%# Eval("PieceCount") %>' CssClass="w40" Type="NumbersOnly" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtNmfcCode" Text='<%# Eval("NMFCCode") %>' CssClass="w55" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtValue" Text='<%# Eval("Value") %>' CssClass="w55" Type="FloatingPointNumbers" />
                                    </td>
                                    <td class="text-center">
                                        <asp:ImageButton ID="ibtnItemDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClick="OnDeleteItemClicked" CausesValidation="false" OnClientClick="ShowProgressNotice(true);" />
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td></td>
                                </tr>
                                <tr class="bottom_shadow pb5">
                                    <td colspan="20">
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label>Description: <span class="red">*</span></label>
                                                <eShip:CustomTextBox runat="server" ID="txtDescription" CssClass="w360" MaxLength="200" Text='<%# Eval("Description") %>' />
                                            </div>
                                            <div class="fieldgroup">
                                                <label>Comments:</label>
                                                <eShip:CustomTextBox runat="server" ID="txtComment" CssClass="w340" MaxLength="50" Text='<%# Eval("Comment") %>' />
                                            </div>
                                            <div class="fieldgroup">
                                                <label>HTS Code:</label>
                                                <eShip:CustomTextBox runat="server" ID="txtHtsCode" Text='<%# Eval("HTSCode") %>' CssClass="w55" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="pr5"><abbr title="Hazardous Material">HazMat</abbr>: </label>  
                                                <eShip:AltUniformCheckBox ID="chkHazmat" runat="server" Checked='<%# Eval("HazardousMaterial") %>' />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lnkAddItemFromLibrary" />
            </Triggers>
        </asp:UpdatePanel>

        <hr />

        <div class="row">
            <div class="col_1_3 mh180">
                <h5>References</h5>
                <table class="stripe">
                    <tr>
                        <th style="width: 45%;">Name
                        </th>
                        <th style="width: 55%;">Value
                        </th>
                    </tr>
                    <tr>
                        <td>PO Number
                            <asp:Literal runat="server" ID="litPONumberRequired" Text='<span class="red">* </span>'
                                Visible="False" />
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtPONumber" CssClass="w150" />
                        </td>
                    </tr>
                    <tr>
                        <td>Shipper Reference #
                            <asp:Literal runat="server" ID="litShipperReferenceNumberRequired" Text='<span class="red">* </span>'
                                Visible="False" />
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtShipperReference" CssClass="w150" />
                        </td>
                    </tr>
                    <asp:ListView ID="lstReferences" runat="server" ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <eShip:CustomHiddenField runat="server" ID="hidDisplayOnOrigin" Value='<%# Eval("DisplayOnOrigin") %>' />
                                    <eShip:CustomHiddenField runat="server" ID="hidDisplayOnDestination" Value='<%# Eval("DisplayOnDestination") %>' />
                                    <asp:Literal ID="litName" runat="server" Text='<%# Eval("Name") %>' />
                                    <asp:Literal runat="server" ID="litRequired" Text='<span class="red">*</span>' Visible='<%# Eval("Required") %>' />
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtValue" CssClass="w150" Text='<%# Eval("Value") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </div>
            <div class="col_1_3 mh180">
                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="rowgroup ">
                            <h5>Haz Mat Information</h5>
                            <div class="row">
                                <div class="fieldgroup">
                                    <eShip:AltUniformCheckBox runat="server" ID="chkHazMat" AutoPostBack="True" OnCheckedChanged="OnHazMatCheckChanged" />
                                    <label class="upper">Shipping Hazardous Materials</label>
                                </div>
                            </div>
                            <asp:Panel runat="server" ID="pnlHazMat" Visible="False">
                                <div class="row mt20">
                                    <div class="fieldgroup mr10">
                                        <label class="wlabel">Contact Name <span class="red">*</span></label>
                                        <eShip:CustomTextBox runat="server" ID="txtHazMatName" CssClass="w150" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Phone <span class="red">*</span></label>
                                        <eShip:CustomTextBox runat="server" ID="txtHazMatPhone" CssClass="w150" MaxLength="50" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr10">
                                        <label class="wlabel">Mobile</label>
                                        <eShip:CustomTextBox runat="server" ID="txtHazMatMobile" CssClass="w150" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Email</label>
                                        <eShip:CustomTextBox runat="server" ID="txtHazMatEmail" CssClass="w150" MaxLength="100" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col_1_3 mh180 pl40">
                <div class="row mt20">
                    <div class="fieldgroup text-center">
                        <asp:Button ID="btnRate" Text="Get Rates" runat="server" CausesValidation="False"
                            OnClientClick="ShowProcessingDivBlock();" Font-Bold="True" CssClass="w200" OnClick="OnRateClicked" />
                    </div>
                </div>
                <div class="row mt10">
                    <div class="fieldgroup text-center">
                        <asp:Button runat="server" ID="btnSendToSpecialist" Text="Send To Specialist" CausesValidation="False"
                            OnClientClick="ShowProcessingDivBlock();" CssClass="w200" Visible="False" OnClick="OnSendToSpecialistClicked" />
                    </div>
                </div>
                <div class="row mt10">
                    <div class="fieldgroup text-center">
                        <asp:Button ID="btnClearRate" Text="Clear Selected Rate" runat="server" CausesValidation="False"
                            OnClientClick="ShowProcessingDivBlock();" CssClass="w200" Visible="False" OnClick="OnClearRateClicked" />
                    </div>
                </div>
            </div>
        </div>

        <hr />

        <div class="row">
            <div class="col_1_2 bbox pr10">
                <asp:Panel runat="server" ID="pnlSelectedRateDetails" Visible="False">
                    <eShip:CustomHiddenField runat="server" ID="hidOriginal" />
                    <eShip:CustomHiddenField runat="server" ID="hidAutoRated" />
                    <eShip:CustomHiddenField runat="server" ID="hidDiscountTierId" />
                    <eShip:CustomHiddenField runat="server" ID="hidCFCRuleId" />
                    <eShip:CustomHiddenField runat="server" ID="hidLTLSellRateId" />
                    <eShip:CustomHiddenField runat="server" ID="hidLTLPackageSpecificRate" />
                    <eShip:CustomHiddenField runat="server" ID="hidFreightProfitAdjustment" />
                    <eShip:CustomHiddenField runat="server" ID="hidFuelProfitAdjustment" />
                    <eShip:CustomHiddenField runat="server" ID="hidAccessorialProfitAdjustment" />
                    <eShip:CustomHiddenField runat="server" ID="hidServiceProfitAdjustment" />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerFr" />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerFrIsPercent" />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerFu" />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerFuIsPercent" />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerAcc" />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerAccIsPercent" />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerSer" />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerSerIsPercent" />
                    <eShip:CustomHiddenField runat="server" ID="hidDirectPoint" />
                    <eShip:CustomHiddenField runat="server" ID="hidApplyDiscount" />
                    <eShip:CustomHiddenField runat="server" ID="hidSmallPackType" />
                    <eShip:CustomHiddenField runat="server" ID="hidSmallPackageEngine" />
                    <eShip:CustomHiddenField runat="server" ID="hidBilledWeight" />
                    <eShip:CustomHiddenField runat="server" ID="hidRatedWeight" />
                    <eShip:CustomHiddenField runat="server" ID="hidRatedCubicFeet" />
                    <eShip:CustomHiddenField runat="server" ID="hidRatedPcf" />
                    <eShip:CustomHiddenField runat="server" ID="hidFuPercent" />
                    <eShip:CustomHiddenField runat="server" ID="hidOriginTerminalCode" />
                    <eShip:CustomHiddenField runat="server" ID="hidDestinationTerminalCode" />
                    <eShip:CustomHiddenField runat="server" ID="hidIsGuaranteedDeliveryService" />
                    <eShip:CustomHiddenField runat="server" ID="hidIsExpeditedService" />
                    <eShip:CustomHiddenField runat="server" ID="hidExpeditedServiceTime" />
                    <eShip:CustomHiddenField runat="server" ID="hidExpeditedServiceRate" />
                    <eShip:CustomHiddenField runat="server" ID="hidGuaranteedDeliveryServiceRate" />
                    <eShip:CustomHiddenField runat="server" ID="hidGuaranteedDeliveryServiceRateType" />
                    <eShip:CustomHiddenField runat="server" ID="hidGuaranteedDeliveryServiceTime" />
                    <eShip:CustomHiddenField runat="server" ID="hidLTLGuarateedChargeId"/>
                    <eShip:CustomHiddenField runat="server" ID="hidMarkupValue"/>
                    <eShip:CustomHiddenField runat="server" ID="hidMarkupPercent"/>
                    <eShip:CustomHiddenField runat="server" ID="hidProject44RateSelected"/>
                    <eShip:CustomHiddenField runat="server" ID="hidProject44QuoteNumber"/>
                    <eShip:CustomHiddenField runat="server" ID="hidProject44ServiceCode"/>
                    <eShip:CustomHiddenField runat="server" ID="hidProject44GuaranteedCharge"/>
                    <eShip:CustomHiddenField runat="server" ID="hidProject44ExpeditedCharge"/>
                    <table class="stripe">
                        <tr>
                            <th colspan="4" class="text-center">SELECTED RATE DETAILS
                            </th>
                        </tr>
                        <tr>
                            <td>Carrier
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtCarrier" ReadOnly="True" CssClass="w130 disabled" />
                                <eShip:CustomHiddenField runat="server" ID="hidVendorId" />
                                <eShip:CustomHiddenField runat="server" ID="hidVendorScac" />
                            </td>
                            <td>Service Mode:
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtServiceMode" ReadOnly="True" CssClass="w130 disabled" />
                                <eShip:CustomHiddenField runat="server" ID="hidServiceMode" />
                            </td>
                        </tr>
                        <tr>
                            <td>Freight ($):
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtFreight" ReadOnly="True" CssClass="w130 disabled" />
                            </td>
                            <td>Fuel ($):
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtFuel" ReadOnly="True" CssClass="w130 disabled" />
                            </td>
                        </tr>
                        <tr>
                            <td>Accessorial ($):
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtAccessorial" ReadOnly="True" CssClass="w130 disabled" />
                            </td>
                            <td>Other ($):
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtOther" ReadOnly="True" CssClass="w130 disabled" />
                            </td>
                        </tr>
                        <tr>
                            <td class="upper fs120em">Total ($)
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtTotal" ReadOnly="True" CssClass="w130 disabled" />
                            </td>
                            <td>
                                <abbr title="Estimated Delivery">Est. Del.</abbr>:
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtEstimatedDelivery" ReadOnly="True" CssClass="w130 disabled" />
                            </td>
                        </tr>
                        <asp:Panel runat="server" ID="pnlTLMileage" Visible="False">
                            <tr>
                                <td>Mileage:
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtMileage" ReadOnly="True" CssClass="w130 disabled" />
                                </td>
                                <td>Source:
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtMileageSource" ReadOnly="True" CssClass="w130 disabled" />
                                    <eShip:CustomHiddenField runat="server" ID="hidMileageSourceId" />
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>


                    <table class="stripe">
                        <tr>
                            <th colspan="5" class="text-center">CHARGE BREAKDOWN
                            </th>
                        </tr>
                        <tr class="header">
                            <td style="width: 15%;">Code
                            </td>
                            <td style="width: 45%;">Description
                            </td>
                            <td style="width: 10%;">Category
                            </td>
                            <td style="width: 5%;" class="text-center">Quantity
                            </td>
                            <td style="width: 20%;" class="text-right">Charge ($)
                            </td>
                        </tr>
                        <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>

                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />

                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HiddenField runat="server" ID="hidChargeCodeId" Value='<%# Eval("ChargeCodeId") %>' />
                                        <asp:HiddenField runat="server" ID="hid1" Value='<%# Eval("UnitBuy") %>' />
                                        <asp:HiddenField runat="server" ID="hid2" Value='<%# Eval("UnitSell") %>' />
                                        <asp:HiddenField runat="server" ID="hid3" Value='<%# Eval("UnitDiscount") %>' />
                                        <asp:HiddenField runat="server" ID="hidChargeCodeComment" Value='<%# Eval("Comment") %>' />
                                        <%# Eval("Code") %>
                                    </td>
                                    <td>
                                        <%# Eval("Description") %>
                                    </td>
                                    <td>
                                        <%# Eval("Category") %>
                                    </td>
                                    <td class="text-center">
                                        <asp:Literal runat="server" ID="litQuantity" Text='<%# Eval("Quantity") %>' />
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("AmountDue", "{0:f2}") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </asp:Panel>
            </div>
            <div class="col_1_2 bbox">
                <asp:Panel ID="pnlNoticeText" runat="server" Visible="False" CssClass="rowgroup">
                    <div class="row pb10 bottom_shadow">
                        <h5 class="mt0">Please Note:</h5>
                        <asp:Literal runat="server" ID="litNoticeText" />
                    </div>
                    <div class="row mt20">
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#<%= btnBook.ClientID %>").click(function (e) {
                                    PreventBookingIfTermsConditionsNotAccepted(e);
                                });
                                
                                $("#<%= btnSaveAsLoadOrder.ClientID %>").click(function (e) {
                                    PreventBookingIfTermsConditionsNotAccepted(e);
                                });

                                $("#<%= btnPayAndBook.ClientID %>").click(function (e) {
                                    PreventBookingIfTermsConditionsNotAccepted(e);
                                });
                            });
                            
                            function PreventBookingIfTermsConditionsNotAccepted(e) {
                                if (!jsHelper.IsChecked('<%= chkTermsAndConditions.ClientID %>')) {
                                    e.preventDefault();
                                    alert("You must accept the terms and conditions before proceeding.");
                                } else {
                                    ShowProcessingDivBlock();
                                }
                            }
                        </script>
                        <div class="fieldgroup right">
                            <asp:Button runat="server" ID="btnBook" Text="Send to Specialist" CssClass="w200" OnClick="OnBookClicked" />
                            <div class='<%= btnBook.Visible ? "pb5" : string.Empty %>'></div>
                            <asp:Button runat="server" ID="btnSaveAsLoadOrder" Text="Save As Quote" CssClass="w200" OnClick="OnSaveAsLoadOrderClicked" Visible="False" />
                            <div class='<%= btnSaveAsLoadOrder.Visible ? "pb5" : string.Empty %>'></div>
                            <asp:Button runat="server" ID="btnPayAndBook" Text="Pay And Schedule Pickup" CssClass="w200" OnClick="OnPayAndBookClicked" Visible="False"/>
                        </div>
                        <asp:Panel runat="server" ID="pnlTermsAndConditionsCheckbox" CssClass="fieldgroup mr20 right">
                            <asp:CheckBox runat="server" ID="chkTermsAndConditions" CssClass="jQueryUniform" />
                            <label>
                                I agree to the
                            <asp:HyperLink runat="server" ID="hypTermsAndConditions" Target="_blank" Text="Terms &amp; Conditions" />
                            </label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlCashOnlyCustomerNote" CssClass="fieldgroup right">
                            <div class="fieldgroup note">
                                This customer account is considered cash-only and is not setup to pay by credit card. Please call Logistics Plus to book your load.
                            </div>
                        </asp:Panel>
                    </div>
                </asp:Panel>
            </div>
        </div>

        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
        <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackground" runat="server" Style="display: none;" />
        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
        <eShip:CustomHiddenField runat="server" ID="hidCustomerOutstandingBalance" />
        <eShip:CustomHiddenField runat="server" ID="hidCustomerCredit" />
        <eShip:CustomHiddenField runat="server" ID="hidShoppedRateId" />
        <eShip:CustomHiddenField runat="server" ID="hidTransferToConfirmation" />
        <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    </div>
</asp:Content>
