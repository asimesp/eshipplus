﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;


namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class AddressBookView : MemberPageBaseWithPageStore, IAddressBookView
    {
        private const string ContactsHeader = "Contacts";
        private const string ServicesHeader = "Services";

        private const string SaveFlag = "S";


        public static string PageAddress { get { return "~/Members/Operations/AddressBookView.aspx"; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public override ViewCode PageCode { get { return ViewCode.AddressBook; } }

        public event EventHandler<ViewEventArgs<AddressBook>> Save;
        public event EventHandler<ViewEventArgs<AddressBook>> Delete;
        public event EventHandler<ViewEventArgs<AddressBook>> Lock;
        public event EventHandler<ViewEventArgs<AddressBook>> UnLock;
        public event EventHandler<ViewEventArgs<AddressBook>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<List<AddressBook>>> BatchImport;

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(AddressBook addressBook)
        {
            LoadAddressBook(addressBook);
            SetEditStatus(!addressBook.IsNew);
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<AddressBook>(new AddressBook(hidAddressBookId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? string.Empty : customer.Id.ToString();

            SetEditStatus(customer != null);
        }


        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlContacts.Enabled = enabled;
            pnlServices.Enabled = enabled;
            memberToolBar.EnableSave = enabled;
            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }

        private void UpdateAddressBookServices(AddressBook addressBook)
        {
            var services = lstServices.Items
                .Select(item => new AddressBookService
                {
                    ServiceId = (item.FindControl("hidServiceId")).ToCustomHiddenField().Value.ToLong(),
                    AddressBook = addressBook,
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            addressBook.Services = services;
        }

        private void UpdateAddressBookContacts(AddressBook addressBook)
        {
            var contacts = lstContacts.Items
                .Select(item =>
                {
                    var id = (item.FindControl("hidContactId")).ToCustomHiddenField().Value.ToLong();
                    return new AddressBookContact(id, id != default(long))
                    {
                        Name = (item.FindControl("txtName")).ToTextBox().Text,
                        Phone = (item.FindControl("txtPhone")).ToTextBox().Text,
                        Mobile = (item.FindControl("txtMobile")).ToTextBox().Text,
                        Fax = (item.FindControl("txtFax")).ToTextBox().Text,
                        Email = (item.FindControl("txtEmail")).ToTextBox().Text.StripSpacesFromEmails(),
                        Primary = (item.FindControl("chkPrimary")).ToAltUniformCheckBox().Checked,
                        ContactTypeId = (item.FindControl("ddlContactType")).ToCachedObjectDropDownList().SelectedValue.ToLong(),
                        Comment = (item.FindControl("txtComment")).ToTextBox().Text,
                        TenantId = ActiveUser.TenantId,
                        AddressBook = addressBook,
                    };
                })
                .ToList();

            addressBook.Contacts = contacts;
        }

        private void LoadAddressBook(AddressBook addressBook)
        {
            hidAddressBookId.Value = addressBook.Id.ToString();

            //details tab
            txtAddressBookDescription.Text = addressBook.Description;
            txtStreetOne.Text = addressBook.Street1;
            txtStreetTwo.Text = addressBook.Street2;
            txtCity.Text = addressBook.City;
            txtState.Text = addressBook.State;
            txtPostalCode.Text = addressBook.PostalCode;
            ddlCountries.SelectedValue = addressBook.CountryId.GetString();
            txtOringinInstructions.Text = addressBook.OriginSpecialInstruction;
            txtDestinationInstructions.Text = addressBook.DestinationSpecialInstruction;
            txtDirections.Text = addressBook.Direction;
            txtGeneralInfo.Text = addressBook.GeneralInfo;

            DisplayCustomer(addressBook.Customer);

            //Contacts tab
            lstContacts.DataSource = addressBook.Contacts;
            lstContacts.DataBind();
            tabContacts.HeaderText = addressBook.Contacts.BuildTabCount(ContactsHeader);

            //Services tab
            var services = new ServiceViewSearchDto().RetrieveAddressBookServices(addressBook)
                .Select(s => new
                {
                    Id = s.Id.ToString(),
                    s.Code,
                    s.Description,
                    s.Category,
                    s.ChargeCodeId,
                    ChargeCode = string.Format("{0} - {1}", s.ChargeCode, s.ChargeCodeDescription),
                }).ToList();

            lstServices.DataSource = services;
            lstServices.DataBind();
            tabServices.HeaderText = services.BuildTabCount(ServicesHeader);

            memberToolBar.ShowUnlock = addressBook.HasUserLock(ActiveUser, addressBook.Id);
        }


        private void ProcessTransferredRequest(long addressBookId)
        {
            if (addressBookId != default(long))
            {
                var addressBook = new AddressBook(addressBookId);
                LoadAddressBook(addressBook);

                if (LoadAuditLog != null)
                    LoadAuditLog(this, new ViewEventArgs<AddressBook>(addressBook));
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new AddressBookHandler(this);
            handler.Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Modify;

            addressBookFinder.OpenForEditEnabled = Access.Modify;

            // set filter on service finder
            serviceFinder.ServiceCategoryFilter = ServiceCategory.Accessorial.ToInt();

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.AddressBookImportTemplate });

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            if (Session[WebApplicationConstants.TransferAddressBookId] != null)
            {
                ProcessTransferredRequest(Session[WebApplicationConstants.TransferAddressBookId].ToLong());
                Session[WebApplicationConstants.TransferAddressBookId] = null;
            }

            SetEditStatus(false);
        }


        protected void OnToolbarExportClicked(object sender, EventArgs e)
        {
            if (ActiveUser.RetrieveShipAsCustomers().Count > 0 || ActiveUser.TenantEmployee)
            {
                pnlExportCustomerSelection.Visible = true;
                pnlDimScreen.Visible = pnlExportCustomerSelection.Visible;
                PopulateCustomerDropDown();
            }

            if (ActiveUser.RetrieveShipAsCustomers().Count < 1 && !ActiveUser.TenantEmployee)
            {
                hidCustomerId.Value = ActiveUser.CustomerId.ToString();
                ExportAddressBook();
            }
        }

        protected void OnContinueExportClick(object sender, EventArgs e)
        {
            if (ddlCustomerSelection.SelectedIndex == 0)
            {
                messageBox.Button = MessageButton.Ok;
                messageBox.Icon = MessageIcon.Error;
                messageBox.Message = "Please select a customer to export for";
                messageBox.Visible = true;
                return;
            }

            hidCustomerId.Value = ddlCustomerSelection.SelectedValue;
            pnlExportCustomerSelection.Visible = false;
            pnlDimScreen.Visible = false;
            ExportAddressBook();

        }

        protected void OnExportCloseClick(object sender, ImageClickEventArgs e)
        {
            pnlExportCustomerSelection.Visible = false;
            pnlDimScreen.Visible = pnlExportCustomerSelection.Visible;
        }

        protected void OnExportCancelClick(object sender, EventArgs e)
        {
            pnlExportCustomerSelection.Visible = false;
            pnlDimScreen.Visible = pnlExportCustomerSelection.Visible;
        }


        protected void OnAddressBookFinderItemSelected(object sender, ViewEventArgs<AddressBook> e)
        {
            if (hidAddressBookId.Value.ToLong() != default(long))
            {
                var oldAddressBook = new AddressBook(hidAddressBookId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<AddressBook>(oldAddressBook));
            }

            var addressBook = e.Argument;
            LoadAddressBook(addressBook);

            addressBookFinder.Visible = false;

            if (addressBookFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<AddressBook>(addressBook));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<AddressBook>(addressBook));

            litErrorMessages.Text = string.Empty;
        }

        protected void OnAddresBookItemSelectionCancelled(object sender, EventArgs e)
        {
            addressBookFinder.Visible = false;
        }


        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var addressBookId = hidAddressBookId.Value.ToLong();

            var customerId = hidCustomerId.Value.ToLong();
            var addressBook = new AddressBook(addressBookId, addressBookId != default(long))
            {
                Description = txtAddressBookDescription.Text,
                Street1 = txtStreetOne.Text,
                Street2 = txtStreetTwo.Text,
                City = txtCity.Text,
                State = txtState.Text,
                PostalCode = txtPostalCode.Text,
                CountryId = ddlCountries.SelectedValue.ToInt(),
                OriginSpecialInstruction = txtOringinInstructions.Text,
                DestinationSpecialInstruction = txtDestinationInstructions.Text,
                Direction = txtDirections.Text,
                GeneralInfo = txtGeneralInfo.Text,
                Customer = customerId == default(long) ? null : new Customer(customerId),
                TenantId = ActiveUser.TenantId
            };

            UpdateAddressBookContacts(addressBook);
            UpdateAddressBookServices(addressBook);

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<AddressBook>(addressBook));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<AddressBook>(addressBook));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var addressBook = new AddressBook(hidAddressBookId.Value.ToLong(), false);

            UpdateAddressBookContacts(addressBook);
            UpdateAddressBookServices(addressBook);

            if (Delete != null)
                Delete(this, new ViewEventArgs<AddressBook>(addressBook));

            SetEditStatus(false);

            addressBookFinder.Reset();
        }

        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            addressBookFinder.Visible = true;
        }

        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            LoadAddressBook(new AddressBook
            {
                Customer = ActiveUser.DefaultCustomer,
                CountryId = ActiveUser.Tenant.DefaultCountryId
            });

            serviceFinder.Reset();
            litErrorMessages.Text = string.Empty;
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var addressBook = new AddressBook(hidAddressBookId.Value.ToLong());

            if (Lock == null || addressBook.IsNew) return;
            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<AddressBook>(addressBook));

            LoadAddressBook(addressBook);
        }

        protected void OnToolbarUnlockClicked(object sender, EventArgs e)
        {
            var group = new AddressBook(hidAddressBookId.Value.ToLong(), false);
            if (UnLock != null && !group.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<AddressBook>(group));
            }
        }


        protected void OnAddContactClicked(object sender, EventArgs e)
        {
            var contacts = lstContacts.Items
                .Select(item => new
                {
                    Id = (item.FindControl("hidContactId")).ToCustomHiddenField().Value.ToLong(),
                    Name = (item.FindControl("txtName")).ToTextBox().Text,
                    Phone = (item.FindControl("txtPhone")).ToTextBox().Text,
                    Mobile = (item.FindControl("txtMobile")).ToTextBox().Text,
                    Fax = (item.FindControl("txtFax")).ToTextBox().Text,
                    Email = (item.FindControl("txtEmail")).ToTextBox().Text.StripSpacesFromEmails(),
                    Primary = (item.FindControl("chkPrimary")).ToAltUniformCheckBox().Checked,
                    ContactTypeId = (item.FindControl("ddlContactType")).ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    Comment = (item.FindControl("txtComment")).ToTextBox().Text,
                })
                .ToList();

            contacts.Add(new
                {
                    Id= default(long),
                    Name = string.Empty,
                    Phone = string.Empty,
                    Mobile = string.Empty,
                    Fax = string.Empty,
                    Email = string.Empty,
                    Primary = false,
                    ContactTypeId = default(long),
                    Comment = string.Empty
                });

            lstContacts.DataSource = contacts;
            lstContacts.DataBind();
            tabContacts.HeaderText = contacts.BuildTabCount(ContactsHeader);
            athtuTabUpdater.SetForUpdate(tabContacts.ClientID, contacts.BuildTabCount(ContactsHeader));
        }

        protected void OnDeleteContactClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;

            var contacts = lstContacts.Items
                .Select(item => new
                {
                    Id = (item.FindControl("hidContactId")).ToCustomHiddenField().Value.ToLong(),
                    Name = (item.FindControl("txtName")).ToTextBox().Text,
                    Phone = (item.FindControl("txtPhone")).ToTextBox().Text,
                    Mobile = (item.FindControl("txtMobile")).ToTextBox().Text,
                    Fax = (item.FindControl("txtFax")).ToTextBox().Text,
                    Email = (item.FindControl("txtEmail")).ToTextBox().Text.StripSpacesFromEmails(),
                    Primary = (item.FindControl("chkPrimary")).ToAltUniformCheckBox().Checked,
                    ContactTypeId = (item.FindControl("ddlContactType")).ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    Comment = (item.FindControl("txtComment")).ToTextBox().Text,
                })
                .ToList();

            contacts.RemoveAt((button.FindControl("hidContactIndex")).ToCustomHiddenField().Value.ToInt());

            lstContacts.DataSource = contacts;
            lstContacts.DataBind();
            tabContacts.HeaderText = contacts.BuildTabCount(ContactsHeader);
            athtuTabUpdater.SetForUpdate(tabContacts.ClientID, contacts.BuildTabCount(ContactsHeader));
        }

        protected void OnClearContactsClicked(object sender, EventArgs e)
        {
            lstContacts.DataSource = new List<Contact>();
            lstContacts.DataBind();
            tabContacts.HeaderText = ContactsHeader;
            athtuTabUpdater.SetForUpdate(tabContacts.ClientID, ContactsHeader);
        }


	    protected void OnServiceFinderSelectionCancelled(object sender, EventArgs e)
        {
            serviceFinder.Visible = false;
        }

        protected void OnServiceFinderMultiItemSelected(object sender, ViewEventArgs<List<ServiceViewSearchDto>> e)
        {
            var vServices = lstServices.Items
                .Select(i => new
                {
                    Id = (i.FindControl("hidServiceId")).ToCustomHiddenField().Value,
                    Code = (i.FindControl("litServiceCode")).ToLiteral().Text,
                    Description = (i.FindControl("litServiceDescription")).ToLiteral().Text,
                    Category = (i.FindControl("litServiceCategory")).ToLiteral().Text.ToInt().ToEnum<ServiceCategory>(),
                    ChargeCodeId = (i.FindControl("hidServiceChargeCodeId")).ToCustomHiddenField().Value.ToLong(),
                    ChargeCode = (i.FindControl("litServiceChargeCode")).ToLiteral().Text,
                })
                .ToList();
            vServices.AddRange(e.Argument
                                .Select(t => new
                                {
                                    Id = t.Id.ToString(),
                                    t.Code,
                                    t.Description,
                                    t.Category,
                                    t.ChargeCodeId,
                                    ChargeCode = string.Format("{0} - {1}", t.ChargeCode, t.ChargeCodeDescription),
                                })
                                .Where(t => !vServices.Select(vt => vt.Id).Contains(t.Id)));

            lstServices.DataSource = vServices;
            lstServices.DataBind();
            tabServices.HeaderText = vServices.BuildTabCount(ServicesHeader);

            serviceFinder.Visible = false;
        }

        protected void OnAddServiceClicked(object sender, EventArgs e)
        {
            serviceFinder.Visible = true;
        }

        protected void OnDeleteServiceClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;

            var services = lstServices.Items
                .Select(i => new
                {
                    Id = (i.FindControl("hidServiceId")).ToCustomHiddenField().Value,
                    Code = (i.FindControl("litServiceCode")).ToLiteral().Text,
                    Description = (i.FindControl("litServiceDescription")).ToLiteral().Text,
                    Category = (i.FindControl("litServiceCategory")).ToLiteral().Text.ToInt().ToEnum<ServiceCategory>(),
                    ChargeCodeId = (i.FindControl("hidServiceChargeCodeId")).ToCustomHiddenField().Value.ToLong(),
                    ChargeCode = (i.FindControl("litServiceChargeCode")).ToLiteral().Text,
                })
                .ToList();

            services.RemoveAt((button.FindControl("hidServiceIndex")).ToCustomHiddenField().Value.ToInt());

            lstServices.DataSource = services;
            lstServices.DataBind();
            tabServices.HeaderText = services.BuildTabCount(ServicesHeader);
            athtuTabUpdater.SetForUpdate(tabServices.ClientID, services.BuildTabCount(ServicesHeader));
        }

        protected void OnClearServicesClicked(object sender, EventArgs e)
        {
            lstServices.DataSource = new List<Service>();
            lstServices.DataBind();
            tabServices.HeaderText = ServicesHeader;
            athtuTabUpdater.SetForUpdate(tabServices.ClientID, ServicesHeader);
        }



        protected void OnPostalCodeFinderItemSelected(object sender, ViewEventArgs<PostalCodeViewSearchDto> e)
        {
            txtCity.Text = e.Argument.City;
            txtState.Text = e.Argument.State;
            txtPostalCode.Text = e.Argument.Code;
            ddlCountries.SelectedValue = e.Argument.CountryId.GetString();

            postalCodeFinder.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnPostalCodeFinderItemSelectionCancelled(object sender, EventArgs e)
        {
            postalCodeFinder.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnPostalCodeSearchClicked(object sender, ImageClickEventArgs e)
        {
            postalCodeFinder.Visible = true;
            pnlDimScreen.Visible = true;
        }



        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }



        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var countrySearch = new CountrySearch();
            var customerSearch = new CustomerSearch();

            var addressBooks = new List<AddressBook>();

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 18;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countryCodes = ProcessorVars.RegistryCache.Countries.Values.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(7, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var invalidPostalCodes = chks
                .Where(i => new PostalCodeSearch().FetchPostalCodeByCode(i.Line[6], countrySearch.FetchCountryIdByCode(i.Line[7])).Id == default(long))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidPostalCodes);
                return;
            }

            var customerCriteria = new CustomerViewSearchCriteria
            {
                Parameters = chks
                    .Select(i => i.Line[0])
                    .Distinct()
                    .Select(n =>
                    {
                        var p = RatingSearchFields.CustomerNumber.ToParameterColumn();
                        p.DefaultValue = n;
                        p.Operator = Operator.Equal;
                        return p;
                    })
                    .ToList()
            };
            var customers = new List<Customer>();
            if (customerCriteria.Parameters.Any())
                customers = customerSearch.FetchCustomers(customerCriteria, ActiveUser.TenantId);
            
            var customerDictionary = customers.Select(c => new KeyValuePair<string, long>(c.CustomerNumber, c.Id)).ToDictionary(c => c.Key, c => c.Value);

            msgs.AddRange(chks.Where(chk => !customerDictionary.ContainsKey(chk.Line[0]))
                              .Select(chk => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new Customer().EntityName() ,chk.Index)));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.Where(chk => !ActiveUser.HasAccessToCustomer(customerDictionary[chk.Line[0]]))
                              .Select(chk => ValidationMessage.Error("User does not have access to customer on line {0}", chk.Index)));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }
            

            foreach (var line in lines)
            {
                var addressBook = new AddressBook
                {
                    Customer = customers.First(c => c.CustomerNumber == line[0]),
                    Description = line[1],
                    Street1 = line[2],
                    Street2 = line[3],
                    City = line[4],
                    State = line[5],
                    PostalCode = line[6],
                    CountryId = countrySearch.FetchCountryIdByCode(line[7]),
                    OriginSpecialInstruction = line[8],
                    DestinationSpecialInstruction = line[9],
                    GeneralInfo = line[10],
                    Direction = line[11],
                    TenantId = ActiveUser.TenantId,
                    Contacts = new List<AddressBookContact>(),
                    Services = new List<AddressBookService>()
                };

                if (!string.IsNullOrEmpty(line[12]))
                    addressBook.Contacts.Add(new AddressBookContact
                    {
                        TenantId = ActiveUser.TenantId,
                        ContactTypeId = ActiveUser.Tenant.DefaultSchedulingContactTypeId,
                        Name = line[12],
                        Phone = line[13],
                        Mobile = line[14],
                        Fax = line[15],
                        Email = line[16],
                        Comment = line[17],
                        Primary = true,
                        AddressBook = addressBook
                    });
                addressBooks.Add(addressBook);
            }

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<AddressBook>>(addressBooks));

            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnToolbarImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "LIBRARY ITEM IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format";
            fileUploader.Visible = true;
            pnlDimScreen.Visible = false;

        }


        private void ExportAddressBook()
        {
            var criteria = new AddressBookViewSearchCriteria { CustomerId = hidCustomerId.ToCustomHiddenField().Value.ToLong(), UserId = ActiveUser.Id };
            var addressBooks = new AddressBookViewSearchDto()
                .FetchAddressBookDtos(ActiveUser.TenantId, criteria, false)
                .Select(i => new AddressBook(i.Id));

            var results = new List<string>();

            foreach (var addressBook in addressBooks)
            {
                addressBook.LoadCollections();

                var contact = addressBook.Contacts.FirstOrDefault(c => c.Primary) ?? new AddressBookContact();

                var line =
                    new[]
                            {
                                addressBook.Customer.CustomerNumber,
                                addressBook.Description,
                                addressBook.Street1,
                                addressBook.Street2,
                                addressBook.City,
                                addressBook.State,
                                addressBook.PostalCode,
                                addressBook.Country.Code,
                                addressBook.OriginSpecialInstruction.WrapDoubleQuotes(),
                                addressBook.DestinationSpecialInstruction.WrapDoubleQuotes(),
                                addressBook.GeneralInfo.WrapDoubleQuotes(),
                                addressBook.Direction.WrapDoubleQuotes(),
                                contact.Name,
                                contact.Phone,
                                contact.Mobile,
                                contact.Fax,
                                contact.Email,
                                contact.Comment.WrapDoubleQuotes()
                            }.TabJoin();

                results.Add(line);
            }

            results.Insert(0, new[]
                                    {
                                        "Customer Number",
                                        "Description",
                                        "Street 1",
                                        "Street 2",
                                        "City",
                                        "State",
                                        "Postal Code",
                                        "Country",
                                        "Origin Special Instructions",
                                        "Destination Special Instructions",
                                        "General Info.",
                                        "Directions",
                                        "Name",
                                        "Phone",
                                        "Mobile",
                                        "Fax",
                                        "Email",
                                        "Primary",
                                        "Contact Type",
                                        "Comment"
                                    }
                                .TabJoin());

            Response.Export(results.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }

        private void PopulateCustomerDropDown()
        {
            List<Customer> customers;

            if (!ActiveUser.TenantEmployee)
            {
                var userCustomers = ActiveUser.RetrieveShipAsCustomers();
                customers = userCustomers.Select(userCustomer => new CustomerSearch().FetchCustomerByNumber(userCustomer.CustomerNumber.ToString(), ActiveUser.TenantId)).ToList();
                customers.Add(new CustomerSearch().FetchCustomerByNumber(ActiveUser.DefaultCustomer.CustomerNumber,
                                                                         ActiveUser.TenantId));
            }
            else
            {
                var criteria = new CustomerViewSearchCriteria { OnlyActiveCustomers = true };
                customers = new CustomerSearch().FetchCustomers(criteria, ActiveUser.TenantId);
            }

            var customersForDropDown = customers
                  .Select(c => new ViewListItem(c.Name, c.Id.ToString())).ToList();
            customersForDropDown.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));

            ddlCustomerSelection.DataSource = customersForDropDown;
            ddlCustomerSelection.DataBind();
        }
    }
}
