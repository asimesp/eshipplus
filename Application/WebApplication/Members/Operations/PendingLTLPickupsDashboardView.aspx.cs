﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class PendingLtlPickupsDashboardView : MemberPageBase, IPendingLtlPickupsDashboardView
    {
        protected const string CurrentlyLockedBy = "Record currently locked by: {0}";
        protected const string LockToolTip = "Lock Record";

        private SearchField SortByField
        {
            get { return OperationsSearchFields.PendingLtlPickupSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        public static string PageAddress { get { return "~/Members/Operations/PendingLtlPickupsDashboardView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.PendingPickupsDashboardLTL; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; } }


        public event EventHandler<ViewEventArgs<PendingLtlPickupViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<Shipment>> UpdateShipment;
        public event EventHandler<ViewEventArgs<Shipment>> Lock;
        public event EventHandler<ViewEventArgs<Shipment>> UnLock;


        public void DisplaySearchResult(List<PendingLtlPickupsDashboardDto> shipments)
        {
            litRecordCount.Text = shipments.BuildRecordCount();
            upcseDataUpdate.DataSource = shipments;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any(m => m.Type == ValidateMessageType.Error || m.Type == ValidateMessageType.Warning)) return;

            litTemporaryScript.Text = string.Empty;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void FailedLock(Lock @lock)
        {
            DisplayMessages(new[] { ValidationMessage.Error(@lock.BuildFailedLockMessage()) });

            if (@lock != null) hidFailedLockUserId.Value = @lock.User.Id.ToString();
        }



        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new PendingLtlPickupViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(PendingLtlPickupViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<PendingLtlPickupViewSearchCriteria>(criteria));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            new PendingLtlPickupsDashboardHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set auto referesh
            tmRefresh.Interval = WebApplicationConstants.DefaultRefreshInterval.ToInt();

            // set sort fields
            ddlSortBy.DataSource = OperationsSearchFields.PendingLtlPickupSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = OperationsSearchFields.ShipmentNumber.Name;

            lbtnSortShipmentNumber.CommandName = OperationsSearchFields.ShipmentNumber.Name;
            lbtnSortDateCreated.CommandName = OperationsSearchFields.DateCreated.Name;
            lbtnSortDesiredPickupDate.CommandName = OperationsSearchFields.DesiredPickupDate.Name;
            lbtnSortCustomerNumber.CommandName = OperationsSearchFields.CustomerNumber.Name;
            lbtnSortCustomerName.CommandName = OperationsSearchFields.CustomerName.Name;
            lbtnSortVendorNumber.CommandName = OperationsSearchFields.VendorNumber.Name;
            lbtnSortVendorName.CommandName = OperationsSearchFields.VendorName.Name;
            lbtnSortStatus.CommandName = OperationsSearchFields.StatusText.Name;
            lbtnSortFaxStatus.CommandName = OperationsSearchFields.FaxStatus.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var criteria = new PendingLtlPickupViewSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = profile != null
                                ? profile.Columns
                                : OperationsSearchFields.DefaultPendingLtlPickups.Select(f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.PendingLtlPickups.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.PendingLtlPickups);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = OperationsSearchFields.PendingLtlPickupSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }


        protected void OnTimerTick(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnConfirmPickupClicked(object sender, EventArgs e)
        {
            var btnConfirmPickup = (Button)sender;

            var shipment = new Shipment(btnConfirmPickup.Parent.FindControl("hidShipmentId").ToCustomHiddenField().Value.ToLong());

            shipment.LoadCollections();

            // pre validation to ensure shipmet has not been already confirmed
            if (shipment.CheckCalls.Any(c => c.EdiStatusCode == ShipStatMsgCode.AA.GetString()))
            {
                DisplayMessages(new []{ValidationMessage.Error("Shipment has already been confirmed for pickup")});
                return;
            }

            shipment.CheckCalls.Add(new CheckCall
            {
                User = ActiveUser,
                Shipment = shipment,
                EventDate = DateTime.Now,
                TenantId = shipment.TenantId,
                CallDate = DateTime.Now,
                CallNotes = btnConfirmPickup.Parent.FindControl("txtNote").ToTextBox().Text,
                EdiStatusCode = ShipStatMsgCode.AA.GetString(),
            });

            if (UpdateShipment != null)
                UpdateShipment(this, new ViewEventArgs<Shipment>(shipment));

            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnLockShipmentClicked(object sender, EventArgs e)
        {
            var ibtnLockShipment = ((Control)sender).ToImageButton();
            var hidLockUserId = ibtnLockShipment.Parent.FindControl("hidLockUserId").ToCustomHiddenField();
			var hidCanDispatch = ibtnLockShipment.Parent.FindControl("hidCanDispatch").ToCustomHiddenField();
	        var ibtnDispatchShipment = ibtnLockShipment.Parent.FindControl("ibtnDispatchShipment").ToImageButton();

            var shipment = new Shipment(ibtnLockShipment.Parent.FindControl("hidShipmentId").ToCustomHiddenField().Value.ToLong());
            
            // check if user already holds lock, if so then unlock
            if (hidLockUserId.Value.ToLong() == ActiveUser.Id)
            {
                litTemporaryScript.Text = "<script type='text/javascript'>$(function(){ $(document).scrollTop(" + hidPageScroll.Value + ") }); </script>";

                if(UnLock != null)
                    UnLock(this, new ViewEventArgs<Shipment>(shipment));

                ibtnLockShipment.ImageUrl = "~/images/icons2/lockBlue.png";
                ibtnLockShipment.ToolTip = LockToolTip;
                hidLockUserId.Value = default(long).ToString();
	            ibtnDispatchShipment.Visible = false;
                return;
            }

            if (Lock != null)
                Lock(this, new ViewEventArgs<Shipment>(shipment));

            var @lock = shipment.RetrieveLock(ActiveUser, shipment.Id);

            if (@lock.IsUserLock(ActiveUser))
            {
                litTemporaryScript.Text = "<script type='text/javascript'>$(function(){ $(document).scrollTop(" + hidPageScroll.Value + ") }); </script>";

                ibtnLockShipment.ImageUrl = "~/images/icons2/unlockBlue.png";
                ibtnLockShipment.ToolTip = string.Format(CurrentlyLockedBy, @lock.User.Username);
                hidLockUserId.Value = @lock.User.Id.ToString();
                ibtnLockShipment.Parent.FindControl("btnConfirmPickup").Visible = true;

	            ibtnDispatchShipment.Visible = hidCanDispatch.Value.ToBoolean();
            }
            else if(hidFailedLockUserId.Value.ToLong() != default(long))
            {
                var lockUser = new User(hidFailedLockUserId.Value.ToLong());

                ibtnLockShipment.ImageUrl = "~/images/icons2/unlockBlue.png";
                ibtnLockShipment.ToolTip = string.Format(CurrentlyLockedBy, lockUser.Username);
                hidLockUserId.Value = lockUser.Id.ToString();
                ibtnLockShipment.Parent.FindControl("btnConfirmPickup").Visible = false;
	            ibtnDispatchShipment.Visible = false;
            }

            hidFailedLockUserId.Value = string.Empty;
        }


        protected void OnShipmentPickupDetailsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var pendingPickupDto = (PendingLtlPickupsDashboardDto)item.DataItem;

            if (pendingPickupDto == null) return;

            var btnConfirmPickup = item.FindControl("btnConfirmPickup");
            btnConfirmPickup.Visible = pendingPickupDto.LockedByUserId == default(long) || pendingPickupDto.LockedByUserId == ActiveUser.Id;

            var rptShipmentItems = item.FindControl("rptShipmentItems").ToRepeater();

            rptShipmentItems.DataSource = pendingPickupDto.Items
                .Select(i => new
                    {
                        i.Description,
                        PackageType = i.PackageType.TypeName,
                        ActualWeight = i.ActualWeight.ToString("n2"),
                        i.Quantity,
                        Dimensions = string.Format("{0} x {1} x {2}", i.ActualLength.ToString("n2"), i.ActualWidth.ToString("n2"), i.ActualHeight.ToString("n2")),
                        i.IsStackable
                    });
            rptShipmentItems.DataBind();

            item.FindControl("litTotalWeight").ToLiteral().Text = pendingPickupDto.Items.Sum(i => i.ActualWeight).ToString("n2");

            if (!pendingPickupDto.Services.Any())
            {
                item.FindControl("divAccessorials").Visible = false;
                return;
            }

            var accesorials = pendingPickupDto.Services.Aggregate(string.Empty, (current, service) => string.Format("{0}{1}, ", current, service.Service.Description));
            if (accesorials.Length > 2) accesorials = accesorials.Substring(0, accesorials.Length - 2);
            item.FindControl("litAccessorials").ToLiteral().Text = accesorials;
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
        }


	    protected void OnDispatchShipmentClicked(object sender, ImageClickEventArgs e)
	    {
            if (string.IsNullOrEmpty(ActiveUser.Phone))
            {
                DisplayMessages(new []{ValidationMessage.Error(WebApplicationConstants.DispatchShipmentMissingUserPhoneNumberErrMsg)});
                return;
            }

			var btnDispatch = (ImageButton)sender;

			var shipment = new Shipment(btnDispatch.Parent.FindControl("hidShipmentId").ToCustomHiddenField().Value.ToLong());

			shipment.LoadCollections();

            var dispatchResponse = this.DispatchShipment(shipment);

            if (dispatchResponse.CheckCall == null)
            {
                var messages = new List<ValidationMessage>
                    {
                        ValidationMessage.Error("Error dispatching shipment.  Please call carrier directly!")
                    };

                if (dispatchResponse.Messages.Any())
                    messages.AddRange(dispatchResponse.Messages);

                DisplayMessages(messages);
                return;
            }

            shipment.CheckCalls.Add(dispatchResponse.CheckCall);
			

			if (UpdateShipment != null)
				UpdateShipment(this, new ViewEventArgs<Shipment>(shipment));

			DoSearchPreProcessingThenSearch(SortByField);
	    }
    }
}