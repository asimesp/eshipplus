﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ClaimView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.ClaimView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true" ShowUnlock="False"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="true"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked"
        OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnCommand="OnToolbarCommand" OnUnlock="OnToolbarUnlockClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Claim
                 <eShip:RecordIdentityDisplayControl runat="server" ID="ridcClaimNumberIdentity" TargetControlId="txtClaimNumber" />
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcClaimCustomerNameIdentity" TargetControlId="txtCustomerName" />
            </h3>
            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>
        <hr class="dark mb5" />
        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:ClaimFinderControl runat="server" ID="claimFinder" Visible="false" OnItemSelected="OnClaimFinderItemSelected"
            OnSelectionCancel="OnClaimFinderItemCancelled" OpenForEditEnabled="True" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:VendorFinderControl EnableMultiSelection="true" ID="vendorFinder" OnlyActiveResults="true"
            OnMultiItemSelected="OnVendorFinderMultiItemSelected" OnSelectionCancel="OnVendorFinderItemCancelled"
            OpenForEditEnabled="false" runat="server" Visible="false" />
        <eShip:ServiceTicketFinderControl EnableMultiSelection="False" runat="server" ID="serviceTicketFinder" Visible="false" OnItemSelected="OnServiceTicketFinderItemSelected"
            OnSelectionCancel="OnServiceTicketFinderItemCancelled" OpenForEditEnabled="false" />
        <eShip:ShipmentFinderControl runat="server" EnableMultiSelection="False" ID="shipmentFinder" Visible="false" OnItemSelected="OnShipmentFinderItemSelected"
            OnSelectionCancel="OnShipmentFinderItemCancelled" OpenForEditEnabled="false" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />
        <eShip:CustomHiddenField ID="hidClaimId" runat="server" />

        <ajax:TabContainer ID="tabServiceTicket" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="col_1_2">
                            <h5>General Details</h5>
                            <div class="rowgroup">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Claim Number</label>
                                        <eShip:CustomTextBox runat="server" ID="txtClaimNumber" CssClass="w240 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup pl20">
                                        <label class="wlabel">Status</label>
                                        <eShip:CustomTextBox runat="server" ID="txtStatus" CssClass="w240 disabled" ReadOnly="true" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Record Date Created</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDateCreated" CssClass="w240 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup pl20">
                                        <label class="wlabel">Created By</label>
                                        <eShip:CustomTextBox runat="server" ID="txtUser" CssClass="w240 disabled" ReadOnly="true" />
                                    </div>
                                </div>
                                <div class="row pt40">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Customer
                                            <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(CustomerView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidCustomerId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w150" OnTextChanged="OnCustomerNumberEntered"
                                            AutoPostBack="True" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvCustomer" ControlToValidate="txtCustomerNumber"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <asp:ImageButton ID="btnFindCustomer" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="false" OnClick="OnCustomerSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w330 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Date LP to Pay Carrier</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDateLpAcctToPayCarrier" CssClass="w240" Type="Date" placeholder="99/99/9999" />
                                    </div>
                                    <div class="fieldgroup pl20">
                                        <label class="wlabel">Acknowledged date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtAcknowledged" CssClass="w240" Type="Date" placeholder="99/99/9999"  />
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col_1_2 bbox vlinedarkleft">
                            <h5>Claim Information</h5>
                            <div class="rowgroup ">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Claimant Reference Number</label>
                                        <eShip:CustomTextBox runat="server" ID="txtClaimantReferenceNumber" CssClass="w420" MaxLength="50" />
                                    </div>
                                </div>
                                
                            </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Claim Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtClaimDate" CssClass="w200" Type="Date" placeholder="99/99/9999" />
                                    </div>
                                    <div class="fieldgroup pl20">
                                        <label class="wlabel">Claim Type</label>
                                        <asp:DropDownList runat="server" ID="ddlClaimType" DataValueField="Value" DataTextField="Text" CssClass="w200"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Total Amount Claimed</label>
                                        <eShip:CustomTextBox runat="server" ID="txtAmountClaimed" CssClass="w200" Type="FloatingPointNumbers" />
                                    </div>
                                    <div class="fieldgroup pl20">
                                        <label class="wlabel">Amount Claimed Type</label>
                                        <asp:DropDownList runat="server" ID="ddlAmountClaimedType" DataValueField="Value" DataTextField="Text" CssClass="w200"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Estimated Cost to Repair</label> 
                                        <eShip:CustomTextBox runat="server" ID="txtEstimatedRepairCost" Type="FloatingPointNumbers" CssClass="w200"/>
                                    </div>
                                    <div class="fieldgroup_s pl20">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkIsRepairable" />
                                        <label class="mr10">Claim Items Are Repairable</label> 
                                    </div>
                                </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Check Number</label> 
                                    <eShip:CustomTextBox runat="server" ID="txtCheckNumber"  CssClass="w300"  MaxLength="100" />
                                </div>
                                <div class="fieldgroup pl20">
                                    <label class="wlabel">Check Amount</label> 
                                    <eShip:CustomTextBox runat="server" ID="txtCheckAmount"  CssClass="w100"  Type="FloatingPointNumbers" />
                                </div>
                            </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Claim Detail
                                         <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleClaimDetailText" MaxLength="1900" TargetControlId="txtClaimDetail" />
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <eShip:CustomTextBox runat="server" ID="txtClaimDetail" CssClass="w420 h150" ReadOnly="False" TextMode="MultiLine" Rows="10" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabVendors" HeaderText="Vendors" runat="server">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlVendors" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlVendors" runat="server">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddVendorFromFinder" Text="Add Vendor" CssClass="mr10" CausesValidation="false" OnClick="OnAddVendorClicked" />
                                            <asp:Button runat="server" ID="btnClearVendors" Text="Clear Vendors" OnClick="OnClearVendorsClicked" CausesValidation="False" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheClaimVendorTable" TableId="claimVendorTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="claimVendorTable">
                                    <tr>
                                        <th></th>
                                        <th style="width: 10%;">Vendor Number </th>
                                        <th style="width: 30%;">Name </th>
                                        <th style="width: 25%;">Pro Number</th>
                                        <th style="width: 25%">Freight Bill Number</th>
                                        <th style="width: 10%;" class="text-center">Action </th>
                                    </tr>
                                    <asp:ListView ID="lstVendors" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidInsuranceAlert" Value='<%# Eval("InsuranceAlert") %>' />
                                                    <eShip:CustomHiddenField runat="server" ID="hidInsuranceTooltip" Value='<%# Eval("InsuranceTooltip") %>' />
                                                    <asp:Image CssClass="shipmentAlert" ID="imgInsuranceAlert" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/images/icons/Insurance.png" runat="server" Width="16px" Visible='<%# Eval("InsuranceAlert").ToBoolean() %>'
                                                        ToolTip='<%# Eval("InsuranceTooltip") %>' />
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidClaimVendorId" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomHiddenField ID="hidVendorIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidVendorId" runat="server" Value='<%# Eval("VendorId") %>' />

                                                    <asp:Label runat="server" ID="lblVendorNumber" Text='<%# Eval("VendorNumber") %>' />
                                                    <%# ActiveUser.HasAccessTo(ViewCode.Vendor) && GetDataItem().HasGettableProperty("VendorId")
                                                            ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='16' class='middle'/></a>", 
                                                                ResolveUrl(VendorView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("VendorId").GetString().UrlTextEncrypt(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>

                                                </td>
                                                <td>
                                                    <asp:Literal ID="litName" runat="server" Text='<%# Eval("Name") %>' />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtProNumber" runat="server" Text='<%# Eval("ProNumber") %>' CssClass="w200" MaxLength="50" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtFreightBillNumber" runat="server" Text='<%# Eval("FreightBillNumber") %>' CssClass="w200" MaxLength="50" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteVendorClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddVendorFromFinder" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabDocuments" runat="server" HeaderText="Documents">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDocuments">
                        <div class="rowgroup mb10">
                            <div class="row">
                                <div class="fieldgroup right">
                                    <asp:Button runat="server" ID="btnAddDocument" Text="Add Document" CssClass="mr10" CausesValidation="false" OnClick="OnAddDocumentClicked" />
                                    <asp:Button runat="server" ID="btnClearDocuments" Text="Clear Documents" OnClick="OnClearDocumentsClicked" CausesValidation="False" />
                                </div>
                            </div>
                        </div>
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheClaimDocumentTable" TableId="claimDocumentTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                        <table class="stripe" id="claimDocumentTable">
                            <tr>
                                <th style="width: 22%;">Name</th>
                                <th style="width: 32%;">Description</th>
                                <th style="width: 21%;">Document Tag</th>
                                <th style="width: 15%;">File</th>
                                <th style="width: 8%;" class="text-center">Action</th>
                            </tr>
                            <asp:ListView ID="lstDocuments" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $("a[id$='lnkClaimsDocumentLocationPath']").each(function () {
                                                jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                                            });
                                        });
                                    </script>

                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidDocumentIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                            <eShip:CustomHiddenField ID="hidDocumentId" runat="server" Value='<%# Eval("Id") %>' />
                                            <asp:Literal ID="litDocumentName" runat="server" Text='<%# Eval("Name") %>' />
                                        </td>
                                        <td>
                                            <asp:Literal ID="litDocumentDescription" runat="server" Text='<%# Eval("Description") %>' />
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidDocumentTagId" runat="server" Value='<%# Eval("DocumentTagId") %>' />
                                            <asp:Literal ID="litDocumentTag" runat="server" Text='<%# new DocumentTag(Eval("DocumentTagId").ToLong()).Description %>' />
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidLocationPath" runat="server" Value='<%# Eval("LocationPath") %>' />
                                            <asp:LinkButton runat="server" ID="lnkClaimsDocumentLocationPath" Text='<%# GetLocationFileName(Eval("LocationPath")) %>'
                                                OnClick="OnLocationPathClicked" CausesValidation="False" CssClass="blue" />
                                        </td>
                                        <td class="text-center top">
                                            <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                CausesValidation="false" OnClick="OnEditDocumentClicked" Enabled='<%# Access.Modify %>' />
                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                CausesValidation="false" OnClick="OnDeleteDocumentClicked" Enabled='<%# Access.Modify %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                        <asp:Panel runat="server" ID="pnlEditDocument" Visible="false" CssClass="popup popupControlOverW500">
                            <div class="popheader mb10">
                                <h4>Add/Modify Document
                                </h4>
                                <asp:ImageButton ID="ImageButton5" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                    CausesValidation="false" OnClick="OnCloseEditDocumentClicked" runat="server" />
                            </div>
                            <table class="poptable">
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Name:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtDocumentName" CssClass="w260" MaxLength="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right top">
                                        <label class="upper">Description:</label>
                                        <br />
                                        <label class="lhInherit">
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDocumentDescription" MaxLength="500" TargetControlId="txtDocumentDescription" />
                                        </label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtDocumentDescription" CssClass="w260 h150" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Document Tag:</label>
                                    </td>
                                    <td>
                                        <eShip:CachedObjectDropDownList Type="DocumentTag" ID="ddlDocumentTag" runat="server" CssClass="w260" EnableChooseOne="True" DefaultValue="0" />
                                        <eShip:CustomHiddenField ID="hidDocumentTagId" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Location Path:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomHiddenField ID="hidLocationPath" runat="server" />
                                        <asp:FileUpload runat="server" ID="fupLocationPath" CssClass="jQueryUniform" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Current File:</label>
                                    </td>
                                    <td>
                                        <asp:Literal ID="litLocationPath" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Button ID="Button2" Text="Done" OnClick="OnEditDocumentDoneClicked" runat="server"
                                            CausesValidation="false" />
                                        <asp:Button ID="Button3" Text="Cancel" OnClick="OnCloseEditDocumentClicked" runat="server"
                                            CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabNotes" HeaderText="Notes">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlNotes">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlNotes">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup left">
                                            <eShip:PaginationExtender runat="server" ID="peLstNotes" TargetControlId="lstNotes" PageSize="10" UseParentDataStore="True" />
                                        </div>
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddNote" Text="Add Note" CssClass="right" CausesValidation="false"
                                                OnClick="OnAddNoteClicked" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheClaimNoteTable" TableId="claimNoteTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="claimNoteTable">
                                    <tr>
                                        <th style="width: 3%;">Index </th>
                                        <th style="width: 10%;">Type </th>
                                        <th style="width: 30%;">Reminder Date</th>
                                        <th style="width: 22%;">Reminder Time</th>
                                        <th style="width: 10%; text-align: center;">Archived </th>
                                        <th style="width: 10%; text-align: center;">Classified </th>
                                        <th style="width: 10%; text-align: center;">Send Reminder</th>
                                        <th style="width: 5%; text-align: center;">Action </th>
                                    </tr>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    <asp:ListView ID="lstNotes" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%# string.Format("{0}. ", (Container.DataItemIndex+1) + (peLstNotes.CurrentPage-1) * peLstNotes.PageSize) %>
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidNoteIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <asp:Literal ID="litType" runat="server" Text='<%# Eval("Type").FormattedString() %>' />
                                                </td>
                                                <td>
                                                    <%# Eval("SendReminderOn").ToDateTime() == DateUtility.SystemEarliestDateTime 
                                                ? string.Empty : Eval("SendReminderOn").FormattedShortDate()  %>
                                                </td>
                                                <td>
                                                    <%# Eval("SendReminderOn").ToDateTime() == DateUtility.SystemEarliestDateTime 
                                                ? string.Empty : Eval("SendReminderOn").ToDateTime().GetTime()  %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("Archived").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("Classified").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("SendReminder").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                        CausesValidation="false" OnClick="OnEditNoteClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden"><td></td></tr>
                                            <tr class="bottom_shadow">
                                                <td colspan="8" class="forceLeftBorder pt0">
                                                        <div class="row pl40">
                                                            <div class="col_1_2">
                                                                <div class="row">
                                                                    <div class="fieldgroup">
                                                                        <label class="wlabel blue w130">Message</label>
                                                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("Message") %>' />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col_1_2">
                                                                <div class="row">
                                                                    <div class="fieldgroup">
                                                                        <label class="wlabel blue w130">Reminder Emails</label>
                                                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("SendReminderEmails") %>' />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <asp:Panel runat="server" ID="pnlEditNote" Visible="false" CssClass="popup popupControlOverW500">
                                    <div class="popheader">
                                        <h4>Add/Modify Note</h4>
                                        <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                            CausesValidation="false" OnClick="OnCloseEditNoteClicked" runat="server" />
                                    </div>
                                    <div class="row pt10">


                                        <table class="poptable">
                                            <tr>
                                                <td class="text-right">
                                                    <label class="upper">Type:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlType" DataValueField="Value" DataTextField="Text" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right top">
                                                    <label class="upper">Message:</label>
                                                    <br />
                                                    <label class="lhInherit">
                                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleClaimNoteMessage" MaxLength="1000" TargetControlId="txtMessage" />
                                                    </label>
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox runat="server" ID="txtMessage" CssClass="w300 h150" TextMode="MultiLine" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkArchived" />
                                                    <label class="active mr10">Archived</label>
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkClassified" />
                                                    <label class="active mr10">Classified</label>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right top">
                                                    <label class="upper">Send Reminder On:</label>
                                                    
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox runat="server" ID="txtReminderDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                                    <eShip:CustomTextBox runat="server" ID="txtReminderTime" CssClass="w55 ml5 mr10" Type="Time" placeholder="99:99" />
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkSendReminder" />
                                                    <label class="active">Send Reminder</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right top">
                                                    <label class="upper">Reminder Emails:</label>
                                                    <br />
                                                    <label class="lhInherit">
                                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleReminderEmails" MaxLength="1500" TargetControlId="txtReminderEmails" />
                                                    </label>
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox runat="server" ID="txtReminderEmails" CssClass="w300 h150" TextMode="MultiLine" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <asp:Button ID="btnEditNoteDone" Text="Done" OnClick="OnEditNoteDoneClicked" runat="server"
                                                        CausesValidation="false" />
                                                    <asp:Button ID="btnEditNoteCancel" Text="Cancel" OnClick="OnCloseEditNoteClicked" runat="server"
                                                        CausesValidation="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabShipments" HeaderText="Shipments">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlShipments" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlShipments">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddShipment" Text="Add Shipment" CssClass="mr10" CausesValidation="false" OnClick="OnAddShipmentClicked" />
                                            <asp:Button runat="server" ID="btnClearShipments" Text="Clear Shipments" OnClick="OnClearShipmentsClicked" CausesValidation="False" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheClaimShipmentTable" TableId="claimShipmentTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="claimShipmentTable">
                                    <tr>
                                        <th style="width: 12%;">Shipment # </th>
                                        <th style="width: 15%;">Date Created </th>
                                        <th style="width: 35%;">Customer </th>
                                        <th style="width: 20%;">User </th>
                                        <th style="width: 8%;" class="text-center">Action </th>
                                    </tr>

                                    <asp:ListView ID="lstShipments" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidShipmentIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidShipmentId" runat="server" Value='<%# Eval("Id")%>' />

                                                    <asp:Label runat="server" ID="lblShipmentNumber" Text='<%# Eval("ShipmentNumber") %>' />
                                                    <%# ActiveUser.HasAccessTo(ViewCode.Shipment) && GetDataItem().HasGettableProperty("ShipmentNumber")
                                                            ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src={3} width='16' class='middle'/></a>", 
                                                                ResolveUrl(ShipmentView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("ShipmentNumber"),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litDateCreated" runat="server" Text='<%# Eval("DateCreated").FormattedShortDate() %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litCustomerNumber" runat="server" Text='<%# Eval("CustomerNumber") %>' />
                                                    <asp:Literal ID="litCustomerName" runat="server" Text='<%# Eval("CustomerName") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litUsername" runat="server" Text='<%# Eval("Username") %>' />
                                                </td>
                                                <td class="text-center top">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteShipmentClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddShipment" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabServiceTickets" HeaderText="Service Tickets">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlServiceTickets" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlServiceTickets">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddServiceTicket" Text="Add Service Ticket" CssClass="mr10" CausesValidation="false" OnClick="OnAddServiceTicketClicked" />
                                            <asp:Button runat="server" ID="btnClearServiceTickets" Text="Clear Service Tickets" OnClick="OnClearServiceTicketsClicked" CausesValidation="False" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheClaimServiceTable" TableId="claimServiceTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="claimServiceTable">
                                    <tr>
                                        <th style="width: 15%;">Service Ticket # </th>
                                        <th style="width: 15%;">Date Created </th>
                                        <th style="width: 32%;">Customer </th>
                                        <th style="width: 20%;">User </th>
                                        <th style="width: 8%;" class="text-center">Action </th>
                                    </tr>

                                    <asp:ListView ID="lstServiceTickets" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidServiceTicketIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidServiceTicketId" runat="server" Value='<%# Eval("Id")%>' />

                                                    <asp:Label runat="server" ID="lblServiceTicketNumber" Text='<%# Eval("ServiceTicketNumber") %>' />
                                                    <%# ActiveUser.HasAccessTo(ViewCode.ServiceTicket) && GetDataItem().HasGettableProperty("ServiceTicketNumber")
                                                            ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src={3} width='16' class='middle'/></a>", 
                                                                ResolveUrl(ServiceTicketView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("ServiceTicketNumber"),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>    
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litDateCreated" runat="server" Text='<%# Eval("DateCreated").FormattedShortDate() %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litCustomerNumber" runat="server" Text='<%# Eval("CustomerNumber") %>' />
                                                    -<asp:Literal ID="litCustomerName" runat="server" Text='<%# Eval("CustomerName") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litUsername" runat="server" Text='<%# Eval("Username") %>' />
                                                </td>
                                                <td class="text-center top">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" CssClass="middle"
                                                        CausesValidation="false" OnClick="OnDeleteServiceTicketClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddServiceTicket" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>
    <script type="text/javascript">
        function ClearHidFlag() {
            jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
        }
    </script>

    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
    <asp:UpdatePanel runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
            <eShip:CustomHiddenField runat="server" ID="hidFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
            <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
            <eShip:CustomHiddenField runat="server" ID="hidEditing" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
