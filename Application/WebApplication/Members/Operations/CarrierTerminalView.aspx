﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CarrierTerminalView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.CarrierTerminalView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Carrier Terminals<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />

        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="VendorTerminals" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />
            <div class="row mb10">
                <div class="left">
                    <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                        CausesValidation="False" />
                    <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" />
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upCarrierTerminal">
            <ContentTemplate>
                <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lvwCarrierTerminal" UpdatePanelToExtendId="upCarrierTerminal" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCarrierTerminalTable" TableId="carrierTerminalTable" HeaderZIndex="2" />
                <div class="rowgroup">
                    <table class="line2 pl2" id="carrierTerminalTable">
                        <tr>
                            <th style="width: 16%;">Name
                            </th>
                            <th style="width: 67%;">Address
                            </th>
                            <th style="width: 8%;" class="text-center">Action
                            </th>
                        </tr>
                        <asp:ListView ID="lvwCarrierTerminal" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td rowspan="2" class="top">
                                        <eShip:CustomHiddenField ID="hidVendorTerminalId" runat="server" Value='<%# Eval("Id") %>' />
                                        <%# Eval("Name") %>
                                    </td>
                                    <td>
                                        <%# Eval("FullAddress")%>                        
                                    </td>
                                    <td rowspan="2" class="text-center top">
                                        <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                                        <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>'
                                            OnClientClick="if (!confirm('Are you sure you want to delete record?')) return false;" />
                                    </td>
                                </tr>
                                <tr class="f9">
                                    <td colspan="1" class="forceLeftBorder">
                                        <div class="rowgroup">
                                            <div class="col_1_3 no-right-border">
                                                <div class="row">
                                                    <label class="wlabel blue">Contact</label>
                                                    <%# Eval("ContactName") %>&nbsp;
                                                </div>
                                                <div class="row mt10">
                                                    <label class="wlabel blue">Fax</label>
                                                    <%# Eval("Fax") %>&nbsp;
                                                </div>
                                            </div>
                                            <div class="col_1_3 no-right-border">
                                                <div class="row">
                                                    <label class="wlabel blue">Phone</label>
                                                    <%# Eval("Phone") %>&nbsp;
                                                </div>
                                                <div class="row mt10">
                                                    <label class="wlabel blue">Email</label>
                                                    <%# Eval("Email") %>&nbsp;
                                                </div>
                                            </div>
                                            <div class="col_1_3">
                                                <div class="row">
                                                    <label class="wlabel blue">Mobile</label>
                                                    <%# Eval("Mobile") %>&nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lvwCarrierTerminal" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Panel ID="pnlEditCarrierTerminal" CssClass="popup popupControlOverW750" runat="server" Visible="false">
            <div class="popheader">
                <h4>
                    <asp:Literal ID="litTitle" runat="server" />
                </h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
            </div>
            <div class="row mt20">
                <table class="poptable center">
                    <tr>
                        <td colspan="20" class="red">
                            <asp:Literal ID="litMessage" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="upper">Vendor: </label>
                            <eShip:CustomHiddenField ID="hidEditVendorId" runat="server" />
                            <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w80" OnTextChanged="OnVendorNumberEntered"
                                AutoPostBack="true" />
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                CausesValidation="false" OnClick="OnVendorSearchClicked" />
                            <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                            <eShip:CustomTextBox runat="server" ID="txtVendorName" CssClass="w200 disabled" ReadOnly="true" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="row mt10">
                <div class="col_1_2 bbox">
                    <div class="row">
                        <table class="poptable">
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Name:</label>
                                </td>
                                <td>
                                    <eShip:CustomHiddenField ID="hidEditVendorTerminalId" runat="server" />
                                    <eShip:CustomTextBox ID="txtTerminalName" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Street 1:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalStreet1" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Street 2:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalStreet2" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">City:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalCity" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">State:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalState" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Postal Code:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalPostalCode" runat="server" MaxLength="10" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Country:</label>
                                </td>
                                <td>
                                    <eShip:CachedObjectDropDownList Type="Countries" ID="ddlCountries" runat="server" CssClass="w200" EnableChooseOne="True" DefaultValue="0" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col_1_2 bbox vlinedarkleft">
                    <div class="row">
                        <table class="poptable">
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Contact Name:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalContactName" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Comment:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalComment" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Phone:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalPhone" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Mobile:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalMobile" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Fax:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalFax" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Email:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtTerminalEmail" runat="server" MaxLength="50" CssClass="w200" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row mt20">
                <div class="row">
                    <table class="poptable center">
                        <tr>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <eShip:VendorFinderControl ID="vendorFinder" runat="server" OnItemSelected="OnVendorFinderItemSelected" EnableMultiSelection="False"
        OnSelectionCancel="OnVendorFinderSelectionCancelled" Visible="false" OpenForEditEnabled="false" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
