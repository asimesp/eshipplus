﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class ServiceTicketView : MemberPageBaseWithPageStore, IServiceTicketView
    {
        private const string ItemsHeader = "Items";
        private const string ChargesHeader = "Charges";
        private const string NotesHeader = "Notes";
        private const string VendorsHeader = "Vendors";
        private const string AssetsHeader = "Assets";
        private const string DocumentsHeader = "Documents";

        private const string ReturnArgs = "Return";
        private const string DuplicateArgs = "Duplicate";
        private const string ImportServiceTicketsArgs = "ImportServiceTickets";

        private const string CloseArgs = "Close";
        private const string ReverseCloseArgs = "ReverseClose";

        private const string SaveFlag = "S";
        private const string VendorUpdateFlag = "V";

        private const string DriverAssetsStoreKey = "DASK";
        private const string TractorAssetsStoreKey = "TASK";
        private const string TrailerAssetsStoreKey = "TRASK";

        private const string HeaderImportLineType = "Header";
        private const string ItemImportLineType = "Item";
        private const string ChargeImportLineType = "Charge";
        private const string NoteImportLineType = "Note";
        private const string ServiceImportLineType = "Service";
        private const string EquipmentImportLineType = "Equipment";

		private const string ConfirmChargesArgs = "ConfirmCharges";

        private readonly List<string> _validImportLineTypes =
            new List<string>
                {
                    HeaderImportLineType,
                    ItemImportLineType,
                    ChargeImportLineType,
                    NoteImportLineType,
                    ServiceImportLineType,
                    EquipmentImportLineType
                };


        public static string PageAddress { get { return "~/Members/Operations/ServiceTicketView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.ServiceTicket; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; } }

        private List<ViewListItem> StoredDriverAssets
        {
            get { return PageStore[DriverAssetsStoreKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
            set { PageStore[DriverAssetsStoreKey] = value; }
        }

        private List<ViewListItem> StoredTractorAssets
        {
            get { return PageStore[TractorAssetsStoreKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
            set { PageStore[TractorAssetsStoreKey] = value; }
        }

        private List<ViewListItem> StoredTrailerAssets
        {
            get { return PageStore[TrailerAssetsStoreKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
            set { PageStore[TrailerAssetsStoreKey] = value; }
        }

        public List<ServiceViewSearchDto> Services
        {
            set
            {
                var services = value.Select(c => new ViewListItem(c.FormattedString(), c.Id.ToString())).ToList();

                rptServices.DataSource = services;
                rptServices.DataBind();
            }
        }

        public List<EquipmentType> EquipmentTypes
        {
            set
            {
                var equipmentTypes = value
                    .OrderBy(t => t.TypeName)
                    .Select(c => new ViewListItem("  " + c.FormattedString(), c.Id.ToString()))
                    .ToList();

                rptEquipmentTypes.DataSource = equipmentTypes;
                rptEquipmentTypes.DataBind();
            }
        }

        public Dictionary<int, string> NoteTypes
        {
            set
            {
                var noteTypes = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                noteTypes.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, string.Empty));

                ddlType.DataSource = noteTypes;
                ddlType.DataBind();
            }
        }

        public List<ViewListItem> VendorCollection
        {
            get
            {
                var viewListVendorItems = new List<ViewListItem>();

                if (lstVendors.Items.Any())
                {
                    viewListVendorItems.AddRange(lstVendors.Items.Select(c => new ViewListItem
                    {
                        Text = c.FindControl("litName").ToLiteral().Text,
                        Value = c.FindControl("hidVendorId").ToCustomHiddenField().Value
                    }).ToList());
                }

                viewListVendorItems.Insert(0, new ViewListItem(WebApplicationConstants.AutoDefault, default(long).ToString()));

                if (hidPrimaryVendorId.Value != null)
                {
                    var primaryId = hidPrimaryVendorId.Value.ToLong();
                    var primaryVendor = new Vendor(primaryId, primaryId != default(long));

                    if (viewListVendorItems.All(w => w.Value.ToLong() != primaryVendor.Id))
                    {
                        viewListVendorItems.Insert(1, new ViewListItem() { Text = primaryVendor.Name, Value = primaryVendor.Id.ToString() });
                    }
                }

                return viewListVendorItems;
            }
        }

        public List<Asset> Assets
        {
            set
            {
                var driverAssets = value
                    .Where(c => c.AssetType == AssetType.Driver)
                    .Select(t => new ViewListItem(t.AssetNumber + " - " + t.Description, t.Id.ToString()))
                    .ToList();

                var tractorAssets = value
                    .Where(c => c.AssetType == AssetType.IndependentTractor || c.AssetType == AssetType.OwnerOpTractor || c.AssetType == AssetType.CompanyTractor)
                    .Select(t => new ViewListItem(t.AssetNumber + " - " + t.Description, t.Id.ToString()))
                    .ToList();

                var trailorAssets = value
                    .Where(c => c.AssetType == AssetType.Trailer)
                    .Select(t => new ViewListItem(t.AssetNumber + " - " + t.Description, t.Id.ToString()))
                    .ToList();

                var item = new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString());
                driverAssets.Insert(0, item);
                tractorAssets.Insert(0, item);
                trailorAssets.Insert(0, item);

                StoredDriverAssets = driverAssets;
                StoredTractorAssets = tractorAssets;
                StoredTrailerAssets = trailorAssets;
            }
        }


        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<ServiceTicket>> Save;
        public event EventHandler<ViewEventArgs<ServiceTicket>> Delete;
        public event EventHandler<ViewEventArgs<ServiceTicket>> Lock;
        public event EventHandler<ViewEventArgs<ServiceTicket>> UnLock;
        public event EventHandler<ViewEventArgs<ServiceTicket>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<string>> AccountBucketSearch;
        public event EventHandler<ViewEventArgs<string>> VendorSearch;
        public event EventHandler<ViewEventArgs<string>> PrefixSearch;
        public event EventHandler<ViewEventArgs<List<ServiceTicket>>> BatchImport;

        public void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers = false)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
            hidCustomerOutstandingBalance.Value = customer == null ? default(decimal).ToString() : customer.OutstandingBalance().ToString();

            if (hidAccountBucketId.Value.ToLong() == default(long) && customer != null)
                DisplayAccountBucket(customer.DefaultAccountBucket);

            if (hidPrefixId.Value.ToLong() == default(long) && customer != null && customer.PrefixId != default(long))
                DisplayPrefix(customer.Prefix);

            // sales rep
            if (updateSalesRepAndResellers) DisplayCustomerSalesRepresentative(customer);

            // require customer reconfigurations!!!
            CheckForCreditAlert(lstCharges.Items.Sum(i => i.FindControl("txtAmountDue").ToTextBox().Text.ToDecimal()));

            if (customer != null)
            {
                if (customer.Rating != null)
                {
                    if (updateSalesRepAndResellers)
                        DisplayResellerAddition(customer.Rating.ResellerAddition, customer.Rating.BillReseller);

                    btnRetrieveDetails.Enabled = customer.Rating.ResellerAddition != null;
                    btnClearDetails.Enabled = customer.Rating.ResellerAddition != null;
                }
                else
                {
                    btnRetrieveDetails.Enabled = false;
                    btnClearDetails.Enabled = false;
                }


            }
            else
            {

                btnRetrieveDetails.Enabled = false;
                btnClearDetails.Enabled = false;
            }

            // customer locations
            var customerLocations = customer != null && customer.Locations.Any()
                    ? customer.Locations
                        .Select(l => new ViewListItem(string.Format("{0} - {1} {2}", l.LocationNumber, l.GetBillToString(), l.FullAddress), l.Id.ToString().Trim()))
                        .ToList()
                    : new List<ViewListItem> { new ViewListItem(WebApplicationConstants.NotAvailable, default(long).GetString()) };

            customerLocations.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, default(long).GetString()));
            ddlCustomerLocation.DataSource = customerLocations;
            ddlCustomerLocation.DataBind();
            var id = customer != null && customer.Locations.Any(l => l.BillToLocation && l.MainBillToLocation)
                        ? customer.Locations.First(l => l.BillToLocation && l.MainBillToLocation).Id
                        : default(long);
            if (ddlCustomerLocation.ContainsValue(id.ToString())) ddlCustomerLocation.SelectedValue = id.ToString();

            if (customer == null) return;

            var customerLocationAddresses =
                customer.Locations
                        .Select(l => new
                        {
                            l.Id,
                            BillTo = l.GetBillToString(),
                            l.Street1,
                            l.Street2,
                            l.City,
                            l.State,
                            l.PostalCode,
                            CountryName = l.Country.Name
                        })
                        .ToList();

            rptCustomerLocations.DataSource = customerLocationAddresses;
            rptCustomerLocations.DataBind();
        }

        public void DisplayAccountBucket(AccountBucket accountBucket)
        {
            txtAccountBucketCode.Text = accountBucket == null ? string.Empty : accountBucket.Code;
            txtAccountBucketDescription.Text = accountBucket == null ? string.Empty : accountBucket.Description;
            hidAccountBucketId.Value = accountBucket == null ? default(long).ToString() : accountBucket.Id.ToString();
            ddlAccountBucketUnit.SetSelectedAccountBucketUnit(default(long), accountBucket != null ? accountBucket.Id : default(long));
        }

        public void DisplayPrefix(Prefix prefix)
        {
            txtPrefixCode.Text = prefix == null ? string.Empty : prefix.Code;
            txtPrefixDescription.Text = prefix == null ? string.Empty : prefix.Description;
            hidPrefixId.Value = prefix == null ? default(long).ToString() : prefix.Id.ToString();
        }

        public void DisplayVendor(Vendor vendor)
        {
            if (hidFlag.Value == VendorUpdateFlag)
            {
                if (vendor != null &&
                    ProcessorVars.VendorInsuranceAlertThresholdDays.ContainsKey(vendor.TenantId) &&
                    vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId]))
                {
                    DisplayMessages(new[]
                        {
                            ValidationMessage.Error(string.Format("{0} - {1} Cannot be primary. Reason: {2}",
                                                                  vendor.VendorNumber, vendor.Name,
                                                                  vendor.VendorInsuranceAlertText(
                                                                      ProcessorVars.VendorInsuranceAlertThresholdDays[
                                                                          vendor.TenantId])))
                        });
                    vendor = null;
                    hidFlag.Value = null;
                }
            }

            var oldPrimaryVendorId = hidPrimaryVendorId.Value.ToLong();

            txtVendorName.Text = vendor == null ? string.Empty : vendor.Name;
            txtVendorNumber.Text = vendor == null ? string.Empty : vendor.VendorNumber;
            hidPrimaryVendorId.Value = vendor == null ? default(long).ToString() : vendor.Id.ToString();

            var ddlVendorsList = lstCharges.Items.Select(c => new {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField(),
                VendorBillNumber = c.FindControl("txtVendorBillNumber").ToTextBox()
            }).ToList();

            if (vendor == null || !ProcessorVars.VendorInsuranceAlertThresholdDays.ContainsKey(vendor.TenantId))
            {
                ddlVendorsList.ForEach(cc =>
                {
                    if(string.IsNullOrEmpty(cc.VendorBillNumber.Text))
                    {
                        cc.Vendors.Items.Clear();
                        BuildVendorsDropdownListOnChargesTab(cc.Vendors, cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId ? 0 : cc.SelectedVendorId.Value.ToLong(), true);
                        cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId ? "0" : cc.SelectedVendorId.Value;
                    }
                });
                upPnlCharges.Update();

                return;
            }

            pnlPrimaryVendorInsuranceAlert.Visible = vendor.AlertVendorInsurance(ProcessorVars.DefaultVendorInsuranceAlertThresholdDays);
            litPrimaryVendorInsuranceAlert.Text = vendor.VendorInsuranceAlertText(ProcessorVars.DefaultVendorInsuranceAlertThresholdDays);

            ddlVendorsList.ForEach(cc =>
            {
                if (string.IsNullOrEmpty(cc.VendorBillNumber.Text))
                {
                    cc.Vendors.Items.Clear();
                    BuildVendorsDropdownListOnChargesTab(
                        cc.Vendors,
                        cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId && oldPrimaryVendorId != vendor.Id ? vendor.Id : cc.SelectedVendorId.Value.ToLong(),
                        string.IsNullOrEmpty(cc.VendorBillNumber.Text));
                    cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId && oldPrimaryVendorId != vendor.Id ? vendor.Id.ToString() : cc.SelectedVendorId.Value;
                }
            });
            upPnlCharges.Update();
        }


        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<ServiceTicket>(new ServiceTicket(hidServiceTicketId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(ServiceTicket serviceTicket)
        {
            LoadServiceTicket(serviceTicket);
            SetEditStatus(!serviceTicket.IsNew && serviceTicket.ShouldEnable());
        }


        private void DisplayCustomerSalesRepresentative(Customer customer)
        {
            var representative = customer == null ? null : customer.SalesRepresentative;

            var tier = representative == null
                           ? null
                           : representative.ApplicableTier(txtDateCreated.Text.ToDateTime(), ServiceMode.NotApplicable, customer);

            var commPercent = tier == null ? string.Empty : tier.CommissionPercent.ToString("n4");
            var floorValue = tier == null ? string.Empty : tier.FloorValue.ToString("n4");
            var ceilingValue = tier == null ? string.Empty : tier.CeilingValue.ToString("n4");
            var addlEntityName = representative == null ? string.Empty : representative.AdditionalEntityName;
            var addlEntityCommPercent = representative == null ? string.Empty : representative.AdditionalEntityCommPercent.ToString("n4");
            DisplaySalesRepresentative(representative, commPercent, floorValue, ceilingValue, addlEntityName, addlEntityCommPercent);
        }

        private void DisplaySalesRepresentative(SalesRepresentative representative, string commPercent, string floorValue, string ceilingValue, string addlEntityName, string addlEntityCommPercent)
        {
            hidSalesRepresentativeId.Value = representative != null ? representative.Id.GetString() : string.Empty;

            var canSeeSalesRepData = ActiveUser.HasAccessTo(ViewCode.CanSeeSalesRepFinancialData);

            txtSalesRepresentativeCommissionPercent.Text = canSeeSalesRepData ? commPercent : WebApplicationConstants.OnFile;
            hidSalesRepresentativeCommissionPercent.Value = commPercent;

            txtSalesRepresentative.Text = representative != null
                                            ? string.Format("{0} - {1}", representative.SalesRepresentativeNumber,
                                                            representative.Name)
                                            : string.Empty;
            txtSalesRepresentativeAddlEntityName.Text = addlEntityName;

            txtSalesRepresentativeAddlEntityCommPercent.Text = canSeeSalesRepData ? addlEntityCommPercent : WebApplicationConstants.OnFile;
            txtSalesRepresentativeMinCommValue.Text = canSeeSalesRepData ? floorValue : WebApplicationConstants.OnFile;
            txtSalesRepresentativeMaxCommValue.Text = canSeeSalesRepData ? ceilingValue : WebApplicationConstants.OnFile;

            hidSalesRepresentativeAddlEntityCommPercent.Value = addlEntityCommPercent;
            hidSalesRepresentativeMinCommValue.Value = floorValue;
            hidSalesRepresentativeMaxCommValue.Value = ceilingValue;
        }

        private void DisplayResellerAddition(ResellerAddition reseller, bool billReseller)
        {
            if (reseller != null)
            {
                hidResellerAdditionId.Value = reseller.Id.ToString();
                litResellerAdditionName.Text = reseller.Name;
                hidBillReseller.Value = billReseller.ToString();
                litBillReseller.Text = billReseller ? "Billing: " : string.Empty;
                rptResellerAdditions.DataSource = RetrieveResellerAdditionData(reseller);
                rptResellerAdditions.DataBind();
            }
            else
            {
                hidResellerAdditionId.Value = default(long).ToString();
                litResellerAdditionName.Text = string.Empty;
                hidBillReseller.Value = false.ToString();
                litBillReseller.Text = string.Empty;
                rptResellerAdditions.DataSource = null;
                rptResellerAdditions.DataBind();
            }
        }


        public static string BuildServiceTicketLinks(string commaDelimitedticketNumbers)
        {
            return commaDelimitedticketNumbers
                .Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
                .Where(s => !string.IsNullOrEmpty(s.Trim()))
                .Select(s => string.Format("{0} <a href='{1}?{2}={0}' target='_blank' class='blue'><img src='~/images/icons2/arrow_newTab.png' alt='' width='16' align='absmiddle'/></a>", s.Trim(), PageAddress, WebApplicationConstants.TransferNumber))
                .ToArray()
                .CommaJoin();
        }


        private List<ToolbarMoreAction> ToolbarMoreActions(bool enabled)
        {
            var brDashboard = new ToolbarMoreAction
            {
                CommandArgs = ReturnArgs,
                ImageUrl = IconLinks.Operations,
                Name = ViewCode.ServiceTicketDashboard.FormattedString(),
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var duplicate = new ToolbarMoreAction
            {
                CommandArgs = DuplicateArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Duplicate Service Ticket",
                NavigationUrl = string.Empty
            };

            var close = new ToolbarMoreAction
            {
                CommandArgs = CloseArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Close Service Ticket",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var reverseClose = new ToolbarMoreAction
            {
                CommandArgs = ReverseCloseArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Reverse Closed Service Ticket",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var importServiceTickets = new ToolbarMoreAction
            {
                CommandArgs = ImportServiceTicketsArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Admin,
                Name = "Import Service Tickets",
                IsLink = false,
                NavigationUrl = string.Empty
            };

			var confirmCharges = new ToolbarMoreAction
			{
				CommandArgs = ConfirmChargesArgs,
				ConfirmCommand = true,
				ImageUrl = IconLinks.Operations,
				IsLink = false,
				Name = "Confirm Vendor Charges",
				NavigationUrl = string.Empty
			};

            var separator = new ToolbarMoreAction { IsSeperator = true };
            var notNew = hidServiceTicketId.Value.ToLong() != default(long);
            var actions = new List<ToolbarMoreAction>();
            if (Access.Modify && hidStatus.Value != ServiceTicketStatus.Invoiced.ToString())
            {
                if (notNew && hidStatus.Value != ServiceTicketStatus.Closed.ToString() && enabled) actions.Add(close);
                if (hidStatus.Value == ServiceTicketStatus.Closed.ToString() && enabled) actions.Add(reverseClose);
            }

            if (Access.Modify && hidServiceTicketId.Value.ToLong() != default(long)) actions.Add(duplicate);

            if (ActiveUser.HasAccessToModify(ViewCode.ServiceTicket))
            {
                if (actions.Any()) actions.Add(new ToolbarMoreAction { IsSeperator = true });
                actions.Add(importServiceTickets);
            }
			
			if (Access.Modify && notNew && !enabled && ActiveUser.CanSeeAllVendorsInVendorPortal)
			{
				var hasUnBilledCharge = new ServiceTicketChargeSearch().HasUnBilledCharges(hidServiceTicketId.Value.ToLong());
				if (hasUnBilledCharge)
				{
					actions.Add(separator);
					actions.Add(confirmCharges);
				}
			}
			if (actions.Any()) actions.Add(separator);
            actions.Add(brDashboard);
            return actions;
        }

        private void SetEditStatus(bool enabled)
        {
            hidEditStatus.Value = enabled.ToString();

            var notNew = (hidServiceTicketId.Value.ToLong() != default(long));
            var isOpen = hidStatus.Value == ServiceTicketStatus.Open.ToString();
            var isClosed = hidStatus.Value == ServiceTicketStatus.Closed.ToString();
            var enabledAndOpen = enabled && isOpen;

            pnlDetails.Enabled = enabledAndOpen;
            pnlAdditionalDetails.Enabled = enabledAndOpen;
            pnlItems.Enabled = enabledAndOpen;
            pnlCharges.Enabled = enabledAndOpen;
            pnlAssets.Enabled = enabledAndOpen;
            pnlVendors.Enabled = enabledAndOpen;

            pnlNotes.Enabled = enabled && !isClosed;
            pnlDocuments.Enabled = notNew && enabled && !isClosed;//service ticket must exist before documents can be added.

            memberToolBar.EnableSave = enabled && !isClosed;
            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

            // must do this here to load correct set of extensible actions
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(enabled));
        }

        private static List<object> RetrieveResellerAdditionData(ResellerAddition resellerAddition)
        {
            var freight = new
            {
                Header = "Freight",
                Type = resellerAddition.LineHaulType.FormattedString(),
                Value = resellerAddition.LineHaulValue.ToString("n4"),
                Percent = resellerAddition.LineHaulPercentage.ToString("n4"),
                UseLower = resellerAddition.UseLineHaulMinimum
            };

            var fuel = new
            {
                Header = "Fuel",
                Type = resellerAddition.FuelType.FormattedString(),
                Value = resellerAddition.FuelValue.ToString("n4"),
                Percent = resellerAddition.FuelPercentage.ToString("n4"),
                UseLower = resellerAddition.UseFuelMinimum
            };

            var accessorial = new
            {
                Header = "Accessorial",
                Type = resellerAddition.AccessorialType.FormattedString(),
                Value = resellerAddition.AccessorialValue.ToString("n4"),
                Percent = resellerAddition.AccessorialPercentage.ToString("n4"),
                UseLower = resellerAddition.UseAccessorialMinimum
            };

            var service = new
            {
                Header = "Service",
                Type = resellerAddition.FuelType.FormattedString(),
                Value = resellerAddition.FuelValue.ToString("n4"),
                Percent = resellerAddition.FuelPercentage.ToString("n4"),
                UseLower = resellerAddition.UseFuelMinimum
            };

            var list = new List<object> { freight, fuel, accessorial, service };
            return list;
        }



        private List<ServiceTicketService> RetrieveServiceTicketServices(ServiceTicket serviceTicket)
        {
            var services = (rptServices
                .Items
                .Cast<RepeaterItem>()
                .Where(item => item.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(item => new ServiceTicketService
                {
                    ServiceTicket = serviceTicket,
                    ServiceId = item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong(),
                    TenantId = ActiveUser.TenantId,
                }))
                .ToList();

            return services;
        }

        private List<ServiceTicketEquipment> RetrieveServiceTicketEquipments(ServiceTicket serviceTicket)
        {
            var equipments = (rptEquipmentTypes
                .Items
                .Cast<RepeaterItem>()
                .Where(item => item.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(item => new ServiceTicketEquipment
                {
                    ServiceTicket = serviceTicket,
                    EquipmentTypeId = item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong(),
                    TenantId = ActiveUser.TenantId,
                }))
                .ToList();

            return equipments;
        }


        private ServiceTicket UpdateServiceTicket()
        {
            var serviceTicketId = hidServiceTicketId.Value.ToLong();
            var serviceTicket = new ServiceTicket(serviceTicketId, serviceTicketId != default(long));

            if (serviceTicket.IsNew)
            {
                serviceTicket.TenantId = ActiveUser.TenantId;
                serviceTicket.User = ActiveUser;
                serviceTicket.DateCreated = DateTime.Now;
            }
            else
            {
                serviceTicket.LoadCollections();
            }

            serviceTicket.Customer = new Customer(hidCustomerId.Value.ToLong());
            serviceTicket.OverrideCustomerLocationId = ddlCustomerLocation.SelectedValue.ToLong();
            serviceTicket.AccountBucket = new AccountBucket(hidAccountBucketId.Value.ToLong());
            serviceTicket.AccountBucketUnitId = ddlAccountBucketUnit.SelectedValue.ToLong();
            if (hidPrefixId.Value.ToLong() != default(long)) serviceTicket.Prefix = new Prefix(hidPrefixId.Value.ToLong());

            serviceTicket.SalesRepresentativeId = hidSalesRepresentativeId.Value.ToLong();
            serviceTicket.SalesRepresentativeCommissionPercent = serviceTicket.SalesRepresentativeId == default(long)
                                                                ? "0.0000".ToDecimal()
                                                                : hidSalesRepresentativeCommissionPercent.Value.ToDecimal();
            serviceTicket.SalesRepAddlEntityCommPercent = serviceTicket.SalesRepresentativeId == default(long)
                                                                ? "0.0000".ToDecimal()
                                                                : hidSalesRepresentativeAddlEntityCommPercent.Value.ToDecimal();
            serviceTicket.SalesRepAddlEntityName = serviceTicket.SalesRepresentativeId == default(long)
                                                   ? string.Empty
                                                   : txtSalesRepresentativeAddlEntityName.Text;
            serviceTicket.SalesRepMinCommValue = serviceTicket.SalesRepresentativeId == default(long)
                                                                ? "0.0000".ToDecimal()
                                                                : hidSalesRepresentativeMinCommValue.Value.ToDecimal();
            serviceTicket.SalesRepMaxCommValue = serviceTicket.SalesRepresentativeId == default(long)
                                                                ? "0.0000".ToDecimal()
                                                                : hidSalesRepresentativeMaxCommValue.Value.ToDecimal();

            txtStatus.Text = serviceTicket.Status.FormattedString();
            serviceTicket.ServiceTicketNumber = txtServiceTicketNumber.Text;

            serviceTicket.TicketDate = string.IsNullOrEmpty(txtTicketDate.Text)
                                            ? DateUtility.SystemEarliestDateTime
                                            : txtTicketDate.Text.ToDateTime();

            serviceTicket.ExternalReference1 = txtExternalReference1.Text;
            serviceTicket.ExternalReference2 = txtExternalReference2.Text;

            serviceTicket.HidePrefix = chkHidePrefix.Checked;

            serviceTicket.Services = RetrieveServiceTicketServices(serviceTicket);
            serviceTicket.Equipments = RetrieveServiceTicketEquipments(serviceTicket);

            DisplayServicesAndEquipmentTypes(serviceTicket);

            UpdateItemsFromView(serviceTicket);
            UpdateAssetsFromView(serviceTicket);
            UpdateNotesFromView(serviceTicket);
            UpdateVendorsFromView(serviceTicket);
            UpdateDocumentsFromView(serviceTicket);

            // charges
            serviceTicket.AuditedForInvoicing = chkAuditedForInvoicing.Checked;
            UpdateChargesFromView(serviceTicket);

            // Reseller Addition
            serviceTicket.ResellerAdditionId = hidResellerAdditionId.Value.ToLong();
            serviceTicket.BillReseller = hidBillReseller.Value.ToBoolean();

            // job
            serviceTicket.JobId = hidJobId.Value.ToLong();
            serviceTicket.JobStep = txtJobStep.Text.ToInt();

            return serviceTicket;
        }


        private void UpdateItemsFromView(ServiceTicket serviceTicket)
        {
            var items = lstItems
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong();

                        return new ServiceTicketItem(id, id != default(long))
                        {
                            Description = i.FindControl("txtDescription").ToTextBox().Text,
                            Comment = i.FindControl("txtComment").ToTextBox().Text,
                            Tenant = serviceTicket.Tenant,
                            ServiceTicket = serviceTicket,
                        };
                    })
                .ToList();
            serviceTicket.Items = items;
        }

        private void UpdateChargesFromView(ServiceTicket serviceTicket)
        {
            var charges = lstCharges
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong();

                        return new ServiceTicketCharge(id, id != default(long))
                        {
                            Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                            UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                            UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                            UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                            Comment = i.FindControl("txtComment").ToTextBox().Text,
                            ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                            ServiceTicket = serviceTicket,
                            Tenant = serviceTicket.Tenant,
                            VendorId = i.FindControl("ddlVendors").ToDropDownList().SelectedValue.ToLong()
                        };
                    })
                .ToList();

            serviceTicket.Charges = charges;
        }

        private void UpdateAssetsFromView(ServiceTicket serviceTicket)
        {
            var assets = lstAssets.Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidAssetId").ToCustomHiddenField().Value.ToLong();
                        return new ServiceTicketAsset(id, id != default(long))
                        {
                            DriverAssetId = i.FindControl("ddlDriverAsset").ToDropDownList().SelectedValue.ToLong(),
                            TractorAssetId = i.FindControl("ddlTractorAsset").ToDropDownList().SelectedValue.ToLong(),
                            TrailerAssetId = i.FindControl("ddlTrailerAsset").ToDropDownList().SelectedValue.ToLong(),
                            MilesRun = i.FindControl("txtMilesRun").ToTextBox().Text.ToDecimal(),
                            Primary = i.FindControl("chkPrimary").ToAltUniformCheckBox().Checked,
                            ServiceTicket = serviceTicket,
                            TenantId = serviceTicket.TenantId
                        };
                    }
                )
                .ToList();

            serviceTicket.Assets = assets;
        }

        private void UpdateNotesFromView(ServiceTicket serviceTicket)
        {
            var notes = lstNotes.Items
                .Select(i =>
                {
                    var id = i.FindControl("hidNoteId").ToCustomHiddenField().Value.ToLong();
                    return new ServiceTicketNote(id, id != default(long))
                    {
                        Type = i.FindControl("hidType").ToCustomHiddenField().Value.ToInt().ToEnum<NoteType>(),
                        Archived = i.FindControl("hidArchived").ToCustomHiddenField().Value.ToBoolean(),
                        Classified = i.FindControl("hidClassified").ToCustomHiddenField().Value.ToBoolean(),
                        Message = i.FindControl("litMessage").ToLiteral().Text,
                        ServiceTicket = serviceTicket,
                        TenantId = serviceTicket.TenantId,
                        User = ActiveUser
                    };
                })
                .ToList();
            if (!ActiveUser.HasAccessTo(ViewCode.CanSeeArchivedNotes)) // account for not listed archived notes.
                notes.AddRange(serviceTicket.Notes.Where(n => n.Archived));
            serviceTicket.Notes = notes;
        }

        private void UpdateVendorsFromView(ServiceTicket serviceTicket)
        {
            var vendors = lstVendors.Items
                .Select(i =>
                {
                    var id = i.FindControl("hidServiceTicketVendorId").ToCustomHiddenField().Value.ToLong();
                    return new ServiceTicketVendor(id, id != default(long))
                    {
                        VendorId = i.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                        ServiceTicket = serviceTicket,
                        TenantId = serviceTicket.TenantId,
                        Primary = false
                    };
                })
                .ToList();

            var serviceTicketVendorId = hidPrimaryServiceTicketVendorId.Value.ToLong();
            var vendorId = hidPrimaryVendorId.Value.ToLong();

            if (vendorId != default(long))
                vendors.Add(new ServiceTicketVendor(serviceTicketVendorId, serviceTicketVendorId != default(long))
                {
                    TenantId = serviceTicket.TenantId,
                    Primary = true,
                    ServiceTicket = serviceTicket,
                    VendorId = vendorId
                });

            serviceTicket.Vendors = vendors;
        }

        private void UpdateDocumentsFromView(ServiceTicket serviceTicket)
        {
            var documents = lstDocuments
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong();
                        return new ServiceTicketDocument(id, id != default(long))
                        {
                            DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                            Name = i.FindControl("litDocumentName").ToLiteral().Text,
                            Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                            LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                            IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
                            ServiceTicket = serviceTicket,
                            TenantId = serviceTicket.TenantId
                        };
                    })
                .ToList();

            serviceTicket.Documents = documents;
        }


        private void LoadServiceTicket(ServiceTicket serviceTicket)
        {
            if (!serviceTicket.IsNew) serviceTicket.LoadCollections();

            hidServiceTicketId.Value = serviceTicket.Id.ToString();
            hidStatus.Value = serviceTicket.Status.ToString();
            //Above Details
            txtServiceTicketNumber.Text = serviceTicket.ServiceTicketNumber;
            SetPageTitle(txtServiceTicketNumber.Text);
            DisplayCustomer(serviceTicket.Customer);
            DisplayResellerAddition(serviceTicket.ResellerAddition, serviceTicket.BillReseller);

            if (ddlCustomerLocation.ContainsValue(serviceTicket.OverrideCustomerLocationId.GetString())) // this must happen after customer is set!
                ddlCustomerLocation.SelectedValue = serviceTicket.OverrideCustomerLocationId.GetString();

            txtStatus.Text = serviceTicket.Status.FormattedString();

            //Details
            var primaryServiceTicket = serviceTicket.Vendors.FirstOrDefault(v => v.Primary);
            hidPrimaryServiceTicketVendorId.Value = primaryServiceTicket == null
                                                ? default(long).ToString()
                                                : primaryServiceTicket.Id.ToString();
            DisplayVendor(primaryServiceTicket == null ? new Vendor() : primaryServiceTicket.Vendor);

            DisplayAccountBucket(serviceTicket.AccountBucket);
            ddlAccountBucketUnit.SetSelectedAccountBucketUnit(serviceTicket.AccountBucketUnitId, serviceTicket.AccountBucket != null ? serviceTicket.AccountBucket.Id : default(long));

            DisplayPrefix(serviceTicket.Prefix);
            chkHidePrefix.Checked = serviceTicket.HidePrefix;
            txtDateCreated.Text = serviceTicket.DateCreated.FormattedShortDate();
            txtTicketDate.Text = serviceTicket.TicketDate == DateUtility.SystemEarliestDateTime ? string.Empty : serviceTicket.TicketDate.FormattedShortDate();
            txtUser.Text = serviceTicket.User != null ? serviceTicket.User.Username : ActiveUser.Username;

            DisplayServicesAndEquipmentTypes(serviceTicket);

            //Vendors
            var vendors = serviceTicket.Vendors
                .Where(qv => !qv.Primary)
                .Select(qv => new
                {
                    qv.Id,
                    qv.Vendor.VendorNumber,
                    qv.Vendor.Name,
                    qv.VendorId,
                    InsuranceAlert = qv.Vendor.AlertVendorInsurance(ProcessorVars.DefaultVendorInsuranceAlertThresholdDays),
                    InsuranceTooltip = qv.Vendor.VendorInsuranceAlertText(ProcessorVars.DefaultVendorInsuranceAlertThresholdDays),
                    IsRelationBetweenVendorAndVendorBill = qv.IsRelationBetweenVendorAndVendorBill
                })
                .ToList();

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);

            //Items
            lstItems.DataSource = serviceTicket.Items;
            lstItems.DataBind();
            tabItems.HeaderText = serviceTicket.Items.BuildTabCount(ItemsHeader);

            //Charges
            chkAuditedForInvoicing.Checked = serviceTicket.AuditedForInvoicing;
            lstCharges.DataSource = serviceTicket.Charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = serviceTicket.Charges.BuildTabCount(ChargesHeader);

            var totalDue = serviceTicket.Charges.Sum(c => c.AmountDue);
            hidLoadedServiceTicketTotalDue.Value = totalDue.ToString();

            chargeStatistics.LoadCharges(serviceTicket.Charges, hidResellerAdditionId.Value.ToLong());
            CheckForCreditAlert(serviceTicket.Charges.Sum(c => c.AmountDue));

            //Assets
            lstAssets.DataSource = serviceTicket.Assets;
            lstAssets.DataBind();
            tabAssets.HeaderText = serviceTicket.Assets.BuildTabCount(AssetsHeader);

            // sales presentative details
            var commissionPercent = serviceTicket.SalesRepresentative != null ? serviceTicket.SalesRepresentativeCommissionPercent.ToString("n4") : string.Empty;
            var floorValue = serviceTicket.SalesRepresentative != null ? serviceTicket.SalesRepMinCommValue.ToString("n4") : string.Empty;
            var ceilingValue = serviceTicket.SalesRepresentative != null ? serviceTicket.SalesRepMaxCommValue.ToString("n4") : string.Empty;
            var addlEntityName = serviceTicket.SalesRepresentative != null ? serviceTicket.SalesRepAddlEntityName : string.Empty;
            var addlEntityCommPercent = serviceTicket.SalesRepresentative != null ? serviceTicket.SalesRepAddlEntityCommPercent.ToString("n4") : string.Empty;
            DisplaySalesRepresentative(serviceTicket.SalesRepresentative, commissionPercent, floorValue, ceilingValue, addlEntityName, addlEntityCommPercent);


            txtExternalReference1.Text = serviceTicket.ExternalReference1;
            txtExternalReference2.Text = serviceTicket.ExternalReference2;

            //Notes
            var notes = (ActiveUser.HasAccessTo(ViewCode.CanSeeArchivedNotes))
                                    ? serviceTicket.Notes
                                    : serviceTicket.Notes.Where(n => !n.Archived);
            lstNotes.DataSource = notes;
            lstNotes.DataBind();
            tabNotes.HeaderText = notes.BuildTabCount(NotesHeader);

            //Documents
            lstDocuments.DataSource = serviceTicket.Documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = serviceTicket.Documents.BuildTabCount(DocumentsHeader);

            // Job
            var job = new Job(serviceTicket.JobId);

            if (job.IsNew)
            {
                hidJobId.Value = string.Empty;
                txtJobNumber.Text = string.Empty;
                txtJobStep.Text = string.Empty;
            }
            else
            {
                hidJobId.Value = job.Id.ToString();
                txtJobNumber.Text = job.JobNumber;
                txtJobStep.Text = serviceTicket.JobStep.ToString();
            }

            memberToolBar.ShowUnlock = serviceTicket.HasUserLock(ActiveUser, serviceTicket.Id);
        }


        private void DisplayServicesAndEquipmentTypes(ServiceTicket serviceTicket)
        {
            DisplayServices(serviceTicket.Services.Select(bs => bs.ServiceId));
            DisplayEquipments(serviceTicket.Equipments.Select(be => be.EquipmentTypeId));
        }

        private void DisplayServices(IEnumerable<long> serviceIds)
        {
            hidServices.Value = string.Empty;

            foreach (var item in rptServices.Items.Cast<RepeaterItem>())
            {
                var check = serviceIds.Contains(item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong());
                item.FindControl("chkSelected").ToCheckBox().Checked = check;
                if (check) hidServices.Value += string.Format("{0},", item.FindControl("lblServiceCode").ToLabel().Text);
            }

            lblServices.Text = hidServices.Value;
        }

        private void DisplayEquipments(IEnumerable<long> equipmentTypeIds)
        {
            hidEquipment.Value = string.Empty;

            foreach (var item in rptEquipmentTypes.Items.Cast<RepeaterItem>())
            {
                var check = equipmentTypeIds.Contains(item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong());
                item.FindControl("chkSelected").ToCheckBox().Checked = check;
                if (check) hidEquipment.Value += string.Format("{0},", item.FindControl("lblEquipmentTypeCode").ToLabel().Text);
            }

            lblEquipment.Text = hidEquipment.Value;
        }

        private void CloseServiceTicket()
        {
            var serviceTicket = new ServiceTicket(hidServiceTicketId.Value.ToLong());
            if (serviceTicket.IsNew)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Unable to close new service ticket that has not been previously saved.") });
                return;
            }

            serviceTicket.LoadCollections();
            serviceTicket.TakeSnapShot();

            UpdateNotesFromView(serviceTicket);
            serviceTicket.Status = ServiceTicketStatus.Closed;

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<ServiceTicket>(serviceTicket));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<ServiceTicket>(serviceTicket));
        }

        private void ReverseCloseServiceTicket()
        {
            var serviceTicket = new ServiceTicket(hidServiceTicketId.Value.ToLong());
            if (serviceTicket.IsNew)
            {
                DisplayMessages(new[]
                    {
                        ValidationMessage.Error(
                            "Unable to reverse close on new service ticket that has not been previously saved and closed.")
                    });
                return;
            }

            serviceTicket.LoadCollections();
            serviceTicket.TakeSnapShot();

            serviceTicket.Status = ServiceTicketStatus.Open;
            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<ServiceTicket>(serviceTicket));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<ServiceTicket>(serviceTicket));
        }
		
        private void DuplicateServiceTicket()
        {
            var serviceTicket = UpdateServiceTicket().Duplicate();

            LoadServiceTicket(serviceTicket);

            // account for sales rep & reseller addition on customer if need be.  Reload customer this way will force refresh on sales rep & reseller additions
            DisplayCustomer(serviceTicket.Customer, true);

            SetEditStatus(true);

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
        }

        private void ReturnToServiceTicketDashboard()
        {
            var oldServiceTicket = new ServiceTicket(hidServiceTicketId.Value.ToLong(), false);

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<ServiceTicket>(oldServiceTicket));

            Response.Redirect(ServiceTicketDashboardView.PageAddress);
        }

        private void ProcessTransferredRequest(ServiceTicket ticket, bool disableEdit = false)
        {
            if (ticket == null) return;
            if (ticket.Customer != null && !ticket.IsNew && !ActiveUser.HasAccessToCustomer(ticket.Customer.Id)) return;

            LoadServiceTicket(ticket);

            if (Lock != null && !ticket.IsNew && Access.Modify && !disableEdit)
            {
                SetEditStatus(ticket.ShouldEnable());
                Lock(this, new ViewEventArgs<ServiceTicket>(ticket));
            }
            else if (ticket.IsNew) SetEditStatus(ticket.ShouldEnable());
            else SetEditStatus(false);

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<ServiceTicket>(ticket));
        }

		private void VendorChargeConfirmation()
		{
			var serviceTicket = new ServiceTicket(hidServiceTicketId.Value.ToLong());
			if (serviceTicket.KeyLoaded)
			{
				var criteria = new SearchDefaultsItem
				{
					Columns = new List<ParameterColumn> { AccountingSearchFields.ChargeNumber.ToParameterColumn() },
					SortAscending = true,
					SortBy = null,
					Type = SearchDefaultsItemType.ServiceTickets
				};
				criteria.Columns[0].DefaultValue = serviceTicket.ServiceTicketNumber;
				criteria.Columns[0].Operator = Operator.Equal;
				Session[WebApplicationConstants.ChargeViewSearchCriteria] = criteria;
			}

			if (ActiveUser.HasAccessTo(ViewCode.VendorChargesConfirmation))
				Response.Redirect(Accounting.VendorChargeConfirmation.PageAddress);
			else if (ActiveUser.HasAccessTo(ViewCode.CustomerLoadDashboard))
				Response.Redirect(CustomerLoadDashboardView.PageAddress);

		}



        protected void Page_Load(object sender, EventArgs e)
        {
            new ServiceTicketHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;

            serviceTicketFinder.OpenForEditEnabled = Access.Modify;

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));

            if (!String.IsNullOrEmpty(hidEquipment.Value)) lblEquipment.Text = hidEquipment.Value;
            if (!String.IsNullOrEmpty(hidServices.Value)) lblServices.Text = hidServices.Value;

            SetPageTitle(txtServiceTicketNumber.Text);

            if (IsPostBack) return;

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.ServiceTicketImportTemplate });

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });
            aceVendor.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });
            aceAccountBucketCode.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });
            acePrefixCode.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });

            if (Loading != null)
                Loading(this, new EventArgs());

            SetEditStatus(false);

            if (Session[WebApplicationConstants.TransferServiceTicketId] != null)
            {
                ProcessTransferredRequest(new ServiceTicket(Session[WebApplicationConstants.TransferServiceTicketId].ToLong()));
                Session[WebApplicationConstants.TransferServiceTicketId] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new ServiceTicketSearch().FetchServiceTicketByServiceTicketNumber(Request.QueryString[WebApplicationConstants.TransferNumber].GetString(), ActiveUser.TenantId), true);

        }



        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            serviceTicketFinder.Visible = true;
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var serviceTicket = UpdateServiceTicket();

            var charges = serviceTicket.Charges.Sum(c => c.AmountDue);
            var outstanding = hidCustomerOutstandingBalance.Value.ToDecimal() - hidLoadedShipmentTotalDue.Value.ToDecimal();
            var credit = serviceTicket.Customer == null ? 0 : serviceTicket.Customer.CreditLimit - outstanding;
            var permission = ActiveUser.RetrievePermission(ViewCode.BypassCustomerCreditStop);
            var bypass = permission.Grant && !permission.Deny;

            if (charges > credit && !bypass)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Insufficient customer credit [{0:c2}] to cover service ticket total [{1:c2}]", credit, charges) });
                return;
            }

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<ServiceTicket>(serviceTicket));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<ServiceTicket>(serviceTicket));

            DisplayServicesAndEquipmentTypes(serviceTicket);
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var serviceTicket = new ServiceTicket(hidServiceTicketId.Value.ToLong(), false);

            if (Delete != null)
                Delete(this, new ViewEventArgs<ServiceTicket>(serviceTicket));

            serviceTicketFinder.Reset();

            lblEquipment.Text = string.Empty;
            lblServices.Text = string.Empty;
        }

        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            var serviceTicket = new ServiceTicket
            {
                Customer = new Customer(),
                DateCreated = DateTime.Now,
                TicketDate = DateUtility.SystemEarliestDateTime,
                Status = ServiceTicketStatus.Open,

            };

            LoadServiceTicket(serviceTicket);

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);

            litErrorMessages.Text = string.Empty;
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var serviceTicket = new ServiceTicket(hidServiceTicketId.Value.ToLong(), false);

            if (Lock == null || serviceTicket.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<ServiceTicket>(serviceTicket));

            LoadServiceTicket(serviceTicket);
        }

        protected void OnToolbarUnlockClicked(object sender, EventArgs e)
        {
            var serviceTicket = new ServiceTicket(hidServiceTicketId.Value.ToLong(), false);
            if (UnLock == null || serviceTicket.IsNew) return;

            SetEditStatus(false);
            memberToolBar.ShowUnlock = false;
            UnLock(this, new ViewEventArgs<ServiceTicket>(serviceTicket));
        }

        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case CloseArgs:
                    CloseServiceTicket();
                    break;
                case ReverseCloseArgs:
                    ReverseCloseServiceTicket();
                    break;
                case DuplicateArgs:
                    DuplicateServiceTicket();
                    break;
                case ReturnArgs:
                    ReturnToServiceTicketDashboard();
                    break;
                case ImportServiceTicketsArgs:
                    fileUploader.Visible = true;
                    break;
				case ConfirmChargesArgs:
					VendorChargeConfirmation();
					break;
            }
        }


        protected void OnServiceTicketFinderItemSelected(object sender, ViewEventArgs<ServiceTicket> e)
        {
            if (hidServiceTicketId.Value.ToLong() != default(long))
            {
                var oldServiceTicket = new ServiceTicket(hidServiceTicketId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<ServiceTicket>(oldServiceTicket));
            }

            var serviceTicket = e.Argument;

            LoadServiceTicket(serviceTicket);

            serviceTicketFinder.Visible = false;

            if (serviceTicketFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<ServiceTicket>(serviceTicket));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<ServiceTicket>(serviceTicket));
            litErrorMessages.Text = string.Empty;
        }

        protected void OnServiceTicketFinderItemCancelled(object sender, EventArgs e)
        {
            serviceTicketFinder.Visible = false;
        }


        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument, true);
            customerFinder.Visible = false;
        }


        protected void OnPrefixCodeEntered(object sender, EventArgs e)
        {
            if (PrefixSearch != null)
                PrefixSearch(this, new ViewEventArgs<string>(txtPrefixCode.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnPrefixSearchClicked(object sender, ImageClickEventArgs e)
        {
            prefixFinder.Visible = true;
        }

        protected void OnPrefixFinderSelectionCancelled(object sender, EventArgs e)
        {
            prefixFinder.Visible = false;
        }

        protected void OnPrefixFinderItemSelected(object sender, ViewEventArgs<Prefix> e)
        {
            DisplayPrefix(e.Argument);
            prefixFinder.Visible = false;
        }


        protected void OnAccountBucketCodeEntered(object sender, EventArgs e)
        {
            if (AccountBucketSearch != null)
                AccountBucketSearch(this, new ViewEventArgs<string>(txtAccountBucketCode.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnAccountBucketSearchClicked(object sender, ImageClickEventArgs e)
        {
            accountBucketFinder.Visible = true;
        }

        protected void OnAccountBucketFinderItemSelected(object sender, ViewEventArgs<AccountBucket> e)
        {
            DisplayAccountBucket(e.Argument);
            accountBucketFinder.Visible = false;
        }

        protected void OnAccountBucketFinderSelectionCancelled(object sender, EventArgs e)
        {
            accountBucketFinder.Visible = false;
        }


        protected void OnAddItemClicked(object sender, EventArgs e)
        {
            var items = lstItems.Items
                .Select(control => new
                {
                    Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
                    Description = control.FindControl("txtDescription").ToTextBox().Text,
                    Comment = control.FindControl("txtComment").ToTextBox().Text,
                })
                .ToList();

            items.Add(new
            {
                Id = default(long),
                Description = string.Empty,
                Comment = string.Empty
            });

            lstItems.DataSource = items;
            lstItems.DataBind();
            tabItems.HeaderText = items.BuildTabCount(ItemsHeader);
            athtuTabUpdater.SetForUpdate(tabItems.ClientID, items.BuildTabCount(ItemsHeader));
        }

        protected void OnDeleteItemClicked(object sender, ImageClickEventArgs e)
        {
            var items = lstItems.Items
                .Select(control => new
                {
                    Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
                    Description = control.FindControl("txtDescription").ToTextBox().Text,
                    Comment = control.FindControl("txtComment").ToTextBox().Text,
                })
                .ToList();

            items.RemoveAt(((ImageButton)sender).Parent.FindControl("hidItemIndex").ToCustomHiddenField().Value.ToInt());

            lstItems.DataSource = items;
            lstItems.DataBind();
            tabItems.HeaderText = items.BuildTabCount(ItemsHeader);
            athtuTabUpdater.SetForUpdate(tabItems.ClientID, items.BuildTabCount(ItemsHeader));
        }

        protected void OnClearAllItemsClicked(object sender, EventArgs e)
        {
            lstItems.DataSource = new List<ServiceTicketItem>();
            lstItems.DataBind();
            tabItems.HeaderText = ItemsHeader;
            athtuTabUpdater.SetForUpdate(tabItems.ClientID, ItemsHeader);
        }


        private void CheckForCreditAlert(decimal charges)
        {
            return; // Temporarily disable function v4.0.18.2
            var credit = hidCustomerCredit.Value.ToDecimal();
            var outstanding = hidCustomerOutstandingBalance.Value.ToDecimal() - hidLoadedServiceTicketTotalDue.Value.ToDecimal();
            var alertThreshold = ProcessorVars.CustomerCreditAlertThreshold[ActiveUser.TenantId];
            pnlCreditAlert.Visible = credit.CreditAlert(outstanding + charges, alertThreshold);
        }


        protected void OnChangeVendorInCharge(object sender, EventArgs e)
        {
            lstCharges.Items.Select(c => new {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField(),
            })
            .ToList()
            .ForEach(cc =>
            {
                cc.SelectedVendorId.Value = cc.Vendors.SelectedValue;
            });

            upPnlCharges.Update();
        }


        protected void OnAddChargeClicked(object sender, EventArgs e)
        {
            var charges = lstCharges.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    Vendors = i.FindControl("ddlVendors").ToDropDownList().SelectedValue.ToInt(),
                    VendorId = i.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value,
                    VendorBillDocumentNumber = i.FindControl("txtVendorBillNumber").ToTextBox().Text,
                    VendorBillId = i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong()
                })
                .ToList();

            charges.Add(new
            {
                Id = default(long),
                Quantity = default(int),
                UnitBuy = default(decimal),
                UnitSell = default(decimal),
                UnitDiscount = default(decimal),
                Comment = string.Empty,
                ChargeCodeId = default(long),
                Vendors = 0,
                VendorId = string.Empty,
                VendorBillDocumentNumber = string.Empty,
                VendorBillId = default(long)
            });

            lstCharges.DataSource = charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, charges.BuildTabCount(ChargesHeader));
        }

        protected void OnDeleteChargeClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var charges = lstCharges.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    VendorId = i.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value,
                    VendorBillDocumentNumber = i.FindControl("txtVendorBillNumber").ToTextBox().Text,
                    VendorBillId = i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong()
                })
                .ToList();

            charges.RemoveAt(imageButton.Parent.FindControl("hidChargeIndex").ToCustomHiddenField().Value.ToInt());

            lstCharges.DataSource = charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, charges.BuildTabCount(ChargesHeader));
            var chargeList = charges
                    .Select(c => new Charge
                    {
                        ChargeCodeId = c.ChargeCodeId,
                        Comment = c.Comment,
                        Quantity = c.Quantity,
                        UnitBuy = c.UnitBuy,
                        UnitSell = c.UnitSell,
                        UnitDiscount = c.UnitDiscount,
                    }).ToList();

            chargeStatistics.LoadCharges(chargeList, hidResellerAdditionId.Value.ToLong());
            CheckForCreditAlert(chargeList.Sum(c => c.AmountDue));
        }

        protected void OnClearAllChargesClicked(object sender, EventArgs e)
        {
            var charges = lstCharges.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    VendorId = i.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value,
                    VendorBillDocumentNumber = i.FindControl("txtVendorBillNumber").ToTextBox().Text,
                    VendorBillId = i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong()
                }).Where(s => s.VendorBillId > 0)
                .ToList();

            lstCharges.DataSource = charges;
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, charges.BuildTabCount(ChargesHeader));

            var chargeList = charges
                    .Select(c => new Charge
                    {
                        ChargeCodeId = c.ChargeCodeId,
                        Comment = c.Comment,
                        Quantity = c.Quantity,
                        UnitBuy = c.UnitBuy,
                        UnitSell = c.UnitSell,
                        UnitDiscount = c.UnitDiscount,
                    }).ToList();

            chargeStatistics.LoadCharges(chargeList, hidResellerAdditionId.Value.ToLong());
            CheckForCreditAlert(chargeList.Sum(c => c.AmountDue));
        }        

        protected void OnRefreshChargeStatisticsClicked(object sender, ImageClickEventArgs e)
        {
            var chargeList = lstCharges
                .Items
                .Select(i => new Charge
                {
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                })
                .ToList();

            chargeStatistics.LoadCharges(chargeList, hidResellerAdditionId.Value.ToLong());
        }



        protected void OnAddAssetClicked(object sender, EventArgs e)
        {
            var assets = lstAssets.Items
                .Select(control => new
                {
                    Id = control.FindControl("hidAssetId").ToCustomHiddenField().Value.ToLong(),
                    DriverAssetId = control.FindControl("ddlDriverAsset").ToDropDownList().SelectedValue.ToLong(),
                    TractorAssetId = control.FindControl("ddlTractorAsset").ToDropDownList().SelectedValue.ToLong(),
                    TrailerAssetId = control.FindControl("ddlTrailerAsset").ToDropDownList().SelectedValue.ToLong(),
                    MilesRun = control.FindControl("txtMilesRun").ToTextBox().Text,
                    Primary = control.FindControl("chkPrimary").ToAltUniformCheckBox().Checked
                })
                .ToList();

            assets.Add(new
            {
                Id = default(long),
                DriverAssetId = default(long),
                TractorAssetId = default(long),
                TrailerAssetId = default(long),
                MilesRun = string.Empty,
                Primary = false
            });

            lstAssets.DataSource = assets;
            lstAssets.DataBind();
            tabAssets.HeaderText = assets.BuildTabCount(AssetsHeader);
            athtuTabUpdater.SetForUpdate(tabAssets.ClientID, assets.BuildTabCount(AssetsHeader));
        }

        protected void OnDeleteAssetClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var assets = lstAssets.Items
                .Select(control => new
                {
                    Id = control.FindControl("hidAssetId").ToCustomHiddenField().Value.ToLong(),
                    DriverAssetId = control.FindControl("ddlDriverAsset").ToDropDownList().SelectedValue.ToLong(),
                    TractorAssetId = control.FindControl("ddlTractorAsset").ToDropDownList().SelectedValue.ToLong(),
                    TrailerAssetId = control.FindControl("ddlTrailerAsset").ToDropDownList().SelectedValue.ToLong(),
                    MilesRun = control.FindControl("txtMilesRun").ToTextBox().Text,
                    Primary = control.FindControl("chkPrimary").ToAltUniformCheckBox().Checked
                })
                .ToList();

            assets.RemoveAt(imageButton.Parent.FindControl("hidAssetIndex").ToCustomHiddenField().Value.ToInt());

            lstAssets.DataSource = assets;
            lstAssets.DataBind();
            tabAssets.HeaderText = assets.BuildTabCount(AssetsHeader);
            athtuTabUpdater.SetForUpdate(tabAssets.ClientID, assets.BuildTabCount(AssetsHeader));
        }

        protected void OnClearAllAssetsClicked(object sender, EventArgs e)
        {
            lstAssets.DataSource = new List<ServiceTicketAsset>();
            lstAssets.DataBind();
            tabAssets.HeaderText = AssetsHeader;
            athtuTabUpdater.SetForUpdate(tabAssets.ClientID, AssetsHeader);
        }

        protected void OnServiceTicketAssetsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var dataItem = item.DataItem;

            var ddlDriverAsset = item.FindControl("ddlDriverAsset").ToDropDownList();
            ddlDriverAsset.DataSource = StoredDriverAssets;
            ddlDriverAsset.DataBind();
            if (dataItem.HasGettableProperty("DriverAssetId")) ddlDriverAsset.SelectedValue = dataItem.GetPropertyValue("DriverAssetId").GetString();

            var ddlTractorAsset = item.FindControl("ddlTractorAsset").ToDropDownList();
            ddlTractorAsset.DataSource = StoredTractorAssets;
            ddlTractorAsset.DataBind();
            if (dataItem.HasGettableProperty("TractorAssetId")) ddlTractorAsset.SelectedValue = dataItem.GetPropertyValue("TractorAssetId").GetString();

            var ddlTrailerAsset = item.FindControl("ddlTrailerAsset").ToDropDownList();
            ddlTrailerAsset.DataSource = StoredTrailerAssets;
            ddlTrailerAsset.DataBind();
            if (dataItem.HasGettableProperty("TrailerAssetId")) ddlTrailerAsset.SelectedValue = dataItem.GetPropertyValue("TrailerAssetId").GetString();
        }


        private void LoadDocumentForEdit(ServiceTicketDocument document)
        {
            chkDocumentIsInternal.Checked = document.IsInternal;
            txtDocumentName.Text = document.Name;
            txtDocumentDescription.Text = document.Description;
            ddlDocumentTag.SelectedValue = document.DocumentTagId.GetString();
            txtLocationPath.Text = GetLocationFileName(document.LocationPath);
            hidLocationPath.Value = document.LocationPath;
	        hidVendorBillId.Value = document.VendorBillId.ToString();
        }

        protected string GetLocationFileName(object relativePath)
        {
            return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
        }

        protected void OnAddDocumentClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadDocumentForEdit(new ServiceTicketDocument());
            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnClearDocumentsClicked(object sender, EventArgs e)
        {
            foreach (var item in lstDocuments.Items)
                hidFilesToDelete.Value += string.Format("{0};", item.FindControl("hidLocationPath").ToCustomHiddenField().Value);

            lstDocuments.DataSource = new List<object>();
            lstDocuments.DataBind();
            tabDocuments.HeaderText = new List<object>().BuildTabCount(DocumentsHeader);
        }

        protected void OnEditDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            hidEditingIndex.Value = control.FindControl("hidDocumentIndex").ToCustomHiddenField().Value;

            var document = new ServiceTicketDocument
            {
                Name = control.FindControl("litDocumentName").ToLiteral().Text,
                Description = control.FindControl("litDocumentDescription").ToLiteral().Text,
                DocumentTagId = control.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                LocationPath = control.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                IsInternal = control.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
	            VendorBillId = control.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong(),
			};

            LoadDocumentForEdit(document);

            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnDeleteDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var documents = lstDocuments
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("litDocumentName").ToLiteral().Text,
                    Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                    DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                    LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                    IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
	                VendorBill = new VendorBill(i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong()),
				})
                .ToList();

            var index = imageButton.Parent.FindControl("hidDocumentIndex").ToCustomHiddenField().Value.ToInt();
            hidFilesToDelete.Value += string.Format("{0};", documents[index].LocationPath);
            documents.RemoveAt(index);

            lstDocuments.DataSource = documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);
        }

        protected void OnCloseEditDocumentClicked(object sender, EventArgs eventArgs)
        {
            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditDocumentDoneClicked(object sender, EventArgs e)
        {
            var virtualPath = WebApplicationSettings.ServiceTicketFolder(ActiveUser.TenantId, hidServiceTicketId.Value.ToLong());
            var physicalPath = Server.MapPath(virtualPath);

            var documents = lstDocuments
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("litDocumentName").ToLiteral().Text,
                    Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                    DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value,
                    LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                    IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
	                VendorBill = new VendorBill(i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong()),
				})
                .ToList();

            var documentIndex = hidEditingIndex.Value.ToInt();

            var documentId = default(long);
            if (documentIndex != -1)
            {
                documentId = documents[documentIndex].Id;
                documents.RemoveAt(documentIndex);
            }

            var locationPath = hidLocationPath.Value; // this should actually be a hidden field holding the existing path during edit
	        var vendorBillid = hidVendorBillId.Value; // this should actually be a hidden field holding the existing path during edit

			documents.Insert(0, new
            {
                Id = documentId,
                Name = txtDocumentName.Text,
                Description = txtDocumentDescription.Text,
                DocumentTagId = ddlDocumentTag.SelectedValue,
                LocationPath = locationPath,
                IsInternal = chkDocumentIsInternal.Checked,
	            VendorBill = new VendorBill(vendorBillid.ToLong()),
			});

            if (fupLocationPath.HasFile)
            {
                // ensure directory is present
                if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

                //ensure unique filename
                var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLocationPath.FileName), true));

                // save file
                fupLocationPath.WriteToFile(physicalPath, fi.Name, Server.MapPath(documents[0].LocationPath));

                // remove updated record
                documents.RemoveAt(0);

                // re-add record with new file
                documents.Insert(0, new
                {
                    Id = documentId,
                    Name = txtDocumentName.Text,
                    Description = txtDocumentDescription.Text,
                    DocumentTagId = ddlDocumentTag.SelectedValue,
                    LocationPath = virtualPath + fi.Name,
                    IsInternal = chkDocumentIsInternal.Checked,
	                VendorBill = new VendorBill(vendorBillid.ToLong())
				});
            }

            lstDocuments.DataSource = documents.OrderBy(d => d.Name);
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);

            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnLocationPathClicked(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
            Response.Export(Server.ReadFromFile(path), button.Text);
        }



        private void LoadNoteForEdit(Note note)
        {
            chkArchived.Checked = note.Archived;
            chkClassified.Checked = note.Classified;
            ddlType.SelectedValue = note.Type.ToInt().ToString();

            txtMessage.Text = note.Message;

            if (!String.IsNullOrEmpty(note.Message))
            {
                txtMessage.ReadOnly = true;
                txtMessage.CssClass = "disabled w300 h150";
            }
            else
            {
                txtMessage.ReadOnly = false;
                txtMessage.CssClass = "w300 h150";
            }
        }

        protected void OnAddNoteClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadNoteForEdit(new ClaimNote { Type = NoteType.Normal });
            pnlEditNote.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnEditNoteClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            hidEditingIndex.Value = control.FindControl("hidNoteIndex").ToCustomHiddenField().Value;

            var note = new ClaimNote
            {
                Type = control.FindControl("hidType").ToCustomHiddenField().Value.ToInt().ToEnum<NoteType>(),
                Archived = control.FindControl("hidArchived").ToCustomHiddenField().Value.ToBoolean(),
                Classified = control.FindControl("hidClassified").ToCustomHiddenField().Value.ToBoolean(),
                Message = control.FindControl("litMessage").ToLiteral().Text
            };

            LoadNoteForEdit(note);

            pnlEditNote.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnCloseEditNoteClicked(object sender, EventArgs e)
        {
            pnlEditNote.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditNoteDoneClicked(object sender, EventArgs e)
        {
            var notes = lstNotes.Items
                .Select(control => new
                {
                    Id = control.FindControl("hidNoteId").ToCustomHiddenField().Value,
                    Type = control.FindControl("hidType").ToCustomHiddenField().Value.ToInt().ToEnum<NoteType>(),
                    Archived = control.FindControl("hidArchived").ToCustomHiddenField().Value.ToBoolean(),
                    Classified = control.FindControl("hidClassified").ToCustomHiddenField().Value.ToBoolean(),
                    Message = control.FindControl("litMessage").ToLiteral().Text,
                })
                .ToList();

            var noteIndex = hidEditingIndex.Value.ToInt();

            var noteId = default(long).ToString();
            if (noteIndex != -1)
            {
                noteId = notes[noteIndex].Id;
                notes.RemoveAt(noteIndex);
            }

            notes.Insert(0, new
            {
                Id = noteId,
                Type = ddlType.SelectedValue.ToInt().ToEnum<NoteType>(),
                Archived = chkArchived.Checked,
                Classified = chkClassified.Checked,
                Message = txtMessage.Text,
            });

            lstNotes.DataSource = notes;
            lstNotes.DataBind();
            tabNotes.HeaderText = notes.BuildTabCount(NotesHeader);

            pnlEditNote.Visible = false;
            pnlDimScreen.Visible = false;
        }


        protected void OnAddVendorClicked(object sender, EventArgs e)
        {
            vendorFinder.Reset();
            vendorFinder.EnableMultiSelection = true;
            vendorFinder.Visible = true;
        }

        protected void OnVendorNumberEntered(object sender, EventArgs e)
        {
            hidFlag.Value = VendorUpdateFlag;

            if (VendorSearch != null)
                VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
        {
            vendorFinder.Reset();
            vendorFinder.EnableMultiSelection = false;
            vendorFinder.Visible = true;
        }

        protected void OnVendorFinderMultiItemSelected(object sender, ViewEventArgs<List<Vendor>> e)
        {
            var vendors = lstVendors.Items
                .Select(c => new
                {
                    Id = c.FindControl("hidServiceTicketVendorId").ToCustomHiddenField().Value.ToLong(),
                    VendorNumber = c.FindControl("lblVendorNumber").ToLabel().Text,
                    Name = c.FindControl("litName").ToLiteral().Text,
                    VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                    InsuranceAlert = c.FindControl("hidInsuranceAlert").ToCustomHiddenField().Value.ToBoolean(),
                    InsuranceTooltip = c.FindControl("hidInsuranceTooltip").ToCustomHiddenField().Value,
                    IsRelationBetweenVendorAndVendorBill = c.FindControl("hidIsRelationBetweenVendorAndVendorBill").ToCustomHiddenField().Value.ToBoolean()
                })
                .ToList();

            var newVendors = e.Argument
                .Select(vendor => new
                {
                    Id = default(long),
                    vendor.VendorNumber,
                    vendor.Name,
                    VendorId = vendor.Id,
                    InsuranceAlert = false,
                    InsuranceTooltip = string.Empty,
                    IsRelationBetweenVendorAndVendorBill = false
                })
                .Where(v => !vendors.Select(qv => qv.VendorId).Contains(v.VendorId));

            vendors.AddRange(newVendors);

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);

            vendorFinder.Visible = false;

            var ddlNewVendors = vendors.Select(t => new ListItem { Text = t.Name, Value = t.VendorId.ToString() }).ToArray();
            lstCharges.Items.Select(c => c.FindControl("ddlVendors").ToDropDownList()).ToList().ForEach(cc => cc.Items.AddRange((ddlNewVendors.Except(cc.Items.Cast<ListItem>().ToArray())).ToArray()));
            upPnlCharges.Update();
        }

        protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
        {
            hidFlag.Value = VendorUpdateFlag;
            DisplayVendor(e.Argument);
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderItemCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }

        protected void OnDeleteVendorClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var vendors = lstVendors.Items
                .Select(c => new
                {
                    Id = c.FindControl("hidServiceTicketVendorId").ToCustomHiddenField().Value.ToLong(),
                    VendorNumber = c.FindControl("lblVendorNumber").ToLabel().Text,
                    Name = c.FindControl("litName").ToLiteral().Text,
                    VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                    InsuranceAlert = c.FindControl("hidInsuranceAlert").ToCustomHiddenField().Value.ToBoolean(),
                    InsuranceTooltip = c.FindControl("hidInsuranceTooltip").ToCustomHiddenField().Value,
                    IsRelationBetweenVendorAndVendorBill = c.FindControl("hidIsRelationBetweenVendorAndVendorBill").ToCustomHiddenField().Value.ToBoolean()
                })
                .ToList();

            vendors.RemoveAt(imageButton.Parent.FindControl("hidVendorIndex").ToCustomHiddenField().Value.ToInt());

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);
            athtuTabUpdater.SetForUpdate(tabVendors.ClientID, vendors.BuildTabCount(VendorsHeader));

            var removedVendorId = imageButton.Parent.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong();

            var ddlVendorsList = lstCharges.Items.Select(c => new {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField(),
                VendorBillNumber = c.FindControl("txtVendorBillNumber").ToTextBox()
            }).ToList();

            ddlVendorsList.ForEach(cc =>
            {
                cc.Vendors.Items.Clear();
                BuildVendorsDropdownListOnChargesTab(
                    cc.Vendors, 
                    cc.SelectedVendorId.Value.ToLong() == removedVendorId ? hidPrimaryVendorId.Value.ToLong() : cc.SelectedVendorId.Value.ToLong(),
                    string.IsNullOrEmpty(cc.VendorBillNumber.Text));
                cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == removedVendorId ? hidPrimaryVendorId.Value : cc.SelectedVendorId.Value;
            });
            upPnlCharges.Update();
        }

        protected void OnClearAllVendorsClicked(object sender, EventArgs e)
        {
            var vendors = lstVendors.Items
                .Select(c => new
                {
                    Id = c.FindControl("hidServiceTicketVendorId").ToCustomHiddenField().Value.ToLong(),
                    VendorNumber = c.FindControl("lblVendorNumber").ToLabel().Text,
                    Name = c.FindControl("litName").ToLiteral().Text,
                    VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                    InsuranceAlert = c.FindControl("hidInsuranceAlert").ToCustomHiddenField().Value.ToBoolean(),
                    InsuranceTooltip = c.FindControl("hidInsuranceTooltip").ToCustomHiddenField().Value,
                    IsRelationBetweenVendorAndVendorBill = c.FindControl("hidIsRelationBetweenVendorAndVendorBill").ToCustomHiddenField().Value.ToBoolean()
                }).Where(s => s.IsRelationBetweenVendorAndVendorBill)
                .ToList();


            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            tabVendors.HeaderText = vendors.Any() ? vendors.BuildTabCount(VendorsHeader) : VendorsHeader;
            athtuTabUpdater.SetForUpdate(tabVendors.ClientID, vendors.Any() ? vendors.BuildTabCount(VendorsHeader) : VendorsHeader);

            var ddlVendorsList = lstCharges.Items.Select(c => new {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField(),
                VendorBillNumber = c.FindControl("txtVendorBillNumber").ToTextBox()
            }).ToList();

            ddlVendorsList.ForEach(cc =>
            {
                if (string.IsNullOrEmpty(cc.VendorBillNumber.Text))
                {
                    cc.Vendors.Items.Clear();
                    BuildVendorsDropdownListOnChargesTab(
                        cc.Vendors,
                        0,
                        string.IsNullOrEmpty(cc.VendorBillNumber.Text));
                    cc.SelectedVendorId.Value = "0";
                }
            });
            upPnlCharges.Update();
        }

        protected void OnAddVendorsToChargesList(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;

            var ddlVendors = item.FindControl("ddlVendors").ToDropDownList();
            var selectedValue = item.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value.ToLong();
            var isEnabled = string.IsNullOrEmpty(item.FindControl("txtVendorBillNumber").ToTextBox().Text);

            BuildVendorsDropdownListOnChargesTab(ddlVendors, selectedValue, isEnabled);
        }

        private void BuildVendorsDropdownListOnChargesTab(DropDownList ddlVendors, long selectedValue, bool isEnabled)
        {
            var vendorCollection = VendorCollection;

            if (!vendorCollection.Select(s => s.Value.ToLong()).Contains(selectedValue))
            {
                var vendor = new Vendor(selectedValue);
                vendorCollection.Add(new ViewListItem() { Text = vendor.Name, Value = vendor.Id.ToString() });
            }

            ddlVendors.DataSource = vendorCollection;
            ddlVendors.SelectedValue = selectedValue == 0 ? (ddlVendors.SelectedValue.ToLong() == 0 ? hidPrimaryVendorId.Value : ddlVendors.SelectedValue) : selectedValue.ToString();
            ddlVendors.Enabled = isEnabled;
            ddlVendors.DataBind();
            upPnlCharges.Update();
        }

        protected void OnClearResellerAdditionDetailsClicked(object sender, EventArgs e)
        {
            DisplayResellerAddition(null, false);
        }

        protected void OnRetrieveResellerAdditionDetailsClicked(object sender, EventArgs e)
        {
            var customer = new Customer(hidCustomerId.Value.ToLong());
            var rating = customer.Rating;

            DisplayResellerAddition(rating != null ? rating.ResellerAddition : null, rating != null && rating.BillReseller);
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 22;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }


            // ensure all lines have a valid line type
            var invalidLineTypes = chks.Where(s => !_validImportLineTypes.Contains(s.Line[0])).ToList();
            msgs.AddRange(invalidLineTypes.Select(invalidLineType => ValidationMessage.Error("Line {0} has an invalid Line Type", invalidLineType.Index)));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;

            }

            // ensure each line has a group id
            var invalidGroupIds = chks.Where(l => l.Line[1] == string.Empty).ToList();
            msgs.AddRange(invalidGroupIds.Select(invalidGroupId => ValidationMessage.Error("Line {0} is missing a Group Id", invalidGroupId.Index)));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;

            }


            // ensure each non-header line has an assosciated header line
            msgs.AddRange(from line in chks.Where(s => s.Line[0] != HeaderImportLineType)
                          where !chks.Any(s => s.Line[0] == HeaderImportLineType && s.Line[1] == line.Line[1])
                          select ValidationMessage.Error("{0} on Line {1} has a Group Id that does not contain a header line", line.Line[0], line.Index));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;

            }

            // ensure each header line has at least one charge and item
            foreach (var line in chks.Where(s => s.Line[0] == HeaderImportLineType))
            {
                if (!chks.Any(s => s.Line[0] == ChargeImportLineType && s.Line[1] == line.Line[1]))
                {
                    msgs.Add(ValidationMessage.Error("Header on Line {0} has no assosciated charges", line.Index));
                }

                if (!chks.Any(s => s.Line[0] == ItemImportLineType && s.Line[1] == line.Line[1]))
                {
                    msgs.Add(ValidationMessage.Error("Header on Line {0} has no assosciated items", line.Index));

                }
            }
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;

            }

            // validate primary vendor numbers
            var vendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), ActiveUser.TenantId);
            var vendorNumbers = vendors.Select(c => c.VendorNumber).ToList();
            msgs.AddRange(chks.Where(c => c.Line[0] == HeaderImportLineType).CodesAreValid(2, vendorNumbers, new Vendor().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            // validate customer number
            var customers = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), ActiveUser.TenantId);
            var customerNumbers = customers.Select(c => c.CustomerNumber).ToList();
            msgs.AddRange(chks.Where(c => c.Line[0] == HeaderImportLineType).CodesAreValid(3, customerNumbers, new Customer().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }


            // validate note types
            var noteTypes = ProcessorUtilities.GetAll<NoteType>().Keys.Select(type => type.GetString()).ToList();
            msgs.AddRange(chks
                .Where(i => !noteTypes.Contains(i.Line[15]) && i.Line[0] == NoteImportLineType)
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedEnumErrMsg, typeof(NoteType).Name.FormattedString(), i.Index))
                .ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            // validate service codes
            var servicesChk = new ServiceSearch().FetchAllServices(ActiveUser.TenantId);
            var serviceCodes = servicesChk.Select(c => c.Code).ToList();
            msgs.AddRange(chks.Where(c => c.Line[0] == ServiceImportLineType).CodesAreValid(19, serviceCodes, new Service().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            // validate equipment codes
            var equipmentTypes = new EquipmentTypeSearch().FetchEquipmentTypes(new List<ParameterColumn>(), ActiveUser.TenantId);
            var equipmentCodes = equipmentTypes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.Where(c => c.Line[0] == EquipmentImportLineType).CodesAreValid(20, equipmentCodes, new EquipmentType().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            // generate the list of service tickets and their grouping id
            var serviceTickets = lines.Where(s => s[0] == HeaderImportLineType)
                                      .Select(s => new
                                      {
                                          GroupId = s[1],
                                          ServiceTicket = new ServiceTicket
                                          {
                                              Vendors = new List<ServiceTicketVendor>
                                                          {
                                                              new ServiceTicketVendor
                                                                  {
                                                                      Primary = true,
                                                                      VendorId =
                                                                          new VendorSearch().FetchVendorByNumber(s[2],ActiveUser.TenantId).Id,
                                                                      TenantId = ActiveUser.TenantId,
                                                                  }
                                                          },
                                              Customer = new CustomerSearch().FetchCustomerByNumber(s[3], ActiveUser.TenantId),
                                              ExternalReference1 = s[4],
                                              ExternalReference2 = s[5],
                                              TicketDate = s[6].ToDateTime(),
                                              Documents = new List<ServiceTicketDocument>(),
                                              Assets = new List<ServiceTicketAsset>(),
                                              TenantId = ActiveUser.TenantId,
                                              User = ActiveUser,
                                              DateCreated = DateTime.Now,
                                              AuditedForInvoicing = s[21].ToBoolean()
                                          }
                                      }).ToList();


            // now populate each service ticket with assosciated charges, items, notes, services, and equipments
            foreach (var serviceTicket in serviceTickets)
            {

                var items = lines.Where(s => s[0] == ItemImportLineType && s[1] == serviceTicket.GroupId)
                                 .Select(s =>
                                         new ServiceTicketItem
                                         {
                                             ServiceTicket = serviceTicket.ServiceTicket,
                                             Description = s[7],
                                             Comment = s[8],
                                             TenantId = ActiveUser.TenantId
                                         }).ToList();

                var charges = lines.Where(s => s[0] == ChargeImportLineType && s[1] == serviceTicket.GroupId)
                                   .Select(s => new ServiceTicketCharge
                                   {
                                       ServiceTicket = serviceTicket.ServiceTicket,
                                       ChargeCodeId =
                                               new ChargeCodeSearch().FetchChargeCodeIdByCode(s[9], ActiveUser.TenantId),
                                       Comment = s[10],
                                       Quantity = s[11].ToInt(),
                                       UnitBuy = s[12].ToDecimal(),
                                       UnitSell = s[13].ToDecimal(),
                                       UnitDiscount = s[14].ToDecimal(),
                                       TenantId = ActiveUser.TenantId
                                   }).ToList();

                var notes = lines.Where(s => s[0] == NoteImportLineType && s[1] == serviceTicket.GroupId)
                                .Select(s => new ServiceTicketNote
                                {
                                    ServiceTicket = serviceTicket.ServiceTicket,
                                    Type = s[15].ToEnum<NoteType>(),
                                    Archived = s[16].ToBoolean(),
                                    Classified = s[17].ToBoolean(),
                                    Message = s[18],
                                    TenantId = ActiveUser.TenantId,
                                    User = ActiveUser
                                }).ToList();

                var services = lines.Where(s => s[0] == ServiceImportLineType && s[1] == serviceTicket.GroupId)
                                .Select(s => new ServiceTicketService
                                {
                                    TenantId = ActiveUser.TenantId,
                                    ServiceId = new ServiceSearch().FetchServiceIdByCode(s[19], ActiveUser.TenantId),
                                    ServiceTicket = serviceTicket.ServiceTicket
                                }).ToList();

                var equipment = lines.Where(s => s[0] == EquipmentImportLineType && s[1] == serviceTicket.GroupId)
                                .Select(s => new ServiceTicketEquipment
                                {
                                    TenantId = ActiveUser.TenantId,
                                    EquipmentType = new EquipmentTypeSearch().FetchEquipmentTypeByCode(s[20], ActiveUser.TenantId),
                                    ServiceTicket = serviceTicket.ServiceTicket
                                }).ToList();

                serviceTicket.ServiceTicket.Items = items;
                serviceTicket.ServiceTicket.Charges = charges;
                serviceTicket.ServiceTicket.Notes = notes;

                var salesRepresentative = serviceTicket.ServiceTicket.Customer.SalesRepresentative;
                serviceTicket.ServiceTicket.SalesRepresentative = salesRepresentative;
                serviceTicket.ServiceTicket.SalesRepAddlEntityName = salesRepresentative != null
                                                                         ? salesRepresentative.AdditionalEntityName
                                                                         : string.Empty;
                serviceTicket.ServiceTicket.AccountBucket = serviceTicket.ServiceTicket.Customer.DefaultAccountBucket;
                serviceTicket.ServiceTicket.Prefix = serviceTicket.ServiceTicket.Customer.Prefix;
                serviceTicket.ServiceTicket.Vendors.First().ServiceTicket = serviceTicket.ServiceTicket;
                serviceTicket.ServiceTicket.Services = services;
                serviceTicket.ServiceTicket.Equipments = equipment;
            }

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<ServiceTicket>>(serviceTickets.Select(s => s.ServiceTicket).ToList()));
            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }
		
        protected void OnFindJobClicked(object sender, EventArgs e)
        {
            jobFinder.Visible = true;
        }

        protected void OnClearJobClicked(object sender, EventArgs e)
        {
            hidJobId.Value = string.Empty;
            txtJobNumber.Text = string.Empty;
            txtJobStep.Text = string.Empty;
        }

        protected void OnJobNumberTextChanged(object sender, EventArgs e)
        {
            var jobNumber = txtJobNumber.Text;

            var job = new JobSearch().FetchJobByJobNumber(jobNumber, ActiveUser.TenantId);

            if (job == null)
            {
                hidJobId.Value = string.Empty;

                txtJobNumber.Text = string.Empty;
                txtJobStep.Text = string.Empty;
                return;
            }

            hidJobId.Value = job.Id.ToString();
            txtJobStep.Text = string.Empty;
        }

        protected void OnJobFinderItemSelected(object sender, ViewEventArgs<Job> e)
        {
            var job = e.Argument;

            hidJobId.Value = job.Id.ToString();

            txtJobNumber.Text = job.JobNumber;
            txtJobStep.Text = string.Empty;

            jobFinder.Visible = false;
        }

        protected void OnJobFinderItemCancelled(object sender, EventArgs e)
        {
            jobFinder.Visible = false;
        }

    }
}
