﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ClaimDashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.ClaimDashboardView"
    EnableEventValidation="false" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageHeader">
        <h3>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                    Claim Dashboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
                </ContentTemplate>
            </asp:UpdatePanel>
        </h3>
    </div>
    <span class="clearfix"></span>
    <hr class="dark mb10" />
    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
        <div class="rowgroup mb10">
            <div class="row">
                <div class="fieldgroup">
                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                        CausesValidation="False" CssClass="add" />
                </div>
                <div class="right">
                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Claims" ShowAutoRefresh="True"
                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected"
                        OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" />
                </div>
            </div>
        </div>
        <hr class="dark mb10" />
        <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
            OnItemDataBound="OnParameterItemDataBound">
            <LayoutTemplate>
                <table class="mb10">
                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <eShip:ReportRunParameterControl ID="reportRunParameter" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                    OnRemoveParameter="OnParameterRemove" />
            </ItemTemplate>
        </asp:ListView>
        <hr class="fat" />
        <div class="rowgroup">
            <div class="row mb10">
                <div class="fieldgroup">
                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                        CausesValidation="False" />
                </div>
                <div class="right">
                    <div class="fieldgroup">
                        <label>Sort By:</label>
                        <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                            OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                    </div>
                    <div class="fieldgroup mr10">
                        <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                            ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                    </div>
                    <div class="fieldgroup">
                        <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                            ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheViewRegistryTable" TableId="claimDashboardTable" HeaderZIndex="2" />
            <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
            <div class="rowgroup">
                <table id="claimDashboardTable" class="line2 pl2">
                    <tr>
                        <th style="width: 11%;">
                            <asp:LinkButton runat="server" ID="lbtnSortClaimNumber" CssClass="link_nounderline blue"
                                CausesValidation="False" OnCommand="OnSortData">
								 Claim #
                            </asp:LinkButton>
                        </th>
                        <th style="width: 14%;">
                            <asp:LinkButton runat="server" ID="lbtnSortStatusText" CssClass="link_nounderline blue" CausesValidation="False"
                                OnCommand="OnSortData">
								 Status
                            </asp:LinkButton>
                        </th>
                        <th style="width: 29%;">Customer 
                            <asp:LinkButton runat="server" ID="lbtnSortCustomerNumber" CssClass="link_nounderline blue"
                                CausesValidation="False" OnCommand="OnSortData">
								     Number
                            </asp:LinkButton>\
                            <asp:LinkButton runat="server" ID="lbtnSortCustomerName" CssClass="link_nounderline blue"
                                CausesValidation="False" OnCommand="OnSortData">
								     Name
                            </asp:LinkButton>
                        </th>
                        <th style="width: 15%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDateCreated" CssClass="link_nounderline blue"
                                CausesValidation="False" OnCommand="OnSortData">
								 Date Created
                            </asp:LinkButton>
                        </th>
                        <th style="width: 24%;">
                            <asp:LinkButton runat="server" ID="lbtnSortUsername" CssClass="link_nounderline blue"
                                CausesValidation="False" OnCommand="OnSortData">
								 Created By
                            </asp:LinkButton>
                        </th>
                        <th style="width: 7%;" class="text-center">Action</th>
                    </tr>
                    <asp:ListView runat="server" ID="lstClaimDetails" ItemPlaceholderID="itemPlaceHolder"
                        OnItemDataBound="OnClaimDetailsItemDataBound">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <eShip:ClaimsDashboardDetailControl ID="claimsDashboardDetail" ItemIndex='<%# Container.DataItemIndex %>' runat="server" />
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnSortClaimNumber" />
            <asp:PostBackTrigger ControlID="lbtnSortDateCreated" />
            <asp:PostBackTrigger ControlID="lbtnSortCustomerNumber" />
            <asp:PostBackTrigger ControlID="lbtnSortCustomerName" />
            <asp:PostBackTrigger ControlID="lbtnSortUsername" />
            <asp:PostBackTrigger ControlID="lbtnSortStatusText" />
        </Triggers>
    </asp:UpdatePanel>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>

