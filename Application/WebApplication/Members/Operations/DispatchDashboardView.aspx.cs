﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.InputControls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using EquipmentType = LogisticsPlus.Eship.Core.Registry.EquipmentType;
using Shipment = LogisticsPlus.Eship.Core.Operations.Shipment;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
	public partial class DispatchDashboardView : MemberPageBaseWithPageStore, IDispatchDashboardView
	{
		private const string HiddenCssClass = " hidden";
		private const int ViewDeliveryStopOrder = 10000;

		protected const string DestinationStopText = "Destination";
		protected const string OriginStopText = "Origin";

		protected const string ModifyingCharges = "MC";
		protected const string ModifyingDates = "MD";
		protected const string ModifyingEquipment = "ME";
		protected const string ModifyingItems = "MI";
		protected const string ModifyingLoadOrderDescription = "MLOD";
		protected const string ModifyingLoadOrderStatus = "MLOS";
		protected const string ModifyingLocation = "ML";
		protected const string ModifyingVendor = "MV";
		protected const string ManageAssetOnDatLoadboard = "MAODL";
		protected const string ModifyingLoadOrderDatPosting = "MLODP";
		
		public override ViewCode PageCode
		{
			get { return ViewCode.DispatchDashboard; }
		}

		public override string SetPageIconImage
		{
			set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
		}

		public static string PageAddress
		{
			get { return "~/Members/Operations/DispatchDashboardView.aspx"; }
		}

		// when calling LockShipment or LockLoadOrder events if acquiring lock fails, this is set to true
		public bool LockFailed { get; set; }

		public LoadOrder LoadOrderBeingEdited { get; set; }

		public Shipment ShipmentBeingEdited { get; set; }

		public List<EquipmentType> EquipmentTypes
		{
			set
			{
				var equipmentTypes = value
					.OrderBy(e => e.TypeName)
					.Select(c => new ViewListItem("  " + c.FormattedString(), c.Id.ToString()))
					.ToList();

				rptEquipmentTypes.DataSource = equipmentTypes;
				rptEquipmentTypes.DataBind();
			}
		}

		public Dictionary<int, string> LoadOrderStatuses
		{
			set
			{
				ddlLoadOrderStatus.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
				ddlLoadOrderStatus.DataBind();
			}
		}


		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<LoadOrder>> SaveLoadOrder;
		public event EventHandler<ViewEventArgs<Shipment>> SaveShipment;
		public event EventHandler<ViewEventArgs<LoadOrder>> LockLoadOrder;
		public event EventHandler<ViewEventArgs<LoadOrder>> UnLockLoadOrder;
		public event EventHandler<ViewEventArgs<Shipment>> LockShipment;
		public event EventHandler<ViewEventArgs<Shipment>> UnLockShipment;
		public event EventHandler<ViewEventArgs<string>> VendorSearch;
		

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			foreach (var message in messages.Where(message => message.Type == ValidateMessageType.Warning))
				message.Message = string.Format("Warning: {0}", message.Message);
			var msg = string.Join(WebApplicationConstants.HtmlBreak,
			                      messages.OrderBy(m => m.Type)
			                              .Where(
				                              m => m.Type == ValidateMessageType.Error || m.Type == ValidateMessageType.Warning)
			                              .Select(m => m.Message)
			                              .ToArray());

			switch (hidFlag.Value)
			{
				case ModifyingLocation:
					litLocationErrorMessages.Text = msg;
					break;
				case ModifyingDates:
					litDatesErrorMessages.Text = msg;
					break;
				case ModifyingItems:
					litItemsErrorMessages.Text = msg;
					break;
				case ModifyingCharges:
					litChargesErrorMessages.Text = msg;
					break;
				case ModifyingVendor:
					litVendorErrorMessages.Text = msg;
					break;
				case ModifyingEquipment:
					litEquipmentErrorMessages.Text = msg;
					break;
				case ModifyingLoadOrderDescription:
					litLoadOrderDescriptionErrorMessages.Text = msg;
					break;
				case ModifyingLoadOrderStatus:
					litLoadOrderStatusErrorMessages.Text = msg;
					break;
			}

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak,
											 messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Button = MessageButton.Ok;

			var scriptManager = ScriptManager.GetCurrent(Page);
			if (scriptManager != null && IsPostBack && scriptManager.IsInAsyncPostBack)
				// disregard showing if in partial post back as message box is not in update panel
				messageBox.Visible = false;
			else
				messageBox.Visible = true;
		}

		public void DisplayVendor(Vendor vendor, bool checkVendor = true)
		{
			if (checkVendor)
			{
				if (vendor != null && ProcessorVars.VendorInsuranceAlertThresholdDays.ContainsKey(vendor.TenantId) &&
				    vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId]))
				{
					DisplayMessages(new[]
						{
							ValidationMessage.Error(string.Format("{0} - {1} Cannot be primary. Reason: {2}",
							                                      vendor.VendorNumber, vendor.Name,
							                                      vendor.VendorInsuranceAlertText(
								                                      ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId])))
						});
					vendor = null;
				}

				if (vendor != null) CheckVendorConstraints(vendor);
			}

			txtVendorName.Text = vendor == null ? string.Empty : vendor.Name;
			txtVendorNumber.Text = vendor == null ? string.Empty : vendor.VendorNumber;
			hidPrimaryVendorId.Value = vendor == null ? default(long).ToString() : vendor.Id.ToString();
		}

		public void Set(Shipment shipment)
		{
			hidGoToCurrentScrollPosition.Value = true.ToString();
			hidEditingRecordPanelClientId.Value = string.Empty;
			if (!pnlModifyLocation.CssClass.Contains(HiddenCssClass)) pnlModifyLocation.CssClass += HiddenCssClass;
			if (!pnlModifyItems.CssClass.Contains(HiddenCssClass)) pnlModifyItems.CssClass += HiddenCssClass;
			if (!pnlModifyCharges.CssClass.Contains(HiddenCssClass)) pnlModifyCharges.CssClass += HiddenCssClass;
			pnlModifyDates.Visible = false;
			pnlModifyVendor.Visible = false;
			pnlModifyEquipment.Visible = false;
			pnlDimScreen.Visible = false;

			if (UnLockShipment != null)
				UnLockShipment(this, new ViewEventArgs<Shipment>(shipment));
		}

		public void Set(LoadOrder loadOrder)
		{
			hidGoToCurrentScrollPosition.Value = true.ToString();
			hidEditingRecordPanelClientId.Value = string.Empty;
			if (!pnlModifyLocation.CssClass.Contains(HiddenCssClass)) pnlModifyLocation.CssClass += HiddenCssClass;
			if (!pnlModifyItems.CssClass.Contains(HiddenCssClass)) pnlModifyItems.CssClass += HiddenCssClass;
			if (!pnlModifyCharges.CssClass.Contains(HiddenCssClass)) pnlModifyCharges.CssClass += HiddenCssClass;
			pnlModifyDates.Visible = false;
			pnlModifyVendor.Visible = false;
			pnlModifyEquipment.Visible = false;
			pnlModifyLoadOrderDescription.Visible = false;
			pnlModifyLoadOrderStatus.Visible = false;
		    pnlModifyLoadOrderDatPosting.Visible = false;
			pnlDimScreen.Visible = false;
			if (UnLockLoadOrder != null)
				UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

			// if load order is one of these statuses the search should rerun to take it off the view
			if (loadOrder.Status == LoadOrderStatus.Shipment || loadOrder.Status == LoadOrderStatus.Lost ||
			    loadOrder.Status == LoadOrderStatus.Cancelled)
				DoSearch();
		}

		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		private void CheckVendorConstraints(Vendor vendor)
		{
			var messages = new List<ValidationMessage>();
			//Service Modes
			Shipment shipment = null;
			LoadOrder loadOrder = null;

			if (hidEditShipmentId.Value.ToLong() != default(long)) shipment = new Shipment(hidEditShipmentId.Value.ToLong());
			if (hidEditLoadOrderId.Value.ToLong() != default(long)) loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong());

			if (shipment == null && loadOrder == null) return;

			var mode = shipment != null
				           ? shipment.ServiceMode
				           : loadOrder.ServiceMode;
			var isPartialTruckload = shipment != null ? shipment.IsPartialTruckload : loadOrder.IsPartialTruckload;

			switch (mode)
			{
				case ServiceMode.LessThanTruckload:
					if (!vendor.HandlesLessThanTruckload)
						messages.Add(
							ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
							                                        ServiceMode.LessThanTruckload.FormattedString())));
					break;
				case ServiceMode.Truckload:
					if (!vendor.HandlesTruckload)
						messages.Add(
							ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
							                                        ServiceMode.Truckload.FormattedString())));
					if (isPartialTruckload && !vendor.HandlesPartialTruckload)
						messages.Add(
							ValidationMessage.Warning(string.Format("{0} does not handle Partial Truckload shipments", vendor.Name)));
					break;
				case ServiceMode.Air:
					if (!vendor.HandlesAir)
						messages.Add(
							ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
							                                        ServiceMode.Air.FormattedString())));
					break;
				case ServiceMode.Rail:
					if (!vendor.HandlesRail)
						messages.Add(
							ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
							                                        ServiceMode.Rail.FormattedString())));
					break;
				case ServiceMode.SmallPackage:
					if (!vendor.HandlesSmallPack)
						messages.Add(
							ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
							                                        ServiceMode.SmallPackage.FormattedString())));
					break;
			}

			//Services
			var services = shipment != null
				               ? shipment.Services.Select(s => s.Service)
				               : loadOrder.Services.Select(s => s.Service);

			var vendorServices = vendor.Services.Select(s => s.ServiceId).ToList();
			messages.AddRange(from service in services
			                  where !vendorServices.Contains(service.Id)
			                  select
				                  ValidationMessage.Warning(string.Format("{0} does not provide the service: {1}", vendor.Name,
				                                                          service.Description)));

			//Equipment
			var equipmentTypes = shipment != null
				                     ? shipment.Equipments.Select(s => s.EquipmentType)
				                     : loadOrder.Equipments.Select(s => s.EquipmentType);
			var vendorEquipment = vendor.Equipments.Select((e => e.EquipmentTypeId)).ToList();
			messages.AddRange(from equipment in equipmentTypes
			                  where !vendorEquipment.Contains(equipment.Id)
			                  select
				                  ValidationMessage.Warning(string.Format("{0} does not provide equipment type: {1}", vendor.Name,
				                                                          equipment.FormattedString())));

			if (messages.Any()) DisplayMessages(messages);
		}

		private void DoSearch()
		{
			hidSerializedParameters.Value = new ShipmentViewSearchCriteria
				{
					Parameters = GetCurrentRunParameters(false),
					ActiveUserId = ActiveUser.Id,
				}.ToXml().ToUtf8Bytes().ToBase64String();
			hidTenantUser.Value = ActiveUser.TenantId.GetString().ToUtf8Bytes().ToBase64String();
		}

		private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
		{
			return new UserSearchProfileControl.ProfileItems {Columns = GetCurrentRunParameters(true)};
		}

		private void ProcessPartialPostbacks()
		{
			var requestEventTarget = Request.Params.Get(WebApplicationConstants.RequestEventTargetIndex);

			if (requestEventTarget == udpModifyItems.ClientID)
			{
				if (hidEditShipmentId.Value.ToLong() != default(long))
					DisplayModifyShipmentItemsPopup(hidEditShipmentId.Value.ToLong());
				else if (hidEditLoadOrderId.Value.ToLong() != default(long))
					DisplayModifyLoadOrderItemsPopup(hidEditLoadOrderId.Value.ToLong());
			}
			else if (requestEventTarget == udpModifyCharges.ClientID)
			{
				if (hidEditShipmentId.Value.ToLong() != default(long))
					DisplayModifyShipmentChargesPopup(hidEditShipmentId.Value.ToLong());
				else if (hidEditLoadOrderId.Value.ToLong() != default(long))
					DisplayModifyLoadOrderChargesPopup(hidEditLoadOrderId.Value.ToLong());
			}
			else if (requestEventTarget == udpModifyEquipment.ClientID)
			{
				if (hidEditShipmentId.Value.ToLong() != default(long))
					DisplayModifyShipmentEquipmentPopup(hidEditShipmentId.Value.ToLong());
				else if (hidEditLoadOrderId.Value.ToLong() != default(long))
					DisplayModifyLoadOrderEquipmentPopup(hidEditLoadOrderId.Value.ToLong());
			}
			else if (requestEventTarget == udpModifyVendor.ClientID)
			{
				if (hidEditShipmentId.Value.ToLong() != default(long))
					DisplayModifyShipmentPrimaryVendorPopup(hidEditShipmentId.Value.ToLong());
				else if (hidEditLoadOrderId.Value.ToLong() != default(long))
					DisplayModifyLoadOrderPrimaryVendorPopup(hidEditLoadOrderId.Value.ToLong());
			}
			else if (requestEventTarget == udpModifyDates.ClientID)
			{
				if (hidEditShipmentId.Value.ToLong() != default(long))
					DisplayModifyShipmentDatesPopup(hidEditShipmentId.Value.ToLong());
				else if (hidEditLoadOrderId.Value.ToLong() != default(long))
					DisplayModifyLoadOrderDatesPopup(hidEditLoadOrderId.Value.ToLong());
			}
			else if (requestEventTarget == udpModifyLoadOrderStatus.ClientID)
			{
				if (hidEditLoadOrderId.Value.ToLong() != default(long))
					DisplayModifyLoadOrderStatusPopup(hidEditLoadOrderId.Value.ToLong());
			}
			else if (requestEventTarget == udpModifyLoadOrderDescription.ClientID)
			{
				if (hidEditLoadOrderId.Value.ToLong() != default(long))
					DisplayModifyLoadOrderDescriptionPopup(hidEditLoadOrderId.Value.ToLong());
			}
			else if (requestEventTarget == udpModifyLocation.ClientID)
			{
				var requestEventArgument = Request.Params.Get(WebApplicationConstants.RequestEventArgumentIndex);
				if (hidEditShipmentId.Value.ToLong() != default(long))
				{
					switch (requestEventArgument)
					{
						case OriginStopText:
							DisplayModifyShipmentOriginPopup(hidEditShipmentId.Value.ToLong());
							break;
						case DestinationStopText:
							DisplayModifyShipmentDestinationPopup(hidEditShipmentId.Value.ToLong());
							break;
					}
				}
				else if (hidEditLoadOrderId.Value.ToLong() != default(long))
				{
					switch (requestEventArgument)
					{
						case OriginStopText:
							DisplayModifyLoadOrderOriginPopup(hidEditLoadOrderId.Value.ToLong());
							break;
						case DestinationStopText:
							DisplayModifyLoadOrderDestinationPopup(hidEditLoadOrderId.Value.ToLong());
							break;
					}
				}
			}
			else if (requestEventTarget == udpManageAssetsOnLoadboard.ClientID)
			{
				DisplayManageLoadOrderOnDatLoadboardClicked(hidEditLoadOrderId.Value.ToLong());
			}
			
		}

        private void RemoveLoadOrderHazmatFlags(LoadOrder loadOrder)
        {
            var hazMatServiceId = ActiveUser.Tenant.HazardousMaterialServiceId;

            if (hazMatServiceId == default(long)) return;

            loadOrder.Services = loadOrder.Services.Where(s => s.ServiceId != hazMatServiceId).ToList();
            loadOrder.HazardousMaterial = false;
            loadOrder.HazardousMaterialContactEmail = string.Empty;
            loadOrder.HazardousMaterialContactMobile = string.Empty;
            loadOrder.HazardousMaterialContactName = string.Empty;
            loadOrder.HazardousMaterialContactPhone = string.Empty;
        }

        private void RemoveShipmentHazmatFlags(Shipment shipment)
        {
            var hazMatServiceId = ActiveUser.Tenant.HazardousMaterialServiceId;

            if (hazMatServiceId == default(long)) return;

            shipment.Services = shipment.Services.Where(s => s.ServiceId != hazMatServiceId).ToList();
            shipment.HazardousMaterial = false;
            shipment.HazardousMaterialContactEmail = string.Empty;
            shipment.HazardousMaterialContactMobile = string.Empty;
            shipment.HazardousMaterialContactName = string.Empty;
            shipment.HazardousMaterialContactPhone = string.Empty;
        }


		[WebMethod]
		[ScriptMethod]
		public static object GetData(string p, string tenant)
		{
			var criteria = p.FromBase64String().FromUtf8Bytes().FromXml<ShipmentViewSearchCriteria>();
			var tenantId = tenant.FromBase64String().FromUtf8Bytes().ToLong();
			var postings = new DatLoadboardAssetPostingSearch().FetchAllDatLoadboardAssetPostings(tenantId);

			return new ShipmentSearch()
				.FetchLoadsShipmentsDashboardDtoForDispatch(criteria, tenantId)
				.Select(s =>
				{
					var posting = postings.FirstOrDefault(ap => ap.IdNumber == s.ShipmentNumber);
					
					return new
					{
						s.ShipmentNumber,
						s.ShipmentStatus,
						s.StatusText,
						s.OriginStreet2,
						s.OriginStreet1,
						s.OriginState,
						s.OriginPostalCode,
						s.OriginName,
						s.OriginCountryName,
						s.OriginCity,
						s.Id,
						EstimatedDeliveryDate = s.EstimatedDeliveryDate.FormattedShortDateSuppressEarliestDate(),
						s.Equipments,
						s.DestinationStreet2,
						s.DestinationStreet1,
						s.DestinationState,
						s.DestinationPostalCode,
						s.DestinationName,
						s.DestinationCountryName,
						s.DestinationCity,
						DesiredPickupDate = s.DesiredPickupDate.FormattedShortDateSuppressEarliestDate(),
						s.Description,
						DateCreated = s.DateCreated.FormattedShortDateSuppressEarliestDate(),
						s.CustomerNumber,
						s.CustomerName,
						s.CustomerId,
						CustomerIdEncrypted = s.CustomerId.GetString().UrlTextEncrypt(),
						ActualPickupDate = s.ActualPickupDate.FormattedShortDateSuppressEarliestDate(),
						ActualDeliveryDate = s.ActualDeliveryDate.FormattedShortDateSuppressEarliestDate(),
						s.VendorProNumber,
						s.VendorNumber,
						s.VendorName,
						s.VendorId,
						VendorIdEncrypted = s.VendorId.GetString().UrlTextEncrypt(),
						Type = s.Type.ToString(),
						s.TotalWeight,
						TotalWeightAsString = s.TotalWeight.ToString("n2"),
						s.TotalAmountDue,
						TotalAmountDueAsString = s.TotalAmountDue.ToString("n2"),
						Mileage = s.Mileage.ToString("n2"),
						HasDatPosting = posting != null,
						DatPostingIsExpired = posting != null && posting.ExpirationDate < DateTime.Now
					};
				})
				.ToList();
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			new DispatchDashboardHandler(this).Initialize();
			
			searchProfiles.GetProfileItems = GetSearchProfileItems;

			ProcessPartialPostbacks();

			if (IsPostBack) return;

			hidRecordTypeShowing.Value = DashboardDtoType.LoadOrder.ToString();

			if (Loading != null)
				Loading(this, new EventArgs());

			aceVendor.SetupExtender(new Dictionary<string, string>
				{
					{AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()}
				});

			//get criteria from Session
			var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
			var searchCriteria = new ShipmentViewSearchCriteria
				{
					ActiveUserId = ActiveUser.Id,
					Parameters = profile != null
						             ? profile.Columns
						             : OperationsSearchFields.Default.Select(f => f.ToParameterColumn()).ToList(),
					SortAscending = profile == null || profile.SortAscending,
					SortBy = profile == null ? null : profile.SortBy,
					DisableNoRecordNotification = true
				};

			lstFilterParameters.DataSource = searchCriteria.Parameters;
			lstFilterParameters.DataBind();
		}



		protected void OnSearchClicked(object sender, EventArgs e)
		{
			hidAreFiltersShowing.Value = false.ToString();
			DoSearch();
		}

		protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType != ListViewItemType.DataItem) return;

			var item = (ListViewDataItem) e.Item;
			var parameter = (ParameterColumn) item.DataItem;

			if (parameter == null) return;

			var field = OperationsSearchFields.Shipments.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

			if (field == null) return;

			var detailControl = (ReportRunParameterControl) item.FindControl("reportRunParameterControl");

			detailControl.LoadParameter(parameter, field.DataType, true);
		}

		protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
		{
			var parameters = GetCurrentRunParameters(true);

			parameters.RemoveAt(e.Argument);

			lstFilterParameters.DataSource = parameters;
			lstFilterParameters.DataBind();

			hidSerializedParameters.Value = string.Empty;
		}

		private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
		{
			var currentRunParameters = lstFilterParameters.Items
			                                              .Select(
				                                              i =>
				                                              ((ReportRunParameterControl)
				                                               i.FindControl("reportRunParameterControl")).RetrieveParameter(
					                                               preserveKeywordDate)).ToList();
			return currentRunParameters;
		}

		protected void OnParameterAddClicked(object sender, EventArgs e)
		{
			hidSerializedParameters.Value = string.Empty;
			parameterSelector.LoadParameterSelector(OperationsSearchFields.Shipments);
			parameterSelector.Visible = true;
		}

		protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			var currentRunParameters = GetCurrentRunParameters(true);
			currentRunParameters.AddRange(e.Argument);

			lstFilterParameters.DataSource = currentRunParameters;
			lstFilterParameters.DataBind();

			parameterSelector.Visible = false;
		}

		protected void OnParameterSelectorClose(object sender, EventArgs e)
		{
			parameterSelector.Visible = false;
		}



		protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
		{
			DisplayMessages(new[] {ValidationMessage.Error(e.Argument)});
		}

		protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
		{
			lstFilterParameters.DataSource = e.Argument.Columns;
			lstFilterParameters.DataBind();
		}



		protected void DisplayModifyShipmentDatesPopup(long shipmentId)
		{
			var shipment = new Shipment(shipmentId);

			pnlModifyDates.Visible = true;
			pnlDimScreen.Visible = true;
			trActualPickupDate.Visible = true;

			litModifyDatesTitle.Text = string.Format("Update Dates For Shipment {0}", shipment.ShipmentNumber);
			litDatesErrorMessages.Text = string.Empty;


			hidFlag.Value = ModifyingDates;
			hidEditShipmentId.Value = shipment.Id.ToString();
			hidEditLoadOrderId.Value = string.Empty;

			txtActualPickupDate.Text = shipment.ActualPickupDate.FormattedShortDateSuppressEarliestDate();
			txtDesiredPickupDate.Text = shipment.DesiredPickupDate.FormattedShortDateSuppressEarliestDate();
			txtEstimatedDeliveryDate.Text = shipment.EstimatedDeliveryDate.FormattedShortDateSuppressEarliestDate();

			if (LockShipment != null)
				LockShipment(this, new ViewEventArgs<Shipment>(shipment));

			pnlModifyDatesBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyDates.Update();
		}

		protected void DisplayModifyLoadOrderDatesPopup(long loadOrderId)
		{
			var loadOrder = new LoadOrder(loadOrderId);

			pnlModifyDates.Visible = true;
			pnlDimScreen.Visible = true;
			trActualPickupDate.Visible = false;

			litModifyDatesTitle.Text = string.Format("Update Dates For Load Order {0}", loadOrder.LoadOrderNumber);
			litDatesErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingDates;
			hidEditShipmentId.Value = string.Empty;
			hidEditLoadOrderId.Value = loadOrder.Id.ToString();

			txtActualPickupDate.Text = string.Empty;
			txtDesiredPickupDate.Text = loadOrder.DesiredPickupDate.FormattedShortDateSuppressEarliestDate();
			txtEstimatedDeliveryDate.Text = loadOrder.EstimatedDeliveryDate.FormattedShortDateSuppressEarliestDate();

			if (LockLoadOrder != null)
				LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

			pnlModifyDatesBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyDates.Update();
		}



		protected void OnCloseModifyDatesClicked(object sender, EventArgs e)
		{
			hidFlag.Value = string.Empty;
			hidGoToCurrentScrollPosition.Value = true.ToString();

			pnlModifyDates.Visible = false;
			pnlDimScreen.Visible = false;

			if (UnLockShipment != null && hidEditShipmentId.Value.ToLong() != default(long))
				UnLockShipment(this, new ViewEventArgs<Shipment>(new Shipment(hidEditShipmentId.Value.ToLong())));

			if (UnLockLoadOrder != null && hidEditLoadOrderId.Value.ToLong() != default(long))
				UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(new LoadOrder(hidEditLoadOrderId.Value.ToLong())));
		}

		protected void OnDoneModifyingDatesClicked(object sender, EventArgs e)
		{
			if (hidEditShipmentId.Value.ToLong() != default(long))
			{
				var shipment = new Shipment(hidEditShipmentId.Value.ToLong(), true);
				shipment.LoadCollections();

				shipment.DesiredPickupDate = txtDesiredPickupDate.Text.ToDateTime();
				shipment.ActualPickupDate = txtActualPickupDate.Text.ToDateTime();
				shipment.EstimatedDeliveryDate = txtEstimatedDeliveryDate.Text.ToDateTime();
				shipment.Status = shipment.InferShipmentStatus(false);

				if (SaveShipment != null)
					SaveShipment(this, new ViewEventArgs<Shipment>(shipment));
			}

			if (hidEditLoadOrderId.Value.ToLong() != default(long))
			{
				var loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong(), true);
				loadOrder.LoadCollections();

				loadOrder.DesiredPickupDate = txtDesiredPickupDate.Text.ToDateTime();
				loadOrder.EstimatedDeliveryDate = txtEstimatedDeliveryDate.Text.ToDateTime();

				if (SaveLoadOrder != null)
					SaveLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
			}
		}



		private void DisplayModifyShipmentItemsPopup(long shipmentId)
		{
			var shipment = new Shipment(shipmentId);

			pnlModifyItems.CssClass = pnlModifyItems.CssClass.Replace(HiddenCssClass, string.Empty);
			pnlDimScreen.Visible = true;

			litModifyItemsTitle.Text = string.Format("Update Items For Shipment {0}", shipment.ShipmentNumber);
			litItemsErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingItems;
			hidEditShipmentId.Value = shipment.Id.ToString();
			hidEditLoadOrderId.Value = string.Empty;

			var nItems = shipment.Items
			                     .Select(i => new
				                     {
					                     i.Id,
					                     i.Pickup,
					                     Delivery = i.Delivery == (shipment.Stops.Count + 1) ? ViewDeliveryStopOrder : i.Delivery,
					                     i.ActualWeight,
					                     i.ActualLength,
					                     i.ActualWidth,
					                     i.ActualHeight,
					                     i.Quantity,
					                     i.PieceCount,
					                     i.Description,
					                     i.Comment,
					                     i.ActualFreightClass,
					                     i.RatedFreightClass,
					                     i.Value,
					                     i.IsStackable,
                                         i.HazardousMaterial,
					                     i.PackageTypeId,
					                     i.NMFCCode,
					                     i.HTSCode
				                     })
			                     .OrderBy(i => i.Pickup).ThenBy(i => i.Delivery)
			                     .ToList();

			lstItems.DataSource = nItems;
			lstItems.DataBind();

			if (LockShipment != null)
				LockShipment(this, new ViewEventArgs<Shipment>(shipment));

			pnlModifyItemsBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyItems.Update();
		}

		private void DisplayModifyLoadOrderItemsPopup(long loadOrderId)
		{
			var loadOrder = new LoadOrder(loadOrderId);
            var datPosting =
                new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(loadOrder.LoadOrderNumber,
                                                                                             loadOrder.TenantId);
            // DAT does not allow us to update items in a load
		    if (datPosting != null) return;

			pnlModifyItems.CssClass = pnlModifyItems.CssClass.Replace(HiddenCssClass, string.Empty);
			pnlDimScreen.Visible = true;

			litModifyItemsTitle.Text = string.Format("Update Items For Load Order {0}", loadOrder.LoadOrderNumber);
			litItemsErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingItems;
			hidEditShipmentId.Value = string.Empty;
			hidEditLoadOrderId.Value = loadOrder.Id.ToString();

			var nItems = loadOrder.Items
			                      .Select(i => new
				                      {
					                      i.Id,
					                      i.Pickup,
					                      Delivery = i.Delivery == (loadOrder.Stops.Count + 1) ? ViewDeliveryStopOrder : i.Delivery,
					                      ActualWeight = i.Weight,
					                      ActualLength = i.Length,
					                      ActualWidth = i.Width,
					                      ActualHeight = i.Height,
					                      i.Quantity,
					                      i.PieceCount,
					                      i.Description,
					                      i.Comment,
					                      ActualFreightClass = i.FreightClass,
					                      RatedFreightClass = string.Empty,
					                      i.Value,
					                      i.IsStackable,
                                          i.HazardousMaterial,
					                      i.PackageTypeId,
					                      i.NMFCCode,
					                      i.HTSCode
				                      })
			                      .OrderBy(i => i.Pickup).ThenBy(i => i.Delivery)
			                      .ToList();

			lstItems.DataSource = nItems;
			lstItems.DataBind();

			if (LockLoadOrder != null)
				LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

			pnlModifyItemsBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyItems.Update();
		}



		protected void OnCloseModifyItemsClicked(object sender, EventArgs eventArgs)
		{
			hidFlag.Value = string.Empty;
			hidGoToCurrentScrollPosition.Value = true.ToString();

			if (!pnlModifyItems.CssClass.Contains(HiddenCssClass)) pnlModifyItems.CssClass += HiddenCssClass;
			pnlDimScreen.Visible = false;

			if (UnLockShipment != null && hidEditShipmentId.Value.ToLong() != default(long))
				UnLockShipment(this, new ViewEventArgs<Shipment>(new Shipment(hidEditShipmentId.Value.ToLong())));

			if (UnLockLoadOrder != null && hidEditLoadOrderId.Value.ToLong() != default(long))
				UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(new LoadOrder(hidEditLoadOrderId.Value.ToLong())));
		}

		protected void OnDoneModifyingItemsClicked(object sender, EventArgs e)
		{
			if (hidEditShipmentId.Value.ToLong() != default(long))
			{
				var shipment = new Shipment(hidEditShipmentId.Value.ToLong(), true);
				shipment.LoadCollections();

				shipment.Items = lstItems
					.Items
					.Select(i =>
						{
							var id = i.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong();
							var txtQuantity = i.FindControl("txtQuantity").ToTextBox();
							if (txtQuantity.Text.ToInt() <= 0) txtQuantity.Text = 1.ToString();
							var delivery = i.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt();
							return new ShipmentItem(id, id != default(long))
								{
									Pickup = i.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
									Delivery = delivery == ViewDeliveryStopOrder ? shipment.Stops.Count + 1 : delivery,
									ActualWeight = i.FindControl("txtWeight").ToTextBox().Text.ToDecimal(),
									ActualLength = i.FindControl("txtLength").ToTextBox().Text.ToDecimal(),
									ActualWidth = i.FindControl("txtWidth").ToTextBox().Text.ToDecimal(),
									ActualHeight = i.FindControl("txtHeight").ToTextBox().Text.ToDecimal(),
									Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
									PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text.ToInt(),
									Description = i.FindControl("txtDescription").ToTextBox().Text,
									Comment = i.FindControl("txtComment").ToTextBox().Text,
									ActualFreightClass = i.FindControl("ddlActualFreightClass").ToDropDownList().SelectedValue.ToDouble(),
									RatedFreightClass = i.FindControl("txtRatedFreightClass").ToTextBox().Text.ToDouble(),
									Value = i.FindControl("txtItemValue").ToTextBox().Text.ToDecimal(),
									IsStackable = i.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
									PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
									NMFCCode = i.FindControl("txtNMFCCode").ToTextBox().Text,
									HTSCode = i.FindControl("txtHTSCode").ToTextBox().Text,
									Tenant = shipment.Tenant,
									Shipment = shipment,
								};
						})
					.ToList();

                // Remove hazmat property & service if all items with hazmat property have been removed
                if (shipment.Items.All(i => !i.HazardousMaterial) && shipment.HazardousMaterial)
                {
                    RemoveShipmentHazmatFlags(shipment);
                }

				if (SaveShipment != null)
					SaveShipment(this, new ViewEventArgs<Shipment>(shipment));
			}
			else if (hidEditLoadOrderId.Value.ToLong() != default(long))
			{
				var loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong(), true);
				loadOrder.LoadCollections();

				loadOrder.Items = lstItems
					.Items
					.Select(i =>
						{
							var id = i.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong();
							var txtQuantity = i.FindControl("txtQuantity").ToTextBox();
							if (txtQuantity.Text.ToInt() <= 0) txtQuantity.Text = 1.ToString();
							var delivery = i.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt();
							return new LoadOrderItem(id, id != default(long))
								{
									Pickup = i.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
									Delivery = delivery == ViewDeliveryStopOrder ? loadOrder.Stops.Count + 1 : delivery,
									Weight = i.FindControl("txtWeight").ToTextBox().Text.ToDecimal(),
									Length = i.FindControl("txtLength").ToTextBox().Text.ToDecimal(),
									Width = i.FindControl("txtWidth").ToTextBox().Text.ToDecimal(),
									Height = i.FindControl("txtHeight").ToTextBox().Text.ToDecimal(),
									Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
									PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text.ToInt(),
									Description = i.FindControl("txtDescription").ToTextBox().Text,
									Comment = i.FindControl("txtComment").ToTextBox().Text,
									FreightClass = i.FindControl("ddlActualFreightClass").ToDropDownList().SelectedValue.ToDouble(),
									Value = i.FindControl("txtItemValue").ToTextBox().Text.ToDecimal(),
									IsStackable = i.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
									PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
									NMFCCode = i.FindControl("txtNMFCCode").ToTextBox().Text,
									HTSCode = i.FindControl("txtHTSCode").ToTextBox().Text,
									Tenant = loadOrder.Tenant,
									LoadOrder = loadOrder,
								};
						})
					.ToList();

                // Remove hazmat property & service if all items with hazmat property have been removed
                if (loadOrder.Items.All(i => !i.HazardousMaterial) && loadOrder.HazardousMaterial)
			    {
			        RemoveLoadOrderHazmatFlags(loadOrder);
			    }

			    if (SaveLoadOrder != null)
					SaveLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
			}
		}

		protected void OnAddItemClicked(object sender, EventArgs e)
		{
			var items = lstItems.Items
			                    .Select(control => new
				                    {
					                    Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
					                    Pickup = control.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
					                    Delivery = control.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt(),
					                    ActualWeight = control.FindControl("txtWeight").ToTextBox().Text,
					                    ActualLength = control.FindControl("txtLength").ToTextBox().Text,
					                    ActualWidth = control.FindControl("txtWidth").ToTextBox().Text,
					                    ActualHeight = control.FindControl("txtHeight").ToTextBox().Text,
					                    Quantity = control.FindControl("txtQuantity").ToTextBox().Text,
					                    PieceCount = control.FindControl("txtPieceCount").ToTextBox().Text,
					                    Description = control.FindControl("txtDescription").ToTextBox().Text,
					                    Comment = control.FindControl("txtComment").ToTextBox().Text,
					                    ActualFreightClass =
				                                       control.FindControl("ddlActualFreightClass")
				                                              .ToDropDownList()
				                                              .SelectedValue.ToDouble(),
					                    RatedFreightClass = control.FindControl("txtRatedFreightClass").ToTextBox().Text.ToDouble(),
					                    Value = control.FindControl("txtItemValue").ToTextBox().Text,
					                    IsStackable = control.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                                        HazardousMaterial = control.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
					                    PackageTypeId =
				                                       control.FindControl("ddlPackageType")
				                                              .ToCachedObjectDropDownList()
				                                              .SelectedValue.ToLong(),
					                    NMFCCode = control.FindControl("txtNMFCCode").ToTextBox().Text,
					                    HTSCode = control.FindControl("txtHTSCode").ToTextBox().Text,
				                    })
			                    .ToList();

			items.Add(new
				{
					Id = default(long),
					Pickup = default(int),
					Delivery = default(int),
					ActualWeight = string.Empty,
					ActualLength = string.Empty,
					ActualWidth = string.Empty,
					ActualHeight = string.Empty,
					Quantity = string.Empty,
					PieceCount = string.Empty,
					Description = string.Empty,
					Comment = string.Empty,
					ActualFreightClass = default(double),
					RatedFreightClass = default(double),
					Value = string.Empty,
					IsStackable = false,
					HazardousMaterial = false,
					PackageTypeId = default(long),
					NMFCCode = string.Empty,
					HTSCode = string.Empty,
				});

			lstItems.DataSource = items;
			lstItems.DataBind();
		}

		protected void OnAddLibraryItemClicked(object sender, EventArgs e)
		{
			libraryItemFinder.Visible = true;
		}

		protected void OnClearItemsClicked(object sender, EventArgs e)
		{
			lstItems.DataSource = new List<object>();
			lstItems.DataBind();
		}

		protected void OnDeleteItemClicked(object sender, ImageClickEventArgs e)
		{
			var items = lstItems.Items
			                    .Select(control => new
				                    {
					                    Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
					                    Pickup = control.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
					                    Delivery = control.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt(),
					                    ActualWeight = control.FindControl("txtWeight").ToTextBox().Text,
					                    ActualLength = control.FindControl("txtLength").ToTextBox().Text,
					                    ActualWidth = control.FindControl("txtWidth").ToTextBox().Text,
					                    ActualHeight = control.FindControl("txtHeight").ToTextBox().Text,
					                    Quantity = control.FindControl("txtQuantity").ToTextBox().Text,
					                    PieceCount = control.FindControl("txtPieceCount").ToTextBox().Text,
					                    Description = control.FindControl("txtDescription").ToTextBox().Text,
					                    Comment = control.FindControl("txtComment").ToTextBox().Text,
					                    ActualFreightClass =
				                                       control.FindControl("ddlActualFreightClass")
				                                              .ToDropDownList()
				                                              .SelectedValue.ToDouble(),
					                    RatedFreightClass = control.FindControl("txtRatedFreightClass").ToTextBox().Text.ToDouble(),
					                    Value = control.FindControl("txtItemValue").ToTextBox().Text,
					                    IsStackable = control.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                                        HazardousMaterial = control.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
					                    PackageTypeId =
				                                       control.FindControl("ddlPackageType")
				                                              .ToCachedObjectDropDownList()
				                                              .SelectedValue.ToLong(),
					                    NMFCCode = control.FindControl("txtNMFCCode").ToTextBox().Text,
					                    HTSCode = control.FindControl("txtHTSCode").ToTextBox().Text,
				                    })
			                    .ToList();

			items.RemoveAt(((ImageButton) sender).Parent.FindControl("hidItemIndex").ToCustomHiddenField().Value.ToInt());

			lstItems.DataSource = items;
			lstItems.DataBind();
		}

		protected void OnSetPackageDimensionsClicked(object sender, ImageClickEventArgs e)
		{
			var control = ((ImageButton) sender).Parent;

			var packageTypeId = control.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong();

			if (packageTypeId == default(long)) return;

			var packageType = new PackageType(packageTypeId);

			// set dimensions, then recalculate density
			var txtLength = control.FindControl("txtLength").ToTextBox();
			var txtWidth = control.FindControl("txtWidth").ToTextBox();
			var txtHeight = control.FindControl("txtHeight").ToTextBox();

			var txtQty = control.FindControl("txtQuantity").ToTextBox();

			txtLength.Text = packageType.DefaultLength.ToString();
			txtWidth.Text = packageType.DefaultWidth.ToString();
			txtHeight.Text = packageType.DefaultHeight.ToString();
			if (txtQty.Text.ToInt() == 0) txtQty.Text = 1.GetString();

			var weight = control.FindControl("txtWeight").ToTextBox().Text.ToDecimal();
			var length = txtLength.Text.ToDecimal();
			var width = txtWidth.Text.ToDecimal();
			var height = txtHeight.Text.ToDecimal();
			var qty = txtQty.Text.ToInt();


			if (length != default(decimal) && width != default(decimal) && height != default(decimal) && qty != default(int))
				control.FindControl("txtEstimatedPcf").ToTextBox().Text =
					((weight/(length.InToFt()*width.InToFt()*height.InToFt()))/qty).ToString("n2");
			else
				control.FindControl("txtEstimatedPcf").ToTextBox().Text = string.Empty;
		}


		protected void OnItemsDataBinding(object sender, EventArgs e)
		{
			ShipmentBeingEdited = hidEditShipmentId.Value.ToLong() != default(long)
				                      ? new Shipment(hidEditShipmentId.Value.ToLong())
				                      : null;
			LoadOrderBeingEdited = hidEditLoadOrderId.Value.ToLong() != default(long)
				                       ? new LoadOrder(hidEditLoadOrderId.Value.ToLong())
				                       : null;
		}

		protected void OnItemsDataBound(object sender, EventArgs e)
		{
			ShipmentBeingEdited = null;
			LoadOrderBeingEdited = null;
		}

		protected void OnItemsItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var item = (ListViewDataItem) e.Item;
			var dataItem = item.DataItem;

			// Bind stops
			var ddlItemPickup = item.FindControl("ddlItemPickup").ToDropDownList();
			var ddlItemDelivery = item.FindControl("ddlItemDelivery").ToDropDownList();

			if (ShipmentBeingEdited != null)
			{
				var stops = ShipmentBeingEdited.Stops;
				var originStops = stops
					.Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
					.ToList();
				originStops.Insert(0, new ViewListItem(OriginStopText, 0.ToString()));

				var destStops = stops
					.Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
					.ToList();
				destStops.Add(new ViewListItem(DestinationStopText, ViewDeliveryStopOrder.ToString()));

				ddlItemPickup.DataSource = originStops;
				ddlItemPickup.DataBind();
				if (dataItem.HasGettableProperty("Pickup"))
					ddlItemPickup.SelectedValue = dataItem.GetPropertyValue("Pickup").GetString();

				ddlItemDelivery.DataSource = destStops;
				ddlItemDelivery.DataBind();
				if (dataItem.HasGettableProperty("Delivery"))
					ddlItemDelivery.SelectedValue = dataItem.GetPropertyValue("Delivery").GetString();
			}
			else if (LoadOrderBeingEdited != null)
			{
				var stops = LoadOrderBeingEdited.Stops;
				var originStops = stops
					.Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
					.ToList();
				originStops.Insert(0, new ViewListItem(OriginStopText, 0.ToString()));

				var destStops = stops
					.Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
					.ToList();
				destStops.Add(new ViewListItem(DestinationStopText, ViewDeliveryStopOrder.ToString()));

				ddlItemPickup.DataSource = originStops;
				ddlItemPickup.DataBind();
				if (dataItem.HasGettableProperty("Pickup"))
					ddlItemPickup.SelectedValue = dataItem.GetPropertyValue("Pickup").GetString();

				ddlItemDelivery.DataSource = destStops;
				ddlItemDelivery.DataBind();
				if (dataItem.HasGettableProperty("Delivery"))
					ddlItemDelivery.SelectedValue = dataItem.GetPropertyValue("Delivery").GetString();
			}

			// Bind freight classes
			var freightClasses = WebApplicationSettings.FreightClasses
			                                           .Select(fc => new ViewListItem(fc.ToString(), fc.ToString()))
			                                           .ToList();
			freightClasses.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicableShort, string.Empty));

			var ddlActualFreightClass = item.FindControl("ddlActualFreightClass").ToDropDownList();
			ddlActualFreightClass.DataSource = freightClasses;
			ddlActualFreightClass.DataBind();
			if (dataItem.HasGettableProperty("ActualFreightClass"))
				ddlActualFreightClass.SelectedValue = dataItem.GetPropertyValue("ActualFreightClass").GetString();
		}


		protected void OnLibraryItemFinderMultiItemSelected(object sender, ViewEventArgs<List<LibraryItem>> e)
		{
			var items = lstItems.Items
			                    .Select(control => new
				                    {
					                    Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
					                    Pickup = control.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
					                    Delivery = control.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt(),
					                    ActualWeight = control.FindControl("txtWeight").ToTextBox().Text,
					                    ActualLength = control.FindControl("txtLength").ToTextBox().Text,
					                    ActualWidth = control.FindControl("txtWidth").ToTextBox().Text,
					                    ActualHeight = control.FindControl("txtHeight").ToTextBox().Text,
					                    Quantity = control.FindControl("txtQuantity").ToTextBox().Text,
					                    PieceCount = control.FindControl("txtPieceCount").ToTextBox().Text,
					                    Description = control.FindControl("txtDescription").ToTextBox().Text,
					                    Comment = control.FindControl("txtComment").ToTextBox().Text,
					                    ActualFreightClass =
				                                       control.FindControl("ddlActualFreightClass")
				                                              .ToDropDownList()
				                                              .SelectedValue.ToDouble(),
					                    RatedFreightClass = control.FindControl("txtRatedFreightClass").ToTextBox().Text.ToDouble(),
					                    Value = control.FindControl("txtItemValue").ToTextBox().Text,
					                    IsStackable = control.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                                        HazardousMaterial = control.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
					                    PackageTypeId =
				                                       control.FindControl("ddlPackageType")
				                                              .ToCachedObjectDropDownList()
				                                              .SelectedValue.ToLong(),
					                    NMFCCode = control.FindControl("txtNMFCCode").ToTextBox().Text,
					                    HTSCode = control.FindControl("txtHTSCode").ToTextBox().Text,
				                    })
			                    .ToList();

			var itemsToAdd = e.Argument
			                  .Select(t => new
				                  {
					                  Id = default(long),
					                  Pickup = 0,
					                  Delivery = ViewDeliveryStopOrder,
					                  ActualWeight = t.Weight.GetString(),
					                  ActualLength = t.Length.GetString(),
					                  ActualWidth = t.Width.GetString(),
					                  ActualHeight = t.Height.GetString(),
					                  Quantity = t.Quantity.GetString(),
					                  PieceCount = t.PieceCount.GetString(),
					                  t.Description,
					                  t.Comment,
					                  ActualFreightClass = t.FreightClass,
					                  RatedFreightClass = t.FreightClass,
					                  Value = 0.GetString(),
					                  t.IsStackable,
                                      t.HazardousMaterial,
					                  PackageTypeId = t.PackageTypeId.ToLong(),
					                  t.NMFCCode,
					                  t.HTSCode,
				                  })
			                  .ToList();

			items.AddRange(itemsToAdd);

			lstItems.DataSource = items;
			lstItems.DataBind();

			libraryItemFinder.Visible = false;
		}

		protected void OnLibraryItemFinderItemCancelled(object sender, EventArgs e)
		{
			libraryItemFinder.Visible = false;
		}


		private void DisplayModifyShipmentChargesPopup(long shipmentId)
		{
			var shipment = new Shipment(shipmentId);

			pnlModifyCharges.CssClass = pnlModifyCharges.CssClass.Replace(HiddenCssClass, string.Empty);
			pnlDimScreen.Visible = true;

			litModifyChargesTitle.Text = string.Format("Update Charges For Shipment {0}", shipment.ShipmentNumber);
			litChargesErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingCharges;
			hidEditShipmentId.Value = shipment.Id.ToString();
			hidEditLoadOrderId.Value = string.Empty;

			lstCharges.DataSource = shipment.Charges.OrderBy(c => c.Quantity);
			lstCharges.DataBind();

			if (LockShipment != null)
				LockShipment(this, new ViewEventArgs<Shipment>(shipment));

			pnlModifyChargesBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyCharges.Update();
		}

		private void DisplayModifyLoadOrderChargesPopup(long loadOrderId)
		{
			var loadOrder = new LoadOrder(loadOrderId);

			pnlModifyCharges.CssClass = pnlModifyCharges.CssClass.Replace(HiddenCssClass, string.Empty);
			pnlDimScreen.Visible = true;

			litModifyChargesTitle.Text = string.Format("Update Charges For Load Order {0}", loadOrder.LoadOrderNumber);
			litChargesErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingCharges;
			hidEditShipmentId.Value = string.Empty;
			hidEditLoadOrderId.Value = loadOrder.Id.ToString();

			lstCharges.DataSource = loadOrder.Charges.OrderBy(c => c.Quantity);
			lstCharges.DataBind();

			if (LockLoadOrder != null)
				LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

			pnlModifyChargesBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyCharges.Update();
		}


		protected void OnCloseModifyChargesClicked(object sender, EventArgs e)
		{
			hidFlag.Value = string.Empty;
			hidGoToCurrentScrollPosition.Value = true.ToString();

			if (!pnlModifyCharges.CssClass.Contains(HiddenCssClass)) pnlModifyCharges.CssClass += HiddenCssClass;
			pnlDimScreen.Visible = false;

			if (UnLockShipment != null && hidEditShipmentId.Value.ToLong() != default(long))
				UnLockShipment(this, new ViewEventArgs<Shipment>(new Shipment(hidEditShipmentId.Value.ToLong())));

			if (UnLockLoadOrder != null && hidEditLoadOrderId.Value.ToLong() != default(long))
				UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(new LoadOrder(hidEditLoadOrderId.Value.ToLong())));
		}

		protected void OnDoneModifyingChargesClicked(object sender, EventArgs e)
		{
			if (hidEditShipmentId.Value.ToLong() != default(long))
			{
				var shipment = new Shipment(hidEditShipmentId.Value.ToLong(), true);
				shipment.LoadCollections();

				shipment.Charges = lstCharges
					.Items
					.Select(i =>
						{
							var id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong();
							var txtQty = (i.FindControl("txtQuantity").ToTextBox());
							if (txtQty.Text.ToInt() <= 0) txtQty.Text = 1.ToString();
							return new ShipmentCharge(id, id != default(long))
								{
									Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
									UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
									UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
									UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
									Comment = i.FindControl("txtComment").ToTextBox().Text,
									ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
									Shipment = shipment,
									Tenant = shipment.Tenant
								};
						})
					.ToList();

				if (SaveShipment != null)
					SaveShipment(this, new ViewEventArgs<Shipment>(shipment));
			}
			else if (hidEditLoadOrderId.Value.ToLong() != default(long))
			{
				var loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong(), true);
				loadOrder.LoadCollections();

				loadOrder.Charges = lstCharges
					.Items
					.Select(i =>
						{
							var id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong();
							var txtQty = (i.FindControl("txtQuantity").ToTextBox());
							if (txtQty.Text.ToInt() <= 0) txtQty.Text = 1.ToString();
							return new LoadOrderCharge(id, id != default(long))
								{
									Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
									UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
									UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
									UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
									Comment = i.FindControl("txtComment").ToTextBox().Text,
									ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
									LoadOrder = loadOrder,
									Tenant = loadOrder.Tenant
								};
						})
					.ToList();

				if (SaveLoadOrder != null)
					SaveLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
			}
		}

		protected void OnAddChargeClicked(object sender, EventArgs e)
		{
			var charges = lstCharges.Items
			                        .Select(i => new
				                        {
					                        Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
					                        Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
					                        UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
					                        UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
					                        UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
					                        Comment = i.FindControl("txtComment").ToTextBox().Text,
					                        ChargeCodeId =
				                                     i.FindControl("ddlChargeCode")
				                                      .ToCachedObjectDropDownList()
				                                      .SelectedValue.ToLong()
				                        })
			                        .ToList();

			charges.Add(new
				{
					Id = default(long),
					Quantity = default(int),
					UnitBuy = default(decimal),
					UnitSell = default(decimal),
					UnitDiscount = default(decimal),
					Comment = string.Empty,
					ChargeCodeId = default(long)
				});

			lstCharges.DataSource = charges.OrderBy(c => c.Quantity);
			lstCharges.DataBind();
		}

		protected void OnClearChargesClicked(object sender, EventArgs e)
		{
			lstCharges.DataSource = new List<object>();
			lstCharges.DataBind();
		}

		protected void OnDeleteChargeClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton) sender;

			var charges = lstCharges.Items
			                        .Select(i => new
				                        {
					                        Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
					                        Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
					                        UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
					                        UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
					                        UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
					                        Comment = i.FindControl("txtComment").ToTextBox().Text,
					                        ChargeCodeId =
				                                     i.FindControl("ddlChargeCode")
				                                      .ToCachedObjectDropDownList()
				                                      .SelectedValue.ToLong()
				                        })
			                        .ToList();

			charges.RemoveAt(imageButton.Parent.FindControl("hidChargeIndex").ToCustomHiddenField().Value.ToInt());

			lstCharges.DataSource = charges.OrderBy(c => c.Quantity);
			lstCharges.DataBind();
		}



		private void DisplayModifyShipmentEquipmentPopup(long shipmentId)
		{
			var shipment = new Shipment(shipmentId);

			pnlModifyEquipment.Visible = true;
			pnlDimScreen.Visible = true;

			litModifyEquipmentTitle.Text = string.Format("Update Equipment For Shipment {0}", shipment.ShipmentNumber);
			litEquipmentErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingEquipment;
			hidEditShipmentId.Value = shipment.Id.ToString();
			hidEditLoadOrderId.Value = string.Empty;

			var equipmentTypeIds = shipment.Equipments.Select(eq => eq.EquipmentTypeId);
			foreach (var item in rptEquipmentTypes.Items.Cast<RepeaterItem>())
			{
				var check = equipmentTypeIds.Contains(item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong());
				item.FindControl("chkSelected").ToAltUniformCheckBox().Checked = check;
			}

			if (LockShipment != null)
				LockShipment(this, new ViewEventArgs<Shipment>(shipment));

			pnlModifyEquipmentBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyEquipment.Update();
		}

		private void DisplayModifyLoadOrderEquipmentPopup(long loadOrderId)
		{
			var loadOrder = new LoadOrder(loadOrderId);
			var datPosting =
				new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(loadOrder.LoadOrderNumber,
				                                                                             loadOrder.TenantId);

			if (datPosting != null) return;

			pnlModifyEquipment.Visible = true;
			pnlDimScreen.Visible = true;

			litModifyEquipmentTitle.Text = string.Format("Update Equipment For Load Order {0}",
			                                             loadOrder.LoadOrderNumber);
			litEquipmentErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingEquipment;
			hidEditShipmentId.Value = string.Empty;
			hidEditLoadOrderId.Value = loadOrder.Id.ToString();

			var equipmentTypeIds = loadOrder.Equipments.Select(eq => eq.EquipmentTypeId);
			foreach (var item in rptEquipmentTypes.Items.Cast<RepeaterItem>())
			{
				var check =
					equipmentTypeIds.Contains(
						item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong());
				item.FindControl("chkSelected").ToAltUniformCheckBox().Checked = check;
			}

			if (LockLoadOrder != null)
				LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

			pnlModifyEquipmentBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyEquipment.Update();
		}

		protected void OnCloseModifyEquipmentClicked(object sender, EventArgs e)
		{
			hidFlag.Value = string.Empty;
			hidGoToCurrentScrollPosition.Value = true.ToString();

			pnlModifyEquipment.Visible = false;
			pnlDimScreen.Visible = false;

			if (UnLockShipment != null && hidEditShipmentId.Value.ToLong() != default(long))
				UnLockShipment(this, new ViewEventArgs<Shipment>(new Shipment(hidEditShipmentId.Value.ToLong())));

			if (UnLockLoadOrder != null && hidEditLoadOrderId.Value.ToLong() != default(long))
				UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(new LoadOrder(hidEditLoadOrderId.Value.ToLong())));
		}

		protected void OnDoneModifyingEquipmentClicked(object sender, EventArgs e)
		{
			if (hidEditShipmentId.Value.ToLong() != default(long))
			{
				var shipment = new Shipment(hidEditShipmentId.Value.ToLong(), true);
				shipment.LoadCollections();

				shipment.Equipments = rptEquipmentTypes
					.Items
					.Cast<RepeaterItem>()
					.Where(item => item.FindControl("chkSelected").ToAltUniformCheckBox().Checked)
					.Select(item => new ShipmentEquipment
						{
							Shipment = shipment,
							EquipmentTypeId =
								item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong(),
							TenantId = ActiveUser.TenantId,
						})
					.ToList();

				if (SaveShipment != null)
					SaveShipment(this, new ViewEventArgs<Shipment>(shipment));
			}
			else if (hidEditLoadOrderId.Value.ToLong() != default(long))
			{
				var loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong(), true);
				loadOrder.LoadCollections();

				loadOrder.Equipments = rptEquipmentTypes
					.Items
					.Cast<RepeaterItem>()
					.Where(item => item.FindControl("chkSelected").ToAltUniformCheckBox().Checked)
					.Select(item => new LoadOrderEquipment
						{
							LoadOrder = loadOrder,
							EquipmentTypeId =
								item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong(),
							TenantId = ActiveUser.TenantId,
						})
					.ToList();

				if (SaveLoadOrder != null)
					SaveLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
			}
		}


		private void DisplayModifyShipmentPrimaryVendorPopup(long shipmentId)
		{
			var shipment = new Shipment(shipmentId);

			pnlModifyVendor.Visible = true;
			pnlDimScreen.Visible = true;

			litModifyVendorTitle.Text = string.Format("Update Primary Vendor For Shipment {0}", shipment.ShipmentNumber);
			litVendorErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingVendor;
			hidEditShipmentId.Value = shipment.Id.ToString();
			hidEditLoadOrderId.Value = string.Empty;

			var primaryVendorRelation = shipment.Vendors.FirstOrDefault(v => v.Primary);
			DisplayVendor(primaryVendorRelation != null ? primaryVendorRelation.Vendor : null, false);
			hidPrimaryVendorRelationId.Value = primaryVendorRelation != null
				                                   ? primaryVendorRelation.Id.GetString()
				                                   : string.Empty;

			if (LockShipment != null)
				LockShipment(this, new ViewEventArgs<Shipment>(shipment));

			pnlModifyVendorBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyVendor.Update();
		}

		private void DisplayModifyLoadOrderPrimaryVendorPopup(long loadOrderId)
		{
			var loadOrder = new LoadOrder(loadOrderId);

			pnlModifyVendor.Visible = true;
			pnlDimScreen.Visible = true;

			litModifyVendorTitle.Text = string.Format("Update Primary Vendor For Load Order {0}", loadOrder.LoadOrderNumber);
			litVendorErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingVendor;
			hidEditShipmentId.Value = string.Empty;
			hidEditLoadOrderId.Value = loadOrder.Id.ToString();

			var primaryVendorRelation = loadOrder.Vendors.FirstOrDefault(v => v.Primary);
			DisplayVendor(primaryVendorRelation != null ? primaryVendorRelation.Vendor : null, false);
			hidPrimaryVendorRelationId.Value = primaryVendorRelation != null
				                                   ? primaryVendorRelation.Id.GetString()
				                                   : string.Empty;

			if (LockLoadOrder != null)
				LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

			pnlModifyVendorBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyVendor.Update();
		}



		protected void OnCloseModifyVendorClicked(object sender, EventArgs e)
		{
			hidFlag.Value = string.Empty;
			hidGoToCurrentScrollPosition.Value = true.ToString();

			pnlModifyVendor.Visible = false;
			pnlDimScreen.Visible = false;

			if (UnLockShipment != null && hidEditShipmentId.Value.ToLong() != default(long))
				UnLockShipment(this, new ViewEventArgs<Shipment>(new Shipment(hidEditShipmentId.Value.ToLong())));

			if (UnLockLoadOrder != null && hidEditLoadOrderId.Value.ToLong() != default(long))
				UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(new LoadOrder(hidEditLoadOrderId.Value.ToLong())));
		}

		protected void OnDoneModifyingVendorClicked(object sender, EventArgs e)
		{
			if (hidEditShipmentId.Value.ToLong() != default(long))
			{
				var shipment = new Shipment(hidEditShipmentId.Value.ToLong(), true);
				shipment.LoadCollections();

				var vendors = shipment.Vendors.Where(v => !v.Primary).ToList();
				if (hidPrimaryVendorId.Value.ToLong() != default(long))
				{
					var primaryId = hidPrimaryVendorRelationId.Value.ToLong();
					var primaryVendor = new ShipmentVendor(primaryId, primaryId != default(long))
						{
							VendorId = hidPrimaryVendorId.Value.ToLong(),
							Shipment = shipment,
							TenantId = shipment.TenantId,
							Primary = true,
						};

					if (primaryVendor.IsNew)
					{
						primaryVendor.FailureComments = string.Empty;
						primaryVendor.ProNumber = string.Empty;
					}

					vendors.Add(primaryVendor);
				}

				shipment.Vendors = vendors;

				if (SaveShipment != null)
					SaveShipment(this, new ViewEventArgs<Shipment>(shipment));
			}

			if (hidEditLoadOrderId.Value.ToLong() != default(long))
			{
				var loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong(), true);
				loadOrder.LoadCollections();

				var vendors = loadOrder.Vendors.Where(v => !v.Primary).ToList();
				if (hidPrimaryVendorId.Value.ToLong() != default(long))
				{
					var primaryId = hidPrimaryVendorRelationId.Value.ToLong();
					var primaryVendor = new LoadOrderVendor(primaryId, primaryId != default(long))
						{
							VendorId = hidPrimaryVendorId.Value.ToLong(),
							LoadOrder = loadOrder,
							TenantId = loadOrder.TenantId,
							Primary = true,
						};

					if (primaryVendor.IsNew) primaryVendor.ProNumber = string.Empty;

					vendors.Add(primaryVendor);
				}

				loadOrder.Vendors = vendors;

				if (SaveLoadOrder != null)
					SaveLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
			}
		}

		protected void OnVendorNumberEntered(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtVendorNumber.Text))
			{
				DisplayVendor(null);
				return;
			}

			if (VendorSearch != null)
				VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}

		protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
		{
			vendorFinder.Visible = true;
		}

		protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
		{
			DisplayVendor(e.Argument);
			vendorFinder.Visible = false;
		}

		protected void OnVendorFinderItemCancelled(object sender, EventArgs e)
		{
			vendorFinder.Visible = false;
		}


		protected void DisplayModifyShipmentOriginPopup(long shipmentId)
		{
			var shipment = new Shipment(shipmentId);

			pnlModifyLocation.CssClass = pnlModifyLocation.CssClass.Replace(HiddenCssClass, string.Empty);
			pnlDimScreen.Visible = true;

			litModifyLocationTitle.Text = string.Format("Update Origin For Shipment {0}", shipment.ShipmentNumber);
			litLocationErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingLocation;
			hidEditShipmentId.Value = shipment.Id.ToString();
			hidEditLoadOrderId.Value = string.Empty;

			oaiModifyLocation.Type = OperationsAddressInputType.Origin;
			oaiModifyLocation.SetCustomerIdSpecificFilter(shipment.Customer.Id);
			oaiModifyLocation.LoadAddress(shipment.Origin);

			if (LockShipment != null)
				LockShipment(this, new ViewEventArgs<Shipment>(shipment));

			pnlModifyLocationBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyLocation.Update();
		}

		protected void DisplayModifyShipmentDestinationPopup(long shipmentId)
		{
			var shipment = new Shipment(shipmentId);

			pnlModifyLocation.CssClass = pnlModifyLocation.CssClass.Replace(HiddenCssClass, string.Empty);
			pnlDimScreen.Visible = true;

			litModifyLocationTitle.Text = string.Format("Update Destination For Shipment {0}", shipment.ShipmentNumber);
			litLocationErrorMessages.Text = string.Empty;


			hidFlag.Value = ModifyingLocation;
			hidEditShipmentId.Value = shipment.Id.ToString();
			hidEditLoadOrderId.Value = string.Empty;

			oaiModifyLocation.Type = OperationsAddressInputType.Destination;
			oaiModifyLocation.SetCustomerIdSpecificFilter(shipment.Customer.Id);
			oaiModifyLocation.LoadAddress(shipment.Destination);

			if (LockShipment != null)
				LockShipment(this, new ViewEventArgs<Shipment>(shipment));

			pnlModifyLocationBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyLocation.Update();
		}

		protected void DisplayModifyLoadOrderOriginPopup(long loadOrderId)
		{
			var loadOrder = new LoadOrder(loadOrderId);
			var datPosting =
				new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(loadOrder.LoadOrderNumber,
				                                                                             loadOrder.TenantId);

			// DAT does not allow us to update the Origin
			if (datPosting != null) return;

			pnlModifyLocation.CssClass = pnlModifyLocation.CssClass.Replace(HiddenCssClass, string.Empty);
			pnlDimScreen.Visible = true;

			litModifyLocationTitle.Text = string.Format("Update Origin For Load Order {0}", loadOrder.LoadOrderNumber);
			litLocationErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingLocation;
			hidEditShipmentId.Value = string.Empty;
			hidEditLoadOrderId.Value = loadOrder.Id.ToString();

			oaiModifyLocation.Type = OperationsAddressInputType.Origin;
			oaiModifyLocation.SetCustomerIdSpecificFilter(loadOrder.Customer.Id);
			oaiModifyLocation.LoadAddress(loadOrder.Origin);

			if (LockLoadOrder != null)
				LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

			pnlModifyLocationBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyLocation.Update();
		}

		protected void DisplayModifyLoadOrderDestinationPopup(long loadOrderId)
		{
			var loadOrder = new LoadOrder(loadOrderId);
			var datPosting =
				new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(loadOrder.LoadOrderNumber,
				                                                                             loadOrder.TenantId);

			// DAT does not allow us to update the Destination
			if (datPosting != null) return;

			pnlModifyLocation.CssClass = pnlModifyLocation.CssClass.Replace(HiddenCssClass, string.Empty);
			pnlDimScreen.Visible = true;

			litModifyLocationTitle.Text = string.Format("Update Destination For Load Order {0}", loadOrder.LoadOrderNumber);
			litLocationErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingLocation;
			hidEditShipmentId.Value = string.Empty;
			hidEditLoadOrderId.Value = loadOrder.Id.ToString();

			oaiModifyLocation.Type = OperationsAddressInputType.Destination;
			oaiModifyLocation.SetCustomerIdSpecificFilter(loadOrder.Customer.Id);
			oaiModifyLocation.LoadAddress(loadOrder.Destination);

			if (LockLoadOrder != null)
				LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

			pnlModifyLocationBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyLocation.Update();
		}



		protected void OnCloseModifyLocationClicked(object sender, EventArgs eventArgs)
		{
			hidFlag.Value = string.Empty;
			hidGoToCurrentScrollPosition.Value = true.ToString();

			if (!pnlModifyLocation.CssClass.Contains(HiddenCssClass)) pnlModifyLocation.CssClass += HiddenCssClass;
			pnlDimScreen.Visible = false;

			if (UnLockShipment != null && hidEditShipmentId.Value.ToLong() != default(long))
				UnLockShipment(this, new ViewEventArgs<Shipment>(new Shipment(hidEditShipmentId.Value.ToLong())));

			if (UnLockLoadOrder != null && hidEditLoadOrderId.Value.ToLong() != default(long))
				UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(new LoadOrder(hidEditLoadOrderId.Value.ToLong())));
		}

		protected void OnDoneModifyingLocationClicked(object sender, EventArgs e)
		{
			if (hidEditShipmentId.Value.ToLong() != default(long))
			{
				var shipment = new Shipment(hidEditShipmentId.Value.ToLong(), true);
				shipment.LoadCollections();

				if (oaiModifyLocation.Type == OperationsAddressInputType.Origin)
				{
					shipment.Origin.TakeSnapShot();
					oaiModifyLocation.RetrieveAddress(shipment.Origin);
				}

				if (oaiModifyLocation.Type == OperationsAddressInputType.Destination)
				{
					shipment.Destination.TakeSnapShot();
					oaiModifyLocation.RetrieveAddress(shipment.Destination);
				}

				if (SaveShipment != null)
					SaveShipment(this, new ViewEventArgs<Shipment>(shipment));
			}

			if (hidEditLoadOrderId.Value.ToLong() != default(long))
			{
				var loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong(), true);
				loadOrder.LoadCollections();

				if (oaiModifyLocation.Type == OperationsAddressInputType.Origin)
				{
					loadOrder.Origin.TakeSnapShot();
					oaiModifyLocation.RetrieveAddress(loadOrder.Origin);
				}

				if (oaiModifyLocation.Type == OperationsAddressInputType.Destination)
				{
					loadOrder.Destination.TakeSnapShot();
					oaiModifyLocation.RetrieveAddress(loadOrder.Destination);
				}

				if (SaveLoadOrder != null)
					SaveLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
			}
		}



		protected void OnAddressInputFindPostalCode(object sender, ViewEventArgs<string> e)
		{
			postalCodeFinder.Visible = true;
			postalCodeFinder.SearchOn(e.Argument);
		}

		protected void OnAddressInputAddAddressFromAddressBook(object sender, ViewEventArgs<string> e)
		{
			addressBookFinder.Visible = true;
			addressBookFinder.SearchOn(e.Argument);
		}



		protected void OnPostalCodeFinderItemSelected(object sender, ViewEventArgs<PostalCodeViewSearchDto> e)
		{
			oaiModifyLocation.LoadPostalCode(e.Argument);
			postalCodeFinder.Visible = false;
			postalCodeFinder.Reset();
		}

		protected void OnPostalCodeFinderItemCancelled(object sender, EventArgs e)
		{
			postalCodeFinder.Visible = false;
			postalCodeFinder.Reset();
		}



		protected void OnAddressBookFinderItemSelected(object sender, ViewEventArgs<AddressBook> e)
		{
			oaiModifyLocation.LoadAddress(e.Argument);
			addressBookFinder.Visible = false;
			addressBookFinder.Reset();
		}

		protected void OnAddressBookFinderItemCancelled(object sender, EventArgs e)
		{
			addressBookFinder.Visible = false;
			addressBookFinder.Reset();
		}



		protected void DisplayModifyLoadOrderStatusPopup(long loadOrderId)
		{
			var loadOrder = new LoadOrder(loadOrderId);

			pnlModifyLoadOrderStatus.Visible = true;
			pnlDimScreen.Visible = true;

			litModifyLoadOrderStatusTitle.Text = string.Format("Update Status For Load Order {0}", loadOrder.LoadOrderNumber);
			litLoadOrderStatusErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingLoadOrderStatus;
			hidEditShipmentId.Value = string.Empty;
			hidEditLoadOrderId.Value = loadOrder.Id.ToString();

			ddlLoadOrderStatus.SelectedValue = loadOrder.Status.ToInt().ToString();

			if (LockLoadOrder != null)
				LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

			pnlModifyLoadOrderStatusBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyLoadOrderStatus.Update();
		}

		protected void OnCloseModifyLoadOrderStatusClicked(object sender, EventArgs eventArgs)
		{
			hidFlag.Value = string.Empty;
			hidGoToCurrentScrollPosition.Value = true.ToString();

			pnlModifyLoadOrderStatus.Visible = false;
			pnlDimScreen.Visible = false;

			if (UnLockShipment != null && hidEditShipmentId.Value.ToLong() != default(long))
				UnLockShipment(this, new ViewEventArgs<Shipment>(new Shipment(hidEditShipmentId.Value.ToLong())));

			if (UnLockLoadOrder != null && hidEditLoadOrderId.Value.ToLong() != default(long))
				UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(new LoadOrder(hidEditLoadOrderId.Value.ToLong())));
		}

		protected void OnDoneModifyingLoadOrderStatusClicked(object sender, EventArgs e)
		{
			var loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong(), true);
			loadOrder.LoadCollections();

			loadOrder.Status = ddlLoadOrderStatus.SelectedValue.ToInt().ToEnum<LoadOrderStatus>();

			if (SaveLoadOrder != null)
				SaveLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
		}



		protected void DisplayModifyLoadOrderDescriptionPopup(long loadOrderId)
		{
			var loadOrder = new LoadOrder(loadOrderId);

			pnlModifyLoadOrderDescription.Visible = true;
			pnlDimScreen.Visible = true;

			litModifyLoadOrderDescriptionTitle.Text = string.Format("Update Loadboard Comments For Load Order {0}",
			                                                        loadOrder.LoadOrderNumber);
			litLoadOrderDescriptionErrorMessages.Text = string.Empty;

			hidFlag.Value = ModifyingLoadOrderDescription;
			hidEditShipmentId.Value = string.Empty;
			hidEditLoadOrderId.Value = loadOrder.Id.ToString();

			txtLoadOrderDescription.Text = loadOrder.Description;

			if (LockLoadOrder != null)
				LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));

			pnlModifyLoadOrderDescriptionBody.Enabled = !LockFailed;
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyLoadOrderDescription.Update();
		}

		protected void OnCloseModifyLoadOrderDescriptionClicked(object sender, EventArgs e)
		{
			hidFlag.Value = string.Empty;
			hidGoToCurrentScrollPosition.Value = true.ToString();

			pnlModifyLoadOrderDescription.Visible = false;
			pnlDimScreen.Visible = false;

			if (UnLockShipment != null && hidEditShipmentId.Value.ToLong() != default(long))
				UnLockShipment(this, new ViewEventArgs<Shipment>(new Shipment(hidEditShipmentId.Value.ToLong())));

			if (UnLockLoadOrder != null && hidEditLoadOrderId.Value.ToLong() != default(long))
				UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(new LoadOrder(hidEditLoadOrderId.Value.ToLong())));
		}

		protected void OnDoneModifyingLoadOrderDescriptionClicked(object sender, EventArgs e)
		{
			var loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong(), true);
			loadOrder.LoadCollections();

			loadOrder.Description = txtLoadOrderDescription.Text;

			if (SaveLoadOrder != null)
				SaveLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
		}



		protected void DisplayManageLoadOrderOnDatLoadboardClicked(long loadOrderId)
		{
			var loadOrder = new LoadOrder(loadOrderId);
			var datPosting =
				new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(loadOrder.LoadOrderNumber,
				                                                                             loadOrder.TenantId);

			litModifyDatLoadOrder.Text = string.Format("Update DAT Posting For Load Order {0}", loadOrder.LoadOrderNumber);
			txtAssetId.Text = datPosting != null ? datPosting.AssetId : string.Empty;
		    btnModifyDatAssetForLoadOrder.Text = datPosting != null
		                                             ? "Modify DAT Posting"
		                                             : "Post Load To DAT";

			pnlModifyLoadOrderDatPosting.Visible = true;
			pnlDimScreen.Visible = true;
			hidFlag.Value = ModifyingLoadOrderDatPosting;

			if (LockLoadOrder != null)
				LockLoadOrder(this, new ViewEventArgs<LoadOrder>(loadOrder));
			if (LockFailed) DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});

			udpModifyLoadOrderDatPosting.Update();
		}

		protected void OnCancelDatAssetModifications(object sender, EventArgs e)
		{
			datLoadboardAssetControl.Visible = false;

			var loadOrder = new LoadOrder(hidEditLoadOrderId.Value.ToLong());

			var idNumber = loadOrder.LoadOrderNumber;

			var datPosting = new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(idNumber,
			                                                                                              loadOrder.TenantId);
			txtAssetId.Text = datPosting == null ? string.Empty : datPosting.AssetId;
            btnModifyDatAssetForLoadOrder.Text = datPosting != null
                                                     ? "Modify DAT Asset"
                                                     : "Post Load To DAT";
		}

		protected void OnCloseDatAssetClicked(object sender, EventArgs e)
		{
			hidFlag.Value = string.Empty;
			hidGoToCurrentScrollPosition.Value = true.ToString();

			pnlModifyLoadOrderDatPosting.Visible = false;
			pnlDimScreen.Visible = false;

			if (UnLockLoadOrder != null && hidEditLoadOrderId.Value.ToLong() != default(long))
				UnLockLoadOrder(this, new ViewEventArgs<LoadOrder>(new LoadOrder(hidEditLoadOrderId.Value.ToLong())));

		}

		protected void OnModifyDatAssetClicked(object sender, EventArgs e)
		{
			datLoadboardAssetControl.LoadDatAssetForLoadOrder(hidEditLoadOrderId.Value.ToLong());
			datLoadboardAssetControl.Visible = true;
		}


		protected void OnOkayProcess(object sender, EventArgs e)
		{
			messageBox.Visible = false;
		
		}

		protected void OnNoProcess(object sender, EventArgs e)
		{
			// Only process using 'No' on message box is 'Go To Shipment'
			messageBox.Visible = false;

		}
	}
}