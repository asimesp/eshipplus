﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerShipmentDetailView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.CustomerShipmentDetailView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowMore="True" OnCommand="OnToolbarCommandClicked"/>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Shipment Detail -
                <asp:Literal runat="server" ID="litShipmentHeader" />
            </h3>
        </div>
        
        <eShip:DocumentDisplayControl runat="server" ID="documentDisplay" Visible="false" OnClose="OnDocumentDisplayClosed" />        

        <hr class="dark mb5" />
        <asp:Panel runat="server" ID="pnlShipment">
            <div class="rowgroup">
                
                <div class="col_1_3">
                    <h5>Origin</h5>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Name/Description</label>
                            <eShip:CustomTextBox runat="server" ID="txtOrigin" CssClass="w330 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Street Address Line 1</label>
                            <eShip:CustomTextBox runat="server" ID="txtOriginStreet1" CssClass="w330 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Street Address Line 2</label>
                            <eShip:CustomTextBox runat="server" ID="txtOriginStreet2" CssClass="w330 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">City</label>
                            <eShip:CustomTextBox runat="server" ID="txtOriginCity" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">State</label>
                            <eShip:CustomTextBox runat="server" ID="txtOriginState" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Country</label>
                            <eShip:CustomTextBox runat="server" ID="txtOriginCountry" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Postal Code</label>
                            <eShip:CustomTextBox runat="server" ID="txtOriginPostalCode" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Primary Contact Name</label>
                            <eShip:CustomTextBox runat="server" ID="txtOriginPrimaryContactName" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Primary Contact Email</label>
                            <eShip:CustomTextBox runat="server" ID="txtOriginPrimaryContactEmail" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Primary Contact Phone</label>
                            <eShip:CustomTextBox runat="server" ID="txtOriginPrimaryContactPhone" CssClass="w160 disabled" ReadOnly="True" />

                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Primary Contact Fax</label>
                            <eShip:CustomTextBox runat="server" ID="txtOriginPrimaryContactFax" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Shipper's Reference</label>
                            <eShip:CustomTextBox runat="server" CssClass="w330 disabled" ReadOnly="True" ID="txtShippersReference" />

                        </div>
                    </div>

                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Pickup Instructions</label>
                            <eShip:CustomTextBox runat="server" ID="txtPickupInstructions" CssClass="w330 h150 disabled" ReadOnly="True" TextMode="MultiLine" />
                        </div>
                    </div>
                </div>
                <div class="col_1_3">
                    <h5>Destination</h5>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Name/Description</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestination" CssClass="w330 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Street Address Line 1</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestinationStreet1" CssClass="w330 disabled" ReadOnly="True" />

                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Street Address Line 2</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestinationStreet2" CssClass="w330 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">City</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestinationCity" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">State</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestinationState" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Country</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestinationCountry" CssClass="w160 disabled" ReadOnly="True" />

                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Postal Code</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestinationPostalCode" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Primary Contact Name</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestinationPrimaryContactName" CssClass="w160 disabled" ReadOnly="True" />

                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Primary Contact Email</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestinationPrimaryContactEmail" CssClass="w160 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Primary Contact Phone</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestinationPrimaryContactPhone" CssClass="w160 disabled" ReadOnly="True" />

                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Primary Contact Fax</label>
                            <eShip:CustomTextBox runat="server" ID="txtDestinationPrimaryContactFax" CssClass="w160 disabled" ReadOnly="True" />

                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">PO Number</label>
                            <eShip:CustomTextBox runat="server" CssClass="w330 disabled" ReadOnly="True" ID="txtPoNumber" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel">Delivery Instructions</label>
                            <eShip:CustomTextBox runat="server" ID="txtDeliveryInstructions" CssClass="w330 h150 disabled" ReadOnly="True" TextMode="MultiLine" />
                        </div>
                    </div>
                </div>
                <div class="col_1_3 wd">
                    <h5>General Information</h5>
                    <div class="row pb20">
                        <div class="fieldgroup">
                            <label class="wlabel">BOL Number</label>
                            <eShip:CustomTextBox runat="server" ID="txtBolNumber" CssClass="w150 disabled" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">PRO Number</label>
                            <eShip:CustomTextBox runat="server" ID="txtProNumber" CssClass="w150 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <h5>Pickup & Delivery</h5>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel w130">Pickup Date</label>
                            <eShip:CustomTextBox runat="server" ID="txtPickupDate" CssClass="w100 disabled" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel shrink mr10">Earliest Pickup</label>
                            <eShip:CustomTextBox runat="server" ID="txtPickupBetweenStart" CssClass="w70 disabled text-center" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel shrink">Latest Pickup</label>
                            <eShip:CustomTextBox runat="server" ID="txtPickupBetweenEnd" CssClass="w70 disabled text-center" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row pb20">
                        <div class="fieldgroup">
                            <label class="wlabel">Carrier</label>
                            <eShip:CustomTextBox runat="server" ID="txtCarrier" CssClass="w315 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <h5>Services</h5>
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:Repeater runat="server" ID="rptServices">
                                <HeaderTemplate>
                                    <ul class="mt0">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <label><%# Eval("Description") %></label>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="dark"/>
            <h5>Items</h5>
            <table class="stripe">
                <tr>
                    <th style="width: 30%;">Description
                    </th>
                    <th style="width: 10%;" class="text-center">Class
                    </th>
                    <th style="width: 10%;">Weight
                    </th>
                    <th style="width: 30%;">Packaging
                    </th>
                    <th style="width: 10%;" class="text-center">Quantity
                    </th>
                    <th style="width: 10%;">NMFC/HTS
                    </th>
                </tr>
                <asp:Repeater runat="server" ID="rptItems">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#Eval("Description") %>
                            </td>
                            <td class="text-center">
                                <%#Eval("Class") %>
                            </td>
                            <td>
                                <%#Eval("Weight") %>
                            </td>
                            <td>
                                <%#Eval("Packaging") %>
                            </td>
                            <td class="text-center">
                                <%#Eval("Quantity") %>
                            </td>
                            <td>
                                <%#Eval("NMFCHTS") %>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            
            <hr class="dark"/>

            <h5>Please Note</h5>
            <div class="rowgroup">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:Literal runat="server" ID="litPleaseNote" />
                    </div>
                </div>
            </div>
            
            <hr class="dark"/>
            
            <h5>Charges</h5>
            <table class="stripe">
                <tr>
                    <th style="width: 50%;">Charge Code
                    </th>
                    <th class="text-center">Quantity
                    </th>
                    <th class="text-right">Charge(s)
                    </th>
                </tr>
                <asp:Repeater runat="server" ID="rptCharges">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#Eval("ChargeCode") %></td>
                            <td class="text-center">
                                <%#Eval("Quantity") %></td>
                            <td class="text-right">
                                <%#Eval("AmountDue") %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <div class="row pt10">
                <div class="right">
                    <label class="upper blue">Total Charges: </label>
                    <eShip:CustomTextBox runat="server" ID="txtTotalCharges" CssClass="w130 disabled text-right" ReadOnly="True" />
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlRecordNotFound" Visible="false">
            <div class="rowgroup">
                <h5 class="red">Record Not Found</h5>
                <div class="row">
                    <div class="fieldgroup">
                        <label>The record you are attempting to access was not found!</label>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:CustomHiddenField runat="server" ID="hidShipmentId"/>
</asp:Content>
