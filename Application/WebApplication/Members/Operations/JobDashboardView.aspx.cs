﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Cache;
using LogisticsPlus.Eship.WebApplication.Members.Operations.Controls;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities.JobImport;
using OfficeOpenXml;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class JobDashboardView : MemberPageBaseWithPageStore, IJobDashboardView
    {
        // import fields
        private List<string> ImportFileSheets
        {
            get
            {
                return new List<string>
                {
                    JobSheet,
                    ShipmentSheet,
                    ShipmentItemSheet,
                    ShipmentChargeSheet,
                    ShipmentCustomerReferencesSheet,
                    ShipmentVendorSheet,
                    ShipmentEquipmentSheet,
                    ShipmentServiceSheet,
                    LoadOrderSheet,
                    LoadOrderItemSheet,
                    LoadOrderChargeSheet,
                    LoadOrderVendorSheet,
                    LoadOrderEquipmentSheet,
                    LoadOrderServiceSheet,
                    ServiceTicketSheet,
                    ServiceTicketItemSheet,
                    ServiceTicketChargeSheet,
                    ServiceTicketVendorSheet,
                    ServiceTicketEquipmentSheet,
                    ServiceTicketServiceSheet,
                };
            }
        }

        private const string JobSheet = "Jobs";
        private const string ShipmentSheet = "Shipments";
        private const string ShipmentItemSheet = "Shipment Items";
        private const string ShipmentChargeSheet = "Shipment Charges";
        private const string ShipmentCustomerReferencesSheet = "Shipment Customer References";
        private const string ShipmentVendorSheet = "Shipment Vendors";
        private const string ShipmentEquipmentSheet = "Shipment Equipment";
        private const string ShipmentServiceSheet = "Shipment Services";
        private const string LoadOrderSheet = "Load Orders";
        private const string LoadOrderItemSheet = "Load Order Items";
        private const string LoadOrderChargeSheet = "Load Order Charges";
        private const string LoadOrderVendorSheet = "Load Order Vendors";
        private const string LoadOrderEquipmentSheet = "Load Order Equipment";
        private const string LoadOrderServiceSheet = "Load Order Services";
        private const string ServiceTicketSheet = "Service Tickets";
        private const string ServiceTicketItemSheet = "Service Ticket Items";
        private const string ServiceTicketChargeSheet = "Service Ticket Charges";
        private const string ServiceTicketVendorSheet = "Service Ticket Vendors";
        private const string ServiceTicketEquipmentSheet = "Service Ticket Equipment";
        private const string ServiceTicketServiceSheet = "Service Ticket Services";

        private const string ImportJobArgs = "IJA";
        private const string JobRemovalArgs = "JRA";
        private const string JobImportTemplate = "JIT";
        private const string JobUnlinkTemplate = "JUT";


        private SearchField SortByField
        {
            get { return OperationsSearchFields.JobSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        public override ViewCode PageCode { get { return ViewCode.JobDashboard; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; } }

        public static string PageAddress { get { return "~/Members/Operations/JobDashboardView.aspx"; } }

        public event EventHandler<ViewEventArgs<JobViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<Tuple<List<Job>, List<Shipment>, List<LoadOrder>, List<ServiceTicket>>>> BatchImport;
        public event EventHandler<ViewEventArgs<Tuple<List<Shipment>, List<LoadOrder>, List<ServiceTicket>>>> UnLinkEntitiesFromJob;

        public void SearchForImportedJobs(string jobString)
        {
            fileUploader.Visible = false;

            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.Add(new ParameterColumn
            {
                DefaultValue = jobString,
                Operator = Operator.In,
                ReadOnly = false,
                ReportColumnName = OperationsSearchFields.JobNumber.DisplayName.ToString()
            });

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            DoSearchPreProcessingThenSearch(SortByField);
        }

        public void DisplaySearchResult(List<JobDashboardDto> jobs)
        {
            litRecordCount.Text = jobs.BuildRecordCount();
            lstJobDashboardDetails.DataSource = jobs;
            lstJobDashboardDetails.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
            fileUploader.Visible = false;
        }


        public void ProcessNotifications(List<Shipment> shipments, List<LoadOrder> loadOrders)
        {
            foreach (var shipment in shipments)
            { 
                this.NotifyNewShipment(shipment, true);
            }

            foreach (var load in loadOrders)
            {
                this.NotifyLoadOrderEvent(load);
            }
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new JobViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        private void DoSearch(JobViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<JobViewSearchCriteria>(criteria));
        }

        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var importJobs = new ToolbarMoreAction
            {
                CommandArgs = ImportJobArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Import Job",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var modifyJobLinks = new ToolbarMoreAction
            {
                CommandArgs = JobRemovalArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Remove Entities from Job",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var jobImportTemplate = new ToolbarMoreAction
            {
                CommandArgs = JobImportTemplate,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Download Job Import Template",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var jobUnLinkTemplate = new ToolbarMoreAction
            {
                CommandArgs = JobUnlinkTemplate,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Download Job Unlink Template",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var actions = new List<ToolbarMoreAction>
            {
                jobImportTemplate,
                jobUnLinkTemplate
            };

            if (ActiveUser.HasAccessToModify(ViewCode.Job))
            {
                actions.Add(importJobs);
                actions.Add(modifyJobLinks);
            }

            memberToolBar.ShowMore = actions.Any();

            return actions;
        }


        private void ProcessJobImport(Stream stream)
        {
            var jobs = new Dictionary<string, Job>();
            var shipments = new Dictionary<string, Shipment>();
            var loadOrders = new Dictionary<string, LoadOrder>();
            var serviceTickets = new Dictionary<string, ServiceTicket>();
            var minJobId = 0;

            // Helpers to keep track of the last entity being modified; aids in identifying what caused exceptions
            var lastEntity = string.Empty;
            var lastKey = string.Empty;

            try
            {
                var errorMessages = new List<ValidationMessage>();

                // Grab excel package from data
                var package = new ExcelPackage(stream);

                // ensure CORRECT worksheets are present
                var workSheetNames = package.Workbook.Worksheets.Select(w => w.Name);

                if (ImportFileSheets.Any(s => !workSheetNames.Contains(s)))
                {
                    DisplayMessages(new[] { ValidationMessage.Error("Import file does not contain all required sheets!") });
                    return;
                }

                // grab jobs
                var jobLines = package.GetLinesFromPackage(true, true, false, JobSheet);

                for (var row = 0; row < jobLines.Count; row++)
                {
                    var headerLine = jobLines[row].ToList();


                    if (!headerLine.Any() || headerLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Job Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (headerLine.Count != 4)
                    {
                        errorMessages.Add(ValidationMessage.Error("Job with key [{0}] has incorrect column count", headerLine[0]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var headerRow = new JobHeaderRow(headerLine);

                    lastEntity = "Job";
                    lastKey = headerRow.JobIdentifier;

                    var dbJob = new JobSearch().FetchJobByJobNumber(headerRow.JobIdentifier, ActiveUser.TenantId);
                    var job = dbJob ?? new Job(--minJobId)
                    {
                        DateCreated = DateTime.Now,
                        CreatedByUser = ActiveUser,
                        ExternalReference1 = headerRow.ExternalReference1,
                        ExternalReference2 = headerRow.ExternalReference2,
                        Customer = new CustomerSearch().FetchCustomerByNumber(headerRow.CustomerCode, ActiveUser.TenantId),
                        TenantId = ActiveUser.TenantId
                    };

                    if (job.Customer == null)
                    {
                        errorMessages.Add(ValidationMessage.Error("Job with key [{0}] has invalid Customer Number", headerLine[0]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    jobs.Add(headerRow.JobIdentifier, job);
                }

                // Generate Shipments
                var shipmentLines = package.GetLinesFromPackage(true, true, false, ShipmentSheet);

                if (shipmentLines.Any() && !ActiveUser.HasAccessToModify(ViewCode.Shipment))
                {
                    DisplayMessages(new[] { ValidationMessage.Error("You are attempting to import a shipment but do not have permission to do so") });
                    return;
                }

                for (var row = 0; row < shipmentLines.Count; row++)
                {
                    var shipmentLine = shipmentLines[row].ToList();

                    if (!shipmentLine.Any() || shipmentLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (shipmentLine.Count != 62)
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Row with key [{0} - {1}] has incorrect column count", shipmentLine[0], shipmentLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var shipmentRow = new ShipmentRow(shipmentLine);
                    if (!jobs.ContainsKey(shipmentRow.JobIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Row with key [{0} - {1}] has an invalid Job Identifier", shipmentRow.JobIdentifier, shipmentRow.ShipmentIdentifier, row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    lastEntity = "Shipment";
                    lastKey = shipmentRow.ShipmentIdentifier;

                    var matchingJob = jobs[shipmentRow.JobIdentifier];

                    //Sets the Prefix for the shipment to either be the Customer prefix if not defined,
                    //or the prefix defined on the import sheet
                    var search = new PrefixSearch();
                    var defaultPrefix = matchingJob.Customer.Prefix;
                    if (!string.IsNullOrEmpty(shipmentRow.Prefix))
                    {
                        defaultPrefix = search.FetchPrefixByCode(shipmentRow.Prefix, ActiveUser.TenantId);
                        if (defaultPrefix == null)
                        {
                            errorMessages.Add(ValidationMessage.Error("Shipment Row with key [{0} - {1}] has an invalid Prefix", shipmentRow.JobIdentifier, shipmentRow.ShipmentIdentifier));
                            DisplayMessages(errorMessages);
                            return;
                        }
                    }

                    var shipment = new Shipment
                    {
                        DateCreated = DateTime.Now,
                        User = ActiveUser,
                        TenantId = ActiveUser.TenantId,
                        Customer = matchingJob.Customer,
                        Prefix = defaultPrefix,
                        HidePrefix = false,
                        ShipperBol = string.Empty,
                        ShipperReference = shipmentRow.ShipperReference,
                        PurchaseOrderNumber = shipmentRow.PurchaseOrderNumber,
                        HazardousMaterial = shipmentRow.HazardousMaterial,
                        HazardousMaterialContactName = shipmentRow.HazardousMaterialContactName,
                        HazardousMaterialContactPhone = shipmentRow.HazardousMaterialContactPhone,
                        HazardousMaterialContactMobile = shipmentRow.HazardousMaterialContactMobile,
                        HazardousMaterialContactEmail = shipmentRow.HazardousMaterialContactEmail.StripSpacesFromEmails(),
                        DesiredPickupDate = shipmentRow.DesiredPickupDate,
                        EarlyPickup = shipmentRow.EarlyPickup,
                        LatePickup = shipmentRow.LatePickup,
                        EstimatedDeliveryDate = shipmentRow.EstimatedDeliveryDate,
                        EarlyDelivery = shipmentRow.EarlyDelivery,
                        LateDelivery = shipmentRow.LateDelivery,
                        ServiceMode = shipmentRow.ServiceMode,
                        DeclineInsurance = shipmentRow.DeclineAdditionalInsurance,
                        MiscField1 = shipmentRow.MiscField1,
                        MiscField2 = shipmentRow.MiscField2,
                        IsPartialTruckload = shipmentRow.IsPartialTruckload,
                        JobStep = shipmentRow.JobStep,
                        ActualDeliveryDate = shipmentRow.ActualDeliveryDate.SetTime(shipmentRow.ActualDeliveryTime),
                        ActualPickupDate = shipmentRow.ActualPickupDate.SetTime(shipmentRow.ActualPickupTime),
                        Items = new List<ShipmentItem>(),
                        Vendors = new List<ShipmentVendor>(),
                        Charges = new List<ShipmentCharge>(),
                        Services = new List<ShipmentService>(),
                        Equipments = new List<ShipmentEquipment>(),
                        Assets = new List<ShipmentAsset>(),
                        AutoRatingAccessorials = new List<ShipmentAutoRatingAccessorial>(),
                        CheckCalls = new List<CheckCall>(),
                        Stops = new List<ShipmentLocation>(),
                        QuickPayOptions = new List<ShipmentQuickPayOption>(),
                        AccountBuckets = new List<ShipmentAccountBucket>(),
                        CustomerReferences = new List<ShipmentReference>(),
                        Documents = new List<ShipmentDocument>(),
                        Notes = new List<ShipmentNote>(),
                        SalesRepresentativeCommissionPercent = default(decimal),
                        SalesRepMinCommValue = default(decimal),
                        SalesRepMaxCommValue = default(decimal),
                        SalesRepAddlEntityName = string.Empty,
                        SalesRepAddlEntityCommPercent = default(decimal),
                        ResellerAdditionId = default(long),
                        BillReseller = false,
                        ShipmentPriorityId = default(long),
                        OriginalRateValue = default(decimal),
                        VendorDiscountPercentage = default(decimal),
                        VendorFreightFloor = default(decimal),
                        VendorFuelPercent = default(decimal),
                        CustomerFreightMarkupValue = default(decimal),
                        CustomerFreightMarkupPercent = default(decimal),
                        UseLowerCustomerFreightMarkup = false,
                        BilledWeight = default(decimal),
                        RatedWeight = default(decimal),
                        RatedCubicFeet = default(decimal),
                        RatedPcf = default(decimal),
                        ApplyDiscount = false,
                        IsLTLPackageSpecificRate = false,
                        LineHaulProfitAdjustmentRatio = default(decimal),
                        FuelProfitAdjustmentRatio = default(decimal),
                        AccessorialProfitAdjustmentRatio = default(decimal),
                        ServiceProfitAdjustmentRatio = default(decimal),
                        ResellerAdditionalFreightMarkup = default(decimal),
                        ResellerAdditionalFreightMarkupIsPercent = false,
                        ResellerAdditionalFuelMarkup = default(decimal),
                        ResellerAdditionalFuelMarkupIsPercent = false,
                        ResellerAdditionalAccessorialMarkup = default(decimal),
                        ResellerAdditionalAccessorialMarkupIsPercent = false,
                        DirectPointRate = false,
                        SmallPackType = string.Empty,
                        SmallPackageEngine = SmallPackageEngine.None,
                        VendorRatingOverrideAddress = string.Empty,
                        CareOfAddressFormatEnabled = false,
                        LinearFootRuleBypassed = false,
                        IsGuaranteedDeliveryService = false,
                        IsExpeditedService = false,
                        ExpeditedServiceRate = 0,
                        ExpeditedServiceTime = string.Empty,
                        GuaranteedDeliveryServiceTime = string.Empty,
                        GuaranteedDeliveryServiceRate = default(decimal),
                        GuaranteedDeliveryServiceRateType = RateType.Flat,
                        Project44QuoteNumber = string.Empty,
                        AuditedForInvoicing = false,
                        BolFtpDelivered = false,
                        CarrierCoordinatorId = default(long),
                        CreatedInError = false,
                        CriticalBolComments = shipmentRow.CriticalBolComments,
                        AccountBucketUnitId = default(long),
                        VendorFreightCeiling = default(long),
                        StatementFtpDelivered = false,
                        ShipmentCoordinatorId = default(long),
                        ShipmentAutorated = false,
                        SalesRepresentativeId = default(long),
                        ResellerAdditionalServiceMarkupIsPercent = false,
                        ResellerAdditionalServiceMarkup = default(decimal),
                        MileageSourceId = default(long),
                        Mileage = default(decimal),
                        EmptyMileage = default(decimal),
                        DriverName = string.Empty,
                        DriverPhoneNumber = string.Empty,
                        DriverTrailerNumber = string.Empty,
                        EnableQuickPay = false,
                        GeneralBolComments = shipmentRow.GeneralBolComments,
                        InDispute = false,
                        InDisputeReason = InDisputeReason.NotApplicable,
                    };

                    shipment.Status = shipment.InferShipmentStatus(true);

                    if (shipment.Customer.SalesRepresentative != null)
                    {
                        var applicableTier = shipment.Customer.SalesRepresentative.ApplicableTier(shipment.DateCreated, ServiceMode.NotApplicable, shipment.Customer) ?? new SalesRepCommTier();

                        shipment.SalesRepresentative = shipment.Customer.SalesRepresentative;
                        shipment.SalesRepresentativeCommissionPercent = applicableTier.CommissionPercent;
                        shipment.SalesRepMinCommValue = applicableTier.FloorValue;
                        shipment.SalesRepMaxCommValue = applicableTier.CeilingValue;
                        shipment.SalesRepAddlEntityName = shipment.Customer.SalesRepresentative.AdditionalEntityName;
                        shipment.SalesRepAddlEntityCommPercent = shipment.Customer.SalesRepresentative.AdditionalEntityCommPercent;
                    }


                    if (shipment.Customer.Rating != null && shipment.Customer.Rating.ResellerAddition != null)
                    {
                        shipment.ResellerAddition = shipment.Customer.Rating.ResellerAddition;
                        shipment.BillReseller = shipment.Customer.Rating.BillReseller;
                    }

                    if (shipment.Customer.DefaultAccountBucket != null)
                    {
                        shipment.AccountBuckets = new List<ShipmentAccountBucket>
                        {
                            new ShipmentAccountBucket
                            {
                                AccountBucket = matchingJob.Customer.DefaultAccountBucket,
                                Primary = true,
                                Shipment = shipment,
                                TenantId = shipment.TenantId
                            }
                        };
                    }

                    shipment.Destination = new ShipmentLocation
                    {
                        Shipment = shipment,
                        TenantId = shipment.TenantId,
                        Street1 = shipmentRow.DestinationStreet1,
                        Street2 = shipmentRow.DestinationStreet2,
                        City = shipmentRow.DestinationCity,
                        PostalCode = shipmentRow.DestinationPostalCode,
                        State = shipmentRow.DestinationState,
                        CountryId = new CountrySearch().FetchCountryIdByCode(shipmentRow.DestinationCountryCode),
                        Description = shipmentRow.DestinationName,
                        SpecialInstructions = shipmentRow.DestinationSpecialInstructions,
                        AppointmentDateTime = shipmentRow.DestinationAppointmentDate <= DateUtility.SystemEarliestDateTime
                                               ? DateUtility.SystemEarliestDateTime
                                               : string.Format("{0} {1}", shipmentRow.DestinationAppointmentDate.Date, shipmentRow.DestinationAppointmentTime).ToDateTime(),
                        Direction = string.Empty,
                        GeneralInfo = string.Empty,
                        MilesFromPreceedingStop = default(decimal),
                        StopOrder = 1
                    };
                    shipment.Destination.Contacts = new List<ShipmentContact>
                    {
                        new ShipmentContact
                        {
                            Name = shipmentRow.DestinationPrimaryContactName,
                            Phone = shipmentRow.DestinationPrimaryContactPhone,
                            Email = shipmentRow.DestinationPrimaryContactEmail.StripSpacesFromEmails(),
                            Mobile = shipmentRow.DestinationPrimaryContactMobile,
                            Fax = shipmentRow.DestinationPrimaryContactFax,
                            ContactTypeId = new ContactTypeSearch().FetchContactTypeIdByCode(shipmentRow.DestinationPrimaryContactType, ActiveUser.TenantId),
                            Comment = shipmentRow.DestinationPrimaryContactComment,
                            TenantId = shipment.TenantId,
                            Primary = true,
                            Location = shipment.Destination
                        }
                    };

                    shipment.Origin = new ShipmentLocation
                    {
                        Shipment = shipment,
                        TenantId = shipment.TenantId,
                        Street1 = shipmentRow.OriginStreet1,
                        Street2 = shipmentRow.OriginStreet2,
                        City = shipmentRow.OriginCity,
                        PostalCode = shipmentRow.OriginPostalCode,
                        State = shipmentRow.OriginState,
                        CountryId = new CountrySearch().FetchCountryIdByCode(shipmentRow.OriginCountryCode),
                        Description = shipmentRow.OriginName,
                        SpecialInstructions = shipmentRow.OriginSpecialInstructions,
                        AppointmentDateTime = shipmentRow.OriginAppointmentDate <= DateUtility.SystemEarliestDateTime
                                                ? DateUtility.SystemEarliestDateTime
                                                : string.Format("{0} {1}", shipmentRow.OriginAppointmentDate.Date, shipmentRow.OriginAppointmentTime).ToDateTime(),
                        Direction = string.Empty,
                        GeneralInfo = string.Empty,
                        MilesFromPreceedingStop = default(decimal),
                        StopOrder = default(int)
                    };
                    shipment.Origin.Contacts = new List<ShipmentContact>
                    {
                        new ShipmentContact
                        {
                            Name = shipmentRow.OriginPrimaryContactName,
                            Phone = shipmentRow.OriginPrimaryContactPhone,
                            Email = shipmentRow.OriginPrimaryContactEmail.StripSpacesFromEmails(),
                            Mobile = shipmentRow.OriginPrimaryContactMobile,
                            Fax = shipmentRow.OriginPrimaryContactFax,
                            ContactTypeId = new ContactTypeSearch().FetchContactTypeIdByCode(shipmentRow.OriginPrimaryContactType, ActiveUser.TenantId),
                            Comment = shipmentRow.OriginPrimaryContactComment,
                            TenantId = shipment.TenantId,
                            Location = shipment.Origin,
                            Primary = true
                        }
                    };


                    if (shipments.ContainsKey(shipmentRow.ShipmentIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Row with key [{0}]  is repeated in file", shipmentRow.ShipmentIdentifier, row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    shipment.Job = matchingJob;
                    shipments.Add(shipmentRow.ShipmentIdentifier, shipment);
                }

                // Generate Shipment Items
                var shipmentItemLines = package.GetLinesFromPackage(true, true, false, ShipmentItemSheet);

                for (var row = 0; row < shipmentItemLines.Count; row++)
                {
                    var itemLine = shipmentItemLines[row].ToList();

                    if (!itemLine.Any() || itemLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Item Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (itemLine.Count != 16)
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Item Row with key [{0} - {1}] has incorrect column count", itemLine[0], itemLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var itemRow = new ShipmentItemRow(itemLine);

                    lastEntity = "Shipment Item";
                    lastKey = itemRow.ShipmentIdentifier;

                    var shipmentItem = new ShipmentItem
                    {
                        Description = itemRow.Description,
                        Comment = itemRow.Comment,
                        ActualFreightClass = itemRow.FreightClass,
                        RatedFreightClass = default(double),
                        ActualWeight = itemRow.ActualWeight,
                        ActualLength = itemRow.ActualLength,
                        ActualWidth = itemRow.ActualWidth,
                        ActualHeight = itemRow.ActualHeight,
                        Quantity = itemRow.Quantity,
                        PieceCount = itemRow.PieceCount,
                        IsStackable = itemRow.IsStackable,
                        HazardousMaterial = itemRow.HazardousMaterial,
                        PackageTypeId = new PackageTypeSearch().FetchPackageTypeIdByTypeName(itemRow.PackageTypeName, ActiveUser.TenantId),
                        NMFCCode = itemRow.NMFCCode,
                        HTSCode = itemRow.HTSCode,
                        Value = itemRow.Value,
                        TenantId = ActiveUser.TenantId,
                        Delivery = 1,
                        Pickup = default(int),
                    };

                    if (!shipments.ContainsKey(itemRow.ShipmentIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Item Row with key [{0} - {1}] has an invalid Shipment Identifier", itemRow.ShipmentIdentifier, itemRow.Description));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var shipment = shipments[itemRow.ShipmentIdentifier];
                    shipmentItem.Shipment = shipment;
                    shipment.Items.Add(shipmentItem);
                }

                // Generate Shipment Charges
                var shipmentChargeLines = package.GetLinesFromPackage(true, true, false, ShipmentChargeSheet);

                for (var row = 0; row < shipmentChargeLines.Count; row++)
                {
                    var chargeLine = shipmentChargeLines[row].ToList();

                    if (!chargeLine.Any() || chargeLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Charge Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (chargeLine.Count != 7)
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Charge Row with key [{0} - {1}] has incorrect column count", chargeLine[0], chargeLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var chargeRow = new ShipmentChargeRow(chargeLine);

                    lastEntity = "Shipment Charge";
                    lastKey = chargeRow.ShipmentIdentifier;

                    var shipmentCharge = new ShipmentCharge
                    {
                        ChargeCodeId = new ChargeCodeSearch().FetchChargeCodeIdByCode(chargeRow.ChargeCode, ActiveUser.TenantId),
                        Quantity = chargeRow.Quantity,
                        UnitBuy = chargeRow.UnitBuy,
                        UnitSell = chargeRow.UnitSell,
                        UnitDiscount = chargeRow.UnitDiscount,
                        Comment = chargeRow.Comment,
                        TenantId = ActiveUser.TenantId,
                        VendorBillId = default(long),
                        VendorId = default(long)
                    };

                    if (!shipments.ContainsKey(chargeRow.ShipmentIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Charge Row with key [{0} - {1}] has an invalid Shipment Identifier", chargeRow.ShipmentIdentifier, chargeRow.ChargeCode));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var shipment = shipments[chargeRow.ShipmentIdentifier];
                    shipmentCharge.Shipment = shipment;
                    shipment.Charges.Add(shipmentCharge);
                }

                // Generate Shipment Customer Reference
                var shipmentCustomerReferencesLines = package.GetLinesFromPackage(true, true, false, ShipmentCustomerReferencesSheet);

                for (var row = 0; row < shipmentCustomerReferencesLines.Count; row++)
                {
                    var customerReferencesLine = shipmentCustomerReferencesLines[row].ToList();

                    if (!customerReferencesLine.Any() || customerReferencesLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Customer References {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (customerReferencesLine.Count != 5)
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Customer References Row with key [{0} - {1}] has incorrect column count", customerReferencesLine[0], customerReferencesLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var customerReferenceRow = new ShipmentCustomerReference(customerReferencesLine);

                    lastEntity = "Shipment Customer Reference";
                    lastKey = customerReferenceRow.ShipmentIdentifier;


                    var shipmentReference = new ShipmentReference
                    {
                        Name = customerReferenceRow.Name,
                        Value = customerReferenceRow.Value,
                        DisplayOnOrigin = customerReferenceRow.DisplayOnOrigin,
                        DisplayOnDestination = customerReferenceRow.DisplayOnDestination,
                        TenantId = ActiveUser.TenantId,
                    };

                    if (!shipments.ContainsKey(customerReferenceRow.ShipmentIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Customer Reference Row with key [{0} - {1}] has an invalid Shipment Identifier", customerReferenceRow.ShipmentIdentifier, customerReferenceRow.Name));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var shipment = shipments[customerReferenceRow.ShipmentIdentifier];
                    shipmentReference.Shipment = shipment;
                    shipment.CustomerReferences.Add(shipmentReference);

                }

                // Generate Shipment Vendors
                var shipmentVendorLines = package.GetLinesFromPackage(true, true, false, ShipmentVendorSheet);

                for (var row = 0; row < shipmentVendorLines.Count; row++)
                {
                    var vendorLine = shipmentVendorLines[row].ToList();

                    if (!vendorLine.Any() || vendorLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Vendor Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (vendorLine.Count != 5)
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Vendor Row with key [{0} - {1}] has incorrect column count", vendorLine[0], vendorLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var vendorRow = new ShipmentVendorRow(vendorLine);

                    lastEntity = "Shipment Vendor";
                    lastKey = vendorRow.ShipmentIdentifier;

                    var shipmentVendor = new ShipmentVendor
                    {
                        Vendor = new VendorSearch().FetchVendorByNumber(vendorRow.VendorNumber, ActiveUser.TenantId),
                        ProNumber = vendorRow.ProNumber,
                        FailureComments = vendorRow.FailureComments,
                        Primary = vendorRow.IsPrimary,
                        TenantId = ActiveUser.TenantId,
                        FailureCodeId = default(long)
                    };

                    if (!shipments.ContainsKey(vendorRow.ShipmentIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Vendor Row with key [{0} - {1}] has an invalid Shipment Identifier", vendorRow.ShipmentIdentifier, vendorRow.VendorNumber));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var shipment = shipments[vendorRow.ShipmentIdentifier];
                    shipmentVendor.Shipment = shipment;
                    shipment.Vendors.Add(shipmentVendor);
                }

                // Generate Shipment Equipment
                var shipmentEquipmentLines = package.GetLinesFromPackage(true, true, false, ShipmentEquipmentSheet);

                for (var row = 0; row < shipmentEquipmentLines.Count; row++)
                {
                    var equipmentLine = shipmentEquipmentLines[row].ToList();

                    if (!equipmentLine.Any() || equipmentLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Equipment Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (equipmentLine.Count != 2)
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Equipment Row with key [{0} - {1}] has incorrect column count", equipmentLine[0], equipmentLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var equipmentRow = new ShipmentEquipmentRow(equipmentLine);

                    lastEntity = "Shipment Equipment";
                    lastKey = equipmentRow.ShipmentIdentifier;

                    var shipmentEquipment = new ShipmentEquipment
                    {
                        EquipmentType = new EquipmentTypeSearch().FetchEquipmentTypeByCode(equipmentRow.EquipmentCode, ActiveUser.TenantId),
                        TenantId = ActiveUser.TenantId
                    };

                    if (!shipments.ContainsKey(equipmentRow.ShipmentIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Equipment Row with key [{0} - {1}] has an invalid Shipment Identifier", equipmentRow.ShipmentIdentifier, equipmentRow.EquipmentCode));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var shipment = shipments[equipmentRow.ShipmentIdentifier];
                    shipmentEquipment.Shipment = shipment;
                    shipment.Equipments.Add(shipmentEquipment);
                }

                // Generate Shipment Services
                var shipmentServicesLines = package.GetLinesFromPackage(true, true, false, ShipmentServiceSheet);

                for (var row = 0; row < shipmentServicesLines.Count; row++)
                {
                    var serviceLine = shipmentServicesLines[row].ToList();

                    if (!serviceLine.Any() || serviceLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Service Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (serviceLine.Count != 2)
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Service Row with key [{0} - {1}] has incorrect column count", serviceLine[0], serviceLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var serviceRow = new ShipmentServiceRow(serviceLine);

                    lastEntity = "Shipment Service";
                    lastKey = serviceRow.ShipmentIdentifier;

                    var shipmentService = new ShipmentService
                    {
                        ServiceId = new ServiceSearch().FetchServiceIdByCode(serviceRow.ServiceCode, ActiveUser.TenantId),
                        TenantId = ActiveUser.TenantId
                    };

                    if (!shipments.ContainsKey(serviceRow.ShipmentIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Shipment Service Row with key [{0} - {1}] has an invalid Shipment Identifier", serviceRow.ShipmentIdentifier, serviceRow.ServiceCode));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var shipment = shipments[serviceRow.ShipmentIdentifier];
                    shipmentService.Shipment = shipment;
                    shipment.Services.Add(shipmentService);
                }


                // Generate Load Orders
                var loadOrderLines = package.GetLinesFromPackage(true, true, false, LoadOrderSheet);


                if (loadOrderLines.Any() && !ActiveUser.HasAccessToModify(ViewCode.LoadOrder))
                {
                    DisplayMessages(new[] { ValidationMessage.Error("You are attempting to import a load order but do not have permission to do so") });
                    return;
                }

                for (var row = 0; row < loadOrderLines.Count; row++)
                {
                    var loadOrderLine = loadOrderLines[row].ToList();

                    if (!loadOrderLine.Any() || loadOrderLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (loadOrderLine.Count != 55)
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Row with key [{0} - {1}] has incorrect column count", loadOrderLine[0], loadOrderLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }
                    var loadOrderRow = new LoadOrderRow(loadOrderLine);

                    if (!jobs.ContainsKey(loadOrderRow.JobIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Row with key [{0} - {1}] has an invalid Job Identifier", loadOrderRow.JobIdentifier, loadOrderRow.LoadOrderIdentifier, row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var matchingJob = jobs[loadOrderRow.JobIdentifier];

                    lastEntity = "Load Order";
                    lastKey = loadOrderRow.LoadOrderIdentifier;

                    var loadOrder = new LoadOrder
                    {
                        DateCreated = DateTime.Now,
                        User = ActiveUser,
                        TenantId = ActiveUser.TenantId,
                        Customer = matchingJob.Customer,
                        Prefix = matchingJob.Customer.Prefix,
                        ShipperBol = string.Empty,
                        ShipperReference = loadOrderRow.ShipperReference,
                        PurchaseOrderNumber = loadOrderRow.PurchaseOrderNumber,
                        HazardousMaterial = loadOrderRow.HazardousMaterial,
                        HazardousMaterialContactName = loadOrderRow.HazardousMaterialContactName,
                        HazardousMaterialContactPhone = loadOrderRow.HazardousMaterialContactPhone,
                        HazardousMaterialContactMobile = loadOrderRow.HazardousMaterialContactMobile,
                        HazardousMaterialContactEmail = loadOrderRow.HazardousMaterialContactEmail.StripSpacesFromEmails(),
                        DesiredPickupDate = loadOrderRow.DesiredPickupDate,
                        EarlyPickup = loadOrderRow.EarlyPickup,
                        LatePickup = loadOrderRow.LatePickup,
                        EstimatedDeliveryDate = loadOrderRow.EstimatedDeliveryDate,
                        EarlyDelivery = loadOrderRow.EarlyDelivery,
                        LateDelivery = loadOrderRow.LateDelivery,
                        ServiceMode = loadOrderRow.ServiceMode,
                        DeclineInsurance = loadOrderRow.DeclineAdditionalInsurance,
                        MiscField1 = loadOrderRow.MiscField1,
                        MiscField2 = loadOrderRow.MiscField2,
                        IsPartialTruckload = loadOrderRow.IsPartialTruckload,
                        JobStep = loadOrderRow.JobStep,
                        Items = new List<LoadOrderItem>(),
                        Vendors = new List<LoadOrderVendor>(),
                        Charges = new List<LoadOrderCharge>(),
                        Services = new List<LoadOrderService>(),
                        Equipments = new List<LoadOrderEquipment>(),
                        Stops = new List<LoadOrderLocation>(),
                        AccountBuckets = new List<LoadOrderAccountBucket>(),
                        CustomerReferences = new List<LoadOrderReference>(),
                        Documents = new List<LoadOrderDocument>(),
                        Notes = new List<LoadOrderNote>(),
                        SalesRepresentativeCommissionPercent = default(decimal),
                        SalesRepMinCommValue = default(decimal),
                        SalesRepMaxCommValue = default(decimal),
                        SalesRepAddlEntityName = string.Empty,
                        SalesRepAddlEntityCommPercent = default(decimal),
                        ResellerAdditionId = default(long),
                        BillReseller = false,
                        Status = LoadOrderStatus.Quoted,
                        ShipmentPriorityId = default(long),
                        LinearFootRuleBypassed = false,
                        CarrierCoordinatorId = default(long),
                        CriticalBolComments = string.Empty,
                        AccountBucketUnitId = default(long),
                        SalesRepresentativeId = default(long),
                        MileageSourceId = default(long),
                        Mileage = default(decimal),
                        EmptyMileage = default(decimal),
                        DriverName = string.Empty,
                        DriverPhoneNumber = string.Empty,
                        DriverTrailerNumber = string.Empty,
                        GeneralBolComments = string.Empty,
                        Description = string.Empty,
                        HidePrefix = false,
                        NotAvailableToLoadboards = false,
                        LoadOrderCoordinatorId = default(long)
                    };

                    if (loadOrder.Customer.SalesRepresentative != null)
                    {
                        var applicableTier = loadOrder.Customer.SalesRepresentative.ApplicableTier(loadOrder.DateCreated, ServiceMode.NotApplicable, loadOrder.Customer) ?? new SalesRepCommTier();

                        loadOrder.SalesRepresentative = loadOrder.Customer.SalesRepresentative;
                        loadOrder.SalesRepresentativeCommissionPercent = applicableTier.CommissionPercent;
                        loadOrder.SalesRepMinCommValue = applicableTier.FloorValue;
                        loadOrder.SalesRepMaxCommValue = applicableTier.CeilingValue;
                        loadOrder.SalesRepAddlEntityName = loadOrder.Customer.SalesRepresentative.AdditionalEntityName;
                        loadOrder.SalesRepAddlEntityCommPercent = loadOrder.Customer.SalesRepresentative.AdditionalEntityCommPercent;
                    }


                    if (loadOrder.Customer.Rating != null && loadOrder.Customer.Rating.ResellerAddition != null)
                    {
                        loadOrder.ResellerAddition = loadOrder.Customer.Rating.ResellerAddition;
                        loadOrder.BillReseller = loadOrder.Customer.Rating.BillReseller;
                    }

                    if (loadOrder.Customer.DefaultAccountBucket != null)
                    {
                        loadOrder.AccountBuckets = new List<LoadOrderAccountBucket>
                        {
                            new LoadOrderAccountBucket
                            {
                                AccountBucket = matchingJob.Customer.DefaultAccountBucket,
                                Primary = true,
                                LoadOrder = loadOrder,
                                TenantId = loadOrder.TenantId
                            }
                        };
                    }

                    loadOrder.Destination = new LoadOrderLocation
                    {
                        LoadOrder = loadOrder,
                        TenantId = loadOrder.TenantId,
                        Street1 = loadOrderRow.DestinationStreet1,
                        Street2 = loadOrderRow.DestinationStreet2,
                        City = loadOrderRow.DestinationCity,
                        PostalCode = loadOrderRow.DestinationPostalCode,
                        State = loadOrderRow.DestinationState,
                        CountryId = new CountrySearch().FetchCountryIdByCode(loadOrderRow.DestinationCountryCode),
                        Description = loadOrderRow.DestinationName,
                        SpecialInstructions = loadOrderRow.DestinationSpecialInstructions,
                        AppointmentDateTime = loadOrderRow.DestinationAppointmentDate <= DateUtility.SystemEarliestDateTime
                                                ? DateUtility.SystemEarliestDateTime
                                                : string.Format("{0} {1}", loadOrderRow.DestinationAppointmentDate.Date, loadOrderRow.DestinationAppointmentTime).ToDateTime(),
                        Direction = string.Empty,
                        GeneralInfo = string.Empty,
                        StopOrder = 1
                    };
                    loadOrder.Destination.Contacts = new List<LoadOrderContact>
                    {
                        new LoadOrderContact
                        {
                            Name = loadOrderRow.DestinationPrimaryContactName,
                            Phone = loadOrderRow.DestinationPrimaryContactPhone,
                            Email = loadOrderRow.DestinationPrimaryContactEmail.StripSpacesFromEmails(),
                            Mobile = loadOrderRow.DestinationPrimaryContactMobile,
                            Fax = loadOrderRow.DestinationPrimaryContactFax,
                            ContactTypeId = new ContactTypeSearch().FetchContactTypeIdByCode(loadOrderRow.DestinationPrimaryContactType, ActiveUser.TenantId),
                            Comment = loadOrderRow.DestinationPrimaryContactComment,
                            TenantId = loadOrder.TenantId,
                            Location = loadOrder.Destination,
                            Primary = true
                        }
                    };

                    loadOrder.Origin = new LoadOrderLocation
                    {
                        LoadOrder = loadOrder,
                        TenantId = loadOrder.TenantId,
                        Street1 = loadOrderRow.OriginStreet1,
                        Street2 = loadOrderRow.OriginStreet2,
                        City = loadOrderRow.OriginCity,
                        PostalCode = loadOrderRow.OriginPostalCode,
                        State = loadOrderRow.OriginState,
                        CountryId = new CountrySearch().FetchCountryIdByCode(loadOrderRow.OriginCountryCode),
                        Description = loadOrderRow.OriginName,
                        SpecialInstructions = loadOrderRow.OriginSpecialInstructions,
                        AppointmentDateTime = loadOrderRow.OriginAppointmentDate <= DateUtility.SystemEarliestDateTime
                                                ? DateUtility.SystemEarliestDateTime
                                                : string.Format("{0} {1}", loadOrderRow.OriginAppointmentDate.Date, loadOrderRow.OriginAppointmentTime).ToDateTime(),
                        Direction = string.Empty,
                        GeneralInfo = string.Empty,
                        StopOrder = default(int)

                    };
                    loadOrder.Origin.Contacts = new List<LoadOrderContact>
                    {
                        new LoadOrderContact
                        {
                            Name = loadOrderRow.OriginPrimaryContactName,
                            Phone = loadOrderRow.OriginPrimaryContactPhone,
                            Email = loadOrderRow.OriginPrimaryContactEmail.StripSpacesFromEmails(),
                            Mobile = loadOrderRow.OriginPrimaryContactMobile,
                            Fax = loadOrderRow.OriginPrimaryContactFax,
                            ContactTypeId = new ContactTypeSearch().FetchContactTypeIdByCode(loadOrderRow.OriginPrimaryContactType, ActiveUser.TenantId),
                            Comment = loadOrderRow.OriginPrimaryContactComment,
                            TenantId = loadOrder.TenantId,
                            Location = loadOrder.Origin,
                            Primary = true
                        }
                    };


                    if (loadOrders.ContainsKey(loadOrderRow.LoadOrderIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Row with key [{0}] is repeated in file", loadOrderRow.LoadOrderIdentifier));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    loadOrder.Job = matchingJob;
                    loadOrders.Add(loadOrderRow.LoadOrderIdentifier, loadOrder);
                }

                // Generate Load Order Items
                var loadOrderItemLines = package.GetLinesFromPackage(true, true, false, LoadOrderItemSheet);

                for (var row = 0; row < loadOrderItemLines.Count; row++)
                {
                    var itemLine = loadOrderItemLines[row].ToList();

                    if (!itemLine.Any() || itemLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Item Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (itemLine.Count != 16)
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Item Row with key [{0} - {1}] has incorrect column count", itemLine[0], itemLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var itemRow = new LoadOrderItemRow(itemLine);

                    lastEntity = "Load Order Item";
                    lastKey = itemRow.LoadOrderIdentifier;

                    var loadOrderItem = new LoadOrderItem
                    {
                        Description = itemRow.Description,
                        Comment = itemRow.Comment,
                        FreightClass = itemRow.FreightClass,
                        Weight = itemRow.ActualWeight,
                        Length = itemRow.ActualLength,
                        Width = itemRow.ActualWidth,
                        Height = itemRow.ActualHeight,
                        Quantity = itemRow.Quantity,
                        PieceCount = itemRow.PieceCount,
                        IsStackable = itemRow.IsStackable,
                        HazardousMaterial = itemRow.HazardousMaterial,
                        PackageTypeId = new PackageTypeSearch().FetchPackageTypeIdByTypeName(itemRow.PackageTypeName, ActiveUser.TenantId),
                        NMFCCode = itemRow.NMFCCode,
                        HTSCode = itemRow.HTSCode,
                        Value = itemRow.Value,
                        TenantId = ActiveUser.TenantId,
                        Delivery = 1,
                        Pickup = default(int)
                    };

                    if (!loadOrders.ContainsKey(itemRow.LoadOrderIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Item Row with key [{0} - {1}] has an invalid Shipment Identifier", itemRow.LoadOrderIdentifier, itemRow.Description));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var loadOrder = loadOrders[itemRow.LoadOrderIdentifier];
                    loadOrderItem.LoadOrder = loadOrder;
                    loadOrder.Items.Add(loadOrderItem);
                }

                // Generate Load Order Charges
                var loadOrderChargeLines = package.GetLinesFromPackage(true, true, false, LoadOrderChargeSheet);

                for (var row = 0; row < loadOrderChargeLines.Count; row++)
                {
                    var chargeLine = loadOrderChargeLines[row].ToList();

                    if (!chargeLine.Any() || chargeLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Charge Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (chargeLine.Count != 7)
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Charge Row with key [{0} - {1}] has incorrect column count", chargeLine[0], chargeLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var chargeRow = new LoadOrderChargeRow(chargeLine);

                    lastEntity = "Load Order Charge";
                    lastKey = chargeRow.LoadOrderIdentifier;

                    var loadOrderCharge = new LoadOrderCharge
                    {
                        ChargeCodeId = new ChargeCodeSearch().FetchChargeCodeIdByCode(chargeRow.ChargeCode, ActiveUser.TenantId),
                        Quantity = chargeRow.Quantity,
                        UnitBuy = chargeRow.UnitBuy,
                        UnitSell = chargeRow.UnitSell,
                        UnitDiscount = chargeRow.UnitDiscount,
                        Comment = chargeRow.Comment,
                        TenantId = ActiveUser.TenantId,
                        VendorId = default(long)
                    };

                    if (!loadOrders.ContainsKey(chargeRow.LoadOrderIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Charge Row with key [{0} - {1}] has an invalid Shipment Identifier", chargeRow.LoadOrderIdentifier, chargeRow.ChargeCode));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var loadOrder = loadOrders[chargeRow.LoadOrderIdentifier];
                    loadOrderCharge.LoadOrder = loadOrder;
                    loadOrder.Charges.Add(loadOrderCharge);
                }

                // Generate Load Order Vendors
                var loadOrderVendorLines = package.GetLinesFromPackage(true, true, false, LoadOrderVendorSheet);

                for (var row = 0; row < loadOrderVendorLines.Count; row++)
                {
                    var vendorLine = loadOrderVendorLines[row].ToList();

                    if (!vendorLine.Any() || vendorLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Vendor Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (vendorLine.Count != 4)
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Vendor Row with key [{0} - {1}] has incorrect column count", vendorLine[0], vendorLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var vendorRow = new LoadOrderVendorRow(vendorLine);

                    lastEntity = "Load Order Vendor";
                    lastKey = vendorRow.LoadOrderIdentifier;

                    var loadOrderVendor = new LoadOrderVendor
                    {
                        Vendor = new VendorSearch().FetchVendorByNumber(vendorRow.VendorNumber, ActiveUser.TenantId),
                        ProNumber = vendorRow.ProNumber,
                        Primary = vendorRow.IsPrimary,
                        TenantId = ActiveUser.TenantId
                    };

                    if (!loadOrders.ContainsKey(vendorRow.LoadOrderIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Vendor Row with key [{0} - {1}] has an invalid Shipment Identifier", vendorRow.LoadOrderIdentifier, vendorRow.VendorNumber));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var loadOrder = loadOrders[vendorRow.LoadOrderIdentifier];
                    loadOrderVendor.LoadOrder = loadOrder;
                    loadOrder.Vendors.Add(loadOrderVendor);
                }

                // Generate Load Order Equipment
                var loadOrderEquipmentLines = package.GetLinesFromPackage(true, true, false, LoadOrderEquipmentSheet);

                for (var row = 0; row < loadOrderEquipmentLines.Count; row++)
                {
                    var equipmentLine = loadOrderEquipmentLines[row].ToList();

                    if (!equipmentLine.Any() || equipmentLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Equipment Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (equipmentLine.Count != 2)
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Equipment Row with key [{0} - {1}] has incorrect column count", equipmentLine[0], equipmentLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var equipmentRow = new LoadOrderEquipmentRow(equipmentLine);

                    lastEntity = "Load Order Equipment";
                    lastKey = equipmentRow.LoadOrderIdentifier;

                    var shipmentEquipment = new LoadOrderEquipment
                    {
                        EquipmentType = new EquipmentTypeSearch().FetchEquipmentTypeByCode(equipmentRow.EquipmentCode, ActiveUser.TenantId),
                        TenantId = ActiveUser.TenantId,
                    };

                    if (!loadOrders.ContainsKey(equipmentRow.LoadOrderIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Equipment Row with key [{0} - {1}] has an invalid Shipment Identifier", equipmentRow.LoadOrderIdentifier, equipmentRow.EquipmentCode));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var loadOrder = loadOrders[equipmentRow.LoadOrderIdentifier];
                    shipmentEquipment.LoadOrder = loadOrder;
                    loadOrder.Equipments.Add(shipmentEquipment);
                }

                // Generate Load Order Services
                var loadOrderServiceLines = package.GetLinesFromPackage(true, true, false, LoadOrderServiceSheet);

                for (var row = 0; row < loadOrderServiceLines.Count; row++)
                {
                    var serviceLine = loadOrderServiceLines[row].ToList();

                    if (!serviceLine.Any() || serviceLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Service Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (serviceLine.Count != 2)
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Service Row with key [{0} - {1}] has incorrect column count", serviceLine[0], serviceLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var serviceRow = new LoadOrderServiceRow(serviceLine);

                    lastEntity = "Load Order Service";
                    lastKey = serviceRow.LoadOrderIdentifier;

                    var loadOrderService = new LoadOrderService
                    {
                        ServiceId = new ServiceSearch().FetchServiceIdByCode(serviceRow.ServiceCode, ActiveUser.TenantId),
                        TenantId = ActiveUser.TenantId
                    };

                    if (!loadOrders.ContainsKey(serviceRow.LoadOrderIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Load Order Service Row with key [{0} - {1}] has an invalid Shipment Identifier", serviceRow.LoadOrderIdentifier, serviceRow.ServiceCode));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var loadOrder = loadOrders[serviceRow.LoadOrderIdentifier];
                    loadOrderService.LoadOrder = loadOrder;
                    loadOrder.Services.Add(loadOrderService);
                }

                // Generate Service Tickets
                var serviceTicketLines = package.GetLinesFromPackage(true, true, false, ServiceTicketSheet);

                if (serviceTicketLines.Any() && !ActiveUser.HasAccessToModify(ViewCode.ServiceTicket))
                {
                    DisplayMessages(new[] { ValidationMessage.Error("You are attempting to import a service ticket but do not have permission to do so") });
                    return;
                }

                for (var row = 0; row < serviceTicketLines.Count; row++)
                {
                    var serviceTicketLine = serviceTicketLines[row].ToList();

                    if (!serviceTicketLine.Any() || serviceTicketLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (serviceTicketLine.Count != 6)
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Row with key [{0} - {1}] has incorrect column count", serviceTicketLine[0], serviceTicketLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var serviceTicketRow = new ServiceTicketRow(serviceTicketLine);

                    if (!jobs.ContainsKey(serviceTicketRow.JobIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Row with key [{0} - {1}] has an invalid Job Identifier", serviceTicketRow.JobIdentifier, serviceTicketRow.ServiceTicketIdentifier, row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var matchingJob = jobs[serviceTicketRow.JobIdentifier];

                    lastEntity = "Service Ticket";
                    lastKey = serviceTicketRow.ServiceTicketIdentifier;

                    var serviceTicket = new ServiceTicket
                    {
                        DateCreated = DateTime.Now,
                        User = ActiveUser,
                        TenantId = ActiveUser.TenantId,
                        Customer = matchingJob.Customer,
                        Prefix = matchingJob.Customer.Prefix,
                        AccountBucket = matchingJob.Customer.DefaultAccountBucket,
                        TicketDate = serviceTicketRow.TicketDate,
                        ExternalReference1 = serviceTicketRow.ExternalReference1,
                        ExternalReference2 = serviceTicketRow.ExternalReference2,
                        JobStep = serviceTicketRow.JobStep,
                        Items = new List<ServiceTicketItem>(),
                        Vendors = new List<ServiceTicketVendor>(),
                        Charges = new List<ServiceTicketCharge>(),
                        Services = new List<ServiceTicketService>(),
                        Equipments = new List<ServiceTicketEquipment>(),
                        Documents = new List<ServiceTicketDocument>(),
                        Notes = new List<ServiceTicketNote>(),
                        Assets = new List<ServiceTicketAsset>(),
                        Status = ServiceTicketStatus.Open,
                        HidePrefix = false,
                        AuditedForInvoicing = false,
                        OverrideCustomerLocationId = default(long),
                        SalesRepresentativeCommissionPercent = default(decimal),
                        SalesRepMinCommValue = default(decimal),
                        SalesRepMaxCommValue = default(decimal),
                        SalesRepAddlEntityName = string.Empty,
                        SalesRepAddlEntityCommPercent = default(decimal),
                        ResellerAdditionId = default(long),
                        BillReseller = false,
                        AccountBucketUnitId = default(long)
                    };

                    if (serviceTicket.Customer.SalesRepresentative != null)
                    {
                        var applicableTier = serviceTicket.Customer.SalesRepresentative.ApplicableTier(serviceTicket.DateCreated, ServiceMode.NotApplicable, serviceTicket.Customer) ?? new SalesRepCommTier();

                        serviceTicket.SalesRepresentativeCommissionPercent = applicableTier.CommissionPercent;
                        serviceTicket.SalesRepMinCommValue = applicableTier.FloorValue;
                        serviceTicket.SalesRepMaxCommValue = applicableTier.CeilingValue;
                        serviceTicket.SalesRepAddlEntityName = serviceTicket.Customer.SalesRepresentative.AdditionalEntityName;
                        serviceTicket.SalesRepAddlEntityCommPercent = serviceTicket.Customer.SalesRepresentative.AdditionalEntityCommPercent;
                    }


                    if (serviceTicket.Customer.Rating != null && serviceTicket.Customer.Rating.ResellerAddition != null)
                    {
                        serviceTicket.ResellerAddition = serviceTicket.Customer.Rating.ResellerAddition;
                        serviceTicket.BillReseller = serviceTicket.Customer.Rating.BillReseller;
                    }


                    if (serviceTickets.ContainsKey(serviceTicketRow.ServiceTicketIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Row with key [{0}] is repeated in file}", serviceTicketRow.ServiceTicketIdentifier));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    serviceTicket.Job = matchingJob;
                    serviceTickets.Add(serviceTicketRow.ServiceTicketIdentifier, serviceTicket);
                }

                // Generate Service Ticket Items
                var serviceTicketItemLines = package.GetLinesFromPackage(true, true, false, ServiceTicketItemSheet);

                for (var row = 0; row < serviceTicketItemLines.Count; row++)
                {
                    var itemLine = serviceTicketItemLines[row].ToList();

                    if (!itemLine.Any() || itemLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Item Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (itemLine.Count != 3)
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Item Row with key [{0} - {1}] has incorrect column count", itemLine[0], itemLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var itemRow = new ServiceTicketItemRow(itemLine);

                    lastEntity = "Service Ticket Item";
                    lastKey = itemRow.ServiceTicketIdentifier;

                    var serviceTicketItem = new ServiceTicketItem
                    {
                        Description = itemRow.Description,
                        Comment = itemRow.Comment,
                        TenantId = ActiveUser.TenantId
                    };

                    if (!serviceTickets.ContainsKey(itemRow.ServiceTicketIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Item Row with key [{0} - {1}] has an invalid Shipment Identifier", itemRow.ServiceTicketIdentifier, itemRow.Description));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var serviceTicket = serviceTickets[itemRow.ServiceTicketIdentifier];
                    serviceTicketItem.ServiceTicket = serviceTicket;
                    serviceTicket.Items.Add(serviceTicketItem);
                }

                // Generate Service Ticket Charges
                var serviceTicketChargeLines = package.GetLinesFromPackage(true, true, false, ServiceTicketChargeSheet);

                for (var row = 0; row < serviceTicketChargeLines.Count; row++)
                {
                    var chargeLine = serviceTicketChargeLines[row].ToList();

                    if (!chargeLine.Any() || chargeLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Charge Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (chargeLine.Count != 7)
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Charge Row with key [{0} - {1}] has incorrect column count", chargeLine[0], chargeLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var chargeRow = new ServiceTicketChargeRow(chargeLine);

                    lastEntity = "Service Ticket Charge";
                    lastKey = chargeRow.ServiceTicketIdentifier;

                    var serviceTicketCharge = new ServiceTicketCharge
                    {
                        ChargeCodeId = new ChargeCodeSearch().FetchChargeCodeIdByCode(chargeRow.ChargeCode, ActiveUser.TenantId),
                        Quantity = chargeRow.Quantity,
                        UnitBuy = chargeRow.UnitBuy,
                        UnitSell = chargeRow.UnitSell,
                        UnitDiscount = chargeRow.UnitDiscount,
                        Comment = chargeRow.Comment,
                        TenantId = ActiveUser.TenantId
                    };

                    if (!serviceTickets.ContainsKey(chargeRow.ServiceTicketIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Charge Row with key [{0} - {1}] has an invalid Service Ticket Identifier", chargeRow.ServiceTicketIdentifier, chargeRow.ChargeCode));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var serviceTicket = serviceTickets[chargeRow.ServiceTicketIdentifier];
                    serviceTicketCharge.ServiceTicket = serviceTicket;
                    serviceTicket.Charges.Add(serviceTicketCharge);
                }

                // Generate Service Ticket Vendors
                var serviceTicketVendorLines = package.GetLinesFromPackage(true, true, false, ServiceTicketVendorSheet);

                for (var row = 0; row < serviceTicketVendorLines.Count; row++)
                {
                    var vendorLine = serviceTicketVendorLines[row].ToList();

                    if (!vendorLine.Any() || vendorLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Vendor Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (vendorLine.Count != 3)
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Vendor Row with key [{0} - {1}] has incorrect column count", vendorLine[0], vendorLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var vendorRow = new ServiceTicketVendorRow(vendorLine);

                    lastEntity = "Service Ticket Vendor";
                    lastKey = vendorRow.ServiceTicketIdentifier;

                    var serviceTicketVendor = new ServiceTicketVendor
                    {
                        Vendor = new VendorSearch().FetchVendorByNumber(vendorRow.VendorNumber, ActiveUser.TenantId),
                        Primary = vendorRow.IsPrimary,
                        TenantId = ActiveUser.TenantId
                    };

                    if (!serviceTickets.ContainsKey(vendorRow.ServiceTicketIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Vendor Row with key [{0} - {1}] has an invalid Shipment Identifier", vendorRow.ServiceTicketIdentifier, vendorRow.VendorNumber));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var serviceTicket = serviceTickets[vendorRow.ServiceTicketIdentifier];
                    serviceTicketVendor.ServiceTicket = serviceTicket;
                    serviceTicket.Vendors.Add(serviceTicketVendor);
                }

                // Generate Service Ticket Equipment
                var serviceTicketEquipmentLines = package.GetLinesFromPackage(true, true, false, ServiceTicketEquipmentSheet);

                for (var row = 0; row < serviceTicketEquipmentLines.Count; row++)
                {
                    var equipmentLine = serviceTicketEquipmentLines[row].ToList();

                    if (!equipmentLine.Any() || equipmentLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Equipment Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (equipmentLine.Count != 2)
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Equipment Row with key [{0} - {1}] has incorrect column count", equipmentLine[0], equipmentLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var equipmentRow = new ServiceTicketEquipmentRow(equipmentLine);

                    lastEntity = "Service Ticket Equipment";
                    lastKey = equipmentRow.ServiceTicketIdentifier;

                    var serviceTicketEquipment = new ServiceTicketEquipment
                    {
                        EquipmentType = new EquipmentTypeSearch().FetchEquipmentTypeByCode(equipmentRow.EquipmentCode, ActiveUser.TenantId),
                        TenantId = ActiveUser.TenantId
                    };

                    if (!serviceTickets.ContainsKey(equipmentRow.ServiceTicketIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Equipment Row with key [{0} - {1}] has an invalid Shipment Identifier", equipmentRow.ServiceTicketIdentifier, equipmentRow.EquipmentCode));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var serviceTicket = serviceTickets[equipmentRow.ServiceTicketIdentifier];
                    serviceTicketEquipment.ServiceTicket = serviceTicket;
                    serviceTicket.Equipments.Add(serviceTicketEquipment);
                }

                // Generate Service Ticket Services
                var serviceTicketServiceLines = package.GetLinesFromPackage(true, true, false, ServiceTicketServiceSheet);

                for (var row = 0; row < serviceTicketServiceLines.Count; row++)
                {
                    var serviceLine = serviceTicketServiceLines[row].ToList();

                    if (!serviceLine.Any() || serviceLine.All(string.IsNullOrEmpty))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Service Row {0} is invalid", row + 2));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    if (serviceLine.Count != 2)
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Service Row with key [{0} - {1}] has incorrect column count", serviceLine[0], serviceLine[1]));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var serviceRow = new ServiceTicketServiceRow(serviceLine);

                    lastEntity = "Service Ticket Service";
                    lastKey = serviceRow.ServiceTicketIdentifier;

                    var serviceTicketService = new ServiceTicketService
                    {
                        ServiceId = new ServiceSearch().FetchServiceIdByCode(serviceRow.ServiceCode, ActiveUser.TenantId),
                        TenantId = ActiveUser.TenantId
                    };

                    if (!serviceTickets.ContainsKey(serviceRow.ServiceTicketIdentifier))
                    {
                        errorMessages.Add(ValidationMessage.Error("Service Ticket Service Row with key [{0} - {1}] has an invalid Shipment Identifier", serviceRow.ServiceTicketIdentifier, serviceRow.ServiceCode));
                        DisplayMessages(errorMessages);
                        return;
                    }

                    var serviceTicket = serviceTickets[serviceRow.ServiceTicketIdentifier];
                    serviceTicketService.ServiceTicket = serviceTicket;
                    serviceTicket.Services.Add(serviceTicketService);
                }
            }
            catch (Exception ex)
            {
                var validationMessages = new List<ValidationMessage> { ValidationMessage.Error(ex.Message) };

                if (lastEntity != string.Empty && lastKey != string.Empty)
                {
                    validationMessages.Insert(0, ValidationMessage.Error("An error occured with entity [{0}] and key [{1}]:", lastEntity, lastKey));
                }

                LogException(ex);
                DisplayMessages(validationMessages);
                return;
            }


            var importTuple = new Tuple<List<Job>, List<Shipment>, List<LoadOrder>, List<ServiceTicket>>(
                jobs.Select(j => j.Value).ToList(), shipments.Select(j => j.Value).ToList(),
                loadOrders.Select(j => j.Value).ToList(), serviceTickets.Select(j => j.Value).ToList());

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<Tuple<List<Job>, List<Shipment>, List<LoadOrder>, List<ServiceTicket>>>(importTuple));
        }

        private void ProcessJobUnLink(FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            if (lines.Count > 10000)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Batch import is limited to 10,000 records, please adjust your file size.") });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 2;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var shipments = new List<Shipment>();
            var loadOrders = new List<LoadOrder>();
            var serviceTickets = new List<ServiceTicket>();

            foreach (var line in chks.Select(c => c.Line))
            {
                if (line[0] == new Shipment().EntityName())
                {
                    if (!ActiveUser.HasAccessToModify(ViewCode.Shipment))
                    {
                        DisplayMessages(new[] { ValidationMessage.Error("You lack permission to unlink shipments") });
                        return;
                    }

                    var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(line[1], ActiveUser.TenantId);

                    if (shipment == null)
                    {
                        DisplayMessages(new[] { ValidationMessage.Error("Could not find Shipment [{0}] for Unlinking", line[1]) });
                        return;
                    }

                    shipment.LoadCollections();
                    shipment.TakeSnapShot();
                    shipment.JobId = default(long);
                    shipment.JobStep = default(int);
                    shipments.Add(shipment);
                }

                if (line[0] == new LoadOrder().EntityName())
                {
                    if (!ActiveUser.HasAccessToModify(ViewCode.LoadOrder))
                    {
                        DisplayMessages(new[] { ValidationMessage.Error("You lack permission to unlink load orders") });
                        return;
                    }

                    var loadOrder = new LoadOrderSearch().FetchLoadOrderByLoadNumber(line[1], ActiveUser.TenantId);

                    if (loadOrder == null)
                    {
                        DisplayMessages(new[] { ValidationMessage.Error("Could not find Load Order [{0}] for Unlinking", line[1]) });
                        return;
                    }

                    loadOrder.LoadCollections();
                    loadOrder.TakeSnapShot();
                    loadOrder.JobId = default(long);
                    loadOrder.JobStep = default(int);
                    loadOrders.Add(loadOrder);
                }

                if (line[0] == new ServiceTicket().EntityName())
                {
                    if (!ActiveUser.HasAccessToModify(ViewCode.ServiceTicket))
                    {
                        DisplayMessages(new[] { ValidationMessage.Error("You lack permission to unlink service tickets") });
                        return;
                    }

                    var serviceTicket = new ServiceTicketSearch().FetchServiceTicketByServiceTicketNumber(line[1], ActiveUser.TenantId);

                    if (serviceTicket == null)
                    {
                        DisplayMessages(new[] { ValidationMessage.Error("Could not find Service Ticket [{0}] for Unlinking", line[1]) });
                        return;
                    }

                    serviceTicket.LoadCollections();
                    serviceTicket.TakeSnapShot();
                    serviceTicket.JobId = default(long);
                    serviceTicket.JobStep = default(int);
                    serviceTickets.Add(serviceTicket);
                }
            }

            var unLinkTuple = new Tuple<List<Shipment>, List<LoadOrder>, List<ServiceTicket>>(shipments, loadOrders, serviceTickets);

            if (UnLinkEntitiesFromJob != null)
                UnLinkEntitiesFromJob(this, new ViewEventArgs<Tuple<List<Shipment>, List<LoadOrder>, List<ServiceTicket>>>(unLinkTuple));
        }

        private void GenerateAndExportImportTemplate()
        {
            var path = "JobImportTemplate.xlsx".AppendImportExportFolder();

            // EPPlus doesn't have functionality to let us modify a worksheet and export the data without saving back to the source file
            var tempPath = Server.MakeUniqueFilename(WebApplicationSettings.TempFolder + string.Format("{0:yyyyMMdd_hhmmss}_JobImportTemplate.xlsx", DateTime.Now));
            File.Copy(Server.MapPath(path), tempPath);

            var file = new FileInfo(tempPath);
            byte[] fileBytes;

            using (ExcelPackage templateBook = new ExcelPackage(file))
            {
                var equipmentTypesWorksheet = templateBook.Workbook.Worksheets["Equipment Types"];
                var equipmentTypeTable = new List<string[]> { new[] { "Code", "Type Name" } };

                foreach (var equipmentType in ProcessorVars.RegistryCache[ActiveUser.TenantId].EquipmentTypes.Where(eq => eq.Active))
                {
                    equipmentTypeTable.Add(new[] { equipmentType.Code, equipmentType.TypeName });
                }

                equipmentTypesWorksheet.Cells["A1"].LoadFromArrays(equipmentTypeTable);

                var chargeCodesWorksheet = templateBook.Workbook.Worksheets["Charge Codes"];
                var chargeCodeTable = new List<string[]> { new[] { "Code", "Description" } };

                foreach (var chargeCode in ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.Where(eq => eq.Active))
                {
                    chargeCodeTable.Add(new[] { chargeCode.Code, chargeCode.Description });
                }

                chargeCodesWorksheet.Cells["A1"].LoadFromArrays(chargeCodeTable);

                var serviceWorksheet = templateBook.Workbook.Worksheets["Services"];
                var serviceTable = new List<string[]> { new[] { "Code", "Description" } };

                foreach (var service in ProcessorVars.RegistryCache[ActiveUser.TenantId].Services)
                {
                    serviceTable.Add(new[] { service.Code, service.Description });
                }

                serviceWorksheet.Cells["A1"].LoadFromArrays(serviceTable);

                var freghtClassWorksheet = templateBook.Workbook.Worksheets["Freight Classes"];
                var freightClassTable = new List<string[]> { new[] { "Class" } };

                foreach (var freightClass in WebApplicationSettings.FreightClasses)
                {
                    freightClassTable.Add(new[] { freightClass.ToString() });
                }

                freghtClassWorksheet.Cells["A1"].LoadFromArrays(freightClassTable);

                var packageTypeWorksheet = templateBook.Workbook.Worksheets["Package Types"];
                var packageTypeTable = new List<string[]> { new[] { "Type Name" } };

                foreach (var packageType in ProcessorVars.RegistryCache[ActiveUser.TenantId].PackageTypes)
                {
                    packageTypeTable.Add(new[] { packageType.TypeName });
                }

                packageTypeWorksheet.Cells["A1"].LoadFromArrays(packageTypeTable);

                var contactTypeWorksheet = templateBook.Workbook.Worksheets["Contact Types"];
                var contactTypeTable = new List<string[]> { new[] { "Code", "Description" } };

                foreach (var contactType in ProcessorVars.RegistryCache[ActiveUser.TenantId].ContactTypes)
                {
                    contactTypeTable.Add(new[] { contactType.Code, contactType.Description });
                }

                contactTypeWorksheet.Cells["A1"].LoadFromArrays(contactTypeTable);

                var prefixesWorksheet = templateBook.Workbook.Worksheets["Prefixes"];
                var prefixesTable = new List<string[]> {new[] {"Prefix Code", "Description"}};

                foreach (var prefix in ProcessorVars.RegistryCache[ActiveUser.TenantId].Prefixes)
                {
                    prefixesTable.Add(new []{prefix.Code, prefix.Description});
                }

                prefixesWorksheet.Cells["A1"].LoadFromArrays(prefixesTable);

                fileBytes = templateBook.GetAsByteArray();
            }

            Response.Export(fileBytes, string.Format("{0}.xlsx", Guid.NewGuid()));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            new JobDashboardHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;


            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
            if (IsPostBack) return;

            // set auto referesh
            tmRefresh.Interval = WebApplicationConstants.DefaultRefreshInterval.ToInt();

            // set sort fields
            ddlSortBy.DataSource = OperationsSearchFields.JobSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = OperationsSearchFields.JobNumber.Name;


            //link up sort buttons
            lbtnSortJobNumber.CommandName = OperationsSearchFields.JobNumber.Name;
            lbtnSortDateCreated.CommandName = OperationsSearchFields.DateCreated.Name;
            lbtnSortCustomerNumber.CommandName = OperationsSearchFields.CustomerNumber.Name;
            lbtnSortCustomerName.CommandName = OperationsSearchFields.CustomerName.Name;
            lbtnSortUsername.CommandName = OperationsSearchFields.CreatedByUsername.Name;
            lbtnSortStatusText.CommandName = OperationsSearchFields.StatusText.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var criteria = new JobViewSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = profile != null
                                 ? profile.Columns
                                 : OperationsSearchFields.DefaultJobs.Select(f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case JobRemovalArgs:
                    hidImportingJob.Value = false.ToString();
                    fileUploader.Visible = true;
                    break;
                case ImportJobArgs:
                    hidImportingJob.Value = true.ToString();
                    fileUploader.Visible = true;
                    break;
                case JobUnlinkTemplate:
                    var path = "JobUnLinkImportTemplate.txt".AppendImportExportFolder();
                    Response.Export(Server.ReadFromFile(path));
                    break;
                case JobImportTemplate:
                    GenerateAndExportImportTemplate();
                    break;
            }
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnJobDashboardDetailsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var st = (JobDashboardDto)item.DataItem;

            if (st == null) return;

            var detailControl = (JobDashboardDetailControl)item.FindControl("JobDetailControl");

            detailControl.LoadJob(st);
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }


        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.Jobs.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.Jobs);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = OperationsSearchFields.JobSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            if (hidImportingJob.Value.ToBoolean())
            {
                ProcessJobImport(e.Stream);
            }
            else
            {
                ProcessJobUnLink(e);
            }

        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }



        protected void OnTimerTick(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }
    }
}