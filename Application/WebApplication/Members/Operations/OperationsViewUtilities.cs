﻿using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
	public static class OperationsViewUtilities
	{
		public static bool ShouldEnable(this Claim claim)
		{
			return claim != null && claim.Status == ClaimStatus.Open;
		}

		public static bool ShouldEnable(this Invoice invoice)
		{
			return invoice != null && (!invoice.Posted);
		}

		public static bool ShouldEnable(this ServiceTicket serviceTicket)
		{
			return serviceTicket != null && serviceTicket.Status == ServiceTicketStatus.Open;
		}

		public static bool ShouldEnable(this VendorBill vendorBill)
		{
			return vendorBill != null && (!vendorBill.Posted);
		}
	}
}
