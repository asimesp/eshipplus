﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ShipmentDashboardView.aspx.cs" EnableEventValidation="false"
    Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.ShipmentDashboardView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowNew="True" OnNew="OnToolbarNewClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                        Shipments Dashboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
        </div>

        <hr class="dark mb10" />
        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="ShipmentDashboard" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" ShowAutoRefresh="True" OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <label>Sort By:</label>
                                    <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                        OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="ddlSortBy" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstShipmentDetails" UpdatePanelToExtendId="upDataUpdate" />
                <eShip:ShippingLabelGeneratorControl runat="server" ID="shippingLabelGenerator" Visible="false" OnClose="OnShippingLabelGeneratorCloseClicked" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentDashboardTable" TableId="shipmentDashboardTable" HeaderZIndex="2" />
                <table class="line2 pl2" id="shipmentDashboardTable">
                    <tr>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortShipmentNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								<abbr title="Shipment Number">Shipment #</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDateCreated" OnCommand="OnSortData" CssClass="link_nounderline blue" CausesValidation="False">
								Date Created
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDesiredPickup" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Desired Pickup
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortActualPickup" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							  Actual Pickup
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortEstimatedDelivery" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							 Estimated Delivery
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortActualDelivery" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							  Actual Delivery
                            </asp:LinkButton>
                        </th>
                        <th style="width: 9%;">Cost
                        </th>
                        <th style="width: 9%;">Amt Due
                        </th>
                        <th style="width: 14%;">
                            <asp:LinkButton runat="server" ID="lbtnSortServiceMode" Text="Service Mode" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" />
                        </th>
                        <th style="width: 9%;">
                            <asp:LinkButton runat="server" ID="lbtnSortStatus" Text="Status" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" />
                        </th>
                        <th style="width: 7%;" class="text-center">Action</th>
                    </tr>
                    <asp:ListView runat="server" ID="lstShipmentDetails" OnItemDataBound="OnShipmentDetailsItemDataBound">
                        <ItemTemplate>
                            <eShip:ShipmentDashboardDetailControl ID="shipmentDashboardDetailControl" ItemIndex='<%# Container.DataItemIndex %>' runat="server" EnableOpen='<%# HasShipmentAccess %>'
                                OpenForShipmentModificaton="True" OnGenerateShippingLabel="OnGenerateShippingLabel" OnModifyShipment="OnShipmentDashboardDetailModifyShipment" />
                        </ItemTemplate>
                    </asp:ListView>
                </table>

                <asp:Panel runat="server" ID="pnlModifyShipment" CssClass="popup popup-position-fixed top50" Visible="False" DefaultButton="btnDoneModifyingShipment">
                    <div class="popheader">
                        <h4>
                            <asp:Literal runat="server" ID="litModifyShipmentTitle" />
                        </h4>
                        <asp:ImageButton runat="server" ID="ibtnCloseModifyShipment" OnClick="OnCloseModifyShipmentClicked" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClientClick="ShowProcessingDivBlock();" />
                    </div>
                    <asp:Panel runat="server" ID="pnlModifyShipmentBody">
                        <div class="row">
                            <div class="fieldgroup errorMsgLit">
                                <asp:Literal ID="litErrorMessages" runat="server" />
                            </div>
                        </div>
                        <div class="row pt20 pb10">
                            <div class="col_1_2 bbox pl20">
                                <div class="row">
                                    <h5 class="pl40">General Information</h5>
                                    <table class="poptable">
                                        <tr>
                                            <td class="text-right pb20">
                                                <label class="upper">ProNumber:</label>
                                            </td>
                                            <td class="pb20">
                                                <eShip:CustomTextBox runat="server" ID="txtProNumber" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Desired Pickup Date:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtDesiredPickupDate" placeholder="99/99/9999" Type="Date" CssClass="w100" />
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trActualPickupDate">
                                            <td class="text-right">
                                                <label class="upper">Actual Pickup Date:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtActualPickupDate" placeholder="99/99/9999" Type="Date" CssClass="w100" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Estimated Delivery Date:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtEstimatedDeliveryDate" placeholder="99/99/9999" Type="Date" CssClass="w100" />
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trActualDeliveryDate">
                                            <td class="text-right">
                                                <label class="upper">Actual Delivery Date:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtActualDeliveryDate" placeholder="99/99/9999" Type="Date" CssClass="w100" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col_1_2 bbox vlinedarkleft pl40 pb10">
                                <div class="row">
                                    <asp:Panel runat="server" ID="pnlCreateCheckCall">
                                        <h5 class="pl20">Create Check Call</h5>
                                        <table class="poptable">
                                            <tr>
                                                <td class="text-right" style="width: 30%">
                                                    <label class="upper">Event Date:</label>
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox runat="server" ID="txtEventDate" placeholder="99/99/9999" Type="Date" CssClass="w100" />
                                                    <eShip:CustomTextBox runat="server" ID="txtEventTime" CssClass="w55" MaxLength="5" placeholder="99:99" Type="Time" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <label class="upper">EDI Status Code:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlEdiStatusCode" DataValueField="Value" DataTextField="Text" CssClass="w330" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right top">
                                                    <label class="upper lhInherit">
                                                        Call Notes:
                                                    </label>
                                                    <br />
                                                    <label class="upper lhInherit">
                                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tfheCallNotes" TargetControlId="txtCallNotes" MaxLength="500" />
                                                    </label>
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtCallNotes" runat="server" TextMode="MultiLine" CssClass="w330" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <h5 class="pl20">Create Note</h5>
                                    <table class="poptable">
                                        <tr>
                                            <td class="text-right" style="width: 30%">
                                                <label class="upper">Note Type:</label>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlNoteType" DataValueField="Value" DataTextField="Text" CssClass="w110" />
                                                <eShip:AltUniformCheckBox runat="server" ID="chkArchived" Checked='<%# Eval("Archived") %>' />
                                                <label>Archived&nbsp;</label>
                                                <eShip:AltUniformCheckBox runat="server" ID="chkClassified" Checked='<%# Eval("Classified") %>' />
                                                <label>Classified</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper lhInherit">Message</label>
                                                <br />
                                                <label class="upper lhInherit">
                                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleMessage" MaxLength="500" TargetControlId="txtMessage" />
                                                </label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtMessage" runat="server" TextMode="MultiLine" CssClass="w330" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td class="pt10">
                                                <asp:Button runat="server" ID="btnDoneModifyingShipment" OnClick="OnDoneModifyShipmentClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Save" />
                                                <asp:Button runat="server" ID="btnCloseModifyShipment" OnClick="OnCloseModifyShipmentClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Cancel" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>

                <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortShipmentNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortDateCreated" />
                <asp:PostBackTrigger ControlID="lbtnSortDesiredPickup" />
                <asp:PostBackTrigger ControlID="lbtnSortActualPickup" />
                <asp:PostBackTrigger ControlID="lbtnSortEstimatedDelivery" />
                <asp:PostBackTrigger ControlID="lbtnSortActualDelivery" />
                <asp:PostBackTrigger ControlID="lbtnSortServiceMode" />
                <asp:PostBackTrigger ControlID="lbtnSortStatus" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <asp:UpdatePanel runat="server" ID="udpHiddenFields" UpdateMode="Always">
        <ContentTemplate>
            <eShip:CustomHiddenField runat="server" ID="hidEditLoadOrderId" />
            <eShip:CustomHiddenField runat="server" ID="hidEditShipmentId" />

            <eShip:CustomHiddenField runat="server" ID="hidFlag" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    <eShip:CustomHiddenField runat="server" ID="hidLastShipmentBolId" />
</asp:Content>
