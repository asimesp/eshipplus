﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.MacroPoint;
using LogisticsPlus.Eship.PluginBase.Mileage;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.InputControls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using ShipmentStatus = LogisticsPlus.Eship.Core.Operations.ShipmentStatus;
using LogisticsPlus.Eship.Smc.Eva;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class ShipmentView : MemberPageBaseWithPageStore, IShipmentView, IVendorRejectionLogProcView,
                                        ILocationCollectionResource
    {
        private const string ItemsHeader = "Items";
        private const string ChargesHeader = "Charges";
        private const string DocumentsHeader = "Documents";
        private const string StopsHeader = "Stops";

        private const string OriginStopText = "Origin";
        private const string DestinationStopText = "Destination";

        private const string RouteSummaryArgs = "RouteSummary";
        private const string ReturnArgs = "Return";
        private const string ReturnShipmentToBeInvoicedArgs = "ReturnToShipmentToBeInvoiced";
        private const string VoidArgs = "Void";
        private const string ReverseVoidArgs = "ReverseVoid";
        private const string DuplicateArgs = "Duplicate";
        private const string ConfirmChargesArgs = "ConfirmCharges";
        private const string DuplicateReserseArgs = "DuplicateReverse";
        private const string VendorLaneHistoryArgs = "VendorLaneHistory";
        private const string BolArgs = "BOL";
        private const string VendorRateAgreementArgs = "RateAgreement";
        private const string ShippingLabelArgs = "ShippingLabel";
        private const string EditInvoicedShipmentArgs = "EditInvoicedShipment";
        private const string MacroPointTrackingStartArgs = "StartMacroPointTrack";
        private const string MacroPointTrackingStopArgs = "StopMacroPointTrack";
        private const string MacroPointRequestLocationUpdateArgs = "RequestLocationUpdate";
        private const string PayForShipmentWithCreditCardArgs = "PayForShipmentWithCreditCard";
        private const string RefundCustomerPaymentsArgs = "RefundCustomerPayments";
        private const string DispatchShipmentArgs = "DispatchShipment";
        private const string InitiateSmc3EvaTrackingArgs = "InitiateSmc3EvaTracking";
        private const string InitiateProject44TrackingArgs = "InitiateProject44TrackingArgs";
        protected const string MessageBoxYesEventArg = "MBYEA";

        private const string SaveFlag = "S";
        private const string SmallPackageInitialSaveFlag = "SPIS";
        private const string SmallPackageFinalSaveFlag = "SPFS";
        private const string NonPrimaryVendorRejectLogFlag = "NPVRLF";
        private const string DeleteFlag = "D";
        private const string VendorUpdateFlag = "V";
        private const string PrimaryVendorRejectLogFlag = "PVRLF";
        private const string CarrierCoordinatorFlag = "CarrierCoordinator";
        private const string ShipmentCoordinatorFlag = "ShipmentCoordinatorFlag";
        private const string VoidShipmentFlag = "VS";
        private const string ReverseVoidShipmentFlag = "RVS";

        private const string MacroPointRequestInitialized = "Request Initialized";

        private const string LatLngPointsStoreKey = "LLPSK";

        private const string ShipmentNoteTypesKey = "ShipmentsNoteTypeKey";
        private const string DriverAssetTypesKey = "DriverAssetTypesKey";
        private const string TractorAssetTypesKey = "TractorAssetTypesKey";
        private const string TrailerAssetTypesKey = "TrailerAssetTypesKey";
        private const string MileageEngineKey = "MileageEngineKey";


        private const int ViewDeliveryStopOrder = 10000;


        private bool _originApptChanged;
        private bool _destApptChanged;
        private bool _doNotRemoveNonPrimaryVendor;
        private bool _shipmentDocRtrvLogRequiredIfApplicable;

        public bool RefundOrVoidMiscReceipts { get; set; }


        private List<double[]> StoredLatLngCoordinates
        {
            get
            {
                var store = PageStore[LatLngPointsStoreKey] as List<double[]>;
                if (store == null) PageStore[LatLngPointsStoreKey] = new List<double[]>();
                return PageStore[LatLngPointsStoreKey] as List<double[]>;
            }
            set { PageStore[LatLngPointsStoreKey] = value; }
        }


        public bool SaveOriginToAddressBook
        {
            get { return chkSaveOrigin.Checked; }
        }

        public bool SaveDestinationToAddressBook
        {
            get { return chkSaveDestination.Checked; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Operations/ShipmentView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.Shipment; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        protected List<ViewListItem> NoteTypeCollection
        {
            get { return PageStore[ShipmentNoteTypesKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
        }

        protected List<ViewListItem> DriveAssetCollection
        {
            get { return PageStore[DriverAssetTypesKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
        }

        protected List<ViewListItem> TractorAssetCollection
        {
            get { return PageStore[TractorAssetTypesKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
        }

        protected List<ViewListItem> TrailerAssetCollection
        {
            get { return PageStore[TrailerAssetTypesKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
        }

        protected List<ViewListItem> MileageEngineCollection
        {
            get { return PageStore[MileageEngineKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
        }

        public List<ViewListItem> VendorCollection
        {
            get
            {
                var viewListVendorItems = new List<ViewListItem>();

                if (lstVendors.Items.Any())
                {
                    viewListVendorItems.AddRange(lstVendors.Items.Select(c => new ViewListItem
                    {
                        Text = c.FindControl("txtName").ToTextBox().Text,
                        Value = c.FindControl("hidVendorId").ToCustomHiddenField().Value
                    }).ToList());
                }

                viewListVendorItems.Insert(0, new ViewListItem(WebApplicationConstants.AutoDefault, default(long).ToString()));

                if (hidPrimaryVendorId.Value != null)
                {
                    var primaryId = hidPrimaryVendorId.Value.ToLong();
                    var primaryVendor = new Vendor(primaryId, primaryId != default(long));

                    if (viewListVendorItems.All(w => w.Value.ToLong() != primaryVendor.Id))
                    {
                        viewListVendorItems.Insert(1, new ViewListItem { Text = primaryVendor.Name, Value = primaryVendor.Id.ToString() });
                    }
                }

                return viewListVendorItems;
            }
        }

        public List<ViewListItem> ContactTypeCollection
        {
            get
            {
                var viewListItems = ProcessorVars.RegistryCache[ActiveUser.TenantId]
                    .ContactTypes
                    .OrderBy(c => c.Description)
                    .Select(t => new ViewListItem(t.Code, t.Id.ToString()))
                    .ToList();
                viewListItems.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));
                return viewListItems;
            }
        }

        public List<ViewListItem> CountryCollection
        {
            get
            {
                var viewListItems = ProcessorVars.RegistryCache.Countries
                                                 .Values
                                                 .OrderByDescending(t => t.SortWeight)
                                                 .ThenBy(t => t.Name)
                                                 .Select(c => new ViewListItem(c.Name, c.Id.ToString()))
                                                 .ToList();
                viewListItems.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));
                return viewListItems;
            }
        }

        public List<ServiceViewSearchDto> Services
        {
            set
            {
                var services = value
                    .OrderBy(s => s.Description)
                    .Select(c => new ViewListItem(c.FormattedString(), c.Id.ToString()))
                    .ToList();

                rptServices.DataSource = services;
                rptServices.DataBind();
            }
        }

        public List<EquipmentType> EquipmentTypes
        {
            set
            {
                var equipmentTypes = value
                    .OrderBy(e => e.TypeName)
                    .Select(c => new ViewListItem("  " + c.FormattedString(), c.Id.ToString()))
                    .ToList();

                rptEquipmentTypes.DataSource = equipmentTypes;
                rptEquipmentTypes.DataBind();
            }
        }

        public List<Asset> Assets
        {
            set
            {
                var driverAssets = value
                    .Where(c => c.AssetType == AssetType.Driver)
                    .Select(t => new ViewListItem(t.AssetNumber + " - " + t.Description, t.Id.ToString()))
                    .ToList();

                var tractorAssets = value
                    .Where(
                        c =>
                        c.AssetType == AssetType.IndependentTractor || c.AssetType == AssetType.OwnerOpTractor ||
                        c.AssetType == AssetType.CompanyTractor)
                    .Select(t => new ViewListItem(t.AssetNumber + " - " + t.Description, t.Id.ToString()))
                    .ToList();

                var trailorAssets = value
                    .Where(c => c.AssetType == AssetType.Trailer)
                    .Select(t => new ViewListItem(t.AssetNumber + " - " + t.Description, t.Id.ToString()))
                    .ToList();


                var item = new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString());
                driverAssets.Insert(0, item);
                tractorAssets.Insert(0, item);
                trailorAssets.Insert(0, item);

                PageStore[DriverAssetTypesKey] = driverAssets;
                PageStore[TractorAssetTypesKey] = tractorAssets;
                PageStore[TrailerAssetTypesKey] = trailorAssets;
            }
        }

        public Dictionary<int, string> DateUnits
        {
            set { vpscVendorPerformance.DateUnits = value; }
        }

        public Dictionary<int, string> InDisputeReasons
        {
            set
            {
                ddlInDisputeReason.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                ddlInDisputeReason.DataBind();
            }
        }

        public Dictionary<int, string> NoteTypes
        {
            set
            {
                var types = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).OrderBy(t => t.Text).ToList();
                PageStore[ShipmentNoteTypesKey] = types;
            }
        }

        public Dictionary<int, string> ServiceModes
        {
            set
            {
                ddlServiceMode.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                ddlServiceMode.DataBind();
            }
        }

        public Dictionary<int, string> MileageEngines
        {
            set
            {
                PageStore[MileageEngineKey] = value
                    .Select(t => new ViewListItem(t.Value, t.Key.ToString()))
                    .OrderBy(t => t.Text)
                    .ToList();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Shipment>> Save;
        public event EventHandler<ViewEventArgs<Shipment>> Delete;
        public event EventHandler<ViewEventArgs<Shipment>> Lock;
        public event EventHandler<ViewEventArgs<Shipment>> UnLock;
        public event EventHandler<ViewEventArgs<Shipment>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<string>> VendorSearch;
        public event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;
        public event EventHandler<ViewEventArgs<SMC3TrackRequestResponse>> SaveSmc3TrackRequestResponse;
        public event EventHandler<ViewEventArgs<Project44TrackingResponse>> SaveProject44TrackRequestResponse;
        public event EventHandler<ViewEventArgs<VendorRejectionLog>> LogVendorRejection;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            // handle small package processing prior to save flag process. NOTE: must happen first!
            if (!messages.HasErrors() && hidFlag.Value == SmallPackageInitialSaveFlag)
            {
                ProcessSmallPackageInitialBookingFinalization();
                return;
            }


            if (!messages.HasErrors() && (hidFlag.Value == SaveFlag || hidFlag.Value == SmallPackageFinalSaveFlag))
            {
                // clean up file cleared out!
                if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
                {
                    Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                    hidFilesToDelete.Value = string.Empty;
                }

                var shipment = new Shipment(hidShipmentId.Value.ToLong(), false);

                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Shipment>(shipment));
                memberToolBar.ShowUnlock = false;

                // send notifications
                if (!string.IsNullOrEmpty(hidNotificationsRequired.Value))
                {
                    ProcessNotifications(shipment);
                    hidNotificationsRequired.Value = string.Empty;
                }

                // if small package finialization, show shipping labels for printing small package label
                if (hidFlag.Value == SmallPackageFinalSaveFlag)
                    LoadShipmentLabelGenerator();

                SetEditStatus(false);
                hidFlag.Value = null;
            }

            if (hidFlag.Value == PrimaryVendorRejectLogFlag)
            {
                hidFlag.Value = null;
                if (!messages.HasErrors()) DisplayVendor(null);
            }

            if (hidFlag.Value == NonPrimaryVendorRejectLogFlag)
            {
                hidFlag.Value = null;
                _doNotRemoveNonPrimaryVendor = messages.HasErrors();
                litVendorsErrorMsg.Text = messages.HasErrors()
                                              ? string.Join(WebApplicationConstants.HtmlBreak,
                                                            messages.Where(m => m.Type == ValidateMessageType.Error)
                                                                    .OrderBy(m => m.Type)
                                                                    .Select(m => m.Message).ToArray())
                                              : string.Empty;
            }
            else litVendorsErrorMsg.Text = string.Empty;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak,
                                             messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Button = MessageButton.Ok;

            var scriptManager = ScriptManager.GetCurrent(Page);
            if (scriptManager != null && IsPostBack && scriptManager.IsInAsyncPostBack)
                // disregard showing if in partial post back as message box is not in update panel
                messageBox.Visible = false;
            else
                messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                               Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            pnlDimScreen.Visible = false;
            pnlEditInvoicedShipment.Visible = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(Shipment shipment)
        {
            if (shipment.IsNew && hidFlag.Value == DeleteFlag)
            {
                Server.RemoveDirectory(WebApplicationSettings.ShipmentFolder(ActiveUser.TenantId, hidShipmentId.Value.ToLong()));
                hidFlag.Value = string.Empty;
            }

            LoadShipment(shipment);
            SetEditStatus(!shipment.IsNew);

            // check for shopped rate
            if (hidShoppedRateId.Value.ToLong() != default(long))
            {
                var shoppedRate = new ShoppedRate(hidShoppedRateId.Value.ToLong());
                if (shoppedRate.KeyLoaded && SaveShoppedRate != null)
                {
                    hidShoppedRateId.Value = string.Empty;
                    shoppedRate.TakeSnapShot();
                    shoppedRate.Shipment = shipment;
                    shoppedRate.ReferenceNumber = shipment.ShipmentNumber;
                    SaveShoppedRate(this, new ViewEventArgs<ShoppedRate>(shoppedRate));
                }
            }

            // check for document retrieval log
            if (_shipmentDocRtrvLogRequiredIfApplicable) shipment.InsertShipmentDocRtrvLogFor(ActiveUser);
        }

        public void Set(ShoppedRate shoppedRate)
        {
            hidShoppedRateId.Value = shoppedRate.Id.ToString();
        }

        public void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers = false)
        {
            var oldCustomerId = hidCustomerId.Value; // preserve old customer id;

            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
            hidCustomerOutstandingBalance.Value = customer == null
                                                      ? default(decimal).ToString()
                                                      : customer.OutstandingBalance().ToString();
            hidCustomerCredit.Value = customer == null ? default(decimal).ToString() : customer.CreditLimit.ToString();
            pnlCashOnlyCustomer.Visible = customer != null && customer.IsCashOnly;
            divNotifyPrimaryDestination.Visible = customer?.AllowLocationContactNotification ?? false;
            divNotifyPrimaryOrigin.Visible = customer?.AllowLocationContactNotification ?? false;


            // require customer reconfigurations!!!
            CheckForCreditAlert(lstCharges.Items.Sum(i => (i.FindControl("txtAmountDue").ToTextBox()).Text.ToDecimal()));

            var customerIdSpecificFilter = customer == null ? default(long) : customer.Id;
            libraryItemFinder.CustomerIdSpecificFilter = customerIdSpecificFilter;
            libraryItemFinder.Reset();
            addressBookFinder.CustomerIdSpecificFilter = customerIdSpecificFilter;
            operationsAddressInputOrigin.SetCustomerIdSpecificFilter(customerIdSpecificFilter);
            operationsAddressInputDestination.SetCustomerIdSpecificFilter(customerIdSpecificFilter);

            if (hidAccountBucketId.Value.ToLong() == default(long) && customer != null)
                DisplayAccountBucket(customer.DefaultAccountBucket);

            if (hidPrefixId.Value.ToLong() == default(long) && customer != null && customer.PrefixId != default(long))
                DisplayPrefix(customer.Prefix);

            if (customer != null)
                ddlMileageSource.SelectedValue = customer.RequiredMileageSourceId.GetString();

            // use of care of address
            chkCareOfAdddressFormatEnabled.Checked = customer != null && customer.CareOfAddressFormatEnabled;

            // sales rep
            if (updateSalesRepAndResellers) DisplayCustomerSalesRepresentative(customer);

            if (customer != null)
            {
                // if change in customer, update insurance status
                if (customer.Rating != null)
                {
                    if (oldCustomerId != hidCustomerId.Value)
                        chkDeclineInsurance.Checked = !customer.Rating.InsuranceEnabled;

                    if (updateSalesRepAndResellers)
                        DisplayResellerAddition(customer.Rating.ResellerAddition, customer.Rating.BillReseller);

                    btnRetrieveDetails.Enabled = customer.Rating.ResellerAddition != null;
                    btnClearDetails.Enabled = customer.Rating.ResellerAddition != null;
                }
                else
                {
                    if (updateSalesRepAndResellers)
                        DisplayResellerAddition(null, false);

                    btnRetrieveDetails.Enabled = false;
                    btnClearDetails.Enabled = false;
                }

                LoadCustomerCustomFields(customer);

                // if change in customer, reset CSR link, remove auto-rated status
                if (oldCustomerId != hidCustomerId.Value)
                {
                    memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));
                    chkAutoRated.Checked = false;
                }
            }
            else
            {
                btnRetrieveDetails.Enabled = false;
                btnClearDetails.Enabled = false;
            }
        }

        public void DisplayVendor(Vendor vendor)
        {
            if (hidFlag.Value == VendorUpdateFlag)
            {
                chkAutoRated.Checked = false; // if changing vendor manually, then not autorated!
                if (ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>() == ServiceMode.LessThanTruckload)
                {
                    hidUpdateTerminalInfo.Value = true.ToString();
                    litOriginTerminal.Text = string.Empty;
                    litDestinationTerminal.Text = string.Empty;
                }
                if (vendor != null &&
                    ProcessorVars.VendorInsuranceAlertThresholdDays.ContainsKey(vendor.TenantId) &&
                    vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId]))
                {
                    DisplayMessages(new[]
                        {
                            ValidationMessage.Error(string.Format
                                                        (
                                                            "{0} - {1} Cannot be primary. Reason: {2}",
                                                            vendor.VendorNumber,
                                                            vendor.Name,
                                                            vendor.VendorInsuranceAlertText(
                                                                ProcessorVars.VendorInsuranceAlertThresholdDays[
                                                                    vendor.TenantId])))
                        });
                    vendor = null;
                    hidFlag.Value = null;
                }

                if (vendor != null) CheckVendorConstraints(vendor);
            }

            var oldPrimaryVendorId = hidPrimaryVendorId.Value.ToLong();

            txtVendorName.Text = vendor == null ? string.Empty : vendor.Name;
            txtVendorNumber.Text = vendor == null ? string.Empty : vendor.VendorNumber;
            hidPrimaryVendorId.Value = vendor == null ? default(long).ToString() : vendor.Id.ToString();
            hidCanDispatch.Value = vendor != null && vendor.Communication != null && vendor.Communication.ShipmentDispatchEnabled && ((chkGuaranteedDeliveryService.Checked && !vendor.Communication.DisableGuaranteedServiceDispatch) || !chkGuaranteedDeliveryService.Checked)
                                       ? true.GetString()
                                       : false.GetString();
            ibtnLogVendorRejection.Enabled = vendor != null;
            ibtnViewVendorPerformanceStatistics.Visible = vendor != null && !vendor.IsNew;

            var ddlVendorsList = lstCharges.Items.Select(c => new
            {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField(),
                VendorBillNumber = c.FindControl("txtVendorBillNumber").ToTextBox()
            }).ToList();

            if (vendor == null || !ProcessorVars.VendorInsuranceAlertThresholdDays.ContainsKey(vendor.TenantId))
            {
                ddlVendorsList.ForEach(cc =>
                {
                    if (string.IsNullOrEmpty(cc.VendorBillNumber.Text))
                    {
                        cc.Vendors.Items.Clear();
                        BuildVendorsDropdownListOnChargesTab(cc.Vendors, cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId ? 0 : cc.SelectedVendorId.Value.ToLong(), true);
                        cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId ? "0" : cc.SelectedVendorId.Value;
                    }
                });
                upPnlCharges.Update();

                pnlPrimaryVendorInsuranceAlert.Visible = false;
                litPrimaryVendorInsuranceAlert.Text = string.Empty;

                return;
            }

            pnlPrimaryVendorInsuranceAlert.Visible =
                vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId]);
            litPrimaryVendorInsuranceAlert.Text =
                vendor.VendorInsuranceAlertText(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId]);

            ddlVendorsList.ForEach(cc =>
            {
                if (string.IsNullOrEmpty(cc.VendorBillNumber.Text))
                {
                    cc.Vendors.Items.Clear();
                    BuildVendorsDropdownListOnChargesTab(
                        cc.Vendors,
                        cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId && oldPrimaryVendorId != vendor.Id ? vendor.Id : cc.SelectedVendorId.Value.ToLong(),
                        string.IsNullOrEmpty(cc.VendorBillNumber.Text));
                    cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == oldPrimaryVendorId && oldPrimaryVendorId != vendor.Id ? vendor.Id.ToString() : cc.SelectedVendorId.Value;
                }
            });
            upPnlCharges.Update();
        }



        private void DisplayCustomerSalesRepresentative(Customer customer)
        {
            var representative = customer == null ? null : customer.SalesRepresentative;

            var tier = representative == null
                           ? null
                           : representative.ApplicableTier(txtDateCreated.Text.ToDateTime(),
                                                           ddlServiceMode.SelectedValue.ToEnum<ServiceMode>(), customer);

            var commPercent = tier == null ? string.Empty : tier.CommissionPercent.ToString("f4");
            var floorValue = tier == null ? string.Empty : tier.FloorValue.ToString("f4");
            var ceilingValue = tier == null ? string.Empty : tier.CeilingValue.ToString("f4");
            var addlEntityName = representative == null ? string.Empty : representative.AdditionalEntityName;
            var addlEntityCommPercent = representative == null
                                            ? string.Empty
                                            : representative.AdditionalEntityCommPercent.ToString("f4");
            DisplaySalesRepresentative(representative, commPercent, floorValue, ceilingValue, addlEntityName,
                                       addlEntityCommPercent);
        }

        private void DisplaySalesRepresentative(SalesRepresentative representative, string commPercent, string floorValue,
                                                string ceilingValue, string addlEntityName, string addlEntityCommPercent)
        {
            hidSalesRepresentativeId.Value = representative != null ? representative.Id.GetString() : string.Empty;

            var canSeeSalesRepData = ActiveUser.HasAccessTo(ViewCode.CanSeeSalesRepFinancialData);

            txtSalesRepresentativeCommissionPercent.Text = canSeeSalesRepData ? commPercent : WebApplicationConstants.OnFile;
            hidSalesRepresentativeCommissionPercent.Value = commPercent;

            txtSalesRepresentative.Text = representative != null
                                              ? string.Format("{0} - {1}", representative.SalesRepresentativeNumber,
                                                              representative.Name)
                                              : string.Empty;

            txtSalesRepresentativeAddlEntityName.Text = addlEntityName;

            txtSalesRepresentativeAddlEntityCommPercent.Text = canSeeSalesRepData
                                                                   ? addlEntityCommPercent
                                                                   : WebApplicationConstants.OnFile;
            txtSalesRepresentativeMinCommValue.Text = canSeeSalesRepData ? floorValue : WebApplicationConstants.OnFile;
            txtSalesRepresentativeMaxCommValue.Text = canSeeSalesRepData ? ceilingValue : WebApplicationConstants.OnFile;

            hidSalesRepresentativeAddlEntityCommPercent.Value = addlEntityCommPercent;
            hidSalesRepresentativeMinCommValue.Value = floorValue;
            hidSalesRepresentativeMaxCommValue.Value = ceilingValue;
        }

        private void DisplayResellerAddition(ResellerAddition reseller, bool billReseller)
        {
            if (reseller != null)
            {
                hidResellerAdditionId.Value = reseller.Id.ToString();
                litResellerAdditionName.Text = reseller.Name;
                hidBillReseller.Value = billReseller.ToString();
                litBillReseller.Text = billReseller ? "Billing: " : string.Empty;
                rptResellerAdditions.DataSource = RetrieveResellerAdditionData(reseller);
                rptResellerAdditions.DataBind();
            }
            else
            {
                hidResellerAdditionId.Value = default(long).ToString();
                litResellerAdditionName.Text = string.Empty;
                hidBillReseller.Value = false.ToString();
                litBillReseller.Text = string.Empty;
                rptResellerAdditions.DataSource = null;
                rptResellerAdditions.DataBind();
            }
        }



        public static string BuildShipmentLinks(string commaDelimitedShipmentNumbers)
        {
            return commaDelimitedShipmentNumbers
                .Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
                .Where(s => !string.IsNullOrEmpty(s.Trim()))
                .Select(
                    s =>
                    string.Format(
                        "{0} <a href='{1}?{2}={0}' target='_blank' class='blue'><img src='~/images/icons2/arrow_newTab.png' alt='' width='16' align='absmiddle'/></a>",
                        s.Trim(), PageAddress, WebApplicationConstants.TransferNumber))
                .ToArray()
                .CommaJoin();
        }


        private void AddServicesRequired(List<AddressBookService> services)
        {
            if (!services.Any()) return;

            var serviceIds = services.Select(s => s.ServiceId).ToList();


            var viewIds = rptServices
                .Items
                .Cast<RepeaterItem>()
                .Where(item => item.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(item => item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong())
                .ToList();


            foreach (var id in viewIds.Where(id => !serviceIds.Contains(id)))
                serviceIds.Add(id);

            DisplayServices(serviceIds);
        }


        private void DisplayAccountBucket(AccountBucket accountBucket)
        {
            txtAccountBucketCode.Text = accountBucket == null ? string.Empty : accountBucket.Code;
            txtAccountBucketDescription.Text = accountBucket == null ? string.Empty : accountBucket.Description;
            hidAccountBucketId.Value = accountBucket == null ? default(long).ToString() : accountBucket.Id.ToString();
            ddlAccountBucketUnit.SetSelectedAccountBucketUnit(default(long), accountBucket != null ? accountBucket.Id : default(long));
        }

        private void DisplayPrefix(Prefix prefix)
        {
            txtPrefixCode.Text = prefix == null ? string.Empty : prefix.Code;
            txtPrefixDescription.Text = prefix == null ? string.Empty : prefix.Description;
            hidPrefixId.Value = prefix == null ? default(long).ToString() : prefix.Id.ToString();
        }

        private void DisplayShipmentCoordinator(User user)
        {
            txtShipmentCoordUsername.Text = user == null ? string.Empty : user.Username;
            txtShipmentCoordName.Text = user == null ? string.Empty : string.Format("{0} {1}", user.FirstName, user.LastName);
            hidShipmentCoordUserId.Value = user == null ? string.Empty : user.Id.ToString();
        }

        private void DisplayCarrierCoordinator(User user)
        {
            txtCarrierCoordUsername.Text = user == null ? string.Empty : user.Username;
            txtCarrierCoordName.Text = user == null ? string.Empty : string.Format("{0} {1}", user.FirstName, user.LastName);
            hidCarrierCoordUserId.Value = user == null ? string.Empty : user.Id.ToString();
        }


        private List<ValidationMessage> CheckVendorConstraints(Vendor vendor, bool displayMessage = true)
        {
            var messages = new List<ValidationMessage>();
            //Service Modes
            var mode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>();
            switch (mode)
            {
                case ServiceMode.LessThanTruckload:
                    if (!vendor.HandlesLessThanTruckload)
                        messages.Add(
                            ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
                                                                    ServiceMode.LessThanTruckload.FormattedString())));
                    break;
                case ServiceMode.Truckload:
                    if (!vendor.HandlesTruckload)
                        messages.Add(
                            ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
                                                                    ServiceMode.Truckload.FormattedString())));
                    if (chkIsPartialTruckLoad.Checked && !vendor.HandlesPartialTruckload)
                        messages.Add(
                            ValidationMessage.Warning(string.Format("{0} does not handle Partial Truckload shipments", vendor.Name)));
                    break;
                case ServiceMode.Air:
                    if (!vendor.HandlesAir)
                        messages.Add(
                            ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
                                                                    ServiceMode.Air.FormattedString())));
                    break;
                case ServiceMode.Rail:
                    if (!vendor.HandlesRail)
                        messages.Add(
                            ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
                                                                    ServiceMode.Rail.FormattedString())));
                    break;
                case ServiceMode.SmallPackage:
                    if (!vendor.HandlesSmallPack)
                        messages.Add(
                            ValidationMessage.Warning(string.Format("{0} does not handle {1} shipments", vendor.Name,
                                                                    ServiceMode.SmallPackage.FormattedString())));
                    break;
            }

            var shipment = new Shipment();

            //Services
            var shipmentServices = RetrieveShipmentServices(shipment);
            var vendorServices = vendor.Services.Select(s => s.ServiceId).ToList();
            messages.AddRange(from service in shipmentServices
                              where !vendorServices.Contains(service.ServiceId)
                              select
                                  ValidationMessage.Warning(string.Format("{0} does not provide the service: {1}", vendor.Name,
                                                                          service.Service.Description)));

            //Equipment
            var shipmentEquipment = RetrieveShipmentEquipments(shipment);
            var vendorEquipment = vendor.Equipments.Select((e => e.EquipmentTypeId)).ToList();
            messages.AddRange(from equipment in shipmentEquipment
                              where !vendorEquipment.Contains(equipment.EquipmentTypeId)
                              select
                                  ValidationMessage.Warning(string.Format("{0} does not provide equipment type: {1}", vendor.Name,
                                                                          equipment.EquipmentType.FormattedString())));
            if (displayMessage && messages.Any()) DisplayMessages(messages);
            return messages;
        }

        private IEnumerable<ValidationMessage> CheckVendorConstraints(IEnumerable<Vendor> vendors)
        {
            var messages = new List<ValidationMessage>();

            //Service Modes
            foreach (var vendor in vendors)
                messages.AddRange(CheckVendorConstraints(vendor, false));
            return messages;
        }


        private List<ToolbarMoreAction> ToolbarMoreActions(bool enabled)
        {
            var brDashboard = new ToolbarMoreAction
            {
                CommandArgs = ReturnArgs,
                ImageUrl = IconLinks.Operations,
                Name = ViewCode.ShipmentDashboard.FormattedString(),
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var cancel = new ToolbarMoreAction
            {
                CommandArgs = VoidArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Void Shipment",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var reverseCancel = new ToolbarMoreAction
            {
                CommandArgs = ReverseVoidArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Reverse Void Shipment",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var duplicate = new ToolbarMoreAction
            {
                CommandArgs = DuplicateArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Duplicate Shipment",
                NavigationUrl = string.Empty
            };

            var duplicateReverse = new ToolbarMoreAction
            {
                CommandArgs = DuplicateReserseArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Duplicate Shipment (Round Trip)",
                NavigationUrl = string.Empty
            };
            var vendorLaneHistory = new ToolbarMoreAction
            {
                CommandArgs = VendorLaneHistoryArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Reports,
                IsLink = false,
                Name = "Vendor Lane Buy/Sell",
                NavigationUrl = string.Empty
            };
            var bol = new ToolbarMoreAction
            {
                CommandArgs = BolArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Generate Bill of Lading",
                NavigationUrl = string.Empty
            };
            var bolp = new ToolbarMoreAction
            {
                CommandArgs = BolArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = true,
                Name = "Generate Bill of Lading (for Print)",
                NavigationUrl = DocumentViewer.GenerateShipmentBolLink(hidShipmentId.Value.ToLong()),
                OpenInNewWindow = true
            };
            var audit = new ToolbarMoreAction
            {
                CommandArgs = string.Empty,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = true,
                Name = "Shipment Audit (Acct.)",
                NavigationUrl =
                        ShipmentAuditView.PageAddress + "?" + WebApplicationConstants.ShipmentAuditId + "=" +
                        hidShipmentId.Value.UrlTextEncrypt(),
                OpenInNewWindow = true
            };
            var csr = new ToolbarMoreAction
            {
                CommandArgs = BolArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = true,
                Name = "Customer Service Rep(s)",
                NavigationUrl = CustCSRView.PageAddress + "?" + WebApplicationConstants.CsrCustId + "=" + hidCustomerId.Value,
                OpenInNewWindow = true
            };

            var rateAgreement = new ToolbarMoreAction
            {
                CommandArgs = VendorRateAgreementArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Generate Vendor Rate Agreement",
                NavigationUrl = string.Empty
            };

            var shippingLabel = new ToolbarMoreAction
            {
                CommandArgs = ShippingLabelArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Generate Shipping Labels",
                NavigationUrl = string.Empty
            };

            var editInvoicedShipment = new ToolbarMoreAction
            {
                CommandArgs = EditInvoicedShipmentArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Edit Invoiced Shipment",
                NavigationUrl = string.Empty
            };

            var generateRouteSummary = new ToolbarMoreAction
            {
                CommandArgs = RouteSummaryArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Generate Route Summary",
                NavigationUrl = string.Empty
            };

            var payForShipmentWithCreditCard = new ToolbarMoreAction
            {
                CommandArgs = PayForShipmentWithCreditCardArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Collect Customer Payment",
                NavigationUrl = string.Empty
            };

            var refundCustomerPayments = new ToolbarMoreAction
            {
                CommandArgs = RefundCustomerPaymentsArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Refund Customer Payments",
                NavigationUrl = string.Empty
            };

            var dispatchShipment = new ToolbarMoreAction
            {
                CommandArgs = DispatchShipmentArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Dispatch Shipment",
                NavigationUrl = string.Empty
            };

            var initiateTracking = new ToolbarMoreAction
            {
                CommandArgs = InitiateSmc3EvaTrackingArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Initiate SMC3 EVA Tracking",
                NavigationUrl = string.Empty
            };

            var initiateProject44Tracking = new ToolbarMoreAction
            {
                CommandArgs = InitiateProject44TrackingArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Initiate Project 44 Tracking",
                NavigationUrl = string.Empty
            };

            var confirmCharges = new ToolbarMoreAction
            {
                CommandArgs = ConfirmChargesArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Confirm Vendor Charges",
                NavigationUrl = string.Empty
            };

            var macroPointOrder = new MacroPointOrderSearch().FetchMacroPointOrderByIdNumber(txtShipmentNumber.Text,
                                                                                             ActiveUser.TenantId);
            var alreadyTracking = macroPointOrder != null;

            var startOrStopMacroPointTracking = new ToolbarMoreAction
            {
                CommandArgs = alreadyTracking ? MacroPointTrackingStopArgs : MacroPointTrackingStartArgs,
                ConfirmCommand = alreadyTracking,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = alreadyTracking ? "Stop MacroPoint Tracking" : "Initiate MacroPoint Tracking",
                NavigationUrl = string.Empty
            };

            var requestLocationUpdate = new ToolbarMoreAction
            {
                CommandArgs = MacroPointRequestLocationUpdateArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Request Location Update",
                NavigationUrl = string.Empty
            };

            var seperator = new ToolbarMoreAction { IsSeperator = true };

            var notNew = hidShipmentId.Value.ToLong() != default(long);
            var actions = new List<ToolbarMoreAction>();

            if (Access.Modify && hidStatus.Value != ShipmentStatus.Invoiced.ToString())
            {
                if (notNew && hidStatus.Value != ShipmentStatus.Void.ToString() && enabled) actions.Add(cancel);
                if (hidStatus.Value == ShipmentStatus.Void.ToString() && enabled) actions.Add(reverseCancel);
            }

            if (Access.Modify && notNew)
            {
                actions.Add(duplicate);
                actions.Add(duplicateReverse);
            }

            if (Access.Modify && hidStatus.Value == ShipmentStatus.Invoiced.ToString()) actions.Add(editInvoicedShipment);

            if (notNew)
            {
                if (actions.Any()) actions.Add(seperator);
                if (!enabled) actions.Add(bol);

                // options only if cannot edit BOL related items.
                var status = string.IsNullOrEmpty(hidStatus.Value)
                                 ? ShipmentStatus.Scheduled
                                 : hidStatus.Value.ToEnum<ShipmentStatus>();
                if (!enabled || status == ShipmentStatus.Invoiced) actions.Add(bolp);

                if (enabled && hidStatus.Value == ShipmentStatus.Scheduled.ToString() && hidCanDispatch.Value.ToBoolean())
                    actions.Add(dispatchShipment);
                if (!enabled) actions.Add(rateAgreement);
                actions.Add(shippingLabel);
                actions.Add(generateRouteSummary);

                if (ActiveUser.HasAccessTo(ViewCode.MacroPointShipmentTracking) &&
                    new[] { ShipmentStatus.InTransit, ShipmentStatus.Scheduled }.Contains(status))
                {
                    var currentlyTrackingStatuses = new[]
                        {
                            OrderTrackingStatusCodes.TrackingNow.ToInt().GetString(),
                            OrderTrackingStatusCodes.ReadyToTrack.ToInt().GetString(),
                            OrderTrackingStatusCodes.RequestingInstallation.ToInt().GetString(),
                            OrderTrackingStatusCodes.TrackingWaitingForUpdate.ToInt().GetString()
                        };
                    if (alreadyTracking || enabled)
                    {
                        actions.Add(seperator);
                        actions.Add(startOrStopMacroPointTracking);
                        if (enabled && alreadyTracking && currentlyTrackingStatuses.Contains(macroPointOrder.TrackingStatusCode))
                            actions.Add(requestLocationUpdate);
                    }
                }

                var primaryVendorCommunication = new Vendor(hidPrimaryVendorId.Value.ToLong()).Communication;
                var trackedShipment = new SMC3TrackRequestResponseSearch().FetchTrackRequestResponseByShipmentId(hidShipmentId.Value.ToLong());
                var shipment = new Shipment(hidShipmentId.Value.ToLong());

                if (!trackedShipment.Any() &&
                        primaryVendorCommunication != null &&
                        enabled && shipment.ServiceMode == ServiceMode.LessThanTruckload)
                {
                    if (primaryVendorCommunication.SMC3EvaEnabled &&primaryVendorCommunication.SMC3EvaSupportsTracking)
                        actions.Add(initiateTracking);

                    if(primaryVendorCommunication.Project44Enabled && primaryVendorCommunication.Project44TrackingEnabled)
                        actions.Add(initiateProject44Tracking);
                }
            }

            if (enabled)
            {
                if (actions.Any()) actions.Add(seperator);
                actions.Add(vendorLaneHistory);

            }

            if (ActiveUser.HasAccessTo(ViewCode.ShipmentAudit) && ActiveUser.TenantEmployee && notNew)
            {
                if (actions.Any()) actions.Add(seperator);
                actions.Add(audit);
            }

            if (ActiveUser.HasAccessTo(ViewCode.Customer) && ActiveUser.TenantEmployee && notNew)
            {
                if (actions.Any()) actions.Add(seperator);
                actions.Add(csr);
            }

            if (actions.Any()) actions.Add(seperator);

            if (hidShipmentId.Value.ToLong() != default(long) && Access.Modify)
            {
                if (ActiveUser.HasAccessTo(ViewCode.CanCollectPaymentFromCustomer) &&
                    new Customer(hidCustomerId.Value.ToLong()).CanPayByCreditCard &&
                    hidStatus.Value.ToEnum<ShipmentStatus>() != ShipmentStatus.Void)
                    actions.Add(payForShipmentWithCreditCard);

                if (ActiveUser.HasAccessTo(ViewCode.CanRefundPaymentFromCustomer) &&
                    hidMiscReceiptsRelatedToShipmentExist.Value.ToBoolean())
                    actions.Add(refundCustomerPayments);
            }

            if (Access.Modify && notNew && !enabled && ActiveUser.CanSeeAllVendorsInVendorPortal)
            {
                var hasUnBilledCharge = new ShipmentChargeSearch().HasUnBilledCharges(hidShipmentId.Value.ToLong());
                if (hasUnBilledCharge)
                {
                    actions.Add(confirmCharges);
                    actions.Add(seperator);
                }
            }

            actions.Add(brDashboard);



            return actions;
        }

        private void SetEditStatus(bool enabled)
        {
            hidEditStatus.Value = enabled.ToString();

            var status = string.IsNullOrEmpty(hidStatus.Value)
                             ? ShipmentStatus.Scheduled
                             : hidStatus.Value.ToEnum<ShipmentStatus>();
            var notNew = (hidShipmentId.Value.ToLong() != default(long));
            var invoiced = status == ShipmentStatus.Invoiced;
            var voided = status == ShipmentStatus.Void;
            var enabledAndNotInvoicedNotVoided = enabled && !invoiced && !voided;

            pnlAboveDetails.Enabled = enabledAndNotInvoicedNotVoided;
            pnlDetails.Enabled = enabledAndNotInvoicedNotVoided;
            pnlStops.Enabled = enabledAndNotInvoicedNotVoided;
            pnlAdditionalDetails.Enabled = enabledAndNotInvoicedNotVoided;
            pnlItems.Enabled = enabledAndNotInvoicedNotVoided;
            pnlReferences.Enabled = enabledAndNotInvoicedNotVoided;
            pnlVendors.Enabled = enabledAndNotInvoicedNotVoided;
            pnlCharges.Enabled = enabledAndNotInvoicedNotVoided;
            pnlNoteAndCheckCalls.Enabled = enabled && !voided;
            pnlRatingDetails.Enabled = enabledAndNotInvoicedNotVoided;
            pnlAccountBuckets.Enabled = enabledAndNotInvoicedNotVoided;
            pnlQuickPayOptions.Enabled = enabledAndNotInvoicedNotVoided;
            pnlAssets.Enabled = enabledAndNotInvoicedNotVoided;
            pnlDocuments.Enabled = (notNew && enabled && !voided); //Shipment must exist before documents can be added.

            memberToolBar.EnableSave = enabled && !voided;
            memberToolBar.EnableImport = enabledAndNotInvoicedNotVoided;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

            // must do this here to load correct set of extensible actions
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(enabled));
        }

        private void BindTimeDropDowns()
        {
            var timeList = TimeUtility.BuildTimeList();

            ddlEarlyPickup.DataSource = timeList;
            ddlEarlyPickup.DataBind();

            ddlLatePickup.DataSource = timeList;
            ddlLatePickup.DataBind();

            ddlEarlyDelivery.DataSource = timeList;
            ddlEarlyDelivery.DataBind();

            ddlLateDelivery.DataSource = timeList;
            ddlLateDelivery.DataBind();
        }

        private static List<object> RetrieveResellerAdditionData(ResellerAddition resellerAddition)
        {
            var freight = new
            {
                Header = "Freight",
                Type = resellerAddition.LineHaulType.FormattedString(),
                Value = resellerAddition.LineHaulValue.ToString("f4"),
                Percent = resellerAddition.LineHaulPercentage.ToString("f4"),
                UseLower = resellerAddition.UseLineHaulMinimum
            };

            var fuel = new
            {
                Header = "Fuel",
                Type = resellerAddition.FuelType.FormattedString(),
                Value = resellerAddition.FuelValue.ToString("f4"),
                Percent = resellerAddition.FuelPercentage.ToString("f4"),
                UseLower = resellerAddition.UseFuelMinimum
            };

            var accessorial = new
            {
                Header = "Accessorial",
                Type = resellerAddition.AccessorialType.FormattedString(),
                Value = resellerAddition.AccessorialValue.ToString("f4"),
                Percent = resellerAddition.AccessorialPercentage.ToString("f4"),
                UseLower = resellerAddition.UseAccessorialMinimum
            };

            var service = new
            {
                Header = "Service",
                Type = resellerAddition.FuelType.FormattedString(),
                Value = resellerAddition.FuelValue.ToString("f4"),
                Percent = resellerAddition.FuelPercentage.ToString("f4"),
                UseLower = resellerAddition.UseFuelMinimum
            };

            var list = new List<object> { freight, fuel, accessorial, service };
            return list;
        }


        private List<ShipmentService> RetrieveShipmentServices(Shipment shipment)
        {
            var services = (rptServices
                .Items
                .Cast<RepeaterItem>()
                .Where(item => item.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(item => new ShipmentService
                {
                    Shipment = shipment,
                    ServiceId = item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong(),
                    TenantId = ActiveUser.TenantId,
                }))
                .ToList();

            return services;
        }

        private List<ShipmentEquipment> RetrieveShipmentEquipments(Shipment shipment)
        {
            var equipments = (rptEquipmentTypes
                .Items
                .Cast<RepeaterItem>()
                .Where(item => item.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(item => new ShipmentEquipment
                {
                    Shipment = shipment,
                    EquipmentTypeId = item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong(),
                    TenantId = ActiveUser.TenantId,
                }))
                .ToList();

            return equipments;
        }


        private Shipment UpdateShipment()
        {
            var shipmentId = hidShipmentId.Value.ToLong();
            var shipment = new Shipment(shipmentId, shipmentId != default(long));

            var editStatus = hidEditStatus.Value.ToBoolean();
            if (!editStatus) return shipment;

            // checking status that do not allow edit but for notes (in this strategy)
            if (shipment.Status == ShipmentStatus.Void || shipment.Status == ShipmentStatus.Invoiced)
            {
                shipment.LoadCollections();
                UpdateNotesFromView(shipment);
                UpdateDocumentsFromView(shipment);
                return shipment;
            }

            var oldStatus = shipment.Status; // save old status

            if (shipment.IsNew)
            {
                shipment.TenantId = ActiveUser.TenantId;
                shipment.Origin = new ShipmentLocation { Shipment = shipment, TenantId = ActiveUser.TenantId };
                shipment.Destination = new ShipmentLocation { Shipment = shipment, TenantId = ActiveUser.TenantId };

                shipment.Status = ShipmentStatus.Scheduled;

                shipment.CreatedInError = false;

                shipment.User = ActiveUser;
                shipment.DateCreated = DateTime.Now;

            }

            // load collections
            shipment.LoadCollections();
            shipment.Origin.LoadCollections();
            shipment.Destination.LoadCollections();


            //details
            shipment.Customer = new Customer(hidCustomerId.Value.ToLong());

            shipment.PrefixId = hidPrefixId.Value.ToLong();
            shipment.HidePrefix = chkHidePrefix.Checked;


            shipment.ShipmentPriorityId = ddlPriority.SelectedValue.ToLong();

            shipment.ShipmentNumber = txtShipmentNumber.Text;
            shipment.ServiceMode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>();
            shipment.IsPartialTruckload = chkIsPartialTruckLoad.Checked;
            shipment.PurchaseOrderNumber = txtPurchaseOrderNumber.Text;
            shipment.ShipperReference = txtShipperReference.Text;
            shipment.ShipperBol = txtShipperBol.Text;
            shipment.DesiredPickupDate = txtDesiredPickupDate.Text.ToDateTime();

            shipment.ActualPickupDate = string.IsNullOrEmpty(txtActualPickupDate.Text)
                                            ? DateUtility.SystemEarliestDateTime
                                            : txtActualPickupDate.Text.ToDateTime();
            if (!string.IsNullOrEmpty(txtActaulPickupTime.Text) && txtActaulPickupTime.Text.IsValidFreeFormTimeString() &&
                shipment.ActualPickupDate != DateUtility.SystemEarliestDateTime)
                shipment.ActualPickupDate = shipment.ActualPickupDate.SetTime(txtActaulPickupTime.Text);

            shipment.EarlyPickup = ddlEarlyPickup.SelectedValue;
            shipment.LatePickup = ddlLatePickup.SelectedValue;
            shipment.EstimatedDeliveryDate = txtEstimatedDeliveryDate.Text.ToDateTime();

            var sadd = shipment.ActualDeliveryDate;
            shipment.ActualDeliveryDate = string.IsNullOrEmpty(txtActualDeliveryDate.Text)
                                              ? DateUtility.SystemEarliestDateTime
                                              : txtActualDeliveryDate.Text.ToDateTime();
            if (!string.IsNullOrEmpty(txtActualDeliveryTime.Text) && txtActualDeliveryTime.Text.IsValidFreeFormTimeString() &&
                shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime)
                shipment.ActualDeliveryDate = shipment.ActualDeliveryDate.SetTime(txtActualDeliveryTime.Text);
            if (sadd != shipment.ActualDeliveryDate)
                _shipmentDocRtrvLogRequiredIfApplicable = true; // change in actual delivery date

            shipment.EarlyDelivery = ddlEarlyDelivery.SelectedValue;
            shipment.LateDelivery = ddlLateDelivery.SelectedValue;
            shipment.Mileage = txtMileage.Text.ToDecimal();

            shipment.EmptyMileage = txtEmptyMileage.Text.ToDecimal();
            shipment.MileageSourceId = ddlMileageSource.SelectedValue.ToLong();

            shipment.MiscField1 = txtMiscField1.Text;
            shipment.MiscField2 = txtMiscField2.Text;

            shipment.DriverName = txtDriverName.Text;
            shipment.DriverPhoneNumber = txtDriverPhoneNumber.Text;
            shipment.DriverTrailerNumber = txtDriverTrailerNumber.Text;

            shipment.Origin.TakeSnapShot();
            var zip = shipment.Origin.PostalCode;
            var appt = shipment.Origin.AppointmentDateTime;
            operationsAddressInputOrigin.RetrieveAddress(shipment.Origin);
            if (!shipment.IsNew && zip != shipment.Origin.PostalCode) chkAutoRated.Checked = false;
            if (!shipment.IsNew && appt != shipment.Origin.AppointmentDateTime)
            {
                _originApptChanged = true;
                hidNotificationsRequired.Value += string.Format("{0};", Milestone.ShipmentCheckCallUpdate);
            }
            shipment.SendShipmentUpdateToOriginPrimaryContact = chkNotifyPrimaryOriginContactOfShipmentUpdates.Checked;

            zip = shipment.Destination.PostalCode;
            appt = shipment.Destination.AppointmentDateTime;
            shipment.Destination.TakeSnapShot();
            operationsAddressInputDestination.RetrieveAddress(shipment.Destination);
            if (!shipment.IsNew && zip != shipment.Destination.PostalCode) chkAutoRated.Checked = false;
            if (!shipment.IsNew && appt != shipment.Destination.AppointmentDateTime)
            {
                _destApptChanged = true;
                hidNotificationsRequired.Value += string.Format("{0};", Milestone.ShipmentCheckCallUpdate);
            }
            shipment.SendShipmentUpdateToDestinationPrimaryContact = chkNotifyPrimaryDestinationContactOfShipmentUpdates.Checked;

            shipment.Stops = RetrieveViewStops(shipment);

            shipment.Origin.StopOrder = 0;
            shipment.Destination.StopOrder = shipment.Stops.Count + 1;

            shipment.ShipmentAutorated = chkAutoRated.Checked; // update autorated flag

            shipment.Services = RetrieveShipmentServices(shipment);


            shipment.Equipments = RetrieveShipmentEquipments(shipment);

            DisplayServicesAndEquipmentTypes(shipment);

            //Add. Details
            shipment.GeneralBolComments = txtGeneralBolComments.Text;
            shipment.CriticalBolComments = txtCriticalBolComments.Text;

            shipment.HazardousMaterial = chkHazardousMaterial.Checked;
            shipment.HazardousMaterialContactName = txtHazardousContactName.Text;
            shipment.HazardousMaterialContactPhone = txtHazardousContactPhone.Text;
            shipment.HazardousMaterialContactMobile = txtHazardousContactMobile.Text;
            shipment.HazardousMaterialContactEmail = txtHazardousContactEmail.Text.StripSpacesFromEmails();



            UpdateItemsFromView(shipment);
            //handle stop order; remember the ViewDeliveryStopOrder set for destination!!! in load method
            foreach (var item in shipment.Items.Where(item => item.Delivery == ViewDeliveryStopOrder))
                item.Delivery = shipment.Destination.StopOrder;

            // Ensure hazmat is set on shipment if any items are set as hazmat
            if (shipment.Items.Any(i => i.HazardousMaterial) && !shipment.HazardousMaterial)
            {
                chkHazardousMaterial.Checked = true;
                shipment.HazardousMaterial = true;
            }

            CheckRequiredHazMatService(shipment);

            UpdateReferencesFromView(shipment);
            UpdateCheckCallsFromView(shipment);
            UpdateNotesFromView(shipment);
            UpdateAccountBucketsFromView(shipment);
            UpdatePrimaryVendor(shipment);
            UpdateVendorsFromView(shipment);
            UpdateQuickPayOptionsFromView(shipment);
            UpdateAssetsFromView(shipment);
            UpdateDocumentsFromView(shipment);
            UpdateRatingDetailsFromView(shipment); // NOTE: must call after vendors have been and after service mode set!

			// this must be called after locations and check calls have been updated.
            ReconcileCheckCallsAndShipmentRecordDates(shipment.CheckCalls, shipment);

            //sales rep
            shipment.SalesRepresentativeId = hidSalesRepresentativeId.Value.ToLong();
            shipment.SalesRepresentativeCommissionPercent = shipment.SalesRepresentativeId == default(long)
                                                                ? "0.0000".ToDecimal()
                                                                : hidSalesRepresentativeCommissionPercent.Value.ToDecimal();
            shipment.SalesRepAddlEntityCommPercent = shipment.SalesRepresentativeId == default(long)
                                                         ? "0.0000".ToDecimal()
                                                         : hidSalesRepresentativeAddlEntityCommPercent.Value.ToDecimal();
            shipment.SalesRepAddlEntityName = shipment.SalesRepresentativeId == default(long)
                                                  ? string.Empty
                                                  : txtSalesRepresentativeAddlEntityName.Text;
            shipment.SalesRepMinCommValue = shipment.SalesRepresentativeId == default(long)
                                                ? "0.0000".ToDecimal()
                                                : hidSalesRepresentativeMinCommValue.Value.ToDecimal();
            shipment.SalesRepMaxCommValue = shipment.SalesRepresentativeId == default(long)
                                                ? "0.0000".ToDecimal()
                                                : hidSalesRepresentativeMaxCommValue.Value.ToDecimal();


            //Charges
            UpdateChargesFromView(shipment);
            shipment.DeclineInsurance = chkDeclineInsurance.Checked;
            shipment.AuditedForInvoicing = chkAuditedForInvoicing.Checked;
            shipment.InDispute = chkInDispute.Checked;
            shipment.InDisputeReason = ddlInDisputeReason.SelectedValue.ToInt().ToEnum<InDisputeReason>();
            shipment.AccountBucketUnitId = hidAccountBucketUnitId.Value.ToLong();

            // handle inferred status change
            //if (shipment.Status != ShipmentStatus.Void && shipment.Status != ShipmentStatus.Invoiced)
            // NOTE: will never be void or invoiced at this point.  Logic above kicks out if void or invoiced.
            shipment.Status = shipment.InferShipmentStatus(true);


            // set notifications
            if (shipment.IsNew && shipment.Status == ShipmentStatus.Scheduled)
                hidNotificationsRequired.Value += string.Format("{0};", Milestone.NewShipmentRequest);
            if (oldStatus != shipment.Status)
                switch (shipment.Status)
                {
                    case ShipmentStatus.Delivered:
                        hidNotificationsRequired.Value += string.Format("{0};", Milestone.ShipmentDelivered);
                        break;
                    case ShipmentStatus.InTransit:
                        hidNotificationsRequired.Value += string.Format("{0};", Milestone.ShipmentInTransit);
                        break;
                }

            // Reseller Addition
            shipment.ResellerAdditionId = hidResellerAdditionId.Value.ToLong();
            shipment.BillReseller = hidBillReseller.Value.ToBoolean();

            // coordinators
            shipment.CarrierCoordinatorId = hidCarrierCoordUserId.Value.ToLong();
            shipment.ShipmentCoordinatorId = hidShipmentCoordUserId.Value.ToLong();

            // job
            shipment.JobId = hidJobId.Value.ToLong();
            shipment.JobStep = txtJobStep.Text.ToInt();

            return shipment;
        }

        private void CheckRequiredHazMatService(Shipment shipment)
        {
            var hazMatServiceId = ActiveUser.Tenant.HazardousMaterialServiceId;

            if (hazMatServiceId != default(long) && shipment.Services.Any(s => s.ServiceId == hazMatServiceId) &&
                !shipment.HazardousMaterial)
            {
                shipment.HazardousMaterial = true;
            }

            else if (hazMatServiceId != default(long) && shipment.HazardousMaterial &&
                     shipment.Services.All(s => s.ServiceId != hazMatServiceId))
            {
                shipment.Services.Add(new ShipmentService
                {
                    Shipment = shipment,
                    TenantId = ActiveUser.TenantId,
                    ServiceId = hazMatServiceId
                });
            }
        }



        private void ConfigureCheckCallAlert(ShipmentStatus status, ServiceMode serviceMode)
        {
            var checkCalls = GetCurrentViewCheckCallsUpdated();
            var validServiceMode = serviceMode == ServiceMode.Air || serviceMode == ServiceMode.Rail ||
                                   serviceMode == ServiceMode.Truckload;
            pnlCheckCallsAlert.Visible = validServiceMode && status == ShipmentStatus.InTransit &&
                                         checkCalls.All(c => c.CallDate.TimeToMinimum() != DateTime.Now.TimeToMinimum());
        }


        private void UpdateItemsFromView(Shipment shipment)
        {
            var items = lstItems
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong();
                        var txtQuantity = i.FindControl("txtQuantity").ToTextBox();
                        if (txtQuantity.Text.ToInt() <= 0) txtQuantity.Text = 1.ToString();
                        var delivery = i.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt();
                        return new ShipmentItem(id, id != default(long))
                        {
                            Pickup = i.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
                            Delivery = delivery == ViewDeliveryStopOrder ? shipment.Stops.Count + 1 : delivery,
                            ActualWeight = i.FindControl("txtWeight").ToTextBox().Text.ToDecimal(),
                            ActualLength = i.FindControl("txtLength").ToTextBox().Text.ToDecimal(),
                            ActualWidth = i.FindControl("txtWidth").ToTextBox().Text.ToDecimal(),
                            ActualHeight = i.FindControl("txtHeight").ToTextBox().Text.ToDecimal(),
                            Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                            PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text.ToInt(),
                            Description = i.FindControl("txtDescription").ToTextBox().Text,
                            Comment = i.FindControl("txtComment").ToTextBox().Text,
                            ActualFreightClass = i.FindControl("ddlActualFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                            RatedFreightClass = i.FindControl("txtRatedFreightClass").ToTextBox().Text.ToDouble(),
                            Value = i.FindControl("txtItemValue").ToTextBox().Text.ToDecimal(),
                            IsStackable = i.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                            HazardousMaterial = i.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
                            PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                            NMFCCode = i.FindControl("txtNMFCCode").ToTextBox().Text,
                            HTSCode = i.FindControl("txtHTSCode").ToTextBox().Text,
                            Tenant = shipment.Tenant,
                            Shipment = shipment,
                        };
                    })
                .ToList();
            shipment.Items = items;
        }

        private void UpdateReferencesFromView(Shipment shipment)
        {
            var references = lstReferences
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidReferenceId").ToCustomHiddenField().Value.ToLong();
                        return new ShipmentReference(id, id != default(long))
                        {
                            Name = i.FindControl("txtName").ToTextBox().Text,
                            Value = i.FindControl("txtValue").ToTextBox().Text,
                            DisplayOnOrigin = i.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
                            DisplayOnDestination = i.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
                            Required = i.FindControl("chkRequired").ToAltUniformCheckBox().Checked,
                            Shipment = shipment,
                            Tenant = shipment.Tenant
                        };
                    })
                .ToList();
            shipment.CustomerReferences = references;
        }

        private void UpdateChargesFromView(Shipment shipment)
        {
            var charges = lstCharges.Items
                                    .Select(i =>
                                        {
                                            var id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong();
                                            var txtQty = (i.FindControl("txtQuantity").ToTextBox());
                                            if (txtQty.Text.ToInt() <= 0) txtQty.Text = 1.ToString();
                                            var selectedChargeVendorId = i.FindControl("ddlVendors").ToDropDownList().SelectedValue.ToLong();
                                            var vendorId = selectedChargeVendorId == default(long)
                                                               ? hidPrimaryVendorId.Value.ToLong()
                                                               : selectedChargeVendorId;
                                            return new ShipmentCharge(id, id != default(long))
                                                {
                                                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                                                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                                                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                                                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                                                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                                                    ChargeCodeId =
                                                        i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                                                    Shipment = shipment,
                                                    Tenant = shipment.Tenant,
                                                    VendorId = vendorId
                                                };
                                        })
                                    .ToList();

            shipment.Charges = charges;
        }

        private void UpdateAccountBucketsFromView(Shipment shipment)
        {
            var accountBuckets = lstAccountBuckets
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidShipmentAccountBucketId").ToCustomHiddenField().Value.ToLong();
                        return new ShipmentAccountBucket(id, id != default(long))
                        {
                            AccountBucketId = i.FindControl("hidAccountBucketId").ToCustomHiddenField().Value.ToLong(),
                            Shipment = shipment,
                            TenantId = shipment.TenantId,
                            Primary = false
                        };
                    })
                .ToList();

            var primaryId = hidShipmentAccountBucketId.Value.ToLong();
            accountBuckets.Add(new ShipmentAccountBucket(primaryId, primaryId != default(long))
            {
                AccountBucketId = hidAccountBucketId.Value.ToLong(),
                Shipment = shipment,
                Primary = true,
                TenantId = shipment.TenantId
            });

            shipment.AccountBuckets = accountBuckets;
        }

        private void UpdatePrimaryVendor(Shipment shipment)
        {
            var vendors = shipment.Vendors.Where(v => !v.Primary).ToList();

            var primaryId = hidPrimaryShipmentVendorId.Value.ToLong();
            var primaryVendor = new ShipmentVendor(primaryId, primaryId != default(long))
            {
                VendorId = hidPrimaryVendorId.Value.ToLong(),
                Shipment = shipment,
                TenantId = shipment.TenantId,
                FailureCodeId = ddlPrimaryFailureCode.SelectedValue.ToLong(),
                FailureComments = txtPrimaryFailureComments.Text,
                Primary = true,
            };

            if (primaryVendor.ProNumber != txtPrimaryProNumber.Text)
                _shipmentDocRtrvLogRequiredIfApplicable = true; // change in pro.
            primaryVendor.ProNumber = txtPrimaryProNumber.Text;

            vendors.Add(primaryVendor);

            shipment.Vendors = vendors;
        }

        private void UpdateTerminalInformation(Shipment shipment, string scac)
        {
            // terminals
            if (!string.IsNullOrEmpty(scac))
            {
                try
                {
                    var variables = ProcessorVars.IntegrationVariables[ActiveUser.TenantId];
                    var info = new CarrierConnect(variables.CarrierConnectParameters);

                    info.SetPrimaryVendorLTLTerminalInfo(shipment);
                }
                catch (Exception ex)
                {
                    shipment.OriginTerminal = null;
                    shipment.DestinationTerminal = null;
                    ErrorLogger.LogError(ex, Context);
                }
            }
            else
            {
                shipment.OriginTerminal = null;
                shipment.DestinationTerminal = null;
            }
        }

        private void UpdateRatingDetailsFromView(Shipment shipment)
        {
            //AutoRating Defaults
            shipment.OriginalRateValue = litOriginalValue.Text.ToDecimal();
            shipment.VendorDiscountPercentage = litVendorDiscountPercentage.Text.ToDecimal();
            shipment.VendorFreightFloor = litVendorFreightFloor.Text.ToDecimal();
            shipment.VendorFuelPercent = litVendorFuelPercent.Text.ToDecimal();
            shipment.CustomerFreightMarkupValue = litCustomerFreightMarkup.Text.ToDecimal();
            shipment.CustomerFreightMarkupPercent = litCustomerFreightMarkupPercent.Text.ToDecimal();
            shipment.UseLowerCustomerFreightMarkup = chkUseLowerCustomerFreightMarkup.Checked;
            shipment.BilledWeight = litBilledWeight.Text.ToDecimal();
            shipment.RatedWeight = litRatedWeight.Text.ToDecimal();
            shipment.RatedCubicFeet = litRatedCubicFeet.Text.ToDecimal();
            shipment.RatedPcf = litRatedPcf.Text.ToDecimal();
            shipment.ApplyDiscount = chkApplyDiscount.Checked;
            shipment.IsLTLPackageSpecificRate = chkLTLPackageSpecificRate.Checked;
            shipment.LineHaulProfitAdjustmentRatio = litLineHaulProfitAdjRatio.Text.ToDecimal();
            shipment.FuelProfitAdjustmentRatio = litFuelProfitAdjustmentRatio.Text.ToDecimal();
            shipment.AccessorialProfitAdjustmentRatio = litAccessorialProfitAdjustmentRatio.Text.ToDecimal();
            shipment.ServiceProfitAdjustmentRatio = litServiceProfitAdjustmentRatio.Text.ToDecimal();
            shipment.ResellerAdditionalFreightMarkup = litResellerAdditionalFreightMarkup.Text.ToDecimal();
            shipment.ResellerAdditionalFreightMarkupIsPercent = chkResellerAdditionalFreightMarkupIsPercent.Checked;
            shipment.ResellerAdditionalFuelMarkup = litResellerAdditionalFuelMarkup.Text.ToDecimal();
            shipment.ResellerAdditionalFuelMarkupIsPercent = chkResellerAdditionalFuelMarkupIsPercent.Checked;
            shipment.ResellerAdditionalAccessorialMarkup = litResellerAdditionalAccessorialMarkup.Text.ToDecimal();
            shipment.ResellerAdditionalAccessorialMarkupIsPercent = chkResellerAdditionalAccessorialMarkupIsPercent.Checked;
            shipment.DirectPointRate = chkDirectPoint.Checked;
            shipment.SmallPackType = litSmallPackType.Text;
            shipment.SmallPackageEngine = hidSmallPackEngine.Value.ToInt().ToEnum<SmallPackageEngine>();
            shipment.VendorRatingOverrideAddress = litRatingOverrideAddress.Text;
            shipment.CareOfAddressFormatEnabled = chkCareOfAdddressFormatEnabled.Checked;
            shipment.LinearFootRuleBypassed = chkLinearFootRuleBypassed.Checked;
            shipment.IsGuaranteedDeliveryService = chkGuaranteedDeliveryService.Checked;
            shipment.GuaranteedDeliveryServiceTime = litGuaranteedDeliveryTime.Text;
            shipment.GuaranteedDeliveryServiceRate = litGuaranteedDeliveryRate.Text.ToDecimal();
            shipment.GuaranteedDeliveryServiceRateType = litGuaranteedDeliveryRateType.Text.ToEnum<RateType>();
            shipment.Project44QuoteNumber = litProject44QuoteNumber.Text;
            shipment.IsExpeditedService = chkExpeditedDeliveryService.Checked;
            shipment.ExpeditedServiceRate = litExpeditedServiceRate.Text.ToDecimal();
            shipment.ExpeditedServiceTime = litExpeditedServiceDetails.Text;

            var accessorials = lstOtherDetails
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidRatingId").ToCustomHiddenField().Value.ToLong();
                        return new ShipmentAutoRatingAccessorial(id, id != default(long))
                        {
                            RateType = i.FindControl("hidRatingRateType").ToCustomHiddenField().Value.ToEnum<RateType>(),
                            Rate = i.FindControl("litRatingRate").ToLiteral().Text.ToDecimal(),
                            BuyFloor = i.FindControl("litRatingBuyFloor").ToLiteral().Text.ToDecimal(),
                            BuyCeiling = i.FindControl("litRatingBuyCeiling").ToLiteral().Text.ToDecimal(),
                            SellFloor = i.FindControl("litRatingSellFloor").ToLiteral().Text.ToDecimal(),
                            SellCeiling = i.FindControl("litRatingSellCeiling").ToLiteral().Text.ToDecimal(),
                            MarkupValue = i.FindControl("litRatingMarkupValue").ToLiteral().Text.ToDecimal(),
                            MarkupPercent = i.FindControl("litRatingMarkupPercent").ToLiteral().Text.ToDecimal(),
                            UseLowerSell = i.FindControl("chkRatingUseLowerSell").ToAltUniformCheckBox().Checked,
                            ServiceDescription = i.FindControl("litRatingServiceDescription").ToLiteral().Text,
                            Shipment = shipment,
                            TenantId = shipment.TenantId
                        };
                    })
                .ToList();

            shipment.AutoRatingAccessorials = accessorials;

            if (hidUpdateTerminalInfo.Value.ToBoolean())
            {
                hidUpdateTerminalInfo.Value = string.Empty;
                if (shipment.ServiceMode == ServiceMode.LessThanTruckload)
                {
                    var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
                    var vendor = sv == null ? null : sv.Vendor;
                    UpdateTerminalInformation(shipment, vendor == null ? string.Empty : vendor.Scac);
                }
                else
                {
                    shipment.OriginTerminal = null;
                    shipment.DestinationTerminal = null;
                }
            }
        }

        private void UpdateNotesFromView(Shipment shipment)
        {
            var notes = GetCurrentViewNotesUpdated()
                .Where(n => !n.IsLoadOrderNote)
                .Select(i =>
                    {
                        var note = new ShipmentNote(i.Id, i.Id != default(long))
                        {
                            Type = i.Type,
                            Archived = i.Archived,
                            Classified = i.Classified,
                            Message = i.Message,
                            Shipment = shipment,
                            Tenant = shipment.Tenant,
                        };
                        if (note.IsNew) note.User = ActiveUser;
                        return note;
                    })
                .ToList();
            if (!ActiveUser.HasAccessTo(ViewCode.CanSeeArchivedNotes)) // account for not listed archived notes.
                notes.AddRange(shipment.Notes.Where(n => n.Archived));
            shipment.Notes = notes;
        }

        private void UpdateCheckCallsFromView(Shipment shipment)
        {
            var checkCalls = GetCurrentViewCheckCallsUpdated()
                .Select(i =>
                    {
                        var checkCall = new CheckCall(i.Id, i.Id != default(long))
                        {
                            CallDate = i.CallDate,
                            EventDate = i.EventDate,
                            CallNotes = i.CallNotes,
                            EdiStatusCode = i.EdiStatusCode,
                            Shipment = shipment,
                            Tenant = ActiveUser.Tenant,
                        };
                        if (checkCall.IsNew) checkCall.User = ActiveUser;
                        return checkCall;
                    })
                .ToList();

            if (checkCalls.Any(c => c.Id == default(long)))
                hidNotificationsRequired.Value += string.Format("{0};", Milestone.ShipmentCheckCallUpdate);

            shipment.CheckCalls = checkCalls;
        }

        private void UpdateVendorsFromView(Shipment shipment)
        {
            var vendors = lstVendors
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidShipmentVendorId").ToCustomHiddenField().Value.ToLong();
                        return new ShipmentVendor(id, id != default(long))
                        {
                            VendorId = i.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                            Shipment = shipment,
                            TenantId = shipment.TenantId,
                            FailureCodeId = i.FindControl("ddlFailureCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                            FailureComments = i.FindControl("txtFailureComments").ToTextBox().Text,
                            ProNumber = i.FindControl("txtProNumber").ToTextBox().Text,
                            Primary = false,
                        };
                    })
                .ToList();

            vendors.Add(shipment.Vendors.FirstOrDefault(sv => sv.Primary));

            shipment.Vendors = vendors;
        }

        private void UpdateQuickPayOptionsFromView(Shipment shipment)
        {
            var options = lstQuickPayOptions
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidQuickPayOptionId").ToCustomHiddenField().Value.ToLong();
                        return new ShipmentQuickPayOption(id, id != default(long))
                        {
                            DiscountPercent = i.FindControl("txtDiscountPercent").ToTextBox().Text.ToDecimal(),
                            Description = i.FindControl("txtDescription").ToTextBox().Text,
                            Shipment = shipment,
                            TenantId = shipment.TenantId
                        };
                    })
                .ToList();

            shipment.QuickPayOptions = options;
        }

        private void UpdateAssetsFromView(Shipment shipment)
        {
            var assets = lstAssets
                .Items
                .Select(control =>
                    {
                        var id = control.FindControl("hidShipmentAssetId").ToCustomHiddenField().Value.ToLong();
                        return new ShipmentAsset(id, id != default(long))
                        {
                            DriverAssetId = control.FindControl("ddlShipmentDriverAsset").ToDropDownList().SelectedValue.ToLong(),
                            TractorAssetId = control.FindControl("ddlShipmentTractorAsset").ToDropDownList().SelectedValue.ToLong(),
                            TrailerAssetId = control.FindControl("ddlShipmentTrailerAsset").ToDropDownList().SelectedValue.ToLong(),
                            MilesRun = control.FindControl("txtShipmentMilesRun").ToTextBox().Text.ToDecimal(),
                            MileageEngine =
                                    control.FindControl("ddlMilageEngine").ToDropDownList().SelectedValue.ToInt().ToEnum<MileageEngine>(),
                            Primary = control.FindControl("chkPrimary").ToCheckBox().Checked,
                            Shipment = shipment,
                            TenantId = shipment.TenantId
                        };
                    })
                .ToList();

            shipment.Assets = assets;
        }

        private void UpdateDocumentsFromView(Shipment shipment)
        {
            var documents = lstDocuments
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong();
                        return new ShipmentDocument(id, id != default(long))
                        {
                            DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                            Name = i.FindControl("litDocumentName").ToLiteral().Text,
                            Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                            LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                            IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
                            Shipment = shipment,
                            TenantId = shipment.TenantId
                        };
                    })
                .ToList();

            shipment.Documents = documents;
        }



		protected void ReconcileCheckCallsAndShipmentRecordDates(IEnumerable<CheckCall> checkCalls, Shipment shipment)
		{
			foreach (var cc in checkCalls.Where(c=>!string.IsNullOrWhiteSpace(c.EdiStatusCode)))
				if (cc.IsNew)
				{
					switch (cc.EdiStatusCode.ToEnum<ShipStatMsgCode>())
					{
						case ShipStatMsgCode.AG:
							if (txtEstimatedDeliveryDate.Text == string.Empty)
							{
								txtEstimatedDeliveryDate.Text = cc.EventDate.FormattedShortDate();
								shipment.EstimatedDeliveryDate = cc.EventDate;
							}
							break;
						case ShipStatMsgCode.AF:
							if (txtActualPickupDate.Text == string.Empty)
							{
								txtActualPickupDate.Text = cc.EventDate.Date.FormattedShortDate();
								shipment.ActualPickupDate = cc.EventDate;
							}
							if (txtActaulPickupTime.Text == string.Empty)
								txtActaulPickupTime.Text = cc.EventDate.GetTime();
							break;
						case ShipStatMsgCode.D1:
							if (txtActualDeliveryDate.Text == string.Empty && txtActualPickupDate.Text != string.Empty)
							{
								txtActualDeliveryDate.Text = cc.EventDate.Date.FormattedShortDate();
								shipment.ActualDeliveryDate = cc.EventDate;
							}
							if (txtActualDeliveryTime.Text == string.Empty && txtActaulPickupTime.Text != string.Empty)
								txtActualDeliveryTime.Text = cc.EventDate.GetTime();
							break;
						case ShipStatMsgCode.AA:
							var origin = new ShipmentLocation();
							operationsAddressInputOrigin.RetrieveAddress(origin);
							if (origin.AppointmentDateTime <= DateUtility.SystemEarliestDateTime)
							{
								shipment.Origin.AppointmentDateTime = cc.EventDate;
								origin.AppointmentDateTime = cc.EventDate;
							}
							operationsAddressInputOrigin.LoadAddress(origin);
							break;
						case ShipStatMsgCode.AB:
							var destination = new ShipmentLocation();
							operationsAddressInputDestination.RetrieveAddress(destination);
							if (destination.AppointmentDateTime <= DateUtility.SystemEarliestDateTime)
							{
								shipment.Destination.AppointmentDateTime = cc.EventDate;
								destination.AppointmentDateTime = cc.EventDate;
							}
							operationsAddressInputDestination.LoadAddress(destination);
							break;
					}
				}
		}


        private List<ShipmentLocation> RetrieveViewStops(Shipment shipment)
        {
            var locations = new List<ShipmentLocation>();
            foreach (var item in lstStops.Items)
            {
                var control = item.FindControl("llicLocation").ToLocationListingInputControl();
                if (control == null) continue;
                var locationId = control.RetrieveLocationId();
                var location = new ShipmentLocation(locationId, locationId != default(long));
                if (location.IsNew)
                {
                    location.TenantId = shipment.TenantId;
                    location.Shipment = shipment;
                }
                control.UpdateLocation(location);

                locations.Add(location);
            }
            return locations;
        }



        private void LoadShipment(Shipment shipment, bool reloadShipmentCollections = true)
        {
            ConfigureServiceModeDisplay(shipment.ServiceMode);
	        ConfigureAccountBucketModeDisplay(shipment);

            var isNew = shipment.IsNew;

            tabDocuments.Visible = !isNew; // cannot add document until shipment is saved at least once.

            if (!isNew && reloadShipmentCollections) shipment.LoadCollections();

            hidShipmentId.Value = shipment.Id.ToString();

            // necessary to clean out old references prior to attempting to load new ones.
            lstReferences.Items.Clear();
            lstReferences.DataSource = new List<object>();
            lstReferences.DataBind();
            litReferenceCount.Text = new List<object>().BuildTabCount();

            //Alerts
            pnlPickupAlert.Visible = shipment.PickupIsLate();
            pnlDeliveryAlert.Visible = shipment.DeliveryIsLate();

            pnlCreatedInError.Visible = shipment.CreatedInError;
            pnlGuaranteedDeliveryServices.Visible = shipment.IsGuaranteedDeliveryService || shipment.Services.Any(s=>s.ServiceId == ActiveUser.Tenant.GuaranteedDeliveryServiceId);
            pnlExpeditedServices.Visible = shipment.IsExpeditedService;
            chkGuaranteedDeliveryService.Checked = shipment.IsGuaranteedDeliveryService;
            litGuaranteedDeliveryTime.Text = shipment.GuaranteedDeliveryServiceTime;
            litGuaranteedDeliveryRate.Text = shipment.GuaranteedDeliveryServiceRate.ToString();
            litGuaranteedDeliveryRateType.Text = shipment.GuaranteedDeliveryServiceRateType.ToString();
            litProject44QuoteNumber.Text = shipment.Project44QuoteNumber;
            chkExpeditedDeliveryService.Checked = shipment.IsExpeditedService;
            litExpeditedServiceDetails.Text = shipment.ExpeditedServiceTime;
            litExpeditedServiceRate.Text = shipment.ExpeditedServiceRate.ToString();

            if (!shipment.IsNew)
            {
                var mpOrder = new MacroPointOrderSearch().FetchMacroPointOrderByIdNumber(shipment.ShipmentNumber,
                                                                                         ActiveUser.TenantId);
                pnlMacroPointTracking.Visible = mpOrder != null;
                litMacroPointTrackingStatus.Text = mpOrder != null
                                                       ? string.IsNullOrEmpty(mpOrder.TrackingStatusDescription)
                                                             ? MacroPointRequestInitialized
                                                             : mpOrder.TrackingStatusDescription
                                                       : string.Empty;
            }
            else
            {
                pnlMacroPointTracking.Visible = false;
                litMacroPointTrackingStatus.Text = string.Empty;
            }


            //Header
            txtShipmentNumber.Text = shipment.ShipmentNumber;
            SetPageTitle(txtShipmentNumber.Text);
            txtStatus.Text = shipment.Status.FormattedString();
            hidStatus.Value = shipment.Status.ToString();
            DisplayCustomer(shipment.Customer);
            DisplayResellerAddition(shipment.ResellerAddition, shipment.BillReseller);

            // primary vendor
            var primaryShipmentVendor = shipment.Vendors.FirstOrDefault(v => v.Primary);
            hidPrimaryShipmentVendorId.Value = primaryShipmentVendor == null
                                                   ? default(long).ToString()
                                                   : primaryShipmentVendor.Id.ToString();
            DisplayVendor(primaryShipmentVendor == null ? null : primaryShipmentVendor.Vendor);

            txtPrimaryProNumber.Text = primaryShipmentVendor != null ? primaryShipmentVendor.ProNumber : string.Empty;
            ddlPrimaryFailureCode.SelectedValue = primaryShipmentVendor != null ? primaryShipmentVendor.FailureCodeId.GetString() : string.Empty;
            txtPrimaryFailureComments.Text = primaryShipmentVendor != null ? primaryShipmentVendor.FailureComments : string.Empty;

            // primary account bucket
            var shipmentAccountBucket = shipment.AccountBuckets.FirstOrDefault(a => a.Primary);
            DisplayAccountBucket(shipmentAccountBucket == null ? null : shipmentAccountBucket.AccountBucket);
            ddlAccountBucketUnit.SetSelectedAccountBucketUnit(shipmentAccountBucket == null ? default(long) : shipment.AccountBucketUnitId, shipmentAccountBucket == null ? default(long) : shipmentAccountBucket.AccountBucketId);
            hidAccountBucketUnitId.Value = shipment.AccountBucketUnitId.ToString();
            hidShipmentAccountBucketId.Value = shipmentAccountBucket == null
                                                   ? default(long).ToString()
                                                   : shipmentAccountBucket.Id.ToString();
            DisplayPrefix(shipment.Prefix);
            chkHidePrefix.Checked = shipment.HidePrefix;


            ddlPriority.SelectedValue = shipment.ShipmentPriorityId.ToString();

            ddlServiceMode.SelectedValue = shipment.ServiceMode.ToInt().ToString();
            chkIsPartialTruckLoad.Checked = shipment.IsPartialTruckload;
            txtPurchaseOrderNumber.Text = shipment.PurchaseOrderNumber;
            txtShipperReference.Text = shipment.ShipperReference;
            txtShipperBol.Text = shipment.ShipperBol;
            txtDateCreated.Text = shipment.DateCreated.FormattedShortDate();
            txtDesiredPickupDate.Text = shipment.DesiredPickupDate == DateUtility.SystemEarliestDateTime
                                            ? string.Empty
                                            : shipment.DesiredPickupDate.FormattedShortDate();
            txtActualPickupDate.Text = shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime
                                           ? string.Empty
                                           : shipment.ActualPickupDate.FormattedShortDate();
            txtActaulPickupTime.Text = shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime
                                           ? string.Empty
                                           : shipment.ActualPickupDate.GetTime();
            ddlEarlyPickup.SelectedValue = shipment.EarlyPickup;
            ddlLatePickup.SelectedValue = shipment.LatePickup;
            txtEstimatedDeliveryDate.Text = shipment.EstimatedDeliveryDate == DateUtility.SystemEarliestDateTime
                                                ? string.Empty
                                                : shipment.EstimatedDeliveryDate.FormattedShortDate();
            txtActualDeliveryDate.Text = shipment.ActualDeliveryDate == DateUtility.SystemEarliestDateTime
                                             ? string.Empty
                                             : shipment.ActualDeliveryDate.FormattedShortDate();
            txtActualDeliveryTime.Text = shipment.ActualDeliveryDate == DateUtility.SystemEarliestDateTime
                                             ? string.Empty
                                             : shipment.ActualDeliveryDate.GetTime();
            ddlEarlyDelivery.SelectedValue = shipment.EarlyDelivery;
            ddlLateDelivery.SelectedValue = shipment.LateDelivery;
            chkAutoRated.Checked = shipment.ShipmentAutorated;

            txtUser.Text = shipment.User != null ? shipment.User.Username : ActiveUser.Username;
            lnkUserPhone.InnerHtml = shipment.User != null ? shipment.User.Phone : ActiveUser.Phone;
            lnkUserPhone.HRef = string.Format("tel:{0}", shipment.User != null ? shipment.User.Phone : ActiveUser.Phone);
            lnkUserMobile.InnerHtml = shipment.User != null ? shipment.User.Mobile : ActiveUser.Mobile;
            lnkUserMobile.HRef = string.Format("tel:{0}", shipment.User != null ? shipment.User.Mobile : ActiveUser.Mobile);
            lnkUserFax.InnerHtml = shipment.User != null ? shipment.User.Fax : ActiveUser.Fax;
            lnkUserFax.HRef = string.Format("tel:{0}", shipment.User != null ? shipment.User.Fax : ActiveUser.Fax);
            lnkUserEmail.InnerHtml = shipment.User != null ? shipment.User.Email : ActiveUser.Email;
            lnkUserEmail.HRef = string.Format("mailto:{0}", shipment.User != null ? shipment.User.Email : ActiveUser.Email);

            txtMileage.Text = shipment.Mileage.ToString("f4");
            txtEmptyMileage.Text = shipment.EmptyMileage.ToString("f4");
            ddlMileageSource.SelectedValue = shipment.MileageSourceId.GetString();

            txtMiscField1.Text = shipment.MiscField1;
            txtMiscField2.Text = shipment.MiscField2;

            txtDriverName.Text = shipment.DriverName;
            txtDriverPhoneNumber.Text = shipment.DriverPhoneNumber;
            txtDriverTrailerNumber.Text = shipment.DriverTrailerNumber;

            operationsAddressInputOrigin.LoadAddress(shipment.Origin ?? new ShipmentLocation());
            chkNotifyPrimaryOriginContactOfShipmentUpdates.Checked = shipment.SendShipmentUpdateToOriginPrimaryContact;
            operationsAddressInputDestination.LoadAddress(shipment.Destination ?? new ShipmentLocation());
            chkNotifyPrimaryDestinationContactOfShipmentUpdates.Checked = shipment.SendShipmentUpdateToDestinationPrimaryContact;

            DisplayServicesAndEquipmentTypes(shipment);

            //Add. Details
            txtGeneralBolComments.Text = shipment.GeneralBolComments;
            txtCriticalBolComments.Text = shipment.CriticalBolComments;



            chkHazardousMaterial.Checked = shipment.HazardousMaterial;
            txtHazardousContactName.Text = shipment.HazardousMaterialContactName;
            txtHazardousContactPhone.Text = shipment.HazardousMaterialContactPhone;
            txtHazardousContactMobile.Text = shipment.HazardousMaterialContactMobile;
            txtHazardousContactEmail.Text = shipment.HazardousMaterialContactEmail;

            DisplayShipmentCoordinator(shipment.ShipmentCoordinator);
            DisplayCarrierCoordinator(shipment.CarrierCoordinator);

            //Items
            var nItems = shipment.Items
                                 .Select(i => new
                                 {
                                     i.Id,
                                     i.Pickup,
                                     Delivery = i.Delivery == (shipment.Stops.Count + 1) ? ViewDeliveryStopOrder : i.Delivery,
                                     i.ActualWeight,
                                     i.ActualLength,
                                     i.ActualWidth,
                                     i.ActualHeight,
                                     i.Quantity,
                                     i.PieceCount,
                                     i.Description,
                                     i.Comment,
                                     i.ActualFreightClass,
                                     i.RatedFreightClass,
                                     i.Value,
                                     i.IsStackable,
                                     i.HazardousMaterial,
                                     i.PackageTypeId,
                                     i.NMFCCode,
                                     i.HTSCode
                                 })
                                 .OrderBy(i => i.Pickup).ThenBy(i => i.Delivery)
                                 .ToList();

            lstItems.DataSource = nItems;
            lstItems.DataBind();
            tabItems.HeaderText = nItems.BuildTabCount(ItemsHeader);

            // non primary account buckets
            //Account Bucket
            var accountBuckets = shipment
                .AccountBuckets
                .Where(ab => !ab.Primary)
                .Select(ab => new
                {
                    ab.Id,
                    ab.AccountBucketId,
                    AccountBucketCode = ab.AccountBucket.Code,
                    AccountBucketDescription = ab.AccountBucket.Description
                })
                .ToList();
            lstAccountBuckets.DataSource = accountBuckets;
            lstAccountBuckets.DataBind();
            litAccountBucketCount.Text = accountBuckets.BuildTabCount();

            // non primary Vendors
            var vendors = shipment
                .Vendors
                .Where(v => !v.Primary)
                .Select(shv => new
                {
                    shv.Id,
                    shv.Vendor.VendorNumber,
                    shv.Vendor.Name,
                    shv.VendorId,
                    shv.FailureCodeId,
                    shv.FailureComments,
                    shv.ProNumber,
                    shv.Primary,
                    InsuranceAlert = shv.Vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[shv.TenantId]),
                    InsuranceTooltip = shv.Vendor.VendorInsuranceAlertText(ProcessorVars.VendorInsuranceAlertThresholdDays[shv.TenantId]),
                    shv.IsRelationBetweenVendorAndVendorBill
                })
                .ToList();
            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            litVendorsCount.Text = vendors.BuildTabCount();

            //References
            var customerReferencesFromList = lstReferences
                .Items
                .Select(control => new
                {
                    Id = control.FindControl("hidReferenceId").ToCustomHiddenField().Value.ToLong(),
                    Name = control.FindControl("txtName").ToTextBox().Text,
                    Value = control.FindControl("txtValue").ToTextBox().Text,
                    DisplayOnOrigin = control.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
                    DisplayOnDestination = control.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
                    Required = control.FindControl("chkRequired").ToAltUniformCheckBox().Checked
                })
                .ToList();

            var references = shipment
                .CustomerReferences
                .Select(r => new { r.Id, r.Name, r.Value, r.DisplayOnOrigin, r.DisplayOnDestination, r.Required })
                .ToList();

            var newReferences = customerReferencesFromList.Where(c => references.All(b => b.Name != c.Name)).ToList();

            newReferences.AddRange(references);
            var referencesToBind = isNew ? references : newReferences;
            lstReferences.DataSource = referencesToBind;
            lstReferences.DataBind();
            litReferenceCount.Text = referencesToBind.BuildTabCount();

            //Charges
            chkDeclineInsurance.Checked = shipment.DeclineInsurance;
            chkAuditedForInvoicing.Checked = shipment.AuditedForInvoicing;
            chkInDispute.Checked = shipment.InDispute;
            ddlInDisputeReason.SelectedValue = shipment.InDisputeReason.ToInt().ToString();
            var amountPaidToShipment = shipment.GetAmountPaidToShipmentViaMiscReceipts();
            txtCollectedPaymentAmount.Text = amountPaidToShipment.GetString();
            hidMiscReceiptsRelatedToShipmentExist.Value = (amountPaidToShipment > 0).ToString();

            lstCharges.DataSource = shipment.Charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = shipment.Charges.BuildTabCount(ChargesHeader);

            var totalDue = shipment.Charges.Sum(c => c.AmountDue);
            hidLoadedShipmentTotalDue.Value = totalDue.ToString();
            chargeStatistics.LoadCharges(shipment.Charges, hidResellerAdditionId.Value.ToLong());
            CheckForCreditAlert(shipment.Charges.Sum(c => c.AmountDue));

            // sales presentative details
            var commissionPercent = shipment.SalesRepresentative != null
                                        ? shipment.SalesRepresentativeCommissionPercent.ToString("f4")
                                        : string.Empty;
            var floorValue = shipment.SalesRepresentative != null ? shipment.SalesRepMinCommValue.ToString("f4") : string.Empty;
            var ceilingValue = shipment.SalesRepresentative != null ? shipment.SalesRepMaxCommValue.ToString("f4") : string.Empty;
            var addlEntityName = shipment.SalesRepresentative != null ? shipment.SalesRepAddlEntityName : string.Empty;
            var addlEntityCommPercent = shipment.SalesRepresentative != null
                                            ? shipment.SalesRepAddlEntityCommPercent.ToString("f4")
                                            : string.Empty;
            DisplaySalesRepresentative(shipment.SalesRepresentative, commissionPercent, floorValue, ceilingValue, addlEntityName,
                                       addlEntityCommPercent);


            //AutoRatingDetails
            litOriginalValue.Text = shipment.OriginalRateValue.ToString("f4");
            litVendorDiscountPercentage.Text = shipment.VendorDiscountPercentage.ToString("f4");
            litVendorFreightFloor.Text = shipment.VendorFreightFloor.ToString("f4");
            litVendorFreightCeiling.Text = shipment.VendorFreightCeiling.ToString("f4");
            litVendorFuelPercent.Text = shipment.VendorFuelPercent.ToString("f4");
            litCustomerFreightMarkup.Text = shipment.CustomerFreightMarkupValue.ToString("f4");
            litCustomerFreightMarkupPercent.Text = shipment.CustomerFreightMarkupPercent.ToString("f4");
            chkUseLowerCustomerFreightMarkup.Checked = shipment.UseLowerCustomerFreightMarkup;
            litBilledWeight.Text = shipment.BilledWeight.ToString("f4");
            litRatedWeight.Text = shipment.RatedWeight.ToString("f4");
            litRatedCubicFeet.Text = shipment.RatedCubicFeet.ToString("f4");
            litRatedPcf.Text = shipment.RatedPcf.ToString("f4");
            chkApplyDiscount.Checked = shipment.ApplyDiscount;
            chkLTLPackageSpecificRate.Checked = shipment.IsLTLPackageSpecificRate;
            litLineHaulProfitAdjRatio.Text = shipment.LineHaulProfitAdjustmentRatio.ToString("f4");
            litFuelProfitAdjustmentRatio.Text = shipment.FuelProfitAdjustmentRatio.ToString("f4");
            litAccessorialProfitAdjustmentRatio.Text = shipment.AccessorialProfitAdjustmentRatio.ToString("f4");
            litServiceProfitAdjustmentRatio.Text = shipment.ServiceProfitAdjustmentRatio.ToString("f4");
            litResellerAdditionalFreightMarkup.Text = shipment.ResellerAdditionalFreightMarkup.ToString("f4");
            chkResellerAdditionalFreightMarkupIsPercent.Checked = shipment.ResellerAdditionalFreightMarkupIsPercent;
            litResellerAdditionalFuelMarkup.Text = shipment.ResellerAdditionalFuelMarkup.ToString("f4");
            chkResellerAdditionalFuelMarkupIsPercent.Checked = shipment.ResellerAdditionalFuelMarkupIsPercent;
            litResellerAdditionalAccessorialMarkup.Text = shipment.ResellerAdditionalAccessorialMarkup.ToString("f4");
            chkResellerAdditionalAccessorialMarkupIsPercent.Checked = shipment.ResellerAdditionalAccessorialMarkupIsPercent;
            litResellerAdditionalServiceMarkup.Text = shipment.ResellerAdditionalServiceMarkup.ToString("f4");
            chkResellerAdditionalServiceMarkupIsPercent.Checked = shipment.ResellerAdditionalServiceMarkupIsPercent;
            chkDirectPoint.Checked = shipment.DirectPointRate;
            litSmallPackType.Text = shipment.SmallPackType;
            litSmallPackEngine.Text = shipment.SmallPackageEngine.FormattedString();
            hidSmallPackEngine.Value = shipment.SmallPackageEngine.ToInt().ToString();
            litRatingOverrideAddress.Text = shipment.VendorRatingOverrideAddress;
            chkCareOfAdddressFormatEnabled.Checked = shipment.CareOfAddressFormatEnabled;
            chkLinearFootRuleBypassed.Checked = shipment.LinearFootRuleBypassed;
            chkBypassLinearFootRuleForRating.Checked = shipment.ShipmentAutorated && shipment.LinearFootRuleBypassed;

            litOriginTerminal.Text = shipment.OriginTerminal != null
                                         ? BuildTerminalDisplay(shipment.OriginTerminal)
                                         : string.Empty;

            litDestinationTerminal.Text = shipment.DestinationTerminal != null
                                              ? BuildTerminalDisplay(shipment.DestinationTerminal)
                                              : string.Empty;

            lstOtherDetails.DataSource = shipment.AutoRatingAccessorials;
            lstOtherDetails.DataBind();

            // quick notes and check call

            // Notes
            var notes = shipment.Notes.Select(i => new OperationsViewNoteObj(i)).OrderByDescending(i => i.Id).ToList();
            var load = string.IsNullOrEmpty(shipment.ShipmentNumber)
                           ? null
                           : new LoadOrderSearch().FetchLoadOrderByLoadNumber(shipment.ShipmentNumber, shipment.TenantId);
            if (load != null)
            {
                var lnotes = load.Notes.Select(i => new OperationsViewNoteObj(i)).OrderByDescending(i => i.Id).ToList();
                notes.AddRange(lnotes);
            }

            btnAddLoadOrderDocuments.Visible = load != null && load.Documents.Any();

            var notesToBind = ActiveUser.HasAccessTo(ViewCode.CanSeeArchivedNotes)
                                  ? notes
                                  : notes.Where(n => !n.Archived).ToList();
            peNotes.LoadData(notesToBind);
            litNoteCount.Text = notesToBind.BuildTabCount(string.Empty);

            // check calls
            var checkCalls =
                shipment.CheckCalls.OrderByDescending(c => c.CallDate).Select(c => new OperationsViewCheckCallObj(c)).ToList();

            //If any check calls contain longitude and latitude coordinates, store them
            StoredLatLngCoordinates = shipment.CheckCalls.GetCoordinatesFromCheckCalls();
            peCheckCalls.LoadData(checkCalls);
            litCheckCallCount.Text = checkCalls.BuildTabCount(string.Empty);

            // check call alert (call only after loading check calls).
            ConfigureCheckCallAlert(shipment.Status, shipment.ServiceMode);

            //QuickPayOptions
            lstQuickPayOptions.DataSource = shipment.QuickPayOptions;
            lstQuickPayOptions.DataBind();
            litQuickPayCount.Text = shipment.QuickPayOptions.BuildTabCount();

            //Assets
            lstAssets.DataSource = shipment.Assets;
            lstAssets.DataBind();
            litAssetsCount.Text = shipment.Assets.BuildTabCount();

            //Documents
            lstDocuments.DataSource = shipment.Documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = shipment.Documents.BuildTabCount(DocumentsHeader);

            //Sort stop contacts with primary being first
            foreach (var stop in shipment.Stops)
                stop.Contacts = stop.Contacts.OrderByDescending(t => t.Primary).ToList();

            lstStops.DataSource = shipment.Stops.OrderBy(t => t.StopOrder).ToList();
            lstStops.DataBind();
            tabStops.HeaderText = shipment.Stops.BuildTabCount(StopsHeader);

            memberToolBar.ShowUnlock = shipment.HasUserLock(ActiveUser, shipment.Id);


            // Job
            var job = new Job(shipment.JobId);

            if (job.IsNew)
            {
                hidJobId.Value = string.Empty;
                txtJobNumber.Text = string.Empty;
                txtJobStep.Text = string.Empty;
            }
            else
            {
                hidJobId.Value = job.Id.ToString();
                txtJobNumber.Text = job.JobNumber;
                txtJobStep.Text = shipment.JobStep.ToString();
            }
        }



        private string BuildTerminalDisplay(LTLTerminalInfo info)
        {
            var b = new StringBuilder();

            if (!string.IsNullOrEmpty(info.Name.Trim())) b.AppendFormat("{0}{1}", info.Name, WebApplicationConstants.HtmlBreak);
            if (!string.IsNullOrEmpty(info.Street1.Trim()))
                b.AppendFormat("{0}{1}", info.Street1, WebApplicationConstants.HtmlBreak);
            if (!string.IsNullOrEmpty(info.Street2.Trim()))
                b.AppendFormat("{0}{1}", info.Street2, WebApplicationConstants.HtmlBreak);
            var cityStateZip = string.Format("{0} {1} {2}", info.City, info.State, info.PostalCode).Trim();
            if (!string.IsNullOrEmpty(cityStateZip))
                b.AppendFormat("{0}{1}", cityStateZip, WebApplicationConstants.HtmlBreak);
            if (!(string.IsNullOrEmpty(info.ContactName.Trim()) && string.IsNullOrEmpty(info.ContactTitle.Trim())))
                b.AppendFormat("{0}: {1}{2}", info.ContactTitle, info.ContactName, WebApplicationConstants.HtmlBreak);
            if (!(string.IsNullOrEmpty(info.Fax.Trim()) && string.IsNullOrEmpty(info.Phone.Trim())))
                b.AppendFormat("<abbr title='Phone'>P.</abbr> {0} &nbsp;&nbsp; <abbr title='Fax'>F.</abbr> {1}{2}", info.Phone,
                               info.Fax, WebApplicationConstants.HtmlBreak);
            if (!(string.IsNullOrEmpty(info.TollFree.Trim()) && string.IsNullOrEmpty(info.Email.Trim())))
                b.AppendFormat("<abbr title='Toll Free Number'>T.</abbr> {0} &nbsp;&nbsp; <abbr title='Email'>E.</abbr> {1}",
                               info.TollFree, info.Email);

            return b.ToString();
        }


        private void DisplayServicesAndEquipmentTypes(Shipment shipment)
        {
            DisplayServices(shipment.Services.Select(bs => bs.ServiceId));
            DisplayEquipments(shipment.Equipments.Select(be => be.EquipmentTypeId));
        }

        private void DisplayServices(IEnumerable<long> serviceIds)
        {
            hidServices.Value = string.Empty;

            foreach (var item in rptServices.Items.Cast<RepeaterItem>())
            {
                var check = serviceIds.Contains(item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong());
                item.FindControl("chkSelected").ToCheckBox().Checked = check;
                if (check)
                    hidServices.Value += string.Format("{0}{1}", item.FindControl("lblServiceCode").ToLabel().Text,
                                                       WebApplicationConstants.HtmlBreak);
            }

            lblServices.Text = hidServices.Value;
            hidServices.Value = hidServices.Value.StripTags();
        }

        private void DisplayEquipments(IEnumerable<long> equipmentTypeIds)
        {
            hidEquipment.Value = string.Empty;

            foreach (var item in rptEquipmentTypes.Items.Cast<RepeaterItem>())
            {
                var check = equipmentTypeIds.Contains(item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong());
                item.FindControl("chkSelected").ToCheckBox().Checked = check;
                if (check) hidEquipment.Value += string.Format("{0},", item.FindControl("lblEquipmentTypeCode").ToLabel().Text);
            }

            if (!string.IsNullOrEmpty(hidEquipment.Value))
                hidEquipment.Value = hidEquipment.Value.Substring(0, hidEquipment.Value.Length - 1);
            lblEquipment.Text = hidEquipment.Value;
        }


        private void LoadCustomerCustomFields(Customer customer)
        {
            var shipmentReferences = lstReferences
                .Items
                .Select(control => new
                {
                    Id = control.FindControl("hidReferenceId").ToCustomHiddenField().Value.ToLong(),
                    Name = control.FindControl("txtName").ToTextBox().Text,
                    Value = control.FindControl("txtValue").ToTextBox().Text,
                    DisplayOnOrigin = control.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
                    DisplayOnDestination = control.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
                    Required = control.FindControl("chkRequired").ToAltUniformCheckBox().Checked
                })
                .ToList();


            var customFieldsFromCustomer =
                customer.CustomFields.Where(c => shipmentReferences.All(b => b.Name != c.Name)).ToList();

            var customerReferences = customFieldsFromCustomer
                .Select(r => new
                {
                    Id = default(long),
                    r.Name,
                    Value = string.Empty,
                    r.DisplayOnOrigin,
                    r.DisplayOnDestination,
                    r.Required
                })
                .ToList();

            shipmentReferences.AddRange(customerReferences);

            lstReferences.DataSource = shipmentReferences;
            lstReferences.DataBind();
            litReferenceCount.Text = shipmentReferences.BuildTabCount();
        }


        private void ProcessSmallPackageInitialBookingFinalization()
        {
            // process to complete small package booking after shipment is saved the first time!


            var shipment = new Shipment(hidShipmentId.Value.ToLong());
            shipment.LoadCollections();

            if (shipment.ServiceMode == ServiceMode.SmallPackage)
            {
                LockRecord(shipment);

                var errs = this.FinalizePackageShipmentBooking(shipment);
                if (errs.Any())
                {
                    DisplayMessages(errs);
                    if (UnLock != null)
                        UnLock(this, new ViewEventArgs<Shipment>(shipment));
                    return;
                }

                hidFlag.Value = SmallPackageFinalSaveFlag;

                if (Save != null)
                    Save(this, new ViewEventArgs<Shipment>(shipment));

                if (LoadAuditLog != null)
                    LoadAuditLog(this, new ViewEventArgs<Shipment>(shipment));
            }

        }



        private void ProcessTransferredRequest(Shipment shipment)
        {
            if (shipment == null) return;

            if (shipment.IsNew)
            {
                shipment.Customer = new Customer();
                shipment.DateCreated = DateTime.Now;
                shipment.EstimatedDeliveryDate = DateTime.Now.AddDays(1);
                shipment.ActualDeliveryDate = DateUtility.SystemEarliestDateTime;
                shipment.ActualPickupDate = DateUtility.SystemEarliestDateTime;
                shipment.DesiredPickupDate = DateTime.Now;
                shipment.EarlyPickup = TimeUtility.DefaultOpen;
                shipment.EarlyDelivery = TimeUtility.DefaultClose;
                shipment.LatePickup = TimeUtility.DefaultOpen;
                shipment.LateDelivery = TimeUtility.DefaultClose;
                shipment.Origin = new ShipmentLocation();
                shipment.Destination = new ShipmentLocation();
                shipment.Status = ShipmentStatus.Scheduled;
                shipment.MileageSourceId = default(long);
            }
            else if (!ActiveUser.HasAccessToCustomer(shipment.Customer.Id)) return;

            LoadShipment(shipment);

            SetEditStatus(shipment.IsNew);

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Shipment>(shipment));
        }


        private void ProcessNotifications(Shipment shipment)
        {
            var notifications = hidNotificationsRequired.Value
                                                        .Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                                                        .Distinct()
                                                        .ToList();
            foreach (var notification in notifications)
                switch (notification.ToEnum<Milestone>())
                {
                    case Milestone.NewShipmentRequest:
                        this.NotifyNewShipment(shipment, true);
                        break;
                    case Milestone.ShipmentInTransit:
                        this.NotifyShipmentInTransit(shipment);
                        break;
                    case Milestone.ShipmentDelivered:
                        this.NotifyShipmentDelivered(shipment);
                        break;
                    case Milestone.ShipmentVoided:
                        this.NotifyCancelledShipment(shipment);
                        break;
                    case Milestone.ShipmentVoidReversed:
                        this.NotifyCancelledShipmentReversed(shipment);
                        break;
                    case Milestone.ShipmentCheckCallUpdate:
                        this.NotifyCheckCallUpdate(shipment, PageStore[DefaultStoreKey] as List<CheckCall>);
                        PageStore[DefaultStoreKey] = null;
                        break;
                }
        }

        private void VoidShipment(bool confirmForSmallPack = true)
        {
            var shipment = new Shipment(hidShipmentId.Value.ToLong());
            if (shipment.IsNew)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Unable to cancel new shipment that has not been previously saved.") });
                return;
            }

            if (confirmForSmallPack && shipment.ServiceMode == ServiceMode.SmallPackage &&
                shipment.Status == ShipmentStatus.InTransit)
            {
                messageBox.Icon = MessageIcon.Warning;
                messageBox.Message =
                    "If you void this shipment, you will need to contact the small package carrier to cancel the shipment as the shipment has already been tendered to them. Do you want to void this shipment?";
                messageBox.Visible = true;
                messageBox.Button = MessageButton.YesNo;
                hidFlag.Value = VoidShipmentFlag;
                return;
            }

            shipment.LoadCollections();
            shipment.TakeSnapShot();

            UpdateNotesFromView(shipment);
            shipment.Status = ShipmentStatus.Void;

            hidFlag.Value = SaveFlag;
            hidNotificationsRequired.Value += string.Format("{0};", Milestone.ShipmentVoided);
            RefundOrVoidMiscReceipts = true;

            if (Save != null)
                Save(this, new ViewEventArgs<Shipment>(shipment));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Shipment>(shipment));
        }

        private void ReverseVoidShipment(bool confirmForSmallPack = true)
        {
            var shipment = new Shipment(hidShipmentId.Value.ToLong());
            if (shipment.IsNew)
            {
                DisplayMessages(new[]
                    {
                        ValidationMessage.Error(
                            "Unable to reverse cancel on new shipment that has not been previously saved and cancelled.")
                    });
                return;
            }
            if (confirmForSmallPack && shipment.ServiceMode == ServiceMode.SmallPackage)
            {
                messageBox.Icon = MessageIcon.Warning;
                messageBox.Message =
                    "If you reverse the void on this shipment, it is your responsibility to recreate the shipment with the small package carrier as well. Do you want to reverse the void on this shipment?";
                messageBox.Visible = true;
                messageBox.Button = MessageButton.YesNo;
                hidFlag.Value = ReverseVoidShipmentFlag;
                return;
            }

            shipment.LoadCollections();
            shipment.TakeSnapShot();

            // account for missing actual pickup date in legacy data.  Cannot have actual delivery date without actual pickup date.
            if ((shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime) && (shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime))
                shipment.ActualPickupDate = shipment.DesiredPickupDate;

            shipment.Status = shipment.InferShipmentStatus(true);
            hidFlag.Value = SaveFlag;

            hidNotificationsRequired.Value += string.Format("{0};", Milestone.ShipmentVoidReversed);

            if (Save != null)
                Save(this, new ViewEventArgs<Shipment>(shipment));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Shipment>(shipment));
        }

        private void DuplicateShipment(bool roundTrip)
        {
            var shipment = UpdateShipment().Duplicate(roundTrip);

            LoadShipment(shipment);

            // account for sales rep & reseller addition on customer if need be.  Reload customer this way will force refresh on sales rep & reseller additions
            if (shipment.Customer.Active)
                DisplayCustomer(shipment.Customer, true);
            else
            {
                DisplayCustomer(null, true);
                DisplayMessages(new[] { ValidationMessage.Error("Customer removed as account is not active!") });
            }

            SetEditStatus(true);

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
        }

        private void ReturnToShipmentDashboard()
        {
            var oldShipment = new Shipment(hidShipmentId.Value.ToLong(), false);

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Shipment>(oldShipment));

            Response.Redirect(ShipmentDashboardView.PageAddress);
        }

        private void ReturnToShipmentsToBeInvoiced()
        {
            var oldShipment = new Shipment(hidShipmentId.Value.ToLong(), false);

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Shipment>(oldShipment));

            Response.Redirect(ShipmentsToBeInvoicedView.PageAddress);
        }

        private void LoadVendorLaneBuySellData()
        {
            var origin = new Location();
            operationsAddressInputOrigin.RetrieveAddress(origin);
            var destination = new Location();
            operationsAddressInputDestination.RetrieveAddress(destination);

            var criteria = new LaneHistorySearchCriteria
            {
                DestinationCountryId = destination.CountryId,
                DestinationPostalCode = destination.PostalCode,
                OriginCountryId = origin.CountryId,
                OriginPostalCode = origin.PostalCode,
                DesiredPickupDate = txtDesiredPickupDate.Text.ToDateTime(),
                TotalWeight = lstItems.Items.Sum(control => control.FindControl("txtWeight").ToTextBox().Text.ToDecimal())
            };
            vendorLaneHistoryBuySell.LoadVendorLaneHistory(criteria);
            vendorLaneHistoryBuySell.Visible = true;
        }

        private void GenerateBOL()
        {
            var shipment = UpdateShipment();
            documentDisplay.Title = string.Format("{0} Bill of Lading", shipment.ShipmentNumber);
            documentDisplay.DisplayHtml(this.GenerateBOL(shipment));
            documentDisplay.Visible = true;
        }

        private void GenerateRouteSummary()
        {
            var shipment = UpdateShipment();
            documentDisplay.Title = string.Format("{0} RouteSummary", shipment.ShipmentNumber);
            documentDisplay.DisplayHtml(this.GenerateShipmentRouteSummary(shipment));
            documentDisplay.Visible = true;
        }

        private void LoadVendorRateAgreementSelection()
        {
            var shipment = new Shipment(hidShipmentId.Value.ToLong());

            // NOTE: IE hack to ensure that when in readonly mode charge code id is loaded with charge codes
            // IE will not render hidden field with value if in disabled panel
            var charges = pnlCharges.Enabled
                              ? lstCharges.Items
                                          .Select(i => new Charge
                                          {
                                              Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                                              UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                                              UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                                              UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                                              Comment = i.FindControl("txtComment").ToTextBox().Text,
                                              ChargeCodeId =
                                                      i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                                          })
                                          .ToList()
                              : shipment.Charges.Cast<Charge>().ToList();

            var vendors = shipment.Vendors
                                  .Where(sv => !sv.Primary)
                                  .Select(i => new
                                  {
                                      Id = i.VendorId,
                                      i.Vendor.Name
                                  })
                                  .ToDictionary(i => i.Id, i => i.Name);

            var pv = shipment.Vendors.FirstOrDefault(sv => sv.Primary);
            var key = hidEditStatus.Value.ToBoolean() // IE hack as ID will not render hidden field if not enabled!
                          ? hidPrimaryVendorId.Value.ToLong()
                          : pv == null ? default(long) : pv.VendorId;
            if (!vendors.ContainsKey(key))
                vendors.Add(key, txtVendorName.Text);

            if (vendors.Count > 1)
            {
                vendorRateAgreement.LoadData(vendors, charges);
                vendorRateAgreement.Visible = true;
            }
            else
            {
                GenerateVendorRateAgreement(true);
            }
        }

        private void LoadShipmentLabelGenerator()
        {
            shippingLabelGenerator.GenerateLabels(hidShipmentId.Value.ToLong());
            shippingLabelGenerator.Visible = true;
        }

        private void GenerateVendorRateAgreement(bool ignoreVendorRateAgreementSelector)
        {
            var shipment = UpdateShipment();

            if (!ignoreVendorRateAgreementSelector && shipment.Vendors.Count > 1)
            {
                shipment.Vendors.RemoveAll(v => v.VendorId != vendorRateAgreement.RetrieveVendorId());
                shipment.Vendors[0].Primary = true; //sets only vendor as primary (for processor)

                var ids = (from idx in vendorRateAgreement.RetrieveSelectedIndices()
                           where idx < shipment.Charges.Count
                           select shipment.Charges[idx].Id).ToList();
                shipment.Charges.RemoveAll(c => !ids.Contains(c.Id));

                vendorRateAgreement.Visible = false;
            }

            documentDisplay.Title = string.Format("{0} Carrier Agreement", shipment.ShipmentNumber);
            documentDisplay.DisplayHtml(this.GenerateCarrierAgreement(shipment));
            documentDisplay.Visible = true;
        }

        private void EditInvoicedShipment()
        {
            var shipment = new Shipment(hidShipmentId.Value.ToLong(), false);

            var vendors = shipment.Vendors.Select(v => new
            {
                v.Id,
                v.Primary,
                v.ProNumber,
                v.Vendor.VendorNumber,
                v.Vendor.Name,
                v.FailureCodeId,
                v.FailureComments
            }).OrderBy(v => v.Primary).ToList();


            lstEditInvoicedShipment.DataSource = vendors;
            lstEditInvoicedShipment.DataBind();

            txtEditActualDeliveryDate.Text = shipment.ActualDeliveryDate == DateUtility.SystemEarliestDateTime
                                                 ? string.Empty
                                                 : shipment.ActualDeliveryDate.FormattedShortDate();
            txtEditActualPickupDate.Text = shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime
                                                 ? string.Empty
                                                 : shipment.ActualPickupDate.FormattedShortDate();
            txtReference.Text = shipment.ShipperReference;
            chkCreatedInError.Checked = shipment.CreatedInError;

            pnlDimScreen.Visible = true;
            pnlEditInvoicedShipment.Visible = true;

            LockRecord(shipment);
        }

        private void LockRecord(Shipment shipment)
        {
            if (Lock == null || shipment.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<Shipment>(shipment));

            LoadShipment(shipment);
        }

        private void ConfigureServiceModeDisplay(ServiceMode serviceMode)
        {
            var show = serviceMode == ServiceMode.Truckload || serviceMode == ServiceMode.Air;
            pnlDelivery.Visible = show || serviceMode == ServiceMode.Rail;
            pnlPartialTruckload.Visible = show;
            tabStops.Visible = show;

            pnlRatingDetails.Visible = serviceMode == ServiceMode.LessThanTruckload || serviceMode == ServiceMode.SmallPackage;
            ddlEarlyDelivery.SelectedValue = TimeUtility.DefaultOpen; // 08:00
            ddlLateDelivery.SelectedValue = TimeUtility.DefaultClose; // 17:00
        }

	    private void ConfigureAccountBucketModeDisplay(Shipment shipment)
	    {
			pnlAccountBucketSearch.Enabled = shipment.ShipmentAccountBucketCanBeChanged();
		}

	    private void StopMacroPointTracking()
        {
            var orderDto = new MacroPointOrderSearch().FetchMacroPointOrderByIdNumber(txtShipmentNumber.Text, ActiveUser.TenantId);
            var order = new MacroPointOrder(orderDto.Id);

            var settings = ProcessorVars.IntegrationVariables[ActiveUser.TenantId].MacroPointParams;
            if (settings == null)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Tenant does not have existing MacroPoint Parameters") });
                return;
            }
            var serviceHandler = new MacroPointServiceHandler(settings, ActiveUser);


            order.TakeSnapShot();
            var stopped = serviceHandler.StopOrder(order);
            if (serviceHandler.Errors.Any())
            {
                DisplayMessages(serviceHandler.Errors.Select(er => ValidationMessage.Error(er)));
                return;
            }

            if (stopped)
            {
                Exception ex;
                var errMsgs = new MacroPointOrderUpdateHandler().SaveMacroPointOrder(order, out ex, ActiveUser);
                if (ex != null) ErrorLogger.LogError(ex, Context);
                if (errMsgs.Any())
                {
                    DisplayMessages(errMsgs);
                    return;
                }

                memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));
                DisplayMessages(new[] { ValidationMessage.Information("MacroPoint tracking order has been succesfully stopped") });
            }
        }

        private void SendMacroPointLocationUpdateRequest()
        {
            var settings = ProcessorVars.IntegrationVariables[ActiveUser.TenantId].MacroPointParams;
            if (settings == null)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Tenant does not have existing MacroPoint Parameters") });
                return;
            }

            var serviceHandler = new MacroPointServiceHandler(settings, ActiveUser);

            var orderDto = new MacroPointOrderSearch().FetchMacroPointOrderByIdNumber(txtShipmentNumber.Text, ActiveUser.TenantId);
            var order = new MacroPointOrder(orderDto.Id);

            var success = serviceHandler.RequestLocationUpdate(order);
            if (serviceHandler.Errors.Any())
            {
                DisplayMessages(serviceHandler.Errors.Select(er => ValidationMessage.Error(er)));
                return;
            }

            if (success)
            {
                var shipment = order.RetrieveCorrespondingShipment();
                var charge = new ShipmentCharge
                {
                    ChargeCode = ActiveUser.Tenant.MacroPointChargeCode,
                    Quantity = 1,
                    UnitBuy = settings.PingCost.ToDecimal(),
                    UnitSell = default(decimal),
                    Tenant = ActiveUser.Tenant,
                    Comment = string.Empty,
                    UnitDiscount = default(decimal),
                    Shipment = shipment
                };

                Exception ex;
                var shipmentUpdateHandler = new ShipmentUpdateHandler();
                var errMsgs = shipmentUpdateHandler.SaveShipmentCharge(charge, out ex, ActiveUser);
                if (errMsgs.Any())
                {
                    DisplayMessages(errMsgs);
                    return;
                }
                AddChargeToView(charge);

                if (LoadAuditLog != null)
                    LoadAuditLog(this, new ViewEventArgs<Shipment>(shipment));

                DisplayMessages(new[] { ValidationMessage.Information("Update Location request has been successfully submitted") });
            }
        }

        private void AddChargeToView(ShipmentCharge charge)
        {
            var charges = lstCharges.Items
                                    .Select(i => new
                                    {
                                        Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
                                        Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                                        UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                                        UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                                        UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                                        Comment = i.FindControl("txtComment").ToTextBox().Text,
                                        VendorBillId = i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong(),
                                        ChargeCodeId =
                                                     i.FindControl("ddlChargeCode")
                                                      .ToCachedObjectDropDownList()
                                                      .SelectedValue.ToLong()
                                    })
                                    .ToList();

            charges.Insert(0, new
            {
                charge.Id,
                charge.Quantity,
                charge.UnitBuy,
                charge.UnitSell,
                charge.UnitDiscount,
                charge.Comment,
                VendorBillId = default(long),
                charge.ChargeCodeId
            });

            lstCharges.DataSource = charges;
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            var chargeList = charges
                .Select(c => new Charge
                {
                    ChargeCodeId = c.ChargeCodeId,
                    Comment = c.Comment,
                    Quantity = c.Quantity,
                    UnitBuy = c.UnitBuy,
                    UnitSell = c.UnitSell,
                    UnitDiscount = c.UnitDiscount,
                })
                .ToList();

            chargeStatistics.LoadCharges(chargeList, hidResellerAdditionId.Value.ToLong());
            CheckForCreditAlert(chargeList.Sum(c => c.AmountDue));
        }

        private void DispatchShipment()
        {
            if (string.IsNullOrEmpty(ActiveUser.Phone))
            {
                DisplayMessages(new[] { ValidationMessage.Error(WebApplicationConstants.DispatchShipmentMissingUserPhoneNumberErrMsg) });
                return;
            }

            var shipment = UpdateShipment();
            var dispatchResponse = this.DispatchShipment(shipment);

            if (dispatchResponse.CheckCall == null)
            {
                var messages = new List<ValidationMessage>
                    {
                        ValidationMessage.Error("Error dispatching shipment.  Please call carrier directly!")
                    };

                if (dispatchResponse.Messages.Any())
                    messages.AddRange(dispatchResponse.Messages);

                DisplayMessages(messages);
                return;
            }

            var checkCall = dispatchResponse.CheckCall;
            checkCall.Tenant = shipment.Tenant;
            checkCall.Shipment = shipment;
            shipment.CheckCalls.Add(checkCall);
            LoadShipment(shipment, false);
            DisplayMessages(new List<ValidationMessage>
                {
                    ValidationMessage.Information("Pickup #: {0}. Remember to save shipment!", checkCall.CallNotes)
                });
        }

        private void InitiateTracking()
        {
            var shipmentId = hidShipmentId.Value.ToLong();
            var shipment = new Shipment(shipmentId, shipmentId != default(long));

            var primaryId = hidPrimaryVendorId.Value.ToLong();
            var primaryVendor = new Vendor(primaryId, primaryId != default(long));

            try
            {

                var trackRequest = new TrackRequest
                    {
                        accountToken = primaryVendor.Communication.SMC3TestAccountToken,
                        eventTypes = new List<Smc.Eva.EventType> {new Smc.Eva.EventType {eventType = EventTypes.All}},
                        referenceNumbers = new List<Smc.Eva.ReferenceNumber>
                            {
                                new Smc.Eva.ReferenceNumber
                                    {
                                        referenceNumber = shipment.ShipmentNumber,
                                        originPostalCode = shipment.Origin.PostalCode,
                                        destinationPostalCode = shipment.Destination.PostalCode,
                                        expectedPickupDate = shipment.DesiredPickupDate.ToString("yyyyMMdd")
                                    },
                                new Smc.Eva.ReferenceNumber
                                    {
                                        referenceNumber = shipment.Prefix.Code + shipment.ShipmentNumber,
                                        originPostalCode = shipment.Origin.PostalCode,
                                        destinationPostalCode = shipment.Destination.PostalCode,
                                        expectedPickupDate = shipment.DesiredPickupDate.ToString("yyyyMMdd")
                                    }
                            },
                        referenceType = ReferenceTypes.BillOfLadingNumber,
                        scac = primaryVendor.Scac,
                    };

                var evaServiceHandler = new EvaServiceHandler(GetSettings());
                var trackResponse = evaServiceHandler.InitiateTracking(trackRequest);

                var trackRequestResponse = new SMC3TrackRequestResponse
                {
                    TransactionId = trackResponse.transactionID,
                    ShipmentId = shipment.Id,
                    RequestData = JsonConvert.SerializeObject(trackRequest),
                    ResponseData = JsonConvert.SerializeObject(trackResponse),
                    Scac = primaryVendor.Scac,
                    DateCreated = DateTime.Now
                };

                if (SaveSmc3TrackRequestResponse != null)
                    SaveSmc3TrackRequestResponse(this, new ViewEventArgs<SMC3TrackRequestResponse>(trackRequestResponse));
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
            }
        }

        private void InitiateProject44Tracking()
        {
            var shipmentId = hidShipmentId.Value.ToLong();
            var shipment = new Shipment(shipmentId, shipmentId != default(long));

            var primaryId = hidPrimaryVendorId.Value.ToLong();
            var primaryVendor = new Vendor(primaryId, primaryId != default(long));

            try
            {

                var p44Settings = new Project44ServiceSettings
                {
                    Hostname = WebApplicationSettings.Project44Host,
                    Username = WebApplicationSettings.Project44Username,
                    Password = WebApplicationSettings.Project44Password,
                    PrimaryLocationId = WebApplicationSettings.Project44PrimaryLocationId
                };

                var p44Wrapper = new Project44Wrapper(p44Settings);
                var trackResponse = p44Wrapper.InitiateTracking(shipment, primaryVendor.Scac);

                if (trackResponse == null)
                {
                    var messages = p44Wrapper.ErrorMessages.Select(e => ValidationMessage.Error(e)).ToList();
                    DisplayMessages(messages);
                    return;
                }

                var trackRequestResponse = new Project44TrackingResponse
                {
                    Project44Id = trackResponse.Shipment.Id.GetString(),
                    ShipmentId = shipment.Id,
                    UserId = ActiveUser.Id,
                    ResponseData = JsonConvert.SerializeObject(trackResponse),
                    Scac = primaryVendor.Scac,
                    DateCreated = DateTime.Now,
                };

                if (SaveProject44TrackRequestResponse != null)
                    SaveProject44TrackRequestResponse(this, new ViewEventArgs<Project44TrackingResponse>(trackRequestResponse));
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
            }
        }

        private void VendorChargeConfirmation()
        {
            var shipment = new Shipment(hidShipmentId.Value.ToLong());
            if (shipment.KeyLoaded)
            {
                var criteria = new SearchDefaultsItem
                {
                    Columns = new List<ParameterColumn> { AccountingSearchFields.ChargeNumber.ToParameterColumn() },
                    SortAscending = true,
                    SortBy = null,
                    Type = SearchDefaultsItemType.Shipments
                };
                criteria.Columns[0].DefaultValue = shipment.ShipmentNumber;
                criteria.Columns[0].Operator = Operator.Equal;
                Session[WebApplicationConstants.ChargeViewSearchCriteria] = criteria;
            }

            if (ActiveUser.HasAccessTo(ViewCode.VendorChargesConfirmation))
                Response.Redirect(Accounting.VendorChargeConfirmation.PageAddress);
            else if (ActiveUser.HasAccessTo(ViewCode.CustomerLoadDashboard))
                Response.Redirect(CustomerLoadDashboardView.PageAddress);

        }

        private EvaSettings GetSettings()
        {
            return WebApplicationSettings.SmcEvaSettings;
        }

        private void BuildVendorsDropdownListOnChargesTab(DropDownList ddlVendors, long selectedValue, bool enableVendorSelection)
        {
            var vendorCollection = VendorCollection;

            if (!vendorCollection.Select(s => s.Value.ToLong()).Contains(selectedValue))
            {
                var vendor = new Vendor(selectedValue);
                vendorCollection.Add(new ViewListItem { Text = vendor.Name, Value = vendor.Id.ToString() });
            }

            ddlVendors.DataSource = vendorCollection;
            ddlVendors.SelectedValue = selectedValue.GetString();
            ddlVendors.Enabled = enableVendorSelection;
            ddlVendors.DataBind();
            upPnlCharges.Update();
        }

	    private void CheckAndAddGuaranteedService(Shipment shipment)
	    {
		    if (shipment.IsGuaranteedDeliveryService && ActiveUser.Tenant.GuaranteedDeliveryServiceId != default(long) &&
		        shipment.Services.All(s => s.ServiceId != ActiveUser.Tenant.GuaranteedDeliveryServiceId))
		    {
			    shipment.Services.Add(new ShipmentService
			    {
				    TenantId = ActiveUser.TenantId,
				    Shipment = shipment,
				    ServiceId = ActiveUser.Tenant.GuaranteedDeliveryServiceId
			    });

		    }
	    }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ShipmentHandler(this).Initialize();
            new VendorRejectionLogProcHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;

            shipmentFinder.OpenForEditEnabled = Access.Modify;

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));

            SetPageTitle(txtShipmentNumber.Text);

            if (IsPostBack)
            {
                if (!string.IsNullOrEmpty(hidEquipment.Value)) lblEquipment.Text = hidEquipment.Value;
                if (!string.IsNullOrEmpty(hidServices.Value)) lblServices.Text = hidServices.Value.ReplaceTags();
                return;
            }

            pecPayment.PaymentGatewayType = ActiveUser.Tenant.PaymentGatewayType;

            aceCustomer.SetupExtender(new Dictionary<string, string>
                {
                    {AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString()}
                });
            aceVendor.SetupExtender(new Dictionary<string, string>
                {
                    {AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()}
                });
            aceAccountBucketCode.SetupExtender(new Dictionary<string, string>
                {
                    {AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()}
                });
            acePrefixCode.SetupExtender(new Dictionary<string, string>
                {
                    {AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()}
                });
            aceShipmentCoordinator.SetupExtender(new Dictionary<string, string>
                {
                    {AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()},
                    {AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString()},
                    {AutoCompleteUtility.Keys.IsShipmentCoordinator, true.ToString()}
                });
            aceCarrierCoordinator.SetupExtender(new Dictionary<string, string>
                {
                    {AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()},
                    {AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString()},
                    {AutoCompleteUtility.Keys.IsCarrierCoordinator, true.ToString()}
                });


            BindTimeDropDowns();

            if (Loading != null)
                Loading(this, new EventArgs());

            SetEditStatus(false);

            if (Session[WebApplicationConstants.TransferShipmentId] != null)
            {
                ProcessTransferredRequest(new Shipment(Session[WebApplicationConstants.TransferShipmentId].ToLong()));
                Session[WebApplicationConstants.TransferShipmentId] = null;
                return;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(
                    new ShipmentSearch().FetchShipmentByShipmentNumber(
                        Request.QueryString[WebApplicationConstants.TransferNumber].GetString(), ActiveUser.TenantId));
        }



        protected void OnShipmentFinderItemSelected(object sender, ViewEventArgs<Shipment> e)
        {
            if (hidShipmentId.Value.ToLong() != default(long))
            {
                var oldShipment = new Shipment(hidShipmentId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Shipment>(oldShipment));
            }

            var shipment = e.Argument;

            LoadShipment(shipment);

            shipmentFinder.Visible = false;

            if (shipmentFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<Shipment>(shipment));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Shipment>(shipment));

            litErrorMessages.Text = string.Empty;
        }

        protected void OnShipmentFinderItemCancelled(object sender, EventArgs e)
        {
            shipmentFinder.Visible = false;
        }



        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            shipmentFinder.Visible = true;
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var shipment = UpdateShipment();

            var charges = shipment.Charges.Sum(c => c.AmountDue);
            var outstanding = hidCustomerOutstandingBalance.Value.ToDecimal() - hidLoadedShipmentTotalDue.Value.ToDecimal();
            var credit = (shipment.Customer ?? new Customer()).CreditLimit - outstanding;

            if (charges > credit && !ActiveUser.HasAccessTo(ViewCode.BypassCustomerCreditStop))
            {
                DisplayMessages(new[]
                    {
                        ValidationMessage.Error("Insufficient customer credit [{0:c2}] to cover shipment total [{1:c2}]", credit, charges)
                    });
                return;
            }

            hidFlag.Value = shipment.IsNew && shipment.ServiceMode == ServiceMode.SmallPackage
                                ? SmallPackageInitialSaveFlag
                                : SaveFlag;

            // get check call(s) for notification and store
            var checkCalls =
                shipment.CheckCalls.Where(cc => cc.IsNew && !string.IsNullOrEmpty(cc.EdiStatusCode)).Select(cc => cc).ToList();
            if (_originApptChanged)
                checkCalls.Add(new CheckCall
                {
                    EventDate = shipment.Origin.AppointmentDateTime,
                    Shipment = shipment,
                    CallDate = DateTime.Now,
                    Tenant = shipment.Tenant,
                    CallNotes = string.Empty,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    User = ActiveUser
                });
            if (_destApptChanged)
                checkCalls.Add(new CheckCall
                {
                    EventDate = shipment.Destination.AppointmentDateTime,
                    Shipment = shipment,
                    CallDate = DateTime.Now,
                    Tenant = shipment.Tenant,
                    CallNotes = string.Empty,
                    EdiStatusCode = ShipStatMsgCode.AB.GetString(),
                    User = ActiveUser
                });
            if (checkCalls.Any()) PageStore[DefaultStoreKey] = checkCalls;


            if (Save != null)
                Save(this, new ViewEventArgs<Shipment>(shipment));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Shipment>(shipment));
        }

       

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var quote = new Shipment(hidShipmentId.Value.ToLong(), false);

            hidFlag.Value = DeleteFlag;

            if (Delete != null)
                Delete(this, new ViewEventArgs<Shipment>(quote));

            shipmentFinder.Reset();
        }

        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            operationsAddressInputOrigin.ClearFields();
            operationsAddressInputDestination.ClearFields();

            var shipment = new Shipment
            {
                Customer = new Customer(),
                DateCreated = DateTime.Now,
                EstimatedDeliveryDate = DateTime.Now.AddDays(1),
                ActualDeliveryDate = DateUtility.SystemEarliestDateTime,
                ActualPickupDate = DateUtility.SystemEarliestDateTime,
                DesiredPickupDate = DateTime.Now,
                EarlyPickup = TimeUtility.DefaultOpen,
                EarlyDelivery = TimeUtility.DefaultOpen,
                LatePickup = TimeUtility.DefaultClose,
                LateDelivery = TimeUtility.DefaultClose,
                Origin =
                        new ShipmentLocation
                        {
                            CountryId = ActiveUser.Tenant.DefaultCountryId,
                            AppointmentDateTime = DateUtility.SystemEarliestDateTime
                        },
                Destination =
                        new ShipmentLocation
                        {
                            CountryId = ActiveUser.Tenant.DefaultCountryId,
                            AppointmentDateTime = DateUtility.SystemEarliestDateTime
                        },
                Status = ShipmentStatus.Scheduled,
                MileageSourceId = default(long),
                ServiceMode = ServiceMode.NotApplicable
            };

            LoadShipment(shipment);

            lblEquipment.Text = string.Empty;
            lblServices.Text = string.Empty;

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);

            litErrorMessages.Text = string.Empty;
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var shipment = new Shipment(hidShipmentId.Value.ToLong(), false);

            LockRecord(shipment);
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var shipment = new Shipment(hidShipmentId.Value.ToLong(), false);
            if (UnLock != null && !shipment.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<Shipment>(shipment));
            }
        }

        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case VoidArgs:
                    VoidShipment();
                    break;
                case ReverseVoidArgs:
                    ReverseVoidShipment();
                    break;
                case DuplicateArgs:
                    DuplicateShipment(false);
                    break;
                case DuplicateReserseArgs:
                    DuplicateShipment(true);
                    break;
                case ReturnArgs:
                    ReturnToShipmentDashboard();
                    break;
                case ReturnShipmentToBeInvoicedArgs:
                    ReturnToShipmentsToBeInvoiced();
                    break;
                case VendorLaneHistoryArgs:
                    LoadVendorLaneBuySellData();
                    break;
                case BolArgs:
                    GenerateBOL();
                    break;
                case VendorRateAgreementArgs:
                    LoadVendorRateAgreementSelection();
                    break;
                case ShippingLabelArgs:
                    LoadShipmentLabelGenerator();
                    break;
                case EditInvoicedShipmentArgs:
                    EditInvoicedShipment();
                    break;
                case RouteSummaryArgs:
                    GenerateRouteSummary();
                    break;
                case MacroPointTrackingStartArgs:
                    macroPointOrderInput.Reset();
                    macroPointOrderInput.Visible = true;
                    break;
                case MacroPointTrackingStopArgs:
                    StopMacroPointTracking();
                    break;
                case MacroPointRequestLocationUpdateArgs:
                    SendMacroPointLocationUpdateRequest();
                    break;
                case PayForShipmentWithCreditCardArgs:
                    pecPayment.Visible = true;
                    pecPayment.LoadShipmentToPayFor(UpdateShipment());
                    break;
                case RefundCustomerPaymentsArgs:
                    prcRefundPayments.Visible = true;
                    prcRefundPayments.LoadShipment(UpdateShipment());
                    break;
                case DispatchShipmentArgs:
                    DispatchShipment();
                    break;
                case InitiateSmc3EvaTrackingArgs:
                    InitiateTracking();
                    break;
                case InitiateProject44TrackingArgs:
                    InitiateProject44Tracking();
                    break;
                case ConfirmChargesArgs:
                    VendorChargeConfirmation();
                    break;
            }
        }



        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument, true);
            customerFinder.Visible = false;
        }



        protected void OnAccountBucketSearchClicked(object sender, ImageClickEventArgs e)
        {
            accountBucketFinder.Reset();
            accountBucketFinder.EnableMultiSelection = false;
            accountBucketFinder.Visible = true;
        }

        protected void OnAccountBucketFinderSelectionCancelled(object sender, EventArgs e)
        {
            accountBucketFinder.Visible = false;
        }

        protected void OnAccountBucketFinderItemSelected(object sender, ViewEventArgs<AccountBucket> e)
        {
            DisplayAccountBucket(e.Argument);
            accountBucketFinder.Visible = false;
        }

        protected void OnAddAccountBucketClicked(object sender, EventArgs e)
        {
            accountBucketFinder.Reset();
            accountBucketFinder.EnableMultiSelection = true;
            accountBucketFinder.Visible = true;
        }

        protected void OnClearAccountBucketsClicked(object sender, EventArgs e)
        {
            lstAccountBuckets.DataSource = new List<object>();
            lstAccountBuckets.DataBind();
            litAccountBucketCount.Text = new List<object>().BuildTabCount();
        }

        protected void OnAccountBucketFinderMultiItemSelected(object sender, ViewEventArgs<List<AccountBucket>> e)
        {
            var accountBuckets = lstAccountBuckets
                .Items
                .Select(c => new
                {
                    Id = c.FindControl("hidShipmentAccountBucketId").ToCustomHiddenField().Value.ToLong(),
                    AccountBucketCode = c.FindControl("txtAccountBucketCode").ToTextBox().Text,
                    AccountBucketDescription = c.FindControl("txtAccountBucketDescription").ToTextBox().Text,
                    AccountBucketId = c.FindControl("hidAccountBucketId").ToCustomHiddenField().Value.ToLong(),
                })
                .ToList();

            var newAccountBucket = e
                .Argument
                .Select(accountBucket => new
                {
                    Id = default(long),
                    AccountBucketCode = accountBucket.Code,
                    AccountBucketDescription = accountBucket.Description,
                    AccountBucketId = accountBucket.Id,
                })
                .Where(v => !accountBuckets.Select(qv => qv.AccountBucketId).Contains(v.AccountBucketId));

            accountBuckets.AddRange(newAccountBucket);

            lstAccountBuckets.DataSource = accountBuckets;
            lstAccountBuckets.DataBind();
            litAccountBucketCount.Text = accountBuckets.BuildTabCount();

            accountBucketFinder.Visible = false;
        }

        protected void OnDeleteAccountBucketClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var accountBuckets = lstAccountBuckets
                .Items
                .Select(c => new
                {
                    Id = c.FindControl("hidShipmentAccountBucketId").ToCustomHiddenField().Value.ToLong(),
                    AccountBucketCode = c.FindControl("txtAccountBucketCode").ToTextBox().Text,
                    AccountBucketDescription = c.FindControl("txtAccountBucketDescription").ToTextBox().Text,
                    AccountBucketId = c.FindControl("hidAccountBucketId").ToCustomHiddenField().Value.ToLong(),
                })
                .ToList();

            accountBuckets.RemoveAt(imageButton.Parent.FindControl("hidAccountBucketIndex").ToCustomHiddenField().Value.ToInt());

            lstAccountBuckets.DataSource = accountBuckets;
            lstAccountBuckets.DataBind();
            litAccountBucketCount.Text = accountBuckets.BuildTabCount();
        }




        protected void OnPrefixSearchClicked(object sender, ImageClickEventArgs e)
        {
            prefixFinder.Visible = true;
        }

        protected void OnPrefixFinderSelectionCancelled(object sender, EventArgs e)
        {
            prefixFinder.Visible = false;
        }

        protected void OnPrefixFinderItemSelected(object sender, ViewEventArgs<Prefix> e)
        {
            DisplayPrefix(e.Argument);
            prefixFinder.Visible = false;
        }



        protected void OnVendorNumberEntered(object sender, EventArgs e)
        {
            hidFlag.Value = VendorUpdateFlag;

            if (VendorSearch != null)
                VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
        {
            vendorFinder.Reset();
            vendorFinder.EnableMultiSelection = false;
            vendorFinder.Visible = true;
        }

        protected void OnVendorFinderItemCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
        {
            hidFlag.Value = VendorUpdateFlag;
            DisplayVendor(e.Argument);
            vendorFinder.Visible = false;
        }

        protected void OnAddVendorClicked(object sender, EventArgs e)
        {
            vendorFinder.Reset();
            vendorFinder.EnableMultiSelection = true;
            vendorFinder.Visible = true;
        }

        protected void OnClearVendorsClicked(object sender, EventArgs e)
        {
            var vendors = lstVendors
               .Items
               .Select(c => new
               {
                   Id = c.FindControl("hidShipmentVendorId").ToCustomHiddenField().Value.ToLong(),
                   VendorNumber = c.FindControl("txtVendorNumber").ToTextBox().Text,
                   Name = c.FindControl("txtName").ToTextBox().Text,
                   VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                   FailureCodeId = c.FindControl("ddlFailureCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                   ProNumber = c.FindControl("txtProNumber").ToTextBox().Text,
                   FailureComments = c.FindControl("txtFailureComments").ToTextBox().Text,
                   InsuranceAlert = c.FindControl("hidInsuranceAlert").ToCustomHiddenField().Value.ToBoolean(),
                   InsuranceTooltip = c.FindControl("hidInsuranceTooltip").ToCustomHiddenField().Value,
                   IsRelationBetweenVendorAndVendorBill = c.FindControl("hidIsRelationBetweenVendorAndVendorBill").ToCustomHiddenField().Value.ToBoolean()
               }).Where(s => s.IsRelationBetweenVendorAndVendorBill)
               .ToList();

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            litVendorsCount.Text = vendors.BuildTabCount();

            var ddlVendorsList = lstCharges.Items.Select(c => new
            {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField(),
                VendorBillNumber = c.FindControl("txtVendorBillNumber").ToTextBox()
            }).ToList();

            ddlVendorsList.ForEach(cc =>
            {
                if (string.IsNullOrEmpty(cc.VendorBillNumber.Text))
                {
                    cc.Vendors.Items.Clear();
                    BuildVendorsDropdownListOnChargesTab(
                        cc.Vendors,
                        0,
                        string.IsNullOrEmpty(cc.VendorBillNumber.Text));
                    cc.SelectedVendorId.Value = "0";
                }
            });
            upPnlCharges.Update();
        }

        protected void OnVendorFinderMultiItemSelected(object sender, ViewEventArgs<List<Vendor>> e)
        {
            var vendors = lstVendors
                .Items
                .Select(c => new
                {
                    Id = c.FindControl("hidShipmentVendorId").ToCustomHiddenField().Value.ToLong(),
                    VendorNumber = c.FindControl("txtVendorNumber").ToTextBox().Text,
                    Name = c.FindControl("txtName").ToTextBox().Text,
                    VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                    FailureCodeId = c.FindControl("ddlFailureCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    ProNumber = c.FindControl("txtProNumber").ToTextBox().Text,
                    FailureComments = c.FindControl("txtFailureComments").ToTextBox().Text,
                    InsuranceAlert = c.FindControl("hidInsuranceAlert").ToCustomHiddenField().Value.ToBoolean(),
                    InsuranceTooltip = c.FindControl("hidInsuranceTooltip").ToCustomHiddenField().Value,
                    IsRelationBetweenVendorAndVendorBill = c.FindControl("hidIsRelationBetweenVendorAndVendorBill").ToCustomHiddenField().Value.ToBoolean()
                })
                .ToList();

            var messages = new List<ValidationMessage>();

            foreach (
                var vendor in
                    e.Argument.Where(
                        vendor => vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId])))
            {
                messages.AddRange(new[]
                    {
                        ValidationMessage.Error(string.Format("{0}-{1} invalid: {2}",
                                                              vendor.VendorNumber, vendor.Name,
                                                              vendor.VendorInsuranceAlertText(
                                                                  ProcessorVars.VendorInsuranceAlertThresholdDays[vendor.TenantId])))
                    });
            }

            var newVendors = e.Argument
                              .Where(v => !v.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[v.TenantId]))
                              .Select(v => new
                              {
                                  Id = default(long),
                                  v.VendorNumber,
                                  v.Name,
                                  VendorId = v.Id,
                                  FailureCodeId = default(long).ToLong(),
                                  ProNumber = string.Empty,
                                  FailureComments = string.Empty,
                                  InsuranceAlert = false,
                                  InsuranceTooltip = string.Empty,
                                  IsRelationBetweenVendorAndVendorBill = false
                              })
                              .Where(v => !vendors.Select(qv => qv.VendorId).Contains(v.VendorId))
                              .ToList();

            vendors.AddRange(newVendors);

            if (newVendors.Any())
                messages.AddRange(CheckVendorConstraints(newVendors.Select(v => new Vendor(v.VendorId)).ToList()));

            DisplayMessages(messages);

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            litVendorsCount.Text = vendors.BuildTabCount();
            vendorFinder.Visible = false;

            var ddlNewVendors = vendors.Select(t => new ListItem { Text = t.Name, Value = t.VendorId.ToString() }).ToArray();
            lstCharges.Items.Select(c => c.FindControl("ddlVendors").ToDropDownList()).ToList().ForEach(cc => cc.Items.AddRange((ddlNewVendors.Except(cc.Items.Cast<ListItem>().ToArray())).ToArray()));
            upPnlCharges.Update();
        }

        protected void OnLogNonPrimaryVendorRejection(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var vendors = lstVendors
                .Items
                .Select(c => new
                {
                    Id = c.FindControl("hidShipmentVendorId").ToCustomHiddenField().Value.ToLong(),
                    VendorNumber = c.FindControl("txtVendorNumber").ToTextBox().Text,
                    Name = c.FindControl("txtName").ToTextBox().Text,
                    VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                    FailureCodeId = c.FindControl("ddlFailureCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    ProNumber = c.FindControl("txtProNumber").ToTextBox().Text,
                    FailureComments = c.FindControl("txtFailureComments").ToTextBox().Text,
                    InsuranceAlert = c.FindControl("hidInsuranceAlert").ToCustomHiddenField().Value.ToBoolean(),
                    InsuranceTooltip = c.FindControl("hidInsuranceTooltip").ToCustomHiddenField().Value,
                    IsRelationBetweenVendorAndVendorBill = c.FindControl("hidIsRelationBetweenVendorAndVendorBill").ToCustomHiddenField().Value.ToBoolean()
                })
                .ToList();

            var index = imageButton.Parent.FindControl("hidVendorIndex").ToCustomHiddenField().Value.ToInt();

            var shipment = UpdateShipment();

            var log = new VendorRejectionLog
            {
                UserId = ActiveUser.Id,
                VendorId = shipment.Vendors[index].VendorId,
                TenantId = ActiveUser.TenantId,
                Customer = shipment.Customer,
                OriginPostalCode = shipment.Origin.PostalCode,
                OriginCity = shipment.Origin.City,
                OriginState = shipment.Origin.State,
                OriginCountryCode = shipment.Origin.Country == null ? string.Empty : shipment.Origin.Country.Code,
                DestinationPostalCode = shipment.Destination.PostalCode,
                DestinationCountryCode = shipment.Destination.Country == null ? string.Empty : shipment.Destination.Country.Code,
                DestinationCity = shipment.Destination.City,
                DestinationState = shipment.Destination.State,
                ShipmentNumber = shipment.ShipmentNumber,
                ShipmentDateCreated = shipment.DateCreated,
                DateCreated = DateTime.Now,
                ServiceMode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>()
            };

            hidFlag.Value = NonPrimaryVendorRejectLogFlag;

            if (LogVendorRejection != null)
                LogVendorRejection(this, new ViewEventArgs<VendorRejectionLog>(log));

            if (_doNotRemoveNonPrimaryVendor)
                return; // this will be set if there is an error in the LogVendorRejection event process

            vendors.RemoveAt(index);

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            litVendorsCount.Text = vendors.BuildTabCount();

            var removedVendorId = imageButton.Parent.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong();
            var ddlVendorsList = lstCharges.Items.Select(c => new
            {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField(),
                VendorBillNumber = c.FindControl("txtVendorBillNumber").ToTextBox()
            }).ToList();

            ddlVendorsList.ForEach(cc =>
            {
                cc.Vendors.Items.Clear();
                BuildVendorsDropdownListOnChargesTab(
                    cc.Vendors,
                    cc.SelectedVendorId.Value.ToLong() == removedVendorId ? hidPrimaryVendorId.Value.ToLong() : cc.SelectedVendorId.Value.ToLong(),
                    string.IsNullOrEmpty(cc.VendorBillNumber.Text));
                cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == removedVendorId ? hidPrimaryVendorId.Value : cc.SelectedVendorId.Value;
            });
            upPnlCharges.Update();
        }

        protected void OnDeleteVendorClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var vendors = lstVendors
                .Items
                .Select(c => new
                {
                    Id = c.FindControl("hidShipmentVendorId").ToCustomHiddenField().Value.ToLong(),
                    VendorNumber = c.FindControl("txtVendorNumber").ToTextBox().Text,
                    Name = c.FindControl("txtName").ToTextBox().Text,
                    VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                    FailureCodeId = c.FindControl("ddlFailureCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    ProNumber = c.FindControl("txtProNumber").ToTextBox().Text,
                    FailureComments = c.FindControl("txtFailureComments").ToTextBox().Text,
                    InsuranceAlert = c.FindControl("hidInsuranceAlert").ToCustomHiddenField().Value.ToBoolean(),
                    InsuranceTooltip = c.FindControl("hidInsuranceTooltip").ToCustomHiddenField().Value,
                    IsRelationBetweenVendorAndVendorBill = c.FindControl("hidIsRelationBetweenVendorAndVendorBill").ToCustomHiddenField().Value.ToBoolean()
                })
                .ToList();

            vendors.RemoveAt(imageButton.Parent.FindControl("hidVendorIndex").ToCustomHiddenField().Value.ToInt());

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            litVendorsCount.Text = vendors.BuildTabCount();

            var removedVendorId = imageButton.Parent.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong();
            var ddlVendorsList = lstCharges.Items.Select(c => new
            {
                Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField(),
                VendorBillNumber = c.FindControl("txtVendorBillNumber").ToTextBox()
            }).ToList();

            ddlVendorsList.ForEach(cc =>
            {
                cc.Vendors.Items.Clear();
                BuildVendorsDropdownListOnChargesTab(
                    cc.Vendors,
                    cc.SelectedVendorId.Value.ToLong() == removedVendorId ? hidPrimaryVendorId.Value.ToLong() : cc.SelectedVendorId.Value.ToLong(),
                    string.IsNullOrEmpty(cc.VendorBillNumber.Text));
                cc.SelectedVendorId.Value = cc.SelectedVendorId.Value.ToLong() == removedVendorId ? hidPrimaryVendorId.Value : cc.SelectedVendorId.Value;
            });
            upPnlCharges.Update();
        }





        protected void OnAddItemClicked(object sender, EventArgs e)
        {
            var items = lstItems.Items
                                .Select(control => new
                                {
                                    Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
                                    Pickup = control.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
                                    Delivery = control.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt(),
                                    ActualWeight = control.FindControl("txtWeight").ToTextBox().Text,
                                    ActualLength = control.FindControl("txtLength").ToTextBox().Text,
                                    ActualWidth = control.FindControl("txtWidth").ToTextBox().Text,
                                    ActualHeight = control.FindControl("txtHeight").ToTextBox().Text,
                                    Quantity = control.FindControl("txtQuantity").ToTextBox().Text,
                                    PieceCount = control.FindControl("txtPieceCount").ToTextBox().Text,
                                    Description = control.FindControl("txtDescription").ToTextBox().Text,
                                    Comment = control.FindControl("txtComment").ToTextBox().Text,
                                    ActualFreightClass =
                                                       control.FindControl("ddlActualFreightClass")
                                                              .ToDropDownList()
                                                              .SelectedValue.ToDouble(),
                                    RatedFreightClass = control.FindControl("txtRatedFreightClass").ToTextBox().Text.ToDouble(),
                                    Value = control.FindControl("txtItemValue").ToTextBox().Text,
                                    IsStackable = control.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                                    HazardousMaterial = control.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
                                    PackageTypeId =
                                                       control.FindControl("ddlPackageType")
                                                              .ToCachedObjectDropDownList()
                                                              .SelectedValue.ToLong(),
                                    NMFCCode = control.FindControl("txtNMFCCode").ToTextBox().Text,
                                    HTSCode = control.FindControl("txtHTSCode").ToTextBox().Text,
                                })
                                .ToList();

            items.Add(new
            {
                Id = default(long),
                Pickup = default(int),
                Delivery = default(int),
                ActualWeight = string.Empty,
                ActualLength = string.Empty,
                ActualWidth = string.Empty,
                ActualHeight = string.Empty,
                Quantity = string.Empty,
                PieceCount = string.Empty,
                Description = string.Empty,
                Comment = string.Empty,
                ActualFreightClass = default(double),
                RatedFreightClass = default(double),
                Value = string.Empty,
                IsStackable = false,
                HazardousMaterial = false,
                PackageTypeId = default(long),
                NMFCCode = string.Empty,
                HTSCode = string.Empty,
            });

            lstItems.DataSource = items;
            lstItems.DataBind();
            tabItems.HeaderText = items.BuildTabCount(ItemsHeader);
            athtuTabUpdater.SetForUpdate(tabItems.ClientID, items.BuildTabCount(ItemsHeader));
            chkAutoRated.Checked = false;

            if (((Control)sender).ClientID == btnAddItemTabBottom.ClientID) btnAddItemTabBottom.Focus();
        }

        protected void OnDeleteItemClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var items = lstItems.Items
                                .Select(control => new
                                {
                                    Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
                                    Pickup = control.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
                                    Delivery = control.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt(),
                                    ActualWeight = control.FindControl("txtWeight").ToTextBox().Text,
                                    ActualLength = control.FindControl("txtLength").ToTextBox().Text,
                                    ActualWidth = control.FindControl("txtWidth").ToTextBox().Text,
                                    ActualHeight = control.FindControl("txtHeight").ToTextBox().Text,
                                    Quantity = control.FindControl("txtQuantity").ToTextBox().Text,
                                    PieceCount = control.FindControl("txtPieceCount").ToTextBox().Text,
                                    Description = control.FindControl("txtDescription").ToTextBox().Text,
                                    Comment = control.FindControl("txtComment").ToTextBox().Text,
                                    ActualFreightClass =
                                                       control.FindControl("ddlActualFreightClass")
                                                              .ToDropDownList()
                                                              .SelectedValue.ToDouble(),
                                    RatedFreightClass = control.FindControl("txtRatedFreightClass").ToTextBox().Text.ToDouble(),
                                    Value = control.FindControl("txtItemValue").ToTextBox().Text,
                                    IsStackable = control.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                                    HazardousMaterial = control.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
                                    PackageTypeId =
                                                       control.FindControl("ddlPackageType")
                                                              .ToCachedObjectDropDownList()
                                                              .SelectedValue.ToLong(),
                                    NMFCCode = control.FindControl("txtNMFCCode").ToTextBox().Text,
                                    HTSCode = control.FindControl("txtHTSCode").ToTextBox().Text,
                                })
                                .ToList();

            items.RemoveAt(imageButton.Parent.FindControl("hidItemIndex").ToCustomHiddenField().Value.ToInt());

            lstItems.DataSource = items;
            lstItems.DataBind();
            tabItems.HeaderText = items.BuildTabCount(ItemsHeader);
            athtuTabUpdater.SetForUpdate(tabItems.ClientID, items.BuildTabCount(ItemsHeader));
            chkAutoRated.Checked = false;
        }

        protected void OnClearItemsClicked(object sender, EventArgs e)
        {
            lstItems.DataSource = new List<object>();
            lstItems.DataBind();
            tabItems.HeaderText = new List<object>().BuildTabCount(ItemsHeader);
            athtuTabUpdater.SetForUpdate(tabItems.ClientID, new List<object>().BuildTabCount(ItemsHeader));
            chkAutoRated.Checked = false;
        }

        protected void OnShipmentItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var dataItem = item.DataItem;

            // Bind stops
            var ddlItemPickup = item.FindControl("ddlItemPickup").ToDropDownList();
            var ddlItemDelivery = item.FindControl("ddlItemDelivery").ToDropDownList();

            var stops = RetrieveViewStops(new Shipment { TenantId = ActiveUser.TenantId });

            var originStops = stops
                .Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
                .ToList();
            originStops.Insert(0, new ViewListItem(OriginStopText, 0.ToString()));

            var destStops = stops
                .Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
                .ToList();
            destStops.Add(new ViewListItem(DestinationStopText, ViewDeliveryStopOrder.ToString()));

            ddlItemPickup.DataSource = originStops;
            ddlItemPickup.DataBind();
            if (dataItem.HasGettableProperty("Pickup"))
                ddlItemPickup.SelectedValue = dataItem.GetPropertyValue("Pickup").GetString();


            ddlItemDelivery.DataSource = destStops;
            ddlItemDelivery.DataBind();
            if (dataItem.HasGettableProperty("Delivery"))
                ddlItemDelivery.SelectedValue = dataItem.GetPropertyValue("Delivery").GetString();

            // Bind freight classes
            var freightClasses = WebApplicationSettings.FreightClasses
                                                       .Select(fc => new ViewListItem(fc.ToString(), fc.ToString()))
                                                       .ToList();
            freightClasses.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicableShort, string.Empty));

            var ddlActualFreightClass = item.FindControl("ddlActualFreightClass").ToDropDownList();
            ddlActualFreightClass.DataSource = freightClasses;
            ddlActualFreightClass.DataBind();
            if (dataItem.HasGettableProperty("ActualFreightClass"))
                ddlActualFreightClass.SelectedValue = dataItem.GetPropertyValue("ActualFreightClass").GetString();
        }



        protected void OnSetPackageDimensionsClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            var packageTypeId = control.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong();

            if (packageTypeId == default(long)) return;

            var packageType = new PackageType(packageTypeId);

            // set dimensions, then recalculate density
            var txtLength = control.FindControl("txtLength").ToTextBox();
            var txtWidth = control.FindControl("txtWidth").ToTextBox();
            var txtHeight = control.FindControl("txtHeight").ToTextBox();

            var txtQty = control.FindControl("txtQuantity").ToTextBox();

            txtLength.Text = packageType.DefaultLength.ToString();
            txtWidth.Text = packageType.DefaultWidth.ToString();
            txtHeight.Text = packageType.DefaultHeight.ToString();
            if (txtQty.Text.ToInt() == 0) txtQty.Text = 1.GetString();

            var weight = control.FindControl("txtWeight").ToTextBox().Text.ToDecimal();
            var length = txtLength.Text.ToDecimal();
            var width = txtWidth.Text.ToDecimal();
            var height = txtHeight.Text.ToDecimal();
            var qty = txtQty.Text.ToInt();


            if (length != default(decimal) && width != default(decimal) && height != default(decimal) && qty != default(int))
            {
                control.FindControl("txtEstimatedPcf").ToTextBox().Text = ((weight / (length.InToFt() * width.InToFt() * height.InToFt())) / qty).ToString("n2");
            }
            else
            {
                control.FindControl("txtEstimatedPcf").ToTextBox().Text = string.Empty;
            }
        }


        protected void OnAddLibraryItemClicked(object sender, EventArgs e)
        {
            libraryItemFinder.Visible = true;
        }

        protected void OnLibraryItemFinderMultiItemSelected(object sender, ViewEventArgs<List<LibraryItem>> e)
        {
            var items = lstItems.Items
                .Select(control => new
                {
                    Id = control.FindControl("hidItemId").ToCustomHiddenField().Value.ToLong(),
                    Pickup = control.FindControl("ddlItemPickup").ToDropDownList().SelectedValue.ToInt(),
                    Delivery = control.FindControl("ddlItemDelivery").ToDropDownList().SelectedValue.ToInt(),
                    ActualWeight = control.FindControl("txtWeight").ToTextBox().Text,
                    ActualLength = control.FindControl("txtLength").ToTextBox().Text,
                    ActualWidth = control.FindControl("txtWidth").ToTextBox().Text,
                    ActualHeight = control.FindControl("txtHeight").ToTextBox().Text,
                    Quantity = control.FindControl("txtQuantity").ToTextBox().Text,
                    PieceCount = control.FindControl("txtPieceCount").ToTextBox().Text,
                    Description = control.FindControl("txtDescription").ToTextBox().Text,
                    Comment = control.FindControl("txtComment").ToTextBox().Text,
                    ActualFreightClass = control.FindControl("ddlActualFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                    RatedFreightClass = control.FindControl("txtRatedFreightClass").ToTextBox().Text.ToDouble(),
                    Value = control.FindControl("txtItemValue").ToTextBox().Text,
                    IsStackable = control.FindControl("chkIsStackable").ToAltUniformCheckBox().Checked,
                    HazardousMaterial = control.FindControl("chkIsHazmat").ToAltUniformCheckBox().Checked,
                    PackageTypeId = control.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    NMFCCode = control.FindControl("txtNMFCCode").ToTextBox().Text,
                    HTSCode = control.FindControl("txtHTSCode").ToTextBox().Text,
                })
                .ToList();

            var itemsToAdd = e.Argument
                .Select(t => new
                {
                    Id = default(long),
                    Pickup = 0,
                    Delivery = ViewDeliveryStopOrder,
                    ActualWeight = t.Weight.GetString(),
                    ActualLength = t.Length.GetString(),
                    ActualWidth = t.Width.GetString(),
                    ActualHeight = t.Height.GetString(),
                    Quantity = t.Quantity.GetString(),
                    PieceCount = t.PieceCount.GetString(),
                    t.Description,
                    t.Comment,
                    ActualFreightClass = t.FreightClass,
                    RatedFreightClass = t.FreightClass,
                    Value = 0.GetString(),
                    t.IsStackable,
                    t.HazardousMaterial,
                    PackageTypeId = t.PackageTypeId.ToLong(),
                    t.NMFCCode,
                    t.HTSCode,
                })
                .ToList();

            items.AddRange(itemsToAdd);

            lstItems.DataSource = items;
            lstItems.DataBind();
            tabItems.HeaderText = items.BuildTabCount(ItemsHeader);

            libraryItemFinder.Visible = false;
        }

        protected void OnLibraryItemFinderItemCancelled(object sender, EventArgs e)
        {
            libraryItemFinder.Visible = false;
        }


        protected void OnChangeVendorInCharge(object sender, EventArgs e)
        {
            lstCharges.Items
                      .Select(c => new
                          {
                              Vendors = c.FindControl("ddlVendors").ToDropDownList(),
                              SelectedVendorId = c.FindControl("hidSelectedVendorId").ToCustomHiddenField(),
                          })
                      .ToList()
                      .ForEach(cc => { cc.SelectedVendorId.Value = cc.Vendors.SelectedValue; });

            upPnlCharges.Update();
        }



        protected void OnAddChargeClicked(object sender, EventArgs e)
        {
            var charges = lstCharges.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    VendorId = i.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value,
                    VendorBillDocumentNumber = i.FindControl("txtVendorBillNumber").ToTextBox().Text,
                    VendorBillId = i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong()
                })
                .ToList();

            charges.Add(new
            {
                Id = default(long),
                Quantity = default(int),
                UnitBuy = default(decimal),
                UnitSell = default(decimal),
                UnitDiscount = default(decimal),
                Comment = string.Empty,
                ChargeCodeId = default(long),
                VendorId = string.Empty,
                VendorBillDocumentNumber = string.Empty,
                VendorBillId = default(long)
            });

            lstCharges.DataSource = charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, charges.BuildTabCount(ChargesHeader));
        }

        protected void OnClearChargesClicked(object sender, EventArgs e)
        {
            var charges = lstCharges.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    VendorId = i.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value,
                    VendorBillDocumentNumber = i.FindControl("txtVendorBillNumber").ToTextBox().Text,
                    VendorBillId = i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong()
                }).Where(s => s.VendorBillId > 0)
                .ToList();

            lstCharges.DataSource = charges;
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, charges.BuildTabCount(ChargesHeader));

            var chargeList = charges
                    .Select(c => new Charge
                    {
                        ChargeCodeId = c.ChargeCodeId,
                        Comment = c.Comment,
                        Quantity = c.Quantity,
                        UnitBuy = c.UnitBuy,
                        UnitSell = c.UnitSell,
                        UnitDiscount = c.UnitDiscount,
                    }).ToList();

            chargeStatistics.LoadCharges(chargeList, hidResellerAdditionId.Value.ToLong());
            CheckForCreditAlert(chargeList.Sum(c => c.AmountDue));
        }

        protected void OnRefreshChargeStatisticsClicked(object sender, ImageClickEventArgs e)
        {
            var chargeList = lstCharges
                .Items
                .Select(i => new Charge
                {
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                })
                .ToList();

            chargeStatistics.LoadCharges(chargeList, hidResellerAdditionId.Value.ToLong());
        }

        protected void OnDeleteChargeClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var charges = lstCharges.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    VendorId = i.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value,
                    VendorBillDocumentNumber = i.FindControl("txtVendorBillNumber").ToTextBox().Text,
                    VendorBillId = i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong()
                })
                .ToList();

            charges.RemoveAt(imageButton.Parent.FindControl("hidChargeIndex").ToCustomHiddenField().Value.ToInt());

            lstCharges.DataSource = charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, charges.BuildTabCount(ChargesHeader));

            var chargeList = charges
                    .Select(c => new Charge
                    {
                        ChargeCodeId = c.ChargeCodeId,
                        Comment = c.Comment,
                        Quantity = c.Quantity,
                        UnitBuy = c.UnitBuy,
                        UnitSell = c.UnitSell,
                        UnitDiscount = c.UnitDiscount,
                    }).ToList();

            chargeStatistics.LoadCharges(chargeList, hidResellerAdditionId.Value.ToLong());
            CheckForCreditAlert(chargeList.Sum(c => c.AmountDue));
        }

        protected void OnChargesItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;

            var ddlVendors = item.FindControl("ddlVendors").ToDropDownList();
            var selectedValue = item.FindControl("hidSelectedVendorId").ToCustomHiddenField().Value.ToLong();
            var isEnabled = string.IsNullOrEmpty(item.FindControl("txtVendorBillNumber").ToTextBox().Text);

            BuildVendorsDropdownListOnChargesTab(ddlVendors, selectedValue, isEnabled);
        }



        protected void OnAutoRateClicked(object sender, EventArgs e)
        {
            var shipment = UpdateShipment();
            CheckServicesForAutoRating(shipment);
            shipment.LinearFootRuleBypassed = chkBypassLinearFootRuleForRating.Checked;

            rateSelectionDisplay.Visible = true;

            rateSelectionDisplay.Rate(shipment);
        }

        protected void OnRateSelectionDisplaySelectionCancelled(object sender, EventArgs e)
        {
            rateSelectionDisplay.Visible = false;
        }

        protected void OnRateSelectionDisplayRateSelected(object sender, ViewEventArgs<Rate> e)
        {
            var rate = e.Argument;
            var shipment = UpdateShipment();

            // add new charges
            shipment.Charges.AddRange(
                rate.Charges.Select(c => new ShipmentCharge
                {
                    TenantId = shipment.TenantId,
                    Shipment = shipment,
                    ChargeCodeId = c.ChargeCodeId,
                    UnitBuy = c.UnitBuy,
                    UnitSell = c.UnitSell,
                    UnitDiscount = c.UnitDiscount,
                    Quantity = c.Quantity,
                    Comment = c.Comment
                }));

            // set charge related items
            ddlServiceMode.SelectedValue = rate.Mode.ToInt().ToString();
            shipment.ServiceMode = rate.Mode;
            Vendor vendor = null;
            switch (rate.Mode)
            {
                case ServiceMode.SmallPackage:
                    vendor = rate.Vendor;
                    break;
                case ServiceMode.LessThanTruckload:
                    vendor = rate.LTLSellRate.VendorRating.Vendor;
                    break;
            }
            if (vendor != null)
            {
                var primaryVendorIndex = -1;
                for (int i = 0; i < shipment.Vendors.Count; i++)
                    if (shipment.Vendors[i].Primary)
                    {
                        primaryVendorIndex = i;
                        break;
                    }
                if (primaryVendorIndex != -1) shipment.Vendors[primaryVendorIndex].Vendor = rate.Vendor;
                else
                    shipment.Vendors
                        .Add(new ShipmentVendor
                        {
                            TenantId = shipment.TenantId,
                            Vendor = rate.Vendor,
                            Shipment = shipment,
                            Primary = true,
                            FailureComments = string.Empty,
                            ProNumber = string.Empty,
                        });
            }

            // estimated delivery
            shipment.EstimatedDeliveryDate = e.Argument.EstimatedDeliveryDate;

            // c/o removal - only valid for non-autorated shipments
            shipment.CareOfAddressFormatEnabled = false;

            // autorated shipment fields
            shipment.ShipmentAutorated = true;
            shipment.LinearFootRuleBypassed = chkBypassLinearFootRuleForRating.Checked;	// will still be checked upon selection
            shipment.OriginalRateValue = rate.OriginalRateValue;
            shipment.LineHaulProfitAdjustmentRatio = rate.FreightProfitAdjustment;
            shipment.FuelProfitAdjustmentRatio = rate.FuelProfitAdjustment;
            shipment.ServiceProfitAdjustmentRatio = rate.ServiceProfitAdjustment;
            shipment.AccessorialProfitAdjustmentRatio = rate.AccessorialProfitAdjustment;
            shipment.ResellerAdditionalAccessorialMarkup = rate.ResellerAccessorialMarkup;
            shipment.ResellerAdditionalAccessorialMarkupIsPercent = rate.ResellerAccessorialMarkupIsPercent;
            shipment.ResellerAdditionalServiceMarkup = rate.ResellerServiceMarkup;
            shipment.ResellerAdditionalServiceMarkupIsPercent = rate.ResellerServiceMarkupIsPercent;
            shipment.ResellerAdditionalFuelMarkup = rate.ResellerFuelMarkup;
            shipment.ResellerAdditionalFuelMarkupIsPercent = rate.ResellerFuelMarkupIsPercent;
            shipment.ResellerAdditionalFreightMarkup = rate.ResellerFreightMarkup;
            shipment.ResellerAdditionalFreightMarkupIsPercent = rate.ResellerFreightMarkupIsPercent;
            shipment.DirectPointRate = rate.DirectPointRate;
            shipment.SmallPackType = rate.SmallPackServiceType;
            shipment.SmallPackageEngine = rate.SmallPackEngine;
            shipment.ApplyDiscount = rate.ApplyDiscount;
            shipment.IsLTLPackageSpecificRate = rate.IsLTLPackageSpecificRate;
            shipment.BilledWeight = rate.BilledWeight;
            shipment.RatedWeight = rate.RatedWeight;
            shipment.RatedCubicFeet = rate.RatedCubicFeet;
            shipment.RatedPcf = rate.RatedPcf;
	        shipment.IsGuaranteedDeliveryService = rate.IsGuaranteed;
			shipment.GuaranteedDeliveryServiceTime = rate.GetGuaranteedRateTime();

            shipment.GuaranteedDeliveryServiceRate = rate.GetGuaranteedRate();

            shipment.GuaranteedDeliveryServiceRateType = rate.GetGuaranteedRateType();

            shipment.IsExpeditedService = rate.IsExpedited;
            shipment.ExpeditedServiceRate = rate.GetExpeditedRate();
            shipment.ExpeditedServiceTime = rate.GetExpeditedRateTime();

            shipment.CriticalBolComments += rate.GetCriticalBolComments(shipment.EstimatedDeliveryDate);

            var tier = rate != null ? rate.DiscountTier ?? new DiscountTier() : new DiscountTier();
            shipment.VendorDiscountPercentage = tier.DiscountPercent;
            shipment.VendorFreightFloor = tier.FloorValue;
            shipment.VendorFreightCeiling = tier.CeilingValue;
            shipment.VendorFuelPercent = rate.FuelMarkupPercent;

            var sellRate = rate.LTLSellRate ?? new LTLSellRate();

            shipment.CustomerFreightMarkupValue = rate.MarkupValue;
            shipment.CustomerFreightMarkupPercent = rate.MarkupPercent;
            shipment.UseLowerCustomerFreightMarkup = sellRate.UseMinimum;
            shipment.VendorRatingOverrideAddress = sellRate.VendorRating == null
                                                    ? string.Empty
                                                    : sellRate.VendorRating.OverrideAddress;
            shipment.CriticalBolComments = sellRate.VendorRating.AppendVendorRatingCriticalCommentToShipment(shipment.CriticalBolComments);

            // update processing
            if (!rate.IsProject44Rate)
            {
                Rater2.UpdateShipmentAutoRatingAccessorilas(shipment, sellRate);
                if (shipment.ServiceMode == ServiceMode.LessThanTruckload)
                {
                    var rule = rate.CFCRule;
                    if (rule.IsNew) rule = null;
                    Rater2.UpdateShipmentItemRatedCalculations(shipment, rule, tier);
                    hidUpdateTerminalInfo.Value = true.ToString();

                    // update terminal information
                    try
                    {
                        var variables = ProcessorVars.IntegrationVariables[ActiveUser.TenantId];
                        var carrierConnect = new CarrierConnect(variables.CarrierConnectParameters); //NOTE: without country map as country id is set below on return
                        var scac = sellRate.VendorRating == null ? string.Empty : sellRate.VendorRating.Vendor.Scac;

                        var terminals = carrierConnect.GetLTLTerminalInfo(scac, new[] { rate.OriginTerminalCode, rate.DestinationTerminalCode });

                        shipment.OriginTerminal = terminals.FirstOrDefault(t => t.Code == rate.OriginTerminalCode);
                        if (shipment.OriginTerminal != null)
                        {
                            shipment.OriginTerminal.Shipment = shipment;
                            shipment.OriginTerminal.TenantId = shipment.TenantId;
                            shipment.OriginTerminal.CountryId = shipment.Origin.CountryId;
                        }
                        shipment.DestinationTerminal = terminals.FirstOrDefault(t => t.Code == rate.DestinationTerminalCode);
                        if (shipment.DestinationTerminal != null)
                        {
                            shipment.DestinationTerminal.Shipment = shipment;
                            shipment.DestinationTerminal.TenantId = shipment.TenantId;
                            shipment.DestinationTerminal.CountryId = shipment.Destination.CountryId;
                        }
                    }
                    catch (Exception ex)
                    {
                        shipment.OriginTerminal = null;
                        shipment.DestinationTerminal = null;
                        ErrorLogger.LogError(ex, Context);
                    }
                }
            }

            shipment.Project44QuoteNumber = rate.Project44QuoteNumber;

            // hide rate selction display
            rateSelectionDisplay.Visible = false;

	        CheckAndAddGuaranteedService(shipment);

            // re-load shipment
            LoadShipment(shipment, false);
        }



        protected void OnAddressInputFindPostalCode(object sender, ViewEventArgs<string> e)
        {
            var addressinput = (OperationsAddressInputControl)sender;
            hidFlag.Value = addressinput.Type.GetString();
            postalCodeFinder.Visible = true;
            postalCodeFinder.SearchOn(e.Argument);
        }

        protected void OnAddressInputAddAddressFromAddressBook(object sender, ViewEventArgs<string> e)
        {
            var addressinput = (OperationsAddressInputControl)sender;
            hidFlag.Value = addressinput.Type.GetString();
            addressBookFinder.Visible = true;
            addressBookFinder.SearchOn(e.Argument);
        }

        protected void OnAddStopFromAddressBookClicked(object sender, EventArgs e)
        {
            addressBookFinder.Reset();
            addressBookFinder.Visible = true;
            hidFlag.Value = OperationsAddressInputType.Stop.GetString();
        }

        protected void OnAddressBookFinderItemCancelled(object sender, EventArgs e)
        {
            addressBookFinder.Visible = false;
            addressBookFinder.Reset();
            hidFlag.Value = string.Empty;
        }

        protected void OnAddressBookFinderItemSelected(object sender, ViewEventArgs<AddressBook> e)
        {
            switch (hidFlag.Value.ToEnum<OperationsAddressInputType>())
            {
                case OperationsAddressInputType.Origin:
                    operationsAddressInputOrigin.LoadAddress(e.Argument);
                    AddServicesRequired(e.Argument.Services);
                    break;
                case OperationsAddressInputType.Destination:
                    operationsAddressInputDestination.LoadAddress(e.Argument);
                    AddServicesRequired(e.Argument.Services);
                    break;
                case OperationsAddressInputType.Stop:
                    var shipment = new Shipment(hidShipmentId.Value.ToLong(), false);
                    var stops = RetrieveViewStops(shipment);
                    var stop = new ShipmentLocation
                    {
                        City = e.Argument.City,
                        CountryId = e.Argument.CountryId,
                        Description = e.Argument.Description,
                        PostalCode = e.Argument.PostalCode,
                        State = e.Argument.State,
                        Street1 = e.Argument.Street1,
                        Street2 = e.Argument.Street2,
                        Shipment = shipment,
                        TenantId = shipment.TenantId,
                        SpecialInstructions =
                                chkOriginInstruction.Checked
                                    ? e.Argument.OriginSpecialInstruction
                                    : chkDestinationInstruction.Checked ? e.Argument.DestinationSpecialInstruction : string.Empty,
                        Direction = e.Argument.Direction,
                        GeneralInfo = e.Argument.GeneralInfo,
                        AppointmentDateTime = DateUtility.SystemEarliestDateTime,
                    };
                    stop.Contacts.AddRange(e.Argument.Contacts.Select(c => new ShipmentContact
                    {
                        Location = stop,
                        Comment = c.Comment,
                        ContactTypeId = c.ContactTypeId,
                        Email = c.Email,
                        Fax = c.Fax,
                        Mobile = c.Mobile,
                        Phone = c.Phone,
                        Name = c.Name,
                        Primary = c.Primary,
                        TenantId = stop.TenantId,
                    }));
                    stops.Add(stop);
                    lstStops.DataSource = stops;
                    lstStops.DataBind();
                    tabStops.HeaderText = stops.BuildTabCount(StopsHeader);
                    break;
            }
            addressBookFinder.Visible = false;
            addressBookFinder.Reset();
            hidFlag.Value = string.Empty;
        }



        protected void OnPostalCodeFinderItemCancelled(object sender, EventArgs e)
        {
            postalCodeFinder.Visible = false;
            postalCodeFinder.Reset();
            hidFlag.Value = string.Empty;
        }

        protected void OnPostalCodeFinderItemSelected(object sender, ViewEventArgs<PostalCodeViewSearchDto> e)
        {
            switch (hidFlag.Value.ToEnum<OperationsAddressInputType>())
            {
                case OperationsAddressInputType.Origin:
                    operationsAddressInputOrigin.LoadPostalCode(e.Argument);
                    break;
                case OperationsAddressInputType.Destination:
                    operationsAddressInputDestination.LoadPostalCode(e.Argument);
                    break;
            }
            postalCodeFinder.Visible = false;
            postalCodeFinder.Reset();
            hidFlag.Value = string.Empty;
        }

        protected void OnShipmentLocationServicesRequired(object sender, ViewEventArgs<List<AddressBookService>> e)
        {
            if (e.Argument.Count > 0) AddAddressBookServicesToViewServices(e.Argument);
        }

        private void AddAddressBookServicesToViewServices(IEnumerable<AddressBookService> services)
        {
            var serviceIds = services.Select(s => s.ServiceId).ToList();

            var viewIds = rptServices
                .Items
                .Cast<RepeaterItem>()
                .Where(item => item.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(item => item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong());

            foreach (var id in viewIds.Where(id => !serviceIds.Contains(id)))
                serviceIds.Add(id);

            DisplayServices(serviceIds);
        }



        protected void OnAddReferenceClicked(object sender, EventArgs e)
        {
            var references = lstReferences
                .Items
                .Select(control => new
                {
                    Id = control.FindControl("hidReferenceId").ToCustomHiddenField().Value,
                    Name = control.FindControl("txtName").ToTextBox().Text,
                    Value = control.FindControl("txtValue").ToTextBox().Text,
                    DisplayOnOrigin = control.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
                    DisplayOnDestination = control.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
                    Required = control.FindControl("chkRequired").ToAltUniformCheckBox().Checked
                })
                .ToList();

            references.Add(new
            {
                Id = default(long).GetString(),
                Name = string.Empty,
                Value = string.Empty,
                DisplayOnOrigin = false,
                DisplayOnDestination = false,
                Required = false
            });

            lstReferences.DataSource = references;
            lstReferences.DataBind();
            litReferenceCount.Text = references.BuildTabCount();
        }

        protected void OnClearReferencesClicked(object sender, EventArgs e)
        {
            lstReferences.DataSource = new List<object>();
            lstReferences.DataBind();
            litReferenceCount.Text = new List<object>().BuildTabCount();
        }

        protected void OnDeleteReferenceClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var references = lstReferences
                .Items
                .Select(control => new
                {
                    Id = control.FindControl("hidReferenceId").ToCustomHiddenField().Value,
                    Name = control.FindControl("txtName").ToTextBox().Text,
                    Value = control.FindControl("txtValue").ToTextBox().Text,
                    DisplayOnOrigin = control.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
                    DisplayOnDestination = control.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
                    Required = control.FindControl("chkRequired").ToAltUniformCheckBox().Checked
                })
                .ToList();

            references.RemoveAt(imageButton.Parent.FindControl("hidReferenceIndex").ToCustomHiddenField().Value.ToInt());

            lstReferences.DataSource = references;
            lstReferences.DataBind();
            litReferenceCount.Text = references.BuildTabCount();
        }



        private void CheckForCreditAlert(decimal charges)
        {
            return; // temporarily disabled feature v4.0.18.2
            var credit = hidCustomerCredit.Value.ToDecimal();
            var outstanding = hidCustomerOutstandingBalance.Value.ToDecimal() - hidLoadedShipmentTotalDue.Value.ToDecimal();
            var alertThreshold = ProcessorVars.CustomerCreditAlertThreshold[ActiveUser.TenantId];
            pnlCreditAlert.Visible = credit.CreditAlert(outstanding + charges, alertThreshold);
        }



        private void CheckServicesForAutoRating(Shipment shipment)
        {
            var bcsId = string.Empty;
            var hmId = string.Empty;
            if (shipment.IsInternational() && ActiveUser.Tenant.BorderCrossingServiceId != default(long) &&
                shipment.Services.All(s => s.ServiceId != ActiveUser.Tenant.BorderCrossingServiceId))
            {
                shipment.Services.Add(new ShipmentService
                {
                    TenantId = ActiveUser.TenantId,
                    Shipment = shipment,
                    ServiceId = ActiveUser.Tenant.BorderCrossingServiceId
                });
                bcsId = ActiveUser.Tenant.BorderCrossingServiceId.ToString();

            }

            if (shipment.HazardousMaterial && ActiveUser.Tenant.HazardousMaterialServiceId != default(long) &&
                shipment.Services.All(s => s.ServiceId != ActiveUser.Tenant.HazardousMaterialServiceId))
            {
                shipment.Services.Add(new ShipmentService
                {
                    TenantId = ActiveUser.TenantId,
                    Shipment = shipment,
                    ServiceId = ActiveUser.Tenant.HazardousMaterialServiceId
                });
                hmId = ActiveUser.Tenant.HazardousMaterialServiceId.ToString();
            }

            if (!string.IsNullOrEmpty(hmId) || !string.IsNullOrEmpty(bcsId))
            {
                foreach (RepeaterItem item in rptServices.Items)
                {
                    var serviceId = item.FindControl("hidServiceId").ToCustomHiddenField().Value;
                    item.FindControl("chkSelected").ToCheckBox().Checked = serviceId == hmId || serviceId == bcsId;
                }
            }
        }



        protected void OnRateSelectionDisplayRatesLoaded(object sender, ViewEventArgs<ShoppedRate> e)
        {
            if (SaveShoppedRate != null)
                SaveShoppedRate(this, new ViewEventArgs<ShoppedRate>(e.Argument));
        }



        protected void OnVendorLaneHistoryClose(object sender, EventArgs e)
        {
            vendorLaneHistoryBuySell.Visible = false;
        }


        protected void OnDocumentDisplayClosed(object sender, EventArgs e)
        {
            documentDisplay.Visible = false;
        }



        protected void OnVendorRateAgreementClose(object sender, EventArgs e)
        {
            vendorRateAgreement.Visible = false;
        }

        protected void OnVendorRateAgreementGenerate(object sender, EventArgs e)
        {
            GenerateVendorRateAgreement(false);
        }


        protected void OnDoneEditInvoicedShipmentClick(object sender, EventArgs e)
        {
            var shipment = new Shipment(hidShipmentId.Value.ToLong(), true)
            {
                ActualDeliveryDate = string.IsNullOrEmpty(txtEditActualDeliveryDate.Text)
                                        ? DateUtility.SystemEarliestDateTime
                                        : txtEditActualDeliveryDate.Text.ToDateTime(),

                ActualPickupDate = string.IsNullOrEmpty(txtEditActualPickupDate.Text)
                                        ? DateUtility.SystemEarliestDateTime
                                        : txtEditActualPickupDate.Text.ToDateTime(),
                CreatedInError = chkCreatedInError.Checked

            };
            if (!string.IsNullOrEmpty(txtReference.Text)) shipment.ShipperReference = txtReference.Text;

            shipment.LoadCollections();

            var uVendors = lstEditInvoicedShipment
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidShipmentVendorId").ToCustomHiddenField().Value.ToLong(),
                    FailureCodeId = i.FindControl("ddlEditFailureCodes").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    FailureComments = i.FindControl("txtEditFailureComments").ToTextBox().Text,
                    ProNumber = i.FindControl("txtProNumber").ToTextBox().Text,
                })
                .ToDictionary(i => i.Id, i => i);


            foreach (var vendor in shipment.Vendors.Where(vendor => uVendors.ContainsKey(vendor.Id)))
            {
                vendor.TakeSnapShot();
                vendor.FailureCodeId = uVendors[vendor.Id].FailureCodeId;
                vendor.FailureComments = uVendors[vendor.Id].FailureComments;
                vendor.ProNumber = uVendors[vendor.Id].ProNumber;
            }

            pnlDimScreen.Visible = false;
            pnlEditInvoicedShipment.Visible = false;

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Shipment>(shipment));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Shipment>(shipment));
        }

        protected void OnCloseEditInvoicedShipmentClicked(object sender, EventArgs e)
        {
            pnlDimScreen.Visible = false;
            pnlEditInvoicedShipment.Visible = false;

            // unlock and return to read-only
            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Shipment>(new Shipment(hidShipmentId.Value.ToLong(), false)));

            SetEditStatus(false);
        }



        protected void OnClearResellerAdditionDetailsClicked(object sender, EventArgs e)
        {
            DisplayResellerAddition(null, false);
        }

        protected void OnRetrieveResellerAdditionDetailsClicked(object sender, EventArgs e)
        {
            var customer = new Customer(hidCustomerId.Value.ToLong());
            var rating = customer.Rating;

            DisplayResellerAddition(rating != null ? rating.ResellerAddition : null, rating != null && rating.BillReseller);
        }



        protected void OnShippingLabelGeneratorCloseClicked(object sender, EventArgs e)
        {
            shippingLabelGenerator.Visible = false;
        }



        protected void OnServiceModeSelectedIndexChanged(object sender, EventArgs e)
        {
            var serviceModeValue = ddlServiceMode.SelectedValue.ToInt();

            if (!serviceModeValue.EnumValueIsValid<ServiceMode>()) return;

            var serviceMode = serviceModeValue.ToEnum<ServiceMode>();

            //configure detail visibility based on service mode
            ConfigureServiceModeDisplay(serviceMode);

            //reset sales rep based on service mode
            DisplayCustomerSalesRepresentative(hidCustomerId.Value.ToLong() == default(long) ? null : new Customer(hidCustomerId.Value.ToLong()));
        }


        protected void OnFindShipmentCoordUserClicked(object sender, ImageClickEventArgs e)
        {
            hidFlag.Value = ShipmentCoordinatorFlag;
            userFinder.FilterForShipmentCoordinator = true;
            userFinder.FilterForCarrierCoordinator = false;
            userFinder.Reset();
            userFinder.Visible = true;
        }

        protected void OnFindCarrierCoordUserClicked(object sender, ImageClickEventArgs e)
        {
            hidFlag.Value = CarrierCoordinatorFlag;
            userFinder.FilterForCarrierCoordinator = true;
            userFinder.FilterForShipmentCoordinator = false;
            userFinder.Reset();
            userFinder.Visible = true;
        }



        protected void OnUserFinderItemSelected(object sender, ViewEventArgs<User> e)
        {
            switch (hidFlag.Value)
            {
                case CarrierCoordinatorFlag:
                    DisplayCarrierCoordinator(e.Argument);
                    break;
                case ShipmentCoordinatorFlag:
                    DisplayShipmentCoordinator(e.Argument);
                    break;
            }
            hidFlag.Value = string.Empty;
            userFinder.Visible = false;
        }

        protected void OnUserFinderSelectionCancelled(object sender, EventArgs e)
        {
            userFinder.Visible = false;
            hidFlag.Value = string.Empty;
        }




        private void CalculateAndSetMiles(TextBox textBoxToUpdate, MileageEngine engine)
        {
            var plugin = engine.RetrieveEnginePlugin();
            if (plugin == null)
            {
                textBoxToUpdate.Text = string.Empty;
                DisplayMessages(new[] { ValidationMessage.Error("Unable to find implemented mileage engine for {0}.", engine) });
                return;
            }
            var origin = new Location();
            operationsAddressInputOrigin.RetrieveAddress(origin);
            var dest = new Location();
            operationsAddressInputDestination.RetrieveAddress(dest);

            var p1 = new Point
            {
                Type = PointType.Address,
                PostalCode = origin.PostalCode,
                Country = engine.IsApplicationDefault()
                            ? origin.CountryId.GetString()
                            : origin.Country == null ? string.Empty : origin.Country.Code
            };
            var p2 = new Point
            {
                Type = PointType.Address,
                PostalCode = dest.PostalCode,
                Country = engine.IsApplicationDefault()
                            ? dest.CountryId.GetString()
                            : dest.Country == null ? string.Empty : dest.Country.Code
            };
            var miles = plugin.GetMiles(p1, p2);
            if (miles.ToInt() == -1)
            {
                textBoxToUpdate.Text = string.Empty;
                DisplayMessages(new[] { ValidationMessage.Error("Error retrieving miles [Message: {0}].", plugin.LastErrorMessage) });
                return;
            }

            textBoxToUpdate.Text = miles.ToString("n4");
        }

        protected void OnGetMilesClicked(object sender, ImageClickEventArgs e)
        {

            var imgBtn = (ImageButton)sender;

            if (imgBtn.ID == btnGetMiles.ID) // get miles on main shipment
            {
                var ms = new MileageSource(ddlMileageSource.SelectedValue.ToLong());
                if (!ms.KeyLoaded)
                {
                    txtMileage.Text = string.Empty;
                    DisplayMessages(new[] { ValidationMessage.Error("Unable to load mileage source for mileage engine. Please ensure mileage source is selected.") });
                    return;
                }

                var shipment = UpdateShipment();
                var msg = shipment.CalculateShipmentMiles(ms.MileageEngine);
                LoadShipment(shipment, false);
                upPnlStops.Update();

                if (msg.Any(m => m.Type == ValidateMessageType.Error)) DisplayMessages(msg);
            }
            else if (imgBtn.ID == "ibtnGetMiles") // miles for assets
            {
                var engine = imgBtn.Parent.FindControl("ddlMilageEngine").ToDropDownList().SelectedValue.ToInt().ToEnum<MileageEngine>();
                var txt = imgBtn.Parent.FindControl("txtShipmentMilesRun").ToTextBox();
                CalculateAndSetMiles(txt, engine);
            }
        }



        protected void OnLogPrimaryVendorRejectionClicked(object sender, ImageClickEventArgs e)
        {
            var shipment = UpdateShipment();

            var log = new VendorRejectionLog
            {
                UserId = ActiveUser.Id,
                VendorId = shipment.Vendors.First(v => v.Primary).VendorId,
                TenantId = ActiveUser.TenantId,
                Customer = shipment.Customer,
                OriginPostalCode = shipment.Origin.PostalCode,
                OriginCity = shipment.Origin.City,
                OriginState = shipment.Origin.State,
                OriginCountryCode = shipment.Origin.Country == null ? string.Empty : shipment.Origin.Country.Code,
                DestinationPostalCode = shipment.Destination.PostalCode,
                DestinationCountryCode = shipment.Destination.Country == null ? string.Empty : shipment.Destination.Country.Code,
                DestinationCity = shipment.Destination.City,
                DestinationState = shipment.Destination.State,
                ShipmentNumber = shipment.ShipmentNumber,
                ShipmentDateCreated = shipment.DateCreated,
                DateCreated = DateTime.Now,
                ServiceMode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>()
            };

            hidFlag.Value = PrimaryVendorRejectLogFlag;

            if (LogVendorRejection != null)
                LogVendorRejection(this, new ViewEventArgs<VendorRejectionLog>(log));
        }



        protected void OnServicesSelectionDoneClicked(object sender, EventArgs e)
        {
            var services = rptServices
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(i => new Service(i.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong()))
                .Where(i => i.KeyLoaded)
                .ToList();

            DisplayServices(services.Select(s => s.Id));

            var serviceChargeCodes = services
                .Select(i => i.ChargeCode)
                .ToList();

            var charges = lstCharges.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidChargeId").ToCustomHiddenField().Value.ToLong(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text,
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    VendorBillDocumentNumber = i.FindControl("txtVendorBillNumber").ToTextBox().Text,
                    VendorBillId = i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong(),
                    VendorId = i.FindControl("ddlVendors").ToDropDownList().SelectedValue.ToLong()
                })
                .ToList();

            var existingCodeIds = charges.Select(i => i.ChargeCodeId).ToList();
            foreach (var scc in serviceChargeCodes.Where(c => !existingCodeIds.Contains(c.Id)))
                charges.Add(new
                    {
                        Id = default(long),
                        Quantity = 1,
                        UnitBuy = 0.0000m,
                        UnitSell = 0.0000m,
                        UnitDiscount = 0.0000m,
                        Comment = string.Empty,
                        ChargeCodeId = scc.Id,
                        VendorBillDocumentNumber = string.Empty,
                        VendorBillId = default(long),
                        VendorId = default(long)
                    });

            lstCharges.DataSource = charges.OrderBy(c => c.Quantity);
            lstCharges.DataBind();
            tabCharges.HeaderText = charges.BuildTabCount(ChargesHeader);
            athtuTabUpdater.SetForUpdate(tabCharges.ClientID, charges.BuildTabCount(ChargesHeader));
            upPnlCharges.Update();
        }



        protected void OnMacroPointOrderInputClosed(object sender, EventArgs e)
        {
            macroPointOrderInput.Visible = false;
        }

        protected void OnMacroPointOrderInputSubmitOrder(object sender, ViewEventArgs<Order> e)
        {
            var order = e.Argument;

            var settings = ProcessorVars.IntegrationVariables[ActiveUser.TenantId].MacroPointParams;
            if (settings == null)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Tenant does not have existing MacroPoint Parameters") });
                return;
            }

            var notification = order.Notifications.First();
            var priceBreak = settings.PriceBreaks
                                     .FirstOrDefault(p =>
                                         notification.TrackDurationInHours == p.TrackDurationHours &&
                                         notification.TrackIntervalInMinutes == p.TrackInternvalMinutes);

            var serviceHandler = new MacroPointServiceHandler(settings, ActiveUser);

            //Preliminary validation so we don't bother to send out bad requests
            if (priceBreak == null)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Invalid price break") });
                return;
            }
            if ((order.Number.Type == NumberType.Mobile.GetString() && order.Number.Value.Length != 10) ||
                (order.Number.Type == NumberType.MacroPoint.GetString() && order.Number.Value.Length != 12))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Invalid Number length, Mobile numbers must be 10 digits long and MacroPoint tracking numbers must be 12 digits long") });
                return;
            }
            if (new MacroPointOrderSearch().MacroPointOrderForIdNumberExists(txtShipmentNumber.Text, ActiveUser.TenantId))
            {
                DisplayMessages(new[] { ValidationMessage.Error("This shipment already has a MacroPoint order associated with it") });
                return;
            }
            var trackStartDateTime = order.TrackStartDateTime.Replace("ET", string.Empty).ToDateTime();
            if (trackStartDateTime <= DateTime.Now.AddMinutes(-15))
            {
                DisplayMessages(new[] { ValidationMessage.Error("MacroPoint order cannot be started chronologically more than 15 minutes in the past") });
                return;
            }
            if (order.EmailCopiesOfUpdatesTo.GetString().Length > 500)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Email Updates To field must be less than 500 characters") });
                return;
            }
            if (order.Notes.GetString().Length > 500)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Notes field must be less than 500 characters") });
                return;
            }

            var originLocation = new ShipmentLocation();
            var destLocation = new ShipmentLocation();
            operationsAddressInputOrigin.RetrieveAddress(originLocation);
            operationsAddressInputDestination.RetrieveAddress(destLocation);
            var idNumber = string.Format("{0} [{1} {2} {3} to {4} {5} {6}]", txtShipmentNumber.Text,
                                                  originLocation.City, originLocation.State, originLocation.PostalCode,
                                                  destLocation.City, destLocation.State, destLocation.PostalCode);
            notification.IDNumber = idNumber.Length > WebApplicationConstants.MacroPointIdNumberStringLength
                                        ? idNumber.Substring(0, WebApplicationConstants.MacroPointIdNumberStringLength - 1)
                                        : idNumber;

            //create trip sheet from origin, destination, and stops
            var tripSheet = new TripSheet
            {
                Stops = new List<Stop>
                        {
                            new Stop
                                {
                                    Name = originLocation.Description,
                                    Address = new Address
                                        {
                                            Line1 = originLocation.Street1,
                                            Line2 = originLocation.Street2,
                                            City = originLocation.City,
                                            PostalCode = originLocation.PostalCode,
                                            StateOrProvince = originLocation.State
                                        },
                                    StartDateTime = txtDesiredPickupDate.Text.ToDateTime().SetTime(ddlEarlyPickup.SelectedValue).FormattedEasternTimeDate(),
                                    EndDateTime = txtDesiredPickupDate.Text.ToDateTime().SetTime(ddlLatePickup.SelectedValue).FormattedEasternTimeDate(),
                                    StopID = 0.ToString(),
                                    StopType = WebApplicationConstants.MacroPointPickupCode,
                                }
                        }
            };

            //iterate through stops adding any with a date where items are picked up or dropped off 
            var cnt = default(int);
            var shipment = new Shipment(hidShipmentId.Value.ToLong());
            UpdateItemsFromView(shipment);
            foreach (var stop in shipment.Stops)
            {
                if (stop.AppointmentDateTime <= DateUtility.SystemEarliestDateTime) continue;
                var isPickup = false;
                var isDelivery = false;
                foreach (var item in shipment.Items)
                {
                    if (item.Pickup == stop.StopOrder) isPickup = true;
                    if (item.Delivery == stop.StopOrder) isDelivery = true;
                }

                if (isPickup || isDelivery)
                {
                    cnt++;
                    tripSheet.Stops.Add(new Stop
                    {
                        Name = stop.Description,
                        Address = new Address
                        {
                            Line1 = stop.Street1,
                            Line2 = stop.Street2,
                            City = stop.City,
                            PostalCode = stop.PostalCode,
                            StateOrProvince = stop.State
                        },
                        StartDateTime = stop.AppointmentDateTime.AddHours(-1).FormattedEasternTimeDate(),
                        EndDateTime = stop.AppointmentDateTime.AddHours(1).FormattedEasternTimeDate(),
                        StopID = cnt.ToString(),
                        StopType = isPickup ? WebApplicationConstants.MacroPointPickupCode : WebApplicationConstants.MacroPointDropOffCode,
                    });
                }
            }
            //add destination to stops
            tripSheet.Stops.Add(new Stop
            {
                Name = destLocation.Description,
                Address = new Address
                {
                    Line1 = destLocation.Street1,
                    Line2 = destLocation.Street2,
                    City = destLocation.City,
                    PostalCode = destLocation.PostalCode,
                    StateOrProvince = destLocation.State
                },
                StartDateTime = txtEstimatedDeliveryDate.Text.ToDateTime().SetTime(ddlEarlyDelivery.SelectedValue).FormattedEasternTimeDate(),
                EndDateTime = txtEstimatedDeliveryDate.Text.ToDateTime().SetTime(ddlLateDelivery.SelectedValue).FormattedEasternTimeDate(),
                StopID = (cnt + 1).ToString(),
                StopType = WebApplicationConstants.MacroPointDropOffCode,
            });

            order.TripSheet = tripSheet;

            var macroPointOrder = serviceHandler.SubmitOrder(order, priceBreak);

            //If we get null back from the SubmitOrder, then the order was not created
            if (macroPointOrder == null)
            {
                DisplayMessages(serviceHandler.Errors.Select(er => ValidationMessage.Error(er)));
                return;
            }

            var errMsgs = new List<ValidationMessage>();
            if (serviceHandler.Errors.Any()) errMsgs = serviceHandler.Errors.Select(er => ValidationMessage.Warning(er)).ToList();

            //Add a charge to the shipment for MacroPoint tracking
            var charge = new ShipmentCharge
            {
                ChargeCode = ActiveUser.Tenant.MacroPointChargeCode,
                Quantity = 1,
                UnitBuy = priceBreak.Price.ToDecimal(),
                UnitSell = 0,
                Tenant = ActiveUser.Tenant,
                Comment = string.Empty,
                UnitDiscount = 0,
                Shipment = shipment
            };

            Exception ex;
            errMsgs.AddRange(new ShipmentUpdateHandler().SaveShipmentCharge(charge, out ex, ActiveUser));
            if (ex != null)
            {
                ErrorLogger.LogError(ex, Context);
            }

            errMsgs.AddRange(new MacroPointOrderUpdateHandler().SaveMacroPointOrder(macroPointOrder, out ex));
            if (ex != null) ErrorLogger.LogError(ex, Context);


            macroPointOrderInput.Visible = false;
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));

            if (errMsgs.Any())
            {
                errMsgs.Insert(0, ValidationMessage.Information("MacroPoint tracking order has been succesfully submitted but there were some errors:"));
                DisplayMessages(errMsgs);
            }
            else
            {
                DisplayMessages(new[] { ValidationMessage.Information("MacroPoint tracking order has been succesfully submitted") });
            }


            pnlMacroPointTracking.Visible = true;
            litMacroPointTrackingStatus.Text = string.IsNullOrEmpty(macroPointOrder.TrackingStatusDescription)
                                                         ? MacroPointRequestInitialized
                                                         : macroPointOrder.TrackingStatusDescription;

            //add the newly added charge to the current listview
            AddChargeToView(charge);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Shipment>(shipment));
        }




        protected void OnMacroPointTrackingIconClicked(object sender, EventArgs e)
        {
            macroPointTrackingMap.CanRequestLocationUpdate = (litMacroPointTrackingStatus.Text == OrderTrackingStatusCodes.TrackingNow.FormattedString() && hidEditStatus.Value.ToBoolean());
            macroPointTrackingMap.SetupMap(StoredLatLngCoordinates);
            macroPointTrackingMap.Visible = true;
        }

        protected void OnMacroPointTrackingMapClosed(object sender, EventArgs e)
        {
            macroPointTrackingMap.Visible = false;
        }

        protected void OnMacroPointTrackingMapRequestLocationUpdate(object sender, EventArgs e)
        {
            SendMacroPointLocationUpdateRequest();
        }


        protected void OnMessageBoxYesClicked(object sender, EventArgs e)
        {
            switch (hidFlag.Value)
            {
                case VoidShipmentFlag:
                    hidFlag.Value = string.Empty;
                    VoidShipment(false);
                    break;
                case ReverseVoidShipmentFlag:
                    hidFlag.Value = string.Empty;
                    ReverseVoidShipment(false);
                    break;
            }
        }

        protected void OnMessabgeBoxNoClicked(object sender, EventArgs e)
        {
            messageBox.Visible = false;
        }


        private List<OperationsViewNoteObj> GetCurrentViewNotesUpdated()
        {
            var notes = peNotes.GetData<OperationsViewNoteObj>();
            var vNotes = lstNotes
                .Items
                .Select(i => new
                {
                    EditIdx = i.FindControl("hidNoteIndex").ToCustomHiddenField().Value.ToInt(),
                    Note = new OperationsViewNoteObj
                    {
                        Id = i.FindControl("hidNoteId").ToCustomHiddenField().Value.ToLong(),
                        Archived = i.FindControl("chkArchived").ToAltUniformCheckBox().Checked,
                        Classified = i.FindControl("chkClassified").ToAltUniformCheckBox().Checked,
                        IsLoadOrderNote = i.FindControl("hidLoadOrderNote").ToCustomHiddenField().Value.ToBoolean(),
                        Message = i.FindControl("txtMessage").ToTextBox().Text,
                        Type = i.FindControl("ddlNoteType").ToDropDownList().SelectedValue.ToInt().ToEnum<NoteType>(),
                        Username = i.FindControl("txtUser").ToTextBox().Text,
                    }
                });
            foreach (var vn in vNotes)
                notes[vn.EditIdx] = vn.Note;

            return notes;
        }

        protected void OnAddNoteClicked(object sender, EventArgs e)
        {
            var notes = GetCurrentViewNotesUpdated();

            notes.Insert(0, new OperationsViewNoteObj
            {
                Id = default(long),
                Type = NoteTypeCollection[0].Value.ToInt().ToEnum<NoteType>(),
                Archived = false,
                Classified = true,
                Message = string.Empty,
                Username = ActiveUser.Username,
                IsLoadOrderNote = false,
            });

            peNotes.LoadData(notes);
            litNoteCount.Text = notes.BuildTabCount(string.Empty);
        }

        protected void OnDeleteUnsavedNoteClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var notes = GetCurrentViewNotesUpdated();
            var itemIdx = imageButton.FindControl("hidNoteIndex").ToCustomHiddenField().Value.ToInt();
            notes.RemoveAt(itemIdx);
            peNotes.LoadData(notes, peNotes.CurrentPage);
            litNoteCount.Text = notes.BuildTabCount(string.Empty);
        }

        protected void OnNotesItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var ddlType = e.Item.FindControl("ddlNotetype").ToDropDownList();

            var type = e.Item.DataItem.GetPropertyValue("Type").ToInt().GetString();
            ddlType.DataSource = NoteTypeCollection;
            ddlType.DataBind();
            if (ddlType.ContainsValue(type)) ddlType.SelectedValue = type;
        }

        protected void OnPeNotesPageChanging(object sender, EventArgs e)
        {
            var notes = GetCurrentViewNotesUpdated();
            peNotes.LoadData(notes, peNotes.CurrentPage);
        }




        private List<OperationsViewCheckCallObj> GetCurrentViewCheckCallsUpdated()
        {
            var checkCalls = peCheckCalls.GetData<OperationsViewCheckCallObj>();
            var vCheckCalls = lstCheckCalls
                .Items
                .Select(i => new
                {
                    EditIdx = i.FindControl("hidCheckCallIndex").ToCustomHiddenField().Value.ToInt(),
                    CheckCall = new OperationsViewCheckCallObj
                    {
                        Id = i.FindControl("hidCheckCallId").ToCustomHiddenField().Value.ToLong(),
                        CallDate = i.FindControl("txtCallDate").ToTextBox().Text.ToDateTime(),
                        EventDate = i.FindControl("txtEventDate")
                                             .ToTextBox()
                                             .Text.ToDateTime()
                                             .SetTime(i.FindControl("txtEventTime").ToTextBox().Text),
                        EdiStatusCode = i.FindControl("ddlEdiCode").ToDropDownList().SelectedValue,
                        CallNotes = i.FindControl("txtCallNotes").ToTextBox().Text,
                        Username = i.FindControl("txtUser").ToTextBox().Text,
                    }
                });
            foreach (var vc in vCheckCalls)
                checkCalls[vc.EditIdx] = vc.CheckCall;

            return checkCalls;
        }

        protected void OnAddCheckCallClicked(object sender, EventArgs e)
        {
            var checkCalls = GetCurrentViewCheckCallsUpdated();

            checkCalls.Insert(0, new OperationsViewCheckCallObj
            {
                Id = default(long),
                Username = ActiveUser.Username,
                CallDate = DateTime.Now,
                EventDate = DateTime.Now,
                CallNotes = string.Empty,
                EdiStatusCode = string.Empty,
            });

            peCheckCalls.LoadData(checkCalls);
            litCheckCallCount.Text = checkCalls.BuildTabCount(string.Empty);
            ConfigureCheckCallAlert(hidStatus.Value.ToEnum<ShipmentStatus>(), ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>());
        }

        protected void OnDeleteCheckCallClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var checkCalls = GetCurrentViewCheckCallsUpdated();
            var itemIdx = imageButton.FindControl("hidCheckCallIndex").ToCustomHiddenField().Value.ToInt();
            checkCalls.RemoveAt(itemIdx);
            peCheckCalls.LoadData(checkCalls, peCheckCalls.CurrentPage);
            litCheckCallCount.Text = checkCalls.BuildTabCount(string.Empty);
        }

        protected void OnCheckCallsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var ddl = e.Item.FindControl("ddlEdiCode").ToDropDownList();

            var code = e.Item.DataItem.GetPropertyValue("EdiStatusCode").GetString();
            var codes = WebApplicationUtilities.GetEdiStatusCodes();
            codes.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, string.Empty));
            ddl.DataSource = codes;
            ddl.DataBind();
            if (ddl.ContainsValue(code)) ddl.SelectedValue = code;
        }

        protected void OnPeCheckCallsPageChanging(object sender, EventArgs e)
        {
            var checkCalls = GetCurrentViewCheckCallsUpdated();
            peCheckCalls.LoadData(checkCalls, peCheckCalls.CurrentPage);
        }



        protected void OnAddQuickPayOptionClicked(object sender, EventArgs e)
        {
            quickPayOptionFinder.Visible = true;
        }

        protected void OnClearQuickPayOptionsClicked(object sender, EventArgs e)
        {
            lstQuickPayOptions.DataSource = new List<object>();
            lstQuickPayOptions.DataBind();
            litQuickPayCount.Text = new List<object>().BuildTabCount();
        }

        protected void OnQuickPayOptionMultiItemSelected(object sender, ViewEventArgs<List<QuickPayOption>> e)
        {
            var options = lstQuickPayOptions
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidQuickPayOptionId").ToCustomHiddenField().Value.ToLong(),
                    DiscountPercent = i.FindControl("txtDiscountPercent").ToTextBox().Text.ToDecimal(),
                    Description = i.FindControl("txtDescription").ToTextBox().Text
                })
                .ToList();

            options.AddRange(e.Argument
                              .Select(a => new
                              {
                                  Id = default(long),
                                  a.DiscountPercent,
                                  a.Description,
                              })
                              .ToList());

            lstQuickPayOptions.DataSource = options;
            lstQuickPayOptions.DataBind();
            litQuickPayCount.Text = options.BuildTabCount();

            quickPayOptionFinder.Visible = false;
        }

        protected void OnQuickPayOptionSelectionCancelled(object sender, EventArgs e)
        {
            quickPayOptionFinder.Visible = false;
        }

        protected void OnDeleteQuickPayOptionClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var options = lstQuickPayOptions
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidQuickPayOptionId").ToCustomHiddenField().Value.ToLong(),
                    DiscountPercent = i.FindControl("txtDiscountPercent").ToTextBox().Text.ToDecimal(),
                    Description = i.FindControl("txtDescription").ToTextBox().Text
                })
                .ToList();

            options.RemoveAt(imageButton.Parent.FindControl("hidQuickPayOptionIndex").ToCustomHiddenField().Value.ToInt());

            lstQuickPayOptions.DataSource = options;
            lstQuickPayOptions.DataBind();
            litQuickPayCount.Text = options.BuildTabCount();
        }



        protected void OnAddAssetClicked(object sender, EventArgs e)
        {
            var assets = lstAssets
                .Items
                .Select(control => new
                {
                    Id = control.FindControl("hidShipmentAssetId").ToCustomHiddenField().Value.ToLong(),
                    DriverAssetId = control.FindControl("ddlShipmentDriverAsset").ToDropDownList().SelectedValue.ToLong(),
                    TractorAssetId = control.FindControl("ddlShipmentTractorAsset").ToDropDownList().SelectedValue.ToLong(),
                    TrailerAssetId = control.FindControl("ddlShipmentTrailerAsset").ToDropDownList().SelectedValue.ToLong(),
                    MilesRun = control.FindControl("txtShipmentMilesRun").ToTextBox().Text.ToDecimal(),
                    MileageEngine = control.FindControl("ddlMilageEngine").ToDropDownList().SelectedValue.ToInt().ToEnum<MileageEngine>(),
                    Primary = control.FindControl("chkPrimary").ToCheckBox().Checked
                })
                .ToList();
            assets.Add(new
            {
                Id = default(long),
                DriverAssetId = default(long),
                TractorAssetId = default(long),
                TrailerAssetId = default(long),
                MilesRun = 0m,
                MileageEngine = MileageEngine.LongitudeLatitude,
                Primary = false
            });

            lstAssets.DataSource = assets;
            lstAssets.DataBind();
            litAssetsCount.Text = assets.BuildTabCount();
        }

        protected void OnClearAssetsClicked(object sender, EventArgs e)
        {
            lstAssets.DataSource = new List<object>();
            lstAssets.DataBind();
            litAssetsCount.Text = new List<object>().BuildTabCount();
        }

        protected void OnDeleteAssetClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var assets = lstAssets
                .Items
                .Select(control => new
                {
                    Id = control.FindControl("hidShipmentAssetId").ToCustomHiddenField().Value.ToLong(),
                    DriverAssetId = control.FindControl("ddlShipmentDriverAsset").ToDropDownList().SelectedValue.ToLong(),
                    TractorAssetId = control.FindControl("ddlShipmentTractorAsset").ToDropDownList().SelectedValue.ToLong(),
                    TrailerAssetId = control.FindControl("ddlShipmentTrailerAsset").ToDropDownList().SelectedValue.ToLong(),
                    MilesRun = control.FindControl("txtShipmentMilesRun").ToTextBox().Text.ToDecimal(),
                    MileageEngine = control.FindControl("ddlMilageEngine").ToDropDownList().SelectedValue.ToInt().ToEnum<MileageEngine>(),
                    Primary = control.FindControl("chkPrimary").ToCheckBox().Checked
                })
                .ToList();

            assets.RemoveAt(imageButton.Parent.FindControl("hidShipmentAssetIndex").ToCustomHiddenField().Value.ToInt());

            lstAssets.DataSource = assets;
            lstAssets.DataBind();
            litAssetsCount.Text = assets.BuildTabCount();
        }

        protected void OnAssetsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var data = e.Item.DataItem;

            var ddlDriver = e.Item.FindControl("ddlShipmentDriverAsset").ToDropDownList();
            ddlDriver.DataSource = DriveAssetCollection;
            ddlDriver.DataBind();
            if (ddlDriver.ContainsValue(data.GetPropertyValue("DriverAssetId").GetString()))
                ddlDriver.SelectedValue = data.GetPropertyValue("DriverAssetId").GetString();

            var ddlTractor = e.Item.FindControl("ddlShipmentTractorAsset").ToDropDownList();
            ddlTractor.DataSource = TractorAssetCollection;
            ddlTractor.DataBind();
            if (ddlTractor.ContainsValue(data.GetPropertyValue("TractorAssetId").GetString()))
                ddlTractor.SelectedValue = data.GetPropertyValue("TractorAssetId").GetString();

            var ddlTrailer = e.Item.FindControl("ddlShipmentTrailerAsset").ToDropDownList();
            ddlTrailer.DataSource = TrailerAssetCollection;
            ddlTrailer.DataBind();
            if (ddlTrailer.ContainsValue(data.GetPropertyValue("TrailerAssetId").GetString()))
                ddlTrailer.SelectedValue = data.GetPropertyValue("TrailerAssetId").GetString();

            var ddlMilesEngine = e.Item.FindControl("ddlMilageEngine").ToDropDownList();
            ddlMilesEngine.DataSource = MileageEngineCollection;
            ddlMilesEngine.DataBind();
            if (ddlMilesEngine.ContainsValue(data.GetPropertyValue("MileageEngine").ToInt().GetString()))
                ddlMilesEngine.SelectedValue = data.GetPropertyValue("MileageEngine").ToInt().GetString();
        }




        private void LoadDocumentForEdit(ShipmentDocument document)
        {
            chkDocumentIsInternal.Checked = document.IsInternal;
            txtDocumentName.Text = document.Name;
            txtDocumentDescription.Text = document.Description;
            ddlDocumentTag.SelectedValue = document.DocumentTagId.GetString();
            txtLocationPath.Text = GetLocationFileName(document.LocationPath);
            hidLocationPath.Value = document.LocationPath;
	        hidVendorBillId.Value = document.VendorBillId.ToString();

        }

        protected string GetLocationFileName(object relativePath)
        {
            return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
        }

        protected void OnAddDocumentClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadDocumentForEdit(new ShipmentDocument());
            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnClearDocumentsClicked(object sender, EventArgs e)
        {
            foreach (var item in lstDocuments.Items)
                hidFilesToDelete.Value += string.Format("{0};", item.FindControl("hidLocationPath").ToCustomHiddenField().Value);

            lstDocuments.DataSource = new List<object>();
            lstDocuments.DataBind();
            tabDocuments.HeaderText = new List<object>().BuildTabCount(DocumentsHeader);
        }

        protected void OnEditDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            hidEditingIndex.Value = control.FindControl("hidDocumentIndex").ToCustomHiddenField().Value;

            var document = new ShipmentDocument
            {
                Name = control.FindControl("litDocumentName").ToLiteral().Text,
                Description = control.FindControl("litDocumentDescription").ToLiteral().Text,
                DocumentTagId = control.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                LocationPath = control.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                IsInternal = control.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
				VendorBillId = control.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong(),
            };

            LoadDocumentForEdit(document);

            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnDeleteDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var documents = lstDocuments
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("litDocumentName").ToLiteral().Text,
                    Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                    DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                    LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                    IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
                    VendorBill = new VendorBill(i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong())
                })
                .ToList();

            var index = imageButton.Parent.FindControl("hidDocumentIndex").ToCustomHiddenField().Value.ToInt();
            hidFilesToDelete.Value += string.Format("{0};", documents[index].LocationPath);
            documents.RemoveAt(index);

            lstDocuments.DataSource = documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);
        }

        protected void OnCloseEditDocumentClicked(object sender, EventArgs eventArgs)
        {
            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditDocumentDoneClicked(object sender, EventArgs e)
        {
            var virtualPath = WebApplicationSettings.ShipmentFolder(ActiveUser.TenantId, hidShipmentId.Value.ToLong());
            var physicalPath = Server.MapPath(virtualPath);

            var documents = lstDocuments
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("litDocumentName").ToLiteral().Text,
                    Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                    DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value,
                    LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                    IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
					VendorBill = new VendorBill(i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong()),
                })
                .ToList();

            var documentIndex = hidEditingIndex.Value.ToInt();

            var documentId = default(long);
            if (documentIndex != -1)
            {
                documentId = documents[documentIndex].Id;
                documents.RemoveAt(documentIndex);
            }

            var locationPath = hidLocationPath.Value; // this should actually be a hidden field holding the existing path during edit
            var vendorBillid = hidVendorBillId.Value; // this should actually be a hidden field holding the existing path during edit

            documents.Insert(0, new
            {
                Id = documentId,
                Name = txtDocumentName.Text,
                Description = txtDocumentDescription.Text,
                DocumentTagId = ddlDocumentTag.SelectedValue,
                LocationPath = locationPath,
                IsInternal = chkDocumentIsInternal.Checked,
				VendorBill = new VendorBill(vendorBillid.ToLong()),
				
            });

            if (fupLocationPath.HasFile)
            {
                // ensure directory is present
                if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

                //ensure unique filename
                var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLocationPath.FileName), true));

                // save file
                fupLocationPath.WriteToFile(physicalPath, fi.Name, Server.MapPath(documents[0].LocationPath));

                // remove updated record
                documents.RemoveAt(0);

                // re-add record with new file
                documents.Insert(0, new
                {
                    Id = documentId,
                    Name = txtDocumentName.Text,
                    Description = txtDocumentDescription.Text,
                    DocumentTagId = ddlDocumentTag.SelectedValue,
                    LocationPath = virtualPath + fi.Name,
                    IsInternal = chkDocumentIsInternal.Checked,
					VendorBill = new VendorBill(vendorBillid.ToLong())
					
                });
            }

            lstDocuments.DataSource = documents.OrderBy(d => d.Name);
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);

            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnLocationPathClicked(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
            Response.Export(Server.ReadFromFile(path), button.Text);
        }


        protected void OnAddLoadOrderDocumentsClicked(object sender, EventArgs e)
        {
            var shipment = new Shipment(hidShipmentId.Value.ToLong());
            var loadOrder = new LoadOrderSearch().FetchLoadOrderByLoadNumber(shipment.ShipmentNumber, shipment.TenantId);

            var documents = lstDocuments
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("litDocumentName").ToLiteral().Text,
                    Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                    DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                    LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                    IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
                    VendorBill = new VendorBill(i.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong())
                })
                .ToList();

            var virtualPath = WebApplicationSettings.ShipmentFolder(ActiveUser.TenantId, hidShipmentId.Value.ToLong());
            var physicalPath = Server.MapPath(virtualPath);

            // ensure directory is present
            if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

	        var transferredCnt = 0;
            foreach (var loadOrderDocument in loadOrder.Documents)
            {
                //ensure unique filename
                var loadFi = new FileInfo(Server.MapPath(loadOrderDocument.LocationPath));
                var shipmentFi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, loadFi.Name), true));

                // copy file (if exists)
	            if (!loadFi.Exists) continue;

	            File.Copy(Server.MapPath(loadOrderDocument.LocationPath), Path.Combine(physicalPath, shipmentFi.Name));

	            var shipmentDocument = new
		            {
			            Id = default(long),
			            loadOrderDocument.Name,
			            loadOrderDocument.Description,
			            loadOrderDocument.DocumentTagId,
			            LocationPath = virtualPath + shipmentFi.Name,
			            loadOrderDocument.IsInternal,
                        VendorBill = new VendorBill()
		            };
	            documents.Add(shipmentDocument);
	            transferredCnt++;
            }

            DisplayMessages(new[]
                {
                    new ValidationMessage
                        {
                            Message = string.Format("{0} load order documents found. {1} Transferred.", loadOrder.Documents.Count, transferredCnt),
                            Type = ValidateMessageType.Information
                        }
                });

            lstDocuments.DataSource = documents;
            lstDocuments.DataBind();
        }


        private void ProcessStopMoves(IList<ShipmentLocation> locations, int newIdx, int currentIdx)
        {
            var l1 = locations[currentIdx];
            var l2 = locations[newIdx];

            locations[currentIdx] = l2;
            locations[newIdx] = l1;

            lstStops.DataSource = locations;
            lstStops.DataBind();
            tabStops.HeaderText = locations.BuildTabCount(StopsHeader);
        }

        private void RefreshItemStops(List<ShipmentLocation> stops)
        {
            foreach (var item in lstItems.Items)
            {
                // Bind stops
                var ddlItemPickup = item.FindControl("ddlItemPickup").ToDropDownList();
                var ddlItemDelivery = item.FindControl("ddlItemDelivery").ToDropDownList();

                var pickupValue = ddlItemPickup.SelectedValue;
                var delValue = ddlItemDelivery.SelectedValue;


                var originStops = stops
                    .OrderBy(s => s.StopOrder)
                    .Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
                    .ToList();
                originStops.Insert(0, new ViewListItem(OriginStopText, 0.ToString()));

                var destStops = stops
                    .OrderBy(s => s.StopOrder)
                    .Select(s => new ViewListItem(string.Format("Stop #{0}", s.StopOrder), s.StopOrder.ToString()))
                    .ToList();
                destStops.Add(new ViewListItem(DestinationStopText, ViewDeliveryStopOrder.ToString()));

                ddlItemPickup.DataSource = originStops;
                ddlItemPickup.DataBind();
                if (ddlItemPickup.ContainsValue(pickupValue)) ddlItemPickup.SelectedValue = pickupValue;


                ddlItemDelivery.DataSource = destStops;
                ddlItemDelivery.DataBind();
                if (ddlItemDelivery.ContainsValue(delValue)) ddlItemDelivery.SelectedValue = delValue;
            }
        }

        protected void OnStopsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = e.Item as ListViewDataItem;
            if (item == null) return;
            var ic = item.FindControl("llicLocation").ToLocationListingInputControl();
            var location = item.DataItem as ShipmentLocation;
            if (location == null) return;
            ic.LoadLocation(location);

        }

        protected void OnAddLocationClicked(object sender, EventArgs e)
        {
            var locations = RetrieveViewStops(new Shipment(hidShipmentId.Value.ToLong(), false));
            locations.Add(new ShipmentLocation
            {
                AppointmentDateTime = DateUtility.SystemEarliestDateTime,
                StopOrder = locations.Count + 1
            });
            lstStops.DataSource = locations;
            lstStops.DataBind();
            tabStops.HeaderText = locations.BuildTabCount(StopsHeader);
            athtuTabUpdater.SetForUpdate(tabStops.ClientID, locations.BuildTabCount(StopsHeader));

            // update items stops by retrieving and reloading!
            RefreshItemStops(locations);
            upPnlItems.Update();
        }

        protected void OnDeleteLocation(object sender, ViewEventArgs<int> e)
        {
            var locations = RetrieveViewStops(new Shipment(hidShipmentId.Value.ToLong(), false));
            locations.RemoveAt(e.Argument);
            lstStops.DataSource = locations;
            lstStops.DataBind();
            tabStops.HeaderText = locations.BuildTabCount(StopsHeader);
            athtuTabUpdater.SetForUpdate(tabStops.ClientID, locations.BuildTabCount(StopsHeader));

            for (var i = 0; i < locations.Count; i++) locations[i].StopOrder = i + 1;

            // update items stops by retrieving and reloading!
            RefreshItemStops(locations);
            upPnlItems.Update();
        }

        protected void OnMoveUpClicked(object sender, EventArgs e)
        {
            var btn = (ImageButton)sender;
            var input = btn.Parent.FindControl("llicLocation").ToLocationListingInputControl();
            var index = input.RetrieveLocatinItemIndex();

            if (index - 1 < 0) return;

            var locations = RetrieveViewStops(new Shipment(hidShipmentId.Value.ToLong(), false));

            ProcessStopMoves(locations, index - 1, index);
        }

        protected void OnMoveDownClicked(object sender, EventArgs e)
        {
            var btn = (ImageButton)sender;
            var input = btn.Parent.FindControl("llicLocation").ToLocationListingInputControl();
            var index = input.RetrieveLocatinItemIndex();

            var locations = RetrieveViewStops(new Shipment(hidShipmentId.Value.ToLong(), false));

            if (index >= locations.Count - 1) return; // i.e. it's already the last one in the list

            ProcessStopMoves(locations, index + 1, index);
        }


        protected void OnVendorPerformanceClose(object sender, EventArgs e)
        {
            vpscVendorPerformance.Visible = false;
        }


        protected void OnViewVendorPerformanceStatisticsClicked(object sender, ImageClickEventArgs e)
        {
            vpscVendorPerformance.Visible = true;
            vpscVendorPerformance.ServiceMode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>();
            vpscVendorPerformance.LoadVendorPerformanceSummary(hidPrimaryVendorId.Value.ToLong());
        }

        protected void OnViewAdditionalVendorPerformanceStatisticsClicked(object sender, ImageClickEventArgs e)
        {
            vpscVendorPerformance.Visible = true;
            vpscVendorPerformance.ServiceMode = ddlServiceMode.SelectedValue.ToInt().ToEnum<ServiceMode>();
            vpscVendorPerformance.LoadVendorPerformanceSummary(((Control)sender).FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong());
        }


        protected void OnPaymentEntryCloseClicked(object sender, EventArgs e)
        {
            pecPayment.Visible = false;
        }

        protected void OnPaymentEntryProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
        {
            DisplayMessages(e.Argument);
        }

        protected void OnPaymentEntryPaymentSuccessfullyProcessed(object sender, EventArgs e)
        {
            pecPayment.Clear();
            pecPayment.Visible = false;

            var amountPaidToShipment = new Shipment(hidShipmentId.Value.ToLong()).GetAmountPaidToShipmentViaMiscReceipts();
            hidMiscReceiptsRelatedToShipmentExist.Value = (amountPaidToShipment > 0).ToString();
            txtCollectedPaymentAmount.Text = amountPaidToShipment.GetString();
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));
        }


        protected void OnPaymentRefundCloseClicked(object sender, EventArgs e)
        {
            prcRefundPayments.Visible = false;
        }

        protected void OnPaymentRefundProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
        {
            DisplayMessages(e.Argument);
        }

        protected void OnRefundPaymentsPaymentRefundProcessed(object sender, EventArgs e)
        {
            prcRefundPayments.Clear();
            prcRefundPayments.Visible = false;
            var amountPaidToShipment = new Shipment(hidShipmentId.Value.ToLong()).GetAmountPaidToShipmentViaMiscReceipts();
            hidMiscReceiptsRelatedToShipmentExist.Value = (amountPaidToShipment > 0).ToString();
            txtCollectedPaymentAmount.Text = amountPaidToShipment.GetString();
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));
        }


        protected void OnFindJobClicked(object sender, EventArgs e)
        {
            jobFinder.Visible = true;
        }

        protected void OnClearJobClicked(object sender, EventArgs e)
        {
            hidJobId.Value = string.Empty;
            txtJobNumber.Text = string.Empty;
            txtJobStep.Text = string.Empty;
        }

        protected void OnJobNumberTextChanged(object sender, EventArgs e)
        {
            var jobNumber = txtJobNumber.Text;

            var job = new JobSearch().FetchJobByJobNumber(jobNumber, ActiveUser.TenantId);

            if (job == null)
            {
                hidJobId.Value = string.Empty;

                txtJobNumber.Text = string.Empty;
                txtJobStep.Text = string.Empty;
                return;
            }

            hidJobId.Value = job.Id.ToString();
            txtJobStep.Text = string.Empty;
        }

        protected void OnJobFinderItemSelected(object sender, ViewEventArgs<Job> e)
        {
            var job = e.Argument;

            hidJobId.Value = job.Id.ToString();

            txtJobNumber.Text = job.JobNumber;
            txtJobStep.Text = string.Empty;

            jobFinder.Visible = false;
        }

        protected void OnJobFinderItemCancelled(object sender, EventArgs e)
        {
            jobFinder.Visible = false;
        }

    }
}