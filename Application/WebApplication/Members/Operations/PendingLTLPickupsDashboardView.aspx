﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="PendingLTLPickupsDashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.PendingLtlPickupsDashboardView"
    EnableEventValidation="false" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                        Pending LTL Pickups Dashboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
        </div>
        <hr class="dark mb10" />
        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="PendingLtlPickupsDashboard" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" ShowAutoRefresh="True" OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort By:</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <script type="text/javascript">
                    $(function () {
                        $(document).scroll(function () {
                            $('#<%= hidPageScroll.ClientID %>').val($(document).scrollTop());
                        });
                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                            $(document).scroll(function () {
                                $('#<%= hidPageScroll.ClientID %>').val($(document).scrollTop());
                            });
                        });
                    });
                </script>
                <asp:Literal runat="server" ID="litTemporaryScript" EnableViewState="False" />
                <eShip:CustomHiddenField runat="server" ID="hidPageScroll" />
                <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstShipmentPickupDetails" UpdatePanelToExtendId="upDataUpdate" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentPickupsDashboardTable" TableId="shipmentPickupsDashboardTable" HeaderZIndex="2" />
                <table class="line2 pl2" id="shipmentPickupsDashboardTable">
                    <tr>
                        <th style="width: 6%" class="text-center">Confirm Pickup
                        </th>
                        <th style="width: 7%;">
                            <asp:LinkButton runat="server" ID="lbtnSortShipmentNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Shipment Number
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDateCreated" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Date Created
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDesiredPickupDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Desired Pickup
                            </asp:LinkButton>
                        </th>
                        <th style="width: 24%;">Customer&nbsp;
                            <asp:LinkButton runat="server" ID="lbtnSortCustomerNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Number
                            </asp:LinkButton>/
                            <asp:LinkButton runat="server" ID="lbtnSortCustomerName" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Name
                            </asp:LinkButton>
                        </th>
                        <th style="width: 24%;">Vendor&nbsp;
                            <asp:LinkButton runat="server" ID="lbtnSortVendorNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Number
                            </asp:LinkButton>/
                            <asp:LinkButton runat="server" ID="lbtnSortVendorName" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Name
                            </asp:LinkButton>
                        </th>
                        <th style="width: 7%;">
                            <asp:LinkButton runat="server" ID="lbtnSortStatus" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Status
                            </asp:LinkButton>
                        </th>
                        <th style="width: 7%;">
                            <asp:LinkButton runat="server" ID="lbtnSortFaxStatus" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Fax Status
                            </asp:LinkButton>
                        </th>
                        <th style="width: 7%">Action
                        </th>
                    </tr>
                    <asp:ListView runat="server" ID="lstShipmentPickupDetails" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnShipmentPickupDetailsItemDataBound">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td rowspan="2" class="top text-center">
                                    <eShip:CustomHiddenField runat="server" ID="hidShipmentId" Value='<%# Eval("Id") %>' />
                                    <asp:Button runat="server" ID="btnConfirmPickup" OnClick="OnConfirmPickupClicked" Text="Confirm" />
                                </td>
                                <td>
                                    <%# Eval("ShipmentNumber") %>
                                </td>
                                <td>
                                    <%# Eval("DateCreated").FormattedLongDate() %>
                                </td>
                                <td>
                                    <%# Eval("DesiredPickupDate").FormattedLongDate() %>
                                </td>
                                <td>
                                    <%# string.Format("{0} - {1}", Eval("CustomerNumber"), Eval("CustomerName")) %>
                                    <%# ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(CustomerView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("CustomerId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                                </td>
                                <td>
                                    <%# string.Format("{0} - {1}", Eval("VendorNumber"), Eval("VendorName")) %>
                                    <%# ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(VendorView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("VendorId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                </td>
                                <td>
                                    <%# Eval("Status") %>
                                </td>
                                <td>
                                    <%# Eval("FaxStatus") %>                                    
                                </td>
                                <td rowspan="2" class="text-center top">
                                    <div class="row">
                                        <%# ActiveUser.HasAccessTo(ViewCode.Shipment) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src='{3}' /></a>", 
                                                    ResolveUrl(ShipmentView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("ShipmentNumber"),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                : string.Empty %>
                                    </div>
                                    <div class="row">
                                        <asp:ImageButton runat="server" ID="ibtnLockShipment" CausesValidation="False" OnClick="OnLockShipmentClicked" ImageUrl='<%# Eval("IsLocked").ToBoolean() ?  "~/images/icons2/unlockBlue.png" :"~/images/icons2/lockBlue.png" %>'
                                            ToolTip='<%# Eval("IsLocked").ToBoolean() ? string.Format(CurrentlyLockedBy, Eval("LockedByUsername")) : LockToolTip %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidLockUserId" Value='<%# Eval("LockedByUserId") %>' />
                                    </div>
                                </td>
                            </tr>
                            <tr class="f9">
                                <td colspan="7" class="forceLeftBorder">
                                    <div class="rowgroup mb0">
                                        <div class="col_1_2">
                                            <div class="col_1_2">
                                                <div class="row pb5">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel blue">Origin</label>
                                                        <label>
                                                            <%# !string.IsNullOrEmpty(Eval("OriginName").GetString()) ? string.Format("{0}<br/>",Eval("OriginName") ) : string.Empty %>
                                                            <%# !string.IsNullOrEmpty(Eval("OriginStreet1").GetString()) ? string.Format("{0}<br/>",Eval("OriginStreet1") ) : string.Empty %>
                                                            <%# !string.IsNullOrEmpty(Eval("OriginStreet2").GetString()) ? string.Format("{0}<br/>",Eval("OriginStreet2") ) : string.Empty %>
                                                            <%# string.IsNullOrEmpty(Eval("OriginCity").GetString()) ? string.Empty : string.Format("{0},", Eval("OriginCity"))%> <%#Eval("OriginState")%> <%#Eval("OriginPostalCode")%> <%#Eval("OriginCountryName")%>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel blue">Origin Contact</label>
                                                        <label>
                                                            <%# !string.IsNullOrEmpty(Eval("OriginPrimaryContactName").GetString()) ? string.Format("{0}<br/>",Eval("OriginPrimaryContactName") ) : string.Empty %>
                                                            <%# !string.IsNullOrEmpty(Eval("OriginPrimaryContactPhone").GetString()) ? string.Format("<abbr title='Phone' class='blue'>P.</abbr> {0}",Eval("OriginPrimaryContactPhone") ) : string.Empty %>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col_1_2">
                                                <div class="row">
                                                    <label class="wlabel blue">Origin Terminal</label>
                                                    <label>
                                                        <%# !string.IsNullOrEmpty(Eval("OriginTerminalName").GetString()) ? string.Format("{0}<br/>",Eval("OriginTerminalName") ) : string.Empty %>
                                                        <%# !string.IsNullOrEmpty(Eval("OriginTerminalStreet1").GetString()) ? string.Format("{0}<br/>",Eval("OriginTerminalStreet1") ) : string.Empty %>
                                                        <%# !string.IsNullOrEmpty(Eval("OriginTerminalStreet2").GetString()) ? string.Format("{0}<br/>",Eval("OriginTerminalStreet2") ) : string.Empty %>
                                                        <%# string.IsNullOrEmpty(Eval("OriginTerminalCity").GetString()) ? string.Empty : string.Format("{0},", Eval("OriginTerminalCity"))%> <%#Eval("OriginTerminalState")%> <%# Eval("OriginTerminalPostalCode") %> <%#Eval("OriginTerminalCountryName")%>
                                                        <br />
                                                        <%# !(string.IsNullOrEmpty(Eval("OriginTerminalContactTitle").GetString()) && string.IsNullOrEmpty(Eval("OriginTerminalContactName").GetString())) 
                                                                ? string.Format("{0}: {1}<br/>",Eval("OriginTerminalContactTitle"), Eval("OriginTerminalContactName")) 
                                                                : string.Empty %>
                                                        <%# !(string.IsNullOrEmpty(Eval("OriginTerminalPhone").GetString()) && string.IsNullOrEmpty(Eval("OriginTerminalFax").GetString())) 
                                                                ? string.Format("<abbr title='Phone' class='blue'>P.</abbr> {0} &nbsp;&nbsp; <abbr title='Fax' class='blue'>F.</abbr> {1}<br/>", Eval("OriginTerminalPhone"), Eval("OriginTerminalFax"))
                                                                : string.Empty %>
                                                        <%# !string.IsNullOrEmpty(Eval("OriginTerminalTollFree").GetString()) ? string.Format("<abbr title='Toll Free Number' class='blue'>T.</abbr> {0}", Eval("OriginTerminalTollFree")) : string.Empty %>
                                                        <br />
                                                        <%# !(string.IsNullOrEmpty(Eval("OriginTerminalEmail").GetString())) ? string.Format("<abbr title='Email' class='blue'>E.</abbr> <span class='fs75em'>{0}</span>", Eval("OriginTerminalEmail")) : string.Empty %>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col_1_2">
                                            <div class="col_1_2">
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel blue">Destination</label>
                                                        <label>
                                                            <%# !string.IsNullOrEmpty(Eval("DestinationName").GetString()) ? string.Format("{0}<br/>",Eval("DestinationName") ) : string.Empty %>
                                                            <%# !string.IsNullOrEmpty(Eval("DestinationStreet1").GetString()) ? string.Format("{0}<br/>",Eval("DestinationStreet1") ) : string.Empty %>
                                                            <%# !string.IsNullOrEmpty(Eval("DestinationStreet2").GetString()) ? string.Format("{0}<br/>",Eval("DestinationStreet2") ) : string.Empty %>
                                                            <%# string.IsNullOrEmpty(Eval("DestinationCity").GetString()) ? string.Empty : string.Format("{0},", Eval("DestinationCity"))%> <%#Eval("DestinationState")%> <%#Eval("DestinationPostalCode")%> <%#Eval("DestinationCountryName")%>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel blue">Prefix</label>
                                                        <%# string.Format("{0} - {1}",Eval("Prefix"),Eval("PrefixDescription")) %>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel blue">Account Bucket</label>
                                                        <%# string.Format("{0} - {1}",Eval("AccountBucketCode"),Eval("AccountBucketDescription")) %>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col_1_2">
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel blue">
                                                            Check Call Notes<eShip:TextAreaMaxLengthExtender runat="server" ID="tamleNote" TargetControlId="txtNote" MaxLength="500" />
                                                            <asp:ImageButton runat="server" ID="ibtnDispatchShipment" ImageUrl="~/images/icons2/cog.png" OnClick="OnDispatchShipmentClicked"
                                                                ToolTip="Dispatch Shipment" Visible='<%# Eval("IsLocked").ToBoolean() && Eval("ShipmentDispatchEnabled").ToBoolean() %>' Width="20px" />
                                                            <eShip:CustomHiddenField runat="server" ID="hidCanDispatch" Value='<%# Eval("ShipmentDispatchEnabled") %>' />
                                                        </label>
                                                        <eShip:CustomTextBox ID="txtNote" runat="server" TextMode="MultiLine" CssClass="w240 h125" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="blue upper">Is Hazardous Material: </label>
                                                        <%# Eval("HazardousMaterial").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="rowgroup mb0">
                                        <div class="col_1_2">
                                            <div class="col_1_2">
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel blue">Shipper Reference</label>
                                                        <label>
                                                            <%# Eval("ShipperReference") %>&nbsp;
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col_1_2">
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel blue">Pickup Time Between</label>
                                                        <label>
                                                            <%# Eval("EarlyPickup") %> - <%# Eval("LatePickup") %>
                                                            <span class='ml5 mr5 flag pl5 pr5 <%# Eval("IsGuaranteedDeliveryService").ToBoolean() ? string.Empty : "hidden" %>' title='<%# string.Format("Guaranteed Delivery Service by {0}" ,Eval("GuaranteedDeliveryServiceTime").GetString().ConvertAmPm()) %>'>GS</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col_1_2">
                                            <div class="row">
                                                <div class="fieldgroup">
                                                    <label class="wlabel blue">Pickup Instructions</label>
                                                    <label>
                                                        <%#Eval("OriginSpecialInstructions") %>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div runat="server" class="rowgroup mb10" id="divAccessorials">
                                        <div class="row">
                                            <label class="wlabel blue">Accessorials</label>
                                            <asp:Literal runat="server" ID="litAccessorials" />
                                        </div>
                                    </div>
                                    <div class="rowgroup mb10">
                                        <div class="row">
                                            <asp:Repeater runat="server" ID="rptShipmentItems">
                                                <HeaderTemplate>
                                                    <h6 class="mt0 mb0">Items</h6>
                                                    <table class="contain pl2">
                                                        <tr class="bbb1">
                                                            <td style="width: 40%;" class="no-top-border blue">Description
                                                            </td>
                                                            <td style="width: 16%;" class="no-top-border no-left-border blue">
                                                                <abbr title="Package Type">Pkg. Type</abbr>
                                                            </td>
                                                            <td style="width: 10%;" class="no-top-border no-left-border blue">Weight
                                                            </td>
                                                            <td style="width: 7%;" class="no-top-border no-left-border blue text-center">
                                                                <abbr title="Quantity">Qty.</abbr>
                                                            </td>
                                                            <td style="width: 20%;" class="no-top-border no-left-border blue">Dimensions (L x W x H)
                                                            </td>
                                                            <td style="width: 7%;" class="no-top-border no-left-border blue">Stackable
                                                            </td>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr class="no-bottom-border">
                                                        <td class="top">
                                                            <%# Eval("Description") %>
                                                        </td>
                                                        <td class="top">
                                                            <%# Eval("PackageType") %>
                                                        </td>
                                                        <td class="top">
                                                            <%# Eval("ActualWeight") %>
                                                        </td>
                                                        <td class="top text-center">
                                                            <%# Eval("Quantity") %>
                                                        </td>
                                                        <td class="top">
                                                            <%# Eval("Dimensions") %>
                                                        </td>
                                                        <td class="top text-center">
                                                            <%# Eval("IsStackable").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup right">
                                                <label class="blue upper">Total Weight: </label>
                                                <asp:Literal runat="server" ID="litTotalWeight" />
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortShipmentNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortDateCreated" />
                <asp:PostBackTrigger ControlID="lbtnSortDesiredPickupDate" />
                <asp:PostBackTrigger ControlID="lbtnSortCustomerNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortCustomerName" />
                <asp:PostBackTrigger ControlID="lbtnSortVendorNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortVendorName" />
                <asp:PostBackTrigger ControlID="lbtnSortStatus" />
                <asp:PostBackTrigger ControlID="lbtnSortFaxStatus" />
                <asp:PostBackTrigger ControlID="lstShipmentPickupDetails" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <eShip:CustomHiddenField runat="server" ID="hidFailedLockUserId" />

</asp:Content>
