﻿using System.Collections.Generic;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
	public interface IOpsLocationCollectionResource : ILocationCollectionResource
	{
		List<ViewListItem> ServiceCollection { get; }
		List<ViewListItem> EquipmentTypeCollection { get; }
	}
}
