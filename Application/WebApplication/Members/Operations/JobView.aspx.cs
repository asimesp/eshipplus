﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ionic.Zip;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class JobView : MemberPageBaseWithPageStore, IJobView
    {
        private const string DocumentsHeader = "Documents";
        private const string JobComponentsHeader = "Job Components";
        private const string VendorBillsHeader = "Vendor Bill Details";
        private const string InvoicesHeader = "Invoice Details";
        private const string SelectShipmentsVoidHeader = "Void Shipments";
        private const string SelectShipmentsUnvoidHeader = "Unvoid Shipments";
        private const string SelectShipmentsVoidButton = "Void";
        private const string SelectShipmentsUnvoidButton = "Unvoid";


        private const string DownloadShipmentBolArgs = "DSBA";
        private const string VoidArgs = "Void";
        private const string ReverseVoidArgs = "ReverseVoid";

        public const string BillOfLadingPdfFileName = "BOL_{0}.pdf";

        private const string SaveFlag = "S";
        private const string DeleteFlag = "D";
        private const string VoidShipmentFlag = "VS";
        private const string ReverseVoidShipmentFlag = "RVS";


        private bool _moveFilesFromTempLocation;

        public static string PageAddress
        {
            get { return "~/Members/Operations/JobView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.Job; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public event EventHandler<ViewEventArgs<Job>> Delete;
        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Job, List<JobComponentDto>>> Save;
        public event EventHandler<ViewEventArgs<Job>> Lock;
        public event EventHandler<ViewEventArgs<Job>> UnLock;
        public event EventHandler<ViewEventArgs<Job>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;


        public Dictionary<int, string> StatusTypes
        {
            set
            {
                var types = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                types.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, string.Empty));

                ddlJobStatus.DataSource = types;
                ddlJobStatus.DataBind();
            }
        }


        public void Set(Job job, List<JobComponentDto> jobComponentDtos)
        {
            if (job.IsNew && hidFlag.Value == DeleteFlag)
            {
                Server.RemoveDirectory(WebApplicationSettings.ShipmentFolder(ActiveUser.TenantId, hidJobId.Value.ToLong()));
                hidFlag.Value = string.Empty;
            }

            LoadJob(job, jobComponentDtos);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Job>(job));
        }

        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && (hidFlag.Value == SaveFlag))
            {
                // clean up file cleared out!
                if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
                {
                    Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                    hidFilesToDelete.Value = string.Empty;
                }

                var job = new Job(hidJobId.Value.ToLong(), false);

                // check for file moves
                // variable set in save method for new files load when record is new record.
                if (_moveFilesFromTempLocation)
                {
                    _moveFilesFromTempLocation = false; // must set to false first to avoid processing load with next save from ProcessFile... method.
                    ProcessFileMoveFromTempLocation(job);
                    return;
                }

                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Job>(new Job(hidJobId.Value.ToLong(), false)));

                SetEditStatus(false);
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }


        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var downloadBols = new ToolbarMoreAction
            {
                CommandArgs = DownloadShipmentBolArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Admin,
                Name = "Download Shipment BOL(s)",
                IsLink = false,
                NavigationUrl = string.Empty
            };
            var cancel = new ToolbarMoreAction
            {
                CommandArgs = VoidArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = SelectShipmentsVoidHeader,
                IsLink = false,
                NavigationUrl = string.Empty,
                CustomConfirmationMessage = "If you void shipments with service mode small package, you will need to contact the small package carrier to cancel the shipment as the shipment has already been tendered to them."
            };

            var reverseCancel = new ToolbarMoreAction
            {
                CommandArgs = ReverseVoidArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = SelectShipmentsUnvoidHeader,
                IsLink = false,
                NavigationUrl = string.Empty,
                CustomConfirmationMessage = "If you reverse the void shipments with service mode small package, it is your responsibility to recreate the shipment with the small package carrier as well."
            };

            var actions = new List<ToolbarMoreAction>();
            var jobId = hidJobId.Value.ToLong();

            if (jobId != default(long))
            {

                if (ActiveUser.HasAccessTo(ViewCode.Shipment))
                {
                    actions.Add(downloadBols);


                    var shipments = lstJobComponents.Items
                        .Where(c => c.FindControl("litComponentType").ToLiteral().Text.ToEnum<ComponentType>() == ComponentType.Shipment)
                        .Select(control => new Shipment((control.FindControl("hidComponentId")).ToCustomHiddenField().Value.ToLong())).ToList();

                    if (Access.Modify && shipments.Any())
                    {
                        actions.Add(cancel);
                        actions.Add(reverseCancel);
                    }

                }
            }

            memberToolBar.ShowMore = actions.Any();

            return actions;
        }


        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlJobComponents.Enabled = enabled;
            pnlDocuments.Enabled = enabled;

            memberToolBar.ShowUnlock = enabled;

            memberToolBar.EnableSave = enabled;
            memberToolBar.EnableImport = enabled;
            hidEditingIndex.Value = enabled.ToString();
            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
        }

        private void LoadDocumentForEdit(JobDocument document)
        {
            txtDocumentName.Text = document.Name;
            txtDocumentDescription.Text = document.Description;
            ddlDocumentTag.SelectedValue = document.DocumentTagId.GetString();
            litLocationPath.Text = GetLocationFileName(document.LocationPath);
            hidLocationPath.Value = document.LocationPath;
            hiddenDateCreated.Value = document.DateCreated.ToString();
        }

        protected string GetLocationFileName(object relativePath)
        {
            return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
        }


        private void ProcessTransferredRequest(Job job, bool disableEdit = false)
        {
            if (job == null) return;

            if (!ActiveUser.HasAccessToCustomer(job.Customer.Id)) return;

            var jobComponentDtos = new JobSearch().FetchJobComponents(job.Id, ActiveUser.TenantId);

            LoadJob(job, jobComponentDtos);

            if (Lock != null && !job.IsNew && Access.Modify && !disableEdit)
            {
                SetEditStatus(true);
                Lock(this, new ViewEventArgs<Job>(job));
            }
            else SetEditStatus(false);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Job>(job));
        }

        private void ProcessNotifications(Shipment shipment)
        {
            var notifications = hidNotificationsRequired.Value
                .Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Distinct()
                .ToList();
            foreach (var notification in notifications)
                switch (notification.ToEnum<Milestone>())
                {
                    case Milestone.NewShipmentRequest:
                        this.NotifyNewShipment(shipment, true);
                        break;
                    case Milestone.ShipmentInTransit:
                        this.NotifyShipmentInTransit(shipment);
                        break;
                    case Milestone.ShipmentDelivered:
                        this.NotifyShipmentDelivered(shipment);
                        break;
                    case Milestone.ShipmentVoided:
                        this.NotifyCancelledShipment(shipment);
                        break;
                    case Milestone.ShipmentVoidReversed:
                        this.NotifyCancelledShipmentReversed(shipment);
                        break;
                    case Milestone.ShipmentCheckCallUpdate:
                        this.NotifyCheckCallUpdate(shipment, PageStore[DefaultStoreKey] as List<CheckCall>);
                        PageStore[DefaultStoreKey] = null;
                        break;
                }
        }

        private void AddComponent(long componentId, string componentNumber, int jobStep, ComponentType componentType)
        {
            litErrorMessages.Text = string.Empty;

            var jobComponents = lstJobComponents.Items
                                        .Select(control => new JobComponentDto
                                        {
                                            ComponentId = (control.FindControl("hidComponentId")).ToCustomHiddenField().Value.ToLong(),
                                            ComponentType = (control.FindControl("litComponentType")).ToLiteral().Text.ToEnum<ComponentType>(),
                                            ComponentNumber = (control.FindControl("lblJobComponentNumber")).ToLabel().Text,
                                            ComponentJobStep = (control.FindControl("txtComponentJobStep")).ToTextBox().Text.ToInt(),
                                        })
                                        .ToList();

            var jobComponentWithSameNumber = jobComponents.Where(s => s.ComponentNumber == componentNumber).ToList();

            if (jobComponentWithSameNumber.Count > 0)
            {
                if (jobComponentWithSameNumber.Select(s => s.ComponentType).Contains(ComponentType.LoadOrder) && componentType == ComponentType.Shipment)
                {
                    jobComponents.RemoveAll(r => r.ComponentNumber == componentNumber && r.ComponentType == ComponentType.LoadOrder);
                }
                else if (jobComponentWithSameNumber.Any(s => s.ComponentType == componentType || (s.ComponentType == ComponentType.Shipment && componentType == ComponentType.LoadOrder)))
                {
                    DisplayMessages(new[] { ValidationMessage.Error(string.Format("Job Component already added.")) });
                    return;
                }
            }

            jobComponents.Add(new JobComponentDto
            {
                ComponentId = componentId,
                ComponentType = componentType,
                ComponentNumber = componentNumber,
                ComponentJobStep = jobStep

            });

            lstJobComponents.DataSource = jobComponents.OrderBy(o => o.ComponentJobStep).ThenBy(o => o.ComponentType).ThenBy(o => o.ComponentNumber);
            lstJobComponents.DataBind();
            tabJobComponents.HeaderText = jobComponents.BuildTabCount(JobComponentsHeader);
        }

        private void OpenSelectShipmentsToVoid()
        {
            pnlSelectShipments.Visible = true;
            pnlDimScreen.Visible = true;

            btnSelectShipmentsAction.Text = SelectShipmentsVoidButton;
            lblSelectShipmentsPopupTitle.Text = SelectShipmentsVoidHeader;


            var shipments = lstJobComponents.Items
                .Where(c => c.FindControl("litComponentType").ToLiteral().Text.ToEnum<ComponentType>() == ComponentType.Shipment)
                .Select(control => new Shipment((control.FindControl("hidComponentId")).ToCustomHiddenField().Value.ToLong()))
                .Where(s => s.Status != ShipmentStatus.Void).ToList();

            lstShipments.DataSource = shipments;
            lstShipments.DataBind();

            hidFlag.Value = VoidShipmentFlag;
        }

        private void OpenSelectShipmentsToUnvoid()
        {
            pnlSelectShipments.Visible = true;
            pnlDimScreen.Visible = true;

            btnSelectShipmentsAction.Text = SelectShipmentsUnvoidButton;
            lblSelectShipmentsPopupTitle.Text = SelectShipmentsUnvoidHeader;

            var shipments = lstJobComponents.Items
                .Where(c => c.FindControl("litComponentType").ToLiteral().Text.ToEnum<ComponentType>() == ComponentType.Shipment)
                .Select(control => new Shipment((control.FindControl("hidComponentId")).ToCustomHiddenField().Value.ToLong()))
                .Where(s => s.Status == ShipmentStatus.Void).ToList();

            lstShipments.DataSource = shipments;
            lstShipments.DataBind();

            hidFlag.Value = ReverseVoidShipmentFlag;
        }

        private void VoidListOfShipments(List<Shipment> shipments)
        {
            var msgs = new List<ValidationMessage>();
            foreach (var shipment in shipments)
            {
                msgs.AddRange(VoidShipment(shipment));
            }
            msgs.Add(ValidationMessage.Information("Process Complete. Check shipments in job components to confirm."));
            DisplayMessages(msgs);
        }

        private void ReverseVoidForListOfShipments(List<Shipment> shipments)
        {
            var msgs = new List<ValidationMessage>();
            foreach (var shipment in shipments)
            {
                msgs.AddRange(ReverseVoidShipment(shipment));
            }
            msgs.Add(ValidationMessage.Information("Process Complete. Check shipments in job components to confirm."));
            DisplayMessages(msgs);
        }

        private IEnumerable<ValidationMessage> VoidShipment(Shipment shipment)
        {
            var msgs = new List<ValidationMessage>();
            var shipmentView = new ShipmentImmitationView(ActiveUser);

            if (shipment.IsNew)
            {
                return (new[] { ValidationMessage.Error("Unable to cancel new shipment [{0}] that has not been previously saved.", shipment.ShipmentNumber) });
            }

            shipment.LoadCollections();
            shipment.TakeSnapShot();
            shipment.Status = ShipmentStatus.Void;

            shipmentView.RefundOrVoidMiscReceipts = true;

            shipmentView.LockRecord(shipment);
            shipmentView.SaveShipment(shipment);
            shipmentView.UnLockRecord(shipment);

            hidNotificationsRequired.Value += string.Format("{0};", Milestone.ShipmentVoided);

            msgs.AddRange(shipmentView.Messages.ToValidationMessages().Where(v => v.Type != ValidateMessageType.Information));

            // send notifications
            if (!string.IsNullOrEmpty(hidNotificationsRequired.Value))
            {
                ProcessNotifications(shipment);
                hidNotificationsRequired.Value = string.Empty;
            }

            return msgs;
        }

        private IEnumerable<ValidationMessage> ReverseVoidShipment(Shipment shipment)
        {
            var msgs = new List<ValidationMessage>();
            var shipmentView = new ShipmentImmitationView(ActiveUser);

            //            Todo
            if (shipment.IsNew)
            {
                return (new[]
                    {
                        ValidationMessage.Error(
                            "Unable to reverse cancel on new shipment that has not been previously saved and cancelled.")
                    });
            }
            shipment.LoadCollections();
            shipment.TakeSnapShot();
            // account for missing actual pickup date in legacy data.  Cannot have actual delivery date without actual pickup date.
            if ((shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime) && (shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime))
                shipment.ActualPickupDate = shipment.DesiredPickupDate;
            shipment.Status = shipment.InferShipmentStatus(true);

            shipmentView.LockRecord(shipment);
            shipmentView.SaveShipment(shipment);
            shipmentView.UnLockRecord(shipment);

            hidNotificationsRequired.Value += string.Format("{0};", Milestone.ShipmentVoided);

            msgs.AddRange(shipmentView.Messages.ToValidationMessages().Where(v => v.Type != ValidateMessageType.Information));

            // send notifications
            if (!string.IsNullOrEmpty(hidNotificationsRequired.Value))
            {
                ProcessNotifications(shipment);
                hidNotificationsRequired.Value = string.Empty;
            }

            return msgs;
        }

        private void ProcessFileMoveFromTempLocation(Job job)
        {
            var jobFolder = WebApplicationSettings.JobFolder(job.TenantId, job.Id);
            if (job.Documents.Any() && !Directory.Exists(Server.MapPath(jobFolder)))
                Directory.CreateDirectory(Server.MapPath(jobFolder));
            foreach (var doc in job.Documents)
                if (!string.IsNullOrEmpty(doc.LocationPath))
                {
                    var virtualOldPath = doc.LocationPath;
                    var absOldPath = Server.MapPath(virtualOldPath);
                    var oldFileInfo = new FileInfo(absOldPath);
                    var virtualNewPath = jobFolder + oldFileInfo.Name;
                    var absNewPath = Server.MapPath(virtualNewPath);

                    if (absOldPath == absNewPath) continue;

                    File.Copy(absOldPath, absNewPath);

                    doc.TakeSnapShot();
                    doc.LocationPath = virtualNewPath;
                    hidFilesToDelete.Value += string.Format("{0};", virtualOldPath);
                }

            var jobComponentDtos = UpdateJobComponentDtos(job);

            if (Lock != null)
                Lock(this, new ViewEventArgs<Job>(job));

            if (Save != null)
                Save(this, new ViewEventArgs<Job, List<JobComponentDto>>(job, jobComponentDtos));
        }

        private void LoadJob(Job job, IEnumerable<JobComponentDto> jobComponentDtos)
        {
            var isNew = job.IsNew;

            hidJobId.Value = job.Id.ToString();

            txtDateCreated.Text = isNew ? DateTime.Now.FormattedShortDate() : job.DateCreated.FormattedShortDate();
            txtJobNumber.Text = job.JobNumber;
            txtCreatedByUser.Text = job.CreatedByUser != null ? job.CreatedByUser.Username : ActiveUser.Username;
            ddlJobStatus.SelectedValue = job.Status.ToInt().ToString();
            txtExternalReference1.Text = isNew ? string.Empty : job.ExternalReference1;
            txtExternalReference2.Text = isNew ? string.Empty : job.ExternalReference2;

            pnlResolvedAlert.Visible = job.Status == JobStatus.Closed;

            DisplayCustomer(job.Customer);

            //Job Components
            lstJobComponents.DataSource = jobComponentDtos.OrderBy(o => o.ComponentJobStep).ThenBy(o => o.ComponentType).ThenBy(o => o.ComponentNumber);
            lstJobComponents.DataBind();
            tabJobComponents.HeaderText = jobComponentDtos.BuildTabCount(JobComponentsHeader);

            //Documents
            lstDocuments.DataSource = job.Documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = job.Documents.BuildTabCount(DocumentsHeader);


            // Fetch associated vendor bills
            var vendorBillDtos = !job.IsNew ? new JobSearch().FetchAuditVendorBillDtosForJob(job.TenantId, job.Id) : new List<AuditVendorBillDto>();
            lstVendorBillDetails.DataSource = vendorBillDtos.OrderBy(dto => dto.ReferenceType).ThenBy(dto => dto.ReferenceNumber);
            lstVendorBillDetails.DataBind();
            tabVendorBills.HeaderText = vendorBillDtos.BuildTabCount(VendorBillsHeader);

            // Fetch associated invoices
            var invoiceDtos = !job.IsNew ? new JobSearch().FetchAuditInvoiceDtosForJob(job.TenantId, job.Id) : new List<AuditInvoiceDto>();
            lstInvoiceDetails.DataSource = invoiceDtos.OrderBy(dto => dto.ReferenceType).ThenBy(dto => dto.ReferenceNumber);
            lstInvoiceDetails.DataBind();
            tabInvoices.HeaderText = invoiceDtos.BuildTabCount(InvoicesHeader);

            // financial summary
            var jobChargesFinalBuy = new List<decimal>();
            var jobChargesAmountDue = new List<decimal>();
            foreach (var component in jobComponentDtos)
            {
                if (component.ComponentType == ComponentType.ServiceTicket)
                {
                    var serviceTicket = new ServiceTicket(component.ComponentId);
                    jobChargesFinalBuy.AddRange(serviceTicket.Charges.Select(c => c.FinalBuy));
                    jobChargesAmountDue.AddRange(serviceTicket.Charges.Select(c => c.AmountDue));
                }
                if (component.ComponentType == ComponentType.Shipment)
                {
                    var shipment = new Shipment(component.ComponentId);
                    jobChargesFinalBuy.AddRange(shipment.Charges.Select(c => c.FinalBuy));
                    jobChargesAmountDue.AddRange(shipment.Charges.Select(c => c.AmountDue));
                }
            }

            var estimateCost = !jobChargesFinalBuy.Any() ? 0m : jobChargesFinalBuy.Sum(c => c);
            var estimateRevenue = !jobChargesAmountDue.Any() ? 0m : jobChargesAmountDue.Sum(c => c);
            var estimateProfit = estimateRevenue - estimateCost;

            txtEstimateCost.Text = estimateCost.ToString("c2");
            txtEstimateRevenue.Text = estimateRevenue.ToString("c2");
            txtEstimateProfit.Text = estimateProfit.ToString("c2");

            var actualCost = vendorBillDtos
                .Select(i => i.BillType == InvoiceType.Credit ? -1 * i.AmountDue : i.AmountDue)
                .Sum();

            var actualRevenue = invoiceDtos
                .Select(i => i.InvoiceType == InvoiceType.Credit ? -1 * i.AmountDue : i.AmountDue)
                .Sum();

            txtActualCost.Text = actualCost.ToString("c2");
            txtActualRevenue.Text = actualRevenue.ToString("c2");

            var actualProfit = (actualRevenue - actualCost);
            txtActualProfit.Text = actualProfit.ToString("c2");

            txtDiffCost.Text = (actualCost - estimateCost).ToString("c2");
            txtDiffRevenue.Text = (actualRevenue - estimateRevenue).ToString("c2");
            txtDiffProfit.Text = (actualProfit - estimateProfit).ToString("c2");

            txtGPMEstimate.Text = estimateCost == 0 ? default(decimal).ToString("f2") : (estimateProfit / estimateCost * 100).ToString("f2");
            txtGPMActual.Text = actualCost == 0 ? default(decimal).ToString("f2") : (actualProfit / actualCost * 100).ToString("f2");

            var paid = invoiceDtos
                .Where(i => i.InvoiceType != InvoiceType.Credit)
                .GroupBy(si => si.InvoiceNumber)
                .Select(i => new { i.Key, Values = i })
                .Sum(x => x.Values.First().PaidAmount);

            txtTotalOutstanding.Text = actualRevenue == 0 || paid > actualRevenue ? 0.ToString("c2") : (actualRevenue - paid).ToString("c2");

            //Finanace records count
            var invoiceCount = invoiceDtos.Where(c => c.InvoiceType == InvoiceType.Invoice).Select(i => i.InvoiceNumber).Distinct().Count();
            var creditCount = invoiceDtos.Where(c => c.InvoiceType == InvoiceType.Credit).Select(i => i.InvoiceNumber).Distinct().Count();
            var supplementalCount = invoiceDtos.Where(c => c.InvoiceType == InvoiceType.Supplemental).Select(i => i.InvoiceNumber).Distinct().Count();
            var vendorBillCount = vendorBillDtos.Where(c => c.BillType == InvoiceType.Invoice).Select(i => string.Format("{0}.{1}", i.DocumentNumber, i.VendorBillId)).Distinct().Count();
            var creditVendorBillCount = vendorBillDtos.Where(c => c.BillType == InvoiceType.Credit).Select(i => i.DocumentNumber).Distinct().Count();

            txtInvoiceCount.Text = invoiceCount.ToString();
            txtCreditInvoicesCount.Text = creditCount.ToString();
            txtSupplementalInvoicesCount.Text = supplementalCount.ToString();
            txtVendorBillsCount.Text = vendorBillCount.ToString();
            txtCreditVendorBillsCount.Text = creditVendorBillCount.ToString();

            // Job Components count
            var totalServiceTickets = jobComponentDtos.Count(j => j.ComponentType == ComponentType.ServiceTicket);
            var totalShipments = jobComponentDtos.Count(j => j.ComponentType == ComponentType.Shipment);
            var totalLoadOrders = jobComponentDtos.Count(j => j.ComponentType == ComponentType.LoadOrder);

            txtShipmentCount.Text = totalShipments.ToString();
            txtServiceTicketCount.Text = totalServiceTickets.ToString();
            txtLoadOrderCount.Text = totalLoadOrders.ToString();
        }

        private Job UpdateJob()
        {
            var jobId = hidJobId.Value.ToLong();
            var job = new Job(jobId, jobId != default(long));

            if (job.IsNew)
            {
                job.TenantId = ActiveUser.TenantId;
                job.DateCreated = DateTime.Now;
                job.Status = JobStatus.InProgress;
                job.CreatedByUser = ActiveUser;
            }

            job.Customer = string.IsNullOrEmpty(hidCustomerId.Value) ? new Customer() : new Customer(hidCustomerId.Value.ToLong(), hidCustomerId.Value.ToLong() != default(long));
            job.Status = ddlJobStatus.SelectedValue.ToInt().ToEnum<JobStatus>();
            job.ExternalReference1 = txtExternalReference1.Text;
            job.ExternalReference2 = txtExternalReference2.Text;

            //Update From View
            UpdateDocumentsFromView(job);

            return job;
        }

        private void UpdateDocumentsFromView(Job job)
        {
            var documents = lstDocuments.Items
                .Select(i =>
                {
                    var id = (i.FindControl("hidDocumentId")).ToCustomHiddenField().Value.ToLong();
                    return new JobDocument(id, id != default(long))
                    {
                        DocumentTagId = (i.FindControl("hidDocumentTagId")).ToCustomHiddenField().Value.ToLong(),
                        Name = (i.FindControl("litDocumentName")).ToLiteral().Text,
                        Description = (i.FindControl("litDocumentDescription")).ToLiteral().Text,
                        LocationPath = (i.FindControl("hidLocationPath")).ToCustomHiddenField().Value,
                        Job = job,
                        TenantId = job.TenantId,
                        DateCreated = (i.FindControl("hidDateCreated")).ToCustomHiddenField().Value.ToDateTime()
                    };
                })
                .ToList();

            job.Documents = documents;
        }

        private List<JobComponentDto> UpdateJobComponentDtos(Job job)
        {
            var jobComponentDtos = lstJobComponents.Items
                                .Select(control => new JobComponentDto
                                {
                                    ComponentId = (control.FindControl("hidComponentId")).ToCustomHiddenField().Value.ToLong(),
                                    ComponentType = (control.FindControl("litComponentType")).ToLiteral().Text.ToEnum<ComponentType>(),
                                    ComponentNumber = (control.FindControl("lblJobComponentNumber")).ToLabel().Text,
                                    ComponentJobStep = (control.FindControl("txtComponentJobStep")).ToTextBox().Text.ToInt(),
                                    ComponentStatus = (control.FindControl("lblJobComponentStatus")).ToLabel().Text,
                                    TenantId = job.TenantId,
                                    JobId = job.Id,

                                })
                                .ToList();

            return jobComponentDtos;
        }


        private void DownloadShipmentBols()
        {
            var jobComponents = new JobSearch().FetchJobComponents(hidJobId.Value.ToLong(), ActiveUser.TenantId);

            var shipments = jobComponents.Where(j => j.ComponentType == ComponentType.Shipment)
                .Select(j => new Shipment(j.ComponentId)).ToList();

            var content = string.Empty;
            ZipFile zip;
            using (zip = new ZipFile())
            {
                foreach (var shipment in shipments)
                {
                    shipment.LoadCollections();

                    // bol
                    var entryName = string.Format(BillOfLadingPdfFileName, shipment.ShipmentNumber);
                    content = this.GenerateBOL(shipment, true);
                    if (!string.IsNullOrEmpty(content))
                        zip.AddEntry(entryName, content.ToPdf(shipment.ShipmentNumber));

                    // shipment statement

                    foreach (var document in shipment.Documents.Where(d => !d.IsNew && !d.IsInternal))
                    {
                        var fileName = Server.MapPath(document.LocationPath);
                        if (!File.Exists(fileName)) continue;
                        var info = new FileInfo(fileName);
                        var name = string.Format("{0}_{1}{2}", shipment.ShipmentNumber, document.Name, info.Extension);
                        if (!zip.ContainsEntry(name))
                            zip.AddEntry(name, fileName.ReadFromFile());
                    }
                }

                Response.Export(zip.GetZippedBytes(), string.Format("{0:yyyyMMdd_hhmmss}_Document_Package.zip", DateTime.Now));
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new JobHandler(this).Initialize();

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
            if (IsPostBack) return;

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            if (Loading != null)
                Loading(this, new EventArgs());

            SetEditStatus(false);

            if (Session[WebApplicationConstants.TransferJobId] != null)
            {
                var disableEdit = Session[WebApplicationConstants.DisableEditModeOnTransfer].ToBoolean();

                ProcessTransferredRequest(new Job(Session[WebApplicationConstants.TransferJobId].ToLong()), disableEdit);
                Session[WebApplicationConstants.TransferJobId] = null;
                Session[WebApplicationConstants.DisableEditModeOnTransfer] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new JobSearch().FetchJobByJobNumber(Request.QueryString[WebApplicationConstants.TransferNumber].GetString(), ActiveUser.TenantId), true);
        }



        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            var job = new Job
            {
                DateCreated = DateTime.Now,
                Status = JobStatus.InProgress,
                CreatedByUser = ActiveUser,
                ExternalReference1 = string.Empty,
                ExternalReference2 = string.Empty,
                Customer = new Customer()
            };

            LoadJob(job, new List<JobComponentDto>());

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);

            litErrorMessages.Text = string.Empty;
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var job = new Job(hidJobId.Value.ToLong(), false);
            if (Lock == null || job.IsNew || job.Status == JobStatus.Closed) return;

            SetEditStatus(true);

            var jobComponentDtos = new JobSearch().FetchJobComponents(job.Id, ActiveUser.TenantId);

            Lock(this, new ViewEventArgs<Job>(job));
            LoadJob(job, jobComponentDtos);
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var job = UpdateJob();
            var jobComponentDtos = UpdateJobComponentDtos(job);

            hidFlag.Value = SaveFlag;

            _moveFilesFromTempLocation = job.IsNew && job.Documents.Any();

            if (Save != null)
                Save(this, new ViewEventArgs<Job, List<JobComponentDto>>(job, jobComponentDtos));

            if (job.Status == JobStatus.Closed) jobFinder.Reset();
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var job = new Job(hidJobId.Value.ToLong(), false);
            hidFlag.Value = DeleteFlag;

            if (Delete != null)
                Delete(this, new ViewEventArgs<Job>(job));

            SetEditStatus(false);
            jobFinder.Reset();
        }

        protected void OnToolbarUnlockClicked(object sender, EventArgs e)
        {
            var job = new Job(hidJobId.Value.ToLong(), false);
            if (UnLock != null && !job.IsNew)
            {
                SetEditStatus(false);
                UnLock(this, new ViewEventArgs<Job>(job));
            }
        }

        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            jobFinder.Visible = true;
        }

        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case DownloadShipmentBolArgs:
                    DownloadShipmentBols();
                    break;
                case VoidArgs:
                    OpenSelectShipmentsToVoid();
                    break;
                case ReverseVoidArgs:
                    OpenSelectShipmentsToUnvoid();
                    break;
            }
        }


        protected void OnJobFinderItemSelected(object sender, ViewEventArgs<Job> e)
        {
            if (hidJobId.Value.ToLong() != default(long))
            {
                var oldJob = new Job(hidJobId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Job>(oldJob));
            }

            var job = e.Argument;
            var jobComponentDtos = new JobSearch().FetchJobComponents(e.Argument.Id, ActiveUser.TenantId);

            LoadJob(job, jobComponentDtos);

            jobFinder.Visible = false;

            if (jobFinder.OpenForEdit && Lock != null && job.Status != JobStatus.Closed)
            {
                SetEditStatus(true);
                Lock(this, new ViewEventArgs<Job>(job));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Job>(job));

            litErrorMessages.Text = string.Empty;
        }

        protected void OnJobFinderItemCancelled(object sender, EventArgs e)
        {
            jobFinder.Visible = false;
        }




        protected void OnCloseSelectShipmentClicked(object sender, EventArgs eventArgs)
        {
            pnlSelectShipments.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnSelectShipmentsActionButtonClicked(object sender, EventArgs e)
        {
            pnlSelectShipments.Visible = false;
            var shipments = lstShipments.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidShipmentId").ToCustomHiddenField().Value.ToInt(),
                    Selection = i.FindControl("chkSelection").ToAltUniformCheckBox().Checked
                })
                .Where(i => i.Selection)
                .Select(i => new Shipment(i.Id, false))
                .ToList();

            if (hidFlag.Value == VoidShipmentFlag)
            {
                VoidListOfShipments(shipments);
            }
            else if (hidFlag.Value == ReverseVoidShipmentFlag)
            {
                ReverseVoidForListOfShipments(shipments);
            }

            var job = UpdateJob();
            var jobComponentDtos = new JobSearch().FetchJobComponents(job.Id, ActiveUser.TenantId);
            LoadJob(job, jobComponentDtos);

            
            pnlDimScreen.Visible = false;

        }


        protected void OnAddDocumentClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadDocumentForEdit(new JobDocument { DateCreated = DateTime.Now });
            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnClearDocumentsClicked(object sender, EventArgs e)
        {
            foreach (var item in lstDocuments.Items)
                hidFilesToDelete.Value += string.Format("{0};", item.FindControl("hidLocationPath").ToCustomHiddenField().Value);

            lstDocuments.DataSource = new List<object>();
            lstDocuments.DataBind();
            tabDocuments.HeaderText = new List<object>().BuildTabCount(DocumentsHeader);
        }

        protected void OnEditDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            hidEditingIndex.Value = control.FindControl("hidDocumentIndex").ToCustomHiddenField().Value;

            var document = new JobDocument
            {
                Name = control.FindControl("litDocumentName").ToLiteral().Text,
                Description = control.FindControl("litDocumentDescription").ToLiteral().Text,
                DocumentTagId = control.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                LocationPath = control.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                DateCreated = control.FindControl("hidDateCreated").ToCustomHiddenField().Value.ToDateTime()
            };

            LoadDocumentForEdit(document);

            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnDeleteDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var documents = lstDocuments.Items
                .Select(i => new
                {
                    Id = (i.FindControl("hidDocumentId")).ToCustomHiddenField().Value.ToLong(),
                    Name = (i.FindControl("litDocumentName")).ToLiteral().Text,
                    Description = (i.FindControl("litDocumentDescription")).ToLiteral().Text,
                    DocumentTagId = (i.FindControl("hidDocumentTagId")).ToCustomHiddenField().Value,
                    LocationPath = (i.FindControl("hidLocationPath")).ToCustomHiddenField().Value,
                    DateCreated = (i.FindControl("hidDateCreated")).ToCustomHiddenField().Value.ToDateTime()
                })
                .ToList();

            var index = imageButton.Parent.FindControl("hidDocumentIndex").ToCustomHiddenField().Value.ToInt();
            hidFilesToDelete.Value += string.Format("{0};", documents[index].LocationPath);
            documents.RemoveAt((imageButton.Parent.FindControl("hidDocumentIndex")).ToCustomHiddenField().Value.ToInt());

            lstDocuments.DataSource = documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);
        }

        protected void OnCloseEditDocumentClicked(object sender, EventArgs eventArgs)
        {
            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditDocumentDoneClicked(object sender, EventArgs e)
        {
            var virtualPath = WebApplicationSettings.JobFolder(ActiveUser.TenantId, hidJobId.Value.ToLong());
            var physicalPath = Server.MapPath(virtualPath);

            var documents = lstDocuments.Items
                .Select(i => new
                {
                    Id = (i.FindControl("hidDocumentId")).ToCustomHiddenField().Value.ToLong(),
                    Name = (i.FindControl("litDocumentName")).ToLiteral().Text,
                    Description = (i.FindControl("litDocumentDescription")).ToLiteral().Text,
                    DocumentTagId = (i.FindControl("hidDocumentTagId")).ToCustomHiddenField().Value,
                    LocationPath = (i.FindControl("hidLocationPath")).ToCustomHiddenField().Value,
                    DateCreated = (i.FindControl("hidDateCreated")).ToCustomHiddenField().Value.ToDateTime()
                })
                .ToList();

            var documentIndex = hidEditingIndex.Value.ToInt();

            var documentId = default(long);
            if (documentIndex != -1)
            {
                documentId = documents[documentIndex].Id;
                documents.RemoveAt(documentIndex);
            }

            var locationPath = hidLocationPath.Value; // this should actually be a hidden field holding the existing path during edit

            documents.Insert(0, new
            {
                Id = documentId,
                Name = txtDocumentName.Text,
                Description = txtDocumentDescription.Text,
                DocumentTagId = ddlDocumentTag.SelectedValue,
                LocationPath = locationPath,
                DateCreated = hiddenDateCreated.Value.ToDateTime()
            });

            if (fupLocationPath.HasFile)
            {
                // ensure directory is present
                if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

                //ensure unique filename
                var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLocationPath.FileName), true));

                // save file
                fupLocationPath.WriteToFile(physicalPath, fi.Name, Server.MapPath(documents[0].LocationPath));

                // remove updated record
                documents.RemoveAt(0);

                // re-add record with new file
                documents.Insert(0, new
                {
                    Id = documentId,
                    Name = txtDocumentName.Text,
                    Description = txtDocumentDescription.Text,
                    DocumentTagId = ddlDocumentTag.SelectedValue,
                    LocationPath = virtualPath + fi.Name,
                    DateCreated = hiddenDateCreated.Value.ToDateTime()
                });
            }

            lstDocuments.DataSource = documents.OrderBy(d => d.Name);
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);

            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnLocationPathClicked(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
            Response.Export(Server.ReadFromFile(path), button.Text);
        }



        protected void OnAddServiceTicketClicked(object sender, EventArgs e)
        {
            serviceTicketFinder.Visible = true;
        }

        protected void OnServiceTicketFinderMultiItemSelected(object sender, ViewEventArgs<List<ServiceTicket>> e)
        {
            foreach (var serviceTicket in e.Argument)
            {
                AddComponent(serviceTicket.Id, serviceTicket.ServiceTicketNumber, serviceTicket.JobStep, ComponentType.ServiceTicket);
            }
            serviceTicketFinder.Visible = false;
        }

        protected void OnServiceTicketFinderItemCancelled(object sender, EventArgs e)
        {
            serviceTicketFinder.Visible = false;
        }



        protected void OnAddShipmentClicked(object sender, EventArgs e)
        {
            shipmentFinder.Visible = true;
        }

        protected void OnShipmentFinderMultiItemSelected(object sender, ViewEventArgs<List<Shipment>> e)
        {
            foreach (var shipment in e.Argument)
            {
                AddComponent(shipment.Id, shipment.ShipmentNumber, shipment.JobStep, ComponentType.Shipment);
            }
            shipmentFinder.Visible = false;
        }

        protected void OnShipmentFinderItemCancelled(object sender, EventArgs e)
        {
            shipmentFinder.Visible = false;
        }



        protected void OnAddLoadOrderClicked(object sender, EventArgs e)
        {
            loadOrderFinder.Visible = true;
        }

        protected void OnLoadOrderFinderMultiItemSelected(object sender, ViewEventArgs<List<LoadOrder>> e)
        {
            foreach (var loadOrder in e.Argument)
            {
                AddComponent(loadOrder.Id, loadOrder.LoadOrderNumber, loadOrder.JobStep, ComponentType.LoadOrder);
            }
            loadOrderFinder.Visible = false;
        }

        protected void OnLoadOrderFinderItemCancelled(object sender, EventArgs e)
        {
            loadOrderFinder.Visible = false;
        }



        protected void OnDeleteComponentClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var jobComponents = lstJobComponents.Items
                                        .Select(control => new JobComponentDto
                                        {
                                            ComponentId = (control.FindControl("hidComponentId")).ToCustomHiddenField().Value.ToLong(),
                                            ComponentType = (control.FindControl("litComponentType")).ToLiteral().Text.ToEnum<ComponentType>(),
                                            ComponentNumber = (control.FindControl("lblJobComponentNumber")).ToLabel().Text,
                                            ComponentJobStep = (control.FindControl("txtComponentJobStep")).ToTextBox().Text.ToInt(),
                                        })
                                        .ToList();

            jobComponents.RemoveAt((imageButton.Parent.FindControl("hidJobComponentIndex")).ToCustomHiddenField().Value.ToInt());

            lstJobComponents.DataSource = jobComponents;
            lstJobComponents.DataBind();
            tabJobComponents.HeaderText = jobComponents.BuildTabCount(JobComponentsHeader);
        }

        protected void OnResortJobComponentsAccordingToStep(object sender, EventArgs e)
        {
            var jobId = hidJobId.Value.ToLong();
            var job = new Job(jobId, jobId != default(long));

            var jobComponentDtos = lstJobComponents.Items
                                .Select(control => new JobComponentDto
                                {
                                    ComponentId = (control.FindControl("hidComponentId")).ToCustomHiddenField().Value.ToLong(),
                                    ComponentType = (control.FindControl("litComponentType")).ToLiteral().Text.ToEnum<ComponentType>(),
                                    ComponentNumber = (control.FindControl("lblJobComponentNumber")).ToLabel().Text,
                                    ComponentJobStep = (control.FindControl("txtComponentJobStep")).ToTextBox().Text.ToInt(),
                                    TenantId = job.TenantId,
                                    JobId = job.Id,

                                })
                                .ToList();

            lstJobComponents.DataSource = jobComponentDtos.OrderBy(o => o.ComponentJobStep).ThenBy(o => o.ComponentType).ThenBy(o => o.ComponentNumber);
            lstJobComponents.DataBind();
        }



        protected void OnClearAllComponentsClicked(object sender, EventArgs e)
        {
            lstJobComponents.DataSource = new List<JobComponentDto>();
            lstJobComponents.DataBind();
            tabJobComponents.HeaderText = JobComponentsHeader;
        }




        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }



    }
}