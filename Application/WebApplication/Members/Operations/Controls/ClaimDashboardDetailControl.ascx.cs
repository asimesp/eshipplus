﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class ClaimDashboardDetailControl : MemberControlBase
    {
        public int ItemIndex
        {
            get { return hidItemIndex.Value.ToInt(); }
            set { hidItemIndex.Value = value.ToString(); }
        }


        public void LoadClaim(ClaimViewSearchDto claim)
        {
            hidClaimId.Value = claim.Id.ToString();

            litClaimNumber.Text = claim.ClaimNumber;
            litDateCreated.Text = claim.DateCreated.FormattedShortDate();
            
            var hasAccessToCustomer = ActiveUser.HasAccessTo(ViewCode.Customer);
            litCustomer.Text = string.Format("{0} - {1}", claim.CustomerNumber, claim.CustomerName);
            hypCustomer.Visible = hasAccessToCustomer;
            hypCustomer.NavigateUrl = string.Format("{0}?{1}={2}", CustomerView.PageAddress, WebApplicationConstants.TransferNumber, claim.CustomerId.GetString().UrlTextEncrypt());

            var hasAccessToUser = ActiveUser.HasAccessTo(ViewCode.User);
            litUser.Text = claim.Username;
            hypUser.Visible = hasAccessToUser;
            hypUser.NavigateUrl = string.Format("{0}?{1}={2}", UserView.PageAddress, WebApplicationConstants.TransferNumber, claim.UserId.GetString().UrlTextEncrypt());
            
            litStatus.Text = claim.Status.FormattedString();

            litDetails.Text = claim.ClaimDetail;

            var httpAddr = Request.ResolveSiteRootWithHttp();
            litShipments.Text = ActiveUser.HasAccessTo(ViewCode.Shipment)
                                    ? ShipmentView.BuildShipmentLinks(claim.Shipments).Replace("~", httpAddr)
                                    : claim.Shipments;
            litServiceTickets.Text = ActiveUser.HasAccessTo(ViewCode.ServiceTicket)
                                         ? ServiceTicketView.BuildServiceTicketLinks(claim.ServiceTickets).Replace("~", httpAddr)
                                         : claim.ServiceTickets;

            litVendorNames.Text = claim.VendorNames;

            hypClaimView.NavigateUrl = string.Format("{0}?{1}={2}", ClaimView.PageAddress, WebApplicationConstants.TransferNumber, claim.ClaimNumber);
        }
    }
}