﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;


namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class AddressBookFinderControl : MemberControlBase, IAddressBookFinderView
    {
        public int ItemIndex
        {
            get { return hidLocationItemIndex.Value.ToInt(); }
            set { hidLocationItemIndex.Value = value.ToString(); }
        }

        public new bool Visible
        {
            get { return pnlAddressBookFinderContent.Visible; }
            set
            {
                base.Visible = value;
                pnlAddressBookFinderContent.Visible = value;
                pnlAddressBookFinderDimScreen.Visible = value;
            }
        }

        public bool OpenForEdit
        {
            get { return hidEditSelected.Value.ToBoolean(); }
        }

        public bool OpenForEditEnabled
        {
            get { return hidOpenForEditEnabled.Value.ToBoolean(); }
            set { hidOpenForEditEnabled.Value = value.ToString(); }
        }

        public long CustomerIdSpecificFilter
        {
            set { hidCustomerIdSpecificFilter.Value = value.ToString(); }
        }

        protected bool CanDelete { get; set; }


        public event EventHandler<ViewEventArgs<AddressBookViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<List<AddressBook>>> DeleteSelected;

        public event EventHandler<ViewEventArgs<AddressBook>> ItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<AddressBookViewSearchDto> addressBooks)
        {
            litRecordCount.Text = addressBooks.BuildRecordCount();
            upcseDataUpdate.DataSource = addressBooks;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void ProcessSuccessfulDelete()
        {
            DoSearchPreProcessingThenSearch();
        }


        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new AddressBookViewSearchCriteria
                {
                    UserId = ActiveUser.Id,
                    CustomerId = hidCustomerIdSpecificFilter.Value.ToLong(),
                    Parameters = GetCurrentRunParameters(false)
                });
        }

        private void DoSearch(AddressBookViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<AddressBookViewSearchCriteria>(criteria));

            var control = lstAddressBooks.FindControl("chkSelectAllRecords") as CheckBox;
            if (control != null)
            {
                control.Checked = false;
                control.Visible = CanDelete;
            }
        }


        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString();
            DisplaySearchResult(new List<AddressBookViewSearchDto>());
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        public void SearchOn(string criteria)
        {
            var parameters = OperationsSearchFields.DefaultAddressBooks.ToParameterColumn();
            foreach (var parameterColumn in parameters.Where(parameterColumn => parameterColumn.ReportColumnName == OperationsSearchFields.Description.DisplayName))
                parameterColumn.DefaultValue = criteria;

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch();
        }


        private void ProcessItemSelection(long addressBookId)
        {
            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<AddressBook>(new AddressBook(addressBookId, false)));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new AddressBookFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            CanDelete = ActiveUser.RetrievePermission(ViewCode.AddressBook).Remove;
            btnDeleteSelected.Visible = CanDelete;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : OperationsSearchFields.DefaultAddressBooks.Select(f => f.ToParameterColumn()).ToList();

            //remove customer columns if filtering as it will be set prior to search
            var customerColumns = new List<ParameterColumn>();
            if (hidCustomerIdSpecificFilter.Value.ToLong() != default(long))
            {
                customerColumns.AddRange(columns.Where(c => c.ReportColumnName == OperationsSearchFields.CustomerName.ToParameterColumn().ReportColumnName));
                customerColumns.AddRange(columns.Where(c => c.ReportColumnName == OperationsSearchFields.CustomerNumber.ToParameterColumn().ReportColumnName));
            }
            foreach (var column in customerColumns)
                columns.Remove(column);

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch();
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = false.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidAddressBookId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnDeleteSelectedClicked(object sender, EventArgs e)
        {
            var addressBooks = (from item in lstAddressBooks.Items
                                let hidden = item.FindControl("hidAddressBookId").ToCustomHiddenField()
                                let checkBox = item.FindControl("chkMarkForDelete").ToCheckBox()
                                where checkBox != null && checkBox.Checked
                                select new AddressBook(hidden.Value.ToLong()))
             .Where(u => u.KeyLoaded)
             .ToList();

            if (DeleteSelected != null)
                DeleteSelected(this, new ViewEventArgs<List<AddressBook>>(addressBooks));
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.AddressBooks.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(hidCustomerIdSpecificFilter.Value.ToLong() != default(long)
                                                        ? OperationsSearchFields.AddressBooks
                                                                                .Where(f => f.Name != OperationsSearchFields.CustomerName.Name
                                                                                    && f.Name != OperationsSearchFields.CustomerNumber.Name)
                                                                                .ToList()
                                                        : OperationsSearchFields.AddressBooks
                                                                                .ToList());
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }

        protected void OnEditSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = true.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidAddressBookId").ToCustomHiddenField().Value.ToLong()); 
        }


        protected void OnAddressBooksItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var data = item.DataItem as AddressBookViewSearchDto;
            var control = item.FindControl("lstContactInfo").ToListView();

            if (data == null || control == null) return;

            control.DataSource = data.Contacts;
            control.DataBind();

            var lnkContactDisplay = item.FindControl("lnkContactDisplay").ToLinkButton();
            lnkContactDisplay.OnClientClick = string.Format("ToggleContactsVisibility({0}); return false;", e.Item.DataItemIndex);
        }
    }
}