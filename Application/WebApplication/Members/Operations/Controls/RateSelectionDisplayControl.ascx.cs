﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Calculations;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class RateSelectionDisplayControl : MemberControlBase
    {
        private const string Unknown = "Unknown";
        private const string FreightFlag = "F";
        private const string EstimatedDateFlag = "E";

        private const string PcfErrMsg = "One or more item(s) listed has a pounds per cubic feet outside the allowable limits of {0} - {1}. Please verify item(s) weight and dimension input";




        protected bool ItemsPackageInValid
        {
            get { return hidItemsPackageInValid.Value.ToBoolean(); }
            set { hidItemsPackageInValid.Value = value.ToString(); }
        }

        protected bool ItemsLTLFreightClassInValid
        {
            get { return hidItemsLTLFreightClassInValid.Value.ToBoolean(); }
            set { hidItemsLTLFreightClassInValid.Value = value.ToString(); }
        }

        protected bool ItemsLTLDensityInValid
        {
            get { return hidItemsLTLDensityInValid.Value.ToBoolean(); }
            set { hidItemsLTLDensityInValid.Value = value.ToString(); }
        }

        protected bool ItemsFTLInValid
        {
            get { return hidItemsFTLInValid.Value.ToBoolean(); }
            set { hidItemsFTLInValid.Value = value.ToString(); }
        }

        protected bool PcfDataErr;

        public new bool Visible
        {
            get { return pnlRateSelection.Visible; }
            set
            {
                base.Visible = value;
                pnlRateSelection.Visible = value;
                pnlRateSelectionDisplayDimScreen.Visible = value;
            }
        }

        public bool EnableEnterMissingInformationButton
        {
            set { btnClose.Visible = value; }
        }

        public bool EnableHightlightMissingInformationButtons
        {
            get { return hidEnableHightlightMissingInformationButtons.Value.ToBoolean(); }
            set { hidEnableHightlightMissingInformationButtons.Value = value.GetString(); }
        }

        public bool EnableRateDownload
        {
            set { pnlRateDownload.Visible = value; }
        }

        public bool DisableRateSelection
        {
            get { return hidDisableRateSelection.Value.ToBoolean(); }
            set { hidDisableRateSelection.Value = value.ToString(); }
        }

        public event EventHandler<ViewEventArgs<Rate>> RateSelected;
        public event EventHandler<ViewEventArgs<bool, List<Rate>>> DownloadRates;
        public event EventHandler<ViewEventArgs<ShoppedRate>> RatesLoaded;
        public event EventHandler<ViewEventArgs<RateSelectionFieldset>> RateSelectionCancelled;


        public void Rate(Shipment shipment)
        {
            try
            {
                pnlError.Visible = false;

                var pmaps = ProcessorVars.RegistryCache[ActiveUser.TenantId].SmallPackagingMaps;
                var smaps = ProcessorVars.RegistryCache[ActiveUser.TenantId].SmallPackageServiceMaps;
                var variables = ProcessorVars.IntegrationVariables[ActiveUser.TenantId];
                var rater = new Rater2(variables.RatewareParameters, variables.CarrierConnectParameters,
                                      variables.FedExParameters, variables.UpsParameters, pmaps, smaps, variables.Project44Settings);
                var rates = rater.GetRates(shipment);

                SetItemsValidity(shipment);

                hidOriginCountryId.Value = shipment.Origin.CountryId.GetString();
                hidDestinationCountryId.Value = shipment.Destination.CountryId.GetString();
                hidShipmentDate.Value = shipment.DesiredPickupDate.FormattedLongDate();
                hidOriginPostalCode.Value = shipment.Origin.PostalCode.GetString();
                hidDestinationPostalCode.Value = shipment.Destination.PostalCode.GetString();
                hidHighestFreightClass.Value = shipment.Items.Any() ? shipment.Items.Max(i => i.ActualFreightClass).ToString() : "0";
                hidTotalWeight.Value = shipment.Items.TotalActualWeight().ToString();


                lstRates.DataSource = rates
                    .OrderBy(r => r.BaseRateCharges.Sum(c => c.AmountDue))
                    .ThenBy(r => r.EstimatedDeliveryDate);
                lstRates.DataBind();

                pnlNoRates.Visible = !rates.Any();
                pnlCertifications.Visible = rates.Any();

                btnCloseRateSelection.Visible = !rates.Any();

                if (rates.Any()) SetCustomVendorSelectionMessage(shipment.Customer);

                var validForShoppedRate = shipment.Customer != null && shipment.Origin.CountryId != default(long) && shipment.Destination.CountryId != default(long);
                if (validForShoppedRate && RatesLoaded != null)
                {
                    // NOTE: already sort in ascending order by total charge above.
                    var rate = rates.Any() ? rates.OrderBy(r => r.BaseRateCharges.Sum(c => c.AmountDue)).First() : null;
                    RatesLoaded(this, new ViewEventArgs<ShoppedRate>(GetShoppedRate(shipment, rate)));
                }
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, Context);
                pnlNoRates.Visible = false;
                pnlError.Visible = true;
            }
        }


        private void SetCustomVendorSelectionMessage(Customer customer)
        {
            if (customer == null) return;
            pnlCustomerCustomVendorMessage.Visible = !string.IsNullOrEmpty(customer.CustomVendorSelectionMessage);
            litCustomerCustomVendorMessage.Text = customer.CustomVendorSelectionMessage;
        }

        private void SetItemsValidity(Shipment shipment)
        {


            var err = shipment.ItemsInvalidForPackageRating();
            ItemsPackageInValid = !string.IsNullOrEmpty(err);
            litPackage.Visible = ItemsPackageInValid;
            litPackage.Text = err;
            btnHighlightSmallPackFields.Visible = ItemsPackageInValid && EnableHightlightMissingInformationButtons;

            err = shipment.ItemsInvalidForLTLFreightClassRating();
            ItemsLTLFreightClassInValid = !string.IsNullOrEmpty(err);
            litLTLFreightClass.Visible = ItemsLTLFreightClassInValid;
            litLTLFreightClass.Text = err;
            btnHighlightLTLFreightClassFields.Visible = ItemsLTLFreightClassInValid && EnableHightlightMissingInformationButtons;

            err = shipment.ItemsInvalidForLTLDensityRating();
            ItemsLTLDensityInValid = !string.IsNullOrEmpty(err);
            litLTLDensity.Visible = ItemsLTLDensityInValid;
            litLTLDensity.Text = err;
            btnHighlightLTLDensityFields.Visible = ItemsLTLDensityInValid && EnableHightlightMissingInformationButtons;

            err = shipment.ItemsInvalidForFTLRating();
            ItemsFTLInValid = !string.IsNullOrEmpty(err);
            litFTL.Visible = ItemsFTLInValid;
            litFTL.Text = err;
            btnHighlightFTLFields.Visible = ItemsFTLInValid && EnableHightlightMissingInformationButtons;

            err = string.Format(PcfErrMsg, CubicFootCalculations.MinPcf, CubicFootCalculations.MaxPcf);
            PcfDataErr = shipment.Items.Any(i => i.IsOutsideAllowablePcfActual());
            litPcfError.Visible = PcfDataErr;
            litPcfError.Text = err;

            pnlMissingOrBadInput.Visible = ItemsFTLInValid || ItemsLTLFreightClassInValid || ItemsPackageInValid || ItemsLTLDensityInValid || PcfDataErr;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            chkShowVendorRatingActualName.Visible = ActiveUser.TenantEmployee;
        }

        protected void OnCancelClicked(object sender, EventArgs e)
        {
            var btn = sender as Button;

            var fieldSetToHighlight = RateSelectionFieldset.NotApplicable;

            if (btn != null)
            {
                if (btn.ID == btnHighlightLTLFreightClassFields.ID) fieldSetToHighlight = RateSelectionFieldset.LtlFreightClass;
                if (btn.ID == btnHighlightLTLDensityFields.ID) fieldSetToHighlight = RateSelectionFieldset.LtlDensity;
                if (btn.ID == btnHighlightSmallPackFields.ID) fieldSetToHighlight = RateSelectionFieldset.SmallPackage;
                if (btn.ID == btnHighlightFTLFields.ID) fieldSetToHighlight = RateSelectionFieldset.Ftl;
            }

            if (RateSelectionCancelled != null)
                RateSelectionCancelled(this, new ViewEventArgs<RateSelectionFieldset>(fieldSetToHighlight));
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var rate = new Rate();
            var control = ((Button)sender).Parent;

            // retrieve rate
            rate.Mode = control.FindControl("hidServiceMode").ToCustomHiddenField().Value.ToEnum<ServiceMode>();
            rate.IsLTLPackageSpecificRate = control.FindControl("hidLTLPackageSpecificRate").ToCustomHiddenField().Value.ToBoolean();
            rate.Vendor = new Vendor(control.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong());
            rate.LTLSellRate = new LTLSellRate(control.FindControl("hidLTLSellRateId").ToCustomHiddenField().Value.ToLong());
            rate.DiscountTier = new DiscountTier(control.FindControl("hidDiscountTierId").ToCustomHiddenField().Value.ToLong());
            rate.CFCRule = new LTLCubicFootCapacityRule(control.FindControl("hidCFCRuleId").ToCustomHiddenField().Value.ToLong());
            rate.SmallPackServiceType = control.FindControl("hidSmallPackType").ToCustomHiddenField().Value;
            rate.SmallPackEngine = control.FindControl("hidSmallPackageEngine").ToCustomHiddenField().Value.ToEnum<SmallPackageEngine>();
            rate.OriginalRateValue = control.FindControl("hidOriginalRateValue").ToCustomHiddenField().Value.ToDecimal();
            rate.DirectPointRate = control.FindControl("hidDirectPointRate").ToCustomHiddenField().Value.ToBoolean();
            rate.ApplyDiscount = control.FindControl("hidApplyDiscount").ToCustomHiddenField().Value.ToBoolean();
            rate.BilledWeight = control.FindControl("hidBilledWeight").ToCustomHiddenField().Value.ToDecimal();
            rate.RatedCubicFeet = control.FindControl("hidRatedCubicFeet").ToCustomHiddenField().Value.ToDecimal();
            rate.RatedWeight = control.FindControl("hidRatedWeight").ToCustomHiddenField().Value.ToDecimal();
            rate.FuelMarkupPercent = control.FindControl("hidFuelMarkupPercent").ToCustomHiddenField().Value.ToDecimal();
            rate.FreightProfitAdjustment = control.FindControl("hidFreightProfitAdjustment").ToCustomHiddenField().Value.ToDecimal();
            rate.FuelProfitAdjustment = control.FindControl("hidFuelProfitAdjustment").ToCustomHiddenField().Value.ToDecimal();
            rate.AccessorialProfitAdjustment = control.FindControl("hidAccessorialProfitAdjustment").ToCustomHiddenField().Value.ToDecimal();
            rate.ServiceProfitAdjustment = control.FindControl("hidServiceProfitAdjustment").ToCustomHiddenField().Value.ToDecimal();
            rate.ResellerFreightMarkup = control.FindControl("hidResellerFreightMarkup").ToCustomHiddenField().Value.ToDecimal();
            rate.ResellerFreightMarkupIsPercent = control.FindControl("hidResellerFreightMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean();
            rate.ResellerFuelMarkup = control.FindControl("hidResellerFuelMarkup").ToCustomHiddenField().Value.ToDecimal();
            rate.ResellerFuelMarkupIsPercent = control.FindControl("hidResellerFuelMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean();
            rate.ResellerAccessorialMarkup = control.FindControl("hidResellerAccessorialMarkup").ToCustomHiddenField().Value.ToDecimal();
            rate.ResellerAccessorialMarkupIsPercent = control.FindControl("hidResellerAccessorialMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean();
            rate.ResellerServiceMarkup = control.FindControl("hidResellerServiceMarkup").ToCustomHiddenField().Value.ToDecimal();
            rate.ResellerServiceMarkupIsPercent = control.FindControl("hidResellerServiceMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean();
            rate.Project44QuoteNumber = control.FindControl("hidProject44QuoteNumber").ToCustomHiddenField().Value;
            rate.IsProject44Rate = control.FindControl("hidIsProject44Rate").ToCustomHiddenField().Value.ToBoolean();


            rate.Mileage = control.FindControl("hidMileage").ToCustomHiddenField().Value.ToDecimal();
            rate.MileageSourceId = control.FindControl("hidMileageSourceId").ToCustomHiddenField().Value.ToLong();

            rate.OriginTerminalCode = control.FindControl("hidOriginTerminalCode").ToCustomHiddenField().Value;
            rate.DestinationTerminalCode = control.FindControl("hidDestinationTerminalCode").ToCustomHiddenField().Value;

            rate.TotalActualShipmentWeight = control.FindControl("hidTotalActualShipmentWeight").ToCustomHiddenField().Value.ToDecimal();
            rate.MarkupPercent = control.FindControl("hidMarkupPercent").ToCustomHiddenField().Value.ToDecimal();
            rate.MarkupValue = control.FindControl("hidMarkupValue").ToCustomHiddenField().Value.ToDecimal();

            rate.NoFreightProfit = rate.FreightProfitAdjustment == 0;
            rate.NoFuelProfit = rate.FuelProfitAdjustment == 0;
            rate.NoAccessorialProfit = rate.AccessorialProfitAdjustment == 0;
            rate.NoServiceProfit = rate.ServiceProfitAdjustment == 0;


            var litEstimatedDeliveryDate = control.FindControl("litEstimatedDeliveryDate").ToLiteral();
            var litTransitTime = control.FindControl("litTransitTime").ToLiteral();
            rate.TransitDays = litTransitTime.Text == Unknown ? Core.Rating.Rate.DefaultTransitTime : litTransitTime.Text.ToInt();
            rate.EstimatedDeliveryDate = litEstimatedDeliveryDate.Text == Unknown
                                             ? DateUtility.SystemEarliestDateTime
                                             : litEstimatedDeliveryDate.Text.ToDateTime();

            var lstGuaranteedServicesItem = control.FindControl("lstGuaranteedServices").ToListView().Items.FirstOrDefault(i => i.FindControl("chkGuaranteedServiceEnabled").ToAltUniformCheckBox().Checked);
            var lstExpeditedServices = control.FindControl("lstExpeditedServices").ToListView().Items.FirstOrDefault(i => i.FindControl("chkExpeditedServiceEnabled").ToAltUniformCheckBox().Checked);

            var gChargeId = lstGuaranteedServicesItem != null
                ? lstGuaranteedServicesItem.FindControl("hidLTLGuaranteedChargeId").ToCustomHiddenField().Value.ToLong()
                : default(long);

            var guaranteedP44ChargeCode = lstGuaranteedServicesItem != null
                ? lstGuaranteedServicesItem.FindControl("hidProject44ChargeCode").ToCustomHiddenField().Value
                : string.Empty;

            var expeditedP44ChargeCode = lstExpeditedServices != null
                ? lstGuaranteedServicesItem.FindControl("hidProject44ChargeCode").ToCustomHiddenField().Value
                : string.Empty;


            var lstCharges = control.FindControl("lstCharges").ToListView();
            rate.Charges.AddRange(
                lstCharges.Items
                    .Select(i => new RateCharge
                    {
                        ChargeCodeId = i.FindControl("hidChargeCodeId").ToCustomHiddenField().Value.ToLong(),
                        UnitBuy = i.FindControl("hidUnitBuy").ToCustomHiddenField().Value.ToDecimal(),
                        UnitSell = i.FindControl("hidUnitSell").ToCustomHiddenField().Value.ToDecimal(),
                        UnitDiscount = i.FindControl("hidUnitDiscount").ToCustomHiddenField().Value.ToDecimal(),
                        Quantity = i.FindControl("litQuantity").ToLiteral().Text.ToInt(),
                        Comment = i.FindControl("hidChargeCodeComment").ToCustomHiddenField().Value,
                        LTLGuaranteedChargeId = i.FindControl("hidLTLGuaranteedChargeId").ToCustomHiddenField().Value.ToLong(),
                        IsProject44GuaranteedCharge = i.FindControl("hidIsProject44GuaranteedCharge").ToCustomHiddenField().Value.ToBoolean(),
                        IsProject44ExpeditedServiceCharge = i.FindControl("hidIsProject44ExpeditedServiceCharge").ToCustomHiddenField().Value.ToBoolean()
                    })
                    .Where(c => (c.LTLGuaranteedChargeId == default(long) && !c.IsProject44GuaranteedCharge && !c.IsProject44ExpeditedServiceCharge
                                 || (c.LTLGuaranteedChargeId != default(long) && c.LTLGuaranteedChargeId == gChargeId)) 
                                 || (c.ChargeCode.Project44Code.ToString() == guaranteedP44ChargeCode)
                                 || (c.ChargeCode.Project44Code.ToString() == expeditedP44ChargeCode)));


            // P44 Guaranteed Deliery Date is stored in Charge comment
            var gCharge = rate.Charges.FirstOrDefault(c => c.IsProject44GuaranteedCharge && c.Comment != string.Empty);
            if (gCharge != null)
            {
                rate.EstimatedDeliveryDate = gCharge.Comment.ToDateTime();
                rate.Project44QuoteNumber = gCharge.Project44QuoteNumber;
            }

            // P44 Expedited Delivery Date is stored in Charge comment
            var expeditedCharge = rate.Charges.FirstOrDefault(c => c.IsProject44ExpeditedServiceCharge && c.Comment != string.Empty);
            if (expeditedCharge != null)
            {
                rate.EstimatedDeliveryDate = expeditedCharge.Comment.ToDateTime();
                rate.Project44QuoteNumber = expeditedCharge.Project44QuoteNumber;
            }

            if (RateSelected != null)
                RateSelected(this, new ViewEventArgs<Rate>(rate));
        }


        protected string GetServiceModeDescription(object eval)
        {
            if (eval == null) return string.Empty;
            switch (eval.GetString().ToEnum<ServiceMode>())
            {
                case ServiceMode.Truckload:
                    return "FTL";
                case ServiceMode.SmallPackage:
                    return "Package";
                default:
                    return "LTL";
            }
        }

        protected bool MissingInputForMode(object eval, object densityLTL)
        {
            if (PcfDataErr) return true;

            switch (eval.GetString().ToEnum<ServiceMode>())
            {
                case ServiceMode.Truckload:
                    return ItemsFTLInValid;
                case ServiceMode.SmallPackage:
                    return ItemsPackageInValid;
                default:
                    return densityLTL.ToBoolean() ? ItemsLTLDensityInValid : ItemsLTLFreightClassInValid;
            }
        }

        protected void OnRatesItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var rate = (Rate)item.DataItem;

            var imgVendorLogo = item.FindControl("imgVendorLogo").ToImageButton();
            var pnlVendorLogo = item.FindControl("pnlVendorLogo").ToPanel();
            var lnkVendor = item.FindControl("lnkVendor").ToLinkButton();
            var litVendor = item.FindControl("litVendor").ToLiteral();
            var hidVendorId = item.FindControl("hidVendorId").ToCustomHiddenField();
            var hidVendorScac = item.FindControl("hidVendorScac").ToCustomHiddenField();
            var imgTsaCertified = item.FindControl("imgTsaCertified");
            var imgCtpatCertified = item.FindControl("imgCtpatCertified");
            var imgSmartWayCertified = item.FindControl("imgSmartWayCertified");
            var imgPipCertified = item.FindControl("imgPIPCertified");

            switch (rate.Mode)
            {
                case ServiceMode.LessThanTruckload:
		            var vendorDisplayName = ActiveUser.TenantEmployee
			                                    ? rate.LTLSellRate.VendorRating.Name
			                                    : rate.LTLSellRate.VendorRating.DisplayName;
		            lnkVendor.Text = vendorDisplayName;
                    lnkVendor.Visible = string.IsNullOrEmpty(rate.LTLSellRate.VendorRating.Vendor.LogoUrl);
                    imgVendorLogo.ImageUrl = rate.LTLSellRate.VendorRating.Vendor.LogoUrl;
                    imgVendorLogo.ToolTip = vendorDisplayName;
                    litVendor.Text = vendorDisplayName;
                    pnlVendorLogo.Visible = !string.IsNullOrEmpty(rate.LTLSellRate.VendorRating.Vendor.LogoUrl);
                    lnkVendor.Enabled = true;
                    hidVendorId.Value = rate.LTLSellRate.VendorRating.Vendor.Id.ToString();
                    hidVendorScac.Value = rate.LTLSellRate.VendorRating.Vendor.Scac;
                    imgTsaCertified.Visible = rate.LTLSellRate.VendorRating.Vendor.TSACertified;
                    imgCtpatCertified.Visible = rate.LTLSellRate.VendorRating.Vendor.CTPATCertified;
                    imgSmartWayCertified.Visible = rate.LTLSellRate.VendorRating.Vendor.SmartWayCertified;
                    imgPipCertified.Visible = rate.LTLSellRate.VendorRating.Vendor.PIPCertified;
                    break;
                case ServiceMode.SmallPackage:
                    lnkVendor.Enabled = false;
                    lnkVendor.Text = string.Format("{0} - {1}", rate.Vendor.Name, rate.SmallPackServiceType);
                    lnkVendor.CssClass = "link_nounderline";
                    lnkVendor.Visible = string.IsNullOrEmpty(rate.Vendor.LogoUrl);
                    imgVendorLogo.Enabled = false;
                    imgVendorLogo.ImageUrl = rate.Vendor.LogoUrl;
                    imgVendorLogo.ToolTip = string.Format("{0} - {1}", rate.Vendor.Name, rate.SmallPackServiceType);
                    litVendor.Text = string.Format("{0} - {1}", rate.Vendor.Name, rate.SmallPackServiceType);
                    pnlVendorLogo.Visible = !string.IsNullOrEmpty(rate.Vendor.LogoUrl);
                    hidVendorId.Value = rate.Vendor.Id.ToString();
                    hidVendorScac.Value = rate.Vendor.Scac;
                    imgTsaCertified.Visible = rate.Vendor.TSACertified;
                    imgCtpatCertified.Visible = rate.Vendor.CTPATCertified;
                    imgSmartWayCertified.Visible = rate.Vendor.SmartWayCertified;
                    imgPipCertified.Visible = rate.Vendor.PIPCertified;
                    break;
                default:
                    lnkVendor.Text = "Contract Rate";
                    lnkVendor.Enabled = false;
                    lnkVendor.CssClass = "link_nounderline";
                    imgVendorLogo.ImageUrl = string.Empty;
                    pnlVendorLogo.Visible = false;
                    hidVendorId.Value = string.Empty;
                    hidVendorScac.Value = string.Empty;
                    imgTsaCertified.Visible = false;
                    imgCtpatCertified.Visible = false;
                    imgSmartWayCertified.Visible = false;
                    imgPipCertified.Visible = false;
                    break;
            }

            var charges = rate.BaseRateCharges;


            item.FindControl("litTotalAccessorialCharge").ToLiteral().Text = charges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Accessorial)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalFreightCharge").ToLiteral().Text = charges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalFuelCharge").ToLiteral().Text = charges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Fuel)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalServiceCharge").ToLiteral().Text = charges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Service)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalOveralCharge").ToLiteral().Text = charges
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litEstimatedDeliveryDate").ToLiteral().Text = rate.TransitDays == Core.Rating.Rate.DefaultTransitTime
                                                                    ? Unknown
                                                                    : rate.EstimatedDeliveryDate.FormattedShortDate();
            item.FindControl("litTransitTime").ToLiteral().Text = rate.TransitDays == Core.Rating.Rate.DefaultTransitTime
                                                                           ? Unknown
                                                                           : rate.TransitDays.GetString();

            item.FindControl("litVendorRatingRatingDetailNote").ToLiteral().Text = rate.LTLSellRate != null ? rate.LTLSellRate.VendorRating.RatingDetailNotes.ReplaceNewLineWithHtmlBreak() : string.Empty;

            item.FindControl("litProject44QuoteNumber").ToLiteral().Text = rate.Project44QuoteNumber;

            var lstCharges = item.FindControl("lstCharges").ToListView();
            lstCharges.DataSource = rate.Charges.Select(c => new
            {
                c.ChargeCodeId,
                c.UnitBuy,
                c.UnitSell,
                c.UnitDiscount,
                c.ChargeCode.Code,
                c.ChargeCode.Description,
                Category = c.ChargeCode.Category.FormattedString(),
                c.Quantity,
                c.AmountDue,
                c.FinalBuy,
                c.Comment,
                rate.Mode,
                c.LTLGuaranteedChargeId,
                Visible = c.LTLGuaranteedChargeId == default(long) && !c.IsProject44GuaranteedCharge && !c.IsProject44ExpeditedServiceCharge,
                c.IsProject44GuaranteedCharge,
                c.IsProject44ExpeditedServiceCharge,
                c.Project44QuoteNumber
            });
            lstCharges.DataBind();

            if (rate.IsProject44Rate)
            {
                var lstGuaranteedServices = item.FindControl("lstGuaranteedServices").ToListView();
                lstGuaranteedServices.DataSource = rate.GuaranteedCharges
                    .OrderBy(c => c.ChargeCode.Project44Code)
                    .Select(c => new
                    {
                        Id = default(long),
                        Description = Project44Constants.GetGuaranteedServiceTime(c.ChargeCode.Project44Code),
                        Project44ChargeCode = c.ChargeCode.Project44Code.ToString()
                    });
                lstGuaranteedServices.DataBind();

                var lstExpeditedServices = item.FindControl("lstExpeditedServices").ToListView();
                lstExpeditedServices.DataSource = rate.ExpeditedCharges
                    .OrderBy(c => c.ChargeCode.Project44Code)
                    .Select(c => new
                    {
                        Description = Project44Constants.GetExpeditedServiceTime(c.ChargeCode.Project44Code),
                        Project44ChargeCode = c.ChargeCode.Project44Code.ToString()
                    });
                lstExpeditedServices.DataBind();
            }
            else
            {
                var lstGuaranteedServices = item.FindControl("lstGuaranteedServices").ToListView();
                lstGuaranteedServices.DataSource = rate.GuaranteedCharges.Select(c => c.LTLGuaranteedCharge)
                    .OrderBy(g => g.Time)
                    .Select(c => new
                    {
                        c.Id,
                        Description = c.Time.ConvertAmPm(),
                        Project44ChargeCode = string.Empty
                    });
                lstGuaranteedServices.DataBind();
            }
            

            var rptTlRouteDetails = item.FindControl("rptTlRouteDetails").ToRepeater();

            rptTlRouteDetails.DataSource = rate.TLRateDetails;
            rptTlRouteDetails.DataBind();

            var lnkChargeBreakdown = item.FindControl("lnkChargeBreakdown").ToLinkButton();
            lnkChargeBreakdown.OnClientClick = string.Format("ToggleChargeBreakdownVisibility({0}); return false;", e.Item.DataItemIndex);
        }


        private ShoppedRate GetShoppedRate(Shipment shipment, Rate rate)
        {
            var vendor = rate == null
                            ? null
                            : rate.Mode == ServiceMode.LessThanTruckload
                                ? rate.LTLSellRate.VendorRating.Vendor
                                : rate.Vendor;

            var sr = new ShoppedRate
            {
                TenantId = ActiveUser.TenantId,
                Shipment = shipment,
                ReferenceNumber = shipment.ShipmentNumber.GetString(),
                Type = DisableRateSelection ? ShoppedRateType.Estimate : ShoppedRateType.Shipment,
                NumberLineItems = shipment.Items.Count,
                Quantity = shipment.Items.Sum(i => i.Quantity),
                Weight = shipment.Items.TotalActualWeight(),
                HighestFreightClass = shipment.Items.Any() ? shipment.Items.Max(i => i.ActualFreightClass) : 0,
                MostSignificantFreightClass = shipment.Items.Any()
                                                ? shipment.Items
                                                    .GroupBy(i => i.ActualFreightClass)
                                                    .Select(g => new { FreightClass = g.Key, Items = g })
                                                    .OrderByDescending(a => a.Items.Count())
                                                    .ThenByDescending(a => a.FreightClass)
                                                    .First().FreightClass
                                                : 0,
                LowestTotalCharge = rate == null ? 0 : rate.BaseRateCharges.Sum(c => c.AmountDue),
                DateCreated = DateTime.Now,
                Customer = shipment.Customer,
                User = ActiveUser,
                OriginCountryId = shipment.Origin.CountryId,
                OriginCity = shipment.Origin.City.GetString(),
                OriginPostalCode = shipment.Origin.PostalCode.GetString(),
                OriginState = shipment.Origin.State.GetString(),
                DestinationCountryId = shipment.Destination.CountryId,
                DestinationCity = shipment.Destination.City.GetString(),
                DestinationPostalCode = shipment.Destination.PostalCode.GetString(),
                DestinationState = shipment.Destination.State.GetString(),
                VendorName = vendor == null ? string.Empty : vendor.Name,
                VendorNumber = vendor == null ? string.Empty : vendor.VendorNumber,
                Services = new List<ShoppedRateService>()
            };

            foreach (var service in shipment.Services)
                sr.Services.Add(new ShoppedRateService { TenantId = ActiveUser.TenantId, ServiceId = service.ServiceId, ShoppedRate = sr, });

            return sr;
        }


	    protected void OnCloseTerminalInformationClicked(object sender, EventArgs e)
        {
            pnlTerminalInformation.Visible = false;
            pnlTerminalInformationDimScreen.Visible = false;
        }

        protected void OnVendorClicked(object sender, EventArgs e)
        {
            pnlTerminalInformation.Visible = true;
            pnlTerminalInformationDimScreen.Visible = true;
            var button = (Control)sender;


            var scac = button.Parent.FindControl("hidVendorScac").ToCustomHiddenField().Value;
            var originTerminalCode = button.Parent.FindControl("hidOriginTerminalCode").ToCustomHiddenField().Value;
            var destinationTerminalCode = button.Parent.FindControl("hidDestinationTerminalCode").ToCustomHiddenField().Value;

            try
            {
                var variables = ProcessorVars.IntegrationVariables[ActiveUser.TenantId];
                var oc = new Country(hidOriginCountryId.Value.ToLong());
                var dc = new Country(hidDestinationCountryId.Value.ToLong());
                var carrierConnect = new CarrierConnect(variables.CarrierConnectParameters, new List<Country> { oc, dc });

                var terminals = carrierConnect.GetLTLTerminalInfo(scac, new[] { originTerminalCode, destinationTerminalCode }, hidShipmentDate.Value.ToDateTime());

                var origin = terminals.FirstOrDefault(t => t.Code == originTerminalCode);
                var dest = terminals.FirstOrDefault(t => t.Code == destinationTerminalCode);

                litTerminalInformationHeader.Text = string.Format("{0} Terminal Information", button as LinkButton == null ? ((ImageButton)button).ToolTip : ((LinkButton)button).Text);
                litOrigin.Text = origin == null ? "Origin Terminal Not Found!" : origin.BuildTerminalDisplay();
                litDestination.Text = dest == null ? "Destination Terminal Not Found!" : dest.BuildTerminalDisplay();
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                litOrigin.Text = "Error retrieving origin terminal information";
                litDestination.Text = "Error retrieve destination terminal information";
            }
        }



        protected void OnbtnPdfClicked(object sender, ImageClickEventArgs e)
        {
            var rates = new List<Rate>();
            foreach (var item in lstRates.Items)
            {
                var rate = new Rate
                {
                    Mode = item.FindControl("hidServiceMode").ToCustomHiddenField().Value.ToEnum<ServiceMode>(),
                    Vendor = new Vendor(item.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong()),
                    LTLSellRate = new LTLSellRate(item.FindControl("hidLTLSellRateId").ToCustomHiddenField().Value.ToLong()),
                    DiscountTier = new DiscountTier(item.FindControl("hidDiscountTierId").ToCustomHiddenField().Value.ToLong()),
                    CFCRule = new LTLCubicFootCapacityRule(item.FindControl("hidCFCRuleId").ToCustomHiddenField().Value.ToLong()),
                    OverlengthRuleApplied = item.FindControl("hidOverlengthRuleAppield").ToCustomHiddenField().Value.ToBoolean(),
                    SmallPackServiceType = item.FindControl("hidSmallPackType").ToCustomHiddenField().Value,
                    SmallPackEngine = item.FindControl("hidSmallPackageEngine").ToCustomHiddenField().Value.ToEnum<SmallPackageEngine>(),
                    OriginalRateValue = item.FindControl("hidOriginalRateValue").ToCustomHiddenField().Value.ToDecimal(),
                    DirectPointRate = item.FindControl("hidDirectPointRate").ToCustomHiddenField().Value.ToBoolean(),
                    ApplyDiscount = item.FindControl("hidApplyDiscount").ToCustomHiddenField().Value.ToBoolean(),
                    BilledWeight = item.FindControl("hidBilledWeight").ToCustomHiddenField().Value.ToDecimal(),
                    RatedCubicFeet = item.FindControl("hidRatedCubicFeet").ToCustomHiddenField().Value.ToDecimal(),
                    RatedWeight = item.FindControl("hidRatedWeight").ToCustomHiddenField().Value.ToDecimal(),
                    FuelMarkupPercent = item.FindControl("hidFuelMarkupPercent").ToCustomHiddenField().Value.ToDecimal(),
                    FreightProfitAdjustment = item.FindControl("hidFreightProfitAdjustment").ToCustomHiddenField().Value.ToDecimal(),
                    FuelProfitAdjustment = item.FindControl("hidFuelProfitAdjustment").ToCustomHiddenField().Value.ToDecimal(),
                    AccessorialProfitAdjustment = item.FindControl("hidAccessorialProfitAdjustment").ToCustomHiddenField().Value.ToDecimal(),
                    ServiceProfitAdjustment = item.FindControl("hidServiceProfitAdjustment").ToCustomHiddenField().Value.ToDecimal(),
                    ResellerFreightMarkup = item.FindControl("hidResellerFreightMarkup").ToCustomHiddenField().Value.ToDecimal(),
                    ResellerFreightMarkupIsPercent = item.FindControl("hidResellerFreightMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean(),
                    ResellerFuelMarkup = item.FindControl("hidResellerFuelMarkup").ToCustomHiddenField().Value.ToDecimal(),
                    ResellerFuelMarkupIsPercent = item.FindControl("hidResellerFuelMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean(),
                    ResellerAccessorialMarkup = item.FindControl("hidResellerAccessorialMarkup").ToCustomHiddenField().Value.ToDecimal(),
                    ResellerAccessorialMarkupIsPercent = item.FindControl("hidResellerAccessorialMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean(),
                    ResellerServiceMarkup = item.FindControl("hidResellerServiceMarkup").ToCustomHiddenField().Value.ToDecimal(),
                    ResellerServiceMarkupIsPercent = item.FindControl("hidResellerServiceMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean(),
                    OriginTerminalCode = item.FindControl("hidOriginTerminalCode").ToCustomHiddenField().Value,
                    DestinationTerminalCode = item.FindControl("hidDestinationTerminalCode").ToCustomHiddenField().Value
                };

                // retrieve rate

                rate.NoFreightProfit = rate.FreightProfitAdjustment == 0;
                rate.NoFuelProfit = rate.FuelProfitAdjustment == 0;
                rate.NoAccessorialProfit = rate.AccessorialProfitAdjustment == 0;
                rate.NoServiceProfit = rate.ServiceProfitAdjustment == 0;


                var litEstimatedDeliveryDate = item.FindControl("litEstimatedDeliveryDate").ToLiteral();
                var litTransitTime = item.FindControl("litTransitTime").ToLiteral();
                rate.TransitDays = litTransitTime.Text == Unknown ? Core.Rating.Rate.DefaultTransitTime : litTransitTime.Text.ToInt();
                rate.EstimatedDeliveryDate = litEstimatedDeliveryDate.Text == Unknown ? DateUtility.SystemEarliestDateTime : litEstimatedDeliveryDate.Text.ToDateTime();

                var lstGuaranteedServices = item.FindControl("lstGuaranteedServices").ToListView();

                var lstGuaranteedServicesItem = lstGuaranteedServices.Items.FirstOrDefault(i => i.FindControl("chkGuaranteedServiceEnabled").ToAltUniformCheckBox().Checked);

                var gChargeId = lstGuaranteedServicesItem != null
                    ? lstGuaranteedServicesItem.FindControl("hidLTLGuaranteedChargeId").ToCustomHiddenField().Value.ToLong()
                    : default(long);
                var p44ChargeCode = lstGuaranteedServicesItem != null ? lstGuaranteedServicesItem.FindControl("hidProject44ChargeCode").ToCustomHiddenField().Value : string.Empty;


                var lstCharges = item.FindControl("lstCharges").ToListView();
                rate.Charges.AddRange(
                    lstCharges.Items
                              .Select(i => new RateCharge
                              {
                                  ChargeCodeId = i.FindControl("hidChargeCodeId").ToCustomHiddenField().Value.ToLong(),
                                  UnitBuy = i.FindControl("hidUnitBuy").ToCustomHiddenField().Value.ToDecimal(),
                                  UnitSell = i.FindControl("hidUnitSell").ToCustomHiddenField().Value.ToDecimal(),
                                  UnitDiscount = i.FindControl("hidUnitDiscount").ToCustomHiddenField().Value.ToDecimal(),
                                  Quantity = i.FindControl("litQuantity").ToLiteral().Text.ToInt(),
                                  LTLGuaranteedChargeId = i.FindControl("hidLTLGuaranteedChargeId").ToCustomHiddenField().Value.ToLong(),
                                  IsProject44GuaranteedCharge = i.FindControl("hidIsProject44GuaranteedCharge").ToCustomHiddenField().Value.ToBoolean(),
                                  IsProject44ExpeditedServiceCharge = i.FindControl("hidIsProject44ExpeditedServiceCharge").ToCustomHiddenField().Value.ToBoolean()
                              })
                              .Where(c => c.LTLGuaranteedChargeId == default(long) && !c.IsProject44GuaranteedCharge
                                          || (c.LTLGuaranteedChargeId != default(long) && gChargeId == c.LTLGuaranteedChargeId)
                                          || (c.ChargeCode.Project44Code.ToString() == p44ChargeCode && c.IsProject44GuaranteedCharge)));

                rates.Add(rate);
            }


            if (DownloadRates != null)
                DownloadRates(this, new ViewEventArgs<bool, List<Rate>>(chkShowVendorRatingActualName.Checked, rates));

        }


        protected void OnSortByEstimatedDevlieryClicked(object sender, EventArgs e)
        {
            hidSortFlag.Value = EstimatedDateFlag;
            SortAndBindRates();
        }

        protected void OnSortByTotalClicked(object sender, EventArgs e)
        {
            hidSortFlag.Value = FreightFlag;
            SortAndBindRates();
        }

        protected void OnSortByTransitClicked(object sender, EventArgs e)
        {
            SortAndBindRates();
        }

        private void SortAndBindRates()
        {
            var rates = new List<Rate>();
            foreach (var item in lstRates.Items)
            {
                var rate = new Rate
                {
                    Mode = item.FindControl("hidServiceMode").ToCustomHiddenField().Value.ToEnum<ServiceMode>(),
                    Vendor = new Vendor(item.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong()),
                    LTLSellRate = new LTLSellRate(item.FindControl("hidLTLSellRateId").ToCustomHiddenField().Value.ToLong()),
                    DiscountTier = new DiscountTier(item.FindControl("hidDiscountTierId").ToCustomHiddenField().Value.ToLong()),
                    CFCRule = new LTLCubicFootCapacityRule(item.FindControl("hidCFCRuleId").ToCustomHiddenField().Value.ToLong()),
                    OverlengthRuleApplied = item.FindControl("hidOverlengthRuleAppield").ToCustomHiddenField().Value.ToBoolean(),
                    SmallPackServiceType = item.FindControl("hidSmallPackType").ToCustomHiddenField().Value,
                    SmallPackEngine =
                        item.FindControl("hidSmallPackageEngine").ToCustomHiddenField().Value.ToEnum<SmallPackageEngine>(),
                    OriginalRateValue = item.FindControl("hidOriginalRateValue").ToCustomHiddenField().Value.ToDecimal(),
                    DirectPointRate = item.FindControl("hidDirectPointRate").ToCustomHiddenField().Value.ToBoolean(),
                    ApplyDiscount = item.FindControl("hidApplyDiscount").ToCustomHiddenField().Value.ToBoolean(),
                    BilledWeight = item.FindControl("hidBilledWeight").ToCustomHiddenField().Value.ToDecimal(),
                    RatedCubicFeet = item.FindControl("hidRatedCubicFeet").ToCustomHiddenField().Value.ToDecimal(),
                    RatedWeight = item.FindControl("hidRatedWeight").ToCustomHiddenField().Value.ToDecimal(),
                    FuelMarkupPercent = item.FindControl("hidFuelMarkupPercent").ToCustomHiddenField().Value.ToDecimal(),
                    FreightProfitAdjustment =
                        item.FindControl("hidFreightProfitAdjustment").ToCustomHiddenField().Value.ToDecimal(),
                    FuelProfitAdjustment = item.FindControl("hidFuelProfitAdjustment").ToCustomHiddenField().Value.ToDecimal(),
                    AccessorialProfitAdjustment =
                        item.FindControl("hidAccessorialProfitAdjustment").ToCustomHiddenField().Value.ToDecimal(),
                    ServiceProfitAdjustment =
                        item.FindControl("hidServiceProfitAdjustment").ToCustomHiddenField().Value.ToDecimal(),
                    ResellerFreightMarkup = item.FindControl("hidResellerFreightMarkup").ToCustomHiddenField().Value.ToDecimal(),
                    ResellerFreightMarkupIsPercent =
                        item.FindControl("hidResellerFreightMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean(),
                    ResellerFuelMarkup = item.FindControl("hidResellerFuelMarkup").ToCustomHiddenField().Value.ToDecimal(),
                    ResellerFuelMarkupIsPercent =
                        item.FindControl("hidResellerFuelMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean(),
                    ResellerAccessorialMarkup =
                        item.FindControl("hidResellerAccessorialMarkup").ToCustomHiddenField().Value.ToDecimal(),
                    ResellerAccessorialMarkupIsPercent =
                        item.FindControl("hidResellerAccessorialMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean(),
                    ResellerServiceMarkup = item.FindControl("hidResellerServiceMarkup").ToCustomHiddenField().Value.ToDecimal(),
                    ResellerServiceMarkupIsPercent =
                        item.FindControl("hidResellerServiceMarkupIsPercent").ToCustomHiddenField().Value.ToBoolean(),
                    OriginTerminalCode = item.FindControl("hidOriginTerminalCode").ToCustomHiddenField().Value,
                    DestinationTerminalCode = item.FindControl("hidDestinationTerminalCode").ToCustomHiddenField().Value,
                    IsProject44Rate = item.FindControl("hidIsProject44Rate").ToCustomHiddenField().Value.ToBoolean(),
                    Project44QuoteNumber = item.FindControl("hidProject44QuoteNumber").ToCustomHiddenField().Value
            };

                // retrieve rate

                rate.NoFreightProfit = rate.FreightProfitAdjustment == 0;
                rate.NoFuelProfit = rate.FuelProfitAdjustment == 0;
                rate.NoAccessorialProfit = rate.AccessorialProfitAdjustment == 0;
                rate.NoServiceProfit = rate.ServiceProfitAdjustment == 0;

                var litEstimatedDeliveryDate = item.FindControl("litEstimatedDeliveryDate").ToLiteral();
                var litTransitTime = item.FindControl("litTransitTime").ToLiteral();
                rate.TransitDays = litTransitTime.Text == Unknown ? Core.Rating.Rate.DefaultTransitTime : litTransitTime.Text.ToInt();
                rate.EstimatedDeliveryDate = litEstimatedDeliveryDate.Text == Unknown ? DateUtility.SystemEarliestDateTime : litEstimatedDeliveryDate.Text.ToDateTime();

                var lstCharges = item.FindControl("lstCharges").ToListView();
                rate.Charges.AddRange(
                    lstCharges.Items
                        .Select(i => new RateCharge
                        {
                            ChargeCodeId = i.FindControl("hidChargeCodeId").ToCustomHiddenField().Value.ToLong(),
                            UnitBuy = i.FindControl("hidUnitBuy").ToCustomHiddenField().Value.ToDecimal(),
                            UnitSell = i.FindControl("hidUnitSell").ToCustomHiddenField().Value.ToDecimal(),
                            UnitDiscount = i.FindControl("hidUnitDiscount").ToCustomHiddenField().Value.ToDecimal(),
                            Quantity = i.FindControl("litQuantity").ToLiteral().Text.ToInt(),
                            Comment = i.FindControl("hidChargeCodeComment").ToCustomHiddenField().Value,
                            LTLGuaranteedChargeId = i.FindControl("hidLTLGuaranteedChargeId").ToCustomHiddenField().Value.ToLong(),
                            IsProject44GuaranteedCharge = i.FindControl("hidIsProject44GuaranteedCharge").ToCustomHiddenField().Value.ToBoolean(),
                            IsProject44ExpeditedServiceCharge = i.FindControl("hidIsProject44ExpeditedServiceCharge").ToCustomHiddenField().Value.ToBoolean(),
                            Project44QuoteNumber = i.FindControl("hidProject44QuoteNumber").ToCustomHiddenField().Value,
                        }));

                var rptTlRouteDetails = item.FindControl("rptTlRouteDetails").ToRepeater();
                rate.TLRateDetails.AddRange(
                    rptTlRouteDetails.Items.Cast<RepeaterItem>()
                        .Select(i => new TLRateDetail
                        {
                            OriginFullAddress = i.FindControl("hidTlRateDetailOriginFullAddress").ToCustomHiddenField().Value,
                            DestinationFullAddress = i.FindControl("hidTlRateDetailDestinationFullAddress").ToCustomHiddenField().Value,
                            RateType = i.FindControl("hidTlRateDetailRateType").ToCustomHiddenField().Value.ToEnum<RateType>(),
                            Rate = i.FindControl("hidTlRateDetailRate").ToCustomHiddenField().Value.ToDecimal(),
                            TariffType = i.FindControl("hidTlRateDetailTariffType").ToCustomHiddenField().Value.ToEnum<TLTariffType>(),
                            Mileage = i.FindControl("hidTlRateDetailMileage").ToCustomHiddenField().Value.ToDecimal(),
                        }));
                rates.Add(rate);
            }

            lstRates.DataSource = hidSortFlag.Value == FreightFlag
                                      ? rates
                                        .OrderBy(r => r.BaseRateCharges.Sum(c => c.AmountDue))
                                        .ThenBy(r => r.EstimatedDeliveryDate)
                                      : hidSortFlag.Value == EstimatedDateFlag
                                            ? rates.OrderBy(r => r.EstimatedDeliveryDate)
                                            : rates.OrderBy(r => r.TransitDays);

            lstRates.DataBind();
            hidSortFlag.Value = string.Empty;
        }

        protected void OnGuaranteedServiceEnabledCheckChanged(object sender, EventArgs e)
        {
            var chkGuaranteedServiceEnabled = sender as AltUniformCheckBox;

            if (chkGuaranteedServiceEnabled == null) return;
            var item = chkGuaranteedServiceEnabled.Parent.Parent.Parent.Parent;

            var serviceMode = item.FindControl("hidServiceMode").ToCustomHiddenField().Value.ToEnum<ServiceMode>();
            var project44QuoteNumber = item.FindControl("hidProject44QuoteNumber").ToCustomHiddenField().Value;


            var lstCharges = item.FindControl("lstCharges").ToListView();
            var charges = lstCharges
                .Items
                .Select(i => new RateCharge
                    {
                        ChargeCodeId = i.FindControl("hidChargeCodeId").ToCustomHiddenField().Value.ToLong(),
                        UnitBuy = i.FindControl("hidUnitBuy").ToCustomHiddenField().Value.ToDecimal(),
                        UnitSell = i.FindControl("hidUnitSell").ToCustomHiddenField().Value.ToDecimal(),
                        UnitDiscount = i.FindControl("hidUnitDiscount").ToCustomHiddenField().Value.ToDecimal(),
                        Quantity = i.FindControl("litQuantity").ToLiteral().Text.ToInt(),
                        Comment = i.FindControl("hidChargeCodeComment").ToCustomHiddenField().Value,
                        LTLGuaranteedChargeId = i.FindControl("hidLTLGuaranteedChargeId").ToCustomHiddenField().Value.ToLong(),
                        IsProject44GuaranteedCharge = i.FindControl("hidIsProject44GuaranteedCharge").ToCustomHiddenField().Value.ToBoolean(),
                        Project44QuoteNumber = i.FindControl("hidProject44QuoteNumber").ToCustomHiddenField().Value,
                        IsProject44ExpeditedServiceCharge = i.FindControl("hidIsProject44ExpeditedServiceCharge").ToCustomHiddenField().Value.ToBoolean()
                }).ToList();

            var gChargeId = chkGuaranteedServiceEnabled.Checked ? chkGuaranteedServiceEnabled.Parent.FindControl("hidLTLGuaranteedChargeId").ToCustomHiddenField().Value.ToLong() : default(long);
            var p44ChargeCode = chkGuaranteedServiceEnabled.Checked ? chkGuaranteedServiceEnabled.Parent.FindControl("hidProject44ChargeCode").ToCustomHiddenField().Value : string.Empty;

            var displayCharges = charges.Where(c => c.LTLGuaranteedChargeId == default(long) && !c.IsProject44GuaranteedCharge && !c.IsProject44ExpeditedServiceCharge
                                                    || (c.LTLGuaranteedChargeId != default(long) && gChargeId == c.LTLGuaranteedChargeId)
                                                    || (c.ChargeCode.Project44Code.ToString() == p44ChargeCode && chkGuaranteedServiceEnabled.Checked)).ToList();

            // rebind information into view
            item.FindControl("litTotalAccessorialCharge").ToLiteral().Text = displayCharges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Accessorial)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalFreightCharge").ToLiteral().Text = displayCharges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalFuelCharge").ToLiteral().Text = displayCharges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Fuel)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalServiceCharge").ToLiteral().Text = displayCharges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Service)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalOveralCharge").ToLiteral().Text = displayCharges
                .Sum(c => c.AmountDue)
                .ToString("n2");

            var chargeList = charges.Select(c => new
            {
                c.ChargeCodeId,
                c.UnitBuy,
                c.UnitSell,
                c.UnitDiscount,
                c.ChargeCode.Code,
                c.ChargeCode.Description,
                Category = c.ChargeCode.Category.FormattedString(),
                c.Quantity,
                c.AmountDue,
                c.FinalBuy,
                c.Comment,
                Mode = serviceMode,
                c.LTLGuaranteedChargeId,
                Visible = (c.LTLGuaranteedChargeId == default(long) && !c.IsProject44GuaranteedCharge && !c.IsProject44ExpeditedServiceCharge
                           || (c.LTLGuaranteedChargeId != default(long)
                               && chkGuaranteedServiceEnabled.Checked
                               && c.LTLGuaranteedChargeId == gChargeId)) ||
                          (c.ChargeCode.Project44Code.ToString() == p44ChargeCode &&
                           chkGuaranteedServiceEnabled.Checked),
                c.IsProject44GuaranteedCharge,
                c.Project44QuoteNumber,
                c.IsProject44ExpeditedServiceCharge
            });
            lstCharges.DataSource = chargeList;
            lstCharges.DataBind();

            var litProject44QuoteNumber = item.FindControl("litProject44QuoteNumber").ToLiteral();
            var hidProject44QuoteNumber = item.FindControl("hidProject44QuoteNumber").ToCustomHiddenField();

            var selectedGuarOption = chargeList.FirstOrDefault(c => c.Visible && c.IsProject44GuaranteedCharge);
            litProject44QuoteNumber.Text = selectedGuarOption == null ? project44QuoteNumber : selectedGuarOption.Project44QuoteNumber;
            hidProject44QuoteNumber.Value = selectedGuarOption == null ? project44QuoteNumber : selectedGuarOption.Project44QuoteNumber;

            foreach (var gItem in item.FindControl("lstGuaranteedServices").ToListView().Items)
            {
                var chk = gItem.FindControl("chkGuaranteedServiceEnabled").ToAltUniformCheckBox();
                if (chk.ClientID != chkGuaranteedServiceEnabled.ClientID) chk.Checked = false;
            }


            foreach (var gItem in item.FindControl("lstExpeditedServices").ToListView().Items)
            {
                var chk = gItem.FindControl("chkExpeditedServiceEnabled").ToAltUniformCheckBox();
                chk.Checked = false;
            }

            // update the relevant update panels
            item.FindControl("upPnlRateInformation").ToUpdatePanel().Update();
            item.FindControl("upPnlTotalFreightCharge").ToUpdatePanel().Update();
            item.FindControl("upPnlTotalFuelCharge").ToUpdatePanel().Update();
            item.FindControl("upPnlTotalAccessorialCharge").ToUpdatePanel().Update();
            item.FindControl("upPnlTotalServiceCharge").ToUpdatePanel().Update();
            item.FindControl("upPnlTotalOverallCharge").ToUpdatePanel().Update();
            item.FindControl("upPnlCharges").ToUpdatePanel().Update();
            item.FindControl("upnlProject44QuoteNumber").ToUpdatePanel().Update();
            item.FindControl("upnlExpeditedServices").ToUpdatePanel().Update();
        }

        protected void OnExpeditedServiceEnabledCheckChanged(object sender, EventArgs e)
        {
            var chkExpeditedService = sender as AltUniformCheckBox;

            if (chkExpeditedService == null) return;
            var item = chkExpeditedService.Parent.Parent.Parent.Parent;

            var serviceMode = item.FindControl("hidServiceMode").ToCustomHiddenField().Value.ToEnum<ServiceMode>();
            var project44QuoteNumber = item.FindControl("hidProject44QuoteNumber").ToCustomHiddenField().Value;


            var lstCharges = item.FindControl("lstCharges").ToListView();
            var charges = lstCharges
                .Items
                .Select(i => new RateCharge
                {
                    ChargeCodeId = i.FindControl("hidChargeCodeId").ToCustomHiddenField().Value.ToLong(),
                    UnitBuy = i.FindControl("hidUnitBuy").ToCustomHiddenField().Value.ToDecimal(),
                    UnitSell = i.FindControl("hidUnitSell").ToCustomHiddenField().Value.ToDecimal(),
                    UnitDiscount = i.FindControl("hidUnitDiscount").ToCustomHiddenField().Value.ToDecimal(),
                    Quantity = i.FindControl("litQuantity").ToLiteral().Text.ToInt(),
                    Comment = i.FindControl("hidChargeCodeComment").ToCustomHiddenField().Value,
                    LTLGuaranteedChargeId = i.FindControl("hidLTLGuaranteedChargeId").ToCustomHiddenField().Value.ToLong(),
                    IsProject44GuaranteedCharge = i.FindControl("hidIsProject44GuaranteedCharge").ToCustomHiddenField().Value.ToBoolean(),
                    IsProject44ExpeditedServiceCharge = i.FindControl("hidIsProject44ExpeditedServiceCharge").ToCustomHiddenField().Value.ToBoolean(),
                    Project44QuoteNumber = i.FindControl("hidProject44QuoteNumber").ToCustomHiddenField().Value,
                }).ToList();


            var p44ChargeCode = chkExpeditedService.Checked ? chkExpeditedService.Parent.FindControl("hidProject44ChargeCode").ToCustomHiddenField().Value : string.Empty;

            var displayCharges = charges.Where(c => c.LTLGuaranteedChargeId == default(long) && !c.IsProject44GuaranteedCharge && !c.IsProject44ExpeditedServiceCharge
                                                    || (c.IsProject44ExpeditedServiceCharge && c.ChargeCode.Project44Code.ToString() == p44ChargeCode && chkExpeditedService.Checked)).ToList();

            // rebind information into view
            item.FindControl("litTotalAccessorialCharge").ToLiteral().Text = displayCharges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Accessorial)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalFreightCharge").ToLiteral().Text = displayCharges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalFuelCharge").ToLiteral().Text = displayCharges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Fuel)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalServiceCharge").ToLiteral().Text = displayCharges
                .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Service)
                .Sum(c => c.AmountDue)
                .ToString("n2");
            item.FindControl("litTotalOveralCharge").ToLiteral().Text = displayCharges
                .Sum(c => c.AmountDue)
                .ToString("n2");

            var chargeList = charges.Select(c => new
            {
                c.ChargeCodeId,
                c.UnitBuy,
                c.UnitSell,
                c.UnitDiscount,
                c.ChargeCode.Code,
                c.ChargeCode.Description,
                Category = c.ChargeCode.Category.FormattedString(),
                c.Quantity,
                c.AmountDue,
                c.FinalBuy,
                c.Comment,
                Mode = serviceMode,
                c.LTLGuaranteedChargeId,
                Visible = (c.LTLGuaranteedChargeId == default(long) && !c.IsProject44GuaranteedCharge  && !c.IsProject44ExpeditedServiceCharge) || 
                          (c.IsProject44ExpeditedServiceCharge && c.ChargeCode.Project44Code.ToString() == p44ChargeCode && chkExpeditedService.Checked),
                c.IsProject44GuaranteedCharge,
                c.IsProject44ExpeditedServiceCharge,
                c.Project44QuoteNumber
            });
            lstCharges.DataSource = chargeList;
            lstCharges.DataBind();

            var litProject44QuoteNumber = item.FindControl("litProject44QuoteNumber").ToLiteral();
            var hidProject44QuoteNumber = item.FindControl("hidProject44QuoteNumber").ToCustomHiddenField();

            var selectedExpeditedOption = chargeList.FirstOrDefault(c => c.Visible && c.IsProject44ExpeditedServiceCharge);
            litProject44QuoteNumber.Text = selectedExpeditedOption == null ? project44QuoteNumber : selectedExpeditedOption.Project44QuoteNumber;
            hidProject44QuoteNumber.Value = selectedExpeditedOption == null ? project44QuoteNumber : selectedExpeditedOption.Project44QuoteNumber;

            foreach (var gItem in item.FindControl("lstGuaranteedServices").ToListView().Items)
            {
                var chk = gItem.FindControl("chkGuaranteedServiceEnabled").ToAltUniformCheckBox();
                chk.Checked = false;
            }

            foreach (var gItem in item.FindControl("lstExpeditedServices").ToListView().Items)
            {
                var chk = gItem.FindControl("chkExpeditedServiceEnabled").ToAltUniformCheckBox();
                if (chk.ClientID != chkExpeditedService.ClientID) chk.Checked = false;
            }

            // update the relevant update panels
            item.FindControl("upPnlRateInformation").ToUpdatePanel().Update();
            item.FindControl("upPnlTotalFreightCharge").ToUpdatePanel().Update();
            item.FindControl("upPnlTotalFuelCharge").ToUpdatePanel().Update();
            item.FindControl("upPnlTotalAccessorialCharge").ToUpdatePanel().Update();
            item.FindControl("upPnlTotalServiceCharge").ToUpdatePanel().Update();
            item.FindControl("upPnlTotalOverallCharge").ToUpdatePanel().Update();
            item.FindControl("upPnlCharges").ToUpdatePanel().Update();
            item.FindControl("upnlProject44QuoteNumber").ToUpdatePanel().Update();
            item.FindControl("upnlGuaranteedServices").ToUpdatePanel().Update();
        }
    }
}