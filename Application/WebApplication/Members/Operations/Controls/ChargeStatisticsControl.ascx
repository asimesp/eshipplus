﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChargeStatisticsControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ChargeStatisticsControl" %>

<div class="fieldgroup note mr5 pt5">
    <asp:Literal ID="litReseller" runat="server" />
</div>
<div class="fieldgroup mr5">
    <label class="upper">Total Buy:</label>
    <label><asp:Literal ID="litTotalBuy" runat="server" /></label>
</div>
<div class="fieldgroup mr5">
    <label class="upper">Total Due:</label>
    <label><asp:Literal ID="litTotalDue" runat="server" /></label>
</div>
<div class="fieldgroup mr5">
    <label class="upper">Profit:</label>
    <label><asp:Literal ID="litProfit" runat="server" /></label>
    <label>(<asp:Literal ID="litProfitPercent" runat="server" />)</label>
</div>
<div class="fieldgroup">
    <label class="upper">
        <abbr title="Gross Profit Margin">GPM</abbr>:</label>
    <label><asp:Literal ID="litGPM" runat="server" /></label>
</div>
