﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class JobDashboardDetailControl : MemberControlBase
    {
        public IOpsLocationCollectionResource ResourcePage { get; set; }

        public int ItemIndex
        {
            get { return hidItemIndex.Value.ToInt(); }
            set { hidItemIndex.Value = value.ToString(); }
        }

        public bool EnableOpen
        {
            set { hypJobView.Visible = value; }
        }

        public List<ViewListItem> ServiceCollection { get { return ResourcePage.ServiceCollection; } }

        public List<ViewListItem> EquipmentTypeCollection { get { return ResourcePage.EquipmentTypeCollection; } }


        public void LoadJob(JobDashboardDto Job)
        {

            if (Job == null) return;

            hidJobId.Value = Job.Id.ToString();

            //details
            litJobNumber.Text = Job.JobNumber;
            litDateCreated.Text = Job.DateCreated.FormattedShortDate();
            litStatus.Text = Job.Status.ToString();

            hypUser.Visible = ActiveUser.HasAccessTo(ViewCode.User);
            hypUser.NavigateUrl = string.Format("{0}?{1}={2}", UserView.PageAddress, WebApplicationConstants.TransferNumber, Job.CreatedByUserId.GetString().UrlTextEncrypt());
            litUser.Text = Job.CreatedByUsername;
            
            hypCustomer.Visible = ActiveUser.HasAccessTo(ViewCode.Customer);
            hypCustomer.NavigateUrl = string.Format("{0}?{1}={2}", CustomerView.PageAddress, WebApplicationConstants.TransferNumber, Job.CustomerId.GetString().UrlTextEncrypt());
            litCustomerNameNumber.Text = string.Format("{0} - {1}", Job.CustomerNumber, Job.CustomerName);

            hypJobView.NavigateUrl = string.Format("{0}?{1}={2}", JobView.PageAddress, WebApplicationConstants.TransferNumber, Job.JobNumber);

            litExternalReference1.Text = Job.ExternalReference1;
            litExternalReference2.Text = Job.ExternalReference2;

            var httpAddr = Request.ResolveSiteRootWithHttp();
            litShipments.Text = ActiveUser.HasAccessTo(ViewCode.Shipment)
                                    ? ShipmentView.BuildShipmentLinks(Job.Shipments).Replace("~", httpAddr)
                                    : Job.Shipments;
            litServiceTickets.Text = ActiveUser.HasAccessTo(ViewCode.ServiceTicket)
                                         ? ServiceTicketView.BuildServiceTicketLinks(Job.ServiceTickets).Replace("~", httpAddr)
                                         : Job.ServiceTickets;
            litLoadOrders.Text = ActiveUser.HasAccessTo(ViewCode.LoadOrder)
                                         ? LoadOrderView.BuildLoadOrderLinks(Job.LoadOrders).Replace("~", httpAddr)
                                         : Job.LoadOrders;

        }
    }
}