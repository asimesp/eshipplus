﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RateSelectionDisplayControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.RateSelectionDisplayControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Rating" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<asp:Panel runat="server" ID="pnlRateSelection" CssClass="popupControl" DefaultButton="ibtnClose">
    <div class="popheader">
        <h4>Rate Selection</h4>
        <asp:ImageButton ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
    </div>
    <asp:Panel runat="server" ID="pnlRateDownload" Visible="false">
        <table class="poptable">
            <tr class="header">
                <th>
                    <div class="row pt5 pb5">
                    <asp:ImageButton ID="ibtnPdf" runat="server" ImageUrl="~/images/icons/downloadPdf.png"
                        OnClick="OnbtnPdfClicked" CausesValidation="False" ToolTip="download pdf" Width="25px" CssClass="left" />
                    <asp:CheckBox runat="server" ID="chkShowVendorRatingActualName" Visible="False" CssClass="jQueryUniform right middle pr10" Text="Use Vendor Rating Actual Name for Rate Download" />
                    </div>
                    <div class="rowgroup pl10 pr10 fs75em text-left " >
                        <p class="row">Origin Postal Code: <u><%= hidOriginPostalCode.Value %></u> &nbsp;&nbsp; Destination Postal Code: <u><%= hidDestinationPostalCode.Value %></u> &nbsp;&nbsp; Total Weight: <u><%= hidTotalWeight.Value %></u> lbs. &nbsp;&nbsp; Highest Freight Class: <u><%= hidHighestFreightClass.Value %></u>
                        </p>
                        <p class="row">These estimates are based on the information you provided. Actual shipment details at time of shipping will take precedence. If additional services are required or requested, additional charges may apply. Delivery dates are estimated based on standard transit days. If your destination is an indirect point or in an area of limited service, then dates are subject to change. This quote might not valid if shipment is greater than 12 feet in length; 
                            or if<br/>shipment occupies 750 cubic feet (height x width x depth, then divide by 1,728) or more and has an average density of less than 6 pounds per cubic foot (weight / cubic feet). Note, if shipment is non-stackable the height will be considered 96”. This quotation is subject to Logistics Plus® eShipPlus™ <a class="blue" href="https://www.eshipplus.com/Public/ToC.aspx" > Terms & Conditions</a>. Contact <a class="blue"  href="mailto:nadops@logisticsplus.net">nadops@logisticsplus.net</a> if any questions.</p>
                        <p  class="row">Guaranteed service shipments must be picked up by carrier no later than 5:00 PM to qualify. If you select Guaranteed service, please inform your driver at the time of pickup. Need faster service?<br/> Contact <a class="blue" href="mailto:expedited@logisticsplus.net">expedited@logisticsplus.net</a> for assistance.
                        </p>
                    </div>
                </th>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlCustomerCustomVendorMessage" Visible="False">
        <div class="row">
            <div class="fieldgroup pl20 pt20 pr20">
                <asp:Literal runat="server" ID="litCustomerCustomVendorMessage" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlMissingOrBadInput" Visible="False">
        <div class="row">
            <div class="fieldgroup pl20 pt20 pr20">
                <span class="red text-center fs85em">THIS IS AN ESTIMATE ONLY!</span>
                <span class="red fs85em">FOR A MORE ACCURATE ESTIMATE YOU MUST ENTER ALL ITEM INFORMATION (AS NOTED BELOW) OR CALL A SPECIALIST.</span><br />
                For most accurate rates, the following input must be present per shipment type:
            </div>
        </div>
        <div class="row pl20 pt20">
            <table class="poptable">
                <tr>
                    <td class="top <%= string.IsNullOrEmpty(litLTLFreightClass.Text) ? "hidden" : string.Empty %> <%= !string.IsNullOrEmpty(litLTLDensity.Text) || !string.IsNullOrEmpty(litPackage.Text) || !string.IsNullOrEmpty(litFTL.Text)? "border-right" : string.Empty %>" style='width: 25%'>
                        <div class="pr20">
                            <asp:Literal runat="server" ID="litLTLFreightClass" Visible="False" />
                        </div>
                    </td>
                    <td class="top <%= string.IsNullOrEmpty(litLTLDensity.Text) ? "hidden" : string.Empty %> <%= !string.IsNullOrEmpty(litFTL.Text) || !string.IsNullOrEmpty(litPackage.Text)? "border-right" : string.Empty %>" style='width: 25%'>
                        <div class="pr20 pl10">
                            <asp:Literal runat="server" ID="litLTLDensity" Visible="False" />
                        </div>
                    </td>
                    <td class="top <%= string.IsNullOrEmpty(litPackage.Text) ? "hidden" : string.Empty %> <%= !string.IsNullOrEmpty(litFTL.Text) ? "border-right" : string.Empty %>" style='width: 25%'>
                        <div class="pr20 pl10">
                            <asp:Literal runat="server" ID="litPackage" Visible="False" />
                        </div>
                    </td>
                    <td class="top" style='width: 25%'>
                        <div class="pr20 pl10">
                            <asp:Literal runat="server" ID="litFTL" Visible="False" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class='<%= string.IsNullOrEmpty(litLTLFreightClass.Text) ? "hidden" : string.Empty %> <%= !string.IsNullOrEmpty(litLTLDensity.Text) || !string.IsNullOrEmpty(litPackage.Text) || !string.IsNullOrEmpty(litFTL.Text)? "border-right" : string.Empty %>'>
                        <asp:Button runat="server" ID="btnHighlightLTLFreightClassFields" OnClick="OnCancelClicked" CssClass="mr10"
                            Text="Hightlight LTL Freight Class Fields" CausesValidation="False" Visible="false" />
                    </td>
                    <td class='<%= string.IsNullOrEmpty(litLTLDensity.Text) ? "hidden" : string.Empty %> <%= !string.IsNullOrEmpty(litFTL.Text) || !string.IsNullOrEmpty(litPackage.Text)? "border-right" : string.Empty %>'>
                        <asp:Button runat="server" ID="btnHighlightLTLDensityFields" OnClick="OnCancelClicked" CssClass="mr10 ml10"
                            Text="Hightlight LTL Density Fields" CausesValidation="False" Visible="false" />
                    </td>
                    <td class='<%= string.IsNullOrEmpty(litPackage.Text) ? "hidden" : string.Empty %> <%= !string.IsNullOrEmpty(litFTL.Text) ? "border-right" : string.Empty %>'>
                        <asp:Button runat="server" ID="btnHighlightSmallPackFields" OnClick="OnCancelClicked" CssClass="mr10 ml10"
                            Text="Hightlight Small Package Fields" CausesValidation="False" Visible="false" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btnHighlightFTLFields" OnClick="OnCancelClicked" CssClass="mr10 ml10"
                            Text="Hightlight FTL Fields" CausesValidation="False" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="pt10">
                        <asp:Literal runat="server" ID="litPcfError" Visible="False" />
                    </td>
                </tr>
            </table>
        </div>

        <div class="row text-center">
            <asp:Button runat="server" ID="btnClose" Text="Enter Missing Information" OnClick="OnCancelClicked" Visible="False" CausesValidation="False" />
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlCertifications" Visible="true">
        <div class="row pt10 pb10">
            <div class="fieldgroup pl10">
                <label class="upper blue">Certifications:</label>
                <asp:Image runat="server" AlternateText="TSA Certified" ToolTip="TSA Certified" ImageUrl="~/images/icons2/greenCheck.png" Width="16px" CssClass="middle" />
                <label class="upper">= TSA</label>
                <asp:Image runat="server" AlternateText="CTPAT Certified" ToolTip="CTPAT Certified" ImageUrl="~/images/icons2/purpleCheck.png" Width="16px" CssClass="middle" />
                <label class="upper">= CTPAT</label>
                <asp:Image runat="server" AlternateText="SmartWay Certified" ToolTip="SmartWay Certified" ImageUrl="~/images/icons2/blueCheck.png" Width="16px" CssClass="middle" />
                <label class="upper">= SmartWay</label>
                <asp:Image runat="server" AlternateText="PIP Certified" ToolTip="PIP Certified" ImageUrl="~/images/icons2/orangeCheck.png" Width="16px" CssClass="middle" />
                <label class="upper">= PIP</label>
            </div>
            <div class="fieldgroup right middle">
                <p class="fs75em note pt5 pr10">&#8226; Click the carrier logo or link to display terminal information</p>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        function ToggleChargeBreakdownVisibility(index) {
            $(jsHelper.AddHashTag('divCharges' + index)).slideToggle("fast");
            $(jsHelper.AddHashTag('hrCharges' + index)).slideToggle("fast");
            $(jsHelper.AddHashTag('divP44RatingInformation' + index)).slideToggle("fast");
            $(jsHelper.AddHashTag('divSMCRatingInformation' + index)).slideToggle("fast");
            $(jsHelper.AddHashTag('divTruckloadRouteDetails' + index)).slideToggle("fast");
        }
    </script>
    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheRateResultsTable" TableId="rateResultsTable" ScrollerContainerId="rateResultsScrollSection" ScrollOffsetControlId="pnlRateSelection"
        IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
    <asp:ListView ID="lstRates" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnRatesItemDataBound">
        <LayoutTemplate>
            <div class="finderScroll" id="rateResultsScrollSection">
                <table class="stripe" id="rateResultsTable">
                    <tr>
                        <th style="width: 7%;" class="text-center"></th>
                        <th style="width: 23%">Carrier/Vendor
                        </th>
                        <th style="width: 5%;" class="text-center">Mode
                        </th>
                        <th style="width: 8%;" class="text-center">Certifications
                        </th>
                        <th style="width: 8%;" class="text-right">Freight ($)
                        </th>
                        <th style="width: 8%;" class="text-right">Fuel ($)
                        </th>
                        <th style="width: 8%;" class="text-right">Accessorial ($)
                        </th>
                        <th style="width: 8%;" class="text-right">Service ($)
                        </th>
                        <th style="width: 9%;" class="text-right">
                            <asp:LinkButton ID="lbtnTotal" runat="server" Text="Total ($)" OnClick="OnSortByTotalClicked" CausesValidation="False"
                                ToolTip="Sort by Total Costs" CssClass="white" />
                        </th>
                        <th style="width: 7%;" class="text-center">
                            <asp:LinkButton ID="lbtnTransit" runat="server" Text="Transit" OnClick="OnSortByTransitClicked" CausesValidation="False"
                                ToolTip="Sort by Transit Days" CssClass="white" />
                        </th>
                        <th style="width: 7%;">
                            <asp:LinkButton ID="lbtnEstimatedDelivery" runat="server" Text="Est. Del." OnClick="OnSortByEstimatedDevlieryClicked" CausesValidation="False"
                                ToolTip="Sort by Estimated Delivery Date" CssClass="white" />
                        </th>
                    </tr>
                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                </table>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td class="text-center top">
                    <eShip:CustomHiddenField runat="server" ID="hidItemIndex" Value='<%# Container.DataItemIndex %>' />

                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upPnlRateInformation">
                        <ContentTemplate>
                            <eShip:CustomHiddenField runat="server" ID="hidServiceMode" Value='<%# Eval("Mode") %>' />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <eShip:CustomHiddenField runat="server" ID="hidLTLSellRateId" Value='<%# Eval("LTLSellRateId") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidOriginalRateValue" Value='<%# Eval("OriginalRateValue") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidDiscountTierId" Value='<%# Eval("DiscountTierId") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidCFCRuleId" Value='<%# Eval("CFCRuleId") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidFreightProfitAdjustment" Value='<%# Eval("FreightProfitAdjustment") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidFuelProfitAdjustment" Value='<%# Eval("FuelProfitAdjustment") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidAccessorialProfitAdjustment" Value='<%# Eval("AccessorialProfitAdjustment") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidServiceProfitAdjustment" Value='<%# Eval("ServiceProfitAdjustment") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerFreightMarkup" Value='<%# Eval("ResellerFreightMarkup") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerFreightMarkupIsPercent" Value='<%# Eval("ResellerFreightMarkupIsPercent") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerFuelMarkup" Value='<%# Eval("ResellerFuelMarkup") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerFuelMarkupIsPercent" Value='<%# Eval("ResellerFuelMarkupIsPercent") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerAccessorialMarkup" Value='<%# Eval("ResellerAccessorialMarkup") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerAccessorialMarkupIsPercent" Value='<%# Eval("ResellerAccessorialMarkupIsPercent") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerServiceMarkup" Value='<%# Eval("ResellerServiceMarkup") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidResellerServiceMarkupIsPercent" Value='<%# Eval("ResellerServiceMarkupIsPercent") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidDirectPointRate" Value='<%# Eval("DirectPointRate") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidApplyDiscount" Value='<%# Eval("ApplyDiscount") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidSmallPackType" Value='<%# Eval("SmallPackServiceType") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidSmallPackageEngine" Value='<%# Eval("SmallPackEngine") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidBilledWeight" Value='<%# Eval("BilledWeight") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidRatedWeight" Value='<%# Eval("RatedWeight") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidRatedCubicFeet" Value='<%# Eval("RatedCubicFeet") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidFuelMarkupPercent" Value='<%# Eval("FuelMarkupPercent") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidMileage" Value='<%# Eval("Mileage") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidMileageSourceId" Value='<%# Eval("MileageSourceId") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidOriginTerminalCode" Value='<%# Eval("OriginTerminalCode") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidDestinationTerminalCode" Value='<%# Eval("DestinationTerminalCode") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidLTLPackageSpecificRate" Value='<%# Eval("IsLTLPackageSpecificRate") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidOverlengthRuleAppield" Value='<%# Eval("OverlengthRuleApplied").ToString() %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidTotalActualShipmentWeight" Value='<%# Eval("TotalActualShipmentWeight").ToString() %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidMarkupPercent" Value='<%# Eval("MarkupPercent").ToString() %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidMarkupValue" Value='<%# Eval("MarkupValue").ToString() %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidProject44QuoteNumber" Value='<%# Eval("Project44QuoteNumber") %>' />
                    <eShip:CustomHiddenField runat="server" ID="hidIsProject44Rate" Value='<%# Eval("IsProject44Rate") %>' />

                    <asp:Image runat="server" ID="imgMissingInput" ImageUrl="~/images/icons2/flag-red.png" ToolTip="Missing/Bad data in required fields for most accurate rate" Width="16px"
                        Visible='<%# MissingInputForMode(Eval("Mode"), Eval("IsLTLDensityRate")) %>' />
                    <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked" CausesValidation="false"
                        Visible='<%# !MissingInputForMode(Eval("Mode"), Eval("IsLTLDensityRate")) && !DisableRateSelection %>' />
                </td>
                <td class="top" rowspan='<%# Eval("LTLSellRate") == null || string.IsNullOrEmpty(Eval("LTLSellRate.VendorRating.RatingDetailNotes").GetString()) ? "3" : "4" %>'>
                    <eShip:CustomHiddenField runat="server" ID="hidVendorId" />
                    <eShip:CustomHiddenField runat="server" ID="hidVendorScac" />
                    <div class="row">
                        <asp:Panel runat="server" ID="pnlVendorLogo">
                            <div class="row logo-vendor">
                                <asp:ImageButton runat="server" ID="imgVendorLogo" OnClick="OnVendorClicked" CausesValidation="False" ImageAlign="AbsMiddle" CssClass="pl0" />
                            </div>
                            <div class="row fs75em">
                                <label class="table-note2 ml4">
                                    <asp:Literal runat="server" ID="litVendor" />
                                </label>
                            </div>
                        </asp:Panel>
                        <asp:LinkButton runat="server" ID="lnkVendor" OnClick="OnVendorClicked" CausesValidation="False" CssClass="blue" />
                    </div>
                    <div <%# Eval("CFCRuleId").ToLong() != default(long) ? "class='fs75em row pt5'" : "class='hidden row pt5'" %>>
                        * Cubic foot adjustments
                    </div>
                    <div <%# Eval("OverlengthRuleApplied").ToBoolean() ? "class='fs75em row pt5'" : "class='hidden row pt5'" %>>
                        * Overlength piece
                    </div>
                </td>
                <td class="text-center top">
                    <%# GetServiceModeDescription(Eval("Mode"))%>
                </td>
                <td class="top">
                    <asp:Image runat="server" ID="imgTsaCertified" ImageUrl="~/images/icons2/greenCheck.png" AlternateText="TSA Certified" ToolTip="TSA Certified" Width="16px" />
                    <asp:Image runat="server" ID="imgCtpatCertified" ImageUrl="~/images/icons2/purpleCheck.png" AlternateText="CTPAT Certified" ToolTip="CTPAT Certified" Width="16px" />
                    <asp:Image runat="server" ID="imgSmartWayCertified" ImageUrl="~/images/icons2/blueCheck.png" AlternateText="SmartWay Certified" ToolTip="SmartWay Certified" Width="16px" />
                    <asp:Image runat="server" ID="imgPIPCertified" ImageUrl="~/images/icons2/orangeCheck.png" AlternateText="PIP Certified" ToolTip="PIP Certified" Width="16px" />
                </td>
                <td class="text-right top">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upPnlTotalFreightCharge">
                        <ContentTemplate>
                            <asp:Literal runat="server" ID="litTotalFreightCharge" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="text-right top">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upPnlTotalFuelCharge">
                        <ContentTemplate>
                            <asp:Literal runat="server" ID="litTotalFuelCharge" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="text-right top">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upPnlTotalAccessorialCharge">
                        <ContentTemplate>
                            <asp:Literal runat="server" ID="litTotalAccessorialCharge" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="text-right top">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upPnlTotalServiceCharge">
                        <ContentTemplate>
                            <asp:Literal runat="server" ID="litTotalServiceCharge" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="text-right top">
                    <asp:LinkButton runat="server" ID="lnkChargeBreakdown" ToolTip="See Charge Breakdown" Text="[+]" CssClass="blue link_nounderline right" />
                    <div class="right">
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upPnlTotalOverallCharge">
                            <ContentTemplate>
                                <asp:Literal runat="server" ID="litTotalOveralCharge" />&nbsp;
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </td>
                <td class="text-center top">
                    <asp:Literal runat="server" ID="litTransitTime" />
                </td>
                <td class="top">
                    <asp:Literal runat="server" ID="litEstimatedDeliveryDate" />
                </td>
            </tr>
            <tr class="hidden">
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="1" class="p_m0"></td>
                <td colspan="7" class="p_m0">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upnlGuaranteedServices">
                        <ContentTemplate>
                            <asp:ListView runat="server" ID="lstGuaranteedServices" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <div class="sm_chk row pb5">
                                        <div class="fieldgroup">
                                            <label class="lhInherit table-note2">Guaranteed By </label>
                                        </div>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    </div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <div class="fieldgroup pr10">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkGuaranteedServiceEnabled" AutoPostBack="True" OnCheckedChanged="OnGuaranteedServiceEnabledCheckChanged" />
                                        <label class="lhInherit table-note2 ml0">
                                            <asp:Literal runat="server" ID="litLTLGuaranteedChargeDescription" Text='<%# Eval("Description") %>' />
                                        </label>
                                        <eShip:CustomHiddenField runat="server" ID="hidLTLGuaranteedChargeId" Value='<%# Eval("Id") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidProject44ChargeCode" Value='<%# Eval("Project44ChargeCode") %>' />
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td colspan="2" class="p_m0"></td>
            </tr>
            <tr class="hidden">
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="1" class="p_m0"></td>
                <td colspan="7" class="p_m0">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upnlExpeditedServices">
                        <ContentTemplate>
                            <asp:ListView runat="server" ID="lstExpeditedServices" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <div class="sm_chk row pb5">
                                        <div class="fieldgroup">
                                            <label class="lhInherit table-note2">Expedited Service:</label>
                                        </div>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    </div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <div class="fieldgroup pr10">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkExpeditedServiceEnabled" AutoPostBack="True" OnCheckedChanged="OnExpeditedServiceEnabledCheckChanged" />
                                        <label class="lhInherit table-note2 ml0">
                                            <asp:Literal runat="server" ID="litExpeditedServiceDescription" Text='<%# Eval("Description") %>' />
                                        </label>
                                        <eShip:CustomHiddenField runat="server" ID="hidProject44ChargeCode" Value='<%# Eval("Project44ChargeCode") %>' />
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td colspan="2" class="p_m0"></td>
            </tr>
            <tr class="hidden">
                <td>&nbsp;</td>
            </tr>
            <tr class='<%# Eval("LTLSellRate") == null || string.IsNullOrEmpty(Eval("LTLSellRate.VendorRating.RatingDetailNotes").GetString()) ? "hidden" : string.Empty %>'>
                <td colspan="1" class="p_m0"></td>
                <td colspan="8" class="p_m0">
                    <hr class="dotted mb10 ml0 mr0" />
                    <label class="lhInherit table-note pb5">
                        <asp:Literal runat="server" ID="litVendorRatingRatingDetailNote" />
                    </label>
                    <hr style="display: none;" id="hrCharges<%# Container.DataItemIndex %>" class='dotted ml0 mr0 mt10' />
                </td>
                <td class="p_m0"></td>
            </tr>
            <tr class="hidden">
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="20" class="p_m0">
                    <div id="divCharges<%# Container.DataItemIndex %>" style="display: none;" class="mb5 border-bottom">
                        <h6 class="text-center pb5">Charge Breakdown</h6>
                        <div class="row">
                            <div class="left" style="width: 10%">
                                <label class="blue pl10 pr10">Code</label>
                            </div>
                            <div class="left" <%# (ServiceMode)Eval("Mode") == ServiceMode.Truckload ? "style='width: 24%'" : "style='width: 52%'" %>>
                                <label class="blue pl10 pr10">Description</label>
                            </div>
                            <asp:Panel runat="server" Visible='<%# (ServiceMode)Eval("Mode") == ServiceMode.Truckload%>'>
                                <div class="left" style="width: 28%">
                                    <label class="blue pl10 pr10">Comment</label>
                                </div>
                            </asp:Panel>
                            <div class="left" style="width: 10%">
                                <label class="blue pl10 pr10">Category</label>
                            </div>
                            <div class="left text-right" style="width: 8%">
                                <label class="blue pl10 pr10">Quantity</label>
                            </div>
                            <asp:Panel runat="server" Visible='<%# ActiveUser.TenantEmployee && Eval("Mode").ToString().ToEnum<ServiceMode>() != ServiceMode.Truckload%>'>
                                <div class="left text-right" style="width: 10%">
                                    <label class="blue pl10 pr10">Cost ($)</label>
                                </div>
                            </asp:Panel>
                            <div class="left text-right" <%# ActiveUser.TenantEmployee && Eval("Mode").ToString().ToEnum<ServiceMode>() != ServiceMode.Truckload ?  "style='width:10%'" : "style='width:20%'" %>>
                                <label class="blue pl10 pr10">Charge ($)</label>
                            </div>
                        </div>
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upPnlCharges">
                            <ContentTemplate>
                                <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <div class='row border-top <%# Eval("Visible").ToBoolean() ? string.Empty : "hidden" %>'>
                                            <eShip:CustomHiddenField runat="server" ID="hidChargeCodeId" Value='<%# Eval("ChargeCodeId") %>' />
                                            <eShip:CustomHiddenField runat="server" ID="hidUnitBuy" Value='<%# Eval("UnitBuy") %>' />
                                            <eShip:CustomHiddenField runat="server" ID="hidUnitSell" Value='<%# Eval("UnitSell") %>' />
                                            <eShip:CustomHiddenField runat="server" ID="hidUnitDiscount" Value='<%# Eval("UnitDiscount") %>' />
                                            <eShip:CustomHiddenField runat="server" ID="hidChargeCodeComment" Value='<%# Eval("Comment") %>' />
                                            <eShip:CustomHiddenField runat="server" ID="hidLTLGuaranteedChargeId" Value='<%# Eval("LTLGuaranteedChargeId") %>' />
                                            <eShip:CustomHiddenField runat="server" ID="hidIsProject44GuaranteedCharge" Value='<%# Eval("IsProject44GuaranteedCharge") %>' />
                                            <eShip:CustomHiddenField runat="server" ID="hidProject44QuoteNumber" Value='<%# Eval("Project44QuoteNumber") %>' />
                                            <eShip:CustomHiddenField runat="server" ID="hidIsProject44ExpeditedServiceCharge" Value='<%# Eval("IsProject44ExpeditedServiceCharge") %>' />

                                            <div class="left border-right" style="width: 10%">
                                                <label class="pl10 pr10"><%# Eval("Code") %></label>
                                            </div>
                                            <div class="left border-right" <%# (ServiceMode)Eval("Mode") == ServiceMode.Truckload ? "style='width: 24%'" : "style='width: 52%'" %>>
                                                <label class="pl10 pr10"><%# Eval("Description") %></label>
                                            </div>
                                            <asp:Panel runat="server" Visible='<%# Eval("Mode").ToString().ToEnum<ServiceMode>() == ServiceMode.Truckload%>'>
                                                <div style="width: 28%" class="left border-right <%# Eval("Mode").ToString().ToEnum<ServiceMode>() == ServiceMode.Truckload ? string.Empty : "hidden" %>">
                                                    <label class="pl10 pr10"><%# Eval("Comment") %></label>
                                                </div>
                                            </asp:Panel>
                                            <div class="left border-right" style="width: 10%">
                                                <label class="pl10 pr10"><%# Eval("Category")%></label>
                                            </div>
                                            <div class="left text-right border-right" style="width: 8%">
                                                <label class="pl10 pr10">
                                                    <asp:Literal runat="server" ID="litQuantity" Text='<%# Eval("Quantity") %>' />
                                                </label>
                                            </div>
                                            <asp:Panel runat="server" Visible='<%# ActiveUser.TenantEmployee && Eval("Mode").ToString().ToEnum<ServiceMode>() != ServiceMode.Truckload %>'>
                                                <div class="left text-right border-right" style="width: 10%">
                                                    <label class="pl10 pr10"><%# Eval("FinalBuy", "{0:n2}")%></label>
                                                </div>
                                            </asp:Panel>
                                            <div class="left text-right" <%# ActiveUser.TenantEmployee && Eval("Mode").ToString().ToEnum<ServiceMode>() != ServiceMode.Truckload ?  "style='width:9%'" : "style='width:19%'" %>>
                                                <label class="pl10 pr10"><%# Eval("AmountDue", "{0:n2}") %></label>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </td>
            </tr>
            <tr class="hidden">
                <td>&nbsp;</td>
            </tr>
            <asp:Panel runat="server" Visible='<%# !ActiveUser.TenantEmployee && Eval("Mode").ToString().ToEnum<ServiceMode>() != ServiceMode.Truckload %>'>
                <tr class="hidden">
                    <td>&nbsp;</td>
                </tr>
            </asp:Panel>
            <asp:Panel runat="server" Visible='<%# ActiveUser.TenantEmployee && Eval("Mode").ToString().ToEnum<ServiceMode>() != ServiceMode.Truckload && Eval("IsProject44Rate").ToBoolean() %>'>
                <tr class="bottom_shadow">
                    <td colspan="20" class="m0 pl0 pt0 pr0 pb5">
                        <div class="rowgroup mb10" id="divP44RatingInformation<%# Container.DataItemIndex %>" style="display: none;">
                            <div class="col_1_2">
                                <div class="col_1_2">
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Project 44 Value ($):</label>
                                        <label class="lhInherit"><%# Eval("OriginalRateValue", "{0:n2}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Direct Point Rate: </label>
                                        <label class="lhInherit">
                                            <%# Eval("DirectPointRate").ToBoolean() ?  WebApplicationConstants.Yes : WebApplicationConstants.No%>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Freight Profit Adj.: </label>
                                        <label class="lhInherit"><%# Eval("FreightProfitAdjustment", "{0:n4}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">R. Freight (<%# Eval("ResellerFreightMarkupIsPercent").ToBoolean() ? "%" : "$"%>): </label>
                                        <label class="lhInherit"><%# Eval("ResellerFreightMarkup", "{0:n2}")%></label>
                                    </div>
                                </div>
                                <div class="col_1_2">
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Cust. Markup (%): </label>
                                        <label class="lhInherit"><%# Eval("MarkupPercent").ToDecimal().ToString("f2")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Cust. Markup Value ($): </label>
                                        <label class="lhInherit"><%# Eval("MarkupValue").ToDecimal().ToString("f2")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Fuel Profit Adj.: </label>
                                        <label class="lhInherit"><%# Eval("FuelProfitAdjustment", "{0:n4}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">R. Fuel (<%# Eval("ResellerFuelMarkupIsPercent").ToBoolean() ? "%" : "$"%>): </label>
                                        <label class="lhInherit"><%# Eval("ResellerFuelMarkup", "{0:n2}")%></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2">
                                <div class="col_1_2">
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upnlProject44QuoteNumber">
                                        <ContentTemplate>
                                            <div class="row">
                                                <label class="blue fs85em w130 text-right">Project 44 Quote Number: </label>
                                                <label class="lhInherit">
                                                    <asp:Literal runat="server" ID="litProject44QuoteNumber" Text='<%# Eval("Project44QuoteNumber")%>' /></label>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Accessorial Profit Adj.: </label>
                                        <label class="lhInherit"><%# Eval("AccessorialProfitAdjustment", "{0:n4}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">R. Accessorial (<%# Eval("ResellerAccessorialMarkupIsPercent").ToBoolean() ? "%" : "$"%>): </label>
                                        <label class="lhInherit"><%# Eval("ResellerAccessorialMarkup", "{0:n2}")%></label>
                                    </div>
                                </div>
                                <div class="col_1_2">
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Service Profit Adj.: </label>
                                        <label class="lhInherit"><%# Eval("ServiceProfitAdjustment", "{0:n4}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">R. Service (<%# Eval("ResellerServiceMarkupIsPercent").ToBoolean() ? "%" : "$"%>): </label>
                                        <label class="lhInherit"><%# Eval("ResellerServiceMarkup", "{0:n2}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Rated Weight (lb): </label>
                                        <label class="lhInherit"><%# Eval("RatedWeight", "{0:n2}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Rated Cubic Feet: </label>
                                        <label class="lhInherit"><%# Eval("RatedCubicFeet", "{0:n2}")%></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel runat="server" Visible='<%# ActiveUser.TenantEmployee && Eval("Mode").ToString().ToEnum<ServiceMode>() != ServiceMode.Truckload && !Eval("IsProject44Rate").ToBoolean() %>'>
                <tr class="bottom_shadow">
                    <td colspan="20" class="m0 pl0 pt0 pr0 pb5">
                        <div class="rowgroup mb10" id="divSMCRatingInformation<%# Container.DataItemIndex %>" style="display: none;">
                            <div class="col_1_2">
                                <div class="col_1_2">
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">SMC Value ($):</label>
                                        <label class="lhInherit"><%# Eval("OriginalRateValue", "{0:n2}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Fuel Markup (%): </label>
                                        <label class="lhInherit"><%# Eval("FuelMarkupPercent", "{0:n2}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Direct Point Rate: </label>
                                        <label class="lhInherit">
                                            <%# Eval("DirectPointRate").ToBoolean() ?  WebApplicationConstants.Yes : WebApplicationConstants.No%>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Freight Profit Adj.: </label>
                                        <label class="lhInherit"><%# Eval("FreightProfitAdjustment", "{0:n4}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">R. Freight (<%# Eval("ResellerFreightMarkupIsPercent").ToBoolean() ? "%" : "$"%>): </label>
                                        <label class="lhInherit"><%# Eval("ResellerFreightMarkup", "{0:n2}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">CFC Applies: </label>
                                        <label class="lhInherit"><%# Eval("CFCRuleId").ToLong() != default(long) ?  WebApplicationConstants.Yes : WebApplicationConstants.No%></label>
                                    </div>
                                </div>
                                <div class="col_1_2">
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Vendor Disc. (%): </label>
                                        <label class="lhInherit"><%# (((DiscountTier)Eval("DiscountTier")) ?? new DiscountTier()).DiscountPercent.ToString("f2") %></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Billed Weight (lb): </label>
                                        <label class="lhInherit"><%# Eval("BilledWeight", "{0:n2}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Cust. Markup (%): </label>
                                        <label class="lhInherit"><%# Eval("MarkupPercent").ToDecimal().ToString("f2")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Fuel Profit Adj.: </label>
                                        <label class="lhInherit"><%# Eval("FuelProfitAdjustment", "{0:n4}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">R. Fuel (<%# Eval("ResellerFuelMarkupIsPercent").ToBoolean() ? "%" : "$"%>): </label>
                                        <label class="lhInherit"><%# Eval("ResellerFuelMarkup", "{0:n2}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Rated Weight (lb): </label>
                                        <label class="lhInherit"><%# Eval("RatedWeight", "{0:n2}")%></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2">
                                <div class="col_1_2">
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Vendor Floor-Ceiling ($): </label>
                                        <label class="lhInherit">
                                            <%# (((DiscountTier)Eval("DiscountTier")) ?? new DiscountTier()).FloorValue.ToString("f2") %>
										            -<%# (((DiscountTier)Eval("DiscountTier")) ?? new DiscountTier()).CeilingValue.ToString("f2") %>
                                        </label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Mileage: </label>
                                        <label class="lhInherit"><%# Eval("Mileage", "{0:n2}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Cust. Markup Value ($): </label>
                                        <label class="lhInherit"><%# Eval("MarkupValue").ToDecimal().ToString("f2")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Accessorial Profit Adj.: </label>
                                        <label class="lhInherit"><%# Eval("AccessorialProfitAdjustment", "{0:n4}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">R. Accessorial (<%# Eval("ResellerAccessorialMarkupIsPercent").ToBoolean() ? "%" : "$"%>): </label>
                                        <label class="lhInherit"><%# Eval("ResellerAccessorialMarkup", "{0:n2}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Rated Cubic Feet: </label>
                                        <label class="lhInherit"><%# Eval("RatedCubicFeet", "{0:n2}")%></label>
                                    </div>
                                </div>
                                <div class="col_1_2">
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Apply Disc.: </label>
                                        <label class="lhInherit"><%# Eval("ApplyDiscount").ToBoolean() ?  WebApplicationConstants.Yes : WebApplicationConstants.No %></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Pkg. Spc. Rate: </label>
                                        <label class="lhInherit"><%# Eval("IsLTLPackageSpecificRate").ToBoolean() ? WebApplicationConstants.Yes : WebApplicationConstants.No %></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Use Lower: </label>
                                        <label class="lhInherit"><%# (((LTLSellRate)Eval("LTLSellRate")) ?? new LTLSellRate()).UseMinimum ?  WebApplicationConstants.Yes : WebApplicationConstants.No%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">Service Profit Adj.: </label>
                                        <label class="lhInherit"><%# Eval("ServiceProfitAdjustment", "{0:n4}")%></label>
                                    </div>
                                    <div class="row">
                                        <label class="blue fs85em w130 text-right">R. Service (<%# Eval("ResellerServiceMarkupIsPercent").ToBoolean() ? "%" : "$"%>): </label>
                                        <label class="lhInherit"><%# Eval("ResellerServiceMarkup", "{0:n2}")%></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel runat="server" Visible='<%# Eval("Mode").ToString().ToEnum<ServiceMode>() == ServiceMode.Truckload %>'>
                <tr class="bottom_shadow">
                    <td colspan="20" class="m0 pt0 pl0 pr0 pb15">
                        <div id="divTruckloadRouteDetails<%# Container.DataItemIndex %>" style="display: none;" class="pb5">
                            <h6 class="text-center">Truckload Route Details</h6>
                            <div class="row">
                                <div class="left" style="width: 25%">
                                    <label class="blue pl10 pr10">Origin</label>
                                </div>
                                <div class="left" style="width: 25%">
                                    <label class="blue pl10 pr10">Destination</label>
                                </div>
                                <div class="left" style="width: 10%">
                                    <label class="blue pl10 pr10">Rate</label>
                                </div>
                                <div class="left" style="width: 15%">
                                    <label class="blue pl10 pr10">Rate Type</label>
                                </div>
                                <div class="left" style="width: 10%">
                                    <label class="blue pl10 pr10">Tariff Type</label>
                                </div>
                                <div class="left" style="width: 15%">
                                    <label class="blue pl10 pr10">Mileage</label>
                                </div>
                            </div>
                        </div>
                        <asp:Repeater runat="server" ID="rptTlRouteDetails">
                            <ItemTemplate>
                                <div class="row border-top">
                                    <div class="left border-right" style="width: 25%">
                                        <eShip:CustomHiddenField runat="server" ID="hidTlRateDetailOriginFullAddress" Value='<%# Eval("OriginFullAddress") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidTlRateDetailDestinationFullAddress" Value='<%# Eval("DestinationFullAddress") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidTlRateDetailRateType" Value='<%# Eval("RateType") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidTlRateDetailRate" Value='<%# Eval("Rate") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidTlRateDetailTariffType" Value='<%# Eval("TariffType") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidTlRateDetailMileage" Value='<%# Eval("Mileage") %>' />
                                        <label class="pl10 pr10">
                                            <%# Eval("OriginFullAddress") %>
                                        </label>
                                    </div>
                                    <div class="left border-right" style="width: 25%">
                                        <label class="pl10 pr10">
                                            <%# Eval("DestinationFullAddress") %>
                                        </label>
                                    </div>
                                    <div class="left text-right border-right" style="width: 10%">
                                        <label class="pl10 pr10">
                                            <%# Eval("Rate", "{0:n2}") %>
                                        </label>
                                    </div>
                                    <div class="left border-right" style="width: 15%">
                                        <label class="pl10 pr10">
                                            <%# Eval("RateType").FormattedString() %>
                                        </label>
                                    </div>
                                    <div class="left border-right" style="width: 10%">
                                        <label class="pl10 pr10">
                                            <%# Eval("TariffType").FormattedString() %>
                                        </label>
                                    </div>
                                    <div class="left text-right" style="width: 14%">
                                        <label class="pl10 pr10">
                                            <%# Eval("Mileage", "{0:n2}") %>
                                        </label>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </asp:Panel>
        </ItemTemplate>
    </asp:ListView>
    <asp:Panel runat="server" ID="pnlNoRates" Visible="False">
        <div class="row">
            <div class="fieldgroup pl20 pt20 pr20">
                Due to the size of your shipment or postal code combination of your request we are
			unable to return a rate immediately. Our specialists can work with our carriers
			to get you rates and service options.<br />
                <br />
                Please submit the request and we will respond to you as quickly as possible with your rate and service options. Thank you.
            </div>

        </div>
        <div class="row text-center pt20 pb20">
            <asp:Button runat="server" ID="btnCloseRateSelection" Text="Close Rate Selection" OnClick="OnCancelClicked" CausesValidation="False" />
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlError" Visible="False">
        <div class="row">
            <div class="fieldgroup pl20 pr20 pt20">
                There was an error during the rating process. Our IT department has been contacted with the problem. Please contact an 
            operations specialist for further assistance.<br />
                <br />
                Thank you.
            </div>
        </div>
        <div class="row text-center pt20 pb20">
            <asp:Button runat="server" ID="btnErrorClose" Text="Close" OnClick="OnCancelClicked" Visible="False" CausesValidation="False" />
        </div>
    </asp:Panel>
</asp:Panel>

<asp:Panel runat="server" ID="pnlTerminalInformation" Visible="False" CssClass="popupControlOver popupControlOverW750">
    <div class="popheader">
        <h4>
            <asp:Literal runat="server" ID="litTerminalInformationHeader" />
        </h4>
        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false"
            OnClick="OnCloseTerminalInformationClicked" runat="server" />
    </div>
    <div class="rowgroup pt10">
        <div class="col_1_2 bbox vlinedarkright pl10">
            <h5>Origin</h5>
            <div class="row">
                <div class="fieldgroup">
                    <label>
                        <asp:Literal runat="server" ID="litOrigin" /></label>
                </div>
            </div>
        </div>
        <div class="col_1_2 bbox pl46">
            <h5>Destination</h5>
            <div class="row">
                <div class="fieldgroup">
                    <label>
                        <asp:Literal runat="server" ID="litDestination" /></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row pb10">
        <div class="fieldgroup right">
            <asp:Button runat="server" ID="btnCloseTerminalInfomation" OnClick="OnCloseTerminalInformationClicked" Text="Close" CausesValidation="False" />
        </div>
    </div>
</asp:Panel>

<asp:Panel ID="pnlTerminalInformationDimScreen" CssClass="dimBackgroundControlOver" runat="server" Visible="false" />
<asp:Panel ID="pnlRateSelectionDisplayDimScreen" CssClass="dimBackground" runat="server" Visible="false" />

<asp:UpdatePanel runat="server" UpdateMode="Always">
    <ContentTemplate>
        <eShip:CustomHiddenField runat="server" ID="hidDisableRateSelection" />
        <eShip:CustomHiddenField runat="server" ID="hidSortFlag" />
        <eShip:CustomHiddenField runat="server" ID="hidItemsLTLFreightClassInValid" />
        <eShip:CustomHiddenField runat="server" ID="hidItemsLTLDensityInValid" />
        <eShip:CustomHiddenField runat="server" ID="hidItemsFTLInValid" />
        <eShip:CustomHiddenField runat="server" ID="hidItemsPackageInValid" />
        <eShip:CustomHiddenField runat="server" ID="hidOriginCountryId" />
        <eShip:CustomHiddenField runat="server" ID="hidOriginPostalCode" />
        <eShip:CustomHiddenField runat="server" ID="hidDestinationPostalCode" />
        <eShip:CustomHiddenField runat="server" ID="hidTotalWeight" />
        <eShip:CustomHiddenField runat="server" ID="hidHighestFreightClass" />
        <eShip:CustomHiddenField runat="server" ID="hidDestinationCountryId" />
        <eShip:CustomHiddenField runat="server" ID="hidShipmentDate" />
        <eShip:CustomHiddenField runat="server" ID="hidEnableHightlightMissingInformationButtons" />
    </ContentTemplate>
</asp:UpdatePanel>
