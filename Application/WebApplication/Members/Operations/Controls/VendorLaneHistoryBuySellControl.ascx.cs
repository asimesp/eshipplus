﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class VendorLaneHistoryBuySellControl : MemberControlBase
    {
        public const string Ltl = "Ltl";
        public const string Tl = "Tl";
        public const string Air = "Air";
        public const string Rail = "Rail";
        public const string SmallPack = "SmallPack"; 

        public new bool Visible
        {
            get { return pnlVendorLaneHistory.Visible; }
            set
            {
                base.Visible = value;
                pnlVendorLaneHistory.Visible = value;
                pnlVendorLaneHistoryDimScreen.Visible = value;
            }
        }

        public event EventHandler VendorLaneHistoryClose;

        public void LoadVendorLaneHistory(LaneHistorySearchCriteria criteria)
        {
            hidOriginPostalCode.Value = criteria.OriginPostalCode;
            hidOriginCountryId.Value = criteria.OriginCountryId.ToString();
            hidDestinationPostalCode.Value = criteria.DestinationPostalCode;
            hidDestinationCountryId.Value = criteria.DestinationCountryId.ToString();
            hidTotalWeight.Value = criteria.TotalWeight.ToString();
            hidDesiredPickupDate.Value = criteria.DesiredPickupDate.ToString();

            txtMileageRadius.Text = 5.ToString();
            criteria.Radius = txtMileageRadius.Text.ToDouble();

            Search(criteria);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            pnlError.Visible = false;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            var criteria = new LaneHistorySearchCriteria
            {
                OriginPostalCode = hidOriginPostalCode.Value,
                OriginCountryId = hidOriginCountryId.Value.ToLong(),
                DestinationPostalCode = hidDestinationPostalCode.Value,
                DestinationCountryId = hidDestinationCountryId.Value.ToLong(),
                TotalWeight = hidTotalWeight.Value.ToDecimal(),
                DesiredPickupDate = hidDesiredPickupDate.Value.ToDateTime(),
                Radius = txtMileageRadius.Text == string.Empty ? -1d : txtMileageRadius.Text.ToDouble()
            };

            Search(criteria);
        }

        protected void OnCloseClicked(object sender, ImageClickEventArgs e)
        {
            if (VendorLaneHistoryClose != null)
                VendorLaneHistoryClose(this, new EventArgs());
        }


        protected void OnVendorNameClick(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var vendorId = button.Parent.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong();
            vendorInfoControl.DisplayVendorInformation(vendorId, hidOriginPostalCode.Value, hidOriginCountryId.Value.ToLong(), hidDestinationPostalCode.Value, hidDestinationCountryId.Value.ToLong(), DateTime.Today, 0);
            vendorInfoControl.Visible = true;
        }

        protected void OnVendorInfoCancel(object sender, EventArgs e)
        {
            vendorInfoControl.Visible = false;
        }


        private void Search(LaneHistorySearchCriteria criteria)
        {
            try
            {
                var rates = new LaneHistoryRateDto().VendorLaneHistory(ActiveUser.TenantId, criteria);

                if (rates == null) return;

                var ltlRates = new List<LaneHistoryRateDto>();
                var tlRates = new List<LaneHistoryRateDto>();
                var airRates = new List<LaneHistoryRateDto>();
                var railRates = new List<LaneHistoryRateDto>();
                var smallPackRates = new List<LaneHistoryRateDto>();

                foreach (var rate in rates)
                {
                    if (rate.ServiceMode == ServiceMode.LessThanTruckload) ltlRates.Add(rate);
                    if (rate.ServiceMode == ServiceMode.Truckload) tlRates.Add(rate);
                    if (rate.ServiceMode == ServiceMode.Air) airRates.Add(rate);
                    if (rate.ServiceMode == ServiceMode.Rail) railRates.Add(rate);
                    if (rate.ServiceMode == ServiceMode.SmallPackage) smallPackRates.Add(rate);
                }

                lstLTLGroup.DataSource = ltlRates;
                lstLTLGroup.DataBind();

                lstTLGroup.DataSource = tlRates;
                lstTLGroup.DataBind();

                lstAirGroup.DataSource = airRates;
                lstAirGroup.DataBind();

                lstRailGroup.DataSource = railRates;
                lstRailGroup.DataBind();

                lstSmallPackGroup.DataSource = smallPackRates;
                lstSmallPackGroup.DataBind();

                tabLTL.HeaderText = string.Format("Less Than Truckload{0}", ltlRates.BuildTabCount());
                tabTL.HeaderText = string.Format("Truckload{0}", tlRates.BuildTabCount());
                tabAir.HeaderText = string.Format("Air{0}", airRates.BuildTabCount());
                tabRail.HeaderText = string.Format("Rail{0}", railRates.BuildTabCount());
                tabSmallPack.HeaderText = string.Format("Small Package{0}", smallPackRates.BuildTabCount());

                pnlNoRates.Visible = !rates.Any();
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, Context);
                pnlNoRates.Visible = false;
                pnlError.Visible = true;
            }
        }


        protected void OnLTLGroupItemDataBound(object sender, ListViewItemEventArgs e)
        {
            BindRateGroupCharges(e, Ltl);
        }

        protected void OnTLGroupItemDataBound(object sender, ListViewItemEventArgs e)
        {
            BindRateGroupCharges(e, Tl);
        }

        protected void OnAirGroupItemDataBound(object sender, ListViewItemEventArgs e)
        {
            BindRateGroupCharges(e, Air);
        }

        protected void OnRailGroupItemDataBound(object sender, ListViewItemEventArgs e)
        {
            BindRateGroupCharges(e, Rail);
        }

        protected void OnSmallPackGroupItemDataBound(object sender, ListViewItemEventArgs e)
        {
            BindRateGroupCharges(e, SmallPack);
        }


        private static void BindRateGroupCharges(ListViewItemEventArgs e, string type)
        {
            var item = (ListViewDataItem)e.Item;
            var rate = (LaneHistoryRateDto)item.DataItem;

            var lstCharges = item.FindControl("lstCharges").ToListView();

            lstCharges.DataSource = rate.Charges.Values;
            lstCharges.DataBind();

            var lnkChargeBreakdown = item.FindControl("lnkChargeBreakdown").ToLinkButton();
            lnkChargeBreakdown.OnClientClick = string.Format("ToggleChargeBreakdownVisibility({0}, '{1}'); return false;", e.Item.DataItemIndex, type);
        }
    }
}