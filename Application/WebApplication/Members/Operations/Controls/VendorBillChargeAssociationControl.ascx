﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorBillChargeAssociationControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.VendorBillChargeAssociationControl"  %>
<asp:Panel runat="server" ID="pnlVendorBillCharge" Visible="false" CssClass="popupControl popupControlOverW500">
    <div class="popheader mb10">
        <h4>Vendor Bill Charge Association</h4>
        <asp:ImageButton ID="ibtnCloseEditDocument" ImageUrl="~/images/icons2/close.png"
            CssClass="close" CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
    </div>

    <div class="rowgroup">
        <div class="row mb10">
            <script type="text/javascript">
                $(document).ready(function () {
                    // enforce mutual exclusion on checkboxes
                    $(jsHelper.AddHashTag('ulBillOptions input:checkbox')).click(function () {
                        if (jsHelper.IsChecked($(this).attr('id'))) {
                            $(jsHelper.AddHashTag('ulBillOptions input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                jsHelper.UnCheckBox($(this).attr('id'));
                            });
                            $.uniform.update();
                        }
                    });
                });
            </script>
            <h5 class="ml10">Options :</h5>
            <div class="pl40">
                <ul class="twocol_list" id="ulBillOptions">
                    <li>
                        <asp:CheckBox runat="server" ID="chkCreateNewVendorBill" CssClass="jQueryUniform" OnCheckedChanged="OnChargeBillOptionsCheckChanged"
                            AutoPostBack="True" />
                        <label>Create New Vendor Bill</label>
                    </li>
                    <li class="pl40">
                        <asp:CheckBox runat="server" ID="chkAddToVendorBill" CssClass="jQueryUniform" OnCheckedChanged="OnChargeBillOptionsCheckChanged"
                            AutoPostBack="True" />
                        <label>Add to Existing Vendor Bill</label>
                    </li>
                </ul>
            </div>
        </div>

        <asp:UpdatePanel ID="upDataUpdate" runat="server" UpdateMode="Always">
            <ContentTemplate>
                    <div class="row pb20">
                        <table class="poptable">
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Document Number:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtDocumentNumber" CssClass="w200" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="row">
                        <div class="fieldgroup right">
                            <asp:Button runat="server" Text="Create New Bill" ID="btnCreateNewBill" OnClick="OnSaveToBillClicked" CausesValidation="False"
                                Visible="False" CssClass="mr10" />
                            <asp:Button runat="server" Text="Save To Bill" ID="btnSaveToBill" OnClick="OnSaveToBillClicked" CausesValidation="False"
                                Visible="False" CssClass="mr10" />
                            <asp:Button runat="server" Text="Cancel" ID="btnCancel" OnClick="OnCancelClicked" CausesValidation="False" />

                        </div>
                    </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSaveToBill"/>
                <asp:PostBackTrigger ControlID="btnCreateNewBill"/>
                <asp:PostBackTrigger ControlID="btnCancel"/>
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Panel>
<asp:UpdatePanel runat="server" UpdateMode="Always">
    <ContentTemplate>
        <eShip:CustomHiddenField runat="server" ID="hidReferenceNumber" />
        <eShip:CustomHiddenField runat="server" ID="hidReferenceType" />
        <eShip:CustomHiddenField runat="server" ID="hidVendorId" />
    </ContentTemplate>
</asp:UpdatePanel>
