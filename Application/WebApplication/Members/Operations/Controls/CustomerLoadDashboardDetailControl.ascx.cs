﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class CustomerLoadDashboardDetailControl : MemberControlBase
    {
        public int ItemIndex
        {
            get { return hidItemIndex.Value.ToInt(); }
            set { hidItemIndex.Value = value.ToString(); }
        }

        public event EventHandler<ViewEventArgs<DetailRecordInfo>> GenerateShippingLabel;


        public void LoadShipment(ShipmentDashboardDto shipment, List<CheckCall> checkCalls)
        {
            if (shipment == null) return;

            hidShipmentId.Value = shipment.Id.ToString();


            hidRecordType.Value = shipment.Type.GetString();
            if (shipment.Type == DashboardDtoType.Shipment)
            {
                hypBillOfLading.NavigateUrl = DocumentViewer.GenerateShipmentBolLink(shipment);
                ibtnShippingLabel.Visible = true;
            }
            else
            {
                hypBillOfLading.Visible = false;
                ibtnShippingLabel.Visible = false;
            }


            litServiceMode.Text = shipment.ServiceMode.FormattedString();
            litShipmentNumber.Text = shipment.ShipmentNumber;
            litBookedDate.Text = shipment.DateCreated.FormattedShortDate();
            litDesiredPickupDate.Text = shipment.DesiredPickupDate == DateUtility.SystemEarliestDateTime
                                    ? string.Empty
                                    : shipment.DesiredPickupDate.FormattedShortDate();
            litPickupDate.Text = shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime
                                    ? string.Empty
                                    : shipment.ActualPickupDate.FormattedShortDate();
            litEstDeliveryDate.Text = shipment.EstimatedDeliveryDate == DateUtility.SystemEarliestDateTime
                                    ? string.Empty
                                    : shipment.EstimatedDeliveryDate.FormattedShortDate();
            litActualDeliveryDate.Text = shipment.ActualDeliveryDate == DateUtility.SystemEarliestDateTime
                                            ? string.Empty
                                            : shipment.ActualDeliveryDate.FormattedShortDate();

            litOriginLocation.Text = string.Format("{0}{1}{2} {3} {4} {5}", shipment.OriginName, WebApplicationConstants.HtmlBreak,
                                                   shipment.OriginCity, shipment.OriginState, shipment.OriginPostalCode, shipment.OriginCountryName);

            litDestinationLocation.Text = string.Format("{0}{1}{2} {3} {4} {5}", shipment.DestinationName, WebApplicationConstants.HtmlBreak,
                                                   shipment.DestinationCity, shipment.DestinationState, shipment.DestinationPostalCode,
                                                   shipment.DestinationCountryName);

            litCustomer.Text = string.Format("{0} - {1}", shipment.CustomerNumber, shipment.CustomerName);
            hypCustomer.NavigateUrl = string.Format("{0}?{1}={2}", CustomerView.PageAddress, WebApplicationConstants.TransferNumber, shipment.CustomerId.GetString().UrlTextEncrypt());
            litPrimaryVendor.Text = shipment.VendorId != default(long)
                                        ? string.Format("{0} - {1}", shipment.VendorNumber, shipment.VendorName)
                                        : string.Empty;
            hypVendor.NavigateUrl = string.Format("{0}?{1}={2}", VendorView.PageAddress, WebApplicationConstants.TransferNumber, shipment.VendorId.GetString().UrlTextEncrypt());
            hypVendor.Visible = ActiveUser.HasAccessTo(ViewCode.Vendor) && shipment.VendorId != default(long);

            litPrimaryVendorPro.Text = shipment.VendorProNumber != string.Empty ? shipment.VendorProNumber : string.Empty;
            hypPrimaryVendorPro.NavigateUrl = shipment.VendorTrackingUrl.Replace(WebApplicationConstants.VendorProPlaceHolder, shipment.VendorProNumber);
            hypPrimaryVendorPro.Visible = !string.IsNullOrEmpty(shipment.VendorProNumber) &&
                                          !string.IsNullOrEmpty(shipment.VendorTrackingUrl);
            spanGuaranteedServices.Attributes["title"] = shipment.IsGuaranteedDeliveryService ? string.Format("Guaranteed Delivery Service by {0}", shipment.GuaranteedDeliveryServiceTime.ConvertAmPm()) : string.Empty;
            spanGuaranteedServices.Visible = shipment.IsGuaranteedDeliveryService;
            litStatus.Text = shipment.StatusText;

            litAmountDue.Text = shipment.TotalAmountDue.ToString("c2");

            if (!checkCalls.Any()) return;

            lstCheckCalls.DataSource = checkCalls.OrderByDescending(c => c.CallDate)
                .Select(c => new
                {
                    CallDate = c.CallDate.FormattedSuppressMidnightDate(),
                    EventDate = c.EventDate == DateUtility.SystemEarliestDateTime ? string.Empty : c.EventDate.FormattedSuppressMidnightDate(),
                    c.CallNotes,
                })
                .ToList();
            lstCheckCalls.DataBind();
        }

        protected void OnShippingLabelsClicked(object sender, ImageClickEventArgs e)
        {
            var info = new DetailRecordInfo(hidShipmentId.Value.ToLong(), hidRecordType.Value.ToEnum<DashboardDtoType>());
            if (GenerateShippingLabel != null)
                GenerateShippingLabel(this, new ViewEventArgs<DetailRecordInfo>(info));
        }
    }
}