﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class LibraryItemFinderControl : MemberControlBase, ILibraryItemFinderView
    {
        public new bool Visible
        {
            get { return pnlLibraryItemFinderContent.Visible; }
            set
            {
                base.Visible = value;
                pnlLibraryItemFinderContent.Visible = value;
                pnlLibraryItemFinderDimScreen.Visible = value;
            }
        }

        public bool EnableMultiSelection
        {
            get { return hidLibraryItemFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidLibraryItemFinderEnableMultiSelection.Value = value.ToString();
                btnSelectAll.Visible = value;
                btnSelectAll2.Visible = value;
                chkConvertPieceCountToQuantity.Visible = value;
                upcseDataUpdate.PreserveRecordSelection = value;
            }
        }

        public bool OpenForEdit
        {
            get { return hidEditSelected.Value.ToBoolean(); }
        }

        public bool OpenForEditEnabled
        {
            get { return hidOpenForEditEnabled.Value.ToBoolean(); }
            set { hidOpenForEditEnabled.Value = value.ToString(); }
        }

        public long CustomerIdSpecificFilter
        {
            set { hidCustomerIdSpecificFilter.Value = value.ToString(); }
        }

        public event EventHandler<ViewEventArgs<LibraryItemSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<LibraryItem>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<LibraryItem>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<LibraryItemViewSearchDto> libraryItems)
        {
            litRecordCount.Text = libraryItems.BuildRecordCount();
            upcseDataUpdate.DataSource = libraryItems;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString();
            DisplaySearchResult(new List<LibraryItemViewSearchDto>());
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new LibraryItemSearchCriteria
            {
                CustomerId = hidCustomerIdSpecificFilter.Value.ToLong(),
                ActiveUserId = ActiveUser.Id,
                Parameters = GetCurrentRunParameters(false)
            });
        }

        private void DoSearch(LibraryItemSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<LibraryItemSearchCriteria>(criteria));

            var control = lstSearchResults.FindControl("chkSelectAllRecords").ToAltUniformCheckBox();
            if (control != null)
            {
                control.Checked = false;
                control.Visible = EnableMultiSelection;
            }
        }



        private void ProcessItemSelection(long libraryItemId)
        {
            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<LibraryItem>(new LibraryItem(libraryItemId, false)));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new LibraryItemFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : OperationsSearchFields.DefaultLibraryItems.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch();
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = false.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidLibraryItemId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnEditSelectClicked(Object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = true.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidLibraryItemId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnSelectAllClicked(object sender, EventArgs e)
        {
            var items = (from item in lstSearchResults.Items
                         let hidden = item.FindControl("hidLibraryItemId").ToCustomHiddenField()
                         let checkBox = item.FindControl("chkSelected").ToAltUniformCheckBox()
                         where checkBox != null && checkBox.Checked
                         select new LibraryItem(hidden.Value.ToLong()))
                .ToList();

            if (chkConvertPieceCountToQuantity.Checked && items.Any())
            {
                rptConvertPieceCountToQuantityItems.DataSource = items;
                rptConvertPieceCountToQuantityItems.DataBind();
                pnlConvertPieceCountToQuantity.Visible = true;
                pnlLibraryItemFinderContent.Visible = false;
                return;
            }

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<LibraryItem>>(items));
        }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.LibraryItemsFinder.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.LibraryItemsFinder);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }



        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }

        
        protected void OnDoneConvertingPieceCountToQuantityClicked(object sender, EventArgs e)
        {
            var items = rptConvertPieceCountToQuantityItems
                .Items
                .Cast<RepeaterItem>()
                .Select(item =>
                    {
                        var libraryItem = new LibraryItem(item.FindControl("hidLibraryItemId").ToCustomHiddenField().Value.ToLong())
                            {
                                PieceCount = item.FindControl("txtPieceCount").ToTextBox().Text.ToInt(),
                            };
                        var originalQuantity = libraryItem.Quantity;
                        libraryItem.Quantity = item.FindControl("txtQuantity").ToTextBox().Text.ToInt();
                        libraryItem.Weight = (libraryItem.Weight / (originalQuantity == 0 ? 1 : originalQuantity)) * (libraryItem.Quantity);
                        return libraryItem;
                    })
                .ToList();
            chkConvertPieceCountToQuantity.Checked = false;
            pnlConvertPieceCountToQuantity.Visible = false;

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<LibraryItem>>(items));
        }

        protected void OnCancelConvertingPieceCountToQuantityClicked(object sender, EventArgs e)
        {
            pnlConvertPieceCountToQuantity.Visible = false;
            pnlLibraryItemFinderContent.Visible = true;
        }
    }
}