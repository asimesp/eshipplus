﻿using LogisticsPlus.Eship.Processor.Dto.Operations;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
	public class DetailRecordInfo
	{
		public long RecordId { get; private set; }
		public DashboardDtoType RecordType { get; private set; }

		public DetailRecordInfo(long recordId, DashboardDtoType recordType)
		{
			RecordId = recordId;
			RecordType = recordType;
		}
	}
}
