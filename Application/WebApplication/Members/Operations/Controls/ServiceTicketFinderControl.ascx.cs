﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class ServiceTicketFinderControl : MemberControlBase, IServiceTicketFinderView
    {
        public int ItemIndex
        {
            get { return hidLocationItemIndex.Value.ToInt(); }
            set { hidLocationItemIndex.Value = value.ToString(); }
        }

        public new bool Visible
        {
            get { return pnlServiceTicketFinderContent.Visible; }
            set
            {
                base.Visible = value;
                pnlServiceTicketFinderContent.Visible = value;
                pnlServiceTicketFinderDimScreen.Visible = value;
            }
        }

        public bool OpenForEdit
        {
            get { return hidEditSelected.Value.ToBoolean(); }
        }

        public bool OpenForEditEnabled
        {
            get { return hidOpenForEditEnabled.Value.ToBoolean(); }
            set { hidOpenForEditEnabled.Value = value.ToString(); }
        }

        public bool EnableMultiSelection
        {
            get { return hidServiceTicketFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidServiceTicketFinderEnableMultiSelection.Value = value.ToString();
                btnProcessSelected.Visible = value;
                btnProcessSelected2.Visible = value;
                upcseDataUpdate.PreserveRecordSelection = value;
            }
        }

        public long CustomerId
        {
            set { hidCustomerId.Value = value.ToString(); }
        }

		public long CustomerLocationId
		{
			set { hidCustomerLocationId.Value = value.ToString(); }
		}

        public bool FilterForServiceTicketsToBeInvoicedOnly
        {
            get { return hidServiceTicketsToBeInvoicedOnly.Value.ToBoolean(); }
            set { hidServiceTicketsToBeInvoicedOnly.Value = value.ToString(); }
        }
        public bool FilterForServiceTicketsToExludeAttachedToJob
        {
            get { return hidServiceTicketsToExludeAttachedToJob.Value.ToBoolean(); }
            set { hidServiceTicketsToExludeAttachedToJob.Value = value.ToString(); }
        }

        public event EventHandler<ViewEventArgs<ServiceTicketViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<ServiceTicket>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<ServiceTicket>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<ServiceTicketDashboardDto> serviceTickets)
        {
            var customerId = hidCustomerId.Value.ToLong();
            var locationId = hidCustomerLocationId.Value.ToLong();

	        var data = serviceTickets
		        .Where(s =>
			        {
				        var r = s.ResellerAdditionId == default(long)
					                ? new ResellerAddition()
					                : new ResellerAddition(s.ResellerAdditionId);
				        var resellerMatch = s.BillReseller && r.KeyLoaded && r.ResellerCustomerAccountId == customerId;
				        var customerMatch = customerId == default(long) || s.CustomerId == customerId || resellerMatch;

				        var locationMatch = locationId == default(long) || s.OverrideCustomerLocationId == default(long) || s.OverrideCustomerLocationId == locationId;

				        return customerMatch && locationMatch;
			        })
		        .ToList();

	        var dtos = customerId != default(long)
		                   ? serviceTickets
			                     .Where(s =>
				                     {
					                     var r = s.ResellerAdditionId == default(long)
						                             ? new ResellerAddition()
						                             : new ResellerAddition(s.ResellerAdditionId);
					                     return s.CustomerId == customerId || (s.BillReseller && r.KeyLoaded && r.ResellerCustomerAccountId == customerId);
				                     })
			                     .ToList()
		                   : serviceTickets.ToList();

            litRecordCount.Text = data.BuildRecordCount();
            upcseDataUpdate.DataSource = data;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString();
            DisplaySearchResult(new List<ServiceTicketDashboardDto>());
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new ServiceTicketViewSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = GetCurrentRunParameters(false),
            });
        }

        private void DoSearch(ServiceTicketViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<ServiceTicketViewSearchCriteria>(criteria));

            var control = lstSearchResults.FindControl("chkSelectAllRecords").ToAltUniformCheckBox();
            if (control != null)
            {
                control.Checked = false;
                control.Visible = EnableMultiSelection;
            }
        }


        private void ProcessItemSelection(long serviceTicketId)
        {
            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<ServiceTicket>(new ServiceTicket(serviceTicketId, false)));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ServiceTicketFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                         ? profile.Columns
                         : OperationsSearchFields.DefaultServiceTickets.Select(f => f.ToParameterColumn()).ToList();
            
            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch();
        }


        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = false.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidServiceTicketId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnEditSelectClicked(Object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = true.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidServiceTicketId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnProcessSelectedClicked(object sender, EventArgs e)
        {
            var serviceTickets = (from item in lstSearchResults.Items
                                  let hidden = item.FindControl("hidServiceTicketId").ToCustomHiddenField()
                                  let checkBox = item.FindControl("chkSelected").ToAltUniformCheckBox()
                                  where checkBox != null && checkBox.Checked
                                  select new ServiceTicket(hidden.Value.ToLong()))
                .Where(u => u.KeyLoaded)
                .ToList();

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<ServiceTicket>>(serviceTickets));
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.ServiceTickets.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.ServiceTickets);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
    }
}