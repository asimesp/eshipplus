﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingLabelGeneratorControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ShippingLabelGeneratorControl" %>
<asp:Panel runat="server" ID="pnlShippingLabelGenerator" CssClass="popupAnnouncement popupControlOverW500 popup-position-fixed" Visible="False">
    <div class="popheader">
        <h4>
            <asp:Label runat="server" ID="lbShipmentNumber" />
            Shipment Documents<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
        </h4>
    </div>
    <div class="row pl20 pt10">
        <label>
            <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Members/Operations/StandardShippingLabel.aspx" Target="_blank" runat="server" CssClass="link_nounderline blue">
                <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle"/>
            </asp:HyperLink>
            &nbsp;Standard Label
        </label>
        <asp:HyperLink ID="hypShipmentStandardLabel" Target="_blank" runat="server" CssClass="link_nounderline blue">
            <img src='<%= ResolveUrl("~/images/icons/downloadPdf.png") %>' alt="" align="absmiddle" Width="20px"  />
        </asp:HyperLink>
      

    </div>
    <div class="row pl20">
        <label>
            <asp:HyperLink ID="HyperLink2" NavigateUrl="~/Members/Operations/AveryShippingLabel.aspx" Target="_blank" runat="server" CssClass="link_nounderline blue">
                <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle"/>
            </asp:HyperLink>
            &nbsp;Avery Label
        </label>
        <asp:HyperLink ID="hypShipmentAveryLabel" Target="_blank" runat="server" CssClass="link_nounderline blue">
            <img src='<%= ResolveUrl("~/images/icons/downloadPdf.png") %>' alt="" align="absmiddle" Width="20px"  />
        </asp:HyperLink>
    </div>
    <div class="row pl20">
        <label>
            <asp:HyperLink ID="HyperLink5" NavigateUrl="~/Members/Operations/ProShippingLabel.aspx" Target="_blank" runat="server" CssClass="link_nounderline blue">
                <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle"/>
            </asp:HyperLink>
            &nbsp;Pro Label - 4x6 inch
        </label>
        <asp:HyperLink ID="hypShipmentProLabel" Target="_blank" runat="server" CssClass="link_nounderline blue">
            <img src='<%= ResolveUrl("~/images/icons/downloadPdf.png") %>' alt="" align="absmiddle" Width="20px"  />
        </asp:HyperLink>
    </div>
    <div class="row pl20 pb10">
        <label>
            <asp:HyperLink ID="HyperLink6" NavigateUrl="~/Members/Operations/ProLabelAvery5164.aspx" Target="_blank" runat="server" CssClass="link_nounderline blue">
                <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle"/>
            </asp:HyperLink>
            &nbsp;Pro Label Avery 5164 - 3x4 inch
        </label>
        <asp:HyperLink ID="hypShipmentAveryLabel5164" Target="_blank" runat="server" CssClass="link_nounderline blue">
            <img src='<%= ResolveUrl("~/images/icons/downloadPdf.png") %>' alt="" align="absmiddle" Width="20px"  />
        </asp:HyperLink>
    </div>
    <div class="pb10">
        <hr class="w100p ml-inherit p_m0" />
    </div>
    <div class="rowgroup pl20">
        <div class="row">
            <label>
                <asp:HyperLink ID="HyperLink3" NavigateUrl="~/Members/Operations/CustomerShipmentDetailView.aspx" Target="_blank" runat="server" CssClass="link_nounderline blue">
                    <img src='<%= ResolveUrl("~/images/icons2/downloadDocument.png")%>' alt="" align="absmiddle"/>
                </asp:HyperLink>
                &nbsp;Shipment Detail
            </label>
        </div>
        <div class="finderScroll">
            <asp:Repeater runat="server" ID="rptDocuments">
                <ItemTemplate>
                    <div class="row">
                        <label>
                            <asp:HyperLink ID="HyperLink4" NavigateUrl='<%# Eval("Url") %>' Target="_blank" runat="server" CssClass="link_nounderline blue">
                                <img src='<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>' alt="" align="absmiddle"/>
                            </asp:HyperLink>
                            &nbsp;<%# Eval("Text")%> 
                        </label>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <asp:Panel runat="server" ID="pnlShipmentStatement" Visible="False">
            <label>
                <asp:HyperLink ID="hypShipmentStatement" Target="_blank" runat="server" CssClass="link_nounderline blue">
                    <img src='<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>' alt="" align="absmiddle"/>
                </asp:HyperLink>
                &nbsp;Shipment Statement
            </label>
        </asp:Panel>
    </div>
  
</asp:Panel>
<asp:Panel ID="pnlShippingLabelGeneratorDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
<eShip:CustomHiddenField runat="server" ID="hidShipmentId" />
