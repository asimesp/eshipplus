﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipmentFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ShipmentFinderControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<asp:Panel runat="server" ID="pnlShipmentFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Shipment Finder<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
        </h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlShipmentFinderContent.ClientID%> div[class="finderScroll"]').offset().top - $('#<%= pnlShipmentFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlShipmentFinderContent.ClientID%> div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table id="finderTable" class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:Button ID="btnProcessSelected2" runat="server" Text="ADD SELECTED" OnClick="OnProcessSelectedClicked" CssClass="ml10" CausesValidation="false" />
                        </div>
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlSearch" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                 <ul class="display-inline left mt0 mb0 pl20">
                                    <li class="note left pr20" style='<%= hidCustomerId.Value.ToLong() != default(long) ? string.Empty: "display:none;" %>'>
                                        Filtering for specific customer enabled.
                                    </li>
                                    <li class="note left" style='<%= FilterForShipmentsToBeInvoicedOnly ? string.Empty: "display:none;" %>'>
                                        Filtering shipments to be invoiced enabled.
                                    </li>
                                </ul>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add" />
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="right">
                                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Shipments" ShowAutoRefresh="False"
                                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>

                        <hr class="mb5" />

                        <script type="text/javascript">$(window).load(function () {SetControlFocus();});</script><table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                        <div class="pl10 row mb5">
                            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                            <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL" CssClass="ml10" CausesValidation="false" />
                            <asp:Button ID="btnProcessSelected" runat="server" Text="ADD SELECTED" OnClick="OnProcessSelectedClicked" CssClass="ml10" CausesValidation="false" />
                            <label class="right pr10">
                                Sort:
                                <asp:Literal runat="server" ID="litSortOrder" Text='<%# WebApplicationConstants.Default %>' />
                            </label>
                        </div>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstShipments" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="shipmentFinderScrollSection"
                SelectionCheckBoxControlId="chkSelected" SelectionUniqueRefHiddenFieldId="hidShipmentId" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheUsersFinderTable" TableId="shipmentFinderTable" ScrollerContainerId="shipmentFinderScrollSection" ScrollOffsetControlId="pnlShipmentFinderContent"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
            <div class="finderScroll" id="shipmentFinderScrollSection">
                <table id="shipmentFinderTable" class="stripe sm_chk">
                    <tr>
                        <th style="width: 15%">
                            <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'shipmentFinderTable');" />
                        </th>
                        <th style="width: 9%;">
                            <asp:LinkButton runat="server" ID="lbtnSortShipmentNumber" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
												<abbr title="Shipment Number">Shipment #</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDesiredPickup" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
												<abbr title="Desired Pickup Date">D. Pickup</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 11%;">
                            <asp:LinkButton runat="server" ID="lbtnSortActualPickup" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
											  <abbr title="Actual Pickup Date">A. Pickup</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortEstimatedDelivery" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
											 <abbr title="Estimated Delivery Date">Est. Del.</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 26%;">Customer
								<asp:LinkButton runat="server" ID="lbtnSortCustomerNumber" Text="Number" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData" />
                            /<asp:LinkButton runat="server" ID="lbtnSortCustomerName" CausesValidation="False" Text="Name" CssClass="link_nounderline white" OnCommand="OnSortData" />
                        </th>
                        <th style="width: 16%;">
                            <asp:LinkButton runat="server" ID="lbtnSortStatus" Text="Status" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData" />
                        </th>
                    </tr>
                    <asp:ListView ID="lstShipments" runat="server" ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked"
                                        Visible='<%# !EnableMultiSelection %>' CausesValidation="false" />
                                    <asp:Button ID="btnEditSelect" runat="server" Text="Edit" OnClick="OnEditSelectClicked"
                                        Visible='<%# !EnableMultiSelection && OpenForEditEnabled %>' CausesValidation="false" />
                                    <eShip:AltUniformCheckBox runat="server" ID="chkSelected" Visible='<%# EnableMultiSelection %>'
                                        OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'usersFinderTable', 'chkSelectAllRecords');" />
                                    <eShip:CustomHiddenField ID="hidShipmentId" Value='<%# Eval("Id") %>' runat="server" />
                                </td>
                                <td>
                                    <%# Eval("ShipmentNumber")  %>
                                </td>
                                <td>
                                    <%# Eval("DesiredPickupDate").ToDateTime().TimeToMinimum() != DateUtility.SystemEarliestDateTime ?  Eval("DesiredPickupDate").FormattedShortDate() : string.Empty  %>
                                </td>
                                <td>
                                    <%# Eval("ActualPickupDate").ToDateTime().TimeToMinimum() != DateUtility.SystemEarliestDateTime ?  Eval("ActualPickupDate").FormattedShortDate() : string.Empty  %>
                                </td>
                                <td>
                                    <%# Eval("EstimatedDeliveryDate").ToDateTime().TimeToMinimum() != DateUtility.SystemEarliestDateTime ?  Eval("EstimatedDeliveryDate").FormattedShortDate() : string.Empty  %>
                                </td>
                                <td>
                                    <%# Eval("CustomerNumber")  %> - <%# Eval("CustomerName")  %>
                                </td>
                                <td>
                                    <%# Eval("StatusText") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnSortShipmentNumber" />
            <asp:PostBackTrigger ControlID="lbtnSortDesiredPickup" />
            <asp:PostBackTrigger ControlID="lbtnSortActualPickup" />
            <asp:PostBackTrigger ControlID="lbtnSortEstimatedDelivery" />
            <asp:PostBackTrigger ControlID="lbtnSortCustomerNumber" />
            <asp:PostBackTrigger ControlID="lbtnSortCustomerName" />
            <asp:PostBackTrigger ControlID="lbtnSortStatus" />
            <asp:PostBackTrigger ControlID="lstShipments" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="pnlShipmentFinderDimScreen" CssClass="dimBackgroundControl" runat="server"
    Visible="false" />
<eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
<eShip:CustomHiddenField ID="hidEditSelected" runat="server" />
<eShip:CustomHiddenField ID="hidOpenForEditEnabled" runat="server" />
<eShip:CustomHiddenField ID="hidOnlyActiveResults" runat="server" />
<eShip:CustomHiddenField ID="hidShipmentFinderEnableMultiSelection" runat="server" />
<eShip:CustomHiddenField ID="hidLocationItemIndex" runat="server" />
<eShip:CustomHiddenField ID="hidCustomerId" runat="server" />
<eShip:CustomHiddenField ID="hidShipmentsToBeInvoicedOnly" runat="server" />
<eShip:CustomHiddenField ID="hidShipmentsToExludeAttachedToJob" runat="server" />
<eShip:CustomHiddenField runat="server" ID="hidSortAscending" />
<eShip:CustomHiddenField runat="server" ID="hidSortField" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />