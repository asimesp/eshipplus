﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerLoadDashboardDetailControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.CustomerLoadDashboardDetailControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<eShip:CustomHiddenField ID="hidShipmentId" runat="server" />
<eShip:CustomHiddenField ID="hidItemIndex" runat="server" />
<eShip:CustomHiddenField ID="hidRecordType" runat="server" />

<tr>
    <td rowspan="2" class="top">
        <asp:Literal runat="server" ID="litShipmentNumber"/>&nbsp;
        <asp:HyperLink ID="hypBillOfLading" Target="_blank" runat="server" ToolTip="Download Bill of Lading">
                <img src='<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>' alt="" align="absmiddle" width="14"/>
        </asp:HyperLink>
    </td>
    <td>
        <asp:Literal runat="server" ID="litBookedDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litDesiredPickupDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litPickupDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litEstDeliveryDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litActualDeliveryDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litAmountDue" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litServiceMode" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litStatus" />
    </td>
    <td rowspan="2" class="text-center top">
        <asp:ImageButton runat="server" ID="ibtnShippingLabel" ImageUrl="~/images/icons2/document.png" CausesValidation="False"
            Width="16px" ToolTip="Generate Shipment(s) Documents" OnClick="OnShippingLabelsClicked" />
    </td>
</tr>
<tr class="f9">
    <td colspan="8" class="top forceLeftBorder">
        <div class="rowgroup mb20">
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <label class="wlabel blue">
                        Customer
                    </label>
                    <label>
                        <asp:Literal runat="server" ID="litCustomer" />&nbsp;
                        <asp:HyperLink ID="hypCustomer" runat="server" Target="_blank" Visible='<%# ActiveUser.HasAccessTo(ViewCode.Customer) %>' ToolTip="Go To Customer Record">
                            <asp:Image ImageUrl="~/images/icons2/arrow_newTab.png" runat="server" Width="16px" CssClass="middle"/>
                        </asp:HyperLink>
                    </label>
                </div>
                <div class="row">
                    <label class="wlabel blue">Origin</label>
                    <label>
                        <asp:Literal runat="server" ID="litOriginLocation" />&nbsp;
                    </label>
                </div>
            </div>
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <label class="wlabel blue">
                        Vendor
                    </label>
                    <label>
                        <asp:Literal runat="server" ID="litPrimaryVendor" />&nbsp;
                        <asp:HyperLink id="hypVendor" runat="server" target="_blank" ToolTip="Go To Vendor Record">
                            <asp:Image ImageUrl="~/images/icons2/arrow_newTab.png" runat="server" Width="16px" CssClass="middle"/>
                        </asp:HyperLink>
                    </label>
                </div>
                <div class="row">
                    <label class="wlabel blue">Destination</label>
                    <label>
                        <asp:Literal runat="server" ID="litDestinationLocation" />&nbsp;
                    </label>
                </div>
            </div>
            <div class="col_1_3">
                <div class="row">
                    <label class="wlabel blue">Pro Number</label>
                    <label>
                        <asp:Literal runat="server" ID="litPrimaryVendorPro" />
                        <asp:HyperLink runat="server" ID="hypPrimaryVendorPro" Target="_blank">
                            <asp:Image runat="server" ImageUrl="~/images/icons2/externalLink.png" AlternateText="external link image" Height="12px" CssClass="ml5" ToolTip="Track on vendor website"/>
                        </asp:HyperLink>&nbsp;
                    </label>
                </div>
                <div class="row">
                    <label>
                        <span runat="server" ID="spanGuaranteedServices" class='mr5 flag pl5 pr5'>GS</span>
                    </label>
                </div>
            </div>
        </div>
        <asp:ListView ID="lstCheckCalls" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup mb10">
                    <div class="row">
                        <h6 class="mt0 mb0">Tracking Updates</h6>
                        <table class="contain pl2">
                            <tr class="bbb1">
                                <th style="width: 18%;" class="no-top-border">Last Update
                                </th>
                                <th style="width: 18%;" class="no-top-border no-left-border">Event Date
                                </th>
                                <th style="width: 64%;" class="no-top-border no-left-border">Status Details
                                </th>
                            </tr>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </table>
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr class="no-bottom-border">
                    <td class="top">
                        <%# Eval("CallDate")%>
                    </td>
                    <td class="top">
                        <%# Eval("EventDate")%>
                    </td>
                    <td class="top">
                        <%# Eval("CallNotes")%>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </td>
</tr>
