﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class ShippingLabelGeneratorControl : MemberControlBase
    {
        private const string Unknown = "Unknown";

        public new bool Visible
        {
            get { return pnlShippingLabelGenerator.Visible; }
            set
            {
                base.Visible = value;
                pnlShippingLabelGenerator.Visible = value;
                pnlShippingLabelGeneratorDimScreen.Visible = value;
            }
        }

        public event EventHandler Close;

        protected void OnCloseClicked(object sender, ImageClickEventArgs e)
        {
            Session[WebApplicationConstants.ShippingLabelShipmentId] = null;
            if (Close != null)
                Close(this, new EventArgs());
        }


        public void GenerateLabels(long shipmentId)
        {
            Session[WebApplicationConstants.ShippingLabelShipmentId] = shipmentId;

            var shipment = new Shipment(shipmentId);
            pnlShipmentStatement.Visible = shipment.Status == ShipmentStatus.Invoiced;
            hidShipmentId.Value = shipmentId.ToString();
            if (!shipment.KeyLoaded)
            {
                lbShipmentNumber.Text = Unknown;
                rptDocuments.DataSource = null;
                rptDocuments.DataBind();
            }
            else
            {
                lbShipmentNumber.Text = shipment.ShipmentNumber;
                var documents = ActiveUser.TenantEmployee
                                    ? shipment.Documents.Where(d => !d.IsNew).ToList()
                                    : shipment.Documents.Where(d => !d.IsNew && !d.IsInternal).ToList();
                var dataSource =
                    !documents.Any()
                        ? null
                        : documents.Select(d => new
                        {
                            Text = d.DocumentTagId != default(long) ? string.Format("{0} - {1}", d.DocumentTag.Code, d.Name) : d.Name,
                            Url = DocumentViewer.GenerateShipmentDocumentLink(d),
                        });
                rptDocuments.DataSource = dataSource;
                rptDocuments.DataBind();

                hypShipmentStandardLabel.NavigateUrl = StandardShippingLabel.GenerateStandardShippingLabelPdfLink(shipmentId);
                hypShipmentAveryLabel.NavigateUrl = AveryShippingLabel.GenerateAveryShippingLabelPdfLink(shipmentId);
                hypShipmentProLabel.NavigateUrl = ProShippingLabel.GenerateAveryShippingLabelPdfLink(shipmentId);
                hypShipmentAveryLabel5164.NavigateUrl = ProLabelAvery5164.GenerateAveryShippingLabelPdfLink(shipmentId);

                if (shipment.Status == ShipmentStatus.Invoiced)
                {
                    hypShipmentStatement.NavigateUrl = DocumentViewer.GenerateShipmentStatementLink(shipment);
                    
                }
            }
        }
    }
}