﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClaimDashboardDetailControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ClaimDashboardDetailControl" %>
<eShip:CustomHiddenField ID="hidItemIndex" runat="server" />
<eShip:CustomHiddenField ID="hidClaimId" runat="server" />
<tr>
    <td class="top" rowspan="2">
        <asp:Literal runat="server" ID="litClaimNumber" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litStatus" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litCustomer" />
        <asp:HyperLink ID="hypCustomer" Target="_blank" runat="server" ToolTip="Go To Customer Record">
                <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle" width="16"/>
        </asp:HyperLink>
    </td>
    <td>
        <asp:Literal runat="server" ID="litDateCreated" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litUser" />
        <asp:HyperLink ID="hypUser" Target="_blank" runat="server" ToolTip="Go To User Record">
                <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle" width="16"/>
        </asp:HyperLink>
    </td>
    <td rowspan="2" class="top text-center">
        <asp:HyperLink runat="server" ID="hypClaimView" Target="_blank">
            <asp:Image runat="server" ID="imgGoToClaimView" ImageUrl="~/images/icons2/arrow_newTab.png"
                AlternateText="Go to Claim Record" />
        </asp:HyperLink>
    </td>
</tr>
<tr class="f9">
    <td colspan="4" class="forceLeftBorder">
        <div class="rowgroup">
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <label class="wlabel blue">Details</label>
                    <label>
                        <asp:Literal runat="server" ID="litDetails" /></label>
                </div>
            </div>
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <label class="wlabel blue">Vendor Names</label>
                    <label>
                        <asp:Literal runat="server" ID="litVendorNames" /></label>
                </div>
            </div>
            <div class="col_1_3 no-right-border">
                <div class="row mb10">
                    <label class="wlabel blue">Shipments</label>
                    <label>
                        <asp:Literal runat="server" ID="litShipments" /></label>
                </div>
                <div class="row">
                    <label class="wlabel blue">Service Tickets</label>
                    <label>
                        <asp:Literal runat="server" ID="litServiceTickets" /></label>
                </div>
            </div>
        </div>
    </td>
</tr>
