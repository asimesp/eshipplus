﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
	public partial class JobFinderControl : MemberControlBase, IJobFinderView
	{
		public int ItemIndex
		{
			get { return hidLocationItemIndex.Value.ToInt(); }
			set { hidLocationItemIndex.Value = value.ToString(); }
		}

		public new bool Visible
		{
			get { return pnlJobFinderContent.Visible; }
			set
			{
				base.Visible = true;
				pnlJobFinderContent.Visible = value;
				pnlJobFinderDimScreen.Visible = value;
			}
		}

		public bool OpenForEdit
		{
			get { return hidEditSelected.Value.ToBoolean(); }
		}

		public bool OpenForEditEnabled
		{
			get { return hidOpenForEditEnabled.Value.ToBoolean(); }
			set { hidOpenForEditEnabled.Value = value.ToString(); }
		}

		public event EventHandler<ViewEventArgs<JobViewSearchCriteria>> Search;
		public event EventHandler<ViewEventArgs<Job>> ItemSelected;
		public event EventHandler SelectionCancel;

		public void DisplaySearchResult(List<JobDashboardDto> jobDashboardDtos)
		{
			litRecordCount.Text = jobDashboardDtos.BuildRecordCount();
			upcseDataUpdate.DataSource = jobDashboardDtos;
			upcseDataUpdate.DataBind();
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;
		}

		public void Reset()
		{
			hidAreFiltersShowing.Value = true.ToString();
			DisplaySearchResult(new List<JobDashboardDto>());
		}

		private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
		{
			return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
		}

		private void DoSearchPreProcessingThenSearch()
		{
			DoSearch(new JobViewSearchCriteria
			{
				Parameters = GetCurrentRunParameters(false),
				ActiveUserId = ActiveUser.Id,
			});
		}

		private void DoSearch(JobViewSearchCriteria criteria)
		{
			if (Search != null)
				Search(this, new ViewEventArgs<JobViewSearchCriteria>(criteria));
		}

		public void ProcessItemSelection(long jobId)
		{
			if (ItemSelected != null)
				ItemSelected(this, new ViewEventArgs<Job>(new Job(jobId, false)));
		}



		protected void Page_Load(object sender, EventArgs e)
		{
			new JobFinderHandler(this).Initialize();

			searchProfiles.GetProfileItems = GetSearchProfileItems;

			if (IsPostBack) return;

			hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;
			var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

			var columns = profile != null && profile.Columns != null
							? profile.Columns
							: OperationsSearchFields.DefaultJobs.Select(f => f.ToParameterColumn()).ToList();

			lstFilterParameters.DataSource = columns;
			lstFilterParameters.DataBind();
		}

		protected void OnSearchClicked(object sender, EventArgs e)
		{
			hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
			upcseDataUpdate.ResetLoadCount();
			DoSearchPreProcessingThenSearch();
		}

		protected void OnSelectClicked(object sender, EventArgs e)
		{
			hidEditSelected.Value = false.ToString();
			var jobId = ((Button)sender).CommandArgument.ToLong();
			ProcessItemSelection(jobId);
		}

		protected void OnEditSelectClicked(Object sender, EventArgs e)
		{
			hidEditSelected.Value = true.ToString();
			var jobId = ((Button)sender).CommandArgument.ToLong();
			ProcessItemSelection(jobId);
		}

		protected void OnCancelClicked(object sender, EventArgs eventArgs)
		{
			if (SelectionCancel != null)
				SelectionCancel(this, new EventArgs());
		}

		private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
		{
			var currentRunParameters = lstFilterParameters.Items
				.Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
			return currentRunParameters;
		}

		protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType != ListViewItemType.DataItem) return;

			var item = (ListViewDataItem)e.Item;
			var parameter = (ParameterColumn)item.DataItem;

			if (parameter == null) return;

			var field = OperationsSearchFields.Jobs.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

			if (field == null) return;

			var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

			detailControl.LoadParameter(parameter, field.DataType, true);
		}

		protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
		{
			var parameters = GetCurrentRunParameters(true);

			parameters.RemoveAt(e.Argument);

			lstFilterParameters.DataSource = parameters;
			lstFilterParameters.DataBind();

			Reset();
		}

		protected void OnParameterAddClicked(object sender, EventArgs e)
		{
			parameterSelector.LoadParameterSelector(OperationsSearchFields.Jobs);
			parameterSelector.Visible = true;
		}

		protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			var currentRunParameters = GetCurrentRunParameters(true);
			currentRunParameters.AddRange(e.Argument);

			lstFilterParameters.DataSource = currentRunParameters;
			lstFilterParameters.DataBind();

			parameterSelector.Visible = false;
			
			Reset();
		}

		protected void OnParameterSelectorClose(object sender, EventArgs e)
		{
			parameterSelector.Visible = false;
		}

		protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
		{
			DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
		}

		protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
		{
			lstFilterParameters.DataSource = e.Argument.Columns;
			lstFilterParameters.DataBind();
		}
	}
}