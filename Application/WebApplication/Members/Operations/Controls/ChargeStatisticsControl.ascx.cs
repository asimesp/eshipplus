﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
	public partial class ChargeStatisticsControl : MemberControlBase
	{
		public void LoadCharges<T>(List<T> charges, long resellerAdditionId) where T : Charge
		{
			var reseller = new ResellerAddition(resellerAdditionId);
			if (!reseller.KeyLoaded) reseller = null;

			if (reseller != null)
				foreach (var charge in charges.Where(charge => charge.ChargeCode != null))
				{
					charge.UnitSell -= charge.GetResellerAdditions(reseller);
					if (charge.UnitSell < 0) charge.UnitSell = 0;
				}

			decimal totalBuy = 0;
			decimal totalDue = 0;

			foreach (var c in charges)
			{
				totalBuy += c.FinalBuy;
				totalDue += c.AmountDue;
			}

			var profit = totalDue - totalBuy;
			var profitPercent = totalBuy == 0 ? 0 : profit / totalBuy;
			var gpm = totalDue == 0 ? 0 : profit / totalDue;

            litReseller.Text = reseller != null ? "&#8226; Reseller additions deducted" : string.Empty;
			litTotalBuy.Text = totalBuy.ToString("c2");
			litTotalDue.Text = totalDue.ToString("c2");
			litProfit.Text = profit.ToString("c2");
			litProfitPercent.Text = profitPercent.ToString("p2");
			litGPM.Text = gpm.ToString("p2");

		}
	}
}