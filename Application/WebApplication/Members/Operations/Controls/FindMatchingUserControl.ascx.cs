﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
	public partial class FindMatchingUserControl : MemberControlBase
	{
		protected const string GotoPage = "Go to page with record index";

		private MemberPageBaseWithPageStore _parentPage;
		private MemberPageBaseWithPageStore ParentPage
		{
			get { return _parentPage ?? (_parentPage = Page as MemberPageBaseWithPageStore); }
		}

		private IEnumerable<User> Users
		{
			get
			{
				return ParentPage == null || !ParentPage.PageStore.ContainsKey(UsersDataStoreKey)
						   ? null
						   : ParentPage.PageStore[UsersDataStoreKey] as List<User>;
			}
		}

		public new bool Visible
		{
			get { return pnlMatchingUserSearch.Visible; }
			set
			{
				base.Visible = true;
				pnlMatchingUserSearch.Visible = value;
				pnlMachingUserFinderDimScreen.Visible = value;
			}
		}

		public string UsersDataStoreKey { get; set; }

		public event EventHandler Close;
		public event EventHandler<ViewEventArgs<int>> SelectedIndex;

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;
		}

		public void Reset()
		{
			txtFirstName.Text = string.Empty;
			txtLastName.Text = string.Empty;
			txtEmail.Text = string.Empty;
			txtUserName.Text = string.Empty;

			lstMatchingUsers.DataSource = new List<object>();
			lstMatchingUsers.DataBind();

		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}
		

		protected void OnFindMatchingUsersClicked(object sender, EventArgs e)
		{
			if (Users == null) return;

			var cnt = 0;
			var users = from item in Users
						let index = ++cnt
						where true
						select new
						{
							Index = index,
							UserName = item.Username,
							item.FirstName,
							item.LastName,
							item.Email
						};

			if (!String.IsNullOrEmpty(txtFirstName.Text))
				users = users.Where(p => p.FirstName.ToLower().Contains(txtFirstName.Text.ToLower()));
			if (!String.IsNullOrEmpty(txtLastName.Text))
				users = users.Where(p => p.LastName.ToLower().Contains(txtLastName.Text.ToLower()));
			if (!String.IsNullOrEmpty(txtUserName.Text))
				users = users.Where(p => p.UserName.ToLower().Contains(txtUserName.Text.ToLower()));
			if (!String.IsNullOrEmpty(txtEmail.Text))
				users = users.Where(p => p.Email.ToLower().Contains(txtEmail.Text.ToLower()));

			lstMatchingUsers.DataSource = users;
			lstMatchingUsers.DataBind();

			if (!users.Any())
				DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
		}

		protected void OnCloseClicked(object sender, EventArgs e)
		{
			Reset();

			if (Close != null)
				Close(this, new EventArgs());
		}

		protected void OnIndexClicked(object sender, EventArgs e)
		{
			var btn = (ImageButton)sender;

			if (SelectedIndex != null)
				SelectedIndex(this, new ViewEventArgs<int>(btn.ToolTip.Replace(GotoPage, string.Empty).ToInt()));

			Reset();
		}

		
	}
}