﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipmentDashboardDetailControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ShipmentDashboardDetailControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<eShip:CustomHiddenField ID="hidShipmentId" runat="server" />
<eShip:CustomHiddenField ID="hidItemIndex" runat="server" />
<eShip:CustomHiddenField ID="hidGoToAudit" runat="server" />
<eShip:CustomHiddenField ID="hidOpenForShipmentModification" runat="server" />
<eShip:CustomHiddenField runat="server" ID="hidRecordType" />
<tr>
    <td rowspan="2" class="top" <%# EnabledMultiSelect ? string.Empty : "style='display:none;'" %>>
        <eShip:AltUniformCheckBox runat="server" ID="chkSelection" Visible='<%# EnabledMultiSelect %>'
            OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'shipmentsTable', 'chkSelectAllRecords');" />
    </td>
    <td class='<%# EnabledMultiSelect ? string.Empty : "no-left-border" %>'>
        <asp:Literal runat="server" ID="litShipmentNumber"/>&nbsp;
        <asp:HyperLink ID="hypBillOfLading" Target="_blank" runat="server" ToolTip="Download Bill of Lading">
                <img src='<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>' alt="" align="absmiddle" width="14"/>
        </asp:HyperLink>
    </td>
    <td>
        <asp:Literal runat="server" ID="litBookedDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litDesiredPickupDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litPickupDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litEstDeliveryDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litActualDeliveryDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litCost" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litAmountDue" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litServiceMode" />
    </td>
    <td>
        <div runat="server" id="divShipmentStatus">
            <asp:Literal runat="server" ID="litStatus" />
            <script type="text/javascript">
                function ShowCheckCalls(cid, did) {
                    if (!jsHelper.IsTrue(jsHelper.XYSet)) return;
                    $(jsHelper.AddHashTag(cid)).css('top', jsHelper.PageY - 205);
                    $(jsHelper.AddHashTag(cid)).css('left', jsHelper.PageX - $(jsHelper.AddHashTag(cid)).width());
                    $(jsHelper.AddHashTag(cid)).show();
                    $(jsHelper.AddHashTag(did)).show();
                }

                function HideEdiStatuses(cid, did) {
                    $(jsHelper.AddHashTag(cid)).hide();
                    $(jsHelper.AddHashTag(did)).hide();

                }
            </script>
        </div>
        <div class="dimBackground" id="divCheckCallDimBackground<%= ItemIndex %>" style="display: none;"></div>
        <div id="checkCallDiv<%= ItemIndex %>" class="popover popupControlOverW750" style="display: none;" onmouseleave="javascript:HideEdiStatuses('checkCallDiv<%= ItemIndex %>', 'divCheckCallDimBackground<%= ItemIndex %>');">
            <div class="popheader" id="div<%= ItemIndex %>">
                <h4>Detail Status Information</h4>
                <img title="Contact Information" align="AbsMiddle" width="20" src='<%= ResolveUrl("~/images/icons2/close.png") %>' class="close" onclick="javascript:HideEdiStatuses('checkCallDiv<%= ItemIndex %>', 'divCheckCallDimBackground<%= ItemIndex %>');"/>
            </div>
            <asp:ListView ID="lstCheckCalls" runat="server" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <div class="row">
                        <table class="stripe">
                            <tr>
                                <th style="width: 15%;">Check Call Date
                                </th>
                                <th style="width: 15%;">Event Date
                                </th>
                                <th style="width: 10%;" class="text-center">EDI Code
                                </th>
                                <th style="width: 60%;">Check Call Notes
                                </th>
                            </tr>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </table>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr class="no-bottom-border">
                        <td class="top">
                            <%# Eval("CallDate")%>
                        </td>
                        <td class="top">
                            <%# Eval("EventDate")%>
                        </td>
                        <td class="top text-center">
                            <%# Eval("EdiStatusCode")%>
                        </td>
                        <td class="top">
                            <%# Eval("CallNotes")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </td>
    <td rowspan="2" class="text-center top">
        <div class="row">
            <asp:HyperLink runat="server" ID="hypShipmentView">
                <asp:Image runat="server" ID="imgGoToShipmentView" ImageUrl="~/images/icons2/arrow_general.png"
                    AlternateText="Go to Record" Width="20px" CssClass="mb5" />
            </asp:HyperLink>
        </div>
        <div class="row">
            <asp:HyperLink runat="server" ID="hypShipmentAudit" Visible="false" Target="_blank">
                <asp:Image runat="server" ID="imgGoToAudit" ImageUrl="~/images/icons2/arrow_newTab.png" CssClass="mb5"
                    ToolTip="Go To Shipment Audit" />
            </asp:HyperLink>
        </div>
        <asp:ImageButton runat="server" ID="ibtnShippingLabel" ImageUrl="~/images/icons2/document.png" CssClass="mb5" ToolTip="Generate Shipping Labels" OnClick="OnShippingLabelsClicked" />
        <asp:ImageButton runat="server" ID="ibtnModifyShipment" ImageUrl="~/images/icons2/editBlue.png" ToolTip="Modify Shipment" OnClick="OnModifyShipmentClicked" />
    </td>
</tr>
<tr class="f9">
    <td class='top <%# EnabledMultiSelect ? "forceLeftBorder" : string.Empty %>'>
        <div class="rowgroup mt10">
            <div class="row">
                <div class="fieldgroup">
                    <label class="wlabel blue">Priority</label>
                    <eShip:CustomTextBox ID="txtPriorityColor" runat="server" Enabled="false" Height="24" CssClass="w60 text-center" />
                </div>
            </div>
            <div class="row">
                <div class="fieldgroup">
                    <label class="wlabel blue">Alerts</label>
                    <label>
                        <asp:Literal runat="server" ID="litAlerts" /></label>
                </div>
            </div>
        </div>
    </td>
    <td colspan="9" class="top">
        <div class="rowgroup">
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel blue">Customer</label>
                        <label>
                            <asp:Literal runat="server" ID="litCustomer" />
                            <asp:HyperLink id="hypCustomer" runat="server" target="_blank" Visible='<%# ActiveUser.HasAccessTo(ViewCode.Customer) %>' ToolTip="Go To Customer Record">
                                <asp:Image ImageUrl="~/images/icons2/arrow_newTab.png" runat="server" Width="16px" CssClass="middle"/>
                             </asp:HyperLink>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel blue">Origin</label>
                        <label>
                            <asp:Literal runat="server" ID="litOriginLocation" /></label>
                    </div>
                </div>
            </div>
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel blue">Vendor</label>
                        <label>
                            <asp:Literal runat="server" ID="litPrimaryVendor" />
                            <asp:HyperLink id="hypVendor" runat="server" target="_blank" ToolTip="Go To Vendor Record">
                                <asp:Image ImageUrl="~/images/icons2/arrow_newTab.png" runat="server" Width="16px" CssClass="middle"/>
                             </asp:HyperLink>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel blue">Destination</label>
                        <label>
                            <asp:Literal runat="server" ID="litDestinationLocation" /></label>
                    </div>
                </div>
            </div>
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel blue">Prefix</label>
                        <label>
                            <asp:Literal runat="server" ID="litPrefix" /></label>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel blue">Pro Number</label>
                        <label>
                            <asp:Literal runat="server" ID="litPrimaryVendorPro" />
                            <asp:HyperLink runat="server" ID="hypPrimaryVendorPro" Target="_blank">
                                <asp:Image runat="server" ImageUrl="~/images/icons2/externalLink.png" AlternateText="external link image" Height="12px" CssClass="ml5" ToolTip="Track on vendor website"/>
                            </asp:HyperLink>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </td>
</tr>
