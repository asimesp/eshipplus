﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MacroPointOrderInputControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.MacroPointOrderInputControl" %>
<asp:Panel runat="server" ID="pnlMacroPointOrderInput" CssClass="popupControl popupControlOverW500" Visible="false">
    <div class="popheader">
        <h4>Macro Point Order Input</h4>
        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseOrderInputClicked" runat="server" />
    </div>
    <asp:UpdatePanel runat="server" ID="udpMacroPointOrderInput">
        <ContentTemplate>
            <div class="row pt10">
                <table class="poptable">
                    <tr>
                        <td class="text-right">
                            <label class="upper">Number Type:</label>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlNumberType" DataValueField="Value" DataTextField="Text" CssClass="w100" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">
                                <abbr title='Mobile number must be 10 digits long'>Number:</abbr>
                            </label>
                        </td>
                        <td>
                            <eShip:CustomTextBox ID="txtNumber" CssClass="w300" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">Date to Start Tracking:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtTrackStartDate" CssClass="w100" Type="Date" placeholder="99/99/9999" AutoPostBack="True" CausesValidation="True" OnTextChanged="OnDateTimePickerTextChanged" />
                            <asp:DropDownList runat="server" ID="ddlTime" DataValueField="Value" DataTextField="Text" CssClass="w100" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">Track Options:</label>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlTrackingOptions" DataValueField="Value" DataTextField="Text" CssClass="w300" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right top">
                            <label class="upper">
                                Email Updates To:
                            </label>
                            <br />
                            <label class="lhInherit">
                                <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleEmailAddresses" TargetControlId="txtEmailAddresses" MaxLength="500" />
                            </label>
                        </td>
                        <td>
                            <eShip:CustomTextBox ID="txtEmailAddresses" runat="server" TextMode="MultiLine" CssClass="w300 h150" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right top">
                            <label class="upper">
                                Notes:
                            </label>
                            <br />
                            <label class="lhInherit">
                                <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleNotes" TargetControlId="txtNotes" MaxLength="500" />
                            </label>
                        </td>
                        <td>
                            <eShip:CustomTextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="w300 h150" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="pt10">
                            <asp:Button ID="btnSubmitOrder" Text="Submit Order" OnClick="OnSubmitOrderClicked" runat="server" CausesValidation="false" />
                            <asp:Button ID="btnCancelOrderInput" Text="Cancel" OnClick="OnCloseOrderInputClicked" runat="server" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="txtTrackStartDate" />
            <asp:PostBackTrigger ControlID="btnSubmitOrder" />
            <asp:PostBackTrigger ControlID="btnCancelOrderInput" />
            <asp:PostBackTrigger ControlID="ibtnClose" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
