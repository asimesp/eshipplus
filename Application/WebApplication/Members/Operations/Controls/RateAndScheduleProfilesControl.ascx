﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RateAndScheduleProfilesControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.RateAndScheduleProfilesControl" %>
<label class="pr10">
    Rating Profile Options:
</label>

<asp:Button runat="server" ID="btnSaveProfileAsDefault" CausesValidation="False" Text="    Add    " />
<asp:Button runat="server" ID="btnUpdateProfile" CausesValidation="False" Text="Update" Visible="False" OnClick="OnUpdateProfileClicked" />
<asp:Button runat="server" ID="btnDeleteCurrentProfile" CausesValidation="False" OnClick="OnDeleteProfileClicked" Text="Delete" CssClass="mr5" />

<asp:DropDownList runat="server" ID="ddlRandDProfiles" DataValueField="Value" DataTextField="Text" AutoPostBack="True" CausesValidation="False"
                  OnSelectedIndexChanged="OnSearchProfilesSelectedIndexChanged" CssClass="middle w200" />

<script type="text/javascript">
    $(document).ready(function () {
        $(jsHelper.AddHashTag('<%= btnSearchProfileClose.ClientID %>')).click(function (e) {
            e.preventDefault();
            $(jsHelper.AddHashTag('profileNameDiv<%= ClientID %>')).hide('slow');
            $(jsHelper.AddHashTag('<%= rateProCtrlPnlDimScreenJS.ClientID %>')).hide();
        });

        $(jsHelper.AddHashTag('<%= btnSaveProfileAsDefault.ClientID %>')).click(function (e) {
            $(jsHelper.AddHashTag('profileNameDiv<%= ClientID %>')).show('slow');
            $(jsHelper.AddHashTag('<%= rateProCtrlPnlDimScreenJS.ClientID %>')).show();
            jsHelper.SetEmpty('<%= txtSearchProfileName.ClientID %>');
            e.preventDefault();
        });
    });
</script>

<div id="profileNameDiv<%= ClientID %>" class="popupControlOver popupControlOverW500 inherit-left inherit-top" style="display: none;">
    <div class="popheader">
        <h4>Saved Profile(s)</h4>
    </div>
    <table class="poptable">
        <tr>
            <td class="text-right"><label class="upper">Profile Name:</label>
            </td>
            <td class="text-left pt10 pb20">
                <asp:TextBox runat="server" ID="txtSearchProfileName" CssClass="w230" />
                <asp:Button runat="server" ID="btnSaveSearchProfile" Text="   Save   " CausesValidation="False" CssClass="ml10" OnClick="OnSaveProfileClicked" />
                <asp:Button runat="server" ID="btnSearchProfileClose" Text="Cancel" CausesValidation="False" />
            </td>
        </tr>
    </table>
</div>

<asp:Panel ID="rateProCtrlPnlDimScreenJS" CssClass="dimBackgroundControlOver" runat="server" Style="display: none;" />
