﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FindMatchingUserControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.FindMatchingUserControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<asp:Panel runat="server" ID="pnlMatchingUserSearch" CssClass="popupControl popupControlOverW750" DefaultButton="btnFindMatchingUsers">
    <div class="popheader">
        <h4>Find Matching User
             <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                 CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
        </h4>
    </div>
	
    <table class="poptable pt16 ">
	    <tr><br/>
			</tr>
        <tr>
	        <td class="pl20">
		        <div class="fieldgroup">
			        <label class="upper">First Name</label>
			        <eShip:CustomTextBox runat="server" ID="txtFirstName" CssClass="w150" MaxLength="10"  />
		        </div>
	        </td>
	        <td>
		        <div class="fieldgroup">
			        <label class="upper">Last Name</label>
			        <eShip:CustomTextBox runat="server" ID="txtLastName" CssClass="w150" MaxLength="10" />
		        </div>
	        </td>
	        <td>
		        <div class="fieldgroup">
			        <label class="upper">Username</label>
			        <eShip:CustomTextBox runat="server" ID="txtUserName" CssClass="w150" MaxLength="50" />
		        </div>
	        </td>
	        <td>
		        <div class="fieldgroup">
			        <label class="upper">Email</label>
			        <eShip:CustomTextBox runat="server" ID="txtEmail" CssClass="w150" MaxLength="50" />
		        </div>
	        </td>
        </tr>
   
	    <tr>
		    <td></td><td></td><td></td>
		    <td class="pb10 pt20 pr10 right">
			    <div>
				    <asp:Button runat="server" ID="btnFindMatchingUsers" Text="SEARCH" OnClick="OnFindMatchingUsersClicked" CausesValidation="false" />
				    <asp:Button runat="server" ID="btnCancel" Text="CANCEL" OnClick="OnCloseClicked" CausesValidation="false" /> 
			    </div>
		    </td>
	    </tr>
     </table>
	 <eShip:TableFreezeHeaderExtender runat="server" ID="tfheUsersFinderTable" TableId="matchingUserFinder" ScrollerContainerId="finderScrollSection" ScrollOffsetControlId="pnlMatchingUserSearch"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
              <div class="finderScroll" id="finderScrollSection">
				
                <asp:ListView runat="server" ID="lstMatchingUsers">
                    <LayoutTemplate>                        
	                         <script type="text/javascript">$(window).load(function () { SetControlFocus(); });</script>
                            <table id="matchingUserFinder" class="stripe sm_chk">
                                <tr>
                                   <th style="width: 25%">Username
                                    </th>
                                    <th style="width: 20%">First Name
                                    </th>
									 <th style="width: 20%">Last Name
                                    </th>
									 <th style="width: 25%">Email
                                    </th>
                                    <th style="width: 5%">Go To</th>
                                </tr>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                           <td>
                                <%# Eval("Username") %>
                            </td>
                            <td>
                                <%# Eval("FirstName") %>
                            </td>
							  <td>
                                <%# Eval("LastName") %>
                            </td>
							  <td>
                                <%# Eval("Email") %>
                            </td>
                            <td>
                                <asp:ImageButton ID="ImageButton1" runat="server" OnClick="OnIndexClicked" ImageUrl="~/images/icons2/arrow_general.png" AlternateText="goto page" ToolTip='<%# GotoPage + Eval("Index").GetString() %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
					</div>
           
</asp:Panel>
<eShip:CustomHiddenField runat="server" ID="hidGroupId" />
<asp:Panel ID="pnlMachingUserFinderDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
