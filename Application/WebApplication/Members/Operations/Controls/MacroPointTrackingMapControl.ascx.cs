﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class MacroPointTrackingMapControl : MemberControlBase
    {
        public new bool Visible
        {
            get { return pnlTrackingMap.Visible; }
            set
            {
                base.Visible = value;
                pnlTrackingMap.Visible = value;
                pnlDimScreen.Visible = value;
            }
        }

        public bool CanRequestLocationUpdate
        {
            get
            {
                return pnlRequestLocationUpdate.Visible;
            }
            set
            {
                pnlRequestLocationUpdate.Visible = value;
            }
        }

        public event EventHandler Close;
        public event EventHandler RequestLocationUpdate;


        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void OnCloseTrackingMapClicked(object sender, ImageClickEventArgs e)
        {
            if (Close != null)
                Close(this, new EventArgs());
        }

        protected void OnRequestLocationUpdateClicked(object sender, EventArgs e)
        {
            if (RequestLocationUpdate != null)
                RequestLocationUpdate(this, new EventArgs());
        }



        public void SetupMap(List<double[]> points)
        {
            hidCoordinatesString.Value = string.Empty;
            if (points != null && points.Any())
                foreach (var point in points)
                    hidCoordinatesString.Value += string.Format("{0},{1};", point[0], point[1]);
        }
    }
}