﻿namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.InputControls
{
	public enum OperationsAddressInputType
	{
		Origin,
		Destination,
		Stop,
	}
}