﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OperationsAddressInputControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.InputControls.OperationsAddressInputControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>

<script type="text/javascript">
    $(function () {
        setVisibilities();
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
            setVisibilities();
        });

        function setVisibilities() {
            $(jsHelper.AddHashTag('<%= pnlAdditionalDetails.ClientID %>')).hide();
            $(jsHelper.AddHashTag('<%= pnlOperationsAddressInput.ClientID %>')).show();

            $(jsHelper.AddHashTag("<%= ClientID %>ToggleMoreDetails")).click(function () {
                $(jsHelper.AddHashTag('<%= pnlAdditionalDetails.ClientID %>')).show();
                $(jsHelper.AddHashTag('<%= pnlOperationsAddressInput.ClientID %>')).hide();
            });

            $(jsHelper.AddHashTag("<%= ClientID %>ToggleAddressDetails")).click(function () {
                $(jsHelper.AddHashTag('<%= pnlAdditionalDetails.ClientID %>')).hide();
                $(jsHelper.AddHashTag('<%= pnlOperationsAddressInput.ClientID %>')).show();
            });
        }
    });
</script>

<asp:Panel ID="pnlOperationsAddressInput" runat="server" CssClass="rowgroup">
    <h5>
        <asp:Literal runat="server" ID="litTitle" />
        Location
        <span class="right pr20">
            <input type="button" value="More Detail" id="<%= ClientID %>ToggleMoreDetails">
        </span>
    </h5>
    <div class="row">
        <div class="fieldgroup">
            <label class="wlabel">Name/Description</label>
            <script type="text/javascript">
                $(function () {
                    bindOnChangeToDescriptionTextbox();
                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                        bindOnChangeToDescriptionTextbox();
                    });

                    function bindOnChangeToDescriptionTextbox() {
                        $(jsHelper.AddHashTag('<%= txtDescription.ClientID %>')).change(function () {
                            var txt = $(this).val();
                            if (txt.length == 0) return;
                            var idx = txt.indexOf('<%= AutoCompleteUtility.AutoCompleteSeperator %>');
                            if (idx != -1 && txt[0] == '<%= AutoCompleteUtility.Equal %>') {
                                setTimeout('__doPostBack(\'<%= txtDescription.ClientID %>\')', 0);
                            }
                        });
                    }
                });
                
                function FormatAddressInputAutoComplete() {
                    $("ul[id*='aceOperationsAddressInputDescription'] li").each(function () {
                        var html = String($(this).html())
                            .replace(/&lt;/g, '<')
                            .replace(/&gt;/g, '>')
                            .replace(/&amp;/g, '&');
                        $(this).html(html);
                    });
                }
            </script>

            <eShip:CustomTextBox ID="txtDescription" runat="server" MaxLength="50" CssClass="w280" OnTextChanged="OnDescriptionTextChanged" />
            <asp:ImageButton ID="imgAddressBookSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                CausesValidation="False" OnClick="OnAddFromAddressBookClicked" />
            <ajax:AutoCompleteExtender runat="server" ID="aceOperationsAddressInputDescription" TargetControlID="txtDescription" ServiceMethod="GetAddressList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" OnClientPopulated="FormatAddressInputAutoComplete" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup">
            <label class="wlabel">Street Address Line 1</label>
            <eShip:CustomTextBox ID="txtStreet1" runat="server" MaxLength="50" CssClass="w315" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup">
            <label class="wlabel">Street Address Line 2</label>
            <eShip:CustomTextBox ID="txtStreet2" runat="server" MaxLength="50" CssClass="w315" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup mr10">
            <label class="wlabel">City</label>
            <eShip:CustomTextBox ID="txtCity" runat="server" MaxLength="50" CssClass="w150" />
        </div>
        <div class="fieldgroup">
            <label class="wlabel">State</label>
            <eShip:CustomTextBox ID="txtState" runat="server" MaxLength="50" CssClass="w150" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup mr10">
            <label class="wlabel">Country</label>
            <eShip:CachedObjectDropDownList Type="Countries" ID="ddlCountries" runat="server" CssClass="w150" EnableChooseOne="True" DefaultValue="0" />
        </div>
        <div class="fieldgroup">
            <label class="wlabel">Postal Code</label>
            <script type="text/javascript">
                $(function () {
                    $('#<%= txtPostalCode.ClientID %>').change(function () {
                        getCityAndState();
                    });

                    $(jsHelper.AddHashTag('<%= ddlCountries.ClientID %>')).change(function () {
                        getCityAndState();
                    });
                    
                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                        $('#<%= txtPostalCode.ClientID %>').change(function () {
                            getCityAndState();
                        });

                        $(jsHelper.AddHashTag('<%= ddlCountries.ClientID %>')).change(function () {
                            getCityAndState();
                        });
                    });
                    
                    function getCityAndState() {
                        var ids = new ControlIds();
                        ids.City = $('#<%= txtCity.ClientID %>').attr('id');
                        ids.State = $('#<%= txtState.ClientID %>').attr('id');
                        FindCityAndState(
                            $('#<%= txtPostalCode.ClientID %>').val(),
                            $('#<%= ddlCountries.ClientID %>').val(),
                            ids
                        );
                    }
                });
            </script>

            <eShip:CustomTextBox ID="txtPostalCode" runat="server" MaxLength="10" CssClass="w110" />
            <asp:ImageButton ID="imgPostalCodeSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                CausesValidation="False" OnClick="OnPostalCodeSearchClicked" />
        </div>
    </div>
    <div class="row bottom_shadow pb5">
        <div class="fieldgroup">
            <label class="wlabel">
                Instructions
                <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleInstructions" TargetControlId="txtInstructions" MaxLength="500" />
            </label>
            <eShip:CustomTextBox ID="txtInstructions" runat="server" TextMode="MultiLine" CssClass="w315" />
        </div>
    </div>

    <asp:UpdatePanel runat="server" ID="upContacts" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlContacts" CssClass="rowgroup mt10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:ImageButton runat="server" ID="ibtnAddContacts" Text="Add Contact" OnClick="OnAddContactClicked" ImageUrl="~/images/icons2/add_button.png"
                            Height="20px" ToolTip="Add Contacts" OnClientClick="ShowProgressNotice(true);" CausesValidation="False" />
                        <label class="upper blue">Contacts</label>
                    </div>
                    <div class="fieldgroup right pr20">
                        <eShip:PaginationExtender runat="server" ID="peLstLocationContacts" TargetControlId="lstLocationContacts" PageSize="1" UseParentDataStore="False" OnPageChanging="OnLocationContactsPageChanging" />
                    </div>
                </div>
                <div class="row">
                    <asp:ListView ID="lstLocationContacts" runat="server" ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <table>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <div class="rowgroup bottom_shadow pb5">
                                        <div class="row">
                                            <div class="fieldgroup mr10">
                                                <label class="wlabel">Name</label>
                                                <eShip:CustomHiddenField runat="server" ID="hidContactId" Value='<%# Eval("Id") %>' />
                                                <eShip:CustomHiddenField runat="server" ID="hidContactIndex" Value='<%# Container.DataItemIndex + (peLstLocationContacts.PageSize*(peLstLocationContacts.CurrentPage-1)) %>' />
                                                <eShip:CustomTextBox ID="txtName" Text='<%# Eval("Name") %>' runat="server" MaxLength="50" CssClass="w280" />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProgressNotice(true);" Visible='<%# peLstLocationContacts.CurrentPage != 1 %>'
                                                    CausesValidation="false" OnClick="OnDeleteContactClicked" />
                                                <asp:Literal runat="server" Visible='<%# peLstLocationContacts.CurrentPage == 1 %>'>
                                                    <label class="flag-shipment"><abbr title="Primary Contact. Cannot Delete Primary Contact" class="blue">P</abbr></label>
                                                </asp:Literal>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup mr10">
                                                <label class="wlabel">Phone</label>
                                                <eShip:CustomTextBox ID="txtPhone" Text='<%# Eval("Phone") %>' runat="server" MaxLength="50" CssClass="w150" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Mobile</label>
                                                <eShip:CustomTextBox ID="txtMobile" Text='<%# Eval("Mobile") %>' runat="server" MaxLength="50" CssClass="w150" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup mr10">
                                                <label class="wlabel">Fax</label>
                                                <eShip:CustomTextBox ID="txtFax" Text='<%# Eval("Fax") %>' runat="server" MaxLength="50" CssClass="w150" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Email</label>
                                                <eShip:CustomTextBox ID="txtEmail" Text='<%# Eval("Email") %>' runat="server" MaxLength="100" CssClass="w150" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup mr10">
                                                <label class="wlabel">Contact Type</label>
                                                <eShip:CachedObjectDropDownList Type="ContactTypes" ID="ddlContactContactType" runat="server" CssClass="w150" 
                                                    EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("ContactTypeId") %>'/>
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Comment</label>
                                                <eShip:CustomTextBox ID="txtComment" Text='<%# Eval("Comment") %>' runat="server" MaxLength="200" CssClass="w150" />
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="pnlAdditionalDetails" runat="server" CssClass="rowgroup">
    <h5>
        <asp:Literal runat="server" ID="litTitle2" />
        More Details
        <span class="right pr20">
            <input type="button" value="Address Info" id="<%= ClientID %>ToggleAddressDetails">
        </span>
    </h5>
    <div class="row">
        <div class="fieldgroup mr20">
            <label class="wlabel">Appointment Date</label>
            <eShip:CustomTextBox runat="server" ID="txtAppointmentDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
        </div>
        <div class="fieldgroup">
            <label class="wlabel">Appointment Time</label>
            <eShip:CustomTextBox runat="server" ID="txtTime" CssClass="w55" MaxLength="5" placeholder="99:99" Type="Time" />
        </div>
    </div>
    <div class="row mt20">
        <div class="fieldgroup">
            <label class="wlabel">
                General Information
                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleGeneralInfo" TargetControlId="txtGeneralInfo" MaxLength="2000" />
            </label>
            <eShip:CustomTextBox ID="txtGeneralInfo" runat="server" TextMode="MultiLine" CssClass="w310 h275" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup">
            <label class="wlabel">
                Directions
                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDirections" TargetControlId="txtDirection" MaxLength="2000" />
            </label>
            <eShip:CustomTextBox ID="txtDirection" runat="server" TextMode="MultiLine" CssClass="w310 h275" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup">
            <label class="wlabel">
                Miles From Previous Location/Stop
            </label>

            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <eShip:CustomTextBox runat="server" ID="txtMilesFromPreviousStop" CssClass="w100" Type="FloatingPointNumbers" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Panel>

<asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
<eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
<eShip:CustomHiddenField runat="server" ID="hidInputType" />
<eShip:CustomHiddenField runat="server" ID="hidIsDestination" />
<eShip:CustomHiddenField runat="server" ID="hidCanAddressBookAdd" />
<eShip:CustomHiddenField runat="server" ID="hidContactObjectTypeLoaded" />
