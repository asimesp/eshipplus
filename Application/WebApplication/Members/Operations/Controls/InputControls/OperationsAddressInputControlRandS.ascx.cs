﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.InputControls
{
	public partial class OperationsAddressInputControlRandS : MemberControlBase
	{
		public OperationsAddressInputType Type
		{
			get { return hidAddressType.Value.ToEnum<OperationsAddressInputType>(); }
			set
			{
				hidAddressType.Value = value.ToString();
				litTitle.Text = value.FormattedString();
			}
		}


		public event EventHandler<ViewEventArgs<string>> AddAddressFromAddressBook;
		public event EventHandler<ViewEventArgs<string>>  FindPostalCode;


		public void SetRatingEditStatus(bool enabled)
		{
			imgAddressBookSearch.Enabled = enabled;
			imgPostalCodeSearch.Enabled = enabled;
			txtPostalCode.Enabled = enabled;
			ddlCountries.Enabled = enabled;
		    aceOperationsAddressInput2Description.Enabled = enabled;
		}



		public void LoadPostalCode(PostalCodeViewSearchDto postalCode)
		{
			txtCity.Text = postalCode.City;
			txtState.Text = postalCode.State;
			txtPostalCode.Text = postalCode.Code;
			ddlCountries.SelectedValue = postalCode.CountryId.GetString();
		}

		public void LoadAddress<T>(T location) where T : Location
		{
			if (location == null) return;

			LoadLocation(location);

			if (location as LoadOrderLocation != null) LoadLoadOrderData(location as LoadOrderLocation);
			if (location as ShipmentLocation != null) LoadShipmentData(location as ShipmentLocation);
			if (location as AddressBook != null) LoadAddressBookRecord(location as AddressBook);
		}

		public void RetrieveAddress<T>(T location) where T : Location
		{
			if (location == null) return;

			location.Street1 = txtStreet1.Text;
			location.Street2 = txtStreet2.Text;
			location.City = txtCity.Text;
			location.State = txtState.Text;
			location.PostalCode = txtPostalCode.Text;
			location.CountryId = ddlCountries.SelectedValue.ToInt();

			if (location as LoadOrderLocation != null) RetrieveLoadOrderData(location as LoadOrderLocation);
			if (location as ShipmentLocation != null) RetrieveShipmentData(location as ShipmentLocation);
		}

		public void ClearFields()
		{
			LoadLocation(new Location());

			txtDescription.Text = string.Empty;
			txtInstructions.Text = string.Empty;

			txtPrimaryName.Text = string.Empty;
			txtPrimaryPhone.Text = string.Empty;
			txtPrimaryMobile.Text = string.Empty;
			txtPrimaryFax.Text = string.Empty;
			txtPrimaryEmail.Text = string.Empty;
			txtPrimaryComment.Text = string.Empty;

			ddlPrimaryContactType.SelectedValue = ActiveUser.Tenant.DefaultSchedulingContactTypeId.GetString();
		}

		public void SetCustomerIdSpecificFilter(long customerId)
		{
			SetContextKeys(customerId);
		}



		private void LoadLocation(Location location)
		{
			txtStreet1.Text = location.Street1;
			txtStreet2.Text = location.Street2;
			txtCity.Text = location.City;
			txtState.Text = location.State;
			txtPostalCode.Text = location.PostalCode;
			ddlCountries.SelectedValue = location.CountryId.GetString();
		}

		private void LoadPrimaryContactDetails(Contact primaryContact)
		{
			txtPrimaryName.Text = primaryContact.Name;
			txtPrimaryEmail.Text = primaryContact.Email;
			txtPrimaryPhone.Text = primaryContact.Phone;
			txtPrimaryMobile.Text = primaryContact.Mobile;
			txtPrimaryFax.Text = primaryContact.Fax;
			ddlPrimaryContactType.SelectedValue = primaryContact.ContactTypeId.GetString();
			txtPrimaryComment.Text = primaryContact.Comment;
		}



		private void LoadLoadOrderData(LoadOrderLocation location)
		{
			txtDescription.Text = location.Description;
			txtInstructions.Text = location.SpecialInstructions;
			txtDirection.Text = location.Direction;
			txtGeneralInfo.Text = location.GeneralInfo;

			var primaryContact = location.Contacts.FirstOrDefault(c => c.Primary) ?? new LoadOrderContact();

			LoadPrimaryContactDetails(primaryContact);
		}

		private void LoadShipmentData(ShipmentLocation location)
		{
			txtDescription.Text = location.Description;
			txtInstructions.Text = location.SpecialInstructions;
			txtDirection.Text = location.Direction;
			txtGeneralInfo.Text = location.GeneralInfo;

			var primaryContact = location.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
			LoadPrimaryContactDetails(primaryContact);
		}

		private void LoadAddressBookRecord(AddressBook address)
		{
			LoadLocation(address);
			txtDescription.Text = address.Description;
			txtInstructions.Text = Type == OperationsAddressInputType.Origin
									   ? address.OriginSpecialInstruction
									   : Type == OperationsAddressInputType.Destination
											 ? address.DestinationSpecialInstruction
											 : string.Empty;
			txtDirection.Text = address.Direction;
			txtGeneralInfo.Text = address.GeneralInfo;
			LoadPrimaryContactDetails(address.Contacts.FirstOrDefault(c => c.Primary) ?? new AddressBookContact());
		}


		private void RetrieveLoadOrderData(LoadOrderLocation location)
		{
			location.Description = txtDescription.Text;
			location.SpecialInstructions = txtInstructions.Text;
			location.Direction = txtDirection.Text;
			location.GeneralInfo = txtGeneralInfo.Text;
			location.AppointmentDateTime = DateUtility.SystemEarliestDateTime;

			var primaryContact = new LoadOrderContact
			{
				Name = txtPrimaryName.Text,
				Phone = txtPrimaryPhone.Text,
				Mobile = txtPrimaryMobile.Text,
				Fax = txtPrimaryFax.Text,
				Email = txtPrimaryEmail.Text.StripSpacesFromEmails(),
				Comment = txtPrimaryComment.Text,
				Primary = true,
				ContactTypeId = ddlPrimaryContactType.SelectedValue.ToLong(),
				TenantId = location.TenantId,
				Location = location
			};

			location.Contacts = new List<LoadOrderContact> { primaryContact };
		}

		private void RetrieveShipmentData(ShipmentLocation location)
		{
			location.Description = txtDescription.Text;
			location.SpecialInstructions = txtInstructions.Text;
			location.Direction = txtDirection.Text;
			location.GeneralInfo = txtGeneralInfo.Text;
			location.AppointmentDateTime = DateUtility.SystemEarliestDateTime;

			var primaryContact = new ShipmentContact
			{
				Name = txtPrimaryName.Text,
				Phone = txtPrimaryPhone.Text,
				Mobile = txtPrimaryMobile.Text,
				Fax = txtPrimaryFax.Text,
				Email = txtPrimaryEmail.Text.StripSpacesFromEmails(),
				Comment = txtPrimaryComment.Text,
				Primary = true,
				ContactTypeId = ddlPrimaryContactType.SelectedValue.ToLong(),
				TenantId = location.TenantId,
				Location = location
			};

			location.Contacts = new List<ShipmentContact> { primaryContact };
		}


		private void SetContextKeys(long customerId)
		{
			var values = new Dictionary<string, string>
				{
					{AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString()},
					{AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()},
					{AutoCompleteUtility.Keys.Customer, customerId.GetString()},
				};
			aceOperationsAddressInput2Description.SetupExtender(values, 3);
		}

		protected bool ValueIsNotEmpty(object value)
		{
			return value != null && !string.IsNullOrEmpty(value.ToString());
		}



		protected void Page_Load(object sender, EventArgs e)
		{
			// account for missing option to search
			imgAddressBookSearch.Visible = AddAddressFromAddressBook != null;
			txtDescription.CssClass = !imgAddressBookSearch.Visible ? "w315" : "w280";

			imgPostalCodeSearch.Visible = FindPostalCode != null;
            txtPostalCode.CssClass = !imgPostalCodeSearch.Visible ? "w150 requiredInput" : "w110 requiredInput";

			if (IsPostBack) return;

			// context key
			SetContextKeys(default(long));

			ddlCountries.SelectedValue = ActiveUser.Tenant.DefaultCountryId.GetString();
			if (ddlPrimaryContactType.SelectedValue.ToLong() == default(long))
				ddlPrimaryContactType.SelectedValue = ActiveUser.Tenant.DefaultSchedulingContactTypeId.GetString();
		}




		protected void OnAddFromAddressBookClicked(object sender, EventArgs e)
		{
			if (AddAddressFromAddressBook != null)
				AddAddressFromAddressBook(this, new ViewEventArgs<string>(txtDescription.Text));
		}

		protected void OnDescriptionTextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtDescription.Text)) return;

			var key = txtDescription.Text.Substring(1).RetrieveSearchCodeFromAutoCompleteString().ToLong();
			var address = new AddressBook(key);
			if (!address.KeyLoaded) return;
			LoadAddressBookRecord(address);
		}

	


		protected void OnPostalCodeSearchClicked(object sender, ImageClickEventArgs e)
		{
			if (FindPostalCode != null)
				FindPostalCode(this, new ViewEventArgs<string>(txtPostalCode.Text));
		}
	}
}