﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OperationsAddressInputControlRandS.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.InputControls.OperationsAddressInputControlRandS" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>

<script type="text/javascript">
    $(function () {
        $(jsHelper.AddHashTag('<%= pnlAdditionalDetails.ClientID %>')).hide();
        $(jsHelper.AddHashTag('<%= pnlOperationsAddressInput.ClientID %>')).show();

        $(jsHelper.AddHashTag("<%= ClientID %>ToggleMoreDetails")).click(function () {
            $(jsHelper.AddHashTag('<%= pnlAdditionalDetails.ClientID %>')).show();
            $(jsHelper.AddHashTag('<%= pnlOperationsAddressInput.ClientID %>')).hide();
        });

        $(jsHelper.AddHashTag("<%= ClientID %>ToggleAddressDetails")).click(function () {
            $(jsHelper.AddHashTag('<%= pnlAdditionalDetails.ClientID %>')).hide();
            $(jsHelper.AddHashTag('<%= pnlOperationsAddressInput.ClientID %>')).show();
        });
    });
</script>



<asp:Panel ID="pnlOperationsAddressInput" runat="server">

    <h5>
        <asp:Literal runat="server" ID="litTitle" />
        Location
        <span class="right pr20">
            <input type="button" value="More Detail" id="<%= ClientID %>ToggleMoreDetails">
        </span>
    </h5>
    <div class="row">
        <div class="fieldgroup">
            <label class="wlabel">Name/Description <span class="red">*</span> </label>
            <script type="text/javascript">
                $(document).ready(function () {
                    $(jsHelper.AddHashTag('<%= txtDescription.ClientID %>')).change(function () {
                        var txt = $(this).val();
                        if (txt.length == 0) return;
                        var idx = txt.indexOf('<%= AutoCompleteUtility.AutoCompleteSeperator %>');
                        if (idx != -1 && txt[0] == '<%= AutoCompleteUtility.Equal %>') {
                            setTimeout('__doPostBack(\'<%= txtDescription.ClientID %>\')', 0);
                        }
                    });
                });

                function FormatAddressInputAutoComplete() {
                    $("ul[id*='aceOperationsAddressInput2Description'] li").each(function () {
                        var html = String($(this).html())
                            .replace(/&lt;/g, '<')
                            .replace(/&gt;/g, '>')
                            .replace(/&amp;/g, '&');
                        $(this).html(html);
                    });
                }
            </script>

            <eShip:CustomTextBox ID="txtDescription" runat="server" MaxLength="50" CssClass="w280" OnTextChanged="OnDescriptionTextChanged" />
            <asp:ImageButton ID="imgAddressBookSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                CausesValidation="False" OnClick="OnAddFromAddressBookClicked" />
            <ajax:AutoCompleteExtender runat="server" ID="aceOperationsAddressInput2Description" TargetControlID="txtDescription" ServiceMethod="GetAddressList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" OnClientPopulated="FormatAddressInputAutoComplete" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup">
            <label class="wlabel">Street Address Line 1 <span class="red">*</span></label>
            <eShip:CustomTextBox ID="txtStreet1" runat="server" MaxLength="50" CssClass="w315" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup">
            <label class="wlabel">Street Address Line 2</label>
            <eShip:CustomTextBox ID="txtStreet2" runat="server" MaxLength="50" CssClass="w315" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup mr10">
            <label class="wlabel">City <span class="red">*</span></label>
            <eShip:CustomTextBox ID="txtCity" runat="server" MaxLength="50" CssClass="w150" />
        </div>
        <div class="fieldgroup">
            <label class="wlabel">State <span class="red">*</span></label>
            <eShip:CustomTextBox ID="txtState" runat="server" MaxLength="50" CssClass="w150" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup mr10">
            <label class="wlabel">Country <span class="red">*</span></label>
            <eShip:CachedObjectDropDownList Type="Countries" ID="ddlCountries" runat="server" CssClass="requiredInput w150" EnableChooseOne="True" DefaultValue="0" />
        </div>
        <div class="fieldgroup">
            <label class="wlabel">Postal Code <span class="red">*</span></label>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#<%= txtPostalCode.ClientID %>').change(function () {
                        GetCityAndState();
                    });
                    $(jsHelper.AddHashTag('<%= ddlCountries.ClientID %>')).change(function () {
                        GetCityAndState();
                    });

                    function GetCityAndState() {
                        var ids = new ControlIds();
                        ids.City = $('#<%= txtCity.ClientID %>').attr('id');
                        ids.State = $('#<%= txtState.ClientID %>').attr('id');
                        FindCityAndState(
                            $('#<%= txtPostalCode.ClientID %>').val(),
                            $('#<%= ddlCountries.ClientID %>').val(),
                            ids
                        );
                    }
                });
            </script>

            <eShip:CustomTextBox ID="txtPostalCode" runat="server" MaxLength="6" CssClass="w110 requiredInput" />
            <asp:ImageButton ID="imgPostalCodeSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                CausesValidation="False" OnClick="OnPostalCodeSearchClicked" />
        </div>
    </div>
    <div class="row">
        <label class="wlabel">
            Instructions
            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleInstructions" TargetControlId="txtInstructions" MaxLength="500" />
        </label>
        <eShip:CustomTextBox ID="txtInstructions" runat="server" TextMode="MultiLine" CssClass="w315" />
    </div>
    <div class="row">
        <div class="fieldgroup mr10">
            <label class="wlabel">Contact <span class="red">*</span></label>
            <eShip:CustomTextBox ID="txtPrimaryName" runat="server" MaxLength="50" CssClass="w150" />
        </div>
        <div class="fieldgroup">
            <label class="wlabel">Phone <span class="red">*</span></label>
            <eShip:CustomTextBox ID="txtPrimaryPhone" runat="server" MaxLength="50" CssClass="w150" />
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlAdditionalDetails" runat="server">
    <h5>
        <asp:Literal runat="server" ID="litTitle2" />
        More Details
        <span class="right pr20">
            <input type="button" value="Address Info" id="<%= ClientID %>ToggleAddressDetails">
        </span>
    </h5>
    <div class="row">
        <div class="fieldgroup mr10">
            <label class="wlabel">Fax</label>
            <eShip:CustomTextBox ID="txtPrimaryFax" runat="server" MaxLength="50" CssClass="w150" />
        </div>
        <div class="fieldgroup">
            <label class="wlabel">Mobile</label>
            <eShip:CustomTextBox ID="txtPrimaryMobile" runat="server" MaxLength="50" CssClass="w150" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup mr10">
            <label class="wlabel">Email</label>
            <eShip:CustomTextBox ID="txtPrimaryEmail" runat="server" MaxLength="50" CssClass="w150" />
        </div>
        <div class="fieldgroup">
            <label class="wlabel">Contact Type <span class="red">*</span></label>
            <eShip:CachedObjectDropDownList Type="ContactTypes" ID="ddlPrimaryContactType" runat="server" CssClass="w150" EnableChooseOne="True" DefaultValue="0" />
        </div>
    </div>
    <div class="row bottom_shadow pb5">
        <div class="fieldgroup">
            <label class="wlabel">Comments</label>
            <eShip:CustomTextBox ID="txtPrimaryComment" runat="server" MaxLength="50" CssClass="w315" />
        </div>
    </div>

    <div class="row">
        <div class="fieldgroup">
            <label class="wlabel">
                General Information
                <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleGeneralInfo" TargetControlId="txtGeneralInfo" MaxLength="2000" />
            </label>
            <eShip:CustomTextBox ID="txtGeneralInfo" runat="server" TextMode="MultiLine" CssClass="w315" />
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup">
            <label class="wlabel">
                Directions
                <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDirections" TargetControlId="txtDirection" MaxLength="2000" />
            </label>
            <eShip:CustomTextBox ID="txtDirection" runat="server" TextMode="MultiLine" CssClass="w315" />
        </div>
    </div>
</asp:Panel>

<asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
<eShip:CustomHiddenField runat="server" ID="hidAddressType" />
