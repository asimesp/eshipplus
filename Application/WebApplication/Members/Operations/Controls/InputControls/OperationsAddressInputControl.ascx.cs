﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.InputControls
{
	public partial class OperationsAddressInputControl : MemberControlBase
	{
		private const string ShipmentContact = "SC";
		private const string LoadOrderContact = "LOC";

		public OperationsAddressInputType Type
		{
			get { return hidInputType.Value.ToEnum<OperationsAddressInputType>(); }
			set
			{
				hidInputType.Value = value.ToString();
				litTitle.Text = value.FormattedString();
				litTitle2.Text = value.FormattedString();
			}
		}

        public event EventHandler<ViewEventArgs<string>> AddAddressFromAddressBook;
        public event EventHandler<ViewEventArgs<string>> FindPostalCode;

		public void HighlightRatingMinimumRequiredFields()
		{
			txtPostalCode.CssClass = "inputHiglight";
			ddlCountries.CssClass = "inputHiglight";
		}

		public void SetRatingEditStatus(bool enabled)
		{
			imgAddressBookSearch.Enabled = enabled;
			imgPostalCodeSearch.Enabled = enabled;
			txtPostalCode.Enabled = enabled;
			ddlCountries.Enabled = enabled;
		}

        public void LoadPostalCode(PostalCodeViewSearchDto postalCode)
        {
            txtCity.Text = postalCode.City;
            txtState.Text = postalCode.State;
            txtPostalCode.Text = postalCode.Code;
            ddlCountries.SelectedValue =  postalCode.CountryId.GetString();
        }


		public void LoadAddress<T>(T location) where T : Location
		{
			if (location == null) return;

			LoadLocation(location);

			if (location as ShipmentLocation != null) LoadShipmentData(location as ShipmentLocation);
			if (location as LoadOrderLocation != null) LoadLoadOrderData(location as LoadOrderLocation);
            if (location as AddressBook != null) LoadAddressBookRecord(location as AddressBook);
		}

		public void RetrieveAddress<T>(T location) where T : Location
		{
			if (location == null) return;

			location.Street1 = txtStreet1.Text;
			location.Street2 = txtStreet2.Text;
			location.City = txtCity.Text;
			location.State = txtState.Text;
			location.PostalCode = txtPostalCode.Text;
			location.CountryId = ddlCountries.SelectedValue.ToInt();

			if (location as ShipmentLocation != null) RetrieveShipmentData(location as ShipmentLocation);
			if (location as LoadOrderLocation != null) RetrieveLoadOrderData(location as LoadOrderLocation);
		}

		public void ClearFields()
		{
			LoadLocation(new Location());

			txtDescription.Text = string.Empty;
			txtInstructions.Text = string.Empty;
		}

		public void SetCustomerIdSpecificFilter(long customerId)
		{
			SetContextKeys(customerId);
		}


		private void LoadLocation(Location location)
		{
			txtStreet1.Text = location.Street1;
			txtStreet2.Text = location.Street2;
			txtCity.Text = location.City;
			txtState.Text = location.State;
			txtPostalCode.Text = location.PostalCode;
			ddlCountries.SelectedValue = location.CountryId.GetString();
		}


		private void LoadShipmentData(ShipmentLocation location)
		{
			txtDescription.Text = location.Description;
			txtInstructions.Text = location.SpecialInstructions;
			txtDirection.Text = location.Direction;
			txtGeneralInfo.Text = location.GeneralInfo;
			txtMilesFromPreviousStop.Text = location.MilesFromPreceedingStop.ToString("f4");

			txtAppointmentDate.Text = location.AppointmentDateTime <= DateUtility.SystemEarliestDateTime
										? string.Empty
										: location.AppointmentDateTime.FormattedShortDate();

			txtTime.Text = location.AppointmentDateTime <= DateUtility.SystemEarliestDateTime ? string.Empty : location.AppointmentDateTime.GetTime();

			hidContactObjectTypeLoaded.Value = ShipmentContact;
		    peLstLocationContacts.LoadData(location.Contacts.Any()
		                                       ? location.Contacts.OrderByDescending(c => c.Primary).ToList()
		                                       : new List<ShipmentContact>
		                                           {
		                                               new ShipmentContact
		                                                   {
		                                                       ContactTypeId = ActiveUser.Tenant.DefaultSchedulingContactTypeId
		                                                   }
		                                           });
		}

		private void LoadLoadOrderData(LoadOrderLocation location)
		{
			txtDescription.Text = location.Description;
			txtInstructions.Text = location.SpecialInstructions;
			txtDirection.Text = location.Direction;
			txtGeneralInfo.Text = location.GeneralInfo;
			txtMilesFromPreviousStop.Text = location.MilesFromPreceedingStop.ToString("f4");

            txtAppointmentDate.Text = location.AppointmentDateTime <= DateUtility.SystemEarliestDateTime
									? string.Empty
									: location.AppointmentDateTime.FormattedShortDate();
			txtTime.Text = location.AppointmentDateTime <= DateUtility.SystemEarliestDateTime ? string.Empty : location.AppointmentDateTime.GetTime();

			hidContactObjectTypeLoaded.Value = LoadOrderContact;
		    peLstLocationContacts.LoadData(location.Contacts.Any()
                                               ? location.Contacts.OrderByDescending(c => c.Primary).ToList()
		                                       : new List<LoadOrderContact>
		                                           {
		                                               new LoadOrderContact
		                                                   {
		                                                       ContactTypeId = ActiveUser.Tenant.DefaultSchedulingContactTypeId
		                                                   }
		                                           });
		}


        private void UpdatePageStoreFromView()
        {
            switch (hidContactObjectTypeLoaded.Value)
            {
                case LoadOrderContact:
                    var loContacts = lstLocationContacts
                        .Items
                        .Select(control => new
                        {
                            EditIdx = control.FindControl("hidContactIndex").ToCustomHiddenField().Value.ToInt(),
                            Contact = new LoadOrderContact(control.FindControl("hidContactId").ToCustomHiddenField().Value.ToLong())
                            {
                                Name = control.FindControl("txtName").ToTextBox().Text,
                                Phone = control.FindControl("txtPhone").ToTextBox().Text,
                                Mobile = control.FindControl("txtMobile").ToTextBox().Text,
                                Fax = control.FindControl("txtFax").ToTextBox().Text,
                                Email = control.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
                                Comment = control.FindControl("txtComment").ToTextBox().Text,
                                ContactTypeId = control.FindControl("ddlContactContactType").ToCachedObjectDropDownList().SelectedValue.ToLong()
                            }
                        })
                        .ToList();
                    var loData = peLstLocationContacts.GetData<LoadOrderContact>();
                    foreach (var contact in loContacts)
                        loData[contact.EditIdx] = contact.Contact;
                    peLstLocationContacts.LoadData(loData, peLstLocationContacts.CurrentPage);
                    break;
                default:
                    var sContacts = lstLocationContacts
                        .Items
                        .Select(control => new
                        {
                            EditIdx = control.FindControl("hidContactIndex").ToCustomHiddenField().Value.ToInt(),
                            Contact = new ShipmentContact(control.FindControl("hidContactId").ToCustomHiddenField().Value.ToLong())
                            {
                                Name = control.FindControl("txtName").ToTextBox().Text,
                                Phone = control.FindControl("txtPhone").ToTextBox().Text,
                                Mobile = control.FindControl("txtMobile").ToTextBox().Text,
                                Fax = control.FindControl("txtFax").ToTextBox().Text,
                                Email = control.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
                                Comment = control.FindControl("txtComment").ToTextBox().Text,
                                ContactTypeId = control.FindControl("ddlContactContactType").ToCachedObjectDropDownList().SelectedValue.ToLong()
                            }
                        })
                        .ToList();
                    var sData = peLstLocationContacts.GetData<ShipmentContact>();
                    foreach (var contact in sContacts)
                        sData[contact.EditIdx] = contact.Contact;
                    peLstLocationContacts.LoadData(sData, peLstLocationContacts.CurrentPage);
                    break;
            }
        }

		private void RetrieveShipmentData(ShipmentLocation location)
		{
			location.Description = txtDescription.Text;
			location.SpecialInstructions = txtInstructions.Text;
			location.Direction = txtDirection.Text;
			location.GeneralInfo = txtGeneralInfo.Text;
			location.MilesFromPreceedingStop = txtMilesFromPreviousStop.Text.ToDecimal();

			location.AppointmentDateTime = string.IsNullOrEmpty(txtAppointmentDate.Text)
											? DateUtility.SystemEarliestDateTime
											: txtAppointmentDate.Text.ToDateTime();

			if (!string.IsNullOrEmpty(txtTime.Text) && txtTime.Text.IsValidFreeFormTimeString() && location.AppointmentDateTime != DateUtility.SystemEarliestDateTime)
				location.AppointmentDateTime = location.AppointmentDateTime.SetTime(txtTime.Text);

		    UpdatePageStoreFromView();

			var data = peLstLocationContacts.GetData<ShipmentContact>();
			var contacts = data
				.Select((t, i) => new ShipmentContact(t.Id, t.Id != default(long))
					{
						Name = t.Name,
						Phone = t.Phone,
						Mobile = t.Mobile,
						Fax = t.Fax,
						Email = t.Email,
						Comment = t.Comment,
						ContactTypeId = t.ContactTypeId,
						Primary = i == 0,
						Location = location,
						TenantId = location.TenantId
					})
				.ToList();

			location.Contacts = contacts;
		}

		private void RetrieveLoadOrderData(LoadOrderLocation location)
		{
			location.Description = txtDescription.Text;
			location.SpecialInstructions = txtInstructions.Text;
			location.Direction = txtDirection.Text;
			location.GeneralInfo = txtGeneralInfo.Text;
			location.MilesFromPreceedingStop = txtMilesFromPreviousStop.Text.ToDecimal();

			location.AppointmentDateTime = string.IsNullOrEmpty(txtAppointmentDate.Text)
										? DateUtility.SystemEarliestDateTime
										: txtAppointmentDate.Text.ToDateTime();
			if (!string.IsNullOrEmpty(txtTime.Text) && txtTime.Text.IsValidFreeFormTimeString() && location.AppointmentDateTime != DateUtility.SystemEarliestDateTime)
				location.AppointmentDateTime = location.AppointmentDateTime.SetTime(txtTime.Text);

            UpdatePageStoreFromView();

			var data = peLstLocationContacts.GetData<LoadOrderContact>();
			var contacts = data
				.Select((t, i) => new LoadOrderContact(t.Id, t.Id != default(long))
				{
					Name = t.Name,
					Phone = t.Phone,
					Mobile = t.Mobile,
					Fax = t.Fax,
					Email = t.Email,
					Comment = t.Comment,
					ContactTypeId = t.ContactTypeId,
					Primary = i == 0,
					Location = location,
					TenantId = location.TenantId
				})
				.ToList();

			location.Contacts = contacts;
		}


		private void SetContextKeys(long customerId)
		{
			var values = new Dictionary<string, string>
				{
					{AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString()},
					{AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()},
					{AutoCompleteUtility.Keys.Customer, customerId.GetString()},
				};
			aceOperationsAddressInputDescription.SetupExtender(values, 3);
		}


		protected bool ValueIsNotEmpty(object value)
		{
			return value != null && !string.IsNullOrEmpty(value.ToString());
		}




		protected void Page_Load(object sender, EventArgs e)
		{
			// account for missing option to search
            imgAddressBookSearch.Visible = AddAddressFromAddressBook != null;
            txtDescription.CssClass = !imgAddressBookSearch.Visible ? "w315" : "w280";

            imgPostalCodeSearch.Visible = FindPostalCode != null;
            txtPostalCode.CssClass = !imgPostalCodeSearch.Visible ? "w150" : "w110";

			if (IsPostBack) return;

			// context key
			SetContextKeys(default(long));

			if (ddlCountries.SelectedValue.ToLong() == 0)
				ddlCountries.SelectedValue = ActiveUser.Tenant.DefaultCountryId.GetString();
		}



        private void LoadAddressBookRecord(AddressBook address)
        {
            LoadLocation(address);
            txtDescription.Text = address.Description;
            txtInstructions.Text = Type == OperationsAddressInputType.Origin
                                       ? address.OriginSpecialInstruction
                                       : Type == OperationsAddressInputType.Destination
                                             ? address.DestinationSpecialInstruction
                                             : string.Empty;
            txtDirection.Text = address.Direction;
            txtGeneralInfo.Text = address.GeneralInfo;

            var contacts = address.Contacts
                                  .Select(c => new
                                      {
                                          Id = default(long),
                                          c.Name,
                                          c.Phone,
                                          c.Mobile,
                                          c.Fax,
                                          c.Email,
                                          c.Comment,
                                          ContactTypeId = c.ContactTypeId.ToString()
                                      })
                                  .ToList();
            lstLocationContacts.DataSource = contacts;
            lstLocationContacts.DataBind();

            switch (hidContactObjectTypeLoaded.Value)
            {
                case LoadOrderContact:
                    var loData = address
                        .Contacts
                        .Select(i => new LoadOrderContact
                            {
                                Name = i.Name,
                                Phone = i.Phone,
                                Mobile = i.Mobile,
                                Fax = i.Fax,
                                Email = i.Email,
                                Comment = i.Comment,
                                ContactTypeId = i.ContactTypeId,
                                Primary = i.Primary,
                            })
                        .ToList();
                    peLstLocationContacts.LoadData(loData);
                    break;
                default:
                    var sData = address
                        .Contacts
                        .Select(i => new ShipmentContact
                            {
                                Name = i.Name,
                                Phone = i.Phone,
                                Mobile = i.Mobile,
                                Fax = i.Fax,
                                Email = i.Email,
                                Comment = i.Comment,
                                ContactTypeId = i.ContactTypeId,
                                Primary = i.Primary,
                            })
                        .ToList();
                    peLstLocationContacts.LoadData(sData);

                    break;
            }
        }

		protected void OnAddFromAddressBookClicked(object sender, EventArgs e)
		{
            if (AddAddressFromAddressBook != null)
                AddAddressFromAddressBook(this, new ViewEventArgs<string>(txtDescription.Text));
		}

		
		protected void OnDescriptionTextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtDescription.Text)) return;

			var key = txtDescription.Text.Substring(1).RetrieveSearchCodeFromAutoCompleteString().ToLong();
			var address = new AddressBook(key);
			if (!address.KeyLoaded) return;
			LoadAddressBookRecord(address);
		}



		protected void OnAddContactClicked(object sender, EventArgs e)
		{
			switch (hidContactObjectTypeLoaded.Value)
			{
				case LoadOrderContact:
					var loContact = lstLocationContacts
						.Items
						.Select(control => new
							{
								EditIdx = control.FindControl("hidContactIndex").ToCustomHiddenField().Value.ToInt(),
								Contact = new LoadOrderContact(control.FindControl("hidContactId").ToCustomHiddenField().Value.ToLong())
									{
										Name = control.FindControl("txtName").ToTextBox().Text,
										Phone = control.FindControl("txtPhone").ToTextBox().Text,
										Mobile = control.FindControl("txtMobile").ToTextBox().Text,
										Fax = control.FindControl("txtFax").ToTextBox().Text,
										Email = control.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
										Comment = control.FindControl("txtComment").ToTextBox().Text,
										ContactTypeId = control.FindControl("ddlContactContactType").ToCachedObjectDropDownList().SelectedValue.ToLong()
									}
							})
						.ToList();
					var loData = peLstLocationContacts.GetData<LoadOrderContact>();
					foreach (var contact in loContact)
						loData[contact.EditIdx] = contact.Contact;

					loData.Add(new LoadOrderContact
						{
							Name = string.Empty,
							Phone = string.Empty,
							Mobile = string.Empty,
							Fax = string.Empty,
							Email = string.Empty,
							Comment = string.Empty,
							ContactTypeId = ActiveUser.Tenant.DefaultSchedulingContactTypeId,
						});

					peLstLocationContacts.LoadData(loData, loData.Count);
					break;
				default:
					var sContacts = lstLocationContacts
						.Items
						.Select(control => new
							{
								EditIdx = control.FindControl("hidContactIndex").ToCustomHiddenField().Value.ToInt(),
								Contact = new ShipmentContact(control.FindControl("hidContactId").ToCustomHiddenField().Value.ToLong())
									{
										Name = control.FindControl("txtName").ToTextBox().Text,
										Phone = control.FindControl("txtPhone").ToTextBox().Text,
										Mobile = control.FindControl("txtMobile").ToTextBox().Text,
										Fax = control.FindControl("txtFax").ToTextBox().Text,
										Email = control.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
										Comment = control.FindControl("txtComment").ToTextBox().Text,
										ContactTypeId = control.FindControl("ddlContactContactType").ToCachedObjectDropDownList().SelectedValue.ToLong()
									}
							})
						.ToList();
					var sData = peLstLocationContacts.GetData<ShipmentContact>();
					foreach (var contact in sContacts)
						sData[contact.EditIdx] = contact.Contact;

					sData.Add(new ShipmentContact
						{
							Name = string.Empty,
							Phone = string.Empty,
							Mobile = string.Empty,
							Fax = string.Empty,
							Email = string.Empty,
							Comment = string.Empty,
							ContactTypeId = ActiveUser.Tenant.DefaultSchedulingContactTypeId,
						});

					peLstLocationContacts.LoadData(sData, sData.Count);
					break;

			}
		}

		protected void OnDeleteContactClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var removeIndex = imageButton.Parent.FindControl("hidContactIndex").ToCustomHiddenField().Value.ToInt();
			switch (hidContactObjectTypeLoaded.Value)
			{
				case LoadOrderContact:
					var loData = peLstLocationContacts.GetData<LoadOrderContact>();
					loData.RemoveAt(removeIndex);
					peLstLocationContacts.LoadData(loData, peLstLocationContacts.CurrentPage - 1);
					break;
				default:
					var sData = peLstLocationContacts.GetData<ShipmentContact>();
					sData.RemoveAt(removeIndex);
					peLstLocationContacts.LoadData(sData, peLstLocationContacts.CurrentPage - 1);
					break;
			}
		}



		protected void OnPostalCodeSearchClicked(object sender, ImageClickEventArgs e)
		{
            if (FindPostalCode != null)
                FindPostalCode(this, new ViewEventArgs<string>(txtPostalCode.Text));
		}

		protected void OnLocationContactsPageChanging(object sender, EventArgs e)
		{
		    UpdatePageStoreFromView();	
		}
	}
}