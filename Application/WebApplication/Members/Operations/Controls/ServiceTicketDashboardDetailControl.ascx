﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServiceTicketDashboardDetailControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.ServiceTicketDashboardDetailControl" %>
<eShip:CustomHiddenField ID="hidItemIndex" runat="server" />
<eShip:CustomHiddenField ID="hidServiceTicketId" runat="server" />
<tr>
    <td>
        <asp:Literal runat="server" ID="litServiceTicketNumber" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litDateCreated" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litTicketDate" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litCustomerNameNumber" />
        <asp:HyperLink ID="hypCustomer" Target="_blank" runat="server" ToolTip="Go To Customer Record" Visible="False">
            <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle" width="16"/>
        </asp:HyperLink>
    </td>
    <td>
        <asp:Literal runat="server" ID="litUser" />
        <asp:HyperLink ID="hypUser" Target="_blank" runat="server" ToolTip="Go To User Record" Visible="False">
            <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle" width="16"/>
        </asp:HyperLink>
    </td>
    <td>
        <asp:Literal runat="server" ID="litStatus" />
    </td>
    <td class="text-center">
        <asp:HyperLink runat="server" ID="hypServiceTicketView" Target="_blank">
            <asp:Image runat="server" ID="imgGoToServiceTicketView" ImageUrl="~/images/icons2/arrow_newTab.png" AlternateText="Go to Service Ticket Record" />
        </asp:HyperLink>
    </td>
</tr>
