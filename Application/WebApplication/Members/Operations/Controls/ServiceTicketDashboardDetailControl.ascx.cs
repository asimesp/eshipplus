﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class ServiceTicketDashboardDetailControl : MemberControlBase
    {
        public IOpsLocationCollectionResource ResourcePage { get; set; }

        public int ItemIndex
        {
            get { return hidItemIndex.Value.ToInt(); }
            set { hidItemIndex.Value = value.ToString(); }
        }

        public bool EnableOpen
        {
            set { hypServiceTicketView.Visible = value; }
        }

        public List<ViewListItem> ServiceCollection { get { return ResourcePage.ServiceCollection; } }

        public List<ViewListItem> EquipmentTypeCollection { get { return ResourcePage.EquipmentTypeCollection; } }


        public void LoadServiceTicket(ServiceTicketDashboardDto serviceTicket)
        {
            if (serviceTicket == null) return;

            hidServiceTicketId.Value = serviceTicket.Id.ToString();

            //details
            litServiceTicketNumber.Text = serviceTicket.ServiceTicketNumber;
            litDateCreated.Text = serviceTicket.DateCreated.FormattedShortDate();
            litTicketDate.Text = serviceTicket.TicketDate.FormattedShortDate();
            litStatus.Text = serviceTicket.Status.ToString();

            var hasAccessToUser = ActiveUser.HasAccessTo(ViewCode.User);
            litUser.Text = serviceTicket.Username;
            hypUser.Visible = hasAccessToUser;
            hypUser.NavigateUrl = string.Format("{0}?{1}={2}", UserView.PageAddress, WebApplicationConstants.TransferNumber, serviceTicket.UserId.GetString().UrlTextEncrypt());


            var hasAccessToCustomer = ActiveUser.HasAccessTo(ViewCode.Customer);
            litCustomerNameNumber.Text = string.Format("{0} - {1}", serviceTicket.CustomerNumber, serviceTicket.CustomerName);
            hypCustomer.Visible = hasAccessToCustomer;
            hypCustomer.NavigateUrl = string.Format("{0}?{1}={2}", CustomerView.PageAddress, WebApplicationConstants.TransferNumber, serviceTicket.CustomerId.GetString().UrlTextEncrypt());

            hypServiceTicketView.NavigateUrl = string.Format("{0}?{1}={2}", ServiceTicketView.PageAddress, WebApplicationConstants.TransferNumber, serviceTicket.ServiceTicketNumber);
        }
    }
}