﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuoteRequestFormControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.QuoteRequestFormControl" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>

<asp:Panel runat="server" ID="pnlQuoteRequestForm">
    <div class="clearfix">
        <div class="rowgroup">
            <div class="fieldgroup mr20">
                <label class="wlabel blue">Customer</label>
            </div>
            <div class="fieldgroup mr20">
                <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w100" OnTextChanged="OnCustomerNumberEntered"
                    AutoPostBack="True" />
                <asp:RequiredFieldValidator runat="server" ID="rfvCustomer" ControlToValidate="txtCustomerNumber"
                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                    CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w270 disabled" ReadOnly="True" />
                <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                    EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
            </div>
            <div class="fieldgroup mr20">
                <label class="wlabel blue">Shipment Date</label>
            </div>
            <div class="fieldgroup mr20">
                <eShip:CustomTextBox runat="server" ID="txtShipmentDate" CausesValidation="False"  CssClass="w100" Type="Date" placeholder="99/99/9999" />
            </div>
            <asp:Panel runat="server" ID="pnlDesiredDeliveryDate">
                <div class="fieldgroup mr20">
                    <label class="wlabel blue">Desired Delivery Date</label>
                </div>
                <div class="fieldgroup mr20">
                    <eShip:CustomTextBox runat="server" ID="txtDeliveryDateTruckload" CausesValidation="False" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                </div>
            </asp:Panel>
        </div>
        <hr class="dark">
        <div class="rowgroup">
            <div class="col_1_2 bbox bbox vlinedarkright">
                <h5>Origin</h5>
                <div class="fieldgroup">
                    <label class="wlabel">Country</label>
                    <eShip:CachedObjectDropDownList Type="Countries" ID="ddlOriginCountry" runat="server" CssClass="w200" EnableChooseOne="True" DefaultValue="0"/>
                </div>
                <div class="fieldgroup">
                    <label class="wlabel">Postal Code</label>
                    <eShip:CustomTextBox ID="txtOriginPostalCode" runat="server" MaxLength="50" CssClass="w100" />
                    <asp:ImageButton ID="imgPostalCodeSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                        CausesValidation="False" OnClick="OnOriginPostalCodeSearchClicked" />
                </div>
                <div class="fieldgroup">
                    <label class="wlabel">Business Type</label>
                    <asp:DropDownList runat="server" ID="ddlOriginalBusinessType" CssClass="w150">
                        <Items>
                            <asp:ListItem Text="Business" Value="Business" />
                            <asp:ListItem Text="Residential" Value="Residential" />
                        </Items>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col_1_2 bbox pl40">
                <h5>Destination</h5>
                <div class="fieldgroup">
                    <label class="wlabel">Country</label>
                    <eShip:CachedObjectDropDownList Type="Countries" ID="ddlDestinationCountry" runat="server" CssClass="w200" EnableChooseOne="True" DefaultValue="0"/>
                </div>
                <div class="fieldgroup">
                    <label class="wlabel">Postal Code</label>
                    <eShip:CustomTextBox ID="txtDesPostalCode" runat="server" MaxLength="50" CssClass="w100" />
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                        CausesValidation="False" OnClick="OnDestinationPostalCodeSearchClicked" />
                </div>


                <div class="fieldgroup">
                    <label class="wlabel">Business Type</label>
                    <asp:DropDownList runat="server" ID="ddlDestinationBusinessType" CssClass="w150">
                        <Items>
                            <asp:ListItem Text="Business" Value="Business" />
                            <asp:ListItem Text="Residential" Value="Residential" />
                        </Items>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <hr />
        <asp:Panel runat="server" ID="pnlLTLOrVolume">
            <h5>Special Services</h5>
            <div class="hlist mt0">
                <asp:Repeater runat="server" ID="rptServices">
                    <HeaderTemplate>
                        <ul class="sm_chk chk_list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:CheckBox runat="server" ID="chkService" Text='<%# Eval("Text") %>' CssClass="jQueryUniform" />
                            <eShip:CustomHiddenField runat="server" ID="hidServiceId" Value='<%# Eval("Value") %>' />
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlTruckload">
            <h5>Equipment</h5>
            <script type="text/javascript">
                $(document).ready(function () {
                    // enforce mutual exclusion on checkboxes in quoteRequestFormEquipment
                    $(jsHelper.AddHashTag('quoteRequestFormEquipment input:checkbox')).click(function () {
                        if (jsHelper.IsChecked($(this).attr('id'))) {
                            $(jsHelper.AddHashTag('quoteRequestFormEquipment input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                jsHelper.UnCheckBox($(this).attr('id'));
                            });
                            $.uniform.update();
                        }
                    });
                });
            </script>
            <div class="hlist mt0" id="quoteRequestFormEquipment">
                <asp:Repeater runat="server" ID="rptEquipment">
                    <HeaderTemplate>
                        <ul class="sm_chk chk_list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:CheckBox runat="server" ID="chkEquipment" Text='<%# Eval("Text") %>' CssClass="jQueryUniform" />
                            <eShip:CustomHiddenField runat="server" ID="hidEquipmentId" Value='<%# Eval("Value") %>' />
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
        <hr class="fat">
        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h5>Items</h5>
                <div class="row mr5">
                    <div class="fieldgroup mr5">
                        <asp:LinkButton runat="server" ID="lnkAddItem" OnClick="OnAddAdditionalItemClick" CausesValidation="false" CssClass="add">
                       Add Item
                        </asp:LinkButton>
                    </div>
                    <div class="fieldgroup mr5">
                        <asp:LinkButton runat="server" ID="lnkAddItemFromLibrary" OnClick="OnAddLibraryItemClicked" CausesValidation="false" CssClass="add">
                                Add Item From Library
                        </asp:LinkButton>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        if (jsHelper.IsTrue('<%= (QuoteRequestServiceMode != ServiceMode.Truckload).ToString() %>')) {
                            $(jsHelper.AddHashTag("itemsTable [id$=tdFreightClassHeader]")).show();
                            $(jsHelper.AddHashTag("itemsTable [id$=tdFreightClass]")).show();
                        }
                        else {
                            $(jsHelper.AddHashTag("itemsTable [id$=tdFreightClassHeader]")).hide();
                            $(jsHelper.AddHashTag("itemsTable [id$=tdFreightClass]")).hide();
                        }
                    });

                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                        if (jsHelper.IsTrue('<%= (QuoteRequestServiceMode != ServiceMode.Truckload).ToString() %>')) {
                            $(jsHelper.AddHashTag("itemsTable [id$=tdFreightClassHeader]")).show();
                            $(jsHelper.AddHashTag("itemsTable [id$=tdFreightClass]")).show();
                        }
                        else {
                            $(jsHelper.AddHashTag("itemsTable [id$=tdFreightClassHeader]")).hide();
                            $(jsHelper.AddHashTag("itemsTable [id$=tdFreightClass]")).hide();
                        }
                    });
                </script>
                <div class="row mt10">
                    <table class="stripe" id="itemsTable">
                        <tr class="header">
                            <th style="width: 10%;">Total Weight
                                <abbr title="Pounds">(lb)</abbr>
                            </th>
                            <th style="width: 10%;">Length
                                <abbr title="Inches">(in)</abbr>
                            </th>
                            <th style="width: 10%;">Width
                                <abbr title="Inches">(in)</abbr>
                            </th>
                            <th style="width: 10%;">Height
                                <abbr title="Inches">(in)</abbr>
                            </th>
                            <th style="width: 10%;">
                                <abbr title="Package Quantity">Package Qty.</abbr>
                            </th>
                            <th style="width: 10%;">Package Type
                            </th>
                            <th style="width: 10%" id="tdFreightClassHeader">Freight Class
                            </th>
                            <th style="width: 30%">Description
                            </th>
                            <th style="width: 10%;" class="text-center">Action
                            </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstQuoteItems" OnItemDataBound="OnItemsItemDataBound" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <eShip:CustomHiddenField runat="server" ID="hidItemIndex" Value='<%# Container.DataItemIndex %>' />
                                        <eShip:CustomTextBox runat="server" ID="txtTotalWeight" CssClass="w80" Text='<%# Eval("Weight") %>' MaxLength="50" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtLength" CssClass="w80" Text='<%# Eval("Length") %>' MaxLength="50" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtWidth" CssClass="w80" Text='<%# Eval("Width") %>' MaxLength="50" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtHeight" CssClass="w80" Text='<%# Eval("Height") %>' MaxLength="50" Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtQuantity" CssClass="w80" Text='<%# Eval("Quantity") %>' MaxLength="50" Type="NumbersOnly" />
                                    </td>
                                    <td>
                                        <eShip:CachedObjectDropDownList Type="PackageType" ID="ddlPackageType" runat="server" CssClass="w130" EnableChooseOne="True"
                                            DefaultValue="0" SelectedValue='<%# Eval("PackageTypeId") %>' />
                                    </td>
                                    <td id="tdFreightClass">
                                        <asp:DropDownList ID="ddlFreightClass" runat="server" CssClass="w60" DataTextField="Text"
                                            DataValueField="Value" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtDescription" CssClass='<%# QuoteRequestServiceMode == ServiceMode.Truckload ? "w360" : "w270" %>' Text='<%# Eval("Description") %>' MaxLength="100" />
                                    </td>
                                    <td class="text-center">
                                        <asp:ImageButton ID="ibtnItemDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            OnClick="OnDeleteItemClicked" CausesValidation="false" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Please Add Additional Shipping Instructions
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleSpecialInstructions" TargetControlId="txtSpecialInstructions" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtSpecialInstructions" CssClass="w740" runat="server" TextMode="MultiLine" Rows="5" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lnkAddItemFromLibrary" />
            </Triggers>
        </asp:UpdatePanel>
        <hr />
        <div class="rowgroup">
            <div class='<%= QuoteRequestServiceMode == ServiceMode.Truckload ? "col_1_2 bbox vlinedarkright" : "col_1_3 mh180"%>'>
                <h5>References</h5>
                <table class="stripe">
                    <tr>
                        <th style="width: 45%;">Name
                        </th>
                        <th style="width: 55%;">Value
                        </th>
                    </tr>
                    <tr>
                        <td>PO Number
                            <asp:Literal runat="server" ID="litPONumberRequired" Text='<span class="red">* </span>'
                                Visible="False" />
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtPONumber" CssClass="w150" />
                        </td>
                    </tr>
                    <tr>
                        <td>Shipper Reference #
                            <asp:Literal runat="server" ID="litShipperReferenceNumberRequired" Text='<span class="red">* </span>'
                                Visible="False" />
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtShipperReference" CssClass="w150" />
                        </td>
                    </tr>
                    <asp:ListView ID="lstReferences" runat="server" ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <eShip:CustomHiddenField runat="server" ID="hidDisplayOnOrigin" Value='<%# Eval("DisplayOnOrigin") %>' />
                                    <eShip:CustomHiddenField runat="server" ID="hidDisplayOnDestination" Value='<%# Eval("DisplayOnDestination") %>' />
                                    <asp:Literal ID="litName" runat="server" Text='<%# Eval("Name") %>' />
                                    <asp:Literal runat="server" ID="litRequired" Text='<span style="color: red;">*</span>' Visible='<%# Eval("Required") %>' />
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtValue" CssClass="w150" Text='<%# Eval("Value") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </div>
             <div class='<%= QuoteRequestServiceMode == ServiceMode.Truckload ? "" : "col_1_3 mh180"%>'>
                <asp:UpdatePanel ID="pnlHazardousMaterial" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="rowgroup ">
                            <h5>Haz Mat Information</h5>
                            <div class="row">
                                <div class="fieldgroup">
                                    <eShip:AltUniformCheckBox runat="server" ID="chkHazMat" AutoPostBack="True" OnCheckedChanged="OnHazMatCheckChanged" />
                                    <label class="upper">Shipping Hazardous Materials</label>
                                </div>
                            </div>
                            <asp:Panel runat="server" ID="pnlHazMat" Visible="False">
                                <div class="row mt20">
                                    <div class="fieldgroup mr10">
                                        <label class="wlabel">Contact Name <span class="red">*</span></label>
                                        <eShip:CustomTextBox runat="server" ID="txtHazMatName" CssClass="w150" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Phone <span class="red">*</span></label>
                                        <eShip:CustomTextBox runat="server" ID="txtHazMatPhone" CssClass="w150" MaxLength="50" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr10">
                                        <label class="wlabel">Mobile</label>
                                        <eShip:CustomTextBox runat="server" ID="txtHazMatMobile" CssClass="w150" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Email</label>
                                        <eShip:CustomTextBox runat="server" ID="txtHazMatEmail" CssClass="w150" MaxLength="100" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
              <div class='<%= QuoteRequestServiceMode == ServiceMode.Truckload ? "col_1_2 bbox pl40 text-center" : "col_1_3 mh180"%>'>
                <div class="row mt20">
                    <div class="fieldgroup text-center">
                        <asp:Button runat="server" ID="btnSubmitQuoteRequest" Visible="False" CssClass="w200" Text="Submit Quote Request" OnClick="OnSubmitQuoteRequestClicked" />
                    </div>
                </div>
            </div>
        </div>
        <hr />
    </div>
</asp:Panel>
<eShip:CustomHiddenField runat="server" ID="hidServiceMode" />
<eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
<eShip:CustomHiddenField runat="server" ID="hidHasLoadedDynamicLists" />
<eShip:CustomHiddenField runat="server" ID="hidPostalCodeSet" />
