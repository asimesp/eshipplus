﻿using System;
using LogisticsPlus.Eship.WebApplication.Members.Administration;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class RateNoticeControl : MemberControlBase
    {
        protected void OnDisableNoticeClicked(object sender, EventArgs e)
        {
            Response.Redirect(UserProfileView.PageAddress);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            litPhone.Text = string.Format("Please call {0} at {1} if:",ActiveUser.Tenant.Name,  ActiveUser.DefaultCustomer.Tier.TollFreeContactNumber);
            lblRatingNotice.Text = ActiveUser.Tenant.RatingNotice;
        }
    }
}