﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class ShipmentFinderControl : MemberControlBase, IShipmentFinderView
    {
        private SearchField SortByField
        {
            get { return OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == hidSortField.Value); }
        }


        public int ItemIndex
        {
            get { return hidLocationItemIndex.Value.ToInt(); }
            set { hidLocationItemIndex.Value = value.ToString(); }
        }

        public new bool Visible
        {
            get { return pnlShipmentFinderContent.Visible; }
            set
            {
                base.Visible = value;
                pnlShipmentFinderContent.Visible = value;
                pnlShipmentFinderDimScreen.Visible = value;
            }
        }

        public bool OpenForEdit
        {
            get { return hidEditSelected.Value.ToBoolean(); }
        }

        public bool OpenForEditEnabled
        {
            get { return hidOpenForEditEnabled.Value.ToBoolean(); }
            set { hidOpenForEditEnabled.Value = value.ToString(); }
        }

        public bool EnableMultiSelection
        {
            get { return hidShipmentFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidShipmentFinderEnableMultiSelection.Value = value.ToString();
                btnProcessSelected.Visible = value;
                btnProcessSelected2.Visible = value;
                chkSelectAllRecords.Visible = value;
                upcseDataUpdate.PreserveRecordSelection = value;
            }
        }

        public long CustomerId
        {
            set { hidCustomerId.Value = value.ToString(); }
        }

        public bool FilterForShipmentsToBeInvoicedOnly
        {
            get { return hidShipmentsToBeInvoicedOnly.Value.ToBoolean(); }
            set { hidShipmentsToBeInvoicedOnly.Value = value.ToString(); }
        }

        public bool FilterForShipmentsToExludeAttachedToJob
        {
            get { return hidShipmentsToExludeAttachedToJob.Value.ToBoolean(); }
            set { hidShipmentsToExludeAttachedToJob.Value = value.ToString(); }
        }

        public event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<Shipment>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<Shipment>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<ShipmentDashboardDto> shipments)
        {
            var id = hidCustomerId.Value.ToLong();
            var dtos = id != default(long)
                           ? shipments
                                 .Where(s =>
                                 {
                                     var r = s.ResellerAdditionId == default(long)
                                                 ? new ResellerAddition()
                                                 : new ResellerAddition(s.ResellerAdditionId);
                                     return s.CustomerId == id ||
                                            (s.BillReseller && r.KeyLoaded && r.ResellerCustomerAccountId == id);
                                 })
                                 .ToList()
                           : shipments;

            litRecordCount.Text = dtos.BuildRecordCount();
            upcseDataUpdate.DataSource = dtos;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString();
            DisplaySearchResult(new List<ShipmentDashboardDto>());
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = hidSortAscending.Value.ToBoolean(),
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            var sortAscending = hidSortAscending.Value.ToBoolean();

            litSortOrder.SetSortDisplay(SortByField, sortAscending);

            DoSearch(new ShipmentViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = sortAscending
            });
        }

        private void DoSearch(ShipmentViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<ShipmentViewSearchCriteria>(criteria));

            chkSelectAllRecords.Checked = false;
        }




        private void ProcessItemSelection(long shipmentId)
        {
            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<Shipment>(new Shipment(shipmentId, false)));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ShipmentFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;

            // set sort fields
            lbtnSortShipmentNumber.CommandName = OperationsSearchFields.ShipmentNumber.Name;
            lbtnSortDesiredPickup.CommandName = OperationsSearchFields.DesiredPickupDate.Name;
            lbtnSortActualPickup.CommandName = OperationsSearchFields.ActualPickupDate.Name;
            lbtnSortEstimatedDelivery.CommandName = OperationsSearchFields.EstimatedDeliveryDate.Name;
            lbtnSortCustomerName.CommandName = OperationsSearchFields.CustomerName.Name;
            lbtnSortCustomerNumber.CommandName = OperationsSearchFields.CustomerNumber.Name;
            lbtnSortStatus.CommandName = OperationsSearchFields.Status.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : OperationsSearchFields.Default.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            hidSortAscending.Value = (profile != null && profile.SortAscending).GetString();

            if (profile != null && profile.SortBy != null)
                hidSortField.Value = profile.SortBy.Name;

            litSortOrder.SetSortDisplay(SortByField, hidSortAscending.Value.ToBoolean());
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = false.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidShipmentId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnProcessSelectedClicked(object sender, EventArgs e)
        {
            var shipments = (from item in lstShipments.Items
                             let hidden = item.FindControl("hidShipmentId").ToCustomHiddenField()
                             let checkBox = item.FindControl("chkSelected").ToAltUniformCheckBox()
                             where checkBox != null && checkBox.Checked
                             select new Shipment(hidden.Value.ToLong()))
                .Where(u => u.KeyLoaded)
                .ToList();

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<Shipment>>(shipments));
        }

        protected void OnEditSelectClicked(Object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = true.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidShipmentId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }



        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.Shipments.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.Shipments);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            hidSortAscending.Value = e.Argument.SortAscending.GetString();
            hidSortField.Value = e.Argument.SortBy == null ? string.Empty : e.Argument.SortBy.Name;

            litSortOrder.SetSortDisplay(SortByField, hidSortAscending.Value.ToBoolean());
        }



        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;

            if (hidSortField.Value == field.Name && hidSortAscending.Value.ToBoolean())
            {
                hidSortAscending.Value = false.GetString();
            }
            else if (hidSortField.Value == field.Name && !hidSortAscending.Value.ToBoolean())
            {
                hidSortAscending.Value = true.GetString();
            }

            hidSortField.Value = field.Name;

            litSortOrder.SetSortDisplay(field, hidSortAscending.Value.ToBoolean());

            DoSearchPreProcessingThenSearch(field);
        }
    }
}