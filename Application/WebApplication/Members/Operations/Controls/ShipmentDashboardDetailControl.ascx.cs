﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class ShipmentDashboardDetailControl : MemberControlBase
    {
        public bool EnableOpen { set { hypShipmentView.Visible = value; } }

        public bool EnabledMultiSelect { get; set; }

        public int ItemIndex
        {
            get { return hidItemIndex.Value.ToInt(); }
            set { hidItemIndex.Value = value.ToString(); }
        }

        public bool OpenForAudit
        {
            get { return !string.IsNullOrEmpty(hidGoToAudit.Value) && hidGoToAudit.Value.ToBoolean(); }
            set { hidGoToAudit.Value = value.ToString(); }
        }

        public bool OpenForShipmentModificaton
        {
            get { return hidOpenForShipmentModification.Value.ToBoolean(); }
            set { hidOpenForShipmentModification.Value = value.ToString(); }
        }

        public event EventHandler<ViewEventArgs<DetailRecordInfo>> GenerateShippingLabel;
        public event EventHandler<ViewEventArgs<long, DashboardDtoType>> ModifyShipment;


        public void LoadShipment(ShipmentDashboardDto shipment)
        {
            LoadShipment(shipment, new List<CheckCall>());
        }

        public void LoadShipment(ShipmentDashboardDto shipment, List<CheckCall> checkCalls)
        {
            if (shipment == null) return;

            hypShipmentView.Visible = !OpenForAudit;
            hypShipmentAudit.Visible = OpenForAudit;

            hidRecordType.Value = shipment.Type.GetString();
            if (shipment.Type == DashboardDtoType.Shipment)
            {
                hypShipmentAudit.NavigateUrl = string.Format("{0}?{1}={2}", ShipmentAuditView.PageAddress, WebApplicationConstants.ShipmentAuditId, shipment.Id.GetString().UrlTextEncrypt());
                hypShipmentView.NavigateUrl = string.Format("{0}?{1}={2}", ShipmentView.PageAddress, WebApplicationConstants.TransferNumber, shipment.ShipmentNumber);
                hypBillOfLading.NavigateUrl = DocumentViewer.GenerateShipmentBolLink(shipment);
                ibtnShippingLabel.Visible = true;
                imgGoToShipmentView.ToolTip = "Open Shipment Record";
            }
            else
            {
                if (shipment.Type == DashboardDtoType.LoadOrder)
                {
                    hypShipmentView.NavigateUrl = string.Format("{0}?{1}={2}", LoadOrderView.PageAddress, WebApplicationConstants.TransferNumber, shipment.ShipmentNumber);
                    imgGoToShipmentView.ToolTip = "Open Load Order Record";
                }

                ibtnShippingLabel.Visible = false;
                hypBillOfLading.Visible = false;
            }


            if (!string.IsNullOrEmpty(shipment.PriorityColor))
            {
                txtPriorityColor.Attributes["style"] = string.Format("background-color:#{0} !important; color: white;", shipment.PriorityColor);
                txtPriorityColor.Text = shipment.PriorityCode;
            }
            else
                txtPriorityColor.Text = string.Empty;


            ibtnModifyShipment.Visible = OpenForShipmentModificaton && ((shipment.Type== DashboardDtoType.LoadOrder && ActiveUser.HasAccessToModify(ViewCode.LoadOrder)) || (shipment.Type == DashboardDtoType.Shipment && ActiveUser.HasAccessToModify(ViewCode.Shipment)));

            hidShipmentId.Value = shipment.Id.ToString();
            litServiceMode.Text = shipment.ServiceMode.FormattedString();
            litShipmentNumber.Text = shipment.ShipmentNumber;
            litBookedDate.Text = shipment.DateCreated.FormattedShortDate();
            litDesiredPickupDate.Text = shipment.DesiredPickupDate == DateUtility.SystemEarliestDateTime
                                            ? string.Empty
                                            : shipment.DesiredPickupDate.FormattedShortDate();
            litPickupDate.Text = shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime
                                    ? string.Empty
                                    : shipment.ActualPickupDate.FormattedShortDate();
            litEstDeliveryDate.Text = shipment.EstimatedDeliveryDate == DateUtility.SystemEarliestDateTime
                                          ? string.Empty
                                          : shipment.EstimatedDeliveryDate.FormattedShortDate();
            litActualDeliveryDate.Text = shipment.ActualDeliveryDate == DateUtility.SystemEarliestDateTime
                                            ? string.Empty
                                            : shipment.ActualDeliveryDate.FormattedShortDate();
            litOriginLocation.Text = string.Format("{0}{1}{2} {3} {4} {5}", shipment.OriginName, WebApplicationConstants.HtmlBreak,
                                                   shipment.OriginCity, shipment.OriginState, shipment.OriginPostalCode, shipment.OriginCountryName);
            litDestinationLocation.Text = string.Format("{0}{1}{2} {3} {4} {5}", shipment.DestinationName, WebApplicationConstants.HtmlBreak,
                                                   shipment.DestinationCity,
                                                   shipment.DestinationState, shipment.DestinationPostalCode, shipment.DestinationCountryName);
            litCustomer.Text = string.Format("{0} - {1}", shipment.CustomerNumber, shipment.CustomerName);
            hypCustomer.NavigateUrl = string.Format("{0}?{1}={2}", CustomerView.PageAddress, WebApplicationConstants.TransferNumber, shipment.CustomerId.GetString().UrlTextEncrypt());

            litPrimaryVendor.Text = shipment.VendorId != default(long)
                                        ? string.Format("{0} - {1}", shipment.VendorNumber, shipment.VendorName)
                                        : "&nbsp;";
            hypVendor.NavigateUrl = string.Format("{0}?{1}={2}", VendorView.PageAddress, WebApplicationConstants.TransferNumber, shipment.VendorId.GetString().UrlTextEncrypt());
            hypVendor.Visible = ActiveUser.HasAccessTo(ViewCode.Vendor) && shipment.VendorId != default(long);

            litPrimaryVendorPro.Text = shipment.VendorProNumber != string.Empty ? shipment.VendorProNumber : string.Empty;
            hypPrimaryVendorPro.NavigateUrl = shipment.VendorTrackingUrl.Replace(WebApplicationConstants.VendorProPlaceHolder, shipment.VendorProNumber);
            hypPrimaryVendorPro.Visible = !string.IsNullOrEmpty(shipment.VendorProNumber) &&
                                          !string.IsNullOrEmpty(shipment.VendorTrackingUrl);

            litPrefix.Text = string.Format("{0} - {1}", shipment.Prefix, shipment.PrefixDescription);
            litStatus.Text = string.Format("{0}{1}", shipment.StatusText, checkCalls.Any() ? "<sup><b>+</b></sup>" : string.Empty);

            litCost.Text = shipment.TotalCost.ToString("c2");
            litAmountDue.Text = shipment.TotalAmountDue.ToString("c2");

            if (shipment.ActualPickupDate > DateUtility.SystemEarliestDateTime) litPickupDate.Text = shipment.ActualPickupDate.FormattedShortDate();
            if (shipment.EstimatedDeliveryDate > DateUtility.SystemEarliestDateTime) litEstDeliveryDate.Text = shipment.EstimatedDeliveryDate.FormattedShortDate();
            if (shipment.ActualDeliveryDate > DateUtility.SystemEarliestDateTime) litActualDeliveryDate.Text = shipment.ActualDeliveryDate.FormattedShortDate();

            litAlerts.Text += !shipment.OnTimePickup
                                ? " <span title='Late Pickup' class='mr5 flag pl5 pr5'>LP</span> "
                                : string.Empty;

            var validServiceMode = shipment.ServiceMode == ServiceMode.Air || shipment.ServiceMode == ServiceMode.Rail || shipment.ServiceMode == ServiceMode.Truckload;

            litAlerts.Text += shipment.Type == DashboardDtoType.Shipment && shipment.ShipmentStatus == ShipmentStatus.InTransit && shipment.CheckCallAlert && validServiceMode
                                ? " <span title='Check Call' class='mr5 flag pl5 pr5'>CC</span> "
                                : string.Empty;

            litAlerts.Text += shipment.VendorInsuranceAlert
                                ? " <span title='Vendor Insurance' class='mr5 flag pl5 pr5'>VI</span> "
                                : string.Empty;

            litAlerts.Text += !shipment.OnTimeDelivery
                                ? " <span title='Late Delivery' class='mr5 flag pl5 pr5'>LD</span> "
                                : string.Empty;
            litAlerts.Text += shipment.IsGuaranteedDeliveryService
                                ? string.Format(" <span title='Guaranteed Delivery Service by {0}' class='mr5 flag pl5 pr5'>GS</span> ", shipment.GuaranteedDeliveryServiceTime.ConvertAmPm())
                                : string.Empty;

            if (!checkCalls.Any()) return;

            divShipmentStatus.Attributes.Add("onmouseover", string.Format("javascript:ShowCheckCalls('checkCallDiv{0}', 'divCheckCallDimBackground{0}');", ItemIndex));

            lstCheckCalls.DataSource = checkCalls.OrderByDescending(c => c.CallDate)
                .Select(c => new
                {
                    CallDate = c.CallDate.FormattedSuppressMidnightDate(),
                    EventDate = c.EventDate == DateUtility.SystemEarliestDateTime ? string.Empty : c.EventDate.FormattedSuppressMidnightDate(),
                    c.EdiStatusCode,
                    c.CallNotes,
                })
                .ToList();
            lstCheckCalls.DataBind();
        }


        protected void OnLocationPathClicked(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
            Response.Export(Server.ReadFromFile(path), button.Text);
        }

        protected void OnShippingLabelsClicked(object sender, ImageClickEventArgs e)
        {
            var info = new DetailRecordInfo(hidShipmentId.Value.ToLong(), hidRecordType.Value.ToEnum<DashboardDtoType>());
            if (GenerateShippingLabel != null)
                GenerateShippingLabel(this, new ViewEventArgs<DetailRecordInfo>(info));
        }


        protected void OnModifyShipmentClicked(object sender, ImageClickEventArgs e)
        {
            if(ModifyShipment != null)
                ModifyShipment(this, new ViewEventArgs<long, DashboardDtoType>(hidShipmentId.Value.ToLong(), hidRecordType.Value.ToEnum<DashboardDtoType>()));
        }
    }
}