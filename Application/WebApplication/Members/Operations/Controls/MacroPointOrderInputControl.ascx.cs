﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.MacroPoint;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class MacroPointOrderInputControl : MemberControlBase
    {
        public new bool Visible
        {
            get { return pnlMacroPointOrderInput.Visible; }
            set
            {
                base.Visible = value;
                pnlMacroPointOrderInput.Visible = value;
                pnlDimScreen.Visible = value;
            }
        }

        public event EventHandler SelectionCancel;
        public event EventHandler<ViewEventArgs<Order>> SubmitOrder;


        public void Reset()
        {
            ddlNumberType.SelectedValue = NumberType.Mobile.GetString();

            txtTrackStartDate.Attributes.Add("eShipMinDate", DateTime.Now.Date.FormattedShortDate());
            txtTrackStartDate.Text = DateTime.Now.ToShortDateString();

            ddlTime.DataSource = TimeUtility.
                BuildTimeList(new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second))
                .Select(i => new ViewListItem(i, i));
            ddlTime.DataBind();

            txtEmailAddresses.Text = string.Empty;
            txtNotes.Text = string.Empty;
            txtNumber.Text = string.Empty;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            // set up
            ddlNumberType.DataSource = ProcessorUtilities.GetAll<NumberType>(false).Select(i => new ViewListItem(i.Value.FormattedString(), i.Value));
            ddlNumberType.DataBind();
            ddlNumberType.SelectedValue = NumberType.Mobile.GetString();

            txtTrackStartDate.Attributes.Add("eShipMinDate", DateTime.Now.Date.FormattedShortDate());
            txtTrackStartDate.Text = DateTime.Now.ToShortDateString();

            ddlTime.DataSource = TimeUtility.
                BuildTimeList(new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second))
                .Select(i => new ViewListItem(i, i));
            ddlTime.DataBind();

            var mps = ProcessorVars.IntegrationVariables[ActiveUser.TenantId].MacroPointParams;
            if (mps == null) return;
            ddlTrackingOptions.DataSource = mps.PriceBreaks
                                               .OrderBy(b => b.Category)
                                               .ThenBy(b => b.TrackDurationHours.ToEnum<TrackDuration>().ToInt())
                                               .ThenBy(b => b.TrackInternvalMinutes.ToEnum<TrackDuration>().ToInt())
                                               .Select(b => new ViewListItem(b.ListKey, b.ListKey));
            ddlTrackingOptions.DataBind();
            ddlTrackingOptions.SelectedIndex = 0;
        }


        protected void OnSubmitOrderClicked(object sender, EventArgs e)
        {
            var settings = ProcessorVars.IntegrationVariables[ActiveUser.TenantId].MacroPointParams;
            var pb = settings.PriceBreaks.First(b => b.ListKey == ddlTrackingOptions.SelectedValue);
            var order = new Order
                {
                    Carrier = null,
                    Number = new Number
                        {
                            Type = ddlNumberType.SelectedValue,
                            Value = txtNumber.Text,
                        },
                    Notifications = new List<Notification>
                        {
                            new Notification
                                {
                                    IDNumber = string.Empty,
                                    PartnerMPID = settings.Key,
                                    TrackDurationInHours = pb.TrackDurationHours,
                                    TrackIntervalInMinutes = pb.TrackInternvalMinutes
                                }
                        },
                    TripSheet = null,
                    TrackStartDateTime = txtTrackStartDate.Text.ToDateTime().FormattedShortDateAlt() + " " + ddlTime.SelectedValue + " ET",
                    EmailCopiesOfUpdatesTo = !string.IsNullOrEmpty(txtEmailAddresses.Text)
                                                 ? txtEmailAddresses.Text.Split(WebApplicationUtilities.ImportSeperators(),StringSplitOptions.RemoveEmptyEntries).Select(p => p.StripSpacesFromEmails()).ToArray().CommaJoin()
                                                 : null,
                    Notes = txtNotes.Text != string.Empty ? txtNotes.Text : null
                };

            if (SubmitOrder != null)
                SubmitOrder(this, new ViewEventArgs<Order>(order));
        }

        protected void OnCloseOrderInputClicked(object sender, EventArgs e)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnDateTimePickerTextChanged(object sender, EventArgs e)
        {
            //Validate the datetime data and set the page accordingly as well as the selected option
            var dateTimeChosen = txtTrackStartDate.Text.ToDateTime();
            if (dateTimeChosen.Date == DateTime.Now.Date)
            {
                ddlTime.DataSource = TimeUtility
                    .BuildTimeList(new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second))
                    .Select(i => new ViewListItem(i, i));
                ddlTime.DataBind();

            }
            else
            {
                ddlTime.DataSource = TimeUtility.BuildTimeList().Select(i => new ViewListItem(i, i));
                ddlTime.DataBind();
            }
        }
    }
}