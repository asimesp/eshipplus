﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Handlers;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class RateAndScheduleProfilesControl : MemberControlBase
    {

        public delegate RateAndScheduleDefaultsItem GetProfileDelegate();


        public event EventHandler<ViewEventArgs<string>> ProcessingError;
        public event EventHandler<ViewEventArgs<RateAndScheduleDefaultsItem>> SearchProfileSelected;


        public GetProfileDelegate GetProfile;


        public RateAndScheduleDefaultsItem RetrieveDefaultProfile(bool setToDefault = false)
        {
            var @default = SearchDefaults?.Default();
            if (setToDefault) SetSelectedProfile(@default);
            return @default;
        }

        public void SetSelectedProfile(RateAndScheduleDefaultsItem item = null)
        {
            var profileName = item == null ? string.Empty : item.Name;
            btnUpdateProfile.Visible = (profileName != string.Empty);
            if (ddlRandDProfiles.ContainsValue(profileName)) ddlRandDProfiles.SelectedValue = profileName;
        }


        private void LoadDropdownProfiles()
        {
            var profiles = SearchDefaults == null
                            ? new List<ViewListItem>()
                            : SearchDefaults.RateAndScheduleProfiles.Select(i => new ViewListItem(i.Name, i.Name)).ToList();
            profiles.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, string.Empty));
            ddlRandDProfiles.DataSource = profiles;
            ddlRandDProfiles.DataBind();
            ddlRandDProfiles.SelectedValue = string.Empty;
            btnUpdateProfile.Visible = false;
        }


        protected void SaveProfile(string profileName)
        {
            if (string.IsNullOrEmpty(profileName))
            {
                ProcessingError?.Invoke(this, new ViewEventArgs<string>("Rate and Schedule Profile name cannot be empty!"));
                return;
            }

            var item = GetProfile();
            try
            {
                item.Name = profileName;
                var profiles = SearchDefaults ?? new SearchDefaults();
                profiles.AddOrUpdate(item);

                var searchProfileDefault = SearchDefaults == null
                    ? new SearchProfileDefault { User = ActiveUser, Tenant = ActiveUser.Tenant }
                    : new SearchProfileDefault(ActiveUser);

                searchProfileDefault.RateAndScheduleProfiles = profiles.RateAndScheduleProfiles.ToXml();
                var ex = new SearchProfileDefaultHandler().SaveSearchProfileDefaults(searchProfileDefault);
                if (ex != null) ErrorLogger.LogError(ex, Context);
                else SearchDefaults = new SearchDefaults(searchProfileDefault);

                LoadDropdownProfiles();

                if (ddlRandDProfiles.ContainsValue(item.Name)) ddlRandDProfiles.SelectedValue = item.Name;
                btnUpdateProfile.Visible = (ddlRandDProfiles.SelectedValue != string.Empty);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                ProcessingError?.Invoke(this, new ViewEventArgs<string>(ex.Message));
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                LoadDropdownProfiles();
        }

        protected void OnDeleteProfileClicked(object sender, EventArgs e)
        {
            try
            {
                var profiles = SearchDefaults ?? new SearchDefaults();
                var profile = profiles.RateAndScheduleProfiles.FirstOrDefault(p => p.Name == ddlRandDProfiles.SelectedValue);
                if (profile != null) profiles.Remove(profile);

                var searchProfileDefault = SearchDefaults == null
                                            ? new SearchProfileDefault { User = ActiveUser }
                                            : new SearchProfileDefault(ActiveUser);

                searchProfileDefault.RateAndScheduleProfiles = profiles.RateAndScheduleProfiles.ToXml();
                var ex = new SearchProfileDefaultHandler().SaveSearchProfileDefaults(searchProfileDefault);
                if (ex != null) ErrorLogger.LogError(ex, Context);
                else SearchDefaults = new SearchDefaults(searchProfileDefault);


                LoadDropdownProfiles();

            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                ProcessingError?.Invoke(this, new ViewEventArgs<string>(ex.Message));
            }

        }

        protected void OnSearchProfilesSelectedIndexChanged(object sender, EventArgs e)
        {
            btnUpdateProfile.Visible = (ddlRandDProfiles.SelectedValue != string.Empty);
            if (ddlRandDProfiles.SelectedValue == string.Empty) return;

            try
            {
                var profiles = SearchDefaults != null
                                ? SearchDefaults.RateAndScheduleProfiles
                                : new List<RateAndScheduleDefaultsItem>();
                var profile = profiles.FirstOrDefault(p => p.Name == ddlRandDProfiles.SelectedValue);

                SearchProfileSelected?.Invoke(this, new ViewEventArgs<RateAndScheduleDefaultsItem>(profile));
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                ProcessingError?.Invoke(this, new ViewEventArgs<string>(ex.Message));
            }
        }

        protected void OnSaveProfileClicked(object sender, EventArgs e)
        {
            SaveProfile(txtSearchProfileName.Text);
        }

        protected void OnUpdateProfileClicked(object sender, EventArgs e)
        {
            SaveProfile(ddlRandDProfiles.SelectedValue);
        }


    }
}