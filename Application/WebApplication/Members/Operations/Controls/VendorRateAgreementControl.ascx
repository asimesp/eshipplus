﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorRateAgreementControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.VendorRateAgreementControl" %>
<asp:Panel runat="server" ID="pnlVendorRateAgreement" CssClass="popupControl popupControlOverW500">
    <div class="popheader">
        <h4>Vendor Rate Agreement</h4>
        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
    </div>
    <div class="row">
        <div class="fieldgroup pl20 pt20">
            <label class="upper blue">Vendor:</label>
            <asp:DropDownList runat="server" ID="ddlVendor" DataTextField="Text" DataValueField="Value" CssClass="w200" />
        </div>
    </div>
    <h5 class="text-center">Charges</h5>
    <table class="stripe">
        <tr class="header">
            <th style="width: 5%;"></th>
            <th style="width: 55%;">Charge Code</th>
            <th style="width: 40%;" class="text-right">Final Buy ($)</th>
        </tr>
        <asp:ListView runat="server" ID="lstCharges" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="hidChargeItemIndexId" runat="server" Value='<%# Container.DataItemIndex %>' />
                        <asp:CheckBox runat="server" ID="chkSelection" CssClass="jQueryUniform"/>
                    </td>
                    <td>
                        <%# Eval("Code") + " - " + Eval("Description") %>
                    </td>
                    <td class="text-right">
                        <%# Eval("FinalBuy") %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </table>
    <div class="row pt10 pb10 text-right">
        <asp:Button runat="server" ID="btnGenerate" Text="Generate" OnClick="OnGenerateClicked" CausesValidation="False"/>
        <asp:Button runat="server" ID="btnClose" Text="Cancel" OnClick="OnCloseClicked" CausesValidation="False"/>
    </div>
</asp:Panel>
<asp:Panel ID="pnlVendorRateAgreementDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
