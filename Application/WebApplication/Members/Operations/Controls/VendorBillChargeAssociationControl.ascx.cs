﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class VendorBillChargeAssociationControl : MemberControlBaseWithControlStore, IVendorBillChargeAssociationView
    {
		private const string ChargeIdStoreKey = "CSK";
		protected const string SaveToBillCommandArgument = "SaveToBill";
        protected const string CreateBillCommandArgument = "CreateBill";

		public new bool Visible
		{
			get { return pnlVendorBillCharge.Visible; }
			set
			{
				base.Visible = true;
				pnlVendorBillCharge.Visible = value;
			}
		}
		
		public List<long> ChargeIds
		{
			get
			{
				return ControlStore.ContainsKey(ChargeIdStoreKey)
						   ? ControlStore[ChargeIdStoreKey] as List<long> : new List<long>();
			}
			set
			{
				if (!ControlStore.ContainsKey(ChargeIdStoreKey)) ControlStore.Add(ChargeIdStoreKey, value);
				else ControlStore[ChargeIdStoreKey] = value;
			}
		}

		public long ChargeVendorId
		{
            get { return hidVendorId.Value.ToLong(); }
            set { hidVendorId.Value = value.ToString(); }
		}

	    public string ReferenceEntityId
	    {
	        get { return hidReferenceNumber.Value; }
            set { hidReferenceNumber.Value = value; }
	    }

        public VendorBillAssosciationType ReferenceType
        {
            get { return hidReferenceType.Value.ToEnum<VendorBillAssosciationType>(); }
            set { hidReferenceType.Value = value.ToInt().ToString(); }
        }

		public event EventHandler Cancel;
        public event EventHandler<ViewEventArgs<List<ShipmentCharge>, VendorBill>> SaveToVendorBillForShipment;
        public event EventHandler<ViewEventArgs<List<ServiceTicketCharge>, VendorBill>> SaveToVendorBillForServiceTicket;
        public event EventHandler<ViewEventArgs<IEnumerable<ValidationMessage>>> ProcessingComplete;
		
        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            txtDocumentNumber.Text = string.Empty;

            if (ProcessingComplete != null)
                ProcessingComplete(this, new ViewEventArgs<IEnumerable<ValidationMessage>>(messages));
        }

	    public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }


		protected void Page_Load(object sender, EventArgs e)
		{
			new VendorBillChargeAssociationHandler(this).Initialize();

		    if (IsPostBack) return;

		    btnCreateNewBill.CommandArgument = CreateBillCommandArgument;
            btnSaveToBill.CommandArgument = SaveToBillCommandArgument;
		}


		protected void OnCancelClicked(object sender, EventArgs eventArgs)
		{
            txtDocumentNumber.Text = string.Empty;
		    chkAddToVendorBill.Checked = false;
		    chkCreateNewVendorBill.Checked = false;
            btnCreateNewBill.Visible = false;
            btnSaveToBill.Visible = false;

			if (Cancel != null)
				Cancel(this, new EventArgs());
		}

	    protected void OnSaveToBillClicked(object sender, EventArgs eventArgs)
	    {
	        VendorBill vendorBill;
            var vendor = new Vendor(ChargeVendorId);

            if (vendor.IsNew)
            {
                txtDocumentNumber.Text = string.Empty;
                chkCreateNewVendorBill.Checked = false;
                chkAddToVendorBill.Checked = false;
                btnCreateNewBill.Visible = false;
                btnSaveToBill.Visible = false;

                if (ProcessingComplete != null)
                    ProcessingComplete(this, new ViewEventArgs<IEnumerable<ValidationMessage>>(new[] { ValidationMessage.Error("Vendor Bill Vendor must be valid") }));
                return;
            }

	        var btnSave = (Button) sender;
            if (btnSave.CommandArgument == CreateBillCommandArgument)
            {
                vendorBill = new VendorBill
                    {
                        DocumentNumber = txtDocumentNumber.Text,
                        DateCreated = DateTime.Now,
                        DocumentDate = DateUtility.SystemEarliestDateTime,
                        PostDate = DateUtility.SystemEarliestDateTime,
                        ExportDate = DateUtility.SystemEarliestDateTime,
                        BillType = InvoiceType.Invoice,
                        ApplyToDocumentId = default(long),
                        Details = new List<VendorBillDetail>(),
                        TenantId = ActiveUser.TenantId,
                        User = ActiveUser
                    };
            }
            else
            {
                vendorBill = new VendorBillSearch().FetchVendorBillByVendorBillNumber(txtDocumentNumber.Text,
                                                                                             vendor.Locations.First(v => v.Primary).Id,
                                                                                             ActiveUser.Tenant.Id);

                if (vendorBill == null)
                {
                    txtDocumentNumber.Text = string.Empty;
                    chkAddToVendorBill.Checked = false;
                    btnSaveToBill.Visible = false;

                    if (ProcessingComplete != null)
                        ProcessingComplete(this, new ViewEventArgs<IEnumerable<ValidationMessage>>(new[] { ValidationMessage.Error("Could not find matching Vendor Bill") }));
                    return;
                }
            }

            var charge = new Charge();
            var accountBucketId = default(long);
            var referenceType = DetailReferenceType.Shipment;
		    var referenceNumber = string.Empty;
	        var chargeCodeId = default(long);
	        var shipmentCharges = new List<ShipmentCharge>();
	        var serviceTicketCharges = new List<ServiceTicketCharge>();

	        foreach (var chargeId in ChargeIds)
	        {
	            switch (ReferenceType)
	            {
	                case VendorBillAssosciationType.Shipment:
	                    charge = new ShipmentCharge(chargeId);
	                    var shipment = new Shipment(ReferenceEntityId.ToLong());
	                    accountBucketId = shipment.AccountBuckets.First(a => a.Primary).AccountBucketId;
	                    referenceNumber = shipment.ShipmentNumber;
	                    referenceType = DetailReferenceType.Shipment;
	                    chargeCodeId = charge.ChargeCodeId;
	                    shipmentCharges.Add(new ShipmentCharge(chargeId));
	                    break;

	                case VendorBillAssosciationType.ServiceTicket:
	                    charge = new ServiceTicketCharge(chargeId);
	                    var serviceTicket = new ServiceTicket(ReferenceEntityId.ToLong());
	                    accountBucketId = serviceTicket.AccountBucket.Id;
	                    referenceNumber = serviceTicket.ServiceTicketNumber;
	                    referenceType = DetailReferenceType.ServiceTicket;
                        chargeCodeId = charge.ChargeCodeId;
                        serviceTicketCharges.Add(new ServiceTicketCharge(chargeId));
	                    break;
	            }

	            if (!vendorBill.Details.Where(d => d.ReferenceNumber == referenceNumber && d.ReferenceType == referenceType)
	                                   .Select(d => d.ChargeCodeId)
	                                   .Contains(chargeCodeId))
	            {
	                vendorBill.Details.Add(new VendorBillDetail
	                    {
	                        ReferenceNumber = referenceNumber,
	                        ReferenceType = referenceType,
	                        Quantity = charge.Quantity,
	                        UnitBuy = charge.UnitBuy,
	                        ChargeCodeId = charge.ChargeCodeId,
	                        AccountBucketId = accountBucketId,
	                        Note = charge.Comment,
	                        TenantId = ActiveUser.TenantId,
	                        VendorBill = vendorBill
	                    });
	            }
	        }

	        var vendorLocationId = vendor.Locations.First(vl => vl.Primary).Id;
	        vendorBill.VendorLocationId = vendorLocationId;

            txtDocumentNumber.Text = string.Empty;
            chkCreateNewVendorBill.Checked = false;
	        chkAddToVendorBill.Checked = false;
            btnCreateNewBill.Visible = false;
	        btnSaveToBill.Visible = false;

            if (ReferenceType == VendorBillAssosciationType.Shipment)
            {
                if (SaveToVendorBillForShipment != null)
                    SaveToVendorBillForShipment(this, new ViewEventArgs<List<ShipmentCharge>, VendorBill>(shipmentCharges, vendorBill));
            }
            else
            {
                if (SaveToVendorBillForServiceTicket != null)
                    SaveToVendorBillForServiceTicket(this, new ViewEventArgs<List<ServiceTicketCharge>, VendorBill>(serviceTicketCharges, vendorBill));
            }
	        
	    }


        protected void OnChargeBillOptionsCheckChanged(object sender, EventArgs e)
        {
            btnCreateNewBill.Visible = chkCreateNewVendorBill.Checked;
            btnSaveToBill.Visible = chkAddToVendorBill.Checked;
        }
	}
}