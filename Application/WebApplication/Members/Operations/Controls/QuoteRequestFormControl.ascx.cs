﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class QuoteRequestFormControl : MemberControlBase
    {
        private const string Origin = "O";
        private const string Destination = "D";


        public ServiceMode QuoteRequestServiceMode
        {
            get { return hidServiceMode.Value.ToEnum<ServiceMode>(); }
            set
            {
                hidServiceMode.Value = value.ToString();
                switch (value)
                {
                    case ServiceMode.LessThanTruckload:
                        pnlLTLOrVolume.Visible = true;
                        pnlTruckload.Visible = false;
                        pnlHazardousMaterial.Visible = true;
                        pnlDesiredDeliveryDate.Visible = false;
                        break;
                    case ServiceMode.Truckload:
                        pnlLTLOrVolume.Visible = false;
                        pnlTruckload.Visible = true;
                        pnlHazardousMaterial.Visible = false;
                        pnlDesiredDeliveryDate.Visible = true;
                        break;
                    default:
                        pnlLTLOrVolume.Visible = true;
                        pnlTruckload.Visible = false;
                        pnlHazardousMaterial.Visible = true;
                        pnlDesiredDeliveryDate.Visible = false;
                        break;
                }
            }
        }

        public bool EnableSubmitQuoteButton
        {
            set { btnSubmitQuoteRequest.Visible = value; }
        }

        public List<EquipmentType> EquipmentTypes
        {
            set
            {
                var equipmentTypes = value
                    .OrderBy(e => e.TypeName)
                    .Select(c => new ViewListItem("  " + c.FormattedString(), c.Id.ToString()))
                    .ToList();

                rptEquipment.DataSource = equipmentTypes;
                rptEquipment.DataBind();
            }
        }

        public List<ServiceViewSearchDto> Services
        {
            set
            {
                var services = value
                    .Select(c => new ViewListItem(string.Format(" {0}", c.Description), c.Id.ToString()))
                    .OrderBy(v => v.Text)
                    .ToList();

                rptServices.DataSource = services;
                rptServices.DataBind();
            }
        }


        public event EventHandler OpenCustomerFinder;
        public event EventHandler OpenPostalCodeFinder;
        public event EventHandler OpenLibraryItemFinder;
        public event EventHandler<ViewEventArgs<LoadOrder>> SubmitQuote;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<string>> ProcessingError;


        private void AddSingleBlankItem()
        {
            var vItems = lstQuoteItems
                .Items
                .Select(i => new
                {
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
                    Weight = i.FindControl("txtTotalWeight").ToTextBox().Text,
                    PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                    Length = i.FindControl("txtLength").ToTextBox().Text,
                    Width = i.FindControl("txtWidth").ToTextBox().Text,
                    Height = i.FindControl("txtHeight").ToTextBox().Text,
                    Description = i.FindControl("txtDescription").ToTextBox().Text,
                })
                .ToList();

            vItems.Add(new
            {
                Quantity = string.Empty,
                Weight = string.Empty,
                PackageTypeId = default(long),
                FreightClass = 0d,
                Length = string.Empty,
                Width = string.Empty,
                Height = string.Empty,
                Description = string.Empty,
            });

            lstQuoteItems.DataSource = vItems;
            lstQuoteItems.DataBind();
        }

        private void SingleCustomerSetup()
        {
            if (!ActiveUser.DefaultCustomer.Active)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>("Customer is not active"));
                return;
            }

            DisplayCustomer(ActiveUser.DefaultCustomer);
        }
        


        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();

            lstReferences.DataSource = customer == null
                                           ? new List<Reference>()
                                           : customer.CustomFields
                                                     .Select(f => new Reference
                                                     {
                                                         Name = f.Name,
                                                         DisplayOnDestination = f.DisplayOnDestination,
                                                         DisplayOnOrigin = f.DisplayOnOrigin,
                                                         Required = f.Required,
                                                         Value = string.Empty
                                                     })
                                                     .OrderBy(r => r.Name)
                                                     .ToList();
            lstReferences.DataBind();
        }

        public void DisplayPostalCode(PostalCodeViewSearchDto postalCode)
        {
            switch (hidPostalCodeSet.Value)
            {
                case Origin:
                    txtOriginPostalCode.Text = postalCode.Code;
                    ddlOriginCountry.SelectedValue = postalCode.CountryId.ToString();
                    break;
                case Destination:
                    txtDesPostalCode.Text = postalCode.Code;
                    ddlDestinationCountry.SelectedValue = postalCode.CountryId.ToString();
                    break;
            }
            hidPostalCodeSet.Value = string.Empty;
        }

        public void AddLibraryItemsToItems(List<LibraryItem> libraryItems)
        {
            var vItems = lstQuoteItems
                .Items
                .Select(i => new
                {
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
                    Weight = i.FindControl("txtTotalWeight").ToTextBox().Text,
                    PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue,
                    Length = i.FindControl("txtLength").ToTextBox().Text,
                    Width = i.FindControl("txtWidth").ToTextBox().Text,
                    Height = i.FindControl("txtHeight").ToTextBox().Text,
                    Description = i.FindControl("txtDescription").ToTextBox().Text,
                })
                .ToList();

            for (var i = vItems.Count - 1; i >= 0; i--)
            {
                var item = vItems[i];
                if (string.IsNullOrEmpty(item.Description) && string.IsNullOrEmpty(item.Weight) &&
                    string.IsNullOrEmpty(item.Length) && string.IsNullOrEmpty(item.Width) &&
                    string.IsNullOrEmpty(item.Height) && string.IsNullOrEmpty(item.Quantity) &&
                    item.PackageTypeId == default(long) && string.IsNullOrEmpty(item.FreightClass))
                    vItems.RemoveAt(i);
            }

            var itemsToAdd = libraryItems
                                .Select(t => new
                                {
                                    Quantity = t.Quantity.ToString(),
                                    Weight = t.Weight.ToString(),
                                    t.PackageTypeId,
                                    FreightClass = t.FreightClass.ToString(),
                                    Length = t.Length.ToString(),
                                    Width = t.Width.ToString(),
                                    Height = t.Height.ToString(),
                                    t.Description,
                                })
            .ToList();

            vItems.AddRange(itemsToAdd);

            lstQuoteItems.DataSource = vItems;
            lstQuoteItems.DataBind();
        }

        public void ProcessErrorMessage(string message)
        {
            if (ProcessingError != null)
                ProcessingError(this, new ViewEventArgs<string>(message));
        }


        public void LoadLoadOrder(LoadOrder loadOrder)
        {
            DisplayCustomer(loadOrder.Customer);

            txtShipmentDate.Text = loadOrder.DesiredPickupDate.FormattedShortDate();
            if (loadOrder.Origin != null)
            {
                txtOriginPostalCode.Text = loadOrder.Origin.PostalCode;
                ddlOriginCountry.SelectedValue = loadOrder.Origin.CountryId.ToString();
            }
            if (loadOrder.Destination != null)
            {
                txtDesPostalCode.Text = loadOrder.Destination.PostalCode;
                ddlDestinationCountry.SelectedValue = loadOrder.Destination.CountryId.ToString();
            }

            lstQuoteItems.DataSource = loadOrder.Items ?? new List<LoadOrderItem>();
            lstQuoteItems.DataBind();

            txtPONumber.Text = loadOrder.PurchaseOrderNumber.GetString();
            txtShipperReference.Text = loadOrder.ShipperReference.GetString();

            lstReferences.DataSource = loadOrder
                .CustomerReferences
                .Select(r => new { r.DisplayOnOrigin, r.DisplayOnDestination, r.Name, r.Value, r.Required });
            lstReferences.DataBind();

            switch (QuoteRequestServiceMode)
            {
                case ServiceMode.LessThanTruckload:
                    chkHazMat.Checked = loadOrder.HazardousMaterial;
                    txtHazMatEmail.Text = loadOrder.HazardousMaterialContactEmail;
                    txtHazMatMobile.Text = loadOrder.HazardousMaterialContactMobile;
                    txtHazMatName.Text = loadOrder.HazardousMaterialContactName;
                    txtHazMatPhone.Text = loadOrder.HazardousMaterialContactPhone;
                    var serviceIds = loadOrder.Services.Select(l => l.ServiceId.ToString()).ToList();
                    foreach (RepeaterItem item in rptServices.Items)
                    {
                        var hid = item.FindControl("hidServiceId").ToCustomHiddenField().Value;
                        item.FindControl("chkService").ToCheckBox().Checked = serviceIds.Contains(hid);
                    }
                    break;
                case ServiceMode.Truckload:
                    txtDeliveryDateTruckload.Text = loadOrder.EstimatedDeliveryDate.FormattedShortDate();

                    var equipmentTypeIds = loadOrder.Equipments.Select(l => l.EquipmentTypeId.ToString()).ToList();
                    foreach (RepeaterItem item in rptEquipment.Items)
                    {
                        var hid = item.FindControl("hidEquipmentId").ToCustomHiddenField().Value;
                        item.FindControl("chkEquipment").ToCheckBox().Checked = equipmentTypeIds.Contains(hid);
                    }
                    break;
            }
        }

        public LoadOrder RetrieveLoadOrder()
        {
            var loadOrder = new LoadOrder
            {
                DesiredPickupDate = txtShipmentDate.Text.ToDateTime(),
                ServiceMode = QuoteRequestServiceMode,
                TenantId = ActiveUser.TenantId,
                User = ActiveUser,
                DateCreated = DateTime.Now,
                EstimatedDeliveryDate = DateUtility.SystemEarliestDateTime,
                Status = LoadOrderStatus.Offered,
                Description = string.Empty,
                ShipperBol = string.Empty,
                PurchaseOrderNumber = txtPONumber.Text,
                ShipperReference = txtShipperReference.Text,
                SalesRepAddlEntityName = string.Empty,
                HazardousMaterialContactName = string.Empty,
                HazardousMaterialContactEmail = string.Empty,
                HazardousMaterialContactMobile = string.Empty,
                HazardousMaterialContactPhone = string.Empty,

                EarlyDelivery = TimeUtility.DefaultOpen,
                EarlyPickup = TimeUtility.DefaultOpen,
                LateDelivery = TimeUtility.DefaultClose,
                LatePickup = TimeUtility.DefaultClose,

                GeneralBolComments = string.Empty,
                CriticalBolComments = string.Empty,
                MiscField1 = string.Empty,
                MiscField2 = string.Empty,

				DriverName = string.Empty,
				DriverPhoneNumber = string.Empty,	
				DriverTrailerNumber = string.Empty,

                Charges = new List<LoadOrderCharge>(),
                Vendors = new List<LoadOrderVendor>(),
                Stops = new List<LoadOrderLocation>(),
                Equipments = new List<LoadOrderEquipment>(),
                Items = new List<LoadOrderItem>(),
                Services = new List<LoadOrderService>(),
                Notes = new List<LoadOrderNote>(),
                CustomerReferences = new List<LoadOrderReference>(),
            };

            var customer = new Customer(hidCustomerId.Value.ToLong());
            loadOrder.Customer = customer;
            if (loadOrder.Customer != null)
            {
                loadOrder.AccountBuckets.Add(new LoadOrderAccountBucket
                {
                    AccountBucket = customer.DefaultAccountBucket,
                    LoadOrder = loadOrder,
                    TenantId = ActiveUser.TenantId,
                    Primary = true,
                });
                loadOrder.Prefix = loadOrder.Customer.Prefix;
                loadOrder.HidePrefix = loadOrder.Customer.HidePrefix;
            }

            var origin = new LoadOrderLocation
            {
                PostalCode = txtOriginPostalCode.Text,
                CountryId = ddlOriginCountry.SelectedValue.ToLong(),
                GeneralInfo = ddlOriginalBusinessType.SelectedValue,
                AppointmentDateTime = DateUtility.SystemEarliestDateTime,
                Description = string.Empty,
                Street1 = string.Empty,
                Street2 = string.Empty,
                Contacts = new List<LoadOrderContact>(),
                SpecialInstructions = string.Empty,
                LoadOrder = loadOrder,
                TenantId = loadOrder.TenantId,
                StopOrder = 0,
                Direction = string.Empty
            };


            var destination = new LoadOrderLocation
            {
                PostalCode = txtDesPostalCode.Text,
                CountryId = ddlDestinationCountry.SelectedValue.ToLong(),
                GeneralInfo = ddlDestinationBusinessType.SelectedValue,
                AppointmentDateTime = DateUtility.SystemEarliestDateTime,
                Description = string.Empty,
                Street1 = string.Empty,
                Street2 = string.Empty,
                Contacts = new List<LoadOrderContact>(),
                SpecialInstructions = string.Empty,
                LoadOrder = loadOrder,
                TenantId = loadOrder.TenantId,
                StopOrder = 1,
                Direction = string.Empty
            };
            loadOrder.Origin = origin;
            loadOrder.Destination = destination;

            var search = new PostalCodeSearch();

            var code = search.FetchPostalCodeByCode(loadOrder.Origin.PostalCode, loadOrder.Origin.CountryId);
            loadOrder.Origin.City = code.City.GetString();
            loadOrder.Origin.State = code.State.GetString();

            code = search.FetchPostalCodeByCode(loadOrder.Destination.PostalCode, loadOrder.Destination.CountryId);
            loadOrder.Destination.City = code.City.GetString();
            loadOrder.Destination.State = code.State.GetString();
            loadOrder.CriticalBolComments = txtSpecialInstructions.Text;

            loadOrder.Items = lstQuoteItems
                .Items
                .Select(i =>
                        new LoadOrderItem
                        {
                            LoadOrder = loadOrder,
                            TenantId = loadOrder.TenantId,
                            Weight = i.FindControl("txtTotalWeight").ToTextBox().Text.ToDecimal(),
                            Length = i.FindControl("txtLength").ToTextBox().Text.ToDecimal(),
                            Width = i.FindControl("txtWidth").ToTextBox().Text.ToDecimal(),
                            Height = i.FindControl("txtHeight").ToTextBox().Text.ToDecimal(),
                            Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                            Comment = string.Empty,
                            PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                            FreightClass = QuoteRequestServiceMode == ServiceMode.Truckload
                                               ? 0
                                               : i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                            Description = i.FindControl("txtDescription").ToTextBox().Text,
                            Pickup = 0,
                            Delivery = 1,
                            HTSCode = string.Empty,
                            NMFCCode = string.Empty,
                            IsStackable = false,
                        })
                .ToList();

            loadOrder.CustomerReferences = lstReferences.Items
                .Select(i => new LoadOrderReference
                {
                    TenantId = loadOrder.TenantId,
                    LoadOrder = loadOrder,
                    DisplayOnOrigin = i.FindControl("hidDisplayOnOrigin").ToCustomHiddenField().Value.ToBoolean(),
                    DisplayOnDestination = i.FindControl("hidDisplayOnDestination").ToCustomHiddenField().Value.ToBoolean(),
                    Name = i.FindControl("litName").ToLiteral().Text,
                    Value = i.FindControl("txtValue").ToTextBox().Text,
                    Required = i.FindControl("litRequired").Visible,
                })
                .ToList();

            switch (QuoteRequestServiceMode)
            {
                case ServiceMode.LessThanTruckload:
                    loadOrder.HazardousMaterial = chkHazMat.Checked;
                    loadOrder.HazardousMaterialContactEmail = txtHazMatEmail.Text.StripSpacesFromEmails();
                    loadOrder.HazardousMaterialContactMobile = txtHazMatMobile.Text;
                    loadOrder.HazardousMaterialContactName = txtHazMatName.Text;
                    loadOrder.HazardousMaterialContactPhone = txtHazMatPhone.Text;
                    loadOrder.Services = rptServices
                                                .Items
                                                .Cast<RepeaterItem>()
                                                .Where(i => i.FindControl("chkService").ToCheckBox().Checked)
                                                .Select(i => new LoadOrderService
                                                {
                                                    LoadOrder = loadOrder,
                                                    ServiceId = i.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong(),
                                                    TenantId = ActiveUser.TenantId,
                                                }).ToList();
                    break;
                case ServiceMode.Truckload:
                    loadOrder.EstimatedDeliveryDate = txtDeliveryDateTruckload.Text.ToDateTime();
                    loadOrder.Equipments = rptEquipment
                                                .Items
                                                .Cast<RepeaterItem>()
                                                .Where(i => i.FindControl("chkEquipment").ToCheckBox().Checked)
                                                .Select(i => new LoadOrderEquipment
                                                {
                                                    LoadOrder = loadOrder,
                                                    EquipmentTypeId = i.FindControl("hidEquipmentId").ToCustomHiddenField().Value.ToLong(),
                                                    TenantId = ActiveUser.TenantId,
                                                }).ToList();
                    break;
            }

            return loadOrder;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            if (!ActiveUser.TenantEmployee && !ActiveUser.UserShipAs.Any())
                SingleCustomerSetup();

            if (!lstQuoteItems.Items.Any())
                AddSingleBlankItem();

            if (ddlOriginCountry.SelectedValue.ToLong() == 0 && ddlOriginCountry.ContainsValue(ActiveUser.Tenant.DefaultCountryId.ToString()))
                ddlOriginCountry.SelectedValue = ActiveUser.Tenant.DefaultCountryId.ToString();
            if (ddlDestinationCountry.SelectedValue.ToLong() == 0 && ddlDestinationCountry.ContainsValue(ActiveUser.Tenant.DefaultCountryId.ToString()))
                ddlDestinationCountry.SelectedValue = ActiveUser.Tenant.DefaultCountryId.ToString();
        }



        protected void OnAddAdditionalItemClick(object sender, EventArgs e)
        {
            AddSingleBlankItem();
        }

        protected void OnItemsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = e.Item as ListViewDataItem;
            if (item == null) return;

            var freightClasses = WebApplicationSettings.FreightClasses
                .Select(fc => new ViewListItem(fc.ToString(), fc.ToString()))
                .ToList();
            freightClasses.Insert(0, new ViewListItem(string.Empty, string.Empty));

            var ddlFreightClass = item.FindControl("ddlFreightClass").ToDropDownList();
            ddlFreightClass.DataSource = freightClasses;
            ddlFreightClass.DataBind();

            if (!item.DataItem.HasGettableProperty("FreightClass")) return;
            
            var value = item.DataItem.GetPropertyValue("FreightClass").ToString();
            if (ddlFreightClass.ContainsValue(value)) ddlFreightClass.SelectedValue = value;
            else ddlFreightClass.SelectedIndex = 0;
        }


        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCustomerNumber.Text))
            {
                txtCustomerName.Text = string.Empty;
                lstReferences.DataSource = new List<Reference>();
                lstReferences.DataBind();
                return;
            }

            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            if (OpenCustomerFinder != null)
                OpenCustomerFinder(this, new EventArgs());
        }

        protected void OnAddLibraryItemClicked(object sender, EventArgs e)
        {
            if (OpenLibraryItemFinder != null)
                OpenLibraryItemFinder(this, new EventArgs());
        }

        protected void OnOriginPostalCodeSearchClicked(object sender, ImageClickEventArgs e)
        {
            hidPostalCodeSet.Value = Origin;
            if (OpenPostalCodeFinder != null)
                OpenPostalCodeFinder(this, new EventArgs());
        }

        protected void OnDestinationPostalCodeSearchClicked(object sender, ImageClickEventArgs e)
        {
            hidPostalCodeSet.Value = Destination;
            if (OpenPostalCodeFinder != null)
                OpenPostalCodeFinder(this, new EventArgs());
        }


        protected void OnDeleteItemClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var rowItemIndex = button.FindControl("hidItemIndex").ToCustomHiddenField().Value.ToLong();


            var vItems = lstQuoteItems
                .Items
                .Select(i => new
                {
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
                    Weight = i.FindControl("txtTotalWeight").ToTextBox().Text,
                    PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                    Length = i.FindControl("txtLength").ToTextBox().Text,
                    Width = i.FindControl("txtWidth").ToTextBox().Text,
                    Height = i.FindControl("txtHeight").ToTextBox().Text,
                    Description = i.FindControl("txtDescription").ToTextBox().Text,
                    rowItemIndex = i.FindControl("hidItemIndex").ToCustomHiddenField().Value.ToLong()
                })
                .Where(item => item.rowItemIndex != rowItemIndex)
                .ToList();

            lstQuoteItems.DataSource = vItems;
            lstQuoteItems.DataBind();
        }

        protected void OnHazMatCheckChanged(object sender, EventArgs e)
        {
            pnlHazMat.Visible = chkHazMat.Checked;
        }

        protected void OnSubmitQuoteRequestClicked(object sender, EventArgs e)
        {
            if (SubmitQuote != null)
                SubmitQuote(this, new ViewEventArgs<LoadOrder>(RetrieveLoadOrder()));
        }
    }
}