﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Dat;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class DatLoadboardAssetControl : MemberControlBase, IDatAssetManagerView
    {
        public const int DatCommentMaxLength = 70;

        public new bool Visible
        {
            get { return pnlDatLoadboardAssetContent.Visible; }
            set
            {
                base.Visible = true;
                pnlDatLoadboardAssetContent.Visible = value;
                pnlDatLoadboardAssetDimScreen.Visible = value;
            }
        }

        public Dictionary<int, string> RateBasedOnTypes
        {
            set
            {
                ddlDatRateTypeForAssetPost.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                ddlDatRateTypeForAssetPost.DataBind();
            }
        }

        public long LoadOrderId
        {
            get { return hidLoadOrderId.Value.ToLong(); }
            set { hidLoadOrderId.Value = value.ToString(); }
        }



        public event EventHandler Loading;
        public event EventHandler DoneModifying;

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new DatAssetManagerHandler(this).Initialize();

            if (IsPostBack) return;

            if (ActiveUser.HasAccessTo(ViewCode.DatLoadboard) && !string.IsNullOrWhiteSpace(ActiveUser.DatLoadboardUsername)) // ensure username is present before attempting to use functionality.
            {
	            var user = new User(ActiveUser.Id);
	            var connexion = new Connexion(user.DatLoadboardUsername, user.DatLoadboardPassword);
                if (!connexion.AccountHasAccessTo(CapabilityType.Search)) btnLookupRatesForAsset.CssClass += WebApplicationConstants.CssClassHiddenWithSpace;
                if (!connexion.AccountHasAccessTo(CapabilityType.ManagePostings))
                {
                    btnEnterRatePostAssetToDat.CssClass += WebApplicationConstants.CssClassHiddenWithSpace;
                    btnUpdateAssetAtDat.CssClass += WebApplicationConstants.CssClassHiddenWithSpace;
                    btnDeleteAssetFromDat.CssClass += WebApplicationConstants.CssClassHiddenWithSpace;
                }
            }

            if (Loading != null)
                Loading(this, new EventArgs());
        }
        

        protected void OnCloseManageAssetsOnLoadboardClicked(object sender, EventArgs e)
        {
            rptDatAssetComments.Visible = false;
            rptDatAssetComments.DataSource = new List<object>();
            rptDatAssetComments.DataBind();

			if (DoneModifying != null)
                DoneModifying(this, new EventArgs());
        }


        protected void OnAddDatCommentClicked(object sender, ImageClickEventArgs e)
        {
            var comments = rptDatAssetComments
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new
                {
                    Comment = i.FindControl("txtDatAssetComment").ToTextBox().Text,
                    Exists = i.FindControl("hidDatCommentExists").ToCustomHiddenField().Value.ToBoolean()
                })
                .ToList();
            comments.Add(new { Comment = string.Empty, Exists = false });
            rptDatAssetComments.DataSource = comments;
            rptDatAssetComments.DataBind();
            rptDatAssetComments.Visible = comments.Any();

            if (comments.Count == 2)
            {
                ibtnAddComment.Visible = false;
            }
        }

        protected void OnUpdateClicked(object sender, EventArgs e)
        {
            var posting = new DatLoadboardAssetPosting(hidAssetId.Value, hidTenantId.Value.ToLong());

            if (posting.IsNew) return;

            var mileage = hidLoadOrderMileage.Value.ToDecimal();

            txtBaseRateForAssetPost.Text = posting.BaseRate.ToString();
            txtMileageForAssetPost.Text = mileage.ToString();
            ddlDatRateTypeForAssetPost.SelectedValue = posting.RateType.ToInt().ToString();

            var total = posting.RateType == RateBasedOnType.Flat ? posting.BaseRate : posting.BaseRate * mileage;
            txtDatRateEntryTotalForAssetPost.Text = total.ToString();

            rptDatAssetComments.DataSource = posting.Comments.Select(c => new { Comment = c, Exists = true });
            rptDatAssetComments.DataBind();
            rptDatAssetComments.Visible = posting.Comments.Any();
        }

		protected void DeleteAsset(object sender, EventArgs e)
		{
		    txtBaseRateForAssetPost.Text = string.Empty;
		    txtDatRateEntryTotalForAssetPost.Text = string.Empty;
		    ddlDatRateTypeForAssetPost.SelectedValue = RateBasedOnType.Flat.ToInt().ToString();

		    hidAssetId.Value = string.Empty;
		    hidTenantId.Value = string.Empty;

			rptDatAssetComments.DataSource = new List<object>();
			rptDatAssetComments.DataBind();
		}

        protected void OnDeleteDatCommentClicked(object sender, ImageClickEventArgs e)
        {
            var comments = rptDatAssetComments
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new
                {
                    Comment = i.FindControl("txtDatAssetComment").ToTextBox().Text,
                    Exists = i.FindControl("hidDatCommentExists").ToCustomHiddenField().Value.ToBoolean()
                })
                .ToList();
            comments.RemoveAt(((Control)sender).FindControl("hidDatCommentIndex").ToCustomHiddenField().Value.ToInt());
            rptDatAssetComments.DataSource = comments;
            rptDatAssetComments.DataBind();
            rptDatAssetComments.Visible = comments.Any();

            if (comments.Count < 2)
            {
                ibtnAddComment.Visible = true;
            }
        }


        public void LoadDatAssetForLoadOrder(long loadOrderId)
        {
            if (loadOrderId == default(long))
            {
                LoadOrderId = default(long);
                txtDatBaseRate.Text = string.Empty;
                return;
            }

            var loadOrder = new LoadOrder(loadOrderId);
            LoadOrderId = loadOrderId;

            litShipmentOrLoadOrderNumberLabel.Text = loadOrder.EntityName().FormattedString();
            lblDatLoadboardErrorMessages.Text = string.Empty;

            txtLoadNumber.Text = loadOrder.LoadOrderNumber;
            hidLoadOrderId.Value = loadOrder.Id.ToString();
            hidLoadOrderMileage.Value = Math.Ceiling(loadOrder.Mileage).ToString();
            hidLoadOrderUnitBuyTotal.Value = loadOrder.Charges.Sum(c => c.AmountDue).ToString("n2");
            hidUserId.Value = ActiveUser.Id.ToString().UrlTextEncrypt();

            var posting = new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(loadOrder.LoadOrderNumber, loadOrder.TenantId);

            if (posting != null)
            {
                hidAssetId.Value = posting.AssetId;
                hidTenantId.Value = posting.TenantId.ToString();
                txtDatAssetId.Text = posting.AssetId;
                txtDatBaseRate.Text = posting.BaseRate.ToString("n2");
                txtDatRateType.Text = posting.RateType.FormattedString();
                txtDatPostedByUser.Text = posting.User.FullName;
                txtDatMileage.Text = posting.Mileage.ToString();
                txtDatExpirationDate.Text = posting.ExpirationDate.FormattedLongDate();
                txtDatDateCreated.Text = posting.DateCreated.FormattedLongDate();
                ddlDatRateTypeForAssetPost.SelectedValue = posting.RateType.ToInt().ToString();
                chkWasSentAsHazMat.Checked = posting.HazardousMaterial;
                rptDatAssetComments.DataSource = posting.Comments.Select(c => new { Comment = c, Exists = true });
                rptDatAssetComments.DataBind();
                rptDatAssetComments.Visible = posting.Comments.Any();
                if (posting.Comments.Count == 2) ibtnAddComment.Visible = false;
            }
            else
            {
                hidAssetId.Value = string.Empty;
                hidTenantId.Value = string.Empty;
                txtDatAssetId.Text = string.Empty;
                txtDatBaseRate.Text = string.Empty;
                txtDatRateType.Text = string.Empty;
                txtDatPostedByUser.Text = string.Empty;
                txtDatMileage.Text = string.Empty;
                txtDatExpirationDate.Text = string.Empty;
                txtDatDateCreated.Text = string.Empty;
                chkWasSentAsHazMat.Checked = false;
                ddlDatRateTypeForAssetPost.SelectedValue = RateBasedOnType.Flat.ToInt().ToString();

                // Assets can only have two comments, each having a max-length of 70
                var datComments = new List<string>();
                if (loadOrder.Description.Length > 140)
                {
                    datComments.Add(loadOrder.Description.Substring(0, DatCommentMaxLength));
                    datComments.Add(loadOrder.Description.Substring(DatCommentMaxLength, DatCommentMaxLength));
                }
                else if (loadOrder.Description.Length > DatCommentMaxLength)
                {
                    datComments.Add(loadOrder.Description.Substring(0, DatCommentMaxLength));
                    datComments.Add(loadOrder.Description.Substring(DatCommentMaxLength, loadOrder.Description.Length - 71));
                }
                else if (loadOrder.Description != string.Empty)
                {
                    datComments.Add(loadOrder.Description);
                }

                rptDatAssetComments.DataSource = datComments.Select(c => new { Comment = c, Exists = false});
                rptDatAssetComments.DataBind();
                rptDatAssetComments.Visible = datComments.Any();
            }
        }


	   }
}