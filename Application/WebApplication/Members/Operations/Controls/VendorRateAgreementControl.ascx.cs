﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations.Controls
{
    public partial class VendorRateAgreementControl : MemberControlBase
    {
        public new bool Visible
        {
            get { return pnlVendorRateAgreement.Visible; }
            set
            {
                base.Visible = true;
                pnlVendorRateAgreement.Visible = value;
                pnlVendorRateAgreementDimScreen.Visible = value;
            }
        }

        public event EventHandler Close;
        public event EventHandler Generate;


        public void LoadData(IDictionary<long, string> vendorIdAndNames, IEnumerable<Charge> charges)
        {
            ddlVendor.DataSource = vendorIdAndNames.Keys.Select(k => new ViewListItem(vendorIdAndNames[k], k.ToString()));
            ddlVendor.DataBind();

            lstCharges.DataSource = charges.Select(c => new { Checked = false, c.ChargeCode.Code, c.ChargeCode.Description, c.FinalBuy }).ToList();
            lstCharges.DataBind();
        }

        public long RetrieveVendorId()
        {
            return ddlVendor.SelectedValue.ToLong();
        }

        public IEnumerable<int> RetrieveSelectedIndices()
        {
            var indices = lstCharges.Items
                .Select(i => new
                {
                    Index = i.FindControl("hidChargeItemIndexId").ToCustomHiddenField().Value.ToInt(),
                    Selection = i.FindControl("chkSelection").ToCheckBox().Checked
                })
                .Where(i => i.Selection).ToList();

            var result = indices.Select(i => i.Index);

            return result;
        }


        protected void OnGenerateClicked(object sender, EventArgs e)
        {
            if (Generate != null)
                Generate(this, new EventArgs());
        }

        protected void OnCloseClicked(object sender, EventArgs eventArgs)
        {
            if (Close != null)
                Close(this, new EventArgs());
        }
    }
}