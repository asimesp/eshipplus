﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="JobDashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.JobDashboardView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowMore="True" OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                        Job Dashboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />

        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Jobs" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" ShowAutoRefresh="True" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort By:</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstJobDetails" UpdatePanelToExtendId="upDataUpdate" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheJobDashboardTable" TableId="jobDashboardTable" HeaderZIndex="2" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <div class="rowgroup">
                    <table id="jobDashboardTable" class="line2 pl2">
                        <tr>
                            <th style="width: 5%;">
                                <asp:LinkButton runat="server" ID="lbtnSortJobNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							        Job #
                                </asp:LinkButton>
                            </th>
                            <th style="width: 5%;">
                                <asp:LinkButton runat="server" ID="lbtnSortDateCreated" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							        Date Created
                                </asp:LinkButton>
                            </th>
                            <th style="width: 23%;">Customer&nbsp;
                                <asp:LinkButton runat="server" ID="lbtnSortCustomerNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							          Number
                                </asp:LinkButton>\
                                <asp:LinkButton runat="server" ID="lbtnSortCustomerName" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							          Name
                                </asp:LinkButton>
                            </th>
                            <th style="width: 16%;">
                                <asp:LinkButton runat="server" ID="lbtnSortUsername" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							        User
                                </asp:LinkButton>
                            </th>
                            <th style="width: 23%;">
                                <asp:LinkButton runat="server" ID="lbtnSortExternalReference1" CssClass="link_nounderline blue" CausesValidation="False">
							        External Reference 1
                                </asp:LinkButton>
                            </th>
                            <th style="width: 23%;">
                                <asp:LinkButton runat="server" ID="lbtnExternalReference2" CssClass="link_nounderline blue" CausesValidation="False">
							        External Reference 2
                                </asp:LinkButton>
                            </th>
                            <th style="width: 5%;">
                                <asp:LinkButton runat="server" ID="lbtnSortStatusText" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
							        Status
                                </asp:LinkButton>
                            </th>
                            <th style="width: 5%;" class="text-center">Action</th>
                        </tr>
                        <asp:ListView runat="server" ID="lstJobDashboardDetails" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnJobDashboardDetailsItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <eShip:JobDashboardDetailControl runat="server" ID="jobDetailControl" ItemIndex='<%# Container.DataItemIndex %>' />
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortJobNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortDateCreated" />
                <asp:PostBackTrigger ControlID="lbtnSortCustomerNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortCustomerName" />
                <asp:PostBackTrigger ControlID="lbtnSortUsername" />
                <asp:PostBackTrigger ControlID="lbtnSortStatusText" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Title="Import Users" Instructions="**Please refer to More menu for import file format." Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
                               OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    
    <asp:UpdatePanel runat="server" UpdateMode="Always">
        <ContentTemplate>
            <eShip:CustomHiddenField runat="server" ID="hidImportingJob" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
