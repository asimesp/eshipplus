﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;


namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class CarrierPreferredLaneView : MemberPageBase, ICarrierPreferredLaneView
    {
        private const string ExportFlag = "E";

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public override ViewCode PageCode { get { return ViewCode.CarrierPreferredLane; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public static string PageAddress { get { return "~/Members/Operations/CarrierPreferredLaneView.aspx"; } }

        public event EventHandler<ViewEventArgs<VendorPreferredLane>> Save;
        public event EventHandler<ViewEventArgs<VendorPreferredLane>> Delete;
        public event EventHandler<ViewEventArgs<VendorPreferredLane>> Lock;
        public event EventHandler<ViewEventArgs<VendorPreferredLane>> UnLock;
        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<string>> VendorSearch;
        public event EventHandler<ViewEventArgs<List<VendorPreferredLane>>> BatchImport;

        public void DisplaySearchResult(List<VendorPreferredLaneViewSearchDto> vendorPreferredLanes)
        {
            litRecordCount.Text = vendorPreferredLanes.BuildRecordCount();
            upcseDataUpdate.DataSource = vendorPreferredLanes;
            upcseDataUpdate.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var lines = vendorPreferredLanes
                    .Select(a => new[]
					             	{
                                        a.VendorNumber,
                                        a.VendorName,
					             		a.OriginCity,
                                        a.OriginState,
                                        a.OriginPostalCode,
                                        a.OriginCountryCode,
                                        a.DestinationCity,
                                        a.DestinationState,
                                        a.DestinationPostalCode,
                                        a.DestinationCountryCode
					             	}.TabJoin())
                    .ToList();
                lines.Insert(0,
                             new[]
				             	{
				             		"Vendor Number", "Vendor Name", "Origin City", "Origin State", "Origin Postal Code",
				             		"Origin Country", "Destination City", "Destination State","Destination Postal Code","Destination Country"
				             	}.TabJoin());

                hidFlag.Value = string.Empty;

                Response.Export(lines.ToArray().NewLineJoin(), "CarrierPreferredLanes.txt");
            }
        }

        public void DisplayVendor(Vendor vendor)
        {
            txtVendorNumber.Text = vendor == null ? string.Empty : vendor.VendorNumber;
            txtVendorName.Text = vendor == null ? string.Empty : vendor.Name;
            hidEditVendorId.Value = vendor == null ? default(long).ToString() : vendor.Id.ToString();

            var inputEnabled = vendor != null && Access.Modify;
            memberToolBar.ShowNew = inputEnabled;
            memberToolBar.ShowImport = inputEnabled;

        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() || messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearch(List<ParameterColumn> columns)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(columns));
        }


        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEditCarrierPreferredLane.Visible = true;
            pnlDimScreen.Visible = pnlEditCarrierPreferredLane.Visible;
        }

        private void CloseEditPopup()
        {
            pnlEditCarrierPreferredLane.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<VendorPreferredLane>(new VendorPreferredLane(hidEditCarrierPreferredLaneId.Value.ToLong(), false)));
        }

        private void LoadCarrierPreferredLaneForEdit(VendorPreferredLane vendorPreferredLane)
        {
            hidEditCarrierPreferredLaneId.Value = vendorPreferredLane.Id.ToString();
            hidEditVendorId.Value = vendorPreferredLane.VendorId.ToString();
            DisplayVendor(vendorPreferredLane.Vendor);

            txtEditOriginCity.Text = vendorPreferredLane.OriginCity;
            txtEditOriginState.Text = vendorPreferredLane.OriginState;
            txtEditOriginPostalCode.Text = vendorPreferredLane.OriginPostalCode;
            ddlEditOriginCountries.SelectedValue = vendorPreferredLane.OriginCountryId.GetString();

            txtEditDestCity.Text = vendorPreferredLane.DestinationCity;
            txtEditDestState.Text = vendorPreferredLane.DestinationState;
            txtEditDestPostalCode.Text = vendorPreferredLane.DestinationPostalCode;
            ddlEditDestCountries.SelectedValue = vendorPreferredLane.DestinationCountryId.GetString();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new CarrierPreferredLanesHandler(this).Initialize();

            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            btnSave.Enabled = Access.Modify;

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            aceVendor.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : OperationsSearchFields.CarrierPreferredLanes.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.CarrierPreferredLanesImportTemplate });
        }


        protected void OnSaveClick(object sender, EventArgs e)
        {
            var vendorPreferredLaneId = hidEditCarrierPreferredLaneId.Value.ToLong();

            var vendorPreferredLane = new VendorPreferredLane(vendorPreferredLaneId, vendorPreferredLaneId != default(long))
            {
                VendorId = hidEditVendorId.Value.ToLong(),
                OriginCity = txtEditOriginCity.Text,
                OriginState = txtEditOriginState.Text,
                OriginPostalCode = txtEditOriginPostalCode.Text,
                OriginCountryId = ddlEditOriginCountries.SelectedValue.ToLong(),
                DestinationCity = txtEditDestCity.Text,
                DestinationState = txtEditDestState.Text,
                DestinationPostalCode = txtEditDestPostalCode.Text,
                DestinationCountryId = ddlEditDestCountries.SelectedValue.ToLong()
            };

            var isNew = vendorPreferredLane.IsNew;
            if (isNew) vendorPreferredLane.TenantId = ActiveUser.TenantId;

            if (Save != null)
                Save(this, new ViewEventArgs<VendorPreferredLane>(vendorPreferredLane));

            if (!isNew) DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnNewClick(object sender, EventArgs e)
        {
            GeneratePopup("Add Carrier Preferred Lane");
            LoadCarrierPreferredLaneForEdit(new VendorPreferredLane
            {
                OriginCountryId = ActiveUser.Tenant.DefaultCountryId,
                DestinationCountryId = ActiveUser.Tenant.DefaultCountryId
            });
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            GeneratePopup("Modify Carrier Preferred Lane");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidCarrierPrefferedLaneId").ToCustomHiddenField();

            var vendorPreferredLane = new VendorPreferredLane(hidden.Value.ToLong(), false);

            LoadCarrierPreferredLaneForEdit(vendorPreferredLane);

            if (Lock != null)
                Lock(this, new ViewEventArgs<VendorPreferredLane>(vendorPreferredLane));
        }

        protected void OnDeleteClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidCarrierPrefferedLaneId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<VendorPreferredLane>(new VendorPreferredLane(hidden.Value.ToLong(), false)));

            DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnCloseClicked(object sender, EventArgs eventArgs)
        {
            CloseEditPopup();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearch(GetCurrentRunParameters(false));
        }


        protected void OnVendorNumberEntered(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtVendorNumber.Text))
            {
                DisplayVendor(null);
                return;
            }

            if (VendorSearch != null)
                VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
        {
            vendorFinder.Visible = true;
        }

        protected void OnVendorFinderSelectionCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
        {
            DisplayVendor(e.Argument);
            vendorFinder.Visible = false;
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "CARRIER PREFERRED LANES IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 9;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countries = ProcessorVars.RegistryCache.Countries.Values;
            var countryCodes = countries.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(4, countryCodes, new Country().EntityName()));
            msgs.AddRange(chks.CodesAreValid(8, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var postalCodeSearch = new PostalCodeSearch();
            var postalCodes = new List<PostalCodeViewSearchDto>();
            msgs.AddRange(chks
                .Where(i => !string.IsNullOrEmpty(i.Line[3]))
                .Where(i =>
                {
                    var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[3], countries.First(c => c.Code == i.Line[4]).Id);
                    postalCodes.Add(code);
                    return code.Id == default(long);
                })
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList());
            msgs.AddRange(chks
                .Where(i => !string.IsNullOrEmpty(i.Line[7]))
                .Where(i =>
                {
                    var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[7], countries.First(c => c.Code == i.Line[8]).Id);
                    postalCodes.Add(code);
                    return code.Id == default(long);
                })
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var vendorSearch = new VendorSearch();
            var vParameters = chks
                .Select(i => i.Line[0])
                .Distinct()
                .Select(n =>
                {
                    var p = AccountingSearchFields.VendorNumber.ToParameterColumn();
                    p.DefaultValue = n;
                    p.Operator = Operator.Equal;
                    return p;
                })
                .ToList();
            var vendorList = vendorSearch.FetchVendors(vParameters, ActiveUser.TenantId);
            var vendorNumbers = vendorList.Select(c => c.VendorNumber).ToList();
            msgs.AddRange(chks.CodesAreValid(0, vendorNumbers, new Vendor().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var vendorPreferredLanes = lines
                .Select(s =>
                {
                    var originPostalCode = !string.IsNullOrEmpty(s[3]) ? postalCodes.First(pc => pc.Code.ToLower() == s[3].ToLower()).Code : string.Empty;
                    var destPostalCode = !string.IsNullOrEmpty(s[7]) ? postalCodes.First(pc => pc.Code.ToLower() == s[7].ToLower()).Code : string.Empty;
                    return new VendorPreferredLane
                    {
                        VendorId = vendorList.First(v => v.VendorNumber == s[0]).Id,
                        OriginCity = s[1],
                        OriginState = s[2],
                        OriginPostalCode = originPostalCode,
                        OriginCountryId = countries.First(c => c.Code == s[4]).Id,
                        DestinationCity = s[5],
                        DestinationState = s[6],
                        DestinationPostalCode = destPostalCode,
                        DestinationCountryId = countries.First(c => c.Code == s[8]).Id,
                        TenantId = ActiveUser.TenantId
                    };
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<VendorPreferredLane>>(vendorPreferredLanes));
            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.CarrierPreferredLanes.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.CarrierPreferredLanes);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
    }
}
