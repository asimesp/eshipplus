﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
	[Serializable]
	[Entity("OperationsViewNoteObj", ReadOnly = true, Source = EntitySource.TableView)]
	public class OperationsViewNoteObj : Note
	{
		public long Id { get; set; }
		public string Username { get; set; }
		public bool IsLoadOrderNote { get; set; }

		public OperationsViewNoteObj(ShipmentNote note)
			: this((Note)note)
		{
			Copy((Note) note);
			Id = note.Id;
			IsLoadOrderNote = false;
		}

		public OperationsViewNoteObj(LoadOrderNote note) : this((Note)note)
		{
			Copy((Note) note);
			Id = note.Id;
			IsLoadOrderNote = true;
		}

		public OperationsViewNoteObj(Note note)
		{
			Copy(note);
			Username = note.User == null ? string.Empty : note.User.Username;
		}

		public OperationsViewNoteObj()
		{
		}
	}
}