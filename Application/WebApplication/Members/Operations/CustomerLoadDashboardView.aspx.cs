﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations.Controls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class CustomerLoadDashboardView : MemberPageBase, ICustomerLoadDashboardView
    {
        private SearchField SortByField
        {
            get { return OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        private const string VoidArgs = "Void";
        private const string RebookArgs = "Rebook";

        public static string PageAddress { get { return "~/Members/Operations/CustomerLoadDashboardView.aspx"; } }
        public override ViewCode PageCode { get { return ViewCode.CustomerLoadDashboard; } }
        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; } }

        public event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<Shipment>> VoidShipment;

        public void ProcessVoidComplete(Shipment shipment)
        {
            this.NotifyCancelledShipment(shipment);
            DisplayMessages(new[] { ValidationMessage.Information("Shipment Cancel Request Complete!") });
        }

        public void DisplaySearchResult(List<ShipmentDashboardDto> shipments)
        {
            litRecordCount.Text = shipments.BuildRecordCount();
            upcseDataUpdate.DataSource = shipments;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new ShipmentViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(ShipmentViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<ShipmentViewSearchCriteria>(criteria));
        }


        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var cancel = new ToolbarMoreAction
            {
                CommandArgs = VoidArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                Name = "Request Shipment Cancellation",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var rebook = new ToolbarMoreAction
            {
                CommandArgs = RebookArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                Name = "Rebook Shipment",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var actions = new List<ToolbarMoreAction> { cancel };

            if (ActiveUser.HasAccessTo(ViewCode.RateAndSchedule)) actions.Add(rebook);

            return actions;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new CustomerLoadDashboardHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

            if (IsPostBack) return;

            // set auto referesh
            tmRefresh.Interval = WebApplicationConstants.DefaultRefreshInterval.ToInt();

            // set sort fields
            ddlSortBy.DataSource = OperationsSearchFields.ShipmentSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = OperationsSearchFields.ShipmentNumber.Name;

            lbtnSortShipmentNumber.CommandName = OperationsSearchFields.ShipmentNumber.Name;
            lbtnSortDateCreated.CommandName = OperationsSearchFields.DateCreated.Name;
            lbtnSortDesiredPickup.CommandName = OperationsSearchFields.DesiredPickupDate.Name;
            lbtnSortActualPickup.CommandName = OperationsSearchFields.ActualPickupDate.Name;
            lbtnSortEstimatedDelivery.CommandName = OperationsSearchFields.EstimatedDeliveryDate.Name;
            lbtnSortActualDelivery.CommandName = OperationsSearchFields.ActualDeliveryDate.Name;
            lbtnSortServiceMode.CommandName = OperationsSearchFields.ServiceMode.Name;
            lbtnSortStatus.CommandName = OperationsSearchFields.Status.Name;


            //get criteria from Session
            var searchDefault = Session[WebApplicationConstants.ShipmentDashboardSearchCriteria] as SearchDefaultsItem;
            var storedCriteria = searchDefault == null
                                    ? null
                                    : new ShipmentViewSearchCriteria
                                    {
                                        ActiveUserId = ActiveUser.Id,
                                        Parameters = searchDefault.Columns,
                                        SortAscending = searchDefault.SortAscending,
                                        SortBy = searchDefault.SortBy
                                    };


            /*This has not been moved to the Operations Search Fields defaults for the following reasons:  
             *      - It is only used here in this page, and only once
             *      - To move it to the search defaults and then assign values we would need to check
             *      if the correct parameters were in the defaults (ie. the defaults can be changed with 
             *      the developer never knowing this method expects them and tries to assign default values here.
             */
            var defaultP = new List<ParameterColumn>
			               	{
			               		OperationsSearchFields.ShipmentNumber.ToParameterColumn(),
			               		OperationsSearchFields.VendorProNumber.ToParameterColumn(),
			               		OperationsSearchFields.PurchaseOrderNumber.ToParameterColumn(),
			               		OperationsSearchFields.ShipperReference.ToParameterColumn(),

			               	};

            var dc = OperationsSearchFields.DateCreated.ToParameterColumn();
            dc.DefaultValue = DateTime.Now.AddDays(-10).ToString();
            dc.DefaultValue2 = DateTime.Now.AddDays(1).TimeToMinimum().ToString();
            defaultP.Add(dc);

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet();
            var defaultCriteria = new ShipmentViewSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = profile != null && profile.Columns != null
                                ? profile.Columns
                                : defaultP,
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };


            var criteria = storedCriteria ?? defaultCriteria;
            criteria.DisableNoRecordNotification = true;

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;

            if (storedCriteria == null) searchProfiles.SetSelectedProfile(profile);

            if (!ActiveUser.TenantEmployee) DoSearch(criteria);

            Session[WebApplicationConstants.ShipmentDashboardSearchCriteria] = null;
        }


        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            Response.Redirect(RateAndScheduleView.PageAddress);
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case VoidArgs:
                    pnlDimScreen.Visible = true;
                    pnlRequestShipentCancellation.Visible = true;
                    break;
                case RebookArgs:
                    pnlDimScreen.Visible = true;
                    pnlRebookShipment.Visible = true;
                    break;
            }
        }

        protected void OnShipmentDetailsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var sh = (ShipmentDashboardDto)item.DataItem;

            if (sh == null) return;

            var detailControl = (CustomerLoadDashboardDetailControl)item.FindControl("customerLoadDashboardDetailControl");
            detailControl.LoadShipment(sh, sh.CheckCalls);
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (hidFlag.Value == VoidArgs)
            {
                hidFlag.Value = string.Empty;
                litBillofLading.Text = string.Empty;
                btnVoidShipment.Visible = false;
                pnlDimScreen.Visible = false;
                pnlRequestShipentCancellation.Visible = false;
                DoSearchPreProcessingThenSearch(SortByField);
            }
        }


        protected void OnShipmentCancellationRequestCancelClicked(object sender, EventArgs e)
        {
            pnlDimScreen.Visible = false;
            litBillofLading.Text = string.Empty;
            pnlRequestShipentCancellation.Visible = false;
        }

        protected void OnShipmentRequestCancellationContinueClicked(object sender, EventArgs e)
        {
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(txtVoidShipmentNumber.Text, ActiveUser.TenantId);

            if (shipment != null && !ActiveUser.HasAccessToCustomer(shipment.Customer.Id)) shipment = null;

            if (shipment == null)
            {
                btnVoidShipment.Visible = false;
                litBillofLading.Text = string.Empty;
                DisplayMessages(new[] { ValidationMessage.Error("Requested shipment is not found.") });
                return;
            }

            if (shipment.Status != ShipmentStatus.Scheduled)
            {
                btnVoidShipment.Visible = false;
                litBillofLading.Text = string.Empty;
                DisplayMessages(new[] { ValidationMessage.Error("Requested shipment cannot be cancelled because it is in '{0}' status.", shipment.Status.ToString()) });
                return;
            }

            btnVoidShipment.Visible = true;
            litBillofLading.Text = string.Format("<hr class=\"w100p ml-inherit dark dotted mb0\" /> {0}", this.GenerateBOL(shipment, true));
        }

        protected void OnShipmentRequestCancellationVoidShipmentClicked(object sender, EventArgs e)
        {
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(txtVoidShipmentNumber.Text, ActiveUser.TenantId);

            hidFlag.Value = VoidArgs;

            if (VoidShipment != null)
                VoidShipment(this, new ViewEventArgs<Shipment>(shipment));
        }



        protected void OnRebookShipmentClicked(object sender, EventArgs e)
        {
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(txtRebookShipmentNumber.Text, ActiveUser.TenantId);

            if (shipment != null && !ActiveUser.HasAccessToCustomer(shipment.Customer.Id)) shipment = null;

            if (shipment == null)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Requested shipment is not found.") });
                return;
            }

            Session[WebApplicationConstants.CustomerLoadDashboardShipmentTransfer] = shipment;
            Response.Redirect(RateAndScheduleView.PageAddress);
        }

        protected void OnCancelRebookShipmentClicked(object sender, EventArgs e)
        {
            pnlDimScreen.Visible = false;
            pnlRebookShipment.Visible = false;
        }


        protected void OnShippingLabelGeneratorCloseClicked(object sender, EventArgs e)
        {
            shippingLabelGenerator.Visible = false;
        }

        protected void OnGenerateShippingLabel(object sender, ViewEventArgs<DetailRecordInfo> e)
        {
            shippingLabelGenerator.GenerateLabels(e.Argument.RecordId);
            shippingLabelGenerator.Visible = true;
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.CustomerLoads.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.CustomerLoads);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }



        protected void OnTimerTick(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnCloseRebookShipmentClicked(object sender, ImageClickEventArgs e)
        {
            pnlRebookShipment.Visible = false;
            pnlDimScreen.Visible = false;

        }

        protected void OnCloseShipmentCancellationClicked(object sender, ImageClickEventArgs e)
        {
            pnlRequestShipentCancellation.Visible = false;
            pnlDimScreen.Visible = false;
        }
    }
}