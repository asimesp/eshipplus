﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class ClaimView : MemberPageBaseWithPageStore, IClaimView
    {
        private const string VendorsHeader = "Vendors";
        private const string DocumentsHeader = "Documents";
        private const string NotesHeader = "Notes";
        private const string ShipmentsHeader = "Shipments";
        private const string ServiceTicketsHeader = "Service Tickets";


        private const string CancelArgs = "Cancel";
        private const string CloseResolvedArgs = "Resolved";
        private const string CloseUnresolvedArgs = "Unresolved";
        private const string ReturnArgs = "Return";
        private const string DuplicateArgs = "Duplicate";
        private const string SaveFlag = "S";

        private const string ClaimNoteStoreKey = "CNSK";

        private bool _moveFilesFromTempLocation;

        private List<ClaimNote> StoredClaimNotes
        {
            get { return PageStore[ClaimNoteStoreKey] as List<ClaimNote> ?? new List<ClaimNote>(); }
        }

        public static string PageAddress { get { return "~/Members/Operations/ClaimView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.Claim; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
        }

        public Dictionary<int, string> NoteTypes
        {
            set
            {
                var types = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                types.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, string.Empty));

                ddlType.DataSource = types;
                ddlType.DataBind();
            }
        }

        public Dictionary<int, string> ClaimTypes
        {
            set
            {
                ddlClaimType.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                ddlClaimType.DataBind();
            }
        }

        public Dictionary<int, string> AmountClaimedTypes
        {
            set
            {
                ddlAmountClaimedType.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                ddlAmountClaimedType.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Claim>> Save;
        public event EventHandler<ViewEventArgs<Claim>> Delete;
        public event EventHandler<ViewEventArgs<Claim>> Lock;
        public event EventHandler<ViewEventArgs<Claim>> UnLock;
        public event EventHandler<ViewEventArgs<Claim>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;

        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // clean up file cleared out!
                if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
                {
                    Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                    hidFilesToDelete.Value = string.Empty;
                }

                var claim = new Claim(hidClaimId.Value.ToLong(), false);

                // check for file moves
                // variable set in save method for new files load when record is new record.
                if (_moveFilesFromTempLocation)
                {
                    _moveFilesFromTempLocation = false; // must set to false first to avoid processing load with next save from ProcessFile... method.
                    claim.LoadCollections(); // require all connections reloaded
                    ProcessFileMoveFromTempLocation(claim);
                    return;
                }

                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Claim>(new Claim(hidClaimId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(Claim claim)
        {
            LoadClaim(claim);
            SetEditStatus(!claim.IsNew && claim.ShouldEnable());
        }


        private List<ToolbarMoreAction> ToolbarMoreActions(bool enabled)
        {
            var brDashboard = new ToolbarMoreAction
            {
                CommandArgs = ReturnArgs,
                ImageUrl = IconLinks.Operations,
                Name = ViewCode.ClaimDashboard.FormattedString(),
                IsLink = false,
            };

            var duplicate = new ToolbarMoreAction
            {
                CommandArgs = DuplicateArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                IsLink = false,
                Name = "Duplicate Claim",
                NavigationUrl = PageAddress
            };

            var cancel = new ToolbarMoreAction
            {
                CommandArgs = CancelArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Cancel Claim",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var closeResolved = new ToolbarMoreAction
            {
                CommandArgs = CloseResolvedArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Close Claim - Resolved",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var closeUnresolved = new ToolbarMoreAction
            {
                CommandArgs = CloseUnresolvedArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Operations,
                Name = "Close Claim - Unresolved",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var actions = new List<ToolbarMoreAction>();

            if (Access.Modify && hidClaimId.Value.ToLong() != default(long))
            {
                actions.Add(duplicate);
                if (enabled) actions.Add(cancel);
                if (enabled) actions.Add(closeResolved);
                if (enabled) actions.Add(closeUnresolved);
            }

            if (actions.Any()) actions.Add(new ToolbarMoreAction { IsSeperator = true });

            actions.Add(brDashboard);

            return actions;
        }

        private void CancelClaim()
        {
            var claim = new Claim(hidClaimId.Value.ToLong());
            if (claim.IsNew)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Unable to cancel new claim that has not been previously saved.") });
                return;
            }
            claim.LoadCollections();
            claim.TakeSnapShot();
            claim.Status = ClaimStatus.Cancelled;
            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Claim>(claim));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Claim>(claim));
        }

        private void DuplicateClaim()
        {
            var claim = UpdateClaim().Duplicate();
            LoadClaim(claim);

            SetEditStatus(true);

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
        }

        private void CloseUnresolvedClaim()
        {
            var claim = new Claim(hidClaimId.Value.ToLong());
            if (claim.IsNew)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Unable to close new claim that has not been previously saved.") });
                return;
            }
            claim.LoadCollections();
            claim.TakeSnapShot();
            claim.Status = ClaimStatus.ClosedUnresolved;
            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Claim>(claim));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Claim>(claim));
        }

        private void CloseResolvedClaim()
        {
            var claim = new Claim(hidClaimId.Value.ToLong());
            if (claim.IsNew)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Unable to close new claim that has not been previously saved.") });
                return;
            }
            claim.LoadCollections();
            claim.TakeSnapShot();
            claim.Status = ClaimStatus.ClosedResolved;
            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Claim>(claim));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Claim>(claim));
        }

        private void ReturnToClaimsDashboard()
        {
            var oldClaim = new Claim(hidClaimId.Value.ToLong(), false);

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Claim>(oldClaim));

            Response.Redirect(ClaimDashboardView.PageAddress);
        }


        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlVendors.Enabled = enabled;
            pnlDocuments.Enabled = enabled;
            pnlNotes.Enabled = enabled;
            pnlServiceTickets.Enabled = enabled;
            pnlShipments.Enabled = enabled;

            memberToolBar.EnableSave = enabled;
            memberToolBar.EnableImport = enabled;
            hidEditing.Value = enabled.ToString();
            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

            // must do this here to load correct set of extensible actions
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(enabled));
        }


        private Claim UpdateClaim()
        {
            var claimId = hidClaimId.Value.ToLong();
            var claim = new Claim(claimId, claimId != default(long));

            if (claim.IsNew)
            {
                claim.TenantId = ActiveUser.TenantId;
                claim.User = ActiveUser;
                claim.DateCreated = DateTime.Now;
            }
            else
            {
                claim.LoadCollections();
            }

            claim.ClaimDate = txtClaimDate.Text.ToDateTime();
            claim.Customer = new Customer(hidCustomerId.Value.ToLong());

            txtStatus.Text = claim.Status.FormattedString();
            claim.ClaimNumber = txtClaimNumber.Text;
            claim.ClaimDetail = txtClaimDetail.Text;

            claim.ClaimantReferenceNumber = txtClaimantReferenceNumber.Text;
            claim.ClaimType = ddlClaimType.SelectedValue.ToInt().ToEnum<ClaimType>();

            claim.IsRepairable = chkIsRepairable.Checked;
            claim.EstimatedRepairCost = txtEstimatedRepairCost.Text.ToDecimal();

            claim.AmountClaimed = txtAmountClaimed.Text.ToDecimal();
            claim.AmountClaimedType = ddlAmountClaimedType.SelectedValue.ToInt().ToEnum<AmountClaimedType>();

            claim.DateLpAcctToPayCarrier = txtDateLpAcctToPayCarrier.Text.ToDateTime();
            claim.Acknowledged = txtAcknowledged.Text.ToDateTime();

            claim.CheckNumber = txtCheckNumber.Text;
            claim.CheckAmount = txtCheckAmount.Text.ToDecimal();

            //Update From View
            UpdateVendorsFromView(claim);
            UpdateDocumentsFromView(claim);
            UpdateNotesFromView(claim);
            UpdateShipmentsFromView(claim);
            UpdateServiceTicketsFromView(claim);

            return claim;
        }

        private void UpdateVendorsFromView(Claim claim)
        {
            var vendors = lstVendors
                .Items
                .Select(i => 
                {
                    var id = i.FindControl("hidClaimVendorId").ToCustomHiddenField().Value.ToLong();
                    return new ClaimVendor(id, id != default(long))
                        {
                            VendorId = (i.FindControl("hidVendorId")).ToCustomHiddenField().Value.ToLong(),
                            Claim = claim,
                            ProNumber = (i.FindControl("txtProNumber")).ToTextBox().Text,
                            FreightBillNumber = (i.FindControl("txtFreightBillNumber")).ToTextBox().Text,
                            TenantId = claim.TenantId
                        };
                })
                .ToList();

            claim.Vendors = vendors;
        }

        private void UpdateDocumentsFromView(Claim claim)
        {
            var documents = lstDocuments.Items
                .Select(i =>
                {
                    var id = (i.FindControl("hidDocumentId")).ToCustomHiddenField().Value.ToLong();
                    return new ClaimDocument(id, id != default(long))
                    {
                        DocumentTagId = (i.FindControl("hidDocumentTagId")).ToCustomHiddenField().Value.ToLong(),
                        Name = (i.FindControl("litDocumentName")).ToLiteral().Text,
                        Description = (i.FindControl("litDocumentDescription")).ToLiteral().Text,
                        LocationPath = (i.FindControl("hidLocationPath")).ToCustomHiddenField().Value,
                        Claim = claim,
                        TenantId = claim.TenantId
                    };
                })
                .ToList();

            claim.Documents = documents;
        }

        private void UpdateNotesFromView(Claim claim)
        {
            var notes = StoredClaimNotes;
            if (!ActiveUser.HasAccessTo(ViewCode.CanSeeArchivedNotes)) // account for not listed archived notes.
                notes.AddRange(claim.Notes.Where(n => n.Archived));

            claim.Notes = notes;
            if (claim.IsNew)
                foreach (var psNote in claim.Notes)
                {
                    psNote.TenantId = claim.TenantId;
                    psNote.User = claim.User;
                    psNote.Claim = claim;
                }
        }

        private void UpdateShipmentsFromView(Claim claim)
        {
            var shipments = lstShipments.Items
                .Select(i => new ClaimShipment
                {
                    ShipmentId = (i.FindControl("hidShipmentId")).ToCustomHiddenField().Value.ToLong(),
                    Claim = claim,
                    TenantId = claim.TenantId,
                })
                .ToList();

            claim.Shipments = shipments;
        }

        private void UpdateServiceTicketsFromView(Claim claim)
        {
            var shipments = lstServiceTickets.Items
                .Select(i => new ClaimServiceTicket
                {
                    ServiceTicketId = (i.FindControl("hidServiceTicketId")).ToCustomHiddenField().Value.ToLong(),
                    Claim = claim,
                    TenantId = claim.TenantId,
                })
                .ToList();

            claim.ServiceTickets = shipments;
        }


        private void LoadClaim(Claim claim)
        {
            if (!claim.IsNew) claim.LoadCollections();

            hidClaimId.Value = claim.Id.ToString();

            //Above Details
            txtClaimNumber.Text = claim.ClaimNumber;
			SetPageTitle(txtClaimNumber.Text);
            DisplayCustomer(claim.Customer);
            txtStatus.Text = claim.Status.FormattedString();

            //Details
            txtDateCreated.Text = claim.DateCreated.ToShortDateString();
            txtClaimDate.Text = claim.IsNew ? String.Empty : claim.ClaimDate.FormattedShortDateSuppressEarliestDate();
            txtUser.Text = claim.User != null ? claim.User.Username : ActiveUser.Username;
            txtClaimDetail.Text = claim.ClaimDetail;

            txtDateLpAcctToPayCarrier.Text = claim.IsNew ? String.Empty : claim.DateLpAcctToPayCarrier.FormattedShortDateSuppressEarliestDate();
            txtAcknowledged.Text = claim.IsNew ? String.Empty : claim.Acknowledged.FormattedShortDateSuppressEarliestDate();
            txtCheckNumber.Text = claim.CheckNumber;
            txtCheckAmount.Text = claim.CheckAmount.GetString();

            txtAmountClaimed.Text = claim.AmountClaimed.GetString();
            ddlAmountClaimedType.SelectedValue = claim.AmountClaimedType.ToInt().ToString();
            ddlClaimType.SelectedValue = claim.ClaimType.ToInt().ToString();
            chkIsRepairable.Checked = claim.IsRepairable;
            txtEstimatedRepairCost.Text = claim.EstimatedRepairCost.GetString();
            txtClaimantReferenceNumber.Text = claim.ClaimantReferenceNumber;

            //Shipments
            var shipments = claim.Shipments
                .Select(s => new
                {
                    s.Shipment.Id,
                    s.Shipment.ShipmentNumber,
                    s.Shipment.DateCreated,
                    s.Shipment.Customer.CustomerNumber,
                    CustomerName = s.Shipment.Customer.Name,
                    s.Shipment.User.Username
                })
                .ToList();

            lstShipments.DataSource = shipments;
            lstShipments.DataBind();
            tabShipments.HeaderText = shipments.BuildTabCount(ShipmentsHeader);

            //Service Tickets
            var serviceTickets = claim.ServiceTickets
                .Select(s => new
                {
                    s.ServiceTicket.Id,
                    s.ServiceTicket.ServiceTicketNumber,
                    s.ServiceTicket.DateCreated,
                    s.ServiceTicket.Customer.CustomerNumber,
                    CustomerName = s.ServiceTicket.Customer.Name,
                    s.ServiceTicket.User.Username
                })
                .ToList();

            lstServiceTickets.DataSource = serviceTickets;
            lstServiceTickets.DataBind();
            tabServiceTickets.HeaderText = serviceTickets.BuildTabCount(ServiceTicketsHeader);

            //Vendors
            var vendors = claim.Vendors
                .Select(claimVendor => new
                {
                    claimVendor.Id,
                    claimVendor.Vendor.VendorNumber,
                    claimVendor.Vendor.Name,
                    claimVendor.VendorId,
                    claimVendor.ProNumber,
                    claimVendor.FreightBillNumber,
                    InsuranceAlert = claimVendor.Vendor.AlertVendorInsurance(ProcessorVars.VendorInsuranceAlertThresholdDays[claimVendor.TenantId]),
                    InsuranceTooltip = claimVendor.Vendor.VendorInsuranceAlertText(ProcessorVars.VendorInsuranceAlertThresholdDays[claimVendor.TenantId])
                })
                .ToList();

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);

            //Documents
            lstDocuments.DataSource = claim.Documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = claim.Documents.BuildTabCount(DocumentsHeader);

            //Notes
            var notes = ActiveUser.HasAccessTo(ViewCode.CanSeeArchivedNotes)
                                    ? claim.Notes
                                    : claim.Notes.Where(n => !n.Archived).ToList();
            peLstNotes.LoadData(notes);
            tabNotes.HeaderText = notes.BuildTabCount(NotesHeader);

            memberToolBar.ShowUnlock = claim.HasUserLock(ActiveUser, claim.Id);
        }

        private void ProcessFileMoveFromTempLocation(Claim claim)
        {
            var claimFolder = WebApplicationSettings.ClaimFolder(claim.TenantId, claim.Id);
            if (claim.Documents.Any() && !Directory.Exists(Server.MapPath(claimFolder)))
                Directory.CreateDirectory(Server.MapPath(claimFolder));
            foreach (var doc in claim.Documents)
                if (!string.IsNullOrEmpty(doc.LocationPath))
                {
                    var virtualOldPath = doc.LocationPath;
                    var absOldPath = Server.MapPath(virtualOldPath);
                    var oldFileInfo = new FileInfo(absOldPath);
                    var virtualNewPath = claimFolder + oldFileInfo.Name;
                    var absNewPath = Server.MapPath(virtualNewPath);

                    if (absOldPath == absNewPath) continue;

                    File.Copy(absOldPath, absNewPath);

                    doc.TakeSnapShot();
                    doc.LocationPath = virtualNewPath;
                    hidFilesToDelete.Value += string.Format("{0};", virtualOldPath);
                }

            if (Lock != null)
                Lock(this, new ViewEventArgs<Claim>(claim));

            if (Save != null)
                Save(this, new ViewEventArgs<Claim>(claim));
        }

        private void ProcessTransferredRequest(Claim claim, bool disableEdit = false)
        {
            if (claim == null) return;

            if (!ActiveUser.HasAccessToCustomer(claim.Customer.Id)) return;

            LoadClaim(claim);

            if (Lock != null && !claim.IsNew && Access.Modify && !disableEdit)
            {
                SetEditStatus(claim.ShouldEnable());
                Lock(this, new ViewEventArgs<Claim>(claim));
            }
            else if (claim.IsNew) SetEditStatus(claim.ShouldEnable());
            else SetEditStatus(false);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Claim>(claim));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            new ClaimHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditing.Value.ToBoolean()));

			SetPageTitle(txtClaimNumber.Text);

            // setup pager
            peLstNotes.UseParentDataStore = true;
            peLstNotes.ParentDataStoreKey = ClaimNoteStoreKey;

            if (IsPostBack) return;

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            if (Loading != null)
                Loading(this, new EventArgs());

            SetEditStatus(false);

            if (Session[WebApplicationConstants.TransferClaimId] != null)
            {
                var disableEdit = Session[WebApplicationConstants.DisableEditModeOnTransfer].ToBoolean();

                ProcessTransferredRequest(new Claim(Session[WebApplicationConstants.TransferClaimId].ToLong()), disableEdit);
                Session[WebApplicationConstants.TransferClaimId] = null;
                Session[WebApplicationConstants.DisableEditModeOnTransfer] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new ClaimSearch().FetchClaimByClaimNumber(Request.QueryString[WebApplicationConstants.TransferNumber].GetString(), ActiveUser.TenantId), true);
        }


        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            claimFinder.Visible = true;
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var claim = UpdateClaim();

            hidFlag.Value = SaveFlag;

            _moveFilesFromTempLocation = claim.IsNew && claim.Documents.Any();

            if (Save != null)
                Save(this, new ViewEventArgs<Claim>(claim));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Claim>(claim));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var claim = new Claim(hidClaimId.Value.ToLong(), false);

            if (Delete != null)
                Delete(this, new ViewEventArgs<Claim>(claim));

            claimFinder.Reset();
        }

        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            var claim = new Claim
            {
                Customer = new Customer(),
                DateCreated = DateTime.Now,
                ClaimDate = DateTime.Now,
                Status = ClaimStatus.Open,
            };

            LoadClaim(claim);

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);

            litErrorMessages.Text = string.Empty;
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var claim = new Claim(hidClaimId.Value.ToLong(), false);

            if (Lock == null || claim.IsNew) return;

            SetEditStatus(claim.ShouldEnable());
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<Claim>(claim));

            LoadClaim(claim);
        }

        protected void OnToolbarUnlockClicked(object sender, EventArgs e)
        {
            var claim = new Claim(hidClaimId.Value.ToLong(), false);
            if (UnLock != null && !claim.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<Claim>(claim));
            }
        }

        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case CancelArgs:
                    CancelClaim();
                    break;
                case DuplicateArgs:
                    DuplicateClaim();
                    break;
                case ReturnArgs:
                    ReturnToClaimsDashboard();
                    break;
                case CloseResolvedArgs:
                    CloseResolvedClaim();
                    break;
                case CloseUnresolvedArgs:
                    CloseUnresolvedClaim();
                    break;
            }
        }

        protected void OnClaimFinderItemSelected(object sender, ViewEventArgs<Claim> e)
        {
            if (hidClaimId.Value.ToLong() != default(long))
            {
                var oldClaim = new Claim(hidClaimId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Claim>(oldClaim));
            }

            var claim = e.Argument;

            LoadClaim(claim);

            claimFinder.Visible = false;

            if (claimFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(claim.ShouldEnable());
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<Claim>(claim));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Claim>(claim));

            litErrorMessages.Text = string.Empty;
        }

        protected void OnClaimFinderItemCancelled(object sender, EventArgs e)
        {
            claimFinder.Visible = false;
        }


        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }


        protected void OnAddVendorClicked(object sender, EventArgs e)
        {
            vendorFinder.Reset();
            vendorFinder.Visible = true;
        }

        protected void OnDeleteVendorClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var vendors = lstVendors.Items
                .Select(c => new
                {
                    Id = (c.FindControl("hidClaimVendorId")).ToCustomHiddenField().Value.ToLong(),
                    VendorNumber = (c.FindControl("lblVendorNumber")).ToLabel().Text,
                    Name = (c.FindControl("litName")).ToLiteral().Text,
                    ProNumber = (c.FindControl("txtProNumber")).ToTextBox().Text,
                    FreightBillNumber = (c.FindControl("txtFreightBillNumber")).ToTextBox().Text,
                    VendorId = (c.FindControl("hidVendorId")).ToCustomHiddenField().Value.ToLong(),
                    InsuranceAlert = (c.FindControl("hidInsuranceAlert")).ToCustomHiddenField().Value.ToBoolean(),
                    InsuranceTooltip = (c.FindControl("hidInsuranceTooltip")).ToCustomHiddenField().Value,
                })
                .ToList();

            vendors.RemoveAt((imageButton.Parent.FindControl("hidVendorIndex")).ToCustomHiddenField().Value.ToInt());

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);
            athtuTabUpdater.SetForUpdate(tabVendors.ClientID, vendors.BuildTabCount(VendorsHeader));
        }

        protected void OnVendorFinderMultiItemSelected(object sender, ViewEventArgs<List<Vendor>> e)
        {
            var vendors = lstVendors.Items
                .Select(c => new
                {
                    Id = (c.FindControl("hidClaimVendorId")).ToCustomHiddenField().Value.ToLong(),
                    VendorNumber = (c.FindControl("lblVendorNumber")).ToLabel().Text,
                    Name = (c.FindControl("litName")).ToLiteral().Text,
                    VendorId = (c.FindControl("hidVendorId")).ToCustomHiddenField().Value.ToLong(),
                    ProNumber = (c.FindControl("txtProNumber")).ToTextBox().Text,
                    FreightBillNumber = (c.FindControl("txtFreightBillNumber")).ToTextBox().Text,
                    InsuranceAlert = (c.FindControl("hidInsuranceAlert")).ToCustomHiddenField().Value.ToBoolean(),
                    InsuranceTooltip = (c.FindControl("hidInsuranceTooltip")).ToCustomHiddenField().Value,
                })
                .ToList();

            var newVendors = e.Argument
                .Select(vendor => new
                {
                    Id = default(long),
                    vendor.VendorNumber,
                    vendor.Name,
                    VendorId = vendor.Id,
                    ProNumber = string.Empty,
                    FreightBillNumber = string.Empty,
                    InsuranceAlert = false,
                    InsuranceTooltip = string.Empty
                })
                .Where(v => !vendors.Select(qv => qv.VendorId).Contains(v.VendorId));

            vendors.AddRange(newVendors);

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();
            tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderItemCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }

        protected void OnClearVendorsClicked(object sender, EventArgs e)
        {
            lstVendors.DataSource = new List<Vendor>();
            lstVendors.DataBind();
            tabVendors.HeaderText = VendorsHeader;
            athtuTabUpdater.SetForUpdate(tabVendors.ClientID, VendorsHeader);
        }


        private void LoadDocumentForEdit(Document document)
        {
            txtDocumentName.Text = document.Name;
            txtDocumentDescription.Text = document.Description;
            ddlDocumentTag.SelectedValue = document.DocumentTagId.GetString();
            hidDocumentTagId.Value = document.DocumentTagId.ToString();
            litLocationPath.Text = GetLocationFileName(document.LocationPath);
            hidLocationPath.Value = document.LocationPath;
        }

        protected string GetLocationFileName(object relativePath)
        {
            return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
        }

        protected void OnAddDocumentClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadDocumentForEdit(new ClaimDocument());
            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnEditDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            hidEditingIndex.Value = (control.FindControl("hidDocumentIndex")).ToCustomHiddenField().Value;

            var document = new ClaimDocument
            {
                Name = (control.FindControl("litDocumentName")).ToLiteral().Text,
                Description = (control.FindControl("litDocumentDescription")).ToLiteral().Text,
                DocumentTagId = (control.FindControl("hidDocumentTagId")).ToCustomHiddenField().Value.ToLong(),
                LocationPath = (control.FindControl("hidLocationPath")).ToCustomHiddenField().Value
            };

            LoadDocumentForEdit(document);

            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnDeleteDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var documents = lstDocuments.Items
                .Select(i => new
                {
                    Id = (i.FindControl("hidDocumentId")).ToCustomHiddenField().Value.ToLong(),
                    Name = (i.FindControl("litDocumentName")).ToLiteral().Text,
                    Description = (i.FindControl("litDocumentDescription")).ToLiteral().Text,
                    DocumentTagId = (i.FindControl("hidDocumentTagId")).ToCustomHiddenField().Value,
                    LocationPath = (i.FindControl("hidLocationPath")).ToCustomHiddenField().Value,
                })
                .ToList();

            var index = imageButton.Parent.FindControl("hidDocumentIndex").ToCustomHiddenField().Value.ToInt();
            hidFilesToDelete.Value += string.Format("{0};", documents[index].LocationPath);
            documents.RemoveAt((imageButton.Parent.FindControl("hidDocumentIndex")).ToCustomHiddenField().Value.ToInt());

            lstDocuments.DataSource = documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);
        }

        protected void OnCloseEditDocumentClicked(object sender, EventArgs eventArgs)
        {
            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditDocumentDoneClicked(object sender, EventArgs e)
        {
            var virtualPath = WebApplicationSettings.ClaimFolder(ActiveUser.TenantId, hidClaimId.Value.ToLong());
            var physicalPath = Server.MapPath(virtualPath);

            var documents = lstDocuments.Items
                .Select(i => new
                {
                    Id = (i.FindControl("hidDocumentId")).ToCustomHiddenField().Value.ToLong(),
                    Name = (i.FindControl("litDocumentName")).ToLiteral().Text,
                    Description = (i.FindControl("litDocumentDescription")).ToLiteral().Text,
                    DocumentTagId = (i.FindControl("hidDocumentTagId")).ToCustomHiddenField().Value,
                    LocationPath = (i.FindControl("hidLocationPath")).ToCustomHiddenField().Value,
                })
                .ToList();

            var documentIndex = hidEditingIndex.Value.ToInt();

            var documentId = default(long);
            if (documentIndex != -1)
            {
                documentId = documents[documentIndex].Id;
                documents.RemoveAt(documentIndex);
            }

            var locationPath = hidLocationPath.Value; // this should actually be a hidden field holding the existing path during edit

            documents.Insert(0, new
            {
                Id = documentId,
                Name = txtDocumentName.Text,
                Description = txtDocumentDescription.Text,
                DocumentTagId = ddlDocumentTag.SelectedValue,
                LocationPath = locationPath
            });

            if (fupLocationPath.HasFile)
            {
                // ensure directory is present
                if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

                //ensure unique filename
                var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLocationPath.FileName), true));

                // save file
                fupLocationPath.WriteToFile(physicalPath, fi.Name, Server.MapPath(documents[0].LocationPath));

                // remove updated record
                documents.RemoveAt(0);

                // re-add record with new file
                documents.Insert(0, new
                {
                    Id = documentId,
                    Name = txtDocumentName.Text,
                    Description = txtDocumentDescription.Text,
                    DocumentTagId = ddlDocumentTag.SelectedValue,
                    LocationPath = virtualPath + fi.Name
                });
            }

            lstDocuments.DataSource = documents.OrderBy(d => d.Name);
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);

            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnClearDocumentsClicked(object sender, EventArgs e)
        {
            lstDocuments.DataSource = new List<Document>();
            lstDocuments.DataBind();
            tabDocuments.HeaderText = DocumentsHeader;
        }
        
        protected void OnLocationPathClicked(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
            Response.Export(Server.ReadFromFile(path), button.Text);
        }


        private void LoadNoteForEdit(ClaimNote note)
        {
            chkArchived.Checked = note.Archived;
            chkClassified.Checked = note.Classified;
            chkSendReminder.Checked = note.SendReminder;
            ddlType.SelectedValue = note.Type.ToInt().ToString();

            txtMessage.Text = note.Message;
            txtReminderEmails.Text = note.SendReminderEmails;

            txtReminderDate.Text = note.SendReminderOn == DateUtility.SystemEarliestDateTime
                                       ? string.Empty
                                       : note.SendReminderOn.FormattedShortDate();

            txtReminderTime.Text = note.SendReminderOn == DateUtility.SystemEarliestDateTime
                                       ? string.Empty
                                       : note.SendReminderOn.GetTime();

            if (!String.IsNullOrEmpty(note.Message))
            {
                txtMessage.ReadOnly = true;
                txtMessage.CssClass = "disabled w300 h150";
            }
            else
            {
                txtMessage.ReadOnly = false;
                txtMessage.CssClass = "w300 h150";
            }
        }

        protected void OnAddNoteClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadNoteForEdit(new ClaimNote
                {
                    Type = NoteType.Normal,
                    SendReminderOn = DateUtility.SystemEarliestDateTime
                });
            pnlEditNote.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnEditNoteClicked(object sender, ImageClickEventArgs e)
        {
            var control = (ImageButton)sender;

            var psClaimNotes = StoredClaimNotes;

            var pageIdx = peLstNotes.CurrentPage - 1;
            var itemIdx = control.FindControl("hidNoteIndex").ToCustomHiddenField().Value.ToInt();
            hidEditingIndex.Value = itemIdx.ToString();

            var psClaimNote = psClaimNotes[(pageIdx * peLstNotes.PageSize) + itemIdx];
            LoadNoteForEdit(psClaimNote);

            pnlEditNote.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnCloseEditNoteClicked(object sender, EventArgs e)
        {
            pnlEditNote.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditNoteDoneClicked(object sender, EventArgs e)
        {
            var psNotes = StoredClaimNotes;
            var claim = new Claim(hidClaimId.Value.ToLong());

            var pageIdx = peLstNotes.CurrentPage - 1;

            var editingIndex = (pageIdx * peLstNotes.PageSize) + hidEditingIndex.Value.ToInt();
            var isNew = hidEditingIndex.Value.ToInt() == WebApplicationConstants.InvalidIndex;

            var psNote = isNew
                             ? new ClaimNote()
                             : psNotes[editingIndex];

            if (!isNew && !psNote.HasChanges())
                psNote.TakeSnapShot();
 
            psNote.Type = ddlType.SelectedValue.ToInt().ToEnum<NoteType>();
            psNote.Archived = chkArchived.Checked;
            psNote.Classified = chkClassified.Checked;
            psNote.Message = txtMessage.Text;
            psNote.TenantId = claim.TenantId;
            psNote.SendReminderEmails = txtReminderEmails.Text.StripSpacesFromEmails();
            psNote.SendReminder = chkSendReminder.Checked;
            psNote.SendReminderOn = txtReminderDate.Text == string.Empty
                                        ? DateUtility.SystemEarliestDateTime
                                        : txtReminderDate.Text.ToDateTime();

            if (!string.IsNullOrEmpty(txtReminderTime.Text) && txtReminderTime.Text.IsValidFreeFormTimeString() &&
                psNote.SendReminderOn != DateUtility.SystemEarliestDateTime)
                psNote.SendReminderOn = psNote.SendReminderOn.SetTime(txtReminderTime.Text);
            psNote.Claim = claim;
            psNote.User = claim.User;
            if (isNew) psNote.DateCreated = DateTime.Now;
          

            if (isNew)
            {
                psNotes.Add(psNote);
            }
            else
            {
                psNotes[editingIndex] = psNote;
            }

            peLstNotes.LoadData(psNotes, peLstNotes.CurrentPage);
            tabNotes.HeaderText = psNotes.BuildTabCount(NotesHeader);
            pnlEditNote.Visible = false;
            pnlDimScreen.Visible = false;
        }


        protected void OnAddShipmentClicked(object sender, EventArgs e)
        {
            shipmentFinder.Visible = true;
        }

        protected void OnShipmentFinderItemSelected(object sender, ViewEventArgs<Shipment> e)
        {
            //current shipments
            var shipments = lstShipments.Items
                                        .Select(control => new
                                        {
                                            Id = (control.FindControl("hidShipmentId")).ToCustomHiddenField().Value.ToLong(),
                                            ShipmentNumber = (control.FindControl("lblShipmentNumber")).ToLabel().Text,
                                            DateCreated = (control.FindControl("litDateCreated")).ToLiteral().Text,
                                            CustomerNumber = (control.FindControl("litCustomerNumber")).ToLiteral().Text,
                                            CustomerName = (control.FindControl("litCustomerName")).ToLiteral().Text,
                                            Username = (control.FindControl("litUsername")).ToLiteral().Text,

                                        })
                                        .ToList();

            var newShipment = e.Argument;
            if (shipments.Select(t => t.Id).Contains(newShipment.Id))
            {
                DisplayMessages(new[] { ValidationMessage.Error(String.Format("Shipment already added.")) });
                return;
            }


            shipments.Add(new
            {
                newShipment.Id,
                newShipment.ShipmentNumber,
                DateCreated = newShipment.DateCreated.FormattedShortDate(),
                newShipment.Customer.CustomerNumber,
                CustomerName = newShipment.Customer.Name,
                newShipment.User.Username
            });

            lstShipments.DataSource = shipments;
            lstShipments.DataBind();
            tabShipments.HeaderText = shipments.BuildTabCount(ShipmentsHeader);
            shipmentFinder.Visible = false;
        }

        protected void OnShipmentFinderItemCancelled(object sender, EventArgs e)
        {
            shipmentFinder.Visible = false;
        }

        protected void OnDeleteShipmentClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var shipments = lstShipments.Items
                .Select(control => new
                {
                    Id = (control.FindControl("hidShipmentId")).ToCustomHiddenField().Value.ToLong(),
                    ShipmentNumber = (control.FindControl("lblShipmentNumber")).ToLabel().Text,
                    DateCreated = (control.FindControl("litDateCreated")).ToLiteral().Text,
                    CustomerNumber = (control.FindControl("litCustomerNumber")).ToLiteral().Text,
                    CustomerName = (control.FindControl("litCustomerName")).ToLiteral().Text,
                    Username = (control.FindControl("litUsername")).ToLiteral().Text,

                })
                .ToList();

            shipments.RemoveAt((imageButton.Parent.FindControl("hidShipmentIndex")).ToCustomHiddenField().Value.ToInt());

            lstShipments.DataSource = shipments;
            lstShipments.DataBind();
            tabShipments.HeaderText = shipments.BuildTabCount(ShipmentsHeader);
            athtuTabUpdater.SetForUpdate(tabShipments.ClientID, shipments.BuildTabCount(ShipmentsHeader));
        }

        protected void OnClearShipmentsClicked(object sender, EventArgs e)
        {
            lstShipments.DataSource = new List<Shipment>();
            lstShipments.DataBind();
            tabShipments.HeaderText = ShipmentsHeader;
            athtuTabUpdater.SetForUpdate(tabShipments.ClientID, ShipmentsHeader);
        }



        protected void OnAddServiceTicketClicked(object sender, EventArgs e)
        {
            serviceTicketFinder.Visible = true;
        }

        protected void OnServiceTicketFinderItemSelected(object sender, ViewEventArgs<ServiceTicket> e)
        {
            //current service tickets
            var serviceTickets = lstServiceTickets.Items
                .Select(control => new
                {
                    Id = (control.FindControl("hidServiceTicketId")).ToCustomHiddenField().Value.ToLong(),
                    ServiceTicketNumber = (control.FindControl("lblServiceTicketNumber")).ToLabel().Text,
                    DateCreated = (control.FindControl("litDateCreated")).ToLiteral().Text,
                    CustomerNumber = (control.FindControl("litCustomerNumber")).ToLiteral().Text,
                    CustomerName = (control.FindControl("litCustomerName")).ToLiteral().Text,
                    Username = (control.FindControl("litUsername")).ToLiteral().Text,

                })
                .ToList();

            var newServiceTicket = e.Argument;
            if (serviceTickets.Select(t => t.Id).Contains(newServiceTicket.Id))
            {
                DisplayMessages(new[] { ValidationMessage.Error(String.Format("Service Ticket already added.")) });
                return;
            }

            serviceTickets.Add(new
            {
                newServiceTicket.Id,
                newServiceTicket.ServiceTicketNumber,
                DateCreated = newServiceTicket.DateCreated.FormattedShortDate(),
                newServiceTicket.Customer.CustomerNumber,
                CustomerName = newServiceTicket.Customer.Name,
                newServiceTicket.User.Username
            });

            lstServiceTickets.DataSource = serviceTickets;
            lstServiceTickets.DataBind();
            tabServiceTickets.HeaderText = serviceTickets.BuildTabCount(ServiceTicketsHeader);
            serviceTicketFinder.Visible = false;
        }

        protected void OnServiceTicketFinderItemCancelled(object sender, EventArgs e)
        {
            serviceTicketFinder.Visible = false;
        }

        protected void OnDeleteServiceTicketClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var serviceTickets = lstServiceTickets.Items
                .Select(control => new
                {
                    Id = (control.FindControl("hidServiceTicketId")).ToCustomHiddenField().Value.ToLong(),
                    ServiceTicketNumber = (control.FindControl("lblServiceTicketNumber")).ToLabel().Text,
                    DateCreated = (control.FindControl("litDateCreated")).ToLiteral().Text,
                    CustomerNumber = (control.FindControl("litCustomerNumber")).ToLiteral().Text,
                    CustomerName = (control.FindControl("litCustomerName")).ToLiteral().Text,
                    Username = (control.FindControl("litUsername")).ToLiteral().Text,

                })
                .ToList();

            serviceTickets.RemoveAt((imageButton.Parent.FindControl("hidServiceTicketIndex")).ToCustomHiddenField().Value.ToInt());

            lstServiceTickets.DataSource = serviceTickets;
            lstServiceTickets.DataBind();
            tabServiceTickets.HeaderText = serviceTickets.BuildTabCount(ServiceTicketsHeader);
            athtuTabUpdater.SetForUpdate(tabServiceTickets.ClientID, serviceTickets.BuildTabCount(ServiceTicketsHeader));
        }

        protected void OnClearServiceTicketsClicked(object sender, EventArgs e)
        {
            lstServiceTickets.DataSource = new List<ServiceTicket>();
            lstServiceTickets.DataBind();
            tabServiceTickets.HeaderText = ServiceTicketsHeader;
            athtuTabUpdater.SetForUpdate(tabServiceTickets.ClientID, ServiceTicketsHeader);
        }
    }
}
