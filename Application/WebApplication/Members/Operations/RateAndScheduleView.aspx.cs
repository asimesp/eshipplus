﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.InputControls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
	public partial class RateAndScheduleView : MemberPageBase, IRateAndScheduleView
	{
		protected bool ScrollToBottom;

	    private const string GenerateRateAndSchedulePreauthorization = "GenerateRateAndSchedulePreauthorization";
        private const string CannotProcessPaymentForOfferedLoad = "Cannot process payment for offered load";

	    private const string ProcessingPaymentFlag = "PPF";
	    private const string DemoProcessingFlag = "DPF";
		private const string DemoNumber = "Demo";

		public static string PageAddress
		{
			get { return "~/Members/Operations/RateAndScheduleView.aspx"; }
		}

		public override ViewCode PageCode
		{
			get { return ViewCode.RateAndSchedule; }
		}

		public override string SetPageIconImage
		{
			set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
		}

		public bool SaveOriginToAddressBook
		{
			get { return chkSaveOrigin.Checked; }
		}

		public bool SaveOriginWithServicesToAddressBook
		{
			get { return chkSaveOriginWithServices.Checked; }
		}

		public bool SaveDestinationToAddressBook
		{
			get { return chkSaveDestination.Checked; }
		}

		public bool SaveDestinationWithServicesToAddressBook
		{
			get { return chkSaveDestinationWithServices.Checked; }
		}

		public List<ServiceViewSearchDto> Services
		{
			set
			{
				var services = value
					.Select(c => new ViewListItem(string.Format(" {0}", c.Description), c.Id.ToString()))
					.OrderBy(v => v.Text)
					.ToList();

				rptServices.DataSource = services;
				rptServices.DataBind();
			}
		}

		public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Shipment, PaymentInformation>> SaveShipment;
        public event EventHandler<ViewEventArgs<LoadOrder, PaymentInformation>> SaveLoadOrder;
		public event EventHandler<ViewEventArgs<string>> CustomerSearch;
		public event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;


	    public void DisplayCustomer(Customer customer)
		{
			var oldCustomerId = hidCustomerId.Value; // preserve old customer id;

			txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
			txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
			hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
			hidCustomerOutstandingBalance.Value = customer == null ? default(decimal).ToString() : customer.OutstandingBalance().ToString();
			hidCustomerCredit.Value = customer == null ? default(decimal).ToString() : customer.CreditLimit.ToString();
			litPONumberRequired.Visible = customer != null && customer.ShipmentRequiresPurchaseOrderNumber;
			litShipperReferenceNumberRequired.Visible = customer != null && customer.InvoiceRequiresShipperReference;
		    liNotifyPrimaryOrigin.Visible = customer?.AllowLocationContactNotification ?? false;
		    liNotifyPrimaryDestination.Visible = customer?.AllowLocationContactNotification ?? false;

			// require customer reconfigurations!!!

			var customerIdSpecificFilter = customer == null ? default(long) : customer.Id;

			addressBookFinder.CustomerIdSpecificFilter = customerIdSpecificFilter;
			operationsAddressInputOrigin.SetCustomerIdSpecificFilter(customerIdSpecificFilter);
			operationsAddressInputDestination.SetCustomerIdSpecificFilter(customerIdSpecificFilter);
			libraryItemFinder.CustomerIdSpecificFilter = customerIdSpecificFilter;
			libraryItemFinder.Reset();

			if (customer != null)
			{
				// if change in customer, update insurance status
				if (customer.Rating != null && oldCustomerId != hidCustomerId.Value)
					chkDeclineInsurance.Checked = !customer.Rating.InsuranceEnabled;

				pnlDeclineInsurance.Visible = customer.Rating != null && customer.Rating.InsuranceEnabled;

				LoadCustomerCustomFields(customer);
			}

			ddlShipmentDate.Focus();
		}

		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

		    var messagesList = messages.ToList();

            if (hidFlag.Value == ProcessingPaymentFlag && messages.HasErrors())
                messagesList.Add(ValidationMessage.Error("Due to the errors no payment has gone through."));

			if (hidFlag.Value == DemoProcessingFlag && !messages.HasErrors())
				ProcessDemoProcessingNotifications();
			

			hidFlag.Value = string.Empty;

            messageBox.Icon = messagesList.GenerateIcon();
			messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messagesList.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;

            litErrorMessages.Text = messagesList.HasErrors()
										? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messagesList.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
														Select(m => m.Message).ToArray())
										: string.Empty;
		}

		

		public void Set(ShoppedRate shoppedRate)
		{
			hidShoppedRateId.Value = shoppedRate.Id.ToString();
		}

		public void Set(Shipment shipment)
		{
			// check for shopped rate
			if (hidShoppedRateId.Value.ToLong() != default(long))
			{
				var shoppedRate = new ShoppedRate(hidShoppedRateId.Value.ToLong());
				if (shoppedRate.KeyLoaded && SaveShoppedRate != null)
				{
					hidShoppedRateId.Value = string.Empty;
					shoppedRate.TakeSnapShot();
					shoppedRate.Shipment = shipment;
					shoppedRate.ReferenceNumber = shipment.ShipmentNumber;
					SaveShoppedRate(this, new ViewEventArgs<ShoppedRate>(shoppedRate));
				}
			}

			// setup Transfer
			Session[WebApplicationConstants.RateAndScheduleTransferId] = shipment.Id;
			Session[WebApplicationConstants.RateAndScheduleTransferType] = WebApplicationConstants.RateAndScheduleShipmentTransfer;
			hidTransferToConfirmation.Value = true.ToString();
		}

		public void Set(LoadOrder loadOrder)
		{
			switch (loadOrder.Status)
			{
				case LoadOrderStatus.Offered:
					// setup Transfer
					Session[WebApplicationConstants.RateAndScheduleTransferId] = loadOrder.Id;
					Session[WebApplicationConstants.RateAndScheduleTransferType] = WebApplicationConstants.RateAndScheduleOfferedLoadTransfer;
					hidTransferToConfirmation.Value = true.ToString();
					break;
				case LoadOrderStatus.Quoted:
					if (hidShoppedRateId.Value.ToLong() != default(long))
					{
						var shoppedRate = new ShoppedRate(hidShoppedRateId.Value.ToLong());
						if (shoppedRate.KeyLoaded && SaveShoppedRate != null)
						{
							hidShoppedRateId.Value = string.Empty;
							shoppedRate.TakeSnapShot();
							shoppedRate.LoadOrder = loadOrder;
							shoppedRate.ReferenceNumber = loadOrder.LoadOrderNumber;
							SaveShoppedRate(this, new ViewEventArgs<ShoppedRate>(shoppedRate));
						}
					}

					// setup transfer
					Session[WebApplicationConstants.RateAndScheduleTransferId] = loadOrder.Id;
					Session[WebApplicationConstants.RateAndScheduleTransferType] = WebApplicationConstants.RateAndScheduleQuotedLoadTransfer;
					hidTransferToConfirmation.Value = true.ToString();
					break;
			}
		}



		private void ProcessDemoProcessingNotifications()
		{
			//handle demo notifications
			if (string.IsNullOrEmpty(hidServiceMode.Value))
			{
				var order = RetrieveLoadOrder(LoadOrderStatus.Offered);
				order.LoadOrderNumber = DemoNumber;
				this.NotifyLoadOrderEvent(order);
			}
			else
			{
				switch (hidServiceMode.Value.ToEnum<ServiceMode>())
				{
					case ServiceMode.LessThanTruckload:
					case ServiceMode.SmallPackage:
						var shipment = RetrieveShipment();
						shipment.ShipmentNumber = DemoNumber;
                        shipment.Status = ShipmentStatus.Void;
						this.NotifyNewShipment(shipment);
						break;
					case ServiceMode.Truckload:
						var quotedOrder = RetrieveLoadOrder(LoadOrderStatus.Quoted);
						quotedOrder.LoadOrderNumber = DemoNumber;
						this.NotifyLoadOrderEvent(quotedOrder);
						break;
					default:
						var offeredOrder = RetrieveLoadOrder(LoadOrderStatus.Offered);
						offeredOrder.LoadOrderNumber = DemoNumber;
						this.NotifyLoadOrderEvent(offeredOrder);
						break;
				}
			}
		}



        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var generateRateAndSchedulePreauthorization = new ToolbarMoreAction
            {
                CommandArgs = GenerateRateAndSchedulePreauthorization,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                Name = "Generate Rate And Schedule Preauthorization",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var moreActions = new List<ToolbarMoreAction> ();

            if(ActiveUser.TenantEmployee && hidAutoRated.Value.ToBoolean())
                moreActions.Add(generateRateAndSchedulePreauthorization);

            memberToolBar.ShowMore = moreActions.Any();

            return moreActions;
        }
         

		private void AddServicesRequired(List<AddressBookService> services)
		{
			if (!services.Any()) return;

			var serviceIds = services.Select(s => s.ServiceId).ToList();

			var viewIds = rptServices
				.Items
				.Cast<RepeaterItem>()
				.Where(i => i.FindControl("chkService").ToCheckBox().Checked)
				.Select(i => i.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong());

			foreach (var id in viewIds.Where(id => !serviceIds.Contains(id)))
				serviceIds.Add(id);

			var items = rptServices
				.Items
				.Cast<RepeaterItem>()
				.Select(i => new
				{
					Chk = i.FindControl("chkService").ToCheckBox(),
					Hid = i.FindControl("hidServiceId").ToCustomHiddenField()
				})
				.ToList();
			foreach (var item in items)
				item.Chk.Checked = serviceIds.Any(id => id == item.Hid.Value.ToLong());
		}


        private RateAndScheduleDefaultsItem GetProfileDelegate()
        {
            return new RateAndScheduleDefaultsItem
            {
                Shipment = RetrieveShipmentProfileData()
            };
        }

        private bool ValidateShipmentDateAndTime()
		{
			var msg = new List<ValidationMessage>();

			if (string.IsNullOrEmpty(ddlShipmentDate.SelectedValue))
				msg.Add(ValidationMessage.Error("Shipment date is invalid!"));
			if (!ddlReadyEarly.SelectedValue.IsValidTimeString())
				msg.Add(ValidationMessage.Error("Pickup window start time is invalid!"));
			if (!ddlReadyLate.SelectedValue.IsValidTimeString())
				msg.Add(ValidationMessage.Error("Pickup window end time is invalid!"));

			if (msg.Any())
			{
				DisplayMessages(msg);
				return false;
			}

			return true;
		}

		private Shipment GenerateShipment()
		{
			var shipment = new Shipment { User = ActiveUser, Tenant = ActiveUser.Tenant, HazardousMaterial = chkHazMat.Checked };

			shipment.Origin = new ShipmentLocation { Shipment = shipment, TenantId = ActiveUser.TenantId, AppointmentDateTime = DateUtility.SystemEarliestDateTime };
			shipment.Destination = new ShipmentLocation { Shipment = shipment, TenantId = ActiveUser.TenantId, AppointmentDateTime = DateUtility.SystemEarliestDateTime };
			shipment.ShipmentNumber = string.Empty;

			operationsAddressInputOrigin.RetrieveAddress(shipment.Origin);
			operationsAddressInputDestination.RetrieveAddress(shipment.Destination);

			shipment.Customer = new Customer(hidCustomerId.Value.ToLong());
			shipment.DesiredPickupDate = ddlShipmentDate.SelectedValue.ToDateTime();

			shipment.DeclineInsurance = chkDeclineInsurance.Checked;

			shipment.EarlyPickup = ddlReadyEarly.SelectedValue;
			shipment.EarlyDelivery = ddlDeliverEarly.SelectedValue;
			shipment.LatePickup = ddlReadyLate.SelectedValue;
			shipment.LateDelivery = ddlDeliverLate.SelectedValue;

			shipment.EmptyMileage = 0m;
			shipment.MiscField1 = string.Empty;
			shipment.MiscField2 = string.Empty;
			shipment.DriverName = string.Empty;
			shipment.DriverPhoneNumber = string.Empty;
			shipment.DriverTrailerNumber = string.Empty;
			shipment.AccountBucketUnitId = default(long);

			shipment.LinearFootRuleBypassed = chkBypassLinearFootRule.Checked;

			//Items
			var convert = chkMetric.Checked;
			var items = lstItems.Items
				.Select(i =>
				{
					var txtQty = i.FindControl("txtQuantity").ToTextBox();
					if (txtQty.Text.ToInt() <= 0) txtQty.Text = 1.ToString();
					var weight = convert ? i.FindControl("txtWeight").ToTextBox().Text.ToDecimal().KgToLb() : i.FindControl("txtWeight").ToTextBox().Text.ToDecimal();
					var length = convert ? i.FindControl("txtLength").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtLength").ToTextBox().Text.ToDecimal();
					var width = convert ? i.FindControl("txtWidth").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtWidth").ToTextBox().Text.ToDecimal();
					var height = convert ? i.FindControl("txtHeight").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtHeight").ToTextBox().Text.ToDecimal();
					return new ShipmentItem
					{
						Pickup = 0,
						Delivery = 1,
						ActualWeight = weight,
						ActualLength = length,
						ActualWidth = width,
						ActualHeight = height,
						Quantity = txtQty.Text.ToInt(),
						PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text.ToInt(),
						Description = i.FindControl("txtDescription").ToTextBox().Text,
						Comment = i.FindControl("txtComment").ToTextBox().Text,
						RatedFreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
						ActualFreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
						Value = i.FindControl("txtValue").ToTextBox().Text.ToDecimal(),
						IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
						PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
						NMFCCode = i.FindControl("txtNMFCCode").ToTextBox().Text,
						HTSCode = i.FindControl("txtHTSCode").ToTextBox().Text,
						Tenant = shipment.Tenant,
						Shipment = shipment,
						HazardousMaterial = i.FindControl("chkHazmat").ToAltUniformCheckBox().Checked,
					};
				})
				.ToList();
			shipment.Items = items;

			//Services
			shipment.Services = rptServices
				.Items
				.Cast<RepeaterItem>()
				.Where(i => i.FindControl("chkService").ToCheckBox().Checked)
				.Select(i => new ShipmentService
					{
						Shipment = shipment,
						ServiceId = i.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong(),
						TenantId = ActiveUser.TenantId,
					})
				.ToList();
			CheckForAutoServices(shipment); // must call after services and items loaded as we check items also for auto services
			if (shipment.Services.Any(s => s.ServiceId == ActiveUser.Tenant.HazardousMaterialServiceId) && !shipment.HazardousMaterial)
			{
				shipment.HazardousMaterial = true;
				chkHazMat.Checked = true;
			}


			

			return shipment;
		}

		private Shipment RetrieveShipment()
		{
			var shipment = new Shipment
			{
				User = ActiveUser,
				TenantId = ActiveUser.TenantId,
				DateCreated = DateTime.Now,
				ActualDeliveryDate = DateUtility.SystemEarliestDateTime,
				ActualPickupDate = DateUtility.SystemEarliestDateTime,
				Status = ShipmentStatus.Scheduled,
				Notes = new List<ShipmentNote>(),
				Stops = new List<ShipmentLocation>(),
				Equipments = new List<ShipmentEquipment>(),
				Documents = new List<ShipmentDocument>(),
				Assets = new List<ShipmentAsset>(),
				Vendors = new List<ShipmentVendor>(),
				AccountBuckets = new List<ShipmentAccountBucket>(),
				AutoRatingAccessorials = new List<ShipmentAutoRatingAccessorial>(),
				GeneralBolComments = string.Empty,
				CriticalBolComments = string.Empty,

				HazardousMaterial = chkHazMat.Checked,
				HazardousMaterialContactEmail = txtHazMatEmail.Text.StripSpacesFromEmails(),
				HazardousMaterialContactMobile = txtHazMatMobile.Text,
				HazardousMaterialContactName = txtHazMatName.Text,
				HazardousMaterialContactPhone = txtHazMatPhone.Text,
				ShipperBol = string.Empty,

				MiscField1 = string.Empty,
				MiscField2 = string.Empty,
				AccountBucketUnitId = default(long),

				DriverName = string.Empty,
				DriverPhoneNumber = string.Empty,
				DriverTrailerNumber = string.Empty,
	
				CreatedInError = false,

				EmptyMileage = 0m,
				Mileage = txtMileage.Text.ToDecimal(),
				MileageSourceId = hidMileageSourceId.Value.ToLong(),

				LinearFootRuleBypassed = chkBypassLinearFootRule.Checked,
                Project44QuoteNumber = hidProject44QuoteNumber.Value
			};

			shipment.Origin = new ShipmentLocation { Shipment = shipment, TenantId = ActiveUser.TenantId, StopOrder = 0 };
		    shipment.SendShipmentUpdateToOriginPrimaryContact = chkNotifyPrimaryOriginContactOfShipmentUpdates.Checked;
			shipment.Destination = new ShipmentLocation { Shipment = shipment, TenantId = ActiveUser.TenantId, StopOrder = 1 };
		    shipment.SendShipmentUpdateToDestinationPrimaryContact = chkNotifyPrimaryDestinationContactOfShipmentUpdates.Checked;
			shipment.ShipmentNumber = string.Empty;

			operationsAddressInputOrigin.RetrieveAddress(shipment.Origin);
			operationsAddressInputDestination.RetrieveAddress(shipment.Destination);

			var customer = new Customer(hidCustomerId.Value.ToLong());
			shipment.Customer = customer;

			shipment.CareOfAddressFormatEnabled = false;	// setting only affected when not in autorating scenario.

			shipment.ResellerAdditionId = customer.Rating.ResellerAdditionId;
			shipment.BillReseller = customer.Rating.ResellerAdditionId != default(long) && customer.Rating.BillReseller;

			shipment.AccountBuckets.Add(new ShipmentAccountBucket
			{
				AccountBucket = customer.DefaultAccountBucket,
				Shipment = shipment,
				TenantId = ActiveUser.TenantId,
				Primary = true,
			});
			shipment.Prefix = shipment.Customer.Prefix;
			shipment.HidePrefix = shipment.Customer.HidePrefix;


			shipment.DesiredPickupDate = ddlShipmentDate.SelectedValue.ToDateTime();

			var vendor = new Vendor(hidVendorId.Value.ToLong());
			shipment.EstimatedDeliveryDate = txtEstimatedDelivery.Text.ToDateTime();
			shipment.EarlyPickup = ddlReadyEarly.SelectedValue;
			shipment.EarlyDelivery = ddlDeliverEarly.SelectedValue;
			shipment.LatePickup = ddlReadyLate.SelectedValue;
			shipment.LateDelivery = ddlDeliverLate.SelectedValue;
			shipment.PurchaseOrderNumber = txtPONumber.Text;
			shipment.ShipperReference = txtShipperReference.Text;
			shipment.Vendors.Add(new ShipmentVendor
			{
				Shipment = shipment,
				TenantId = shipment.TenantId,
				Vendor = vendor,
				Primary = true,
				FailureComments = string.Empty,
				ProNumber = string.Empty,
			});
			shipment.ServiceMode = hidServiceMode.Value.ToEnum<ServiceMode>();
			shipment.DeclineInsurance = chkDeclineInsurance.Checked;

			// sales representative
			var srtier = customer.SalesRepresentative == null
							 ? null
							 : customer.SalesRepresentative.ApplicableTier(shipment.DateCreated, shipment.ServiceMode, customer);
			shipment.SalesRepresentative = customer.SalesRepresentative;
			shipment.SalesRepresentativeCommissionPercent = srtier != null ? srtier.CommissionPercent : 0m;
			shipment.SalesRepAddlEntityCommPercent = customer.SalesRepresentative != null ? customer.SalesRepresentative.AdditionalEntityCommPercent : 0m;
			shipment.SalesRepAddlEntityName = customer.SalesRepresentative != null ? customer.SalesRepresentative.AdditionalEntityName : string.Empty;
			shipment.SalesRepMaxCommValue = srtier != null ? srtier.CeilingValue : 0m;
			shipment.SalesRepMinCommValue = srtier != null ? srtier.FloorValue : 0m;

            //Items
            var convert = chkMetric.Checked;
            var items = lstItems.Items
                .Select(i =>
                {
                    var txtQty = i.FindControl("txtQuantity").ToTextBox();
                    if (txtQty.Text.ToInt() <= 0) txtQty.Text = 1.ToString();
                    var weight = convert ? i.FindControl("txtWeight").ToTextBox().Text.ToDecimal().KgToLb() : i.FindControl("txtWeight").ToTextBox().Text.ToDecimal();
                    var length = convert ? i.FindControl("txtLength").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtLength").ToTextBox().Text.ToDecimal();
                    var width = convert ? i.FindControl("txtWidth").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtWidth").ToTextBox().Text.ToDecimal();
                    var height = convert ? i.FindControl("txtHeight").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtHeight").ToTextBox().Text.ToDecimal();
                    return new ShipmentItem
                    {
                        Pickup = shipment.Origin.StopOrder,
                        Delivery = shipment.Destination.StopOrder,
                        ActualWeight = weight,
                        ActualLength = length,
                        ActualWidth = width,
                        ActualHeight = height,
                        Quantity = txtQty.Text.ToInt(),
                        PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text.ToInt(),
                        Description = i.FindControl("txtDescription").ToTextBox().Text,
                        Comment = i.FindControl("txtComment").ToTextBox().Text,
                        RatedFreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                        ActualFreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                        Value = i.FindControl("txtValue").ToTextBox().Text.ToDecimal(),
                        IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
                        HazardousMaterial = i.FindControl("chkHazmat").ToAltUniformCheckBox().Checked,
                        PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                        NMFCCode = i.FindControl("txtNMFCCode").ToTextBox().Text,
                        HTSCode = i.FindControl("txtHTSCode").ToTextBox().Text,
                        Tenant = shipment.Tenant,
                        Shipment = shipment,
                    };
                })
                .ToList();
            shipment.Items = items;

            // auto mark shipment as hazmat if there are hazmat items
            if (shipment.Items.Any(i => i.HazardousMaterial) && !shipment.HazardousMaterial)
            {
                shipment.HazardousMaterial = true;
                chkHazMat.Checked = true;
                pnlHazMat.Visible = true;
            }

			//Services
			shipment.Services = rptServices
				.Items
				.Cast<RepeaterItem>()
				.Where(i => i.FindControl("chkService").ToCheckBox().Checked)
				.Select(i => new ShipmentService
					{
						Shipment = shipment,
						ServiceId = i.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong(),
						TenantId = ActiveUser.TenantId,
					})
				.ToList();
			CheckForAutoServices(shipment); // must call after services and items loaded as we check items also for auto services

		   
			if (shipment.Services.Any(s => s.ServiceId == ActiveUser.Tenant.HazardousMaterialServiceId) && !shipment.HazardousMaterial)
				shipment.HazardousMaterial = true;

			// charges
			shipment.Charges = lstCharges.Items
				.Select(i => new ShipmentCharge
				{
					TenantId = shipment.TenantId,
					Shipment = shipment,
					ChargeCodeId = ((HiddenField)i.FindControl("hidChargeCodeId")).Value.ToLong(),
					UnitBuy = ((HiddenField)i.FindControl("hid1")).Value.ToDecimal(),
					UnitSell = ((HiddenField)i.FindControl("hid2")).Value.ToDecimal(),
					UnitDiscount = ((HiddenField)i.FindControl("hid3")).Value.ToDecimal(),
					Quantity = ((Literal)i.FindControl("litQuantity")).Text.ToInt(),
					Comment = i.FindControl("hidChargeCodeComment").ToHiddenField().Value,
				})
				.ToList();

            // references
            shipment.CustomerReferences = lstReferences
				.Items
				.Select(i => new ShipmentReference
					{
						TenantId = shipment.TenantId,
						Shipment = shipment,
						DisplayOnOrigin = i.FindControl("hidDisplayOnOrigin").ToCustomHiddenField().Value.ToBoolean(),
						DisplayOnDestination = i.FindControl("hidDisplayOnDestination").ToCustomHiddenField().Value.ToBoolean(),
						Name = i.FindControl("litName").ToLiteral().Text,
						Value = i.FindControl("txtValue").ToTextBox().Text,
						Required = i.FindControl("litRequired").Visible,
					})
				.ToList();

			


			// auto rating details
			shipment.OriginalRateValue = hidOriginal.Value.ToDecimal();
			shipment.ShipmentAutorated = hidAutoRated.Value.ToBoolean();
			shipment.LineHaulProfitAdjustmentRatio = hidFreightProfitAdjustment.Value.ToDecimal();
			shipment.FuelProfitAdjustmentRatio = hidFuelProfitAdjustment.Value.ToDecimal();
			shipment.ServiceProfitAdjustmentRatio = hidServiceProfitAdjustment.Value.ToDecimal();
			shipment.AccessorialProfitAdjustmentRatio = hidAccessorialProfitAdjustment.Value.ToDecimal();
			shipment.ResellerAdditionalAccessorialMarkup = hidResellerAcc.Value.ToDecimal();
			shipment.ResellerAdditionalAccessorialMarkupIsPercent = hidResellerAccIsPercent.Value.ToBoolean();
			shipment.ResellerAdditionalServiceMarkup = hidResellerSer.Value.ToDecimal();
			shipment.ResellerAdditionalServiceMarkupIsPercent = hidResellerSerIsPercent.Value.ToBoolean();
			shipment.ResellerAdditionalFuelMarkup = hidResellerFu.Value.ToDecimal();
			shipment.ResellerAdditionalFuelMarkupIsPercent = hidResellerFuIsPercent.Value.ToBoolean();
			shipment.ResellerAdditionalFreightMarkup = hidResellerFr.Value.ToDecimal();
			shipment.ResellerAdditionalFreightMarkupIsPercent = hidResellerFrIsPercent.Value.ToBoolean();
			shipment.DirectPointRate = hidDirectPoint.Value.ToBoolean();
			shipment.SmallPackType = hidSmallPackType.Value;
			shipment.SmallPackageEngine = hidSmallPackageEngine.Value.ToEnum<SmallPackageEngine>();
			shipment.ApplyDiscount = hidApplyDiscount.Value.ToBoolean();
			shipment.IsLTLPackageSpecificRate = hidLTLPackageSpecificRate.Value.ToBoolean();
			shipment.BilledWeight = hidBilledWeight.Value.ToDecimal();
			shipment.RatedCubicFeet = hidRatedCubicFeet.Value.ToDecimal();
			shipment.RatedWeight = hidRatedWeight.Value.ToDecimal();
			shipment.RatedPcf = hidRatedPcf.Value.ToDecimal();
		    shipment.GuaranteedDeliveryServiceTime = hidGuaranteedDeliveryServiceTime.Value;
		    shipment.IsGuaranteedDeliveryService = hidIsGuaranteedDeliveryService.Value.ToBoolean();
		    shipment.GuaranteedDeliveryServiceRate = hidGuaranteedDeliveryServiceRate.Value.ToDecimal();
		    shipment.GuaranteedDeliveryServiceRateType = !string.IsNullOrEmpty(hidGuaranteedDeliveryServiceRateType.Value)
		                                                     ? hidGuaranteedDeliveryServiceRateType.Value.ToEnum<RateType>()
		                                                     : RateType.Flat;


		    shipment.IsExpeditedService = hidIsExpeditedService.Value.ToBoolean();
		    shipment.ExpeditedServiceRate = hidExpeditedServiceRate.Value.ToDecimal();
		    shipment.ExpeditedServiceTime = hidExpeditedServiceTime.Value;


            var ltlGuarCharge = new LTLGuaranteedCharge(hidLTLGuarateedChargeId.Value.ToLong());

		    shipment.CriticalBolComments = shipment.IsGuaranteedDeliveryService
		        ? ltlGuarCharge.Id != default(long) 
		            ? string.IsNullOrEmpty(ltlGuarCharge.CriticalNotes) // eShip Guaranteed Service
                        ? string.Format(WebApplicationConstants.GuaranteedServiceCriticalBolComments, ltlGuarCharge.Time, shipment.EstimatedDeliveryDate.FormattedShortDate())
		                : ltlGuarCharge.CriticalNotes.Replace(WebApplicationConstants.DeliveryDateTag, shipment.EstimatedDeliveryDate.FormattedShortDate())
                    : !string.IsNullOrEmpty(hidProject44GuaranteedCharge.Value)
		                ? string.Format(WebApplicationConstants.GuaranteedServiceCriticalBolComments, // Project 44 Guaranteed Service
		                    hidProject44GuaranteedCharge.Value.ToEnum<Project44ChargeCode>().GetGuaranteedServiceTime(),
		                    shipment.EstimatedDeliveryDate.FormattedShortDate())
                        : string.Empty
		        : shipment.IsExpeditedService
		            ? !string.IsNullOrEmpty(hidProject44ExpeditedCharge.Value)
		                ? string.Format(WebApplicationConstants.ExpeditedCriticalBolComments, // Project 44 Expedited Service
		                    hidProject44ExpeditedCharge.Value.ToEnum<Project44ChargeCode>().GetExpeditedServiceTime(),
		                    shipment.EstimatedDeliveryDate.FormattedShortDate())
		                : string.Empty
                    : string.Empty;
			

			var tier = new DiscountTier(hidDiscountTierId.Value.ToLong(), false);
			shipment.VendorDiscountPercentage = tier.DiscountPercent;
			shipment.VendorFreightFloor = tier.FloorValue;
			shipment.VendorFreightCeiling = tier.CeilingValue;
			shipment.VendorFuelPercent = hidFuPercent.Value.ToDecimal();

			var sellRate = new LTLSellRate(hidLTLSellRateId.Value.ToLong(), false);

            // TODO: MP: use new markup percent methods
			
            shipment.CustomerFreightMarkupValue = hidMarkupValue.Value.ToDecimal();
            shipment.CustomerFreightMarkupPercent = hidMarkupPercent.Value.ToDecimal();
			shipment.UseLowerCustomerFreightMarkup = sellRate.UseMinimum;
			shipment.VendorRatingOverrideAddress = sellRate.VendorRating == null || string.IsNullOrEmpty(sellRate.VendorRating.OverrideAddress)
													? string.Empty
													: sellRate.VendorRating.OverrideAddress;
			shipment.CriticalBolComments = sellRate.VendorRating.AppendVendorRatingCriticalCommentToShipment(shipment.CriticalBolComments);

			// update processing for SMC3 rated shipments
            if(!hidProject44RateSelected.Value.ToBoolean())
            { 
			    Rater2.UpdateShipmentAutoRatingAccessorilas(shipment, sellRate);
			    if (shipment.ServiceMode == ServiceMode.LessThanTruckload)
			    {
				    var rule = new LTLCubicFootCapacityRule(hidCFCRuleId.Value.ToLong());
				    if (!rule.KeyLoaded) rule = null;
				    Rater2.UpdateShipmentItemRatedCalculations(shipment, rule, tier);

				    // terminals
				    try
				    {
					    var variables = ProcessorVars.IntegrationVariables[ActiveUser.TenantId];
					    var carrierConnect = new CarrierConnect(variables.CarrierConnectParameters); //NOTE: without country map as country id is set below on return
					    var terminalCodes = new[] { hidOriginTerminalCode.Value, hidDestinationTerminalCode.Value };
					    var terminals = carrierConnect.GetLTLTerminalInfo(hidVendorScac.Value, terminalCodes, ddlShipmentDate.SelectedValue.ToDateTime());

					    shipment.OriginTerminal = terminals.FirstOrDefault(t => t.Code == hidOriginTerminalCode.Value) ?? new LTLTerminalInfo();
					    shipment.OriginTerminal.Shipment = shipment;
					    shipment.OriginTerminal.TenantId = shipment.TenantId;
					    shipment.OriginTerminal.CountryId = shipment.Origin.CountryId;

					    shipment.DestinationTerminal = terminals.FirstOrDefault(t => t.Code == hidDestinationTerminalCode.Value) ?? new LTLTerminalInfo();
					    shipment.DestinationTerminal.Shipment = shipment;
					    shipment.DestinationTerminal.TenantId = shipment.TenantId;
					    shipment.DestinationTerminal.CountryId = shipment.Destination.CountryId;
				    }
				    catch (Exception ex)
				    {
					    ErrorLogger.LogError(ex, Context);
				    }
			    }
            }

            return shipment;
		}

        private RateAndScheduleProfileDataDto RetrieveShipmentProfileData()
        {
          
            var shipment = new RateAndScheduleProfileDataDto()
            { 
                HazardousMaterial = chkHazMat.Checked,
                HazardousMaterialContactEmail = txtHazMatEmail.Text.StripSpacesFromEmails(),
                HazardousMaterialContactMobile = txtHazMatMobile.Text,
                HazardousMaterialContactName = txtHazMatName.Text,
                HazardousMaterialContactPhone = txtHazMatPhone.Text
                
            };

            var origin = new ShipmentLocation { TenantId = ActiveUser.TenantId, StopOrder = 0 };
            operationsAddressInputOrigin.RetrieveAddress(origin);

            var destination = new ShipmentLocation { TenantId = ActiveUser.TenantId, StopOrder = 1 };
            operationsAddressInputDestination.RetrieveAddress(destination);


            shipment.OriginStreet1 = origin.Street1;
            shipment.OriginStreet2 = origin.Street2;
            shipment.OriginCity = origin.City;
            shipment.OriginState = origin.State;
            shipment.OriginDescription = origin.Description;
            shipment.OriginSpecialInstructions = origin.SpecialInstructions;
            shipment.OriginCountryId = origin.CountryId;
            shipment.OriginSpecialInstructions = origin.SpecialInstructions;
            shipment.OriginContact = origin.Contacts.FirstOrDefault()?.Name;
            shipment.OriginPhone = origin.Contacts.FirstOrDefault()?.Phone;
            shipment.OriginPostalCode = origin.PostalCode;

            shipment.DestinationStreet1 = destination.Street1;
            shipment.DestinationStreet2 = destination.Street2;
            shipment.DestinationCity = destination.City;
            shipment.DestinationState = destination.State;
            shipment.DestinationDescription = destination.Description;
            shipment.DestinationSpecialInstructions = destination.SpecialInstructions;
            shipment.DestinationCountryId = destination.CountryId;
            shipment.DestinationSpecialInstructions = origin.SpecialInstructions;
            shipment.DestinationContact = origin.Contacts.FirstOrDefault()?.Name;
            shipment.DestinationPhone = origin.Contacts.FirstOrDefault()?.Phone;
            shipment.DestinationPostalCode = destination.PostalCode;

            shipment.ShipperReference = txtShipperReference.Text;
            shipment.PurchaseOrderNumber = txtPONumber.Text;

            var customer = new Customer(hidCustomerId.Value.ToLong());
            shipment.CustomerNumber = customer.CustomerNumber;
            


            shipment.DesiredPickupDate = ddlShipmentDate.SelectedValue.ToDateTime();
            shipment.EarlyPickup = ddlReadyEarly.SelectedValue;
            shipment.LatePickup = ddlReadyLate.SelectedValue;
            shipment.EnglishInputUnits = chkEnglish.Checked;
            shipment.MetricInputUnits = chkMetric.Checked;
            shipment.BypassLinearFootRule = chkBypassLinearFootRule.Checked;
           

            //Items
            var convert = chkMetric.Checked;
            var items = lstItems.Items
                .Select(i =>
                {
                    var txtQty = i.FindControl("txtQuantity").ToTextBox();
                    if (txtQty.Text.ToInt() <= 0) txtQty.Text = 1.ToString();
                    var weight = convert ? i.FindControl("txtWeight").ToTextBox().Text.ToDecimal().KgToLb() : i.FindControl("txtWeight").ToTextBox().Text.ToDecimal();
                    var length = convert ? i.FindControl("txtLength").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtLength").ToTextBox().Text.ToDecimal();
                    var width = convert ? i.FindControl("txtWidth").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtWidth").ToTextBox().Text.ToDecimal();
                    var height = convert ? i.FindControl("txtHeight").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtHeight").ToTextBox().Text.ToDecimal();
                    return new RateAndScheduleShipmentItemDto
                    {
                        Pickup = origin.StopOrder,
                        Delivery = destination.StopOrder,
                        ActualWeight = weight,
                        ActualLength = length,
                        ActualWidth = width,
                        ActualHeight = height,
                        Quantity = txtQty.Text.ToInt(),
                        PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text.ToInt(),
                        Description = i.FindControl("txtDescription").ToTextBox().Text,
                        Comment = i.FindControl("txtComment").ToTextBox().Text,
                        RatedFreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                        ActualFreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                        Value = i.FindControl("txtValue").ToTextBox().Text.ToDecimal(),
                        IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
                        HazardousMaterial = i.FindControl("chkHazmat").ToAltUniformCheckBox().Checked,
                        PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                        NMFCCode = i.FindControl("txtNMFCCode").ToTextBox().Text,
                        HTSCode = i.FindControl("txtHTSCode").ToTextBox().Text
                       
                    };
                })
                .ToList();
            shipment.Items = items;

            // auto mark shipment as hazmat if there are hazmat items
            if (shipment.Items.Any(i => i.HazardousMaterial) && !shipment.HazardousMaterial)
            {
                shipment.HazardousMaterial = true;
                chkHazMat.Checked = true;
                pnlHazMat.Visible = true;
            }
           
            //Services
            shipment.Services =   rptServices
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkService").ToCheckBox().Checked)
                .Select(i =>i.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong())
                .ToList();
            
            if (shipment.Services.Any(s => s == ActiveUser.Tenant.HazardousMaterialServiceId) && !shipment.HazardousMaterial)
                shipment.HazardousMaterial = true;
            
            return shipment;
        }

        private LoadOrder RetrieveLoadOrder(LoadOrderStatus status)
		{
			var load = new LoadOrder
			{
				User = ActiveUser,
				TenantId = ActiveUser.TenantId,
				DateCreated = DateTime.Now,
				Description = string.Empty,
				Status = status,
				Notes = new List<LoadOrderNote>(),
				Stops = new List<LoadOrderLocation>(),
				Equipments = new List<LoadOrderEquipment>(),
				Documents = new List<LoadOrderDocument>(),
				Vendors = new List<LoadOrderVendor>(),
				Charges = new List<LoadOrderCharge>(),

				HazardousMaterial = chkHazMat.Checked,
				HazardousMaterialContactEmail = txtHazMatEmail.Text.StripSpacesFromEmails(),
				HazardousMaterialContactMobile = txtHazMatMobile.Text,
				HazardousMaterialContactName = txtHazMatName.Text,
				HazardousMaterialContactPhone = txtHazMatPhone.Text,

				MiscField1 = string.Empty,
				MiscField2 = string.Empty,
				AccountBucketUnitId = default(long),

				DriverName = string.Empty,
				DriverPhoneNumber = string.Empty,
				DriverTrailerNumber = string.Empty,

				EmptyMileage = 0m,
				Mileage = txtMileage.Text.ToDecimal(),
				MileageSourceId = hidMileageSourceId.Value.ToLong(),

				ShipperBol = string.Empty,
				GeneralBolComments = string.Empty,
				CriticalBolComments = string.Empty,
			};

			load.Origin = new LoadOrderLocation { LoadOrder = load, TenantId = ActiveUser.TenantId, StopOrder = 0 };
			load.Destination = new LoadOrderLocation { LoadOrder = load, TenantId = ActiveUser.TenantId, StopOrder = 1 };
			load.LoadOrderNumber = string.Empty;

			operationsAddressInputOrigin.RetrieveAddress(load.Origin);
			operationsAddressInputDestination.RetrieveAddress(load.Destination);

			var customer = new Customer(hidCustomerId.Value.ToLong());
			load.Customer = customer;

			var rating = customer.Rating;
			load.ResellerAdditionId = rating == null ? default(long) : rating.ResellerAdditionId;
			load.BillReseller = rating != null && rating.ResellerAdditionId != default(long) && rating.BillReseller;

			load.AccountBuckets.Add(new LoadOrderAccountBucket
			{
				LoadOrder = load,
				AccountBucket = customer.DefaultAccountBucket,
				Primary = true,
				TenantId = load.TenantId,
			});
			load.Prefix = customer.Prefix;
			load.HidePrefix = customer.HidePrefix;

			load.DesiredPickupDate = ddlShipmentDate.SelectedValue.ToDateTime();
			load.EstimatedDeliveryDate = load.DesiredPickupDate.AddDays(1);
			load.EarlyPickup = ddlReadyEarly.SelectedValue;
			load.EarlyDelivery = ddlDeliverEarly.SelectedValue;
			load.LatePickup = ddlReadyLate.SelectedValue;
			load.LateDelivery = ddlDeliverLate.SelectedValue;
			load.PurchaseOrderNumber = txtPONumber.Text;
			load.ShipperReference = txtShipperReference.Text;
			load.ServiceMode = string.IsNullOrEmpty(hidServiceMode.Value)
								? ServiceMode.NotApplicable
								: hidServiceMode.Value.ToEnum<ServiceMode>();
			load.DeclineInsurance = chkDeclineInsurance.Checked;

			// sales representative
			var srtier = customer.SalesRepresentative == null
							 ? null
							 : customer.SalesRepresentative.ApplicableTier(load.DateCreated, load.ServiceMode, customer);
			load.SalesRepresentative = customer.SalesRepresentative;
			load.SalesRepresentativeCommissionPercent = srtier != null ? srtier.CommissionPercent : 0m;
			load.SalesRepAddlEntityCommPercent = customer.SalesRepresentative != null ? customer.SalesRepresentative.AdditionalEntityCommPercent : 0m;
			load.SalesRepAddlEntityName = customer.SalesRepresentative != null ? customer.SalesRepresentative.AdditionalEntityName : string.Empty;
			load.SalesRepMaxCommValue = srtier != null ? srtier.CeilingValue : 0m;
			load.SalesRepMinCommValue = srtier != null ? srtier.FloorValue : 0m;

            //Items
            var items = lstItems.Items
                .Select(i =>
                {
                    var convert = chkMetric.Checked;
                    var txtQty = i.FindControl("txtQuantity").ToTextBox();
                    if (txtQty.Text.ToInt() <= 0) txtQty.Text = 1.ToString();
                    return new LoadOrderItem
                    {
                        Pickup = load.Origin.StopOrder,
                        Delivery = load.Destination.StopOrder,
                        Weight = convert ? i.FindControl("txtWeight").ToTextBox().Text.ToDecimal().KgToLb() : i.FindControl("txtWeight").ToTextBox().Text.ToDecimal(),
                        Length = convert ? i.FindControl("txtLength").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtLength").ToTextBox().Text.ToDecimal(),
                        Width = convert ? i.FindControl("txtWidth").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtWidth").ToTextBox().Text.ToDecimal(),
                        Height = convert ? i.FindControl("txtHeight").ToTextBox().Text.ToDecimal().CmToIn() : i.FindControl("txtHeight").ToTextBox().Text.ToDecimal(),
                        Quantity = txtQty.Text.ToInt(),
                        PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text.ToInt(),
                        Description = i.FindControl("txtDescription").ToTextBox().Text,
                        Comment = i.FindControl("txtComment").ToTextBox().Text,
                        PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                        FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
                        Value = i.FindControl("txtValue").ToTextBox().Text.ToDecimal(),
                        IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
                        HazardousMaterial = i.FindControl("chkHazmat").ToAltUniformCheckBox().Checked,
                        NMFCCode = i.FindControl("txtNMFCCode").ToTextBox().Text,
                        HTSCode = i.FindControl("txtHTSCode").ToTextBox().Text,
                        Tenant = load.Tenant,
                        LoadOrder = load,
                    };
                })
                .ToList();
            load.Items = items;

            // auto mark load as hazmat if there are hazmat items
            if (load.Items.Any(i => i.HazardousMaterial) && !load.HazardousMaterial)
            {
                load.HazardousMaterial = true;
                chkHazMat.Checked = true;
                pnlHazMat.Visible = true;
            }

			//Services
			var services = rptServices
				.Items
				.Cast<RepeaterItem>()
				.Where(i => i.FindControl("chkService").ToCheckBox().Checked)
				.Select(i => new LoadOrderService
					{
						LoadOrder = load,
						ServiceId = i.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong(),
						TenantId = ActiveUser.TenantId,
					})
				.ToList();
			load.Services = services;
			CheckForAutoServices(load);
			if (load.Services.Any(s => s.ServiceId == ActiveUser.Tenant.HazardousMaterialServiceId) && !load.HazardousMaterial)
				load.HazardousMaterial = true;

			// charges
			if (status == LoadOrderStatus.Quoted)
			{
				load.Charges = lstCharges.Items
					.Select(i =>
					{
						var litQty = ((Literal)i.FindControl("litQuantity"));
						if (litQty.Text.ToInt() <= 0) litQty.Text = 1.ToString();
						return new LoadOrderCharge
						{
							TenantId = load.TenantId,
							LoadOrder = load,
							ChargeCodeId = ((HiddenField)i.FindControl("hidChargeCodeId")).Value.ToLong(),
							UnitBuy = ((HiddenField)i.FindControl("hid1")).Value.ToDecimal(),
							UnitSell = ((HiddenField)i.FindControl("hid2")).Value.ToDecimal(),
							UnitDiscount = ((HiddenField)i.FindControl("hid3")).Value.ToDecimal(),
							Quantity = ((Literal)i.FindControl("litQuantity")).Text.ToInt(),
							Comment = i.FindControl("hidChargeCodeComment").ToHiddenField().Value,
						};
					})
					.ToList();
			}

			// references
			load.CustomerReferences = lstReferences.Items
				.Select(i => new LoadOrderReference
				{
					TenantId = load.TenantId,
					LoadOrder = load,
					DisplayOnOrigin = i.FindControl("hidDisplayOnOrigin").ToCustomHiddenField().Value.ToBoolean(),
					DisplayOnDestination = i.FindControl("hidDisplayOnDestination").ToCustomHiddenField().Value.ToBoolean(),
					Name = i.FindControl("litName").ToLiteral().Text,
					Value = i.FindControl("txtValue").ToTextBox().Text,
					Required = i.FindControl("litRequired").Visible,
				})
				.ToList();

			

			return load;
		}



		private void CheckForAutoServices(Shipment shipment)
		{
			var bcsId = string.Empty;
			var hmId = string.Empty;
			if (shipment.IsInternational() && ActiveUser.Tenant.BorderCrossingServiceId != default(long) &&
				shipment.Services.All(s => s.ServiceId != ActiveUser.Tenant.BorderCrossingServiceId))
			{
				shipment.Services.Add(new ShipmentService
				{
					TenantId = ActiveUser.TenantId,
					Shipment = shipment,
					ServiceId = ActiveUser.Tenant.BorderCrossingServiceId
				});
				bcsId = ActiveUser.Tenant.BorderCrossingServiceId.ToString();

			}

			var shipmentIsHarzardous = shipment.HazardousMaterial || shipment.Items.Any(i => i.HazardousMaterial);
			if (shipmentIsHarzardous && ActiveUser.Tenant.HazardousMaterialServiceId != default(long) &&
				shipment.Services.All(s => s.ServiceId != ActiveUser.Tenant.HazardousMaterialServiceId))
			{
				shipment.Services.Add(new ShipmentService
				{
					TenantId = ActiveUser.TenantId,
					Shipment = shipment,
					ServiceId = ActiveUser.Tenant.HazardousMaterialServiceId
				});
				hmId = ActiveUser.Tenant.HazardousMaterialServiceId.ToString();
			}

			if (!string.IsNullOrEmpty(hmId) || !string.IsNullOrEmpty(bcsId))
			{
				foreach (RepeaterItem item in rptServices.Items)
				{
					var hid = item.FindControl("hidServiceId").ToCustomHiddenField().Value;
					if (hid == hmId || hid == bcsId)
						item.FindControl("chkService").ToCheckBox().Checked = true;
				}
			}


		    if (hidProject44ServiceCode.Value == string.Empty)
		    {
		        if (hidIsGuaranteedDeliveryService.Value.ToBoolean() &&
		            ActiveUser.Tenant.GuaranteedDeliveryServiceId != default(long) &&
		            shipment.Services.All(s => s.ServiceId != ActiveUser.Tenant.GuaranteedDeliveryServiceId))
		        {
		            shipment.Services.Add(new ShipmentService
		            {
		                TenantId = ActiveUser.TenantId,
		                Shipment = shipment,
		                ServiceId = ActiveUser.Tenant.GuaranteedDeliveryServiceId
		            });
		        }
            }
		    else if (hidIsGuaranteedDeliveryService.Value.ToBoolean() || hidIsExpeditedService.Value.ToBoolean())
            {
		        if (shipment.Services.All(s => s.Service.Project44Code != hidProject44ServiceCode.Value.ToEnum<Project44ServiceCode>()))
		        {
		            shipment.Services.Add(new ShipmentService
		            {
		                TenantId = ActiveUser.TenantId,
		                Shipment = shipment,
		                ServiceId = new ServiceSearch().FetchServiceIdByProject44Code(hidProject44ServiceCode.Value.ToEnum<Project44ServiceCode>().ToInt(), shipment.TenantId)
		            });
		        }
            }
        }

		private void CheckForAutoServices(LoadOrder loadOrder)
		{
			var bcsId = string.Empty;
			var hmId = string.Empty;
			if (loadOrder.IsInternational() && ActiveUser.Tenant.BorderCrossingServiceId != default(long) &&
				loadOrder.Services.All(s => s.ServiceId != ActiveUser.Tenant.BorderCrossingServiceId))
			{
				loadOrder.Services.Add(new LoadOrderService
				{
					TenantId = ActiveUser.TenantId,
					LoadOrder = loadOrder,
					ServiceId = ActiveUser.Tenant.BorderCrossingServiceId
				});
				bcsId = ActiveUser.Tenant.BorderCrossingServiceId.ToString();
			}

			if (loadOrder.HazardousMaterial && ActiveUser.Tenant.HazardousMaterialServiceId != default(long) &&
				loadOrder.Services.All(s => s.ServiceId != ActiveUser.Tenant.HazardousMaterialServiceId))
			{
				loadOrder.Services.Add(new LoadOrderService
				{
					TenantId = ActiveUser.TenantId,
					LoadOrder = loadOrder,
					ServiceId = ActiveUser.Tenant.HazardousMaterialServiceId
				});
				hmId = ActiveUser.Tenant.HazardousMaterialServiceId.ToString();
			}

			if (!string.IsNullOrEmpty(hmId) || !string.IsNullOrEmpty(bcsId))
			{
				foreach (RepeaterItem item in rptServices.Items)
				{
					var hid = item.FindControl("hidServiceId").ToCustomHiddenField().Value;
					if (hid == hmId || hid == bcsId)
						item.FindControl("chkService").ToCheckBox().Checked = true;
				}
			}
            

		    if (hidProject44ServiceCode.Value == string.Empty)
		    {
		        if (hidIsGuaranteedDeliveryService.Value.ToBoolean() &&
		            ActiveUser.Tenant.GuaranteedDeliveryServiceId != default(long) &&
		            loadOrder.Services.All(s => s.ServiceId != ActiveUser.Tenant.GuaranteedDeliveryServiceId))
		        {
		            loadOrder.Services.Add(new LoadOrderService
		            {
		                TenantId = ActiveUser.TenantId,
		                LoadOrder = loadOrder,
		                ServiceId = ActiveUser.Tenant.GuaranteedDeliveryServiceId
		            });
		        }
		    }
		    else if (hidIsGuaranteedDeliveryService.Value.ToBoolean() || hidIsExpeditedService.Value.ToBoolean())
		    {
		        if(loadOrder.Services.All(s => s.Service.Project44Code != hidProject44ServiceCode.Value.ToEnum<Project44ServiceCode>()))
		        {
		            loadOrder.Services.Add(new LoadOrderService
		            {
		                TenantId = ActiveUser.TenantId,
		                LoadOrder = loadOrder,
		                ServiceId = new ServiceSearch().FetchServiceIdByProject44Code(hidProject44ServiceCode.Value.ToEnum<Project44ServiceCode>().ToInt(), loadOrder.TenantId)
		            });
		        }
		    }


            
        }


		private void InitialDataSetup()
		{
			// customer check
			if (!ActiveUser.TenantEmployee && !ActiveUser.UserShipAs.Any()) SingleCustomerSetup();

			// default shipment date
			var dates = new List<ViewListItem>();
			for (var i = 0; i <= ProcessorVars.AdvancedCustomerLTLScheduleLimit[ActiveUser.TenantId]; i++)
			{
				var date = DateTime.Now.AddDays(i);
				dates.Add(new ViewListItem(string.Format("{0:dddd MMM dd}", date), date.FormattedShortDate()));
			}
			dates.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, string.Empty));
			ddlShipmentDate.DataSource = dates;
			ddlShipmentDate.DataBind();
			ddlShipmentDate.SelectedValue = string.Empty;

			// ready and delivery time
			var readyTimes = TimeUtility.BuildTimeList();
			readyTimes.Insert(0, string.Empty);
			ddlReadyEarly.DataSource = readyTimes;
			ddlReadyEarly.DataBind();
			ddlReadyEarly.SelectedValue = string.Empty;
			ddlReadyLate.DataSource = readyTimes;
			ddlReadyLate.DataBind();
			ddlReadyLate.SelectedValue = string.Empty;
			ddlDeliverEarly.DataSource = TimeUtility.BuildTimeList();
			ddlDeliverEarly.DataBind();
			ddlDeliverEarly.SelectedValue = TimeUtility.DefaultOpen;
			ddlDeliverLate.DataSource = TimeUtility.BuildTimeList();
			ddlDeliverLate.DataBind();
			ddlDeliverLate.SelectedValue = TimeUtility.DefaultClose;

			// single item
			AddSingleBlankItem();
		}

		private void SingleCustomerSetup()
		{
			if (!ActiveUser.DefaultCustomer.Active)
			{
				DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
				return;
			}

			DisplayCustomer(ActiveUser.DefaultCustomer);

			txtCustomerNumber.ReadOnly = true;
			txtCustomerNumber.BackColor = Color.LightGray;
			imgCustomerSearch.Enabled = false;
		}

		private void LoadTransferShipment(Shipment shipment)
		{
			// customer
			DisplayCustomer(shipment.Customer);

			// insurance
			chkDeclineInsurance.Checked = shipment.DeclineInsurance;

			// shipment date
			if (ddlShipmentDate.ContainsValue(shipment.DesiredPickupDate.FormattedShortDate()))
				ddlShipmentDate.SelectedValue = shipment.DesiredPickupDate.FormattedShortDate();

			if (ddlReadyEarly.ContainsValue(shipment.EarlyPickup))
				ddlReadyEarly.SelectedValue = shipment.EarlyPickup;

			if (ddlReadyLate.ContainsValue(shipment.LatePickup))
				ddlReadyLate.SelectedValue = shipment.LatePickup;

			// origin destination
			operationsAddressInputOrigin.LoadAddress(shipment.Origin);
			operationsAddressInputDestination.LoadAddress(shipment.Destination);

			// services
			var serviceIds = shipment.Services.Select(s => s.ServiceId.ToString()).ToList();
			foreach (RepeaterItem item in rptServices.Items)
			{
				var hid = item.FindControl("hidServiceId").ToCustomHiddenField().Value;
				item.FindControl("chkService").ToCheckBox().Checked = serviceIds.Contains(hid);
			}

			// by pass linear foot rule
			chkBypassLinearFootRule.Checked = shipment.LinearFootRuleBypassed;

			// items
			var vItems = lstItems.Items
				.Select(i => new
				{
					Description = i.FindControl("txtDescription").ToTextBox().Text,
					Weight = i.FindControl("txtWeight").ToTextBox().Text,
					Length = i.FindControl("txtLength").ToTextBox().Text,
					Width = i.FindControl("txtWidth").ToTextBox().Text,
					Height = i.FindControl("txtHeight").ToTextBox().Text,
					Density = i.FindControl("txtDensity").ToTextBox().Text,
					IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
					Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
					PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
					FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
					PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text,
					NMFCCode = i.FindControl("txtNmfcCode").ToTextBox().Text,
					HTSCode = i.FindControl("txtHtsCode").ToTextBox().Text,
					Value = i.FindControl("txtValue").ToTextBox().Text,
					Comment = i.FindControl("txtComment").ToTextBox().Text,
                    HazardousMaterial = i.FindControl("chkHazmat").ToAltUniformCheckBox().Checked
				})
				.ToList();
			vItems.Clear(); // remove blank link from initial setup
			foreach (var item in shipment.Items)
				vItems.Add(new
				{
					item.Description,
					Weight = item.ActualWeight.ToString("f4"),
					Length = item.ActualLength.ToString("f4"),
					Width = item.ActualWidth.ToString("f4"),
					Height = item.ActualHeight.ToString("f4"),
					Density = CalculateDensity(item.ActualWeight, item.ActualLength, item.ActualWidth, item.ActualHeight, item.Quantity),
					item.IsStackable,
					Quantity = item.Quantity.ToString(),
					item.PackageTypeId,
					FreightClass = item.ActualFreightClass,
					PieceCount = item.PieceCount.ToString(),
					item.NMFCCode,
					item.HTSCode,
					Value = item.Value.ToString("f4"),
					item.Comment,
                    item.HazardousMaterial
				});
			lstItems.DataSource = vItems;
			lstItems.DataBind();

			// references
			txtPONumber.Text = shipment.PurchaseOrderNumber;
			txtShipperReference.Text = shipment.ShipperReference;
			var vReferences = lstReferences
				.Items
				.Select(i => new
					{
						DisplayOnOrigin = i.FindControl("hidDisplayOnOrigin").ToCustomHiddenField().Value.ToBoolean(),
						DisplayOnDestination = i.FindControl("hidDisplayOnDestination").ToCustomHiddenField().Value.ToBoolean(),
						Name = i.FindControl("litName").ToLiteral().Text,
						Value = i.FindControl("txtValue").ToTextBox().Text,
						Required = i.FindControl("litRequired").Visible,
					});

			var rs = vReferences.ToList();
			rs.Clear();
			rs.AddRange(shipment.CustomerReferences.Select(r => new { r.DisplayOnOrigin, r.DisplayOnDestination, r.Name, r.Value, r.Required }));
			foreach (var vReference in vReferences.Where(vReference => rs.All(r => r.Name != vReference.Name)))
				rs.Add(vReference);
			lstReferences.DataSource = rs;
			lstReferences.DataBind();

			// haz mat.
			chkHazMat.Checked = shipment.HazardousMaterial;
			txtHazMatName.Text = shipment.HazardousMaterialContactName;
			txtHazMatEmail.Text = shipment.HazardousMaterialContactEmail;
			txtHazMatMobile.Text = shipment.HazardousMaterialContactMobile;
			txtHazMatPhone.Text = shipment.HazardousMaterialContactPhone;
		}

		private void RateSelectionedLockDown(bool @lock, ServiceMode mode)
		{
			txtCustomerNumber.ReadOnly = @lock;
			imgCustomerSearch.Enabled = !@lock;

			operationsAddressInputOrigin.SetRatingEditStatus(!@lock);
			operationsAddressInputDestination.SetRatingEditStatus(!@lock);

			ddlShipmentDate.Enabled = !@lock;

			chkBypassLinearFootRule.Enabled = !@lock;
			foreach (var item in lstItems.Items)
			{
				item.FindControl("txtWeight").ToTextBox().Enabled = !@lock;
				item.FindControl("txtLength").ToTextBox().Enabled = !@lock;
				item.FindControl("txtWidth").ToTextBox().Enabled = !@lock;
				item.FindControl("txtHeight").ToTextBox().Enabled = !@lock;
				item.FindControl("txtQuantity").ToTextBox().Enabled = !@lock;
				if (mode == ServiceMode.SmallPackage) item.FindControl("ddlPackageType").ToCachedObjectDropDownList().Enabled = !@lock;
				else item.FindControl("ddlPackageType").ToCachedObjectDropDownList().Enabled = true;
				item.FindControl("ddlFreightClass").ToDropDownList().Enabled = !@lock;
				item.FindControl("txtValue").ToTextBox().Enabled = !@lock;
				item.FindControl("chkStackable").ToAltUniformCheckBox().Enabled = !@lock;
				item.FindControl("ibtnItemDelete").ToImageButton().Enabled = !@lock;
				item.FindControl("chkHazmat").ToAltUniformCheckBox().Enabled = !@lock;
			}

			lnkAddItem.Enabled = !@lock;
		    lnkAddItem.OnClientClick = @lock ? string.Empty : "ShowProgressNotice(true);";
			lnkAddItemFromLibrary.Enabled = !@lock;

			foreach (RepeaterItem item in rptServices.Items)
				item.FindControl("chkService").ToCheckBox().Enabled = !@lock;

			chkDeclineInsurance.Enabled = !@lock;

			chkHazMat.Enabled = !@lock;
		}

		private void LoadCustomerCustomFields(Customer customer)
		{
			lstReferences.DataSource = customer.CustomFields
				.Select(f => new Reference
				{
					Name = f.Name,
					DisplayOnDestination = f.DisplayOnDestination,
					DisplayOnOrigin = f.DisplayOnOrigin,
					Required = f.Required,
					Value = string.Empty
				})
				.OrderBy(r => r.Name)
				.ToList();
			lstReferences.DataBind();
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			new RateAndScheduleHandler(this).Initialize();

			// configure

			pnlHazMat.Visible = chkHazMat.Checked;

			chkBypassLinearFootRule.Visible = ActiveUser.TenantEmployee;
			pnlBypassLinearFoot.Visible = ActiveUser.TenantEmployee;
            
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
            searchProfiles.GetProfile = GetProfileDelegate;
            if (IsPostBack) return;

		    pecPayment.PaymentGatewayType = ActiveUser.Tenant.PaymentGatewayType;

			txtCustomerNumber.Focus();

			hidTransferToConfirmation.Value = false.ToString();

			hypTermsAndConditions.NavigateUrl = !string.IsNullOrEmpty(ActiveUser.Tenant.TermsAndConditionsFile) &&
												ActiveUser.Tenant.TermsAndConditionsFile.Length > 0
													? ActiveUser.Tenant.TermsAndConditionsFile
													: string.Empty;
			litNoticeText.Text = ActiveUser.Tenant.AutoRatingNoticeText;

			aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

			if (Loading != null)
				Loading(this, new EventArgs());

			// New Record Setup
			InitialDataSetup();
        
            if (Session[WebApplicationConstants.EstimateRateShipmentTransfer] != null)
			{
				var shipment = Session[WebApplicationConstants.EstimateRateShipmentTransfer] as Shipment;
				Session[WebApplicationConstants.EstimateRateShipmentTransfer] = null;
				LoadTransferShipment(shipment);
			}

			if (Session[WebApplicationConstants.CustomerLoadDashboardShipmentTransfer] != null)
			{
				var shipment = Session[WebApplicationConstants.CustomerLoadDashboardShipmentTransfer] as Shipment;
				Session[WebApplicationConstants.CustomerLoadDashboardShipmentTransfer] = null;
				LoadTransferShipment(shipment);
			}
		}


        protected void OnToolbarCommandClicked(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case GenerateRateAndSchedulePreauthorization:
                    var shipment = RetrieveShipment();
                    documentDisplay.Title = "Rate And Schedule Preauthorization";
                    documentDisplay.DisplayHtml(this.GenerateRateAndSchedulePreAuthorization(shipment));
                    documentDisplay.Visible = true;
                    break;
                
            }
        }


		protected void OnCustomerNumberEntered(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtCustomerNumber.Text))
			{
				txtCustomerName.Text = string.Empty;
				addressBookFinder.CustomerIdSpecificFilter = default(long);
				operationsAddressInputOrigin.SetCustomerIdSpecificFilter(default(long));
				operationsAddressInputDestination.SetCustomerIdSpecificFilter(default(long));
				return;
			}

			litErrorMessages.Text = string.Empty;
			if (CustomerSearch != null)
				CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}

		protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
		{
			customerFinder.Visible = false;
		}

		protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
		{
			DisplayCustomer(e.Argument);
			customerFinder.Visible = false;
		}

		protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
		{
			customerFinder.Visible = true;
		}


		private string CalculateDensity(decimal weight, decimal length, decimal width, decimal height, int qty)
		{
			if (length != default(long) && width != default(long) && height != default(long) && qty != default(int))
			{
				return chkEnglish.Checked
						? ((weight / (length.InToFt() * width.InToFt() * height.InToFt())) / qty).ToString("n2")
						: ((weight / (length.CmToM() * width.CmToM() * height.CmToM())) / qty).ToString("n2");
			}
			return string.Empty;
		}

		protected void OnItemsItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var item = e.Item as ListViewDataItem;
			if (item == null) return;

			var ddlFreightClass = item.FindControl("ddlFreightClass").ToDropDownList();

			var freightClasses = WebApplicationSettings.FreightClasses
				.Select(fc => new ViewListItem(fc.ToString(), fc.ToString()))
				.ToList();
			freightClasses.Insert(0, new ViewListItem(string.Empty, string.Empty));
			ddlFreightClass.DataSource = freightClasses;
			ddlFreightClass.DataBind();

			if (item.DataItem.HasGettableProperty("FreightClass"))
			{
				var value = item.DataItem.GetPropertyValue("FreightClass").ToString();
				if (ddlFreightClass.ContainsValue(value)) ddlFreightClass.SelectedValue = value;
				else ddlFreightClass.SelectedIndex = 0;
			}

			var weight = e.Item.FindControl("txtWeight").ToTextBox();
			var length = e.Item.FindControl("txtLength").ToTextBox();
			var width = e.Item.FindControl("txtWidth").ToTextBox();
			var height = e.Item.FindControl("txtHeight").ToTextBox();
			var density = e.Item.FindControl("txtDensity").ToTextBox();
			var qty = e.Item.FindControl("txtQuantity").ToTextBox();

			if (weight != null && length != null && width != null && height != null && density != null && qty != null)
			{
				var wgt = weight.Text.ToDecimal();
				var l = length.Text.ToDecimal();
				var w = width.Text.ToDecimal();
				var h = height.Text.ToDecimal();
				var q = qty.Text.ToInt();

				var densityValue = CalculateDensity(wgt, l, w, h, q);
				density.Text = densityValue;

				const string format = "javascript:UpdateDensity('{0}', '{1}', '{2}', '{3}', '{4}', '', '{5}');";
				weight.Attributes.Add("onblur", string.Format(format, weight.ClientID, length.ClientID, width.ClientID, height.ClientID, density.ClientID, qty.ClientID));
                length.Attributes.Add("onblur", string.Format(format, weight.ClientID, length.ClientID, width.ClientID, height.ClientID, density.ClientID, qty.ClientID));
                width.Attributes.Add("onblur", string.Format(format, weight.ClientID, length.ClientID, width.ClientID, height.ClientID, density.ClientID, qty.ClientID));
                height.Attributes.Add("onblur", string.Format(format, weight.ClientID, length.ClientID, width.ClientID, height.ClientID, density.ClientID, qty.ClientID));
                qty.Attributes.Add("onblur", string.Format(format, weight.ClientID, length.ClientID, width.ClientID, height.ClientID, density.ClientID, qty.ClientID));
			}

		}


		private void AddSingleBlankItem()
		{
			var vItems = lstItems.Items
								 .Select(i => new
									 {
										 Description = i.FindControl("txtDescription").ToTextBox().Text,
										 Weight = i.FindControl("txtWeight").ToTextBox().Text,
										 Length = i.FindControl("txtLength").ToTextBox().Text,
										 Width = i.FindControl("txtWidth").ToTextBox().Text,
										 Height = i.FindControl("txtHeight").ToTextBox().Text,
										 Density = i.FindControl("txtDensity").ToTextBox().Text,
										 IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
										 Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
										 PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
										 FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
										 PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text,
										 NMFCCode = i.FindControl("txtNmfcCode").ToTextBox().Text,
										 HTSCode = i.FindControl("txtHtsCode").ToTextBox().Text,
										 Value = i.FindControl("txtValue").ToTextBox().Text,
										 Comment = i.FindControl("txtComment").ToTextBox().Text,
                                         HazardousMaterial = i.FindControl("chkHazmat").ToAltUniformCheckBox().Checked,
									 })
								 .ToList();

			vItems.Add(new
				{
					Description = string.Empty,
					Weight = string.Empty,
					Length = string.Empty,
					Width = string.Empty,
					Height = string.Empty,
					Density = string.Empty,
					IsStackable = false,
					Quantity = string.Empty,
					PackageTypeId = default(long),
					FreightClass = 0d,
					PieceCount = string.Empty,
					NMFCCode = string.Empty,
					HTSCode = string.Empty,
					Value = string.Empty,
					Comment = string.Empty,
                    HazardousMaterial = false
				});

			lstItems.DataSource = vItems;
			lstItems.DataBind();
		}

		protected void OnAddItemClicked(object sender, EventArgs e)
		{
			AddSingleBlankItem();
		}

		protected void OnDeleteItemClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;
			var rowItemIndex = button.FindControl("hidItemIndex").ToCustomHiddenField().Value.ToLong();


			var vItems = lstItems.Items
								 .Select(i => new
									 {
										 Description = i.FindControl("txtDescription").ToTextBox().Text,
										 Weight = i.FindControl("txtWeight").ToTextBox().Text,
										 Length = i.FindControl("txtLength").ToTextBox().Text,
										 Width = i.FindControl("txtWidth").ToTextBox().Text,
										 Height = i.FindControl("txtHeight").ToTextBox().Text,
										 Density = i.FindControl("txtDensity").ToTextBox().Text,
										 IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
										 Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
										 PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
										 FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue.ToDouble(),
										 PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text,
										 NMFCCode = i.FindControl("txtNmfcCode").ToTextBox().Text,
										 HTSCode = i.FindControl("txtHtsCode").ToTextBox().Text,
										 Value = i.FindControl("txtValue").ToTextBox().Text,
										 Comment = i.FindControl("txtComment").ToTextBox().Text,
                                         HazardousMaterial = i.FindControl("chkHazmat").ToAltUniformCheckBox().Checked,

										 rowItemIndex = i.FindControl("hidItemIndex").ToCustomHiddenField().Value.ToLong()
									 })
								 .Where(item => item.rowItemIndex != rowItemIndex)
								 .ToList();

			lstItems.DataSource = vItems;
			lstItems.DataBind();

		}


		protected void OnAddLibraryItemClicked(object sender, EventArgs e)
		{
			libraryItemFinder.Visible = true;
		}

		protected void OnLibraryItemFinderMultiItemSelected(object sender, ViewEventArgs<List<LibraryItem>> e)
		{
			var items = lstItems.Items
								.Select(i => new
									{
										Description = i.FindControl("txtDescription").ToTextBox().Text,
										Weight = i.FindControl("txtWeight").ToTextBox().Text,
										Length = i.FindControl("txtLength").ToTextBox().Text,
										Width = i.FindControl("txtWidth").ToTextBox().Text,
										Height = i.FindControl("txtHeight").ToTextBox().Text,
										Density = i.FindControl("txtDensity").ToTextBox().Text,
										IsStackable = i.FindControl("chkStackable").ToAltUniformCheckBox().Checked,
										Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
										PackageTypeId = i.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
										FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue,
										PieceCount = i.FindControl("txtPieceCount").ToTextBox().Text,
										NMFCCode = i.FindControl("txtNmfcCode").ToTextBox().Text,
										HTSCode = i.FindControl("txtHtsCode").ToTextBox().Text,
										Value = i.FindControl("txtValue").ToTextBox().Text,
										Comment = i.FindControl("txtComment").ToTextBox().Text,
                                        HazardousMaterial = i.FindControl("chkHazmat").ToAltUniformCheckBox().Checked
									})
								.ToList();

			for (var i = items.Count - 1; i >= 0; i--)
			{
				var item = items[i];
				if (string.IsNullOrEmpty(item.Description) && string.IsNullOrEmpty(item.Weight) &&
					string.IsNullOrEmpty(item.Length) && string.IsNullOrEmpty(item.Width) &&
					string.IsNullOrEmpty(item.Height) && string.IsNullOrEmpty(item.Quantity) &&
					item.PackageTypeId == default(long) && string.IsNullOrEmpty(item.FreightClass) &&
					string.IsNullOrEmpty(item.PieceCount) && string.IsNullOrEmpty(item.NMFCCode) &&
					string.IsNullOrEmpty(item.HTSCode) && string.IsNullOrEmpty(item.Value) &&
					string.IsNullOrEmpty(item.Comment))
					items.RemoveAt(i);
			}

			var itemsToAdd = e.Argument
								.Select(t => new
								{
									t.Description,
									Weight = t.Weight.ToString(),
									Length = t.Length.ToString(),
									Width = t.Width.ToString(),
									Height = t.Height.ToString(),
									Density = CalculateDensity(t.Weight, t.Length, t.Width, t.Height, t.Quantity),
									t.IsStackable,
									Quantity = t.Quantity.ToString(),
									t.PackageTypeId,
									FreightClass = t.FreightClass.ToString(),
									PieceCount = t.PieceCount.ToString(),
									t.NMFCCode,
									t.HTSCode,
									Value = 0.ToString(),
									t.Comment,
                                    t.HazardousMaterial
								})
			.ToList();

			items.AddRange(itemsToAdd);

			lstItems.DataSource = items;
			lstItems.DataBind();

			libraryItemFinder.Visible = false;
		}

		protected void OnLibraryItemFinderItemCancelled(object sender, EventArgs e)
		{
			libraryItemFinder.Visible = false;
		}


		private void ProcessRateSelection(Rate rate)
		{
			hidAutoRated.Value = (rate != null).ToString();

            //reload set of extensible actions
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
            
			var charges = rate != null ? rate.Charges : new List<RateCharge>();

			lstCharges.DataSource = charges.Select(c => new
			{
				c.ChargeCodeId,
				c.UnitBuy,
				c.UnitSell,
				c.UnitDiscount,
				c.ChargeCode.Code,
				c.ChargeCode.Description,
				Category = c.ChargeCode.Category.FormattedString(),
				c.Quantity,
				c.AmountDue,
				c.Comment
			});
			lstCharges.DataBind();


			var totalDue = charges.Sum(c => c.AmountDue);
			CheckForCreditAlert(totalDue);
			txtTotal.Text = totalDue.ToString("f2");
			txtFreight.Text = charges
				.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight)
				.Sum(c => c.AmountDue)
				.ToString("f2");
			txtFuel.Text = charges
				.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Fuel)
				.Sum(c => c.AmountDue)
				.ToString("f2");
			txtAccessorial.Text = charges
				.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Accessorial)
				.Sum(c => c.AmountDue)
				.ToString("f2");
			txtOther.Text = charges
				.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Service)
				.Sum(c => c.AmountDue)
				.ToString("f2");

			// set charge related items

			txtServiceMode.Text = rate == null ? string.Empty : rate.Mode.FormattedString();
			hidServiceMode.Value = rate == null ? string.Empty : rate.Mode.ToString();

			hidOriginal.Value = rate == null ? string.Empty : rate.OriginalRateValue.ToString();
			hidDiscountTierId.Value = rate == null ? string.Empty : rate.DiscountTierId.ToString();
			hidCFCRuleId.Value = rate == null ? string.Empty : rate.CFCRuleId.ToString();
			hidLTLSellRateId.Value = rate == null ? string.Empty : rate.LTLSellRateId.ToString();
			hidLTLPackageSpecificRate.Value = rate == null ? string.Empty : rate.IsLTLPackageSpecificRate.ToString();
			hidFreightProfitAdjustment.Value = rate == null ? string.Empty : rate.FreightProfitAdjustment.ToString();
			hidFuelProfitAdjustment.Value = rate == null ? string.Empty : rate.FuelProfitAdjustment.ToString();
			hidServiceProfitAdjustment.Value = rate == null ? string.Empty : rate.ServiceProfitAdjustment.ToString();
			hidAccessorialProfitAdjustment.Value = rate == null ? string.Empty : rate.AccessorialProfitAdjustment.ToString();
			hidResellerAcc.Value = rate == null ? string.Empty : rate.ResellerAccessorialMarkup.ToString();
			hidResellerAccIsPercent.Value = rate == null ? string.Empty : rate.ResellerAccessorialMarkupIsPercent.ToString();
			hidResellerFr.Value = rate == null ? string.Empty : rate.ResellerFreightMarkup.ToString();
			hidResellerFrIsPercent.Value = rate == null ? string.Empty : rate.ResellerFreightMarkupIsPercent.ToString();
			hidResellerFu.Value = rate == null ? string.Empty : rate.ResellerFuelMarkup.ToString();
			hidResellerFuIsPercent.Value = rate == null ? string.Empty : rate.ResellerFuelMarkupIsPercent.ToString();
			hidResellerSer.Value = rate == null ? string.Empty : rate.ResellerServiceMarkup.ToString();
			hidResellerSerIsPercent.Value = rate == null ? string.Empty : rate.ResellerServiceMarkupIsPercent.ToString();
			hidDirectPoint.Value = rate == null ? string.Empty : rate.DirectPointRate.ToString();
			hidApplyDiscount.Value = rate == null ? string.Empty : rate.ApplyDiscount.ToString();
			hidSmallPackType.Value = rate == null ? string.Empty : rate.SmallPackServiceType;
			hidSmallPackageEngine.Value = rate == null ? string.Empty : rate.SmallPackEngine.ToString();
			hidBilledWeight.Value = rate == null ? string.Empty : rate.BilledWeight.ToString();
			hidRatedWeight.Value = rate == null ? string.Empty : rate.RatedWeight.ToString();
			hidRatedCubicFeet.Value = rate == null ? string.Empty : rate.RatedCubicFeet.ToString();
		    hidRatedPcf.Value = rate == null ? string.Empty : rate.RatedPcf.ToString();
			hidFuPercent.Value = rate == null ? string.Empty : rate.FuelMarkupPercent.ToString();
			hidOriginTerminalCode.Value = rate == null ? string.Empty : rate.OriginTerminalCode;
			hidDestinationTerminalCode.Value = rate == null ? string.Empty : rate.DestinationTerminalCode;
		    hidProject44QuoteNumber.Value = rate == null ? string.Empty : rate.Project44QuoteNumber;
		    hidProject44ServiceCode.Value = rate == null || !rate.IsProject44Rate || (!rate.IsExpedited && !rate.IsGuaranteed)
		        ? string.Empty
		        : rate.IsExpedited
		            ? rate.ExpeditedCharges.First().ChargeCode.Project44Code.ToString()
		            : rate.GuaranteedCharges.First().ChargeCode.Project44Code.ToString();
		    hidProject44GuaranteedCharge.Value = rate == null 
		        ? string.Empty
		        : rate.GuaranteedCharges.Any(c => c.IsProject44GuaranteedCharge) 
		            ? rate.GuaranteedCharges.First().ChargeCode.Project44Code.ToString() 
		            : string.Empty;
		    hidProject44RateSelected.Value = rate == null ? false.ToString() : rate.IsProject44Rate.ToString();


            hidMarkupValue.Value = rate == null ? string.Empty : rate.MarkupValue.ToString();
			hidMarkupPercent.Value = rate == null ? string.Empty : rate.MarkupPercent.ToString();


		    hidIsExpeditedService.Value = rate == null ? string.Empty : rate.IsExpedited.ToString();
		    hidExpeditedServiceRate.Value = rate == null ? string.Empty : rate.GetExpeditedRate().ToString();
		    hidExpeditedServiceTime.Value = rate.GetExpeditedRateTime();
		    hidProject44ExpeditedCharge.Value = rate == null
		        ? string.Empty
		        : rate.ExpeditedCharges.Any()
		            ? rate.ExpeditedCharges.First().ChargeCode.Project44Code.ToString()
		            : string.Empty;

            hidGuaranteedDeliveryServiceTime.Value = rate.GetGuaranteedRateTime();

            hidIsGuaranteedDeliveryService.Value = rate == null ? string.Empty : rate.IsGuaranteed.ToString();

		    hidGuaranteedDeliveryServiceRate.Value = rate.GetGuaranteedRate().ToString();

		    hidGuaranteedDeliveryServiceRateType.Value = rate.GetGuaranteedRateType().ToString();

		    hidLTLGuarateedChargeId.Value = rate == null
		                                        ? default(long).ToString()
		                                        : !rate.IsProject44Rate && rate.GuaranteedCharges.Any()
		                                              ? rate.GuaranteedCharges.First().LTLGuaranteedChargeId.ToString()
		                                              : default(long).ToString();

			hidMileageSourceId.Value = string.Empty;
			txtMileage.Text = string.Empty;
			txtMileageSource.Text = string.Empty;
			pnlTLMileage.Visible = false;

			const string boldFont = "font-weight:bold;";
			const string normalFont = "font-weight:normal;";
			if (rate != null)
			{
				btnRate.Attributes["style"] = normalFont;
				btnBook.Attributes["style"] = boldFont;
				switch (rate.Mode)
				{
					case ServiceMode.SmallPackage:
						txtEstimatedDelivery.Text = rate.TransitDays == Rate.DefaultTransitTime ? string.Empty : rate.EstimatedDeliveryDate.FormattedShortDate();
						txtCarrier.Text = rate.Vendor.Name;
						hidVendorId.Value = rate.Vendor.Id.ToString();
						hidVendorScac.Value = rate.Vendor.Scac;
						btnBook.Text = "Book Shipment";
						pnlDeliveryWindow.Visible = false;
						pnlNoticeText.Visible = true;
						btnSendToSpecialist.Visible = false;
						btnClearRate.Visible = true;
						break;
					case ServiceMode.LessThanTruckload:
						txtEstimatedDelivery.Text = rate.TransitDays == Rate.DefaultTransitTime ? string.Empty : rate.EstimatedDeliveryDate.FormattedShortDate();
						txtCarrier.Text = rate.LTLSellRate.VendorRating.Vendor.Name;
						hidVendorId.Value = rate.LTLSellRate.VendorRating.Vendor.Id.ToString();
						hidVendorScac.Value = rate.LTLSellRate.VendorRating.Vendor.Scac;
						btnBook.Text = "Schedule Pickup";
						pnlDeliveryWindow.Visible = false;
						pnlNoticeText.Visible = true;
						btnSendToSpecialist.Visible = false;
						btnClearRate.Visible = true;
						break;
					case ServiceMode.Truckload:
						txtEstimatedDelivery.Text = string.Empty;
						txtCarrier.Text = "Contract Rate";
						hidVendorId.Value = string.Empty;
						hidVendorScac.Value = string.Empty;
						btnBook.Text = "Accept Quote";
						pnlDeliveryWindow.Visible = true;
						pnlNoticeText.Visible = true;
						btnSendToSpecialist.Visible = false;
						btnClearRate.Visible = true;

						var source = new MileageSource(rate.MileageSourceId);
						txtMileage.Text = rate.Mileage.ToString();
						txtMileageSource.Text = string.Format("{0} - {1}", source.Code, source.Description);
						hidMileageSourceId.Value = rate.MileageSourceId.ToString();
						pnlTLMileage.Visible = true;

						break;
					default:
						txtEstimatedDelivery.Text = string.Empty;
						txtCarrier.Text = string.Empty;
						hidVendorId.Value = string.Empty;
						hidVendorScac.Value = string.Empty;
						btnBook.Text = "Send to Specialist";
						pnlDeliveryWindow.Visible = false;
						btnRate.Attributes["style"] = boldFont;
						btnBook.Attributes["style"] = normalFont;
						pnlNoticeText.Visible = false;
						btnSendToSpecialist.Visible = true;
						btnClearRate.Visible = false;
						break;
				}
			}
			else
			{
				pnlDeliveryWindow.Visible = false;
				pnlNoticeText.Visible = false;
				btnBook.Text = "Send to Specialist";
				txtEstimatedDelivery.Text = string.Empty;
				btnRate.Attributes["style"] = boldFont;
				btnBook.Attributes["style"] = normalFont;
				btnClearRate.Visible = false;

				txtCarrier.Text = string.Empty;
				hidVendorId.Value = string.Empty;
			}

			// show details of rate
			pnlSelectedRateDetails.Visible = rate != null;

            var customer = new Customer(hidCustomerId.Value.ToLong());
            btnPayAndBook.Visible = rate != null && customer.CanPayByCreditCard;
            btnSaveAsLoadOrder.Visible = rate != null && ActiveUser.HasAccessTo(ViewCode.CanSubmitRatedQuote);
            btnBook.Visible = rate != null && !customer.IsCashOnly;
            pnlTermsAndConditionsCheckbox.Visible = rate != null && (btnBook.Visible || btnPayAndBook.Visible || btnSaveAsLoadOrder.Visible);
            pnlCashOnlyCustomerNote.Visible = rate != null && !btnBook.Visible && !btnPayAndBook.Visible && !btnSaveAsLoadOrder.Visible;

			if (rate != null) btnBook.Focus();
		}

		private void CheckForCreditAlert(decimal charges)
		{
			return;
			// temporarily disabled feature v4.0.18.2
			var credit = hidCustomerCredit.Value.ToDecimal();
			var outstanding = hidCustomerOutstandingBalance.Value.ToDecimal();
			var alertThreshold = ProcessorVars.CustomerCreditAlertThreshold[ActiveUser.TenantId];
			pnlCreditAlert.Visible = credit.CreditAlert(outstanding + charges, alertThreshold);
		}

		protected void OnClearRateClicked(object sender, EventArgs e)
		{
			ProcessRateSelection(null);
			RateSelectionedLockDown(false, ServiceMode.SmallPackage);
		}

		protected void OnRateClicked(object sender, EventArgs e)
		{
			if (!ValidateShipmentDateAndTime()) return;

			var shipment = GenerateShipment();

            if(shipment.Items.Any(i => i.HazardousMaterial))
		    {
		        chkHazMat.Checked = true;
		        pnlHazMat.Visible = true;

		        var hazmatServiceId = shipment.Tenant.HazardousMaterialServiceId;
                foreach (RepeaterItem item in rptServices.Items)
				{
					var hid = item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong();
					if (hid == hazmatServiceId)
						item.FindControl("chkService").ToCheckBox().Checked = true;
				}
		    }

			rateSelectionDisplay.Visible = true;
			rateSelectionDisplay.Rate(shipment);
		}

        protected void OnRateSelectionDisplaySelectionCancelled(object sender, ViewEventArgs<RateSelectionFieldset> e)
		{
			rateSelectionDisplay.Visible = false;
			btnSendToSpecialist.Visible = true;

            // highlight fieldset if applicable

            if (e.Argument == RateSelectionFieldset.NotApplicable) return;

            foreach (var item in lstItems.Items)
            {
                var txtQuantity = item.FindControl("txtQuantity").ToTextBox();
                var txtLength = item.FindControl("txtLength").ToTextBox();
                var txtWidth = item.FindControl("txtWidth").ToTextBox();
                var txtHeight = item.FindControl("txtHeight").ToTextBox();
                var ddlPackageType = item.FindControl("ddlPackageType").ToCachedObjectDropDownList();
                var ddlFreightClass = item.FindControl("ddlFreightClass").ToDropDownList();

                if (!txtQuantity.CssClass.Contains("requiredInput")) txtQuantity.CssClass += " requiredInput";

                switch (e.Argument)
                {
                    case RateSelectionFieldset.LtlFreightClass:
                        if (!ddlFreightClass.CssClass.Contains("requiredInput")) ddlFreightClass.CssClass += " requiredInput";
                        ddlPackageType.CssClass = ddlPackageType.CssClass.Replace("requiredInput", string.Empty);
                        goto highlightDimensions;

                    case RateSelectionFieldset.LtlDensity:
                        ddlPackageType.CssClass = ddlPackageType.CssClass.Replace("requiredInput", string.Empty);
                        ddlFreightClass.CssClass = ddlFreightClass.CssClass.Replace("requiredInput", string.Empty);
                        goto highlightDimensions;

                    case RateSelectionFieldset.SmallPackage:
                        if (!ddlPackageType.CssClass.Contains("requiredInput")) ddlPackageType.CssClass += " requiredInput";
                        ddlFreightClass.CssClass = ddlFreightClass.CssClass.Replace("requiredInput", string.Empty);
                        goto highlightDimensions;
                    
                    default:
                        ddlPackageType.CssClass = ddlPackageType.CssClass.Replace("requiredInput", string.Empty);
                        ddlFreightClass.CssClass = ddlFreightClass.CssClass.Replace("requiredInput", string.Empty);
                        txtLength.CssClass = txtLength.CssClass.Replace("requiredInput", string.Empty);
                        txtWidth.CssClass = txtWidth.CssClass.Replace("requiredInput", string.Empty);
                        txtHeight.CssClass = txtHeight.CssClass.Replace("requiredInput", string.Empty);
                        break;
                    
                    highlightDimensions:
                        if (!txtLength.CssClass.Contains("requiredInput")) txtLength.CssClass += " requiredInput";
                        if (!txtWidth.CssClass.Contains("requiredInput")) txtWidth.CssClass += " requiredInput";
                        if (!txtHeight.CssClass.Contains("requiredInput")) txtHeight.CssClass += " requiredInput";
                        break;
                }
            }
		}

		protected void OnRateSelectionDisplayRateSelected(object sender, ViewEventArgs<Rate> e)
		{
			ProcessRateSelection(e.Argument);
			RateSelectionedLockDown(true, e.Argument.Mode);
			rateSelectionDisplay.Visible = false;
			ScrollToBottom = true;
		}

		protected void OnRateSelectionDisplayRatesLoaded(object sender, ViewEventArgs<ShoppedRate> e)
		{
			if (SaveShoppedRate != null)
				SaveShoppedRate(this, new ViewEventArgs<ShoppedRate>(e.Argument));
		}

        protected void OnRateSelectionDisplayDownloadRates(object sender, ViewEventArgs<bool, List<Rate>> e)
		{
			var title = string.Format("{0}_RatesDownload.pdf", Guid.NewGuid());
            Response.Export(this.GenerateRateDownload(GenerateShipment(), e.Argument2, e.Argument).ToPdf(title), title);
		}


		

		protected void OnAddressInputFindPostalCode(object sender, ViewEventArgs<string> e)
		{
			var addressinput = (OperationsAddressInputControlRandS)sender;
			hidFlag.Value = addressinput.Type.GetString();
			postalCodeFinder.Visible = true;
			postalCodeFinder.SearchOn(e.Argument);
		}

		protected void OnAddressInputAddAddressFromAddressBook(object sender, ViewEventArgs<string> e)
		{
			var addressinput = (OperationsAddressInputControlRandS) sender;
			hidFlag.Value = addressinput.Type.GetString();
			addressBookFinder.Visible = true;
			addressBookFinder.SearchOn(e.Argument);
		}


        protected void OnSaveAsLoadOrderClicked(object sender, EventArgs e)
        {
            var quotedLoad = RetrieveLoadOrder(LoadOrderStatus.Quoted);

            var vendor = new Vendor(hidVendorId.Value.ToLong());
            quotedLoad.Vendors.Add(new LoadOrderVendor
            {
                LoadOrder = quotedLoad,
                TenantId = quotedLoad.TenantId,
                Vendor = vendor,
                Primary = true,
                ProNumber = string.Empty,
            });

            if (SaveLoadOrder != null)
                SaveLoadOrder(this, new ViewEventArgs<LoadOrder, PaymentInformation>(quotedLoad, null));

            if (hidTransferToConfirmation.Value.ToBoolean())
                Response.Redirect(RateAndScheduleConfirmationView.PageAddress);
        }

		protected void OnBookClicked(object sender, EventArgs e)
		{
		    if (new Customer(hidCustomerId.Value.ToLong()).RateAndScheduleInDemo)
		        hidFlag.Value = DemoProcessingFlag;

			if (string.IsNullOrEmpty(hidServiceMode.Value))
			{
				var loadOrder = RetrieveLoadOrder(LoadOrderStatus.Offered);

				if (SaveLoadOrder != null)
					SaveLoadOrder(this, new ViewEventArgs<LoadOrder, PaymentInformation>(loadOrder, null));

				if (hidTransferToConfirmation.Value.ToBoolean())
					Response.Redirect(RateAndScheduleConfirmationView.PageAddress);

				return;
			}

			// TODO: if re-instatement of stop process for credit is done, it should happen here!

			switch (hidServiceMode.Value.ToEnum<ServiceMode>())
			{
				case ServiceMode.LessThanTruckload:
				case ServiceMode.SmallPackage:
					var shipment = RetrieveShipment();
					if (SaveShipment != null)
						SaveShipment(this, new ViewEventArgs<Shipment, PaymentInformation>(shipment, null));
					break;
				case ServiceMode.Truckload:
					var quotedLoad = RetrieveLoadOrder(LoadOrderStatus.Quoted);
					if (SaveLoadOrder != null)
						SaveLoadOrder(this, new ViewEventArgs<LoadOrder, PaymentInformation>(quotedLoad, null));
					break;
				default:
					var offeredLoad = RetrieveLoadOrder(LoadOrderStatus.Offered);
					if (SaveLoadOrder != null)
						SaveLoadOrder(this, new ViewEventArgs<LoadOrder, PaymentInformation>(offeredLoad, null));
					break;
			}

			if (hidTransferToConfirmation.Value.ToBoolean())
				Response.Redirect(RateAndScheduleConfirmationView.PageAddress);
		}

		protected void OnSendToSpecialistClicked(object sender, EventArgs e)
		{
			if (!ValidateShipmentDateAndTime()) return;

			var offeredLoad = RetrieveLoadOrder(LoadOrderStatus.Offered);

			// pre-validation to ensure all items have a description before sending to specialist
			if (offeredLoad.Items.Any(i => string.IsNullOrEmpty(i.Description)))
			{
				DisplayMessages(new[] { ValidationMessage.Error("Each item must have a description before sending to specialist") });
				return;
			}

            if (new Customer(hidCustomerId.Value.ToLong()).RateAndScheduleInDemo)
                hidFlag.Value = DemoProcessingFlag;

			if (SaveLoadOrder != null)
				SaveLoadOrder(this, new ViewEventArgs<LoadOrder, PaymentInformation>(offeredLoad, null));

			if (hidTransferToConfirmation.Value.ToBoolean())
				Response.Redirect(RateAndScheduleConfirmationView.PageAddress);
		}


		protected void OnDisableNoticeClicked(object sender, EventArgs e)
		{
			Response.Redirect("~/Members/Administration/UserProfiles.aspx");
		}


		protected void OnHazMatCheckChanged(object sender, EventArgs e)
		{
			pnlHazMat.Visible = chkHazMat.Checked;
			if(chkHazMat.Checked) txtHazMatName.Focus();
			else chkHazMat.Focus();
		}


		protected void OnAddressBookFinderItemCancelled(object sender, EventArgs e)
		{
			addressBookFinder.Visible = false;
			addressBookFinder.Reset();

		}

		protected void OnAddressBookFinderItemSelected(object sender, ViewEventArgs<AddressBook> e)
		{
			switch (hidFlag.Value.ToEnum<OperationsAddressInputType>())
			{
				case OperationsAddressInputType.Origin:
					operationsAddressInputOrigin.LoadAddress(e.Argument);
					break;
				case OperationsAddressInputType.Destination:
					operationsAddressInputDestination.LoadAddress(e.Argument);
					break;
			}
			AddServicesRequired(e.Argument.Services);
			addressBookFinder.Visible = false;
			addressBookFinder.Reset();
		}



		protected void OnPostalCodeFinderItemCancelled(object sender, EventArgs e)
		{
			postalCodeFinder.Visible = false;
			postalCodeFinder.Reset();
		}

		protected void OnPostalCodeFinderItemSelected(object sender, ViewEventArgs<PostalCodeViewSearchDto> e)
		{
			switch (hidFlag.Value.ToEnum<OperationsAddressInputType>())
			{
				case OperationsAddressInputType.Origin:
					operationsAddressInputOrigin.LoadPostalCode(e.Argument);
					break;
				case OperationsAddressInputType.Destination:
					operationsAddressInputDestination.LoadPostalCode(e.Argument);
					break;
			}
			postalCodeFinder.Visible = false;
			postalCodeFinder.Reset();
		}


        protected void OnDocumentDisplayClosed(object sender, EventArgs e)
        {
            documentDisplay.Visible = false;
        }


	    protected void OnPaymentEntryCloseClicked(object sender, EventArgs e)
	    {
            pecPayment.Visible = false;
	    }

	    protected void OnPaymentEntryProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
	    {
            DisplayMessages(e.Argument);
	    }

	    protected void OnPaymentEntryCustomAuthorizeAndCapturePayment(object sender, ViewEventArgs<PaymentInformation> e)
	    {
            if (string.IsNullOrEmpty(hidServiceMode.Value))
            {
                DisplayMessages(new[] { ValidationMessage.Error(CannotProcessPaymentForOfferedLoad) });
                return;
            }

	        hidFlag.Value = ProcessingPaymentFlag;

            switch (hidServiceMode.Value.ToEnum<ServiceMode>())
            {
                case ServiceMode.LessThanTruckload:
                case ServiceMode.SmallPackage:
                    var shipment = RetrieveShipment();
                    if (SaveShipment != null)
                        SaveShipment(this, new ViewEventArgs<Shipment, PaymentInformation>(shipment, e.Argument));
                    break;
                case ServiceMode.Truckload:
                    var quotedLoad = RetrieveLoadOrder(LoadOrderStatus.Quoted);
                    if (SaveLoadOrder != null)
                        SaveLoadOrder(this, new ViewEventArgs<LoadOrder, PaymentInformation>(quotedLoad, e.Argument));
                    break;
                default:
                    DisplayMessages(new[] { ValidationMessage.Error(CannotProcessPaymentForOfferedLoad) });
                    return;
            }

            if (hidTransferToConfirmation.Value.ToBoolean())
                Response.Redirect(RateAndScheduleConfirmationView.PageAddress);
	    }


	    protected void OnPayAndBookClicked(object sender, EventArgs e)
	    {
            if (new Customer(hidCustomerId.Value.ToLong()).RateAndScheduleInDemo)
                hidFlag.Value = DemoProcessingFlag;

            if (string.IsNullOrEmpty(hidServiceMode.Value))
            {
                DisplayMessages(new[] { ValidationMessage.Error(CannotProcessPaymentForOfferedLoad) });
                return;
            }

            switch (hidServiceMode.Value.ToEnum<ServiceMode>())
            {
                case ServiceMode.LessThanTruckload:
                case ServiceMode.SmallPackage:
                    var shipment = RetrieveShipment();
                    pecPayment.LoadShipmentToPayFor(shipment);
                    pecPayment.Visible = true;
                    break;
                case ServiceMode.Truckload:
                    var quotedLoad = RetrieveLoadOrder(LoadOrderStatus.Quoted);
                    pecPayment.LoadLoadOrderToBePaidFor(quotedLoad);
                    pecPayment.Visible = true;
                    break;
                default:
                    DisplayMessages(new []{ValidationMessage.Error(CannotProcessPaymentForOfferedLoad)});
                    break;
            }
	    }

        protected void OnRateAndScheduleProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnRateAndScheduleProfilesProfileSelected(object sender, ViewEventArgs<RateAndScheduleDefaultsItem> e)
        {
            try
            {
                // all units in English
                chkEnglish.Checked = true;
                chkMetric.Checked = false;

                var p = e.Argument;

                // load account
                txtCustomerNumber.Text = p.Shipment.CustomerNumber;
                hidCustomerId.Value = string.Empty;
                CustomerSearch?.Invoke(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
                if (hidCustomerId.Value.ToLong() == default(long)) return; // an error message would have already been display via the regular account search processing.

                var data = e.Argument.Shipment;
                // load addresses
                var location2 = new ShipmentLocation
                {
                    Street1 = data.OriginStreet1,
                    Street2 = data.OriginStreet2,
                    City = data.OriginCity,
                    State = data.OriginState,
                    CountryId = data.OriginCountryId,
                    Description = data.OriginDescription,
                    SpecialInstructions = data.OriginSpecialInstructions,
                    PostalCode = data.OriginPostalCode
                };
                var contact = new ShipmentContact {Name = data.OriginContact, Phone = data.OriginPhone, Primary = true};
                location2.Contacts = new List<ShipmentContact> {contact};
                operationsAddressInputOrigin.LoadAddress(location2);

                location2 = new ShipmentLocation
                {
                    Street1 = data.DestinationStreet1,
                    Street2 = data.DestinationStreet2,
                    City = data.DestinationCity,
                    State = data.DestinationState,
                    CountryId = data.DestinationCountryId,
                    Description = data.DestinationDescription,
                    SpecialInstructions = data.DestinationSpecialInstructions,
                    PostalCode = data.DestinationPostalCode
                };
                contact = new ShipmentContact { Name = data.DestinationContact, Phone = data.DestinationPhone, Primary = true};
                location2.Contacts = new List<ShipmentContact> { contact };

                operationsAddressInputDestination.LoadAddress(location2);
               
                

                // load shipment data
                ddlShipmentDate.SelectedIndex = 1; // select today as shipping date.  First index is choose one!

                chkHazMat.Checked = data.HazardousMaterial;
                pnlHazMat.Visible = chkHazMat.Checked;

                txtHazMatEmail.Text = data.HazardousMaterialContactEmail;
                txtHazMatMobile.Text = data.HazardousMaterialContactMobile;
                txtHazMatName.Text = data.HazardousMaterialContactName;
                txtHazMatPhone.Text = data.HazardousMaterialContactPhone;

                chkEnglish.Checked = data.EnglishInputUnits;
                chkMetric.Checked = data.MetricInputUnits;
                chkBypassLinearFootRule.Checked = data.BypassLinearFootRule;

                txtPONumber.Text = data.PurchaseOrderNumber;
                txtShipperReference.Text = data.ShipperReference;
                
                if (data.EarlyPickup != null && ddlReadyEarly.ContainsValue(data.EarlyPickup))
                    ddlReadyEarly.SelectedValue = data.EarlyPickup;
                if (data.LatePickup != null && ddlReadyLate.ContainsValue(data.LatePickup))
                    ddlReadyLate.SelectedValue = data.LatePickup;
                

                var vItems = p.Shipment.Items
                    .Select(item => new
                    {
                        item.Description,
                        Weight = item.ActualWeight,
                        Length = item.ActualLength,
                        Width = item.ActualWidth,
                        Height = item.ActualHeight,
                        Density = CalculateDensity(item.ActualWeight, item.ActualLength, item.ActualWidth, item.ActualHeight, item.Quantity),
                        item.IsStackable,
                        Quantity = item.Quantity.ToString(),
                        item.PackageTypeId,
                        FreightClass = item.ActualFreightClass,
                        PieceCount = item.PieceCount.ToString(),
                        item.NMFCCode,
                        item.HTSCode,
                        Value = item.Value,
                        item.Comment,
                        item.HazardousMaterial
                    })
                    .ToList();

                lstItems.DataSource = vItems;
                lstItems.DataBind();

                // load services
                foreach (var item in rptServices.Items.Cast<RepeaterItem>())
                {
                    var sc = item.FindControl("hidServiceId").ToCustomHiddenField();
                    var isChecked = data.Services.Any(s => s == sc.Value.ToLong());
                    item.FindControl("chkService").ToCheckBox().Checked = isChecked;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                DisplayMessages(new[] { ValidationMessage.Error(ex.Message) });
            }
        }

    }
}