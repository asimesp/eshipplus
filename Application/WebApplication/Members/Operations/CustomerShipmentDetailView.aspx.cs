﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Operations
{
    public partial class CustomerShipmentDetailView : MemberPageBase
    {
        private const string GenerateShipmentDetailsDocument = "GenerateShipmentDetailsDocument";

        public override ViewCode PageCode { get { return ViewCode.CustomerShipmentDetail; } }

        public static string PageAddress { get { return "~/Members/Operations/CustomerShipmentDetailView.aspx"; } }

        public override string SetPageIconImage
        {
            set
            {
                imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue;
            }
        }


        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var generateShipmentDetailsDocument = new ToolbarMoreAction
            {
                CommandArgs = GenerateShipmentDetailsDocument,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Operations,
                Name = "Generate Shipment Details Document",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var goToShipment = new ToolbarMoreAction
                {
                    ConfirmCommand = false,
                    ImageUrl = IconLinks.Operations,
                    Name = "Go To Shipment",
                    IsLink = true,
                    OpenInNewWindow = true,
                    NavigationUrl = string.Format("{0}?{1}={2}", ShipmentView.PageAddress, WebApplicationConstants.TransferNumber, litShipmentHeader.Text)
                };

            var moreActions = new List<ToolbarMoreAction> { generateShipmentDetailsDocument };

            if(ActiveUser.HasAccessTo(ViewCode.Shipment))
                moreActions.Add(goToShipment);

            return moreActions;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var key = Session[WebApplicationConstants.ShippingLabelShipmentId].ToLong();

            if (key == default(long))
            {
                pnlShipment.Visible = false;
                pnlRecordNotFound.Visible = true;
                return;
            }

            var shipment = new Shipment(key);

            LoadShipment(shipment);

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
        }


        protected void OnToolbarCommandClicked(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case GenerateShipmentDetailsDocument:
                    var shipment = new Shipment(hidShipmentId.Value.ToLong());
                    documentDisplay.Title = string.Format("{0} Shipment Details", shipment.ShipmentNumber);
                    documentDisplay.DisplayHtml(this.GenerateShipmentDetails(shipment));
                    documentDisplay.Visible = true;
                    break;
            }
        }



        private void LoadShipment(Shipment shipment)
        {
            if (!shipment.KeyLoaded)
            {
                pnlShipment.Visible = false;
                return;
            }

            hidShipmentId.Value = shipment.Id.ToString();
            shipment.LoadCollections();

            litShipmentHeader.Text = shipment.ShipmentNumber;
            txtBolNumber.Text = string.Format("{0}{1}",
                                              shipment.Prefix != null && !shipment.HidePrefix
                                                ? shipment.Prefix.Code
                                                : string.Empty,
                                              shipment.ShipmentNumber);
            txtProNumber.Text = shipment.Vendors.First(v => v.Primary).ProNumber;

            var origin = shipment.Origin;
            txtOrigin.Text = origin.Description;
            txtOriginStreet1.Text = origin.Street1;
            txtOriginStreet2.Text = origin.Street2;
            txtOriginCity.Text = origin.City;
            txtOriginState.Text = origin.State;
            txtOriginCountry.Text = origin.Country.Name;
            txtOriginPostalCode.Text = origin.PostalCode;


            var destination = shipment.Destination;
            txtDestination.Text = destination.Description;
            txtDestinationStreet1.Text = destination.Street1;
            txtDestinationStreet2.Text = destination.Street2;
            txtDestinationCity.Text = destination.City;
            txtDestinationState.Text = destination.State;
            txtDestinationCountry.Text = destination.Country.Name;
            txtDestinationPostalCode.Text = destination.PostalCode;

            var originContact = shipment.Origin.Contacts.First(c => c.Primary);
            txtOriginPrimaryContactName.Text = originContact.Name;
            txtOriginPrimaryContactEmail.Text = originContact.Email;
            txtOriginPrimaryContactPhone.Text = originContact.Phone;
            txtOriginPrimaryContactFax.Text = originContact.Fax;

            var destinationContact = shipment.Destination.Contacts.First(c => c.Primary);
            txtDestinationPrimaryContactName.Text = destinationContact.Name;
            txtDestinationPrimaryContactEmail.Text = destinationContact.Email;
            txtDestinationPrimaryContactPhone.Text = destinationContact.Phone;
            txtDestinationPrimaryContactFax.Text = destinationContact.Fax;

            txtShippersReference.Text = shipment.ShipperReference;
            txtPoNumber.Text = shipment.PurchaseOrderNumber;

            txtPickupInstructions.Text = origin.SpecialInstructions;
            txtDeliveryInstructions.Text = destination.SpecialInstructions;

            txtPickupDate.Text = shipment.DesiredPickupDate.FormattedShortDate();
            txtPickupBetweenStart.Text = shipment.EarlyPickup;
            txtPickupBetweenEnd.Text = shipment.LatePickup;
            txtCarrier.Text = shipment.Vendors.First(v => v.Primary).Vendor.Name;
            rptServices.DataSource = shipment.Services.Select(s => new { s.Service.Description });
            rptServices.DataBind();

            var items = shipment.Items
                .Select(i =>
                    new
                    {
                        i.Description,
                        Class = i.ActualFreightClass,
                        Weight = i.ActualWeight.ToString("n2"),
                        Packaging = string.Format("{0} ({1}x{2}x{3})", i.PackageType.TypeName, i.ActualLength.ToString("n0"), i.ActualWidth.ToString("n0"), i.ActualHeight.ToString("n0")),
                        i.Quantity,
                        NMFCHTS = i.NMFCCode != string.Empty && i.HTSCode != string.Empty
                                ? string.Format("{0} / {1}", i.NMFCCode, i.HTSCode)
                                : string.Empty
                    })
                .ToList();

            rptItems.DataSource = items;
            rptItems.DataBind();

            litPleaseNote.Text = shipment.Tenant.AutoRatingNoticeText;

            var charges = shipment
                .Charges
                .Select(c =>
                        new
                            {
                                ChargeCode = c.ChargeCode.Description,
                                c.Quantity,
                                AmountDue = c.AmountDue.ToString("c2")
                            })
                .ToList();

            rptCharges.DataSource = charges;
            rptCharges.DataBind();

            var totalAmountDue = shipment.Charges.Sum(c => c.AmountDue);
            txtTotalCharges.Text = totalAmountDue.ToString("c2");
        }

        protected void OnDocumentDisplayClosed(object sender, EventArgs e)
        {
            documentDisplay.Visible = false;
        }
    }
}
