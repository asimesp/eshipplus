﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public class MemberControlBaseWithControlStore : MemberControlBase
	{
		private Dictionary<string, object> _controlStore = new Dictionary<string, object>();

		public Dictionary<string, object> ControlStore { get { return _controlStore ?? (_controlStore = new Dictionary<string, object>()); } }

		protected override void OnInit(EventArgs e)
		{
			Page.RegisterRequiresControlState(this);
			base.OnInit(e);
		}

		protected override void LoadControlState(object savedState)
		{
			var fromSave = savedState as List<Object>;

			if (fromSave == null) return;
			if (fromSave.Count < 2) return;

			_controlStore = fromSave[1] as Dictionary<string, object>;

			base.LoadControlState(fromSave[0]);
		}

		protected override object SaveControlState()
		{
			var toSave = new List<object>
				{
					base.SaveControlState(),
					ControlStore
				};
			return toSave;
		}
	}
}