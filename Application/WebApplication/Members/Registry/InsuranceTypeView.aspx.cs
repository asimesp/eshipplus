﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class InsuranceTypeView : MemberPageBase, IInsuranceTypeView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/InsuranceTypeView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.InsuranceType; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public event EventHandler<ViewEventArgs<InsuranceType>> Save;
        public event EventHandler<ViewEventArgs<InsuranceType>> Delete;
        public event EventHandler<ViewEventArgs<string>> Search;
        public event EventHandler<ViewEventArgs<InsuranceType>> Lock;
        public event EventHandler<ViewEventArgs<InsuranceType>> UnLock;
        public event EventHandler<ViewEventArgs<List<InsuranceType>>> BatchImport;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<InsuranceType> insuranceTypes)
        {
            litRecordCount.Text = insuranceTypes.BuildRecordCount();
            lvwRegistry.DataSource = insuranceTypes;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<InsuranceType>(new InsuranceType(hidInsuranceTypeId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
        }

        private InsuranceType RetrieveViewInsuranceType()
        {
            var insuranceTypeId = hidInsuranceTypeId.Value.ToLong();

            var insuranceType = new InsuranceType(insuranceTypeId, insuranceTypeId != default(long))
            {
                Code = txtCode.Text,
                Description = txtDescription.Text
            };

            if (insuranceType.IsNew) insuranceType.TenantId = ActiveUser.TenantId;

            return insuranceType;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new InsuranceTypeHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.InsuranceTypesImportTemplate });

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidInsuranceTypeId.Value = string.Empty;
            GeneratePopup("Add Insurance Type");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Insurance Type");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("insuranceTypeId").ToCustomHiddenField();

            var insuranceType = new InsuranceType(hidden.Value.ToLong(), false);

            hidInsuranceTypeId.Value = insuranceType.Id.ToString();
            txtCode.Text = insuranceType.Code;
            txtDescription.Text = insuranceType.Description;

            if (Lock != null)
                Lock(this, new ViewEventArgs<InsuranceType>(insuranceType));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var insuranceType = RetrieveViewInsuranceType();

            if (Save != null)
                Save(this, new ViewEventArgs<InsuranceType>(insuranceType));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("insuranceTypeId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<InsuranceType>(new InsuranceType(hidden.Value.ToLong(), true)));

            DoSearch();
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "INSURANCE TYPE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                    .Select(item => new[]
					             	{
									    (item.FindControl("litCode")).ToLiteral().Text,
										(item.FindControl("litDescription")).ToLiteral().Text
					             	}.TabJoin())
                    .ToList();
            lines.Insert(0, new[] { "Code", "Description" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "InsuranceTypes.txt");
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 2;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var insuranceTypes = lines
                .Select(s => new InsuranceType
                {
                    Code = s[0],
                    Description = s[1],
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<InsuranceType>>(insuranceTypes));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (!CloseEditPopUp) return;

            CloseEditPopup();

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<InsuranceType>(RetrieveViewInsuranceType()));
        }
    }
}
