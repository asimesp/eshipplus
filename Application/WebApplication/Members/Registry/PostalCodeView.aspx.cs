﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class PostalCodeView : AdminMemberPageBase, IPostalCodeView
    {
        private SearchField SortByField
        {
            get { return RegistrySearchFields.PostalCodeSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        private bool _forcePrimary;

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/PostalCodeView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.PostalCode; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public bool ForcePrimary
        {
            get { return _forcePrimary; }
        }

        public List<Country> Countries
        {
            set
            {
                ddlCountries.DataSource = value ?? new List<Country>();
                ddlCountries.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<PostalCode>> Save;
        public event EventHandler<ViewEventArgs<PostalCode>> Delete;
        public event EventHandler<ViewEventArgs<List<PostalCode>>> BatchImport;
        public event EventHandler<ViewEventArgs<PostalCodeViewSearchCriteria>> Search;
        public event EventHandler ExportAllPostalCodes;

        public void DisplaySearchResult(List<PostalCodeViewSearchDto> postalCodes)
        {
            litRecordCount.Text = postalCodes.BuildRecordCount();
            upcseDataUpdate.DataSource = postalCodes;
            upcseDataUpdate.DataBind();
        }

        public void ExportPostalCodes(List<PostalCodeViewSearchDto> codes)
        {
            var lines = codes.Any()
                            ? codes.Select(code => new[]
                                {
                                    code.Code,
                                    code.City,
                                    code.CityAlias,
                                    code.State,
                                    code.CountryCode,
                                    code.Primary.ToString(),
                                    code.Latitude.ToString(),
                                    code.Longitude.ToString()
                                }.TabJoin()).ToList()
                            : lvwPostalCode.Items
                                           .Select(item => new[]
                                               {
                                                   item.FindControl("litCode").ToLiteral().Text,
                                                   item.FindControl("litCity").ToLiteral().Text,
                                                   item.FindControl("litCityAlias").ToLiteral().Text,
                                                   item.FindControl("litState").ToLiteral().Text,
                                                   item.FindControl("hidCountryCode").ToCustomHiddenField().Value,
                                                   item.FindControl("hidPrimary").ToCustomHiddenField().Value,
                                                   item.FindControl("litLatitude").ToLiteral().Text,
                                                   item.FindControl("litLongitude").ToLiteral().Text,
                                               }.TabJoin())
                                           .ToList();

            lines.Insert(0, new[] { "Postal Code", "City", "City Alias", "State", "Country Code", "Primary", "Latitude", "Longitude" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "PostalCodes.txt");
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch(SearchField field)
        {
            var criteria = new PostalCodeViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(),
                SortBy = field,
                SortAscending = rbAsc.Checked
            };

            if (Search != null)
                Search(this, new ViewEventArgs<PostalCodeViewSearchCriteria>(criteria));
        }


        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtCityAlias.Text = string.Empty;
            txtState.Text = string.Empty;
            ddlCountries.SelectedIndex = 0;
            chkPrimary.Checked = true;
            txtLatitude.Text = string.Empty;
            txtLongitude.Text = string.Empty;
        }


        private PostalCode RetrievePostalCode()
        {
            var postalCodeId = hidPostalCodeId.Value.ToLong();

            var postalCode = new PostalCode(postalCodeId, postalCodeId != default(long))
            {
                Code = txtCode.Text,
                City = txtCity.Text,
                CityAlias = txtCityAlias.Text,
                State = txtState.Text,
                CountryId = ddlCountries.SelectedValue.ToLong(),
                Primary = chkPrimary.Checked,
                Latitude = txtLatitude.Text.ToDouble(),
                Longitude = txtLongitude.Text.ToDouble()
            };

            return postalCode;
        }

        private static long GetCountryId(string countryCode, Dictionary<string, long> countryIds, CountrySearch search)
        {
            if (countryIds.ContainsKey(countryCode)) return countryIds[countryCode];
            var id = search.FetchCountryIdByCode(countryCode);
            if (id != default(long)) countryIds.Add(countryCode, id);
            return id;
        }

        
        private void DoSave(PostalCode postalCode)
        {
            if (Save != null)
                Save(this, new ViewEventArgs<PostalCode>(postalCode));

            DoSearch(SortByField);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new PostalCodeHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.PostalCodesImportTemplate });

            if (Loading != null)
                Loading(this, new EventArgs());

            // set sort fields
            ddlSortBy.DataSource = RegistrySearchFields.PostalCodeSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = RegistrySearchFields.Code.Name;

            //link up sort buttons
            lbtnSortPostalCode.CommandName = RegistrySearchFields.Code.Name;
            lbtnSortCity.CommandName = RegistrySearchFields.City.Name;
            lbtnSortCityAlias.CommandName = RegistrySearchFields.CityAlias.Name;
            lbtnSortState.CommandName = RegistrySearchFields.State.Name;
            lbtnSortCountryName.CommandName = RegistrySearchFields.CountryName.Name;

            lstFilterParameters.DataSource = RegistrySearchFields.DefaultPostalCodes.Select(f => f.ToParameterColumn()).ToList();
            lstFilterParameters.DataBind();
        }


        protected void OnNewClicked(object sender, EventArgs e)
        {
            ClearFields();
            hidPostalCodeId.Value = string.Empty;
            GeneratePopup("Add Postal Code");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Postal Code");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidPostalCodeId").ToCustomHiddenField();

            var postalCode = new PostalCode(hidden.Value.ToLong());

            hidPostalCodeId.Value = postalCode.Id.ToString();
            txtCode.Text = postalCode.Code;
            txtCity.Text = postalCode.City;
            txtCityAlias.Text = postalCode.CityAlias;
            txtState.Text = postalCode.State;
            txtLatitude.Text = postalCode.Latitude.ToString();
            txtLongitude.Text = postalCode.Longitude.ToString();

            ddlCountries.SelectedValue = postalCode.CountryId.ToString();

            chkPrimary.Checked = postalCode.Primary;
        }

        protected void OnCloseEditPopupClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var postalCode = RetrievePostalCode();

            if (postalCode.Primary)
            {
                messageBox.Button = MessageButton.YesNo;
                messageBox.Icon = MessageIcon.Question;
                messageBox.Message =
                    @"This Postal Code has been marked primary. This may conflict with an existing primary postal code with the same code in this country. Click yes to set this as the new primary or no the keep the existing primary.";

                messageBox.Visible = true;
                return;
            }

            DoSave(postalCode);
        }

        protected void OnDoNotForcePrimaryPostalCode(object sender, EventArgs e)
        {
            _forcePrimary = false;
            DoSave(RetrievePostalCode());
        }

        protected void OnForcePrimaryPostalCode(object sender, EventArgs e)
        {
            _forcePrimary = true;
            DoSave(RetrievePostalCode());
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidPostalCodeId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<PostalCode>(new PostalCode(hidden.Value.ToLong())));

            DoSearch(SortByField);
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearch(SortByField);
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 8;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countryCodes = ProcessorVars.RegistryCache.Countries.Values.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(4, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countrySearch = new CountrySearch();
            var countryIds = new Dictionary<string, long>();

            var postalCodes = lines
                .Select(s => new PostalCode
                {
                    Code = s[0],
                    City = s[1],
                    CityAlias = s[2],
                    State = s[3],
                    CountryId = GetCountryId(s[4], countryIds, countrySearch),
                    Primary = s[5].ToBoolean(),
                    Latitude = s[6].ToDouble(),
                    Longitude = s[7].ToDouble()
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<PostalCode>>(postalCodes));
            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "POSTAL CODE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }


        protected void OnExportClicked(object sender, EventArgs e)
        {
            pnlExport.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnExportPostalCodesClicked(object sender, EventArgs e)
        {
            var export = false;

            if (chkExportCurrentSelection.Checked)
            {
                ExportPostalCodes(new List<PostalCodeViewSearchDto>());
                export = true;
            }

            if (chkExportAllPostalCodes.Checked)
            {
                if (ExportAllPostalCodes != null)
                    ExportAllPostalCodes(this, new EventArgs());

                export = true;
            }

            if (!export)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error("Please select an option for exporting postal codes") });
            }
        }

        protected void OnCloseExportClicked(object sender, object e)
        {
            pnlExport.Visible = false;
            pnlDimScreen.Visible = false;
        }

       

        private List<ParameterColumn> GetCurrentRunParameters()
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter()).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = RegistrySearchFields.PostalCodes.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters();

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(RegistrySearchFields.PostalCodes);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters();
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = RegistrySearchFields.PostalCodeSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearch(SortByField);
        }

    }
}