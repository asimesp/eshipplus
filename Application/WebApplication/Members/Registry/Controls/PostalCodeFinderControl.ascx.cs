﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry.Controls
{
    public partial class PostalCodeFinderControl : MemberControlBase, IPostalCodeFinderView
    {
        public new bool Visible
        {
            get { return pnlPostalCodeFinderContent.Visible; }
            set
            {
                base.Visible = true;
                pnlPostalCodeFinderContent.Visible = value;
                pnlPostalCodeFinderDimScreen.Visible = value;
            }
        }

        public bool EnableMultiSelection
        {
            get { return hidPostalCodeFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidPostalCodeFinderEnableMultiSelection.Value = value.ToString();
                btnSelectAll.Visible = value;
                btnSelectAll2.Visible = value;
                chkSelectAllRecords.Visible = value;
                upcseDataUpdate.PreserveRecordSelection = value;
            }
        }
        
        private SearchField SortByField
        {
            get { return RegistrySearchFields.PostalCodeSortFields.FirstOrDefault(c => c.Name == hidSortField.Value); }
        }


        public event EventHandler<ViewEventArgs<PostalCodeViewSearchCriteria>> Search;

        public event EventHandler<ViewEventArgs<PostalCodeViewSearchDto>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<PostalCodeViewSearchDto>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<PostalCodeViewSearchDto> postalCodes)
        {
            litRecordCount.Text = postalCodes.BuildRecordCount();
            upcseDataUpdate.DataSource = postalCodes;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


		public void Reset()
		{
            hidAreFiltersShowing.Value = true.ToString();
			DisplaySearchResult(new List<PostalCodeViewSearchDto>());
		}


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = hidSortAscending.Value.ToBoolean(),
                SortBy = SortByField,
            };
        }

        public void SearchOn(string criteria)
        {
            //if the criteria being passed in is an empty string, we will get a massive amount of returned results for no reason so return
            if (string.IsNullOrEmpty(criteria)) return;

            var parameters = RegistrySearchFields.PostalCodes.ToParameterColumn();
            foreach (var parameterColumn in parameters.Where(parameterColumn => parameterColumn.ReportColumnName == RegistrySearchFields.Code.DisplayName))
                parameterColumn.DefaultValue = criteria;

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            var sortAscending = hidSortAscending.Value.ToBoolean();

            litSortOrder.SetSortDisplay(SortByField, sortAscending);

            DoSearch(new PostalCodeViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                SortBy = field,
                SortAscending = sortAscending
            });
        }

        private void DoSearch(PostalCodeViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<PostalCodeViewSearchCriteria>(criteria));

            chkSelectAllRecords.Checked = false;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new PostalCodeFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;

            //link up sort buttons
            lbtnSortPostalCode.CommandName = RegistrySearchFields.Code.Name;
            lbtnSortCity.CommandName = RegistrySearchFields.City.Name;
            lbtnSortCityAlias.CommandName = RegistrySearchFields.CityAlias.Name;
            lbtnSortState.CommandName = RegistrySearchFields.State.Name;
            lbtnSortCountryName.CommandName = RegistrySearchFields.CountryName.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : RegistrySearchFields.DefaultPostalCodes.Select(f => f.ToParameterColumn()).ToList();

            var country = columns
                .FirstOrDefault(c => c.ReportColumnName == RegistrySearchFields.CountryName
                .ToParameterColumn().ReportColumnName);

            if (country != null)
            {
                country.DefaultValue = ActiveUser.Tenant.DefaultCountry.Name;
                country.Operator = Operator.Equal;
            }

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            hidSortAscending.Value = (profile != null && profile.SortAscending).GetString();

            if (profile != null && profile.SortBy != null)
                hidSortField.Value = profile.SortBy.Name;

            litSortOrder.SetSortDisplay(SortByField, hidSortAscending.Value.ToBoolean());
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnCancelClicked(object sender, EventArgs e)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnSelectAllClicked(object sender, EventArgs e)
        {
            var codes = (from item in lstSearchResults.Items
                         let hidden = item.FindControl("hidPostalCodeId").ToCustomHiddenField()
                         let checkBox = item.FindControl("chkSelected").ToAltUniformCheckBox()
                         where checkBox != null && checkBox.Checked
                         select new PostalCodeViewSearchDto(hidden.Value.ToLong()))
                .ToList();

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<PostalCodeViewSearchDto>>(codes));
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var hidden = button.FindControl("hidPostalCodeId").ToCustomHiddenField();

            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<PostalCodeViewSearchDto>(new PostalCodeViewSearchDto(hidden.Value.ToLong())));
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = RegistrySearchFields.PostalCodes.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(RegistrySearchFields.PostalCodes);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            hidSortAscending.Value = e.Argument.SortAscending.GetString();
            hidSortField.Value = e.Argument.SortBy == null ? string.Empty : e.Argument.SortBy.Name;

            var field = SortByField;
            litSortOrder.SetSortDisplay(field, hidSortAscending.Value.ToBoolean());
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = RegistrySearchFields.PostalCodeSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;

            if (hidSortField.Value == field.Name && hidSortAscending.Value.ToBoolean())
            {
                hidSortAscending.Value = false.GetString();
            }
            else if (hidSortField.Value == field.Name && !hidSortAscending.Value.ToBoolean())
            {
                hidSortAscending.Value = true.GetString();
            }

            hidSortField.Value = field.Name;

            litSortOrder.SetSortDisplay(field, hidSortAscending.Value.ToBoolean());

            DoSearchPreProcessingThenSearch(field);
        }
    }
}