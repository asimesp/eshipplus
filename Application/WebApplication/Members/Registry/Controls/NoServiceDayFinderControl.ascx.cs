﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry.Controls
{
    public partial class NoServiceDayFinderControl : MemberControlBase, INoServiceDayFinderView
    {
        public new bool Visible
        {
            get { return pnlNoServiceDayFinderContent.Visible; }
            set
            {
                base.Visible = true;
                pnlNoServiceDayFinderContent.Visible = value;
                pnlNoServiceDayFinderDimScreen.Visible = value;
            }
        }

        public bool EnableMultiSelection
        {
            get { return hidNoServiceDayFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidNoServiceDayFinderEnableMultiSelection.Value = value.ToString();
                btnSelectAll.Visible = value;
                btnSelectAll2.Visible = value;
                upcseDataUpdate.PreserveRecordSelection = value;
            }
        }

        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<NoServiceDay>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<NoServiceDay>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<NoServiceDay> noServiceDays)
        {
            litRecordCount.Text = noServiceDays.BuildRecordCount();
            upcseDataUpdate.DataSource = noServiceDays.OrderBy(nsd => nsd.DateOfNoService).ToList();
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString();
            DisplaySearchResult(new List<NoServiceDay>());
        }
        

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearch(List<ParameterColumn> columns)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(columns));

            var control = lstSearchResults.FindControl("chkSelectAllRecords").ToAltUniformCheckBox();
            if (control != null)
            {
                control.Checked = false;
                control.Visible = EnableMultiSelection;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new NoServiceDayFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : RegistrySearchFields.NoServiceDays.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var hidden = button.FindControl("hidNoServiceDayId").ToCustomHiddenField();

            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<NoServiceDay>(new NoServiceDay(hidden.Value.ToLong(), false)));
        }

        protected void OnSelectAllClicked(object sender, EventArgs e)
        {
            var noServiceDays = (from item in lstSearchResults.Items
                                 let hidden = item.FindControl("hidNoServiceDayId").ToCustomHiddenField()
                                 let checkBox = item.FindControl("chkSelected").ToAltUniformCheckBox()
                                 where checkBox != null && checkBox.Checked
                                 select new NoServiceDay(hidden.Value.ToLong(), false))
                .Where(u => u.KeyLoaded)
                .ToList();

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<NoServiceDay>>(noServiceDays));
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = RegistrySearchFields.NoServiceDays.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(RegistrySearchFields.NoServiceDays);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
    }
}