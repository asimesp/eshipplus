﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry.Controls
{
    public partial class PrefixFinderControl : MemberControlBase, IPrefixFinderView
    {
        public new bool Visible
        {
            get { return pnlPrefixFinderContent.Visible; }
            set
            {
                base.Visible = true;
                pnlPrefixFinderContent.Visible = value;
                pnlPrefixFinderDimScreen.Visible = value;
            }
        }

        public bool EnableMultiSelection
        {
            get { return hidPrefixFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidPrefixFinderEnableMultiSelection.Value = value.ToString();
                btnSelectAll.Visible = value;
                btnSelectAll2.Visible = value;
                upcseDataUpdate.PreserveRecordSelection = value;
            }
        }

        public bool ShowActiveRecordsOnly
        {
            get { return hidPrefixFinderFetchOnlyActiveRecords.Value.ToBoolean(); }
            set { hidPrefixFinderFetchOnlyActiveRecords.Value = value.ToString(); }
        }

        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;

        public event EventHandler<ViewEventArgs<Prefix>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<Prefix>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<Prefix> prefixes)
        {
            litRecordCount.Text = prefixes.BuildRecordCount();
            upcseDataUpdate.DataSource = prefixes;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString();
            DisplaySearchResult(new List<Prefix>());
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch()
        {
            var columns = GetCurrentRunParameters(false);

            // filter for non posted only if necessary
            if (ShowActiveRecordsOnly)
            {
                var activeParameter = RegistrySearchFields.Active.ToParameterColumn();
                activeParameter.DefaultValue = true.ToString();
                activeParameter.Operator = Operator.Equal;
                columns.Add(activeParameter);
            }

            DoSearch(columns);
        }

        private void DoSearch(List<ParameterColumn> columns)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(columns));

            var control = lstSearchResults.FindControl("chkSelectAllRecords").ToAltUniformCheckBox();
            if (control != null)
            {
                control.Checked = false;
                control.Visible = EnableMultiSelection;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new PrefixFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : RegistrySearchFields.Prefixes.Select(f => f.ToParameterColumn()).ToList();

            //remove active if filtering as it will be set prior to search
            var activeColumns = new List<ParameterColumn>();
            if (ShowActiveRecordsOnly)
                activeColumns.AddRange(columns.Where(c => c.ReportColumnName == RegistrySearchFields.Active.ToParameterColumn().ReportColumnName));

            foreach (var column in activeColumns)
                columns.Remove(column);

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch();
        }

        protected void OnCancelClicked(object sender, EventArgs e)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnSelectAllClicked(object sender, EventArgs e)
        {
            var codes = (from item in lstSearchResults.Items
                         let hidden = item.FindControl("hidPrefixId").ToCustomHiddenField()
                         let checkBox = item.FindControl("chkSelected").ToAltUniformCheckBox()
                         where checkBox != null && checkBox.Checked
                         select new Prefix(hidden.Value.ToLong()))
                .ToList();

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<Prefix>>(codes));
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var hidden = button.FindControl("hidPrefixId").ToCustomHiddenField();

            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<Prefix>(new Prefix(hidden.Value.ToLong())));
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = RegistrySearchFields.Prefixes.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(ShowActiveRecordsOnly
                                  ? RegistrySearchFields.Prefixes
                                        .Where(f => f.Name != RegistrySearchFields.Active.Name)
                                        .ToList()
                                  : RegistrySearchFields.Prefixes
                                        .ToList());
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
    }
}