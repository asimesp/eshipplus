﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class EquipmentTypeView : MemberPageBase, IEquipmentTypeView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/EquipmentTypeView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.EquipmentType; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }


        public Dictionary<int, string> DatEquipmentTypes
        {
            set
            {
                ddlDatEquipmentType.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).OrderBy(t => t.Text).ToList();
                ddlDatEquipmentType.DataBind();
            }
        }

        public Dictionary<int, string> Groups
        {
            set
            {
                ddlGroup.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).OrderBy(t => t.Text).ToList();
                ddlGroup.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<EquipmentType>> Save;
        public event EventHandler<ViewEventArgs<EquipmentType>> Delete;
        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<EquipmentType>> Lock;
        public event EventHandler<ViewEventArgs<EquipmentType>> UnLock;
        public event EventHandler<ViewEventArgs<List<EquipmentType>>> BatchImport;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<EquipmentType> equipmentTypes)
        {
            litRecordCount.Text = equipmentTypes.BuildRecordCount();
            lvwRegistry.DataSource = equipmentTypes.OrderBy(e => e.TypeName);
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(new List<ParameterColumn>()));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<EquipmentType>(new EquipmentType(hidEquipmentTypeId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtTypeName.Text = string.Empty;
            chkActive.Checked = false;
        }

        private EquipmentType RetrieveViewEquipmentType()
        {
            var equipmentTypeId = hidEquipmentTypeId.Value.ToLong();

            var equipmentType = new EquipmentType(equipmentTypeId, equipmentTypeId != default(long))
            {
                Code = txtCode.Text,
                TypeName = txtTypeName.Text,
                Active = chkActive.Checked,
                Group = ddlGroup.SelectedValue.ToEnum<EquipmentTypeGroup>(),
                DatEquipmentType = ddlDatEquipmentType.SelectedValue.ToEnum<DatEquipmentType>()
            };
            if (equipmentType.IsNew) equipmentType.TenantId = ActiveUser.TenantId;
            return equipmentType;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new EquipmentTypeHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.EquipmentTypesImportTemplate });

            if (Loading != null)
                Loading(this, new EventArgs());

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidEquipmentTypeId.Value = string.Empty;
            GeneratePopup("Add Equipment Type");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Equipment Type");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("equipmentTypeId").ToCustomHiddenField();

            var equipmentType = new EquipmentType(hidden.Value.ToLong(), false);

            hidEquipmentTypeId.Value = equipmentType.Id.ToString();
            txtTypeName.Text = equipmentType.TypeName;
            txtCode.Text = equipmentType.Code;
            chkActive.Checked = equipmentType.Active;
            ddlGroup.SelectedValue = equipmentType.Group.ToInt().ToString();
            ddlDatEquipmentType.SelectedValue = equipmentType.DatEquipmentType.ToInt().ToString();

            if (Lock != null)
                Lock(this, new ViewEventArgs<EquipmentType>(equipmentType));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var equipmentType = RetrieveViewEquipmentType();

            if (Save != null)
                Save(this, new ViewEventArgs<EquipmentType>(equipmentType));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("equipmentTypeId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<EquipmentType>(new EquipmentType(hidden.Value.ToLong(), true)));

            DoSearch();
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "EQUIPMENT TYPE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                .Select(item => new[]
									{
										item.FindControl("litCode").ToLiteral().Text.ToString(),
										item.FindControl("litTypeName").ToLiteral().Text.ToString(),
										item.FindControl("hidGroup").ToCustomHiddenField().Value.ToEnum<EquipmentTypeGroup>().ToInt().ToString(),
										item.FindControl("hidDatEquipmentType").ToCustomHiddenField().Value.ToEnum<DatEquipmentType>().ToInt().ToString(),
										item.FindControl("hidActive").ToCustomHiddenField().Value
									}.TabJoin())
                .ToList();
            lines.Insert(0, new[] { "Code", "Type Name", "Group", "DAT Equipment Type", "Active" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "EquipmentTypes.txt");
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 5;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<EquipmentTypeGroup>(2));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<DatEquipmentType>(3));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var equipmentTypes = lines
                .Select(s => new EquipmentType
                {
                    Code = s[0],
                    TypeName = s[1],
                    Group = s[2].ToEnum<EquipmentTypeGroup>(),
                    DatEquipmentType = s[3].ToEnum<DatEquipmentType>(),
                    Active = s[4].ToBoolean(),
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<EquipmentType>>(equipmentTypes));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<EquipmentType>(RetrieveViewEquipmentType()));
            }
        }
    }
}