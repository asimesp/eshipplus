﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class ServiceView : MemberPageBase, IServiceView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/ServiceView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.Service; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public Dictionary<int, string> Categories
        {
            set
            {
                ddlCategories.DataSource = (value ?? new Dictionary<int, string>()).OrderBy(i=>i.Value);
                ddlCategories.DataBind();
            }
        }


		public Dictionary<int, string> DispatchFlag
		{
			set
			{
				ddlDispatchFlag.DataSource = (value ?? new Dictionary<int, string>()).OrderBy(i=>i.Value);
				ddlDispatchFlag.DataBind();
			}
		}

        public Dictionary<int, string> Project44Codes
        {
            set
            {
                var source = (value ?? new Dictionary<int, string>());
                ddlProject44Code.DataSource = source.OrderBy(k => k.Value).ToDictionary(k => k.Key,
                    k => string.Format("{0} ({1})", k.Value.ToEnum<Project44ServiceCode>().GetServiceCodeDescription(), k.Value));
                ddlProject44Code.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Service>> Save;
        public event EventHandler<ViewEventArgs<Service>> Delete;
        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<List<Service>>> BatchImport;
        public event EventHandler<ViewEventArgs<Service>> Lock;
        public event EventHandler<ViewEventArgs<Service>> UnLock;

        public long GetChargeCodeId(string chargeCodeCode, Dictionary<string, long> chargeCodeIds, ChargeCodeSearch search)
        {
            if (chargeCodeIds.ContainsKey(chargeCodeCode.Trim())) return chargeCodeIds[chargeCodeCode];
            var id = search.FetchChargeCodeIdByCode(chargeCodeCode, ActiveUser.TenantId);
            if (id != default(long)) chargeCodeIds.Add(chargeCodeCode, id);
            return id;
        }

        public void FaildedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<ServiceViewSearchDto> services)
        {
            litRecordCount.Text = services.BuildRecordCount();
            lvwRegistry.DataSource = services;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(new List<ParameterColumn>()));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Service>(new Service(hidServiceId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            ddlCategories.SelectedIndex = 0;
            ddlChargeCodes.SelectedIndex = 0;
	        ddlDispatchFlag.SelectedIndex = 0;
            ddlProject44Code.SelectedIndex = 0;
	        chkDelivery.Checked = false;
	        chkPickup.Checked = false;
        }

        private Service RetrieveViewService()
        {
            var serviceId = hidServiceId.Value.ToLong();

	        var service = new Service(serviceId, serviceId != default(long))
		        {

			        Code = txtCode.Text,
			        Description = txtDescription.Text,
			        Category = ddlCategories.SelectedValue.ToEnum<ServiceCategory>(),
			        ChargeCodeId = ddlChargeCodes.SelectedValue.ToInt(),
			        DispatchFlag = ddlDispatchFlag.SelectedValue.ToEnum<ServiceDispatchFlag>(),
                    Project44Code = ddlProject44Code.SelectedValue.ToEnum<Project44ServiceCode>(),
			        ApplicableAtDelivery = chkDelivery.Checked,
			        ApplicableAtPickup = chkPickup.Checked,
		        };
            if (service.IsNew) service.TenantId = ActiveUser.TenantId;
            return service;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new ServiceHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.ServicesImportTemplate });

            if (Loading != null)
                Loading(this, new EventArgs());

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidServiceId.Value = string.Empty;
            GeneratePopup("Add Service");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Service");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("serviceId").ToCustomHiddenField();

            var service = new Service(hidden.Value.ToLong(), false);

            hidServiceId.Value = service.Id.ToString();
            txtCode.Text = service.Code;
            txtDescription.Text = service.Description;
            ddlCategories.SelectedValue = service.Category.ToInt().ToString();
            ddlChargeCodes.SelectedValue = service.ChargeCodeId.GetString();
	        ddlDispatchFlag.SelectedValue = service.DispatchFlag.ToInt().GetString();
            ddlProject44Code.SelectedValue = service.Project44Code.ToInt().GetString();
	        chkDelivery.Checked = service.ApplicableAtDelivery;
	        chkPickup.Checked = service.ApplicableAtPickup;

            if (Lock != null)
                Lock(this, new ViewEventArgs<Service>(service));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var service = RetrieveViewService();

            if (Save != null)
                Save(this, new ViewEventArgs<Service>(service));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("serviceId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<Service>(new Service(hidden.Value.ToLong(), true)));

            DoSearch();
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "SERVICE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                    .Select(item => new[]
					             	{
									    (item.FindControl("litCode")).ToLiteral().Text,
										(item.FindControl("litCategory").ToLiteral().Text.ToEnum<ServiceCategory>()).ToInt().ToString(),
                                        item.FindControl("litDescription").ToLiteral().Text,
										item.FindControl("hidDispatchFlag").ToCustomHiddenField().Value,
										item.FindControl("hidPickup").ToCustomHiddenField().Value.ToBoolean().GetString(),
										item.FindControl("hidDelivery").ToCustomHiddenField().Value.ToBoolean().GetString(),
										item.FindControl("litChargeCode").ToLiteral().Text,
					             	}.TabJoin())
                    .ToList();
            lines.Insert(0, new[] { "Code", "Category", "Description", "Dispatch Flag", "Applicable at Pickup", "Applicable at Delivery", "Charge Code" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "Services.txt");
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }


            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 7;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<ServiceCategory>(1));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

			msgs.AddRange(chks.EnumsAreValid<ServiceDispatchFlag>(3));
			if (msgs.Any())
			{
				msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
				DisplayMessages(msgs);
				return;
			}
   
            var chargeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(6, chargeCodes, new ChargeCode().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var chargeCodeSearch = new ChargeCodeSearch();
            var chargeCodeIds = new Dictionary<string, long>();

            var services = lines
                .Select(s => new Service
                {
                    Code = s[0],
                    Category = s[1].ToEnum<ServiceCategory>(),
                    Description = s[2],
					DispatchFlag = s[3].ToEnum<ServiceDispatchFlag>(),
					ApplicableAtPickup = s[4].ToBoolean(),
					ApplicableAtDelivery = s[5].ToBoolean(),
                    ChargeCodeId = GetChargeCodeId(s[6], chargeCodeIds, chargeCodeSearch),
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<Service>>(services));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Service>(RetrieveViewService()));
            }
        }
    }
}
