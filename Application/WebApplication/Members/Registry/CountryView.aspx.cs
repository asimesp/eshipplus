﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class CountryView : AdminMemberPageBase, ICountryView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/CountryView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.Country; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public Dictionary<int, string> Project44CountryCodes
        {
            set
            {
                var source = (value ?? new Dictionary<int, string>());
                ddlProject44CountryCode.DataSource = source.OrderBy(k => k.Value).ToDictionary(k => k.Key,
                    k => string.Format("{0} ({1})", k.Value.ToEnum<Project44CountryCode>().GetCountryCodeDescription(), k.Value));
                ddlProject44CountryCode.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Country>> Save;
        public event EventHandler<ViewEventArgs<Country>> Delete;
        public event EventHandler<ViewEventArgs<string>> Search;
        public event EventHandler<ViewEventArgs<List<Country>>> BatchImport;

        public void DisplaySearchResult(List<Country> countries)
        {
            litRecordCount.Text = countries.BuildRecordCount();

            lvwCountry.DataSource = countries.OrderBy(c => c.Name);
            lvwCountry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>("%"));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtName.Text = string.Empty;
            txtPostalCodeValidation.Text = string.Empty;
            chkEmploysPostalCode.Checked = true;
            txtSortWeight.Text = string.Empty;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            new CountryHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;


            if (IsPostBack) return;

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.CountryImportTemplate });

            if (Loading != null)
                Loading(this, new EventArgs());

            DoSearch();
        }

        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidCountryId.Value = string.Empty;
            GeneratePopup("Add Country");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Country");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("countryId").ToCustomHiddenField();

            var country = new Country(hidden.Value.ToLong());

            hidCountryId.Value = country.Id.ToString();
            txtCode.Text = country.Code;
            txtName.Text = country.Name;
            txtPostalCodeValidation.Text = country.PostalCodeValidation;
            chkEmploysPostalCode.Checked = country.EmploysPostalCodes;
            txtSortWeight.Text = country.SortWeight.ToString();
            ddlProject44CountryCode.SelectedValue = country.Project44CountryCode.ToInt().ToString();
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var countryId = hidCountryId.Value.ToLong();

            var country = new Country(countryId, countryId != default(long))
            {
                Code = txtCode.Text,
                Name = txtName.Text,
                PostalCodeValidation = txtPostalCodeValidation.Text,
                EmploysPostalCodes = chkEmploysPostalCode.Checked,
                SortWeight = txtSortWeight.Text.ToInt(),
                Project44CountryCode = ddlProject44CountryCode.SelectedValue.ToInt().ToEnum<Project44CountryCode>()
            };

            if (Save != null)
                Save(this, new ViewEventArgs<Country>(country));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("countryId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<Country>(new Country(hidden.Value.ToLong())));

            DoSearch();
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 6;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.Where(c => c.Line[5] != string.Empty).EnumsAreValid<Project44CountryCode>(5));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countries = lines
                .Select(s => new Country
                {
                    Code = s[0],
                    Name = s[1],
                    EmploysPostalCodes = s[2].ToBoolean(),
                    PostalCodeValidation = s[3],
                    SortWeight = s[4].ToInt(),
                    Project44CountryCode = s[5].ToInt().ToEnum<Project44CountryCode>()
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<Country>>(countries));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "COUNTRY IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwCountry.Items
                                  .Select(item => new[]
                                      {
                                          item.FindControl("litCode").ToLiteral().Text,
                                          item.FindControl("litName").ToLiteral().Text,
                                          item.FindControl("hidEmploysPostalCodes").ToCustomHiddenField().Value,
                                          item.FindControl("litPostalCodeValidation").ToLiteral().Text,
                                          item.FindControl("litSortWeight").ToLiteral().Text,
                                          item.FindControl("litProject44CountryCode").ToLiteral().Text,
                                      }.TabJoin())
                                  .ToList();

            lines.Insert(0, new[] { "Code", "Name", "Employs Postal Code", "Postal Code Validation", "Sort Weight", "Project 44 Country Code"}.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "Countries.txt");
        }
    }
}
