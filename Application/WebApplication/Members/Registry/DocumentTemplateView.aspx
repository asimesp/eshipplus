﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="DocumentTemplateView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.DocumentTemplateView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Document Templates<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="fat" />
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheDocumentTemplateTable" TableId="documentTemplateTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="documentTemplateTable" class="line2 pl2">
                        <tr>
                            <th style="width: 4%;">&nbsp;</th>
                            <th style="width: 8%;">Code</th>
                            <th style="width: 15%;">Template File</th>
                            <th style="width: 10%;" class="text-center">Round Weight To</th>
                            <th style="width: 10%;" class="text-center">Round Dims To</th>
                            <th style="width: 11%;" class="text-center">Round Currency To</th>
                            <th style="width: 14%;">Category</th>
                            <th style="width: 13%;">Service Mode</th>
                            <th style="width: 5%;" class="text-center">Primary</th>
                            <th style="width: 10%;" class="text-center">Action</th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td rowspan="2" class="top">
                        <%# string.Format("{0}", Container.DataItemIndex+1)%>
                    </td>
                    <td>
                        <eShip:CustomHiddenField ID="documentTemplateId" runat="server" Value='<%# Eval("Id") %>' />
                        <%# Eval("Code") %>
                    </td>
                    <td>
                        <eShip:CustomHiddenField runat="server" ID="hidTemplatePath" Value='<%# Eval("TemplatePath") %>' />
                        <asp:LinkButton runat="server" ID="lnkTemplatePath" Text='<%# GetTemplateFileName(Eval("TemplatePath")) %>'
                            CausesValidation="False" OnClick="OnTempathPathClicked" CssClass="blue"/>
                    </td>
                    <td class="text-center">
                        <%# Eval("WeightDecimals")%>
                    </td>
                    <td class="text-center">
                        <%# Eval("DimensionDecimals")%>
                    </td>
                    <td class="text-center">
                        <%# Eval("CurrencyDecimals")%>
                    </td>
                    <td>
                        <%# Eval("Category").FormattedString() %>
                    </td>
                    <td>
                        <%# Eval("ServiceMode").FormattedString() %>
                    </td>
                    <td class="text-center">
                        <%# Eval("Primary").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                    </td>
                    <td rowspan="2" class="top">
                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
                <tr class="f9">
                    <td colspan="8" class="forceLeftBorder">
                        <div class="fieldgroup">
                            <span class="blue"><b>Description: </b></span><%# Eval("Description") %>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Code:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtCode" MaxLength="50" runat="server" CssClass="w300" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtCode"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Round Weight To:</label>
                            </td>
                            <td class="text-left">
                                <asp:DropDownList DataTextField="Text" DataValueField="Value" ID="ddlWeightDecimals"
                                    runat="server" CssClass="w80" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Round Dimensions To:</label>
                            </td>
                            <td class="text-left">
                                <asp:DropDownList DataTextField="Text" DataValueField="Value" ID="ddlDimensionDecimals" runat="server" CssClass="w80" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Round Currency To:</label>
                            </td>
                            <td class="text-left">
                                <asp:DropDownList DataTextField="Text" DataValueField="Value" ID="ddlCurrencyDecimals" runat="server" CssClass="w80" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Category:</label>
                            </td>
                            <td class="text-left">
                                <asp:DropDownList DataTextField="Value" DataValueField="Key" ID="ddlCategories" runat="server"
                                    CssClass="w200" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Service Mode:</label>
                            </td>
                            <td class="text-left">
                                <asp:DropDownList DataTextField="Value" DataValueField="Key" ID="ddlServiceModes" runat="server" CssClass="w200" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-left">
                                <asp:CheckBox ID="chkPrimary" runat="server" CssClass="jQueryUniform" />
                                <label class="upper">Primary</label>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">
                                    Description:
                                </label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="500" />
                                </label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtDescription" runat="server" TextMode="MultiLine"
                                    Rows="3" CssClass="w300" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Template Path:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomHiddenField ID="hidDocumentTemplateId" runat="server" />
                                <asp:FileUpload runat="server" ID="fupTemplate" CssClass="jQueryUniform" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Current File:</label>
                            </td>
                            <td class="text-left">
                                <asp:Literal ID="litTemplate" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" CausesValidation="False" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" OnYes="OnForcePrimaryDocumentTemplate" OnNo="OnDoNotForcePrimaryDocumentTemplate" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField ID="hidTemplatePath" runat="server" />
    <eShip:CustomHiddenField ID="hidTemporaryPath" runat="server" />
    <eShip:CustomHiddenField runat="server" ID="hidTempUniqueCode" />
    <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
</asp:Content>
