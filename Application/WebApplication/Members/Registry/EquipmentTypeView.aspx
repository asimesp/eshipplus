﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="EquipmentTypeView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.EquipmentTypeView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Equipment Types<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="fat" />
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheEquipmentTypeTable" TableId="equipmentTypeTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="equipmentTypeTable" class="line2 pl2">
                        <tr>
                            <th style="width: 20%;">Code
                            </th>
                            <th style="width: 32%;">Type Name
                            </th>
                            <th style="width: 15%;">Group
                            </th>
                            <th style="width: 15%;">DAT Equipment Type
                            </th>
                            <th style="width: 10%;" class="text-center">Active
                            </th>
                            <th style="width: 8%;">Action
                            </th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="equipmentTypeId" runat="server" Value='<%# Eval("Id") %>' />
                        <asp:Literal ID="litCode" runat="server" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litTypeName" runat="server" Text='<%# Eval("TypeName") %>' />
                    </td>
                    <td>
                        <%# Eval("Group").FormattedString() %>
                        <eShip:CustomHiddenField ID="hidGroup" runat="server" Value='<%# Eval("Group") %>' />
                    </td>
                    <td>
                        <%# Eval("DatEquipmentType").FormattedString() %>
                        <eShip:CustomHiddenField ID="hidDatEquipmentType" runat="server" Value='<%# Eval("DatEquipmentType") %>' />
                    </td>
                    <td class="text-center">
                        <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                        <eShip:CustomHiddenField runat="server" ID="hidActive" Value='<%# Eval("Active").ToBoolean().GetString() %>' />
                    </td>
                    <td>
                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server"/>
                    </h4>
                        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                            CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Code:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomHiddenField ID="hidEquipmentTypeId" runat="server" />
                                <eShip:CustomTextBox ID="txtCode" CssClass="w300"  MaxLength="50" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtCode"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Type Name:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtTypeName" CssClass="w300"  MaxLength="50" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvTypeName" ControlToValidate="txtTypeName"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Group:</label>
                            </td>
                            <td class="text-left">
                                <asp:DropDownList ID="ddlGroup" CssClass="w150" DataTextField="Text" DataValueField="Value" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">DAT Equipment Type:</label>
                            </td>
                            <td class="text-left">
                                <asp:DropDownList ID="ddlDatEquipmentType" CssClass="w150" DataTextField="Text" DataValueField="Value" runat="server" />
                                <asp:CheckBox ID="chkActive" runat="server" CssClass="jQueryUniform ml10"/>
                                <label class="upper">Active</label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
		OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
