﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="FailureCodeView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.FailureCodeView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowImport="true" OnNew="OnNewClick" OnImport="OnImportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Failure Codes<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="dark mb10" />
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="FailureCodes" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                        <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheFailureCodeTable" TableId="failureCodeTable" HeaderZIndex="2"/>
        <div class="rowgroup">
            <table id="failureCodeTable" class="line2 pl2">
                <tr>
                    <th style="width: 15%;">Code
                    </th>
                    <th style="width: 37%;">Description
                    </th>
                    <th style="width: 20%;">Category
                    </th>
                    <th style="width: 15%;" class="text-center">Exclude Statistics on Scorecard
                    </th>
                    <th style="width: 5%;" class="text-center">Active
                    </th>
                    <th style="width: 8%;">Action
                    </th>
                </tr>
                <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <eShip:CustomHiddenField ID="failureCodeId" runat="server" Value='<%# Eval("Id") %>' />
                                <%# Eval("Code") %>
                            </td>
                            <td>
                                <%# Eval("Description") %>
                            </td>
                            <td>
                                <%# Eval("Category").FormattedString() %>
                            </td>
                            <td class="text-center">
                                <%# Eval("ExcludeOnScoreCard").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                            </td>
                            <td class="text-center">
                                <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                            </td>
                            <td class="imageButtons">
                                <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                                    CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                                <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                                    CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                                <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                                    ConfirmText="Are you sure you want to delete record?" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </div>

        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="20" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Code:</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidFailureCodeId" runat="server" />
                                <eShip:CustomTextBox ID="txtCode" MaxLength="50" runat="server" CssClass="w300" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvTemplatePath" ControlToValidate="txtCode"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">
                                    Description:
                                </label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="500" />
                                </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="w300" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Category:</label>
                            </td>
                            <td>
                                <asp:DropDownList DataTextField="Value" DataValueField="Key" ID="ddlFailureCodeCategories"
                                    runat="server" CssClass="w300" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right"></td>
                            <td>
                                <asp:CheckBox ID="chkExcludeOnScoreCard" runat="server" CssClass="jQueryUniform" />
                                <label class="upper w90">Stats Exclude</label>
                                <asp:CheckBox ID="chkActive" runat="server" CssClass="jQueryUniform ml10" />
                                <label class="upper w90">Active</label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
