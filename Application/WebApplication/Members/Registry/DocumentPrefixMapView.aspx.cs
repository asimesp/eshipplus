﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class DocumentPrefixMapView : MemberPageBase, IDocumentPrefixMapView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/DocumentPrefixMapView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.DocumentPrefixMap; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public List<DocumentTemplate> DocumentTemplates
        {
            set
            {
                var items = value
                    .Select(d => new ViewListItem(string.Format("{0} - {1}", d.Code, d.Description), d.Id.GetString()))
                    .OrderBy(v => v.Text)
                    .ToList();
                items.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, 0.ToString()));

                ddlDocumentTemplates.DataSource = items;
                ddlDocumentTemplates.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<DocumentPrefixMap>> Save;
        public event EventHandler<ViewEventArgs<DocumentPrefixMap>> Delete;
        public event EventHandler<ViewEventArgs<DocumentPrefixMapViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<DocumentPrefixMap>> Lock;
        public event EventHandler<ViewEventArgs<DocumentPrefixMap>> UnLock;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<DocumentPrefixMapViewSearchDto> documentPrefixMap)
        {
            litRecordCount.Text = documentPrefixMap.BuildRecordCount();
            lvwRegistry.DataSource = documentPrefixMap.OrderBy(d => d.Prefix).ThenBy(d => d.Template);
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() || messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private void DoSearch()
        {
            var criteria = new DocumentPrefixMapViewSearchCriteria { TenantId = ActiveUser.TenantId };

            if (Search != null)
                Search(this, new ViewEventArgs<DocumentPrefixMapViewSearchCriteria>(criteria));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            ddlPrefixes.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<DocumentPrefixMap>(new DocumentPrefixMap(hidDocumentPrefixMapId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            ddlPrefixes.SelectedValue = string.Empty;
            ddlDocumentTemplates.SelectedIndex = 0;
        }

        private DocumentPrefixMap RetrieveViewDocumentPrefixMap()
        {
            var documentPrefixMapId = hidDocumentPrefixMapId.Value.ToLong();

            var documentPrefixMap = new DocumentPrefixMap(documentPrefixMapId, documentPrefixMapId != default(long))
            {
                PrefixId = ddlPrefixes.SelectedValue.ToInt(),
                DocumentTemplateId = ddlDocumentTemplates.SelectedValue.ToInt()
            };
            if (documentPrefixMap.IsNew) documentPrefixMap.TenantId = ActiveUser.TenantId;
            return documentPrefixMap;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new DocumentPrefixMapHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;


            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidDocumentPrefixMapId.Value = string.Empty;
            GeneratePopup("Add Document Prefix Map");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Document Prefix Map");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("documentPrefixMapId").ToCustomHiddenField();

            var documentPrefixMap = new DocumentPrefixMap(hidden.Value.ToLong(), false);

            hidDocumentPrefixMapId.Value = documentPrefixMap.Id.ToString();
            ddlPrefixes.SelectedValue = documentPrefixMap.PrefixId.GetString();
            ddlDocumentTemplates.SelectedValue = documentPrefixMap.DocumentTemplateId.ToString();

            if (Lock != null)
                Lock(this, new ViewEventArgs<DocumentPrefixMap>(documentPrefixMap));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var documentPrefixMap = RetrieveViewDocumentPrefixMap();

            if (Save != null)
                Save(this, new ViewEventArgs<DocumentPrefixMap>(documentPrefixMap));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("documentPrefixMapId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<DocumentPrefixMap>(new DocumentPrefixMap(hidden.Value.ToLong(), true)));

            DoSearch();
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<DocumentPrefixMap>(RetrieveViewDocumentPrefixMap()));
            }
        }
    }
}
