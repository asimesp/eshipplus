﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class P44ServiceMappingView : MemberPageBase, IP44ServiceView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/P44ServiceMappingView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.Project44ServiceMapping; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public Dictionary<int, string> Project44Codes
        {
            set
            {
                var source = (value ?? new Dictionary<int, string>());
                ddlProject44Code.DataSource = source.OrderBy(k => k.Value).ToDictionary(k => k.Key,
                    k => $"{k.Value.ToEnum<Project44ServiceCode>().GetServiceCodeDescription()} ({k.Value})");
                ddlProject44Code.DataBind();
            }
        }

        public List<Service> ServiceCodes
        {
            set
            {
                var source = (value ?? new List<Service>());
                ddlServiceCodes.DataSource = source.OrderBy(k => k.Id).ToDictionary(k => k.Id, k => string.Format("{0} ({1})", k.Description, k.Code));
                ddlServiceCodes.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<P44ServiceMapping>> Save;
        public event EventHandler<ViewEventArgs<P44ServiceMapping>> Delete;
        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<List<P44ServiceMapping>>> BatchImport;
        public event EventHandler<ViewEventArgs<P44ServiceMapping>> Lock;
        public event EventHandler<ViewEventArgs<P44ServiceMapping>> UnLock;

	    public void FaildedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<P44ServiceMapping> p44ServiceMapping)
        {
            litRecordCount.Text = p44ServiceMapping.BuildRecordCount();
            lvwRegistry.DataSource = p44ServiceMapping;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            Search?.Invoke(this, new ViewEventArgs<List<ParameterColumn>>(new List<ParameterColumn>()));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            ddlProject44Code.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            UnLock?.Invoke(this, new ViewEventArgs<P44ServiceMapping>(new P44ServiceMapping(hidP44ServiceMappingId.Value.ToLong())));
        }

        private void ClearFields()
        {
            ddlServiceCodes.SelectedIndex = 0;
            ddlProject44Code.SelectedIndex = 0;
        }

        private P44ServiceMapping RetrieveViewP44Service()
        {
            var p44ServiceMappingId = hidP44ServiceMappingId.Value.ToLong();

            var p44ServiceMapping = new P44ServiceMapping(p44ServiceMappingId, p44ServiceMappingId != default(long))
            {
                ServiceId = ddlServiceCodes.SelectedValue.ToInt(),
                Project44Code = ddlProject44Code.SelectedValue.ToInt().ToEnum<Project44ServiceCode>(),
            };
            if (p44ServiceMapping.IsNew) p44ServiceMapping.TenantId = ActiveUser.TenantId;
            return p44ServiceMapping;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new P44ServiceHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.P44ServiceMappingImportTemplate });

            Loading?.Invoke(this, new EventArgs());

            DoSearch();
        }

        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidP44ServiceMappingId.Value = string.Empty;
            GeneratePopup("Add P44 Service Code Mapping");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            try
            {
                ClearFields();
                GeneratePopup("Modify P44 Service Code Mapping");

                var imageButton = (ImageButton)sender;
                var hidden = imageButton.Parent.FindControl("hidP44ServiceMappingId").ToCustomHiddenField();
                var p44ServiceMapping = new P44ServiceMapping(hidden.Value.ToLong(), false);

                hidP44ServiceMappingId.Value = p44ServiceMapping.Id.ToString();
                ddlServiceCodes.SelectedValue = p44ServiceMapping.ServiceId.GetString();
                ddlProject44Code.SelectedValue = p44ServiceMapping.Project44Code.ToInt().GetString();

                Lock?.Invoke(this, new ViewEventArgs<P44ServiceMapping>(p44ServiceMapping));
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                DisplayMessages(new[] { ValidationMessage.Error(ex.Message), });
            }
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var p44ServiceMapping = RetrieveViewP44Service();
            Save?.Invoke(this, new ViewEventArgs<P44ServiceMapping>(p44ServiceMapping));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidP44ServiceMappingId").ToCustomHiddenField();

            Delete?.Invoke(this, new ViewEventArgs<P44ServiceMapping>(new P44ServiceMapping(hidden.Value.ToLong())));

            DoSearch();
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "P44 Service Code Mapping IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                    .Select(item => new[]
                                     {
                                        item.FindControl("litServiceCode").ToLiteral().Text,
                                        item.FindControl("litServiceCodeDescription").ToLiteral().Text,
                                        item.FindControl("litProject44ServiceCode").ToLiteral().Text,
                                        item.FindControl("litProject44ServiceCodeDescription").ToLiteral().Text,
                                     }.TabJoin())
                    .ToList();
            lines.Insert(0, new[] { "Service Code", "Description", "Project 44 Code", "Description" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "P44ServiceMapping.txt");
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 2;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var serviceCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].Services
                .ToDictionary(c => c.Code, c => c.Id);
            msgs.AddRange(chks.CodesAreValid(0, serviceCodes.Keys.ToList(), new Service().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var projectServiceMappings = lines
                .Select(s => new P44ServiceMapping
                {
                    ServiceId = serviceCodes[s[0]],
                    Project44Code = s[2].ToInt().ToEnum<Project44ServiceCode>(),
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            BatchImport?.Invoke(this, new ViewEventArgs<List<P44ServiceMapping>>(projectServiceMappings));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                UnLock?.Invoke(this, new ViewEventArgs<P44ServiceMapping>(RetrieveViewP44Service()));
            }
        }
    }
}
