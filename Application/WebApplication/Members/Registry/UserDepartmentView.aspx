﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="UserDepartmentView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.UserDepartmentView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                User Departments<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="fat" />
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheUserDepartmentTable" TableId="userDepartmentTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="userDepartmentTable" class="line2 pl2">
                        <tr>
                            <th style="width: 10%;">Code
                            </th>
                            <th style="width: 20%;">Description
                            </th>
                            <th style="width: 10%;">Phone
                            </th>
                            <th style="width: 10%;">Fax
                            </th>
                            <th style="width: 15%;">Email
                            </th>
                            <th style="width: 27%;">Comments
                            </th>
                            <th style="width: 8%;">Action
                            </th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="userDepartmentId" runat="server" Value='<%# Eval("Id") %>' />
                        <asp:Literal ID="litCode" runat="server" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litDescription" runat="server" Text='<%# Eval("Description") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litPhone" runat="server" Text='<%# Eval("Phone") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litFax" runat="server" Text='<%# Eval("Fax") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litEmail" runat="server" Text='<%# Eval("Email") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litComments" runat="server" Text='<%# Eval("Comments") %>' />
                    </td>
                    <td class="text-center">
                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="3" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Code:</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidUserDepartmentId" runat="server" />
                                <eShip:CustomTextBox ID="txtCode" MaxLength="50" runat="server" CssClass="w300" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtCode"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Description:</label>

                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtDescription" MaxLength="200" runat="server" CssClass="w300" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvDescription" ControlToValidate="txtDescription"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Phone:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtPhone" MaxLength="50" CssClass="w300"></eShip:CustomTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Fax:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtFax" MaxLength="50" CssClass="w300"></eShip:CustomTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Email:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtEmail" MaxLength="100" CssClass="w300"></eShip:CustomTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">
                                    Comments:
                                </label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleComments" TargetControlId="txtComments" MaxLength="500" />
                                </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtComments" TextMode="MultiLine" CssClass="w300" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
