﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class UserDepartmentView : MemberPageBase, IUserDepartmentView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/UserDepartmentView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.UserDepartment; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public event EventHandler<ViewEventArgs<UserDepartment>> Save;
        public event EventHandler<ViewEventArgs<UserDepartment>> Delete;
        public event EventHandler<ViewEventArgs<UserDepartment>> Lock;
        public event EventHandler<ViewEventArgs<UserDepartment>> UnLock;
        public event EventHandler<ViewEventArgs<string>> Search;
        public event EventHandler<ViewEventArgs<List<UserDepartment>>> BatchImport;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<UserDepartment> userDepartments)
        {
            litRecordCount.Text = userDepartments.BuildRecordCount();
            lvwRegistry.DataSource = userDepartments.OrderBy(c => c.Code);
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<UserDepartment>(new UserDepartment(hidUserDepartmentId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtComments.Text = string.Empty;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new UserDepartmentHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.UserDepartmentImportTemplate });
            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidUserDepartmentId.Value = string.Empty;
            GeneratePopup("Add User Department");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify User Department");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("userDepartmentId").ToCustomHiddenField();

            var userDepartment = new UserDepartment(hidden.Value.ToLong(), false);

            hidUserDepartmentId.Value = userDepartment.Id.ToString();
            txtCode.Text = userDepartment.Code;
            txtDescription.Text = userDepartment.Description;
            txtPhone.Text = userDepartment.Phone;
            txtFax.Text = userDepartment.Fax;
            txtEmail.Text = userDepartment.Email;
            txtComments.Text = userDepartment.Comments;

            if (Lock != null)
                Lock(this, new ViewEventArgs<UserDepartment>(userDepartment));
        }

        protected void OnCloseClicked(object sender, EventArgs eventArgs)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var userDepartmentId = hidUserDepartmentId.Value.ToLong();

            var userDepartment = new UserDepartment(userDepartmentId, userDepartmentId != default(long))
            {
                Code = txtCode.Text,
                Description = txtDescription.Text,
                Phone = txtPhone.Text,
                Fax = txtFax.Text,
                Email = txtEmail.Text.StripSpacesFromEmails(),
                Comments = txtComments.Text
            };

            if (userDepartment.IsNew) userDepartment.TenantId = ActiveUser.TenantId;

            if (Save != null)
                Save(this, new ViewEventArgs<UserDepartment>(userDepartment));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("userDepartmentId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<UserDepartment>(new UserDepartment(hidden.Value.ToLong(), true)));

            DoSearch();
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "USER DEPARTMENTS IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                     .Select(item => new[]
					             	{
									    item.FindControl("litCode").ToLiteral().Text,
										item.FindControl("litDescription").ToLiteral().Text,
										item.FindControl("litPhone").ToLiteral().Text,
										item.FindControl("litFax").ToLiteral().Text,
										item.FindControl("litEmail").ToLiteral().Text,
										item.FindControl("litComments").ToLiteral().Text
					             	}.TabJoin())
                     .ToList();
            lines.Insert(0, new[] { "Code", "Description", "Phone", "Fax", "Email", "Comments" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "User Departments.txt");
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 6;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var userDepartments = lines
                .Select(s => new UserDepartment
                {
                    Code = s[0],
                    Description = s[1],
                    TenantId = ActiveUser.TenantId,
                    Phone = s[2],
                    Fax = s[3],
                    Email = s[4].StripSpacesFromEmails(),
                    Comments = s[5]
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<UserDepartment>>(userDepartments));
            fileUploader.Visible = false;
            DoSearch();
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }
    }
}