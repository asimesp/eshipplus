﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ShipmentPriorityView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.ShipmentPriorityView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Shipment Priorities<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="fat" />
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentPriorityTable" TableId="shipmentPriorityTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="shipmentPriorityTable" class="line2 pl2">
                        <tr>
                            <th style="width: 20%;">Code</th>
                            <th style="width: 45%;">Description</th>
                            <th style="width: 15%;" class="text-center">Priority Color</th>
                            <th style="width: 5%;" class="text-center">Default</th>
                            <th style="width: 5%;" class="text-center">Active</th>
                            <th style="width: 10%;" class="text-center">Action</th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="shipmentPriorityId" runat="server" Value='<%# Eval("Id") %>' />
                        <eShip:CustomHiddenField ID="hidPriorityColor" runat="server" Value='<%# Eval("PriorityColor") %>' />
                        <asp:Literal ID="litCode" runat="server" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litDescription" runat="server" Text='<%# Eval("Description") %>' />
                    </td>
                    <td class="text-center">
                        <eShip:CustomTextBox ID="txtPriorityColor" runat="server" Enabled="false" CssClass="w40" Style='<%# Eval("PriorityColor", "background-color: #{0} !important") %>' />
                    </td>
                    <td class="text-center">
                        <%# Eval("Default").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                        <eShip:CustomHiddenField runat="server" ID="hidDefault" Value='<%# Eval("Default").ToBoolean().GetString() %>' />
                    </td>
                    <td class="text-center">
                        <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                        <eShip:CustomHiddenField runat="server" ID="hidActive" Value='<%# Eval("Active").ToBoolean().GetString() %>' />
                    </td>
                    <td>
                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Code: </label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomHiddenField ID="hidShipmentPriorityId" runat="server" />
                                <eShip:CustomTextBox ID="txtCode" CssClass="w300" MaxLength="50" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtCode"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">
                                    Description: 
                                </label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="500" />
                                </label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtDescription" runat="server" CssClass="w300" TextMode="MultiLine"
                                    Rows="3" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvDescription" ControlToValidate="txtDescription"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Priority Color: </label>
                            </td>
                            <td class="text-left">
                                <script type="text/javascript">
                                    $(document).delegate('#<%= txtEditPriorityColor.ClientID %>', 'click select', function () {
                                        $('#<%= Colorpickerextender1.ClientID  %>_popupDiv').css('z-index', '10000');
                                    });

                                    $(document).ready(function () {
                                        // if there is no text value set by ajaxColorPicker, then no background color should be set, therefore remove background-color style
                                        var txtEditPriorityColor = $(jsHelper.AddHashTag('<%= txtEditPriorityColor.ClientID %>'));
                                        if (txtEditPriorityColor.val() == jsHelper.EmptyString && txtEditPriorityColor.attr('style') != null) {
                                            txtEditPriorityColor.attr('style').replace(getBackGroundColorStyle(txtEditPriorityColor.attr('style')), jsHelper.EmptyString);
                                        }

                                        setBackGroundColorImportant();
                                    });

                                    function setBackGroundColorImportant() {
                                        // needs to wait 50 milliseconds before executing due to how ColorPickerExtender OnClientColorSelectionChanged works
                                        $(this).delay(150).queue(function (nextFunction) {
                                            var style = $(jsHelper.AddHashTag('<%= txtEditPriorityColor.ClientID %>')).attr('style');
                                            if (style == null) return;

                                            var bgColorStyle = getBackGroundColorStyle(style);

                                            $(jsHelper.AddHashTag('<%= txtEditPriorityColor.ClientID %>')).attr('style', style.replace(bgColorStyle, bgColorStyle + " !important;"));

                                            nextFunction();
                                        });
                                    }

                                    function getBackGroundColorStyle(style) {
                                        var bgColorIndexStart = style.indexOf("background-color:");
                                        if (bgColorIndexStart == -1) return jsHelper.EmptyString;

                                        var bgColorIndexEnd = style.substr(bgColorIndexStart).indexOf(";");
                                        var bgColorStyle = style.substr(bgColorIndexStart, bgColorIndexEnd);

                                        return bgColorStyle;
                                    }
                                </script>
                                <eShip:CustomTextBox ID="txtEditPriorityColor" runat="server" CssClass="w40" ForeColor="transparent" />
                                <ajax:ColorPickerExtender ID="Colorpickerextender1" runat="server" TargetControlID="txtEditPriorityColor"
                                    SampleControlID="txtEditPriorityColor" OnClientColorSelectionChanged="setBackGroundColorImportant" />
                                <asp:CheckBox ID="chkDefault" runat="server" CssClass="jQueryUniform ml10" />
                                <label class="upper w70">Default</label>
                                <asp:CheckBox ID="chkActive" runat="server" CssClass="jQueryUniform" />
                                <label class="upper w90">Active</label></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" OnYes="OnForceDefaultShipmantPriority" OnNo="OnDoNotForceDefaultShipmantPriority" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
