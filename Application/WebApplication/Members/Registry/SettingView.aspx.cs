﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class SettingView : MemberPageBase, ISettingView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/SettingView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.Setting; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public Dictionary<int, string> Codes
        {
            set
            {
                ddlSettingCodes.DataSource = value ?? new Dictionary<int, string>();
                ddlSettingCodes.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Setting>> Save;
        public event EventHandler<ViewEventArgs<string>> Search;
        public event EventHandler<ViewEventArgs<Setting>> Lock;
        public event EventHandler<ViewEventArgs<Setting>> UnLock;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<Setting> settings)
        {
            litRecordCount.Text = settings.BuildRecordCount();
            lvwRegistry.DataSource = settings;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtValue.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Setting>(new Setting(hidSettingId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            ddlSettingCodes.SelectedIndex = 0;
            txtValue.Text = string.Empty;
        }

        private Setting RetrieveViewSetting()
        {
            var settingId = hidSettingId.Value.ToLong();

            var setting = new Setting(settingId, settingId != default(long))
            {

                Code = ddlSettingCodes.SelectedValue.ToEnum<SettingCode>()
            };

            switch (setting.Code)
            {
                case SettingCode.SpecialistSupportEmail:
                    setting.Value = txtValue.Text.StripSpacesFromEmails();
                    break;
                case SettingCode.SalesTeamEmail:
                    setting.Value = txtValue.Text.StripSpacesFromEmails();
                    break;
                case SettingCode.MonitorEmail:
                    setting.Value = txtValue.Text.StripSpacesFromEmails();
                    break;
                case SettingCode.DeveloperAccessRequestSupportEmail:
                    setting.Value = txtValue.Text.StripSpacesFromEmails();
                    break;
                case SettingCode.TenantAdminNotificationEmail:
                    setting.Value = txtValue.Text.StripSpacesFromEmails();
                    break;
                default:
                    setting.Value = txtValue.Text.Trim();
                    break;
            }

            if (setting.IsNew) setting.TenantId = ActiveUser.TenantId;
            return setting;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new SettingHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidSettingId.Value = string.Empty;
            GeneratePopup("Add Setting");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Setting");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("settingId").ToCustomHiddenField();

            var setting = new Setting(hidden.Value.ToLong(), false);

            hidSettingId.Value = setting.Id.ToString();
            ddlSettingCodes.SelectedValue = ((int)setting.Code).ToString();
            txtValue.Text = setting.Value;

            if (Lock != null)
                Lock(this, new ViewEventArgs<Setting>(setting));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var setting = RetrieveViewSetting();

            if (Save != null)
                Save(this, new ViewEventArgs<Setting>(setting));

            DoSearch();

            // reset setting if applicable.  If save failed, then value is set to previous anyways
            setting.UpdateApplicableSetting();
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Setting>(RetrieveViewSetting()));
            }
        }
    }
}
