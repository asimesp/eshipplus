﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class DocumentTemplateView : MemberPageBase, IDocumentTemplateView
    {
        private bool _forceDefault;

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/DocumentTemplateView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.DocumentTemplate; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public bool ForceDefault
        {
            get { return _forceDefault; }
        }

        public List<string> DecimalPlaces
        {
            set
            {
                var source = value.Select(v => new ViewListItem(v, v)).ToList();

                ddlCurrencyDecimals.DataSource = source;
                ddlCurrencyDecimals.DataBind();

                ddlDimensionDecimals.DataSource = source;
                ddlDimensionDecimals.DataBind();

                ddlWeightDecimals.DataSource = source;
                ddlWeightDecimals.DataBind();
            }
        }

        public Dictionary<int, string> Categories
        {
            set
            {
                ddlCategories.DataSource = value ?? new Dictionary<int, string>();
                ddlCategories.DataBind();
            }
        }

        public Dictionary<int, string> ServiceModes
        {
            set
            {
                ddlServiceModes.DataSource = value ?? new Dictionary<int, string>();
                ddlServiceModes.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<DocumentTemplate>> Save;
        public event EventHandler<ViewEventArgs<DocumentTemplate>> Delete;
        public event EventHandler<ViewEventArgs<string>> Search;
        public event EventHandler<ViewEventArgs<DocumentTemplate>> Lock;
        public event EventHandler<ViewEventArgs<DocumentTemplate>> UnLock;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<DocumentTemplate> documentTemplates)
        {
            litRecordCount.Text = documentTemplates.BuildRecordCount();
            lvwRegistry.DataSource = documentTemplates;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings())
            {
                CloseEditPopUp = true;

                // clean up file cleared out!
                if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
                    Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
            }
            else
            {
                CloseEditPopUp = false;
            }

            hidFilesToDelete.Value = string.Empty;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public bool TemplateFileHasBeenSaved()
        {
            if (!string.IsNullOrEmpty(hidTemporaryPath.Value))
            {
                try
                {
                    var newFilePath = Server.MapPath(hidTemplatePath.Value);
                    var oldFilePath = hidTemporaryPath.Value;
                    hidTemporaryPath.Value = string.Empty;

                    File.Copy(oldFilePath, newFilePath);
                    File.Delete(oldFilePath);
                }
                catch (Exception ex)
                {
                    LogException(ex);
                    return false;
                }
            }

            return true;
        }



        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        private void DoSave(DocumentTemplate documentTemplate)
        {
            if (Save != null)
            {
                // remove from cache
                DocumentProcessor.Drop(documentTemplate);



                Save(this, new ViewEventArgs<DocumentTemplate>(documentTemplate));
            }

            DoSearch();
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<DocumentTemplate>(new DocumentTemplate(hidDocumentTemplateId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            litTemplate.Text = string.Empty;
            hidTemplatePath.Value = string.Empty;
            chkPrimary.Checked = false;
        }

        private DocumentTemplate RetrieveViewDocumentTemplate()
        {
            var documentTemplateId = hidDocumentTemplateId.Value.ToLong();

            var documentTemplate = new DocumentTemplate(documentTemplateId, documentTemplateId != default(long))
            {
                TemplatePath = hidTemplatePath.Value,
                Description = txtDescription.Text,
                Code = txtCode.Text,
                Primary = chkPrimary.Checked,
                Category = ddlCategories.SelectedValue.ToEnum<DocumentTemplateCategory>(),
                ServiceMode = ddlServiceModes.SelectedValue.ToEnum<ServiceMode>(),
                WeightDecimals = ddlWeightDecimals.SelectedValue,
                DimensionDecimals = ddlDimensionDecimals.SelectedValue,
                CurrencyDecimals = ddlCurrencyDecimals.SelectedValue,
            };

            if (documentTemplate.IsNew) documentTemplate.TenantId = ActiveUser.TenantId;

            return documentTemplate;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new DocumentTemplateHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;

            hidTemplatePath.Value = string.Empty;

            if (Loading != null)
                Loading(this, new EventArgs());

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidDocumentTemplateId.Value = string.Empty;
            GeneratePopup("Add Document Template");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Document Template");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("documentTemplateId").ToCustomHiddenField();

            var documentTemplate = new DocumentTemplate(hidden.Value.ToLong(), false);

            hidDocumentTemplateId.Value = documentTemplate.Id.ToString();
            litTemplate.Text = GetTemplateFileName(documentTemplate.TemplatePath);
            hidTemplatePath.Value = documentTemplate.TemplatePath;
            txtCode.Text = documentTemplate.Code;
            txtDescription.Text = documentTemplate.Description;
            chkPrimary.Checked = documentTemplate.Primary;
            ddlCategories.SelectedValue = documentTemplate.Category.ToInt().ToString();
            ddlServiceModes.SelectedValue = documentTemplate.ServiceMode.ToInt().ToString();
            ddlWeightDecimals.SelectedValue = documentTemplate.WeightDecimals;
            ddlDimensionDecimals.SelectedValue = documentTemplate.DimensionDecimals;
            ddlCurrencyDecimals.SelectedValue = documentTemplate.CurrencyDecimals;

            if (Lock != null)
                Lock(this, new ViewEventArgs<DocumentTemplate>(documentTemplate));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var documentTemplate = RetrieveViewDocumentTemplate();

            if (fupTemplate.HasFile)
            {
                var templateFolder = WebApplicationSettings.DocumentTemplateFolder(documentTemplate.TenantId);
                var virtualDocTemplatePath = Path.Combine(templateFolder, fupTemplate.FileName);
                var physicalDocTemplatePath = Server.MapPath(virtualDocTemplatePath);

                var fi = new FileInfo(Server.MakeUniqueFilename(physicalDocTemplatePath, true));

                var tempUniqueCode = Guid.NewGuid().ToString();
                hidTempUniqueCode.Value = tempUniqueCode;
                var tempUniqueName = string.Format("{0}{1}", fi.Name, tempUniqueCode);
                fupTemplate.WriteToFile(Server.MapPath(WebApplicationSettings.TempFolder), tempUniqueName, string.Empty);
                hidTemporaryPath.Value = Server.MapPath(Path.Combine(WebApplicationSettings.TempFolder, tempUniqueName));

                //set into hidden field here in case the template must be marked primary
                hidTemplatePath.Value = Path.Combine(templateFolder, fi.Name);

                if (!documentTemplate.IsNew)
                    hidFilesToDelete.Value += string.Format("{0};", documentTemplate.TemplatePath);

                documentTemplate.TemplatePath = hidTemplatePath.Value;
            }

            if (documentTemplate.Primary)
            {
                messageBox.Button = MessageButton.YesNo;
                messageBox.Icon = MessageIcon.Question;
                messageBox.Message =
                    @"This Document Template has been marked primary. This may conflict with an existing primary document template. Click yes to set this as the new default or no the keep the existing default.";

                messageBox.Visible = true;
                return;
            }

            DoSave(documentTemplate);
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("documentTemplateId").ToCustomHiddenField();

            if (Delete != null)
            {
                var documentTemplate = new DocumentTemplate(hidden.Value.ToLong(), true);

                // setup to remove template
                hidFilesToDelete.Value += string.Format("{0};", documentTemplate.TemplatePath);

                // remove from cache
                DocumentProcessor.Drop(documentTemplate);

                Delete(this, new ViewEventArgs<DocumentTemplate>(documentTemplate));
            }

            DoSearch();
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<DocumentTemplate>(RetrieveViewDocumentTemplate()));
            }
        }


        protected void OnDoNotForcePrimaryDocumentTemplate(object sender, EventArgs e)
        {
            _forceDefault = false;
            DoSave(RetrieveViewDocumentTemplate());
        }

        protected void OnForcePrimaryDocumentTemplate(object sender, EventArgs e)
        {
            _forceDefault = true;
            DoSave(RetrieveViewDocumentTemplate());
        }


        protected string GetTemplateFileName(object relativePath)
        {
            if (relativePath == null) return string.Empty;
            var path = relativePath.ToString();
            if (string.IsNullOrEmpty(path)) return string.Empty;
            return new FileInfo(Server.MapPath(path)).Name;
        }


        protected void OnTempathPathClicked(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var path = button.Parent.FindControl("hidTemplatePath").ToCustomHiddenField().Value;
            Response.Export(Server.ReadFromFile(path), button.Text);
        }
    }
}
