﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using P44SDK.V4.Model;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class PackageTypeView : MemberPageBase, IPackageTypeView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/PackageTypeView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.PackageType; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public Dictionary<int, string> Project44Codes
        {
            set
            {
                var source = (value ?? new Dictionary<int, string>());
                ddlProject44Code.DataSource = source.OrderBy(k => k.Key).ToDictionary(k => k.Key,
                    k => string.Format("{0} ({1})", k.Value.ToEnum<LineItem.PackageTypeEnum>().GetPackageTypeDescription(), k.Value));
                ddlProject44Code.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<PackageType>> Save;
        public event EventHandler<ViewEventArgs<PackageType>> Delete;
        public event EventHandler<ViewEventArgs<string>> Search;
        public event EventHandler<ViewEventArgs<List<PackageType>>> BatchImport;
        public event EventHandler<ViewEventArgs<PackageType>> Lock;
        public event EventHandler<ViewEventArgs<PackageType>> UnLock;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<PackageType> packageTypes)
        {
            litRecordCount.Text = packageTypes.BuildRecordCount();
            lvwRegistry.DataSource = packageTypes;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtTypeName.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<PackageType>(new PackageType(hidPackageTypeId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtTypeName.Text = string.Empty;
            txtEdiOid.Text = string.Empty;
            chkActive.Checked = true;
            txtDefaultLength.Text = string.Empty;
            txtDefaultHeight.Text = string.Empty;
            txtDefaultWidth.Text = string.Empty;
            txtSortWeight.Text = string.Empty;
            chkEditableDimensions.Checked = true;

        }

        private PackageType RetrieveViewPackageType()
        {
            var packageTypeId = hidPackageTypeId.Value.ToLong();

            var packageType = new PackageType(packageTypeId, packageTypeId != default(long))
            {

                TypeName = txtTypeName.Text,
                EdiOid = txtEdiOid.Text,
                Active = chkActive.Checked,
                DefaultLength = txtDefaultLength.Text.ToDecimal(),
                DefaultHeight = txtDefaultHeight.Text.ToDecimal(),
                DefaultWidth = txtDefaultWidth.Text.ToDecimal(),
                SortWeight = txtSortWeight.Text.ToInt(),
                EditableDimensions = chkEditableDimensions.Checked,
                Project44Code = ddlProject44Code.SelectedValue.ToInt()
            };
            if (packageType.IsNew) packageType.TenantId = ActiveUser.TenantId;
            return packageType;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new PackageTypeHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.PackageTypesImportTemplate });

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidPackageTypeId.Value = string.Empty;
            ddlProject44Code.SelectedIndex = 0;
            GeneratePopup("Add Package Type");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Package Type");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("packageTypeId").ToCustomHiddenField();

            var packageType = new PackageType(hidden.Value.ToLong(), false);

            hidPackageTypeId.Value = packageType.Id.ToString();
            txtTypeName.Text = packageType.TypeName;
            txtEdiOid.Text = packageType.EdiOid;
            chkActive.Checked = packageType.Active;
            txtDefaultLength.Text = packageType.DefaultLength.ToString();
            txtDefaultHeight.Text = packageType.DefaultHeight.ToString();
            txtDefaultWidth.Text = packageType.DefaultWidth.ToString();
            txtSortWeight.Text = packageType.SortWeight.ToString();
            chkEditableDimensions.Checked = packageType.EditableDimensions;
            ddlProject44Code.SelectedValue = packageType.Project44Code.ToString();

            if (Lock != null)
                Lock(this, new ViewEventArgs<PackageType>(packageType));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var packageType = RetrieveViewPackageType();

            if (Save != null)
                Save(this, new ViewEventArgs<PackageType>(packageType));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("packageTypeId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<PackageType>(new PackageType(hidden.Value.ToLong(), true)));

            DoSearch();
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "PACKAGE TYPE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                    .Select(item => new[]
					             	{
									    item.FindControl("litTypeName").ToLiteral().Text.ToString(),
									    item.FindControl("litEdiOid").ToLiteral().Text.ToString(),
										item.FindControl("litDefaultLength").ToLiteral().Text.ToString(),
										item.FindControl("litDefaultWidth").ToLiteral().Text.ToString(),
										item.FindControl("litDefaultHeight").ToLiteral().Text.ToString(),
										item.FindControl("litSortWeight").ToLiteral().Text.ToString(),
										item.FindControl("hidEditableDimensions").ToCustomHiddenField().Value,
										item.FindControl("hidActive").ToCustomHiddenField().Value
					             	}.TabJoin())
                    .ToList();
            lines.Insert(0, new[] { "Type Name", "EDI OID", "Default Length", "Default Width", "Default Height", "Sort Weight", "Can Edit Dims", "Active" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "PackageTypes.txt");
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 8;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var packageTypes = lines

                .Select(s => new PackageType
                {
                    TypeName = s[0],
                    EdiOid = s[1],
                    DefaultLength = s[2].ToDecimal(),
                    DefaultWidth = s[3].ToDecimal(),
                    DefaultHeight = s[4].ToDecimal(),
                    SortWeight = s[5].ToInt(),
                    EditableDimensions = s[6].ToBoolean(),
                    Active = s[7].ToBoolean(),
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<PackageType>>(packageTypes));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<PackageType>(RetrieveViewPackageType()));
            }
        }

    }
}
