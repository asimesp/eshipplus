﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class UserDefinedPermissionDetailView : MemberPageBase, IPermissionDetailView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/UserDefinedPermissionDetailView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.UserDefinedPermissionDetail; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }


        public Dictionary<int, string> Categories
        {
            set
            {
                var source = (value ?? new Dictionary<int, string>());
                ddlCategory.DataSource = source.OrderBy(k => k.Value).ToDictionary(k => k.Key, k => k.Value);
                ddlCategory.DataBind();
            }
        }

        public Dictionary<int, string> Types
        {
            set
            {
                var source = (value ?? new Dictionary<int, string>());
                ddlType.DataSource = source.OrderBy(k => k.Value).ToDictionary(k => k.Key, k => k.Value);
                ddlType.DataBind();
            }
        }


        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<PermissionDetail>> Save;
        public event EventHandler<ViewEventArgs<PermissionDetail>> Delete;
        public event EventHandler<ViewEventArgs<string>> Search;
        public event EventHandler<ViewEventArgs<List<PermissionDetail>>> BatchImport;
        public event EventHandler<ViewEventArgs<PermissionDetail>> Lock;
        public event EventHandler<ViewEventArgs<PermissionDetail>> UnLock;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<PermissionDetail> details)
        {
            litRecordCount.Text = details.BuildRecordCount();

            lvwRegistry.DataSource = details.OrderBy(c => c.Category).ThenBy(c => c.Code);
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak,
                                             messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<PermissionDetail>(new PermissionDetail(hidPermissionDetailId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDisplayCode.Text = string.Empty;
            ddlCategory.SelectedValue = PermissionCategory.Administration.ToInt().ToString();
            ddlType.SelectedValue = PermissionType.System.ToInt().ToString();
            ddlViewCode.SelectedValue = ViewCode.Shipment.ToInt().ToString();
            txtNavigationUrl.Text = string.Empty;
        }

        private PermissionDetail RetrieveViewPermissionDetail()
        {
            var detailId = hidPermissionDetailId.Value.ToLong();

            var type = ddlType.SelectedValue.ToInt().ToEnum<PermissionType>();
	        var code = type == PermissionType.System ? ddlViewCode.SelectedValue.ToInt().ToEnum<ViewCode>().ToString() : txtCode.Text;

	        var detail = new PermissionDetail(detailId, detailId != default(long))
		        {
			        Code = code,
			        Type = type,
			        DisplayCode = txtDisplayCode.Text,
			        NavigationUrl = type == PermissionType.System ? string.Empty : txtNavigationUrl.Text,
			        Category = type == PermissionType.System
				                   ? GetCategory(code)
				                   : ddlCategory.SelectedValue.ToInt().ToEnum<PermissionCategory>(),
		        };
            if (detail.IsNew) detail.TenantId = ActiveUser.TenantId;
            return detail;
        }

		private PermissionCategory GetCategory(string code)
		{
			if(PermissionGenerator<Permission>.Accounting().Any(p=>p.Code == code)) return PermissionCategory.Accounting;
			if(PermissionGenerator<Permission>.Operations().Any(p=>p.Code == code)) return PermissionCategory.Operations;
			if(PermissionGenerator<Permission>.Registry().Any(p=>p.Code == code)) return PermissionCategory.Registry;
			if(PermissionGenerator<Permission>.Rating().Any(p=>p.Code == code)) return PermissionCategory.Rating;
			if(PermissionGenerator<Permission>.BusinessIntelligence().Any(p=>p.Code == code)) return PermissionCategory.BusinessIntelligence;
			if(PermissionGenerator<Permission>.Connect().Any(p=>p.Code == code)) return PermissionCategory.Connect;
			if(PermissionGenerator<Permission>.Administration().Any(p=>p.Code == code)) return PermissionCategory.Administration;
			return PermissionCategory.Utilities;
		}


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new PermissionDetailHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.UserDefinedPermissionDetailImportTemplate });

            if (Loading != null)
                Loading(this, new EventArgs());

            var source = (ProcessorUtilities.GetAll<ViewCode>() ?? new Dictionary<int, string>());
	        ddlViewCode.DataSource = source
		        .Where(k => !k.Key.ToEnum<ViewCode>().ExcludePermission()
		                    && k.Key.ToEnum<ViewCode>() != ViewCode.Null
		                    && k.Key.ToEnum<ViewCode>() != ViewCode.DashboardPadding)
		        .OrderBy(k => k.Value)
		        .ToDictionary(k => k.Key, k => k.Value);
            ddlViewCode.DataBind();

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidPermissionDetailId.Value = string.Empty;
            GeneratePopup("Add User Defined Permission Detail");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify User Defined Permission Detail");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidPermissionDetailId").ToCustomHiddenField();

            var detail = new PermissionDetail(hidden.Value.ToLong(), false);

            hidPermissionDetailId.Value = detail.Id.ToString();

            if (detail.Type == PermissionType.Custom)
            {
	            txtCode.Text = detail.Code;
				ddlCategory.SelectedValue = detail.Category.ToInt().ToString();
            }
            else
            {
	            ddlViewCode.SelectedValue = detail.Code.ToEnum<ViewCode>().ToInt().ToString();
            }

            ddlType.SelectedValue = detail.Type.ToInt().ToString();
            txtDisplayCode.Text = detail.DisplayCode;
            txtNavigationUrl.Text = detail.NavigationUrl;


            if (Lock != null)
                Lock(this, new ViewEventArgs<PermissionDetail>(detail));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var chargeCode = RetrieveViewPermissionDetail();

            if (Save != null)
                Save(this, new ViewEventArgs<PermissionDetail>(chargeCode));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidPermissionDetailId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<PermissionDetail>(new PermissionDetail(hidden.Value.ToLong(), true)));

            DoSearch();
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "USER DEFINED PERMISSION DETAIL IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                                   .Select(item => new[]
                                       {
                                           item.FindControl("litCode").ToLiteral().Text.ToString(),
                                           item.FindControl("litDisplayCode").ToLiteral().Text.ToString(),
                                           item.FindControl("litNavigationUrl").ToLiteral().Text.ToString(),
                                           item.FindControl("litCategory").ToLiteral().Text.ToString(),
                                           item.FindControl("litType").ToLiteral().Text.ToString()
                                       }.TabJoin())
                                   .ToList();
            lines.Insert(0, new[] { "Code", "Display Code", "Navigation Url", "Category", "Type" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "UserDefinedPermissionDetails.txt");
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 5;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<PermissionCategory>(3));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<PermissionType>(4));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.Where(c => c.Line[4].ToEnum<PermissionType>() == PermissionType.System).CodesAreValid(0, Enum.GetNames(typeof(ViewCode)).ToList(), "View Code"));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var chargeCodes = lines
                .Select(s =>
                    {
                        var type = s[4].ToEnum<PermissionType>();

                        return new PermissionDetail
                            {
                                Code = s[0],
                                DisplayCode = s[1],
                                NavigationUrl = type == PermissionType.Custom ? s[2] : string.Empty,
                                Category = s[3].ToEnum<PermissionCategory>(),
                                Type = type,
                                TenantId = ActiveUser.TenantId
                            };
                    })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<PermissionDetail>>(chargeCodes));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<PermissionDetail>(RetrieveViewPermissionDetail()));
            }
        }
    }
}