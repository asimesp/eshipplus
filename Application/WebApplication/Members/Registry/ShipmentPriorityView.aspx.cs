﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class ShipmentPriorityView : MemberPageBase, IShipmentPriorityView
    {
        private bool _forceDefault;

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/ShipmentPriorityView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.ShipmentPriority; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public bool ForceDefault
        {
            get { return _forceDefault; }
        }

        public event EventHandler<ViewEventArgs<ShipmentPriority>> Save;
        public event EventHandler<ViewEventArgs<ShipmentPriority>> Delete;
        public event EventHandler<ViewEventArgs<string>> Search;
        public event EventHandler<ViewEventArgs<ShipmentPriority>> Lock;
        public event EventHandler<ViewEventArgs<ShipmentPriority>> UnLock;
        public event EventHandler<ViewEventArgs<List<ShipmentPriority>>> BatchImport;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<ShipmentPriority> shipmentPriorities)
        {
            litRecordCount.Text = shipmentPriorities.BuildRecordCount();
            lvwRegistry.DataSource = shipmentPriorities;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        private void DoSave(ShipmentPriority shipmentPriority)
        {
            if (Save != null)
                Save(this, new ViewEventArgs<ShipmentPriority>(shipmentPriority));

            DoSearch();
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<ShipmentPriority>(new ShipmentPriority(hidShipmentPriorityId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtEditPriorityColor.Text = string.Empty;
            chkDefault.Checked = true;
            chkActive.Checked = true;
        }

        private ShipmentPriority RetrieveViewShipmentPriority()
        {
            var shipmentPriorityId = hidShipmentPriorityId.Value.ToLong();

            var shipmentPriority = new ShipmentPriority(shipmentPriorityId, shipmentPriorityId != default(long))
            {
                Code = txtCode.Text,
                Description = txtDescription.Text,
                Default = chkDefault.Checked,
                Active = chkActive.Checked,
                PriorityColor = txtEditPriorityColor.Text
            };

            if (shipmentPriority.IsNew) shipmentPriority.TenantId = ActiveUser.TenantId;

            return shipmentPriority;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new ShipmentPriorityHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.ShipmentPrioritiesImportTemplate });

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidShipmentPriorityId.Value = string.Empty;
            GeneratePopup("Add Shipment Priority");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Shipment Priority");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("shipmentPriorityId").ToCustomHiddenField();

            var shipmentPriority = new ShipmentPriority(hidden.Value.ToLong(), false);

            hidShipmentPriorityId.Value = shipmentPriority.Id.ToString();
            txtCode.Text = shipmentPriority.Code;
            txtDescription.Text = shipmentPriority.Description;
            chkDefault.Checked = shipmentPriority.Default;
            chkActive.Checked = shipmentPriority.Active;
            txtEditPriorityColor.Text = string.Format(shipmentPriority.PriorityColor, "#{0}");

            if (Lock != null)
                Lock(this, new ViewEventArgs<ShipmentPriority>(shipmentPriority));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var shipmentPriority = RetrieveViewShipmentPriority();

            if (shipmentPriority.Default)
            {
                messageBox.Button = MessageButton.YesNo;
                messageBox.Icon = MessageIcon.Question;
                messageBox.Message =
                    @"This Shipment Priority has been marked default. This may conflict with an existing default shipment priority. Click yes to set this as the new default or no the keep the existing default.";

                messageBox.Visible = true;
                return;
            }

            DoSave(shipmentPriority);
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("shipmentPriorityId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<ShipmentPriority>(new ShipmentPriority(hidden.Value.ToLong(), true)));

            DoSearch();
        }


        protected void OnDoNotForceDefaultShipmantPriority(object sender, EventArgs e)
        {
            _forceDefault = false;
            DoSave(RetrieveViewShipmentPriority());
        }

        protected void OnForceDefaultShipmantPriority(object sender, EventArgs e)
        {
            _forceDefault = true;
            DoSave(RetrieveViewShipmentPriority());
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "SHIPMENT PRIORITY IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                    .Select(item => new[]
					             	{
									    item.FindControl("litCode").ToLiteral().Text,
										item.FindControl("litDescription").ToLiteral().Text,
										item.FindControl("hidDefault").ToCustomHiddenField().Value,
										item.FindControl("hidActive").ToCustomHiddenField().Value,
                                        item.FindControl("hidPriorityColor").ToCustomHiddenField().Value
					             	}.TabJoin())
                    .ToList();
            lines.Insert(0, new[] { "Code", "Description", "Default", "Active", "Color" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "ShipmentPriorities.txt");
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 5;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var shipmentPriorities = lines
                .Select(s => new ShipmentPriority
                {
                    Code = s[0],
                    Description = s[1],
                    Default = s[2].ToBoolean(),
                    Active = s[3].ToBoolean(),
                    PriorityColor = s[4],
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<ShipmentPriority>>(shipmentPriorities));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<ShipmentPriority>(RetrieveViewShipmentPriority()));
            }
        }
    }
}
