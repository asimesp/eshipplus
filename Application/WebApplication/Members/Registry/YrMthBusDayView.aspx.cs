﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class YrMthBusDayView : MemberPageBase, IYrMthBusDayView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/YrMthBusDayView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.YearMonthlyBusinessDay; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public event EventHandler<ViewEventArgs<YrMthBusDay>> Save;
        public event EventHandler<ViewEventArgs<YrMthBusDay>> Delete;
        public event EventHandler<ViewEventArgs<YrMthBusDay>> Lock;
        public event EventHandler<ViewEventArgs<YrMthBusDay>> UnLock;
        public event EventHandler<ViewEventArgs<string>> Search;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<YrMthBusDay> mthBusDays)
        {
            litRecordCount.Text = mthBusDays.BuildRecordCount();
            lvwRegistry.DataSource = mthBusDays.OrderByDescending(m => m.Year.ToInt());
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtYear.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<YrMthBusDay>(new YrMthBusDay(hidYrMthBusDayId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtYear.Text = string.Empty;
            txtJanuary.Text = string.Empty;
            txtFebruary.Text = string.Empty;
            txtMarch.Text = string.Empty;
            txtApril.Text = string.Empty;
            txtMay.Text = string.Empty;
            txtJune.Text = string.Empty;
            txtJuly.Text = string.Empty;
            txtAugust.Text = string.Empty;
            txtSeptember.Text = string.Empty;
            txtOctober.Text = string.Empty;
            txtNovember.Text = string.Empty;
            txtDecember.Text = string.Empty;
        }

        private YrMthBusDay RetrieveViewMthBusDay()
        {
            var id = hidYrMthBusDayId.Value.ToLong();

            var mthBusDay = new YrMthBusDay(id, id != default(long))
            {
                Year = txtYear.Text,
                January = txtJanuary.Text.ToInt(),
                February = txtFebruary.Text.ToInt(),
                March = txtMarch.Text.ToInt(),
                April = txtApril.Text.ToInt(),
                May = txtMay.Text.ToInt(),
                June = txtJune.Text.ToInt(),
                July = txtJuly.Text.ToInt(),
                August = txtAugust.Text.ToInt(),
                September = txtSeptember.Text.ToInt(),
                October = txtOctober.Text.ToInt(),
                November = txtNovember.Text.ToInt(),
                December = txtDecember.Text.ToInt(),
            };
            if (mthBusDay.IsNew) mthBusDay.TenantId = ActiveUser.TenantId;
            return mthBusDay;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new YrMthBusDayHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs eventArgs)
        {
            ClearFields();
            hidYrMthBusDayId.Value = string.Empty;
            GeneratePopup("Add Year Monthly Business Days");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Year Monthly Business Days");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("yrMthBusDayId").ToCustomHiddenField();

            var mthBusDay = new YrMthBusDay(hidden.Value.ToLong(), false);

            hidYrMthBusDayId.Value = mthBusDay.Id.ToString();
            txtYear.Text = mthBusDay.Year;
            txtJanuary.Text = mthBusDay.January.ToString();
            txtFebruary.Text = mthBusDay.February.ToString();
            txtMarch.Text = mthBusDay.March.ToString();
            txtApril.Text = mthBusDay.April.ToString();
            txtMay.Text = mthBusDay.May.ToString();
            txtJune.Text = mthBusDay.June.ToString();
            txtJuly.Text = mthBusDay.July.ToString();
            txtAugust.Text = mthBusDay.August.ToString();
            txtSeptember.Text = mthBusDay.September.ToString();
            txtOctober.Text = mthBusDay.October.ToString();
            txtNovember.Text = mthBusDay.November.ToString();
            txtDecember.Text = mthBusDay.December.ToString();

            if (Lock != null)
                Lock(this, new ViewEventArgs<YrMthBusDay>(mthBusDay));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var mthBusDay = RetrieveViewMthBusDay();

            if (Save != null)
                Save(this, new ViewEventArgs<YrMthBusDay>(mthBusDay));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("yrMthBusDayId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<YrMthBusDay>(new YrMthBusDay(hidden.Value.ToLong(), true)));

            DoSearch();
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<YrMthBusDay>(RetrieveViewMthBusDay()));
            }
        }
    }
}
