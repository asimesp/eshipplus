﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class MileageSourceView : MemberPageBase, IMileageSourceView
    {
        private bool _forcePrimary;

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/MileageSourceView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.MileageSource; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public bool ForcePrimary
        {
            get { return _forcePrimary; }
        }

        public Dictionary<int, string> MileageEngines
        {
            set
            {
                ddlMilageEngine.DataSource = value
                    .Select(t => new ViewListItem(t.Value, t.Key.ToString()))
                    .OrderBy(t => t.Text)
                    .ToList();
                ddlMilageEngine.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<MileageSource>> Save;
        public event EventHandler<ViewEventArgs<MileageSource>> Delete;
        public event EventHandler<ViewEventArgs<string>> Search;
        public event EventHandler<ViewEventArgs<MileageSource>> Lock;
        public event EventHandler<ViewEventArgs<MileageSource>> UnLock;
        public event EventHandler<ViewEventArgs<List<MileageSource>>> BatchImport;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();

        }

        public void DisplaySearchResult(List<MileageSource> mileageSources)
        {
            litRecordCount.Text = mileageSources.BuildRecordCount();
            lvwRegistry.DataSource = mileageSources;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        private void DoSave(MileageSource mileageSource)
        {
            if (Save != null)
                Save(this, new ViewEventArgs<MileageSource>(mileageSource));

            DoSearch();
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;
            if (UnLock != null)
                UnLock(this, new ViewEventArgs<MileageSource>(new MileageSource(hidMileageSourceId.Value.ToLong(), false)));

        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            chkPrimary.Checked = false;
        }

        private MileageSource RetrieveMileageSource()
        {
            var mileageSourceId = hidMileageSourceId.Value.ToLong();

            var mileageSource = new MileageSource(mileageSourceId, mileageSourceId != default(long))
            {
                Code = txtCode.Text,
                Description = txtDescription.Text,
                Primary = chkPrimary.Checked,
                MileageEngine = ddlMilageEngine.SelectedValue.ToInt().ToEnum<MileageEngine>()
            };

            if (mileageSource.IsNew) mileageSource.TenantId = ActiveUser.TenantId;
            return mileageSource;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new MileageSourceHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.MileageSourcesImportTemplate });

            if (Loading != null)
                Loading(this, new EventArgs());

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidMileageSourceId.Value = string.Empty;
            var defaultMileageEngine = ActiveUser.Tenant.DefaultMileageEngine.ToInt().GetString();
            if (ddlMilageEngine.ContainsValue(defaultMileageEngine)) ddlMilageEngine.SelectedValue = defaultMileageEngine;
            GeneratePopup("Add Mileage Source");
        }


        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Mileage Source");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("mileageSourceId").ToCustomHiddenField();

            var mileageSource = new MileageSource(hidden.Value.ToLong(), false);

            hidMileageSourceId.Value = mileageSource.Id.ToString();
            txtCode.Text = mileageSource.Code;
            txtDescription.Text = mileageSource.Description;
            chkPrimary.Checked = mileageSource.Primary;
            ddlMilageEngine.SelectedValue = mileageSource.MileageEngine.ToInt().ToString();

            if (Lock != null)
                Lock(this, new ViewEventArgs<MileageSource>(mileageSource));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var mileageSource = RetrieveMileageSource();

            if (mileageSource.Primary)
            {
                messageBox.Button = MessageButton.YesNo;
                messageBox.Icon = MessageIcon.Question;
                messageBox.Message =
                    @"This Mileage Source has been marked primary. This may conflict with an existing primary mileage source. Click yes to set this as the new primary or no the keep the existing primary.";

                messageBox.Visible = true;
                return;
            }

            DoSave(mileageSource);
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("mileageSourceId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<MileageSource>(new MileageSource(hidden.Value.ToLong(), true)));

            DoSearch();
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "MILEAGE SOURCE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                    .Select(item => new[]
					             	{
									    item.FindControl("litCode").ToLiteral().Text.ToString(),
										item.FindControl("litDescription").ToLiteral().Text.ToString(),
										item.FindControl("hidPrimary").ToCustomHiddenField().Value,
										item.FindControl("hidMileageEngine").ToCustomHiddenField().Value.FormattedString()
					             	}.TabJoin())
                    .ToList();
            lines.Insert(0, new[] { "Code", "Description", "Primary", "Mileage Engine" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "MileageSources.txt");
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 4;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<MileageEngine>(3));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var mileageSource = lines
                .Select(s => new MileageSource
                {
                    Code = s[0],
                    Description = s[1],
                    Primary = s[2].ToBoolean(),
                    MileageEngine = s[3].ToEnum<MileageEngine>(),
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<MileageSource>>(mileageSource));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<MileageSource>(RetrieveMileageSource()));
            }
        }


        protected void OnDoNotForcePrimaryMileageSource(object sender, EventArgs e)
        {
            _forcePrimary = false;
            DoSave(RetrieveMileageSource());
        }

        protected void OnForcePrimaryMileageSource(object sender, EventArgs e)
        {
            _forcePrimary = true;
            DoSave(RetrieveMileageSource());
        }
    }
}
