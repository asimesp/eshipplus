﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class ChargeCodeView : MemberPageBase, IChargeCodeView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/ChargeCodeView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.ChargeCode; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public Dictionary<int, string> Categories
        {
            set
            {
                var source = (value ?? new Dictionary<int, string>());
                ddlCategories.DataSource = source.OrderBy(k => k.Value).ToDictionary(k => k.Key, k => k.Value);
                ddlCategories.DataBind();
            }
        }

        public Dictionary<int, string> Project44Codes
        {
            set
            {
                var source = (value ?? new Dictionary<int, string>());
                ddlProject44Code.DataSource = source.OrderBy(k => k.Key).ToDictionary(k => k.Key,
                    k => string.Format("{0} ({1})", k.Value.ToEnum<Project44ChargeCode>().GetChargeCodeDescription(), k.Value));
                ddlProject44Code.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<ChargeCode>> Save;
        public event EventHandler<ViewEventArgs<ChargeCode>> Delete;
        public event EventHandler<ViewEventArgs<string>> Search;
        public event EventHandler<ViewEventArgs<List<ChargeCode>>> BatchImport;
        public event EventHandler<ViewEventArgs<ChargeCode>> Lock;
        public event EventHandler<ViewEventArgs<ChargeCode>> UnLock;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<ChargeCode> chargeCodes)
        {
            litRecordCount.Text = chargeCodes.BuildRecordCount();

            lvwRegistry.DataSource = chargeCodes.OrderBy(c => c.Code).ThenBy(c => c.Category);
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak,
                                             messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<ChargeCode>(new ChargeCode(hidChargeCodeId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            ddlCategories.SelectedIndex = 0;
            ddlProject44Code.SelectedIndex = 0;
        }

        private ChargeCode RetrieveViewChargeCode()
        {
            var chargeCodeId = hidChargeCodeId.Value.ToLong();

            var chargeCode = new ChargeCode(chargeCodeId, chargeCodeId != default(long))
                {
                    Code = txtCode.Text,
                    Description = txtDescription.Text,
                    Active = chkActive.Checked,
                    Category = ddlCategories.SelectedValue.ToEnum<ChargeCodeCategory>(),
                    SurpressOnCarrierRateAgreement = chkSuppress.Checked,
                    Project44Code = ddlProject44Code.SelectedValue.ToEnum<Project44ChargeCode>()
                };
            if (chargeCode.IsNew) chargeCode.TenantId = ActiveUser.TenantId;
            return chargeCode;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new ChargeCodeHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> {WebApplicationConstants.ChargeCodeImportTemplate});

            if (Loading != null)
                Loading(this, new EventArgs());

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidChargeCodeId.Value = string.Empty;
            GeneratePopup("Add Charge Code");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Charge Code");

            var imageButton = (ImageButton) sender;
            var hidden = imageButton.Parent.FindControl("chargeCodeId").ToCustomHiddenField();

            var chargeCode = new ChargeCode(hidden.Value.ToLong(), false);

            hidChargeCodeId.Value = chargeCode.Id.ToString();
            txtCode.Text = chargeCode.Code;
            txtDescription.Text = chargeCode.Description;
            ddlCategories.SelectedValue = ((int) (chargeCode.Category)).ToString();
            ddlProject44Code.SelectedValue = (chargeCode.Project44Code.ToInt()).ToString();
            chkActive.Checked = chargeCode.Active;
            chkSuppress.Checked = chargeCode.SurpressOnCarrierRateAgreement;

            if (Lock != null)
                Lock(this, new ViewEventArgs<ChargeCode>(chargeCode));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var chargeCode = RetrieveViewChargeCode();

            if (Save != null)
                Save(this, new ViewEventArgs<ChargeCode>(chargeCode));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton) sender;
            var hidden = imageButton.Parent.FindControl("chargeCodeId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<ChargeCode>(new ChargeCode(hidden.Value.ToLong(), true)));

            DoSearch();
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "CHARGE CODE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                                   .Select(item => new[]
                                       {
                                           item.FindControl("litCode").ToLiteral().Text.ToString(),
                                           item.FindControl("litDescription").ToLiteral().Text.ToString(),
                                           item.FindControl("litCategory").ToLiteral().Text.ToEnum<ChargeCodeCategory>().ToInt().ToString(),
                                           item.FindControl("hidActive").ToCustomHiddenField().Value.ToString(),
                                           item.FindControl("hidSuppress").ToCustomHiddenField().Value.ToString()
                                       }.TabJoin())
                                   .ToList();
            lines.Insert(0, new[] {"Code", "Description", "Category", "Active", "Suppress on Rate Confirmation"}.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "ChargeCodes.txt");
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> {ValidationMessage.Error(ex.Message)});
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 5;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<ChargeCodeCategory>(2));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var chargeCodes = lines
                .Select(s => new ChargeCode
                    {
                        Code = s[0],
                        Description = s[1],
                        Category = s[2].ToEnum<ChargeCodeCategory>(),
                        Active = s[3].ToBoolean(),
                        SurpressOnCarrierRateAgreement = s[4].ToBoolean(),
                        TenantId = ActiveUser.TenantId
                    })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<ChargeCode>>(chargeCodes));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<ChargeCode>(RetrieveViewChargeCode()));
            }
        }
    }
}