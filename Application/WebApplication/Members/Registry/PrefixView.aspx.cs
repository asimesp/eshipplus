﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class PrefixView : MemberPageBase, IPrefixView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/PrefixView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.Prefix; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public event EventHandler<ViewEventArgs<Prefix>> Save;
        public event EventHandler<ViewEventArgs<Prefix>> Delete;
        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<List<Prefix>>> BatchImport;
        public event EventHandler<ViewEventArgs<Prefix>> Lock;
        public event EventHandler<ViewEventArgs<Prefix>> UnLock;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<Prefix> prefixes)
        {
            litRecordCount.Text = prefixes.BuildRecordCount();
	        lvwRegistry.DataSource = prefixes.OrderBy(p => p.Description);
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(new List<ParameterColumn>()));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {

            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Prefix>(new Prefix(hidPrefixId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            chkActive.Checked = true;
        }

        private Prefix RetrieveViewPrefix()
        {
            var prefixId = hidPrefixId.Value.ToLong();

            var prefix = new Prefix(prefixId, prefixId != default(long))
            {

                Code = txtCode.Text,
                Description = txtDescription.Text,
                Active = chkActive.Checked
            };
            if (prefix.IsNew) prefix.TenantId = ActiveUser.TenantId;
            return prefix;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new PrefixHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.PrefixesImportTemplate });
            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidPrefixId.Value = string.Empty;
            GeneratePopup("Add Prefix");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Prefix");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("prefixId").ToCustomHiddenField();

            var prefix = new Prefix(hidden.Value.ToLong(), false);

            hidPrefixId.Value = prefix.Id.ToString();
            txtCode.Text = prefix.Code;
            txtDescription.Text = prefix.Description;
            chkActive.Checked = prefix.Active;

            if (Lock != null)
                Lock(this, new ViewEventArgs<Prefix>(prefix));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var prefix = RetrieveViewPrefix();

            if (Save != null)
                Save(this, new ViewEventArgs<Prefix>(prefix));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("prefixId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<Prefix>(new Prefix(hidden.Value.ToLong(), true)));

            DoSearch();
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "PREFIX IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                    .Select(item => new[]
					             	{
									    item.FindControl("litCode").ToLiteral().Text.ToString(),
										item.FindControl("litDescription").ToLiteral().Text.ToString(),
										item.FindControl("hidActive").ToCustomHiddenField().Value
					             	}.TabJoin())
                    .ToList();
            lines.Insert(0, new[] { "Code", "Description", "Active" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "Prefixes.txt");
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 3;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var prefixes = lines
                .Select(s => new Prefix
                {
                    Code = s[0],
                    Description = s[1],
                    Active = s[2].ToBoolean(),
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<Prefix>>(prefixes));

            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Prefix>(RetrieveViewPrefix()));
            }
        }
    }
}
