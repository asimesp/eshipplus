﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Members/Member.master" CodeBehind="AccountBucketView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.AccountBucketView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Account Buckets <small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="fat" />

        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAccountBucketTable" TableId="acctBuckDisplayTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnAccountBucketItemDataBound">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="acctBuckDisplayTable" class="line2 pl2">
                        <tr>
                            <th style="width: 15%;">Code</th>
                            <th style="width: 60%;">Description</th>
                            <th style="width: 10%;" class="text-center">Active</th>
                            <th style="width: 10%;">Action</th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td <%# Eval("HasUnits").ToBoolean() ? "rowspan='2' class='top'" : string.Empty %> >
                        <eShip:CustomHiddenField ID="hidAccountBucketId" runat="server" Value='<%# Eval("Id") %>' />
                        <asp:Literal ID="litCode" runat="server" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litDescription" runat="server" Text='<%# Eval("Description") %>' />
                    </td>
                    <td class="text-center">
                        <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                        <eShip:CustomHiddenField runat="server" ID="hidActive" Value='<%# Eval("Active").ToBoolean().GetString() %>' />
                    </td>
                    <td <%# Eval("HasUnits").ToBoolean() ? "rowspan='2'" : string.Empty %> class="top">
                        <asp:ImageButton ID="ibtnEdit" ToolTip="Edit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" ToolTip="Delete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>

                <asp:ListView ID="lstAccountBucketUnits" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <tr class="f9">
                            <td colspan="2" class="forceLeftBorder">
                                <table class="contain pl2">
                                    <tr class="bbb1">
                                        <th style="width: 25%;" class="no-top-border">Account Bucket Unit Name
                                        </th>
                                        <th style="width: 65%;" class="no-top-border no-left-border">Account Bucket Unit Description
                                        </th>
                                         <th style="width: 10%;" class="no-top-border no-left-border text-center">Active
                                        </th>
                                    </tr>
                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                </table>
                            </td>
                        </tr>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class="no-bottom-border">
                            <td class="text-left top">
                                <%# Eval("Name")%>
                            </td>
                            <td class="text-left top">
                                <%# Eval("Description")%>
                            </td>
                             <td class="text-center top">
                                <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </ItemTemplate>
        </asp:ListView>

        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Code:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomHiddenField ID="hidAccountBucketId" runat="server" />
                                <eShip:CustomTextBox ID="txtCode" CssClass="w300" MaxLength="50" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtCode"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">
                                    Description:
                                </label>
                                <br/>
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="200" />
                                </label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtDescription" runat="server" TextMode="MultiLine"
                                    Rows="3" CssClass="w300"/>
                                <asp:RequiredFieldValidator runat="server" ID="rfvDescription" ControlToValidate="txtDescription"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" ForeColor="red"
                                    Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">&nbsp;
                            </td>
                            <td class="text-left">
                                <asp:CheckBox ID="chkActive" runat="server" CssClass="jQueryUniform" />
                                <label class="upper">Active</label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                                <asp:Button runat="server" ID="btnAddAccountBucketUnit" Text="Add Account Bucket Unit" OnClick="OnAddAccountBucketUnitClicked"
                                    CausesValidation="false" />
                            </td>
                        </tr>

                        <asp:ListView runat="server" ID="lstEditAccountBuckets" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <tr>
                                    <td colspan="2">
                                        <hr class="w100p ml-inherit" />
                                        <h5 class="ml10 dark">Account Bucket Units
                                        </h5>
                                        <hr class="w100p ml-inherit dark dotted mb0" />
                                        <table class="ml10 mr20 mb10">
                                            <tr>
                                                <th style="width: 40%;" class="text-left blue">
                                                    <label class="upper">Name</label>
                                                </th>
                                                <th style="width: 50%;" class="text-left blue">
                                                    <label class="upper">Description</label>
                                                </th>
                                                <th style="width: 10%;" class="text-center blue">
                                                    <label class="upper">Active</label>
                                                </th>
                                                <th style="width: 10%;" class="text-center blue">
                                                    <label class="upper">Delete</label>
                                                </th>
                                            </tr>
                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Button ID="btnSave2" Text="Save" OnClick="OnSaveClick" runat="server" />
                                        <asp:Button ID="btnCancel2" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                            CausesValidation="false" />
                                    </td>
                                </tr>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="text-left">
                                        <eShip:CustomHiddenField ID="hidEditAccountBucketUnitId" runat="server" Value='<%# Eval("Id") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidAccountBucketUnitItemIndex" Value='<%# Container.DataItemIndex %>' />
                                        <eShip:CustomTextBox ID="txtName" MaxLength="50" runat="server" Text='<%# Eval("Name") %>' CssClass="w160"/>
                                    </td>
                                    <td class="text-left">
                                        <eShip:CustomTextBox ID="txtDescription" MaxLength="50" runat="server" Text='<%# Eval("Description") %>' CssClass="w170" />
                                    </td>
                                    <td class="text-center">
                                        <asp:CheckBox runat="server" ID="chkActive" CssClass="jQueryUniform" Checked='<%# Eval("Active").ToBoolean() %>'/>
                                    </td>
                                    <td class="text-center">
                                        <asp:ImageButton ID="ibtnAccountBucketUnitDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            OnClick="OnAccountBucketUnitDeleteClicked" CausesValidation="False" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
