﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class AccountBucketView : MemberPageBase, IAccountBucketView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress { get { return "~/Members/Registry/AccountBucketView.aspx"; } }

        public override ViewCode PageCode
        {
            get { return ViewCode.AccountBucket; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public event EventHandler<ViewEventArgs<AccountBucket>> Save;
        public event EventHandler<ViewEventArgs<AccountBucket>> Delete;
        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<List<AccountBucket>>> BatchImport;
        public event EventHandler<ViewEventArgs<AccountBucket>> Lock;
        public event EventHandler<ViewEventArgs<AccountBucket>> UnLock;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<AccountBucket> accountBucket)
        {
	        var data = accountBucket
		        .Select(a => new
			        {
				        a.Id,
				        a.AccountBucketUnits,
				        a.Code,
				        a.Description,
				        a.Active,
				        HasUnits = a.AccountBucketUnits.Any()
			        })
		        .ToList();

	        litRecordCount.Text = data.BuildRecordCount();
	        lvwRegistry.DataSource = data;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(new List<ParameterColumn>()));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

			txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<AccountBucket>(new AccountBucket(hidAccountBucketId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            chkActive.Checked = false;

            lstEditAccountBuckets.DataSource = new List<AccountBucketUnit>();
            lstEditAccountBuckets.DataBind();
        }

        private AccountBucket RetrieveViewAccountBucket()
        {
            var accountBucketId = hidAccountBucketId.Value.ToLong();

            var accountBucket = new AccountBucket(accountBucketId, accountBucketId != default(long))
            {
                Code = txtCode.Text,
                Description = txtDescription.Text,
                Active = chkActive.Checked
            };
            if (accountBucket.IsNew) accountBucket.TenantId = ActiveUser.TenantId;

            accountBucket.AccountBucketUnits = lstEditAccountBuckets
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidEditAccountBucketUnitId").ToCustomHiddenField().Value.ToLong();
                        return new AccountBucketUnit(id, id != default(long))
                            {
                                Name = i.FindControl("txtName").ToTextBox().Text,
                                Description = i.FindControl("txtDescription").ToTextBox().Text,
                                TenantId = accountBucket.TenantId,
                                AccountBucket = accountBucket,
                                Active = i.FindControl("chkActive").ToCheckBox().Checked
                            };
                    }
                )
                .ToList();

            return accountBucket;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new AccountBucketHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;

            if (IsPostBack) return;

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.AccountBucketImportTemplate });

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs eventArgs)
        {
            ClearFields();
            hidAccountBucketId.Value = string.Empty;
            GeneratePopup("Add Account Bucket");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Account Bucket");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidAccountBucketId").ToCustomHiddenField();

            var accountBucket = new AccountBucket(hidden.Value.ToLong(), false);

            hidAccountBucketId.Value = accountBucket.Id.ToString();
            txtCode.Text = accountBucket.Code;
            txtDescription.Text = accountBucket.Description;
            chkActive.Checked = accountBucket.Active;

            lstEditAccountBuckets.DataSource = accountBucket.AccountBucketUnits;
            lstEditAccountBuckets.DataBind();

            if (Lock != null)
                Lock(this, new ViewEventArgs<AccountBucket>(accountBucket));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var accountBucket = RetrieveViewAccountBucket();

            if (Save != null)
                Save(this, new ViewEventArgs<AccountBucket>(accountBucket));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidAccountBucketId").ToCustomHiddenField();

            var accountBucket = new AccountBucket(hidden.Value.ToLong(), true);
            accountBucket.LoadAccountBucketUnits();

            if (Delete != null)
                Delete(this, new ViewEventArgs<AccountBucket>(accountBucket));

            DoSearch();
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "ACCOUNT BUCKET IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                    .Select(item => new[]
					             	{
									    item.FindControl("litCode").ToLiteral().Text.ToString(),
										item.FindControl("litDescription").ToLiteral().Text.ToString(),
										item.FindControl("hidActive").ToCustomHiddenField().Value
					             	}.TabJoin())
                    .ToList();
            lines.Insert(0,
                         new[]
				             	{
				             		"Code", "Description", "Active"
				             	}.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "AccountBuckets.txt");
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 3;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

	        var accountBuckets = lines
		        .Select(s => new AccountBucket
			        {
				        Code = s[0],
				        Description = s[1],
				        Active = s[2].ToBoolean(),
				        TenantId = ActiveUser.TenantId,
				        AccountBucketUnits = new List<AccountBucketUnit>()
			        })
		        .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<AccountBucket>>(accountBuckets));

            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<AccountBucket>(RetrieveViewAccountBucket()));
            }
        }


        protected void OnAccountBucketUnitDeleteClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var rowItemIndex = button.FindControl("hidAccountBucketUnitItemIndex").ToCustomHiddenField().Value.ToLong();

            var vAccountBucketUnits = lstEditAccountBuckets.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidEditAccountBucketUnitId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("txtName").ToTextBox().Text,
                    Description = i.FindControl("txtDescription").ToTextBox().Text,
                    Active = i.FindControl("chkActive").ToCheckBox().Checked,
                    RowItemIndex = i.FindControl("hidAccountBucketUnitItemIndex").ToCustomHiddenField().Value.ToLong()
                })
                .Where(a => a.RowItemIndex != rowItemIndex)
                .ToList();

            lstEditAccountBuckets.DataSource = vAccountBucketUnits;
            lstEditAccountBuckets.DataBind();

        }

        protected void OnAddAccountBucketUnitClicked(object sender, EventArgs e)
        {

            var vAccountBucketUnits = lstEditAccountBuckets.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidEditAccountBucketUnitId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("txtName").ToTextBox().Text,
                    Description = i.FindControl("txtDescription").ToTextBox().Text,
                    Active = i.FindControl("chkActive").ToCheckBox().Checked
                })
                .ToList();

            vAccountBucketUnits.Add(new
            {
                Id = default(long),
                Name = string.Empty,
                Description = string.Empty,
                Active = true
            });

            lstEditAccountBuckets.DataSource = vAccountBucketUnits;
            lstEditAccountBuckets.DataBind();
        }

        protected void OnAccountBucketItemDataBound(object sender, ListViewItemEventArgs e)
        {

            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
			if (!e.Item.DataItem.HasGettableProperty("AccountBucketUnits")) return;

            var lstAccountBucketUnits = item.FindControl("lstAccountBucketUnits").ToListView();

	        lstAccountBucketUnits.DataSource = e.Item.DataItem.GetPropertyValue("AccountBucketUnits");
            lstAccountBucketUnits.DataBind();
        }
    }
}