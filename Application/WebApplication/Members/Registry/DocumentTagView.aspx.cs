﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class DocumentTagView : MemberPageBase, IDocumentTagView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Registry/DocumentTagView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.DocumentTag; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public event EventHandler<ViewEventArgs<DocumentTag>> Save;
        public event EventHandler<ViewEventArgs<DocumentTag>> Delete;
        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<List<DocumentTag>>> BatchImport;
        public event EventHandler<ViewEventArgs<DocumentTag>> Lock;
        public event EventHandler<ViewEventArgs<DocumentTag>> UnLock;

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<DocumentTag> documentTags)
        {
            litRecordCount.Text = documentTags.BuildRecordCount();
            lvwRegistry.DataSource = documentTags.OrderBy(d => d.Code);
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }


        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(new List<ParameterColumn>()));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<DocumentTag>(new DocumentTag(hidDocumentTagId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            chkNonEmployeeVisible.Checked = false;
        }

        private DocumentTag RetrieveViewDocumentTag()
        {
            var documentTagId = hidDocumentTagId.Value.ToLong();

            var documentTag = new DocumentTag(documentTagId, documentTagId != default(long))
            {

                Code = txtCode.Text,
                Description = txtDescription.Text,
                NonEmployeeVisible = chkNonEmployeeVisible.Checked
            };
            if (documentTag.IsNew) documentTag.TenantId = ActiveUser.TenantId;
            return documentTag;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new DocumentTagHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;

            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.DocumentTagsImportTemplate });
            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidDocumentTagId.Value = string.Empty;
            GeneratePopup("Add Document Tag");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Document Tag");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("documentTagId").ToCustomHiddenField();

            var documentTag = new DocumentTag(hidden.Value.ToLong(), false);

            hidDocumentTagId.Value = documentTag.Id.ToString();
            txtCode.Text = documentTag.Code;
            txtDescription.Text = documentTag.Description;
            chkNonEmployeeVisible.Checked = documentTag.NonEmployeeVisible;

            if (Lock != null)
                Lock(this, new ViewEventArgs<DocumentTag>(documentTag));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var documentTag = RetrieveViewDocumentTag();

            if (Save != null)
                Save(this, new ViewEventArgs<DocumentTag>(documentTag));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("documentTagId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<DocumentTag>(new DocumentTag(hidden.Value.ToLong(), true)));

            DoSearch();
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
            {
                CloseEditPopup();
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<DocumentTag>(RetrieveViewDocumentTag()));
            }
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }


            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 3;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var documentTags = lines
                .Select(s => new DocumentTag
                {
                    Code = s[0],
                    Description = s[1],
                    NonEmployeeVisible = s[2].ToBoolean(),
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<DocumentTag>>(documentTags));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "DOCUMENT TAG IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lvwRegistry.Items
                    .Select(item => new[]
					             	{
									    item.FindControl("litCode").ToLiteral().Text.ToString(),
										item.FindControl("litDescription").ToLiteral().Text.ToString(),
										item.FindControl("hidNonEmployeeVisible").ToCustomHiddenField().Value
					             	}.TabJoin())
                    .ToList();

            lines.Insert(0,
                         new[] { "Code", "Description", "Non-Employee Visible" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "DocumentTags.txt");
        }
    }
}
