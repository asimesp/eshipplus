﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Registry
{
    public partial class FailureCodeView : MemberPageBase, IFailureCodeView
    {
        private const string ExportFlag = "E";

        public static string PageAddress
        {
            get { return "~/Members/Registry/FailureCodeView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.FailureCode; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public Dictionary<int, string> FailureCodeCategories
        {
            set
            {
                var codes = value.ToList();

                codes.Insert(0, new KeyValuePair<int, string>(-1, WebApplicationConstants.NotApplicable));

                ddlFailureCodeCategories.DataSource = value ?? new Dictionary<int, string>();
                ddlFailureCodeCategories.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<FailureCode>> Save;
        public event EventHandler<ViewEventArgs<FailureCode>> Delete;
        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<FailureCode>> Lock;
        public event EventHandler<ViewEventArgs<FailureCode>> UnLock;
        public event EventHandler<ViewEventArgs<List<FailureCode>>> BatchImport;



        public void FaildedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<FailureCode> failureCodes)
        {
            litRecordCount.Text = failureCodes.BuildRecordCount();
            lvwRegistry.DataSource = failureCodes;
            lvwRegistry.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var lines = failureCodes
                    .Select(a => new[]
			                        {
										a.Code.ToString(),
										a.Description.ToString(),
			                            ((int) a.Category).ToString(),
			                            a.ExcludeOnScoreCard.ToString(),
										a.Active.ToString()
			                        }.TabJoin())
                    .ToList();
                lines.Insert(0, new[] { "Code", "Description", "Category", "Stats. Exclude", "Active" }.TabJoin());

                hidFlag.Value = string.Empty;

                Response.Export(lines.ToArray().NewLineJoin(), "FailureCodes.txt");
            }
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems{Columns = GetCurrentRunParameters(true)};
        }

        private void DoSearch(List<ParameterColumn> criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(criteria));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtCode.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<FailureCode>(new FailureCode(hidFailureCodeId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            chkActive.Checked = false;
            chkExcludeOnScoreCard.Checked = false;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new FailureCodeHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;


            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : RegistrySearchFields.FailureCodes.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.FailureCodeImportTemplate });

            if (Loading != null)
                Loading(this, new EventArgs());
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidFailureCodeId.Value = string.Empty;
            GeneratePopup("Add Failure Code");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Failure Code");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("failureCodeId").ToCustomHiddenField();

            var failureCode = new FailureCode(hidden.Value.ToLong(), false);

            hidFailureCodeId.Value = failureCode.Id.ToString();
            txtCode.Text = failureCode.Code;
            txtDescription.Text = failureCode.Description;
            chkActive.Checked = failureCode.Active;
            chkExcludeOnScoreCard.Checked = failureCode.ExcludeOnScoreCard;
            ddlFailureCodeCategories.SelectedValue = ((int)failureCode.Category).ToString();

            if (Lock != null)
                Lock(this, new ViewEventArgs<FailureCode>(failureCode));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var failureCodeId = hidFailureCodeId.Value.ToLong();

            var failureCode = new FailureCode(failureCodeId, failureCodeId != default(long))
            {
                Id = failureCodeId,
                Code = txtCode.Text,
                Description = txtDescription.Text,
                Category = ddlFailureCodeCategories.SelectedValue.ToEnum<FailureCodeCategory>(),
                Active = chkActive.Checked,
                ExcludeOnScoreCard = chkExcludeOnScoreCard.Checked,
            };

            if (failureCode.IsNew) failureCode.TenantId = ActiveUser.TenantId;

            if (Save != null)
                Save(this, new ViewEventArgs<FailureCode>(failureCode));

            DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("failureCodeId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<FailureCode>(new FailureCode(hidden.Value.ToLong(), true)));

            DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearch(GetCurrentRunParameters(false));
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "FAILURE CODE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearch(GetCurrentRunParameters(false));
        }


        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 5;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<FailureCodeCategory>(2));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var failureCodes = lines
                .Select(s => new FailureCode
                {
                    Code = s[0],
                    Description = s[1],
                    Category = s[2].ToEnum<FailureCodeCategory>(),
                    ExcludeOnScoreCard = s[3].ToBoolean(),
                    Active = s[4].ToBoolean(),
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<FailureCode>>(failureCodes));
            fileUploader.Visible = false;
            DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = RegistrySearchFields.FailureCodes.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(RegistrySearchFields.FailureCodes);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
    }
}
