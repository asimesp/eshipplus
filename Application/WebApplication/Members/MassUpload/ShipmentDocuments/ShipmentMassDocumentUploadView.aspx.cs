﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using AjaxControlToolkit;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.MassUpload;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.MassUpload;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.MassUpload.ShipmentDocuments
{
    public partial class ShipmentMassDocumentUploadView : MemberPageBase, IShipmentMassUploadView
    {
        private const string ErrorMessagesKey = "ShipmentMassDocumentUpload2ErrorMessages";

        protected const string DocumentTagIdKey = "ShipmentMassDocumentUploadTagId";
        protected const string IsInternalKey = "ShipmentMassDocumentUploadIsInternal";

        public static string PageAddress
        {
            get { return "~/Members/MassUpload/ShipmentDocuments/ShipmentMassDocumentUploadView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.ShipmentMassDocumentUpload; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.UtilitiesBlue; }
        }

        protected string DeleteImageElement
        {
            get
            {
                return string.Format("<img style='border-width:0px;' alt='Delete' src='{0}{1}' title='Delete'>",
                                     WebApplicationSettings.SiteRoot, IconLinks.Delete.Replace("~", string.Empty));
            }
        }

        private Dictionary<string, List<string>> ErrorMessages
        {
            get
            {
                var msgs = Session[ErrorMessagesKey] as Dictionary<string, List<string>>;
                if (msgs == null)
                {
                    msgs = new Dictionary<string, List<string>>();
                    Session[ErrorMessagesKey] = msgs;
                }
                return msgs;
            }
        }

        public event EventHandler<ViewEventArgs<ShipmentDocument>> Save;

        public void ProcessFailedShimpentDocumentSave(ShipmentDocument document, IEnumerable<ValidationMessage> messages)
        {
            Server.RemoveFiles(new[] { document.LocationPath });
            AddErrorMessages(document.Name, messages.Select(m => m.Message));
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }


        private void AddErrorMessages(string documentName, IEnumerable<string> message)
        {
            if (!ErrorMessages.ContainsKey(documentName)) ErrorMessages.Add(documentName, new List<string>());
            ErrorMessages[documentName].AddRange(message);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ShipmentMassUploadHandler(this).Initialize();

            fupMassUpload.Visible = ActiveUser.HasAccessToModify(ViewCode.Shipment);

            if (IsPostBack || fupMassUpload.IsInFileUploadPostBack) return;

            ddlIsInternal.DataSource = new List<ViewListItem>
			    {
				    new ViewListItem(WebApplicationConstants.Yes, true.ToString()),
				    new ViewListItem(WebApplicationConstants.No, false.ToString())
			    };
            ddlIsInternal.DataBind();

            // setup first selected options!
            SetOptions(ddlDocumentTags.SelectedValue, ddlIsInternal.SelectedValue);
        }


        protected void OnFileUploadComplete(object sender, AjaxFileUploadEventArgs e)
        {
            var fileContents = e.GetContents();
            var contentLength = fileContents.Length;

            // check content lengths
            if (contentLength == 0)
            {
                AddErrorMessages(e.FileName, new[] { "No file content!" });
                e.DeleteTemporaryData();
                return;
            }
            if (contentLength > WebApplicationSettings.SystemFileUploadMaxSize)
            {
                var msg = string.Format("Exceeds file size limit of {0} bytes [{1:n2} kb or {2:n2} mb or {3:n2} gb]",
                                        WebApplicationSettings.SystemFileUploadMaxSize,
                                        (float)WebApplicationSettings.SystemFileUploadMaxSize /
                                        WebApplicationConstants.Kb,
                                        (float)WebApplicationSettings.SystemFileUploadMaxSize /
                                        WebApplicationConstants.Mb,
                                        (float)WebApplicationSettings.SystemFileUploadMaxSize /
                                        WebApplicationConstants.Gb);
                AddErrorMessages(e.FileName, new[] { msg });
                return;
            }

            // find corresponding shipment record
            var index = e.FileName.LastIndexOf(".", StringComparison.Ordinal);
            var shipmentNumber = index != -1 ? e.FileName.Substring(0, index) : e.FileName;
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(shipmentNumber, ActiveUser.TenantId);

            if (shipment == null)
            {
                AddErrorMessages(e.FileName, new[] { "Record not found!" });
                e.DeleteTemporaryData();
                return;
            }

            // create and save shipment document
            var shipmentFolder = WebApplicationSettings.ShipmentFolder(shipment.TenantId, shipment.Id);
            var filePath = Path.Combine(shipmentFolder, e.FileName);
            var fi = new FileInfo(Server.MakeUniqueFilename(filePath)); // ensure unique filename
            var tag = new DocumentTag(Session[DocumentTagIdKey].ToLong());

            var document = new ShipmentDocument
            {
                TenantId = shipment.TenantId,
                Shipment = shipment,
                Description = string.Format("{0} {1}", shipment.ShipmentNumber, tag.Code),
                DocumentTag = tag,
                IsInternal = Session[IsInternalKey].ToBoolean(),
                LocationPath = Path.Combine(shipmentFolder, fi.Name),
                Name = fi.Name,
            };

            fileContents.WriteToFile(Server.MapPath(shipmentFolder), fi.Name, string.Empty);
            e.DeleteTemporaryData();

            if (Save != null)
                Save(this, new ViewEventArgs<ShipmentDocument>(document));
            else
                ProcessFailedShimpentDocumentSave(document, new[] { ValidationMessage.Error("Incorrect page configuration. Missing save event handler!") });


        }

        [ScriptMethod]
        [WebMethod]
        public static object FileUploadCompletedProcesses()
        {
            var msgs = HttpContext.Current.Session[ErrorMessagesKey] as Dictionary<string, List<string>>;

            // clear keys
            HttpContext.Current.Session[ErrorMessagesKey] = null;
            HttpContext.Current.Session[DocumentTagIdKey] = null;
            HttpContext.Current.Session[IsInternalKey] = null;

            return msgs == null
                       ? null
                       : msgs.Keys
                             .Select(k => new
                             {
                                 Filename = k,
                                 Errors = msgs[k].Select(e => e).ToArray().CommaJoin()
                             })
                             .ToList();
        }

        [ScriptMethod]
        [WebMethod]
        public static object SetOptions(string tagId, string isInternal)
        {
            HttpContext.Current.Session[DocumentTagIdKey] = tagId;
            HttpContext.Current.Session[IsInternalKey] = isInternal;
            return true;
        }
    }
}