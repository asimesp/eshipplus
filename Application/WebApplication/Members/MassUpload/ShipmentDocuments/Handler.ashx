﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.WebApplication;
using LogisticsPlus.Eship.WebApplication.Utilities;
using ObjToSql.Core;

public class Handler : EntityBase, IHttpHandler
{
	public void ProcessRequest(HttpContext context)
	{
		context.Response.ContentType = "text/plain";
		var results = new System.Collections.Generic.List<ViewDataUploadFilesResult>();
		var javaScriptSerializer = new JavaScriptSerializer();
		var search = new ShipmentSearch();
		var user = new User(context.Request.Form["hidContextKey"].ToLong());
		var tag = new DocumentTag(context.Request.Form["hidDocumentTag"].ToLong());
		var isInternal = context.Request.Form["hidIsInternal"].ToBoolean();

		if (!user.KeyLoaded)
		{
			// return results
			var jsonObj = javaScriptSerializer
				.Serialize(new {files = new[] {new ViewDataUploadFilesResult {ErrorMessage = "Unable to find active user"}}});
			context.Response.Write(jsonObj);
			return;
		}

		if (!tag.KeyLoaded)
		{
			// return results
			var jsonObj = javaScriptSerializer
				.Serialize(new {files = new[] {new ViewDataUploadFilesResult {ErrorMessage = "Unable to load document tag"}}});
			context.Response.Write(jsonObj);
			return;
		}

		foreach (string file in context.Request.Files)
		{
			var postedFile = context.Request.Files[file];
			string fileName;
			if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
			{
				string[] files = postedFile.FileName.Split(new[] { '\\' });
				fileName = files[files.Length - 1];
			}
			else
			{
				fileName = postedFile.FileName;
			}

			var result = new ViewDataUploadFilesResult { Name = fileName }; // result

			if (postedFile.ContentLength == 0)
			{
				result.ErrorMessage = "Posted File [" + fileName + "] Content Length = 0";
				results.Add(result);
				continue;
			}
			result.Length = postedFile.ContentLength;
			result.Type = postedFile.ContentType;

			try
			{
				// to get shipment from filename
				var index = fileName.LastIndexOf(".", StringComparison.Ordinal);
				var shipmentNumber = index != -1 ? fileName.Substring(0, index) : fileName;
				var shipment = search.FetchShipmentByShipmentNumber(shipmentNumber, user.TenantId);

				if (shipment == null)
				{
					result.ErrorMessage = "Unable to find corresponding shipment [Number: " + shipmentNumber + "]";
					results.Add(result);
					continue;
				}

				// lock shipment
				var @lock = shipment.ObtainLock(user, shipment.Id);
				if (!@lock.IsUserLock(user))
				{
					result.ErrorMessage = ProcessorVars.UnableToObtainLockErrMsg + " [Number: " + shipmentNumber + "]";
					results.Add(result);
					continue;
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					// link up
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					var shipmentFolder = WebApplicationSettings.ShipmentFolder(shipment.TenantId, shipment.Id);
					var physicalPath = context.Server.MapPath(shipmentFolder);
					if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);


                    var fi = new FileInfo(Path.Combine(physicalPath, fileName));
                    var cnt = 1;
                    var newPath = fi.FullName;
                    while (File.Exists(newPath))
                        newPath = string.Format("{0}({1}){2}", fi.FullName.Replace(fi.Extension, string.Empty), cnt++, fi.Extension);
					
                    fi = new FileInfo(newPath);
                    
					var document = new ShipmentDocument
					               	{
					               		Connection = Connection,
					               		Transaction = Transaction,
					               		TenantId = shipment.TenantId,
					               		Shipment = shipment,
					               		Description = string.Format("{0} {1}", shipment.ShipmentNumber, tag.Code),
					               		DocumentTag = tag,
					               		IsInternal = isInternal,
					               		LocationPath = Path.Combine(shipmentFolder, fi.Name),
					               		Name = fi.Name,
					               	};

					document.Save();

					// save file
					postedFile.SaveAs(context.Server.MapPath(document.LocationPath));

					// audit log
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Document Ref#:{3}",
                                                    AuditLogConstants.AddedNew, document.EntityName(), shipment.ShipmentNumber,
													document.Id),
						TenantId = user.TenantId,
						User = user,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					// delete lock
					@lock.Delete();

					// commit transaction
					CommitTransaction();

					// update result
					result.ThumbnailUrl = string.Format("../../..{0}", document.LocationPath.Substring(1));
					result.Url = result.ThumbnailUrl;
					result.AltText = document.Name;
				}
				catch(Exception ex1)
				{
					RollBackTransaction();
					result.ErrorMessage = "Error processing addition of document to shipment record [Number: " + shipmentNumber + "]";
					results.Add(result);
					ErrorLogger.LogError(ex1, context);
					continue;
				}

				// add valid result to results
				results.Add(result);
			}
			catch(Exception ex2)
			{
				result.ErrorMessage = "Error processing record for file [" + fileName + "]";
				results.Add(result);
				ErrorLogger.LogError(ex2, context);
			}
		}

		// return results
		context.Response.Write(javaScriptSerializer.Serialize(new { files = results.ToArray() }));
	}

	public bool IsReusable
	{
		get
		{
			return false;
		}
	}
}

public class ViewDataUploadFilesResult
{
	public string ThumbnailUrl { get; set; }
	public string Name { get; set; }
	public int Length { get; set; }
	public string Type { get; set; }
	public string Url { get; set; }
	public string AltText { get; set; }
	public string ErrorMessage { get; set; }
}