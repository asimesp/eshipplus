﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public partial class DocumentAccessView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
	        var shipmentNumber = Request.QueryString["shipmentnumber"];
	        var documentAccessKey = Request.QueryString["documentkey"];
	        var accessKey = Request.QueryString["accessKey"];
	        var tenantCode = Request.QueryString["tenantcode"];

			var tenant = new TenantSearch().FetchTenantByCode(tenantCode);
			if (tenant == null) return;

			var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(shipmentNumber, tenant.Id);
			if (shipment?.Customer.Communication == null) return;
			if (shipment.Customer.Communication.DocumentLinkAccessKey != accessKey.ToGuid()) return;

			var doc = shipment.Documents.FirstOrDefault(d => d.Name.ToLower() == documentAccessKey.GetString().ToLower());
			if (doc == null) return;
			if (doc.IsInternal) return;

			Response.Export(Server.ReadFromFile(doc.LocationPath), documentAccessKey);
        }
    }
}