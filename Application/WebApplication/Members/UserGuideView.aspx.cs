﻿using System;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    public partial class UserGuideView : MemberPageBase
    {
        public static string PageAddress { get { return "~/Members/UserGuideView.aspx"; } }

        public override ViewCode PageCode
        {
            get { return ViewCode.UserGuide; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber])) return;
            
            var viewCode = Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToEnum<ViewCode>();
            if (ActiveUser.HasAccessTo(viewCode) || viewCode.ExcludePermission())
                embedUserGuide.Src = string.Format("{0}{1}.pdf", WebApplicationSettings.UserGuidesFolder,viewCode);
        }
    }
}