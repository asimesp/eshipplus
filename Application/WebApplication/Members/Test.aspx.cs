﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public partial class Test : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

			var items = new List<ListItem>();

			for (var i = 0; i < 5; i++)
				items.Add(new ListItem(string.Format("Test {0}", i + 1), i.ToString()));


			ddl2.DataSource = items;
			ddl2.DataBind();

			if (!string.IsNullOrEmpty(Request.QueryString["t"]))
			{
				ddl.SelectedValue = Request.QueryString["t"];
				ddl2.SelectedValue = Request.QueryString["t"];
			}
		}

		protected void OnDdl2SelectedIndexChanged(object sender, EventArgs e)
		{
			var x = ddl.SelectedValue;
		}
	}
}