﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="DeveloperAccessRequestView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.DeveloperAccessRequestView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Developer Access Request</h3>
        </div>
        <hr class="dark mb5" />
        <table>
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pnlNoAccess" Visible="false" CssClass="w100p">
                        <table>
                            <tr class="header">
                                <td>DEVELOPER ACCESS REQUEST
                                </td>
                            </tr>
                            <tr>
                                <td>Please use the button below to request access to our developer tools and resources.
                                </td>
                            </tr>
                            <tr>
                                <td class="text-left pl0 pr10 pt10 pb10">
                                    <asp:Button runat="server" ID="btnRequestAccess" Text="Request Access" OnClick="OnRequestAccessClick" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pnlPendingAccess" Visible="False" CssClass="w100p">
                        <table>
                            <tr>
                                <td>DEVELOPER ACCESS PENDING
                                </td>
                            </tr>
                            <tr>
                                <td>A request is pending for your account, please use the button below to communicate
								changes to your request.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button runat="server" ID="btnUpdateRequest" Text="Update Request" OnClick="OnUpdateRequestClick" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <label class="upper blue">Sections:</label>
                            </td>
                            <td>
                                <a class="link_nounderline blue" href="#PartnerAuthentication">Partner Authentication</a>
                            </td>
                            <td>|
                            </td>
                            <td>
                                <a class="link_nounderline blue" href="#WebServices">Web Services</a>
                            </td>
                            <td>|
                            </td>
                            <td>
                                <a class="link_nounderline blue" href="#DocumentDelivery">Document Delivery</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr class="fat" />
        <asp:Panel runat="server" ID="pnlGrantedAccess" Visible="True">
            <div class="row">
                <div class="rowgroup">
                    <div class="fieldgroup">
                        <h3>Partner Authentication <a name="PartnerAuthentication">&nbsp;</a></h3>
                        You can login to eShipPlus from your website in two ways:
								<ul>
                                    <li><b>From HTML form</b>
                                        <p>
                                            Use this html code in the body of your page.
                                        </p>
                                        <div>
                                            &lt;form action="<asp:Literal runat="server" ID="litPartnerAuthenticationSiteRoot" />/Default.aspx"
											method="post"&gt;
											<div class="pl10">
                                                AccessCode: &lt;input name="AccessCode" type="text" id="accessCode" /&gt;
												<br />
                                                Username:&lt;input name="Username" type="text" id="username" /&gt;
												<br />
                                                UserPassword: &lt;input name="UserPassword" type="password" id="userPassword" /&gt;
												<br />
                                                &lt;input name="ReturnUrl" type="hidden" id="returnUrl" value="http://www.yourwebsitehere.com"
												/&gt;
												<br />
                                                &lt;input name="HomeUrl" type="hidden" id="homeUrl" value="http://www.yourwebsitehere.com"/&gt;
												<br />
                                                &lt;input name="AuthenticationErrorReturnUrl" type="hidden" id="authenticationErrorReturnUrl"
												value="http://www.yourwebsitehere.com" /&gt;
												<br />
                                                &lt;input type="Submit" name="submitButton" value="Submit" id="submitButton"/&gt;
                                            </div>
                                            &lt;/form&gt;
                                        </div>
                                        <br />
                                        <br />
                                    </li>
                                    <li><b>Via Request URL</b>
                                        <p>
                                            <asp:Literal runat="server" ID="litRequestURLSiteRoot" />/Default.aspx?AccessCode=<b>accessCode</b>&amp;UserName=<b>username</b>
                                            &amp;UserPassword=<b>password</b>&amp;ReturnUrl=<b>returnUrl</b>
                                            &amp;HomeUrl=<b>homeUrl</b>&amp;AuthenticationErrorReturnUrl=<b>authenticationErrorReturnUrl</b>
                                        </p>
                                        Replace the following accordingly:
										<ul>
                                            <li><b>accessCode</b>: login access code.</li>
                                            <li><b>username</b>: login email address.</li>
                                            <li><b>password</b>: login password.</li>
                                            <li><b>returnUrl</b>: Url to redirect users on sign out. If not provided/set, user is
												redirected to eShipPlus homepage. <b>Note: Url must contain http:// if provided.</b></li>
                                            <li><b>homeUrl</b>: Url to redirect users to when home link is clicked in eShipPlus.
												If not provided/set, user is redirected to eShipPlus homepage. <b>Note: Url must contain
													http:// if provided.</b></li>
                                            <li><b>authenticationErrorReturnUrl</b>: Url to redirect users to when attempted attempt
												to login fails. If not provided/set, default error message on eShipPlus.com is displayed.
												<b>Note: Url must contain http:// if provided.</b></li>
                                        </ul>
                                        <br />
                                        <br />
                                    </li>
                                    <li><b>To Retrieve Password - Via Request URL</b>
                                        <p>
                                            <asp:Literal runat="server" ID="litRetrievePasswordSiteRoot" />/Default.aspx?Key=pwd&amp;
                                            AccessCode=<b>accessCode</b>&amp;Username=<b>username</b>
                                        </p>
                                        <b>Note: Url must contain parameter 'Key=pwd' to retrieve the password</b>
                                        <br />
                                        <br />
                                        Replace the following accordingly:
										<ul>
                                            <li><b>accessCode</b>: login access code.</li>
                                            <li><b>username</b>: login email address.</li>
                                        </ul>
                                    </li>
                                </ul>
                    </div>
                </div>
            </div>
            <hr class="dark mb20" />
            <div class="row">
                <div class="fieldgroup">
                    <h3>Web Services <a name="WebServices">&nbsp;</a>
                    </h3>
                    <p>
                        For more advanced integration, including shipment rating and scheduling, utilize
									eShipPlusWS (eShipPlus Web Services).
                    </p>
                    <b>Steps to utilizing eShipPlusWS:</b> [<asp:LinkButton runat="server" ID="lbtnRequestDeveloperSupport"
                        Text="Email Support" OnClick="OnRequestDeveloperSupportClicked" class="link_nounderline blue" />]
								<ol>
                                    <li><b>Download Resources and Sample Code.</b>
                                        <p>
                                            Developer Guide &amp; WSDL file: <a class="link_nounderline blue" href="WebServicesItems/eShipPlusWSDevGuideAndWSDL.zip">Dev Guide &amp; wsdl</a>
                                        </p>
                                        <p>
                                            Sample Code: <a class="link_nounderline blue" href="WebServicesItems/eShipPlusWS-cSharp.zip">C#</a>
                                        </p>
                                    </li>
                                    <asp:Panel ID="TestAccessDetails" runat="server" Visible="false">
                                        <li><b>Test Info:</b>
                                            <table class="pt10 pb10 pr10 pl10">
                                                <tr>
                                                    <td>
                                                        <asp:Literal ID="litTestInfo" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </li>
                                    </asp:Panel>
                                    <li><b>Develop and Test Your Application.</b><br />
                                        <br />
                                    </li>
                                    <li><b>Get Certified.</b>
                                        <p>
                                            Certification involves submitting sample labels to our development team. We ensure
											that your labels are printed at the quality necessary for shipping.
                                        </p>
                                    </li>
                                    <li><b>Request Production Access.</b>
                                        <p>
                                            <asp:LinkButton runat="server" ID="lbtnRequestProductionAccess" OnClick="OnRequestProductionAccessClicked"
                                                Text="Send Request" class="link_nounderline blue" />
                                        </p>
                                        <asp:Panel ID="prodAccessRequest" runat="server" Visible="False">
                                            <table class="pt10 pb10 pr10 pl10">
                                                <tr>
                                                    <td>Service Url:
                                                    </td>
                                                    <td>http://www.eshipplus.com/services/eShipPlusWSv4.asmx
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pt10 pb10 pr10 pl10">Key(s):
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Literal runat="server" ID="litGuidInfo" />
                                        </asp:Panel>
                                    </li>
                                </ol>
                </div>
            </div>
            <hr class="dark mb20" />
            <div class="row">
                <div class="fieldgroup">
                    <h3>Document Delivery <a name="DocumentDelivery">&nbsp;</a>
                    </h3>
                    <p>
                        You can have system generated documents and uploaded tagged documents delivered via FTP.
                    </p>
                    <b>Deliverable Documents:</b> [<asp:LinkButton runat="server" ID="lnkRequestDeveloperAccessSupportDocDelivery"
                        Text="Email Support" OnClick="OnRequestDeveloperSupportClicked" class="link_nounderline blue" />]
								<ul>
                                    <li><b>System Generated Documents</b>
                                        <ul class="mt10 mr0">
                                            <li>Bill of Lading</li>
                                            <li>Shipment Statement</li>
                                            <li>Invoice</li>
                                        </ul>
                                    </li>
                                    <li><b>Tagged Documents</b>
                                        <p>
                                            Tagged documents include items like the POD (proof of delivery document). These can also include special documents
											requested as part of your SOP (standard operating procedure).
                                        </p>
                                    </li>
                                </ul>
                    <b>To take advantage of the document delivery functionality:</b>
                    <ul>
                        <li>Identify which document(s) you would like delivered via FTP</li>
                        <li>Provide the following FTP credential information:
										<ol class="mt10 mr0">
                                            <li>FTP server Uri (url)</li>
                                            <li>FTP server username</li>
                                            <li>FTP server password</li>
                                            <li>Indicate if FTP server utilizes SSL</li>
                                            <li>FTP server folder (if applicable)</li>
                                        </ol>
                        </li>
                        <li>NOTE: documents, when delivered are named in the following formats
									
										<table class="mt10 mr0 mb10 ml10 w500">
                                            <tr>
                                                <td>Bill of Lading</td>
                                                <td><i>shipment#_Bol.pdf</i></td>
                                            </tr>
                                            <tr>
                                                <td>Shipment Statement</td>
                                                <td><i>shipment#_Statement.pdf</i></td>
                                            </tr>
                                            <tr>
                                                <td>Invoice</td>
                                                <td><i>invoice#_Invoice.pdf</i></td>
                                            </tr>
                                            <tr>
                                                <td>Tagged Shipment Document</td>
                                                <td><i>shipment#_tag/code_document name.extension</i></td>
                                            </tr>
                                        </table>
                        </li>
                        <li><b>Document Access Link</b><br/>
                            You can embed a link to your shipment document by using the following link configuration:<br/>
                            <br/>
                            <asp:Panel ID="prodAccessRequest2" runat="server" Visible="False">
                                <table class="pt10 pb10 pr10 pl10">
                                    <tr>
                                        <td>Service Url:
                                        </td>
                                        <td><asp:Literal runat="server" ID="litDocumentAccessLinkSiteRoot" />/members/documentaccessview.aspx?shipmentnumber=12345&documentkey=filenameOfdocument&accesskey=AccessKey&TenantCode=TenantCode
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="pt10 pb10 pr10 pl10">Key(s):
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <asp:Literal runat="server" ID="litDocumentAccessKeys" />
                            </asp:Panel>                            
                        </li>
                    </ul>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlRequestAccess" runat="server" Visible="False">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>REQUEST DEVELOPER ACCESS</h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseDeveloperAccessRequestClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td class="text-right">
                                <label class="upper">User:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox runat="server" ID="txtUserName" BackColor="lightgray"
                                    Enabled="false" CssClass="w200" />
                            </td>
                            <td style="width: 30%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Customer:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox runat="server" ID="txtCustomer" Width="200px" BackColor="lightgray"
                                    Enabled="false" />
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Contact Name:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtContactName" MaxLength="50" runat="server" Width="200px" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvName" ControlToValidate="txtContactName"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Contact Email:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtContactEmail" MaxLength="100" runat="server" Width="200px" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtContactEmail"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveDeveloperAccessRequestClick"
                                    runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseDeveloperAccessRequestClicked"
                                    runat="server" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlEmail" Visible="False">
            <div class="popupControl popupControlOverW750">
                <div class="popheader">
                    <h4>
                        <asp:Literal runat="server" ID="litEmailTitle" /></h4>
                    <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseAccessRequestEmailClicked" runat="server" />
                    <eShip:CustomHiddenField runat="server" ID="hidEmailFlag" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <asp:Panel runat="server" ID="pnlSupport" Visible="True">
                            <tr>
                                <td style="width: 20%;" class="text-right">
                                    <label class="upper">Category:</label>
                                </td>
                                <td style="width: 70%;" class="text-left">
                                    <asp:DropDownList runat="server" ID="ddlCategory" CssClass="w200" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Message:</label>
                            </td>
                            <td class="text-left">
                                <ajax:Editor runat="server" ID="ediAccountRequestUpdate" CssClass="h150 w500" />
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td colspan="2" class="text-right">
                                <asp:Button runat="server" ID="btnSend" Text="SEND" OnClick="OnSendAccessRequestEmailClick" />
                                <asp:Button runat="server" ID="Button1" Text="CANCEL" OnClick="OnCloseAccessRequestEmailClicked"
                                    CausesValidation="False" />
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" />
    <eShip:CustomHiddenField runat="server" ID="hidSaveHtmlWrapper" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
</asp:Content>
