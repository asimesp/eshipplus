﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    public partial class ApplicationLogListingView : MemberPageBase
    {
        public static string PageAddress
        {
            get { return "~/Members/ApplicationLogListingView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.ApplicationLog; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var folder = WebApplicationSettings.ApplicationLogsFolder;
                var path = Server.MapPath(folder);

                var logs = Directory.GetFiles(path)
                    .Select(f => new FileInfo(f))
                    .Select(i => new { FileName = i.Name.Replace(i.Extension, string.Empty), DateCreated = i.CreationTime })
                    .ToList();
                litRecordCount.Text = logs.BuildRecordCount();
                lvwApplicationLog.DataSource = logs.OrderByDescending(l => l.DateCreated);
                lvwApplicationLog.DataBind();
            }
        }

        protected void OnPurgeClicked(object sender, EventArgs e)
        {
            var folder = WebApplicationSettings.ApplicationLogsFolder;
            var path = Server.MapPath(folder);
            var cutOffDate = txtCutOffDate.Text.ToDateTime().TimeToMaximum();

            var fileInfos = Directory.GetFiles(path)
                .Select(f => new FileInfo(f))
                .ToList();

            for (int i = fileInfos.Count - 1; i > -1; i--)
                if (fileInfos[i].CreationTime < cutOffDate)
                {
                    fileInfos[i].Delete();
                    fileInfos.RemoveAt(i);
                }

            lvwApplicationLog.DataSource = fileInfos
                .Select(i => new { FileName = i.Name.Replace(i.Extension, string.Empty), DateCreated = i.CreationTime })
                .OrderByDescending(l => l.DateCreated)
                .ToList();
            lvwApplicationLog.DataBind();
        }

        protected void OnDisplayApplicationLogClicked(object sender, CommandEventArgs e)
        {
            pnlApplicationLogDisplay.Visible = true;
            pnlApplicationLogDisplayDimsScreen.Visible = true;
            litApplicationLogTitle.Text = e.CommandArgument.GetString();

            var reader = new StreamReader(string.Format("{0}{1}.txt", Server.MapPath(WebApplicationSettings.ApplicationLogsFolder), e.CommandArgument));
            litApplicationLogContent.Text = reader.ReadToEnd().ReplaceNewLineWithHtmlBreak();

            //reset maintained scroll position
            hidScrollPosition.Value = 0.ToString();
        }

        protected void OnCloseApplicationLogDisplayClicked(object sender, ImageClickEventArgs e)
        {
            pnlApplicationLogDisplay.Visible = false;
            pnlApplicationLogDisplayDimsScreen.Visible = false;
        }

        protected void OnRefreshClicked(object sender, EventArgs e)
        {
            var reader = new StreamReader(string.Format("{0}{1}.txt", Server.MapPath(WebApplicationSettings.ApplicationLogsFolder), litApplicationLogTitle.Text));
            litApplicationLogContent.Text = reader.ReadToEnd().ReplaceNewLineWithHtmlBreak();
        }
    }
}