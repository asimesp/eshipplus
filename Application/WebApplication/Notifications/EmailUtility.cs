﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Notifications
{
    public static class EmailUtility
    {
        public static bool SendPasswordRecoveryEmail(this HttpRequest request, string email, string password)
        {
            string body;
            using (var reader = new StreamReader(request.MapPath(SystemTemplates.GeneralEmailTemplate)))
                body = reader.ReadToEnd();

            body = body.Replace(EmailTemplateFields.Title, "Password Recovery");
            body = body.Replace(EmailTemplateFields.SiteRoot, request.ResolveSiteRootWithHttp());
            body = body.Replace(EmailTemplateFields.Body, string.Format("Your password is: {0}", password));
            body = body.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);

            return SendEmail(new[] { email }, body, "eShipPlus Password Recovery", default(long));
        }

        public static bool SendErrorLogEmail(this string body)
        {
            var emails = WebApplicationSettings.ErrorLogNotificationEmails
                .Split(WebApplicationUtilities.ImportSeperators(), StringSplitOptions.RemoveEmptyEntries);
            return emails.SendEmail(body, WebApplicationSettings.Brand, default(long));
        }

        public static bool SendEmail(this IEnumerable<string> toAddresses, string body, string subject, long tenantId)
        {
            return toAddresses.SendEmail(new string[0], new string[0], body, subject, new Attachment[0], tenantId);
        }

        public static bool SendEmail(this IEnumerable<string> tos, IEnumerable<string> ccs, IEnumerable<string> bbcs,
                                     string body, string subject, long tenantId)
        {
            return SendEmail(tos, ccs, bbcs, body, subject, new Attachment[0], tenantId);
        }

        public static bool SendEmail(this IEnumerable<string> tos, IEnumerable<string> ccs, IEnumerable<string> bbcs,
                                     string body, string subject, IEnumerable<Attachment> attachments, long tenantId)
        {
            try
            {
                var msg = new MailMessage();

                var distinctTos = tos.Distinct();
                var distinctCcs = ccs.Distinct();
                var distinctBbcs = bbcs.Distinct();

                foreach (var addr in distinctTos)
                    try
                    {
                        var email = addr.Trim();

                        if (!email.EmailIsValid())
                        {
                            var message = string.Format("Incorrect email format [{0}]. Subject: {1}",
                                email, subject);
                            ErrorLogger.LogError(new Exception(message), WebApplicationSettings.ErrorsFolder);
                            continue;
                        }

                        msg.To.Add(email);
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.LogError(ex, ProcessorVars.ErrorFolder);
                    }

                foreach (var addr in distinctCcs)
                    try
                    {
                        var email = addr.Trim();

                        if (!email.EmailIsValid())
                        {
                            var message = string.Format("Incorrect email format [{0}]. Subject: {1}",
                                email, subject);
                            ErrorLogger.LogError(new Exception(message), WebApplicationSettings.ErrorsFolder);
                            continue;
                        }

                        msg.CC.Add(email);
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.LogError(ex, ProcessorVars.ErrorFolder);
                    }

                foreach (var addr in distinctBbcs)
                    try
                    {
                        var email = addr.Trim();

                        if (!email.EmailIsValid())
                        {
                            var message = string.Format("Incorrect email format [{0}]. Subject: {1}",
                                email, subject);
                            ErrorLogger.LogError(new Exception(message), WebApplicationSettings.ErrorsFolder);
                            continue;
                        }

                        msg.Bcc.Add(email);
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.LogError(ex, ProcessorVars.ErrorFolder);
                    }

                msg.From = new MailAddress(WebApplicationSettings.DoNotReplyEmail);
                msg.Subject = subject;
                msg.Body = body;
                msg.IsBodyHtml = true;

                foreach (var attachment in attachments)
                    msg.Attachments.Add(attachment);

                try
                {
                    var emailLog = new EmailLog
                    {
                        Tos = string.Join("; ", distinctTos),
                        Ccs = string.Join("; ", distinctCcs),
                        Bbcs = string.Join("; ", distinctBbcs),
                        DateCreated = DateTime.Now,
                        Subject = subject,
                        Body = body,
                        EmailLogAttachments = new List<EmailLogAttachment>(),
                        TenantId = tenantId,
                        From = msg.From.Address,
                        Sent = true
                    };

                    foreach (var attachment in attachments)
                    {
                        byte[] bytes;

                        using (var streamReader = new MemoryStream())
                        {
                            attachment.ContentStream.CopyTo(streamReader);
                            attachment.ContentStream.Position = 0;
                            bytes = streamReader.ToArray();
                        }

                        emailLog.EmailLogAttachments.Add(new EmailLogAttachment
                        {
                            AttachmentBytes = bytes,
                            EmailLog = emailLog,
                            Name = attachment.Name,
                            TenantId = emailLog.TenantId,
                        });
                    }

                    // log email sending
                    if (tenantId != default(long))
                    {
                        Exception exception;
                        var ehandler = new EmailLogCreationHandler();
                        var errs = ehandler.SaveEmailLog(emailLog, out exception);
                        if (exception != null)
                            ErrorLogger.LogError(exception, HttpContext.Current);

                        if (errs.Any())
                            foreach (var err in errs)
                                ErrorLogger.LogError(new Exception(err.Message), HttpContext.Current);
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex, ProcessorVars.ErrorFolder);
                }

                var client = new SmtpClient(WebApplicationSettings.SmtpNetworkHost) { DeliveryMethod = SmtpDeliveryMethod.Network };
                if (!string.IsNullOrEmpty(WebApplicationSettings.SmtpPort)) client.Port = WebApplicationSettings.SmtpPort.ToInt();
                client.Send(msg);

                return true;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, ProcessorVars.ErrorFolder);
                return false;
            }
        }


        public static bool QueueEmail(this IEnumerable<string> tos, IEnumerable<string> ccs, IEnumerable<string> bbcs, string body, string subject, long tenantId)
        {
            return QueueEmail(tos, ccs, bbcs, body, subject, new Attachment[0], tenantId);
        }

        public static bool QueueEmail(this IEnumerable<string> tos, IEnumerable<string> ccs, IEnumerable<string> bccs, string body, string subject, IEnumerable<Attachment> attachments, long tenantId)
        {
            try
            {
                var emailLog = new EmailLog
                {
                    Tos = string.Join("; ", tos.Distinct()),
                    Ccs = string.Join("; ", ccs.Distinct()),
                    Bbcs = string.Join("; ", bccs.Distinct()),
                    DateCreated = DateTime.Now,
                    Subject = subject,
                    Body = body,
                    EmailLogAttachments = new List<EmailLogAttachment>(),
                    From = new MailAddress(WebApplicationSettings.DoNotReplyEmail).Address,
                    TenantId = tenantId,
                    Sent = false
                };

                foreach (var attachment in attachments)
                {
                    byte[] bytes;

                    using (var streamReader = new MemoryStream())
                    {
                        attachment.ContentStream.CopyTo(streamReader);
                        bytes = streamReader.ToArray();
                    }

                    emailLog.EmailLogAttachments.Add(new EmailLogAttachment
                    {
                        AttachmentBytes = bytes,
                        EmailLog = emailLog,
                        Name = attachment.Name,
                        TenantId = tenantId
                    });
                }

                // log email sending
                Exception exception;
                new EmailLogCreationHandler().SaveEmailLog(emailLog, out exception);
                if (exception != null)
                {
                    ErrorLogger.LogError(exception, HttpContext.Current);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, ProcessorVars.ErrorFolder);
                return false;
            }

            return true;
        }
    }
}
