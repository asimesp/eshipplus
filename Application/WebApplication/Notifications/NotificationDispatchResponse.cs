﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.WebApplication.Notifications
{
    public class NotificationDispatchResponse
    {
        public CheckCall CheckCall;
        public List<ValidationMessage> Messages;
    }
}