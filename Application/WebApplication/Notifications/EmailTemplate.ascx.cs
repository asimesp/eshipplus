﻿using System;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Members;

namespace LogisticsPlus.Eship.WebApplication.Notifications
{
    public partial class EmailTemplate : MemberControlBase
    {
        public new bool Visible
        {
            get { return pnlEmail.Visible; }
            set
            {
                base.Visible = true;
                pnlEmail.Visible = value;
                pnlEmailDimScreen.Visible = value;
            }
        }

        public bool ShowCc
        {
            get { return pnlCc.Visible; }
            set { pnlCc.Visible = value; }
        }

        public bool ShowBcc
        {
            get { return pnlBcc.Visible; }
            set { pnlBcc.Visible = value; }
        }

        public string Title
        {
            get { return title.Text; }
            set { title.Text = value; }
        }

        public string Attachment
        {
            get { return attachment.Text; }
            set { attachment.Text = value; }
        }

        public event EventHandler<ViewEventArgs<EmailMessage>> SendEmail;
        public event EventHandler EmailCancel;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnSendClicked(object sender, EventArgs e)
        {
            var email = new EmailMessage
                            {
                                To = txtTo.Text,
                                Cc = txtCc.Text,
                                Bcc = txtBcc.Text,
                                Subject = txtSubject.Text,
                                Body = txtBody.Text
                            };

            if (SendEmail != null)
                SendEmail(this, new ViewEventArgs<EmailMessage>(email));
        }

        protected void OnCancelClicked(object sender, EventArgs e)
        {
            if (EmailCancel != null)
                EmailCancel(this, new EventArgs());
        }
    }
}