﻿using LogisticsPlus.Eship.WebApplication.Documents;

namespace LogisticsPlus.Eship.WebApplication.Notifications
{
    public class EmailTemplateFields : BaseFields
    {
        public const string Title = "[#Title#]";
        public const string LogoUrl = "[#LogoUrl#]";
        public const string Body = "[#Body#]";

        public const string AccountName = "[#AccountName#]";
        public const string Brand = "[#Brand#]";
        public const string SiteRoot = "[#SiteRoot#]";

        public const string BookingRequestNumber = "[#BookingRequestNumber#]";
        public const string ViewBookingRequestDashboardWrapperStart = "[#ViewBookingRequestDashboardWrapperStart#]";
        public const string ViewBookingRequestDashboardWrapperEnd = "[#ViewBookingRequestDashboardWrapperEnd#]";

        public const string QuoteNumber = "[#QuoteNumber#]";
        public const string LoadNumber = "[#LoadNumber#]";
		public const string LoadStatus = "[#LoadStatus#]";
        
        public const string ViewLoadsDashboardWrapperStart = "[#ViewLoadsDashboardWrapperStart#]";
        public const string ViewLoadsDashboardWrapperEnd = "[#ViewLoadsDashboardWrapperEnd#]";

        public const string ViewQuoteDashboardWrapperStart = "[#ViewQuoteDashboardWrapperStart#]";
        public const string ViewQuoteDashboardWrapperEnd = "[#ViewQuoteDashboardWrapperEnd#]";


        public const string ShipmentNumber = "[#ShipmentNumber#]";
        public const string ShipmentPickupDate = "[#ShipmentPickupDate#]";
        public const string ShipmentEstimatedDeliveryDate = "[#ShipmentEstimatedDeliveryDate#]";
        public const string ShipmentDeliveryDate = "[#ShipmentDeliveryDate#]";
        public const string ViewShipmentDashboardWrapperStart = "[#ViewShipmentDashboardWrapperStart#]";
        public const string ViewShipmentDashboardWrapperEnd = "[#ViewShipmentDashboardWrapperEnd#]";
        public const string AdditionalShipmentInsuranceWrapperStart = "[#AdditionalShipmentInsuranceWrapperStart#]";
        public const string AdditionalShipmentInsuranceWrapperEnd = "[#AdditionalShipmentInsuranceWrapperEnd#]";

        public const string InvoiceNumber = "[#InvoiceNumber#]";
        public const string InvoiceDocumentDownloadLink = "[#InvoiceDocumentDownloadLink#]";
        public const string PostDate = "[#PostDate#]";
        public const string DueDate = "[#DueDate#]";
        public const string ViewInvoiceDashboardWrapperStart = "[#ViewInvoiceDashboardWrapperStart#]";
        public const string ViewInvoiceDashboardWrapperEnd = "[#ViewInvoiceDashboardWrapperEnd#]";


        public const string User = "[#User#]";
        public const string CustomerNumber = "[#CustomerNumber#]";
        public const string CustomerName = "[#CustomerName#]";
        public const string ContactName = "[#ContactName#]";
        public const string ContactEmail = "[#ContactEmail#]";
        public const string SupportMessage = "[#SupportMessage#]";
        public const string SupportCategory = "[#SupportCategory#]";
        public const string VendorName = "[#VendorName#]";
        public const string VendorNumber = "[#VendorNumber#]";

        public const string Prefix = "[#Prefix#]";

        public const string ReportName = "[#ReportName#]";

        public const string TranmissionType = "[#TranmissionType#]";
        public const string XmlTransmissionErrorMessage = "[#XmlTransmissionErrorMessage#]";

        public const string BackgroundReportWrapperStart = "[#BackgroundReportWrapperStart#]";
        public const string BackgroundReportWrapperEnd = "[#BackgroundReportWrapperEnd#]";

        public const string NoAttachmentWrapperStart = "[#NoAttachmentWrapperStart#]";
        public const string NoAttachmentWrapperEnd = "[#NoAttachmentWrapperEnd#]";

        public const string AttachmentWrapperStart = "[#AttachmentWrapperStart#]";
        public const string AttachmentWrapperEnd = "[#AttachmentWrapperEnd#]";

        public const string SupportRequestBody = "[#SupportRequestBody#]";
        public const string ReplyToEmail = "[#ReplyToEmail#]";

        public const string CheckCallWrapperStart = "[#CheckCallWrapperStart#]";
        public const string CheckCallDate = "[#CheckCallDate#]";
		public const string CheckCallEventDate = "[#CheckCallEventDate#]";
        public const string CheckCallNote = "[#CheckCallNote#]";
        public const string CheckCallWrapperEnd = "[#CheckCallWrapperEnd#]";

        public const string RequestDate = "[#RequestDate#]";

        public const string ClaimNumber = "[#ClaimNumber#]";
        public const string ClaimNoteMessage = "[#ClaimNoteMessage#]";

    }
}
