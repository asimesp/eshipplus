﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern;
using LogisticsPlus.Eship.Extern.Edi;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Fax;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using P44SDK.V4.Model;
using Shipment = LogisticsPlus.Eship.Core.Operations.Shipment;

namespace LogisticsPlus.Eship.WebApplication.Notifications
{
	public static class NotificationProcessor
	{
        private const string PickupRequestCancelled = "Pickup Request Cancelled.";
        private const string PickupRequestCancelledWithMsg = "Pickup Request Cancelled. Msg: {0}";

		public static void NotifyLoadOrderEvent(this MemberPageBase page, LoadOrder loadOrder)
		{
			// compile emails
			var seperators = WebApplicationUtilities.Seperators();
			var customer = loadOrder.Customer;
			var emails = customer == null || customer.RateAndScheduleInDemo
							? new List<string>()
							: customer.Tier.ApplicableEmails(loadOrder.ServiceMode)
								.Split(seperators, StringSplitOptions.RemoveEmptyEntries)
								.ToList();
			var milestones = loadOrder.Status == LoadOrderStatus.Offered
								? new List<Milestone> { Milestone.NewLoadOrder }
								: loadOrder.Status == LoadOrderStatus.Cancelled
									? new List<Milestone> { Milestone.LoadOrderVoided }
									: new List<Milestone>();

			var cm = customer != null && customer.Communication != null
						? customer.Communication.Notifications
							.Where(n => milestones.Contains(n.Milestone) && n.Enabled)
							.ToList()
						: new List<CustomerNotification>();
			foreach (var email in cm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Email).Select(m => m.Email))
				emails.AddRange(email.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct());
			if (!string.IsNullOrEmpty(loadOrder.User.Email)) emails.Add(loadOrder.User.Email);
			emails = emails.Distinct().ToList();

			if (emails.Any())
			{
				// body
				var body = page.GenerateLoadOrderNotification(loadOrder);

				var title = string.Format("eShipPlus TMS {0} {1} Load Details", WebApplicationSettings.Version, loadOrder.LoadOrderNumber);
				var bookingRequestHtml = page.GenerateLoadOrderDetails(loadOrder, true);
				var attachment = new Attachment(new MemoryStream(bookingRequestHtml.ToPdf(title)), string.Format("{0}.pdf", title));

				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);
				var subject = string.Format("{0} New {1} Load Submission - {2}",loadOrder.Tenant.AutoNotificationSubjectPrefix,  loadOrder.Status.GetString().FormattedString(), loadOrder.LoadOrderNumber);
                new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, new[] { attachment }, page.ActiveUser.TenantId);
			}
		}


		public static void NotifyCancelledShipmentReversed(this MemberPageBase page, Shipment shipment)
		{
			var toEmailAddresses = new[] { WebApplicationSettings.DoNotReplyEmail };

			// compile emails
			var customer = shipment.Customer;
			var seperators = WebApplicationUtilities.Seperators();
			var emails = customer == null
							? new List<string>()
							: customer.Tier.ApplicableEmails(shipment.ServiceMode)
								.Split(seperators, StringSplitOptions.RemoveEmptyEntries)
								.Distinct()
								.ToList();
			var cm = customer != null && customer.Communication != null
						? customer.Communication.Notifications
							.Where(n => n.Milestone == Milestone.ShipmentVoided && n.Enabled)
							.ToList()
						: new List<CustomerNotification>();
			foreach (var email in cm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Email).Select(m => m.Email))
				emails.AddRange(email.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct());
			if (!string.IsNullOrEmpty(shipment.User.Email)) emails.Add(shipment.User.Email);

		    if (shipment.SendShipmentUpdateToOriginPrimaryContact)
		    {
		        var opc = (shipment.Origin ?? new ShipmentLocation()).Contacts.FirstOrDefault(c => c.Primary);
		        var em = opc != null ? opc.Email : string.Empty;
		        if(!string.IsNullOrEmpty(em)) emails.Add(em);
		    }
		    if (shipment.SendShipmentUpdateToDestinationPrimaryContact)
		    {
		        var dpc = (shipment.Destination ?? new ShipmentLocation()).Contacts.FirstOrDefault(c => c.Primary);
		        var em = dpc != null ? dpc.Email : string.Empty;
		        if (!string.IsNullOrEmpty(em)) emails.Add(em);
		    }

            emails = emails.Distinct().ToList();


			if (emails.Any())
			{
				

				// body
				var body = page.GenerateCancelledShipmentReversedNotification(shipment);
				var title = string.Format("eShipPlus TMS {0} {1} Cancelled Bill of Lading Reversed", WebApplicationSettings.Version, shipment.ShipmentNumber);
				var bolHtml = page.GenerateBOL(shipment, true);
				var attachment = new Attachment(new MemoryStream(bolHtml.ToPdf(title)), string.Format("{0}.pdf", title));


				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);
				var subject = string.Format("{0} Cancelled Shipment Reversed - {1}",shipment.Tenant.AutoNotificationSubjectPrefix, shipment.ShipmentNumber);
                toEmailAddresses.QueueEmail(new string[0], emails, body, subject, new[] { attachment }, page.ActiveUser.TenantId);
			}
		}

		public static void NotifyCancelledShipment(this MemberPageBase page, Shipment shipment)
		{
			var toEmailAddresses = new[] { WebApplicationSettings.DoNotReplyEmail };

			// CUSTOMER

			// body
			var body = page.GenerateCancelledShipmentNotification(shipment);
			var title = string.Format("eShipPlus TMS {0} {1} Cancelled Bill of Lading", WebApplicationSettings.Version, shipment.ShipmentNumber);
			var bolHtml = page.GenerateBOL(shipment, true);
			var attachment = new Attachment(new MemoryStream(bolHtml.ToPdf(title)), string.Format("{0}.pdf", title));

			// compile emails
			var customer = shipment.Customer;
			var seperators = WebApplicationUtilities.Seperators();
			var emails = customer == null
							? new List<string>()
							: customer.Tier.ApplicableEmails(shipment.ServiceMode)
								.Split(seperators, StringSplitOptions.RemoveEmptyEntries)
								.Distinct()
								.ToList();
			var cCommunication = customer != null ? customer.Communication : null;
			var cm = cCommunication != null
						? cCommunication.Notifications
							.Where(n => n.Milestone == Milestone.ShipmentVoided && n.Enabled)
							.ToList()
						: new List<CustomerNotification>();
			foreach (var email in cm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Email).Select(m => m.Email))
				emails.AddRange(email.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct());
			if (!string.IsNullOrEmpty(shipment.User.Email)) emails.Add(shipment.User.Email);

		    if (shipment.SendShipmentUpdateToOriginPrimaryContact)
		    {
		        var opc = (shipment.Origin ?? new ShipmentLocation()).Contacts.FirstOrDefault(c => c.Primary);
		        var em = opc != null ? opc.Email : string.Empty;
		        if (!string.IsNullOrEmpty(em)) emails.Add(em);
		    }
		    if (shipment.SendShipmentUpdateToDestinationPrimaryContact)
		    {
		        var dpc = (shipment.Destination ?? new ShipmentLocation()).Contacts.FirstOrDefault(c => c.Primary);
		        var em = dpc != null ? dpc.Email : string.Empty;
		        if (!string.IsNullOrEmpty(em)) emails.Add(em);
		    }

            emails = emails.Distinct().ToList();


			if (emails.Any())
			{
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);
				var subject = string.Format("{0} Cancelled Shipment - {1}",shipment.Tenant.AutoNotificationSubjectPrefix,  shipment.ShipmentNumber);
                toEmailAddresses.QueueEmail(new string[0], emails, body, subject, new[] { attachment }, page.ActiveUser.TenantId);
			}



			// check customer handles vendor notifications
			if (shipment.Customer.Communication != null && shipment.Customer.Communication.StopVendorNotifications) return;

			// VENDOR
			var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
			if (sv == null) return;
			emails.Clear();
			var bol = string.Format("{0}{1}",
									!shipment.HidePrefix && shipment.Prefix != null ? shipment.Prefix.Code : string.Empty,
									shipment.ShipmentNumber);
			var vm = sv.Vendor.Communication != null
						? sv.Vendor.Communication.Notifications
							.Where(n => n.Milestone == Milestone.ShipmentVoided && n.Enabled)
							.ToList()
						: new List<VendorNotification>();
			foreach (var email in vm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Email).Select(m => m.Email))
				emails.AddRange(email.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct());


			if (emails.Any())
			{
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);
				emails = emails.Distinct().ToList();
				var subject = string.Format("{0} Cancelled Shipment Notification: {1}",shipment.Tenant.AutoNotificationSubjectPrefix,  bol);
                toEmailAddresses.QueueEmail(new string[0], emails, bolHtml, subject, page.ActiveUser.TenantId);
			}

			// process fax notification
			page.ProcessFax(vm, shipment, bolHtml, title, true);
		}

		public static void NotifyNewShipment(this MemberPageBase page, Shipment shipment,bool isAveryOrStandardLabelRequired = false)
		{
			var toEmailAddresses = new[] { WebApplicationSettings.DoNotReplyEmail };

			// CUSTOMER

			// body
			var body = page.GenerateNewShipmentNotification(shipment);
			var title = string.Format("eShipPlus TMS {0} {1} Bill of Lading", WebApplicationSettings.Version, shipment.ShipmentNumber);
			var avaryLabelTitle = string.Format("eShipPlus TMS {0} {1} Avery Label", WebApplicationSettings.Version, shipment.ShipmentNumber);
			var standardLabelTitle = string.Format("eShipPlus TMS {0} {1} Standard Label", WebApplicationSettings.Version, shipment.ShipmentNumber);
			var bolHtml = page.GenerateBOL(shipment, true);
			var attachment = new Attachment(new MemoryStream(bolHtml.ToPdf(title)), string.Format("{0}.pdf", title));

			// compile emails
			var customer = shipment.Customer;
			var seperators = WebApplicationUtilities.Seperators();
			var emails = customer == null || customer.RateAndScheduleInDemo
							? new List<string>()
							: customer.Tier.ApplicableEmails(shipment.ServiceMode)
								.Split(seperators, StringSplitOptions.RemoveEmptyEntries)
								.Distinct()
								.ToList();
			var cCommunication = customer != null ? customer.Communication : null;
			var cm = cCommunication != null
						? cCommunication.Notifications
							.Where(n => n.Milestone == Milestone.NewShipmentRequest && n.Enabled)
							.ToList()
						: new List<CustomerNotification>();
			foreach (var email in cm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Email).Select(m => m.Email))
				emails.AddRange(email.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct());
			if (!string.IsNullOrEmpty(shipment.User.Email)) emails.Add(shipment.User.Email);

            if (emails.Any())
			{
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);
				emails = emails.Distinct().ToList();
				var subject = string.Format("{0} New {1} Shipment Submission - {2}",shipment.Tenant.AutoNotificationSubjectPrefix,  shipment.ServiceMode.FormattedString(),
											shipment.ShipmentNumber);
				var attachments = shipment.Documents.Select(d => new Attachment(page.Server.MapPath(d.LocationPath))).ToList();
				attachments.Add(attachment);

				//check if AveryLabel/Standard is to be send as email attachment
				if (isAveryOrStandardLabelRequired && cCommunication != null)
				{
					const string cssPath = WebApplicationConstants.Member2CssUrl;
					var absolute = cssPath.Replace("../..", WebApplicationSettings.SiteRoot);

					if (cCommunication.SendAveryLabel)
					{
						var writer = new StringWriter();
						page.Server.Execute(
							string.Format("{0}?{1}={2}", AveryShippingLabel.PageAddress, WebApplicationConstants.ShipmentAuditId,
										  shipment.Id.GetString().UrlTextEncrypt()), writer);

						attachments.Add(
							new Attachment(
								new MemoryStream(writer.GetStringBuilder().ToString().Replace(cssPath, absolute).ToUtf8Bytes()),
								string.Format("{0}.html", avaryLabelTitle)));
					}

					if (cCommunication.SendStandardLabel)
					{
						var standardWriter = new StringWriter();
						page.Server.Execute(
							string.Format("{0}?{1}={2}", StandardShippingLabel.PageAddress, WebApplicationConstants.ShipmentAuditId,
							              shipment.Id.GetString().UrlTextEncrypt()), standardWriter);

						attachments.Add(
							new Attachment(
								new MemoryStream(standardWriter.GetStringBuilder().ToString().Replace(cssPath, absolute).ToUtf8Bytes()),
								string.Format("{0}.html", standardLabelTitle)));
					}
				}
                toEmailAddresses.QueueEmail(new string[0], emails, body, subject, attachments, page.ActiveUser.TenantId);
			}


			// check customer in demo mode
			if (shipment.Customer.RateAndScheduleInDemo) return;

			// check customer handles vendor notifications
			if (shipment.Customer.Communication != null && shipment.Customer.Communication.StopVendorNotifications) return;

		

			// VENDOR
			var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
			if (sv == null) return;
			emails.Clear();
			var bol = string.Format("{0}{1}",
									!shipment.HidePrefix && shipment.Prefix != null ? shipment.Prefix.Code : string.Empty,
									shipment.ShipmentNumber);
			var vm = sv.Vendor.Communication != null
						? sv.Vendor.Communication.Notifications
							.Where(n => n.Milestone == Milestone.NewShipmentRequest && n.Enabled)
							.ToList()
						: new List<VendorNotification>();
			foreach (var email in vm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Email).Select(m => m.Email))
				emails.AddRange(email.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct());

			if (emails.Any())
			{
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);
				emails = emails.Distinct().ToList();
				var subject = string.Format("{0} Shipment Notification: {1}",shipment.Tenant.AutoNotificationSubjectPrefix,  bol);
                toEmailAddresses.QueueEmail(new string[0], emails, bolHtml, subject, page.ActiveUser.TenantId);
			}

			// process fax notification
			page.ProcessFax(vm, shipment, bolHtml, title);
		}

        public static void NotifyVendorChargeDispute(this MemberPageBase page, List<ChargeViewSearchDto> charges)
        {
            var body = page.GenerateVendorChargeDisputeNotification(charges);

            var emails = page.ActiveUser.DefaultCustomer.Tier
                             .AccountPayableDisputeNotificationEmails
                             .Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
                             .Distinct()
                             .ToList();
            emails.Add(page.ActiveUser.Email);

            if (emails.Any())
            {
                // monitor emails
                emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

                var subject = string.Format("{0} Vendor Charge Dispute Notification - {1}",
                                            page.ActiveUser.Tenant.AutoNotificationSubjectPrefix,
                                            page.ActiveUser.DefaultCustomer.Name);
                new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
            }
        }

        public static void NotifyVendorChargeConfirmation(this MemberPageBase page, List<ChargeViewSearchDto> charges)
        {
            var body = page.GenerateVendorChargeConfirmNotification(charges);

            var emails = page.ActiveUser.DefaultCustomer.Tier
                             .AccountPayableDisputeNotificationEmails
                             .Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
                             .Distinct()
                             .ToList();
            emails.Add(page.ActiveUser.Email);

            if (emails.Any())
            {
                // monitor emails
                emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

                var subject = string.Format("{0} Vendor Charge Confirmation Notification - {1}",
                                            page.ActiveUser.Tenant.AutoNotificationSubjectPrefix,
                                            page.ActiveUser.DefaultCustomer.Name);
                new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
            }
        }

		public static void NotifyVendorDisputeResolved(this MemberPageBase page, List<ChargeViewSearchDto> charges, VendorDispute dispute)
		{
			var body = page.GenerateVendorDisputeResolvedNotification(charges, dispute);
			//TODO: Judy not sure if email should go out to AccountPayableDisputeNotificationEmails group. Remove if needed after Judy confirm 
			var emails = page.ActiveUser.DefaultCustomer.Tier
							 .AccountPayableDisputeNotificationEmails
							 .Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
							 .Distinct()
							 .ToList();
			emails.Add(page.ActiveUser.Email);
			if (!string.IsNullOrWhiteSpace(dispute.CreatedByUser.Email))
			          emails.Add(dispute.CreatedByUser.Email);

			if (emails.Any())
			{
				//monitor emails
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

				var subject = string.Format("{0} Vendor Dispute Resolved Notification - {1}",
											page.ActiveUser.Tenant.AutoNotificationSubjectPrefix,
											page.ActiveUser.DefaultCustomer.Name);
				new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
			}
		}


        public static void NotifyShipmentDelivered(this MemberPageBase page, Shipment shipment)
		{
			var toEmailAddresses = new[] { WebApplicationSettings.DoNotReplyEmail };

			// CUSTOMER

			// body
			var body = page.GenerateShipmentDeliveredNotification(shipment);

			// compile emails
			var customer = shipment.Customer;
			var seperators = WebApplicationUtilities.Seperators();
			var emails = new List<string>();
			var communication = customer != null ? customer.Communication : null;
			var cm = communication != null
						? communication.Notifications
							.Where(n => n.Milestone == Milestone.ShipmentDelivered && n.Enabled)
							.ToList()
						: new List<CustomerNotification>();
			foreach (var email in cm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Email).Select(m => m.Email))
				emails.AddRange(email.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct());

            if (shipment.SendShipmentUpdateToOriginPrimaryContact)
            {
                var opc = (shipment.Origin ?? new ShipmentLocation()).Contacts.FirstOrDefault(c => c.Primary);
                var em = opc != null ? opc.Email : string.Empty;
                if (!string.IsNullOrEmpty(em)) emails.Add(em);
            }
            if (shipment.SendShipmentUpdateToDestinationPrimaryContact)
            {
                var dpc = (shipment.Destination ?? new ShipmentLocation()).Contacts.FirstOrDefault(c => c.Primary);
                var em = dpc != null ? dpc.Email : string.Empty;
                if (!string.IsNullOrEmpty(em)) emails.Add(em);
            }

            emails = emails.Distinct().ToList();



			if (emails.Any())
			{
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);
				var subject = string.Format("{0} Shipment Delivered - {1}", shipment.Tenant.AutoNotificationSubjectPrefix, shipment.ShipmentNumber);
                toEmailAddresses.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
			}

			// phantom check call for edi status message (delivery message)
			var shipDelCheckCall = new List<CheckCall>
				{
					new CheckCall
						{
							EventDate = shipment.ActualDeliveryDate,
							CallDate = DateTime.Now,
							CallNotes = string.Empty,
							EdiStatusCode = ShipStatMsgCode.D1.GetString(),
							TenantId = shipment.TenantId,
							Shipment = shipment,
							User = page.ActiveUser,
						}
				};

			// EDI
			if (communication != null && communication.EdiEnabled && cm.Any(n => n.NotificationMethod == NotificationMethod.Edi))
				ProcessXmlTransmission(page.Server, shipment, NotificationMethod.Edi, communication, shipDelCheckCall, page.ActiveUser);

			// FTP
			if (communication != null && communication.FtpEnabled && cm.Any(n => n.NotificationMethod == NotificationMethod.Ftp))
				ProcessXmlTransmission(page.Server, shipment, NotificationMethod.Ftp, communication, shipDelCheckCall, page.ActiveUser);
		}

		public static void NotifyShipmentInTransit(this MemberPageBase page, Shipment shipment)
		{
			var toEmailAddresses = new[] { WebApplicationSettings.DoNotReplyEmail };

			// CUSTOMER

			// body
			var body = page.GenerateShipmentInTransitNotification(shipment);

			// compile emails
			var customer = shipment.Customer;
			var seperators = WebApplicationUtilities.Seperators();
			var emails = new List<string>();
			var communication = customer != null ? customer.Communication : null;
			var cm = communication != null
						? communication.Notifications
							.Where(n => n.Milestone == Milestone.ShipmentInTransit && n.Enabled)
							.ToList()
						: new List<CustomerNotification>();
			foreach (var email in cm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Email).Select(m => m.Email))
				emails.AddRange(email.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct());

		    if (shipment.SendShipmentUpdateToOriginPrimaryContact)
		    {
		        var opc = (shipment.Origin ?? new ShipmentLocation()).Contacts.FirstOrDefault(c => c.Primary);
		        var em = opc != null ? opc.Email : string.Empty;
		        if (!string.IsNullOrEmpty(em)) emails.Add(em);
		    }
		    if (shipment.SendShipmentUpdateToDestinationPrimaryContact)
		    {
		        var dpc = (shipment.Destination ?? new ShipmentLocation()).Contacts.FirstOrDefault(c => c.Primary);
		        var em = dpc != null ? dpc.Email : string.Empty;
		        if (!string.IsNullOrEmpty(em)) emails.Add(em);
		    }

            emails = emails.Distinct().ToList();


			if (emails.Any())
			{
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);
				var subject = string.Format("{0} Shipment in Transit - {1}",shipment.Tenant.AutoNotificationSubjectPrefix, shipment.ShipmentNumber);
                toEmailAddresses.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
			}

			// phantom check call for edi status message (in transit/pickup  message)
			var shipDelCheckCall = new List<CheckCall>
				{
					new CheckCall
						{
							EventDate = shipment.ActualPickupDate,
							CallDate = DateTime.Now,
							CallNotes = string.Empty,
							EdiStatusCode = ShipStatMsgCode.AF.GetString(),
							TenantId = shipment.TenantId,
							Shipment = shipment,
							User = page.ActiveUser,
						}
				};


			// EDI
			if (communication != null && communication.EdiEnabled && cm.Any(n => n.NotificationMethod == NotificationMethod.Edi))
				ProcessXmlTransmission(page.Server, shipment, NotificationMethod.Edi, communication, shipDelCheckCall, page.ActiveUser);

			// FTP
			if (communication != null && communication.FtpEnabled && cm.Any(n => n.NotificationMethod == NotificationMethod.Ftp))
				ProcessXmlTransmission(page.Server, shipment, NotificationMethod.Ftp, communication, shipDelCheckCall, page.ActiveUser);
		}

		public static void NotifyCheckCallUpdate(this MemberPageBase page, Shipment shipment, List<CheckCall> ediFtpCheckCalls = null)
		{
			var toEmailAddresses = new[] { WebApplicationSettings.DoNotReplyEmail };

			// CUSTOMER

			// body
			var body = page.GenerateCheckCallNotification(shipment);

			// compile emails
			var customer = shipment.Customer;
			var seperators = WebApplicationUtilities.Seperators();
			var emails = new List<string>();
			var communication = customer != null ? customer.Communication : null;
			var cm = communication != null
						? communication.Notifications
							.Where(n => n.Milestone == Milestone.ShipmentCheckCallUpdate && n.Enabled)
							.ToList()
						: new List<CustomerNotification>();
			foreach (var email in cm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Email).Select(m => m.Email))
				emails.AddRange(email.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct());

		    if (shipment.SendShipmentUpdateToOriginPrimaryContact)
		    {
		        var opc = (shipment.Origin ?? new ShipmentLocation()).Contacts.FirstOrDefault(c => c.Primary);
		        var em = opc != null ? opc.Email : string.Empty;
		        if (!string.IsNullOrEmpty(em)) emails.Add(em);
		    }
		    if (shipment.SendShipmentUpdateToDestinationPrimaryContact)
		    {
		        var dpc = (shipment.Destination ?? new ShipmentLocation()).Contacts.FirstOrDefault(c => c.Primary);
		        var em = dpc != null ? dpc.Email : string.Empty;
		        if (!string.IsNullOrEmpty(em)) emails.Add(em);
		    }

            emails = emails.Distinct().ToList();

			if (emails.Any())
			{
				// monitor emails
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

				var subject = string.Format("{0} Shipment Check Call Notification - {1}", shipment.Tenant.AutoNotificationSubjectPrefix, shipment.ShipmentNumber);
                toEmailAddresses.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
			}

			if (ediFtpCheckCalls == null || !ediFtpCheckCalls.Any()) return;

			// EDI
			if (communication != null && communication.EdiEnabled && cm.Any(n => n.NotificationMethod == NotificationMethod.Edi))
				ProcessXmlTransmission(page.Server, shipment, NotificationMethod.Edi, communication, ediFtpCheckCalls, page.ActiveUser);

			// FTP
			if (communication != null && communication.FtpEnabled && cm.Any(n => n.NotificationMethod == NotificationMethod.Ftp))
				ProcessXmlTransmission(page.Server, shipment, NotificationMethod.Ftp, communication, ediFtpCheckCalls, page.ActiveUser);
		}

		public static void NotifyCheckCallUpdateEdiFtpOnly(this HttpServerUtility server, Shipment shipment, List<CheckCall> ediFtpCheckCalls, User processingUser)
		{
			// EDI
			var communication = shipment.Customer.Communication;
			var cm = communication.Notifications;
			if (communication != null && communication.EdiEnabled && cm.Any(n => n.NotificationMethod == NotificationMethod.Edi))
				ProcessXmlTransmission(server, shipment, NotificationMethod.Edi, communication, ediFtpCheckCalls, processingUser);

			// FTP
			if (communication != null && communication.FtpEnabled && cm.Any(n => n.NotificationMethod == NotificationMethod.Ftp))
				ProcessXmlTransmission(server, shipment, NotificationMethod.Ftp, communication, ediFtpCheckCalls, processingUser);
		}

        public static void NotifyClaimNoteReminder(this HttpServerUtility server, ClaimNote note)
        {
            var body = server.GenerateClaimNoteReminderNotification(note);

            var emails = note.SendReminderEmails
                .Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
                .Distinct()
                .ToList();

            if (emails.Any())
            {
                var subject = string.Format("Reminder Note For Claim {0}", note.Claim.ClaimNumber);
                emails.QueueEmail(new string[0], ProcessorVars.MonitorEmails[note.TenantId], body, subject, note.TenantId); // monitor emails Bcc'ed.
            }
        }



		public static NotificationDispatchResponse DispatchShipment(this MemberPageBase page, Shipment shipment, string pickupToUpdatePickupNumber = null)
		{
			if (!WebApplicationSettings.ShipmentDispatchEnabled) return null;

			try
			{
				var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
				if (sv == null || sv.Vendor.Communication == null) return null;
				var vcomm = sv.Vendor.Communication;
				if (!vcomm.ShipmentDispatchEnabled) return null;

				NotificationDispatchResponse response = null;
                // saved as formatted string, must replace empty spaces
				switch (vcomm.CarrierIntegrationEngine.Replace(" ", string.Empty).ToEnum<CarrierEngine>())
				{
					case CarrierEngine.DaytonFreight:
				        response = DispatchDayton(page, shipment, vcomm, pickupToUpdatePickupNumber);
						break;
					case CarrierEngine.Estes:
						response = DispatchEstes(page, shipment, vcomm, pickupToUpdatePickupNumber);
						break;
                    case CarrierEngine.FedExFreightEconomy:
                        response = DispatchFedExEconomy(page, shipment, vcomm, pickupToUpdatePickupNumber);
				        break;
                    case CarrierEngine.FedExFreightPriority:
                        response = DispatchFedExPriority(page, shipment, vcomm, pickupToUpdatePickupNumber);
				        break;
                    case CarrierEngine.Holland:
                        response = DispatchHolland(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
                    case CarrierEngine.PittOhio:
				        response = DispatchPittOhio(page, shipment, vcomm, pickupToUpdatePickupNumber);
				        break;
                    case CarrierEngine.SouthEasternFreight:
                        response = DispatchSouthEasternFreight(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
                    case CarrierEngine.SouthwesternMotorTransport:
                        response = DispatchSouthwesternMotorTransport(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
                    case CarrierEngine.Ups:
                        response = DispatchUps(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
                    case CarrierEngine.Ward:
				        response = DispatchWard(page, shipment, vcomm, pickupToUpdatePickupNumber);
				        break;
                    case CarrierEngine.Project44:
                        if (!string.IsNullOrEmpty(pickupToUpdatePickupNumber))
                        {
                            return new NotificationDispatchResponse
                            {
                                Messages = new List<ValidationMessage>
                                {
                                    new ValidationMessage
                                    {
                                        Message = "Dispatched shipments cannot be updated through Project 44. Please cancel this shipment and perform dispatch again.",
                                        Type = ValidateMessageType.Error
                                    }
                                },
                                CheckCall = null
                            };
                        }

                        response = DispatchProject44(page, shipment, vcomm);
                        break;

				}
				return response;
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
			    return new NotificationDispatchResponse
			        {
			            Messages = new List<ValidationMessage>
				            {
			                    new ValidationMessage
			                        {
			                            Message = ex.Message,
			                            Type = ValidateMessageType.Error
			                        }
			                },
			            CheckCall = null
			        };
			}
		}

		public static CheckCall CancelShipmentDispatch(this MemberPageBase page, Shipment shipment, string pickupToUpdatePickupNumber)
		{
			if (!WebApplicationSettings.ShipmentDispatchEnabled) return null;

			try
			{
				var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
				if (sv == null || sv.Vendor.Communication == null) return null;
				var vcomm = sv.Vendor.Communication;
				if (!vcomm.ShipmentDispatchEnabled) return null;

				CheckCall checkCall = null;
                // saved as formatted string, must replace empty spaces
                switch (vcomm.CarrierIntegrationEngine.Replace(" ", string.Empty).ToEnum<CarrierEngine>())
				{
					case CarrierEngine.DaytonFreight:
						checkCall = CancelDaytonDispatch(page, shipment, vcomm, pickupToUpdatePickupNumber);
						break;
					case CarrierEngine.Estes:
						checkCall = CancelEstesDispatch(page, shipment, vcomm, pickupToUpdatePickupNumber);
						break;
                    case CarrierEngine.FedExFreightEconomy:
                        checkCall = CancelFedExEconomyDispatch(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
                    case CarrierEngine.FedExFreightPriority:
                        checkCall = CancelFedExPriorityDispatch(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
                    case CarrierEngine.Holland:
                        checkCall = CancelHollandDispatch(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
                    case CarrierEngine.PittOhio:
                        checkCall = CancelPittOhioDispatch(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
                    case CarrierEngine.SouthEasternFreight:
                        checkCall = CancelSouthEasternFreightDispatch(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
                    case CarrierEngine.SouthwesternMotorTransport:
                        checkCall = CancelSouthwesternMotorTransportDispatch(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
                    case CarrierEngine.Ups:
                        checkCall = CancelUpsDispatch(page, shipment, vcomm, pickupToUpdatePickupNumber);
                        break;
				    case CarrierEngine.Project44:
				        checkCall = CancelProject44Dispatch(page, shipment, pickupToUpdatePickupNumber);
				        break;
                }
				return checkCall;
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
				return null;
			}
		}


		private static NotificationDispatchResponse DispatchEstes(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
		{
		    var dispatchResponse = new NotificationDispatchResponse
		        {
		            Messages = new List<ValidationMessage>(),
                    CheckCall = null
		        };

			var estes = new EstesDispatch(new CarrierAuth
				{
					Username = vcomm.CarrierIntegrationUsername,
					Password = vcomm.CarrierIntegrationPassword
				});
			var response = !string.IsNullOrEmpty(pickupNumber)
				               ? estes.UpdateDispatchedShipment(shipment, page.ActiveUser, pickupNumber)
				               : estes.DispatchShipment(shipment, page.ActiveUser);
			if (response.HasErrors)
            {
                dispatchResponse.Messages.AddRange(response.Errors.Select(e => new ValidationMessage { Message = e, Type = ValidateMessageType.Error }));
                ErrorLogger.LogError(new Exception(response.Errors.ToArray().NewLineJoin()),
				                     page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
            }

			if (!string.IsNullOrEmpty(response.PickupNumber))
                dispatchResponse.CheckCall = new CheckCall
					{
						Tenant = shipment.Tenant,
						CallDate = DateTime.Now,
						CallNotes = response.PickupNumber,
						EdiStatusCode = ShipStatMsgCode.AA.GetString(),
						EventDate = DateTime.Now,
						Shipment = shipment,
						User = page.ActiveUser
					};

            return dispatchResponse;
		}

		private static CheckCall CancelEstesDispatch(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
		{
			var estes = new EstesDispatch(new CarrierAuth
			{
				Username = vcomm.CarrierIntegrationUsername,
				Password = vcomm.CarrierIntegrationPassword
			});
			var response = estes.CancelDispatchedShipment(pickupNumber);
			var checkCall = new CheckCall
				{
					Tenant = shipment.Tenant,
					CallDate = DateTime.Now,
                    CallNotes = string.Format(PickupRequestCancelledWithMsg, response),
					EdiStatusCode = string.Empty,
					EventDate = DateTime.Now,
					Shipment = shipment,
					User = page.ActiveUser
				};
			return checkCall;
		}

        private static NotificationDispatchResponse DispatchWard(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
	    {
            var dispatchResponse = new NotificationDispatchResponse
            {
                Messages = new List<ValidationMessage>(),
                CheckCall = null
            };

			var ward = new WardDispatch(new CarrierAuth
			{
				Username = vcomm.CarrierIntegrationUsername,
				Password = vcomm.CarrierIntegrationPassword
			});
            var dispatchPickupNumber = string.Empty;
            try
	        {
                dispatchPickupNumber = !string.IsNullOrEmpty(pickupNumber)
					? ward.UpdateDispatchedShipment(shipment, page.ActiveUser, pickupNumber)
					: ward.DispatchShipment(shipment, page.ActiveUser);
	        }
            catch (Exception ex)
            {
				ErrorLogger.LogError(new Exception(ex.Message), ProcessorVars.ErrorFolder);
                dispatchResponse.Messages.Add(ValidationMessage.Error(ex.Message));
            }

			if (string.IsNullOrEmpty(dispatchPickupNumber) && ward.Messages.Any())
			{
                dispatchResponse.Messages.AddRange(ward.Messages.Select(e => new ValidationMessage { Message = e, Type = ValidateMessageType.Error }));
			    ErrorLogger.LogError(new Exception(ward.Messages.ToArray().NewLineJoin().ReplaceNewLineWithHtmlBreak()), ProcessorVars.ErrorFolder);
			}

            if (!String.IsNullOrEmpty(dispatchPickupNumber))
                dispatchResponse.CheckCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = dispatchPickupNumber,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return dispatchResponse;
        }


        private static NotificationDispatchResponse DispatchDayton(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            var response = new NotificationDispatchResponse
                {
                    Messages = new List<ValidationMessage>(),
                    CheckCall = null
                };

            var dayton = new DaytonDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });

            var dispatchPickupNumber = string.Empty;
            try
            {
                dispatchPickupNumber = !string.IsNullOrEmpty(pickupNumber)
                              ? dayton.UpdateDispatchedShipment(shipment, page.ActiveUser, pickupNumber)
                              : dayton.DispatchShipment(shipment, page.ActiveUser);
            }
            catch (WebException wex)
            {
                using (var data = wex.Response.GetResponseStream())
                    if (data != null)
                        using (var reader = new StreamReader(data))
                        {
                            var error = reader.ReadToEnd();

                            response.Messages.Add(ValidationMessage.Error(error));
                            ErrorLogger.LogError(new Exception(error),
                                                 page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
                        }

            }
            catch (Exception ex)
            {
                response.Messages.Add(ValidationMessage.Error(ex.Message));
                ErrorLogger.LogError(new Exception(ex.Message), page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
            }

            if (!string.IsNullOrEmpty(dispatchPickupNumber))
                response.CheckCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = dispatchPickupNumber,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };


            return response;
        }

        private static CheckCall CancelDaytonDispatch(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            CheckCall checkCall = null;
            var dayton = new DaytonDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var wasCancelled = dayton.CancelDispatchedShipment(pickupNumber);

            if (wasCancelled)
                checkCall = new CheckCall
                    {
                        Tenant = shipment.Tenant,
                        CallDate = DateTime.Now,
                        CallNotes = PickupRequestCancelled,
                        EdiStatusCode = string.Empty,
                        EventDate = DateTime.Now,
                        Shipment = shipment,
                        User = page.ActiveUser
                    };
            return checkCall;
        }

        private static NotificationDispatchResponse DispatchFedExEconomy(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            var dispatchResponse = new NotificationDispatchResponse
                {
                    Messages = new List<ValidationMessage>(),
                    CheckCall = null
                };

            var fedExEconomyDispatch = new FedExEconomyDispatch(new CarrierAuth
                {
                    Username = vcomm.CarrierIntegrationUsername,
                    Password = vcomm.CarrierIntegrationPassword
                });
            var response = !string.IsNullOrEmpty(pickupNumber)
                               ? fedExEconomyDispatch.UpdateDispatchedShipment(shipment, page.ActiveUser, pickupNumber)
                               : fedExEconomyDispatch.DispatchShipment(shipment, page.ActiveUser);

            if (response.HasErrors)
            {
                dispatchResponse.Messages.AddRange(response.Errors.Select(e => new ValidationMessage { Message = e, Type = ValidateMessageType.Error }));
                ErrorLogger.LogError(new Exception(response.Errors.ToArray().NewLineJoin()), page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
            }

            if (!string.IsNullOrEmpty(response.PickupNumber))
                dispatchResponse.CheckCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = response.PickupNumber,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return dispatchResponse;
        }

        private static CheckCall CancelFedExEconomyDispatch(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            CheckCall checkCall = null;
            var fedExEconomyDispatch = new FedExEconomyDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });

            var wasCancelled = fedExEconomyDispatch.CancelDispatchedShipment(pickupNumber);
            if (wasCancelled)
                checkCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = PickupRequestCancelled,
                    EdiStatusCode = string.Empty,
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return checkCall;
        }

        private static NotificationDispatchResponse DispatchFedExPriority(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            var dispatchResponse = new NotificationDispatchResponse
            {
                Messages = new List<ValidationMessage>(),
                CheckCall = null
            };

            var fedExPriorityDispatch = new FedExPriorityDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var response = !string.IsNullOrEmpty(pickupNumber)
                               ? fedExPriorityDispatch.UpdateDispatchedShipment(shipment, page.ActiveUser, pickupNumber)
                               : fedExPriorityDispatch.DispatchShipment(shipment, page.ActiveUser);

            if (response.HasErrors)
            {
                dispatchResponse.Messages.AddRange(response.Errors.Select(e => new ValidationMessage { Message = e, Type = ValidateMessageType.Error }));
                ErrorLogger.LogError(new Exception(response.Errors.ToArray().NewLineJoin()), page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
            }

            if (!string.IsNullOrEmpty(response.PickupNumber))
                dispatchResponse.CheckCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = response.PickupNumber,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return dispatchResponse;
        }

        private static CheckCall CancelFedExPriorityDispatch(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            CheckCall checkCall = null;
            var fedExPriorityDispatch = new FedExPriorityDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });

            var wasCancelled = fedExPriorityDispatch.CancelDispatchedShipment(pickupNumber);
            if (wasCancelled)
                checkCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = PickupRequestCancelled,
                    EdiStatusCode = string.Empty,
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return checkCall;
        }

        private static NotificationDispatchResponse DispatchHolland(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            var dispatchResponse = new NotificationDispatchResponse
            {
                Messages = new List<ValidationMessage>(),
                CheckCall = null
            };

            var hollandDispatch = new HollandDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var response = !string.IsNullOrEmpty(pickupNumber)
                               ? hollandDispatch.UpdateDispatchedShipment(shipment, page.ActiveUser, pickupNumber)
                               : hollandDispatch.DispatchShipment(shipment, page.ActiveUser);

            if (response.HasErrors)
            {
                dispatchResponse.Messages.AddRange(response.Errors.Select(e => new ValidationMessage { Message = e, Type = ValidateMessageType.Error }));
                ErrorLogger.LogError(new Exception(response.Errors.ToArray().NewLineJoin()), page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
            }

            if (!string.IsNullOrEmpty(response.PickupNumber))
                dispatchResponse.CheckCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = response.PickupNumber,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return dispatchResponse;
        }

        private static CheckCall CancelHollandDispatch(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            CheckCall checkCall = null;
            var fedExPriorityDispatch = new FedExPriorityDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });

            var wasCancelled = fedExPriorityDispatch.CancelDispatchedShipment(pickupNumber);
            if (wasCancelled)
                checkCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = PickupRequestCancelled,
                    EdiStatusCode = string.Empty,
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return checkCall;
        }

        private static NotificationDispatchResponse DispatchPittOhio(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            var dispatchResponse = new NotificationDispatchResponse
            {
                Messages = new List<ValidationMessage>(),
                CheckCall = null
            };

            var pittOhio = new PittOhioDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var response = !string.IsNullOrEmpty(pickupNumber)
                               ? pittOhio.UpdateDispatchedShipment(shipment, page.ActiveUser, pickupNumber)
                               : pittOhio.DispatchShipment(shipment, page.ActiveUser);
            if (response.HasErrors)
            {
                dispatchResponse.Messages.AddRange(response.Errors.Select(e => new ValidationMessage { Message = e, Type = ValidateMessageType.Error }));
                ErrorLogger.LogError(new Exception(response.Errors.ToArray().NewLineJoin()),
                                     page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
            }

            if (!string.IsNullOrEmpty(response.PickupNumber))
                dispatchResponse.CheckCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = response.PickupNumber,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return dispatchResponse;
        }

        private static CheckCall CancelPittOhioDispatch(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            CheckCall checkCall = null;
            var pittOhio = new PittOhioDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var wasCancelled = pittOhio.CancelDispatchedShipment(pickupNumber);
            if (wasCancelled)
                checkCall = new CheckCall
                    {
                        Tenant = shipment.Tenant,
                        CallDate = DateTime.Now,
                        CallNotes = PickupRequestCancelled,
                        EdiStatusCode = string.Empty,
                        EventDate = DateTime.Now,
                        Shipment = shipment,
                        User = page.ActiveUser
                    };
            return checkCall;
        }

        private static NotificationDispatchResponse DispatchSouthEasternFreight(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            var dispatchResponse = new NotificationDispatchResponse
            {
                Messages = new List<ValidationMessage>(),
                CheckCall = null
            };

            var southEasternFreightDispatch = new SouthEasternFreightDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var response = !string.IsNullOrEmpty(pickupNumber)
                               ? southEasternFreightDispatch.UpdateDispatchedShipment(shipment, page.ActiveUser, pickupNumber)
                               : southEasternFreightDispatch.DispatchShipment(shipment, page.ActiveUser);
            if (response.HasErrors)
            {
                dispatchResponse.Messages.AddRange(response.Errors.Select(e => new ValidationMessage { Message = e, Type = ValidateMessageType.Error }));
                ErrorLogger.LogError(new Exception(response.Errors.ToArray().NewLineJoin()),
                                     page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
            }

            if (!string.IsNullOrEmpty(response.PickupNumber))
                dispatchResponse.CheckCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = response.PickupNumber,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return dispatchResponse;
        }

        private static CheckCall CancelSouthEasternFreightDispatch(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            CheckCall checkCall = null;
            var southEasternFreightDispatch = new SouthEasternFreightDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var wasCancelled = southEasternFreightDispatch.CancelDispatchedShipment(pickupNumber);
            if (wasCancelled)
                checkCall = new CheckCall
                    {
                        Tenant = shipment.Tenant,
                        CallDate = DateTime.Now,
                        CallNotes = PickupRequestCancelled,
                        EdiStatusCode = string.Empty,
                        EventDate = DateTime.Now,
                        Shipment = shipment,
                        User = page.ActiveUser
                    };
            return checkCall;
        }

        private static NotificationDispatchResponse DispatchSouthwesternMotorTransport(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            var dispatchResponse = new NotificationDispatchResponse
            {
                Messages = new List<ValidationMessage>(),
                CheckCall = null
            };

            var southwesternMotorTransportDispatch = new SouthwesternMotorTransportDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var response = !string.IsNullOrEmpty(pickupNumber)
                               ? southwesternMotorTransportDispatch.UpdateDispatchedShipment(shipment, page.ActiveUser, pickupNumber)
                               : southwesternMotorTransportDispatch.DispatchShipment(shipment, page.ActiveUser);
            if (response.HasErrors)
            {
                dispatchResponse.Messages.AddRange(response.Errors.Select(e => new ValidationMessage { Message = e, Type = ValidateMessageType.Error }));
                ErrorLogger.LogError(new Exception(response.Errors.ToArray().NewLineJoin()),
                                     page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
            }

            if (!string.IsNullOrEmpty(response.PickupNumber))
                dispatchResponse.CheckCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = response.PickupNumber,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return dispatchResponse;
        }

        private static CheckCall CancelSouthwesternMotorTransportDispatch(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            CheckCall checkCall = null;
            var southwesternMotorTransportDispatch = new SouthwesternMotorTransportDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var wasCancelled = southwesternMotorTransportDispatch.CancelDispatchedShipment(pickupNumber);
            if (wasCancelled)
                checkCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = PickupRequestCancelled,
                    EdiStatusCode = string.Empty,
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return checkCall;
        }

        private static NotificationDispatchResponse DispatchUps(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            var dispatchResponse = new NotificationDispatchResponse
            {
                Messages = new List<ValidationMessage>(),
                CheckCall = null
            };

            var upsDispatch = new UpsDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var response = !string.IsNullOrEmpty(pickupNumber)
                               ? upsDispatch.UpdateDispatchedShipment(shipment, page.ActiveUser, pickupNumber)
                               : upsDispatch.DispatchShipment(shipment, page.ActiveUser);
            if (response.HasErrors)
            {
                dispatchResponse.Messages.AddRange(response.Errors.Select(e => new ValidationMessage { Message = e, Type = ValidateMessageType.Error }));
                ErrorLogger.LogError(new Exception(response.Errors.ToArray().NewLineJoin()),
                                     page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
            }

            if (!string.IsNullOrEmpty(response.PickupNumber))
                dispatchResponse.CheckCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = response.PickupNumber,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return dispatchResponse;
        }

        private static CheckCall CancelUpsDispatch(MemberPageBase page, Shipment shipment, VendorCommunication vcomm, string pickupNumber)
        {
            CheckCall checkCall = null;
            var upsDispatch = new UpsDispatch(new CarrierAuth
            {
                Username = vcomm.CarrierIntegrationUsername,
                Password = vcomm.CarrierIntegrationPassword
            });
            var wasCancelled = upsDispatch.CancelDispatchedShipment(pickupNumber);
            if (wasCancelled)
                checkCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = PickupRequestCancelled,
                    EdiStatusCode = string.Empty,
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };
            return checkCall;
        }


        private static NotificationDispatchResponse DispatchProject44(MemberPageBase page, Shipment shipment, VendorCommunication vcomm)
        {
            var response = new NotificationDispatchResponse
            {
                Messages = new List<ValidationMessage>(),
                CheckCall = null
            };

            var p44Settings = new Project44ServiceSettings
            {
                Hostname = WebApplicationSettings.Project44Host,
                Username = WebApplicationSettings.Project44Username,
                Password = WebApplicationSettings.Project44Password,
                PrimaryLocationId = WebApplicationSettings.Project44PrimaryLocationId
            };

            var project44Wrapper = new Project44Wrapper(p44Settings);

            var dispatchPickupNumber = string.Empty;
            try
            {
                var shipmentConfirmation = project44Wrapper.DispatchShipment(shipment, vcomm.Vendor.Scac);

                if(shipmentConfirmation != null)
                { 
                    // TODO: P44 states that when available carriers will return a pro number in this response
                    var pickupIdentifier  = shipmentConfirmation.ShipmentIdentifiers.FirstOrDefault(i => i.Type == LtlShipmentIdentifier.TypeEnum.PICKUP);

                    dispatchPickupNumber = pickupIdentifier != null ? pickupIdentifier.Value : string.Empty;
                }
            }
            catch (WebException wex)
            {
                using (var data = wex.Response.GetResponseStream())
                    if (data != null)
                        using (var reader = new StreamReader(data))
                        {
                            var error = reader.ReadToEnd();

                            response.Messages.Add(ValidationMessage.Error(error));
                            ErrorLogger.LogError(new Exception(error),
                                                 page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
                        }

            }
            catch (Exception ex)
            {
                response.Messages.Add(ValidationMessage.Error(ex.Message));
                ErrorLogger.LogError(new Exception(ex.Message), page.Server.MapPath(WebApplicationSettings.ErrorsFolder));
            }

            if (!string.IsNullOrEmpty(dispatchPickupNumber))
                response.CheckCall = new CheckCall
                {
                    Tenant = shipment.Tenant,
                    CallDate = DateTime.Now,
                    CallNotes = dispatchPickupNumber,
                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
                    EventDate = DateTime.Now,
                    Shipment = shipment,
                    User = page.ActiveUser
                };

            if(project44Wrapper.ErrorMessages.Any())
                response.Messages.AddRange(project44Wrapper.ErrorMessages.Select(m => ValidationMessage.Error(m)));

            return response;
        }

	    private static CheckCall CancelProject44Dispatch(MemberPageBase page, Shipment shipment, string pickupNumber)
	    {
	        CheckCall checkCall = null;
	        var p44Settings = new Project44ServiceSettings
	        {
	            Hostname = WebApplicationSettings.Project44Host,
	            Username = WebApplicationSettings.Project44Username,
	            Password = WebApplicationSettings.Project44Password,
	            PrimaryLocationId = WebApplicationSettings.Project44PrimaryLocationId
	        };

            var project44Wrapper = new Project44Wrapper(p44Settings);

            var cancelResponse = project44Wrapper.CancelDispatchedShipment(shipment, pickupNumber);
	        if (cancelResponse != null && cancelResponse.InfoMessages.All(m => m.Severity == Message.SeverityEnum.INFO))
	            checkCall = new CheckCall
	            {
	                Tenant = shipment.Tenant,
	                CallDate = DateTime.Now,
	                CallNotes = PickupRequestCancelled,
	                EdiStatusCode = string.Empty,
	                EventDate = DateTime.Now,
	                Shipment = shipment,
	                User = page.ActiveUser
	            };

	        return checkCall;
	    }



        public static void NotifyDeveloperAccessRequest(this MemberPageBase page, DeveloperAccessRequest request)
		{
			var body = page.GenerateDeveloperAccessRequestNotification(request, false);
			var emails = ProcessorVars.DeveloperAccessRequestSupportEmails[page.ActiveUser.TenantId].ToList();

			if (emails.Any())
			{
				// monitor emails
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

				var subject = string.Format("{0} Developer Access Request - {1}", request.Tenant.AutoNotificationSubjectPrefix, request.Customer.Name);
                new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
			}
		}

		public static void NotifyDeveloperAccessRequestDevelopmentApproval(this MemberPageBase page, DeveloperAccessRequest request, bool production, string name)
		{
			var body = production
					? page.GenerateProductionAccessRequestApprovalNotification(request, name)
					: page.GenerateDevelopmentAccessRequestApprovalNotification(request, name);

			var emails = request.ContactEmail.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
				.ToList();

			if (!emails.Any()) return;

			// monitor emails
			emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

			emails.AddRange(ProcessorVars.DeveloperAccessRequestSupportEmails[page.ActiveUser.TenantId]);
			var subject = string.Format("{0} {1} Access Request Approval - {2}", request.Tenant.AutoNotificationSubjectPrefix,  production ? "Production" : "Development", request.Customer.Name);
            new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
		}



		public static void NotifyDeveloperAccessRequestUpdated(this MemberPageBase page, DeveloperAccessRequest request, string message)
		{
			var body = page.GenerateDeveloperAccessRequestUpdateNotification(request, message);
			var emails = request.ContactEmail.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
				.ToList();
			if (!emails.Any()) return;

			// monitor emails
			emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

			emails.AddRange(ProcessorVars.DeveloperAccessRequestSupportEmails[page.ActiveUser.TenantId]);
			var subject = string.Format("{0} Developer Access Request Update - {1}", request.Tenant.AutoNotificationSubjectPrefix, request.Customer.Name);
            new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
		}

		public static void NotifyDeveloperAccessSupportRequest(this MemberPageBase page, DeveloperAccessRequest request, string message, string category)
		{
			var body = page.GenerateDeveloperAccessSupportRequestNotification(request, message, category);
			var emails = request.ContactEmail.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
				.ToList();

			if (!emails.Any()) return;

			// monitor emails
			emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

			emails.AddRange(ProcessorVars.DeveloperAccessRequestSupportEmails[page.ActiveUser.TenantId]);
			var subject = string.Format("{0} Developer Access Support - {1} {2}",request.Tenant.AutoNotificationSubjectPrefix,  request.Customer.Name, category);
            new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
		}

		public static void NotifyProductionAccessRequest(this MemberPageBase page, DeveloperAccessRequest request)
		{
			var body = page.GenerateDeveloperAccessRequestNotification(request, true);
			var emails = new List<string> { request.ContactEmail };

			if (!emails.Any()) return;

			// monitor emails
			emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

			emails.AddRange(ProcessorVars.DeveloperAccessRequestSupportEmails[page.ActiveUser.TenantId]);
			var subject = string.Format("{0} Production Access Request - {1}",request.Tenant.AutoNotificationSubjectPrefix,  request.Customer.Name);
            new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
		}



		public static void NotifyReadyReport(this HttpServerUtility server, FileInfo info, string name, ReportSchedule schedule)
		{
			string body;
			Attachment attachment = null;
			if (info.Length > WebApplicationSettings.MaxReportAttachmentSize) // attachment to big
			{
				body = server.GenerateReportReadyNotification(name, false);
			}
			else
			{
				body = server.GenerateReportReadyNotification(name, true);
				attachment = new Attachment(info.FullName);
			}
			var emails = schedule.NotifyEmails
				.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
				.Distinct()
				.ToList();

			if (emails.Any())
			{
				var subject = string.Format("{0} TMS Ready Report - {1}", schedule.Tenant.AutoNotificationSubjectPrefix, name);
				var attachments = attachment == null ? new Attachment[0] : new[] { attachment };
                emails.QueueEmail(new string[0], ProcessorVars.MonitorEmails[schedule.TenantId], body, subject, attachments, schedule.TenantId); // monitor emails Bcc'ed.
			}

		}




		public static void NotifyIncomingLoadTenderEvent(this HttpServerUtility server, XmlConnect xc, Customer customer, List<EdiDiff> knownUpdatesToRecord = null)
		{
			// compile emails
			var seperators = WebApplicationUtilities.Seperators();
			var emails = customer.Tier.ApplicableEmails().Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList();

			if (emails.Any())
			{
				// body
				var edi204 = xc.Xml.FromXml<Edi204>();
				var tender = edi204.Loads.First(l => l.BeginningSegment.ShipmentIdNumber == xc.ShipmentIdNumber);

				var body = server.GenerateLoadTender(tender, customer, xc.Direction, true, knownUpdatesToRecord);

				var load = xc.Xml.FromXml<Edi204>().Loads
							 .FirstOrDefault(l => l.BeginningSegment.ShipmentIdNumber == xc.ShipmentIdNumber);
				var status = load == null ? string.Empty : load.SetPurpose.TransactionSetPurpose.GetDescription();
				var title = string.Format("eShipPlus TMS {0} {1} {2} Load Tender", WebApplicationSettings.Version, status, xc.ShipmentIdNumber);
				var attachment = new Attachment(new MemoryStream(body.ToPdf(title)), string.Format("{0}.pdf", title));

				emails.AddRange(ProcessorVars.MonitorEmails[xc.TenantId]);
		
				var subject = string.Format("{0}: {1} Load Tender (Incoming) Submission  - {2}", status, xc.Direction, xc.ShipmentIdNumber);
                new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, new[] { attachment }, xc.TenantId);
			}
		}

		public static void NotifyOutgoingLoadTenderResponseEvent(this HttpServerUtility server, XmlConnect xc, LoadOrder load)
		{
			// compile emails
			var emails = new List<string> { load.CarrierCoordinator == null ? string.Empty : load.CarrierCoordinator.Email };

			if (emails.Any())
			{
				// body
				var edi990 = xc.Xml.FromXml<Edi990>();
				var response = edi990.Responses.First(l => l.BeginningSegment.ShipmentIdentificationNumber == xc.ShipmentIdNumber);
				var vendor = load.Vendors.FirstOrDefault(v => v.Vendor.VendorNumber == xc.VendorNumber && v.Primary);
				var body = server.GenerateLoadTenderResponseNotification(response, vendor == null ? null : vendor.Vendor, true);

				var status = xc.Status.GetString();
				var title = string.Format("eShipPlus TMS {0} {1} {2} Load Tender Response", WebApplicationSettings.Version, status, xc.ShipmentIdNumber);
				var attachment = new Attachment(new MemoryStream(body.ToPdf(title)), string.Format("{0}.pdf", title));

				emails.AddRange(ProcessorVars.MonitorEmails[xc.TenantId]);
				
				var subject = string.Format("{0} Load Tender Response Submission from {1}  - {2}", status, vendor == null ? string.Empty : vendor.Vendor.Name, xc.ShipmentIdNumber);
				new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, new[] { attachment }, xc.TenantId);
			}
		}


		public static void NotifyOutgoingLoadTenderResponseEmail(this HttpServerUtility server, LoadOrder load,
		                                                         User activeUser)
		{
			// compile emails
			var emails = new List<string> {load.CarrierCoordinator == null ? string.Empty : load.CarrierCoordinator.Email};

			if (emails.Any())
			{
				// body
				
				var vendor = load.Vendors.FirstOrDefault(v => v.Primary);
				var body = server.GenerateLoadTenderResponseEmail(load, vendor == null ? null : vendor.Vendor, true);

				var title = string.Format("eShipPlus TMS {0} {1} {2} Load Tender Response", WebApplicationSettings.Version, XmlConnectStatus.Accepted,
				                          load.LoadOrderNumber);
				var attachment = new Attachment(new MemoryStream(body.ToPdf(title)), string.Format("{0}.pdf", title));

				emails.AddRange(ProcessorVars.MonitorEmails[activeUser.TenantId]);

				var subject = string.Format("{0} Load Tender Response Submission from {1}  - {2}", XmlConnectStatus.Accepted,
				                            vendor == null ? string.Empty : vendor.Vendor.Name, load.LoadOrderNumber);
				new[] {WebApplicationSettings.DoNotReplyEmail}.QueueEmail(new string[0], emails, body, subject, new[] {attachment},
				                                                          activeUser.TenantId);
			}
		}

		public static void NotifyLoadTenderByEmail(this HttpServerUtility server, Load loadTender, Customer customer,
		                                           User activeUser, Vendor vendor,string emailPurpose = "", List<EdiDiff> knownUpdatesToRecord = null)
		{
			// compile emails
			var seperators = WebApplicationUtilities.Seperators();
			var notificationEmail = vendor.Communication != null
				                        ? vendor.Communication.Notifications
				                                .Where(n => n.Milestone == Milestone.TruckloadBidNotification && n.Enabled)
				                                .Select(n => n.Email).ToList()
				                        : new List<string>();
			var emails = new List<string>();
			foreach (var e in notificationEmail)
			{
				emails.AddRange(e.Split(seperators, StringSplitOptions.RemoveEmptyEntries)
				                 .Distinct()
				                 .ToList());
			}

			if (emails.Any())
			{
				// body
				var purpose = string.Empty;
				if (emailPurpose == SetPurposeTransType.OR.ToString()) purpose = "New";
				if (emailPurpose == SetPurposeTransType.UP.ToString()) purpose = "Update";
				if (emailPurpose == SetPurposeTransType.CA.ToString()) purpose = "Cancellation";
				var tender = loadTender.BeginningSegment ?? new BeginningSegmentShipmentInfo();

				var body = server.GenerateLoadTender(loadTender, customer, Direction.Outbound, true, knownUpdatesToRecord, true, vendor.Id.ToString());

				var title = string.Format("eShipPlus TMS {0} {1} {2} Load Tender", WebApplicationSettings.Version, string.Empty,
				                          tender.ShipmentIdNumber);
				var attachment = new Attachment(new MemoryStream(body.ToPdf(title)), string.Format("{0}.pdf", title));

				emails.AddRange(ProcessorVars.MonitorEmails[activeUser.TenantId]);

				var subject = string.Format("{0}: {1} Load Tender (Incoming) Submission  - {2}",purpose, Direction.Outbound,
				                            tender.ShipmentIdNumber);
				new[] {WebApplicationSettings.DoNotReplyEmail}.QueueEmail(new string[0], emails, body, subject, new[] {attachment},
				                                                          activeUser.TenantId);
			}
		}



		public static void SendLoadTenderResponse(this HttpServerUtility server, Edi990 edi990, CustomerCommunication ccom,
												  User processingUser = null)
		{
			var xmlTransHandler = new XmlTransmissionProcessHandler();

			var xc = new XmlConnect();
			xc.SetEmpty();
			xc.TenantId = ccom.TenantId;
			xc.Direction = Direction.Outbound;
			xc.DocumentType = EdiDocumentType.EDI990;
			xc.Status = XmlConnectStatus.Pending;
			xc.StatusMessage = string.Empty;
			xc.DateCreated = DateTime.Now;
			xc.CustomerNumber = ccom.Customer.CustomerNumber;
			xc.ShipmentIdNumber = edi990.Responses[0].BeginningSegment.ShipmentIdentificationNumber;
			xc.Xml = edi990.ToXml();
			xc.ControlNumber = edi990.TransactionSetHeader.TransactionSetControlNumber;

			Exception ex;
			xmlTransHandler.SaveTransmission(xc, processingUser ?? xc.Tenant.DefaultSystemUser, out ex);
			if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);

			xc.TakeSnapShot();
			var content = server.GenerateEdiMessage(ccom, xc.Xml, EdiDocumentType.EDI990.GetDocTypeString(), xc.ControlNumber).ToUtf8Bytes();
			var saveAs = string.Format("{0}.{1}", edi990.TransactionSetHeader.TransactionSetControlNumber,
									   ReportExportExtension.xml);
			xc.TransmissionOkay = ccom.FtpEnabled
									  ? ccom.SendFtp(saveAs, content, EdiDocumentType.EDI990)
									  : ccom.SendEdi(saveAs, content, EdiDocumentType.EDI990);


			xmlTransHandler.SaveTransmission(xc, processingUser ?? xc.Tenant.DefaultSystemUser, out ex);
			if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
		}

		public static void SendLoadTender(this MemberPageBase page, Edi204 edi204, VendorCommunication vcom, string truckLoadBidExpirationTime = "")
		{
			var xmlTransHandler = new XmlTransmissionProcessHandler();
			Exception ex;
			var ediXml = edi204.ToXml();

			var shipmentIdNumber = edi204.Loads[0].BeginningSegment.ShipmentIdNumber;
			var stops = edi204.Loads[0].Stops.OrderBy(s => s.StopOffDetails.StopSequenceNumber).ToList();
			var origin = stops[0];
			var dest = stops[stops.Count - 1];

			// go to database (for original) just as a safety check.  If record already present, use that record
			var dto = new XmlConnectSearch().FetchOutBoundLoadTenderByVendShipIdNumAndDocType(vcom.Vendor.VendorNumber, shipmentIdNumber, vcom.TenantId, page.ActiveUser.Id);
			var xc = (dto == null ? null : dto.TenderRecord) ?? new XmlConnect();
			if (!xc.IsNew) xc.TakeSnapShot();
			else
			{
				xc.SetEmpty();
				xc.TenantId = vcom.TenantId;
				xc.Direction = Direction.Outbound;
				xc.DocumentType = EdiDocumentType.EDI204;
				xc.DateCreated = DateTime.Now;
				xc.VendorNumber = vcom.Vendor.VendorNumber;
				xc.VendorScac = vcom.Vendor.Scac;
			    xc.VendorPro = string.Empty;
				xc.ShipmentIdNumber = shipmentIdNumber;
			}
			xc.ControlNumber = edi204.TransactionSetHeader.TransactionSetControlNumber;

			xc.ExpirationDate = truckLoadBidExpirationTime != string.Empty
				                    ? truckLoadBidExpirationTime.ToDateTime()
				                    : DateTime.Now.AddMinutes(vcom.LoadTenderExpAllowance); 
			xc.EquipmentDescriptionCode = edi204.Loads[0].EquipmentDetail == null
				                              ? string.Empty
				                              : edi204.Loads[0].EquipmentDetail.EquipmentDescriptionCode;
			xc.DestinationCity = dest.GeographicLocation.CityName;
			xc.DestinationCountryCode = dest.GeographicLocation.CountryCode;
			xc.DestinationCountryName = string.Empty;
			xc.DestinationPostalCode = dest.GeographicLocation.PostalCode;
			xc.DestinationState = dest.GeographicLocation.StateOrProvinceCode;
			xc.DestinationStreet1 = dest.LocationStreetInformation.AddressInfo1;
			xc.DestinationStreet2 = dest.LocationStreetInformation.AddressInfo2;
			xc.OriginCity = origin.GeographicLocation.CityName;
			xc.OriginCountryCode = origin.GeographicLocation.CountryCode;
			xc.OriginCountryName = string.Empty;
			xc.OriginPostalCode = origin.GeographicLocation.PostalCode;
			xc.OriginState = origin.GeographicLocation.StateOrProvinceCode;
			xc.OriginStreet1 = origin.LocationStreetInformation.AddressInfo1;
			xc.OriginStreet2 = origin.LocationStreetInformation.AddressInfo2;
			xc.PurchaseOrderNumber = (edi204.Loads[0].LoadReferenceNumbers.FirstOrDefault(
				r => r.ReferenceIdentificationQualifier == RefIdQualifier.PO) ??
			                          new ReferenceNumber()).ReferenceIdentification ?? string.Empty;
			xc.ShipperReference = (edi204.Loads[0].LoadReferenceNumbers.FirstOrDefault(
				r => r.ReferenceIdentificationQualifier == RefIdQualifier.SHR) ??
			                       new ReferenceNumber()).ReferenceIdentification ?? string.Empty;
			xc.TotalPackages = edi204.Loads.Sum(l => l.ShipmentWeightAndTrackingData.LadingPackages);
			xc.TotalPieces = edi204.Loads.Sum(l => l.ShipmentWeightAndTrackingData.LadingPieces);
			xc.TotalStops = edi204.Loads.SelectMany(l => l.Stops).Count();
			xc.TotalWeight = edi204.Loads.Sum(l => l.ShipmentWeightAndTrackingData.Weight.ToDecimal());
			xc.Xml = ediXml;
			xc.Status = XmlConnectStatus.Pending;
			xc.StatusMessage = string.Empty;
			xc.FtpTransmission = vcom.FtpEnabled;


			xmlTransHandler.SaveTransmission(xc, page.ActiveUser, out ex);
			if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);

			var content = page.Server.GenerateEdiMessage(vcom, ediXml, EdiDocumentType.EDI204.GetDocTypeString(), xc.ControlNumber).ToUtf8Bytes();
			var saveAs = string.Format("{0}.{1}", edi204.TransactionSetHeader.TransactionSetControlNumber, ReportExportExtension.xml);

			var ediTranmissionOkay = false;
			var ftpTransmissionOkay = false;

			if (vcom.EdiEnabled) ediTranmissionOkay = vcom.SendEdi(saveAs, content, EdiDocumentType.EDI204);
			if (vcom.FtpEnabled) ftpTransmissionOkay = vcom.SendFtp(saveAs, content, EdiDocumentType.EDI204);

			xc.TakeSnapShot();
			xc.TransmissionOkay = ediTranmissionOkay || ftpTransmissionOkay;
			xmlTransHandler.SaveTransmission(xc, page.ActiveUser, out ex);
			if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
		}




		public static void NotifyXmlTransmissionError(this HttpServerUtility server, Exception ex, string transmissionType, long tenantId)
		{
			// file name
			var name = string.Format("{0:yyyyMMdd_hhmmss_ffff}_eShipPlus_TMS_Xml_Trans_Error", DateTime.Now);

			var body = server.GenerateXmlTransmissionErrorNotification(transmissionType, ex.Message);
			var bytes = body.ToPdf(name);
			Attachment attachment = null;
			if (bytes.Length <= WebApplicationSettings.MaxReportAttachmentSize) // ensure attachment not too big
				attachment = new Attachment(new MemoryStream(bytes), string.Format("{0}.pdf", name));

			var emails = ProcessorVars.TenantAdminNotificationEmails.ContainsKey(tenantId)
							? ProcessorVars.TenantAdminNotificationEmails[tenantId].Distinct().ToList()
							: new List<string>();

			if (emails.Any())
			{
				// monitor emails
				emails.AddRange(ProcessorVars.MonitorEmails[tenantId]);

				var attachments = attachment == null ? new Attachment[0] : new[] { attachment };
                emails.QueueEmail(new string[0], new string[0], body, name, attachments, tenantId);
			}
		}



		public static void NotifyPostedInvoice(this MemberPageBase page, Invoice invoice)
		{
			var customer = invoice.CustomerLocation.Customer;
			var communication = customer == null ? null : customer.Communication;
			if (communication == null) return;

			var cm = communication.Notifications.Where(n => n.Milestone == Milestone.InvoicePosted && n.Enabled).ToList();
			if (!cm.Any()) return;

			var toEmailAddresses = new[] { WebApplicationSettings.DoNotReplyEmail };

			// compile emails
			var seperators = WebApplicationUtilities.Seperators();
			var emails = new List<string>();

			foreach (var email in cm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Email).Select(m => m.Email))
				emails.AddRange(email.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Distinct());
			emails = emails.Distinct().ToList();

			if (emails.Any())
			{
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

				// body
				var body = page.GeneratePostedInvoiceNotification(invoice);
				var title = string.Format("eShipPlus TMS {0} {1} Posted Invoice", WebApplicationSettings.Version,
										  invoice.InvoiceNumber);
				var invoiceHtml = page.GenerateInvoice(invoice, true);
				var attachment = new Attachment(new MemoryStream(invoiceHtml.ToPdf(title)), string.Format("{0}.pdf", title));

				var attachments = new List<Attachment> { attachment };
				var shipments = invoice.RetrieveAssociatedShipments();
                if (communication.EmailDocDeliveryEnabled && shipments.Count() == 1)
                {
                    var tags = communication.DocDeliveryDocTags.Select(d => d.DocumentTag.Code).ToList();
                    var shipment = shipments.First();
                    foreach (var document in shipment.Documents.Where(i => !i.IsInternal && i.DocumentTagId != default(long) && tags.Contains(i.DocumentTag.Code)))
                    {
                        var fileName = page.Server.MapPath(document.LocationPath);
                        if (!File.Exists(fileName)) continue;
                        attachments.Add(new Attachment(new MemoryStream(page.Server.ReadFromFile(document.LocationPath)),
                                                       string.Format("{0}_{1}{2}", document.DocumentTag.Code, document.Name, new FileInfo(fileName).Extension)));
                    }

                    if (communication.DeliverBolDoc)
                    {
                        title = string.Format("eShipPlus TMS {0} {1} Shipment Bill of Lading", WebApplicationSettings.Version,
                                              shipment.ShipmentNumber);
                        attachments.Add(new Attachment(new MemoryStream(page.GenerateBOL(shipment, true).ToPdf(title)),
                                                       string.Format("{0}.pdf", title)));
                    }

                    if (communication.DeliverShipmentStatementDoc)
                    {
                        var auditdtos = new AuditInvoiceDto().FetchAuditInvoiceDtos(shipment.ShipmentNumber, DetailReferenceType.Shipment,
                                                                                    shipment.TenantId);
                        title = string.Format("eShipPlus TMS {0} {1} Shipment Statement", WebApplicationSettings.Version,
                                              shipment.ShipmentNumber);
                        attachments.Add(
                            new Attachment(new MemoryStream(page.GenerateShipmentStatement(shipment, auditdtos, true).ToPdf(title)),
                                           string.Format("{0}.pdf", title)));
                    }
                }

				var subject = string.Format("{0} Posted Invoice  - {1}", invoice.Tenant.AutoNotificationSubjectPrefix,
				                            invoice.InvoiceNumber);
                toEmailAddresses.QueueEmail(new string[0], emails, body, subject, attachments, page.ActiveUser.TenantId);
			}

			// EDI
			if (communication.EdiEnabled && cm.Any(n => n.NotificationMethod == NotificationMethod.Edi))
				ProcessXmlTransmission(page.Server, invoice, NotificationMethod.Edi, communication, page.ActiveUser);

			// FTP
			if (communication.FtpEnabled && cm.Any(n => n.NotificationMethod == NotificationMethod.Ftp))
				ProcessXmlTransmission(page.Server, invoice, NotificationMethod.Ftp, communication, page.ActiveUser);

		}


		public static void SendSupportEmail(this MemberControlBase control, EmailMessage message, string category)
		{
			// body
			var body = control.GenerateSupportEmail(message);

			// compile emails
			var emails = new List<string>();


			emails.AddRange(control.ActiveUser.DefaultCustomer.Tier.TierSupportEmails
								.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries));

			if (!emails.Any())
				emails.AddRange(ProcessorVars.SpecialistSupportEmails[control.ActiveUser.TenantId]); //added only if tier emails doesnt exist;

			if (!string.IsNullOrEmpty(control.ActiveUser.Email)) emails.Add(control.ActiveUser.Email); // user email always added

			// monitor emails
			emails.AddRange(ProcessorVars.MonitorEmails[control.ActiveUser.TenantId]);

			var subject = string.Format("{0} Customer Support Request {1} - {2}",
			                            control.ActiveUser.Tenant.AutoNotificationSubjectPrefix,
			                            control.ActiveUser.DefaultCustomer.Name, category);
            new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails.Distinct(), body, subject, control.ActiveUser.TenantId);
		}

		public static void SendSupportEmail(this MemberPageBase page, EmailMessage message, string category)
		{
			// body
			var body = page.GenerateSupportEmail(message);

			// compile emails
			var emails = new List<string>();


			emails.AddRange(page.ActiveUser.DefaultCustomer.Tier.TierSupportEmails
								.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries));

			if (!emails.Any())
				emails.AddRange(ProcessorVars.SpecialistSupportEmails[page.ActiveUser.TenantId]); //added only if tier emails doesnt exist;

			if (!string.IsNullOrEmpty(page.ActiveUser.Email)) emails.Add(page.ActiveUser.Email); // user email always added

			// monitor emails
			emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

			var subject = string.Format("{0} Customer Support Request {1} - {2}",
			                            page.ActiveUser.Tenant.AutoNotificationSubjectPrefix,
			                            page.ActiveUser.DefaultCustomer.Name, category);
            new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails.Distinct(), body, subject, page.ActiveUser.TenantId);
		}


	    public static void NotifyNewPendingVendor(this MemberPageBase page, PendingVendor pendingVendor)
	    {
            var body = page.GenerateNewPendingVendorNotification(pendingVendor);
	        var emails = page.ActiveUser.DefaultCustomer.Tier
	                         .PendingVendorNotificationEmails
	                         .Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries)
	                         .Distinct()
	                         .ToList();
            if (emails.Any())
            {
				// monitor emails
				emails.AddRange(ProcessorVars.MonitorEmails[page.ActiveUser.TenantId]);

	            var subject = string.Format("{0} New Pending Vendor Notification - {1}",
	                                        page.ActiveUser.Tenant.AutoNotificationSubjectPrefix,
	                                        page.ActiveUser.DefaultCustomer.Name);
                new[] { WebApplicationSettings.DoNotReplyEmail }.QueueEmail(new string[0], emails, body, subject, page.ActiveUser.TenantId);
            }
	    }


		private static void ProcessFax(this MemberPageBase page, IEnumerable<VendorNotification> vm, Shipment shipment,
		                               string bolHtml, string title = "" , bool isVoidShipment = false)
		{
			var originTerminalFax = shipment.OriginTerminal == null ? string.Empty : shipment.OriginTerminal.Fax;
			var used = false;
			var handler = new ShipmentFaxTransmissionHandler();
			foreach (var milestone in vm.Where(m => m.Enabled && m.NotificationMethod == NotificationMethod.Fax))
			{
				var fax = string.IsNullOrEmpty(originTerminalFax) ? milestone.Fax : originTerminalFax;
				if (string.IsNullOrEmpty(fax)) continue;
				if (fax == originTerminalFax && used) continue;
				if (fax == originTerminalFax) used = true;

				fax = fax.ToArray().Where(char.IsDigit).Aggregate(string.Empty, (current, ch) => current + ch);
				if (fax.ToArray().All(ch => ch == '0')) continue;
				fax = "1" + fax; // must have 1 to dial long distance!

				// setup fax transmission to save
				var faxTransmission = new FaxTransmission
					{
						Message = (isVoidShipment ? ProcessorVars.VoidShipmentFaxTransMsg : ProcessorVars.NewShipmentFaxTransMsg) + " Fax #: " + fax,
						ResolutionComment = string.Empty,
						Resolved = false,
						ResponseDateTime = DateUtility.SystemEarliestDateTime,
						LastStatusCheckDateTime = DateUtility.SystemEarliestDateTime,
						SendDateTime = DateTime.Now,
						ShipmentNumber = shipment.ShipmentNumber,
						TenantId = shipment.TenantId,
						Status = FaxTransmissionStatus.Pending,
						TransmissionKey = Guid.NewGuid(),
						ExternalReference = string.Empty,
                        FaxWrapper = string.Empty,
						ServiceProvider = WebApplicationSettings.FaxServiceProvider,
					};

				// interfax or ring central fax option
				if (WebApplicationSettings.FaxServiceEnabled)
				{
                    if (WebApplicationSettings.FaxServiceModeIsTest) fax = WebApplicationSettings.FaxServiceTestNumber;

					var faxWrapper = new FaxWrapper
						{
							CoverPage = new CoverPage {Text = string.Empty, Type = CoverPageType.None},
							RecipientFaxNumbers = new List<string>{fax},
							Resolution = Resolution.High,
							SendTime = string.Empty,
							Attachments = new List<FaxDocument>
								{
									new FaxDocument
										{
											Name = shipment.ShipmentNumber + ".pdf",
											Content = bolHtml.ToPdf(title)
										}
								}
						};
				    faxTransmission.FaxWrapper = faxWrapper.ToXml();
                    faxTransmission.Status = FaxTransmissionStatus.Queued;
				}

				// legacy relay fax option
				if (!WebApplicationSettings.FaxServiceEnabled && !string.IsNullOrEmpty(WebApplicationSettings.FaxServiceEmail))
				{
					var subject = string.Format("{0}, eShip PLUS {1} Shipment Notification: {2}", fax, isVoidShipment ? "Cancelled" : string.Empty, faxTransmission.TransmissionKey);
					new[] {WebApplicationSettings.FaxServiceEmail}.QueueEmail(new string[0], ProcessorVars.MonitorEmails[page.ActiveUser.TenantId], bolHtml, subject, page.ActiveUser.TenantId);
				}


				// save fax transmission
				var errs = handler.SaveFaxTransmission(faxTransmission);
				if (errs.Any())
					ErrorLogger.LogError(new Exception(errs.Select(em => em.Message).ToArray().NewLineJoin()), HttpContext.Current);

			}
		}




		private static void ProcessXmlTransmission(HttpServerUtility server, Shipment shipment, NotificationMethod method, CustomerCommunication ccom, List<CheckCall> checkCalls, User processingUser)
		{
			var xmlTransHandler = new XmlTransmissionProcessHandler();
			var controlNumber = new AutoNumberProcessor().NextNumber(AutoNumberCode.EdiTransactionSetControlNumber, processingUser).GetString();
			var edi214 = shipment.ToEdi214(checkCalls, controlNumber);

			var xc = new XmlConnect
					{
						TenantId = shipment.TenantId,
						ControlNumber = controlNumber,
						Status = XmlConnectStatus.Pending,
						ShipmentIdNumber = edi214.Statuses[0].BeginningSegment.ShipmentIdentificationNumber,
						Direction = Direction.Outbound,
						ExpirationDate = DateUtility.SystemEarliestDateTime,
						EquipmentDescriptionCode = string.Empty,
						TotalPieces = edi214.Statuses[0].ShipmentWghtPkgAndQtyData.LadingPieces,
						TotalStops = shipment.Stops.Count + 2,
						TotalWeight = edi214.Statuses[0].ShipmentWghtPkgAndQtyData.Weight,
						CustomerNumber = ccom.Customer.CustomerNumber,
						DateCreated = DateTime.Now,
						DestinationCity = shipment.Destination.City,
						DestinationCountryCode = shipment.Destination.Country.Code,
						DestinationCountryName = shipment.Destination.Country.Name,
						DestinationPostalCode = shipment.Destination.PostalCode,
						DestinationState = shipment.Destination.State,
						DestinationStreet1 = shipment.Destination.Street1,
						DestinationStreet2 = shipment.Destination.Street2,
						DocumentType = EdiDocumentType.EDI214,
						PurchaseOrderNumber = shipment.PurchaseOrderNumber,
						ShipperReference = shipment.ShipperReference,
						StatusMessage = string.Empty,
						TotalPackages = edi214.Statuses[0].ShipmentWghtPkgAndQtyData.LadingPackages,
						VendorNumber = string.Empty,
                        VendorPro = string.Empty,
						VendorScac = string.Empty,
						Xml = edi214.ToXml(),
						OriginCity = shipment.Origin.City,
						OriginCountryCode = shipment.Origin.Country.Code,
						OriginCountryName = shipment.Origin.Country.Name,
						OriginPostalCode = shipment.Origin.PostalCode,
						OriginState = shipment.Origin.State,
						OriginStreet1 = shipment.Origin.Street1,
						OriginStreet2 = shipment.Origin.Street2,
						ReceiptDate = DateUtility.SystemEarliestDateTime,
						FtpTransmission = method == NotificationMethod.Ftp,
						TransmissionOkay = false,
					};

			Exception ex;
			xmlTransHandler.SaveTransmission(xc, processingUser, out ex);
			if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
			xc.TakeSnapShot();
			var content = server.GenerateEdiMessage(ccom, xc.Xml, EdiDocumentType.EDI214.GetDocTypeString(), xc.ControlNumber).ToUtf8Bytes();
			var saveAs = string.Format("{0}.{1}", edi214.TransactionSetHeader.TransactionSetControlNumber, ReportExportExtension.xml);
			switch (method)
			{
				case NotificationMethod.Edi:
					xc.TransmissionOkay = ccom.SendEdi(saveAs, content, xc.DocumentType);
					break;
				case NotificationMethod.Ftp:
					xc.TransmissionOkay = ccom.SendFtp(saveAs, content, xc.DocumentType);
					break;
				default:
					xc.TransmissionOkay = false;
					break;
			}

			xmlTransHandler.SaveTransmission(xc, processingUser, out ex);
			if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
		}

		private static void ProcessXmlTransmission(HttpServerUtility server, Invoice invoice, NotificationMethod method, CustomerCommunication ccom, User processingUser)
		{
			var xmlTransHandler = new XmlTransmissionProcessHandler();

			var controlNumber = new AutoNumberProcessor().NextNumber(AutoNumberCode.EdiTransactionSetControlNumber, processingUser).GetString();
			var edi210 = invoice.ToEdi210(controlNumber);
			var shipment = invoice.Details.GroupBy(d => d.ReferenceNumber, d => d).Select(g => g.Key).Count() == 1 &&
						   invoice.Details.First().ReferenceType == DetailReferenceType.Shipment
							   ? new ShipmentSearch().FetchShipmentByShipmentNumber(invoice.Details.First().ReferenceNumber, invoice.TenantId)
							   : null;

			var xc = new XmlConnect
				{
					TenantId = invoice.TenantId,
					ControlNumber = controlNumber,
					Status = XmlConnectStatus.Pending,
					ShipmentIdNumber = edi210.Invoices[0].BeginningSegment.InvoiceNumber,
					Direction = Direction.Outbound,
					ExpirationDate = DateUtility.SystemEarliestDateTime,
					EquipmentDescriptionCode = shipment != null && shipment.Equipments.Any() ? shipment.Equipments[0].EquipmentType.Code : string.Empty,
					TotalPieces = edi210.Invoices.SelectMany(i => i.FreightInvoiceDetails).Sum(d => d.LadingPieces),
					TotalStops = edi210.Invoices.SelectMany(i => i.Stops).Count(),
					TotalWeight = edi210.Invoices.SelectMany(i => i.FreightInvoiceDetails).Sum(d => d.Weight),
					CustomerNumber = ccom.Customer.CustomerNumber,
					DateCreated = DateTime.Now,
					DestinationCity = shipment == null ? string.Empty : shipment.Destination.City,
					DestinationCountryCode = shipment == null ? string.Empty : shipment.Destination.Country.Code,
					DestinationCountryName = shipment == null ? string.Empty : shipment.Destination.Country.Name,
					DestinationPostalCode = shipment == null ? string.Empty : shipment.Destination.PostalCode,
					DestinationState = shipment == null ? string.Empty : shipment.Destination.State,
					DestinationStreet1 = shipment == null ? string.Empty : shipment.Destination.Street1,
					DestinationStreet2 = shipment == null ? string.Empty : shipment.Destination.Street2,
					DocumentType = EdiDocumentType.EDI210,
					PurchaseOrderNumber = shipment == null ? string.Empty : shipment.PurchaseOrderNumber,
					ShipperReference = shipment == null ? string.Empty : shipment.ShipperReference,
					StatusMessage = string.Empty,
					TotalPackages = edi210.Invoices.SelectMany(i => i.FreightInvoiceDetails).Sum(d => d.LadingQuantity),
					VendorNumber = string.Empty,
                    VendorPro = string.Empty,
					VendorScac = string.Empty,
					Xml = edi210.ToXml(),
					OriginCity = shipment == null ? string.Empty : shipment.Origin.City,
					OriginCountryCode = shipment == null ? string.Empty : shipment.Origin.Country.Code,
					OriginCountryName = shipment == null ? string.Empty : shipment.Origin.Country.Name,
					OriginPostalCode = shipment == null ? string.Empty : shipment.Origin.PostalCode,
					OriginState = shipment == null ? string.Empty : shipment.Origin.State,
					OriginStreet1 = shipment == null ? string.Empty : shipment.Origin.Street1,
					OriginStreet2 = shipment == null ? string.Empty : shipment.Origin.Street2,
					ReceiptDate = DateUtility.SystemEarliestDateTime,
					FtpTransmission = method == NotificationMethod.Ftp,
					TransmissionOkay = false,
				};

			Exception ex;
			xmlTransHandler.SaveTransmission(xc, processingUser, out ex);
			if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
			xc.TakeSnapShot();
			var content = server.GenerateEdiMessage(ccom, xc.Xml, EdiDocumentType.EDI210.GetDocTypeString(), xc.ControlNumber).ToUtf8Bytes();
			var saveAs = string.Format("{0}.{1}", edi210.TransactionSetHeader.TransactionSetControlNumber, ReportExportExtension.xml);
			switch (method)
			{
				case NotificationMethod.Edi:
					xc.TransmissionOkay = ccom.SendEdi(saveAs, content, xc.DocumentType);
					break;
				case NotificationMethod.Ftp:
					xc.TransmissionOkay = ccom.SendFtp(saveAs, content, xc.DocumentType);
					break;
				default:
					xc.TransmissionOkay = false;
					break;
			}

			xmlTransHandler.SaveTransmission(xc, processingUser, out ex);
			if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
		}
	}
}
