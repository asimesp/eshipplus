﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.Utilities.Que;
using System;
using System.IO;
using System.Linq;
using System.Net;
using LogisticsPlus.Eship.Project44.StatusUpdates;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.p44
{
    public partial class StatusUpdate : Project44TrackingProxyPageBase
    {
	    public override void QueTrackingDataAsCheckCalls(Project44AsyncTrackingResponse trackingData)
	    {
		    try
		    {
		        var trackSearch = new Project44TrackResponseSearch();
		        var dispatchSearch = new Project44DispatchResponseSearch();

			    var record = trackSearch.FetchTrackResponseByShipmentId(trackingData.latestShipmentStatus.shipment.id.ToLong());
		        Shipment shipment;

                // If no matching tracking response found, see if it was dipatched (dispatch auto-initiates tracking, so we don't manually initiate tracking through Rate and Schedule / Web Services)
                if (record == null)
		        {
		            var dispatchRecord = dispatchSearch.FetchDispatchResponseByShipmentId(trackingData.latestShipmentStatus.shipment.id.ToLong());

		            if (dispatchRecord == null)
		            {
		                ErrorLogger.LogError(new Exception(string.Format("Tracking data transaction record not found! [{0}]",
		                    JsonConvert.SerializeObject(trackingData))), Context);
		                return;
		            }

		            shipment = dispatchRecord.Shipment;
                }
                else
                {
                    shipment = record.Shipment;
                }

			    if (shipment == null)
			    {
				    ErrorLogger.LogError(new Exception(string.Format("Tracking data not associated with a shipment! [{0}]",
				                                                     JsonConvert.SerializeObject(trackingData))), Context);
				    return;
			    }

		        var number =
		        (trackingData.latestShipmentStatus.shipment.shipmentIdentifiers.FirstOrDefault(i =>
		             i.type == Project44TrackingIdentifierType.BillOfLading) ?? new Project44AsyncShipmentIdentifier()).value.StripNonDigits();

			    if (shipment.ShipmentNumber != number || shipment.Vendors.First(v => v.Primary).Vendor.Scac != trackingData.latestShipmentStatus.shipment.capacityProviderAccountGroup.accounts.First().code)
			    {
				    ErrorLogger.LogError(
					    new Exception(string.Format("Tracking data information keys do not match shipment keys [{0}]",
					                                JsonConvert.SerializeObject(trackingData))), Context);
				    return;
			    }

		        // TODO: Ask Ajka about Stop Type and Stop Status. Stop Type is in  trackingData.latestShipmentStatus.latestStatusUpdate, but appears to always be blank. Stop Status is nowhere to be found.
		        var matchingEdiCode = ConvertProject44StatusToEdiStatus(
		            trackingData.latestShipmentStatus.latestStatusUpdate.statusCode,
		            trackingData.latestShipmentStatus.latestStatusUpdate.statusReason.code, string.Empty, string.Empty);


		        var checkCallDescription = string.Empty;

                // Figure out what to use for checkcall notes
		        if (trackingData.latestShipmentStatus.latestStatusUpdate.statusCode == Project44TrackingStatusCode.AtStop || trackingData.latestShipmentStatus.latestStatusUpdate.statusCode == Project44TrackingStatusCode.OutToStop)
		        {
                    // TODO: StopType doesn't ever seem to be populated (docs even say when status is AT_STOP it will "always" be populated)
                    var stopLocation = trackingData.latestShipmentStatus.latestStatusUpdate.stopType == Project44TrackingStopType.Origin
                                       ? trackingData.latestShipmentStatus.shipment.shipmentStops.First(s => s.stopType == Project44TrackingStopType.Origin)
                                       : trackingData.latestShipmentStatus.latestStatusUpdate.stopType == Project44TrackingStopType.Terminal
                                         ? trackingData.latestShipmentStatus.shipment.shipmentStops.First(s => s.stopType == Project44TrackingStopType.Terminal)
                                         : trackingData.latestShipmentStatus.shipment.shipmentStops.First(s => s.stopType == Project44TrackingStopType.Destination);

		            checkCallDescription = string.Format("{0} Stop Location: {1}",
		                trackingData.latestShipmentStatus.latestStatusUpdate.statusReason.description,
		                string.Format("{0} {1}, {2} {3}", stopLocation.location.address.addressLines.ToArray().SpaceJoin(),
		                    stopLocation.location.address.city, stopLocation.location.address.state,
		                    stopLocation.location.address.postalCode));
		        }
		        else
		        {
		            checkCallDescription = trackingData.latestShipmentStatus.latestStatusUpdate.statusReason.description;
		        }


                var checkCall = new CheckCall
				    {
					    CallNotes = checkCallDescription,
					    CallDate = DateTime.Now,
					    Shipment = shipment,
					    TenantId = shipment.TenantId,
					    User = shipment.Tenant.DefaultSystemUser,
						EdiStatusCode = matchingEdiCode,
						EventDate = trackingData.latestShipmentStatus.latestStatusUpdate.retrievalDateTime.ToLocalTime()
                };


                // gather image references
		        var shipmentDocuments = new List<ProcQueObj>();

                foreach (var image in trackingData.latestShipmentStatus.imageReferences)
		        {
		            byte[] imageBytes;
                    var fileName = Path.GetFileName(image.imageUrl);

		            if (shipment.Documents.Select(d => Path.GetFileName(Server.MapPath(d.LocationPath))).Contains(fileName))
		            {
                        // we've already downloaded this file
                        continue;
		            }

                    using (var webClient = new WebClient())
		            {
		                imageBytes = webClient.DownloadData(image.imageUrl);
		            }

                    if(imageBytes == null)
                        continue;

		            var virtualPath = WebApplicationSettings.ShipmentFolder(shipment.TenantId, shipment.Id);
		            var physicalPath = Server.MapPath(virtualPath);

		            // ensure directory is present
		            if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

		            //ensure unique filename
		            var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fileName), true));

                    // save file
		            imageBytes.WriteToFile(physicalPath, fi.Name, String.Empty);

                    var document = new ShipmentDocument
		            {
		                Shipment = shipment,
		                TenantId = shipment.TenantId,
		                DocumentTagId = default(long),
		                Description = image.documentType,
		                Name = image.documentType,
		                FtpDelivered = false,
		                IsInternal = image.documentType == Project44TrackingDocumentType.Invoice || image.documentType == Project44TrackingDocumentType.WeightCertificate,
		                VendorBillId = default(long),
                        LocationPath = virtualPath + fi.Name
                    };

		            shipmentDocuments.Add(new ProcQueObj
		            {
		                ObjType = ProcQueObjType.ShipmentDocumentQue,
		                Xml = new ShipmentDocumentQue(document).ToXml(),
		                DateCreated = DateTime.Now
		            });
		        }


                var proQue = new ShipmentStatusUpdateQuePro(
		            trackingData.latestShipmentStatus.shipment.capacityProviderAccountGroup.accounts.First().code,
		            (trackingData.latestShipmentStatus.shipment.shipmentIdentifiers.FirstOrDefault(i => i.type == Project44TrackingIdentifierType.Pro) ?? new Project44AsyncShipmentIdentifier()).value);
			    var proc = new ProcQueObj
				    {
					    ObjType = ProcQueObjType.ShipmentStatusQue,
					    Xml = new ShipmentStatusUpdateQue(checkCall, new[] {proQue}.ToList()).ToXml(),
					    DateCreated = DateTime.Now
				    };

			    Exception ex;

			    new ProcQueObjProcessHandler().Save(proc, out ex);
			    if (ex != null) ErrorLogger.LogError(ex, Context);

		        ex = null;
                // Save shipment Documents
		        foreach (var shipmentDocument in shipmentDocuments)
		        {
		            new ProcQueObjProcessHandler().Save(shipmentDocument, out ex);
		            if (ex != null) ErrorLogger.LogError(ex, Context);
		            ex = null;
                }
            }
		    catch (Exception e)
		    {
			    ErrorLogger.LogError(e, Context);
		    }
	    }

	    public override void SaveP44TrackingInformation(Project44AsyncTrackingResponse data)
		{
			try
			{
				var proc = new Project44TrackingDataProcessor();
				Exception ex;
				var messages = proc.SaveTrackingData(data, out ex);

                if(ex != null) ErrorLogger.LogError(ex, Context);
				if (messages.Any()) ErrorLogger.LogError(new Exception(messages.Select(m => m.Message).ToArray().NewLineJoin()), Context);
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
			}
		}
        

	    public override void LogErrors(List<string> errors)
	    {
		    ErrorLogger.LogError(new Exception(errors.ToArray().NewLineJoin()), Context);
	    }

	    protected void Page_Load(object sender, EventArgs e)
		{
            try
            {
                ProcessTrackingResponse();
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
            }
		}

        // TODO: AA, A3, A7, A9, AG, AI, AJ, AM, AP (AH mapping?), AR, AV, B6, BA, BC, C1, CD, CL, CP, I1, J1, K1, L1, OA, OO, PR, R1, RL, S1, X1, X2, X3, X4, X5, X6, X8, XB
        private string ConvertProject44StatusToEdiStatus(string p44StatusCode, string p44StatusCodeReason, string p44StopType, string p44StopTypeStatus)
        {
            if (p44StatusCode == Project44TrackingStatusCode.Info && p44StatusCodeReason == Project44TrackingStatusCodeReason.DeliveryAppointment)
            {
                return ShipStatMsgCode.AB.ToString();
            }

            if (p44StatusCode == Project44TrackingStatusCode.InTransit && p44StopType == Project44TrackingStopType.Origin && p44StopTypeStatus == Project44TrackingStopTypeStatus.Departed)
            {
                return ShipStatMsgCode.AF.ToString();
            }

            if (p44StatusCode == Project44TrackingStatusCode.Info && p44StatusCodeReason == Project44TrackingStatusCodeReason.DeliveryMissed)
            {
                return ShipStatMsgCode.AH.ToString();
            }

            if (p44StatusCode == Project44TrackingStatusCode.Completed && p44StatusCodeReason == Project44TrackingStatusCodeReason.Canceled)
            {
                return ShipStatMsgCode.CA.ToString();
            }

            if (p44StatusCode == Project44TrackingStatusCode.Completed && p44StatusCodeReason == Project44TrackingStatusCodeReason.Delivered && p44StopTypeStatus == Project44TrackingStopTypeStatus.Departed)
            {
                return ShipStatMsgCode.D1.ToString();
            }

            if (p44StatusCode == Project44TrackingStatusCode.InTransit && p44StopType == Project44TrackingStopType.Terminal && p44StopTypeStatus == Project44TrackingStopTypeStatus.Departed)
            {
                return ShipStatMsgCode.P1.ToString();
            }

            if (p44StatusCode == Project44TrackingStatusCode.Info && p44StatusCodeReason == Project44TrackingStatusCodeReason.Delayed)
            {
                return ShipStatMsgCode.SD.ToString();
            }

            return string.Empty;
        }
    }
}