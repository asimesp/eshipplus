﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Project44.StatusUpdates;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.p44
{
	public partial class P44Test : Project44TrackingProxyPageBase
    {
		public override void QueTrackingDataAsCheckCalls(Project44AsyncTrackingResponse data)
		{
			throw new NotImplementedException();
		}

		public override void SaveP44TrackingInformation(Project44AsyncTrackingResponse data)
		{
			throw new NotImplementedException();
		}

		public override void LogErrors(List<string> errors)
		{
			ErrorLogger.LogError(new Exception(errors.ToArray().NewLineJoin()), Context);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			litMsg.Text = string.Empty;
		}

		protected void OnSubmitTrackingClicked(object sender, EventArgs e)
		{
			try
			{
			    var p44Settings = new Project44ServiceSettings
			    {
			        Hostname = WebApplicationSettings.Project44Host,
			        Username = WebApplicationSettings.Project44Username,
			        Password = WebApplicationSettings.Project44Password,
			        PrimaryLocationId = WebApplicationSettings.Project44PrimaryLocationId
			    };

			    var shipment = new Shipment
			    {
			        Prefix = new Prefix(1) {Code = txtPrefix.Text},
			        ShipmentNumber = txtBol.Text,
                    DesiredPickupDate = txtDesiredPickupDate.Text.ToDateTime(),
                    EstimatedDeliveryDate = txtDesiredDeliveryDate.Text.ToDateTime(),
			        Origin = new ShipmentLocation
			        {
                        Description = txtOriginName.Text,
			            Street1 = txtOriginStreet1.Text,
			            Street2 = txtOriginStreet2.Text,
			            City = txtOriginCity.Text,
			            State = txtOriginState.Text,
			            PostalCode = txtOriginPostalCode.Text,
			            Contacts = new List<ShipmentContact>
			            {
			                new ShipmentContact
			                {
			                    Primary = true,
			                    Name = txtOriginContactName.Text,
			                    Phone = txtOriginContactPhone.Text,
			                    Email = txtOriginContactEmail.Text,
			                    Fax = txtOriginContactFax.Text
			                }
			            }
			        },
			        Destination = new ShipmentLocation
			        {
			            Description = txtDestinationName.Text,
                        Street1 = txtDestinationStreet1.Text,
			            Street2 = txtDestinationStreet2.Text,
			            City = txtDestinationCity.Text,
			            State = txtDestinationState.Text,
			            PostalCode = txtDestinationPostalCode.Text,
			            Contacts = new List<ShipmentContact>
			            {
			                new ShipmentContact
			                {
			                    Primary = true,
			                    Name = txtDestinationContactName.Text,
			                    Phone = txtDestinationContactPhone.Text,
			                    Email = txtDestinationContactEmail.Text,
			                    Fax = txtDestinationContactFax.Text
			                }
			            }
			        }
                };

			    var p44Wrapper = new Project44Wrapper(p44Settings);
			    var trackResponse = p44Wrapper.InitiateTracking(shipment, txtScac.Text);

			    if (trackResponse == null)
			    {
			        litMsg.Text = string.Format(@"
                                        JSON Response:
                                        {0}", JsonConvert.SerializeObject(p44Wrapper.ErrorMessages));
			        return;
			    }

			    var trackRequestResponse = new Project44TrackingResponse
			    {
			        Project44Id = trackResponse.Shipment.Id.GetString(),
			        ShipmentId = txtShipmentId.Text.ToLong(),
                    UserId = default(long),
			        ResponseData = JsonConvert.SerializeObject(trackResponse),
			        Scac = txtScac.Text,
			        DateCreated = DateTime.Now,
			    };

                var body = JsonConvert.SerializeObject(trackResponse);

				new[] {"kyle.grygo@logisticsplus.net"}.SendEmail(body, "P44 Test Tracking Request", default(long));

			    var validator = new Project44ResponseValidator();

			    var isValid = false;
                if (validator.IsValid(trackRequestResponse))
                {
                    trackRequestResponse.Save();
                    isValid = true;
                }

			    litMsg.Text = isValid
                                  ? string.Format(@"
                                        JSON Response:
                                        {0}",  JsonConvert.SerializeObject(trackResponse))
                                  : string.Format(
                                      @"
                                        JSON Response:
                                        {0}

                                        Errors occurred while saving to database:

                                        {1}",  JsonConvert.SerializeObject(trackResponse), validator.Messages.Select(m => m.Message).ToArray().NewLineJoin());
			}
			catch (Exception ex)
			{
				litMsg.Text = ex.ToString();
			}
		}
	}
}