﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogisticsPlus.Eship.PCMiler25Plugin;
using LogisticsPlus.Eship.PluginBase;
using LogisticsPlus.Eship.PluginBase.Mileage;

namespace LogisticsPlus.BatchMiles
{
	class Program
	{
		static void Main(string[] args)
		{
			var infile = args.Length > 0 ? args[0] :  "input.txt";
			var outFile = args.Length > 1 ? args[1] : "output.txt";

			using (var engine = new PcMiler25())
			using(var inStream = new FileStream(infile, FileMode.Open))
			using(var reader = new StreamReader(inStream))
			using(var outStream = new FileStream(outFile, FileMode.Create))
			using (var writer = new StreamWriter(outStream))
			{
				try
				{
					if (!engine.Initialize())
					{
						writer.WriteLine(engine.LastErrorMessage);
						return;
					}
				}
				catch (Exception ex)
				{
					writer.WriteLine(ex.ToString());
					return;
				}

				var line = reader.ReadLine(); // header
				line = line + "\tMiles\tMessage";
				writer.WriteLine(line);

				const string outLineFormat = "{0}\t{1}\t{2}";
				while (!string.IsNullOrEmpty(line = reader.ReadLine()))
					try
					{
						var parts = line.Split(new[] {'\t'});
						
						if (parts.Length < 4)
						{
							writer.WriteLine(outLineFormat, line, PluginBaseConstants.ErrorMilesValue,
							                 "Incorrect line construct.  Should have zip country zip country");
							continue;
						}
						var origin = new Point {Type = PointType.Address, PostalCode = parts[0], Country = parts[1]};
						var dest = new Point {Type = PointType.Address, PostalCode = parts[2], Country = parts[3]};
						var miles = engine.GetMiles(origin, dest);
						writer.WriteLine(outLineFormat, line, miles,
						                 (int) miles == (int) PluginBaseConstants.ErrorMilesValue ? engine.LastErrorMessage : string.Empty);
					}
					catch (Exception ex)
					{
						writer.WriteLine(outLineFormat, line, PluginBaseConstants.ErrorMilesValue, ex.Message);
					}
			}
		}
	}
}
