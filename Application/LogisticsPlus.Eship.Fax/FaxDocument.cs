﻿using System;

namespace LogisticsPlus.Eship.Fax
{
	[Serializable]
	public class FaxDocument
	{
		/// <summary>
		/// Document name
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Document content
		/// </summary>
		public byte[] Content { get; set; }
	}
}
