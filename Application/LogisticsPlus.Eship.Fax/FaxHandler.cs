﻿using System;
using System.Reflection;
using System.Linq;
using RingCentral.SDK;
using RingCentral.SDK.Helper;
using RingCentral.SDK.Http;
using LogisticsPlus.Eship.Core.Connect;

namespace LogisticsPlus.Eship.Fax
{
	public class FaxHandler
	{
		private const string ApplicationName = "eShipPlus";
		private const string FaxMessageUrl = "/restapi/v1.0/account/~/extension/~/fax";
		private const string FaxMessageStoreUrl = "/restapi/v1.0/account/~/extension/~/message-store";
		private const string SuccessfulMessage = "Successful";

		private const string JsonToField = "to";
		private const string JsonPhoneNumberField = "phoneNumber";
		private const string JsonMessageStatusField = "messageStatus";

		private readonly FaxSettings _settings;

		private readonly object _lockObj = new object();


		private SDK RingCentralApi { get; set; }

		private Interfax.InterFax InterfaxApi { get; set; }

		public string LastMessage { get; private set; }


		public FaxHandler(FaxSettings settings)
		{
			_settings = settings;
			LastMessage = string.Empty;

			var version = Assembly.GetExecutingAssembly().GetName().Version;

			RingCentralApi = new SDK(settings.RingCentralAppKey,
				settings.RingCentralAppSecret,
				settings.RingCentralBaseUrl,
				ApplicationName,
				string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision));

			InterfaxApi = new Interfax.InterFax();
		}


		public string Send(FaxWrapper faxWrapper, ServiceProvider serviceProvider)
		{
			if (faxWrapper == null)
			{
				LastMessage = "Fax Wrapper cannot be null";
				return string.Empty;
			}

			var result = string.Empty;


			switch (serviceProvider)
			{
				case ServiceProvider.RingCentral:
					lock (_lockObj)
					{
						try
						{
							if (!RingCentralApi.GetPlatform().IsAuthorized())
								RingCentralApi.GetPlatform().Authorize(_settings.RingCentralUser, _settings.RingCentralUserExtension, _settings.RingCentralUserPassword, true);
						}
						catch (Exception ex)
						{
							LastMessage = ex.Message;
							return string.Empty;
						}
					}

					var faxOptionsJson = faxWrapper.RecipientFaxNumbers.Aggregate("{\"to\":[", (current, recipient) => current + ("{\"phoneNumber\":\"" + recipient + "\"},"));

					// remove last comma
					faxOptionsJson = faxOptionsJson.Substring(0, faxOptionsJson.Length - 1);
					faxOptionsJson += string.Format("],\"faxResolution\":\"{0}\",\"coverIndex\":\"{1}\",\"coverPageText\":\"{2}\"", faxWrapper.Resolution.ToString(), faxWrapper.CoverPage.Type.ToInt(), faxWrapper.CoverPage.Text);
					if (!string.IsNullOrEmpty(faxWrapper.SendTime)) faxOptionsJson += string.Format(",\"sendTime\":\"{0}\"", faxWrapper.SendTime);
					faxOptionsJson += "}";

					try
					{
						var faxRequest = new Request(FaxMessageUrl, faxOptionsJson, faxWrapper.Attachments.Select(a => new Attachment(a.Name, a.GetContentType(), a.Content)).ToList());
						var faxResponse = RingCentralApi.GetPlatform().Post(faxRequest);
						LastMessage = faxResponse.CheckStatus() ? SuccessfulMessage : faxResponse.GetError();
						result = faxResponse.GetJson()["id"].ToString();
					}
					catch (Exception ex)
					{
						LastMessage = ex.Message;
						result = string.Empty;
					}
					break;

				case ServiceProvider.Interfax:
					try
					{
						const int retries = 3;
						const string csid = "LOGISTICSPLUS";
						const string header = "N";

						var recipientList = faxWrapper.RecipientFaxNumbers.Aggregate((a, b) => a + ";" + b);
						var fileExtension = faxWrapper.Attachments[0].Name;
						fileExtension = fileExtension.Substring(fileExtension.IndexOf(".", StringComparison.Ordinal));
						fileExtension = fileExtension.Trim('.');

						var faxResult = InterfaxApi
							.SendfaxEx_2(_settings.InterfaxUser, _settings.InterfaxUserPassword, recipientList, string.Empty,
																	faxWrapper.Attachments[0].Content, fileExtension,
																	faxWrapper.Attachments[0].Content.Length.ToString(),
																	DateTime.Now,
																	retries,
																	csid,
																	header,
																	string.Empty,
																	string.Empty,
																	string.Empty,
																	string.Empty,
																	string.Empty,
																	faxWrapper.Resolution == Resolution.High,
																	true);
						//By default the page orientation is Portrait 

						LastMessage = faxResult >= 0 ? SuccessfulMessage : string.Format("Interfax Error, Code: {0}", faxResult);
						result = faxResult.ToString();
					}
					catch (Exception ex)
					{
						LastMessage = ex.Message;
						result = string.Empty;
					}
					break;

			}
			return result;
		}

		public FaxStatus RetrieveFaxStatus(out Exception ex, string faxId, ServiceProvider serviceProvider)
		{
			if (string.IsNullOrEmpty(faxId))
			{
				ex = new Exception("Cannot retrieve fax status without fax identifier");
				return null;
			}

			var result = new FaxStatus();

			switch (serviceProvider)
			{
				case ServiceProvider.RingCentral:
					lock (_lockObj)
					{
						try
						{
							if (!RingCentralApi.GetPlatform().IsAuthorized())
								RingCentralApi.GetPlatform().Authorize(_settings.RingCentralUser, _settings.RingCentralUserExtension, _settings.RingCentralUserPassword, true);
						}
						catch (Exception exception)
						{
							ex = exception;
							return null;
						}
					}

					try
					{
						var messageRequest = new Request(string.Format("{0}/{1}", FaxMessageStoreUrl, faxId));
						var messageResponse = RingCentralApi.GetPlatform().Get(messageRequest);

						var jsonFaxStatusRecord = messageResponse.GetJson();
						if (jsonFaxStatusRecord[JsonToField] == null || jsonFaxStatusRecord[JsonToField][0] == null || jsonFaxStatusRecord[JsonToField][0][JsonPhoneNumberField] == null
							|| jsonFaxStatusRecord[JsonMessageStatusField] == null)
						{
							ex = new Exception("Could not locate status for fax identifier");
							return null;
						}
						result = new FaxStatus()
							{
								FaxNumber = jsonFaxStatusRecord[JsonToField][0][JsonPhoneNumberField].ToString(),
								SendResult = jsonFaxStatusRecord[JsonMessageStatusField].ToString().ToEnum<FaxTransmissionResult>()
							};
					}
					catch (Exception exception)
					{
						ex = exception;
						return null;
					}
					break;

				case ServiceProvider.Interfax:
					try
					{
						var queryForm = new Interfax.QueryForm
							{
								TransactionID = new Interfax.QueryCondition { Verb = Interfax.SQLOperatorEnum.Equals, VerbData = faxId }
							};

						var queryControl = new Interfax.QueryControl
							{
								OnlyParents = false,
								NumOfResults = 1,
								StartingRecord = 0,
								OrderBy = Interfax.OrderByColumnEnum.TransactionID,
								AscOrderDirection = false,
								ReturnItems = true,
								ReturnStats = false
							};

						var queryResult = InterfaxApi.FaxQuery2(_settings.InterfaxUser, _settings.InterfaxUserPassword, queryForm, queryControl);

						if (queryResult.ResultCode != 0)
						{
							ex = new Exception("Could not locate status for fax identifier");
							return null;
						}

						if (queryResult.FaxItems[0].Status < 0)
						{
							result = new FaxStatus
							{
								FaxNumber = queryResult.FaxItems[0].DestinationFax,
								SendResult = FaxTransmissionResult.Queued
							};
						}
						else if (queryResult.FaxItems[0].Status > 0)
						{
							result = new FaxStatus
							{
								FaxNumber = queryResult.FaxItems[0].DestinationFax,
								SendResult = FaxTransmissionResult.SendingFailed
							};
						}
						else if (queryResult.FaxItems[0].Status == 0)
						{
							result = new FaxStatus
							{
								FaxNumber = queryResult.FaxItems[0].DestinationFax,
								SendResult = FaxTransmissionResult.Sent
							};
						}

					}
					catch (Exception exception)
					{
						ex = exception;
						return null;
					}
					break;
			}
			ex = null;
			return result;
		}
	}
}
