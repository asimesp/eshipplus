﻿using System;

namespace LogisticsPlus.Eship.Fax
{
	internal static class FaxUtilities
	{
		internal static int ToInt(this object value)
		{
			var x = default(int);
			return value == null
					? x
					: value.GetType().IsEnum
						? (int)value
						: Int32.TryParse(value.ToString(), out x) ? x : default(int);
		}

        internal static string GetContentType(this FaxDocument attachment)
        {
	        var ext = attachment.Name.LastIndexOf(".", StringComparison.Ordinal) == -1
		                  ? string.Empty
		                  : attachment.Name.Substring(attachment.Name.LastIndexOf(".", StringComparison.Ordinal)).Replace(".", string.Empty).Trim().ToLower();
            switch (ext)
            {
                case "htm":
                case "html":
                    return "text/html";
                case "pdf":
                    return "application/pdf";
                case "txt":
                    return "text/plain";
				case "xml":
					return "text/xml";
				case "jpg":
				case "jpeg":
				case "png":
				case "gif":
				case "bmp":
		            return string.Format("image/{0}", ext);
                default:
                    return string.Empty;
            }
        }


		internal static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
	}
}
