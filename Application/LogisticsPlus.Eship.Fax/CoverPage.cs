﻿using System;

namespace LogisticsPlus.Eship.Fax
{
	[Serializable]
	public class CoverPage
	{
		/// <summary>
		/// Cover page type
		/// </summary>
		public CoverPageType Type { get; set; }

		/// <summary>
		/// Text that should be printed on cover page.  If Coverpage option is none, this is ignored
		/// </summary>
		public string Text { get; set; }
	}
}
