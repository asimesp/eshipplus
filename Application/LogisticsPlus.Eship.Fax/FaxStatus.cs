﻿namespace LogisticsPlus.Eship.Fax
{
    public class FaxStatus
    {
        public string FaxNumber{ get; set; }
        public FaxTransmissionResult SendResult { get; set; }
    }
}
