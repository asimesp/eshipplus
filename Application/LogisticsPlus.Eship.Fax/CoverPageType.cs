﻿namespace LogisticsPlus.Eship.Fax

{
	public enum CoverPageType
	{
        //Values are explicit to ensure they match the values for the RingCentral API
		None = 0,
		Ancient,
        Birthday,
        Blank,
        ClasMod,
        Classic = 5,
        Confidential,
        Contempo,
        Elegant,
        Express,
        Formal = 10,
        Jazzy,
        Modern,
        Urgent
	}
}
