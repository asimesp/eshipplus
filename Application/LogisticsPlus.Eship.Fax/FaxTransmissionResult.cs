﻿namespace LogisticsPlus.Eship.Fax
{
    // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFFAXTRANSMISSIONRESULT
    public enum FaxTransmissionResult
    {
        Queued = 0,
        Sent,
        SendingFailed
    }
}
