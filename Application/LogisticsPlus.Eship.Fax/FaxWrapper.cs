﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Fax
{
	[Serializable]
	public class FaxWrapper
	{
		/// <summary>
		/// Fax recipients
		/// </summary>
		public List<string> RecipientFaxNumbers { get; set; }

		/// <summary>
		/// Cover page option
		/// </summary>
		public CoverPage CoverPage { get; set; }

		/// <summary>
		/// Represents resolution in which fax will be sent to recipients
		/// </summary>
		public Resolution Resolution { get; set; }

		/// <summary>
		/// Schedule Time. GMT time in format dd:mm:yy hh:mm If parameter is not specified or invalid, current time is used
		/// </summary>
		public string SendTime { get; set; }

		/// <summary>
		/// Document to be faxed. 
		/// </summary>
		public List<FaxDocument> Attachments { get; set; }
	}
}
