﻿namespace LogisticsPlus.Eship.Fax
{
	public enum Resolution
	{
        //Values are explicit to ensure they match the values for the RingCentral API 
        High = 1,
		Low = 2,
	}
}
