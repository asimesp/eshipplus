﻿namespace LogisticsPlus.Eship.Fax
{
	public class FaxSettings
	{
		public string RingCentralUser { get; set; }
		public string RingCentralUserPassword { get; set; }
		public string RingCentralUserExtension { get; set; }
		public string RingCentralAppKey { get; set; }
		public string RingCentralAppSecret { get; set; }
		public string RingCentralBaseUrl { get; set; }
		public string InterfaxUser { get; set; }
		public string InterfaxUserPassword { get; set; }
	}
}
