﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Core
{

	[Serializable]
	[Entity("EmailLogsViewSearchDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class EmailLogViewSearchDto : ObjectBase
	{

			[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
			public long Id { get; private set; }

			[Property("From", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
			public string From { get; set; }

			[Property("Tos",AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
			public string Tos { get; set; }

			[Property("Ccs",AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
			public string Ccs { get; set; }

			[Property("Bbcs",AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
			public string Bbcs { get; set; }
			
			[Property("Subject",AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
			public string Subject { get; set; }

			[Property("DateCreated",AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
			public DateTime DateCreated { get; set; }
		
			[Property("Sent",AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
			public bool Sent { get; set; }


			public bool IsNew
			{
				get { return Id == default(long); }
			}

			public EmailLogViewSearchDto(long id)
			{
				Id = id;
				if (Id == default(long)) return;
				KeyLoaded = Load();
			}

			public EmailLogViewSearchDto(DbDataReader reader)
			{
				Load(reader);
			}

			public EmailLogViewSearchDto()
			{

			}
	}
}
