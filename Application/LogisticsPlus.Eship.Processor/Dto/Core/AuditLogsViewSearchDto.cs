﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Core
{
    [Serializable]
	[Entity("AuditLogsViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "AuditLogsViewSearch")]
	public class AuditLogsViewSearchDto : EntityBase
	{
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

		[Property("EntityCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EntityCode { get; set; }

		[Property("EntityId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EntityId { get; set; }

		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[Property("LogDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LogDateTime { get; set; }

		[Property("FirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FirstName { get; set; }

		[Property("LastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LastName { get; set; }

		public string UserName
		{
			get { return string.Format("{0} {1}", FirstName, LastName); }
		}

		public AuditLogsViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}

		public override string ToString()
		{
			return new[] {EntityCode, EntityId, Description, LogDateTime.ToString(), UserName}.TabJoin();
		}

		public static string Header()
		{
			return new[] {"Entity Code", "Entity Id", "Description", "Log Date Time", "User"}.TabJoin();
		}
	}
}
