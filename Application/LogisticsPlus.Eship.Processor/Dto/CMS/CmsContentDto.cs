﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.CMS
{
    [Entity("CmsContentDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "CmsContent")]
    public class CmsContentDto : EntityBase
    {
        [Property("IdentifierCode",AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string IdentifierCode { get; set; }

        [Property("Count", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int Count { get; set; }

        public CmsContentDto()
        {
        	IdentifierCode = string.Empty;
        	Count = default(int);
        }

        public CmsContentDto(DbDataReader reader)
        {
            Load(reader);
        }


        public List<CmsContentDto> FetchContentDtos()
        {
            const string query = @"SELECT 
                                        IdentifierCode, COUNT(Id) AS 'Count' 
                                    FROM 
                                        CmsContent GROUP BY IdentifierCode
                                    UNION
                                    SELECT 
                                        'News' AS 'sectionCode', COUNT(*) 
                                    FROM 
                                        CmsNews";
            var versionsList = new List<CmsContentDto>();
            using (var reader = GetReader(query, null))
                while (reader.Read())
                    versionsList.Add(new CmsContentDto(reader));
            Connection.Close();
            return versionsList;
        }
    }
}
