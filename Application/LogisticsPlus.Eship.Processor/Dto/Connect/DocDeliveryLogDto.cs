﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Connect
{
    [Serializable]
    [Entity("DocDeliveryLogDto", ReadOnly = true, Source = EntitySource.TableView)]
    public class DocDeliveryLogDto : EntityBase 
    {
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; set; }

        [Property("TenantId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long TenantId { get; set; }

        [Property("EntityId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long EntityId { get; set; }

        [Property("EntityType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public DocDeliveryEntityType EntityType { get; set; }

        [Property("LocationPath", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LocationPath { get; set; }

        [Property("DocumentName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DocumentName { get; set; }

        [Property("LogDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime LogDateTime { get; set; }

        [Property("DeliveryWasSuccessful", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool DeliveryWasSuccessful { get; set; }

        [Property("FailedDeliveryMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string FailedDeliveryMessage { get; set; }

        [Property("DocumentTagId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long DocumentTagId { get; set; }

        [Property("DocumentTagCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DocumentTagCode { get; set; }

        [Property("DocumentTagDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DocumentTagDescription { get; set; }

		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long UserId { get; set; }

		[Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Username { get; set; }

		[Property("UserFirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "FirstName")]
		public string UserFirstName { get; set; }

		[Property("UserLastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "LastName")]
		public string UserLastName { get; set; }


        public DocDeliveryLogDto()
		{
			
		}

        public DocDeliveryLogDto(DbDataReader reader)
		{
			Load(reader);
		}


        public override string ToString()
        {
        	return
        		new[]
        			{
        				EntityType.FormattedString(), DocumentName, LogDateTime.FormattedLongDateAlt(),
        				DeliveryWasSuccessful.ToString(), FailedDeliveryMessage, DocumentTagCode, DocumentTagDescription,
						Username, UserFirstName, UserLastName
        			}.TabJoin();
        }

        public static string Header()
        {
        	return
        		new[]
        			{
        				"Entity Type", "Document Name", "Log Date Time", "Delivery Was Succesful", "Failed Delivery Message",
        				"Document Tag Code", "Document Tag Description", "Username", "User First Name", "User Last Name"
        			}.TabJoin();
        }
    }
}