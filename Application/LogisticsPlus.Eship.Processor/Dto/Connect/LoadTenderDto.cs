﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Connect;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Connect
{
    [Serializable]
    [Entity("LoadTenderDto", ReadOnly = false, Source = EntitySource.TableView)]
    public class LoadTenderDto : EntityBase
    {
	    private XmlConnect _xc204;
	    private XmlConnect _xc990;


        [Property("LoadTenderId", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long LoadTenderId { get; protected set; }

        [Property("LoadTenderDirection", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public Direction LoadTenderDirection { get; set; }

        [Property("LoadTenderStatus", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public XmlConnectStatus LoadTenderStatus { get; set; }

        [Property("LoadTenderResponseId", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long LoadTenderResponseId { get; protected set; }

        [Property("LoadTenderResponseDirection", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public Direction LoadTenderResponseDirection { get; set; }

        [Property("LoadTenderResponseStatus", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public XmlConnectStatus LoadTenderResponseStatus { get; set; }

        [Property("LoadTenderExpirationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime LoadTenderExpirationDate { get; set; }

        [Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomerNumber { get; set; }

        [Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorNumber { get; set; }

        [Property("VendorScac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorScac { get; set; }

        [Property("ShipmentIdNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentIdNumber { get; set; }

	    public XmlConnect TenderRecord
	    {
			get { return LoadTenderId == default(long) ? null : _xc204 ?? (_xc204 = new XmlConnect(LoadTenderId)); }
	    }
		public XmlConnect TenderResponse
		{
			get { return LoadTenderResponseId == default(long) ? null : _xc990 ?? (_xc990 = new XmlConnect(LoadTenderResponseId)); }
		}


	    public bool TenderRecordExpired
	    {
			get { return TenderRecord != null && TenderRecord.ExpirationDate < DateTime.Now; }
	    }

        public LoadTenderDto(DbDataReader reader)
        {
            Load(reader);
        }

        public LoadTenderDto()
        {
        }
    }
}
