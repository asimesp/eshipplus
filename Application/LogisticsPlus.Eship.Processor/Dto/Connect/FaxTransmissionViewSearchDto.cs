﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Connect;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Connect
{
	[Serializable]
	[Entity("FaxTransmissionViewSearchDto", ReadOnly = false, Source = EntitySource.TableView)]
	public class FaxTransmissionViewSearchDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipmentNumber { get; set; }

		[Property("SendDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime SendDateTime { get; set; }

		[Property("ResponseDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ResponseDateTime { get; set; }

		[Property("LastStatusCheckDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LastStatusCheckDateTime { get; set; }

		[Property("Message", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Message { get; set; }

		[Property("ResolutionComment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ResolutionComment { get; set; }

		[Property("Resolved", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Resolved { get; set; }

		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public FaxTransmissionStatus Status { get; set; }

		[Property("TransmissionKey", AutoValueOnInsert = false, DataType = SqlDbType.UniqueIdentifier, Key = false)]
		public Guid TransmissionKey { get; set; }

		[Property("ExternalReference", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ExternalReference { get; set; }
		
		[Property("ServiceProvider", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceProvider ServiceProvider { get; set; }

		public FaxTransmissionViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}
