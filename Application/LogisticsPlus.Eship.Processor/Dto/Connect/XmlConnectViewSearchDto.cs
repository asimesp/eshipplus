﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Connect;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Connect
{
	[Serializable]
	[Entity("XmlConnectViewSearchDto", ReadOnly = false, Source = EntitySource.TableView)]
	public class XmlConnectViewSearchDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("Xml", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Xml { get; set; }

		[Property("ShipmentIdNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipmentIdNumber { get; set; }

		[Property("OriginStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginStreet1 { get; set; }

		[Property("OriginStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginStreet2 { get; set; }

		[Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCity { get; set; }

		[Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginState { get; set; }

		[Property("OriginCountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCountryCode { get; set; }

		[Property("OriginCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCountryName { get; set; }

		[Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginPostalCode { get; set; }

		[Property("DestinationStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationStreet1 { get; set; }

		[Property("DestinationStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationStreet2 { get; set; }

		[Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCity { get; set; }

		[Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationState { get; set; }

		[Property("DestinationCountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCountryCode { get; set; }

		[Property("DestinationCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCountryName { get; set; }

		[Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationPostalCode { get; set; }

		[Property("ExpirationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExpirationDate { get; set; }

		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[Property("ReceiptDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ReceiptDate { get; set; }

		[Property("Direction", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public Direction Direction { get; set; }

		[Property("DirectionText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DirectionText { get; set; }

		[Property("DocumentType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public EdiDocumentType DocumentType { get; set; }

		[Property("DocumentTypeText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DocumentTypeText { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }

		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }

		[Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorName { get; set; }

		[Property("VendorScac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorScac { get; set; }

		[Property("PurchaseOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PurchaseOrderNumber { get; set; }

		[Property("ShipperReference", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipperReference { get; set; }

		[Property("EquipmentDescriptionCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EquipmentDescriptionCode { get; set; }

		[Property("TotalStops", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalStops { get; set; }

		[Property("TotalWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TotalWeight { get; set; }

		[Property("TotalPackages", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalPackages { get; set; }

		[Property("TotalPieces", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalPieces { get; set; }

		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public XmlConnectStatus Status { get; set; }

		[Property("TransmissionOkay", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool TransmissionOkay { get; set; }

		[Property("StatusText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string StatusText { get; set; }

		[Property("StatusMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string StatusMessage { get; set; }

		[Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long LoadOrderId { get; protected set; }

		[Property("LoadTenderResponseId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long LoadTenderResponseId { get; protected set; }

		public XmlConnectViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}
