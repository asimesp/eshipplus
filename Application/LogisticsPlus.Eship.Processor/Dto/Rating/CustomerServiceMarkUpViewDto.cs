﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Rating
{
	[Entity("CustomerServiceMarkUpViewDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class CustomerServiceMarkUpViewDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("MarkupPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupPercent { get; set; }

		[Property("MarkupValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupValue { get; set; }

		[Property("UseMinimum", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseMinimum { get; set; }

		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[Property("ServiceChargeCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ServiceChargeCeiling { get; set; }

		[Property("ServiceChargeFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ServiceChargeFloor { get; set; }

		[Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ServiceId { get; set; }

		[Property("ServiceCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ServiceCode { get; set; }

		[Property("ServiceDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ServiceDescription { get; set; }

		[EnableSnapShot("CustomerRatingId", Description = "Customer Rating Reference")]
		[Property("CustomerRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerRatingId{ get; set; }

		public CustomerServiceMarkUpViewDto() { }

		public CustomerServiceMarkUpViewDto(DbDataReader reader)
		{
			Load(reader);
		}

		public List<CustomerServiceMarkUpViewDto> RetrieveCustomerRatingCustomerServiceMarkUps(CustomerRating customerRating)
		{
			var markUps = new List<CustomerServiceMarkUpViewDto>();
			const string query = @"SELECT [CustomerServiceMarkup].*, [Service].Code as 'ServiceCode', [Service].Description as 'ServiceDescription' 
								   FROM [CustomerServiceMarkup], [Service] 
								   WHERE [CustomerServiceMarkup].ServiceId = [Service].Id AND 
								         [CustomerServiceMarkup].CustomerRatingId = @CustomerRatingId AND
										 [CustomerServiceMarkup].TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", customerRating.TenantId }, { "CustomerRatingId", customerRating.Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					markUps.Add(new CustomerServiceMarkUpViewDto(reader));
			Connection.Close();
			return markUps;
		}
	}
}
