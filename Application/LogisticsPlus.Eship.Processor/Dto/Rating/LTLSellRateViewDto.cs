﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Rating
{
    [Serializable]
    [Entity("LTLSellRateViewDto", ReadOnly = true, Source = EntitySource.TableView)]
    public class LTLSellRateViewDto : EntityBase
    {
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [Property("LTLIndirectPointEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool LTLIndirectPointEnabled { get; set; }

        [Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime EffectiveDate { get; set; }

        [Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Active { get; set; }

        [Property("MarkupPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal MarkupPercent { get; set; }

        [Property("MarkupValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal MarkupValue { get; set; }

        [Property("UseMinimum", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool UseMinimum { get; set; }


		[Property("StartOverrideWeightBreak", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public WeightBreak StartOverrideWeightBreak { get; set; }

		[Property("OverrideMarkupPercentL5C", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentL5C { get; set; }

		[Property("OverrideMarkupValueL5C", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueL5C { get; set; }

		[Property("OverrideMarkupPercentM5C", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM5C { get; set; }

		[Property("OverrideMarkupValueM5C", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM5C { get; set; }

		[Property("OverrideMarkupPercentM1M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM1M { get; set; }

		[Property("OverrideMarkupValueM1M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM1M { get; set; }

		[Property("OverrideMarkupPercentM2M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM2M { get; set; }

		[Property("OverrideMarkupValueM2M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM2M { get; set; }

		[Property("OverrideMarkupPercentM5M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM5M { get; set; }

		[Property("OverrideMarkupValueM5M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM5M { get; set; }

		[Property("OverrideMarkupPercentM10M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM10M { get; set; }

		[Property("OverrideMarkupValueM10M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM10M { get; set; }

		[Property("OverrideMarkupPercentM20M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM20M { get; set; }

		[Property("OverrideMarkupValueM20M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM20M { get; set; }

		[Property("OverrideMarkupPercentM30M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM30M { get; set; }

		[Property("OverrideMarkupValueM30M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM30M { get; set; }

		[Property("OverrideMarkupPercentM40M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM40M { get; set; }

		[Property("OverrideMarkupValueM40M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM40M { get; set; }

        [Property("OverrideMarkupPercentVendorFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal OverrideMarkupPercentVendorFloor { get; set; }

        [Property("OverrideMarkupValueVendorFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal OverrideMarkupValueVendorFloor { get; set; }

        [Property("OverrideMarkupVendorFloorEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool OverrideMarkupVendorFloorEnabled { get; set; }

        [Property("VendorRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorRatingId { get; set; }

        [Property("VendorRatingIsActive", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool VendorRatingIsActive { get; set; }

        [Property("CustomerRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long CustomerRatingId { get; set; }

        public LTLSellRateViewDto() { }

        public LTLSellRateViewDto(DbDataReader reader)
		{
			Load(reader);
		}

        public List<LTLSellRateViewDto> RetrieveCustomerRatingLTLSellRates(CustomerRating customerRating)
		{
            var services = new List<LTLSellRateViewDto>();
            const string query = @"SELECT 
	                                    LTLSellRate.*,
	                                    VendorRating.Active 'VendorRatingIsActive'
                                    FROM LTLSellRate
	                                    Inner Join VendorRating on VendorRating.Id = LTLSellRate.VendorRatingId
                                    WHERE
	                                    LTLSellRate.CustomerRatingId = @CustomerRatingId AND
	                                    LTLSellRate.TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", customerRating.TenantId }, { "CustomerRatingId", customerRating.Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
                    services.Add(new LTLSellRateViewDto(reader));
			Connection.Close();
			return services;
		}
    }
}
