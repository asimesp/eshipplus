﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Rating
{
	[Serializable]
	[Entity("AreaViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "AreaViewSearch")]
	public class AreaViewSearchDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("PostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PostalCode { get; set; }

		[Property("City", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string City { get; set; }

		[Property("CityAlias", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CityAlias { get; set; }

		[Property("State", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string State { get; set; }

		[Property("CountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CountryId { get; set; }

		[Property("CountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CountryName { get; set; }

		[Property("SubRegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long SubRegionId { get; set; }

		[Property("UseSubRegion", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseSubRegion { get; set; }

		[Property("RegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long RegionId { get; set; }

		[Property("SubRegionName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SubRegionName { get; set; }

		public AreaViewSearchDto(){}

		public AreaViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}

		public List<AreaViewSearchDto> FetchRegionAreas(Region region)
		{
			var areas = new List<AreaViewSearchDto>();
			const string query =
				@"Select * from AreaViewSearch where RegionId = @RegionId and  TenantId = @TenantId
				order by UseSubRegion desc, SubRegionName asc, CountryName asc, [State] asc, PostalCode asc, City Asc";
			var parameters = new Dictionary<string, object> {{"TenantId", region.TenantId}, {"RegionId", region.Id}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					areas.Add(new AreaViewSearchDto(reader));
			Connection.Close();
			return areas;
		}

		public List<AreaViewSearchDto> FetchRegionAreas(long regionId, long tenantId)
		{
			var areas = new List<AreaViewSearchDto>();
			const string query =
				@"Select * from AreaViewSearch where RegionId = @RegionId and  TenantId = @TenantId
				order by UseSubRegion desc, SubRegionName asc, CountryName asc, [State] asc, PostalCode asc, City Asc";
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "RegionId", regionId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					areas.Add(new AreaViewSearchDto(reader));
			Connection.Close();
			return areas;
		}
	}
}
