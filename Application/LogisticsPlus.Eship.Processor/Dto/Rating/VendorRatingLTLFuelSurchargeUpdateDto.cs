﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Rating
{
	[Entity("VendorRatingLTLFuelSurchargeUpdateDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "VendorRatingLTLFuelSurchargeUpdateList")]
	public class VendorRatingLTLFuelSurchargeUpdateDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[Property("FuelIndexRegion", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public FuelIndexRegion FuelIndexRegion { get; set; }

		[Property("CurrentLTLFuelMarkup", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CurrentLTLFuelMarkup { get; set; }

		[Property("FuelMarkupCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FuelMarkupCeiling { get; set; }

		[Property("FuelMarkupFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FuelMarkupFloor { get; set; }

		[Property("FuelUpdatesOn", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public DayOfWeek FuelUpdatesOn { get; set; }

		[Property("Surcharge", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false, Column = "NewSurcharge")]
		public decimal Surcharge { get; set; }

		public decimal NewSurcharge
		{
			get
			{
				return Surcharge < FuelMarkupFloor
				       	? FuelMarkupFloor
				       	: Surcharge > FuelMarkupCeiling
				       	  	? FuelMarkupCeiling
				       	  	: Surcharge;
			}
		}

		public VendorRatingLTLFuelSurchargeUpdateDto()
		{
		}

		public VendorRatingLTLFuelSurchargeUpdateDto(DbDataReader reader)
		{
			Load(reader);
		}

		public List<VendorRatingLTLFuelSurchargeUpdateDto> RetrieveListForUpdate(DateTime effectiveAsOf, long tenantId)
		{
			var dtos = new List<VendorRatingLTLFuelSurchargeUpdateDto>();
			const string query = "exec VendorRatingLTLFuelSurchargeUpdateList2 @PriceEffectiveDate, @TenantId";
			var parameters = new Dictionary<string, object> {{"PriceEffectiveDate", effectiveAsOf}, {"TenantId", tenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					dtos.Add(new VendorRatingLTLFuelSurchargeUpdateDto(reader));
			Connection.Close();
			return dtos;
		}
	}
}
