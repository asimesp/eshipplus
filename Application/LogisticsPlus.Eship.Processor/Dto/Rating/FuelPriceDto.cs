﻿using System;
using System.Collections.Generic;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Rating
{
	[Entity("FuelPriceDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class FuelPriceDto : EntityBase
	{
		public decimal GetFuelPrice(int fuelIndexRegion, DateTime effectiveDate, long tenantId)
		{
			const string query = @"SELECT dbo.FuelPrice(@FuelIndexRegion, @EffectiveDate, @TenantId)";

			var parameters = new Dictionary<string, object> { { "FuelIndexRegion", fuelIndexRegion }, { "EffectiveDate", effectiveDate}, {"TenantId", tenantId} };

			var fuelPrice = ExecuteScalar(query, parameters).ToDecimal();
			return fuelPrice;
		}
	}
}
