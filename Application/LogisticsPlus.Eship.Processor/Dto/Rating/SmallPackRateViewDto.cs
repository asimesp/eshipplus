﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Rating
{
    [Serializable]
	[Entity("SmallPackRateViewDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class SmallPackRateViewDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("SmallPackageEngine", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public SmallPackageEngine SmallPackageEngine { get; set; }

		[Property("SmallPackType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SmallPackType { get; set; }

		[Property("MarkupPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupPercent { get; set; }

		[Property("MarkupValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupValue { get; set; }

		[Property("UseMinimum", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseMinimum { get; set; }

		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId { get; set; }

		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }
		
		[Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorName { get; set; }

		[Property("CustomerRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerRatingId { get; set; }

		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId { get; set; }

		[Property("ChargeCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCode { get; set; }

		[Property("ChargeCodeDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCodeDescription { get; set; }

		public SmallPackRateViewDto() { }

		public SmallPackRateViewDto(DbDataReader reader)
		{
			Load(reader);
		}

		public List<SmallPackRateViewDto> RetrieveCustomerRatingSmallPackRates(CustomerRating customerRating)
		{
			var services = new List<SmallPackRateViewDto>();
			const string query = @"SELECT [SmallPackRate].*, [Vendor].VendorNumber, [Vendor].Name as 'VendorName',
									[ChargeCode].Id as 'ChargeCodeId', [ChargeCode].Code as 'ChargeCode', [ChargeCode].[Description] as 'ChargeCodeDescription'
								   FROM [SmallPackRate], [Vendor], [ChargeCode]
								   WHERE
										[SmallPackRate].ChargeCodeId = [ChargeCode].Id AND 
										[SmallPackRate].VendorId = [Vendor].Id AND 
										[SmallPackRate].CustomerRatingId = @CustomerRatingId AND
										[SmallPackRate].TenantId = @TenantId
									ORDER BY [SmallPackRate].SmallPackageEngine ASC, [SmallPackRate].SmallPackType ASC";
			var parameters = new Dictionary<string, object> { { "TenantId", customerRating.TenantId }, { "CustomerRatingId", customerRating.Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					services.Add(new SmallPackRateViewDto(reader));
			Connection.Close();
			return services;
		}
	}
}
