﻿using System.Collections.Generic;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Rating
{
	[Entity("MatchingVendorRatingRegionIdsDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class MatchingVendorRatingRegionIdsDto : EntityBase
	{
		private const string OriginQuery =
			"exec MatchingVendorRatingOriginRegionIds @RatingId, @TenantId, @PostalCode, @CountryId";

		private const string DestinationQuery =
			"exec MatchingVendorRatingDestinationRegionIds @RatingId, @TenantId, @PostalCode, @CountryId";

		private const string TenantId = "TenantId";
		private const string PostalCode = "PostalCode";
		private const string CountryId = "CountryId";
		private const string VendorRatingId = "RatingId";

		private readonly List<long> _origin = new List<long>();
		private readonly List<long> _destination = new List<long>();

		public List<long> OriginRegionIds
		{
			get { return _origin; }
		}

		public List<long> DestinationRegionIds
		{
			get { return _destination; }
		}

		public MatchingVendorRatingRegionIdsDto(long tenantId, MatchingRatingRegionIdCriteria criteria)
		{
			var parameters = new Dictionary<string, object>();
			parameters[TenantId] = tenantId;
			parameters[PostalCode] = criteria.OriginPostalCode;
			parameters[CountryId] = criteria.OriginCountryId;
			parameters[VendorRatingId] = criteria.RatingId;

			// origin
			using (var reader = GetReader(OriginQuery, parameters))
				while (reader.Read())
					_origin.Add(reader.GetValue(reader.GetOrdinal("Id")).ToLong());
			Connection.Close();


			parameters[PostalCode] = criteria.DestinationPostalCode;
			parameters[CountryId] = criteria.DestinationCountryId;

			// destination
			using (var reader = GetReader(DestinationQuery, parameters))
				while (reader.Read())
					_destination.Add(reader.GetValue(reader.GetOrdinal("Id")).ToLong());
			Connection.Close();
		}
	}
}
