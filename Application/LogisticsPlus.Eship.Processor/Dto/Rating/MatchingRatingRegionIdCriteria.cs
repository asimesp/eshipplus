﻿namespace LogisticsPlus.Eship.Processor.Dto.Rating
{
	public class MatchingRatingRegionIdCriteria
	{
		public string OriginPostalCode { get; set; }
		public long OriginCountryId { get; set; }
		public string DestinationPostalCode { get; set; }
		public long DestinationCountryId { get; set; }
		public long RatingId { get; set; }
	}
}
