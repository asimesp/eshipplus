﻿using System.Collections.Generic;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Rating
{
    [Entity("MatchingRegionPostalCodeWithinRegionDto", ReadOnly = true, Source = EntitySource.TableView)]
    public class MatchingRegionPostalCodeWithinRegionDto : EntityBase
    {
        private const string TenantId = "TenantId";
        private const string PostalCode = "PostalCode";
        private const string CountryId = "CountryId";
        private const string ParentRegionId = "ParentRegionId";

        private const string Query = "exec MatchingRegionPostalCodeWithinRegion @ParentRegionId, @TenantId, @PostalCode, @CountryId";


        private readonly List<long> _regionIds = new List<long>();

        public List<long> RegionIds
        {
            get { return _regionIds; }
        }

        public MatchingRegionPostalCodeWithinRegionDto(long tenantId, long countryId, long parentRegionId, string postalCode)
		{
			var parameters = new Dictionary<string, object>();
			parameters[TenantId] = tenantId;
            parameters[PostalCode] = postalCode;
            parameters[CountryId] = countryId;
            parameters[ParentRegionId] = parentRegionId;

			// origin
            using (var reader = GetReader(Query, parameters))
				while (reader.Read())
                    _regionIds.Add(reader.GetValue(reader.GetOrdinal("Id")).ToLong());
			Connection.Close();
		}
    }
}
