﻿using System;
using System.Linq;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
    [Entity("JobComponentDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "")]
    public class JobComponentDto : EntityBase
    {
        private List<Shipment> _shipments;
        private List<ServiceTicket> _serviceTickets;
        private List<LoadOrder> _loadOrders;
        
        [Property("ComponentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
        public long ComponentId { get; set; }

        [Property("ComponentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ComponentNumber { get; set; }

        [Property("ComponentStatus", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ComponentStatus { get; set; }

        [Property("ComponentType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ComponentType ComponentType { get; set; }

        [Property("ComponentJobStep", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int ComponentJobStep { get; set; }

        public long JobId { get; set; }

        public long TenantId { get; set; }

        public JobComponentDto()
        {
        }

        public JobComponentDto(Job job)
        {
            JobId = job.Id;
            TenantId = job.TenantId;
        }

        public JobComponentDto(DbDataReader reader)
        {
            Load(reader);
        }

        public List<Shipment> Shipments
        {
            get
            {
                if (_shipments == null) LoadShipments();
                return _shipments;
            }
            set { _shipments = value; }
        }

        public List<ServiceTicket> ServiceTickets
        {
            get
            {
                if (_serviceTickets == null) LoadServiceTickets();
                return _serviceTickets;
            }
            set { _serviceTickets = value; }
        }
        public List<LoadOrder> LoadOrders
        {
            get
            {
                if (_loadOrders == null) LoadLoadOrders();
                return _loadOrders;
            }
            set { _loadOrders = value; }
        }

        public void LoadCollections()
        {
            LoadShipments();
            LoadServiceTickets();
            LoadLoadOrders();
        }

        private void LoadShipments()
        {
            _shipments = new List<Shipment>();
            const string query = @"SELECT * FROM Shipment WHERE JobId = @JobId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "JobId", JobId }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _shipments.Add(new Shipment(reader));
            Connection.Close();
        }

        private void LoadServiceTickets()
        {
            _serviceTickets = new List<ServiceTicket>();
            const string query = @"SELECT * FROM ServiceTicket WHERE JobId = @JobId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "JobId", JobId }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _serviceTickets.Add(new ServiceTicket(reader));
            Connection.Close();
        }

        private void LoadLoadOrders()
        {
            _loadOrders = new List<LoadOrder>();
            const string query = @"SELECT * FROM LoadOrder WHERE JobId = @JobId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "JobId", JobId }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _loadOrders.Add(new LoadOrder(reader));
            Connection.Close();
        }
    }
}