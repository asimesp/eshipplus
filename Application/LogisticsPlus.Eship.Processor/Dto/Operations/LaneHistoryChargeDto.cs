﻿using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
	[Entity("LaneHistoryChargeDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class LaneHistoryChargeDto : EntityBase
	{
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId { get; set; }

		[Property("ChargeCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCode { get; set; }

		[Property("ChargeCodeDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCodeDescription { get; set; }

		[Property("LowestBuy", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LowestBuy { get; set; }

		[Property("AverageBuy", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AverageBuy { get; set; }

		[Property("HighestBuy", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal HighestBuy { get; set; }

		[Property("LowestSell", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LowestSell { get; set; }

		[Property("AverageSell", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AverageSell { get; set; }

		[Property("HighestSell", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal HighestSell { get; set; }

		public LaneHistoryChargeDto()
		{
		}

		public LaneHistoryChargeDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}
