﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Entity("ShoppedRateDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "ShoppedRateSearchDto")]
    public class ShoppedRateViewSearchDto : EntityBase
    {
        private List<ServiceViewSearchDto> _services;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [Property("TenantId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long TenantId { get; set; }

		[Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long LoadOrderId { get; set; }

        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShipmentId { get; set; }

        [Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ReferenceNumber { get; set; }

        [Property("Type", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ShoppedRateType Type { get; set; }

        [Property("NumberLineItems", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int NumberLineItems { get; set; }

        [Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int Quantity { get; set; }

        [Property("Weight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Weight { get; set; }

        [Property("HighestFreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
        public double HighestFreightClass { get; set; }

        [Property("MostSignificantFreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
        public double MostSignificantFreightClass { get; set; }

        [Property("LowestTotalCharge", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal LowestTotalCharge { get; set; }

        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long UserId { get; set; }

        [Property("UserName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string UserName { get; set; }

        [Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long CustomerId { get; set; }

        [Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomerName { get; set; }

        [Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomerNumber{ get; set; }

		[Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorName { get; set; }

		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }

        [Property("OriginCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long OriginCountryId { get; set; }

        [Property("DestinationCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long DestinationCountryId { get; set; }

        [Property("OriginCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCountryName { get; set; }

        [Property("DestinationCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCountryName { get; set; }

        [Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCity { get; set; }

        [Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginState { get; set; }

        [Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPostalCode { get; set; }

        [Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCity { get; set; }

        [Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationState { get; set; }

        [Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationPostalCode { get; set; }

		[Property("QuoteExists", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool QuoteExists { get; set; }

		[Property("ShipmentExists", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ShipmentExists { get; set; }

        [Property("TypeText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string TypeText { get; set; }

        [Property("CustomerRateAndScheduleInDemo", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool CustomerRateAndScheduleInDemo { get; set; }

	    public bool HasServices
	    {
		    get { return Services.Any(); }
	    }

	    public List<ServiceViewSearchDto> Services
        {
            get
            {
                if (_services == null) LoadServices();
                return _services;
            }
            set { _services = value; }
        }

        public ShoppedRateViewSearchDto() { }

        public ShoppedRateViewSearchDto(long id)
        {
            Id = id;
            if (Id == default(long)) return;
            Load();
        }

        public ShoppedRateViewSearchDto(DbDataReader reader)
        {
            Load(reader);
        }

        private void LoadServices()
        {
            _services = new ServiceViewSearchDto().RetrieveShoppedRateServices(this);
        }

        public List<ShoppedRateViewSearchDto> FetchShoppedRateDtos(ShoppedRateSearchCriteria criteria, long tenantId)
        {
            var shoppedRateDtos = new List<ShoppedRateViewSearchDto>();

            var query = @"SELECT * FROM ShoppedRateViewSearch2(@TenantId, @ActiveUserId) WHERE";

            var parameters = new Dictionary<string, object>
                                 {
                                     {"TenantId", tenantId},
                                     {"ActiveUserId", criteria.ActiveUserId}
                                 };
            query += criteria.Parameters.BuildWhereClause(parameters, OperationsSearchFields.ShoppedRates);

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? "ASC" : "DESC");

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    shoppedRateDtos.Add(new ShoppedRateViewSearchDto(reader));
            Connection.Close();

            return shoppedRateDtos;

        }
    }
}
