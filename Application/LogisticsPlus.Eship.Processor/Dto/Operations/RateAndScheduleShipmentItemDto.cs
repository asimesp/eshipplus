﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
    [Entity("RateAndScheduleShipmentItemDto", ReadOnly = true, Source = EntitySource.TableView)]
    public class RateAndScheduleShipmentItemDto : EntityBase
    {
       
        [Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Description { get; set; }
        
        [Property("ActualFreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
        public double ActualFreightClass { get; set; }
        
        [Property("RatedFreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
        public double RatedFreightClass { get; set; }
        

        [Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Comment { get; set; }

       
        [Property("ActualWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal ActualWeight { get; set; }

       
        [Property("ActualLength", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal ActualLength { get; set; }

       
        [Property("ActualWidth", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal ActualWidth { get; set; }

       
        [Property("ActualHeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal ActualHeight { get; set; }

      
        [Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int Quantity { get; set; }

       
        [Property("PieceCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int PieceCount { get; set; }

        
        [Property("IsStackable", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsStackable { get; set; }

       
        [Property("HazardousMaterial", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool HazardousMaterial { get; set; }

     
        [Property("PackageTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long PackageTypeId { get; set; }

     
        [Property("Pickup", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int Pickup { get; set; }

     
        [Property("Delivery", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int Delivery { get; set; }

     
        [Property("NMFCCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string NMFCCode { get; set; }

       
        [Property("HTSCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HTSCode { get; set; }

       
        [Property("Value", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Value { get; set; }


        public RateAndScheduleShipmentItemDto(DbDataReader reader)
        {
            Load(reader);
        }

        public RateAndScheduleShipmentItemDto()
        {
            
        }

    }
}
