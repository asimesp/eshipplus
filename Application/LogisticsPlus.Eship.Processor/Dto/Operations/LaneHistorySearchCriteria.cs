﻿using System;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
	public class LaneHistorySearchCriteria
	{
		public string OriginPostalCode { get; set; }
		public long OriginCountryId { get; set; }
		public string DestinationPostalCode { get; set; }
		public long DestinationCountryId { get; set; }
		public decimal TotalWeight { get; set; }
		public DateTime DesiredPickupDate { get; set; }
		public double Radius { get; set; }
	}
}
