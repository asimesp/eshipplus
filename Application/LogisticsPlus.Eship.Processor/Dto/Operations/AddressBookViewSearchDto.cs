﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
	[Entity("AddressBookViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "[UserVisibleCustomerAddressBookAddresses]")]
	public class AddressBookViewSearchDto : EntityBase
	{
		private List<AddressBookContact> _contacts;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("TenantId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long TenantId { get; private set; }

        [Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Description { get; set; }

		[Property("OriginSpecialInstruction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginSpecialInstruction { get; set; }

		[Property("DestinationSpecialInstruction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationSpecialInstruction { get; set; }

		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId { get; set; }

		[Property("Street1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Street1 { get; set; }

		[Property("Street2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Street2 { get; set; }

		[Property("City", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string City { get; set; }

		[Property("State", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string State { get; set; }

		[Property("CountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CountryId { get; set; }

		[Property("PostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PostalCode { get; set; }

		[Property("CountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CountryCode { get; set; }

		[Property("CountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CountryName { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }

		[Property("ContactStringXml", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "Contacts")]
		public string ContactStringXml { get; set; }


		public string ContactStringXmlAsHtmlRowWithRowNumber
		{
			get
			{
				if (string.IsNullOrEmpty(ContactStringXml)) return string.Empty;

				var cnt = 0;
				return ContactStringXml
					.Replace("wrapper", "tr")
					.Replace("Name", "td")
					.Replace("Phone", "td")
					.Replace("Mobile", "td")
					.Replace("Fax", "td")
					.Replace("Email", "td")
					.Replace("<Primary>1</Primary>", "<td>&#x2713;</td>")
					.Replace("<Primary>0</Primary>", "<td></td>")
					.Split(new[] {"<tr>"}, StringSplitOptions.RemoveEmptyEntries)
					.Select(s => string.Format("<tr><td>{0}. </td>{1}", ++cnt, s))
					.ToArray()
					.SpaceJoin();
			}
		}

		public string FullAddress
		{
			get { return string.Format("{0} {1} {2} {3} {4}", CombinedStreet, City, State, PostalCode, CountryName); }
		}

		public string CombinedStreet
		{
			get { return string.Format("{0}{1}{2}", Street1, string.IsNullOrEmpty(Street2) ? string.Empty : ", ", Street2); }
		}

		public List<AddressBookContact> Contacts
		{
			get
			{
				if (_contacts == null) LoadContacts();
				return _contacts;
			}
			set { _contacts = value; }
		}

		public AddressBookViewSearchDto(){}

		public AddressBookViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}

		private void LoadContacts()
		{
			_contacts = new List<AddressBookContact>();
		    const string query =
		        @"SELECT * FROM AddressBookContact WHERE AddressBookId = @AddressBookId AND TenantId = @TenantId";

			IDictionary<string, object> parameters = new Dictionary<string, object> { { "AddressBookId", Id }, { "TenantId", TenantId } };

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_contacts.Add(new AddressBookContact(reader));
			Connection.Close();
		}

		public List<AddressBookViewSearchDto> FetchAddressBookDtos(long tenantId, AddressBookViewSearchCriteria criteria, bool top100 = true )
        {
            var addressBookDtos = new List<AddressBookViewSearchDto>();

            var query = string.Format(@"SELECT {0} * FROM UserVisibleCustomerAddressBookAddresses(@UserId, @CustomerId, @TenantId) WHERE ", top100 ? "TOP 100" : string.Empty);

        	var parameters = new Dictionary<string, object>
        	                 	{
        	                 		{"UserId", criteria.UserId},
        	                 		{"CustomerId", criteria.CustomerId},
        	                 		{"TenantId", tenantId},
        	                 	};
            query += criteria.Parameters.BuildWhereClause(parameters, OperationsSearchFields.AddressBooks);
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    addressBookDtos.Add(new AddressBookViewSearchDto(reader));
            Connection.Close();

            return addressBookDtos;
        }

		public List<AddressBookViewSearchDto> FetchAddressBookDtoSingleCriteriaAndDefaultsSearch(long tenantId, long userId, long customerId, string criteria, Operator op)
		{
			var addressBookDtos = new List<AddressBookViewSearchDto>();

			var query = @"SELECT top 100 * FROM UserVisibleCustomerAddressBookAddresses(@UserId, @CustomerId, @TenantId) WHERE ";

			var parameters = new Dictionary<string, object>
        	                 	{
        	                 		{"UserId", userId},
        	                 		{"CustomerId", customerId},
        	                 		{"TenantId", tenantId},
        	                 	};
			var defaultParams = OperationsSearchFields.DefaultAddressBooks.Select(sf=>sf.ToParameterColumn()).ToList();
			foreach (var p in defaultParams)
			{
				p.DefaultValue = criteria;
				p.DefaultValue2 = string.Empty;
				p.Operator = op;
			}
			query += defaultParams.BuildWhereClause(parameters, OperationsSearchFields.AddressBooks).Replace(" AND ", " OR ");
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					addressBookDtos.Add(new AddressBookViewSearchDto(reader));
			Connection.Close();

			return addressBookDtos;
		}
	}
}