﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
	[Entity("ClaimViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "ClaimViewSearch")]
	public class ClaimViewSearchDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

        [Property("TenantId", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long TenantId { get; protected set; }

		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("ClaimDetail", AutoValueOnInsert = false, DataType = SqlDbType.Text, Key = false)]
		public string ClaimDetail { get; set; }

        [Property("ClaimDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime ClaimDate { get; set; }

		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }

		[Property("ClaimNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ClaimNumber { get; set; }

        [Property("IsRepairable", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsRepairable { get; set; }

        [Property("EstimatedRepairCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal EstimatedRepairCost { get; set; }

        [Property("AmountClaimed", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal AmountClaimed { get; set; }

        [Property("AmountClaimedType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public AmountClaimedType AmountClaimedType { get; set; }

        [Property("AmountClaimedTypeText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string AmountClaimedTypeText { get; set; }

        [Property("ClaimType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ClaimType ClaimType { get; set; }

        [Property("ClaimTypeText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ClaimTypeText { get; set; }

        [Property("ClaimantReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ClaimantReferenceNumber { get; set; }

		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ClaimStatus Status { get; set; }

		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long UserId { get; set; }

		[Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Username { get; set; }

		[Property("UserFirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string UserFirstName { get; set; }

		[Property("UserLastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string UserLastName { get; set; }

		[Property("Shipments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Shipments { get; set; }

		[Property("ServiceTickets", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ServiceTickets { get; set; }

		[Property("VendorNames", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNames { get; set; }
        
        [Property("DateLpAcctToPayCarrier", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateLpAcctToPayCarrier { get; set; }
       
        [Property("Acknowledged", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime Acknowledged { get; set; }
        
        [Property("CheckNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CheckNumber { get; set; }

       [Property("CheckAmount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal CheckAmount { get; set; }

        public ClaimViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}