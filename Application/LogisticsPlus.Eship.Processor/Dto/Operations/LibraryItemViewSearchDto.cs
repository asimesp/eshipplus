﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
	[Entity("LibraryItemViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "LibraryItemViewSearchDto")]
	public class LibraryItemViewSearchDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[Property("FreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FreightClass { get; set; }

		[Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Comment { get; set; }

		[Property("Weight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Weight { get; set; }

		[Property("Length", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Length { get; set; }

		[Property("Width", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Width { get; set; }

		[Property("Height", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Height { get; set; }

		[Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Quantity { get; set; }

		[Property("PieceCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int PieceCount { get; set; }

		[Property("PackageTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PackageTypeId { get; set; }
		
		[Property("PackageTypeName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PackageTypeName { get; set; }

		[Property("NMFCCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string NMFCCode { get; set; }

		[Property("HTSCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string HTSCode { get; set; }

		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }

		public LibraryItemViewSearchDto(){}

		public LibraryItemViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}