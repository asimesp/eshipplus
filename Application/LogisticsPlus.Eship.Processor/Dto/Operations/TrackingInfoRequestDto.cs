﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
	[Entity("TrackingInfoRequestDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "")]
	public class TrackingInfoRequestDto : EntityBase
    {
		[Property("Scac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true)]
		public string Scac { get; set; }

		[Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true)]
    	public string ShipmentNumber { get; set; }

		[Property("ScheduledPickupDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = true)]
        public DateTime ScheduledPickupDate { get; set; }

		[Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true)]
        public string OriginPostalCode { get; set; }

		[Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true)]
        public string DestinationPostalCode { get; set; }

		[Property("OriginCountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true)]
        public string OriginCountryCode { get; set; }

		[Property("DestinationCountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true)]
        public string DestinationCountryCode { get; set; }

		[Property("ProNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true)]
        public string ProNumber { get; set; }

		public TrackingInfoRequestDto()
		{
		}

		public TrackingInfoRequestDto(DbDataReader reader)
		{
			Load(reader);
		}


		public List<TrackingInfoRequestDto> FetchTrackingInfoRequests(string scac, DateTime cutoffDate, long tenantId)
		{
			var trackingInfoRequests = new List<TrackingInfoRequestDto>();

			const string query =
                @"SELECT
						v.Scac,
						sh.ShipmentNumber,
						sh.DesiredPickupDate As 'ScheduledPickupDate',
						ol.PostalCode AS 'OriginPostalCode',
						oc.Code AS 'OriginCountryCode',
						dl.PostalCode AS 'DestinationPostalCode',
						dc.Code AS 'DestinationCountryCode',
						shv.ProNumber
					FROM
						Shipment AS sh
							INNER JOIN ShipmentLocation AS ol ON sh.OriginId = ol.Id
							INNER JOIN Country AS oc on ol.CountryId = oc.Id
							INNER JOIN ShipmentLocation AS dl ON sh.DestinationId = dl.Id
							INNER JOIN Country AS dc ON dc.Id = dl.CountryId
							INNER JOIN ShipmentVendor AS shv ON sh.Id = shv.ShipmentId
							INNER JOIN Vendor AS v ON v.Id = shv.VendorId
					WHERE
						(sh.ActualDeliveryDate = '1753-01-01 00:00:00.000'
						OR shv.ProNumber = ''
						OR sh.ActualPickupDate = '1753-01-01 00:00:00.000' )
						AND v.Scac = @Scac
						AND sh.TenantId = @TenantId
                        AND sh.[Status] <> 4
						AND sh.DateCreated >= @CutoffDate";

			var parameters = new Dictionary<string, object>
				                 	{
				                 		{"Scac", scac},
				                 		{"CutoffDate", cutoffDate},
										{"TenantId", tenantId }
				                 	};

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					trackingInfoRequests.Add(new TrackingInfoRequestDto(reader));
			Connection.Close();
			return trackingInfoRequests;
		}
    }
}
