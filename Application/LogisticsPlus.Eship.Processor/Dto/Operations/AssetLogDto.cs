﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
	[Entity("AssetLogDto", ReadOnly = false, Source = EntitySource.TableView)]
	public class AssetLogDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Comment { get; set; }

		[Property("MileageEngine", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public MileageEngine MileageEngine { get; set; }

		[Property("MilesRun", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MilesRun { get; set; }

		[Property("LogDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LogDate { get; set; }

		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ShipmentId { get; set; }

		[Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipmentNumber { get; set; }

		[Property("ShipmentExists", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ShipmentExists { get; set; }

		[Property("AssetId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long AssetId { get; set; }

		[Property("AssetNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AssetNumber { get; set; }

		[Property("AssetDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AssetDescription { get; set; }

		[Property("Street1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Street1 { get; set; }

		[Property("Street2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Street2 { get; set; }

		[Property("City", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string City { get; set; }

		[Property("State", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string State { get; set; }

		[Property("PostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PostalCode { get; set; }

		[Property("CountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CountryId { get; set; }

		[Property("CountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CountryCode { get; set; }

		[Property("CountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CountryName { get; set; }


		public AssetLogDto()
		{
			
		}

		public AssetLogDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}
