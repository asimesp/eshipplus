﻿namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
	public enum DashboardDtoType
	{
		NotApplicable = 0,
		LoadOrder,
		Shipment
	}
}
