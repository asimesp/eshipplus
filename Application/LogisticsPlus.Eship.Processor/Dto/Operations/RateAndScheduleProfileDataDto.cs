﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
    [Entity("RateAndScheduleProfileDataDto", ReadOnly = true, Source = EntitySource.TableView)]
    public class RateAndScheduleProfileDataDto : EntityBase
    {
        
        [Property("OriginStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginStreet1 { get; set; }
        
        [Property("Street2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginStreet2 { get; set; }
        
        [Property("City", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCity { get; set; }
        
        [Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginState { get; set; }
        
        [Property("OriginCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long OriginCountryId { get; set; }
        
        [Property("OriginDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginDescription { get; set; }
        
        [Property("OriginSpecialInstructions", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginSpecialInstructions { get; set; }
        
        [Property("OriginContact", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginContact { get; set; }
        
        [Property("OriginPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPhone { get; set; }
        
        [Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPostalCode { get; set; }


        
        [Property("DestinationStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationStreet1 { get; set; }
        
        [Property("DestinationStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationStreet2 { get; set; }
        
        [Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCity { get; set; }
        
        [Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationState { get; set; }
        
        [Property("DestinationCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long DestinationCountryId { get; set; }

        [Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationPostalCode { get; set; }

        
        [Property("DestinationDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationDescription { get; set; }
        
        [Property("DestinationSpecialInstructions", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationSpecialInstructions { get; set; }
        
        [Property("DestinationContact", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationContact { get; set; }
             
        [Property("DestinationPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationPhone { get; set; }



        [Property("HazardousMaterial", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool HazardousMaterial { get; set; }

        [Property("HazardousMaterialContactEmail", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactEmail { get; set; }

        [Property("HazardousMaterialContactMobile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactMobile { get; set; }

        [Property("HazardousMaterialContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactName { get; set; }

        [Property("HazardousMaterialContactPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactPhone { get; set; }


        [Property("PurchaseOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PurchaseOrderNumber { get; set; }

        [Property("ShipperReference", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipperReference { get; set; }



        [Property("EarlyPickup", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string EarlyPickup { get; set; }

        [Property("LatePickup", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LatePickup { get; set; }

        [Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomerNumber { get; set; }

        [Property("DesiredPickupDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DesiredPickupDate { get; set; }



        [Property("EnglishInputUnits", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool EnglishInputUnits { get; set; }

        [Property("MetricInputUnits", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool MetricInputUnits { get; set; }

        [Property("BypassLinearFootRule", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool BypassLinearFootRule { get; set; }


        public List<RateAndScheduleShipmentItemDto> Items { get; set; }
        public List<long> Services { get; set; }


        public RateAndScheduleProfileDataDto(DbDataReader reader)
        {
            Load(reader);
        }

        public RateAndScheduleProfileDataDto()
        {
            
        }

    }

}
