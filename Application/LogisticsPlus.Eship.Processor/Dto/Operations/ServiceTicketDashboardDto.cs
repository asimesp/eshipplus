﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
	[Entity("ServiceTicketDashboardDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "ServiceTicketViewSearch2")]
	public class ServiceTicketDashboardDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }

		[Property("ServiceTicketNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ServiceTicketNumber { get; set; }

		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceTicketStatus Status { get; set; }

		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[Property("TicketDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime TicketDate { get; set; }

		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long UserId { get; set; }

		[Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Username { get; set; }

		[Property("ResellerAdditionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ResellerAdditionId { get; set; }

		[Property("BillReseller", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool BillReseller { get; set; }

		[Property("AuditedForInvoicing", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool AuditedForInvoicing { get; set; }

		[Property("OverrideCustomerLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long OverrideCustomerLocationId { get; set; }

        [Property("JobId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long JobId { get; set; }

        [Property("JobNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string JobNumber { get; set; }

        [Property("JobStatusText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string JobStatusText { get; set; }

        [Property("JobStep", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int JobStep { get; set; }
		

		public ServiceTicketDashboardDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}