﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
	[Entity("ShipmentDashboardDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "")]
	public class ShipmentDashboardDto : EntityBase
    {
	    private List<CheckCall> _checkCalls;

		[Property("Type", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = true, Column = "RecordType")]
		public DashboardDtoType Type { get; set; }

		[Property("Id", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipmentNumber { get; set; }

        [Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Description { get; set; }

		[Property("ServiceMode", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceMode ServiceMode { get; set; }


        [Property("PurchaseOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.VarChar, Key = false)]
		public string PurchaseOrderNumber { get; set; }




		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[Property("DesiredPickupDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DesiredPickupDate { get; set; }

		[Property("ActualPickupDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ActualPickupDate { get; set; }

		[Property("OnTimePickup", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool OnTimePickup { get; set; }

		[Property("EstimatedDeliveryDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EstimatedDeliveryDate { get; set; }

		[Property("ActualDeliveryDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ActualDeliveryDate { get; set; }

		[Property("OnTimeDelivery", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool OnTimeDelivery { get; set; }


		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		protected int Status { get; set; }

		public ShipmentStatus ShipmentStatus
		{
			get { return Status.ToEnum<ShipmentStatus>(); }
			set { Status = value.ToInt(); }
		}

		public LoadOrderStatus LoadOrderStatus
		{
			get { return Status.ToEnum<LoadOrderStatus>(); }
			set { Status = value.ToInt(); }
		}


		[Property("StatusText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string StatusText { get; set; }

		[Property("PriorityColor", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PriorityColor { get; set; }

		[Property("PriorityCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PriorityCode { get; set; }


		[Property("OriginName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginName { get; set; }

		[Property("OriginStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginStreet1 { get; set; }

		[Property("OriginStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginStreet2 { get; set; }

		[Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCity { get; set; }

		[Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginState { get; set; }

		[Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginPostalCode { get; set; }

		[Property("OriginCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCountryName { get; set; }


		[Property("DestinationName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationName { get; set; }

		[Property("DestinationStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationStreet1 { get; set; }

		[Property("DestinationStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationStreet2 { get; set; }

		[Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCity { get; set; }

		[Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationState { get; set; }

		[Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationPostalCode { get; set; }

		[Property("DestinationCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCountryName { get; set; }


		[Property("TotalAmountDue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TotalAmountDue { get; set; }

		[Property("TotalCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TotalCost { get; set; }

		[Property("TotalWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TotalWeight { get; set; }


		[Property("Prefix", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Prefix { get; set; }

        [Property("PrefixDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PrefixDescription { get; set; }


		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }


		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId { get; set; }

		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }

		[Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorName { get; set; }

		[Property("VendorProNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorProNumber { get; set; }
		
		[Property("VendorTrackingUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorTrackingUrl { get; set; }



		[Property("ResellerAdditionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ResellerAdditionId { get; set; }

		[Property("BillReseller", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool BillReseller { get; set; }

		[Property("CheckCallsPresent", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CheckCallsPresent { get; set; }

		[Property("CheckCallAlert", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CheckCallAlert { get; set; }

		[Property("VendorInsuranceAlert", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool VendorInsuranceAlert { get; set; }

		[Property("AuditedForInvoicing", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool AuditedForInvoicing { get; set; }

        [Property("InDispute", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool InDispute { get; set; }

        [Property("InDisputeReason", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int InDisputeReason { get; set; }

        [Property("InDisputeReasonText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string InDisputeReasonText { get; set; }

        [Property("IsGuaranteedDeliveryService", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsGuaranteedDeliveryService { get; set; }

        [Property("GuaranteedDeliveryServiceTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string GuaranteedDeliveryServiceTime { get; set; }
       


        [Property("ShipmentCoordinatorUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentCoordinatorUsername { get; set; }

        [Property("ShipmentCoordinatorFirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentCoordinatorFirstName { get; set; }

        [Property("ShipmentCoordinatorLastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentCoordinatorLastName { get; set; }


        [Property("CarrierCoordinatorUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CarrierCoordinatorUsername { get; set; }

        [Property("CarrierCoordinatorFirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CarrierCoordinatorFirstName { get; set; }

        [Property("CarrierCoordinatorLastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CarrierCoordinatorLastName { get; set; }


        [Property("Equipments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Equipments { get; set; }

        [Property("Mileage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Mileage { get; set; }

        [Property("JobId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long JobId { get; set; }

        [Property("JobNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string JobNumber { get; set; }

        [Property("JobStatusText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string JobStatusText { get; set; }
        
        [Property("JobStep", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int JobStep { get; set; }

		public List<CheckCall> CheckCalls
		{
			get
			{
				if (_checkCalls == null) LoadCheckCalls();
				return _checkCalls;
			}
			set { _checkCalls = value; }
		}


		public ShipmentDashboardDto()
		{
			Type = DashboardDtoType.NotApplicable;
		}

		public ShipmentDashboardDto(DbDataReader reader, DashboardDtoType type)
		{
			Type = type;
			Load(reader);
		}

		public ShipmentDashboardDto(Shipment shipment)
		{
			Type = DashboardDtoType.Shipment;

			Id = shipment.Id;
			ShipmentNumber = shipment.ShipmentNumber;
			ActualDeliveryDate = shipment.ActualDeliveryDate;
			ActualPickupDate = shipment.ActualPickupDate;
			BillReseller = shipment.BillReseller;

			ServiceMode = shipment.ServiceMode;
			var status = shipment.Status;
			var checkCalls = shipment.CheckCalls;
			var validServiceMode = ServiceMode == ServiceMode.Air || ServiceMode == ServiceMode.Rail || ServiceMode == ServiceMode.Truckload;
			CheckCallAlert = validServiceMode && status == ShipmentStatus.InTransit && checkCalls.All(c => c.CallDate.TimeToMinimum() != DateTime.Now.TimeToMinimum());
		    PurchaseOrderNumber = shipment.PurchaseOrderNumber;

            CustomerId = shipment.Customer.Id;
			CustomerName = shipment.Customer.Name;
			CustomerNumber = shipment.Customer.CustomerNumber;

			var v = shipment.Vendors.FirstOrDefault(sv => sv.Primary) ??
			        new ShipmentVendor {Vendor = new Vendor {Insurances = new List<VendorInsurance>()}};
			int thresholdDays = ProcessorVars.VendorInsuranceAlertThresholdDays[shipment.User.TenantId];
			VendorId = v.VendorId;
			VendorInsuranceAlert = v.Vendor.AlertVendorInsurance(thresholdDays);
			VendorName = v.Vendor.Name;
			VendorNumber = v.Vendor.VendorNumber;
			VendorProNumber = v.ProNumber;
			VendorTrackingUrl = v.Vendor.TrackingUrl;

			DateCreated = shipment.DateCreated;
			DesiredPickupDate = shipment.DesiredPickupDate;
			
			DestinationCity = shipment.Destination.City;
			DestinationCountryName = shipment.Destination.Country.Name;
			DestinationName = shipment.Destination.Description;
			DestinationPostalCode = shipment.Destination.PostalCode;
			DestinationState = shipment.Destination.State;
			DestinationStreet1 = shipment.Destination.Street1;
			DestinationStreet2 = shipment.Destination.Street2;

			OriginCity = shipment.Origin.City;
			OriginCountryName = shipment.Origin.Country.Name;
			OriginName = shipment.Origin.Description;
			OriginPostalCode = shipment.Origin.PostalCode;
			OriginState = shipment.Origin.State;
			OriginStreet1 = shipment.Origin.Street1;
			OriginStreet2 = shipment.Origin.Street2;

			EstimatedDeliveryDate = shipment.EstimatedDeliveryDate;
			Prefix = shipment.Prefix == null ? string.Empty : shipment.Prefix.Code;
			ResellerAdditionId = shipment.ResellerAdditionId;
			ShipmentStatus = shipment.Status;
			TotalAmountDue = shipment.Charges.Sum(c => c.AmountDue);
			TotalCost = shipment.Charges.Sum(c => c.FinalBuy);
		}


		private void LoadCheckCalls()
		{
			_checkCalls = new List<CheckCall>();
			if (Id == default(long)) return;
			if (!CheckCallsPresent) return;
			const string query = @"SELECT * FROM CheckCall WHERE ShipmentId = @ShipmentId";
			var parameters = new Dictionary<string, object> {{"ShipmentId", Id}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_checkCalls.Add(new CheckCall(reader));
			Connection.Close();
		}
	}
}