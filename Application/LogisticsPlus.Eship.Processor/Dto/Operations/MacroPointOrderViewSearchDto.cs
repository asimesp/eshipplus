﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
    [Entity("MacroPointOrderViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "MacroPointOrderViewSearch")]
    public class MacroPointOrderViewSearchDto : TenantBase
    {
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [Property("IdNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string IdNumber { get; set; }

        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [Property("DateStopRequested", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateStopRequested { get; set; }

        [Property("Number", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Number { get; set; }

        [Property("NumberType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string NumberType { get; set; }

        [Property("OrderId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OrderId { get; set; }

        [Property("TrackCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal TrackCost { get; set; }

        [Property("TrackDurationHours", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int TrackDurationHours { get; set; }

        [Property("TrackIntervalMinutes", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int TrackIntervalMinutes { get; set; }

        [Property("TrackingStatusCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string TrackingStatusCode { get; set; }

        [Property("TrackingStatusDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string TrackingStatusDescription { get; set; }

        [Property("StartTrackDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime StartTrackDateTime { get; set; }

        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long UserId { get; set; }

        [Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Username { get; set; }

        [Property("UserFirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string UserFirstName { get; set; }

        [Property("UserLastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string UserLastName { get; set; }

        public MacroPointOrderViewSearchDto(DbDataReader reader)
        {
            Load(reader);
        }


        public override string ToString()
        {
            return
                new[]
                    {
                        IdNumber, NumberType, Number, DateCreated.FormattedLongDateAlt(),
                        StartTrackDateTime.FormattedLongDateAlt(),
                        DateStopRequested.FormattedLongDateAlt(), TrackCost.ToString("c2"),
                        TrackDurationHours.ToString(),
                        TrackIntervalMinutes.ToString(), TrackingStatusDescription, Username, UserFirstName,
                        UserLastName
                    }.TabJoin();
        }

        public static string Header()
        {
            return
                new[]
        			{
        				"Id Number", "Number Type", "Number", "Date Created", "Start Date",
        				"Stop Date","Cost","Track For (hrs)", "Update Every (min)", "Status", "Username", "User First Name", "User Last Name"
        			}.TabJoin();
        }
    }

}
