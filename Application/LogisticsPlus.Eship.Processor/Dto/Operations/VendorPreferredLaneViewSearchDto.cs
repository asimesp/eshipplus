﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
    [Entity("VendorPreferredLaneViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "VendorPreferredLaneViewSearchDto")]
    public class VendorPreferredLaneViewSearchDto : EntityBase
    {
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCity { get; set; }

        [Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginState { get; set; }

        [Property("OriginCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long OriginCountryId { get; set; }

        [Property("OriginCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCountryName { get; set; }

        [Property("OriginCountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCountryCode { get; set; }

        [Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPostalCode { get; set; }

        [Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCity { get; set; }

        [Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationState { get; set; }

        [Property("DestinationCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long DestinationCountryId { get; set; }

        [Property("DestinationCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCountryName { get; set; }

        [Property("DestinationCountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCountryCode { get; set; }

        [Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationPostalCode { get; set; }

        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorId { get; set; }

        [Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorName { get; set; }

        [Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorNumber { get; set; }


        public VendorPreferredLaneViewSearchDto(){}

        public VendorPreferredLaneViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}
    }
}
