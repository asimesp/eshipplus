﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
    [Serializable]
    [Entity("PendingLtlPickupsDashboardDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "PendingLtlPickupsDashboardViewSerch")]
    public class PendingLtlPickupsDashboardDto : EntityBase
    {
        private List<ShipmentItem> _items;
        private List<ShipmentService> _services;


        [Property("Id", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentNumber { get; set; }
        

        [Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long CustomerId { get; set; }

        [Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomerNumber { get; set; }

        [Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomerName { get; set; }


        [Property("Status", Column = "StatusText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Status { get; set; }
        

        [Property("OriginName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginName { get; set; }

        [Property("OriginStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginStreet1 { get; set; }

        [Property("OriginStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginStreet2 { get; set; }

        [Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCity { get; set; }

        [Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginState { get; set; }

        [Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPostalCode { get; set; }

        [Property("OriginCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCountryName { get; set; }


        [Property("DestinationName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationName { get; set; }

        [Property("DestinationStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationStreet1 { get; set; }

        [Property("DestinationStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationStreet2 { get; set; }

        [Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCity { get; set; }

        [Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationState { get; set; }

        [Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationPostalCode { get; set; }

        [Property("DestinationCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCountryName { get; set; }

        
        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorId { get; set; }

        [Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorNumber { get; set; }

        [Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorName { get; set; }

		[Property("ShipmentDispatchEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ShipmentDispatchEnabled { get; set; }


        [Property("DesiredPickupDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DesiredPickupDate { get; set; }

        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }


        [Property("OriginTerminalInfoId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long OriginTerminalInfoId { get; set; }

        [Property("OriginTerminalName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalName { get; set; }

        [Property("OriginTerminalCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalCity { get; set; }

        [Property("OriginTerminalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalCode { get; set; }

        [Property("OriginTerminalContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalContactName { get; set; }

        [Property("OriginTerminalContactTitle", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalContactTitle { get; set; }

        [Property("OriginTerminalCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalCountryName { get; set; }

        [Property("OriginTerminalEmail", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalEmail { get; set; }

        [Property("OriginTerminalFax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalFax { get; set; }

        [Property("OriginTerminalPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalPhone { get; set; }

        [Property("OriginTerminalPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalPostalCode { get; set; }

        [Property("OriginTerminalState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalState { get; set; }

        [Property("OriginTerminalStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalStreet1 { get; set; }

        [Property("OriginTerminalStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalStreet2 { get; set; }

        [Property("OriginTerminalTollFree", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalTollFree { get; set; }

        [Property("FaxStatus", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string FaxStatus { get; set; }

        [Property("IsGuaranteedDeliveryService", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsGuaranteedDeliveryService { get; set; }

        [Property("GuaranteedDeliveryServiceTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string GuaranteedDeliveryServiceTime { get; set; }


        [Property("OriginSpecialInstructions", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginSpecialInstructions { get; set; }

        [Property("ShipperReference", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipperReference { get; set; }

        [Property("OriginPrimaryContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPrimaryContactName { get; set; }

        [Property("OriginPrimaryContactPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPrimaryContactPhone { get; set; }

        [Property("EarlyPickup", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string EarlyPickup { get; set; }

        [Property("LatePickup", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LatePickup { get; set; }


        [Property("HazardousMaterial", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool HazardousMaterial { get; set; }


        [Property("AccessorialsPresent", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool AccessorialsPresent { get; set; }


        [Property("IsLocked", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsLocked { get; set; }

        [Property("LockedByUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LockedByUsername { get; set; }

        [Property("LockedByUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long LockedByUserId { get; set; }


        [Property("Prefix", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Prefix { get; set; }

        [Property("PrefixDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PrefixDescription { get; set; }

        [Property("AccountBucketCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string AccountBucketCode { get; set; }

        [Property("AccountBucketDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string AccountBucketDescription { get; set; }



        public List<ShipmentItem> Items
        {
            get
            {
                if (_items == null) LoadItems();
                return _items;
            }
            set { _items = value; }
        }

        public List<ShipmentService> Services
        {
            get
            {
                if (_services == null) LoadServices();
                return _services;
            }
            set { _services = value; }
        }


        public PendingLtlPickupsDashboardDto()
        {
            
        }

        public PendingLtlPickupsDashboardDto(DbDataReader reader)
		{
			Load(reader);
		}


        private void LoadItems()
        {
            _items = new List<ShipmentItem>();
            if (Id == default(long)) return;
            const string query = @"SELECT * FROM ShipmentItem WHERE ShipmentId = @ShipmentId";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }};
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _items.Add(new ShipmentItem(reader));
            Connection.Close();
        }

        private void LoadServices()
        {
            _services = new List<ShipmentService>();
            if (Id == default(long) || !AccessorialsPresent) return;
            const string query = @"SELECT * FROM ShipmentService WHERE ShipmentId = @ShipmentId";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }};
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _services.Add(new ShipmentService(reader));
            Connection.Close();
        }


    }
}
