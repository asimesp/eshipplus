﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
	[Serializable]
	[Entity("JobDashboardDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class JobDashboardDto : EntityBase
	{
		
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [Property("JobNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string JobNumber { get; set; }


        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public JobStatus Status { get; set; }

        [Property("StatusText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string StatusText { get; set; }

        [Property("ExternalReference1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ExternalReference1 { get; set; }

        
        [Property("ExternalReference2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ExternalReference2 { get; set; }

		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId { get; set; }


		[Property("CreatedByUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CreatedByUserId { get; set; }


		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }


		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }


		[Property("CreatedByUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CreatedByUsername { get; set; }

		[Property("CreatedByUserFirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CreatedByUserFirstName { get; set; }

		[Property("CreatedByUserLastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CreatedByUserLastName { get; set; }

		[Property("Shipments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Shipments { get; set; }

		[Property("ServiceTickets", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ServiceTickets { get; set; }

		[Property("LoadOrders", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LoadOrders { get; set; }


		public JobDashboardDto() { }

		public JobDashboardDto(DbDataReader reader)
		{
			Load(reader);
		}
		
    }
}
