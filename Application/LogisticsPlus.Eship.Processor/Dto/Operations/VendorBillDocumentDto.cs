﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
	[Serializable]
	[Entity("VendorBillDocumentDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class VendorBillDocumentDto: EntityBase	
	{

		[Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ReferenceNumber { get; set; }

		[Property("ReferenceType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ReferenceType { get; set; }

		[Property("LocationPath", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationPath { get; set; }

		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		  public VendorBillDocumentDto()
        {

        }

		  public VendorBillDocumentDto(DbDataReader reader)
        {
            Load(reader);
        }


	}
}
