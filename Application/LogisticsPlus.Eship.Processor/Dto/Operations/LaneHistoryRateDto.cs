﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Operations
{
	[Entity("LaneHistoryRateDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class LaneHistoryRateDto : EntityBase
	{
		private const string HistoricalBuySellQuery =
			"exec VendorHistoricalLaneBuySell @TenantId, @OriginPostalCode, @OriginCountryId, @DestinationPostalCode, @DestinationCountryId, @DesiredPickupDate, @Radius";

		private const string TenantId = "TenantId";
		private const string OriginPostalCode = "OriginPostalCode";
		private const string OriginCountryId = "OriginCountryId";
		private const string DestinationPostalCode = "DestinationPostalCode";
		private const string DestinationCountryId = "DestinationCountryId";
		private const string DesiredPickupDate = "DesiredPickupDate";
		private const string Radius = "Radius";

		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId { get; set; }

		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }

		[Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorName { get; set; }

		[Property("ServiceMode", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceMode ServiceMode { get; set; }

		public bool ContractRate { get; set; }
		public int RecordCount { get; set; }

		public decimal TotalLowestBuy { get { return Charges.Keys.Sum(k => Charges[k].LowestBuy); } }
		public decimal TotalLowestSell { get { return Charges.Keys.Sum(k => Charges[k].LowestSell); } }
		public decimal TotalHighestBuy { get { return Charges.Keys.Sum(k => Charges[k].HighestBuy); } }
		public decimal TotalHighestSell { get { return Charges.Keys.Sum(k => Charges[k].HighestSell); } }
		public decimal TotalAvgBuy { get { return Charges.Keys.Sum(k=> Charges[k].AverageBuy); } }
		public decimal TotalAvgSell { get { return Charges.Keys.Sum(k => Charges[k].AverageSell); } }



		public Dictionary<string, LaneHistoryChargeDto> Charges { get; set; }

		public LaneHistoryRateDto()
		{
			Charges = new Dictionary<string, LaneHistoryChargeDto>();
			ContractRate = false;
			RecordCount = 1;
		}

		public LaneHistoryRateDto(DbDataReader reader) : this()
		{
			Load(reader);
		}

		public LaneHistoryRateDto(Vendor vendor) : this()
		{
			VendorId = vendor.Id;
			VendorName = vendor.Name;
			VendorNumber = vendor.VendorNumber;
		}

		public List<LaneHistoryRateDto> VendorLaneHistory(long tenantId, LaneHistorySearchCriteria criteria)
		{
			var parameters = new Dictionary<string, object>();
			parameters[TenantId] = tenantId;
			parameters[OriginPostalCode] = criteria.OriginPostalCode;
			parameters[OriginCountryId] = criteria.OriginCountryId;
			parameters[DestinationPostalCode] = criteria.DestinationPostalCode;
			parameters[DestinationCountryId] = criteria.DestinationCountryId;
			parameters[DesiredPickupDate] = criteria.DesiredPickupDate;
			parameters[Radius] = criteria.Radius;

			// fetch historical buy sell
			var vendorBuySells = new Dictionary<string, LaneHistoryRateDto>();
			using (var reader = GetReader(HistoricalBuySellQuery, parameters))
				while (reader.Read())
				{
					var laneData = new LaneHistoryRateDto(reader);
					var chargeData = new LaneHistoryChargeDto(reader);

					var key = string.Format("{0}{1}", laneData.VendorId, laneData.ServiceMode);

					if (!vendorBuySells.ContainsKey(key)) vendorBuySells.Add(key, laneData);
					else vendorBuySells[key].RecordCount++;

					if (!vendorBuySells[key].Charges.ContainsKey(chargeData.ChargeCode))
						vendorBuySells[key].Charges.Add(chargeData.ChargeCode, chargeData);
				}
			Connection.Close();

			var laneRates = vendorBuySells.Values.ToList();

			return laneRates
				.OrderBy(lr => lr.TotalLowestBuy)
				.ThenBy(lr => lr.TotalHighestBuy)
				.ThenBy(lr => lr.TotalLowestSell)
				.ThenBy(lr => lr.TotalHighestSell)
				.ThenBy(lr => lr.VendorName)
				.ToList();
		}
	}
}
