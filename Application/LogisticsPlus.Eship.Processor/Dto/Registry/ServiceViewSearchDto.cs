﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Registry
{
    [Serializable]
	[Entity("ServiceViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "ServiceViewSearch")]
	public class ServiceViewSearchDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

        [Property("TenantId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long TenantId { get; set; }

		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[Property("Category", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceCategory Category { get; set; }

	    [Property("Project44Code", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Project44Code { get; set; }

		[Property("DispatchFlag", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceDispatchFlag DispatchFlag { get; set; }

		[Property("ApplicableAtPickup", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ApplicableAtPickup { get; set; }

		[Property("ApplicableAtDelivery", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ApplicableAtDelivery { get; set; }

		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId { get; set; }

		[Property("ChargeCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCode { get; set; }

		[Property("ChargeCodeDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCodeDescription { get; set; }

		public ServiceViewSearchDto() { }

		public ServiceViewSearchDto(long id)
		{
			Id = id;
			if (Id == default(long)) return;
			Load();
		}

		public ServiceViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}

		public ServiceViewSearchDto(Service service)
		{
			Id = service.Id;
			TenantId = service.TenantId;
			Code = service.Code;
			Description = service.Description;
			Category = service.Category;
			ChargeCodeId = service.ChargeCodeId;
			ChargeCode = service.ChargeCodeId == default(long) ? string.Empty : service.ChargeCode.Code;
			ChargeCodeDescription = service.ChargeCodeId == default(long) ? string.Empty : service.ChargeCode.Description;
			DispatchFlag = service.DispatchFlag;
			ApplicableAtDelivery = service.ApplicableAtDelivery;
			ApplicableAtPickup = service.ApplicableAtPickup;
		}

		public List<ServiceViewSearchDto> RetrieveVendorServices(Vendor vendor)
		{
			var services = new List<ServiceViewSearchDto>();
			const string query = @"SELECT [Service].*, ChargeCode.Code AS 'ChargeCode', ChargeCode.[Description] AS 'ChargeCodeDescription' 
									FROM [Service], VendorService, ChargeCode 
									WHERE [Service].[Id] = VendorService.ServiceId
											AND [Service].ChargeCodeId = ChargeCode.Id 
											AND VendorService.VendorId = @VendorId 
											AND VendorService.TenantId = @TenantId";

			var parameters = new Dictionary<string, object> {{"TenantId", vendor.TenantId}, {"VendorId", vendor.Id}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					services.Add(new ServiceViewSearchDto(reader));
			Connection.Close();
			return services;
		}

		public List<ServiceViewSearchDto> RetrieveVendorServices(PendingVendor pendingVendor)
		{
			var services = new List<ServiceViewSearchDto>();
			const string query = @"SELECT [Service].*, ChargeCode.Code AS 'ChargeCode', ChargeCode.[Description] AS 'ChargeCodeDescription' 
									FROM [Service], PendingVendorService, ChargeCode 
									WHERE [Service].[Id] = PendingVendorService.ServiceId
											AND [Service].ChargeCodeId = ChargeCode.Id 
											AND PendingVendorService.PendingVendorId = @PendingVendorId 
											AND PendingVendorService.TenantId = @TenantId";

			var parameters = new Dictionary<string, object> { { "TenantId", pendingVendor.TenantId }, { "PendingVendorId", pendingVendor.Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					services.Add(new ServiceViewSearchDto(reader));
			Connection.Close();
			return services;
		}

		public List<ServiceViewSearchDto> RetrieveAddressBookServices(AddressBook addressBook)
		{
			var services = new List<ServiceViewSearchDto>();

			const string query = @"SELECT  [Service].*, ChargeCode.Code AS 'ChargeCode', ChargeCode.[Description] AS 'ChargeCodeDescription'
									FROM [Service], AddressBookService, ChargeCode 
									WHERE [Service].Id = AddressBookService.ServiceId
											AND [Service].ChargeCodeId = ChargeCode.Id
											AND AddressBookService.AddressBookId = @AddressBookId
											AND AddressBookService.TenantId = @TenantId";

			var parameters = new Dictionary<string, object> { { "TenantId", addressBook.TenantId }, { "AddressBookId", addressBook.Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					services.Add(new ServiceViewSearchDto(reader));
			Connection.Close();
			return services;
		}

        public List<ServiceViewSearchDto> RetrieveShoppedRateServices(ShoppedRateViewSearchDto shoppedRate)
        {
            var services = new List<ServiceViewSearchDto>();

            const string query = @"SELECT  [Service].*, ChargeCode.Code AS 'ChargeCode', ChargeCode.[Description] AS 'ChargeCodeDescription'
									FROM [Service], ShoppedRateService, ChargeCode 
									WHERE [Service].ChargeCodeId = ChargeCode.Id
											AND ShoppedRateService.ShoppedRateId = @ShoppedRateId
											AND ShoppedRateService.TenantId = @TenantId
											AND ShoppedRateService.ServiceId = [Service].Id";

            var parameters = new Dictionary<string, object> { { "TenantId", shoppedRate.TenantId }, { "ShoppedRateId", shoppedRate.Id } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    services.Add(new ServiceViewSearchDto(reader));
            Connection.Close();
            return services;
        }
	}
}
