﻿using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Registry
{
	[Entity("DocumentPrefixMapViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "DocumentPrefixMapViewSearch")]
	public class DocumentPrefixMapViewSearchDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("PrefixId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PrefixId { get; set; }

		[Property("DocumentTemplateId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DocumentTemplateId { get; set; }

		[Property("Prefix", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "PrefixCode")]
		public string Prefix { get; set; }

		[Property("PrefixDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "PrefixDescription")]
		public string PrefixDescription { get; set; }

		[Property("Template", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "TemplateCode")]
		public string Template { get; set; }

		[Property("TemplateDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "TemplateDescription")]
		public string TemplateDescription { get; set; }

		public DocumentPrefixMapViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}
