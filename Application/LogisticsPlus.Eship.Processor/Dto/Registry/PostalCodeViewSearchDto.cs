﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Registry
{
    [Serializable]
	[Entity("PostalCodeViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "PostalCodeViewSearch")]
	public class PostalCodeViewSearchDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[Property("City", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string City { get; set; }

		[Property("CityAlias", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CityAlias { get; set; }

		[Property("State", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string State { get; set; }

		[Property("CountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CountryId { get; set; }

		[Property("CountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CountryName { get; set; }

		[Property("CountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CountryCode { get; set; }

		[Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Primary { get; set; }

		[Property("Latitude", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double Latitude { get; set; }

		[Property("Longitude", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double Longitude { get; set; }

		public PostalCodeViewSearchDto() { }

		public PostalCodeViewSearchDto(long id)
		{
			Id = id;
			if (Id == default(long)) return;
			Load();
		}

		public PostalCodeViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}
