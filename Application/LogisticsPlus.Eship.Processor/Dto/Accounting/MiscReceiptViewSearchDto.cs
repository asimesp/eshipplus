﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
    [Serializable]
    [Entity("MiscReceiptViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "MiscReceiptViewSearch")]
    public class MiscReceiptViewSearchDto : EntityBase
    {
        private List<MiscReceiptApplication> _miscReceiptApplications;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShipmentId { get; set; }

        [Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentNumber { get; set; }

        [Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long LoadOrderId { get; set; }

        [Property("LoadOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LoadOrderNumber { get; set; }

        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long UserId { get; set; }

        [Property("UserUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string UserUsername { get; set; }

        [Property("UserFirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string UserFirstName { get; set; }

        [Property("UserLastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string UserLastName { get; set; }

        [Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long CustomerId { get; set; }

        [Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomerNumber { get; set; }

        [Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomerName { get; set; }

        [Property("AmountPaid", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal AmountPaid { get; set; }

        [Property("PaymentDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime PaymentDate { get; set; }

        [Property("GatewayTransactionId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string GatewayTransactionId { get; set; }

        [Property("PaymentGatewayType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PaymentGatewayType { get; set; }

        [Property("Reversal", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Reversal { get; set; }

        [Property("MiscReceiptApplicationsPresent", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool MiscReceiptApplicationsPresent { get; set; }

        [Property("OriginalMiscReceiptId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long OriginalMiscReceiptId { get; set; }

        [Property("NameOnCard", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string NameOnCard { get; set; }

        [Property("PaymentProfileId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PaymentProfileId { get; set; }

        [Property("AppliableAmount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal AppliableAmount { get; set; }


        public List<MiscReceiptApplication> MiscReceiptApplications
        {
            get
            {
                if (_miscReceiptApplications == null) LoadMiscReceiptApplications();
                return _miscReceiptApplications;
            }
            set { _miscReceiptApplications = value; }
        }


        public MiscReceiptViewSearchDto()
		{
			
		}

        public MiscReceiptViewSearchDto(DbDataReader reader)
		{
            Load(reader);
		}


        private void LoadMiscReceiptApplications()
        {
            _miscReceiptApplications = new List<MiscReceiptApplication>();
            if (!MiscReceiptApplicationsPresent) return;
            const string query = @"SELECT * FROM MiscReceiptApplication WHERE MiscReceiptId = @MiscReceiptId";
            var parameters = new Dictionary<string, object> { { "MiscReceiptId", Id } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _miscReceiptApplications.Add(new MiscReceiptApplication(reader));
            Connection.Close();
        }

        public override string ToString()
        {
            return new[]
                    {
                        ShipmentNumber, UserUsername, UserFirstName, UserLastName, CustomerNumber, CustomerName, 
                        GatewayTransactionId, AmountPaid.ToString(), PaymentDate.ToString(),PaymentGatewayType, 
                        Reversal.ToString() 
                    }.TabJoin();
        }

        public static string Header()
        {
            return new[]
        			{
        				"Shipment Number", "User Username", "User First Name", "User Last Name", "Customer Number",
                        "Customer Name", "Gateway Transaction Id", "Amount Paid", "Payment Date", "Payment Gateway Type",
                        "Reversal"
        			}.TabJoin();
        }
    }
}
