﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{

    [Serializable]
    [Entity("ChargeViewSearchDto", ReadOnly = false, Source = EntitySource.TableView)]
    public class ChargeViewSearchDto : Charge
    {
        [Property("Id", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }
        
        [EnableSnapShot("ChargeType", Description = "Charge Type")]
        [Property("ChargeType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ChargeType ChargeType { get; set; }

        [EnableSnapShot("Number", Description = "Identifier Number")]
        [Property("Number", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Number { get; set; }

        [EnableSnapShot("ParentId", Description = "ParentId")]
        [Property("ParentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ParentId { get; set; }

        [EnableSnapShot("Status", Description = "Status")]
        [Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ChargeStatus Status { get; set; }
        
        [EnableSnapShot("VendorId", Description = "Vendor Id")]
        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorId { get; set; }
        
        [EnableSnapShot("VendorNumber", Description = "Vendor Number")]
        [Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorNumber { get; set; }

        [EnableSnapShot("VendorName", Description = "Vendor Name")]
        [Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorName { get; set; }


        [EnableSnapShot("VendorBillId", Description = "Vendor Bill Id")]
        [Property("VendorBillId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorBillId { get; set; }

        [EnableSnapShot("DisputedId", Description = "Vendor Dispute Id")]
        [Property("DisputedId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long DisputedId { get; set; }

        [EnableSnapShot("VendorLocationId", Description = "Vendor Location Id")]
        [Property("VendorLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorLocationId { get; set; }

        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public List<Document> Documents { get; set; }

		public string DisputeComments { get; set; }



        public ChargeViewSearchDto()
        {

        }
        
        public ChargeViewSearchDto(DbDataReader reader)
        {
            Load(reader);
        }
    }
}
