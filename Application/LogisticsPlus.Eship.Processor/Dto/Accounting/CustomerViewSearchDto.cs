﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
    [Serializable]
    [Entity("CustomerViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "CustomerViewSearch")]
    public class CustomerViewSearchDto : EntityBase
    {
        [Property("Id", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomerNumber { get; set; }

		[Property("CustomerTypeText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerTypeText { get; set; }

        [Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Name { get; set; }

        [Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Active { get; set; }

        [Property("AdditionalBillOfLadingText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string AdditionalBillOfLadingText { get; set; }
        
        [Property("CustomVendorSelectionMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomVendorSelectionMessage { get; set; }

        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }
        
        [Property("InvalidPurchaseOrderNumberMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string InvalidPurchaseOrderNumberMessage { get; set; }

        [Property("ShipperBillOfLadingPrefix", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipperBillOfLadingPrefix { get; set; }

        [Property("ShipperBillOfLadingSuffix", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipperBillOfLadingSuffix { get; set; }
        
        [Property("TierName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string TierName { get; set; }

        [Property("TierNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string TierNumber { get; set; }

        [Property("AllowLocationContactNotification", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool AllowLocationContactNotification { get; set; }


        public CustomerViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}
    }
}
