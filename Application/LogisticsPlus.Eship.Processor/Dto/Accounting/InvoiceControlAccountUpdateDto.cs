﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Processor.Validation;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
	[Entity("InvoiceControlAccountUpdateDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class InvoiceControlAccountUpdateDto : EntityBase
	{
		[Property("InvoiceId", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long InvoiceId { get; internal set; }

		[Property("InvoiceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string InvoiceNumber { get; set; }

		[Property("InvoiceDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime InvoiceDate { get; set; }

		[Property("CustomerId", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long CustomerId { get; set; }

		[Property("InvoiceCustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string InvoiceCustomerNumber { get; set; }

		[Property("InvoiceCustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string InvoiceCustomerName { get; set; }

		[Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipmentNumber { get; set; }

		[Property("ShipmentOrigin", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipmentOrigin { get; set; }

		[Property("ShipmentDestination", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipmentDestination { get; set; }

		[Property("UserFirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string UserFirstName { get; set; }

		[Property("UserLastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string UserLastName { get; set; }

		[Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Username { get; set; }

        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long UserId { get; set; }

		public ValidationMessage Message { get; set; }
		public bool IsError { get { return Message != null && Message.Type == ValidateMessageType.Error; } }
		public string MessageText { get { return Message != null ? Message.Message : string.Empty; } }

		public InvoiceControlAccountUpdateDto() { }

		public InvoiceControlAccountUpdateDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}
