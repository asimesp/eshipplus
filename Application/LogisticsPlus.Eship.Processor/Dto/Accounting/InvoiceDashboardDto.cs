﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
    [Serializable]
	[Entity("InvoiceDashboardDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class InvoiceDashboardDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; internal set; }

		[Property("InvoiceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string InvoiceNumber { get; set; }

		[Property("InvoiceDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime InvoiceDate { get; set; }

		[Property("PostDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime PostDate { get; set; }

		[Property("Posted", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Posted { get; set; }


		[Property("DueDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DueDate { get; set; }

		[Property("ExportDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExportDate { get; set; }

		[Property("Exported", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Exported { get; set; }

		[Property("InvoiceType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public InvoiceType InvoiceType { get; set; }

		[Property("PaidAmount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal PaidAmount { get; set; }

		[Property("SpecialInstruction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SpecialInstruction { get; set; }

		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }


		[Property("LocationNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationNumber { get; set; }

		[Property("LocationStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationStreet1 { get; set; }

		[Property("LocationStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationStreet2 { get; set; }

		[Property("LocationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationCity { get; set; }

		[Property("LocationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationState { get; set; }

		[Property("LocationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationPostalCode { get; set; }

		[Property("LocationCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationCountryName { get; set; }


		[Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Username { get; set; }

		[Property("AmountDue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AmountDue { get; set; }

		[Property("Shipments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Shipments { get; set; }

		[Property("ServiceTickets", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ServiceTickets { get; set; }

		[Property("OriginalInvoiceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginalInvoiceNumber { get; set; }

        [Property("InvoiceTypeText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string InvoiceTypeText { get; set; }


        [Property("PrefixCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PrefixCode { get; set; }

        [Property("PrefixDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PrefixDescription { get; set; }

		
		public InvoiceDashboardDto() { }

		public InvoiceDashboardDto(DbDataReader reader)
		{
			Load(reader);
		}

		public InvoiceDashboardDto(Invoice invoice)
		{
			Id = invoice.Id;
			InvoiceNumber = invoice.InvoiceNumber;
			InvoiceDate = invoice.InvoiceDate;
			PostDate = invoice.PostDate;
			Posted = invoice.Posted;
			DueDate = invoice.DueDate;
			ExportDate = invoice.ExportDate;
			Exported = invoice.Exported;
			InvoiceType = invoice.InvoiceType;
			PaidAmount = invoice.PaidAmount;
			SpecialInstruction = invoice.SpecialInstruction;
			CustomerId = invoice.CustomerLocation == null ? default(long) : invoice.CustomerLocation.Customer.Id;
			CustomerNumber = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.Customer.CustomerNumber;
			CustomerName = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.Customer.Name;
			LocationNumber = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.LocationNumber;
			LocationStreet1 = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.Street1;
			LocationStreet2 = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.Street2;
			LocationCity = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.City;
			LocationState = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.State;
			LocationPostalCode = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.PostalCode;
			LocationCountryName = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.Country.Name;
			Username = invoice.User.Username;
			AmountDue = invoice.Details.Any() ? invoice.Details.Sum(d => d.AmountDue) : 0;
			Shipments = invoice.Details.Any()
			            	? string.Join(", ", invoice.Details
			            	                   	.Where(d => d.ReferenceType == DetailReferenceType.Shipment)
			            	                   	.Select(d => d.ReferenceNumber)
			            	                   	.Distinct()
			            	                   	.ToArray())
			            	: string.Empty;
			ServiceTickets = invoice.Details.Any()
			                 	? string.Join(", ", invoice.Details
			                 	                   	.Where(d => d.ReferenceType == DetailReferenceType.ServiceTicket)
			                 	                   	.Select(d => d.ReferenceNumber)
			                 	                   	.Distinct()
			                 	                   	.ToArray())
			                 	: string.Empty;
		    OriginalInvoiceNumber = invoice.InvoiceType == InvoiceType.Credit ||
		                            invoice.InvoiceType == InvoiceType.Supplemental && invoice.OriginalInvoice != null
		                                ? invoice.OriginalInvoice.InvoiceNumber
		                                : string.Empty;
		}
	}
}