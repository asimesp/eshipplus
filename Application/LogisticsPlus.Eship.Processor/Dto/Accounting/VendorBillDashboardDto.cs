﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
    [Serializable]
	[Entity("VendorBillDashboardDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class VendorBillDashboardDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; internal set; }

		[Property("DocumentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DocumentNumber { get; set; }

		[Property("OriginalInvoiceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginalInvoiceNumber { get; set; }

		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[Property("DocumentDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DocumentDate { get; set; }

		[Property("PostDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime PostDate { get; set; }

		[Property("Posted", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Posted { get; set; }

		[Property("ExportDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExportDate { get; set; }

		[Property("Exported", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Exported { get; set; }

		[Property("BillType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public InvoiceType BillType { get; set; }

		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }

		[Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorName { get; set; }

		[Property("LocationNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationNumber { get; set; }

		[Property("LocationStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationStreet1 { get; set; }

		[Property("LocationStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationStreet2 { get; set; }

		[Property("LocationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationCity { get; set; }

		[Property("LocationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationState { get; set; }

		[Property("LocationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationPostalCode { get; set; }

		[Property("LocationCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationCountryName { get; set; }

		[Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Username { get; set; }

		[Property("AmountDue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public Decimal AmountDue { get; set; }

		[Property("Shipments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Shipments { get; set; }

	    [Property("ServiceTickets", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
	    public string ServiceTickets { get; set; }

	    [Property("BillTypeText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BillTypeText { get; set; }

		public VendorBillDashboardDto() { }

		public VendorBillDashboardDto(DbDataReader reader)
		{
			Load(reader);
		}

		public VendorBillDashboardDto(VendorBill bill)
		{
			Id = bill.Id;
            DocumentNumber = bill.DocumentNumber;
            OriginalInvoiceNumber = bill.BillType == InvoiceType.Credit && bill.ApplyToDocument != null
			                        	? bill.ApplyToDocument.DocumentNumber
			                        	: string.Empty;
            DateCreated = bill.DateCreated; 
            DocumentDate = bill.DocumentDate;
            PostDate = bill.PostDate;
            Posted = bill.Posted;
            ExportDate = bill.ExportDate;
            Exported = bill.Exported;
            BillType = bill.BillType;
            VendorNumber = bill.VendorLocation == null ? string.Empty : bill.VendorLocation.Vendor.VendorNumber;
            VendorName = bill.VendorLocation == null ? string.Empty : bill.VendorLocation.Vendor.Name;
            LocationNumber = bill.VendorLocation == null ? string.Empty : bill.VendorLocation.LocationNumber;
            LocationStreet1 = bill.VendorLocation == null ? string.Empty : bill.VendorLocation.Street1;
            LocationStreet2 = bill.VendorLocation == null ? string.Empty : bill.VendorLocation.Street2;
            LocationCity = bill.VendorLocation == null ? string.Empty : bill.VendorLocation.City;
            LocationState = bill.VendorLocation == null ? string.Empty : bill.VendorLocation.State;
            LocationPostalCode = bill.VendorLocation == null ? string.Empty : bill.VendorLocation.PostalCode;
            LocationCountryName = bill.VendorLocation == null ? string.Empty : bill.VendorLocation.Country.Name;
		    Username = bill.User.Username;
            AmountDue = bill.Details.Any() ? bill.Details.Sum(d => d.TotalAmount) : 0;
            Shipments = bill.Details.Any()
                            ? string.Join(", ", bill.Details
                                                .Where(d => d.ReferenceType == DetailReferenceType.Shipment)
                                                .Select(d => d.ReferenceNumber)
                                                .Distinct()
                                                .ToArray())
                            : string.Empty;
            ServiceTickets = bill.Details.Any()
                                ? string.Join(", ", bill.Details
                                                    .Where(d => d.ReferenceType == DetailReferenceType.ServiceTicket)
                                                    .Select(d => d.ReferenceNumber)
                                                    .Distinct()
                                                    .ToArray())
                                : string.Empty;
		}
	}
}
