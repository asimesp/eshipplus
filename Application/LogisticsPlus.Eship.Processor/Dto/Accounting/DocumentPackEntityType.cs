﻿namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
	public enum DocumentPackEntityType
	{
		Shipment = 0,
		Invoice
	}
}
