﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
	[Entity("AuditVendorBillDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class AuditVendorBillDto : EntityBase
	{
		[Property("VendorBillId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long VendorBillId { get; set; }

		[Property("DocumentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DocumentNumber { get; set; }

		[Property("DocumentDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DocumentDate { get; set; }

		[Property("PostDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime PostDate { get; set; }

		[Property("Posted", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Posted { get; set; }

		[Property("BillType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public InvoiceType BillType { get; set; }

        [Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorName { get; set; }
        
        [Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }

        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId { get; set; }

		[Property("LocationNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationNumber { get; set; }

		[Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Quantity { get; set; }

		[Property("UnitBuy", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UnitBuy { get; set; }

		[Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ReferenceNumber { get; set; }

		[Property("ReferenceType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public DetailReferenceType ReferenceType { get; set; }

		[Property("ChargeCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCode { get; set; }

		[Property("ChargeCodeDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCodeDescription { get; set; }

		[Property("AccountBucketCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AccountBucketCode { get; set; }

		[Property("AccountBucketDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AccountBucketDescription { get; set; }

		public decimal AmountDue { get { return UnitBuy*Quantity; } }

		public AuditVendorBillDto() { }

		public AuditVendorBillDto(DbDataReader reader)
		{
			Load(reader);
		}


		public List<AuditVendorBillDto> FetchAuditVendorBillDtos(string referenceNumber, DetailReferenceType referenceType, long tenantId)
		{
			var vendorBills = new List<AuditVendorBillDto>();

			const string query =
                @"SELECT 
					vb.Id 'VendorBillId',
					vb.DocumentDate,
					vb.DocumentNumber, 
					vb.Posted,
					vb.PostDate,
					vb.BillType,
					v.Name as 'VendorName',
					v.VendorNumber,
					v.Id as 'VendorId',
					vl.LocationNumber,
					d.Quantity, 
					d.UnitBuy, 
					d.ReferenceNumber, 
					d.ReferenceType,
					cc.Code 'ChargeCode',
					cc.[Description] 'ChargeCodeDescription',
					ab.Code 'AccountBucketCode',
					ab.[Description] 'AccountBucketDescription'
				FROM VendorBill vb
					INNER JOIN VendorBillDetail d ON d.VendorBillId = vb.Id
					INNER JOIN VendorLocation vl ON vl.Id = vb.VendorLocationId
					INNER JOIN Vendor v ON v.Id = vl.VendorId
					INNER JOIN ChargeCode cc ON cc.Id = d.ChargeCodeId
					INNER JOIN AccountBucket ab ON ab.Id = d.AccountBucketId
				WHERE
					 vb.TenantId = @TenantId
						AND d.ReferenceNumber = @ReferenceNumber 
						AND d.ReferenceType = @ReferenceType";

			var parameters = new Dictionary<string, object>
			                 	{
				                 	{"TenantId", tenantId},
				                 	{"ReferenceNumber", referenceNumber},
				                 	{"ReferenceType", referenceType},
			                 	};

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					vendorBills.Add(new AuditVendorBillDto(reader));
			Connection.Close();
			return vendorBills;
		}
	}
}