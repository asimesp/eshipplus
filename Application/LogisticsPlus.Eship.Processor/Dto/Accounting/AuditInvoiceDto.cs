﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
	[Entity("AuditInvoiceDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class AuditInvoiceDto : EntityBase
	{
		[Property("InvoiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long InvoiceId { get; set; }

		[Property("InvoiceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string InvoiceNumber { get; set; }

		[Property("InvoiceDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime InvoiceDate { get; set; }

		[Property("PostDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime PostDate { get; set; }

		[Property("DueDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DueDate { get; set; }

		[Property("Posted", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Posted { get; set; }

		[Property("InvoiceType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public InvoiceType InvoiceType { get; set; }

        [Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CustomerName { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }
        
        [Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long CustomerId { get; set; }

		[Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Quantity { get; set; }

		[Property("UnitSell", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UnitSell { get; set; }

		[Property("UnitDiscount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UnitDiscount { get; set; }

		[Property("PaidAmount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal PaidAmount { get; set; }

		[Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ReferenceNumber { get; set; }

		[Property("ReferenceType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public DetailReferenceType ReferenceType { get; set; }

		[Property("ChargeCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCode { get; set; }

		[Property("ChargeCodeDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCodeDescription { get; set; }

		[Property("AccountBucketCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AccountBucketCode { get; set; }

		[Property("AccountBucketDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AccountBucketDescription { get; set; }

		public decimal AmountDue
		{
			get { return (UnitSell - UnitDiscount)*Quantity; }
		}


		public AuditInvoiceDto() { }

		public AuditInvoiceDto(DbDataReader reader)
		{
			Load(reader);
		}


		public List<AuditInvoiceDto> FetchAuditInvoiceDtos(string referenceNumber, DetailReferenceType referenceType, long tenantId)
		{
			var invoices = new List<AuditInvoiceDto>();

			const string query =
                @"SELECT 
					i.Id 'InvoiceId',
					i.InvoiceDate,
					i.InvoiceNumber, 
					i.Posted,
					i.PostDate,
					i.DueDate,
					i.PaidAmount,
					i.InvoiceType,
					c.Name as 'CustomerName',
					c.CustomerNumber,
					c.Id as 'CustomerId',
					d.Quantity, 
					d.UnitSell, 
					d.UnitDiscount, 
					d.ReferenceNumber, 
					d.ReferenceType,
					cc.Code 'ChargeCode',
					cc.[Description] 'ChargeCodeDescription',
					ab.Code 'AccountBucketCode',
					ab.[Description] 'AccountBucketDescription'
				FROM Invoice i
					INNER JOIN InvoiceDetail d ON d.InvoiceId = i.Id
					INNER JOIN CustomerLocation cl ON cl.Id = i.CustomerLocationId
					INNER JOIN Customer c ON c.Id = cl.CustomerId
					INNER JOIN ChargeCode cc ON cc.Id = d.ChargeCodeId
					INNER JOIN AccountBucket ab ON ab.Id = d.AccountBucketId
				WHERE
					 i.TenantId = @TenantId
						AND d.ReferenceNumber = @ReferenceNumber 
						AND d.ReferenceType = @ReferenceType";

			var parameters = new Dictionary<string, object>
			                 	{
				                 	{"TenantId", tenantId},
				                 	{"ReferenceNumber", referenceNumber},
				                 	{"ReferenceType", referenceType},
			                 	};

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					invoices.Add(new AuditInvoiceDto(reader));
			Connection.Close();
			return invoices;
		}
	}
}