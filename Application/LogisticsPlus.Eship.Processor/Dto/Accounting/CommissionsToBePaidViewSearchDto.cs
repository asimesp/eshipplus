﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
	[Entity("CommissionsToBePaidViewSearchDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class CommissionsToBePaidViewSearchDto : EntityBase
	{
		[Property("ReferenceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ReferenceId { get; set; }

		[Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ReferenceNumber { get; set; }

        [Property("ShipmentCreatedByUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentCreatedByUsername { get; set; }

        [Property("ShipmentCreatedByFirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentCreatedByFirstName { get; set; }

        [Property("ShipmentCreatedByLastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentCreatedByLastName { get; set; }

        [Property("ShipmentDateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime ShipmentDateCreated { get; set; }

        [Property("ShipmentDateCreatedText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentDateCreatedText { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }

		[Property("TotalCommission", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TotalCommission { get; set; }

		[Property("TotalReversals", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TotalReversals { get; set; }

        [Property("AdditionalEntityTotalCommission", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal AdditionalEntityTotalCommission { get; set; }

        [Property("AdditionalEntityTotalReversals", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal AdditionalEntityTotalReversals { get; set; }

		[Property("SalesRepId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false, Column = "SalesRepresentativeId")]
		public long SalesRepId { get; set; }

		[Property("SalesRepNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "SalesRepresentativeNumber")]
		public string SalesRepNumber { get; set; }

		[Property("SalesRepName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "SalesRepresentativeName")]
		public string SalesRepName { get; set; }

		[Property("SalesRepCompanyName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "SalesRepresentativeCompanyName")]
		public string SalesRepCompanyName { get; set; }

		[Property("SalesRepCommPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false, Column = "SalesRepresentativeCommissionPercent")]
		public decimal SalesRepCommPercent { get; set; }

		[Property("SalesRepCommCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false, Column = "SalesRepresentativeCommissionCeiling")]
		public decimal SalesRepCommCeiling { get; set; }

		[Property("SalesRepCommFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false, Column = "SalesRepresentativeCommissionFloor")]
		public decimal SalesRepCommFloor { get; set; }

		[Property("SalesRepAddlEntityCommPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false, Column = "SalesRepresentativeAddlEntityCommissionPercent")]
		public decimal SalesRepAddlEntityCommPercent { get; set; }

		[Property("SalesRepAddlEntityName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "SalesRepresentativeAddlEntityName")]
		public string SalesRepAddlEntityName { get; set; }

		[Property("EstimatedCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal EstimatedCost { get; set; }

		[Property("NetActualCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal NetActualCost { get; set; }

		[Property("EstimatedRevenue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal EstimatedRevenue { get; set; }

		[Property("NetActualRevenue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal NetActualRevenue { get; set; }

		[Property("InvoicesPaid", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool InvoicesPaid { get; set; }


		public decimal EstimatedProfit
		{
			get { return EstimatedRevenue - EstimatedCost; }
		}
		public decimal ActualProfit
		{
			get { return NetActualRevenue - NetActualCost; }
		}

	    public decimal NetAdditionalEntityCommPayments
	    {
            get { return AdditionalEntityTotalCommission - AdditionalEntityTotalReversals; }
	    }
		public decimal NetCommPayments
		{
			get { return TotalCommission - TotalReversals; }
		}
		
		public decimal EstSalesRepAddlEntityComm 
		{
			get { return EstimatedProfit*SalesRepAddlEntityCommPercent/100; }
		}
		public decimal ActSalesRepAddlEntityComm
		{
			get { return ActualProfit*SalesRepAddlEntityCommPercent/100; }
		}
		public decimal EstSalesRepComm
		{
			get
			{
				var comm = EstimatedProfit*SalesRepCommPercent/100;
				return comm > SalesRepCommCeiling ? SalesRepCommCeiling : comm < SalesRepCommFloor ? SalesRepCommFloor : comm;
			}
		}
		public decimal ActSalesRepComm
		{
			get
			{
				var comm = ActualProfit * SalesRepCommPercent/100;
				return comm > SalesRepCommCeiling ? SalesRepCommCeiling : comm < SalesRepCommFloor ? SalesRepCommFloor : comm;
			}
		}

		public decimal EstOveralComm
		{
			get { return EstSalesRepAddlEntityComm + EstSalesRepComm; }
		}
		public decimal ActOveralComm
		{
			get { return ActSalesRepAddlEntityComm + ActSalesRepComm; }
		}

		public CommissionsToBePaidViewSearchDto() { }

		public CommissionsToBePaidViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}


        public override string ToString()
        {
            return
                new[]
                    {
                        ReferenceNumber, ShipmentCreatedByUsername, ShipmentCreatedByFirstName,
                        ShipmentCreatedByLastName, ShipmentDateCreated.FormattedLongDate(), ShipmentDateCreatedText ,CustomerNumber, CustomerName, SalesRepNumber,
                        SalesRepName, SalesRepCompanyName, InvoicesPaid ? "Yes" : "No", SalesRepCommPercent.ToString(),
                        SalesRepCommCeiling.ToString("c2"), SalesRepCommFloor.ToString("c2"),
                        SalesRepAddlEntityCommPercent.ToString(), EstimatedRevenue.ToString("c2"),
                        EstimatedCost.ToString("c2"), EstSalesRepComm.ToString("c2"), EstSalesRepAddlEntityComm.ToString("c2"),
                        EstOveralComm.ToString("c2"), NetActualRevenue.ToString("c2"), NetActualCost.ToString("c2"),
                        ActSalesRepComm.ToString("c2"), ActSalesRepAddlEntityComm.ToString("c2"),
                        ActOveralComm.ToString("c2"), AdditionalEntityTotalCommission.ToString("c2"), AdditionalEntityTotalReversals.ToString("c2"),
                        NetAdditionalEntityCommPayments.ToString("c2"), TotalCommission.ToString("c2"), TotalReversals.ToString("c2"), NetCommPayments.ToString("c2")
                    }.TabJoin();
        }

        public static string Header()
        {
            return
                new[]
                    {
                        "Shipment Number", "Shipment Created By Username", "Shipment Created By First Name",
                        "Shipment Created By Last Name", "Shipment Date Created", "Shipment Date Created Text" ,"Customer Number", "Customer Name", "Sales Rep Number",
                        "Sales Rep Name", "Sales Rep Company Name",
                        "All Invoices Paid", "Sales Rep Comm.", "Sales Rep Comm. Max", "Sales Rep Comm. Min", "Add'l Entity Comm.", "Estimated Revenue",
                        "Estimated Cost", "Estimated Sales Rep Comm.", "Estimated sales Rep Add'l Entity Comm.", "Estimated Overall Comm.",
                        "Actual Revenue", "Actual Cost", "Actual Sales Rep Comm.", "Actual Sales Rep Add'l Entity Comm."
                        , "Actual Overall Comm.", "Add'l Entity Comm. Paid", "Add'l Entity Comm. Reversed", "Net Add'l Entity Comm. Paid" , 
                        "Comm. Paid", "Comm. Reversed", "Net Comm. Paid"
                    }.TabJoin();
        }
	}
}
