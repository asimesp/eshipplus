﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
	[Entity("DocumentPackEntityDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "")]
	public class DocumentPackEntityDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("Number", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Number { get; set; }

		[Property("Type", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public DocumentPackEntityType Type { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }

		[Property("Date", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime Date { get; set; }

		public DocumentPackEntityDto(){}

		public DocumentPackEntityDto(DbDataReader reader)
		{
			Load(reader);
		}

		public List<DocumentPackEntityDto> RetrieveEntities(DocumentPackEntityDtoSearchCriteria criteria, long activeUserId, long tenantId)
		{
			var entities = new List<DocumentPackEntityDto>();

			var excludeColumns = new List<ParameterColumn>();

			if (criteria.Type == DocumentPackEntityType.Invoice)
			{
				var p = AccountingSearchFields.Posted.ToParameterColumn();
				p.DefaultValue = true.ToString();
				p.Operator = Operator.Equal;
				if (criteria.Parameters.All(pf => pf.ReportColumnName != p.ReportColumnName)) criteria.Parameters.Add(p);

				excludeColumns.Add(AccountingSearchFields.BillingShipmentNumber.ToParameterColumn());
				excludeColumns.Add(AccountingSearchFields.BillingServiceTicketNumber.ToParameterColumn());
			}

			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };

			var refernceColumns = new List<ParameterColumn>
				{
					OperationsSearchFields.ShipmentReferenceName.ToParameterColumn(),
					OperationsSearchFields.ShipmentReferenceValue.ToParameterColumn()
				};
			excludeColumns.AddRange(refernceColumns);
			

			var query = criteria.Type == DocumentPackEntityType.Shipment
                            ? @"SELECT ShipmentNumber 'Number', Id, DesiredPickupDate 'Date', 0 'Type', CustomerNumber, CustomerName FROM ShipmentViewSearch2(@TenantId, @ActiveUserId) WHERE "
                            : @"SELECT InvoiceNumber 'Number', Id, PostDate 'Date', 1 'Type', CustomerNumber, CustomerName FROM InvoiceDashboardViewSearch(@TenantId, @ActiveUserId) WHERE ";

			var fields = criteria.Type == DocumentPackEntityType.Shipment ? OperationsSearchFields.Shipments : AccountingSearchFields.Invoices;
			query += criteria
				.Parameters
				.Where(p => excludeColumns.All(rc => rc.ReportColumnName != p.ReportColumnName))
				.ToList()
				.BuildWhereClause(parameters, fields);

			if (criteria.Parameters.Any(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName)))
			{
				var rquery = @"Select ShipmentNumber From Shipment s Inner Join ShipmentReference r on r.ShipmentId = s.Id Where ";

				var rparams = new Dictionary<string, object>();
				rquery += criteria
					.Parameters
					.Where(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName))
					.ToList()
					.BuildWhereClause(rparams, OperationsSearchFields.Shipments);

				foreach (var item in rparams)
				{
					var newKey = string.Format("r_{0}", item.Key);
					rquery = rquery.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}

				query += string.Format(" AND ShipmentNumber IN ({0})", rquery);
			}

			// begin building subqueries to filter where applicable
			var containsShipmentFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(AccountingSearchFields.BillingShipmentNumber.DisplayName);
			
			if (containsShipmentFilter && criteria.Type == DocumentPackEntityType.Invoice)
			{
				var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
				var filter = criteria.Parameters.Where(p => p.ReportColumnName == AccountingSearchFields.BillingShipmentNumber.DisplayName)
												.ToList()
												.BuildWhereClause(uParams, AccountingSearchFields.Invoices);

				var shipmentQuery = string.Format(@"SELECT i.Id FROM Invoice i
                                                            INNER JOIN InvoiceDetail id ON id.InvoiceId = i.Id
                                                            WHERE id.ReferenceType = {0} AND {1} ", DetailReferenceType.Shipment.ToInt(), filter);

				foreach (var item in uParams)
				{
					var newKey = string.Format("sh_{0}", item.Key);
					shipmentQuery = shipmentQuery.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}

				query += string.Format(" AND (Id IN ({0})) ", shipmentQuery);
			}
			
			var containsServiceTicketFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(AccountingSearchFields.BillingServiceTicketNumber.DisplayName);
			if (containsServiceTicketFilter)
			{
				var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
				var filter = criteria.Parameters.Where(p => p.ReportColumnName == AccountingSearchFields.BillingServiceTicketNumber.DisplayName)
												.ToList()
												.BuildWhereClause(uParams, AccountingSearchFields.Invoices);

				var serviceTicketQuery = string.Format(@"SELECT i.Id FROM Invoice i
                                                            INNER JOIN InvoiceDetail id ON id.InvoiceId = i.Id
                                                            WHERE id.ReferenceType = {0} AND {1} ", DetailReferenceType.ServiceTicket.ToInt(), filter);


				foreach (var item in uParams)
				{
					var newKey = string.Format("st_{0}", item.Key);
					serviceTicketQuery = serviceTicketQuery.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}

				query += string.Format(" AND (Id IN ({0})) ", serviceTicketQuery);
			}

			
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					entities.Add(new DocumentPackEntityDto(reader));
			Connection.Close();
			return entities;
		}
	}
}
