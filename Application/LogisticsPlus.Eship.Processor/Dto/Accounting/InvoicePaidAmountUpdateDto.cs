﻿using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
	public class InvoicePaidAmountUpdateDto
	{
		public string InvoiceNumber { get; set; }
		public string CustomerNumber { get; set; }
		public string CustomerName { get; set; }
		public decimal AmountDue { get; set; }
		public decimal AmountPaid { get; set; }
		public ValidationMessage Message { get; set; }

		public string Customer { get { return string.Format("{0} - {1}", CustomerNumber, CustomerName); } }
		public bool IsError { get { return Message != null && Message.Type == ValidateMessageType.Error; } }
		public string MessageText { get { return Message != null ? Message.Message : string.Empty; } }

		public InvoicePaidAmountUpdateDto()
		{
			Message = ValidationMessage.Information(string.Empty);
		}

		public InvoicePaidAmountUpdateDto(Invoice invoice)
		{
			InvoiceNumber = invoice.InvoiceNumber;
			CustomerNumber = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.Customer.CustomerNumber;
			CustomerName = invoice.CustomerLocation == null ? string.Empty : invoice.CustomerLocation.Customer.Name;
			AmountDue = invoice.Details.Sum(d => d.AmountDue);
			AmountPaid = invoice.PaidAmount;
			Message = ValidationMessage.Information(string.Empty);
		}
	}
}
