﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
	[Entity("InvoiceSummaryDto", ReadOnly = true, Source = EntitySource.TableView)]
	public class InvoiceSummaryDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; set; }

		[Property("InvoiceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string InvoiceNumber { get; set; }

		[Property("Posted", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Posted { get; set; }

		[Property("PostDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime PostDate { get; set; }

		[Property("DueDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DueDate { get; set; }

		[Property("InvoiceType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public InvoiceType InvoiceType { get; set; }

		[Property("SpecialInstruction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SpecialInstruction { get; set; }

		[Property("OriginalInvoiceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginalInvoiceNumber { get; set; }

		[Property("Street1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Street1 { get; set; }

		[Property("Street2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Street2 { get; set; }

		[Property("City", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string City { get; set; }

		[Property("State", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string State { get; set; }

		[Property("PostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PostalCode { get; set; }

		[Property("CountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CountryName { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("TotalAmount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TotalAmount { get; set; }

		public InvoiceSummaryDto() { }

		public InvoiceSummaryDto(DbDataReader reader)
		{
			Load(reader);
		}

		public List<InvoiceSummaryDto> FetchInvoices(string referenceNumber, DetailReferenceType referenceType, long tenantId)
		{
			var invoices = new List<InvoiceSummaryDto>();

			const string query =
				@"SELECT 
					i.Id,
					i.InvoiceNumber, 
					i.PostDate,
					i.Posted,
					i.DueDate,
					i.InvoiceType,
					i.SpecialInstruction,
					'' 'OriginalInvoiceNumber',
					cl.Street1,
					cl.Street2,
					cl.City,
					cl.[State],
					cl.PostalCode,
					co.Name 'CountryName',
					c.CustomerNumber,
					SUM((d.UnitSell - d.UnitDiscount) * d.Quantity) 'TotalAmount'
				FROM Invoice i
					INNER JOIN InvoiceDetail d ON d.InvoiceId = i.Id
					INNER JOIN CustomerLocation cl ON cl.Id = i.CustomerLocationId
					INNER JOIN Country co ON cl.CountryId = co.Id
					INNER JOIN Customer c ON cl.CustomerId = c.Id
				WHERE
					i.TenantId = @TenantId
						AND i.Posted = 1 -- only posted invoices
						AND d.ReferenceNumber = @ReferenceNumber 
						AND d.ReferenceType = @ReferenceType
				GROUP BY
					i.Id, i.InvoiceNumber, i.PostDate, i.Posted, i.DueDate, i.InvoiceType, i.SpecialInstruction,
					cl.Street1, cl.Street2, cl.City, cl.[State], cl.PostalCode, co.Name, c.CustomerNumber";

			var parameters = new Dictionary<string, object>
			                 	{
				                 	{"TenantId", tenantId},
				                 	{"ReferenceNumber", referenceNumber},
				                 	{"ReferenceType", referenceType},
			                 	};

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					invoices.Add(new InvoiceSummaryDto(reader));
			Connection.Close();
			return invoices;
		}

		public List<InvoiceSummaryDto> FetchInvoiceSupplementalsAndCredits(long originalInvoiceId, long tenantId)
		{
			var invoices = new List<InvoiceSummaryDto>();

			const string query =
				@"SELECT 
					i.Id,
					i.InvoiceNumber, 
					i.PostDate,
					i.Posted,
					i.DueDate,
					i.InvoiceType,
					i.SpecialInstruction,
					ISNULL(i2.InvoiceNumber, '') 'OriginalInvoiceNumber',
					cl.Street1,
					cl.Street2,
					cl.City,
					cl.[State],
					cl.PostalCode,
					co.Name 'CountryName',
					c.CustomerNumber,
					SUM((d.UnitSell - d.UnitDiscount) * d.Quantity) 'TotalAmount'
				FROM Invoice i
					INNER JOIN InvoiceDetail d ON d.InvoiceId = i.Id
					INNER JOIN CustomerLocation cl ON cl.Id = i.CustomerLocationId
					INNER JOIN Country co ON cl.CountryId = co.Id
					INNER JOIN Customer c ON cl.CustomerId = c.Id
					LEFT JOIN Invoice i2 ON i.OriginalInvoiceId = i2.Id
				WHERE
					i.TenantId = @TenantId
						AND i.Posted = 1 -- only posted invoices
						AND i.OriginalInvoiceId = @OriginalInvoiceId
						AND i.InvoiceType in (1, 2)
				GROUP BY
					i.Id, i.InvoiceNumber, i.PostDate, i.Posted, i.DueDate, i.InvoiceType, i.SpecialInstruction,
					i2.InvoiceNumber, cl.Street1, cl.Street2, cl.City, cl.[State], cl.PostalCode, co.Name, c.CustomerNumber";

			var parameters = new Dictionary<string, object>
			                 	{
				                 	{"TenantId", tenantId},
				                 	{"OriginalInvoiceId", originalInvoiceId},
			                 	};

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					invoices.Add(new InvoiceSummaryDto(reader));
			Connection.Close();
			return invoices;
		}
	}
}
