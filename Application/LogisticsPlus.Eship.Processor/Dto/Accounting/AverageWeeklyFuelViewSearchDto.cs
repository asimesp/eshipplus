﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
	[Entity("AverageWeeklyFuelViewSearchDto", ReadOnly = true, Source = EntitySource.TableView, SourceName = "AverageWeeklyFuelViewSearch")]
	public class AverageWeeklyFuelViewSearchDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[Property("EastCoastCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal EastCoastCost { get; set; }

		[Property("NewEnglandCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal NewEnglandCost { get; set; }

		[Property("CentralAtlanticCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CentralAtlanticCost { get; set; }

		[Property("LowerAtlanticCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LowerAtlanticCost { get; set; }

		[Property("MidwestCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MidwestCost { get; set; }

		[Property("GulfCoastCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal GulfCoastCost { get; set; }

		[Property("RockyMountainCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal RockyMountainCost { get; set; }

		[Property("WestCoastCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal WestCoastCost { get; set; }

		[Property("WestCoastLessCaliforniaCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal WestCoastLessCaliforniaCost { get; set; }

		[Property("CaliforniaCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CaliforniaCost { get; set; }

		[Property("NationalCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal NationalCost { get; set; }

		[Property("ChargeCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCode { get; set; }

		[Property("ChargeCodeDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ChargeCodeDescription { get; set; }
		

		public AverageWeeklyFuelViewSearchDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}
