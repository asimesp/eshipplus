﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{
	public class DocumentPackEntityDtoSearchCriteria
	{
		public DocumentPackEntityType Type { get; set; }
		public long ActiveUserId { get; set; }
		public List<ParameterColumn> Parameters { get; set; }

		public DocumentPackEntityDtoSearchCriteria()
		{
			Parameters = new List<ParameterColumn>();
		}
	}
}
