﻿using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace LogisticsPlus.Eship.Processor.Dto.Accounting
{

    [Serializable]
    [Entity("ChargeItemDto", ReadOnly = false, Source = EntitySource.TableView)]
    public class ChargeItemDto : EntityBase
    {
        private List<ChargeViewSearchDto> _charges;
        
        [EnableSnapShot("ChargeType", Description = "Charge Type")]
        [Property("ChargeType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ChargeType { get; set; }

        [EnableSnapShot("VendorId", Description = "Vendor Id")]
        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorId { get; set; }

        [EnableSnapShot("VendorName", Description = "Vendor Name")]
        [Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorName { get; set; }

        [EnableSnapShot("Number", Description = "Identifier Number")]
        [Property("Number", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Number { get; set; }

        [EnableSnapShot("UnitBuy", Description = "Unit Buy")]
        [Property("UnitBuy", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal UnitBuy { get; set; }
        
        public ChargeItemDto()
        {

        }
        
        public ChargeItemDto(DbDataReader reader)
        {
            Load(reader);
        }
        public List<ChargeViewSearchDto> Charges
        {
            get
            {
                if (_charges == null) LoadCharges();
                return _charges;
            }
            set { _charges = value; }
        }

        public void LoadCharges()
        {
            _charges = null;
            var charges = new List<ChargeViewSearchDto>();
            var query = string.Format(@"
SELECT * From (
SELECT s.ShipmentNumber AS Number, {0} as ChargeType, vdd.DisputedId,
  case 
   when sc.VendorId = 0 then itemVendor.Id
   else sc.VendorId
  end as VendorId,
  case 
    when sc.VendorId = 0 then itemVendor.VendorNumber
    else chargeVendor.VendorNumber
  end as VendorNumber,
  case 
   when sc.VendorId = 0 then itemVendor.Name
   else chargeVendor.Name
  end as VendorName,
  case 
   when sc.VendorId = 0 then itemVendorLocation.Id
   else chargeVendorLocation.Id
  end as VendorLocationId,
  case 
   when sc.VendorBillId > 0 then {2}
   when vdd.Id IS NOT NULL AND vd.IsResolved = 0 then {3}
   else {4}
  end as Status, 
sc.Id, sc.TenantId, sc.ShipmentId as ParentId, sc.Quantity, sc.UnitBuy, sc.UnitSell, sc.UnitDiscount, sc.Comment, sc.ChargeCodeId, sc.VendorBillId
FROM ShipmentCharge sc 
INNER JOIN Shipment s ON s.id = sc.ShipmentId
LEFT JOIN VendorDisputeDetail vdd ON vdd.ChargeLineId = sc.Id AND (SELECT IsResolved FROM VendorDispute WHERE Id = vdd.DisputedId) = 0
LEFT JOIN VendorDispute vd ON vd.Id = vdd.DisputedId
INNER JOIN ShipmentVendor sv ON (sv.ShipmentId = s.Id AND (sc.VendorId = sv.VendorId OR (sc.VendorId = 0 AND sv.[Primary] = 1)))
LEFT JOIN Vendor itemVendor ON itemVendor.Id = sv.VendorId
LEFT JOIN Vendor chargeVendor ON chargeVendor.Id = sc.VendorId
LEFT JOIN VendorLocation itemVendorLocation ON itemVendor.Id = itemVendorLocation.VendorId AND itemVendorLocation.[Primary] = 1
LEFT JOIN VendorLocation chargeVendorLocation ON chargeVendor.Id = chargeVendorLocation.VendorId AND chargeVendorLocation.[Primary] = 1
WHERE s.ShipmentNumber = @Number AND sv.VendorId = @VendorId
UNION ALL
SELECT st.ServiceTicketNumber AS Number, {1} as ChargeType, vdd.DisputedId,
  case 
   when stc.VendorId = 0 then itemVendor.Id
   else stc.VendorId
 end as VendorId,
 case 
    when stc.VendorId = 0 then itemVendor.VendorNumber
    else chargeVendor.VendorNumber
 end as VendorNumber,
 case 
   when stc.VendorId = 0 then itemVendor.Name
   else chargeVendor.Name
 end as VendorName,
  case 
   when stc.VendorId = 0 then itemVendorLocation.Id
   else chargeVendorLocation.Id
 end as VendorLocationId,
  case 
   when stc.VendorBillId > 0 then {2}
   when vdd.Id IS NOT NULL AND vd.IsResolved = 0 then {3}
   else {4}
 end as Status,
stc.Id, stc.TenantId, stc.ServiceTicketId as ParentId, stc.Quantity, stc.UnitBuy, stc.UnitSell, stc.UnitDiscount, stc.Comment, stc.ChargeCodeId, stc.VendorBillId
FROM ServiceTicketCharge stc 
INNER JOIN ServiceTicket st ON st.Id = stc.ServiceTicketId
LEFT JOIN VendorDisputeDetail vdd ON vdd.ChargeLineId = stc.Id AND (SELECT IsResolved FROM VendorDispute WHERE Id = vdd.DisputedId) = 0
LEFT JOIN VendorDispute vd ON vd.Id = vdd.DisputedId
INNER JOIN ServiceTicketVendor stv ON (stv.ServiceTicketId = st.Id AND (stc.VendorId = stv.VendorId OR (stc.VendorId = 0 AND stv.[Primary] = 1)))
LEFT JOIN Vendor itemVendor ON itemVendor.Id = stv.VendorId
LEFT JOIN Vendor chargeVendor ON chargeVendor.Id = stc.VendorId
LEFT JOIN VendorLocation itemVendorLocation ON itemVendor.Id = itemVendorLocation.VendorId AND itemVendorLocation.[Primary] = 1
LEFT JOIN VendorLocation chargeVendorLocation ON chargeVendor.Id = chargeVendorLocation.VendorId AND chargeVendorLocation.[Primary] = 1
WHERE st.ServiceTicketNumber = @Number AND stv.VendorId = @VendorId
) tableName 
WHERE 
ChargeType = @ChargeType 
", Eship.Core.Accounting.ChargeType.Shipment.ToInt(),
Eship.Core.Accounting.ChargeType.ServiceTicket.ToInt(),
ChargeStatus.Billed.ToInt(),
ChargeStatus.Disputed.ToInt(),
ChargeStatus.Open.ToInt());
            var parameter = new Dictionary<string, object> {
                { "Number", Number },
                { "VendorId", VendorId },
                { "ChargeType", ChargeType.ToEnum<ChargeType>() }
            };

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    charges.Add(new ChargeViewSearchDto(reader));
            Connection.Close();

            _charges = charges;
        }
    }
}
