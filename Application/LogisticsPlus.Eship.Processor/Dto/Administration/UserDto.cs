﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Administration
{
    [Serializable]
	[Entity("UserDto", ReadOnly = false, Source = EntitySource.TableView)]
	public class UserDto : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Username { get; set; }

		[Property("FirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FirstName { get; set; }

		[Property("LastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LastName { get; set; }

		[Property("TenantEmployee", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool TenantEmployee { get; set; }

		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[Property("CustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerName { get; set; }

		[Property("DepartmentCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DepartmentCode { get; set; }

		[Property("DepartmentDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DepartmentDescription { get; set; }

        [Property("IsShipmentCoordinator", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsShipmentCoordinator { get; set; }

        [Property("IsCarrierCoordinator", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsCarrierCoordinator { get; set; }

		public UserDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}
