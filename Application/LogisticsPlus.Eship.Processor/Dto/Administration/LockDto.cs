﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.Administration
{
	[Entity("LockDto", ReadOnly = false, Source = EntitySource.TableView)]
	public class LockDto : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Username { get; set; }

		[Property("FirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FirstName { get; set; }

		[Property("LastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LastName { get; set; }

		[Property("EntityId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EntityId { get; set; }

		[Property("LockUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long LockUserId { get; set; }

		[Property("LockKey", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LockKey { get; set; }

		[Property("LockDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LockDateTime { get; set; }

		public LockDto(DbDataReader reader)
		{
			Load(reader);
		}
	}
}
