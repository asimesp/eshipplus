﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence
{
    [Entity("VendorStatisticsPerformanceShipmentDto", ReadOnly = true)]
    public class VendorStatisticsPerformanceShipmentDto : EntityBase
    {
        [Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentNumber { get; set; }

        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorId { get; set; }

        [Property("OnTimePickup", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool OnTimePickup { get; set; }

        [Property("OnTimeDelivery", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool OnTimeDelivery { get; set; }

        [Property("AmountPaid", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal AmountPaid { get; set; }

        [Property("Mileage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Mileage { get; set; }

        [Property("TotalRejectedLoads", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int TotalRejectedLoads { get; set; }
        

        public VendorStatisticsPerformanceShipmentDto()
        {
        }

        public VendorStatisticsPerformanceShipmentDto(DbDataReader reader)
		{
			Load(reader);
		}


        public List<VendorStatisticsPerformanceShipmentDto> RetrieveVendorStatisticsPerformanceShipmentDtos(long tenantId, string vendorNumber, DateTime startShipmentDateCreated, DateTime endShipmentDateCreated, long userId, ServiceMode mode = ServiceMode.NotApplicable)
        {
            const string query = @"exec dbo.VendorStatisticsPerformanceByShipment2 @TenantId, @CustomerNumber, @VendorNumber, @StartShipmentDateCreated, @EndShipmentDateCreated, @Mode, @DefaultSystemDate, @UserId, 0";

            var parameters = new Dictionary<string, object>
                                  {
                                      { "TenantId", tenantId },
                                      { "CustomerNumber",  string.Empty},
                                      { "VendorNumber",  vendorNumber},
                                      { "StartShipmentDateCreated",  startShipmentDateCreated},
                                      { "EndShipmentDateCreated",  endShipmentDateCreated},
                                      { "Mode",  mode.ToInt()},
                                      { "DefaultSystemDate", DateUtility.SystemEarliestDateTime },
                                      { "UserId",  userId},
                                  };

            var dtos = new List<VendorStatisticsPerformanceShipmentDto>();
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    dtos.Add(new VendorStatisticsPerformanceShipmentDto(reader));
            Connection.Close();

            return dtos;
        }
    }
}
