﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence
{
    [Entity("VendorTerminalLocatorDto", ReadOnly = true)]
    public class VendorTerminalLocatorDto:EntityBase
    {
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = false)]
		public long Id { get; protected set; }

		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

        [Property("VendorDateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime VendorDateCreated { get; set; }

		[Property("ContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ContactName { get; set; }

		[Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Comment { get; set; }

		[Property("Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Phone { get; set; }

		[Property("Mobile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Mobile { get; set; }

		[Property("Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Fax { get; set; }

		[Property("Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Email { get; set; }

        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorId { get; set; }

        [Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorNumber { get; set; }

        [Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorName { get; set; }

        [Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Active { get; set; }

        [Property("Scac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Scac { get; set; }

        [Property("MC", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string MC { get; set; }

        [Property("DOT", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DOT { get; set; }

		[Property("Street1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Street1 { get; set; }

		[Property("Street2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Street2 { get; set; }

		[Property("City", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string City { get; set; }

		[Property("State", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string State { get; set; }

        [Property("CountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long CountryId { get; set; }

        [Property("PostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PostalCode { get; set; }

        [Property("Distance", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Distance { get; set; }


         public VendorTerminalLocatorDto() { }

         public VendorTerminalLocatorDto(DbDataReader reader)
		{
			Load(reader);
		}

         public List<VendorTerminalLocatorDto> FetchVendorTerminals(VendorTerminalLocatorSearchCriteria criteria, long tenantId)
         {
             var terminals = new List<VendorTerminalLocatorDto>();
             const string query =
				 @"SELECT VendorTerminal.*, Vendor.Name AS VendorName, Vendor.VendorNumber, Vendor.MC, Vendor.DOT, Vendor.Scac, Vendor.Active, 
                    Vendor.DateCreated AS  VendorDateCreated, 
					(select top 1 * from dbo.CalculatePostalCodesDistance(VendorTerminal.PostalCode, VendorTerminal.CountryId, @PostalCode, @CountryId, @UseKilometers)) AS Distance
                 FROM VendorTerminal, Vendor
                 WHERE (select top 1 * from dbo.CalculatePostalCodesDistance(VendorTerminal.PostalCode, VendorTerminal.CountryId, @PostalCode, @CountryId, @UseKilometers))<=@Radius
                        AND VendorTerminal.VendorId = Vendor.Id
                        AND VendorTerminal.TenantId = @TenantId
                            ORDER BY Distance ASC, Vendor.Name ASC";

         	var parameters = new Dictionary<string, object>
         	                 	{
         	                 		{"TenantId", tenantId},
         	                 		{"CountryId", criteria.CountryId},
         	                 		{"PostalCode", criteria.PostalCode},
         	                 		{"Radius", criteria.Radius},
         	                 		{"UseKilometers", criteria.UseKilometers ? 1 : 0}
         	                 	};
             using (var reader = GetReader(query, parameters))
                 while (reader.Read())
                     terminals.Add(new VendorTerminalLocatorDto(reader));
             Connection.Close();
             return terminals;
         }
    }
}
