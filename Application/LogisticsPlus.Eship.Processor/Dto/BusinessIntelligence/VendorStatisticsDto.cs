﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence
{
	[Entity("VendorStatisticsDto", ReadOnly = true)]
	public class VendorStatisticsDto : EntityBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = false)]
		public long Id { get; protected set; }

		[Property("Number", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Number { get; set; }

		[Property("Scac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Scac { get; set; }

		[Property("MC", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string MC { get; set; }

		[Property("DOT", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DOT { get; set; }

		[Property("LTLCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int LTLCount { get; set; }

		[Property("LTLLatePickupCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int LTLLatePickupCount { get; set; }

		[Property("LTLLateDeliveryCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int LTLLateDeliveryCount { get; set; }

		[Property("LTLConsideredCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int LTLConsideredCount { get; set; }

		[Property("TLCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TLCount { get; set; }

		[Property("TLLatePickupCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TLLatePickupCount { get; set; }

		[Property("TLLateDeliveryCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TLLateDeliveryCount { get; set; }

		[Property("TLConsideredCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TLConsideredCount { get; set; }

		[Property("AirCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int AirCount { get; set; }

		[Property("AirLatePickupCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int AirLatePickupCount { get; set; }

		[Property("AirLateDeliveryCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int AirLateDeliveryCount { get; set; }

		[Property("AirConsideredCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int AirConsideredCount { get; set; }

		[Property("RailCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int RailCount { get; set; }

		[Property("RailLatePickupCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int RailLatePickupCount { get; set; }

		[Property("RailLateDeliveryCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int RailLateDeliveryCount { get; set; }

		[Property("RailConsideredCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int RailConsideredCount { get; set; }

		[Property("SmallPackageCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int SmallPackageCount { get; set; }

		[Property("SmallPackageLatePickupCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int SmallPackageLatePickupCount { get; set; }

		[Property("SmallPackageLateDeliveryCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int SmallPackageLateDeliveryCount { get; set; }

		[Property("SmallPackageConsideredCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int SmallPackageConsideredCount { get; set; }

		public int LTLFinalConsideredCount { get { return LTLCount - LTLConsideredCount; } }
		public int AirFinalConsideredCount { get { return AirCount - AirConsideredCount; } }
		public int TLFinalConsideredCount { get { return TLCount - TLConsideredCount; } }
		public int RailFinalConsideredCount { get { return RailCount - RailConsideredCount; } }
		public int SmallPackageFinalConsideredCount { get { return SmallPackageCount - SmallPackageConsideredCount; } }

		public string LTLLatePickup
		{
			get
			{
				var percent = LTLFinalConsideredCount == 0
					? 0m : ((LTLLatePickupCount.ToDecimal() / LTLFinalConsideredCount.ToDecimal()) * 100).ToDecimal();
				return string.Format("{0}% ({1})", percent.ToString("f2"), LTLLatePickupCount);
			}
		}
		public string LTLLateDelivery
		{
			get
			{
				var percent = LTLFinalConsideredCount == 0
					? 0m : ((LTLLateDeliveryCount.ToDecimal() / LTLFinalConsideredCount.ToDecimal()) * 100).ToDecimal();
				return string.Format("{0}% ({1})", percent.ToString("f2"), LTLLateDeliveryCount);
			}
		}

		public string TLLatePickup
		{
			get
			{
				var percent = TLFinalConsideredCount == 0
					? 0m : ((TLLatePickupCount.ToDecimal() / TLFinalConsideredCount.ToDecimal()) * 100).ToDecimal();
				return string.Format("{0}% ({1})", percent.ToString("f2"), TLLatePickupCount);
			}
		}
		public string TLLateDelivery
		{
			get
			{
				var percent = TLFinalConsideredCount == 0
					? 0m : ((TLLateDeliveryCount.ToDecimal() / TLFinalConsideredCount.ToDecimal()) * 100).ToDecimal();
				return string.Format("{0}% ({1})", percent.ToString("f2"), TLLateDeliveryCount);
			}
		}

		public string RailLatePickup
		{
			get
			{
				var percent = RailFinalConsideredCount == 0
					? 0m : ((RailLatePickupCount.ToDecimal() / RailFinalConsideredCount.ToDecimal()) * 100).ToDecimal();
				return string.Format("{0}% ({1})", percent.ToString("f2"), RailLatePickupCount);
			}
		}
		public string RailLateDelivery
		{
			get
			{
				var percent = RailFinalConsideredCount == 0
					? 0m : ((RailLateDeliveryCount.ToDecimal() / RailFinalConsideredCount.ToDecimal()) * 100).ToDecimal();
				return string.Format("{0}% ({1})", percent.ToString("f2"), RailLateDeliveryCount);
			}
		}

		public string AirLatePickup
		{
			get
			{
				var percent = AirFinalConsideredCount == 0
					? 0m : ((AirLatePickupCount.ToDecimal() / AirFinalConsideredCount.ToDecimal()) * 100).ToDecimal();
				return string.Format("{0}% ({1})", percent.ToString("f2"), AirLatePickupCount);
			}
		}
		public string AirLateDelivery
		{
			get
			{
				var percent = AirFinalConsideredCount == 0
					? 0m : ((AirLateDeliveryCount.ToDecimal() / AirFinalConsideredCount.ToDecimal()) * 100).ToDecimal();
				return string.Format("{0}% ({1})", percent.ToString("f2"), AirLateDeliveryCount);
			}
		}

		public string SmallPackageLatePickup
		{
			get
			{
				var percent = SmallPackageConsideredCount == 0
					? 0m : ((SmallPackageLatePickupCount.ToDecimal() / SmallPackageFinalConsideredCount.ToDecimal()) * 100).ToDecimal();
				return string.Format("{0}% ({1})", percent.ToString("f2"), SmallPackageLatePickupCount);
			}
		}
		public string SmallPackageLateDelivery
		{
			get
			{
				var percent = SmallPackageConsideredCount == 0
					? 0m : ((SmallPackageLateDeliveryCount.ToDecimal() / SmallPackageFinalConsideredCount.ToDecimal()) * 100).ToDecimal();
				return string.Format("{0}% ({1})", percent.ToString("f2"), SmallPackageLateDeliveryCount);
			}
		}

		public VendorStatisticsDto() { }

		public VendorStatisticsDto(DbDataReader reader)
		{
			Load(reader);
		}

		public VendorStatisticsDto RetrieveVendorStatistics(long vendorId, long tenantId)
		{
			const string query = @"EXEC dbo.VendorStatistics @VendorId, @TenantId";

			var parameters = new Dictionary<string, object>
                                  {
                                      { "VendorId", vendorId },
                                      { "TenantId", tenantId },
                                  };

			VendorStatisticsDto dto = null;
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					dto = new VendorStatisticsDto(reader);
			Connection.Close();

			return dto;
		}

		public List<List<object>> RetrieveVendorCostData(long vendorId, long tenantId, string originPostalCode, long originCountryId,
			string destinationPostalCode, long destinationCountryId, DateTime date, double radius)
		{
			var result = new List<List<object>>();

			var xSeries = new List<object>();
			var ySeries = new List<object>();

			const string query =
					@"SELECT * FROM dbo.VendorHistoricalLaneBuySellStatistics
							(@VendorId, @TenantId, @OriginPostalCode, @OriginCountryId, @DestinationPostalCode, @DestinationCountryId, @Date, @Radius)";

			var parameters = new Dictionary<string, object>
                                  {
                                      { "VendorId", vendorId },
                                      { "TenantId", tenantId },
                                      { "OriginPostalCode", originPostalCode },
                                      { "OriginCountryId", originCountryId },
                                      { "DestinationPostalCode", destinationPostalCode },
									  { "DestinationCountryId", destinationCountryId },
                                      { "Date", date },
                                      { "Radius", radius }
                                  };

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
				{
					ySeries.Add(reader[0]); //Cost
					xSeries.Add(reader[1]); //Year-Month
				}
			Connection.Close();

			result.Add(xSeries);
			result.Add(ySeries);

			return result;
		}
	}
}