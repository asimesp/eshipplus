﻿using System;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence
{
	public class VendorPerformanceSummaryCriteria
	{
		public long TenantId { get; set; }
		public long UserId { get; set; }
		public string CustomerNumber { get; set; }
		public string VendorNumber { get; set; }
		public DateTime StartShipmentDateCreated { get; set; }
		public DateTime EndShipmentDateCreated { get; set; }
		public ServiceMode Mode { get; set; }
	}
}
