﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence
{
	[Entity("VendorPerformanceSummaryDto", Source = EntitySource.TableView, ReadOnly = true, SourceName = "VendorStatisticsPerformanceByShipment2")]
	public class VendorPerformanceSummaryDto : EntityBase
	{
        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
        public long VendorId { get; set; }

		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true)]
		public string VendorNumber { get; set; }

		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[Property("Scac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Scac { get; set; }

		[Property("TotalNonExcludedShipments", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalNonExcludedShipments { get; set; }

		[Property("TotalOnTimeOverall", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalOnTimeOverall { get; set; }

		[Property("TotalOnTimePickup", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalOnTimePickup { get; set; }

		[Property("TotalOnTimeDelivery", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalOnTimeDelivery { get; set; }

		[Property("TotalMileage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TotalMileage { get; set; }

		[Property("TotalAmountPaid", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TotalAmountPaid { get; set; }

		[Property("TotalRejectedLoads", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalRejectedLoads { get; set; }

		[Property("TotalShipments", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalShipments { get; set; }

		public decimal TotalOverallLoadCount
		{
			get { return (TotalShipments + TotalRejectedLoads).ToDecimal(); }
		}
		public decimal PercentOnTimeOverall
		{
			get { return TotalNonExcludedShipments == 0 ? 0m : TotalOnTimeOverall.ToDecimal()/TotalNonExcludedShipments.ToDecimal()*100m; }
		}
		public decimal PercentOnTimePickup
		{
			get { return TotalNonExcludedShipments == 0 ? 0m : TotalOnTimePickup.ToDecimal() / TotalNonExcludedShipments.ToDecimal() * 100m; }
		}
		public decimal PercentOnTimeDelivery
		{
			get { return TotalNonExcludedShipments == 0 ? 0m : TotalOnTimeDelivery.ToDecimal() / TotalNonExcludedShipments.ToDecimal() * 100m; }
		}
		public decimal PercentAcceptedTenders
		{
			get
			{
				return TotalOverallLoadCount == 0
				       	? 0m
						: TotalShipments.ToDecimal() / TotalOverallLoadCount * 100m;
			}
		}




		public VendorPerformanceSummaryDto()
		{
		}

		public VendorPerformanceSummaryDto(DbDataReader reader)
		{
			Load(reader);
		}

		public List<VendorPerformanceSummaryDto> Fetch(VendorPerformanceSummaryCriteria criteria)
		{
			var dtos = new List<VendorPerformanceSummaryDto>();

			const string query = @"exec dbo.VendorStatisticsPerformanceByShipment2 @TenantId, @CustomerNumber, @VendorNumber, @StartShipmentDateCreated, @EndShipmentDateCreated, @Mode, @DefaultSystemDate, @UserId, 1";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"TenantId", criteria.TenantId},
			                 		{"UserId", criteria.UserId},
			                 		{"CustomerNumber", criteria.CustomerNumber},
			                 		{"VendorNumber", criteria.VendorNumber},
			                 		{"StartShipmentDateCreated", criteria.StartShipmentDateCreated},
			                 		{"EndShipmentDateCreated", criteria.EndShipmentDateCreated},
			                 		{"Mode", criteria.Mode.ToInt()},
			                 		{"DefaultSystemDate", DateUtility.SystemEarliestDateTime},
			                 	};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					dtos.Add(new VendorPerformanceSummaryDto(reader));
			Connection.Close();

			return dtos.OrderBy(d => d.Name).ToList();
		}
	}
}
