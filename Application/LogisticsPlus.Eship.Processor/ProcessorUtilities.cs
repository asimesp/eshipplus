﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Calculations;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.PluginBase.Mileage;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace LogisticsPlus.Eship.Processor
{
	public static class ProcessorUtilities
	{
		private const double Epsilon = 0.5;

	    public const string EmailRegExPattern = "^[_0-9a-zA-Z][\\w\\.-]*[a-zA-Z0-9_]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";

        public static string FormattedString(this ServiceViewSearchDto service)
		{
			return service == null ? string.Empty : string.Format(" {0} ({1})", service.Description, service.Code);
		}


		public static int ToInt(this object value)
		{
			var x = default(int);
			return value == null
					? x
					: value.GetType().IsEnum
						? (int)value
						: Int32.TryParse(value.ToString(), out x) ? x : default(int);
		}

		public static long ToLong(this object value)
		{
			var x = default(long);
			return value == null ? x : Int64.TryParse(value.ToString(), out x) ? x : default(long);
		}


		public static decimal ToDecimal(this object value)
		{
			var x = default(decimal);
			return value == null ? x : Decimal.TryParse(value.ToString(), out x) ? x : default(decimal);
		}

		public static double ToDouble(this object value)
		{
			var x = default(double);
			return value == null ? x : double.TryParse(value.ToString(), out x) ? x : default(double);
		}

		public static bool ToBoolean(this object value)
		{
			bool x;
			if (value == null) return false;
			if (bool.TryParse(value.ToString(), out x)) return x;
			return false;
		}

		public static DateTime ToDateTime(this object value)
		{
			var x = DateUtility.SystemEarliestDateTime;

			return value == null
					? x
					: value.ToString().ToArray().All(char.IsDigit)
					   ? value.ToString().IsValidSystemDateTime() ? DateTime.FromOADate(value.ToDouble()) : x
					   : DateTime.TryParse(value.ToString(), out x) ? x : DateUtility.SystemEarliestDateTime;
		}

		public static Guid ToGuid(this object value)
		{
			var g = Guid.Empty;
			return value == null ? g : Guid.TryParse(value.ToString(), out g) ? g : Guid.Empty;
		}

		public static string GetString(this object value)
		{
			return value == null ? string.Empty : value.ToString();
		}

		public static string ToBase64String(this byte[] value)
		{
			return value == null ? string.Empty : Convert.ToBase64String(value);
		}

		public static byte[] FromBase64String(this string value)
		{
			return value == null ? new byte[0] : Convert.FromBase64String(value);
		}

		public static string Truncate(this string value, int maxLength)
		{
			if (string.IsNullOrEmpty(value)) return string.Empty;
			return value.Length <= maxLength ? value : value.Substring(0, maxLength);
		}



		public static decimal Round(this decimal value)
		{
			return Math.Round(value, 4);
		}



		public static string StripTags(this string value)
		{
			return value.Replace("<", "&lt;").Replace(">", "&gt;");
		}

		public static string ReplaceTags(this string value)
		{
			return value.Replace("&lt;", "<").Replace("&gt;", ">");
		}

		public static string ReplaceNewLineWithHtmlBreak(this string value)
		{
			return value.Replace(Environment.NewLine, "<br />");
		}

		public static string ReplaceHtmlBreakWithNewLine(this string value)
		{
			return value.Replace("<br />", Environment.NewLine);
		}

		public static string StripTab(this string value)
		{
			return value.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		}

		public static string ReplaceTab(this string value)
		{
			return value.Replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "\t");
		}	


        public static String NameOf<T, TT>(this T obj, Expression<Func<T, TT>> propertyAccessor)
        {
            if (propertyAccessor.Body.NodeType == ExpressionType.MemberAccess)
            {
                var memberExpression = propertyAccessor.Body as MemberExpression;
                if (memberExpression == null)
                    return null;
                return memberExpression.Member.Name;
            }
            return null;
        }


        public static Dictionary<int, string> GetAll<TEnum>(bool formatName = true) where TEnum : struct
		{
			var enumerationType = typeof(TEnum);

			if (!enumerationType.IsEnum)
				throw new ArgumentException("Enumeration type is expected.");

			var dictionary = new Dictionary<int, string>();

			foreach (int value in Enum.GetValues(enumerationType))
			{
				var name = formatName
							? Enum.GetName(enumerationType, value).FormattedString()
							: Enum.GetName(enumerationType, value);
				dictionary.Add(value, name);
			}

			return dictionary;
		}

		public static bool EnumValueIsValid<TEnum>(this int value) where TEnum : struct
		{
			var enumerationType = typeof(TEnum);

			if (!enumerationType.IsEnum)
				throw new ArgumentException("Enumeration type is expected.");

			return Enum.GetValues(enumerationType).Cast<int>().Any(enumValue => value == enumValue);
		}

		public static T ToEnum<T>(this string value)
		{
			return (T)Enum.Parse(typeof(T), value, true);
		}

		public static T ToEnum<T>(this int value)
		{
			return (T)Enum.Parse(typeof(T), value.ToString());
		}



		public static Lock RetrieveLock(this Tenant tenant, User user, object entityId)
		{
			return new LockSearch().RetrieveExistingUserLock(tenant.Id, user.Id, entityId, tenant.EntityName());
		}

		public static Lock RetrieveLock<T>(this T entity, User user, object entityId) where T : TenantBase
		{
			return new LockSearch().RetrieveExistingUserLock(entity.TenantId, user.Id, entityId, entity.EntityName());
		}

		public static Lock ObtainLock(this Tenant tenant, User user, object entityId)
		{
			var @lock = new LockSearch().RetrieveExistingLock(tenant.Id, entityId, tenant.EntityName());

			if (@lock != null && !@lock.LockIsExpired()) return @lock;
			if (@lock != null && @lock.LockIsExpired()) @lock.Delete();

			@lock = new Lock
			{
				EntityId = entityId.ToString(),
				LockDateTime = DateTime.Now,
				LockKey = tenant.EntityName(),
				TenantId = tenant.Id,
				User = user
			};
			@lock.Secure();
			return @lock;
		}

		public static Lock ObtainLock<T>(this T entity, User user, object entityId) where T : TenantBase
		{
			var @lock = new LockSearch().RetrieveExistingLock(entity.TenantId, entityId, entity.EntityName());

			if (@lock != null && !@lock.LockIsExpired()) return @lock;
			if (@lock != null && @lock.LockIsExpired()) @lock.Delete();

			@lock = new Lock
						{
							EntityId = entityId.ToString(),
							LockDateTime = DateTime.Now,
							LockKey = entity.EntityName(),
							TenantId = entity.TenantId,
							User = user
						};
			@lock.Secure();
			return @lock;
		}

		public static bool IsUserLock(this Lock @lock, User user)
		{
			return @lock != null && @lock.User.Id == user.Id;
		}

		public static bool LockIsExpired(this Lock @lock)
		{
			if (@lock == null) return true;
			var timeOutLimit = ProcessorVars.RecordLockLimit.ContainsKey(@lock.TenantId)
								? ProcessorVars.RecordLockLimit[@lock.TenantId]
								: ProcessorVars.DefaultRecordLockLimit;
			return (DateTime.Now - @lock.LockDateTime).TotalSeconds > timeOutLimit;
		}

		public static bool HasUserLock(this Tenant tenant, User user, object entityId)
		{
			return tenant.RetrieveLock(user, entityId).IsUserLock(user);
		}

		public static bool HasUserLock<T>(this T entity, User user, object entityId) where T : TenantBase
		{
			return entity.RetrieveLock(user, entityId).IsUserLock(user);
		}



		public static string SpaceJoin(this string[] values)
		{
			return string.Join(" ", values);
		}

		public static string CommaJoin(this string[] values)
		{
			return string.Join(",", values);
		}

		public static string TabJoin(this string[] values)
		{
			return string.Join("\t", values);
		}

		public static string NewLineJoin(this string[] values)
		{
			return string.Join(Environment.NewLine, values);
		}

		public static string WrapDoubleQuotes(this string value)
		{
			return string.Format("\"{0}\"", value);
		}

	    public static string WrapSingleQuotes(this string value)
	    {
	        return string.Format("'{0}'", value);
	    }



        internal static double FetchFAK(this DiscountTier tier, double freightClass)
		{
			if (tier == null) return freightClass;

			if (Math.Abs(freightClass - 50) < Epsilon) return tier.FAK50;
			if (Math.Abs(freightClass - 55) < Epsilon) return tier.FAK55;
			if (Math.Abs(freightClass - 60) < Epsilon) return tier.FAK60;
			if (Math.Abs(freightClass - 65) < Epsilon) return tier.FAK65;
			if (Math.Abs(freightClass - 70) < Epsilon) return tier.FAK70;
			if (Math.Abs(freightClass - 77.5) < Epsilon) return tier.FAK775;
			if (Math.Abs(freightClass - 85) < Epsilon) return tier.FAK85;
			if (Math.Abs(freightClass - 92.5) < Epsilon) return tier.FAK925;
			if (Math.Abs(freightClass - 100) < Epsilon) return tier.FAK100;
			if (Math.Abs(freightClass - 110) < Epsilon) return tier.FAK110;
			if (Math.Abs(freightClass - 125) < Epsilon) return tier.FAK125;
			if (Math.Abs(freightClass - 150) < Epsilon) return tier.FAK150;
			if (Math.Abs(freightClass - 175) < Epsilon) return tier.FAK175;
			if (Math.Abs(freightClass - 200) < Epsilon) return tier.FAK200;
			if (Math.Abs(freightClass - 250) < Epsilon) return tier.FAK250;
			if (Math.Abs(freightClass - 300) < Epsilon) return tier.FAK300;
			if (Math.Abs(freightClass - 400) < Epsilon) return tier.FAK400;
			if (Math.Abs(freightClass - 500) < Epsilon) return tier.FAK500;

			return freightClass;
		}

		internal static LTLCubicFootCapacityRule GetCFCRule(this VendorRating rating, decimal totalCubitFeet, decimal totalPoundPerCubitFeet, DateTime effectiveDateCutOff)
		{
			var list = new Dictionary<string, List<LTLCubicFootCapacityRule>>();
			foreach (var r in rating.LTLCubicFootCapacityRules)
			{
				var key = string.Format("{0}-{1}", r.LowerBound, r.UpperBound);
				if (!list.ContainsKey(key)) list.Add(key, new List<LTLCubicFootCapacityRule>());
				if (r.EffectiveDate <= effectiveDateCutOff) list[key].Add(r);
			}

			var rule = list.Keys
				.Select(k => list[k].OrderByDescending(r => r.EffectiveDate).FirstOrDefault())
				.Where(r => r != null)
				.OrderBy(r => r.LowerBound).ThenBy(r => r.UpperBound)
				.FirstOrDefault(r => r.LowerBound <= totalCubitFeet && totalCubitFeet < r.UpperBound);
			if (rule == null) return null;

			if (rule.AveragePoundPerCubicFootApplies && totalPoundPerCubitFeet > rule.AveragePoundPerCubicFootLimit) return null;

			return rule;
		}

		internal static string GetNmfcClass(this DiscountTier tier, double freightClass, LTLCubicFootCapacityRule rule = null)
		{
			var fc = tier.FetchFAK(freightClass).ToString();

			if (rule == null) return fc;

			var appliedFcValue = rule.AppliedFreightClass.ToInt();
			if (appliedFcValue == 0 && !rule.ApplyFAK) return freightClass.ToString();
			if (appliedFcValue == 0 && rule.ApplyFAK) return fc;
			if (appliedFcValue != 0 && !rule.ApplyFAK) return rule.AppliedFreightClass.ToString();
			if (appliedFcValue != 0 && rule.ApplyFAK) return tier.FetchFAK(rule.AppliedFreightClass).ToString();

			return fc;
		}


		internal static decimal GetRatedWeight(this LTLCubicFootCapacityRule rule, RatingItem item)
		{
			return (item.RatedLength * item.RatedWidth * item.RatedHeight /
					CubicFootCalculations.CubicFoot) * rule.PenaltyPoundPerCubicFoot;
		}


		public static decimal OutstandingBalance(this Customer customer)
		{
			return customer == null ? 0 : new AccountingFunctions().OutstandingCustomerBalance(customer.Id);
		}


		public static decimal GetResellerAdditions<T>(this T charge, ResellerAddition reseller) where T : Charge
		{
			switch (charge.ChargeCode.Category)
			{
				case ChargeCodeCategory.Fuel:
					return charge.GetResellerAdditions(reseller.FuelPercentage, reseller.FuelValue, reseller.FuelType,
																   reseller.UseFuelMinimum);
				case ChargeCodeCategory.Freight:
					return charge.GetResellerAdditions(reseller.LineHaulPercentage, reseller.LineHaulValue,
																   reseller.LineHaulType, reseller.UseLineHaulMinimum);
				case ChargeCodeCategory.Service:
					return charge.GetResellerAdditions(reseller.ServicePercentage, reseller.ServiceValue,
																   reseller.ServiceType, reseller.UseServiceMinimum);
				case ChargeCodeCategory.Accessorial:
					return charge.GetResellerAdditions(reseller.AccessorialPercentage, reseller.AccessorialValue,
																   reseller.AccessorialType, reseller.UseAccessorialMinimum);
			}
			return 0;
		}

		public static decimal GetResellerAdditions<T>(this T charge, decimal percentageMarkup, decimal valueMarkup, ValuePercentageType markupType, bool useLower) where T : Charge
		{
			var percentage = percentageMarkup / 100;
			var valueBackDown = charge.UnitSell - valueMarkup;
			var percentBackDown = charge.UnitSell / (1 + percentage);
			switch (markupType)
			{
				case ValuePercentageType.Value:
					return charge.UnitSell - valueBackDown;
				case ValuePercentageType.Percentage:
					return charge.UnitSell - percentBackDown;
				default:
					var vp = charge.UnitSell - valueBackDown;
					var pp = charge.UnitSell - percentBackDown;
					return useLower
							? vp < pp ? vp : pp
							: vp > pp ? vp : pp;
			}
		}




		public static bool IsInternational(this Shipment shipment)
		{
			return shipment.Origin != null && shipment.Destination != null &&
				   shipment.Origin.CountryId != shipment.Destination.CountryId;
		}

		public static bool IsInternational(this LoadOrder loadOrder)
		{
			return loadOrder.Origin != null && loadOrder.Destination != null &&
				   loadOrder.Origin.CountryId != loadOrder.Destination.CountryId;
		}

		public static bool CanDeleteLane(long id)
		{
			var valid = true;
			var validator = new CustomerTLTenderingProfileValidator();
			if (!validator.CanDeleteLane(id)) valid = false;

			return valid;

		}


		public static Dictionary<int, string> GetExportFileExtension()
		{
			return new Dictionary<int, string>
			       	{
			       		{ReportExportExtension.xlsx.ToInt(), "Excel (xlsx)"},
			       		{ReportExportExtension.txt.ToInt(), "Text File (txt)"},
                        {ReportExportExtension.csv.ToInt(), "Text File (csv)"}
			       	};
		}


		public static object Value(this DbDataReader reader, string columnName)
		{
			return reader.GetValue(reader.GetOrdinal(columnName));
		}


		public static double DegreeToRadian(this double angle)
		{
			return Math.PI * angle / 180.0;
		}

		public static double RadianToDegree(this double angle)
		{
			return angle * (180.0 / Math.PI);
		}



		public static void AddOrUpdatePlugin(this MileageEngine engine, IMileagePlugin plugin, bool defaultInitialize = true)
		{
			if (!ProcessorVars.MileagePlugins.ContainsKey(engine)) ProcessorVars.MileagePlugins.Add(engine, plugin);
			else ProcessorVars.MileagePlugins[engine] = plugin;


			if (ProcessorVars.MileagePlugins[engine] == null) ProcessorVars.MileagePlugins.Remove(engine);
			else if (!ProcessorVars.MileagePlugins[engine].IsInitialized && defaultInitialize)
				ProcessorVars.MileagePlugins[engine].Initialize();
		}

		public static IMileagePlugin RetrieveEnginePlugin(this MileageEngine engine)
		{
			return ProcessorVars.MileagePlugins.ContainsKey(engine)
					? ProcessorVars.MileagePlugins[engine]
					: null;
		}

		public static bool IsApplicationDefault(this MileageEngine engine)
		{
			return engine == MileageEngine.LongitudeLatitude;
		}


		public static Rate ToRate(this TLSellRate sellRate, Shipment shipment)
		{
			var totalWeight = shipment.Items.TotalActualWeight();
			var rating = shipment.Customer.Rating;
			var freightChargeUnitSell = sellRate.RateType.CalculateRate(sellRate.Rate, sellRate.Mileage, totalWeight, 0m);

			return new Rate
				{
					BilledWeight = totalWeight,
					Mileage = sellRate.Mileage,
					MileageSourceId = sellRate.MileageSourceId,
					RatedCubicFeet = 0,
					CFCRule = null,
					AccessorialProfitAdjustment = 0m,
					ApplyDiscount = false,
					Charges = new List<RateCharge>
						{
							new RateCharge
								{
									TenantId = sellRate.TenantId,
									ChargeCodeId = rating.TLFreightChargeCodeId,
									Comment =
										string.Format("{0} @ {1} ({2}), {3} mi", sellRate.RateType.FormattedString(), sellRate.Rate.ToString("n2"),
										              sellRate.TariffType.FormattedString(), sellRate.Mileage.ToString("n2")),
									Quantity = 1,
									UnitBuy = 0m,
									UnitDiscount = 0m,
									UnitSell = freightChargeUnitSell < sellRate.MinimumCharge ? sellRate.MinimumCharge : freightChargeUnitSell,
								},
							new RateCharge
								{
									TenantId = sellRate.TenantId,
									ChargeCodeId = rating.TLFuelChargeCodeId,
									Comment = string.Format("{0} @ {1}", rating.TLFuelRateType.FormattedString(), rating.TLFuelRate.ToString("n2")),
									Quantity = 1,
									UnitBuy = 0m,
									UnitDiscount = 0m,
									UnitSell = rating.TLFuelRateType.CalculateRate(rating.TLFuelRate, sellRate.Mileage, totalWeight, freightChargeUnitSell),
								}
						},
					DestinationTerminalCode = string.Empty,
					DirectPointRate = true,
					DiscountTier = null,
					EstimatedDeliveryDate = shipment.EstimatedDeliveryDate,
					FreightProfitAdjustment = 0m,
					FuelMarkupPercent = 0m,
					FuelProfitAdjustment = 0m,
					IsLTLPackageSpecificRate = false,
					LTLSellRate = null,
					Mode = ServiceMode.Truckload,
					NoAccessorialProfit = false,
					NoFreightProfit = false,
					NoFuelProfit = false,
					NoServiceProfit = false,
					OriginTerminalCode = string.Empty,
					OriginalRateValue = 0m,
					OverlengthRuleApplied = false,
					RatedWeight = totalWeight,
					ResellerAccessorialMarkup = 0m,
					ResellerAccessorialMarkupIsPercent = false,
					ResellerFreightMarkup = 0m,
					ResellerFreightMarkupIsPercent = false,
					ResellerFuelMarkup = 0m,
					ResellerFuelMarkupIsPercent = false,
					ResellerServiceMarkup = 0m,
					ResellerServiceMarkupIsPercent = false,
					ServiceLevel = sellRate.TariffType.GetString(),
					ServiceProfitAdjustment = 0m,
					SmallPackEngine = SmallPackageEngine.None,
					SmallPackServiceType = string.Empty,
					TransitDays = 0,
					Vendor = null,
				};
		}

		public static Rate ToRate(this PhantomRate phantonRate, Shipment shipment)
		{
			return new Rate
			{
				BilledWeight = shipment.Items.TotalActualWeight(),
				Mileage = phantonRate.Mileage,
				MileageSourceId = shipment.Customer.RequiredMileageSourceId,
				RatedCubicFeet = 0,
				CFCRule = null,
				AccessorialProfitAdjustment = 0m,
				ApplyDiscount = false,
				Charges = phantonRate.Charges.Select(c => new RateCharge(c)).ToList(),
				DestinationTerminalCode = string.Empty,
				DirectPointRate = true,
				DiscountTier = null,
				EstimatedDeliveryDate = shipment.EstimatedDeliveryDate,
				FreightProfitAdjustment = 0m,
				FuelMarkupPercent = 0m,
				FuelProfitAdjustment = 0m,
				IsLTLPackageSpecificRate = false,
				LTLSellRate = null,
				Mode = ServiceMode.Truckload,
				NoAccessorialProfit = false,
				NoFreightProfit = false,
				NoFuelProfit = false,
				NoServiceProfit = false,
				OriginTerminalCode = string.Empty,
				OriginalRateValue = 0m,
				OverlengthRuleApplied = false,
				RatedWeight = shipment.Items.TotalActualWeight(),
				ResellerAccessorialMarkup = 0m,
				ResellerAccessorialMarkupIsPercent = false,
				ResellerFreightMarkup = 0m,
				ResellerFreightMarkupIsPercent = false,
				ResellerFuelMarkup = 0m,
				ResellerFuelMarkupIsPercent = false,
				ResellerServiceMarkup = 0m,
				ResellerServiceMarkupIsPercent = false,
				ServiceLevel = phantonRate.TariffType.GetString(),
				ServiceProfitAdjustment = 0m,
				SmallPackEngine = SmallPackageEngine.None,
				SmallPackServiceType = string.Empty,
				TLRateDetails = phantonRate.TLRateDetails,
				TransitDays = Rate.DefaultTransitTime,
				Vendor = null,
			};
		}


		public static decimal AmountLeftToBeAppliedOrRefunded(this MiscReceipt miscReceipt, long tenantId, long miscReceiptApplicationId = 0)
		{
			return new MiscReceiptSearch().GetMiscReceiptAmountLeftToBeAppliedOrRefunded(miscReceipt.Id, tenantId, miscReceiptApplicationId);
		}

		public static decimal GetAmountPaidToLoadOrderViaMiscReceipts(this LoadOrder loadOrder)
		{
			return new MiscReceiptSearch().GetMiscReceiptsAmountPaidToLoadOrder(loadOrder.Id, loadOrder.TenantId);
		}

		public static decimal GetAmountPaidToShipmentViaMiscReceipts(this Shipment shipment)
		{
			return new MiscReceiptSearch().GetMiscReceiptsAmountPaidToShipment(shipment.Id, shipment.TenantId);
		}


		public static TransactionStatus GetTransactionStatusViaPaymentGateway(this MiscReceipt miscReceipt)
		{
			return miscReceipt.Tenant.GetPaymentGatewayService().GetTransactionStatus(miscReceipt.GatewayTransactionId).TransactionStatus;
		}

		public static PaymentGatewayService GetPaymentGatewayService(this Tenant tenant)
		{
			return new PaymentGatewayService(tenant.PaymentGatewayType,
											 new PaymentGatewayCredentials
												 {
													 LoginId = tenant.PaymentGatewayLoginId,
													 Secret = tenant.PaymentGatewaySecret,
													 TransactionId = tenant.PaymentGatewayTransactionId
												 },
											 tenant.PaymentGatewayInTestMode);
		}


		public static List<List<T>> Batch<T>(this List<T> data, int batchSize)
		{
			var batches = new List<List<T>>();
			for (var i = 0; i < data.Count.ToDouble() / batchSize; i++)
				batches.Add(data.Skip(i * batchSize).Take(batchSize).ToList());
			return batches;
		}



		public static CustomerTLTenderingProfileLane RetrieveMatchingLane(this CustomerTLTenderingProfile profile, LoadOrder order)
		{
			if (profile == null) return null;
			const int coeffientDeault = 10000;
			var match = profile
				.Lanes
				.Select(l =>
					{
						var opc = l.OriginPostalCode.ToLower() == order.Origin.PostalCode.ToLower() &&
						          l.OriginCountryId == order.Origin.CountryId;
						var dpc = l.DestinationPostalCode.ToLower() == order.Destination.PostalCode.ToLower() &&
						          l.DestinationCountryId == order.Destination.CountryId;

						var ocsc = l.OriginCity.ToLower() == order.Origin.City.ToLower()
						           && l.OriginState.ToLower() == order.Origin.State.ToLower()
						           && l.OriginCountryId == order.Origin.CountryId;
						var dcsc = l.DestinationCity.ToLower() == order.Destination.City.ToLower()
						           && l.DestinationState.ToLower() == order.Destination.State.ToLower()
						           && l.DestinationCountryId == order.Destination.CountryId;

						var osc = string.IsNullOrEmpty(l.OriginCity)
						          && l.OriginState.ToLower() == order.Origin.State.ToLower()
						          && l.OriginCountryId == order.Origin.CountryId;
						var dsc = string.IsNullOrEmpty(l.DestinationCity)
						          && l.DestinationState.ToLower() == order.Destination.State.ToLower()
						          && l.DestinationCountryId == order.Destination.CountryId;

						var weight = coeffientDeault;

						// postal && postal 
						if (opc && dpc && weight > 0) weight = 0;

						// postal code & city state country
						if (opc && dcsc && weight > 1) weight = 1;
						if (ocsc && dpc && weight > 1) weight = 1;

						// city state country & city state country
						if (ocsc && dcsc && weight > 2) weight = 2;

						// postal code & state and country
						if (opc && dsc && weight > 3) weight = 3;
						if (osc && dpc && weight > 3) weight = 3;

						// city state country & city state
						if (ocsc && dsc && weight > 4) weight = 4;
						if (osc && dcsc && weight > 4) weight = 4;

						// state country & state country
						if (osc && dsc && weight > 5) weight = 5;

						return new {Lane = l, Coeffient = weight};
					})
				.Where(i => i.Coeffient < coeffientDeault)
				.OrderBy(i => i.Coeffient)
				.FirstOrDefault();

			return match == null ? null : match.Lane;
		}
		
		public static TruckloadBid CheckIfTrcuckloadBidExist(this long loadOrderId, long tenantId)
		{
			var truckloadBid = new TruckloadBidSearch().FetchTruckloadBidByLoadOrderIdAndStatus(loadOrderId, TruckloadBidStatus.Processing.ToInt(),tenantId);
			if (truckloadBid != null)
				return truckloadBid;

			return null;
		}

        public static decimal GetMarkupPercent(this LTLSellRate sellRate, decimal totalShipmentWeight)
        {
            var weightBreak = totalShipmentWeight.GetBilledWeightBreak();

            if (totalShipmentWeight <= sellRate.StartOverrideWeightBreak.GetWeightBreakValue().ToDecimal() && sellRate.StartOverrideWeightBreak != WeightBreak.Ignore)
            {
                switch (weightBreak)
                {
                    case WeightBreak.L5C:
                        return sellRate.OverrideMarkupPercentL5C;
                    case WeightBreak.M5C:
                        return sellRate.OverrideMarkupPercentM5C;
                    case WeightBreak.M1M:
                        return sellRate.OverrideMarkupPercentM1M;
                    case WeightBreak.M2M:
                        return sellRate.OverrideMarkupPercentM2M;
                    case WeightBreak.M5M:
                        return sellRate.OverrideMarkupPercentM5M;
                    case WeightBreak.M10M:
                        return sellRate.OverrideMarkupPercentM10M;
                    case WeightBreak.M20M:
                        return sellRate.OverrideMarkupPercentM20M;
                    case WeightBreak.M30M:
                        return sellRate.OverrideMarkupPercentM30M;
                    case WeightBreak.M40M:
                        return sellRate.OverrideMarkupPercentM40M;
                }
            }

            return sellRate.MarkupPercent;
        }

	    public static decimal GetMarkupValue(this LTLSellRate sellRate, decimal totalShipmentWeight)
	    {
	        var weightBreak = totalShipmentWeight.GetBilledWeightBreak();

	        if (totalShipmentWeight <= sellRate.StartOverrideWeightBreak.GetWeightBreakValue().ToDecimal() &&
	            sellRate.StartOverrideWeightBreak != WeightBreak.Ignore)
	        {
	            switch (weightBreak)
	            {
	                case WeightBreak.L5C:
	                    return sellRate.OverrideMarkupValueL5C;
	                case WeightBreak.M5C:
	                    return sellRate.OverrideMarkupValueM5C;
	                case WeightBreak.M1M:
	                    return sellRate.OverrideMarkupValueM1M;
	                case WeightBreak.M2M:
	                    return sellRate.OverrideMarkupValueM2M;
	                case WeightBreak.M5M:
	                    return sellRate.OverrideMarkupValueM5M;
	                case WeightBreak.M10M:
	                    return sellRate.OverrideMarkupValueM10M;
	                case WeightBreak.M20M:
	                    return sellRate.OverrideMarkupValueM20M;
	                case WeightBreak.M30M:
	                    return sellRate.OverrideMarkupValueM30M;
	                case WeightBreak.M40M:
	                    return sellRate.OverrideMarkupValueM40M;
	            }
	        }

	        return sellRate.MarkupValue;
	    }



		public static List<ShipmentCharge> VendorApplicableCharges(this Shipment shipment, Vendor compareVendor)
		{
			return shipment.VendorApplicableCharges(compareVendor.Id);
		}

		public static List<ShipmentCharge> VendorApplicableCharges(this Shipment shipment, long compareVendorId)
		{
			var isShipmentPrimaryVendor = shipment.Vendors.First(p => p.Primary).VendorId == compareVendorId;
			return shipment.Charges
			               .Where(c => c.VendorId == compareVendorId || (c.VendorId == default(long) && isShipmentPrimaryVendor))
			               .ToList();
		}

		public static List<ServiceTicketCharge> VendorApplicableCharges(this ServiceTicket ticket, Vendor compareVendor)
		{
			return ticket.VendorApplicableCharges(compareVendor.Id);
		}

		public static List<ServiceTicketCharge> VendorApplicableCharges(this ServiceTicket ticket, long compareVendorId)
		{
			var isShipmentPrimaryVendor = ticket.Vendors.First(p => p.Primary).VendorId == compareVendorId;
			return ticket.Charges
						   .Where(c => c.VendorId == compareVendorId || (c.VendorId == default(long) && isShipmentPrimaryVendor))
						   .ToList();
		}


	    public static bool EmailIsValid(this string email)
	    {
	        return Regex.Match(email, EmailRegExPattern, RegexOptions.IgnoreCase).Success;
	    }


	    public static int FindLastOccurance(this string data, string lookUpString, int length)
	    {
            var lastPosition = data.Substring(0, length).LastIndexOf(lookUpString, StringComparison.Ordinal);

	        return lastPosition <= 0 ? length : lastPosition;
	    }
    }
}
