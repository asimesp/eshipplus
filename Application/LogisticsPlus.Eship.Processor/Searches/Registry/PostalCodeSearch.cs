﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class PostalCodeSearch : EntityBase
	{
	    public List<PostalCodeViewSearchDto> FetchPostalCodes(PostalCodeViewSearchCriteria criteria )
        {
            var codes = new List<PostalCodeViewSearchDto>();
            var query = @"SELECT TOP 10000 * FROM PostalCodeViewSearch WHERE ";

            var parameters = new Dictionary<string, object>();
           
            query += criteria.Parameters.BuildWhereClause(parameters, RegistrySearchFields.PostalCodes);

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    codes.Add(new PostalCodeViewSearchDto(reader));
            Connection.Close();
            return codes;
        }

        public List<PostalCodeViewSearchDto> FetchAllPostalCodes()
        {
            var codes = new List<PostalCodeViewSearchDto>();
            const string query = @"SELECT * FROM PostalCodeViewSearch";

            var parameters = new Dictionary<string, object>();

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    codes.Add(new PostalCodeViewSearchDto(reader));
            Connection.Close();
            return codes;
        }

		public PostalCodeViewSearchDto FetchPostalCodeByCode(string code, long countryId)
		{
			var codes = new List<PostalCodeViewSearchDto>();
			const string query = @"Select * from PostalCodeViewSearch where Code = @Code AND CountryId = @CountryId";
			var parameters = new Dictionary<string, object> {{"Code", code}, {"CountryId", countryId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					codes.Add(new PostalCodeViewSearchDto(reader));
			Connection.Close();

			return codes.Count == 0 ? new PostalCodeViewSearchDto() : codes.FirstOrDefault(c => c.Primary) ?? codes[0];
		}

		public List<PostalCodeViewSearchDto> FetchPostalCodesByCodeAndState(string code, string state)
		{
			var codes = new List<PostalCodeViewSearchDto>();
			const string query = @"Select * from PostalCodeViewSearch where Code = @Code AND State = @State";

			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", code},
			                 		{"State", state}
			                 	};
			
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					codes.Add(new PostalCodeViewSearchDto(reader));
			Connection.Close();
			return codes;
		}

		public PostalCode CurrentPrimaryPostalCode(string code, long countryId)
		{
			const string query =
				@"Select * from PostalCode where PostalCode.Code = @Code and PostalCode.CountryId = @CountryId and PostalCode.[Primary] = @Primary";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", code},
			                 		{"CountryId", countryId},
			                 		{"Primary", true}
			                 	};
			PostalCode postalCode = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					postalCode = new PostalCode(reader);
			Connection.Close();
			return postalCode;
		}
	}
}
