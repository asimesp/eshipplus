﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class ContactTypeSearch : EntityBase
	{
		public List<ContactType> FetchContactTypes(string criteria, long tenantId)
		{
			var contactTypes = new List<ContactType>();
			const string query = "SELECT * FROM ContactType WHERE (Code LIKE @Criteria OR Description LIKE @Criteria) AND TenantId = @TenantId";
			var parameter = new Dictionary<string, object> {{"Criteria", criteria}, {"TenantId", tenantId}};
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					contactTypes.Add(new ContactType(reader));
			Connection.Close();
			return contactTypes;
		}

		public long FetchContactTypeIdByCode(string code, long tenantId)
		{
			const string query = "SELECT [Id] FROM ContactType WHERE Code = @Code AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "Code", code }, { "TenantId", tenantId } };
			return ExecuteScalar(query, parameters).ToLong();
		}

	}
}
