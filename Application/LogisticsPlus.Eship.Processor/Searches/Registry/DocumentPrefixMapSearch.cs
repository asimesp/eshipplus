﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class DocumentPrefixMapSearch : EntityBase
	{
		public List<DocumentPrefixMapViewSearchDto> FetchDocumentPrefixMaps(long tenantId)
		{
			var codes = new List<DocumentPrefixMapViewSearchDto>();
			const string query = "Select * from DocumentPrefixMapViewSearch where TenantId = @TenantId";
			var parameter = new Dictionary<string, object> { { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					codes.Add(new DocumentPrefixMapViewSearchDto(reader));
			Connection.Close();
			return codes;
		}
	}
}
