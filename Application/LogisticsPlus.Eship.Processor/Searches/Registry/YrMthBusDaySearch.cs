﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
    public class YrMthBusDaySearch : EntityBase
    {
        public List<YrMthBusDay> FetchYrMthBusDays(string criteria, long tenantId)
        {
            var mthBusDays = new List<YrMthBusDay>();
            const string query = @"SELECT * FROM YrMthBusDay WHERE ([Year] LIKE @Criteria) AND TenantId = @TenantId";
            var parameter = new Dictionary<string, object> { { "Criteria", criteria }, { "TenantId", tenantId } };
            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    mthBusDays.Add(new YrMthBusDay(reader));
            Connection.Close();
            return mthBusDays;
        }
         
    }
}