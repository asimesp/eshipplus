﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class P44ServiceMappingSearch : EntityBase
	{
		public List<P44ServiceMapping> FetchAllP44ServiceMappings(long tenantId)
		{
			var codes = new List<P44ServiceMapping>();

            var query =
                $"SELECT * FROM P44ServiceMapping WHERE TenantId = @TenantId ORDER BY Id {SearchUtilities.Ascending}";
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					codes.Add(new P44ServiceMapping(reader));
			Connection.Close();
			return codes;
		}

	    public List<P44ServiceMapping> FetchP44ServiceMappings(List<ParameterColumn> columns , long tenantId)
        {
            var codes = new List<P44ServiceMapping>();
            
            var query = "SELECT * FROM P44ServiceMapping WHERE TenantId = @TenantId AND";
            
            var parameters = new Dictionary<string, object> {{ "TenantId", tenantId }};

            query += columns.BuildWhereClause(parameters, RegistrySearchFields.P44ServiceMappings);

            query += $"ORDER BY Id {SearchUtilities.Ascending}";

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    codes.Add(new P44ServiceMapping(reader));
            Connection.Close();
            return codes;
        }

	    public long FetchP44ServiceMappingIdByProject44Code(int p44Code, long tenantId)
	    {
	        const string query = "Select [Id] from P44ServiceMapping where Project44Code = @Code and TenantId = @TenantId";
	        var parameters = new Dictionary<string, object> { { "Code", p44Code }, { "TenantId", tenantId } };
	        return ExecuteScalar(query, parameters).ToLong();
	    }
	}
}
