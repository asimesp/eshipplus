﻿using System.Collections.Generic;
using System.Data;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
    public static class RegistrySearchFields
    {
        private static readonly object LockObj = new object();

        private static bool _isInitialized;

        public static SearchField Active { get; set; }
        public static SearchField City { get; set; }
        public static SearchField CityAlias { get; set; }
        public static SearchField Code { get; set; }
        public static SearchField CountryName { get; set; }
		public static SearchField Description { get; set; }
        public static SearchField State { get; set; }
        public static SearchField Year { get; set; }

        //---------------------------------------

        //Default search field lists
        public static List<SearchField> DefaultPostalCodes { get; set; }
        public static List<SearchField> DefaultServices { get; set; }
        //---------------------------------------

        //Search Fields Lists
        public static List<SearchField> AccountBuckets { get; set; }
        public static List<SearchField> DocumentTags { get; set; }
        public static List<SearchField> EquipmentTypes { get; set; }
        public static List<SearchField> FailureCodes { get; set; }
        public static List<SearchField> FuelTables { get; set; }
        public static List<SearchField> NoServiceDays { get; set; }
        public static List<SearchField> PostalCodes { get; set; }
        public static List<SearchField> Prefixes { get; set; }
        public static List<SearchField> Services { get; set; }
        public static List<SearchField> P44ChargeCodeMappings { get; set; }
        public static List<SearchField> P44ServiceMappings { get; set; }
        public static List<SearchField> QuickPayOptions { get; set; }
        //---------------------------------------

        //Search Fields Sort Lists
        public static List<SearchField> PostalCodeSortFields { get; set; }
        //---------------------------------------
        public static void Initialize()
        {
            if (_isInitialized) return;

            var anotherLockInitialized = false;
            lock (LockObj)
            {
                if (!_isInitialized) _isInitialized = true;
                else anotherLockInitialized = true;
            }

            if (anotherLockInitialized) return;

            Active = new SearchField { Name = "Active", DataType = SqlDbType.Bit };
            City = new SearchField {Name = "City", DataType = SqlDbType.NVarChar};
            CityAlias = new SearchField {Name = "CityAlias", DataType = SqlDbType.NVarChar};
            Code = new SearchField { Name = "Code", DataType = SqlDbType.NVarChar };
            CountryName = new SearchField { Name = "CountryName", DataType = SqlDbType.NVarChar };
	        Description = new SearchField {Name = "Description", DataType = SqlDbType.NVarChar};
            State = new SearchField { Name = "State", DataType = SqlDbType.NVarChar };
            Year = new SearchField { Name = "YEAR(DateOfNoService)", DisplayName = "Year", DataType = SqlDbType.Int };

            // ----------------------------------------------

            //if any Default Search Field Lists, Initialize them here
            DefaultPostalCodes = new List<SearchField>
                                     {
                                          Code,
                                          new SearchField {Name = "City", DataType = SqlDbType.NVarChar},
                                          new SearchField {Name = "State", DataType = SqlDbType.NVarChar},
                                          CountryName
                                     };
            DefaultServices = new List<SearchField>
                                     {
                                         Code,
                                         Description,
                                     };

            // ----------------------------------------------


            AccountBuckets = new List<SearchField>
                              {
                                  Active,
                                  Code,
                                  Description,
                              };

            DocumentTags = new List<SearchField>
                              {
                                  Code,
                                  Description,
                              };

            EquipmentTypes = new List<SearchField>
                              {
                                  new SearchField {Name = "TypeName", DisplayName = "Equipment Type Name", DataType = SqlDbType.NVarChar},
                              };

            FailureCodes = new List<SearchField>
                              {
                                  Code,
                                  Description,
                                  new SearchField {Name = "Active", DataType = SqlDbType.Bit},
                                  new SearchField{Name="CategoryText", DataType = SqlDbType.NVarChar}
                              };

            FuelTables = new List<SearchField>
                              {
                                  new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},
                              };

            NoServiceDays = new List<SearchField>
                              {
                                  Description,
                                  Year
                              };

            PostalCodeSortFields = new List<SearchField>
                                       {
                                           Code,
                                           City,
                                           CityAlias,
                                           State,
                                           CountryName,
                                       };

            PostalCodes = new List<SearchField>
                              {
                                  new SearchField {Name = "CountryCode", DataType = SqlDbType.NVarChar},
                                  new SearchField {Name = "[Primary]", DisplayName = "Primary", DataType = SqlDbType.Bit},
                              };
            PostalCodes.AddRange(PostalCodeSortFields);

            Prefixes = new List<SearchField>
                              {
                                  Active,
                                  Code,
                                  Description,
                              };

            Services = new List<SearchField>
                              {
                                  Code,
                                  Description,
                                  new SearchField {Name = "ChargeCode", DataType = SqlDbType.NVarChar},
                                  new SearchField {Name = "ChargeCodeDescription", DataType = SqlDbType.NVarChar},
                              };

            QuickPayOptions = new List<SearchField>
                              {
                                  Active,
                                  Code,
                                  Description,
                              };
        }
    }
}