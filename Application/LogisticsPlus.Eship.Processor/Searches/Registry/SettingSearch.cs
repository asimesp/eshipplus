﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
    public class SettingSearch : EntityBase
    {
        public List<Setting> FetchSettings(string criteria, long tenantId)
        {
            var settings = new List<Setting>();
            const string query = "Select * from Setting where (Code like @Criteria or Value like @Criteria) and TenantId = @TenantId";
            var parameter = new Dictionary<string, object> { { "Criteria", criteria }, { "TenantId", tenantId } };
            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    settings.Add(new Setting(reader));
            Connection.Close();
            return settings;
        }

		public Setting FetchSettingByCode(SettingCode code, long tenantId)
		{
			Setting setting = null;
			const string query = "Select * from Setting where Code = @Code and TenantId = @TenantId";
			var parameter = new Dictionary<string, object> {{"Code", code}, {"TenantId", tenantId}};
			using (var reader = GetReader(query, parameter))
				if (reader.Read())
					setting = new Setting(reader);
			Connection.Close();
			return setting ?? new Setting();
		}
    }
}
