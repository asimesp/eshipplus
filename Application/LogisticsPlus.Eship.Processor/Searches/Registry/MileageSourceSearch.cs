﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class MileageSourceSearch : EntityBase
	{
		public List<MileageSource> FetchMileageSources(string criteria, long tenantId)
		{
			var mileageSources = new List<MileageSource>();
			var query = string.Format(@"Select * from MileageSource where (Code like @Criteria or Description like @Criteria) and TenantId = @TenantId
							Order by Code {0}", SearchUtilities.Ascending);
			var parameter = new Dictionary<string, object> { { "Criteria", criteria }, { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					mileageSources.Add(new MileageSource(reader));
			Connection.Close();
			return mileageSources;
		}

		public long FetchMileageSourceIdByCode(string code, long tenantId)
		{
			const string query = "Select [Id] from MileageSource where Code = @Code and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"Code", code}, {"TenantId", tenantId}};
			return ExecuteScalar(query, parameters).ToLong();
		}

		public MileageSource CurrentPrimaryMileageSource(long tenantId)
		{
			const string query = "Select * from MileageSource where TenantId = @TenantId and [Primary] = @Primary";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"TenantId", tenantId},
			                 		{"Primary", true}
			                 	};
			MileageSource mileageSource = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					mileageSource = new MileageSource(reader);
			Connection.Close();
			return mileageSource;
		}
	}
}
