﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class PrefixSearch : EntityBase
	{
	    public List<Prefix> FetchPrefixes(List<ParameterColumn> columns, long tenantId)
        {
            var prefixes = new List<Prefix>();
            
            var query = @"SELECT * FROM Prefix WHERE TenantId = @TenantId AND ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameters, RegistrySearchFields.Prefixes);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    prefixes.Add(new Prefix(reader));
            Connection.Close();
            return prefixes;
        }

		public Prefix FetchPrefixByCode(string code, long tenantId)
		{
			Prefix prefix = null;
			const string query = "Select * from Prefix where Code = @Code and TenantId = @TenantId";
			var parameter = new Dictionary<string, object> {{"Code", code}, {"TenantId", tenantId}};
			using (var reader = GetReader(query, parameter))
				if (reader.Read())
					prefix = new Prefix(reader);
			Connection.Close();
			return prefix;
		}
	}
}
