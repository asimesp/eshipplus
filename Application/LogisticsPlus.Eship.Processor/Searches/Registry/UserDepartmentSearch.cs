﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
    public class UserDepartmentSearch : EntityBase
    {
        public List<UserDepartment> FetchUserDepartments(string criteria, long tenantId)
        {
            var userDepartments = new List<UserDepartment>();
            const string query = @"SELECT * FROM UserDepartment WHERE 
				(Code LIKE @Criteria 
					OR [Description] LIKE @Criteria
					OR Phone LIKE @Criteria
					OR Fax LIKE @Criteria
					OR Email LIKE @Criteria) 
				AND TenantId = @TenantId";
            var parameter = new Dictionary<string, object> { { "Criteria", criteria }, { "TenantId", tenantId } };
            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    userDepartments.Add(new UserDepartment(reader));
            Connection.Close();
            return userDepartments;
        }
    }
}
