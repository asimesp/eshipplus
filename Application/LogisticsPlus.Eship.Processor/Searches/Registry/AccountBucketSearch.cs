﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class AccountBucketSearch : EntityBase
	{
	    public List<AccountBucket> FetchAccountBuckets(List<ParameterColumn> columns , long tenantId)
        {
            var accountBuckets = new List<AccountBucket>();

            var query = @"SELECT * FROM AccountBucket WHERE TenantId = @TenantId AND ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameters, RegistrySearchFields.AccountBuckets);

            query += string.Format("ORDER BY Code {0}", SearchUtilities.Ascending);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    accountBuckets.Add(new AccountBucket(reader));
            Connection.Close();
            return accountBuckets;
        }

	    public AccountBucket FetchAccountBucketByCode(string code, long tenantId)
		{
			const string query = @"Select * from AccountBucket where Code = @Code and TenantId = @TenantId";
			var parameter = new Dictionary<string, object> {{"Code", code}, {"TenantId", tenantId}};
			AccountBucket accountBucket = null;
			using (var reader = GetReader(query, parameter))
				if (reader.Read())
					accountBucket = new AccountBucket(reader);
			Connection.Close();
			return accountBucket;
		}
	}
}
