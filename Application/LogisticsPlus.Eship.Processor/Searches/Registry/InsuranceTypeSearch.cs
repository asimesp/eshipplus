﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class InsuranceTypeSearch : EntityBase
	{
		public List<InsuranceType> FetchInsuranceTypes(string criteria, long tenantId)
		{
			var insuranceTypes = new List<InsuranceType>();
			const string query = "Select * from InsuranceType where (Code like @Criteria or Description like @Criteria) and TenantId = @TenantId";
			var parameter = new Dictionary<string, object> {{"Criteria", criteria}, {"TenantId", tenantId}};
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					insuranceTypes.Add(new InsuranceType(reader));
			Connection.Close();
			return insuranceTypes;
		}
	}
}
