﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class QuickPayOptionSearch : EntityBase
	{
	    public List<QuickPayOption> FetchQuickPayOptions(List<ParameterColumn> columns , long tenantId)
        {
            var codes = new List<QuickPayOption>();
            
            var query = @"SELECT * FROM QuickPayOption WHERE TenantId = @TenantId AND ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameters, RegistrySearchFields.QuickPayOptions);

            query += string.Format("ORDER BY Code {0}", SearchUtilities.Ascending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    codes.Add(new QuickPayOption(reader));
            Connection.Close();
            return codes;
        }
	}
}
