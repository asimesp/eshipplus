﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class ShipmentPrioritySearch : EntityBase
	{
		public List<ShipmentPriority> FetchShipmentPriorities(string criteria, long tenantId)
		{
			var codes = new List<ShipmentPriority>();
			const string query = "Select * from ShipmentPriority where (Code like @Criteria or [Description] like @Criteria) and TenantId = @TenantId";
			var parameter = new Dictionary<string, object> { { "Criteria", criteria }, { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					codes.Add(new ShipmentPriority(reader));
			Connection.Close();
			return codes;
		}

		public ShipmentPriority CurrentDefaultShipmentPriority(long tenantId)
		{
			const string query = "Select * from ShipmentPriority where TenantId = @TenantId and [Default] = @Default";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"TenantId", tenantId},
			                 		{"Default", true}
			                 	};
			ShipmentPriority shipmentPriority = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					shipmentPriority = new ShipmentPriority(reader);
			Connection.Close();
			return shipmentPriority;
		}

		public ShipmentPriority FetchShipmentPriorityByCode(string code, long tenantId)
		{
			ShipmentPriority priority = null;
			const string query = "Select * from ShipmentPriority where Code = @Code and TenantId = @TenantId";
			var parameter = new Dictionary<string, object> { { "Code", code }, { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameter))
				if (reader.Read())
					priority = new ShipmentPriority(reader);
			Connection.Close();
			return priority;
		}
	}
}
