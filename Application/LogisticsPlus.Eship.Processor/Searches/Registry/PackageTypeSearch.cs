﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class PackageTypeSearch : EntityBase
	{
		public List<PackageType> FetchPackageTypes(string criteria, long tenantId)
		{
			var packageTypeNames = new List<PackageType>();
			const string query = "SELECT * FROM PackageType WHERE TypeName LIKE @Criteria AND TenantId = @TenantId";
			var parameter = new Dictionary<string, object> { { "Criteria", criteria }, { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					packageTypeNames.Add(new PackageType(reader));
			Connection.Close();
			return packageTypeNames;
		}

        public long FetchPackageTypeIdByTypeName(string typeName, long tenantId)
        {
            const string query = "SELECT [Id] FROM PackageType WHERE TypeName = @TypeName";
            var parameters = new Dictionary<string, object> { { "TypeName", typeName }, {"TenantId",tenantId } };
            return ExecuteScalar(query, parameters).ToLong();
        }

		public long FetchPackageTypeIdByEdiOid(string ediOid, long tenantId)
		{
			const string query = "SELECT [Id] FROM PackageType WHERE EdiOid = @EdiOid";
			var parameters = new Dictionary<string, object> { { "EdiOid", ediOid }, { "TenantId", tenantId } };
			return ExecuteScalar(query, parameters).ToLong();
		}
	}
}
