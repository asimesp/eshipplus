﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
    public class EquipmentTypeSearch : EntityBase
    {
        public List<EquipmentType> FetchEquipmentTypes(List<ParameterColumn> columns, long tenantId)
        {
            var equipmentTypes = new List<EquipmentType>();
            var query = "SELECT * FROM EquipmentType WHERE TenantId = @TenantId AND";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameters, RegistrySearchFields.EquipmentTypes);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    equipmentTypes.Add(new EquipmentType(reader));
            Connection.Close();
            return equipmentTypes;
        }

		public EquipmentType FetchEquipmentTypeByCode(string code, long tenantId)
		{
			const string query = "SELECT * FROM EquipmentType WHERE TenantId = @TenantId AND Code = @Code";
			var parameters = new Dictionary<string, object> {{"TenantId", tenantId}, {"Code", code}};
			EquipmentType et = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					et = new EquipmentType(reader);
			Connection.Close();
			return et;
		}
    }
}
