﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class DocumentTemplateSearch : EntityBase
	{
		public List<DocumentTemplate> FetchDocumentTemplates(string criteria, long tenantId)
		{
			var documentTemplates = new List<DocumentTemplate>();
			var query = string.Format(@"Select * from DocumentTemplate where (Code like @Criteria or Description like @Criteria) and TenantId = @TenantId
					Order By ServiceMode {0}, Category {0}, Code {0}", SearchUtilities.Ascending);
			var parameter = new Dictionary<string, object> {{"Criteria", criteria},{"TenantId", tenantId}};
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					documentTemplates.Add(new DocumentTemplate(reader));
			Connection.Close();
			return documentTemplates;
		}

		public DocumentTemplate CurrentPrimaryDocumentTemplate(long tenantId, ServiceMode serviceMode, DocumentTemplateCategory category)
		{
			const string query =
				@"Select * from DocumentTemplate where [Primary] = @Primary and ServiceMode = @ServiceMode
					and Category = @Category and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Primary", true},
			                 		{"ServiceMode", serviceMode},
			                 		{"Category", category},
			                 		{"TenantId", tenantId}
			                 	};
			DocumentTemplate documentTemplate = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					documentTemplate = new DocumentTemplate(reader);
			Connection.Close();
			return documentTemplate;
		}

		public DocumentTemplate FetchApplicableDocumentTemplate(long tenantId, long prefixId, ServiceMode serviceMode, DocumentTemplateCategory category)
		{
			const string query = "Exec dbo.ApplicableDocumentTemplateSearch @PrefixId, @TenantId, @Category, @ServiceMode";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"PrefixId", prefixId},
			                 		{"TenantId", tenantId},
			                 		{"Category", category},
			                 		{"ServiceMode", serviceMode},
			                 	};
			DocumentTemplate documentTemplate = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					documentTemplate = new DocumentTemplate(reader);
			Connection.Close();
			return documentTemplate;
		}

        public DocumentTemplate FetchApplicableDocumentTemplate(long tenantId, long prefixId, DocumentTemplateCategory category)
        {
            return FetchApplicableDocumentTemplate(tenantId, prefixId, ServiceMode.NotApplicable, category);
        }
	}
}
