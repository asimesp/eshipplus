﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class PermissionDetailSearch : EntityBase
	{
		public List<PermissionDetail> FetchPermissionDetails(long tenantId)
		{
			var details = new List<PermissionDetail>();
			const string query = "Select * from PermissionDetail where TenantId = @TenantId";
			var parameter = new Dictionary<string, object> { { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					details.Add(new PermissionDetail(reader));
			Connection.Close();
			return details;
		}

		public PermissionDetail FetchPermissionDetailByCode(string code, long tenantId)
		{
			const string query = "Select * from PermissionDetail where Code = @Code and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "Code", code }, { "TenantId", tenantId } };
			PermissionDetail detail = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					detail = new PermissionDetail(reader);
			return detail;
		}
	}
}
