﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class NoServiceDaySearch : EntityBase
	{
	    public List<NoServiceDay> FetchNoServiceDays(List<ParameterColumn> columns , long tenantId)
        {
            var noServiceDays = new List<NoServiceDay>();

            var query = "SELECT * FROM NoServiceDay WHERE TenantId = @TenantId AND ";

            var parameters = new Dictionary<string, object> {{ "TenantId", tenantId }};

            //change year from int to string so query will work
            var searchFields = RegistrySearchFields.NoServiceDays;
            foreach (var searchField in searchFields.Where( sf => sf.Name == RegistrySearchFields.Year.Name))
                searchField.DataType = SqlDbType.NVarChar;

            query += columns.BuildWhereClause(parameters,searchFields);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    noServiceDays.Add(new NoServiceDay(reader));
            Connection.Close();
            return noServiceDays;
        }

		public List<NoServiceDay> FetchYearNoServiceDays(string year, long tenantId)
		{
			var noServiceDays = new List<NoServiceDay>();
			const string query = "select * from NoServiceDay where YEAR(DateOfNoService) = @Year and TenantId = @TenantId";
			var parameter = new Dictionary<string, object> { { "Year", year }, { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					noServiceDays.Add(new NoServiceDay(reader));
			Connection.Close();
			return noServiceDays;
		}
	}
}
