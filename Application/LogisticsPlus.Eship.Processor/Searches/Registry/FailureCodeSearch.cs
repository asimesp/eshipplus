﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
    public class FailureCodeSearch : EntityBase
    {
        public List<FailureCode> FetchFailureCodes(List<ParameterColumn> columns, long tenantId)
		{
			var failureCodes = new List<FailureCode>();

		    var query = "SELECT * FROM FailureCodeViewSearch WHERE TenantId = @TenantId AND ";

			var parameters = new Dictionary<string, object>{{"TenantId", tenantId}};

            query += columns.BuildWhereClause(parameters, RegistrySearchFields.FailureCodes);

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					failureCodes.Add(new FailureCode(reader));
			Connection.Close();
			return failureCodes;
		}

		public FailureCode FetchFailureCodeByCode(string code, long tenantId)
		{
			FailureCode failureCode = null;
			const string query = "Select * from FailureCode where Code = @Code and TenantId = @TenantId";
			var parameter = new Dictionary<string, object> { { "Code", code }, { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameter))
				if (reader.Read())
					failureCode = new FailureCode(reader);
			Connection.Close();
			return failureCode;
		}
    }
}
