﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class ServiceSearch : EntityBase
	{
		public List<Service> FetchAllServices(long tenantId)
		{
			var codes = new List<Service>();

            var query = string.Format("SELECT * FROM Service WHERE TenantId = @TenantId ORDER BY Code {0}", SearchUtilities.Ascending);
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					codes.Add(new Service(reader));
			Connection.Close();
			return codes;
		}

	    public List<ServiceViewSearchDto> FetchServices(List<ParameterColumn> columns , long tenantId)
        {
            var codes = new List<ServiceViewSearchDto>();
            
            var query = "SELECT * FROM ServiceViewSearch WHERE TenantId = @TenantId AND";
            
            var parameters = new Dictionary<string, object> {{ "TenantId", tenantId }};

            query += columns.BuildWhereClause(parameters, RegistrySearchFields.Services);

            query += string.Format("ORDER BY Code {0}", SearchUtilities.Ascending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    codes.Add(new ServiceViewSearchDto(reader));
            Connection.Close();
            return codes;
        }

	    public long FetchServiceIdByProject44Code(int p44Code, long tenantId)
	    {
	        const string query = "Select [Id] from Service where Project44Code = @Code and TenantId = @TenantId";
	        var parameters = new Dictionary<string, object> { { "Code", p44Code }, { "TenantId", tenantId } };
	        return ExecuteScalar(query, parameters).ToLong();
	    }

        public List<ServiceViewSearchDto> FetchServicesByCategory(ServiceCategory category, long tenantId)
		{
			var codes = new List<ServiceViewSearchDto>();
			var query =
                string.Format("SELECT * FROM ServiceViewSearch WHERE Category = @Category AND TenantId = @TenantId ORDER BY Code {0}", SearchUtilities.Ascending);
			var parameter = new Dictionary<string, object> {{"Category",  category.ToInt()}, {"TenantId", tenantId}};
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					codes.Add(new ServiceViewSearchDto(reader));
			Connection.Close();
			return codes;
		}

		public long FetchServiceIdByCode(string code, long tenantId)
		{
            const string query = "SELECT [Id] FROM ServiceViewSearch WHERE Code = @Code AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "Code", code }, { "TenantId", tenantId } };
			return ExecuteScalar(query, parameters).ToLong();
		}
	}
}
