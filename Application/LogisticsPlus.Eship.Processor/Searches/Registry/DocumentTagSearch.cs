﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class DocumentTagSearch : EntityBase
	{
	    public List<DocumentTag> FetchDocumentTags(List<ParameterColumn> columns , long tenantId)
        {
            var documentTags = new List<DocumentTag>();
            
            var query = "SELECT * FROM DocumentTag WHERE TenantId = @TenantId AND ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameters, RegistrySearchFields.DocumentTags);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    documentTags.Add(new DocumentTag(reader));
            Connection.Close();
            return documentTags;
        }
	}
}
