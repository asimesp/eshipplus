﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class P44ChargeCodeMappingSearch : EntityBase
	{
		public List<P44ChargeCodeMapping> FetchAllP44ChargeCodeMappings(long tenantId)
		{
			var codes = new List<P44ChargeCodeMapping>();

            var query = string.Format("SELECT * FROM P44ChargeCodeMapping WHERE TenantId = @TenantId ORDER BY Id {0}", SearchUtilities.Ascending);
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					codes.Add(new P44ChargeCodeMapping(reader));
			Connection.Close();
			return codes;
		}

	    public List<P44ChargeCodeMapping> FetchP44ChargeCodeMappings(List<ParameterColumn> columns , long tenantId)
        {
            var codes = new List<P44ChargeCodeMapping>();
            
            var query = "SELECT * FROM P44ChargeCodeMapping WHERE TenantId = @TenantId AND";
            
            var parameters = new Dictionary<string, object> {{ "TenantId", tenantId }};

            query += columns.BuildWhereClause(parameters, RegistrySearchFields.P44ChargeCodeMappings);

            query += $"ORDER BY Id {SearchUtilities.Ascending}";

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    codes.Add(new P44ChargeCodeMapping(reader));
            Connection.Close();
            return codes;
        }
        public long FetchP44ChargeCodeIdByProject44Code(int p44Code, long tenantId)
        {
            const string query = "Select ChargeCodeId from P44ChargeCodeMapping where Project44Code = @Code and TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "Code", p44Code }, { "TenantId", tenantId } };
            return ExecuteScalar(query, parameters).ToLong();
        }
        public long FetchP44ChargeCodeMappingIdByProject44Code(int p44Code, long tenantId)
	    {
	        const string query = "Select [Id] from P44ChargeCodeMapping where Project44Code = @Code and TenantId = @TenantId";
	        var parameters = new Dictionary<string, object> { { "Code", p44Code }, { "TenantId", tenantId } };
	        return ExecuteScalar(query, parameters).ToLong();
	    }
	}
}
