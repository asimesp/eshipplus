﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class ChargeCodeSearch : EntityBase
	{
		public List<ChargeCode> FetchChargeCodes(string criteria, long tenantId)
		{
			var chargeCodes = new List<ChargeCode>();
			const string query = "Select * from ChargeCode where (Code like @Criteria or Description like @Criteria) and TenantId = @TenantId";
			var parameter = new Dictionary<string, object> {{"Criteria", criteria},{"TenantId", tenantId}};
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					chargeCodes.Add(new ChargeCode(reader));
			Connection.Close();
			return chargeCodes;
		}

        public long FetchChargeCodeIdByCode(string code, long tenantId)
        {
            const string query = "Select [Id] from ChargeCode where Code = @Code and TenantId = @TenantId";
        	var parameters = new Dictionary<string, object> {{"Code", code}, {"TenantId", tenantId}};
            return ExecuteScalar(query, parameters).ToLong();
        }

	    public long FetchChargeCodeIdByProject44Code(int p44Code, long tenantId)
	    {
	        const string query = "Select [Id] from ChargeCode where Project44Code = @Code and TenantId = @TenantId";
	        var parameters = new Dictionary<string, object> { { "Code", p44Code }, { "TenantId", tenantId } };
	        return ExecuteScalar(query, parameters).ToLong();
	    }

        public List<ChargeCode>FetchActiveChargeCodesForAssets(long tenantId)
        {
            var chargeCodes = new List<ChargeCode>();

            const string query =
                @"SELECT * FROM ChargeCode WHERE TenantId = @TenantId AND Active = 'True'";
            var parameters = new Dictionary<string, object> {{"TenantId", tenantId}};
            using (var reader = GetReader(query,parameters))
                while (reader.Read())
                    chargeCodes.Add(new ChargeCode(reader));
            Connection.Close();
            return chargeCodes;
        }

		public List<ChargeCode> FetchChargeCodes(long tenantId)
		{
			var chargeCodes = new List<ChargeCode>();
			const string query = "Select * from ChargeCode where TenantId = @TenantId";
			var parameter = new Dictionary<string, object> {{ "TenantId", tenantId } };
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					chargeCodes.Add(new ChargeCode(reader));
			Connection.Close();
			return chargeCodes;
		}
	}
}
