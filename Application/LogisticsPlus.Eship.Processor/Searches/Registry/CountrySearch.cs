﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Registry
{
	public class CountrySearch : EntityBase
	{
		public List<Country> FetchCountries(string criteria)
		{
			var countries = new List<Country>();
            var query = string.Format("Select * from Country where Code like @Criteria or [Name] like @Criteria order by [Name] {0}", SearchUtilities.Ascending);
			var parameter = new Dictionary<string, object> {{"Criteria", criteria}};
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					countries.Add(new Country(reader));
			Connection.Close();
			return countries;
		}

		public List<Country> FetchCountriesThatRequirePostalCode(string criteria)
		{
			var countries = new List<Country>();
			const string query = @"Select * from Country where (Code like @Criteria or Name like @Criteria) and EmploysPostalCodes = @EmploysPostalCodes";
			var parameter = new Dictionary<string, object> {{"Criteria", criteria}, {"EmploysPostalCodes", true}};
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					countries.Add(new Country(reader));
			Connection.Close();
			return countries;
		}

		public long FetchCountryIdByCode(string code)
		{
			const string query = "Select [Id] from Country where Code = @Code";
			var parameters = new Dictionary<string, object> { { "Code", code } };
			return ExecuteScalar(query, parameters).ToLong();
		}
	}
}
