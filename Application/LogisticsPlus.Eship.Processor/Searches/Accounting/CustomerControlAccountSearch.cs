﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
	public class CustomerControlAccountSearch : EntityBase
	{
	    public List<CustomerControlAccount> FetchCustomerControlAccounts(CustomerControlAccountSearchCriteria criteria, long tenantId)
        {
            var accounts = new List<CustomerControlAccount>();

            var query = @"SELECT * From CustomerControlAccount WHERE TenantId = @TenantId AND CustomerId = @CustomerId AND ";

            
            var parameters = new Dictionary<string, object>
			                	{
			                		{"TenantId", tenantId},
			                		{"CustomerId", criteria.CustomerId},
			                	};

            var baseCountryCols = new List<SearchField>
                                   {
                                       AccountingSearchFields.NonAliasedCountryCode,
                                       AccountingSearchFields.NonAliasedCountryName,
                                   }.ToParameterColumn()
               .Select(c => c.ReportColumnName)
               .ToList();

            var countryCols = criteria.Parameters.Where(c => baseCountryCols.Contains(c.ReportColumnName)).ToList();
            var rcCols = criteria.Parameters.Where(c => !baseCountryCols.Contains(c.ReportColumnName)).ToList();

            query += rcCols.BuildWhereClause(parameters, AccountingSearchFields.CustomerControlAccounts);

            if (countryCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var cQuery = "SELECT Id FROM Country WHERE ";

                cQuery += countryCols.BuildWhereClause(uParams, AccountingSearchFields.CustomerControlAccounts);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    cQuery = cQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND (CountryId IN ({0}))", cQuery);
            }

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    accounts.Add(new CustomerControlAccount(reader));
            Connection.Close();
            return accounts;
        }
	}
}
