﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
	public class SalesRepresentativeSearch : EntityBase
	{
        public List<SalesRepresentative> FetchSalesRepresentatives(List<ParameterColumn> columns, long tenantId)
		{
			var salesRepresentatives = new List<SalesRepresentative>();
            var query = @"SELECT * FROM SalesRepresentative WHERE TenantId = @TenantId AND ";
            var parameters = new Dictionary<string, object> {{ "TenantId", tenantId }};

            var baseCountryCols = new List<SearchField>
                                   {
                                       AccountingSearchFields.NonAliasedCountryCode,
                                       AccountingSearchFields.NonAliasedCountryName,
                                   }.ToParameterColumn()
              .Select(c => c.ReportColumnName)
              .ToList();

            var countryCols = columns.Where(c => baseCountryCols.Contains(c.ReportColumnName)).ToList();
            var srCols = columns.Where(c => !baseCountryCols.Contains(c.ReportColumnName)).ToList();

            query += srCols.BuildWhereClause(parameters, AccountingSearchFields.SalesRepresentatives);

            if (countryCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var cQuery = "SELECT Id FROM Country WHERE ";

                cQuery += countryCols.BuildWhereClause(uParams, AccountingSearchFields.SalesRepresentatives);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    cQuery = cQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND (CountryId IN ({0}))", cQuery);
            }
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    salesRepresentatives.Add(new SalesRepresentative(reader));
            Connection.Close();
			return salesRepresentatives;
		}

		public SalesRepresentative FetchSalesRepresentativesByNumber(string salesRepNumber, long tenantId)
		{
			const string query ="SELECT * FROM SalesRepresentative WHERE SalesRepresentativeNumber = @Criteria AND TenantId = @TenantId";

			var parameter = new Dictionary<string, object> { { "Criteria", salesRepNumber }, { "TenantId", tenantId } };

			SalesRepresentative salesRepresentative = null;
			using (var reader = GetReader(query, parameter))
				if (reader.Read())
					salesRepresentative = new SalesRepresentative(reader);
			Connection.Close();

			return salesRepresentative;
		}

		public List<SalesRepresentative> FetchSaleRepresentativeByCriteriaForNameOrCompanyName(long tenantId, string criteria, Operator op)
		{
			var reps = new List<SalesRepresentative>();

			var query = @"SELECT * FROM SalesRepresentative WHERE TenantId = @TenantId AND ({0})";

			var parameters = new Dictionary<string, object>
        	                 	{
        	                 		{"Criteria", criteria},
        	                 		{"TenantId", tenantId},
        	                 	};
			var defaultParams = new List<ParameterColumn>
				{
					AccountingSearchFields.SaleRepresentativeCompanyName.ToParameterColumn(),
					AccountingSearchFields.SaleRepresentativeName.ToParameterColumn()
				};
			foreach (var p in defaultParams)
			{
				p.DefaultValue = criteria;
				p.DefaultValue2 = string.Empty;
				p.Operator = op;
			}
			query = string.Format(query, defaultParams.BuildWhereClause(parameters, AccountingSearchFields.SalesRepresentatives).Replace(" AND ", " OR "));
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					reps.Add(new SalesRepresentative(reader));
			Connection.Close();

			return reps;
		}

	}
}
