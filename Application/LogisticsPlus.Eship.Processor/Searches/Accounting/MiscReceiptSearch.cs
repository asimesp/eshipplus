﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
    public class MiscReceiptSearch : EntityBase
    {
        public List<MiscReceiptViewSearchDto> FetchMiscReceipts(MiscReciptViewSearchCriteria criteria, long tenantId)
        {
            var miscReceipts = new List<MiscReceiptViewSearchDto>();
            var query = @"SELECT * FROM MiscReceiptViewSearch WHERE TenantId = @TenantId AND ";
            var parameter = new Dictionary<string, object> { { "TenantId", tenantId } };
            query += criteria.Parameters.BuildWhereClause(parameter, AccountingSearchFields.MiscReceipts);

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    miscReceipts.Add(new MiscReceiptViewSearchDto(reader));
            Connection.Close();

            return miscReceipts;
        }

        public List<MiscReceiptViewSearchDto> FetchUnappliedMiscReceipts(MiscReciptViewSearchCriteria criteria, long tenantId)
        {
            var miscReceipts = new List<MiscReceiptViewSearchDto>();
            var query =
                @"SELECT DISTINCT 
                    mr.* 
                  FROM MiscReceiptViewSearch mr 
                    INNER JOIN InvoiceDetail id ON mr.ShipmentNumber = id.ReferenceNumber AND id.ReferenceType = 0 
                    INNER JOIN Invoice i ON i.Id = id.InvoiceId
				  WHERE i.Posted = 1 AND 
 
				    (SELECT SUM((d.UnitSell-d.UnitDiscount)* d.Quantity)
				    FROM InvoiceDetail d
				    WHERE d.InvoiceId = i.Id) > i.PaidAmount AND 

				    mr.TenantId = @TenantId AND
                    mr.Reversal = 0 AND
				    mr.AppliableAmount > 0 AND ";

            var parameter = new Dictionary<string, object> { { "TenantId", tenantId } };
            query += criteria.Parameters.BuildWhereClause(parameter, AccountingSearchFields.MiscReceipts);

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    miscReceipts.Add(new MiscReceiptViewSearchDto(reader));
            Connection.Close();

            return miscReceipts;
        }

        public decimal GetMiscReceiptAmountLeftToBeAppliedOrRefunded(long miscReceiptId, long tenantId, long excludedMiscReceiptApplicationId = 0)
        {
            const string query = "select dbo.MiscReceiptAmountLeftToBeAppliedOrRefunded(@OriginalMiscReceiptId, @TenantId, @ExcludedMiscReceiptApplicationId)";
            var parameters = new Dictionary<string, object>
                {
                    {"OriginalMiscReceiptId", miscReceiptId},
                    {"TenantId", tenantId},
                    {"ExcludedMiscReceiptApplicationId", excludedMiscReceiptApplicationId}
                };
            return ExecuteScalar(query, parameters).ToDecimal();
        }

        public List<MiscReceiptViewSearchDto> FetchMiscReceiptsApplicableToPostedInvoice(long invoiceId, long tenantId)
        {
            var miscReceipts = new List<MiscReceiptViewSearchDto>();
            const string query = @"Select
	                                    Distinct mr.*
                                    From MiscReceiptViewSearch mr
	                                    Left Join InvoiceDetail id on mr.ShipmentNumber = id.ReferenceNumber And id.ReferenceType = 0  And mr.ShipmentId <> 0
	                                    Left Join MiscReceiptApplication mra on mr.Id = mra.MiscReceiptId
	                                    Left Join Invoice i on i.Id = id.InvoiceId Or i.Id = mra.InvoiceId 	
                                    Where i.Id = @InvoiceId And mr.TenantId = @TenantId And i.Posted = 1 And mr.Reversal = 0";
            var parameter = new Dictionary<string, object> { { "TenantId", tenantId }, { "InvoiceId", invoiceId } };

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    miscReceipts.Add(new MiscReceiptViewSearchDto(reader));
            Connection.Close();

            return miscReceipts;
        }

        public List<MiscReceipt> FetchMiscReceiptsRelatedToLoadOrder(long loadOrderId, long tenantId)
        {
            var miscReceipts = new List<MiscReceipt>();
            const string query = @"SELECT * FROM MiscReceipt WHERE TenantId = @TenantId AND LoadOrderId = @LoadOrderId";
            var parameter = new Dictionary<string, object> { { "TenantId", tenantId }, { "LoadOrderId", loadOrderId } };
            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    miscReceipts.Add(new MiscReceipt(reader));
            Connection.Close();
            return miscReceipts;
        }

        public List<MiscReceipt> FetchMiscReceiptsRelatedToShipment(long shipmentId, long tenantId)
        {
            var miscReceipts = new List<MiscReceipt>();
            const string query = @"SELECT * FROM MiscReceipt WHERE TenantId = @TenantId AND ShipmentId = @ShipmentId";
            var parameter = new Dictionary<string, object> { { "TenantId", tenantId }, { "ShipmentId", shipmentId } };
            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    miscReceipts.Add(new MiscReceipt(reader));
            Connection.Close();
            return miscReceipts;
        }

        public List<MiscReceipt> FetchMiscReceiptsUsingPaymentProfile(string paymentProfileId, long tenantId)
        {
            var miscReceipts = new List<MiscReceipt>();
            const string query = @"SELECT * FROM MiscReceipt WHERE TenantId = @TenantId AND PaymentProfileId = @PaymentProfileId";
            var parameter = new Dictionary<string, object> { { "TenantId", tenantId }, { "PaymentProfileId", paymentProfileId } };
            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    miscReceipts.Add(new MiscReceipt(reader));
            Connection.Close();
            return miscReceipts;
        }


        public decimal GetMiscReceiptsAmountPaidToLoadOrder(long loadOrderId, long tenantId)
        {
            const string query = "Select Sum(Case When mr.Reversal = 0 then mr.AmountPaid Else -1 * mr.AmountPaid End) From MiscReceipt mr Where mr.LoadOrderId = @LoadOrderId And mr.TenantId = @TenantId";
            var parameters = new Dictionary<string, object>
                {
                    {"LoadOrderId", loadOrderId},
                    {"TenantId", tenantId},
                };
            return ExecuteScalar(query, parameters).ToDecimal();
        }

        public decimal GetMiscReceiptsAmountPaidToShipment(long shipmentId, long tenantId)
        {
            const string query = "Select Sum(Case When mr.Reversal = 0 then mr.AmountPaid Else -1 * mr.AmountPaid End) From MiscReceipt mr Where mr.ShipmentId = @ShipmentId And mr.TenantId = @TenantId";
            var parameters = new Dictionary<string, object>
                {
                    {"ShipmentId", shipmentId},
                    {"TenantId", tenantId},
                };
            return ExecuteScalar(query, parameters).ToDecimal();
        }
    }
}
