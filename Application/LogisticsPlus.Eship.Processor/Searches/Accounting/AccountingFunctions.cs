﻿using System.Collections.Generic;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
	public class AccountingFunctions : EntityBase
	{
		public decimal OutstandingCustomerBalance(long customerId)
		{
			const string query = "select top 1 * from dbo.CustomerOutstandingBalance(@Id)";
			var parameters = new Dictionary<string, object> {{"Id", customerId}};
			var balance = ExecuteScalar(query, parameters).ToDecimal();
			return balance;
		}
	}
}
