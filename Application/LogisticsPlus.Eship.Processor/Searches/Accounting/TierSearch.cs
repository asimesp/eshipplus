﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
	public class TierSearch : EntityBase
	{
		public Tier FetchTierByTierNumber(string tierNumber, long tenantId)
		{
			Tier tier = null;

			const string query = @"Select * from Tier where TierNumber = @TierNumber and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TierNumber", tierNumber }, { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					tier = new Tier(reader);
			Connection.Close();

			return tier;
		}

        public List<Tier> FetchTiers(List<ParameterColumn> columns, long tenantId)
        {
            var tiers = new List<Tier>();

            var query = @"SELECT * FROM Tier WHERE TenantId = @TenantId AND ";
            var parameters = new Dictionary<string, object> {  { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameters, AccountingSearchFields.Tiers);
            query += string.Format(" ORDER BY [Name] {0} ", SearchUtilities.Ascending);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    tiers.Add(new Tier(reader));
            Connection.Close();

            return tiers;
        }
	}
}
