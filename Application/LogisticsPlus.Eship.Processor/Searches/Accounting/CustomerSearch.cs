﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
    public class CustomerSearch : EntityBase
    {
        public List<Customer> FetchCustomers(CustomerViewSearchCriteria criteria, long tenantId)
        {
            var customers = new List<Customer>();

            var query = string.Format(
				@"SELECT * FROM CustomerViewSearch WHERE TenantId = @TenantId {0} AND ",
                criteria.OnlyActiveCustomers ? "AND Active = @Active " : string.Empty);

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };
            if (criteria.OnlyActiveCustomers) parameters.Add("Active", true);

            query += criteria.Parameters.BuildWhereClause(parameters, AccountingSearchFields.Customers);
            query += string.Format(" ORDER BY [Name] {0}, CustomerNumber {0}", SearchUtilities.Ascending);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    customers.Add(new Customer(reader));
            Connection.Close();

            return customers;
        }

        public Customer FetchCustomerByNumber(string customerNumber, long tenantId)
        {
            var query = string.Format(
                @"SELECT * FROM Customer WHERE TenantId = @TenantId
                                         AND CustomerNumber = @CustomerNumber
                                         ORDER BY[Name] {0}, CustomerNumber {0}", SearchUtilities.Ascending);

            var parameters = new Dictionary<string, object> { { "CustomerNumber", customerNumber }, { "TenantId", tenantId } };
            Customer customer = null;
            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    customer = new Customer(reader);
            Connection.Close();

            return customer;
        }

		public List<Customer> FetchCustomersWithRating(List<ParameterColumn> columns , long tenantId)
		{
			var customers = new List<Customer>();

			var query = string.Format(@"SELECT 
                                            CustomerViewSearch.* 
                                        FROM CustomerViewSearch 
                                            INNER JOIN CustomerRating ON CustomerViewSearch.[Id] = CustomerRating.CustomerId 
								        WHERE 
                                            CustomerViewSearch.TenantId = @TenantId 
                                            AND CustomerViewSearch.RatingId > 0 
                                            AND ") ;

			var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameters, AccountingSearchFields.Customers);
            query += string.Format(" ORDER BY CustomerViewSearch  .[Name] {0}, CustomerViewSearch.CustomerNumber {0}", SearchUtilities.Ascending);

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					customers.Add(new Customer(reader));
			Connection.Close();

			return customers;
		}

        public List<CustomerViewSearchDto> FetchCustomersWithCommunication(List<ParameterColumn> columns, long tenantId)
		{
            var customers = new List<CustomerViewSearchDto>();

		    var query =
		        string.Format(
                    @"SELECT
                        CustomerViewSearch.* 
	                FROM CustomerViewSearch 
	                    INNER JOIN CustomerCommunication ON CustomerViewSearch.[Id] = CustomerCommunication.CustomerId 
                    WHERE
                        CustomerViewSearch.TenantId = @TenantId 
                        AND CustomerViewSearch.CommunicationId > 0  
                        AND ");

			var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameters, AccountingSearchFields.Customers);
            query += string.Format(" ORDER BY CustomerViewSearch.[Name] {0}, CustomerViewSearch.CustomerNumber {0}", SearchUtilities.Ascending);

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					customers.Add(new CustomerViewSearchDto(reader));
			Connection.Close();

			return customers;
		}

		public Customer FetchCustomerByCommunicationConnectGuid(Guid guid, long tenantId)
		{
			const string query = @"Select c.* from Customer as c, CustomerCommunication as cc 
				where c.CommunicationId = cc.Id and cc.ConnectGuid = @guid and c.TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"guid", guid}, {"TenantId", tenantId}};
			Customer customer = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					customer = new Customer(reader);
			Connection.Close();
			return customer;
		}

        public List<CustomerViewSearchDto> FetchCustomerViewSearchDtos(CustomerViewSearchCriteria criteria, long tenantId)
        {
            var customers = new List<CustomerViewSearchDto>();

            var query = string.Format(
                @"SELECT * FROM CustomerViewSearch WHERE TenantId = @TenantId {0} AND ",
                criteria.OnlyActiveCustomers ? "AND Active = @Active " : string.Empty);

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };
            if (criteria.OnlyActiveCustomers) parameters.Add("Active", true);

            query += criteria.Parameters.BuildWhereClause(parameters, AccountingSearchFields.Customers);
            query += string.Format(" ORDER BY [Name] {0}, CustomerNumber {0}", SearchUtilities.Ascending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    customers.Add(new CustomerViewSearchDto(reader));
            Connection.Close();

            return customers;
        }


		public List<CustomerCommunication> FetchCustomerCommunicationsWithEdiOrFtp()
		{
			var communications = new List<CustomerCommunication>();

			var query =
				string.Format(@"SELECT * FROM CustomerCommunication WHERE EdiEnabled = @Enabled OR FtpEnabled = @Enabled");

			var parameters = new Dictionary<string, object> { { "Enabled", true } };

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					communications.Add(new CustomerCommunication(reader));
			Connection.Close();

			return communications;
		}

		public List<CustomerCommunication> FetchCustomerCommunicationsWithDocDelivery()
		{
			var communications = new List<CustomerCommunication>();

			var query =
				string.Format(@"SELECT * FROM CustomerCommunication WHERE FtpDocDeliveryEnabled = @Enabled");

			var parameters = new Dictionary<string, object> { { "Enabled", true } };

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					communications.Add(new CustomerCommunication(reader));
			Connection.Close();

			return communications;
		}
    }
}
