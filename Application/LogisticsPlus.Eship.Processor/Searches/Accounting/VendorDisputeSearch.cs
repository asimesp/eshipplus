﻿using System.Collections.Generic;
using ObjToSql.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
    public class VendorDisputeSearch : EntityBase
    {
        public VendorDispute FetchVendorDisputeByDisputerReferenceNumber(string disputerReferenceNumber, long tenantId)
        {
            VendorDispute verdorDispute = null;

            const string query = @"SELECT * FROM VendorDispute WHERE DisputerReferenceNumber = @DisputerReferenceNumber AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "DisputerReferenceNumber", disputerReferenceNumber }, { "TenantId", tenantId }, };

            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    verdorDispute = new VendorDispute(reader);
            Connection.Close();

            return verdorDispute;
        }

        public List<VendorDispute> FetchVendorDisputes(VendorDisputeViewSearchCriteria criteria, long tenantId)
        {
            var vendorDisputes = new List<VendorDispute>();

            var query = @"SELECT * FROM (SELECT Top 1000 vd.* FROM VendorDispute vd INNER JOIN Vendor v ON vd.VendorId = v.Id) TEMP WHERE ";

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };
            query += criteria.Parameters.BuildWhereClause(parameters, AccountingSearchFields.VendorDisputes);

            SearchUtilities.AddQuerySorting(criteria.SortBy, criteria.SortAscending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    vendorDisputes.Add(new VendorDispute(reader));
            Connection.Close();

            return vendorDisputes;
        }
    }
}