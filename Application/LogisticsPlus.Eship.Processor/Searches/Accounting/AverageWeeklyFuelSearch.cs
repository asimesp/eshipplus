﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
	public class AverageWeeklyFuelSearch : EntityBase
	{
		public List<AverageWeeklyFuelViewSearchDto> FetchAverageWeeklyFuels(string criteria, DateTime startDate, DateTime endDate, long tenantId)
		{
			var averageWeeklyFuels = new List<AverageWeeklyFuelViewSearchDto>();
		    var query = string.Format(@"SELECT * FROM AverageWeeklyFuelViewSearch 
									WHERE EffectiveDate >= @StartDate
                                    AND EffectiveDate < @EndDate
                                    AND TenantId = @TenantId ORDER BY EffectiveDate {0}",SearchUtilities.Descending);

			var parameter = new Dictionary<string, object> 
												{ 
													{ "Criteria", criteria },
													{ "StartDate",startDate.TimeToMinimum() },
													{ "EndDate",endDate.TimeToMaximum() }, 
													{ "TenantId", tenantId } 
												};
			using (var reader = GetReader(query, parameter))
				while (reader.Read())
					averageWeeklyFuels.Add(new AverageWeeklyFuelViewSearchDto(reader));
			Connection.Close();

			return averageWeeklyFuels;
		}
	}
}
