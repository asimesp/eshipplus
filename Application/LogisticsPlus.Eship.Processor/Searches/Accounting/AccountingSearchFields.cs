﻿using System.Collections.Generic;
using System.Data;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
	public static class AccountingSearchFields
	{
		private static readonly object LockObj = new object();

		private static bool _isInitialized;

		public static SearchField Active;
		public static SearchField AmountDue;
		public static SearchField AppliableAmount;
		public static SearchField BillTypeText;
        public static SearchField NonAliasedCountryCode;
		public static SearchField NonAliasedCountryName;
        public static SearchField ChargeType;
        public static SearchField ChargeNumber;
        public static SearchField ChargeUnitBuy;
        public static SearchField CustomField1;
		public static SearchField CustomField2;
		public static SearchField CustomField3;
		public static SearchField CustomField4;
		public static SearchField CustomerName;
		public static SearchField CustomerNumber;
		public static SearchField DateCreated;
		public static SearchField DocumentNumber;
		public static SearchField DocumentDate;
		public static SearchField DueDate;
		public static SearchField NonAliasEquipmentTypeName;
		public static SearchField ExpirationDate;
		public static SearchField InvoiceDate;
		public static SearchField InvoiceNumber;
		public static SearchField InvoiceTypeText;
		public static SearchField BillingShipmentNumber;
		public static SearchField BillingServiceTicketNumber;
		public static SearchField OriginalInvoiceNumber;
		public static SearchField PaidAmount;
		public static SearchField PostDate;
		public static SearchField Posted;
		public static SearchField PrefixCode;
		public static SearchField SaleRepresentativeName;
		public static SearchField SaleRepresentativeCompanyName;
		public static SearchField TierName;
		public static SearchField Username;
		public static SearchField VendorName;
		public static SearchField VendorNumber;
		public static SearchField NonAliasedVendorTerminalCity;
		public static SearchField NonAliasedVendorTerminalState;
		public static SearchField NonAliasedVendorTerminalPostalCode;
		public static SearchField NonAliasedVendorTerminalCountryName;
		public static SearchField NonAliasedVendorTerminalCountryCode;
		public static SearchField ReferenceNumber;

		public static SearchField ShipmentNumber;
		public static SearchField LoadOrderNumber;
		public static SearchField UserUsername;
		public static SearchField AmountPaid;
		public static SearchField PaymentDate;
		public static SearchField GatewayTransactionId;
		public static SearchField PaymentGatewayType;
		public static SearchField Reversal;

        // ------------------------------------------------

        public static List<SearchField> DefaultVendorChargeConfirmation { get; set; }
        public static List<SearchField> DefaultCommissionsToBePaid { get; set; }
		public static List<SearchField> DefaultCustomerControlAccounts { get; set; }
		public static List<SearchField> DefaultCustomers { get; set; }
		public static List<SearchField> DefaultCustomerPurchaseOrders { get; set; }
		public static List<SearchField> DefaultInvoices { get; set; }
		public static List<SearchField> DefaultMiscReceipts { get; set; }
		public static List<SearchField> DefaultPendingVendors { get; set; }
		public static List<SearchField> DefaultSalesRepresentatives { get; set; }
		public static List<SearchField> DefaultVendors { get; set; }
        public static List<SearchField> DefaultVendorDisputes { get; set; }
        public static List<SearchField> DefaultVendorBills { get; set; }
		public static List<SearchField> DefaultVendorCommunications { get; set; }
		public static List<SearchField> DefaultTiers { get; set; }

		// ------------------------------------------------

		public static List<SearchField> CommissionsToBePaid { get; set; }
        public static List<SearchField> Charges { get; set; }
		public static List<SearchField> CustomerControlAccounts { get; set; }
		public static List<SearchField> Customers { get; set; }
		public static List<SearchField> CustomerInvoices { get; set; }
		public static List<SearchField> CustomerPurchaseOrders { get; set; }
		public static List<SearchField> Invoices { get; set; }
		public static List<SearchField> MiscReceipts { get; set; }
		public static List<SearchField> PendingVendors { get; set; }
		public static List<SearchField> SalesRepresentatives { get; set; }
		public static List<SearchField> Vendors { get; set; }
        public static List<SearchField> VendorDisputes { get; set; }
        public static List<SearchField> VendorBills { get; set; }
		public static List<SearchField> VendorCommunications { get; set; }
		public static List<SearchField> Tiers { get; set; }

		// ------------------------------------------------

		public static List<SearchField> BatchPaymentApplicationSortFields { get; set; }
        public static List<SearchField> VendorChargeConfirmationSortFields { get; set; }
        public static List<SearchField> CustomerInvoiceSortFields { get; set; }
		public static List<SearchField> InvoiceSortFields { get; set; }
		public static List<SearchField> InvoicePostingSortFields { get; set; }
		public static List<SearchField> MiscReceiptSortFields { get; set; }
		public static List<SearchField> VendorBillsSortFields { get; set; }
		public static List<SearchField> VendorBillsPostingSortFields { get; set; }

		// ------------------------------------------------

		public static void Initialize()
		{
			if (_isInitialized) return;

			var anotherLockInitialized = false;
			lock (LockObj)
			{
				if (!_isInitialized) _isInitialized = true;
				else anotherLockInitialized = true;
			}

			if (anotherLockInitialized) return;

			Active = new SearchField { Name = "Active", DataType = SqlDbType.Bit };
			AmountDue = new SearchField { Name = "AmountDue", DataType = SqlDbType.Decimal };
			AppliableAmount = new SearchField { Name = "AppliableAmount", DisplayName = "Appliable Amount", DataType = SqlDbType.Decimal };
			BillTypeText = new SearchField { Name = "BillTypeText", DataType = SqlDbType.NVarChar };
			NonAliasedCountryCode = new SearchField { Name = "Code", DisplayName = "Country Code", DataType = SqlDbType.NVarChar };
			NonAliasedCountryName = new SearchField { Name = "Name", DisplayName = "Country Name", DataType = SqlDbType.NVarChar };
            ChargeType = new SearchField { Name = "ChargeType", DisplayName = "Charge Type", DataType = SqlDbType.NVarChar };
            ChargeNumber = new SearchField { Name = "Number", DisplayName = "Number", DataType = SqlDbType.NVarChar };
            ChargeUnitBuy = new SearchField { Name = "UnitBuy", DisplayName = "UnitBuy", DataType = SqlDbType.NVarChar };
            CustomField1 = new SearchField { Name = "CustomField1", DataType = SqlDbType.NVarChar };
			CustomField2 = new SearchField { Name = "CustomField2", DataType = SqlDbType.NVarChar };
			CustomField3 = new SearchField { Name = "CustomField3", DataType = SqlDbType.NVarChar };
			CustomField4 = new SearchField { Name = "CustomField4", DataType = SqlDbType.NVarChar };
			CustomerName = new SearchField { Name = "CustomerName", DataType = SqlDbType.NVarChar };
			CustomerNumber = new SearchField { Name = "CustomerNumber", DataType = SqlDbType.NVarChar };
			DateCreated = new SearchField { Name = "DateCreated", DataType = SqlDbType.DateTime };
			DocumentNumber = new SearchField { Name = "DocumentNumber", DataType = SqlDbType.NVarChar };
			DocumentDate = new SearchField { Name = "DocumentDate", DataType = SqlDbType.DateTime };
			DueDate = new SearchField { Name = "DueDate", DataType = SqlDbType.DateTime };
			NonAliasEquipmentTypeName = new SearchField { Name = "TypeName", DisplayName = "Equipment Type Name", DataType = SqlDbType.NVarChar };
			ExpirationDate = new SearchField { Name = "ExpirationDate", DataType = SqlDbType.DateTime };
			InvoiceNumber = new SearchField { Name = "InvoiceNumber", DataType = SqlDbType.NVarChar };
			InvoiceDate = new SearchField { Name = "InvoiceDate", DataType = SqlDbType.DateTime };
			InvoiceTypeText = new SearchField { Name = "InvoiceTypeText", DataType = SqlDbType.NVarChar };
			BillingShipmentNumber = new SearchField { Name = "ReferenceNumber", DisplayName = "Shipment Number", DataType = SqlDbType.NVarChar };
            BillingServiceTicketNumber = new SearchField { Name = "ReferenceNumber", DisplayName = "Service Ticket Number", DataType = SqlDbType.NVarChar };
			OriginalInvoiceNumber = new SearchField { Name = "OriginalInvoiceNumber", DataType = SqlDbType.NVarChar };
			PaidAmount = new SearchField { Name = "PaidAmount", DataType = SqlDbType.Decimal };
			PostDate = new SearchField { Name = "PostDate", DataType = SqlDbType.DateTime };
			Posted = new SearchField { Name = "Posted", DataType = SqlDbType.Bit };
			PrefixCode = new SearchField { Name = "PrefixCode", DisplayName = "Prefix", DataType = SqlDbType.NVarChar };
			SaleRepresentativeName = new SearchField { Name = "Name", DataType = SqlDbType.NVarChar };
			SaleRepresentativeCompanyName = new SearchField { Name = "CompanyName", DataType = SqlDbType.NVarChar };
			TierName = new SearchField { Name = "Name", DataType = SqlDbType.NVarChar };
			Username = new SearchField { Name = "Username", DataType = SqlDbType.NVarChar };
			VendorName = new SearchField { Name = "VendorName", DataType = SqlDbType.NVarChar };
			VendorNumber = new SearchField { Name = "VendorNumber", DataType = SqlDbType.NVarChar };
			NonAliasedVendorTerminalCity = new SearchField { Name = "City", DisplayName = "Vendor Terminal City", DataType = SqlDbType.NVarChar };
			NonAliasedVendorTerminalState = new SearchField { Name = "State", DisplayName = "Vendor Terminal State", DataType = SqlDbType.NVarChar };
			NonAliasedVendorTerminalPostalCode = new SearchField { Name = "PostalCode", DisplayName = "Vendor Terminal Postal Code", DataType = SqlDbType.NVarChar };
			NonAliasedVendorTerminalCountryName = new SearchField { Name = "Name", DisplayName = "Vendor Terminal Country Name", DataType = SqlDbType.NVarChar };
			NonAliasedVendorTerminalCountryCode = new SearchField { Name = "Code", DisplayName = "Vendor Terminal Country Code", DataType = SqlDbType.NVarChar };
			ReferenceNumber = new SearchField { Name = "ReferenceNumber", DisplayName = "Shipment Number", DataType = SqlDbType.NVarChar };

			ShipmentNumber = new SearchField { Name = "ShipmentNumber", DataType = SqlDbType.NVarChar };
			LoadOrderNumber = new SearchField { Name = "LoadOrderNumber", DataType = SqlDbType.NVarChar };
			AmountPaid = new SearchField { Name = "AmountPaid", DataType = SqlDbType.Decimal };
			PaymentDate = new SearchField { Name = "PaymentDate", DataType = SqlDbType.DateTime };
			GatewayTransactionId = new SearchField { Name = "GatewayTransactionId", DataType = SqlDbType.NVarChar };
			PaymentGatewayType = new SearchField { Name = "PaymentGatewayType", DataType = SqlDbType.NVarChar };
			Reversal = new SearchField { Name = "Reversal", DataType = SqlDbType.Bit };
			UserUsername = new SearchField { Name = "UserUsername", DataType = SqlDbType.NVarChar };

            // ------------------------------------------------

            DefaultVendorChargeConfirmation = new List<SearchField>
                {
                    ChargeType,
                    ChargeNumber
                };
            DefaultCommissionsToBePaid = new List<SearchField>
				{
					new SearchField {Name = "ReferenceNumber", DisplayName = "Shipment Number", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "SalesRepresentativeName", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "ShipmentDateCreated", DataType = SqlDbType.DateTime},
				};
			DefaultCustomerControlAccounts = new List<SearchField>
		        {
		            new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},
		            new SearchField {Name = "AccountNumber", DataType = SqlDbType.NVarChar}
		        };
			DefaultCustomers = new List<SearchField>
		        {
		            new SearchField {Name = "Name", DisplayName = "Customer Name", DataType = SqlDbType.NVarChar},
		            CustomerNumber
		        };
			DefaultCustomerPurchaseOrders = new List<SearchField>
                                   {
                                       new SearchField {Name = "PurchaseOrderNumber", DataType = SqlDbType.NVarChar},
                                       ExpirationDate,
                                   };
			DefaultInvoices = new List<SearchField> { InvoiceNumber, DueDate };

			DefaultMiscReceipts = new List<SearchField>
		        {
                    ShipmentNumber,
                    PaymentDate
		        };

			DefaultPendingVendors = new List<SearchField>
			                 	{
			                 		new SearchField {Name = "Name", DisplayName = "Vendor Name", DataType = SqlDbType.NVarChar},
			                 		VendorNumber,
			                 	};
			DefaultSalesRepresentatives = new List<SearchField>
				{
					SaleRepresentativeName,
					new SearchField {Name = "SalesRepresentativeNumber", DataType = SqlDbType.NVarChar}
				};
			DefaultVendors = new List<SearchField>
			                 	{
			                 		new SearchField {Name = "Name", DisplayName = "Vendor Name", DataType = SqlDbType.NVarChar},
			                 		VendorNumber,
			                 	};
            DefaultVendorDisputes = new List<SearchField>
                                 {
                                     new SearchField {Name = "DisputerReferenceNumber", DisplayName = "Disputer Reference Number", DataType = SqlDbType.NVarChar}
                                 };
            DefaultVendorCommunications = new List<SearchField>
			                              	{
			                              		new SearchField {Name = "Name", DisplayName = "Vendor Name", DataType = SqlDbType.NVarChar},
			                              		VendorNumber,
			                              	};
			DefaultVendorBills = new List<SearchField> { DocumentNumber, DateCreated };
			DefaultTiers = new List<SearchField> { TierName, new SearchField { Name = "TierNumber", DataType = SqlDbType.NVarChar }, };

			// ------------------------------------------------

			BatchPaymentApplicationSortFields = new List<SearchField>
                {
                    ShipmentNumber,
                    LoadOrderNumber,
                    GatewayTransactionId,
                    AmountPaid,
                    AppliableAmount
                };
			CommissionsToBePaid = new List<SearchField>
				{
					ReferenceNumber,
					new SearchField {Name = "ServiceModeText", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "ShipmentDateCreated", DataType = SqlDbType.DateTime},
					new SearchField {Name = "CustomerName", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "CustomerNumber", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "SalesRepresentativeName", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "SalesRepresentativeNumber", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "SalesRepresentativeCompanyName", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "SalesRepresentativeAddlEntityName", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "InvoicesPaid", DataType = SqlDbType.Bit},
				};

            VendorChargeConfirmationSortFields = new List<SearchField>
            {
                    ChargeUnitBuy
            };
            VendorChargeConfirmationSortFields.AddRange(DefaultVendorChargeConfirmation);

            Charges = new List<SearchField>
            {
                    ChargeUnitBuy
            };
            Charges.AddRange(DefaultVendorChargeConfirmation);

            CustomerInvoiceSortFields = new List<SearchField>
                                    {
                                        InvoiceNumber,
                                        PostDate,
                                        DueDate,
                                        AmountDue,
                                        PaidAmount,
                                        InvoiceTypeText,
                                        CustomerName,
                                        CustomerNumber,
                                        new SearchField {Name = "LocationStreet1", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationStreet2", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationCity", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationState", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationPostalCode", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationCountryName", DataType = SqlDbType.NVarChar},
                                        OriginalInvoiceNumber, 
                                    };


			InvoiceSortFields = new List<SearchField>
                                    {
                                        InvoiceNumber,
                                        InvoiceDate,
                                        PostDate,
                                        DueDate,
                                        AmountDue,
                                        PaidAmount,
                                        InvoiceTypeText,
                                        Username,
                                        new SearchField {Name = "ExportDate", DataType = SqlDbType.DateTime},
                                        CustomerName,
                                        CustomerNumber,
                                        new SearchField {Name = "LocationStreet1", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationStreet2", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationCity", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationState", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationPostalCode", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationCountryName", DataType = SqlDbType.NVarChar},
                                        OriginalInvoiceNumber, 
                                    };

			InvoicePostingSortFields = new List<SearchField>
                                    {
                                        InvoiceNumber,
                                        InvoiceDate,
                                        AmountDue,
                                        PaidAmount,
                                        InvoiceTypeText,
                                        Username,
                                        CustomerName,
                                        CustomerNumber,
                                        PrefixCode,
                                        new SearchField {Name = "LocationStreet1", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationStreet2", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationCity", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationState", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationPostalCode", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationCountryName", DataType = SqlDbType.NVarChar},
                                        OriginalInvoiceNumber
                                    };

			MiscReceiptSortFields = new List<SearchField>
		        {
		            UserUsername,
                    AmountPaid,
                    GatewayTransactionId,
                    CustomerNumber,
                    CustomerName,
                    PaymentGatewayType,
                    Reversal,
                    PaymentDate,
                    ShipmentNumber,
                    LoadOrderNumber
		        };

			VendorBillsSortFields = new List<SearchField>
                                    {
                                        DocumentNumber,
                                        DateCreated,
                                        DocumentDate,
                                        PostDate,
                                        AmountDue,
                                        VendorName,
                                        VendorNumber,
                                        Username,
                                        BillTypeText,
                                        new SearchField {Name = "ExportDate", DataType = SqlDbType.DateTime},
                                        new SearchField {Name = "LocationStreet1", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationStreet2", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationCity", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationState", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationPostalCode", DataType = SqlDbType.NVarChar},
			           		            new SearchField {Name = "LocationCountryName", DataType = SqlDbType.NVarChar},
                                         OriginalInvoiceNumber
                                    };

            CustomerControlAccounts = new List<SearchField>
                            {
                                new SearchField { Name = "Description", DataType = SqlDbType.NVarChar }, 
                                new SearchField { Name = "AccountNumber", DataType = SqlDbType.NVarChar },
                                new SearchField {Name = "Street1", DataType = SqlDbType.NVarChar},
	           		            new SearchField {Name = "Street2", DataType = SqlDbType.NVarChar},
	           		            new SearchField {Name = "City", DataType = SqlDbType.NVarChar},
	           		            new SearchField {Name = "State", DataType = SqlDbType.NVarChar},
	           		            new SearchField {Name = "PostalCode", DataType = SqlDbType.NVarChar},
	           		            new SearchField {Name = "GenericCategory", DataType = SqlDbType.NVarChar},
                                NonAliasedCountryCode,
                                NonAliasedCountryName,
                            };

			Customers = new List<SearchField>
                            {
								CustomField1,
								CustomField2,
								CustomField3,
								CustomField4,
                                CustomerNumber,
                                new SearchField{Name = "Name", DisplayName = "Customer Name", DataType = SqlDbType.NVarChar},
                                new SearchField{Name = "CustomerTypeText", DisplayName = "Customer Type", DataType = SqlDbType.NVarChar},
                                new SearchField{Name = "AdditionalBillOfLadingText", DataType = SqlDbType.NVarChar},
                                new SearchField{Name = "ShipperBillOfLadingPrefix", DataType = SqlDbType.NVarChar},
                                new SearchField{Name = "ShipperBillOfLadingSuffix", DataType = SqlDbType.NVarChar},
                                new SearchField{Name = "InvalidPurchaseOrderNumberMessage", DataType = SqlDbType.NVarChar},
                                new SearchField{Name = "CustomVendorSelectionMessage", DataType = SqlDbType.NVarChar},
                                new SearchField{ Name = "TierName", DataType = SqlDbType.NVarChar},
                                new SearchField{ Name = "TierNumber", DataType = SqlDbType.NVarChar}
                            };

			// ensure that all fields in customer invoices are present in invoices!!!!!
			CustomerInvoices = new List<SearchField>
			                   	{
			                   		InvoiceNumber,
			                   		InvoiceDate,
			                   		PostDate,
			                   		DueDate,
			                   		new SearchField {Name = "InvoiceTypeText", DataType = SqlDbType.NVarChar},
			                   		new SearchField {Name = "SpecialInstruction", DataType = SqlDbType.NVarChar},
			                   		new SearchField {Name = "PaidAmount", DataType = SqlDbType.Decimal},
			                   		new SearchField {Name = "LocationStreet1", DataType = SqlDbType.NVarChar},
			                   		new SearchField {Name = "LocationStreet2", DataType = SqlDbType.NVarChar},
			                   		new SearchField {Name = "LocationCity", DataType = SqlDbType.NVarChar},
			                   		new SearchField {Name = "LocationState", DataType = SqlDbType.NVarChar},
			                   		new SearchField {Name = "LocationPostalCode", DataType = SqlDbType.NVarChar},
			                   		new SearchField {Name = "LocationCountryName", DataType = SqlDbType.NVarChar},
			                   		CustomerName,
                                    CustomerNumber,
			                   		OriginalInvoiceNumber,
			                   		new SearchField {Name = "AmountDue", DataType = SqlDbType.Decimal},
                                    BillingServiceTicketNumber,
                                    BillingShipmentNumber
			                   	};

			CustomerPurchaseOrders = new List<SearchField>
                            {
                                new SearchField{Name = "PurchaseOrderNumber", DataType = SqlDbType.NVarChar},
                                new SearchField{Name = "ValidPostalCode", DataType = SqlDbType.NVarChar},
                                new SearchField{Name = "ApplyOnOrigin", DataType = SqlDbType.Bit},
                                new SearchField{Name = "ApplyOnDestination", DataType = SqlDbType.Bit},
                                new SearchField{Name = "MaximumUses", DataType = SqlDbType.Int},
                                ExpirationDate,
                                new SearchField{Name = "AdditionalPurchaseOrderNotes", DataType = SqlDbType.NVarChar},
                                NonAliasedCountryCode,
                                NonAliasedCountryName,
                            };


			Invoices = new List<SearchField>
			           	{
			           		Posted,
			           		new SearchField {Name = "Exported", DataType = SqlDbType.Bit},
			           		new SearchField {Name = "SpecialInstruction", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "UserFirstName", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "UserLastName", DataType = SqlDbType.NVarChar},
			           		OriginalInvoiceNumber,
                            BillingShipmentNumber,
                            BillingServiceTicketNumber
			           	};
			Invoices.AddRange(InvoiceSortFields);

			MiscReceipts = new List<SearchField>
		        {
                    UserUsername,
                    AmountPaid,
                    GatewayTransactionId,
                    CustomerNumber,
                    CustomerName,
                    PaymentGatewayType,
                    Reversal,
		            new SearchField {Name = "UserFirstName", DataType = SqlDbType.NVarChar},
		            new SearchField {Name = "UserLastName", DataType = SqlDbType.NVarChar},
		        };
			MiscReceipts.AddRange(DefaultMiscReceipts);

			PendingVendors = new List<SearchField>
			           	{
			           		new SearchField {Name = "Name", DisplayName = "Vendor Name", DataType = SqlDbType.NVarChar},
			           		VendorNumber,
                            new SearchField {Name = "Approved", DataType = SqlDbType.Bit},
                            new SearchField {Name = "DateCreated", DataType = SqlDbType.DateTime},
                            new SearchField {Name = "Notation", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "TSACertified", DisplayName = "TSA Certified", DataType = SqlDbType.Bit},
                            new SearchField {Name = "CTPATCertified", DisplayName = "CTPAT Certified", DataType = SqlDbType.Bit},
                            new SearchField {Name = "SmartWayCertified", DataType = SqlDbType.Bit},
                            new SearchField {Name = "PIPNumber", DisplayName = "PIP Number", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "Scac", DisplayName = "SCAC", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "FederalIDNumber", DisplayName = "Federal ID Number", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "BrokerReferenceNumber", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "MC", DisplayName = "MC", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "DOT", DisplayName = "DOT", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "HandlesLessThanTruckload", DataType = SqlDbType.Bit},
                            new SearchField {Name = "HandlesTruckload", DataType = SqlDbType.Bit},
                            new SearchField {Name = "HandlesAir", DataType = SqlDbType.Bit},
                            new SearchField {Name = "HandlesPartialTruckload", DataType = SqlDbType.Bit},
                            new SearchField {Name = "HandlesRail", DataType = SqlDbType.Bit},
                            new SearchField {Name = "HandlesSmallPack", DataType = SqlDbType.Bit},
                            new SearchField {Name = "IsCarrier", DisplayName = "Is a Carrier", DataType = SqlDbType.Bit},
                            new SearchField {Name = "IsAgent", DisplayName = "Is an Agent", DataType = SqlDbType.Bit},
                            new SearchField {Name = "IsBroker", DisplayName = "Is a Broker", DataType = SqlDbType.Bit},
                            NonAliasEquipmentTypeName,
			           	};

			SalesRepresentatives = new List<SearchField>
                                       {
                                           SaleRepresentativeName,
                                           new SearchField {Name = "SalesRepresentativeNumber", DataType = SqlDbType.NVarChar},
                                           SaleRepresentativeCompanyName,
                                           new SearchField {Name = "DateCreated", DataType = SqlDbType.DateTime},
                                           new SearchField {Name = "Street1", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Street2", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "City", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "State", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "PostalCode", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Phone", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Mobile", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Fax", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Email", DataType = SqlDbType.NVarChar},
                                           NonAliasedCountryCode,
                                           NonAliasedCountryName,
                                           new SearchField {Name = "AdditionalEntityName", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "AdditionalEntityCommPercent", DataType = SqlDbType.Decimal}
                                       };
			Vendors = new List<SearchField>
				{
					CustomField1,
					CustomField2,
					CustomField3,
					CustomField4,
					new SearchField {Name = "Name", DisplayName = "Vendor Name", DataType = SqlDbType.NVarChar},
					VendorNumber,
					Active,
					new SearchField {Name = "DateCreated", DataType = SqlDbType.DateTime},
					new SearchField {Name = "Notation", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "TSACertified", DisplayName = "TSA Certified", DataType = SqlDbType.Bit},
					new SearchField {Name = "CTPATCertified", DisplayName = "CTPAT Certified", DataType = SqlDbType.Bit},
					new SearchField {Name = "SmartWayCertified", DataType = SqlDbType.Bit},
					new SearchField {Name = "PIPNumber", DisplayName = "PIP Number", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "Scac", DisplayName = "SCAC", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "FederalIDNumber", DisplayName = "Federal ID Number", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "BrokerReferenceNumber", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "MC", DisplayName = "MC", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "DOT", DisplayName = "DOT", DataType = SqlDbType.NVarChar},
					new SearchField {Name = "HandlesLessThanTruckload", DataType = SqlDbType.Bit},
					new SearchField {Name = "HandlesTruckload", DataType = SqlDbType.Bit},
					new SearchField {Name = "HandlesAir", DataType = SqlDbType.Bit},
					new SearchField {Name = "HandlesPartialTruckload", DataType = SqlDbType.Bit},
					new SearchField {Name = "HandlesRail", DataType = SqlDbType.Bit},
					new SearchField {Name = "HandlesSmallPack", DataType = SqlDbType.Bit},
					new SearchField {Name = "IsCarrier", DisplayName = "Is a Carrier", DataType = SqlDbType.Bit},
					new SearchField {Name = "IsAgent", DisplayName = "Is an Agent", DataType = SqlDbType.Bit},
					new SearchField {Name = "IsBroker", DisplayName = "Is a Broker", DataType = SqlDbType.Bit},
					NonAliasEquipmentTypeName,
					NonAliasedVendorTerminalCity,
					NonAliasedVendorTerminalState,
					NonAliasedVendorTerminalPostalCode,
					NonAliasedVendorTerminalCountryCode,
					NonAliasedVendorTerminalCountryName
				};

            VendorDisputes = new List<SearchField>
                {
                    new SearchField {Name = "DisputerReferenceNumber", DisplayName = "Disputer Reference Number", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DateCreated", DataType = SqlDbType.DateTime},
                    new SearchField {Name = "ResolutionDate", DisplayName="Resolution Date", DataType = SqlDbType.DateTime},
                    new SearchField {Name = "IsResolved", DisplayName = "Is Resolved", DataType = SqlDbType.Bit}
                };


            VendorBills = new List<SearchField>
			           	{
			           		Posted,
			           		new SearchField {Name = "Exported", DataType = SqlDbType.Bit},
			           		new SearchField {Name = "UserFirstName", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "UserLastName", DataType = SqlDbType.NVarChar},
                            BillingShipmentNumber,
                            BillingServiceTicketNumber
			           	};
			VendorBills.AddRange(VendorBillsSortFields);


			VendorCommunications = new List<SearchField>
			           	{
			           		new SearchField {Name = "Name", DisplayName = "Vendor Name", DataType = SqlDbType.NVarChar},
			           		VendorNumber,
                            new SearchField {Name = "Notation", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "Scac", DisplayName = "SCAC", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "FederalIDNumber", DisplayName = "Federal ID Number", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "BrokerReferenceNumber", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "MC", DisplayName = "MC", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "DOT", DisplayName = "DOT", DataType = SqlDbType.NVarChar},
			           	};

			Tiers = new List<SearchField>
			           	{
			           		TierName,
			           		new SearchField {Name = "LogoUrl", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "TierNumber", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "AdditionalBillOfLadingText", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "TollFreeContactNumber", DataType = SqlDbType.NVarChar},
			           	};
		}
	}
}
