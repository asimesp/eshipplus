﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
	public class VendorBillSearch : EntityBase
	{
		public List<VendorBillDashboardDto> FetchVendorBillDashboardDtos(VendorBillViewSearchCriteria criteria, long tenantId)
		{
			var vendorBills = new List<VendorBillDashboardDto>();

            // these columns will be handled by subqueries and excluded from the call to BuildWhereClause for criteria
            var subQueryReportColumns = new List<SearchField>
                {
                    AccountingSearchFields.BillingShipmentNumber,
                    AccountingSearchFields.BillingServiceTicketNumber,
                }.ToParameterColumn()
                 .Select(c => c.ReportColumnName)
                 .ToList();

            var containsShipmentFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(AccountingSearchFields.BillingShipmentNumber.DisplayName);
            var containsServiceTicketFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(AccountingSearchFields.BillingServiceTicketNumber.DisplayName);


			var query = @"SELECT Top 1000 * FROM VendorBillDashboardViewSearch(@TenantId, @ActiveUserId) vbdvs WHERE ";
			var parameters = new Dictionary<string, object> {{"TenantId", tenantId}, {"ActiveUserId", criteria.ActiveUserId}};

            // begin building subqueries to filter where applicable
            if (containsShipmentFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == AccountingSearchFields.BillingShipmentNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, AccountingSearchFields.VendorBills);

                var shipmentQuery = string.Format(@"SELECT vb.Id FROM VendorBill vb
                                                            INNER JOIN VendorBillDetail vbd ON vbd.VendorBillId = vb.Id
                                                            WHERE vbd.ReferenceType = {0} AND {1} ", DetailReferenceType.Shipment.ToInt(), filter);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("sh_{0}", item.Key);
                    shipmentQuery = shipmentQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format("(vbdvs.Id IN ({0})) AND ", shipmentQuery);
            }

            if (containsServiceTicketFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == AccountingSearchFields.BillingServiceTicketNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, AccountingSearchFields.VendorBills);

                var serviceTicketQuery = string.Format(@"SELECT vb.Id FROM VendorBill vb
                                                            INNER JOIN VendorBillDetail vbd ON vbd.VendorBillId = vb.Id
                                                            WHERE vbd.ReferenceType = {0} AND {1} ", DetailReferenceType.ServiceTicket.ToInt(), filter);


                foreach (var item in uParams)
                {
                    var newKey = string.Format("st_{0}", item.Key);
                    serviceTicketQuery = serviceTicketQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format("(vbdvs.Id IN ({0})) AND ", serviceTicketQuery);
            }

            query += criteria.Parameters.Where(p => !subQueryReportColumns.Contains(p.ReportColumnName))
                                        .ToList()
                                        .BuildWhereClause(parameters, AccountingSearchFields.VendorBills);

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);
            
            using (var reader = GetReader(query, parameters))
				while (reader.Read())
					vendorBills.Add(new VendorBillDashboardDto(reader));

			Connection.Close();
			return vendorBills;
		}

		public VendorBill FetchVendorBillByVendorBillNumber(string number, long vendorLocationId, long tenantId)
		{
			VendorBill invoice = null;
			const string query = @"SELECT * FROM VendorBill WHERE DocumentNumber = @DocumentNumber AND VendorLocationId = @VendorLocationId AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "DocumentNumber", number }, { "TenantId", tenantId }, { "VendorLocationId", vendorLocationId } };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					invoice = new VendorBill(reader);
			Connection.Close();
			return invoice;
		}

		public decimal FetchInvoiceTotalCreditAdjustments(long vendorBillId, long tenantId)
		{
			const string query =
				@"select ISNULL(SUM(TotalCredit),0) as 'TotalCredit' from VendorBillCreditsSummary 
					where ApplyToDocumentId = @ApplyToDocumentId and TenantId = @TenantId";

			var parameters = new Dictionary<string, object> { { "ApplyToDocumentId", vendorBillId }, { "TenantId", tenantId }, };

			return ExecuteScalar(query, parameters).ToDecimal();
		}

        public bool ChargesAreLinkedToVendorBill(long billId, long tenantId)
        {
            const string query =
                @"SELECT COUNT(*) FROM
                    (
                    SELECT Id From ShipmentCharge sc
                    WHERE sc.VendorBillId = @VendorBillId AND sc.TenantId = @TenantId

                    UNION

                    SELECT Id FROM ServiceTicketCharge stc
                    WHERE stc.VendorBillId = @VendorBillId AND stc.TenantId = @TenantId
                    ) temp";

            var parameters = new Dictionary<string, object>
                {
                    {"VendorBillId", billId},
                    {"TenantId", tenantId},
                };

            return ExecuteScalar(query, parameters).ToInt() > 0;
        }

		public List<VendorBillDocumentDto> FetchAssociatedDocuments(long vendorBillId)
		{
			string query = @"SELECT 
								(SELECT top 1 [ShipmentNumber] FROM [Shipment] where Id = ShipmentId) as ReferenceNumber, 
								'Shipment' as ReferenceType, 
								LocationPath, 
								[Name] 
							FROM ShipmentDocument 
							WHERE VendorBillId = @vendorBillId AND VendorBillId <> 0

							UNION

							SELECT 
								(SELECT top 1 ServiceTicketNumber FROM [ServiceTicket] where Id = ServiceTicketId) as ReferenceNumber, 
								'ServiceTicket' as ReferenceType, 
								LocationPath, [Name] 
							FROM ServiceTicketDocument 
							WHERE VendorBillId = @vendorBillId AND VendorBillId <> 0";

			var parameters = new Dictionary<string, object> { { "vendorBillId", vendorBillId }};
			var vendorDocuments = new List<VendorBillDocumentDto>();
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					vendorDocuments.Add(new VendorBillDocumentDto(reader));
			Connection.Close();
			return vendorDocuments;

		}
	}
}