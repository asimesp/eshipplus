﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
	public class InvoiceSearch : EntityBase
	{
		public Invoice FetchInvoiceByInvoiceNumber(string number, long tenantId)
		{
			Invoice invoice = null;
			const string query = @"SELECT * FROM Invoice WHERE InvoiceNumber = @InvoiceNumber AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "InvoiceNumber", number }, { "TenantId", tenantId }, };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					invoice = new Invoice(reader);
			Connection.Close();
			return invoice;
		}

		public List<InvoiceDashboardDto> FetchInvoiceDashboardDtos(InvoiceViewSearchCriteria criteria, long tenantId)
		{
			var invoices = new List<InvoiceDashboardDto>();

            // these columns will be handled by subqueries and excluded from the call to BuildWhereClause for criteria
			var subQueryReportColumns = new List<SearchField>
				{
					AccountingSearchFields.BillingShipmentNumber,
					AccountingSearchFields.BillingServiceTicketNumber,
				}.ToParameterColumn()
				 .Select(c => c.ReportColumnName)
				 .ToList();

            var containsShipmentFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(AccountingSearchFields.BillingShipmentNumber.DisplayName);
            var containsServiceTicketFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(AccountingSearchFields.BillingServiceTicketNumber.DisplayName);

			var query = @"SELECT Top 1000 * FROM InvoiceDashboardViewSearch(@TenantId, @ActiveUserId) idvs WHERE ";
			var parameters = new Dictionary<string, object>{{"TenantId", tenantId}, {"ActiveUserId", criteria.ActiveUserId}};

            // begin building subqueries to filter where applicable
            if (containsShipmentFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == AccountingSearchFields.BillingShipmentNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, AccountingSearchFields.Invoices);

                var shipmentQuery = string.Format(@"SELECT i.Id FROM Invoice i
                                                            INNER JOIN InvoiceDetail id ON id.InvoiceId = i.Id
                                                            WHERE id.ReferenceType = {0} AND {1} ", DetailReferenceType.Shipment.ToInt(), filter);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("sh_{0}", item.Key);
                    shipmentQuery = shipmentQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format("(idvs.Id IN ({0})) AND ", shipmentQuery);
            }

            if (containsServiceTicketFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == AccountingSearchFields.BillingServiceTicketNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, AccountingSearchFields.Invoices);

                var serviceTicketQuery = string.Format(@"SELECT i.Id FROM Invoice i
                                                            INNER JOIN InvoiceDetail id ON id.InvoiceId = i.Id
                                                            WHERE id.ReferenceType = {0} AND {1} ", DetailReferenceType.ServiceTicket.ToInt(), filter);


                foreach (var item in uParams)
                {
                    var newKey = string.Format("st_{0}", item.Key);
                    serviceTicketQuery = serviceTicketQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format("(idvs.Id IN ({0})) AND ", serviceTicketQuery);
            }

            query += criteria.Parameters.Where(p => !subQueryReportColumns.Contains(p.ReportColumnName))
                                        .ToList()
                                        .BuildWhereClause(parameters, AccountingSearchFields.Invoices);


            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);
            
            using (var reader = GetReader(query, parameters))
				while (reader.Read())
					invoices.Add(new InvoiceDashboardDto(reader));
			Connection.Close();
			return invoices;
		}

		public long FetchInvoiceIdByInvoiceNumber(string number, long tenantId)
        {
            const string query = @"SELECT Id FROM Invoice WHERE InvoiceNumber = @InvoiceNumber AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "InvoiceNumber", number }, { "TenantId", tenantId }, };

            return ExecuteScalar(query, parameters).ToLong();
        }

		public List<InvoiceControlAccountUpdateDto> FetchInvoicesNotPostedWithoutControlAccounts(long tenantId)
		{
			var dtos = new List<InvoiceControlAccountUpdateDto>();
			const string query =
				@"select distinct
					i.Id 'InvoiceId',
					i.InvoiceNumber,
					i.InvoiceDate,
					c.Id 'CustomerId',
					c.CustomerNumber 'InvoiceCustomerNumber',
					c.[Name] 'InvoiceCustomerName',
					d.ReferenceNumber 'ShipmentNumber',
					coalesce((ol.Street1 + ' ' + ol.Street2 + ' ' + ol.City + ' ' + ol.[State] + ' ' + ol.PostalCode + ' ' + oc.Name), '') 'ShipmentOrigin',
					coalesce((dl.Street1 + ' ' + dl.Street2 + ' ' + dl.City + ' ' + dl.[State] + ' ' + dl.PostalCode + ' ' + dc.Name),'') 'ShipmentDestination',
					u.FirstName 'UserFirstName',
					u.LastName 'UserLastName',
					u.Username,
                    u.Id 'UserId'
				from
					Invoice i
					inner join InvoiceDetail d on i.Id = d.InvoiceId
					inner join Shipment s on d.ReferenceNumber = s.ShipmentNumber and d.ReferenceType = 0 -- 0 = shipment
					inner join ShipmentLocation ol on s.OriginId = ol.Id
					inner join Country as oc on ol.CountryId = oc.Id
					inner join ShipmentLocation dl on s.DestinationId = dl.Id
					inner join Country as dc on dl.CountryId = dc.Id
					inner join [User] as u on i.UserId = u.Id
					inner join CustomerLocation cl on i.CustomerLocationId = cl.Id
					inner join Customer c on cl.CustomerId = c.Id
				where 
					i.TenantId = @TenantId
					and i.Posted = 0
					and (i.CustomerControlAccountNumber = '' or i.CustomerControlAccountNumber is null)
					and c.InvoiceRequiresControlAccount = 1
				order by
					c.CustomerNumber, i.InvoiceNumber";
			var parameters = new Dictionary<string, object> {{"TenantId", tenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					dtos.Add(new InvoiceControlAccountUpdateDto(reader));
			Connection.Close();
			return dtos;
		}

		public List<Invoice> FetchInvoiceNotDocDelivered(DateTime startCheckDate, long customerId, long tenantId)
		{
			var invoices = new List<Invoice>();
			const string query = @"Select i.*
					From 
						Invoice i
						Inner Join CustomerLocation cl On i.CustomerLocationId = cl.Id
						Inner Join Customer c On cl.CustomerId = c.Id
					Where 
						i.TenantId = @TenantId
						And FtpDelivered = '0'
						And Posted = '1'
						And i.InvoiceDate >= @StartCheckDate
						And c.Id = @CustomerId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"TenantId", tenantId},
			                 		{"StartCheckDate", startCheckDate.TimeToMinimum()},
			                 		{"CustomerId", customerId}
			                 	};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					invoices.Add(new Invoice(reader));
			Connection.Close();
			return invoices;

		}

		public decimal FetchInvoiceTotalCreditAdjustments(long invoiceId, long tenantId)
		{
			const string query =
				@"select ISNULL(SUM(TotalCredit),0) as 'TotalCredit' from InvoiceCreditsAndSupplementalsSummary 
					where OriginalInvoiceId = @InvoiceId and TenantId = @TenantId";

			var parameters = new Dictionary<string, object> { { "InvoiceId", invoiceId }, { "TenantId", tenantId }, };

			return ExecuteScalar(query, parameters).ToDecimal();
		}

		public decimal FetchInvoiceTotalSupplementalAdjustments(long invoiceId, long tenantId)
		{
			const string query =
				@"select ISNULL(SUM(TotalSupplementalCharges),0) as 'TotalSupplementalCharges' from InvoiceCreditsAndSupplementalsSummary 
					where OriginalInvoiceId = @InvoiceId and TenantId = @TenantId";

			var parameters = new Dictionary<string, object> { { "InvoiceId", invoiceId }, { "TenantId", tenantId }, };

			return ExecuteScalar(query, parameters).ToDecimal();
		}

        public List<InvoiceDashboardDto> FetchUnpaidPostedInvoiceDtosForShipment(string shipmentNumber, long userId, long tenantId)
        {
            var invoices = new List<InvoiceDashboardDto>();
            const string query = @"Select Distinct i.* From InvoiceDashboardViewSearch(@TenantId, @ActiveUserId) i Inner join InvoiceDetail id on i.Id = id.InvoiceId 
                                    Where id.ReferenceType = 0 And id.ReferenceNumber = @ShipmentNumber And i.Posted = 1 And i.PaidAmount < i.AmountDue And i.InvoiceType <> 2";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ShipmentNumber", shipmentNumber }, { "ActiveUserId", userId } };

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    invoices.Add(new InvoiceDashboardDto(reader));
            Connection.Close();
            return invoices;
        }

	}
}