﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
    public class CustomerPurchaseOrderSearch: EntityBase
    {
        public List<CustomerPurchaseOrder> FetchCustomerPurchaseOrders(PurchaseOrderViewSearchCriteria criteria, long tenantId)
        {
            var customerPurchaseOrders = new List<CustomerPurchaseOrder>();

            var query =
                @"SELECT * FROM CustomerPurchaseOrder 
					WHERE TenantId = @TenantId AND CustomerId = @CustomerId AND";

            var parameters = new Dictionary<string, object> {{ "TenantId", tenantId },{"CustomerId", criteria.CustomerId} };

            var baseCountryCols = new List<SearchField>
                                   {
                                       AccountingSearchFields.NonAliasedCountryCode,
                                       AccountingSearchFields.NonAliasedCountryName,
                                   }.ToParameterColumn()
               .Select(c => c.ReportColumnName)
               .ToList();


            var countrycols = criteria.Parameters.Where(c => baseCountryCols.Contains(c.ReportColumnName)).ToList();
            var vtCols = criteria.Parameters.Where(c => !baseCountryCols.Contains(c.ReportColumnName)).ToList();

            query += vtCols.BuildWhereClause(parameters, AccountingSearchFields.CustomerPurchaseOrders);

            if (countrycols.Any())
            {
                var cParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var cQuery = "SELECT Id FROM Country WHERE ";

                cQuery += countrycols.BuildWhereClause(cParams, AccountingSearchFields.CustomerPurchaseOrders);

                foreach (var item in cParams)
                {
                    var newKey = string.Format("c_{0}", item.Key);
                    cQuery = cQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND (CountryId IN ({0}))", cQuery);
            }


            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    customerPurchaseOrders.Add(new CustomerPurchaseOrder(reader));
            Connection.Close();
            return customerPurchaseOrders;
        }

        public CustomerPurchaseOrder FetchCustomerPurchaseOrder (long tenantId, long customerId, string purchaseOrderNumber)
        {
            CustomerPurchaseOrder customerPurchaseOrder = null;

            const string query = @"SELECT * FROM CustomerPurchaseOrder 
					WHERE TenantId = @TenantId
                          AND CustomerId = @CustomerId
                          AND PurchaseOrderNumber = @PurchaseOrderNumber";

            var parameter = new Dictionary<string, object> { { "TenantId", tenantId }, { "CustomerId", customerId },{"PurchaseOrderNumber", purchaseOrderNumber} };

            using (var reader = GetReader(query, parameter))
                if (reader.Read())
                 customerPurchaseOrder = new CustomerPurchaseOrder(reader);
            Connection.Close();
            return customerPurchaseOrder;
  
        }
    }
}
