﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
    public class VendorSearch : EntityBase
    {
        public Vendor FetchVendorByNumber(string vendorNumber, long tenantId)
        {
            var query = string.Format(
                @"SELECT * FROM Vendor WHERE TenantId = @TenantId
                                         AND VendorNumber = @VendorNumber
                                         ORDER BY[Name] {0}, VendorNumber {0}", SearchUtilities.Ascending);

            var parameters = new Dictionary<string, object> { { "VendorNumber", vendorNumber }, { "TenantId", tenantId } };
            Vendor vendor = null;
            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    vendor = new Vendor(reader);
            Connection.Close();

            return vendor;
        }


        public List<Vendor> FetchVendors(List<ParameterColumn> columns, long tenantId)
        {
            var vendors = new List<Vendor>();

            var query = "SELECT * FROM Vendor WHERE TenantId = @TenantId AND ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };


        	var baseEquipmentCols = new List<SearchField> {AccountingSearchFields.NonAliasEquipmentTypeName,}.ToParameterColumn()
        		.Select(c => c.ReportColumnName)
        		.ToList();

            var baseTerminaltCols = new List<SearchField>
                                   {
                                       AccountingSearchFields.NonAliasedVendorTerminalCity,
                                       AccountingSearchFields.NonAliasedVendorTerminalState,
                                       AccountingSearchFields.NonAliasedVendorTerminalPostalCode,
                                       AccountingSearchFields.NonAliasedVendorTerminalCountryCode,
                                       AccountingSearchFields.NonAliasedVendorTerminalCountryName,
                                   }.ToParameterColumn()
               .Select(c => c.ReportColumnName)
               .ToList();

            var equipmentCols = columns.Where(c => baseEquipmentCols.Contains(c.ReportColumnName)).ToList();
            var terminalCols = columns.Where(c => baseTerminaltCols.Contains(c.ReportColumnName)).ToList();

            var vendorCols = columns.Where(c => !baseEquipmentCols.Contains(c.ReportColumnName) && !baseTerminaltCols.Contains(c.ReportColumnName)).ToList();

            query += vendorCols.BuildWhereClause(parameters, AccountingSearchFields.Vendors);

            if (equipmentCols.Any())
            {
            	var etParams = new Dictionary<string, object>();
                var etQuery = @"SELECT VendorId FROM VendorEquipment
                                INNER JOIN EquipmentType ON VendorEquipment.EquipmentTypeId = EquipmentType.Id WHERE ";

                etQuery += equipmentCols.BuildWhereClause(etParams, AccountingSearchFields.Vendors);

                foreach (var item in etParams)
                {
                    var newKey = string.Format("e_{0}", item.Key);
                    etQuery = etQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND (Vendor.Id IN ({0}))", etQuery);
            }


            if (terminalCols.Any())
            {
            	var tParams = new Dictionary<string, object>();
                var tQuery = @"SELECT VendorId FROM VendorTerminal WHERE ";

            	var baseCtCols = new List<SearchField>
            	                 	{
            	                 		AccountingSearchFields.NonAliasedVendorTerminalCountryCode,
            	                 		AccountingSearchFields.NonAliasedVendorTerminalCountryName,
            	                 	}.ToParameterColumn()
            		.Select(c => c.ReportColumnName)
            		.ToList();

            	var tcCols = terminalCols.Where(c => baseCtCols.Contains(c.ReportColumnName)).ToList();
            	var rtcCols = terminalCols.Where(c => !baseCtCols.Contains(c.ReportColumnName)).ToList();

                tQuery += rtcCols.BuildWhereClause(tParams, AccountingSearchFields.Vendors);

                foreach (var item in tParams)
                {
                    var newKey = string.Format("t_{0}", item.Key);
                    tQuery = tQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

				if(tcCols.Any())
				{
					tParams.Clear();
					var tcQuery = "SELECT Id FROM Country WHERE ";
					tcQuery += tcCols.BuildWhereClause(tParams, AccountingSearchFields.Vendors);

					foreach (var item in tParams)
					{
						var newKey = string.Format("tc_{0}", item.Key);
						tcQuery = tcQuery.Replace(item.Key, newKey);
						parameters.Add(newKey, item.Value);
					}

					tQuery += string.Format(" AND (CountryId IN ({0}))", tcQuery);
				}

                query += string.Format(" AND (Vendor.Id IN ({0}))", tQuery);
            }

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    vendors.Add(new Vendor(reader));
            Connection.Close();

            return vendors;
        }

        public List<Vendor> FetchVendorsWithCommunication(List<ParameterColumn> columns , long tenantId)
        {
            var customers = new List<Vendor>();

            var query = @"SELECT Vendor.* FROM Vendor 
									INNER JOIN [VendorCommunication] ON Vendor.[Id] = VendorCommunication.VendorId 
								WHERE 
                                   Vendor.TenantId = @TenantId AND  ";

            var parameters = new Dictionary<string, object> {{ "TenantId", tenantId }};

            query += columns.BuildWhereClause(parameters, AccountingSearchFields.VendorCommunications);

            query += string.Format("ORDER BY Vendor.[Name] {0}, Vendor.VendorNumber {0}", SearchUtilities.Ascending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    customers.Add(new Vendor(reader));
            Connection.Close();

            return customers;
        }

        public List<Vendor> FetchVendorsForLTLRating(long tenantId)
        {
            var vendors = new List<Vendor>();
            const string query = @"Select distinct v.* from Vendor as v, VendorRating as r, DiscountTier as d
					where 
						d.VendorRatingId = r.Id 
						and r.VendorId = v.Id
						and v.Active = @Active
						and v.HandlesLessThanTruckload = @handlesLessThanTruckLoad
						and v.TenantId = @TenantId";

            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"handlesLessThanTruckLoad", true},
			                 		{"TenantId", tenantId},
			                 		{"Active", true}
			                 	};
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    vendors.Add(new Vendor(reader));
            Connection.Close();

            return vendors;
        }

        public List<string> FetchVendorEmailsForAvailableLoads(AvailableLoadsSearchCriteria criteria, long tenantId)
        {
            var parameters = new Dictionary<string, object>();

            var vendorIds = new HashSet<long>();

            // checking vendor types
            var qeVtVid = @"select Id 'VendorId' From Vendor where TenantId = @TenantId and Active = 1";
            if (criteria.IncludeAgents) qeVtVid += " and IsAgent = 1";
            if (criteria.IncludeBrokers) qeVtVid += " and IsBroker = 1";
            if (criteria.IncludeCarrier) qeVtVid += " and IsCarrier = 1";
            parameters.Add("TenantId", tenantId);
            using (var reader = GetReader(qeVtVid, parameters))
                while (reader.Read())
                    vendorIds.Add(reader.GetValue(reader.GetOrdinal("VendorId")).ToLong());
            Connection.Close();

            // check for equipment types
            if (criteria.CheckEquipment)
            {
                var eVid = new HashSet<long>();
                var qeVid = string.Format("Select VendorId from VendorEquipment where EquipmentTypeId in ({0}) and TenantId = @TenantId",
                                  criteria.EquipmentTypeIds.Select(i => i.ToString()).ToArray().CommaJoin());
                parameters.Clear();
                parameters.Add("TenantId", tenantId);
                using (var reader = GetReader(qeVid, parameters))
                    while (reader.Read())
                        eVid.Add(reader.GetValue(reader.GetOrdinal("VendorId")).ToLong());
                Connection.Close();
                vendorIds.IntersectWith(eVid);
            }

            // check for terminals
            if ((criteria.CheckOrigin || criteria.CheckDestination) && criteria.CheckTerminals)
            {
                var tVid = new HashSet<long>();
                const string qtVid =
                    @"select VendorId from VendorTerminal where City like @City and [State] like @State 
				and PostalCode like @PostalCode and CountryId = @CountryId and TenantId = @TenantId";

                if (criteria.CheckOrigin)
                {
                    parameters.Clear();
                    parameters.Add("TenantId", tenantId);
                    parameters.Add("City", criteria.OriginCity.WrapWithSqlWildCard());
                    parameters.Add("State", criteria.OriginState.WrapWithSqlWildCard());
                    parameters.Add("PostalCode", criteria.OriginPostalCode.WrapWithSqlWildCard());
                    parameters.Add("CountryId", criteria.OriginCountryId);
                    using (var reader = GetReader(qtVid, parameters))
                        while (reader.Read())
                            tVid.Add(reader.GetValue(reader.GetOrdinal("VendorId")).ToLong());
                    Connection.Close();
                }
                if (criteria.CheckDestination)
                {
                    parameters.Clear();
                    parameters.Add("TenantId", tenantId);
                    parameters.Add("City", criteria.DestinationCity.WrapWithSqlWildCard());
                    parameters.Add("State", criteria.DestinationState.WrapWithSqlWildCard());
                    parameters.Add("PostalCode", criteria.DestinationPostalCode.WrapWithSqlWildCard());
                    parameters.Add("CountryId", criteria.DestinationCountryId);
                    using (var reader = GetReader(qtVid, parameters))
                        while (reader.Read())
                            tVid.Add(reader.GetValue(reader.GetOrdinal("VendorId")).ToLong());
                    Connection.Close();
                }
                vendorIds.IntersectWith(tVid);
            }

            // check lanes
            if ((criteria.CheckOrigin || criteria.CheckDestination) && !criteria.IgnoreLanes)
            {
                var lVid = new HashSet<long>();

                if (criteria.CheckOrigin && criteria.CheckDestination)
                {

                    // parameters
                    parameters.Clear();
                    parameters.Add("TenantId", tenantId);
                    parameters.Add("OriginCity", criteria.OriginCity.WrapWithSqlWildCard());
                    parameters.Add("OriginState", criteria.OriginState.WrapWithSqlWildCard());
                    parameters.Add("OriginPostalCode", criteria.OriginPostalCode.WrapWithSqlWildCard());
                    parameters.Add("OriginCountryId", criteria.OriginCountryId);
                    parameters.Add("DestinationCity", criteria.DestinationCity.WrapWithSqlWildCard());
                    parameters.Add("DestinationState", criteria.DestinationState.WrapWithSqlWildCard());
                    parameters.Add("DestinationPostalCode", criteria.DestinationPostalCode.WrapWithSqlWildCard());
                    parameters.Add("DestinationCountryId", criteria.DestinationCountryId);

                    // check for preferred lanes
                    const string qPlVid =
                        @"select VendorId from VendorPreferredLane 
					where 
						OriginCity like @OriginCity 
						and OriginState like @OriginState 
						and OriginPostalCode like @OriginPostalCode 
						and OriginCountryId = @OriginCountryId 
						and DestinationCity like @DestinationCity 
						and DestinationState like @DestinationState 
						and DestinationPostalCode like @DestinationPostalCode 
						and DestinationCountryId = @DestinationCountryId
						and TenantId = @TenantId";
                    using (var reader = GetReader(qPlVid, parameters))
                        while (reader.Read())
                            lVid.Add(reader.GetValue(reader.GetOrdinal("VendorId")).ToLong());
                    Connection.Close();

                    // check for run lanes (within last year from today)
                    const string qRlVid =
                        @"Select vendorId
					From Shipment s
					Inner Join ShipmentVendor sv on sv.ShipmentId = s.Id and sv.[Primary] = 1
					Inner Join ShipmentLocation slo on slo.ShipmentId = s.OriginId
					Inner Join ShipmentLocation sld on sld.ShipmentId = s.DestinationId
					where 
						slo.City like @OriginCity 
						and slo.State like @OriginState 
						and slo.PostalCode like @OriginPostalCode 
						and slo.CountryId = @OriginCountryId 
						and sld.City like @DestinationCity 
						and sld.State like @DestinationState 
						and sld.PostalCode like @DestinationPostalCode 
						and sld.CountryId = @DestinationCountryId
						and s.TenantId = @TenantId
						and @StartDate <= s.DateCreated and s.DateCreated <= @EndDate";
                    parameters.Add("StartDate", DateTime.Now.AddYears(-2).TimeToMinimum());
                    parameters.Add("EndDate", DateTime.Now.TimeToMaximum());
                    using (var reader = GetReader(qRlVid, parameters))
                        while (reader.Read())
                            lVid.Add(reader.GetValue(reader.GetOrdinal("VendorId")).ToLong());
                    Connection.Close();
                }

                vendorIds.IntersectWith(lVid);
            }

            var emails = new List<string>();

            var ors = vendorIds.Select(i => string.Format(" OR VendorId = {0}", i)).ToArray();
            if (ors.Any()) ors[0] = ors[0].Replace(" OR ", string.Empty);
            var qWhere = vendorIds.Any()
                            ? string.Format("where {0}", ors.SpaceJoin())
                            : "where VendorId = 0";
            var query = string.Format(@"SELECT * FROM dbo.VendorEmailsForAvailableLoads(@ContactTypeId, @TenantId) {0}", qWhere);
            parameters.Clear();
            parameters.Add("ContactTypeId", criteria.ContactTypeId);
            parameters.Add("TenantId", tenantId);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                {
                    var email = reader.GetValue(reader.GetOrdinal("Email")).GetString();
                    if (!string.IsNullOrEmpty(email)) emails.Add(email);
                }
            Connection.Close();

            return emails;
        }


        public List<VendorCommunication> FetchVendorCommunicationsWithEdiOrFtp()
        {
            var communications = new List<VendorCommunication>();

            var query =
                string.Format(@"SELECT * FROM VendorCommunication WHERE EdiEnabled = @Enabled OR FtpEnabled = @Enabled");

            var parameters = new Dictionary<string, object> { { "Enabled", true } };

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    communications.Add(new VendorCommunication(reader));
            Connection.Close();

            return communications;
        }

		public List<VendorCommunication> FetchVendorCommunicationsWithImgRtrv()
		{
			var communications = new List<VendorCommunication>();

			var query =
				string.Format(@"SELECT * FROM VendorCommunication WHERE ImgRtrvEnabled = @Enabled");

			var parameters = new Dictionary<string, object> { { "Enabled", true } };

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					communications.Add(new VendorCommunication(reader));
			Connection.Close();

			return communications;
		}
    }
}