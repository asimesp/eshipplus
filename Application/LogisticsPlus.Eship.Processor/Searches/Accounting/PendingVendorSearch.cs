﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
	public class PendingVendorSearch : EntityBase
	{
		public PendingVendor FetchPendingVendorByNumber(string vendorNumber, long tenantId)
		{
			var query = string.Format(
				@"SELECT * FROM PendingVendor WHERE TenantId = @TenantId
                                         AND VendorNumber = @VendorNumber
                                         ORDER BY[Name] {0}, VendorNumber {0}", SearchUtilities.Ascending);

			var parameters = new Dictionary<string, object> { { "VendorNumber", vendorNumber }, { "TenantId", tenantId } };
			PendingVendor pendingVendor = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					pendingVendor = new PendingVendor(reader);
			Connection.Close();

			return pendingVendor;
		}


		public List<PendingVendor> FetchPendingVendors(List<ParameterColumn> columns, long tenantId)
		{
			var pendingVendors = new List<PendingVendor>();

			var query = "SELECT * FROM PendingVendor WHERE TenantId = @TenantId AND ";
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };


			var baseEquipmentCols = new List<SearchField> { AccountingSearchFields.NonAliasEquipmentTypeName, }.ToParameterColumn()
				.Select(c => c.ReportColumnName)
				.ToList();

			var equipmentCols = columns.Where(c => baseEquipmentCols.Contains(c.ReportColumnName)).ToList();

			var pendingVendorCols = columns.Where(c => !baseEquipmentCols.Contains(c.ReportColumnName)).ToList();

			query += pendingVendorCols.BuildWhereClause(parameters, AccountingSearchFields.PendingVendors);

			if (equipmentCols.Any())
			{
				var etParams = new Dictionary<string, object>();
				var etQuery = @"SELECT PendingVendorId FROM PendingVendorEquipment
                                INNER JOIN EquipmentType ON PendingVendorEquipment.EquipmentTypeId = EquipmentType.Id WHERE ";

				etQuery += equipmentCols.BuildWhereClause(etParams, AccountingSearchFields.PendingVendors);

				foreach (var item in etParams)
				{
					var newKey = string.Format("e_{0}", item.Key);
					etQuery = etQuery.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}
				query += string.Format(" AND (PendingVendor.Id IN ({0}))", etQuery);
			}

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					pendingVendors.Add(new PendingVendor(reader));
			Connection.Close();

			return pendingVendors;
		}
	}
}
