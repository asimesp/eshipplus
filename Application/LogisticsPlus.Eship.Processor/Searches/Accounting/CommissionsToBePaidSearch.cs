﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
	public class CommissionsToBePaidSearch : EntityBase
	{
		public List<CommissionsToBePaidViewSearchDto> FetchCommissionsToBePaid(CommissionsToBePaidViewSearchCriteria criteria, long tenantId)
		{
			var commissions = new List<CommissionsToBePaidViewSearchDto>();

			var query = @"SELECT top 10000 * FROM CommissionsToBePaidViewSearch(@TenantId, @ActiveUserId) WHERE ";
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };
			query += criteria.Parameters.BuildWhereClause(parameters, AccountingSearchFields.CommissionsToBePaid);

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					commissions.Add(new CommissionsToBePaidViewSearchDto(reader));
			Connection.Close();


			return criteria.OnlyCommsWithNoPayment
					   ? commissions.Where(c => c.NetCommPayments == 0).ToList()
					   : commissions;
		}
	}
}
