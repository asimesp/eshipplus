﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using System.Linq;

namespace LogisticsPlus.Eship.Processor.Searches.Accounting
{
    public class ChargeSearch : EntityBase
    {
        public List<ChargeItemDto> FetchChargeGroups(ChargeViewSearchCriteria criteria, long tenantId, long vendorId, bool canSeeAllVendorsInVendorPortal)
        {
            var charges = new List<ChargeItemDto>();
            var query = string.Format(@"
SELECT TOP 10000 * FROM 
(
SELECT 
  s.ShipmentNumber AS Number, 
  '{0}' as ChargeType, 
  SUM(sc.UnitBuy * sc.Quantity) AS UnitBuy,
  v.Id as VendorId,
  v.Name as VendorName
FROM 
ShipmentCharge sc
INNER JOIN Shipment s ON s.Id = sc.ShipmentId
INNER JOIN ShipmentVendor sv ON sv.ShipmentId = s.Id AND (sc.VendorId = sv.VendorId OR (sc.VendorId = 0 AND sv.[Primary] = 1)) {2}
INNER JOIN Vendor v ON v.Id = sv.VendorId
WHERE sc.VendorBillId = 0 AND sc.TenantId = @TenantId
GROUP BY s.ShipmentNumber, v.Name, v.Id
UNION 
SELECT 
  st.ServiceTicketNumber AS Number, 
  '{1}' as ChargeType, 
  SUM(stc.UnitBuy * stc.Quantity) AS UnitBuy,
  v.Id as VendorId,
  v.Name as VendorName
FROM 
ServiceTicketCharge stc
INNER JOIN ServiceTicket st ON st.Id = stc.ServiceTicketId
INNER JOIN ServiceTicketVendor stv ON stv.ServiceTicketId = st.Id  AND (stc.VendorId = stv.VendorId OR (stc.VendorId = 0 AND stv.[Primary] = 1)) {3}
INNER JOIN Vendor v ON v.Id = stv.VendorId
WHERE stc.VendorBillId = 0 AND stc.TenantId = @TenantId
GROUP BY st.ServiceTicketNumber, v.Name, v.Id
) temp 
  WHERE 
",
ChargeType.Shipment,
ChargeType.ServiceTicket,
canSeeAllVendorsInVendorPortal ? string.Empty : "AND sv.VendorId = " + vendorId,
canSeeAllVendorsInVendorPortal ? string.Empty : "AND stv.VendorId = " + vendorId);

            var parameter = new Dictionary<string, object> { { "TenantId", tenantId } };
            query += criteria.Parameters.BuildWhereClause(parameter, AccountingSearchFields.Charges);

            query += SearchUtilities.AddQuerySorting(criteria.SortBy, criteria.SortAscending);

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    charges.Add(new ChargeItemDto(reader));
            Connection.Close();

            return charges;
        }
        
        public List<ChargeViewSearchDto> FetchChargesByTypeAndNumber(string number, ChargeType chargeType)
        {
            var charges = new List<ChargeViewSearchDto>();
            var query = string.Format(@"
SELECT * From (
SELECT s.ShipmentNumber AS Number, 
    {0} as ChargeType, 
    vdd.DisputedId,
    case 
       when sc.VendorId = 0 then itemVendor.Id
       else sc.VendorId
     end as VendorId,
      case 
        when sc.VendorId = 0 then itemVendor.VendorNumber
        else chargeVendor.VendorNumber
     end as VendorNumber,
      case 
       when sc.VendorId = 0 then itemVendor.Name
       else chargeVendor.Name
     end as VendorName,
      case 
       when sc.VendorId = 0 then itemVendorLocation.Id
       else chargeVendorLocation.Id
     end as VendorLocationId,
      case 
       when sc.VendorBillId > 0 then {2}
       when vdd.Id IS NOT NULL then {3}
       else {4}
     end as Status, 
sc.Id, sc.TenantId, sc.ShipmentId as ParentId, sc.Quantity, sc.UnitBuy, sc.UnitSell, sc.UnitDiscount, sc.Comment, sc.ChargeCodeId, sc.VendorBillId
-- sc.* 
FROM ShipmentCharge sc 
INNER JOIN Shipment s ON s.id = sc.ShipmentId
LEFT JOIN VendorDisputeDetail vdd ON vdd.ChargeLineId = sc.Id
INNER JOIN ShipmentVendor sv ON s.Id = sv.ShipmentId AND (sc.VendorId = sv.VendorId OR (sc.VendorId = 0 AND sv.[Primary] = 1))
LEFT JOIN Vendor itemVendor ON itemVendor.Id = sv.VendorId
LEFT JOIN Vendor chargeVendor ON chargeVendor.Id = sc.VendorId
LEFT JOIN VendorLocation itemVendorLocation ON itemVendor.Id = itemVendorLocation.VendorId AND itemVendorLocation.[Primary] = 1
LEFT JOIN VendorLocation chargeVendorLocation ON chargeVendor.Id = chargeVendorLocation.VendorId AND chargeVendorLocation.[Primary] = 1
WHERE s.ShipmentNumber = @Number
UNION ALL
SELECT st.ServiceTicketNumber AS Number, 
{1} as ChargeType, 
vdd.DisputedId,
  case 
   when stc.VendorId = 0 then itemVendor.Id
   else stc.VendorId
 end as VendorId,
  case 
    when stc.VendorId = 0 then itemVendor.VendorNumber
    else chargeVendor.VendorNumber
 end as VendorNumber,
  case 
   when stc.VendorId = 0 then itemVendor.Name
   else chargeVendor.Name
 end as VendorName,
  case 
   when stc.VendorId = 0 then itemVendorLocation.Id
   else chargeVendorLocation.Id
 end as VendorLocationId,
  case 
   when stc.VendorBillId > 0 then {2}
   when vdd.Id IS NOT NULL then {3}
   else {4}
 end as Status,
stc.Id, stc.TenantId, stc.ServiceTicketId as ParentId, stc.Quantity, stc.UnitBuy, stc.UnitSell, stc.UnitDiscount, stc.Comment, stc.ChargeCodeId, stc.VendorBillId
 --  stc.* 
FROM ServiceTicketCharge stc 
INNER JOIN ServiceTicket st ON st.Id = stc.ServiceTicketId
LEFT JOIN VendorDisputeDetail vdd ON vdd.ChargeLineId = stc.Id
INNER JOIN ServiceTicketVendor stv ON st.Id = stv.ServiceTicketId AND (stc.VendorId = stv.VendorId OR (stc.VendorId = 0 AND stv.[Primary] = 1))
LEFT JOIN Vendor itemVendor ON itemVendor.Id = stv.VendorId
LEFT JOIN Vendor chargeVendor ON chargeVendor.Id = stc.VendorId
LEFT JOIN VendorLocation itemVendorLocation ON itemVendor.Id = itemVendorLocation.VendorId AND itemVendorLocation.[Primary] = 1
LEFT JOIN VendorLocation chargeVendorLocation ON chargeVendor.Id = chargeVendorLocation.VendorId AND chargeVendorLocation.[Primary] = 1
WHERE st.ServiceTicketNumber = @Number
) tableName 
WHERE 
ChargeType = @ChargeType 
", ChargeType.Shipment.ToInt(), ChargeType.ServiceTicket.ToInt(), ChargeStatus.Billed.ToInt(), ChargeStatus.Disputed.ToInt(), ChargeStatus.Open.ToInt());
            var parameter = new Dictionary<string, object> {
                { "Number", number },
                { "ChargeType", chargeType.ToInt() }
            };
            
            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    charges.Add(new ChargeViewSearchDto(reader));
            Connection.Close();

            return charges;
        }

        public ChargeViewSearchDto FetchChargesByTypeAndId(string id, string chargeNumber, ChargeType chargeType)
        {
            return FetchChargesByTypeAndNumber(chargeNumber, chargeType).FirstOrDefault(i => i.Id == id.ToInt());
        }
    }
}
