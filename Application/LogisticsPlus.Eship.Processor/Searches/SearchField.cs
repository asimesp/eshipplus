﻿using System;
using System.Data;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.Processor.Searches
{
	[Serializable]
	public class SearchField
	{
		private string _displayName = string.Empty;

		public string Name { get; set; }
		public string DisplayName
		{
			get { return string.IsNullOrEmpty(_displayName) ? Name.FormattedString() : _displayName; }
			set { _displayName = value; }
		}
		public SqlDbType DataType { get; set; }
	}
}
