﻿using System.Collections.Generic;
using ObjToSql.Core;
using LogisticsPlus.Eship.Core.Cms;

namespace LogisticsPlus.Eship.Processor.Searches.Cms
{
	public class NewsSearch : EntityBase
	{
		public List<News> FetchNonHiddenNews()
		{
			const string query = "SELECT * FROM CmsNews WHERE Hidden = @Hidden";
			var news = new List<News>();
			var parameters = new Dictionary<string, object> { { "Hidden", 0 } };

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					news.Add(new News(reader));
			Connection.Close();

			return news;
		}

		public News FetchNewsByNewsKey(string newsKey)
		{
			const string query = "SELECT * FROM CmsNews WHERE NewsKey = @NewsKey";
			var parameters = new Dictionary<string, object> { { "NewsKey", newsKey } };
			News news = null;
			using (var reader = GetReader(query,parameters))
				if (reader.Read())
					news = new News(reader);
			Connection.Close();

			return news;
		}

        public List<News> FetchAllNews()
        {
            const string query = "SELECT * FROM CmsNews";
            var news = new List<News>();

            using (var reader = GetReader(query, null))
                while (reader.Read())
                    news.Add(new News(reader));
            Connection.Close();

            return news;
        }
	}
}
