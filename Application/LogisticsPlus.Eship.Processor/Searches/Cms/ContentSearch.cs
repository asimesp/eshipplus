﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Cms;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Cms
{
	public class ContentSearch : EntityBase
	{
		public List<Content> FetchAllContent()
		{
			const string query = "SELECT * FROM CmsContent";
			var contents = new List<Content>();

			using (var reader = GetReader(query, null))
				while (reader.Read())
					contents.Add(new Content(reader));
			Connection.Close();

			return contents;
		}

		public Content FetchContentByContentKey(string contentKey)
		{
			const string query = "SELECT * FROM CmsContent WHERE ContentKey = @ContentKey";
			var parameters = new Dictionary<string, object> { { "ContentKey", contentKey } };
			Content content = null;

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					content = new Content(reader);
			Connection.Close();
			return content;
		}

		public List<Content> FetchContentByIdentifierCode(string identifierCode)
		{
			const string query = "SELECT * FROM CmsContent WHERE IdentifierCode = @IdentifierCode";
			var parameters = new Dictionary<string, object> { { "IdentifierCode", identifierCode } };
			var contents = new List<Content>();

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					contents.Add(new Content(reader));
			Connection.Close();

			return contents;
		}

		public Content FetchCurrentContentByIdentifierCode(string identifierCode)
		{
			const string query = "SELECT * FROM CmsContent WHERE IdentifierCode = @IdentifierCode AND [Current] = @Current";
			var parameters = new Dictionary<string, object> {{"IdentifierCode", identifierCode}, {"Current", 1}};
			Content content = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					content = new Content(reader);
			Connection.Close();
			return content;
		}
	}
}
