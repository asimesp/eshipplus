﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class SMC3TrackRequestResponseSearch : EntityBase
	{
        public SMC3TrackRequestResponse FetchTrackRequestResponseByTransactionId(string transactionid)
        {
            SMC3TrackRequestResponse res = null;

			const string query = @"SELECT * FROM SMC3TrackRequestResponse WHERE TransactionId = @transactionid";
            var parameters = new Dictionary<string, object> { { "transactionid", transactionid } };

            using (var reader = GetReader(query, parameters))
	            if (reader.Read())
		            res = new SMC3TrackRequestResponse(reader);
            Connection.Close();
            return res;
        }

        public List<SMC3TrackRequestResponse> FetchTrackRequestResponseByShipmentId(long shipmentId, string scac = "")
        {
	        var query = @"SELECT * FROM SMC3TrackRequestResponse WHERE ShipmentId = @ShipmentId";
	        var parameters = new Dictionary<string, object> { { "ShipmentId", shipmentId } };

	        if (!string.IsNullOrEmpty(scac))
            {
	            query += " AND Scac = @scac";
	            parameters.Add("scac", scac);
            }

	        var res = new List<SMC3TrackRequestResponse>();
	        using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    res.Add(new SMC3TrackRequestResponse(reader));
            Connection.Close();
            return res;
        }
	}
}
