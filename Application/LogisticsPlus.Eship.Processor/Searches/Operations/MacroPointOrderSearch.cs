﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
    public class MacroPointOrderSearch : EntityBase
    {
        public List<MacroPointOrderViewSearchDto> FetchMacroPointOrders(MacroPointOrderSearchCriteria criteria, long tenantId)
        {
            var orders = new List<MacroPointOrderViewSearchDto>();

            var query = @"SELECT * 
                            FROM MacroPointOrderViewSearch 
                            Inner Join (Select ShipmentNumber, shlo.City 'OriginCity', shlo.[State] 'OriginState', shlo.PostalCode 'OriginPostalCode', shld.City'DestinationCity', 
					            shld.[State] 'DestinationState', shld.PostalCode 'DestinationPostalCode' from Shipment s 
				                    INNER JOIN ShipmentLocation shlo ON OriginId = shlo.Id
				                    INNER JOIN ShipmentLocation shld ON DestinationId = shld.Id  ) as  sh on sh.ShipmentNumber = IdNumber
                WHERE TenantId = @TenantId AND";

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };
            query += criteria.Parameters.BuildWhereClause(parameters, ConnectSearchFields.MacroPoint);

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    orders.Add(new MacroPointOrderViewSearchDto(reader));
            Connection.Close();

            return orders;
        }

        public MacroPointOrderViewSearchDto FetchMacroPointOrderByOrderId(string orderId, long tenantId)
        {
            const string query = @"SELECT * FROM MacroPointOrderViewSearch WHERE OrderId = @OrderId AND TenantId = @TenantId ";

            var parameters = new Dictionary<string, object>
                {
                    { "TenantId", tenantId }, 
                    { "OrderId", orderId }
                };

            MacroPointOrderViewSearchDto order = null;
            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    order = new MacroPointOrderViewSearchDto(reader);
            Connection.Close();

            return order;
        }

        public MacroPointOrderViewSearchDto FetchMacroPointOrderByIdNumber(string idNumber, long tenantId)
        {
            const string query = @"SELECT * FROM MacroPointOrderViewSearch WHERE IdNumber = @IdNumber AND TenantId = @TenantId ";

            var parameters = new Dictionary<string, object>
                {
                    { "TenantId", tenantId }, 
                    { "IdNumber", idNumber }
                };

            MacroPointOrderViewSearchDto order = null;
            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    order = new MacroPointOrderViewSearchDto(reader);
            Connection.Close();

            return order;
        }

		public List<long> FetchTenantIdsByIdNumAndOrderId(string orderId, string idNumber)
		{
			const string query = @"SELECT TenantId FROM MacroPointOrderViewSearch WHERE IdNumber = @IdNumber AND OrderId = @OrderId ";

			var parameters = new Dictionary<string, object>
                {
                    { "OrderId", orderId }, 
                    { "IdNumber", idNumber }
                };

			var ids = new List<long>();
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					ids.Add(reader.Value("TenantId").ToLong());
			Connection.Close();

			return ids;
		}

		public bool MacroPointOrderForIdNumberExists(string idNumber, long tenantId)
		{
			const string query = @"SELECT count(*) FROM MacroPointOrderViewSearch WHERE IdNumber = @IdNumber AND TenantId = @TenantId ";

			var parameters = new Dictionary<string, object>
                {
                    { "TenantId", tenantId }, 
                    { "IdNumber", idNumber }
                };

			return ExecuteScalar(query, parameters).ToInt() > 0;
		}
    }
}
