﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
    public class LibraryItemSearch : EntityBase
    {
        public List<LibraryItem> FetchLibraryItems(LibraryItemSearchCriteria criteria, long tenantId)
        {
            var libraryItems = new List<LibraryItem>();

            var query = @"SELECT * FROM LibraryItem WHERE TenantId = @TenantId AND CustomerId = @CustomerId AND ";

            var parameters = new Dictionary<string, object>
                                 {
                                     { "TenantId", tenantId }, 
                                     { "CustomerId", criteria.CustomerId}
                                 };

            query += criteria.Parameters.BuildWhereClause(parameters, OperationsSearchFields.LibraryItems);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    libraryItems.Add(new LibraryItem(reader));
            Connection.Close();

            return libraryItems;
        }

        public List<LibraryItemViewSearchDto> FetchLibraryItemDtos(LibraryItemSearchCriteria criteria, long tenantId)
        {
            var libraryItemDtos = new List<LibraryItemViewSearchDto>();

            var query = @"SELECT * FROM dbo.[UserVisibleCustomerLibraryItems2](@UserId, @CustomerId, @TenantId) WHERE ";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"UserId", criteria.ActiveUserId},
			                 		{"CustomerId", criteria.CustomerId},
			                 		{"TenantId", tenantId}
			                 	};

            query += criteria.Parameters.BuildWhereClause(parameters, OperationsSearchFields.LibraryItemsFinder);

            query += string.Format("ORDER BY CustomerNumber {0}, [Description] {0}", SearchUtilities.Ascending);
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    libraryItemDtos.Add(new LibraryItemViewSearchDto(reader));
            Connection.Close();

            return libraryItemDtos;
        }
    }
}
