﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class LoadOrderSearch : EntityBase
	{

		public List<LoadOrder> FetchAvailableLoads(long tenantId)
		{
			var availableLoads = new List<LoadOrder>();

			const string query = @"SELECT * FROM AvailableLoadsViewSearch(@TenantId)";

			var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					availableLoads.Add(new LoadOrder(reader));
			Connection.Close();

			return availableLoads;
		}

        public List<LoadOrder> FetchExpiredLoads()
        {
            var expiredLoads = new List<LoadOrder>();

            // fetch all applicable load orders where the current date is greater than the desired pickup date
            const string query = @"SELECT l.* 
	                                FROM LoadOrder l 
	                                INNER JOIN Customer c ON c.Id = l.CustomerId
	                                WHERE @CurrentDate > l.DesiredPickupDate 
                                        AND c.PurgeExpiredQuotes = @PurgeQuotes 
                                        AND l.Status != @Status";

            var parameters = new Dictionary<string, object>
                {
                    {"CurrentDate", DateTime.Now.Date}, 
                    {"PurgeQuotes", 1},
                    {"Status", LoadOrderStatus.Shipment},
                };


            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    expiredLoads.Add(new LoadOrder(reader));
            Connection.Close();

            return expiredLoads;
        }

        public LoadOrder FetchLoadOrderByLoadNumber(string number, long tenantId)
		{
            LoadOrder loadOrder = null;

            const string query = @"SELECT * FROM LoadOrder WHERE LoadOrderNumber = @LoadOrderNumber AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "LoadOrderNumber", number }, { "TenantId", tenantId }, };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
                    loadOrder = new LoadOrder(reader);
			Connection.Close();
			return loadOrder;
		}

		public LoadOrder FetchLoadOrderByShipperBol(string shipperBol, long tenantId)
		{
			LoadOrder loadOrder = null;

			const string query = @"SELECT * FROM LoadOrder WHERE ShipperBol = @ShipperBol AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "ShipperBol", shipperBol }, { "TenantId", tenantId }, };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					loadOrder = new LoadOrder(reader);
			Connection.Close();
			return loadOrder;
		}

		public List<ShipmentDashboardDto> FetchLoadsDashboardDto(ShipmentViewSearchCriteria criteria, long tenantId)
		{
			var loadOrders = new List<ShipmentDashboardDto>();

			var query = @"SELECT TOP 1000 * FROM LoadOrderViewSearch(@TenantId, @ActiveUserId) WHERE ";
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };

			var refernceColumns = new List<ParameterColumn>
				{
					OperationsSearchFields.ShipmentReferenceName.ToParameterColumn(),
					OperationsSearchFields.ShipmentReferenceValue.ToParameterColumn()
				};

			query += criteria
				.Parameters
				.Where(p => refernceColumns.All(rc => rc.ReportColumnName != p.ReportColumnName))
				.ToList()
				.BuildWhereClause(parameters, OperationsSearchFields.LoadOrders);


			if (criteria.Parameters.Any(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName)))
			{
				var rquery = @"Select l.LoadOrderNumber 'ShipmentNumber' from LoadOrder l Inner Join LoadOrderReference r on r.LoadOrderId = l.Id where ";

				var rparams = new Dictionary<string, object>();
				rquery += criteria
					.Parameters
					.Where(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName))
					.ToList()
					.BuildWhereClause(rparams, OperationsSearchFields.LoadOrders);

				foreach (var item in rparams)
				{
					var newKey = string.Format("r_{0}", item.Key);
					rquery = rquery.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}

				query += string.Format(" AND ShipmentNumber IN ({0})", rquery);
			}

			if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);
			

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					loadOrders.Add(new ShipmentDashboardDto(reader, DashboardDtoType.LoadOrder));
			Connection.Close();
			return loadOrders;
		}

        public List<ShipmentDashboardDto> FetchLoadsDashboardDtoExludeAttachedToJob(ShipmentViewSearchCriteria criteria, long tenantId)
        {
            var loadOrders = new List<ShipmentDashboardDto>();

            var query = @"SELECT TOP 1000 * FROM LoadOrderViewSearch(@TenantId, @ActiveUserId) WHERE JobId = '0' AND  ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };
            
            query += criteria.Parameters.BuildWhereClause(parameters, OperationsSearchFields.LoadOrders);
            query += SearchUtilities.AddQuerySorting(criteria.SortBy, criteria.SortAscending);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    loadOrders.Add(new ShipmentDashboardDto(reader, DashboardDtoType.LoadOrder));
            Connection.Close();
            return loadOrders;
        }
    }
}
