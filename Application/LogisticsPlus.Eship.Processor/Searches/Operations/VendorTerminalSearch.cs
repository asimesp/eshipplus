﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
    public class VendorTerminalSearch : EntityBase
    {
        public List<VendorTerminal> FetchVendorTerminals(VendorTerminalSearchCriteria criteria, long tenantId)
        {
            var terminals = new List<VendorTerminal>();

            var query = @"SELECT 
                            * 
                       FROM 
                            VendorTerminalViewSearch 
                       WHERE
                            TenantId = @TenantId AND ";

	        var parameters = new Dictionary<string, object>
		        {
			        {"TenantId", tenantId}
		        };

            if (criteria.VendorId != default(long))
            {
                query += "VendorId = @VendorId AND ";
                parameters.Add("VendorId", criteria.VendorId);
            }

            query += criteria.Parameters.BuildWhereClause(parameters, OperationsSearchFields.VendorTerminals);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    terminals.Add(new VendorTerminal(reader));
            Connection.Close();
            return terminals;
        }
    }
}
