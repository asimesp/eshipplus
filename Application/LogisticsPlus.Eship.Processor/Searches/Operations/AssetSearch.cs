﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class AssetSearch : EntityBase
	{
	    public List<Asset> FetchAssets(List<ParameterColumn> columns, long tenantId)
        {
            var assets = new List<Asset>();
            var query = @"SELECT * FROM AssetViewSearch WHERE TenantId = @TenantId AND ";
            var parameter = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameter, OperationsSearchFields.Assets);

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    assets.Add(new Asset(reader));
            Connection.Close();

            return assets;
        }

		public Asset FetchAssetByAssetNumber(string assetNumber, long tenantId)
        {
            Asset asset = null;
            const string query = @"SELECT * FROM Asset WHERE AssetNumber = @AssetNumber AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "AssetNumber", assetNumber }, { "TenantId", tenantId }, };

            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    asset = new Asset(reader);
            Connection.Close();
            return asset;
        }
	}
}
