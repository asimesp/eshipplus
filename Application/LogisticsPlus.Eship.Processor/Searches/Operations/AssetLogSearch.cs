﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
    public class AssetLogSearch : EntityBase
    {
        public List<AssetLogDto> FetchAssetLogs(long assetId, long tenantId)
        {
            var logs = new List<AssetLogDto>();

            var query =
                string.Format(@"SELECT TOP 1000 * FROM AssetLogViewSearch 
                                WHERE AssetId = @AssetId AND TenantId = @TenantId ORDER BY LogDate {0}, DateCreated {0}", SearchUtilities.Descending);

            var parameters = new Dictionary<string, object>
                                 {
                                     {"AssetId", assetId},
                                     {"TenantId", tenantId},
                                 };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    logs.Add(new AssetLogDto(reader));
            Connection.Close();
            return logs;
        }

		public AssetLogDto FetchPreviousLog(long assetLogId, long assetId, long tenantId)
		{
			var query =
				string.Format(@"select top 1 * from AssetLogViewSearch 
                                where Id < @AssetLogId AND AssetId = @AssetId AND TenantId = @TenantId  order by Id {0}", SearchUtilities.Descending);
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"AssetLogId", assetLogId},
			                 		{"TenantId", tenantId},
			                 		{"AssetId", assetId},
			                 	};
			AssetLogDto log = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					log = new AssetLogDto(reader);
			Connection.Close();
			return log;
		}

		public AssetLogDto FetchLastLogEntry(long assetId, long tenantId)
		{
            var query = string.Format(@"select top 1 * from AssetLogViewSearch where AssetId = @AssetId AND TenantId = @TenantId  order by Id {0}", SearchUtilities.Descending);
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"AssetId", assetId},
			                 		{"TenantId", tenantId},
			                 	};
			AssetLogDto log = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					log = new AssetLogDto(reader);
			Connection.Close();
			return log;
		}
    }
}
