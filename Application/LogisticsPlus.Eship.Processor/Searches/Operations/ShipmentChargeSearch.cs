﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class ShipmentChargeSearch : EntityBase
	{
		public bool HasUnBilledCharges(long shipmentId)
		{
			var shipmentCharges = new List<ShipmentCharge>();
			const string query = @"SELECT * FROM ShipmentCharge where ShipmentId = @Id and VendorBillId = 0";
			var parameters = new Dictionary<string, object> { { "Id", shipmentId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					shipmentCharges.Add(new ShipmentCharge(reader));
			Connection.Close();
			return shipmentCharges.Count > 0;

		}
	}
}
