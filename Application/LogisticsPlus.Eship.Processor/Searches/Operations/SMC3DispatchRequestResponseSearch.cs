﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class SMC3DispatchRequestResponseSearch : EntityBase
	{
		public SMC3DispatchRequestResponse FetchDispatchRequestResponseByTransactionId(string transactionid)
		{
			SMC3DispatchRequestResponse res = null;

			const string query = @"SELECT * FROM SMC3DispatchRequestResponse WHERE TransactionId = @transactionid";
			var parameters = new Dictionary<string, object> { { "transactionid", transactionid } };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					res = new SMC3DispatchRequestResponse(reader);
			Connection.Close();
			return res;
		}

		public List<SMC3DispatchRequestResponse> FetchTrackRequestResponseByShipmentId(long shipmentId, string scac = "")
		{
			var query = @"SELECT * FROM SMC3DispatchRequestResponse WHERE ShipmentId = @ShipmentId";
			var parameters = new Dictionary<string, object> { { "ShipmentId", shipmentId } };

			if (!string.IsNullOrEmpty(scac))
			{
				query += " AND Scac = @scac";
				parameters.Add("scac", scac);
			}

			var res = new List<SMC3DispatchRequestResponse>();
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					res.Add(new SMC3DispatchRequestResponse(reader));
			Connection.Close();
			return res;
		}
	}
}
