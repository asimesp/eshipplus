﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class JobSearch : EntityBase
	{
		public Job FetchJobByJobNumber(string jobNumber, long tenantId)
		{
            Job job = null;

            const string query = @"SELECT * FROM Job WHERE JobNumber = @JobNumber AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "JobNumber", jobNumber }, { "TenantId", tenantId }, };

            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    job = new Job(reader);
            Connection.Close();

            return job;
        }

        public List<JobDashboardDto> FetchJobDashboardDtos(JobViewSearchCriteria criteria, long tenantId)
        {
            var jobDashboard = new List<JobDashboardDto>();

            // these columns will be handled by subqueries and excluded from the call to BuildWhereClause for criteria
            var subQueryReportColumns = new List<SearchField>
                {
                    OperationsSearchFields.InvoiceNumber,
                    OperationsSearchFields.VendorBillNumber,
                    OperationsSearchFields.JobShipmentNumber,
                    OperationsSearchFields.JobServiceTicketNumber,
                    OperationsSearchFields.JobLoadOrderNumber,
                    OperationsSearchFields.ContainerNumber,
                }.ToParameterColumn()
                 .Select(c => c.ReportColumnName)
                 .ToList();

            var containsInvoiceFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(OperationsSearchFields.InvoiceNumber.DisplayName);
            var containsVendorBillFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(OperationsSearchFields.VendorBillNumber.DisplayName);
            var containsShipmentFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(OperationsSearchFields.JobShipmentNumber.DisplayName);
            var containsServiceTicketFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(OperationsSearchFields.JobServiceTicketNumber.DisplayName);
            var containsLoadOrderFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(OperationsSearchFields.JobLoadOrderNumber.DisplayName);
            var containsContainerFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(OperationsSearchFields.ContainerNumber.DisplayName);

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };

            var query = @"SELECT Top 1000 jvs.* FROM JobViewSearch(@TenantId, @ActiveUserId) jvs WHERE ";


            // begin building subqueries to filter where applicable
            if (containsInvoiceFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == OperationsSearchFields.InvoiceNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, OperationsSearchFields.Jobs);
               
                var invoiceQuery = string.Format(@"(SELECT job.Id FROM Job job 
                                                          INNER JOIN Shipment s on s.JobId = job.Id
                                                          INNER JOIN InvoiceDetail id ON id.ReferenceNumber = s.ShipmentNumber AND id.ReferenceType = {0}
                                                          INNER JOIN Invoice i ON id.InvoiceId = i.Id WHERE {1})
                                            UNION
                                            (SELECT job.Id FROM Job job 
                                                          INNER JOIN ServiceTicket st on st.JobId = job.Id
                                                          INNER JOIN InvoiceDetail id ON id.ReferenceNumber = st.ServiceTicketNumber AND id.ReferenceType = {2}
                                                          INNER JOIN Invoice i ON id.InvoiceId = i.Id WHERE {1}) ",
                                       DetailReferenceType.Shipment.ToInt(),
                                       filter,
                                       DetailReferenceType.ServiceTicket.ToInt());

                foreach (var item in uParams)
                {
                    var newKey = string.Format("i_{0}", item.Key);
                    invoiceQuery = invoiceQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format("(jvs.Id IN ({0})) AND ", invoiceQuery);
            }

            if (containsVendorBillFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == OperationsSearchFields.VendorBillNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, OperationsSearchFields.Jobs);

                var billQuery = string.Format(@"(SELECT job.Id FROM Job job 
                                                          INNER JOIN Shipment s on s.JobId = job.Id
                                                          INNER JOIN VendorBillDetail vbd ON vbd.ReferenceNumber = s.ShipmentNumber AND vbd.ReferenceType = {0}
                                                          INNER JOIN VendorBill vb ON vbd.VendorBillId = vb.Id WHERE {1})
                                            UNION
                                            (SELECT job.Id FROM Job job 
                                                          INNER JOIN ServiceTicket st on st.JobId = job.Id
                                                          INNER JOIN VendorBillDetail vbd ON vbd.ReferenceNumber = st.ServiceTicketNumber AND vbd.ReferenceType = {2}
                                                          INNER JOIN VendorBill vb ON vbd.VendorBillId = vb.Id WHERE {1}) ",
                                       DetailReferenceType.Shipment.ToInt(),
                                       filter,
                                       DetailReferenceType.ServiceTicket.ToInt());

                foreach (var item in uParams)
                {
                    var newKey = string.Format("vb_{0}", item.Key);
                    billQuery = billQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format("(jvs.Id IN ({0})) AND ", billQuery);
            }

            if (containsShipmentFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == OperationsSearchFields.JobShipmentNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, OperationsSearchFields.Jobs);

                var shipmentQuery = string.Format(@"SELECT job.Id FROM Job job INNER JOIN Shipment s on s.JobId = job.Id WHERE {0} ", filter);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("sh_{0}", item.Key);
                    shipmentQuery = shipmentQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format("(jvs.Id IN ({0})) AND ", shipmentQuery);
            }

            if (containsLoadOrderFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == OperationsSearchFields.JobLoadOrderNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, OperationsSearchFields.Jobs);

                var loadOrderQuery = string.Format(@"SELECT job.Id FROM Job job INNER JOIN LoadOrder lo on lo.JobId = job.Id WHERE {0} ", filter);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("lo_{0}", item.Key);
                    loadOrderQuery = loadOrderQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format("(jvs.Id IN ({0})) AND ", loadOrderQuery);
            }

            if (containsServiceTicketFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == OperationsSearchFields.JobServiceTicketNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, OperationsSearchFields.Jobs);

                var serviceTicketQuery = string.Format(@"SELECT job.Id FROM Job job INNER JOIN ServiceTicket st on st.JobId = job.Id WHERE {0} ", filter);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("st_{0}", item.Key);
                    serviceTicketQuery = serviceTicketQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format("(jvs.Id IN ({0})) AND ", serviceTicketQuery);
            }

            if (containsContainerFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == OperationsSearchFields.ContainerNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, OperationsSearchFields.Jobs);

                var serviceTicketContainerQuery = string.Format(@"SELECT job.Id FROM Job job 
                                                            INNER JOIN ServiceTicket st ON st.JobId = job.Id
                                                            INNER JOIN ServiceTicketItem sti ON sti.ServiceTicketId = st.Id 
                                                            WHERE {0}", filter);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("stc_{0}", item.Key);
                    serviceTicketContainerQuery = serviceTicketContainerQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format("(jvs.Id IN ({0})) AND ", serviceTicketContainerQuery);
            }


            query += criteria.Parameters.Where(p => !subQueryReportColumns.Contains(p.ReportColumnName)).ToList().BuildWhereClause(parameters, OperationsSearchFields.Jobs);
            query += SearchUtilities.AddQuerySorting(criteria.SortBy, criteria.SortAscending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    jobDashboard.Add(new JobDashboardDto(reader));
            Connection.Close();

            return jobDashboard;
        }

		public List<JobComponentDto> FetchJobComponents(long jobId, long tenantId)
		{
			var jobComponents = new List<JobComponentDto>();

		    const string query = @"SELECT
									  sh.Id as 'ComponentId', 
									  1 as 'ComponentType',
									  sh.ShipmentNumber as 'ComponentNumber',
									  sh.JobStep as 'ComponentJobStep',
                                      ss.ShipmentStatusText as 'ComponentStatus'
								FROM Shipment sh
                                LEFT JOIN RefShipmentStatus ss on sh.[Status] = ss.ShipmentStatusIndex
								WHERE sh.JobId = @JobId AND sh.TenantId = @TenantId
                                

								UNION ALL

								SELECT
									  st.Id as 'ComponentId', 
									  2 as 'ComponentType',
									  st.ServiceTicketNumber as 'ComponentNumber',
									  st.JobStep as 'ComponentJobStep',
                                      ss.ServiceTicketStatusText as 'ComponentStatus'
								FROM ServiceTicket st
                                LEFT JOIN RefServiceTicketStatus ss on st.[Status] = ss.ServiceTicketStatusIndex
								WHERE st.JobId = @JobId AND st.TenantId = @TenantId

								UNION ALL

								SELECT
									  lo.Id as 'ComponentId', 
									  0 as 'ComponentType',
									  lo.LoadOrderNumber as 'ComponentNumber',
									  lo.JobStep as 'ComponentJobStep',
                                      ss.LoadOrderStatusText as 'ComponentStatus'
								FROM LoadOrder lo
                                LEFT join RefLoadOrderStatus ss on lo.[Status] = ss.LoadOrderStatusIndex
                                WHERE lo.JobId = @JobId AND lo.TenantId = @TenantId;";

			var parameters = new Dictionary<string, object> { { "JobId", jobId }, { "TenantId", tenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					jobComponents.Add(new JobComponentDto(reader));
			Connection.Close();

			return jobComponents;
		}

        public List<LoadOrder> RelatedLoadOrders(List<string> shipmentsNumber, long tenantId)
        {
	        if (!shipmentsNumber.Any()) return new List<LoadOrder>();

            const string queryLoadOrder = @"SELECT * FROM LoadOrder WHERE TenantId = @TenantId AND LoadOrderNumber IN ({0});";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };
            var numbers = string.Join(",", shipmentsNumber.Select(s => string.Format("'{0}'", s)));
			var relatedLoadOrders = new List<LoadOrder>();
            using (var reader = GetReader(string.Format(queryLoadOrder, numbers), parameters))
                while (reader.Read())
                    relatedLoadOrders.Add(new LoadOrder(reader));

            Connection.Close();

            return relatedLoadOrders;
        }

        public List<AuditVendorBillDto> FetchAuditVendorBillDtosForJob(long tenantId, long jobId)
        {
            var vendorBills = new List<AuditVendorBillDto>();

            const string query =
				@"SELECT 
					vb.Id 'VendorBillId',
					vb.DocumentDate,
					vb.DocumentNumber, 
					vb.Posted,
					vb.PostDate,
					vb.BillType,
					v.Name as 'VendorName',
					v.VendorNumber,
					v.Id as 'VendorId',
					vl.LocationNumber,
					d.Quantity, 
					d.UnitBuy, 
					d.ReferenceNumber, 
					d.ReferenceType,
					cc.Code 'ChargeCode',
					cc.[Description] 'ChargeCodeDescription',
					ab.Code 'AccountBucketCode',
					ab.[Description] 'AccountBucketDescription'
				FROM VendorBill vb
					INNER JOIN VendorBillDetail d ON d.VendorBillId = vb.Id
					INNER JOIN VendorLocation vl ON vl.Id = vb.VendorLocationId
					INNER JOIN Vendor v ON v.Id = vl.VendorId
					INNER JOIN ChargeCode cc ON cc.Id = d.ChargeCodeId
					INNER JOIN AccountBucket ab ON ab.Id = d.AccountBucketId                   
					INNER JOIN ServiceTicket st ON st.ServiceTicketNumber = d.ReferenceNumber AND d.ReferenceType = 1
				WHERE
					 vb.TenantId = @TenantId
                     AND st.JobId = @JobId
		UNION ALL

		SELECT 
					vb.Id 'VendorBillId',
					vb.DocumentDate,
					vb.DocumentNumber, 
					vb.Posted,
					vb.PostDate,
					vb.BillType,
					v.Name as 'VendorName',
					v.VendorNumber,
					v.Id as 'VendorId',
					vl.LocationNumber,
					d.Quantity, 
					d.UnitBuy, 
					d.ReferenceNumber, 
					d.ReferenceType,
					cc.Code 'ChargeCode',
					cc.[Description] 'ChargeCodeDescription',
					ab.Code 'AccountBucketCode',
					ab.[Description] 'AccountBucketDescription'
				FROM VendorBill vb
					INNER JOIN VendorBillDetail d ON d.VendorBillId = vb.Id
					INNER JOIN VendorLocation vl ON vl.Id = vb.VendorLocationId
					INNER JOIN Vendor v ON v.Id = vl.VendorId
					INNER JOIN ChargeCode cc ON cc.Id = d.ChargeCodeId
					INNER JOIN AccountBucket ab ON ab.Id = d.AccountBucketId
                    INNER JOIN Shipment s ON s.ShipmentNumber = d.ReferenceNumber AND d.ReferenceType = 0
				WHERE
					 vb.TenantId = @TenantId
                     AND s.JobId = @JobId";

            var parameters = new Dictionary<string, object>
			                 	{
				                 	{"TenantId", tenantId},
				                 	{"JobId", jobId},
			                 	};

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    vendorBills.Add(new AuditVendorBillDto(reader));
            Connection.Close();
            return vendorBills;
        }

        public List<AuditInvoiceDto> FetchAuditInvoiceDtosForJob(long tenantId, long jobId)
        {
            var invoices = new List<AuditInvoiceDto>();

            const string query = 
                @"SELECT 
					i.Id 'InvoiceId',
					i.InvoiceDate,
					i.InvoiceNumber, 
					i.Posted,
					i.PostDate,
					i.DueDate,
					i.PaidAmount,
					i.InvoiceType,
					c.Name as 'CustomerName',
					c.CustomerNumber,
					c.Id as 'CustomerId',
					d.Quantity, 
					d.UnitSell, 
					d.UnitDiscount, 
					d.ReferenceNumber, 
					d.ReferenceType,
					cc.Code 'ChargeCode',
					cc.[Description] 'ChargeCodeDescription',
					ab.Code 'AccountBucketCode',
					ab.[Description] 'AccountBucketDescription'
				FROM Invoice i
					INNER JOIN InvoiceDetail d ON d.InvoiceId = i.Id
					INNER JOIN CustomerLocation cl ON cl.Id = i.CustomerLocationId
					INNER JOIN Customer c ON c.Id = cl.CustomerId
					INNER JOIN ChargeCode cc ON cc.Id = d.ChargeCodeId
					INNER JOIN AccountBucket ab ON ab.Id = d.AccountBucketId
                    INNER JOIN Shipment s ON s.ShipmentNumber = d.ReferenceNumber AND d.ReferenceType = 0
				WHERE
					 i.TenantId = @TenantId
                     AND s.JobId = @JobId
			UNION ALL
			SELECT 
					i.Id 'InvoiceId',
					i.InvoiceDate,
					i.InvoiceNumber, 
					i.Posted,
					i.PostDate,
					i.DueDate,
					i.PaidAmount,
					i.InvoiceType,
					c.Name as 'CustomerName',
					c.CustomerNumber,
					c.Id as 'CustomerId',
					d.Quantity, 
					d.UnitSell, 
					d.UnitDiscount, 
					d.ReferenceNumber, 
					d.ReferenceType,
					cc.Code 'ChargeCode',
					cc.[Description] 'ChargeCodeDescription',
					ab.Code 'AccountBucketCode',
					ab.[Description] 'AccountBucketDescription'
				FROM Invoice i
					INNER JOIN InvoiceDetail d ON d.InvoiceId = i.Id
					INNER JOIN CustomerLocation cl ON cl.Id = i.CustomerLocationId
					INNER JOIN Customer c ON c.Id = cl.CustomerId
					INNER JOIN ChargeCode cc ON cc.Id = d.ChargeCodeId
					INNER JOIN AccountBucket ab ON ab.Id = d.AccountBucketId
					INNER JOIN ServiceTicket st ON st.ServiceTicketNumber = d.ReferenceNumber AND d.ReferenceType = 1 
				WHERE
					 i.TenantId = @TenantId
                     AND St.JobId = @JobId";

            var parameters = new Dictionary<string, object>
			                 	{
				                 	{"TenantId", tenantId},
				                 	{"JobId", jobId}
			                 	};

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    invoices.Add(new AuditInvoiceDto(reader));
            Connection.Close();
            return invoices;
        }
    }
}