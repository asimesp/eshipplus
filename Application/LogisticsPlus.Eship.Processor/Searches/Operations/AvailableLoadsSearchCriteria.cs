﻿using System.Collections.Generic;
using System.Linq;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class AvailableLoadsSearchCriteria
	{
		public long ContactTypeId { get; set; }

		public string OriginCity { get; set; }
		public string OriginState { get; set; }
		public string OriginPostalCode { get; set; }
		public long OriginCountryId { get; set; }
		

		public string DestinationCity { get; set; }
		public string DestinationState { get; set; }
		public string DestinationPostalCode { get; set; }
		public long DestinationCountryId { get; set; }


		public bool CheckTerminals { get; set; }
		public bool IgnoreLanes { get; set; }

		public bool IncludeCarrier { get; set; }
		public bool IncludeBrokers { get; set; }
		public bool IncludeAgents { get; set; }

		public List<long> EquipmentTypeIds { get; set; }

		public bool CheckEquipment
		{
			get { return EquipmentTypeIds.Any(); }
		}
		public bool CheckOrigin
		{
			get
			{
				return !string.IsNullOrEmpty(OriginCity) || !string.IsNullOrEmpty(OriginState) ||
				       !string.IsNullOrEmpty(OriginPostalCode);
			}
		}
		public bool CheckDestination
		{
			get
			{
				return !string.IsNullOrEmpty(DestinationCity) || !string.IsNullOrEmpty(DestinationState) ||
				       !string.IsNullOrEmpty(DestinationPostalCode);
			}
		}
		public bool CheckVendorTypes
		{
			get { return IncludeAgents || IncludeBrokers || IncludeCarrier; }
		}


		public AvailableLoadsSearchCriteria()
		{
			ContactTypeId = default(long);
			EquipmentTypeIds = new List<long>();

			OriginCity = string.Empty;
			OriginPostalCode = string.Empty;
			OriginState = string.Empty;
			OriginCountryId = default(long);
			IgnoreLanes = false;

			DestinationCity = string.Empty;
			DestinationPostalCode = string.Empty;
			DestinationState = string.Empty;
			DestinationCountryId = default(long);
			CheckTerminals = false;
		}
	}
}
