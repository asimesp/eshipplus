﻿using System.Collections.Generic;
using System.Data;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
    public static class OperationsSearchFields
    {
        private static readonly object LockObj = new object();

        private static bool _isInitialized;

        public static SearchField AuditedForInvoicing;
        public static SearchField BidNumber;
        public static SearchField ClaimNumber;
        public static SearchField AcknowledgedDate;
        public static SearchField DateLpAcctToPayCarrierDate;
        public static SearchField CheckAmount;
        public static SearchField CheckNumber;
        public static SearchField DateCreated;
        public static SearchField Description;
        public static SearchField PurchaseOrderNumber;
        public static SearchField ServiceTicketNumber;
        public static SearchField JobNumber;
        public static SearchField InvoiceNumber;
        public static SearchField VendorBillNumber;
        public static SearchField JobShipmentNumber;
        public static SearchField JobServiceTicketNumber;
        public static SearchField JobLoadOrderNumber;
        public static SearchField ShipmentNumber;
        public static SearchField ShipperReference;
        public static SearchField VendorProNumber;
        public static SearchField CreatedByUsername;

        public static SearchField ActualDeliveryDate;
        public static SearchField ActualPickupDate;
        public static SearchField CustomerName;
        public static SearchField CustomerNumber;
        public static SearchField DesiredPickupDate;
        public static SearchField EstimatedDeliveryDate;
        public static SearchField ServiceMode;
        public static SearchField Status;

        public static SearchField Active;
        public static SearchField NonAliasedCountryName;
        public static SearchField NonAliasedCountryCode;
        public static SearchField NonAliasedDestinationCountryCode;
        public static SearchField NonAliasedDestinationCountryName;
        public static SearchField DestinationCity;
        public static SearchField DestinationPostalCode;
        public static SearchField HighestFreightClass;
        public static SearchField LowestTotalCharge;
        public static SearchField MostSignificantFreightClass;
        public static SearchField NumberLineItems;
        public static SearchField NonAliasedOriginCountryCode;
        public static SearchField NonAliasedOriginCountryName;
        public static SearchField OriginCity;
        public static SearchField OriginPostalCode;
        public static SearchField Quantity;
        public static SearchField ReferenceNumber;
        public static SearchField StatusText;
        public static SearchField TicketDate;
        public static SearchField TypeText;
        public static SearchField Username;
        public static SearchField VendorName;
        public static SearchField VendorNumber;
        public static SearchField Weight;
        public static SearchField FaxStatus;

        public static SearchField DatAssetIdNumber;
        public static SearchField DatLoadOrderNumber;
        public static SearchField DatExpirationDate;
        public static SearchField DatDateCreated;

		public static SearchField ExternalReference1;
		public static SearchField ExternalReference2;

        public static SearchField ServiceTicketDocuments;

	    public static SearchField ShipmentReferenceName;
	    public static SearchField ShipmentReferenceValue;

        public static SearchField ContainerNumber;

        // ----------------------------------------------

        public static List<SearchField> Default { get; set; }
        public static List<SearchField> DefaultAddressBooks { get; set; }
        public static List<SearchField> DefaultAssets { get; set; }
        public static List<SearchField> DefaultBids { get; set; }
        public static List<SearchField> DefaultClaims { get; set; }
        public static List<SearchField> DefaultCarrierPreferredLanes { get; set; }
        public static List<SearchField> DefaultDatAssets { get; set; }
        public static List<SearchField> DefaultServiceTickets { get; set; }
        public static List<SearchField> DefaultJobs { get; set; }
        public static List<SearchField> DefaultLibraryItems { get; set; }
        public static List<SearchField> DefaultShoppedRates { get; set; }
        public static List<SearchField> DefaultVendorTerminals { get; set; }
        public static List<SearchField> DefaultPendingLtlPickups { get; set; }

        // ----------------------------------------------

        public static List<SearchField> AddressBooks { get; set; }
        public static List<SearchField> Assets { get; set; }
        public static List<SearchField> Bids { get; set; }
        public static List<SearchField> CarrierPreferredLanes { get; set; }
        public static List<SearchField> Claims { get; set; }
        public static List<SearchField> CustomerLoads { get; set; }
        public static List<SearchField> DatLoadboardAssets { get; set; }
        public static List<SearchField> LibraryItems { get; set; }
        public static List<SearchField> LibraryItemsFinder { get; set; }
        public static List<SearchField> LoadOrders { get; set; }
        public static List<SearchField> PendingLtlPickups { get; set; }
        public static List<SearchField> ServiceTickets { get; set; }
        public static List<SearchField> Jobs { get; set; }
        public static List<SearchField> Shipments { get; set; }
        public static List<SearchField> ShipmentDashboard { get; set; }
        public static List<SearchField> ShoppedRates { get; set; }
        public static List<SearchField> VendorTerminals { get; set; }

        // ----------------------------------------------


        public static List<SearchField> ClaimsSortFields { get; set; }
        public static List<SearchField> PendingLtlPickupSortFields { get; set; }
        public static List<SearchField> ServiceTicketSortFields { get; set; }
        public static List<SearchField> JobSortFields { get; set; }
        public static List<SearchField> ShipmentSortFields { get; set; }
        public static List<SearchField> ShoppedRateSortFields { get; set; }


        // ----------------------------------------------

        

        // ----------------------------------------------

        public static void Initialize()
        {
            if (_isInitialized) return;

            var anotherLockInitialized = false;
            lock (LockObj)
            {
                if (!_isInitialized) _isInitialized = true;
                else anotherLockInitialized = true;
            }

            if (anotherLockInitialized) return;

            AuditedForInvoicing = new SearchField { Name = "AuditedForInvoicing", DataType = SqlDbType.Bit };
            BidNumber = new SearchField { Name = "BidNumber", DataType = SqlDbType.NVarChar };
            DateCreated = new SearchField { Name = "DateCreated", DataType = SqlDbType.DateTime };
            ClaimNumber = new SearchField { Name = "ClaimNumber", DataType = SqlDbType.NVarChar };
            CheckNumber = new SearchField { Name = "CheckNumber", DataType = SqlDbType.NVarChar };
            CheckAmount = new SearchField { Name = "CheckAmount", DataType = SqlDbType.NVarChar };
            DateLpAcctToPayCarrierDate = new SearchField { Name = "DateLpAcctToPayCarrier", DataType = SqlDbType.DateTime };
            AcknowledgedDate = new SearchField { Name = "Acknowledged", DataType = SqlDbType.DateTime };
            Description = new SearchField { Name = "Description", DataType = SqlDbType.NVarChar };
            PurchaseOrderNumber = new SearchField { Name = "PurchaseOrderNumber", DataType = SqlDbType.NVarChar };
            ServiceTicketNumber = new SearchField { Name = "ServiceTicketNumber", DataType = SqlDbType.NVarChar };
            JobNumber = new SearchField { Name = "JobNumber", DataType = SqlDbType.NVarChar };
            InvoiceNumber = new SearchField { Name = "InvoiceNumber", DataType = SqlDbType.NVarChar };
            VendorBillNumber = new SearchField { Name = "DocumentNumber", DisplayName = "Vendor Bill Number", DataType = SqlDbType.NVarChar };
            JobShipmentNumber = new SearchField { Name = "ShipmentNumber", DataType = SqlDbType.NVarChar };
            JobServiceTicketNumber = new SearchField { Name = "ServiceTicketNumber", DataType = SqlDbType.NVarChar };
            JobLoadOrderNumber = new SearchField { Name = "LoadOrderNumber", DataType = SqlDbType.NVarChar };
            ExternalReference1 = new SearchField { Name = "ExternalReference1", DataType = SqlDbType.NVarChar };
			ExternalReference2 = new SearchField { Name = "ExternalReference2", DataType = SqlDbType.NVarChar };
            ShipperReference = new SearchField { Name = "ShipperReference", DataType = SqlDbType.NVarChar };
            ShipmentNumber = new SearchField { Name = "ShipmentNumber", DataType = SqlDbType.NVarChar };
            VendorProNumber = new SearchField { Name = "VendorProNumber", DataType = SqlDbType.NVarChar };
            CreatedByUsername = new SearchField { Name = "CreatedByUsername", DisplayName = "User", DataType = SqlDbType.NVarChar };

            ActualDeliveryDate = new SearchField { Name = "ActualDeliveryDate", DataType = SqlDbType.DateTime };
            ActualPickupDate = new SearchField { Name = "ActualPickupDate", DataType = SqlDbType.DateTime };
            CustomerName = new SearchField { Name = "CustomerName", DataType = SqlDbType.NVarChar };
            CustomerNumber = new SearchField { Name = "CustomerNumber", DataType = SqlDbType.NVarChar };
            DesiredPickupDate = new SearchField { Name = "DesiredPickupDate", DataType = SqlDbType.DateTime };
            EstimatedDeliveryDate = new SearchField { Name = "EstimatedDeliveryDate", DataType = SqlDbType.DateTime };
            ServiceMode = new SearchField { Name = "ServiceModeText", DataType = SqlDbType.NVarChar };
            Status = new SearchField { Name = "StatusText", DataType = SqlDbType.NVarChar };

            Active = new SearchField { Name = "Active", DataType = SqlDbType.Bit };
            NonAliasedCountryCode = new SearchField { Name = "Code", DisplayName = "Country Code", DataType = SqlDbType.NVarChar };
            NonAliasedCountryName = new SearchField { Name = "Name", DisplayName = "Country Name", DataType = SqlDbType.NVarChar };
            NonAliasedDestinationCountryCode = new SearchField { Name = "Code", DisplayName = "Destination Country Code", DataType = SqlDbType.NVarChar };
            NonAliasedDestinationCountryName = new SearchField { Name = "Name", DisplayName = "Destination Country Name", DataType = SqlDbType.NVarChar };
            DestinationPostalCode = new SearchField { Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar };
            HighestFreightClass = new SearchField { Name = "HighestFreightClass", DataType = SqlDbType.Float };
            LowestTotalCharge = new SearchField { Name = "LowestTotalCharge", DataType = SqlDbType.Decimal };
            MostSignificantFreightClass = new SearchField { Name = "MostSignificantFreightClass", DataType = SqlDbType.Float };
            NumberLineItems = new SearchField { Name = "NumberLineItems", DataType = SqlDbType.Int };
            OriginPostalCode = new SearchField { Name = "OriginPostalCode", DataType = SqlDbType.NVarChar };
            NonAliasedOriginCountryCode = new SearchField { Name = "Code", DisplayName = "Origin Country Code", DataType = SqlDbType.NVarChar };
            NonAliasedOriginCountryName = new SearchField { Name = "Name", DisplayName = "Origin Country Name", DataType = SqlDbType.NVarChar };
            Quantity = new SearchField { Name = "Quantity", DataType = SqlDbType.Int };
            ReferenceNumber = new SearchField { Name = "ReferenceNumber", DataType = SqlDbType.NVarChar };
            StatusText = new SearchField { Name = "StatusText", DataType = SqlDbType.NVarChar };
            TicketDate = new SearchField { Name = "TicketDate", DataType = SqlDbType.DateTime };
            TypeText = new SearchField { Name = "TypeText", DataType = SqlDbType.NVarChar };
            Username = new SearchField { Name = "Username", DataType = SqlDbType.NVarChar };
            VendorName = new SearchField { Name = "VendorName", DataType = SqlDbType.NVarChar };
            VendorNumber = new SearchField { Name = "VendorNumber", DataType = SqlDbType.NVarChar };
            Weight = new SearchField { Name = "Weight", DataType = SqlDbType.Decimal };
            FaxStatus = new SearchField{ Name = "FaxStatus", DataType = SqlDbType.NVarChar };

            OriginCity = new SearchField { Name = "OriginCity", DataType = SqlDbType.NVarChar };
            DestinationCity = new SearchField { Name = "DestinationCity", DataType = SqlDbType.NVarChar };

			ShipmentReferenceName = new SearchField { Name = "Name", DisplayName = "Shipment Reference Name", DataType = SqlDbType.NVarChar };
			ShipmentReferenceValue = new SearchField { Name = "Value", DisplayName = "Shipment Reference Value", DataType = SqlDbType.NVarChar };

			ServiceTicketDocuments = new SearchField { Name = "Name", DisplayName = "Service Ticket Documents", DataType = SqlDbType.NVarChar };


            DatAssetIdNumber = new SearchField { Name = "AssetId", DisplayName = "Asset Id Number", DataType = SqlDbType.NVarChar };
            DatLoadOrderNumber = new SearchField { Name = "IdNumber", DisplayName = "Asset Load Order Number", DataType = SqlDbType.NVarChar };
            DatExpirationDate = new SearchField { Name = "ExpirationDate", DisplayName = "Asset Expiration Date", DataType = SqlDbType.DateTime };
            DatDateCreated = new SearchField { Name = "DateCreated", DisplayName = "Asset Date Created", DataType = SqlDbType.DateTime };

            ContainerNumber = new SearchField { Name = "Description", DisplayName = "Container Number", DataType = SqlDbType.NVarChar };

            // ----------------------------------------------

            Default = new List<SearchField> { ShipmentNumber, DateCreated, };
            DefaultAddressBooks = new List<SearchField>
                                      {
                                          Description,
                                          new SearchField {Name = "Street1", DataType = SqlDbType.NVarChar},
                                          new SearchField {Name = "City", DataType = SqlDbType.NVarChar},
                                          new SearchField {Name = "State", DataType = SqlDbType.NVarChar},
                                          new SearchField {Name = "PostalCode", DataType = SqlDbType.NVarChar}
                                      };
            DefaultAssets = new List<SearchField> { new SearchField { Name = "AssetNumber", DataType = SqlDbType.NVarChar }, Description };
            DefaultBids = new List<SearchField> { BidNumber, DateCreated, };
            DefaultCarrierPreferredLanes = new List<SearchField>
                                               {
                                                   new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
                                                   new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
                                                   new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
                                                   NonAliasedOriginCountryName,
                                                   new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
                                                   new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
                                                   new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
                                                   NonAliasedDestinationCountryName,
                                               };
            DefaultClaims = new List<SearchField> { ClaimNumber, DateCreated, };
            DefaultLibraryItems = new List<SearchField> { new SearchField { Name = "Description", DataType = SqlDbType.NVarChar }, new SearchField { Name = "Comment", DataType = SqlDbType.NVarChar } };
            DefaultPendingLtlPickups = new List<SearchField>{ DesiredPickupDate};
            DefaultShoppedRates = new List<SearchField> { DateCreated, new SearchField { Name = "ReferenceNumber", DataType = SqlDbType.NVarChar } };
            DefaultServiceTickets = new List<SearchField> { ServiceTicketNumber, DateCreated };
            DefaultJobs = new List<SearchField> { JobNumber, DateCreated };
            DefaultVendorTerminals = new List<SearchField>
                                         {
                                             new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},
                                             new SearchField {Name = "Street1", DataType = SqlDbType.NVarChar},
                                             new SearchField {Name = "Street2", DataType = SqlDbType.NVarChar},
                                             new SearchField {Name = "City", DataType = SqlDbType.NVarChar},
                                             new SearchField {Name = "State", DataType = SqlDbType.NVarChar},
                                             new SearchField {Name = "PostalCode", DataType = SqlDbType.NVarChar},
                                             VendorNumber,
                                             VendorName
                                         };

            DefaultDatAssets = new List<SearchField> { DatAssetIdNumber, DatLoadOrderNumber };

            // ---------------------------------------------------

            ClaimsSortFields = new List<SearchField>
                {
                    ClaimNumber,
                    DateCreated,
                    CustomerNumber,
                    CustomerName,
                    Username,
                    StatusText,
                    CheckAmount,
                    CheckNumber,
                    DateLpAcctToPayCarrierDate,
                    AcknowledgedDate,
                    new SearchField {Name = "ClaimDate", DataType = SqlDbType.DateTime},
                    new SearchField {Name = "IsRepairable", DataType = SqlDbType.Bit},
                    new SearchField {Name = "EstimatedRepairCost", DataType = SqlDbType.Decimal},
                    new SearchField {Name = "AmountClaimed", DataType = SqlDbType.Decimal},
                    new SearchField {Name = "AmountClaimedTypeText", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "ClaimTypeText", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "ClaimantReferenceNumber", DataType = SqlDbType.NVarChar},
                };
            
            DatLoadboardAssets = new List<SearchField>
                {
                    DatLoadOrderNumber,
                    DatAssetIdNumber,
                    DatExpirationDate,
                    DatDateCreated
                };

            ServiceTicketSortFields = new List<SearchField>
                                          {
                                              ServiceTicketNumber,
                                              DateCreated,
                                              TicketDate,
                                              CustomerNumber,
                                              CustomerName,
                                              Username,
                                              StatusText,
											  ExternalReference1,
											  ExternalReference2
                                          };
            
            JobSortFields = new List<SearchField>
                {
                    JobNumber,
                    DateCreated,
                    CustomerNumber,
                    CustomerName,
                    CreatedByUsername,
                    StatusText,
					ExternalReference1,
					ExternalReference2,
                };

            ShipmentSortFields = new List<SearchField>
        	                     	{
        	                     		ShipmentNumber,
        	                     		ActualPickupDate,
        	                     		DesiredPickupDate,
        	                     		ActualDeliveryDate,
        	                     		EstimatedDeliveryDate,
        	                     		new SearchField {Name = "Prefix", DataType = SqlDbType.NVarChar},
        	                     		DateCreated,
        	                     		new SearchField {Name = "OriginName", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginStreet1", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginStreet2", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginCountryName", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationName", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationStreet1", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationStreet2", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationCountryName", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "VendorName", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "VendorNumber", DataType = SqlDbType.NVarChar},
        	                     		CustomerNumber,
										CustomerName,
        	                     		ServiceMode,
        	                     		Status,
        	                     	};

            PendingLtlPickupSortFields = new List<SearchField>
                {
                    ShipmentNumber,
                    DesiredPickupDate,
                    DateCreated,
                    new SearchField {Name = "OriginName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginStreet1", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginStreet2", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginCountryName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationStreet1", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationStreet2", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationCountryName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "VendorName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "VendorNumber", DataType = SqlDbType.NVarChar},
                    CustomerNumber,
                    CustomerName,
                    Status,
                    FaxStatus,
                    new SearchField {Name = "OriginSpecialInstructions", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "ShipperReference", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginPrimaryContactName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginPrimaryContactPhone", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "EarlyPickup", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "LatePickup", DataType = SqlDbType.NVarChar},
                };

            ShoppedRateSortFields = new List<SearchField>
                                          {
                                            ReferenceNumber,
                                            TypeText,
                                            DateCreated,
                                            Username,
                                            CustomerNumber,
                                            CustomerName,
                                            VendorNumber,
                                            VendorName,
                                            OriginPostalCode,
                                            DestinationPostalCode,
                                            NumberLineItems,
                                            Quantity,
                                            Weight,
                                            LowestTotalCharge,
                                            HighestFreightClass,
                                            MostSignificantFreightClass,
                                          };
            //-----------------------------------------------------

            AddressBooks = new List<SearchField>
			               	{
			               		Description,
			               		new SearchField {Name = "Street1", DataType = SqlDbType.NVarChar},
			               		new SearchField {Name = "Street2", DataType = SqlDbType.NVarChar},
			               		new SearchField {Name = "City", DataType = SqlDbType.NVarChar},
			               		new SearchField {Name = "State", DataType = SqlDbType.NVarChar},
			               		new SearchField {Name = "PostalCode", DataType = SqlDbType.NVarChar},
			               		new SearchField {Name = "CountryCode", DataType = SqlDbType.NVarChar},
			               		new SearchField {Name = "CountryName", DataType = SqlDbType.NVarChar},
			               		CustomerNumber,
			               		CustomerName,
			               	};

            Assets = new List<SearchField>
			               	{
			               		Description,
			               		new SearchField {Name = "AssetNumber", DataType = SqlDbType.NVarChar},
			               		new SearchField {Name = "DateCreated", DataType = SqlDbType.DateTime},
			               		Active,
                                new SearchField{Name = "AssetTypeText", DisplayName = "Asset Type", DataType = SqlDbType.NVarChar}
			               	};


            Bids = new List<SearchField>
			       	{
			       		BidNumber,
			       		DateCreated,
			       		new SearchField {Name = "BidDate", DataType = SqlDbType.DateTime},
			       		new SearchField {Name = "ServiceModeText", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "StatusText", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "OriginName", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "OriginStreet1", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "OriginStreet2", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "OriginCountryName", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "DestinationName", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "DestinationStreet1", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "DestinationStreet2", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "DestinationCountryName", DataType = SqlDbType.NVarChar},
			       		CustomerNumber,
			       		CustomerName,
			       		new SearchField {Name = "Username", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "UserFirstName", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "UserLastName", DataType = SqlDbType.NVarChar},
			       		new SearchField {Name = "Equipments", DataType = SqlDbType.NVarChar}
			       	};

            Claims = new List<SearchField>
			         	{
                            new SearchField {Name = "UserFirstName", DataType = SqlDbType.NVarChar},
			         		new SearchField {Name = "UserLastName", DataType = SqlDbType.NVarChar},
			         		new SearchField {Name = "Shipments", DataType = SqlDbType.NVarChar},
			         		new SearchField {Name = "ServiceTickets", DataType = SqlDbType.NVarChar},
			         		new SearchField {Name = "VendorNames", DataType = SqlDbType.NVarChar},
			         	};
            CarrierPreferredLanes = new List<SearchField>
                                        {
                                            new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginCountryCode", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginCountryName", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationCountryCode", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationCountryName", DataType = SqlDbType.NVarChar},
                                            VendorNumber,
                                            VendorName
                                        };
            Claims.AddRange(ClaimsSortFields);

            // ensure that all fields in customer loads are present in shipment!!!!!
            CustomerLoads = new List<SearchField>
                {
                    ShipmentNumber,
                    new SearchField {Name = "ActualPickupDate", DataType = SqlDbType.DateTime},
                    new SearchField {Name = "DesiredPickupDate", DataType = SqlDbType.DateTime},
                    new SearchField {Name = "ActualDeliveryDate", DataType = SqlDbType.DateTime},
                    new SearchField {Name = "EstimatedDeliveryDate", DataType = SqlDbType.DateTime},
                    DateCreated,
                    VendorProNumber,
                    PurchaseOrderNumber,
                    ShipperReference,
                    new SearchField {Name = "OriginName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginStreet1", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginStreet2", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginCountryName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationStreet1", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationStreet2", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationCountryName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "VendorName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "VendorNumber", DataType = SqlDbType.NVarChar},
                    CustomerNumber,
                    CustomerName,
                    new SearchField {Name = "ServiceModeText", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "Equipments", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "StatusText", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "TotalWeight", DataType = SqlDbType.Decimal},
                    new SearchField {Name = "TotalAmountDue", DataType = SqlDbType.Decimal},
                    new SearchField {Name = "IsGuaranteedDeliveryService", DataType = SqlDbType.Bit},
                    new SearchField {Name = "Username", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "UserFirstName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "UserLastName", DataType = SqlDbType.NVarChar},
					ShipmentReferenceName,
					ShipmentReferenceValue,
                    InvoiceNumber,
                    VendorBillNumber
                };

            LibraryItems = new List<SearchField>
			         	{
                            new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "NMFCCode", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "HTSCode", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "Comment", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "FreightClass", DataType = SqlDbType.Float},
                            new SearchField {Name = "Length", DataType = SqlDbType.Decimal},
                            new SearchField {Name = "Width", DataType = SqlDbType.Decimal},
                            new SearchField {Name = "Height", DataType = SqlDbType.Decimal},
                            new SearchField {Name = "Weight", DataType = SqlDbType.Decimal},
                            new SearchField {Name = "Quantity", DataType = SqlDbType.Int},
                            new SearchField {Name = "PieceCount", DataType = SqlDbType.Int},
			         	};

            LibraryItemsFinder = new List<SearchField>
			         	{
                            new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "NMFCCode", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "HTSCode", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "Comment", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "FreightClass", DataType = SqlDbType.Float},
                            new SearchField {Name = "Length", DataType = SqlDbType.Decimal},
                            new SearchField {Name = "Width", DataType = SqlDbType.Decimal},
                            new SearchField {Name = "Height", DataType = SqlDbType.Decimal},
                            new SearchField {Name = "Weight", DataType = SqlDbType.Decimal},
                            new SearchField {Name = "Quantity", DataType = SqlDbType.Int},
                            new SearchField {Name = "PieceCount", DataType = SqlDbType.Int},
                            new SearchField {Name = "PackageTypeName", DataType = SqlDbType.NVarChar}
			         	};

            LoadOrders = new List<SearchField>
        	                     	{
        	                     		ShipmentNumber,
        	                     		ActualPickupDate,
        	                     		DesiredPickupDate,
        	                     		ActualDeliveryDate,
        	                     		EstimatedDeliveryDate,
        	                     		new SearchField {Name = "Prefix", DataType = SqlDbType.NVarChar},
        	                     		DateCreated,
        	                     		new SearchField {Name = "OriginStreet1", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginStreet2", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "OriginCountryName", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationName", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationStreet1", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationStreet2", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "DestinationCountryName", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "VendorName", DataType = SqlDbType.NVarChar},
        	                     		new SearchField {Name = "VendorNumber", DataType = SqlDbType.NVarChar},
        	                     		CustomerNumber,
										CustomerName,
        	                     		ServiceMode,
        	                     		Status,
                                        new SearchField {Name = "AccountBucketCode", DataType = SqlDbType.NVarChar},
			            		        VendorProNumber,
			            		        PurchaseOrderNumber,
			            		        ShipperReference,
			            		        new SearchField {Name = "Username", DataType = SqlDbType.NVarChar},
			            		        new SearchField {Name = "UserFirstName", DataType = SqlDbType.NVarChar},
			            		        new SearchField {Name = "UserLastName", DataType = SqlDbType.NVarChar},
			            		        new SearchField {Name = "Equipments", DataType = SqlDbType.NVarChar},
			            		        new SearchField {Name = "TotalWeight", DataType = SqlDbType.Decimal},
                                        new SearchField {Name = "ShipmentCoordinatorUserName", DataType = SqlDbType.NVarChar},
                                        new SearchField {Name = "ShipmentCoordinatorFirstName", DataType = SqlDbType.NVarChar},
                                        new SearchField {Name = "ShipmentCoordinatorLastName", DataType = SqlDbType.NVarChar},
                                        new SearchField {Name = "CarrierCoordinatorUserName", DataType = SqlDbType.NVarChar},
                                        new SearchField {Name = "CarrierCoordinatorFirstName", DataType = SqlDbType.NVarChar},
                                        new SearchField {Name = "CarrierCoordinatorLastName", DataType = SqlDbType.NVarChar},
										ShipmentReferenceName,
										ShipmentReferenceValue,
        	                     	};

	        ServiceTickets = new List<SearchField>
		        {
			        new SearchField {Name = "Prefix", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "AccountBucketCode", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "VendorName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "VendorNumber", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "UserFirstName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "UserLastName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "Equipments", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "TotalAmountDue", DataType = SqlDbType.Decimal},
			        new SearchField {Name = "TotalCost", DataType = SqlDbType.Decimal},
			        AuditedForInvoicing,
                    ServiceTicketDocuments,
					InvoiceNumber,
                    VendorBillNumber
		        };
            ServiceTickets.AddRange(ServiceTicketSortFields);
            
            Jobs = new List<SearchField>
                {
                    new SearchField { Name = "CustomerId", DataType = SqlDbType.NVarChar },
                    JobShipmentNumber,
                    JobServiceTicketNumber,
                    JobLoadOrderNumber,
                    InvoiceNumber,
                    VendorBillNumber,
                    ContainerNumber
                };
            Jobs.AddRange(JobSortFields);

            Shipments = new List<SearchField>
		        {
			        new SearchField {Name = "AccountBucketCode", DataType = SqlDbType.NVarChar},
			        VendorProNumber,
			        PurchaseOrderNumber,
			        ShipperReference,
			        new SearchField {Name = "Username", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "UserFirstName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "UserLastName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "Equipments", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "TotalWeight", DataType = SqlDbType.Decimal},
			        new SearchField {Name = "TotalAmountDue", DataType = SqlDbType.Decimal},
			        new SearchField {Name = "TotalCost", DataType = SqlDbType.Decimal},
			        AuditedForInvoicing,
			        new SearchField {Name = "ShipmentCoordinatorUserName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "ShipmentCoordinatorFirstName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "ShipmentCoordinatorLastName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "CarrierCoordinatorUserName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "CarrierCoordinatorFirstName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "CarrierCoordinatorLastName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "IsGuaranteedDeliveryService", DataType = SqlDbType.Bit},
			        new SearchField {Name = "InDispute", DataType = SqlDbType.Bit},
			        new SearchField {Name = "InDisputeReasonText", DataType = SqlDbType.NVarChar},
			        ShipmentReferenceName,
			        ShipmentReferenceValue
		        };
            Shipments.AddRange(ShipmentSortFields);


            ShipmentDashboard = new List<SearchField>()
                {
                    InvoiceNumber,
                    VendorBillNumber
                };
            ShipmentDashboard.AddRange(Shipments);

            PendingLtlPickups = new List<SearchField>
                {
                    new SearchField {Name = "IsGuaranteedDeliveryService", DataType = SqlDbType.Bit},
                    new SearchField {Name = "OriginTerminalName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalCity", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalCode", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalContactName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalContactTitle", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalCountryName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalEmail", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalFax", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalPhone", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalPostalCode", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalState", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalStreet1", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalStreet2", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginTerminalTollFree", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "IsLocked", DataType = SqlDbType.Bit},
                    new SearchField {Name = "LockedByUsername", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "Prefix", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "PrefixDescription", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "AccountBucketCode", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "AccountBucketDescription", DataType = SqlDbType.NVarChar},
                };
            PendingLtlPickups.AddRange(PendingLtlPickupSortFields);

            ShoppedRates = new List<SearchField>
                               {
                                   new SearchField { Name = "OriginState", DataType = SqlDbType.NVarChar},
                                   new SearchField { Name = "DestinationState", DataType = SqlDbType.NVarChar},
                                   new SearchField { Name = "OriginCountryName", DataType = SqlDbType.NVarChar},
                                   new SearchField { Name = "OriginCity", DataType = SqlDbType.NVarChar },
                                   new SearchField { Name = "DestinationCity", DataType = SqlDbType.NVarChar },
                                   new SearchField { Name = "DestinationCountryName", DataType = SqlDbType.NVarChar},
                                   new SearchField { Name = "CustomerRateAndScheduleInDemo", DataType = SqlDbType.Bit}
                               };
            ShoppedRates.AddRange(ShoppedRateSortFields);

            VendorTerminals = new List<SearchField>
                                  {
                                      new SearchField {Name = "CountryName", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "CountryCode", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "Street1", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "Street2", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "City", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "State", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "PostalCode", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "Phone", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "Mobile", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "Fax", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "Email", DataType = SqlDbType.NVarChar},
                                      new SearchField {Name = "ContactName", DataType = SqlDbType.NVarChar},
                                      VendorNumber,
                                      VendorName
                                  };

        }
    }
}
