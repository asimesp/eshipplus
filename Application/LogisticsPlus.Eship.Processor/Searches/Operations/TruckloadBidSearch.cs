﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class TruckloadBidSearch : EntityBase
	{
		public TruckloadBid FetchTruckloadBidByLoadOrderId(long loadOrderId, long tenantId)
		{
			TruckloadBid bid = null;

			const string query = @"SELECT * FROM TruckloadBid WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "LoadOrderId", loadOrderId }, { "TenantId", tenantId }, };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					bid = new TruckloadBid(reader);
			Connection.Close();
			return bid;
		}

		public TruckloadBid FetchTruckloadBidByProfileId(long tlProfileId, long tenantId)
		{
			TruckloadBid bid = null;

			const string query = @"SELECT * FROM TruckloadBid WHERE TLTenderingProfileId = @TLTenderingProfileId AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TLTenderingProfileId", tlProfileId }, { "TenantId", tenantId }, };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					bid = new TruckloadBid(reader);
			Connection.Close();
			return bid;
		}
		
		public List<TruckloadBid> FetchAvailableTruckloadBids(long tenantId)
		{
			var bids = new List<TruckloadBid>();

			const string query = @"SELECT * FROM TruckloadBid WHERE TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }};

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					bids.Add(new TruckloadBid(reader));
			Connection.Close();
			return bids;
		}
		public List<TruckloadBid> FetchTruckloadBidByStatus(int bidStatus, long tenantId)
		{
			var bids = new List<TruckloadBid>();

			const string query = @"SELECT * FROM TruckloadBid WHERE BidStatus = @BidStatus AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "BidStatus", bidStatus }, { "TenantId", tenantId }, };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					bids.Add(new TruckloadBid(reader));
			Connection.Close();
			return bids;
		}

		public TruckloadBid FetchTruckloadBidByLoadOrderIdAndStatus(long loadOrderId, int bidStatus, long tenantId)
		{
			TruckloadBid bid = null;

			const string query = @"SELECT * FROM TruckloadBid WHERE LoadOrderId = @LoadOrderId  AND BidStatus = @BidStatus AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "LoadOrderId", loadOrderId }, { "BidStatus", bidStatus }, { "TenantId", tenantId }, };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					bid = new TruckloadBid(reader);
			Connection.Close();
			return bid;
		}

	
	}
}
