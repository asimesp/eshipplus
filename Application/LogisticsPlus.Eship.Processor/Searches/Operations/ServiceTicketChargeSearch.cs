﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class ServiceTicketChargeSearch : EntityBase
	{
		public bool HasUnBilledCharges(long serviceTicketId)
		{
			var serviceTicketCharges = new List<ServiceTicketCharge>();
			const string query = @"SELECT * FROM ServiceTicketCharge where ServiceTicketId = @Id and VendorBillId = 0";
			var parameters = new Dictionary<string, object> { { "Id", serviceTicketId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					serviceTicketCharges.Add(new ServiceTicketCharge(reader));
			Connection.Close();
			return serviceTicketCharges.Count > 0;


		}
	}
}
