﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class ClaimSearch : EntityBase
	{
		public List<ClaimViewSearchDto> FetchClaimDtos(ClaimViewSearchCriteria criteria, long tenantId)
		{
			var claims = new List<ClaimViewSearchDto>();
			var query = @"SELECT Top 1000 * FROM ClaimViewSearch2(@TenantId, @ActiveUserId) WHERE ";
			
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };
			query += criteria.Parameters.BuildWhereClause(parameters, OperationsSearchFields.Claims);

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);
            
            using (var reader = GetReader(query, parameters))
				while (reader.Read())
					claims.Add(new ClaimViewSearchDto(reader));
			Connection.Close();
			return claims;
		}

        public Claim FetchClaimByClaimNumber(string number, long tenantId)
        {
            Claim claim = null;

            const string query = @"SELECT * FROM Claim WHERE ClaimNumber = @ClaimNumber AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "ClaimNumber", number }, { "TenantId", tenantId }, };

            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    claim = new Claim(reader);
            Connection.Close();
            return claim;
        }

        public List<ClaimNote> FetchClaimNoteRemindersReadyForRun()
        {
            var notes = new List<ClaimNote>();

            const string query = @"SELECT * FROM ClaimNote WHERE @CurrentDate >= SendReminderOn AND SendReminder = 1";

            var parameters = new Dictionary<string, object>
                {
                    {"CurrentDate", DateTime.Now}
                };

            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    notes.Add(new ClaimNote(reader));
            Connection.Close();
            return notes;
        }
	}
}
