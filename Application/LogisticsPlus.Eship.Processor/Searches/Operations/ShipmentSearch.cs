﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class ShipmentSearch : EntityBase
	{
		public Shipment FetchShipmentByShipmentNumber(string number, long tenantId)
		{
			Shipment shipment = null;

			const string query = @"SELECT * FROM Shipment WHERE ShipmentNumber = @ShipmentNumber AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "ShipmentNumber", number }, { "TenantId", tenantId }, };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					shipment = new Shipment(reader);
			Connection.Close();
			return shipment;
		}

		public Shipment FetchShipmentByPrimaryVendorProAndScac(string pro, string scac, long tenantId)
		{
			Shipment shipment = null;

			const string query = @"select s.* from Shipment s, ShipmentVendor sv, Vendor v 
				where s.Id = sv.ShipmentId and sv.VendorId = v.Id and sv.ProNumber = @ProNumber and v.Scac = @Scac and s.TenantId = @TenantId and sv.[Primary] = 1";
			var parameters = new Dictionary<string, object> {{"ProNumber", pro}, {"Scac", scac}, {"TenantId", tenantId},};

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					shipment = new Shipment(reader);
			Connection.Close();
			return shipment;
		}

		public bool ShipmentExists(string number, long tenantId)
		{
			const string query = @"SELECT count(*) FROM Shipment WHERE ShipmentNumber = @ShipmentNumber AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "ShipmentNumber", number }, { "TenantId", tenantId }, };
			return ExecuteScalar(query, parameters).ToInt() > 0;
		}

		public List<ShipmentDashboardDto> FetchShipmentsToBeInvoiced(ShipmentViewSearchCriteria criteria, long tenantId)
		{
			var shipments = new List<ShipmentDashboardDto>();

			// status 3, 4 = Invoiced, Void!
			var query = @"SELECT Top 1000 * FROM ShipmentViewSearch2(@TenantId, @ActiveUserId) WHERE [Status] NOT IN (3,4) AND AuditedForInvoicing = 1 AND ";
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };

			var refernceColumns = new List<ParameterColumn>
				{
					OperationsSearchFields.ShipmentReferenceName.ToParameterColumn(),
					OperationsSearchFields.ShipmentReferenceValue.ToParameterColumn()
				};

			query += criteria
				.Parameters
				.Where(p => refernceColumns.All(rc => rc.ReportColumnName != p.ReportColumnName))
				.ToList()
				.BuildWhereClause(parameters, OperationsSearchFields.Shipments);

			if (criteria.Parameters.Any(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName)))
			{
				var rquery = @"Select ShipmentNumber From Shipment s Inner Join ShipmentReference r on r.ShipmentId = s.Id Where ";

				var rparams = new Dictionary<string, object>();
				rquery += criteria
					.Parameters
					.Where(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName))
					.ToList()
					.BuildWhereClause(rparams, OperationsSearchFields.Shipments);

				foreach (var item in rparams)
				{
					var newKey = string.Format("r_{0}", item.Key);
					rquery = rquery.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}

				query += string.Format(" AND ShipmentNumber IN ({0})", rquery);
			}

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);
            
            using (var reader = GetReader(query, parameters))
				while (reader.Read())
					shipments.Add(new ShipmentDashboardDto(reader, DashboardDtoType.Shipment));
			Connection.Close();
			return shipments;
		}

        public List<ShipmentDashboardDto> FetchShipmentsExludeAttachetToJob(ShipmentViewSearchCriteria criteria, long tenantId)
        {
            var shipments = new List<ShipmentDashboardDto>();

            var query = @"SELECT Top 1000 * FROM ShipmentViewSearch2(@TenantId, @ActiveUserId) WHERE JobId = '0' AND  ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };
            
            query += criteria.Parameters.BuildWhereClause(parameters, OperationsSearchFields.Shipments);
            query += SearchUtilities.AddQuerySorting(criteria.SortBy, criteria.SortAscending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    shipments.Add(new ShipmentDashboardDto(reader, DashboardDtoType.Shipment));
            Connection.Close();

            return shipments;
        }

        public List<Shipment> FetchShipmentNotBolDelivered(DateTime startCheckDate, long customerId, long tenantId, bool invoicedStatusOnly)
		{
			var shipments = new List<Shipment>();
			var query = string.Format(
					@"Select s.*
					From 
						shipment s
						Inner Join Customer c On s.CustomerId = c.Id
					Where 
						s.TenantId = @TenantId
						And s.BolFtpDelivered = '0'
						And s.DateCreated >= @StartCheckDate
						{0}
						And c.Id = @CustomerId",
					invoicedStatusOnly ? "And s.[Status] = 3 -- invoiced" : "And s.[Status] <> 4 -- void");

			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"TenantId", tenantId},
			                 		{"StartCheckDate", startCheckDate.TimeToMinimum()},
			                 		{"CustomerId", customerId}
			                 	};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					shipments.Add(new Shipment(reader));
			Connection.Close();
			return shipments;
		}

		public List<Shipment> FetchShipmentNotShipmentStatementDelivered(DateTime startCheckDate, long customerId, long tenantId)
		{
			var shipments = new List<Shipment>();
			const string query = @"Select s.*
					From 
						shipment s
						Inner Join Customer c On s.CustomerId = c.Id
					Where 
						s.TenantId = @TenantId
						And s.StatementFtpDelivered = '0'
						And s.DateCreated >= @StartCheckDate
						And s.[Status] = 3 -- invoiced
						And c.Id = @CustomerId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"TenantId", tenantId},
			                 		{"StartCheckDate", startCheckDate.TimeToMinimum()},
			                 		{"CustomerId", customerId}
			                 	};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					shipments.Add(new Shipment(reader));
			Connection.Close();
			return shipments;
		}

		public List<ShipmentDocument> FetchShipmentDocumentsNotDelivered(DateTime startCheckDate, long customerId, long tenantId, bool invoicedStatusOnly, bool includeInternalDocs, List<long> documentTagIds)
		{
			var docs = new List<ShipmentDocument>();
			var query = string.Format(
				@"Select d.*
					From 
						ShipmentDocument d
						Inner Join shipment s On d.ShipmentId = s.Id
						Inner Join Customer c On s.CustomerId = c.Id
					Where 
						s.TenantId = @TenantId
						And d.FtpDelivered = '0'
						And s.DateCreated >= @StartCheckDate
						{0}
						And c.Id = @CustomerId
						And d.DocumentTagId in ({1})
						{2}",
				invoicedStatusOnly ? "And s.[Status] = 3 -- invoiced" : "And s.[Status] <> 4 -- void",
				documentTagIds.Any() ? documentTagIds.Select(id => id.GetString()).ToArray().CommaJoin() : new[] {"-1"}.CommaJoin(),
				includeInternalDocs ? string.Empty :  "And d.IsInternal = '0'");

			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"TenantId", tenantId},
			                 		{"StartCheckDate", startCheckDate.TimeToMinimum()},
			                 		{"CustomerId", customerId}
			                 	};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					docs.Add(new ShipmentDocument(reader));
			Connection.Close();
			return docs;
		}


		public bool IsShipmentPresentOnInvoice(long invoiceId, string shipmentNumber, long tenantId)
		{
			const string query =
				@"SELECT COUNT(*) FROM InvoiceDetail WHERE ReferenceType = @Reference AND ReferenceNumber = @ShipmentNumber AND InvoiceId <> @InvoiceId";

			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Reference", DetailReferenceType.Shipment},
				                 	{"TenantId", tenantId},
									{"ShipmentNumber", shipmentNumber},
				                 	{"InvoiceId", invoiceId},
			                 	};

			return ExecuteScalar(query, parameters).ToInt() > 0;
		}

		public List<ShipmentDashboardDto> FetchShipmentsDashboardDto(ShipmentViewSearchCriteria criteria, long tenantId)
		{
			var shipments = new List<ShipmentDashboardDto>();

			var query = @"SELECT Top 1000 * FROM ShipmentViewSearch2(@TenantId, @ActiveUserId) WHERE ";
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };

			var refernceColumns = new List<ParameterColumn>
				{
					OperationsSearchFields.ShipmentReferenceName.ToParameterColumn(),
					OperationsSearchFields.ShipmentReferenceValue.ToParameterColumn()
				};

			query += criteria
				.Parameters
				.Where(p => refernceColumns.All(rc => rc.ReportColumnName != p.ReportColumnName))
				.ToList()
				.BuildWhereClause(parameters, OperationsSearchFields.Shipments);

			if (criteria.Parameters.Any(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName)))
			{
				var rquery = @"Select ShipmentNumber From Shipment s Inner Join ShipmentReference r on r.ShipmentId = s.Id Where ";

				var rparams = new Dictionary<string, object>();
				rquery += criteria
					.Parameters
					.Where(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName))
					.ToList()
					.BuildWhereClause(rparams, OperationsSearchFields.Shipments);

				foreach (var item in rparams)
				{
					var newKey = string.Format("r_{0}", item.Key);
					rquery = rquery.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}

				query += string.Format(" AND ShipmentNumber IN ({0})", rquery);
			}

		

			if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);
			

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					shipments.Add(new ShipmentDashboardDto(reader, DashboardDtoType.Shipment));
			Connection.Close();
			return shipments;
		}

		public List<ShipmentDashboardDto> FetchLoadsShipmentsDashboardDto(ShipmentViewSearchCriteria criteria, long tenantId, bool loadCheckCalls = false)
		{
			var shipments = new List<ShipmentDashboardDto>();

			var query1 = @"SELECT Top 1000 * FROM ShipmentViewSearch2(@TenantId, @ActiveUserId) WHERE ";
			var query3 = @"SELECT ShipmentNumber FROM ShipmentViewSearch2(@TenantId, @ActiveUserId) WHERE ";
			var query2 = @"SELECT Top 1000 * FROM LoadOrderViewSearch(@TenantId, @ActiveUserId) WHERE ";
			var query4 = string.Empty;

            var subQueryReportColumns = new List<SearchField>
                {
                    OperationsSearchFields.InvoiceNumber,
                    OperationsSearchFields.VendorBillNumber,
                }.ToParameterColumn()
                .Select(p => p.ReportColumnName)
                 .ToList();

            var containsInvoiceFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(OperationsSearchFields.InvoiceNumber.DisplayName);
            var containsVendorBillFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(OperationsSearchFields.VendorBillNumber.DisplayName);

			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };

			var refernceColumns = new List<ParameterColumn>
				{
					OperationsSearchFields.ShipmentReferenceName.ToParameterColumn(),
					OperationsSearchFields.ShipmentReferenceValue.ToParameterColumn()
				};

			if (criteria.Parameters.Any(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName)))
			{
				const string rquery = @"Select ShipmentNumber From Shipment s Inner Join ShipmentReference r on r.ShipmentId = s.Id Where {0}
							Union
							Select L.LoadOrderNumber 'ShipmentNumber' from LoadOrder l Inner Join LoadOrderReference r on r.LoadOrderId = l.Id where {0}";

				var rparams = new Dictionary<string, object>();
				var rWhereClause = criteria
					.Parameters
					.Where(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName))
					.ToList()
					.BuildWhereClause(rparams, OperationsSearchFields.Shipments);
				query4 = string.Format(rquery, rWhereClause);

				foreach (var item in rparams)
				{
					var newKey = string.Format("r_{0}", item.Key);
					query4 = query4.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}
			}
			
			var whereClause = criteria
				.Parameters
				.Where(p=> refernceColumns.All(rc => rc.ReportColumnName != p.ReportColumnName) && !subQueryReportColumns.Contains(p.ReportColumnName) )
				.ToList()
				.BuildWhereClause(parameters, OperationsSearchFields.Shipments);

			query1 += whereClause;
			query3 += whereClause;
			query2 += string.Format("{0} AND ShipmentNumber NOT IN ({1})", whereClause, query3);

			if (!string.IsNullOrEmpty(query4))
			{
				query1 += string.Format(" AND ShipmentNumber IN ({0})", query4);
				query2 += string.Format(" AND ShipmentNumber IN ({0})", query4);
			}

			var sortBy = string.Empty;
			if(criteria.SortBy != null)
                sortBy = string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

            if (containsInvoiceFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == OperationsSearchFields.InvoiceNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, OperationsSearchFields.ShipmentDashboard);

                var vendorBillQuery = string.Format(@"(SELECT sh.Id FROM Shipment sh 
                                                          INNER JOIN InvoiceDetail id ON id.ReferenceNumber = sh.ShipmentNumber AND id.ReferenceType = {0}
                                                          INNER JOIN Invoice i ON id.InvoiceId = i.Id WHERE {1})",
                                       DetailReferenceType.Shipment.ToInt(),
                                       filter);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("i_{0}", item.Key);
                    vendorBillQuery = vendorBillQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query1 += string.Format("  AND (Id IN ({0})) ", vendorBillQuery);
                query2 += string.Format("  AND (Id IN ({0})) ", vendorBillQuery);
            }
            
            if (containsVendorBillFilter)
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var filter = criteria.Parameters.Where(p => p.ReportColumnName == OperationsSearchFields.VendorBillNumber.DisplayName)
                                                .ToList()
                                                .BuildWhereClause(uParams, OperationsSearchFields.ShipmentDashboard);

                var invoiceQuery = string.Format(@"(SELECT sh.Id FROM Shipment sh 
                                                          INNER JOIN VendorBillDetail vbd ON vbd.ReferenceNumber = sh.ShipmentNumber AND vbd.ReferenceType = {0}
                                                          INNER JOIN VendorBill vb ON vbd.VendorBillId = vb.Id WHERE {1})",
                                       DetailReferenceType.Shipment.ToInt(),
                                       filter);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("vb_{0}", item.Key);
                    invoiceQuery = invoiceQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query1 += string.Format("  AND (Id IN ({0})) ", invoiceQuery);
                query2 += string.Format("  AND (Id IN ({0})) ", invoiceQuery);
            }

            var comb = string.Format(@"Select * from (({0} {2}) union ({1} {2})) as q ", query1, query2, sortBy);
		    comb += sortBy;

			using (var reader = GetReader(comb, parameters))
				while (reader.Read())
					shipments.Add(new ShipmentDashboardDto(reader, DashboardDtoType.Shipment));
			Connection.Close();


			if (loadCheckCalls && shipments.Any(s => s.Type == DashboardDtoType.Shipment && s.CheckCallsPresent))
			{
				var checkCalls = new List<CheckCall>();
				var ids = shipments
					.Where(s => s.Type == DashboardDtoType.Shipment && s.CheckCallsPresent)
					.Select(s => s.Id.ToString())
					.ToArray()
					.CommaJoin();
				var query = string.Format("SELECT * FROM CheckCall WHERE ShipmentId in ({0})",ids);
				using (var reader = GetReader(query, null))
					while (reader.Read())
						checkCalls.Add(new CheckCall(reader));
				Connection.Close();

				foreach (var shipment in shipments.Where(s => s.Type == DashboardDtoType.Shipment && s.CheckCallsPresent))
					shipment.CheckCalls = checkCalls.Where(c => c.ShipmentId == shipment.Id).ToList();
			}
			
			return shipments;
			
		}

        public List<ShipmentDashboardDto> FetchLoadsShipmentsDashboardDtoForDispatch(ShipmentViewSearchCriteria criteria, long tenantId, bool loadCheckCalls = false)
        {
            var shipments = new List<ShipmentDashboardDto>();

            var query1 = @"SELECT Top 1000 * FROM ShipmentViewSearch2(@TenantId, @ActiveUserId) WHERE Status not in (2,3,4) And ServiceMode <> 0 And ";
            var query3 = @"SELECT ShipmentNumber FROM ShipmentViewSearch2(@TenantId, @ActiveUserId) WHERE ";
            var query2 = @"SELECT Top 1000 * FROM LoadOrderViewSearch(@TenantId, @ActiveUserId) WHERE Status not in (2,4,6) And ServiceMode <> 0 And ";
			var query4 = string.Empty;

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };

			var refernceColumns = new List<ParameterColumn>
				{
					OperationsSearchFields.ShipmentReferenceName.ToParameterColumn(),
					OperationsSearchFields.ShipmentReferenceValue.ToParameterColumn()
				};

			if (criteria.Parameters.Any(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName)))
			{
				const string rquery = @"Select ShipmentNumber From Shipment s Inner Join ShipmentReference r on r.ShipmentId = s.Id Where {0}
							Union
							Select L.LoadOrderNumber 'ShipmentNumber' from LoadOrder l Inner Join LoadOrderReference r on r.LoadOrderId = l.Id where {0}";

				var rparams = new Dictionary<string, object>();
				var rWhereClause = criteria
					.Parameters
					.Where(p => refernceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName))
					.ToList()
					.BuildWhereClause(rparams, OperationsSearchFields.Shipments);
				query4 = string.Format(rquery, rWhereClause);

				foreach (var item in rparams)
				{
					var newKey = string.Format("r_{0}", item.Key);
					query4 = query4.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}
			}


			var whereClause = criteria
				.Parameters
				.Where(p => refernceColumns.All(rc => rc.ReportColumnName != p.ReportColumnName))
				.ToList()
				.BuildWhereClause(parameters, OperationsSearchFields.Shipments);

            query1 += whereClause;
            query3 += whereClause;
            query2 += string.Format("{0} AND ShipmentNumber NOT IN ({1})", whereClause, query3);

			if (!string.IsNullOrEmpty(query4))
			{
				query1 += string.Format(" AND ShipmentNumber IN ({0})", query4);
				query2 += string.Format(" AND ShipmentNumber IN ({0})", query4);
			}

            var sortBy = string.Empty;
            if (criteria.SortBy != null)
                sortBy = string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

            var comb = string.Format(@"Select * from (({0} {2}) union ({1} {2})) as q {2}", query1, query2, sortBy);

            using (var reader = GetReader(comb, parameters))
                while (reader.Read())
                    shipments.Add(new ShipmentDashboardDto(reader, DashboardDtoType.Shipment));
            Connection.Close();


            if (loadCheckCalls && shipments.Any(s => s.Type == DashboardDtoType.Shipment && s.CheckCallsPresent))
            {
                var checkCalls = new List<CheckCall>();
                var ids = shipments
                    .Where(s => s.Type == DashboardDtoType.Shipment && s.CheckCallsPresent)
                    .Select(s => s.Id.ToString())
                    .ToArray()
                    .CommaJoin();
                var query = string.Format("SELECT * FROM CheckCall WHERE ShipmentId in ({0})", ids);
                using (var reader = GetReader(query, null))
                    while (reader.Read())
                        checkCalls.Add(new CheckCall(reader));
                Connection.Close();

                foreach (var shipment in shipments.Where(s => s.Type == DashboardDtoType.Shipment && s.CheckCallsPresent))
                    shipment.CheckCalls = checkCalls.Where(c => c.ShipmentId == shipment.Id).ToList();
            }


            return shipments;
        }

        public List<Shipment> FetchSmallPackShipmentsForTracking(SmallPackageEngine smallPackageEngine)
        {
            var shipments = new List<Shipment>();
            const string query = "Select * From Shipment Where SmallPackageEngine = @SmallPackageEngine And [Status] != @Delivered And [Status] != @Invoiced And [Status] != @Void";
            var parameters = new Dictionary<string, object>
			                 	{
                                    {"SmallPackageEngine", smallPackageEngine.ToInt()},
			                 		{"Delivered", ShipmentStatus.Delivered.ToInt()},
			                 		{"Invoiced", ShipmentStatus.Invoiced.ToInt()},
			                 		{"Void", ShipmentStatus.Void.ToInt()}
			                 	};
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    shipments.Add(new Shipment(reader));
            Connection.Close();
            return shipments;
        }

        public List<PendingLtlPickupsDashboardDto> FetchShipmentPickupDashboardDto(PendingLtlPickupViewSearchCriteria criteria, long tenantId)
        {
            var shipments = new List<PendingLtlPickupsDashboardDto>();

            var query = @"SELECT Top 1000 * FROM PendingLTLPickupsDashboardViewSearch(@TenantId, @ActiveUserId) WHERE ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };
            query += criteria.Parameters.BuildWhereClause(parameters, OperationsSearchFields.PendingLtlPickups);

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);


            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    shipments.Add(new PendingLtlPickupsDashboardDto(reader));
            Connection.Close();
            return shipments;
        }


		public List<ShipmentDocRtrvLog> RetrieveNonExpiredShipmentDocRtrvLogs()
		{
			var logs = new List<ShipmentDocRtrvLog>();
			const string query = "Select * from ShipmentDocRtrvLog where @Today <= ExpirationDate AND (PodDate = @DefaultDate OR BolDate = @DefaultDate OR WniDate = @DefaultDate)";
			var parameters = new Dictionary<string, object>
				{
					{"Today", DateTime.Now.TimeToMaximum()},
					{"DefaultDate", DateUtility.SystemEarliestDateTime},
				};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					logs.Add(new ShipmentDocRtrvLog(reader));
			Connection.Close();
			return logs;
		}

		public bool ShipmentDocRtrvLogExistsForShipment(string shipmentNumber, long tenantId)
		{
			const string query = "Select count(*) from ShipmentDocRtrvLog where ShipmentNumber = @ShipmentNumber AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"TenantId", tenantId}, {"ShipmentNumber", shipmentNumber},};

			return ExecuteScalar(query, parameters).ToInt() > 0;
		}

		public List<Shipment> RetrieveDispatchableShipmentsForTracking(long tenantId)
		{
			var shipments = new List<Shipment>();

			const string query = @"Select
									s.*
								From
									Shipment s
									Inner Join ShipmentVendor sv on sv.ShipmentId = s.Id
									Inner Join Vendor v on sv.VendorId = v.Id
									Inner Join VendorCommunication vc on vc.VendorId = v.Id
								Where
									s.[Status] = 0 -- scheduled
									And s.TenantId = @TenantId
									And sv.[Primary] = 1 -- primary vendor only
									And vc.ShipmentDispatchEnabled = 1
									And v.Active = 1 -- active vendor only
									And sv.ProNumber = ''";
			var parameters = new Dictionary<string, object> {{"TenantId", tenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					shipments.Add(new Shipment(reader));
			Connection.Close();

			return shipments;
		}
	}
}
