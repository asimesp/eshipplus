﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class Project44TrackResponseSearch : EntityBase
	{
        public Project44TrackingResponse FetchTrackResponseByShipmentId(long projec44Id)
        {
	        var query = @"SELECT * 
                            FROM Project44TrackingResponse
                            WHERE Project44Id = @Project44Id";
	        var parameters = new Dictionary<string, object> { { "Project44Id", projec44Id } };

            Project44TrackingResponse res = null;

            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    res = new Project44TrackingResponse(reader);
            Connection.Close();
            return res;
        }
	}
}
