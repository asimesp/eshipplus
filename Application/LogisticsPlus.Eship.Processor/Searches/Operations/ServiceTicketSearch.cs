﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
	public class ServiceTicketSearch : EntityBase
	{
		public List<ServiceTicketDashboardDto> FetchServiceTicketDashboardDtos(ServiceTicketViewSearchCriteria criteria, long tenantId)
		{
			var serviceTickets = new List<ServiceTicketDashboardDto>();

			var subQueryReportColumns = new List<SearchField>
                {
                    OperationsSearchFields.InvoiceNumber,
                    OperationsSearchFields.VendorBillNumber,
                   }.ToParameterColumn()
			  .Select(c => c.ReportColumnName)
			  .ToList();

			var containsInvoiceFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(OperationsSearchFields.InvoiceNumber.DisplayName);
			var containsVendorBillFilter = criteria.Parameters.Select(p => p.ReportColumnName).Contains(OperationsSearchFields.VendorBillNumber.DisplayName);

			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };

			var query = @"SELECT Top 1000 sts.* FROM ServiceTicketViewSearch(@TenantId, @ActiveUserId) sts WHERE ";

			if (containsInvoiceFilter)
			{
				var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
				var filter = criteria.Parameters.Where(p => p.ReportColumnName == OperationsSearchFields.InvoiceNumber.DisplayName)
												.ToList()
												.BuildWhereClause(uParams, OperationsSearchFields.Jobs);

				var invoiceQuery = string.Format(@"(SELECT st.id FROM ServiceTicket st                                                          
                                                          INNER JOIN InvoiceDetail id ON id.ReferenceNumber = st.ServiceTicketNumber AND id.ReferenceType = {0}
                                                          INNER JOIN Invoice i ON id.InvoiceId = i.Id WHERE {1})                                        
                                             ",
									   DetailReferenceType.ServiceTicket.ToInt(),
									   filter);

				foreach (var item in uParams)
				{
					var newKey = string.Format("i_{0}", item.Key);
					invoiceQuery = invoiceQuery.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}

				query += string.Format("(sts.Id IN ({0})) AND ", invoiceQuery);
			}

			if (containsVendorBillFilter)
			{
				var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
				var filter = criteria.Parameters.Where(p => p.ReportColumnName == OperationsSearchFields.VendorBillNumber.DisplayName)
												.ToList()
												.BuildWhereClause(uParams, OperationsSearchFields.Jobs);

				var billQuery = string.Format(@"(SELECT st.id FROM ServiceTicket st                                                   
                                                          INNER JOIN VendorBillDetail vbd ON vbd.ReferenceNumber = st.ServiceTicketNumber AND vbd.ReferenceType = {0}
                                                          INNER JOIN VendorBill vb ON vbd.VendorBillId = vb.Id WHERE {1})
                                            ",
										DetailReferenceType.ServiceTicket.ToInt(),
									   filter);

				foreach (var item in uParams)
				{
					var newKey = string.Format("vb_{0}", item.Key);
					billQuery = billQuery.Replace(item.Key, newKey);
					parameters.Add(newKey, item.Value);
				}

				query += string.Format("(sts.Id IN ({0})) AND ", billQuery);
			}
			
			
            var referenceColumns = new List<ParameterColumn>
				{
					OperationsSearchFields.ServiceTicketDocuments.ToParameterColumn(),
				};

            var whereClause = criteria
                .Parameters
				.Where(p => referenceColumns.All(rc => rc.ReportColumnName != p.ReportColumnName && !subQueryReportColumns.Contains(p.ReportColumnName)))
                .ToList()
                .BuildWhereClause(parameters, OperationsSearchFields.ServiceTickets);

		    query += whereClause;

            if (criteria.Parameters.Any(p => referenceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName)))
            {
                const string rquery = @"Select ServiceTicketId From ServiceTicketDocument d Where {0}";

                var rparams = new Dictionary<string, object>();
                var rWhereClause = criteria
                    .Parameters
                    .Where(p => referenceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName))
                    .ToList()
                    .BuildWhereClause(rparams, OperationsSearchFields.ServiceTickets);
                var query2 = string.Format(rquery, rWhereClause);

                foreach (var item in rparams)
                {
                    var newKey = string.Format("d_{0}", item.Key);
                    query2 = query2.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format(" AND Id IN ({0})", query2);
            }

			

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

            using (var reader = GetReader(query, parameters))
				while (reader.Read())
					serviceTickets.Add(new ServiceTicketDashboardDto(reader));
			Connection.Close();
			return serviceTickets;
		}

		public List<ServiceTicketDashboardDto> FetchServiceTicketReadyForInvoicing(ServiceTicketViewSearchCriteria criteria, long tenantId)
		{
			var serviceTickets = new List<ServiceTicketDashboardDto>();

			var query = @"SELECT Top 1000 * FROM ServiceTicketViewSearch(@TenantId, @ActiveUserId) WHERE AuditedForInvoicing = 1 AND ";
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };

            var referenceColumns = new List<ParameterColumn>
				{
					OperationsSearchFields.ServiceTicketDocuments.ToParameterColumn(),
				};

            var whereClause = criteria
                .Parameters
                .Where(p => referenceColumns.All(rc => rc.ReportColumnName != p.ReportColumnName))
                .ToList()
                .BuildWhereClause(parameters, OperationsSearchFields.ServiceTickets);

            query += whereClause;

            if (criteria.Parameters.Any(p => referenceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName)))
            {
                const string rquery = @"Select ServiceTicketId From ServiceTicketDocument d Where {0}";

                var rparams = new Dictionary<string, object>();
                var rWhereClause = criteria
                    .Parameters
                    .Where(p => referenceColumns.Select(i => i.ReportColumnName).Contains(p.ReportColumnName))
                    .ToList()
                    .BuildWhereClause(rparams, OperationsSearchFields.ServiceTickets);
                var query2 = string.Format(rquery, rWhereClause);

                foreach (var item in rparams)
                {
                    var newKey = string.Format("d_{0}", item.Key);
                    query2 = query2.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }

                query += string.Format(" AND Id IN ({0})", query2);
            }

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					serviceTickets.Add(new ServiceTicketDashboardDto(reader));
			Connection.Close();
			return serviceTickets;
		}

        public List<ServiceTicketDashboardDto> FetchServiceTicketExludeAttachetToJob(ServiceTicketViewSearchCriteria criteria, long tenantId)
        {
            var serviceTickets = new List<ServiceTicketDashboardDto>();

            var query = @"SELECT Top 1000 * FROM ServiceTicketViewSearch(@TenantId, @ActiveUserId) WHERE JobId = '0' AND ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "ActiveUserId", criteria.ActiveUserId } };
            
            query += criteria.Parameters.BuildWhereClause(parameters, OperationsSearchFields.ServiceTickets);
            query += SearchUtilities.AddQuerySorting(criteria.SortBy, criteria.SortAscending);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    serviceTickets.Add(new ServiceTicketDashboardDto(reader));
            Connection.Close();
            return serviceTickets;
        }

        public List<ServiceTicket> FetchServiceTicketByExternalReference1(string externalReference1, long tenantId)
		{
			var serviceTickets = new List<ServiceTicket>();

			const string query = @"SELECT * FROM ServiceTicket WHERE ExternalReference1 = @ExternalReference1 AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "ExternalReference1", externalReference1 }, { "TenantId", tenantId }, };
		
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					serviceTickets.Add(new ServiceTicket(reader));
			Connection.Close();
			return serviceTickets;
		}

		public List<ServiceTicket> FetchServiceTicketByExternalReference2(string externalReference2, long tenantId)
		{
			var serviceTickets = new List<ServiceTicket>();

			const string query = @"SELECT * FROM ServiceTicket WHERE ExternalReference2 = @ExternalReference2 AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "ExternalReference2", externalReference2 }, { "TenantId", tenantId }, };

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					serviceTickets.Add(new ServiceTicket(reader));
			Connection.Close();
			return serviceTickets;
		}

		public ServiceTicket FetchServiceTicketByServiceTicketNumber(string number, long tenantId)
		{
			ServiceTicket serviceTicket = null;

			const string query = @"SELECT * FROM ServiceTicket WHERE ServiceTicketNumber = @ServiceTicketNumber AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "ServiceTicketNumber", number }, { "TenantId", tenantId }, };

			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					serviceTicket = new ServiceTicket(reader);
			Connection.Close();
			return serviceTicket;
		}

		public bool ServiceTicketExists(string number, long tenantId)
		{
			const string query = @"SELECT count(*) FROM ServiceTicket WHERE ServiceTicketNumber = @ServiceTicketNumber AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "ServiceTicketNumber", number }, { "TenantId", tenantId }, };
			return ExecuteScalar(query, parameters).ToInt() > 0;
		}

		public bool IsServiceTicketPresentOnInvoice(long invoiceId, string serviceTicketNumber, long tenantId)
		{
			const string query =
				@"SELECT COUNT(*) FROM InvoiceDetail WHERE ReferenceType = @Reference AND ReferenceNumber = @ServiceTicketNumber AND InvoiceId <> @InvoiceId";

			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Reference", DetailReferenceType.ServiceTicket},
				                 	{"TenantId", tenantId},
									{"ServiceTicketNumber", serviceTicketNumber},
				                 	{"InvoiceId", invoiceId},
			                 	};

			return ExecuteScalar(query, parameters).ToInt() > 0;
		}


		public List<ServiceTicket> FetchThem(long tenantId)
		{
			var q = "select * from ServiceTicket where ExternalReference1 <> '' and JobId = 0 and tenantId = @TenantId";

			var serviceTickets = new List<ServiceTicket>();

			var parameters = new Dictionary<string, object> {{ "TenantId", tenantId }, };

			using (var reader = GetReader(q, parameters))
				while (reader.Read())
					serviceTickets.Add(new ServiceTicket(reader));
			Connection.Close();
			return serviceTickets;
		}
	}
}