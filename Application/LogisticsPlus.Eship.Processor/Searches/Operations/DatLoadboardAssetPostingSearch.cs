﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
    public class DatLoadboardAssetPostingSearch : EntityBase
    {
        public DatLoadboardAssetPosting FetchDatLoadboardAssetPostingByIdNumber(string idNumber, long tenantId)
        {
            const string query = @"SELECT * FROM DatLoadboardAssetPosting WHERE IdNumber = @IdNumber AND TenantId = @TenantId ";

            var parameters = new Dictionary<string, object>
                {
                    { "TenantId", tenantId }, 
                    { "IdNumber", idNumber }
                };

            DatLoadboardAssetPosting posting = null;
            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    posting = new DatLoadboardAssetPosting(reader);
            Connection.Close();

            return posting;
        }

        public List<DatLoadboardAssetPosting> FetchAllDatLoadboardAssetPostings(long tenantId)
        {
            const string query = @"SELECT * FROM DatLoadboardAssetPosting WHERE TenantId = @TenantId";
            var postings = new List<DatLoadboardAssetPosting>();
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    postings.Add(new DatLoadboardAssetPosting(reader));
            Connection.Close();

            return postings;
        }

        public List<DatLoadboardAssetPosting> FetchDatLoadboardAssets(DatAssetSearchCriteria criteria, long tenantId)
        {
            var columns = criteria.Parameters;
            var query = @"SELECT * FROM DatLoadboardAssetPosting WHERE TenantId = @TenantId AND ";
            var assets = new List<DatLoadboardAssetPosting>();
            var parameter = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameter, OperationsSearchFields.DatLoadboardAssets);

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    assets.Add(new DatLoadboardAssetPosting(reader));
            Connection.Close();

            return assets;
        } 
    }
}
