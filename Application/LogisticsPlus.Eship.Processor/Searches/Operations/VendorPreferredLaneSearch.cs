﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Operations
{
    public class VendorPreferredLaneSearch : EntityBase
    {
        public List<VendorPreferredLaneViewSearchDto> FetchVendorPreferredLanes(List<ParameterColumn> columns, long tenantId)
		{
            var lanes = new List<VendorPreferredLaneViewSearchDto>();

			var query = @"SELECT * FROM VendorPreferredLaneViewSearch WHERE TenantId = @TenantId AND ";
			var parameters = new Dictionary<string, object> {{"TenantId", tenantId}};

            query += columns.BuildWhereClause(parameters, OperationsSearchFields.CarrierPreferredLanes);

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
                    lanes.Add(new VendorPreferredLaneViewSearchDto(reader));
			Connection.Close();
			return lanes;
		}
    }
}
