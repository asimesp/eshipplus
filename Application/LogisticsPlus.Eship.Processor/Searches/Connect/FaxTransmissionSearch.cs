﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Dto.Connect;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Connect
{
	public class FaxTransmissionSearch : EntityBase
	{
		public List<FaxTransmissionViewSearchDto> FetchFaxTransmissions(FaxTransmissionSearchCriteria criteria, long tenantId)
		{
			var transmissions = new List<FaxTransmissionViewSearchDto>();

			var query = @"SELECT * FROM FaxTransmissionViewSearch(@TenantId) WHERE ";

			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }};
			query += criteria.Parameters.BuildWhereClause(parameters, ConnectSearchFields.FaxTransmission);

			if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					transmissions.Add(new FaxTransmissionViewSearchDto(reader));
			Connection.Close();

			return transmissions;
		}

		public List<FaxTransmission> FetchAllFaxTransmissionWithNoResponse()
		{
			var transmissions = new List<FaxTransmission>();

			const string query = @"SELECT * FROM FaxTransmission WHERE ResponseDateTime = @ResponseDateTime";

			var parameters = new Dictionary<string, object> { { "ResponseDateTime", DateUtility.SystemEarliestDateTime } };

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					transmissions.Add(new FaxTransmission(reader));
			Connection.Close();

			return transmissions;
		}
        
		public FaxTransmission FetchFaxTransmission(Guid key)
		{
			const string query = "Select * from FaxTransmission where TransmissionKey = @Key";
			var parameter = new Dictionary<string, object> {{"Key", key}};
			FaxTransmission transmission = null;
			using (var reader = GetReader(query, parameter))
				if (reader.Read())
					transmission = new FaxTransmission(reader);
			Connection.Close();
			return transmission;
		}
	}
}
