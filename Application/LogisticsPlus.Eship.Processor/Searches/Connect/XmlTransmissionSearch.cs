﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Connect
{
	public class XmlTransmissionSearch : EntityBase
	{
		public List<XmlTransmission> FetchXmlTransmissions(XmlTransmissionViewSearchCriteria criteria, long tenantId)
		{
			var transmissions = new List<XmlTransmission>();

			const string _query =
				@"SELECT * FROM XmlTransmission 
										WHERE (ReferenceNumber LIKE @Criteria OR AcknowledgementMessage LIKE @Criteria OR DocumentType LIKE @Criteria)
											AND (@StartSendDateTime <= TransmissionDateTime AND TransmissionDateTime <= @EndSendDateTime)
											AND (AcknowledgementDateTime = '1753-01-01 00:00:00' OR (@StartAcknowledgementDateTime <= AcknowledgementDateTime AND AcknowledgementDateTime <= @EndAcknowledgementDateTime))
											AND (@SendOkay = -1  OR SendOkay = @SendOkay)
											AND (@Direction = -1 OR Direction = @Direction)
											AND TenantId = @TenantId";

			var parameter = new Dictionary<string, object>
			                	{
			                		{ "Criteria", criteria.Criteria }, 
			                		{ "StartSendDateTime", criteria.StartSendDateTime }, 
			                		{ "EndSendDateTime", criteria.EndSendDateTime }, 
									{ "StartAcknowledgementDateTime", criteria.StartAcknowledgementDateTime }, 
			                		{ "EndAcknowledgementDateTime", criteria.EndAcknowledgementDateTime }, 
			                		{ "SendOkay", criteria.SendOkay }, 
									{ "Direction" , criteria.Direction},
									{ "TenantId", tenantId }
			                	};

			using (var reader = GetReader(_query, parameter))
				while (reader.Read())
					transmissions.Add(new XmlTransmission(reader));
			Connection.Close();

			return transmissions;
		}

		public XmlTransmission FetchXmlTransmission(Guid key)
		{
			const string query = "SELECT * FROM XmlTransmission WHERE TransmissionKey = @Key";
			var parameter = new Dictionary<string, object> { { "Key", key } };
			XmlTransmission transmission = null;
			using (var reader = GetReader(query, parameter))
				if (reader.Read())
					transmission = new XmlTransmission(reader);
			Connection.Close();
			return transmission;
		}
	}
}