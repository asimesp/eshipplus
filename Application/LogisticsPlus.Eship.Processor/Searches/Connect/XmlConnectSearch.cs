﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Dto.Connect;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Connect
{
	public class XmlConnectSearch : EntityBase
	{
		public List<XmlConnectViewSearchDto> FetchXmlConnectRecords(XmlConnectSearchCriteria criteria, long tenantId)
		{
			var connects = new List<XmlConnectViewSearchDto>();

			var query = @"SELECT * FROM XmlConnectViewSearch(@TenantId, @ActiveUserId) WHERE ";

			var parameters = new Dictionary<string, object> {{"TenantId", tenantId}, {"ActiveUserId", criteria.ActiveUserId}};
			query += criteria.Parameters.BuildWhereClause(parameters, ConnectSearchFields.XmlConnect);

			if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					connects.Add(new XmlConnectViewSearchDto(reader));
			Connection.Close();

			return connects;
		}


		public List<XmlConnect> FetchOutBoundXmlConnectBySetCtrlNumAndDocType(string number, EdiDocumentType documentType, long tenantId, long userId)
		{
			var connectRecords = new List<XmlConnect>();

			const string query = @"SELECT * FROM XmlConnectViewSearch(@TenantId, @ActiveUserId) WHERE ControlNumber = @ControlNumber And DocumentType = @DocumentType and Direction = @Direction";

			var parameters = new Dictionary<string, object>
				{
					{"TenantId", tenantId},
					{"ActiveUserId", userId},
					{"ControlNumber", number},
					{"DocumentType", documentType.ToInt()},
					{"Direction", Direction.Outbound.ToInt()},
				};

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					connectRecords.Add(new XmlConnect(reader));
			Connection.Close();

			return connectRecords;
		}

		public LoadTenderDto FetchOutBoundLoadTenderByVendShipIdNumAndDocType(string vendorNumber, string shipmentIdNumber, long tenantId, long userId)
		{
			if (string.IsNullOrEmpty(vendorNumber)) return null; // for outbound, there must be a vendor!


			const string query = @"SELECT * FROM LoadTenderViewSearch(@TenantId, @ActiveUserId) WHERE VendorNumber = @VendorNumber And ShipmentIdNumber = @ShipmentIdNumber";

			var parameters = new Dictionary<string, object>
				{
					{"TenantId", tenantId},
					{"ActiveUserId", userId},
					{"VendorNumber", vendorNumber},
					{"ShipmentIdNumber", shipmentIdNumber},
				};
			LoadTenderDto dto = null;
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					dto = new LoadTenderDto(reader);
			Connection.Close();
			
			return dto;
		}


		public XmlConnect FetchInboundLoadTenderByShipmentIdNumber(string number, long tenantId, long userId, string customerNumber = "")
		{
			return FetchInboundXmlConnectByShipmentIdNumberAndDocumentType(number, EdiDocumentType.EDI204, tenantId, userId, customerNumber);
		}

		public XmlConnect FetchInboundLoadTenderResponseByShipmentIdNumber(string number, long tenantId, long userId, string customerNumber = "")
		{
			return FetchInboundXmlConnectByShipmentIdNumberAndDocumentType(number, EdiDocumentType.EDI990, tenantId, userId, customerNumber);
		}


		public XmlConnect FetchOutboundLoadTenderResponseByShipmentIdNumber(string number, long tenantId, long userId, string vendorNumber)
		{
			var dto = FetchOutBoundLoadTenderByVendShipIdNumAndDocType(vendorNumber, number, tenantId, userId);

			return dto == null ? null : dto.TenderResponse;
		}



		private XmlConnect FetchInboundXmlConnectByShipmentIdNumberAndDocumentType(string number, EdiDocumentType documentType, long tenantId, long userId, string customerNumber = "")
		{
			// NOTE: number is unique across system for outbound load tenders responses but unique by customer for inbound load tenders response

			XmlConnect connect = null;

			var query = @"SELECT * FROM XmlConnectViewSearch(@TenantId, @ActiveUserId) WHERE ShipmentIdNumber = @ShipmentIdNumber And DocumentType = @DocumentType";

			var parameters = new Dictionary<string, object>
				{
					{"TenantId", tenantId},
					{"ActiveUserId", userId},
					{"ShipmentIdNumber", number},
					{"DocumentType", documentType.ToInt()},
					{"Direction", Direction.Inbound.ToInt()},
				};

			if (!string.IsNullOrEmpty(customerNumber))
			{
				parameters.Add("CustomerNumber", customerNumber);
				query += " And CustomerNumber = @CustomerNumber";
			}

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					connect = new XmlConnect(reader);
			Connection.Close();

			return connect;
		}
	}
}