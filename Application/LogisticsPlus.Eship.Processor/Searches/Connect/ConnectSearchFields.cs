﻿using System.Collections.Generic;
using System.Data;

namespace LogisticsPlus.Eship.Processor.Searches.Connect
{
    public static class ConnectSearchFields
    {
        private static readonly object LockObj = new object();

        private static bool _isInitialized;

	    public static SearchField SendDateTime;
	    public static SearchField ResponseDateTime;
	    public static SearchField LastStatusCheckDateTime;
	    public static SearchField StatusText;
		public static SearchField ServiceProviderText;
        public static SearchField ShipmentIdNumber;
	    public static SearchField ShipmentNumber;
        public static SearchField ExpirationDate;
        public static SearchField DateCreated;
        public static SearchField Direction;
        public static SearchField PurchaseOrderNumber;
        public static SearchField ShipperReference;
        public static SearchField TotalWeight;
        public static SearchField DocumentType;

        public static SearchField IdNumber;
        public static SearchField StartTrackDateTime;
        public static SearchField DateStopRequested;
        public static SearchField TrackCost;
        public static SearchField StatusDescription;
        public static SearchField Username;
        public static SearchField TrackDurationHours;
        public static SearchField TrackIntervalMinutes;
        public static SearchField NumberType;
        public static SearchField Number;

        // ------------------------------------------------

        public static List<SearchField> DocDeliveryLogs { get; set; }
        public static List<SearchField> MacroPoint { get; set; } 
        public static List<SearchField> XmlConnect { get; set; }
        public static List<SearchField> FaxTransmission { get; set; }
        
        // ------------------------------------------------

        public static List<SearchField> DefaultDocDeliveryLogs { get; set; }
        public static List<SearchField> DefaultMacroPoint { get; set; } 
        public static List<SearchField> DefaultXmlConnect { get; set; }
        public static List<SearchField> DefaultFaxTransmission { get; set; }

        // ------------------------------------------------

		public static List<SearchField> XmlConnectSortFields { get; set; }
		public static List<SearchField> FaxTransmissionSortFields { get; set; }
        public static List<SearchField> MacroPointSortFields { get; set; }

        // ------------------------------------------------

        public static void Initialize()
        {
            if (_isInitialized) return;

            var anotherLockInitialized = false;
            lock (LockObj)
            {
                if (!_isInitialized) _isInitialized = true;
                else anotherLockInitialized = true;
            }

            if (anotherLockInitialized) return;

	        SendDateTime = new SearchField {Name = "SendDateTime", DataType = SqlDbType.DateTime};
            ShipmentIdNumber = new SearchField {Name = "ShipmentIdNumber", DataType = SqlDbType.NVarChar};
	        ShipmentNumber = new SearchField {Name = "ShipmentNumber", DataType = SqlDbType.NVarChar};
            ExpirationDate = new SearchField {Name = "ExpirationDate", DataType = SqlDbType.DateTime};
            DateCreated = new SearchField {Name = "DateCreated", DataType = SqlDbType.DateTime};
            Direction = new SearchField {Name = "DirectionText", DataType = SqlDbType.NVarChar};
            PurchaseOrderNumber = new SearchField {Name = "PurchaseOrderNumber", DataType = SqlDbType.NVarChar};
            ShipperReference = new SearchField {Name = "ShipperReference", DataType = SqlDbType.NVarChar};
            TotalWeight = new SearchField {Name = "TotalWeight", DataType = SqlDbType.NVarChar};
            DocumentType = new SearchField {Name = "DocumentTypeText", DataType = SqlDbType.NVarChar};
	        ResponseDateTime = new SearchField {Name = "ResponseDateTime", DataType = SqlDbType.DateTime};
	        LastStatusCheckDateTime = new SearchField {Name = "LastStatusCheckDateTime", DataType = SqlDbType.DateTime};
	        StatusText = new SearchField {Name = "StatusText", DataType = SqlDbType.NVarChar};
			ServiceProviderText = new SearchField { Name = "ServiceProviderText", DataType = SqlDbType.NVarChar };

            IdNumber = new SearchField { Name = "IdNumber", DataType = SqlDbType.NVarChar };
            StartTrackDateTime = new SearchField { Name = "StartTrackDateTime", DataType = SqlDbType.DateTime };
            DateStopRequested = new SearchField { Name = "DateStopRequested", DataType = SqlDbType.DateTime };
            TrackCost = new SearchField { Name = "TrackCost", DataType = SqlDbType.Decimal };
            StatusDescription = new SearchField { Name = "TrackingStatusDescription", DataType = SqlDbType.NVarChar };
            Username = new SearchField { Name = "Username", DataType = SqlDbType.NVarChar };
            TrackDurationHours = new SearchField { Name = "TrackDurationHours", DataType = SqlDbType.Int };
            TrackIntervalMinutes = new SearchField { Name = "TrackIntervalMinutes", DataType = SqlDbType.Int };
            NumberType = new SearchField {Name = "NumberType", DataType = SqlDbType.NVarChar};
            Number = new SearchField {Name = "Number", DataType = SqlDbType.NVarChar};
            // ------------------------------------------------

            DefaultDocDeliveryLogs = new List<SearchField>
                                         {
                                            new SearchField {Name = "EntityTypeText", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DocumentName", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DocumentTagCode", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DocumentTagDescription", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "LogDateTime", DataType = SqlDbType.DateTime},
                                            };

	        DefaultFaxTransmission = new List<SearchField>
		        {
			        ShipmentNumber,
			        SendDateTime,
			        StatusText,
		        };

            DefaultMacroPoint = new List<SearchField>
                {
                    IdNumber,
                    DateCreated
                };

	        DefaultXmlConnect = new List<SearchField>
		        {
			        new SearchField {Name = "ShipmentIdNumber", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "OriginCountryCode", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "DestinationCountryName", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "DestinationCountryCode", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "DateCreated", DataType = SqlDbType.DateTime},
			        new SearchField {Name = "DirectionText", DataType = SqlDbType.NVarChar},
			        new SearchField {Name = "DocumentTypeText", DataType = SqlDbType.NVarChar},
		        };


	        // ------------------------------------------------

	        FaxTransmissionSortFields = new List<SearchField>
		        {
			        ShipmentNumber,
			        SendDateTime,
			        new SearchField {Name = "StatusText", DataType = SqlDbType.NVarChar},
			        ResponseDateTime,
			        LastStatusCheckDateTime,
					ServiceProviderText,
		        };

            MacroPointSortFields = new List<SearchField>
                {
                    IdNumber,
                    Number,
                    NumberType,
                    DateCreated,
                    StartTrackDateTime,
                    DateStopRequested,
                    TrackCost,
                    StatusDescription,
                    Username,
                    TrackDurationHours,
                    TrackIntervalMinutes,
                };

	        XmlConnectSortFields = new List<SearchField>
                                         {
                                            ShipmentIdNumber,
                                            new SearchField {Name = "OriginStreet1", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginStreet2", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginCountryName", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginCountryCode", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
											new SearchField {Name = "DestinationStreet1", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationStreet2", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationCountryName", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationCountryCode", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
                                            ExpirationDate,
                                            DateCreated,
                                            new SearchField {Name = "ReceiptDate", DataType = SqlDbType.DateTime},
                                            Direction,
                                            new SearchField {Name = "CustomerNumber", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "VendorNumber", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "VendorScac", DataType = SqlDbType.NVarChar},
                                            PurchaseOrderNumber,
                                            ShipperReference,
                                            new SearchField {Name = "EquipmentDescriptionCode", DataType = SqlDbType.NVarChar},
                                            DocumentType,
											new SearchField {Name = "StatusText", DataType = SqlDbType.NVarChar},
                                            TotalWeight
                                            };

	        // ------------------------------------------------

            DocDeliveryLogs = new List<SearchField>
			           	{
			           		new SearchField {Name = "EntityTypeText", DataType = SqlDbType.NVarChar },
			           		new SearchField {Name = "DocumentName", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "LogDateTime", DataType = SqlDbType.DateTime},
			           		new SearchField {Name = "DeliveryWasSuccessful", DataType = SqlDbType.Bit},
			           		new SearchField {Name = "FailedDeliveryMessage", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "DocumentTagCode", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "DocumentTagDescription", DataType = SqlDbType.NVarChar},
			           	};

	        FaxTransmission = new List<SearchField>
		        {
			        ShipmentNumber,
			        SendDateTime,
			        StatusText,
			        new SearchField {Name = "Message", DataType = SqlDbType.NVarChar},
			        ResponseDateTime,
			        LastStatusCheckDateTime,
			        new SearchField {Name = "Resolved", DataType = SqlDbType.Bit},
			        new SearchField {Name = "ResolutionComment", DataType = SqlDbType.NVarChar},
					ServiceProviderText,
		        };

            MacroPoint = new List<SearchField>
                {
                    new SearchField {Name = "TrackingStatusCode", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "UserFirstName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "UserLastName", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "Notes", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "EmailCopiesOfUpdatesTo", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
                };

            MacroPoint.AddRange(MacroPointSortFields);

            XmlConnect = new List<SearchField>
                                         {
                                            ShipmentIdNumber,
                                            new SearchField {Name = "ControlNumber", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginStreet1", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginStreet2", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginCity", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginState", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginCountryName", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginCountryCode", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "OriginPostalCode", DataType = SqlDbType.NVarChar},
											new SearchField {Name = "DestinationStreet1", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationStreet2", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationCity", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationState", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationCountryName", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationCountryCode", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "DestinationPostalCode", DataType = SqlDbType.NVarChar},
                                            ExpirationDate,
                                            DateCreated,
                                            new SearchField {Name = "ReceiptDate", DataType = SqlDbType.DateTime},
                                            Direction,
                                            new SearchField {Name = "CustomerNumber", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "VendorNumber", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "VendorPro", DataType = SqlDbType.NVarChar},
                                            new SearchField {Name = "VendorScac", DataType = SqlDbType.NVarChar},
                                            PurchaseOrderNumber,
                                            ShipperReference,
                                            new SearchField {Name = "EquipmentDescriptionCode", DataType = SqlDbType.NVarChar},
                                            DocumentType,
											new SearchField {Name = "StatusText", DataType = SqlDbType.NVarChar},
                                            };
        }
    }
}