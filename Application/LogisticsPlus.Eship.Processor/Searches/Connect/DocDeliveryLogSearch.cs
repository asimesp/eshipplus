﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Processor.Dto.Connect;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Connect
{
    public class DocDeliveryLogSearch : EntityBase
    {
        public List<DocDeliveryLogDto> FetchDocDeliveryLogs(DocDeliveryLogSearchCriteria criteria, long tenantId)
        {
            var invoices = new List<DocDeliveryLogDto>();
            var query = @"SELECT * FROM DocDeliveryLogViewSearch WHERE ";

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }};
            query += criteria.Parameters.BuildWhereClause(parameters, ConnectSearchFields.DocDeliveryLogs);
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    invoices.Add(new DocDeliveryLogDto(reader));
            Connection.Close();
            return invoices;  
        }
    }
}