﻿using System.Collections.Generic;
using System.Data;

namespace LogisticsPlus.Eship.Processor.Searches.Administration
{
    public static class AdminSearchFields
    {
        private static readonly object LockObj = new object();

        private static bool _isInitialized;

        // static SearchFields
        public static SearchField CountryCode;
        public static SearchField CountryName;
        public static SearchField CustomerName;
        public static SearchField CustomerNumber;
        public static SearchField FirstName;
        public static SearchField LastName;
        public static SearchField Username;
		public static SearchField TenantEmployee;
		public static SearchField IsCarrierCoordinator;
		public static SearchField IsShipmentCoordinator;

        // ------------------------------------------------

        // Default SearchField lists
        public static List<SearchField> DefaultAdminUsers { get; set; }
        public static List<SearchField> DefaultGroups { get; set; }
        public static List<SearchField> DefaultTenants { get; set; }
        public static List<SearchField> DefaultUsers { get; set; }
        // ------------------------------------------------

        // Regular SearchField lists 
        public static List<SearchField> AdminUsers { get; set; }
        public static List<SearchField> Groups { get; set; }
        public static List<SearchField> Tenants { get; set; }
        public static List<SearchField> Users { get; set; }
        // ------------------------------------------------

        // Sorting Searchfield lists
        // ------------------------------------------------

        public static List<SearchField> UserSortFields { get; set; }


        public static void Initialize()
        {
            if (_isInitialized) return;

            var anotherLockInitialized = false;
            lock (LockObj)
            {
                if (!_isInitialized) _isInitialized = true;
                else anotherLockInitialized = true;
            }

            if (anotherLockInitialized) return;

            CountryCode = new SearchField { Name = "CountryCode", DisplayName = "Country Code", DataType = SqlDbType.NVarChar };
            CountryName = new SearchField { Name = "CountryName", DisplayName = "Country Name", DataType = SqlDbType.NVarChar };
            CustomerName = new SearchField { Name = "CustomerName", DisplayName = "Customer Name", DataType = SqlDbType.NVarChar };
            CustomerNumber = new SearchField { Name = "CustomerNumber", DisplayName = "Customer Number",DataType = SqlDbType.NVarChar };
            FirstName = new SearchField { Name = "FirstName", DisplayName = "User First Name", DataType = SqlDbType.NVarChar };
            LastName = new SearchField { Name = "LastName", DisplayName = "User Last Name", DataType = SqlDbType.NVarChar };
            Username = new SearchField { Name = "Username", DataType = SqlDbType.NVarChar };
        	TenantEmployee = new SearchField {Name = "TenantEmployee", DataType = SqlDbType.Bit, DisplayName = "Employee"};
			IsCarrierCoordinator = new SearchField { Name = "IsCarrierCoordinator", DataType = SqlDbType.Bit, DisplayName = "Carrier Coordinator" };
			IsShipmentCoordinator = new SearchField { Name = "IsShipmentCoordinator", DataType = SqlDbType.Bit, DisplayName = "Shipment Coordinator" };


            // ------------------------------------------------

            DefaultAdminUsers = new List<SearchField>
                                {
                                    FirstName,
                                    LastName,
									Username
                                };

            DefaultGroups = new List<SearchField>
                                {
                                    new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},  
                                    new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},  
                                };

            DefaultTenants = new List<SearchField>
                                {
                                    new SearchField {Name = "Code", DataType = SqlDbType.NVarChar},  
                                    new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},  
                                };

            DefaultUsers = new List<SearchField>
                                {
                                    FirstName,
                                    LastName, 
									Username
                                };
            // ------------------------------------------------


            AdminUsers = new List<SearchField>
                                       {
                                           Username,
                                           FirstName,
                                           LastName,
                                           new SearchField {Name = "Email", DataType = SqlDbType.NVarChar},
                                       };

            Groups = new List<SearchField>
                                       {
                                           new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},
                                           Username,
                                           FirstName,
                                           LastName
                                       };

            Tenants = new List<SearchField>
                                       {
                                           new SearchField {Name = "Code", DataType = SqlDbType.NVarChar},  
                                           new SearchField {Name = "Name", DataType = SqlDbType.NVarChar}, 
                                           new SearchField {Name = "Active", DataType = SqlDbType.Bit}, 
                                       };

        	Users = new List<SearchField>
        	        	{
        	        		FirstName,
        	        		LastName,
        	        		Username,
        	        		new SearchField {Name = "Phone", DataType = SqlDbType.NVarChar},
        	        		new SearchField {Name = "Fax", DataType = SqlDbType.NVarChar},
        	        		new SearchField {Name = "Mobile", DataType = SqlDbType.NVarChar},
        	        		new SearchField {Name = "Email", DataType = SqlDbType.NVarChar},
        	        		new SearchField {Name = "Street1", DataType = SqlDbType.NVarChar},
        	        		new SearchField {Name = "Street2", DataType = SqlDbType.NVarChar},
        	        		new SearchField {Name = "City", DataType = SqlDbType.NVarChar},
        	        		new SearchField {Name = "State", DataType = SqlDbType.NVarChar},
        	        		new SearchField {Name = "PostalCode", DataType = SqlDbType.NVarChar},
                            new SearchField {Name = "AdUserName", DisplayName = "Active Directory User Name",  DataType = SqlDbType.NVarChar},
        	        		IsCarrierCoordinator,
        	        		IsShipmentCoordinator,
        	        		TenantEmployee,
        	        		CountryCode,
        	        		CountryName,
        	        		CustomerNumber,
        	        		CustomerName
        	        	};

            UserSortFields = new List<SearchField>
        	        	{
        	        		FirstName,
        	        		LastName,
        	        		Username,
        	        	};
        	// ------------------------------------------------
        }

    }
}