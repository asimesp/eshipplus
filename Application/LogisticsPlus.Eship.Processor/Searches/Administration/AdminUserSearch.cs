﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Administration
{
    public class AdminUserSearch : EntityBase
    {
        public AdminUser FetchUserByUsername(string userName)
        {
            const string query = @"SELECT * FROM AdminUser WHERE Username = @Username";
            var parameters = new Dictionary<string, object> { { "Username", userName } };
            AdminUser user = null;
            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    user = new AdminUser(reader);
            Connection.Close();
            return user;
        }

        public List<AdminUser> FetchAdminUsers(List<ParameterColumn> columns)
        {
            var users = new List<AdminUser>();
            
            var parameters = new Dictionary<string, object>();
            
            var query = @"SELECT * FROM AdminUser WHERE ";
            query += columns.BuildWhereClause(parameters, AdminSearchFields.AdminUsers);

            query += string.Format("Order by Username {0}, FirstName {0}, Lastname {0}", SearchUtilities.Ascending);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    users.Add(new AdminUser(reader));
            Connection.Close();

            return users;
        }
    }
}
