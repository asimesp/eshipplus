﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Administration;
using LogisticsPlus.Eship.Processor.Views.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Administration
{
    public class UserSearch : EntityBase
    {
        public List<UserDto> FetchUsers(UserSearchCriteria criteria, long tenantId)
        {
            var users = new List<UserDto>();
            var columns = criteria.Parameters;


            var query = @"SELECT * FROM UserViewSearch WHERE TenantId = @TenantId AND ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            var baseCountryCols = new List<SearchField>
                                   {
                                       AdminSearchFields.CountryCode,
                                       AdminSearchFields.CountryName,
                                   }.ToParameterColumn()
                .Select(c => c.ReportColumnName)
                .ToList();

        	var countryCols = columns.Where(c => baseCountryCols.Contains(c.ReportColumnName)).ToList();
            var uCols = columns.Where(c => !baseCountryCols.Contains(c.ReportColumnName)).ToList();

            query += uCols.BuildWhereClause(parameters, AdminSearchFields.Users);

            if (countryCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var cQuery = "SELECT Id FROM Country WHERE ";

                cQuery += countryCols.BuildWhereClause(uParams, AdminSearchFields.Users);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    cQuery = cQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND (CountryId IN ({0}))", cQuery);
            }

            query += criteria.SortBy != null
                         ? string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name,
                                         criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending)
                         : string.Format("Order By Username {0}, FirstName {0}, Lastname {0}", SearchUtilities.Ascending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    users.Add(new UserDto(reader));
            Connection.Close();
            return users;
        }

		public List<UserDto> FetchUsersByCriteriaWithSearchDefaults(long tenantId, string criteria, Operator op)
		{
			var users = new List<UserDto>();

			var query = @"SELECT * FROM UserViewSearch WHERE TenantId = @TenantId AND ({0})";

			var parameters = new Dictionary<string, object>
        	                 	{
        	                 		{"Criteria", criteria},
        	                 		{"TenantId", tenantId},
        	                 	};
			var defaultParams = AdminSearchFields.DefaultUsers
			                                     .Select(sf =>
				                                     {
					                                     var c = sf.ToParameterColumn();
					                                     c.Operator = op;
					                                     c.DefaultValue = criteria;
					                                     return c;
				                                     })
			                                     .ToList();
			foreach (var p in defaultParams)
			{
				p.DefaultValue = criteria;
				p.DefaultValue2 = string.Empty;
				p.Operator = op;
			}
			query = string.Format(query, defaultParams.BuildWhereClause(parameters, AdminSearchFields.Users).Replace(" AND ", " OR "));
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					users.Add(new UserDto(reader));
			Connection.Close();

			return users;
		}

		public User FetchUserByUsernameAndAccessCode(string userName, string tenantCode)
		{
			const string query =
				@"SELECT [User].* FROM [User],Tenant WHERE [User].TenantId = Tenant.Id AND [User].Username = @Username AND Tenant.Code = @TenantCode";

			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Username", userName},
			                 		{"TenantCode", tenantCode},
			                 	};

			User user = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					user = new User(reader);
			Connection.Close();

			return user;
		}

        public User FetchUserByAdUsername(string userName, string tenantCode)
        {
            const string query = @"SELECT [User].* FROM [User],Tenant WHERE [User].AdUsername = @AdUsername AND [User].TenantId = Tenant.Id AND Tenant.Code = @TenantCode";

            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"AdUsername", userName},
			                 		{"TenantCode", tenantCode},
			                 	};

            User user = null;
            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    user = new User(reader);
            Connection.Close();

            return user;
        }
    }
}
