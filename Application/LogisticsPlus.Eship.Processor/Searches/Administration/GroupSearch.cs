﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Administration
{
	public class GroupSearch : EntityBase
	{
	    public List<Group> FetchGroups(List<ParameterColumn> columns , long tenantId)
        {
            var groups = new List<Group>();

            var query = @"SELECT * FROM [Group] WHERE TenantId = @TenantId AND ";
            
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            var baseUserCols = new List<SearchField>
                                   {
                                       AdminSearchFields.Username,
                                       AdminSearchFields.LastName,
                                       AdminSearchFields.FirstName
                                   }.ToParameterColumn()
                .Select(c => c.ReportColumnName)
                .ToList();

            var userCols = columns.Where(c => baseUserCols.Contains(c.ReportColumnName)).ToList();
            var gCols = columns.Where(c => !baseUserCols.Contains(c.ReportColumnName)).ToList();

            query += gCols.BuildWhereClause(parameters, AdminSearchFields.Groups);


            if (userCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var uQuery = @"SELECT
		                        DISTINCT GroupId 
	                        FROM 
		                        GroupUser,
		                        [User] 
	                        WHERE
                                GroupUser.UserId = [User].Id
		                        AND  ";

                uQuery += userCols.BuildWhereClause(uParams, AdminSearchFields.Groups);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    uQuery = uQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND [Group].Id IN ({0})", uQuery);
            }

            query += string.Format("ORDER BY [Name] {0}", SearchUtilities.Ascending);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    groups.Add(new Group(reader));
            Connection.Close();

            return groups;
        }
	}
}
