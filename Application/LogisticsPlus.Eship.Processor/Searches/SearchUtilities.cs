﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Reports;
using ObjToSql.Core;
using System.Data.Common;

namespace LogisticsPlus.Eship.Processor.Searches
{
	public static class SearchUtilities
	{
	    public static string WildCard = "%";
	    public static string Ascending = "ASC";
	    public static string Descending = "DESC";

		public static ParameterColumn ToParameterColumn(this SearchField field)
		{
			return new ParameterColumn
			       	{
			       		ReportColumnName = field.DisplayName,
			       		Operator = field.DataType.SearchDefaultParameter(),
						DefaultValue = string.Empty,
						DefaultValue2 = string.Empty
			       	};
		}

		public static List<ParameterColumn>  ToParameterColumn(this IEnumerable<SearchField> fields)
		{
			return fields.Select(f => f.ToParameterColumn()).ToList();
		}

		public static string BuildWhereClause(this List<ParameterColumn> parameterColumns, IDictionary<string, object> parameters, IEnumerable<SearchField> fields)
		{
			// build where clause
			var whereClause = parameterColumns.GetSqlParameters(parameters);
			if (parameterColumns.Any())
				foreach (var field in fields)
					if (whereClause.Contains(field.DisplayName.WrapBraces()))
						whereClause = whereClause.Replace(field.DisplayName.WrapBraces(), field.Name.WrapBraces());
			return whereClause;
		}

		public static Operator SearchDefaultParameter(this SqlDbType type)
		{
			switch (type)
			{
				case SqlDbType.NVarChar:
					return Operator.Contains;
				case SqlDbType.DateTime:
					return Operator.Between;
				default:
					return Operator.Equal;
			}
		}

		public static string WrapWithSqlWildCard(this string value)
		{
			return string.Format("%{0}%", value ?? string.Empty);
		}

        public static string AddQueryCriteriaParameters(List<ParameterColumn> criteriaParameters, Dictionary<string, object> parameters, List<SearchField> searchFields)
        {
            return criteriaParameters.BuildWhereClause(parameters, searchFields); ;
        }

        public static string AddQuerySorting(SearchField searchField, bool sortAscending)
        {
            return (searchField != null)
                       ? string.Format(" ORDER BY {0} {1}", searchField.Name, sortAscending ? Ascending : Descending)
                       : string.Empty;
        }
    }
}
