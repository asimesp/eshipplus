﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Views.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Core
{
    public class AnnouncementSearch : EntityBase
    {
        public List<Announcement> FetchAnnouncements(AnnouncementViewSearchCriteria criteria, long tenantId)
        {
            var announcements = new List<Announcement>();

            var query = "SELECT * FROM AnnouncementViewSearch WHERE TenantId = @TenantId AND ";
        	var parameters = new Dictionary<string, object>{{"TenantId", tenantId},};

            query += criteria.Parameters.BuildWhereClause(parameters, CoreSearchFields.Announcements);
        	
        	using (var reader = GetReader(query, parameters))
				while (reader.Read()) 
                    announcements.Add(new Announcement(reader));
            Connection.Close();
            return announcements;
        }

        public List<Announcement> FetchEnabledAnnouncements (long tenantId)
        {
            var announcements = new List<Announcement>();

        	const string query = @"Select * from Announcement where TenantId = @TenantId AND Enabled = @Enabled";

        	var parameter = new Dictionary<string, object>
        	                	{
        	                		{"TenantId", tenantId},
        	                		{"Enabled", true}
        	                	};

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    announcements.Add(new Announcement(reader));
            Connection.Close();

            return announcements;
        }
    }
}
