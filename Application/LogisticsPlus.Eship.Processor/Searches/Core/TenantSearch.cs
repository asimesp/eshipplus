﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Core
{
	public class TenantSearch : EntityBase
	{
	    public List<Tenant> FetchTenants(List<ParameterColumn> columns)
        {
            var tenants = new List<Tenant>();

            var query = @"SELECT * FROM Tenant WHERE ";
            var parameters = new Dictionary<string, object> ();

            query += columns.BuildWhereClause(parameters, AdminSearchFields.Tenants);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    tenants.Add(new Tenant(reader));
            Connection.Close();

            return tenants;
        }

        public Tenant FetchTenantByCode(string code)
        {
            var tenant = new Tenant();

            const string query = @"SELECT * FROM Tenant WHERE Code = @Code";

            var parameters = new Dictionary<string, object> { { "Code", code } };

            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    tenant = new Tenant(reader);
            Connection.Close();

            return tenant;
        }
    }
}
