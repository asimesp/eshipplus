﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Views.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Core
{
    public class AdminAuditLogSearch : EntityBase
    {
        public List<AuditLogsViewSearchDto> FetchEntityAuditLogs(string entityCode, object entityId)
        {
            var logs = new List<AuditLogsViewSearchDto>();
            var query =
                string.Format(@"SELECT al.Id, al.EntityCode, al.EntityId, al.Description, al.LogDateTime, au.FirstName,  au.LastName
                    FROM AdminAuditLog AS al 
						INNER JOIN	AdminUser AS au ON al.AdminUserId = au.Id
					WHERE al.EntityCode = @EntityCode 
						AND al.EntityId = @EntityId
                    ORDER BY al.LogDateTime {0}", SearchUtilities.Descending);

            var parameter = new Dictionary<string, object>
			                	{
			                		{"EntityCode", entityCode},
			                		{"EntityId", entityId}
			                	};

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    logs.Add(new AuditLogsViewSearchDto(reader));
            Connection.Close();
            return logs;
        }

        public List<AuditLogsViewSearchDto> FetchAdminAuditLogs(AdminAuditLogSearchCriteria criteria)
        {
            var logs = new List<AuditLogsViewSearchDto>();
            var query = @"Select Top 10000 * From AdminAuditLogsViewSearch Where";

            var parameters = new Dictionary<string, object>();
            query += criteria.Parameters.BuildWhereClause(parameters, CoreSearchFields.AdminAuditLogs);
            query += string.Format("ORDER BY LogDateTime {0}", SearchUtilities.Descending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    logs.Add(new AuditLogsViewSearchDto(reader));
            Connection.Close();
            return logs;
        }
    }
}
