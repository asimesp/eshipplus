﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Core
{
	public class ProcQueObjSearch : EntityBase
	{
		public List<ProcQueObj> FetchQue()
		{
			const string query = @"SELECT* FROM ProcQueObj";
			var objs = new List<ProcQueObj>();
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new ProcQueObj(reader));
			Connection.Close();

			return objs;
		}
	}
}
