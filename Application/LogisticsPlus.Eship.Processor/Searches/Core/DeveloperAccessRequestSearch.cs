﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Core
{
    public class DeveloperAccessRequestSearch : EntityBase
    {
        public List<DeveloperAccessRequest> FetchRequests(List<ParameterColumn> columns , long tenantId)
        {
            var requests = new List<DeveloperAccessRequest>();

            var query = @"SELECT * FROM DeveloperAccessRequest WHERE @TenantId = TenantId AND ";

            var parameters = new Dictionary<string, object>
        	                	{{"TenantId", tenantId}};

            var baseCustomerCols = new List<SearchField>
                                   {
                                       CoreSearchFields.NonAliasedCustomerName,
                                       CoreSearchFields.CustomerNumber,
                                   }.ToParameterColumn()
               .Select(c => c.ReportColumnName)
               .ToList();

            var customerCols = columns.Where(c => baseCustomerCols.Contains(c.ReportColumnName)).ToList();
            var darCols = columns.Where(c => !baseCustomerCols.Contains(c.ReportColumnName)).ToList();

            query += darCols.BuildWhereClause(parameters, CoreSearchFields.DeveloperAccessRequests);

            if (customerCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var uQuery = "SELECT Id FROM Customer WHERE ";

                uQuery += customerCols.BuildWhereClause(uParams, CoreSearchFields.DeveloperAccessRequests);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    uQuery = uQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND (CustomerId IN ({0}))", uQuery);
            }

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    requests.Add(new DeveloperAccessRequest(reader));
            Connection.Close();
            return requests;
        }

        public DeveloperAccessRequest FetchRequest(long customerId, long tenantId)
        {
            const string query = @"SELECT * FROM DeveloperAccessRequest WHERE CustomerId = @CustomerId AND TenantId = @TenantId";

            var parameter = new Dictionary<string, object>
        	                	{
        	                		{"CustomerId", customerId},
        	                		{"TenantId", tenantId},
        	                	};

        	DeveloperAccessRequest request = null;
            using (var reader = GetReader(query, parameter))
                if (reader.Read())
                    request = new DeveloperAccessRequest(reader);
            Connection.Close();
            return request;
        }
    }
}
