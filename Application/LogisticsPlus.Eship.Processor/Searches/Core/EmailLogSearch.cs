﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Core
{
    public class EmailLogSearch : EntityBase
    {
		public List<EmailLogViewSearchDto> FetchEmailLogs(List<ParameterColumn> parameters, long tenantId)
		{
			var logs = new List<EmailLogViewSearchDto>();
			var query = @"SELECT top 10000 Id, [From], Tos, Ccs, Bbcs, Subject, DateCreated, [Sent] FROM EmailLog WHERE";

			var paramList = new Dictionary<string, object>();
			query += parameters.BuildWhereClause(paramList, CoreSearchFields.EmailLogs);
			query += string.Format("ORDER BY DateCreated {0}", SearchUtilities.Descending);

			using (var reader = GetReader(query, paramList))
				while (reader.Read())
					logs.Add(new EmailLogViewSearchDto(reader));
			Connection.Close();
			return logs;
		}

        public List<EmailLog> FetchUnsentEmails()
        {
            var logs = new List<EmailLog>();
            const string query = @"SELECT * FROM EmailLog WHERE [Sent] = 0 ORDER BY DateCreated";

            using (var reader = GetReader(query, null))
                while (reader.Read())
                    logs.Add(new EmailLog(reader));
            Connection.Close();
            return logs;
        } 
    }
}
