﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Views.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Core
{
    public class AuditLogSearch : EntityBase
    {
        public List<AuditLogsViewSearchDto> FetchAuditLogs(AuditLogViewSearchCriteria criteria, long tenantId)
        {
            var logs = new List<AuditLogsViewSearchDto>();
            var query = @"SELECT top 10000 * FROM AuditLogsViewSearch WHERE";

            var parameters = new Dictionary<string, object>();
            query += criteria.Parameters.BuildWhereClause(parameters, CoreSearchFields.AuditLogs);
            query += string.Format("ORDER BY LogDateTime {0}", SearchUtilities.Descending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    logs.Add(new AuditLogsViewSearchDto(reader));
            Connection.Close();
            return logs;
        }

        public List<AuditLogsViewSearchDto> FetchEntityAuditLogs(string entityCode, object entityId, long tenantId)
        {
            var logs = new List<AuditLogsViewSearchDto>();
            var query =
                string.Format(@"SELECT top 1000 * FROM AuditLogsViewSearch WHERE EntityCode = @EntityCode AND EntityId = @EntityId AND TenantId = @TenantId
				ORDER BY LogDateTime {0}",SearchUtilities.Descending);

            var parameter = new Dictionary<string, object>
			                	{
			                		{"EntityCode", entityCode},
			                		{"EntityId", entityId},
			                		{"TenantId", tenantId},
			                	};

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    logs.Add(new AuditLogsViewSearchDto(reader));
            Connection.Close();
            return logs;
        }
    }
}

