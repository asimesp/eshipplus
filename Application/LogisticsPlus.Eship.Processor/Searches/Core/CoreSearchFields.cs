﻿using System.Collections.Generic;
using System.Data;

namespace LogisticsPlus.Eship.Processor.Searches.Core
{
    public class CoreSearchFields
    {
        private static readonly object LockObj = new object();

        private static bool _isInitialized;


        public static SearchField NonAliasedCustomerName;
        public static SearchField CustomerNumber;
        public static SearchField EntityCode;
        public static SearchField Description;
        public static SearchField LogDateTime;
        public static SearchField FirstName;
        public static SearchField LastName;

        // ------------------------------------------------

        public static List<SearchField> AdminAuditLogs { get; set; } 
        public static List<SearchField> AuditLogs { get; set; }
        public static List<SearchField> Announcements { get; set; }
        public static List<SearchField> DeveloperAccessRequests { get; set; }
        public static List<SearchField> EmailLogs { get; set; } 

        // ------------------------------------------------

        public static List<SearchField> DefaultAdminAuditLogs { get; set; } 
        public static List<SearchField> DefaultAuditLogs { get; set; }
        public static List<SearchField> DefaultDeveloperAccessRequests { get; set; }
        public static List<SearchField> DefaultEmailLogs { get; set; } 

        // ------------------------------------------------

        public static void Initialize()
        {
            if (_isInitialized) return;

            var anotherLockInitialized = false;
            lock (LockObj)
            {
                if (!_isInitialized) _isInitialized = true;
                else anotherLockInitialized = true;
            }

            if (anotherLockInitialized) return;

            Description = new SearchField { Name = "Description", DataType = SqlDbType.NVarChar };
            EntityCode = new SearchField {Name = "EntityCode", DataType = SqlDbType.NVarChar};
            CustomerNumber = new SearchField { Name = "CustomerNumber", DataType = SqlDbType.NVarChar };
            FirstName = new SearchField { Name = "FirstName", DataType = SqlDbType.NVarChar };
            LastName = new SearchField { Name = "LastName", DataType = SqlDbType.NVarChar };
            LogDateTime = new SearchField { Name = "LogDateTime", DataType = SqlDbType.DateTime };
            NonAliasedCustomerName = new SearchField {Name = "Name", DisplayName = "Customer Name", DataType = SqlDbType.NVarChar };

            // ------------------------------------------------

            DefaultAdminAuditLogs = new List<SearchField>
                                   {
                                       EntityCode,
                                       LogDateTime,
                                       FirstName,
                                       LastName,
                                   };

            DefaultAuditLogs = new List<SearchField>
                                   {
                                       EntityCode,
                                       LogDateTime,
                                       FirstName,
                                       LastName,
                                   };

            DefaultDeveloperAccessRequests = new List<SearchField>
                                                 {
                                                     NonAliasedCustomerName,
                                                     CustomerNumber,
                                                     new SearchField {Name = "DateCreated", DataType = SqlDbType.DateTime},
                                                 };
            DefaultEmailLogs = new List<SearchField>
                {
                    new SearchField {Name = "DateCreated", DataType = SqlDbType.DateTime},
                    new SearchField {Name = "Tos", DisplayName = "To", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "Subject", DataType = SqlDbType.NVarChar},
                };

            // ------------------------------------------------

            AdminAuditLogs = new List<SearchField>
                {
                    Description
                };
            AdminAuditLogs.AddRange(DefaultAdminAuditLogs);

            AuditLogs = new List<SearchField>
                {
                    Description,
			        new SearchField {DataType = SqlDbType.BigInt, Name = "EntityId"},
                };
            AuditLogs.AddRange(DefaultAuditLogs);

            Announcements = new List<SearchField>
			           	{
			           		new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "Message", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "Enabled", DataType = SqlDbType.Bit},
                            new SearchField {Name = "TypeText", DataType = SqlDbType.NVarChar}
			           	};

            DeveloperAccessRequests = new List<SearchField>
			           	{
                            NonAliasedCustomerName,
                            CustomerNumber,
			           		new SearchField {Name = "DateCreated", DataType = SqlDbType.DateTime},
			           		new SearchField {Name = "ContactName", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "ContactEmail", DataType = SqlDbType.NVarChar},
			           		new SearchField {Name = "ProductionAccess", DataType = SqlDbType.Bit},
			           		new SearchField {Name = "AccessGranted", DataType = SqlDbType.Bit},
			           	};

            EmailLogs = new List<SearchField>
                {
                    new SearchField {Name = "Ccs",  DisplayName = "Cc", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "Bbcs", DisplayName = "Bcc", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "Body", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "[From]", DisplayName = "From", DataType = SqlDbType.NVarChar},
                    new SearchField {Name = "[Sent]", DisplayName = "Sent", DataType = SqlDbType.Bit},
                };
            EmailLogs.AddRange(DefaultEmailLogs);
        }
    }
}