﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Dto.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Core
{
	public class LockSearch : EntityBase
	{
		public Lock RetrieveExistingUserLock(long tenantId, long userId, object entityId, string lockKey)
		{
			const string query =
				@"Select * from EntityLock where TenantId = @TenantId and LockUserId = @LockUserId and EntityId = @EntityId and LockKey = @LockKey";

			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"TenantId", tenantId},
			                 		{"LockUserId", userId},
			                 		{"EntityId", entityId},
			                 		{"LockKey", lockKey}
			                 	};
			Lock @lock = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					@lock = new Lock(reader);
			Connection.Close();
			return @lock;
		}

		public Lock RetrieveExistingLock(long tenantId, object entityId, string lockKey)
		{
			const string query =
				@"Select * from EntityLock where TenantId = @TenantId and EntityId = @EntityId and LockKey = @LockKey";

			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"TenantId", tenantId},
			                 		{"EntityId", entityId},
			                 		{"LockKey", lockKey}
			                 	};
			Lock @lock = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					@lock = new Lock(reader);
			Connection.Close();
			return @lock;
		}

		public List<LockDto> FetchLocks(long tenantId)
		{
			const string query =
				@"Select 
					l.Id, 
					l.LockUserId, 
					l.LockKey, 
					l.LockDateTime, 
					l.TenantId,
					l.EntityId,
					u.Username, 
					u.FirstName, 
					u.LastName
				from 
					EntityLock l, 
					[User] u 
				where 
					l.LockUserId = u.Id
					and l.TenantId = @TenantId
				Order by
					u.FirstName, u.LastName, l.LockDateTime";
			var parameters = new Dictionary<string, object> {{"TenantId", tenantId}};
			var locks = new List<LockDto>();
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					locks.Add(new LockDto(reader));
			Connection.Close();
			return locks;
		}
	}
}
