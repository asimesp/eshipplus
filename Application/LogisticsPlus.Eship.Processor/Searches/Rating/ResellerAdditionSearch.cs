﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
    public class ResellerAdditionSearch : EntityBase
    {
        public List<ResellerAddition> FetchResellerAdditions(List<ParameterColumn> columns, long tenantId )
        {
            var resellerAdditions = new List<ResellerAddition>();

            var query = @"SELECT * FROM ResellerAddition WHERE TenantId = @TenantId AND";

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            var baseCustomerCols = new List<SearchField>
                                   {
                                       RatingSearchFields.NonAliasedCustomerName,
                                       RatingSearchFields.CustomerNumber,
                                   }.ToParameterColumn()
             .Select(c => c.ReportColumnName)
             .ToList();

            var customerCols = columns.Where(c => baseCustomerCols.Contains(c.ReportColumnName)).ToList();
            var raCols = columns.Where(c => !baseCustomerCols.Contains(c.ReportColumnName)).ToList();

            query += raCols.BuildWhereClause(parameters, RatingSearchFields.ResellerAdditions);

            if (customerCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var uQuery = "SELECT Id FROM Customer WHERE ";

                uQuery += customerCols.BuildWhereClause(uParams, RatingSearchFields.ResellerAdditions);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    uQuery = uQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND (ResellerCustomerAccountId IN ({0}))", uQuery);
            }

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    resellerAdditions.Add(new ResellerAddition(reader));
            Connection.Close();

            return resellerAdditions;
        }
    }
}
