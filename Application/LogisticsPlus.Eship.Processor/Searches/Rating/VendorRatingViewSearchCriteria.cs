﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
    public class VendorRatingViewSearchCriteria
    {
       public List<ParameterColumn> Parameters { get; set; }

       public VendorRatingViewSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }    
    }
}