﻿using System.Collections.Generic;
using System.Data;
using System.Security.Policy;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
    public static class RatingSearchFields
    {
        private static readonly object LockObj = new object();

        private static bool _isInitialized;

        // static SearchFields
        public static SearchField NonAliasedCustomerName;
        public static SearchField CustomerNumber;
        public static SearchField RegionName;
        public static SearchField VendorRatingName;
        public static SearchField Project44TradingPartnerCode;
        public static SearchField Active;
		public static SearchField ExpirationDate;
		// ------------------------------------------------

		// Default SearchField lists
		public static List<SearchField> DefaultCustomerTLTenderProfiles { get; set; }
        public static List<SearchField> DefaultResellerAdditions { get; set; }
        public static List<SearchField> DefaultVendorRatings { get; set; }
        // ------------------------------------------------

        // Regular SearchField lists 
        public static List<SearchField> CustomerTLTenderProfiles { get; set; }
        public static List<SearchField> ResellerAdditions { get; set; }
        public static List<SearchField> VendorRatings { get; set; }
        public static List<SearchField> Regions { get; set; }
        // ------------------------------------------------

        // Sorting Searchfield lists
        // ------------------------------------------------

        public static void Initialize()
        {
            if (_isInitialized) return;

            var anotherLockInitialized = false;
            lock (LockObj)
            {
                if (!_isInitialized) _isInitialized = true;
                else anotherLockInitialized = true;
            }

            if (anotherLockInitialized) return;

            NonAliasedCustomerName = new SearchField { Name = "Name", DisplayName = "Customer Name", DataType = SqlDbType.NVarChar };
            CustomerNumber = new SearchField { Name = "CustomerNumber", DataType = SqlDbType.NVarChar };
	        RegionName = new SearchField {Name = "Name", DataType = SqlDbType.NVarChar};
            VendorRatingName = new SearchField { Name = "Name", DataType = SqlDbType.NVarChar };
            Project44TradingPartnerCode = new SearchField { Name = "Project44TradingPartnerCode", DataType = SqlDbType.NVarChar, DisplayName = "Project44 Trading Partner Code" };
            Active = new SearchField {Name = "Active", DataType = SqlDbType.Bit};
			ExpirationDate = new SearchField { Name = "ExpirationDate", DataType = SqlDbType.DateTime };
			// ------------------------------------------------
			DefaultCustomerTLTenderProfiles = new List<SearchField>
                                {
                                    CustomerNumber,
                                };

            DefaultVendorRatings = new List<SearchField>
                                {
                                    VendorRatingName,  
                                    new SearchField {Name = "DisplayName", DataType = SqlDbType.NVarChar},  
                                };
            DefaultResellerAdditions = new List<SearchField>
                                {
                                    new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},  
                                };

            
            // ------------------------------------------------
            CustomerTLTenderProfiles = new List<SearchField>
                                {
                                    CustomerNumber,
                                    NonAliasedCustomerName,
                                };
            
            Regions = new List<SearchField>
                                {
                                    RegionName,  
                                };
            
            ResellerAdditions = new List<SearchField>
                                {
                                    new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},  
                                    NonAliasedCustomerName,
                                    CustomerNumber
                                };
            
            VendorRatings = new List<SearchField>
                                {
                                    new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},  
                                    new SearchField {Name = "DisplayName", DataType = SqlDbType.NVarChar},  
                                    new SearchField {Name = "VendorNumber", DataType = SqlDbType.NVarChar},  
                                    new SearchField {Name = "VendorName", DataType = SqlDbType.NVarChar}, 
                                    new SearchField {Name = "Active", DataType = SqlDbType.Bit},
                                    Project44TradingPartnerCode
                                };
            // ------------------------------------------------
        }

    }
}