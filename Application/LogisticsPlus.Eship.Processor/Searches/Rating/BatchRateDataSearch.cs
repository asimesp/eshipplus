﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
	public class BatchRateDataSearch : EntityBase
	{
		public List<BatchRateData> FetchBatchRateDataSet(ServiceMode mode, long tenantId)
		{
			var data = new List<BatchRateData>();

			var query =string.Format(@"Select * from BatchRateData 
                                        where ServiceMode = @ServiceMode and TenantId = @TenantId order by DateCreated {0}, [Name] {0}", SearchUtilities.Ascending);
			var parameters = new Dictionary<string, object> {{"ServiceMode", mode}, {"TenantId", tenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					data.Add(new BatchRateData(reader));
			Connection.Close();

			return data;
		}

		public List<long> FetchIncompleteBatchRateDataSetIds(long tenantId)
		{
			var data = new List<long>();

			var query = string.Format("Select Id from BatchRateData where DateCompleted = @DefaultDate and TenantId = @TenantId");
			var parameters = new Dictionary<string, object> {{"DefaultDate", DateUtility.SystemEarliestDateTime}, {"TenantId", tenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					data.Add(reader.GetValue(reader.GetOrdinal("Id")).ToLong());
			Connection.Close();

			return data;
		}
	}
}
