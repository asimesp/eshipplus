﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Views.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
	public class CustomerTLTenderingProfileVendorSearch : EntityBase
	{

		public List<CustomerTLTenderingProfileVendor> FetchTLProfileVendorByVendorId(long tlVendorId)
		{
			var profileVendors = new List<CustomerTLTenderingProfileVendor>();

			var query = @"SELECT cpl.* FROM CustomerTLTenderingProfileVendor cpl                                   
								    WHERE cpl.VendorId = @VendorId";

			var parameters = new Dictionary<string, object> { { "VendorId", tlVendorId } };


			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					profileVendors.Add(new CustomerTLTenderingProfileVendor(reader));
			Connection.Close();

			return profileVendors;
		}
	}
}
