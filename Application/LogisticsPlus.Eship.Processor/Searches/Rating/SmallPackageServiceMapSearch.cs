﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
    public class SmallPackageServiceMapSearch:EntityBase 

    {
        public List<SmallPackageServiceMap> FetchSmallPackageServiceMaps(long tenantId)
        {
            var maps = new List<SmallPackageServiceMap>();

			var query = string.Format(@"SELECT * FROM SmallPackageServiceMap
                            WHERE TenantId = @TenantId ORDER BY SmallPackageEngine {0}, SmallPackEngineService {0}", SearchUtilities.Ascending);

            var parameter = new Dictionary<string, object> { { "TenantId", tenantId } };

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    maps.Add(new SmallPackageServiceMap(reader));
            Connection.Close();

            return maps;
        }

    }
}
