﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
    public class SmallPackagingMapSearch : EntityBase
    {
        public List<SmallPackagingMap> FetchSmallPackagingMaps(string criteria, long tenantId)
        {
            var maps = new List<SmallPackagingMap>();

            var query = string.Format(@"SELECT * FROM SmallPackagingMap WHERE SmallPackEngineType LIKE @Criteria 
									AND TenantId = @TenantId ORDER BY SmallPackageEngine {0}, SmallPackEngineType {0}", SearchUtilities.Ascending);

            var parameter = new Dictionary<string, object> { {"Criteria", criteria},{ "TenantId", tenantId } };

            using (var reader = GetReader(query, parameter))
                while (reader.Read())
                    maps.Add(new SmallPackagingMap(reader));
            Connection.Close();

            return maps;
        }
    }
}
