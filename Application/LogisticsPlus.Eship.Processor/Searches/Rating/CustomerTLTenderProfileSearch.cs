﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Views.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
    public class CustomerTLTenderProfileSearch : EntityBase
	{
        public List<CustomerTLTenderingProfile> FetchCustomerTLTenderProfiles(CustomerTLTenderingProfileViewSearchCriteria criteria, long tenantId)
        {
            if (criteria.Parameters == null) return new List<CustomerTLTenderingProfile>();

            var columns = criteria.Parameters;

            var profiles = new List<CustomerTLTenderingProfile>();

            var query = @"SELECT ctlp.* FROM CustomerTLTenderingProfile ctlp
                                    INNER JOIN Customer c ON ctlp.CustomerId = c.Id
								    WHERE ctlp.TenantId =  @TenantId ";

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            var baseCustomerCols = new List<SearchField>
                                   {
                                       RatingSearchFields.CustomerNumber,
                                       RatingSearchFields.NonAliasedCustomerName,
                                   }.ToParameterColumn()
              .Select(c => c.ReportColumnName)
              .ToList();

            var customerCols = columns.Where(c => baseCustomerCols.Contains(c.ReportColumnName)).ToList();

            if (customerCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var uQuery = "SELECT Id FROM [Customer] WHERE ";

                uQuery += customerCols.BuildWhereClause(uParams, RatingSearchFields.CustomerTLTenderProfiles);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    uQuery = uQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format("AND (ctlp.CustomerId IN ({0}))", uQuery);
            }

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    profiles.Add(new CustomerTLTenderingProfile(reader));
            Connection.Close();

            return profiles;
        }
		
		public CustomerTLTenderingProfile FetchCustomerTenderingProfileByCustomerId(long customerId, long tenantId)
		{
			var profile = new CustomerTLTenderingProfile();

			var query = @"SELECT * FROM CustomerTLTenderingProfile                                  
								    WHERE TenantId = @TenantId AND CustomerId = @CustomerId";

			var parameters = new Dictionary<string, object> { { "CustomerId", customerId }, { "TenantId", tenantId } };


			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					profile = new CustomerTLTenderingProfile(reader);
			Connection.Close();

			return profile;
		}
	}
}
