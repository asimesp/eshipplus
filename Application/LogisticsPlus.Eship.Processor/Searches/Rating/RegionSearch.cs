﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
	public class RegionSearch : EntityBase
	{
	    public List<Region> FetchRegions(List<ParameterColumn> columns , long tenantId)
        {
            var regions = new List<Region>();

            var query = string.Format("SELECT * FROM Region WHERE TenantId = @TenantId AND ");
           
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameters, RatingSearchFields.Regions);

            query += string.Format("ORDER BY [Name] {0}", SearchUtilities.Ascending);
            
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    regions.Add(new Region(reader));
            Connection.Close();

            return regions;
        }

		public long FetchRegionIdByName(string name, long tenantId)
		{
			const string query = "Select [Id] from Region where [Name] = @Name and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"Name", name}, {"TenantId", tenantId}};
			return ExecuteScalar(query, parameters).ToLong();
		}
	}
}
