﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
	public class FuelTableSearch : EntityBase
	{
	    public List<FuelTable> FetchFuelTables(List<ParameterColumn>columns , long tenantId)
        {
            var fuelTables = new List<FuelTable>();

            var query = string.Format("SELECT * FROM FuelTable WHERE  TenantId = @TenantId AND ");
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            query += columns.BuildWhereClause(parameters, RegistrySearchFields.FuelTables);

            query += string.Format("ORDER BY [Name] {0}", SearchUtilities.Ascending);
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    fuelTables.Add(new FuelTable(reader));
            Connection.Close();

            return fuelTables;
        }
	}
}
