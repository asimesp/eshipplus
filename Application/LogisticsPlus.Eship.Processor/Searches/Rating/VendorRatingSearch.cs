﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.Rating
{
	public class VendorRatingSearch : EntityBase
	{
	    public List<VendorRating> FetchVendorRatings(VendorRatingViewSearchCriteria criteria, long tenantId)
        {
            var vendorRatings = new List<VendorRating>();

            var query = @"SELECT * FROM VendorRatingViewSearch WHERE (TenantId = @TenantId) AND ";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };
            
            query += criteria.Parameters.BuildWhereClause(parameters, RatingSearchFields.VendorRatings);
            query += string.Format("ORDER BY [Name] {0}", SearchUtilities.Ascending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    vendorRatings.Add(new VendorRating(reader));
            Connection.Close();

            return vendorRatings;
        }

		public long FetchVendorRatingIdByName(string name, long tenantId)
		{
			const string query = "Select [Id] from VendorRating where [Name] = @Name and TenantId = @TenantId";
			
            
            var parameters = new Dictionary<string, object> { { "Name", name }, { "TenantId", tenantId } };
			return ExecuteScalar(query, parameters).ToLong();
		}

		public AdditionalChargeIndex ApplicableAdditionalChargeIndex(long tenantId, long additionalChargeId, long originCountryId, string originPostalCode,
			long destinationCountryId, string destinationPostalCode)
		{
			const string query = "exec [ApplicableAdditionalChargeIndex] @AdditionalChargeId, @TenantId, @OriginPostalCode, @OriginCountryId, @DestinationPostalCode, @DestinationCountryId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"AdditionalChargeId", additionalChargeId},
			                 		{"TenantId", tenantId},
			                 		{"OriginPostalCode", originPostalCode},
			                 		{"OriginCountryId", originCountryId},
			                 		{"DestinationPostalCode", destinationPostalCode},
			                 		{"DestinationCountryId", destinationCountryId},
			                 	};
			AdditionalChargeIndex index = null;
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					index = new AdditionalChargeIndex(reader);
			Connection.Close();

			return index;
		}
	}
}
