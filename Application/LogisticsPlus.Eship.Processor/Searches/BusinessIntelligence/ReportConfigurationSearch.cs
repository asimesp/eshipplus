﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence
{
	public class ReportConfigurationSearch : EntityBase
	{
	    public List<ReportConfiguration> FetchReportConfigurations(List<ParameterColumn> columns, long userId, long tenantId, bool bypassVisibilityFilter = false)
        {
            var reportConfigurations = new List<ReportConfiguration>();

            var query = @"SELECT * FROM ReportConfiguration AS r WHERE r.TenantId = @TenantId AND ";

		    var parameters = new Dictionary<string, object> {{"TenantId", tenantId}};

		    if (!bypassVisibilityFilter)
		    {
			    query += " dbo.ReportConfigurationVisibilityFilter(@TenantId, r.Id, @UserId) = 1 AND ";
			    parameters.Add("UserId", userId);
		    }


		    var baseUserCols = new List<SearchField>
                                   {
                                       BusinessIntelligenceSearchFields.Username,
                                       BusinessIntelligenceSearchFields.LastName,
                                       BusinessIntelligenceSearchFields.FirstName
                                   }.ToParameterColumn()
                .Select(c => c.ReportColumnName)
                .ToList();

            var userCols = columns.Where(c=> baseUserCols.Contains(c.ReportColumnName)).ToList();
            var rcCols = columns.Where(c => !baseUserCols.Contains(c.ReportColumnName)).ToList();

            query += rcCols.BuildWhereClause(parameters, BusinessIntelligenceSearchFields.ReportConfigurations);

            if(userCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var uQuery = "SELECT Id FROM [User] WHERE ";

                uQuery += userCols.BuildWhereClause(uParams, BusinessIntelligenceSearchFields.ReportConfigurations);
                
                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    uQuery = uQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND (UserId IN ({0}))", uQuery);
            }

            query += string.Format(" ORDER BY Name {0}", SearchUtilities.Ascending);
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    reportConfigurations.Add(new ReportConfiguration(reader));
            Connection.Close();

            return reportConfigurations;
        }

		public List<ReportConfiguration>  FetchReportConfigurationsVisibleToUser(long userId, long tenantId)
		{
			const string query =
				@"Select * From ReportConfiguration As r Where dbo.ReportConfigurationVisibilityFilter(@TenantId, r.Id, @UserId) = 1 order by Name asc";

			var reportConfigurations = new List<ReportConfiguration>();
			var parameters = new Dictionary<string, object>
                             	{
                             		{"UserId", userId},
									{"TenantId", tenantId}
                             	};

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					reportConfigurations.Add(new ReportConfiguration(reader));
			Connection.Close();

			return reportConfigurations;
		}
	}
}
