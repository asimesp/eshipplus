﻿using System.Collections.Generic;
using System.Data;

namespace LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence
{
    public static class BusinessIntelligenceSearchFields
    {
        private static readonly object LockObj = new object();

        private static bool _isInitialized;

        // static SearchFields
        public static SearchField Username;
        public static SearchField FirstName;
        public static SearchField LastName;
        public static SearchField NonAliasedCustomerName;
        public static SearchField CustomerNumber;
        public static SearchField ReportScheduleConfigurationName;
        public static SearchField ReportScheduleEndDate;
        public static SearchField ReportScheduleFirstName;
        public static SearchField ReportScheduleLastName;
        public static SearchField ReportScheduleLastRun;
        public static SearchField ReportScheduleStartDate;
        public static SearchField ReportScheduleTime;
        public static SearchField ReportScheduleUsername;
        public static SearchField NonAliasedVendorName;
        public static SearchField VendorNumber;
        // ------------------------------------------------

        // Default SearchField lists

        public static List<SearchField> Default { get; set; }
        public static List<SearchField> DefaultReportConfigurations { get; set; }
        // ------------------------------------------------

        // Regular SearchField lists 
        public static List<SearchField> CustomerGroups { get; set; }
        public static List<SearchField> ReportConfigurations { get; set; }
        public static List<SearchField> ReportSchedules { get; set; }
        public static List<SearchField> ReportTemplates { get; set; }
        public static List<SearchField> VendorGroups { get; set; }
        // ------------------------------------------------

        // Sorting Searchfield lists

        public static List<SearchField> ReportScheduleSortFields { get; set; } 
        // ------------------------------------------------

        public static void Initialize()
        {
            if (_isInitialized) return;

            var anotherLockInitialized = false;
            lock (LockObj)
            {
                if (!_isInitialized) _isInitialized = true;
                else anotherLockInitialized = true;
            }

            if (anotherLockInitialized) return;

            Username = new SearchField { Name = "Username", DataType = SqlDbType.NVarChar };
            FirstName = new SearchField { Name = "FirstName", DisplayName = "User First Name", DataType = SqlDbType.NVarChar };
            LastName = new SearchField { Name = "LastName", DisplayName = "User Last Name",DataType = SqlDbType.NVarChar };
            NonAliasedCustomerName = new SearchField { Name = "Name", DisplayName = "Customer Name",DataType = SqlDbType.NVarChar };
            CustomerNumber = new SearchField { Name = "CustomerNumber", DisplayName = "Customer Number", DataType = SqlDbType.NVarChar };
            ReportScheduleConfigurationName = new SearchField { Name = "Name", DisplayName = "Report Configuration Name", DataType = SqlDbType.NVarChar };
            ReportScheduleEndDate = new SearchField { Name = "[End]", DisplayName = "End Date", DataType = SqlDbType.DateTime };
            ReportScheduleFirstName = new SearchField { Name = "FirstName", DisplayName = "Report Schedule User First Name", DataType = SqlDbType.NVarChar };
            ReportScheduleLastName = new SearchField { Name = "LastName", DisplayName = "Report Schedule User Last Name", DataType = SqlDbType.NVarChar };
            ReportScheduleLastRun = new SearchField { Name = "LastRun", DataType = SqlDbType.DateTime };
            ReportScheduleStartDate = new SearchField { Name = "Start", DisplayName = "Start Date", DataType = SqlDbType.DateTime };
            ReportScheduleTime = new SearchField { Name = "Time", DisplayName = "Time", DataType = SqlDbType.NVarChar };
            ReportScheduleUsername = new SearchField { Name = "Username", DisplayName = "Report Schedule User Username", DataType = SqlDbType.NVarChar };
            NonAliasedVendorName = new SearchField { Name = "Name", DisplayName = "Vendor Name", DataType = SqlDbType.NVarChar };
            VendorNumber = new SearchField { Name = "VendorNumber", DisplayName = "Vendor Number", DataType = SqlDbType.NVarChar };

            // ------------------------------------------------

            Default = new List<SearchField>
                                {
                                    new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},  
                                    new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},  
                                };
            DefaultReportConfigurations = new List<SearchField>
                                {
                                    ReportScheduleConfigurationName,
                                    new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},  
                                };
            
            // ------------------------------------------------
            CustomerGroups = new List<SearchField>
                                       {
                                           new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},
                                           NonAliasedCustomerName,
                                           CustomerNumber,
                                       };
            
            ReportConfigurations = new List<SearchField>
                                       {
                                           new SearchField {Name = "Name", DisplayName = "Report Configuration Name", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "LastRun", DisplayName = "Last Run", DataType = SqlDbType.DateTime},
                                           Username,
                                           FirstName,
                                           LastName
                                       };
            
            ReportSchedules= new List<SearchField>
                                       {
                                           ReportScheduleConfigurationName,
                                           new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},
                                           ReportScheduleFirstName,
                                           ReportScheduleLastName,
                                           ReportScheduleUsername,
                                           ReportScheduleLastRun,
                                           ReportScheduleStartDate,
                                           ReportScheduleEndDate,
                                           new SearchField {Name = "Enabled", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "FtpEnabled", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "ExcludeMonday", DisplayName = "No Monday Run", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "ExcludeTuesday", DisplayName = "No Tuesday Run", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "ExcludeWednesday", DisplayName = "No Wednesday Run", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "ExcludeThursday", DisplayName = "No Thursday Run", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "ExcludeFriday", DisplayName = "No Friday Run", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "ExcludeSaturday", DisplayName = "No Saturday Run", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "ExcludeSunday", DisplayName = "No Sunday Run", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "NotifyEmails", DisplayName = "Notification Emails", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Notify", DisplayName = "Send Notifications", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "Time", DisplayName = "Time of Day To Run", DataType = SqlDbType.Time},
                                       };

            ReportTemplates = new List<SearchField>
                                       {
                                           new SearchField {Name = "Name", DisplayName = "Name", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "CanFilterByCustomerGroup", DataType = SqlDbType.Bit},
                                           new SearchField {Name = "CanFilterByVendorGroup", DataType = SqlDbType.Bit},
                                       };

            VendorGroups = new List<SearchField>
                                       {
                                           new SearchField {Name = "Name", DataType = SqlDbType.NVarChar},
                                           new SearchField {Name = "Description", DataType = SqlDbType.NVarChar},
                                           NonAliasedVendorName,
                                           VendorNumber,
                                       };
            // ------------------------------------------------

            ReportScheduleSortFields = new List<SearchField>
                {
                    ReportScheduleConfigurationName,
                    ReportScheduleStartDate,
                    ReportScheduleEndDate,
                    ReportScheduleLastRun,
                    ReportScheduleTime
                };
        }

    }
}