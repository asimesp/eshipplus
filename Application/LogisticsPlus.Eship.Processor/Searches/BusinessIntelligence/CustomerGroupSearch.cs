﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence
{
    public class  CustomerGroupSearch : EntityBase
    {
        public List<CustomerGroup> FetchCustomerGroups(List<ParameterColumn> columns , long tenantId)
        {
            var customersGroups = new List<CustomerGroup>();

            var query = @"SELECT * FROM CustomerGroup WHERE TenantId = @TenantId AND ";

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

        	var baseCustomerCols = new List<SearchField>
        	                       	{
        	                       		BusinessIntelligenceSearchFields.CustomerNumber,
        	                       		BusinessIntelligenceSearchFields.NonAliasedCustomerName,
        	                       	}.ToParameterColumn()
        		.Select(c => c.ReportColumnName)
        		.ToList();

            var customerCols = columns.Where(c => baseCustomerCols.Contains(c.ReportColumnName)).ToList();
            var cgCols = columns.Where(c => !baseCustomerCols.Contains(c.ReportColumnName)).ToList();

            query += cgCols.BuildWhereClause(parameters, BusinessIntelligenceSearchFields.CustomerGroups);

            if (customerCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var cQuery = @"SELECT
	                            DISTINCT CustomerGroupId 
                            FROM 
	                            CustomerGroupMap,
	                            Customer 
                            WHERE 
                                CustomerGroupMap.CustomerId = Customer.Id
	                            AND  ";

                cQuery += customerCols.BuildWhereClause(uParams, BusinessIntelligenceSearchFields.CustomerGroups);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    cQuery = cQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND CustomerGroup.Id IN ({0})", cQuery);
            }

            query += string.Format("ORDER BY [Name] {0}", SearchUtilities.Ascending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    customersGroups.Add(new CustomerGroup(reader));
            Connection.Close();

            return customersGroups;
        }
    }
}
