﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence
{
    public class ReportTemplateSearch : EntityBase
    {
        public List<ReportTemplate> FetchReportTemplates(List<ParameterColumn> columns )
        {
            var reportTemplates = new List<ReportTemplate>();

            var query = @"SELECT * FROM ReportTemplate WHERE ";

            var parameters = new Dictionary<string, object> ();
            query += columns.BuildWhereClause(parameters, BusinessIntelligenceSearchFields.ReportTemplates);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    reportTemplates.Add(new ReportTemplate(reader));
            Connection.Close();

            return reportTemplates;
        }
    }
}
