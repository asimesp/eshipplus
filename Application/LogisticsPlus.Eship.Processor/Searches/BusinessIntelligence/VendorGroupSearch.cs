﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence
{
    public class VendorGroupSearch : EntityBase
    {
        public List<VendorGroup> FetchVendorGroups(List<ParameterColumn> columns , long tenantId)
        {
            var vendorGroups = new List<VendorGroup>();

            var query = @"SELECT * FROM VendorGroup WHERE TenantId = @TenantId AND ";

            var parameters = new Dictionary<string, object> { { "TenantId", tenantId } };

            var baseUserCols = new List<SearchField>
                                   {
                                       BusinessIntelligenceSearchFields.VendorNumber,
                                       BusinessIntelligenceSearchFields.NonAliasedVendorName,
                                   }.ToParameterColumn()
             .Select(c => c.ReportColumnName)
             .ToList();

            var vendorCols = columns.Where(c => baseUserCols.Contains(c.ReportColumnName)).ToList();
            var vgCols = columns.Where(c => !baseUserCols.Contains(c.ReportColumnName)).ToList();

            query += vgCols.BuildWhereClause(parameters, BusinessIntelligenceSearchFields.VendorGroups);

            if (vendorCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var vQuery = @"SELECT
	                            DISTINCT VendorGroupId 
                            FROM 
	                            VendorGroupMap,
	                            Vendor 
                            WHERE 
                                VendorGroupMap.VendorId = Vendor.Id
	                            AND  ";

                vQuery += vendorCols.BuildWhereClause(uParams, BusinessIntelligenceSearchFields.VendorGroups);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    vQuery = vQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND VendorGroup.Id IN ({0})", vQuery);
            }

            query += string.Format("ORDER BY [Name] {0}", SearchUtilities.Ascending);

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                     vendorGroups.Add(new VendorGroup(reader));
            Connection.Close();

            return vendorGroups;
        }
    }
}
