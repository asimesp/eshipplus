﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence
{
	public class ReportScheduleSearch : EntityBase
	{
        public List<ReportSchedule> FetchReportSchedules(ReportScheduleViewSearchCriteria criteria, long tenantId)
        {
            if(criteria.Parameters == null) return new List<ReportSchedule>();

            var columns = criteria.Parameters;

			var schedules = new List<ReportSchedule>();

			var query = @"SELECT rs.* FROM ReportSchedule rs
										INNER JOIN ReportConfiguration rc ON rs.ReportConfigurationId = rc.Id
								   WHERE rs.TenantId =  @TenantId AND ";

			var parameters = new Dictionary<string, object> {{"TenantId", tenantId}};

            var baseUserCols = new List<SearchField>
                                   {
                                       BusinessIntelligenceSearchFields.ReportScheduleFirstName,
                                       BusinessIntelligenceSearchFields.ReportScheduleLastName,
                                       BusinessIntelligenceSearchFields.ReportScheduleUsername,
                                   }.ToParameterColumn()
              .Select(c => c.ReportColumnName)
              .ToList();

            var userCols = columns.Where(c => baseUserCols.Contains(c.ReportColumnName)).ToList();
            var rcCols = columns.Where(c => !baseUserCols.Contains(c.ReportColumnName)).ToList();

            query += rcCols.BuildWhereClause(parameters, BusinessIntelligenceSearchFields.ReportSchedules);

            if (userCols.Any())
            {
                var uParams = new Dictionary<string, object> { { "TenantId", tenantId } };
                var uQuery = "SELECT Id FROM [User] WHERE ";

                uQuery += userCols.BuildWhereClause(uParams, BusinessIntelligenceSearchFields.ReportSchedules);

                foreach (var item in uParams)
                {
                    var newKey = string.Format("u_{0}", item.Key);
                    uQuery = uQuery.Replace(item.Key, newKey);
                    parameters.Add(newKey, item.Value);
                }
                query += string.Format(" AND (rs.UserId IN ({0}))", uQuery);
            }

            if (criteria.SortBy != null)
                query += string.Format(" ORDER BY {0} {1}", criteria.SortBy.Name, criteria.SortAscending ? SearchUtilities.Ascending : SearchUtilities.Descending);

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					schedules.Add(new ReportSchedule(reader));
			Connection.Close();

			return schedules;
		}

	    public List<ReportSchedule> FetchReportScheduleForRun(DateTime checkDate)
		{
			const string query = "select * from dbo.ScheduleReportsForRun(@NeverRunDate, @CheckDate, @CheckTime)";

			var schedules = new List<ReportSchedule>();

			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"NeverRunDate", DateUtility.SystemEarliestDateTime},
			                 		{"CheckDate", checkDate},
			                 		{"CheckTime", checkDate.ToString("HH:00")}
			                 	};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					schedules.Add(new ReportSchedule(reader));
			Connection.Close();

			return schedules;
		}
    }
}
