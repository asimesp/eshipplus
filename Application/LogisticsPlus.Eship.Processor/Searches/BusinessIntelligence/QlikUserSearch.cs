﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence
{
    public class QlikUserSearch : EntityBase
    {
        public List<QlikUser> FetchAllQlikUsers()
        {
            var qlikUsers = new List<QlikUser>();
            const string query = "Select * from QlikUsers";
            using (var reader = GetReader(query, null))
                while (reader.Read())
                    qlikUsers.Add(new QlikUser(reader));
            Connection.Close();
            return qlikUsers;
        }

        public List<string> FetchAllDistinctQlikUserAttributeTypes(string type)
        {
            var qlikUserAttributeTypes = new List<string>();
            const string query = "Select Distinct [type] From QlikUserAttributes Where [type] like @Type";
            var parameters = new Dictionary<string, object> { { "Type", type } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    qlikUserAttributeTypes.Add(reader["type"].ToString());
            Connection.Close();
            return qlikUserAttributeTypes;
        }

        public List<string> FetchAllDistinctQlikUserAttributeValues(string value, string typeToFilterBy)
        {
            var qlikUserAttributeTypes = new List<string>();
            const string query = "Select Distinct value From QlikUserAttributes Where [value] like @Value And [type] like @Type";
            var parameters = new Dictionary<string, object> { {"Value", value}, { "Type", typeToFilterBy } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    qlikUserAttributeTypes.Add(reader["value"].ToString());
            Connection.Close();
            return qlikUserAttributeTypes;
        }
    }
}
