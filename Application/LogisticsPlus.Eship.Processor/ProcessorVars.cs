﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Fax;
using LogisticsPlus.Eship.PluginBase.Mileage;
using LogisticsPlus.Eship.Processor.Cache;

namespace LogisticsPlus.Eship.Processor
{
	public static class ProcessorVars
	{
        /*EDI Status Code Constants*/
	    public static string EdiStatusCodeFinalDelivery = "D1";
	    public static string EdiStatusCodeActualPickup = "AF";
	    public static string EdiStatusCodeEstimatedDelivery = "AG";
	    public static string EdiStatusCodePickupAppointment = "AA";
	    public static string EdiStatusCodeDeliveryAppointment = "AB";

		/* Processing pace limiters */
		public static int CycleCountLimit = 10;
		public static int SleepTimeOutInMilliSeconds = 3000;

		/* Caches */
		public static RegistryProfileCache RegistryCache = new RegistryProfileCache();
		public static RaterRatingProfileCache RaterCache = new RaterRatingProfileCache();

		/* Temp Folder */
		public static string TempFolder = string.Empty;

		/* Error Folder */
		public static string ErrorFolder = string.Empty;	

		/* Fax Service Handler */
		public static FaxHandler FaxHandler = null;

		/* Maximum failed login attempts */
		public static int MaxFailedLoginAttempts = 5;

		/* Record lock timeout (seconds) */
		public static int DefaultRecordLockLimit = 60;
		private static Dictionary<long, int> _recordLockLimit = new Dictionary<long, int>();
		public static Dictionary<long, int> RecordLockLimit
		{
			get { return _recordLockLimit ?? (_recordLockLimit = new Dictionary<long, int>()); }
		}

		/* Monitor Emails*/
		private static Dictionary<long, string[]> _monitorEmails = new Dictionary<long, string[]>();
		public static Dictionary<long, string[]> MonitorEmails
		{
			get { return _monitorEmails ?? (_monitorEmails = new Dictionary<long, string[]>()); }
		}

		/* Advanced Customer LTL Scheduling Limit days */
		public static int DefaultAdvancedCustomerLTLScheduleLimit = 7;
		private static Dictionary<long, int> _advancedCustomerLTLScheduleLimit = new Dictionary<long, int>();
		public static Dictionary<long, int> AdvancedCustomerLTLScheduleLimit
		{
			get { return _advancedCustomerLTLScheduleLimit ?? (_advancedCustomerLTLScheduleLimit = new Dictionary<long, int>()); }
		}

		/* Vendor Insurance Alert Threshold  days */
		public static int DefaultVendorInsuranceAlertThresholdDays = 7;
		private static Dictionary<long, int> _vendorInsuranceAlertThresholdDays = new Dictionary<long, int>();
		public static Dictionary<long, int> VendorInsuranceAlertThresholdDays
		{
			get { return _vendorInsuranceAlertThresholdDays ?? (_vendorInsuranceAlertThresholdDays = new Dictionary<long, int>()); }
		}

		/* Vendor Insurance Alert Threshold  days */
		public static decimal DefaultCustomerCreditAlertThreshold = 500m;

		private static Dictionary<long, decimal> _customerCreditAlertThresholdDays = new Dictionary<long, decimal>();
		public static Dictionary<long, decimal> CustomerCreditAlertThreshold
		{
			get { return _customerCreditAlertThresholdDays ?? (_customerCreditAlertThresholdDays = new Dictionary<long, decimal>()); }
		}

		/* Rating Integration Utilities */

		private static Dictionary<long, IntegrationVariables> _integrationVariables = new Dictionary<long, IntegrationVariables>();
		public static Dictionary<long, IntegrationVariables> IntegrationVariables
		{
			get { return _integrationVariables ?? (_integrationVariables = new Dictionary<long, IntegrationVariables>()); }
		}

		/* Developer Access Request Support Emails */
		private static Dictionary<long, string[]> _developerAccessRequestSupportEmails = new Dictionary<long, string[]>();
		public static Dictionary<long, string[]> DeveloperAccessRequestSupportEmails
		{
			get
			{
				return _developerAccessRequestSupportEmails ??
					   (_developerAccessRequestSupportEmails = new Dictionary<long, string[]>());
			}
		}

		/* Specialist Support Emails */
		private static Dictionary<long, string[]> _specialistSupportEmails = new Dictionary<long, string[]>();
		public static Dictionary<long, string[]> SpecialistSupportEmails
		{
			get { return _specialistSupportEmails ?? (_specialistSupportEmails = new Dictionary<long, string[]>()); }
		}

		/* Tenant Admin Notification Emails */
		private static Dictionary<long, string[]> _tenantAdminNotificationEmails = new Dictionary<long, string[]>();
		public static Dictionary<long, string[]> TenantAdminNotificationEmails
		{
			get { return _tenantAdminNotificationEmails ?? (_tenantAdminNotificationEmails = new Dictionary<long, string[]>()); }
		}

		/* Batch Rating Record Delete limit (days) */
		public static int DefaultDeleteCompletedRateAnalysisRecordsAfterDays = 3;
		private static Dictionary<long, int> _deleteCompletedRateAnalysisRecordsAfterDays = new Dictionary<long, int>();
		public static Dictionary<long, int> DeleteCompletedRateAnalysisRecordAfterDays
		{
			get { return _deleteCompletedRateAnalysisRecordsAfterDays ?? (_deleteCompletedRateAnalysisRecordsAfterDays = new Dictionary<long, int>()); }
		}

		/* Announcements Cache */
		private static Dictionary<long, List<Announcement>> _announcements = new Dictionary<long, List<Announcement>>();
		public static Dictionary<long, List<Announcement>> Announcements
		{
			get { return _announcements ?? (_announcements = new Dictionary<long, List<Announcement>>()); }
		}

		/* Mileage plugins */
		private static Dictionary<MileageEngine, IMileagePlugin> _mileagePlugins = new Dictionary<MileageEngine, IMileagePlugin>();
		public static Dictionary<MileageEngine, IMileagePlugin> MileagePlugins
		{
			get { return _mileagePlugins ?? (_mileagePlugins = new Dictionary<MileageEngine, IMileagePlugin>()); }
		}


		/* Messages */
		public const string DeleteErrMsg = "Record cannot be deleted as it is referenced by other records in the application.";
		public const string DeleteLockErrMsg = "Attempt to delete record lock failed! A previously obtained lock might have expired.";
		public const string ExpiredPOErrMsgDateFormat = "Purchase order number is expired as of {0}";
		public const string InvalidLocationPOErrMsgFormat = "Purchase order number is invalid for {0} location";
		public const string ImportProcessCompleteMsg = "Import process complete.";
		public const string MaxUsePOErrMsgFormat = "Purchase order number has previously reached maximum uses of {0}";
		public const string MissingPOErrMsg = "Purchase order number is not found. Customer requires a valid purchase order number.";
		public const string NewShipmentFaxTransMsg = "New Shipment Fax.";
		public const string ProcessCompleteMsg = "Process Complete.";
		public const string RecordDeleteMsg = "Record Deleted.";
		public const string RecordDeleteErrMsgFormat = "An error occurred while deleting record. Err: {0}";
		public const string RecordSaveMsg = "Record Saved.";
		public const string RecordSaveErrMsg = "An error occurred while saving record.";
		public const string RecordSaveErrMsgFormat = "An error occurred while saving record. Err: {0}";
		public const string SqlParameterVariable = "p";
		public const string VoidShipmentFaxTransMsg = "Void Shipment Fax.";
		public const string NoSearchResultsMsg = "No search results found.";
        public const string NoSelectedItemsMsg = "No items selected.";
        public const string UnableToObtainLockErrMsg = "Error obtaining exclusive access to record! Previously obtained access might have expired.";
		public const string UnableToGetMileageForEngineErrMsg = "Error retrieving mileage for specified engine";

        public const string RefundSuccessfullyProcessedMessage = "Misc. Receipt refund has been successfully processed.";
        public const string VoidSuccessfullyProcessedMessage = "Misc. Receipt void has been successfully processed.";
        public const string MiscReceiptTransactionNotApplicable = "Misc. Receipt is neither refundable or able to be voided for payment ${0}";
        public const string MiscReceiptTransactionDeclined = "Misc. Receipt has been declined and cannot be refunded or voided for payment ${0}";
        public const string MiscReceiptTransactionExpired = "Misc. Receipt is expired and cannot be refunded or voided for payment ${0}";
        public const string MiscReceiptTransactionVoided = "Misc. Receipt has already been voided for payment ${0}";
        public const string MiscReceiptTransactionCouldNotVoid = "Misc. Receipt could not be voided for payment ${0}";
        public const string MiscReceiptTransactionRefundSettledSuccessfully = "Misc. Receipt has already been settled successfully for payment ${0}";
        public const string MiscReceiptTransactionRefundPendingSettlement = "Misc. Receipt is pending settlement for refund for payment ${0}";
        public const string MiscReceiptCannotVoidPartiallyApplied = "Cannot void a receipt that has been applied to one or more invoices or has been partially refunded for payment ${0}";
        public const string MiscReceiptErrorVoidingTransaction = "An error occurred when attempting to void receipt for payment ${0}: {1}";
        public const string MiscReceiptErrorRefundingTransaction = "An error occurred when attempting to refund receipt for ${0}: {1}";
        public const string MiscReceiptErrorPaymentGatewayForTenantChanged = "The payment gateway used to process payment of ${0} to shipment initially differs from the tenant's current payment gateway setup and cannot be refunded via this application.";

		public const string WSNoPermission = "User does not have permission to perform this function.";
	}
}
