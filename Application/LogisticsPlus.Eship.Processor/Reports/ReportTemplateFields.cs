﻿namespace LogisticsPlus.Eship.Processor.Reports
{
    internal class ReportTemplateFields
    {
        public const string OrderBySqlKeyword = "ORDER BY";

        public const string Columns = "[#Columns#]";
        public const string Filters = "[#Filters#]";
        public const string OrderBy = "[#OrderBy#]";
    }
}
