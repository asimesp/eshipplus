﻿using System.Collections.Generic;
using System.Data;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Reports
{
	public class ReportPivotColumn
	{
		public string Name { get; set; }
		public SqlDbType DataType { get; set; }
		public List<object> RowValues { get; set; }
		public ColumnType ColumnType { get; set; }
		public AggregateType AggregateType { get; set; }
	}
}