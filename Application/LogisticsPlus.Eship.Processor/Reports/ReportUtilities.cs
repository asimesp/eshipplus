﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Reports
{
	public static class ReportUtilities
	{
		const string Separator = ",";
		public const string SumOf = "ForSumOf";

		public static string GetSqlDataColumns(this IEnumerable<ReportColumn> columns, List<Summary> summaries)
		{
			var sqlDataColumns = columns.Select(c => string.Format("{0} as '{1}'", c.Column.WrapBraces(), c.Name)).ToList();
			sqlDataColumns.AddRange(summaries.Select(c => string.Format("{0} as '{1}'", c.ReportColumnName.WrapBraces(), SumOf + c.Name)));
			return sqlDataColumns.Any() ? string.Join(Separator, sqlDataColumns.ToArray()) : " 1 ";
		}

		public static string GetSqlParameters(this IEnumerable<ParameterColumn> columns, IDictionary<string, object> parameters)
		{
			var groupedParameters = columns
				.GroupBy(p => string.Format("{0}{1}", p.ReportColumnName, p.Operator))
				.Select(g => new { g.Key, Columns = g });
			var parameterLines = new List<string>();
			var i = 0;
			foreach (var @group in groupedParameters)
			{
				if (@group.Columns.Count() > 1)
				{
					var orLines = @group.Columns.Select(column => column.GetSqlParameter(i, parameters, out i).WrapBraces()).ToArray();
					parameterLines.Add(string.Join(" OR ", orLines).WrapBraces());
				}
				else
				{
					var column = @group.Columns.First();
					parameterLines.Add(column.GetSqlParameter(i, parameters, out i).WrapBraces());
				}
			}

			var qp = parameterLines.Any() ? string.Join(" AND ", parameterLines.ToArray()) : " 1 = 1 ";
			return qp;
		}

		public static void GetSqlParameters(this IEnumerable<RequiredValueParameter> requiredParams, IDictionary<string, object> parameters)
		{
			foreach (var requiredParam in requiredParams.Where(requiredParam => !parameters.ContainsKey(requiredParam.Name)))
				parameters.Add(requiredParam.Name, requiredParam.GetSqlParameter());
			
		}

		public static string GetSqlSortColumns(this IEnumerable<SortColumn> columns)
		{
			var array = columns
				.OrderBy(c => c.Priority)
				.Select(c => string.Format("{0} {1}", c.ReportColumnName.WrapBraces(), c.Direction == SortDirection.Ascending ? "ASC" : "DESC"))
				.ToArray();

			return string.Join(Separator, array);
		}


		public static string GetOperator(this Operator reportOperator)
		{
			switch (reportOperator)
			{
				case Operator.Contains:
				case Operator.BeginsWith:
				case Operator.EndsWith:
					return "LIKE";
				case Operator.NotContains:
				case Operator.NotBeginsWith:
				case Operator.NotEndsWith:
					return "NOT LIKE";
				case Operator.LessThanOrEqual:
					return "<=";
				case Operator.GreaterThanOrEqual:
					return ">=";
				case Operator.LessThan:
					return "<";
				case Operator.NotEqual:
					return "<>";
				case Operator.GreaterThan:
					return ">";
				case Operator.In:
					return "IN";
				case Operator.NotIn:
					return "NOT IN";
				case Operator.Between:
					return "BETWEEN";
				case Operator.NotBetween:
					return "NOT BETWEEN";
				default:
					return "=";
			}
		}

		public static string WrapBraces(this string columnName)
		{
			return string.Format("({0})", columnName);
		}


		public static Dictionary<int, string> ReportColumnDataTypes()
		{
			var dbTypes = new Dictionary<int, string>
			              	{
			              		{SqlDbType.BigInt.ToInt(), SqlDbType.BigInt.FormattedString()},
			              		{SqlDbType.Float.ToInt(), SqlDbType.Float.FormattedString()},
			              		{SqlDbType.Decimal.ToInt(), SqlDbType.Decimal.FormattedString()},
			              		{SqlDbType.NVarChar.ToInt(), SqlDbType.NVarChar.FormattedString()},
			              		{SqlDbType.DateTime.ToInt(), SqlDbType.DateTime.FormattedString()},
			              		{SqlDbType.Bit.ToInt(), SqlDbType.Bit.FormattedString()},
			              		{SqlDbType.Int.ToInt(), SqlDbType.Int.FormattedString()},
			              	};

			return dbTypes;
		}

		public static bool IsFloatingPoint(this int type)
		{
			switch (type.ToEnum<SqlDbType>())
			{
				case SqlDbType.Float:
				case SqlDbType.Decimal:
					return true;
				default:
					return false;
			}
		}

		public static bool IsNumeric(this int type)
		{
			return type.IsNumber() || type.IsFloatingPoint();
		}

		public static bool IsNumber(this int type)
		{
			switch (type.ToEnum<SqlDbType>())
			{
				case SqlDbType.BigInt:
				case SqlDbType.Int:
					return true;
				default:
					return false;
			}
		}

		public static bool IsBoolean(this int type)
		{
			return type.ToEnum<SqlDbType>() == SqlDbType.Bit;
		}

		public static bool IsDateTime(this int type)
		{
			return type.ToEnum<SqlDbType>() == SqlDbType.DateTime;
		}

		public static bool IsString(this int type)
		{
			return type.ToEnum<SqlDbType>() == SqlDbType.NVarChar;
		}

		public static bool IsNumericOrDateTime(this int type)
		{
			return type.IsNumeric() || type.IsDateTime();
		}


		private static string GetSqlParameter(this ParameterColumn column, int currentIndex, IDictionary<string, object> parameters, out int nextIndex)
		{
			switch (column.Operator)
			{
				case Operator.NotIn:
				case Operator.In:
					nextIndex = currentIndex;
					return string.Format(" {0} {1} ({2}) ",
										 column.ReportColumnName.WrapBraces(),
										 column.Operator.GetOperator(),
										 column.DefaultValue);
				case Operator.Between:
				case Operator.NotBetween:
					var p1 = string.Format("{0}{1}", ProcessorVars.SqlParameterVariable, currentIndex++);
                    var p2 = string.Format("{0}{1}", ProcessorVars.SqlParameterVariable, currentIndex++);
					nextIndex = currentIndex;
					var v1 = column.DefaultValue.IsKeywordDate()
								? column.DefaultValue.ParseKeywordDate().ToString()
					         	: column.DefaultValue;
					var v2 = column.DefaultValue2.IsKeywordDate()
								? column.DefaultValue2.ParseKeywordDate().ToString()
					         	: column.DefaultValue2;
					parameters.Add(p1, v1);
					parameters.Add(p2, v2);
					return string.Format(" {0} {1} @{2} AND @{3} ",
										 column.ReportColumnName.WrapBraces(),
										 column.Operator.GetOperator(),
										 p1, p2);
				default:
                    var p = string.Format("{0}{1}", ProcessorVars.SqlParameterVariable, currentIndex++);
					nextIndex = currentIndex;
					parameters.Add(p, column.FormatSqlParameterValue());
					return string.Format(" {0} {1} @{2} ",
					                     column.ReportColumnName.WrapBraces(),
					                     column.Operator.GetOperator(),
					                     p);
			}
		}

		private static string GetSqlParameter(this RequiredValueParameter requiredValueParam)
		{
			return requiredValueParam.DefaultValue.IsKeywordDate()
			       	? requiredValueParam.DefaultValue.ParseKeywordDate().ToString()
			       	: requiredValueParam.DefaultValue;
		}

		private static string FormatSqlParameterValue(this ParameterColumn column)
		{
			switch (column.Operator)
			{
				case Operator.BeginsWith:
				case Operator.NotBeginsWith:
					return string.Format("{0}%", column.DefaultValue);
				case Operator.EndsWith:
				case Operator.NotEndsWith:
					return string.Format("%{0}", column.DefaultValue);
				case Operator.Contains:
				case Operator.NotContains:
					return string.Format("%{0}%", column.DefaultValue);
				default:
					return column.DefaultValue.IsKeywordDate()
							? column.DefaultValue.ParseKeywordDate().ToString()
					       	: column.DefaultValue;
			}
		}
	}
}
