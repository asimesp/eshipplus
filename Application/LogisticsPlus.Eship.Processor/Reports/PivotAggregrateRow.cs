﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.Processor.Reports
{
	public class PivotAggregrateRow
	{
		public List<PivotAggregrateColumn> Columns { get; set; }
	}
}