﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Reports
{
    public class ReportPivotTable
    {
        public string Name { get; set; }
        public List<ReportPivotColumn> Columns { get; set; }


        public ReportPivotTable(Pivot pivot)
        {
            Name = pivot.TableName;
            Columns = pivot
                .PivotTableColumns
                .Where(c => c.ColumnType == ColumnType.Pivot)
                .Select(c => new ReportPivotColumn
                {
                    Name = c.ReportColumnName,
                    DataType = c.DataType,
                    ColumnType = c.ColumnType,
                    RowValues = new List<object>()
                })
                .ToList();

            Columns.AddRange(pivot
                .PivotTableColumns
                .SelectMany(c => c.AggregateTypes()
                                  .Select(i => new ReportPivotColumn
                                      {
                                          Name = c.ReportColumnName,
                                          DataType = c.DataType,
                                          ColumnType = c.ColumnType,
                                          AggregateType = i,
                                          RowValues = new List<object>()
                                      }))
                .ToList());
        }

        public DataTable GetDataTable()
        {
            var table = new DataTable(Name);

            // add pivot columns first
            var pivotColumns = Columns.Where(i => i.ColumnType == ColumnType.Pivot).ToList();
            foreach (var c in pivotColumns)
            {
                table.Columns.Add(GetColumnName(c), GetTableType(c));
            }

            // add aggregates columns
            var aggregateColumns = Columns.Where(i => i.ColumnType != ColumnType.Pivot).ToList();
            foreach (var c in aggregateColumns)
            {
                table.Columns.Add(GetColumnName(c), GetTableType(c));
            }

            // build data rows
            var aggregateRows = new Dictionary<string, PivotAggregrateRow>();
            var rowCount = Columns[0].RowValues.Count; // NOTE: all columns will have the same number of rows
            for (var rowIndex = 0; rowIndex < rowCount; rowIndex++)
            {
                // build row key/grouping
                var key = pivotColumns.Aggregate(string.Empty, (current, column) => current + column.RowValues[rowIndex].GetString());

                PivotAggregrateRow aggregateRow = null;

                // add row if no existing grouping/row
                if (!aggregateRows.ContainsKey(key))
                {
                    aggregateRow = new PivotAggregrateRow {Columns = new List<PivotAggregrateColumn>()};
                    foreach (var c in pivotColumns) // pivot columns
                        aggregateRow.Columns.Add(new PivotAggregrateColumn
                            {
                                ColumnType = ColumnType.Pivot,
                                Name = c.Name,
								TableType = GetTableType(c),
                                Values = new List<object> { c.RowValues[rowIndex] },
                            });

                    // add empty aggregate columns
                    foreach (var c in aggregateColumns)
                        aggregateRow.Columns.Add(new PivotAggregrateColumn
                            {
                                ColumnType = ColumnType.Aggregate,
                                AggregateType = c.AggregateType,
								Name = c.Name,
								TableType = GetTableType(c),
                                Values = new List<object>(),
                            });
                    aggregateRows.Add(key, aggregateRow);
                }

                // retrieve existing grouping/row
                if (aggregateRow == null) aggregateRow = aggregateRows[key];

                // add values 
                foreach (var c in aggregateColumns)
                    aggregateRow.Columns.First(x => x.AggregateType == c.AggregateType && c.Name == x.Name).Values.Add(c.RowValues[rowIndex]);
            }


            // add rows and return table
            foreach (var aggregrateRow in aggregateRows.Values)
            {
                var row = table.NewRow();
                foreach (var c in aggregrateRow.Columns)
                    row[GetColumnName(c)] = c.ComputedValue;
                table.Rows.Add(row);
            }

            return table;
        }

        private static string GetColumnName(ReportPivotColumn c)
        {
            return c.ColumnType == ColumnType.Aggregate
                       ? string.Format("{0}: {1}", c.AggregateType.FormattedString(), c.Name)
                       : c.Name;
        }

        private static string GetColumnName(PivotAggregrateColumn c)
        {
            return c.ColumnType == ColumnType.Aggregate
                       ? string.Format("{0}: {1}", c.AggregateType.FormattedString(), c.Name)
                       : c.Name;
        }

        private static Type GetTableType(ReportPivotColumn c)
        {
	        var dataType = c.DataType.ToInt();

	        if (dataType.IsNumeric()) return typeof (decimal);

	        if (dataType.IsBoolean()) // boolean only allowed cnt aggregate
		        return c.ColumnType == ColumnType.Aggregate ? typeof (decimal) : typeof (bool);

	        if (dataType.IsDateTime()) // datetime only allowed min, max, cnt aggregate
		        return c.ColumnType == ColumnType.Aggregate
			               ? c.AggregateType == AggregateType.Max || c.AggregateType == AggregateType.Min
				                 ? typeof (DateTime)
				                 : typeof (decimal)
			               : typeof (DateTime);

			// everything else will default to string.  Only allowed cnt aggregate
	        return c.ColumnType == ColumnType.Aggregate ? typeof (decimal) : typeof (string);
        }

        public void AddData(DataRow row)
        {
            foreach (var c in Columns) c.RowValues.Add(row[c.Name]);
        }
    }
}