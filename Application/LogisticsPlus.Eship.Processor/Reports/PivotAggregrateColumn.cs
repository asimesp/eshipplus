﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Reports
{
	public class PivotAggregrateColumn
	{
		public string Name { get; set; }

		public object ComputedValue
		{
			get
			{
				if (Values == null) return null;

				switch (ColumnType)
				{
					case ColumnType.Aggregate:
						switch (AggregateType)
						{
							case AggregateType.Avg:
                                return Values.ConvertAll(input => input.ToDecimal()).Sum() / Values.Count;
							case AggregateType.Cnt:
								return Values.Count();
							case AggregateType.Max:
								return TableType == typeof (DateTime)
									       ? (object) Values.ConvertAll(input => input.ToDateTime()).Max()
									       : Values.ConvertAll(input => input.ToDecimal()).Max();
							case AggregateType.Min:
								return TableType == typeof (DateTime)
									       ? (object) Values.ConvertAll(input => input.ToDateTime()).Min()
									       : Values.ConvertAll(input => input.ToDecimal()).Min();
							case AggregateType.Sum:
                                return Values.ConvertAll(input => input.ToDecimal()).Sum();
						}
						return null;
					default:
						return Values[0];
				}
				
			}
		}

		public ColumnType ColumnType { get; set; }
		public AggregateType AggregateType { get; set; }

		public Type TableType { get; set; }

		public List<object> Values { get; set; }
	}
}