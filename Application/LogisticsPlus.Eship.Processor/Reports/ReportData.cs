﻿using System.Collections.Generic;
using System.Data;

namespace LogisticsPlus.Eship.Processor.Reports
{
	public class ReportData
	{
		private readonly DataTable _table;
	    private readonly List<DataTable> _pivotTables;
		private readonly List<KeyValuePair<string, object>> _summaries;

		public DataTable Table { get { return _table; }  }
		public List<KeyValuePair<string, object>> Summaries { get { return _summaries; } }
        public List<DataTable> PivotTables { get { return _pivotTables; } }

		public bool HasTable { get { return _table != null; } }
        public bool HasPivotTable { get { return _pivotTables.Count > 0; } }
		public bool HasSummaries { get { return _summaries != null; } }

        public ReportData(DataTable table, List<KeyValuePair<string, object>> summaries, List<DataTable> pivotTables)
		{
			_table = table;
		    _pivotTables = pivotTables;
			_summaries = summaries;
		}
	}
}
