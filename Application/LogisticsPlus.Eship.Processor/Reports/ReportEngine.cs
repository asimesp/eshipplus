﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using Newtonsoft.Json;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Reports
{
	public class ReportEngine : EntityBase
	{
		public static int QueryTimeout = -1;

		public ReportData GenerateReport(ReportConfiguration configuration, User user, int maxCellCount = 100000, bool enforceCellLimit = false)
		{
			return GenerateReport(configuration, user, default(long), default(long), maxCellCount, enforceCellLimit);
		}

		public ReportData GenerateReport(ReportConfiguration configuration, User user, long customerGroupId, long vendorGroupId, int maxCellCount = 100000, bool enforceCellLimit = false)
		{
			List<KeyValuePair<string, object>> summaries = null;
			DataTable table;

			var template = configuration.ReportTemplate;
			var query = template.Query;
			var defaultConfig = template.DefaultCustomization.FromXml<ReportCustomization>();
			var config = configuration.SerializedCustomization.FromXml<ReportCustomization>();
			var parameters = new Dictionary<string, object> { { "UserId", user.Id }, { "TenantId", user.TenantId } };

			// customer group and vendor group
			if (configuration.ReportTemplate.CanFilterByCustomerGroup) parameters.Add("CustomerGroupId", customerGroupId);
			if (configuration.ReportTemplate.CanFilterByVendorGroup) parameters.Add("VendorGroupId", vendorGroupId);

			// true-up columns i.e. ensure no changes to db column spec on original template
			foreach (var cc in config.DataColumns.Where(c => !c.Custom))
			{
				var oc = defaultConfig.DataColumns.FirstOrDefault(c => c.Name == cc.Name);
				if (oc != null && oc.Column != cc.Column) cc.Column = oc.Column;
			}

			// columns
			var qc = config.DataColumns.GetSqlDataColumns(config.Summaries);

            // parameters
            var qp = config.Parameters.GetSqlParameters(parameters);

			// required parameters
			config.RequiredValueParameters.GetSqlParameters(parameters);

			// sort columns
			var sc = config.SortColumns.GetSqlSortColumns();

            //find out if the report template uses the tags or the string.format functionality
		    var isUsingTags = query.IndexOf(ReportTemplateFields.Columns, StringComparison.Ordinal) != -1
		                      && query.IndexOf(ReportTemplateFields.Filters, StringComparison.Ordinal) != -1
		                      && query.IndexOf(ReportTemplateFields.OrderBy, StringComparison.Ordinal) != -1;

		    var dataQuery = ConcatenateQueryString(query, isUsingTags, qc, qp, config.SortColumns.Any() ? sc : string.Empty);

		    // decide if the report needs to be queued
		    var recordCountQuery = GenerateRecordCountQuery(query, isUsingTags, qp);

            // display name to sql query column conversions
            // custom columns first (must be first!!!)
            var maxLoops = Math.Pow(config.DataColumns.Count(c => c.Custom), 3).ToInt();
			var cnt = 0;
			do // do loop to handle multiple custom column nesting.
			{
				foreach (var c in config.DataColumns.Where(c => c.Custom))
				{
					var column = c.Name.WrapBraces();
					if (dataQuery.Contains(column)) dataQuery = dataQuery.Replace(column, c.Column.WrapBraces());
				    if (recordCountQuery.Contains(column)) recordCountQuery = recordCountQuery.Replace(column, c.Column.WrapBraces());
                }
            } while (config.DataColumns.Where(c => c.Custom).Any(c => dataQuery.Contains(c.Name.WrapBraces())) && cnt++ < maxLoops);

			foreach (var c in defaultConfig.DataColumns) // regular columns next
			{
				var column = c.Name.WrapBraces();
				if (dataQuery.Contains(column)) dataQuery = dataQuery.Replace(column, c.Column.WrapBraces());
			    if (recordCountQuery.Contains(column)) recordCountQuery = recordCountQuery.Replace(column, c.Column.WrapBraces());
            }
             
		    if (enforceCellLimit)
		    {
		        var rowCount = ExecuteScalar(recordCountQuery, parameters).ToInt();
		        var cellCount = rowCount * config.DataColumns.Count;

		        if (cellCount >= maxCellCount)
		        {
		            var record = new BackgroundReportRunRecord
		            {
		                DateCreated = DateTime.Now,
		                User = user,
		                ReportName = configuration.Name,
		                Query = dataQuery,
                        TenantId = user.TenantId,
		                ReportCustomization = configuration.SerializedCustomization,
		                CompletedReportFileName = string.Empty,
		                Parameters = JsonConvert.SerializeObject(parameters),
		                ExpirationDate = DateUtility.SystemEarliestDateTime
		            };

		            Exception ex;
		            var backgroundReportManager = new BackgroundReportRunRecordManager();
		            backgroundReportManager.SaveBackgroundReport(record, out ex);

		            if (ex != null)
		            {
		                throw ex;
		            }

		            return null;
		        }
		    }

            // run data query

            using (var reader = QueryTimeout == -1 ? GetReader(dataQuery, parameters) : GetReader(dataQuery, parameters, QueryTimeout))
			{
				table = new DataTable(configuration.Name);
				table.Load(reader);
			}
			Connection.Close();

			// build summaries
			if (config.Summaries.Any())
			{
				summaries = new List<KeyValuePair<string, object>>();
				foreach (var sum in config.Summaries)
				{
					var columnHeader = ReportUtilities.SumOf + sum.Name;
					summaries.Add(new KeyValuePair<string, object>(sum.Name, table.Compute(string.Format("Sum([{0}])", columnHeader), string.Empty)));
					table.Columns.Remove(columnHeader);
				}
			}

            // Generate Pivots

			var pvtTab = config
				.Pivots
				.Select(p => new ReportPivotTable(p))
				.ToList();

			foreach(var row in table.Rows.Cast<DataRow>())
				foreach (var pt in pvtTab)
					pt.AddData(row);

			var pivotTables = pvtTab.Select(p => p.GetDataTable()).ToList();

		    return new ReportData(table, summaries, pivotTables);
		}

	    public ReportData GenerateReport(ReportCustomization config, string query, Dictionary<string, object> parameters, string reportName, User user)
	    {
	        List<KeyValuePair<string, object>> summaries = null;
	        DataTable table;

	        // run data query
	        using (var reader = QueryTimeout == -1 ? GetReader(query, parameters) : GetReader(query, parameters, QueryTimeout))
	        {
	            table = new DataTable(reportName);
	            table.Load(reader);
	        }
	        Connection.Close();

	        // build summaries
	        if (config.Summaries.Any())
	        {
	            summaries = new List<KeyValuePair<string, object>>();
	            foreach (var sum in config.Summaries)
	            {
	                var columnHeader = ReportUtilities.SumOf + sum.Name;
	                summaries.Add(new KeyValuePair<string, object>(sum.Name, table.Compute(string.Format("Sum([{0}])", columnHeader), string.Empty)));
	                table.Columns.Remove(columnHeader);
	            }
	        }

	        // Generate Pivots

	        var pvtTab = config
	            .Pivots
	            .Select(p => new ReportPivotTable(p))
	            .ToList();

	        foreach (var row in table.Rows.Cast<DataRow>())
	        foreach (var pt in pvtTab)
	            pt.AddData(row);

	        var pivotTables = pvtTab.Select(p => p.GetDataTable()).ToList();

	        return new ReportData(table, summaries, pivotTables);
	    }

        private static string ConcatenateQueryString(string query, bool isUsingTags, string qc, string qp, string sc)
	    {
            if (isUsingTags)
            {
                query = query.Replace(ReportTemplateFields.Columns, qc);
                query = query.Replace(ReportTemplateFields.Filters, qp);
                query = sc != string.Empty
                        ? query.Replace(ReportTemplateFields.OrderBy, string.Format("{0} {1}", ReportTemplateFields.OrderBySqlKeyword, sc))
                        : query.Replace(ReportTemplateFields.OrderBy, string.Empty);
            }
            else
            {
                // handle order by if no sort columns. NOTE: do here so that don't remove order by in query columns.
                if (sc == string.Empty)
                {
                    var idx = query.ToLower().IndexOf(ReportTemplateFields.OrderBySqlKeyword.ToLower(), StringComparison.Ordinal);
                    if (idx != -1)
                    {
                        var remove = query.Substring(idx, ReportTemplateFields.OrderBySqlKeyword.Length);
                        if (!string.IsNullOrEmpty(remove)) query = query.Replace(remove, string.Empty);
                    }
                }

                query = string.Format(query, qc, qp, sc);
            }
	        return query;
	    }

	    private static string GenerateRecordCountQuery(string query, bool isUsingTags, string qp)
	    {
	        if (isUsingTags)
	        {
	            query = query.Replace(ReportTemplateFields.Columns, " COUNT(*) ");
	            query = query.Replace(ReportTemplateFields.Filters, qp);
	            query = query.Replace(ReportTemplateFields.OrderBy, string.Empty);
	        }
	        else
	        {
	            // remove ORDER BY from the query as we don't need it
	            var idx = query.ToLower().IndexOf(ReportTemplateFields.OrderBySqlKeyword.ToLower(), StringComparison.Ordinal);
	            if (idx != -1)
	            {
	                var remove = query.Substring(idx, ReportTemplateFields.OrderBySqlKeyword.Length);
	                if (!string.IsNullOrEmpty(remove)) query = query.Replace(remove, string.Empty);
	            }

	            query = string.Format(query, " COUNT(*) ", qp, string.Empty);
	        }
	        return query;
	    }
    }
}
