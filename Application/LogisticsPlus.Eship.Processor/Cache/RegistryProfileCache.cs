﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Cache;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Cache
{
	public class RegistryProfileCache
	{
		private readonly Dictionary<long, CachedCollectionItem> _profiles = new Dictionary<long, CachedCollectionItem>();

		public Dictionary<long, Country> Countries
		{
			get { return CoreCache.Countries; }
		}

		public CachedCollectionItem this[long key]
		{
			get
			{
				try
				{
					return _profiles.ContainsKey(key) ? _profiles[key] : null;
				}
				catch
				{
					return null;
				}
			}
			set
			{
				try
				{
					if (value == null) return;
					if (!_profiles.ContainsKey(key)) _profiles.Add(key, value);
					else _profiles[key] = value;
				}
				catch
				{
				}
			}
		}

		public void UpdateCache(Country item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}
	}
}
