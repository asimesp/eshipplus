﻿using System.Linq;
using LogisticsPlus.Eship.Core.Cache;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Processor.Cache
{
	public class RaterRatingProfileCache
	{
		private static readonly object RaterRatingProfileCacheLockObject = new object();

		public CustomerRating this[long key]
		{
			get
			{
				try
				{
					return CoreCache.CustomerRatings.ContainsKey(key) ? CoreCache.CustomerRatings[key] : null;
				}
				catch
				{
					return null;
				}
			}
			set
			{
				try
				{
					if (value == null) return;
					if (!CoreCache.CustomerRatings.ContainsKey(key)) CoreCache.UpdateCache(value);
					else CoreCache.CustomerRatings[key] = value;
				}
				catch
				{
				}
			}
		}

		public void Clear()
		{
			lock (RaterRatingProfileCacheLockObject)
			{
				CoreCache.CustomerRatings.Clear();
			}
		}

		public void Drop(CustomerRating rating)
		{
			CoreCache.UpdateCache(rating, true);
		}

		public void Drop(ResellerAddition addition)
		{
			foreach (var key in CoreCache.CustomerRatings.Keys.Where(k => CoreCache.CustomerRatings[k].ResellerAdditionId == addition.Id).ToList())
				CoreCache.UpdateCache(CoreCache.CustomerRatings[key], true);
		}

		public void Drop(VendorRating rating)
		{
			foreach (var key in CoreCache.CustomerRatings.Keys.Where(k => CoreCache.CustomerRatings[k].LTLSellRates.Any(s => s.VendorRatingId == rating.Id)).ToList())
				CoreCache.UpdateCache(CoreCache.CustomerRatings[key], true);
		}
	}
}
