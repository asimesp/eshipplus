﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Cache;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;

namespace LogisticsPlus.Eship.Processor.Cache
{
	public class CachedCollectionItem
	{
		private readonly long _tenantId = default(long);

		public List<AccountBucket> AccountBuckets { get { return CoreCache.AccountBuckets.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<AccountBucketUnit> AccountBucketUnits { get { return CoreCache.AccountBucketUnits.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<ContactType> ContactTypes { get { return CoreCache.ContactTypes.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<EquipmentType> EquipmentTypes { get { return CoreCache.EquipmentTypes.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<ServiceViewSearchDto> Services { get { return CoreCache.Services.Values.Where(i => i.TenantId == _tenantId).Select(i=>new ServiceViewSearchDto(i)).ToList(); } }
		public List<PackageType> PackageTypes { get { return CoreCache.PackageTypes.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<ChargeCode> ChargeCodes { get { return CoreCache.ChargeCodes.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<MileageSource> MileageSources { get { return CoreCache.MileageSources.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<QuickPayOption> QuickPayOptions { get { return CoreCache.QuickPayOptions.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<FailureCode> FailureCodes { get { return CoreCache.FailureCodes.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<DocumentTag> DocumentTags { get { return CoreCache.DocumentTags.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<Asset> Assets { get { return CoreCache.Assets.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<ShipmentPriority> ShipmentPriorities { get { return CoreCache.ShipmentPriorities.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<InsuranceType> InsuranceTypes { get { return CoreCache.InsuranceTypes.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<PermissionDetail> PermissionDetails { get { return CoreCache.PermissionDetails.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<Prefix> Prefixes { get { return CoreCache.Prefixes.Values.Where(i => i.TenantId == _tenantId).ToList(); } }

		// most applicable location for these two as they are used like registry items.
		public List<SmallPackagingMap> SmallPackagingMaps { get { return CoreCache.SmallPackagingMaps.Values.Where(i => i.TenantId == _tenantId).ToList(); } }
		public List<SmallPackageServiceMap> SmallPackageServiceMaps { get { return CoreCache.SmallPackageServiceMaps.Values.Where(i => i.TenantId == _tenantId).ToList(); } }


		public CachedCollectionItem(long tenantId)
		{
			_tenantId = tenantId;
		}


		public void UpdateCache(AccountBucket item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(AccountBucketUnit item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(ContactType item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(EquipmentType item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(Service item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}
        public void UpdateCache(P44ChargeCodeMapping item, bool drop = false)
        {
            CoreCache.UpdateCache(item, drop);
        }
        public void UpdateCache(P44ServiceMapping item, bool drop = false)
        {
            CoreCache.UpdateCache(item, drop);
        }
        public void UpdateCache(PackageType item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(ChargeCode item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(MileageSource item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(QuickPayOption item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(FailureCode item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(DocumentTag item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(Asset item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(ShipmentPriority item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(InsuranceType item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(PermissionDetail item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(Prefix item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}


		public void UpdateCache(SmallPackagingMap item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}

		public void UpdateCache(SmallPackageServiceMap item, bool drop = false)
		{
			CoreCache.UpdateCache(item, drop);
		}
	}
}
