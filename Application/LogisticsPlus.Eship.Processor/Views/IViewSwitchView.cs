﻿using System;
using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.Processor.Views
{
    public interface IViewSwitchView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<string>> RecordViewSwitch;
    }
}
