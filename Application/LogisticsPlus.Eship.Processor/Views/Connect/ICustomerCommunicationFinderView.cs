﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
	public interface ICustomerCommunicationFinderView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;

        void DisplaySearchResult(List<CustomerViewSearchDto> customers);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
