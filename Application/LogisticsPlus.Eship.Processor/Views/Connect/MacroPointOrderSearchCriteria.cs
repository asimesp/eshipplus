﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
    [Serializable]
    public class MacroPointOrderSearchCriteria
    {
        public List<ParameterColumn> Parameters { get; set; }

        public SearchField SortBy { get; set; }
        public bool SortAscending { get; set; }

        public MacroPointOrderSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }
    }
}
