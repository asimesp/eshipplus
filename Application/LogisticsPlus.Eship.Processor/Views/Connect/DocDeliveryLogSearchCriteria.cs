﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
    [Serializable]
    public class DocDeliveryLogSearchCriteria
    {
		public List<ParameterColumn> Parameters { get; set; }

        public DocDeliveryLogSearchCriteria()
		{
			Parameters = new List<ParameterColumn>();
		}  
    }
}