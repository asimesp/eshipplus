﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Dto.Connect;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
	public interface IFaxTransmissionView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<FaxTransmission>> Save;
		event EventHandler<ViewEventArgs<FaxTransmission>> Delete;
		event EventHandler<ViewEventArgs<FaxTransmission>> Lock;
		event EventHandler<ViewEventArgs<FaxTransmission>> UnLock;
		event EventHandler<ViewEventArgs<FaxTransmission>> LoadAuditLog;
		event EventHandler<ViewEventArgs<FaxTransmissionSearchCriteria>> Search;

		void DisplaySearchResult(List<FaxTransmissionViewSearchDto> transmissions);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
	
		void FailedLock(Lock @lock);
	}
}
