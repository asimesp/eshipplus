﻿using System;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
    public class XmlTransmissionViewSearchCriteria
    {
        public string Criteria { get; set; }
        
		public int SendOkay { get; set; }
    	public int Direction { get; set; }

    	public DateTime StartSendDateTime { get; set; }
    	public DateTime EndSendDateTime { get; set; }
    	public DateTime StartAcknowledgementDateTime { get; set; }
		public DateTime EndAcknowledgementDateTime { get; set; }
    }
}
