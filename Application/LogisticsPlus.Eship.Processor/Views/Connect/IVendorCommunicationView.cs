﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
	public interface IVendorCommunicationView
	{
		User ActiveUser { get; }

		Dictionary<int, string> Milestones { set; }
		Dictionary<int, string> NotificationMethods { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<VendorCommunication>> Save;
		event EventHandler<ViewEventArgs<VendorCommunication>> Delete;
		event EventHandler<ViewEventArgs<VendorCommunication>> Lock;
		event EventHandler<ViewEventArgs<VendorCommunication>> UnLock;
		event EventHandler<ViewEventArgs<VendorCommunication>> LoadAuditLog;
		event EventHandler<ViewEventArgs<string>> VendorSearch;

		void DisplayVendor(Vendor vendor);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void LogException(Exception ex);
		void Set(VendorCommunication communication);
	}
}
