﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Connect;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
    public interface IDocDeliveryLogView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<DocDeliveryLogSearchCriteria>> Search;

        void DisplaySearchResult(List<DocDeliveryLogDto> docDeliveryLogs);
    }
}