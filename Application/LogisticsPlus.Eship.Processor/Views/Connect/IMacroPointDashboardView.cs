﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Operations;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
    public interface IMacroPointDashboardView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<MacroPointOrderSearchCriteria>> Search;

        void DisplaySearchResult(List<MacroPointOrderViewSearchDto> mpOrders);
    }
}
