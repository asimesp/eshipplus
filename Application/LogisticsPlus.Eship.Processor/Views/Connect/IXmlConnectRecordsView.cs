﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Connect;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
	public interface IXmlConnectRecordsView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<XmlConnect, LoadOrder>> ProcessLoadTenderAccept;
		event EventHandler<ViewEventArgs<XmlConnectSearchCriteria>> Search;


		void FinalizeLoadOrderAccepted(XmlConnect connect, LoadOrder order);

		void DisplaySearchResult(List<XmlConnectViewSearchDto> xmlConnectRecords);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
	}
}
