﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
	public interface IVendorCommunicationFinderView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;

		void DisplaySearchResult(List<Vendor> vendors);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
