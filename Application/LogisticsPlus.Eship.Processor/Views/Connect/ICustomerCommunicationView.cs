﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
	public interface ICustomerCommunicationView
	{
		User ActiveUser { get; }

		List<DocumentTag> DocumentTags { set; }
			
		Dictionary<int, string> Milestones { set; }
		Dictionary<int, string> NotificationMethods { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<CustomerCommunication>> Save;
		event EventHandler<ViewEventArgs<CustomerCommunication>> Delete;
		event EventHandler<ViewEventArgs<CustomerCommunication>> Lock;
		event EventHandler<ViewEventArgs<CustomerCommunication>> UnLock;
		event EventHandler<ViewEventArgs<CustomerCommunication>> LoadAuditLog;
		event EventHandler<ViewEventArgs<string>> CustomerSearch;

		void DisplayCustomer(Customer customer);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(CustomerCommunication communication);
	}
}
