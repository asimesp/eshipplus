﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Connect
{
	public interface IXmlTransmissionView
	{
		User ActiveUser { get; }

		Dictionary<int, string> SendOkay { set; }
		Dictionary<int, string> Directions { set; }

		event EventHandler Loading;
		
		event EventHandler<ViewEventArgs<XmlTransmissionViewSearchCriteria>> Search;

		void DisplaySearchResult(List<XmlTransmission> transmissions);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}