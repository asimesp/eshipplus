﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
	public interface ICustomerRatingItemsBatchView
	{
		User ActiveUser { get; set; }

		event EventHandler<ViewEventArgs<List<string[]>>> InsertLTLSellRates;
		event EventHandler<ViewEventArgs<List<string[]>>> RemoveLTLSellRates;

		event EventHandler<ViewEventArgs<List<string[]>>> InsertSmallPackRates;
		event EventHandler<ViewEventArgs<List<string[]>>> RemoveSmallPackRates;

		event EventHandler<ViewEventArgs<List<string[]>>> InsertCustomerServiceMarkups;
		event EventHandler<ViewEventArgs<List<string[]>>> RemoveCustomerServiceMarkups;

        event EventHandler<ViewEventArgs<List<string[]>>> InsertTLSellRates;
        event EventHandler<ViewEventArgs<List<string[]>>> RemoveTLSellRates;

        event EventHandler<ViewEventArgs<List<string[]>>> InsertProfitMarkups;

	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
