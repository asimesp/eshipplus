﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
	public interface IFuelTableView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<FuelTable>> Save;
		event EventHandler<ViewEventArgs<FuelTable>> Delete;
		event EventHandler<ViewEventArgs<FuelTable>> Lock;
		event EventHandler<ViewEventArgs<FuelTable>> UnLock;
		event EventHandler<ViewEventArgs<FuelTable>> LoadAuditLog;


	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(FuelTable fuelTable);
	}
}
