﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
    public interface ISmallPackageServiceMapView
    {
        User ActiveUser { get; }

        Dictionary<int, string> SmallPackageEngines { set; }

        Dictionary<string, string> SmallPackageEngineServices { set; }

        event EventHandler Loading;
        event EventHandler Search;
        event EventHandler<ViewEventArgs<SmallPackageServiceMap>> Save;
        event EventHandler<ViewEventArgs<SmallPackageServiceMap>> Delete;
        event EventHandler<ViewEventArgs<SmallPackageServiceMap>> Lock;
        event EventHandler<ViewEventArgs<SmallPackageServiceMap>> UnLock;

        void DisplaySearchResult(List<SmallPackageServiceMap> smallPackSearchResults);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);

        void FailedLock(Lock @lock);

    }
}
