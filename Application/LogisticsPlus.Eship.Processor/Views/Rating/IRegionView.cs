﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
	public interface IRegionView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<Region>> Save;
		event EventHandler<ViewEventArgs<Region>> Delete;
		event EventHandler<ViewEventArgs<Region>> Lock;
		event EventHandler<ViewEventArgs<Region>> UnLock;
		event EventHandler<ViewEventArgs<Region>> LoadAuditLog;

	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(Region region);
	}
}
