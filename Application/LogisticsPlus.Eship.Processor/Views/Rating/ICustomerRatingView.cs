﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
	public interface ICustomerRatingView
	{
		User ActiveUser { get; }

        List<ResellerAddition> ResellerAdditions { set; }
		List<VendorRating> VendorRatings { set; }

        Dictionary<int, string> RateType { set; }
		Dictionary<int, string> SmallPackEngines { set; }
		Dictionary<SmallPackageEngine, List<string>> SmallPackTypes { set; }
        Dictionary<int, string> TariffType { set; }
		Dictionary<int, string> ValuePercentageTypes { set; }
		Dictionary<int, string> WeightBreaks { set; }
        
		event EventHandler Loading;
		event EventHandler<ViewEventArgs<CustomerRating>> Save;
		event EventHandler<ViewEventArgs<CustomerRating>> Delete;
		event EventHandler<ViewEventArgs<CustomerRating>> Lock;
		event EventHandler<ViewEventArgs<CustomerRating>> UnLock;
		event EventHandler<ViewEventArgs<CustomerRating>> LoadAuditLog;
		event EventHandler<ViewEventArgs<string>> VendorSearch;
		event EventHandler<ViewEventArgs<string>> CustomerSearch;

		void DisplayVendor(Vendor vendor);
		void DisplayCustomer(Customer customer);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(CustomerRating customerRating);
	}
}
