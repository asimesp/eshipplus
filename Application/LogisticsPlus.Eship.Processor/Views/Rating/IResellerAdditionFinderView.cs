﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
    public interface IResellerAdditionFinderView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;

        void DisplaySearchResult(List<ResellerAddition> resellerAdditions);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
