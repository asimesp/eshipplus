﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
	public interface ICustomerTLTenderingProfile
	{
		User ActiveUser { get; }
		
		event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileSave;
        event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileDelete;
        event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileLock;
        event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileUnLock;
        event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileLoadAuditLog;
		event EventHandler<ViewEventArgs<long>> TLProfileCanDeleteLane;


		event EventHandler<ViewEventArgs<string>> CustomerSearch;
		event EventHandler<ViewEventArgs<string>> VendorSearch;

	    void Set(CustomerTLTenderingProfile profile);

		void DisplayCustomer(Customer customer);
        void DisplayVendorForLane(Vendor vendor);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs);
		void FailedLock(Lock @lock);
	
	}
}
