﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
	public interface IVendorRatingLTLFuelSurchargeView
	{
		User ActiveUser { get; }

		Dictionary<int, string> FuelIndexRegions { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<DateTime>> Search;
		event EventHandler<ViewEventArgs<List<VendorRating>>> Save;

		void DisplayVendorRatingSurcharges(List<VendorRatingLTLFuelSurchargeUpdateDto> surcharges);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
