﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Smc;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
	public interface IVendorRatingView
	{
		User ActiveUser { get; }
		SmcServiceSettings RatewareSettings { get; }
		SmcServiceSettings CarrierConnectSettings { get; }

		List<FuelTable> FuelTables { set; }
		List<Region> Regions { set; }
			
		Dictionary<string, string> LTLTariffs { set; }
		Dictionary<string, string> Smc3ServiceLevels { set; }
		Dictionary<int, string> FuelIndexRegions { set; }
		Dictionary<int, string> DiscountTierRateBasis { set; }
		Dictionary<int, string> LTLAccessorialsRateTypes { set; }
        Dictionary<int, string> LTLAdditionalChargeIndexRateTypes { set; }
        Dictionary<int, string> LTLGuaranteedChargeRateTypes { set; }
		Dictionary<int, string> DaysOfWeek { set; }
		Dictionary<int, string> WeightBreaks { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<VendorRating>> Save;
		event EventHandler<ViewEventArgs<VendorRating>> Delete;
		event EventHandler<ViewEventArgs<VendorRating>> Lock;
		event EventHandler<ViewEventArgs<VendorRating>> UnLock;
		event EventHandler<ViewEventArgs<VendorRating>> LoadAuditLog;
		event EventHandler<ViewEventArgs<string>> VendorSearch;

		void DisplayVendor(Vendor vendor);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(VendorRating vendorRating);
	}
}
