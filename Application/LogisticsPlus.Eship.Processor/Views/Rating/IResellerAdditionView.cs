﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
   public interface IResellerAdditionView
    {
       User ActiveUser { get; }

       Dictionary<int, string> ValuePercentagTypes { set; }

       event EventHandler Loading;
       event EventHandler<ViewEventArgs<ResellerAddition>> LoadAuditLog;
       event EventHandler<ViewEventArgs<ResellerAddition>> Save;
       event EventHandler<ViewEventArgs<ResellerAddition>> Delete;
       event EventHandler<ViewEventArgs<ResellerAddition>> Lock;
       event EventHandler<ViewEventArgs<ResellerAddition>> UnLock;
	   event EventHandler<ViewEventArgs<string>> CustomerSearch;

       void LogException(Exception ex);
       void DisplayMessages(IEnumerable<ValidationMessage> messages);
       void DisplayCustomer(Customer customer);
       void FailedLock(Lock @lock);
	   void Set(ResellerAddition addition);
       void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
    }
}
