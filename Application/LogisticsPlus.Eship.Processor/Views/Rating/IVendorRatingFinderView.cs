﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
	public interface IVendorRatingFinderView
	{
		User ActiveUser { get; }

        event EventHandler<ViewEventArgs<VendorRatingViewSearchCriteria>> Search;

		void DisplaySearchResult(List<VendorRating> vendorRatings);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
