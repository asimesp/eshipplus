﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
	public interface IBatchRateDataView
	{
		User ActiveUser { get; }

		List<VendorRating> VendorRatingProfiles { set; }

		Dictionary<int, string> SmallPackEngines { set; }
		Dictionary<SmallPackageEngine, List<string>> SmallPackTypes { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<List<BatchRateData>>> Save;
		event EventHandler<ViewEventArgs<List<BatchRateData>>> Delete;
		event EventHandler<ViewEventArgs<ServiceMode>> Search;
	    event EventHandler<ViewEventArgs<VendorRatingViewSearchCriteria>> RefreshVendorRatingProfiles;

        void DisplayBatchRateDataSet(List<BatchRateData> data);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
	}
}