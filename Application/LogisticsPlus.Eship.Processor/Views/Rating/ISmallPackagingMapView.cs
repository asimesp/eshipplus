﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
    public interface ISmallPackagingMapView
    {
        User ActiveUser { get; }

        Dictionary<int, string> SmallPackageEngines { set; }

        Dictionary<SmallPackageEngine, Dictionary<string, string>> SmallPackageEnginesTypes { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<SmallPackagingMap>> Save;
        event EventHandler<ViewEventArgs<SmallPackagingMap>> Delete;
        event EventHandler<ViewEventArgs<SmallPackagingMap>> Lock;
        event EventHandler<ViewEventArgs<SmallPackagingMap>> UnLock;
        event EventHandler<ViewEventArgs<string>> Search;

        void DisplaySearchResult(List<SmallPackagingMap> smallPackagingMaps);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);

        void FailedLock(Lock @lock);
    }
}
