﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Rating
{
    public interface ICustomerTLTenderProfileFinderView
	{
		User ActiveUser { get; }

        event EventHandler<ViewEventArgs<CustomerTLTenderingProfileViewSearchCriteria>> Search;

		void DisplaySearchResult(List<CustomerTLTenderingProfile> profile);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
