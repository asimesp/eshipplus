﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views
{
	public interface ILoginView
	{
		string UserName { get; }
		string Password { get; }
		string AccessCode { get; }
		bool AdminAuthentication { get; }
        string Domain { get; }
        AuthenticationType AuthenticationType { get; }

		event EventHandler AuthenticateUser;
		event EventHandler RetrieveUserPassword;
		event EventHandler<ViewEventArgs<string>> PasswordChange;

		void NotifyInActiveAccount();
		void NotifyDisabledUser();
		void NotifyUserNotFound();
		void NotifyInvalidPassword();
		void NotifyLoginAttemptsExceeded();
		void NotifySuccesfulLogin(User user);
		void NotifySuccesfulLogin(AdminUser user);
		void ProcessPasswordRetrieval(string password, string userEmail);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void ProcessPasswordReset();
	}
}
