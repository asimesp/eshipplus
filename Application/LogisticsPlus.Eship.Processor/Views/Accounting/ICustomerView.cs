﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface ICustomerView
	{
		User ActiveUser { get; }

        Dictionary<int, string> CustomerTypes { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<Customer>> Save;
		event EventHandler<ViewEventArgs<Customer>> Delete;
		event EventHandler<ViewEventArgs<Customer>> Lock;
		event EventHandler<ViewEventArgs<Customer>> UnLock;
		event EventHandler<ViewEventArgs<Customer>> LoadAuditLog;
        event EventHandler<ViewEventArgs<string>> SalesRepSearch;
		event EventHandler<ViewEventArgs<string>> TierSearch;
		event EventHandler<ViewEventArgs<string>> PrefixSearch;
		event EventHandler<ViewEventArgs<string>> AccountBucketSearch;
	    event EventHandler<ViewEventArgs<List<UserShipAs>>> AddCustomerUsers;
        event EventHandler<ViewEventArgs<Customer>> CreateCustomerInPaymentGateway;
        event EventHandler<ViewEventArgs<Customer>> DeleteCustomerInPaymentGateway;

		void DisplaySalesRep(SalesRepresentative salesRepresentative);
		void DisplayTier(Tier tier);
		void DisplayPrefix(Prefix prefix);
		void DisplayAccountBucket(AccountBucket accountBucket);
        void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void SetId(long id);
	}
}
