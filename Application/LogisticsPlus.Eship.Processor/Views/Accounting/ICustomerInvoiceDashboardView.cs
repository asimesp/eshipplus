﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface ICustomerInvoiceDashboardView
    {
        User ActiveUser { get; }
		
		List<DocumentTag> DocumentTags { set; }

		event EventHandler Loading;
        event EventHandler<ViewEventArgs<InvoiceViewSearchCriteria>> Search;

        void DisplaySearchResult(List<InvoiceDashboardDto> invoices);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
