﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface ICommissionsToBePaidView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<List<CommissionPayment>>> Save;
        event EventHandler<ViewEventArgs<CommissionsToBePaidViewSearchCriteria>> Search;
        event EventHandler<ViewEventArgs<List<CommissionPayment>>> BatchImport;

        void DisplaySearchResult(List<CommissionsToBePaidViewSearchDto> commissions);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
