﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface ISalesRepresentativeView
	{
		User ActiveUser { get; }

        Dictionary<int, string> ServiceModes { set; }

		event EventHandler Loading;
        event EventHandler<ViewEventArgs<SalesRepresentative>> LoadAuditLog;
		event EventHandler<ViewEventArgs<SalesRepresentative>> Save;
		event EventHandler<ViewEventArgs<SalesRepresentative>> Delete;
		event EventHandler<ViewEventArgs<SalesRepresentative>> Lock;
		event EventHandler<ViewEventArgs<SalesRepresentative>> UnLock;
        event EventHandler<ViewEventArgs<List<SalesRepresentative>>> BatchImport;
        event EventHandler<ViewEventArgs<string>> CustomerSearch;

        void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayCustomer(Customer customer);
        void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
        void Set(SalesRepresentative salesRep);
	}
}
