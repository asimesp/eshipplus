﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IVendorBillPostingView
    {
        User ActiveUser { get; }

		event EventHandler<ViewEventArgs<VendorBillViewSearchCriteria>> Search;
		event EventHandler<ViewEventArgs<List<VendorBill>>> Post;

        void DisplaySearchResult(List<VendorBillDashboardDto> vendorBills);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
