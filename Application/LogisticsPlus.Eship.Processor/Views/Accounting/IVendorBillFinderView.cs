﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface IVendorBillFinderView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<VendorBillViewSearchCriteria>> Search;

		void DisplaySearchResult(List<VendorBillDashboardDto> vendorBillDtos);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
