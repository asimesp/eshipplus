﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface IPendingVendorView
	{
		User ActiveUser { get; }
		bool CreateAndSaveVendor { get; }

		Dictionary<int, string> BrokerReferenceTypes { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<PendingVendor>> Save;
		event EventHandler<ViewEventArgs<Vendor>> SaveVendor;
		event EventHandler<ViewEventArgs<PendingVendor>> Delete;
		event EventHandler<ViewEventArgs<PendingVendor>> Lock;
		event EventHandler<ViewEventArgs<PendingVendor>> UnLock;
		event EventHandler<ViewEventArgs<PendingVendor>> LoadAuditLog;

		/// <summary>
		/// Will return destination file path
		/// </summary>
		/// <param name="vendor"></param>
		/// <param name="sourceFileVirtualPath"></param>
		/// <returns></returns>
		string CopyFileForVendor(Vendor vendor, string sourceFileVirtualPath);
		void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(PendingVendor pendingVendor);
		void Set(Vendor vendor);
	}
}
