﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface ICustomerFinderView
	{
		User ActiveUser { get; }

        event EventHandler<ViewEventArgs<CustomerViewSearchCriteria>> Search;

        void DisplaySearchResult(List<CustomerViewSearchDto> customers);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
