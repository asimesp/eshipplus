﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IAverageWeeklyFuelView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<AverageWeeklyFuel>> Save;
        event EventHandler<ViewEventArgs<AverageWeeklyFuel>> Delete;
        event EventHandler<ViewEventArgs<AverageWeeklyFuel>> Lock;
        event EventHandler<ViewEventArgs<AverageWeeklyFuel>> UnLock;
        event EventHandler<ViewEventArgs<AverageWeeklyFuelViewSearchCriteria>> Search;
        event EventHandler<ViewEventArgs<List<AverageWeeklyFuel>>> BatchImport;

        void DisplaySearchResult(List<AverageWeeklyFuelViewSearchDto> averageWeeklyFuels);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);

        void FailedLock(Lock @lock);
    }
}
