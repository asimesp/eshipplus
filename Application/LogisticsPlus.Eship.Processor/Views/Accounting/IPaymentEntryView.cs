﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IPaymentEntryView
    {
        User ActiveUser { get; }

        Dictionary<string, string> PaymentProfiles { set; }

        event EventHandler<ViewEventArgs<long, string>> LoadPaymentProfile;
        event EventHandler<ViewEventArgs<long>> LoadPaymentProfiles;
        event EventHandler<ViewEventArgs<long, string>> DeletePaymentProfile;
        event EventHandler<ViewEventArgs<MiscReceipt, PaymentInformation>> AuthorizeAndCapturePayment;
        event EventHandler<ViewEventArgs<MiscReceipt>> SaveMiscReceipt;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> msgs);
        void DisplayPaymentProfile(PaymentGatewayPaymentProfile profile);
    }
}
