﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IMiscReceiptDashboardView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<MiscReciptViewSearchCriteria>> Search;
        event EventHandler<ViewEventArgs<MiscReceipt, decimal>> RefundMiscReceipt;
        event EventHandler<ViewEventArgs<MiscReceipt>> VoidMiscReceipt;
        
        void LogException(Exception ex);
        void DisplaySearchResult(List<MiscReceiptViewSearchDto> miscReceipts);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
