﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IApplyMiscReceiptToInvoiceView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<List<ValidationMessage>>> DisplayProcessingMessages;
        event EventHandler<ViewEventArgs<Invoice, List<MiscReceiptApplication>>> SaveMiscReceiptApplicationsAndInvoice;
        event EventHandler<ViewEventArgs<Invoice>> LoadAndLockMiscReceiptsApplicableToPostedInvoice;
        event EventHandler<ViewEventArgs<Invoice, List<MiscReceipt>>> UnlockInvoiceAndMiscReceipts;

        void DisplayMiscReceipts(List<MiscReceiptViewSearchDto> miscReceipts);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void FailedMiscReceiptLocks();
        void LogException(Exception ex);
    }
}
