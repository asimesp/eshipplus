﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IVendorDisputeFinderView
    {
        User ActiveUser { get; }

        bool OnlyActiveResults { get; }

        event EventHandler<ViewEventArgs<VendorDisputeViewSearchCriteria>> Search;

        void DisplaySearchResult(List<VendorDispute> vendorDisputes);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}