﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface ICustomerPurchaseOrderView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Save;
        event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Delete;
        event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Lock;
        event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> UnLock;
        event EventHandler<ViewEventArgs<PurchaseOrderViewSearchCriteria>> Search;
        event EventHandler<ViewEventArgs<List<CustomerPurchaseOrder>>> BatchImport;
        event EventHandler<ViewEventArgs<string>> CustomerSearch;

        void DisplaySearchResult(List<CustomerPurchaseOrder> customerPurchaseOrders);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayCustomer(Customer customer);
        void FailedLock(Lock @lock);
    }
}
