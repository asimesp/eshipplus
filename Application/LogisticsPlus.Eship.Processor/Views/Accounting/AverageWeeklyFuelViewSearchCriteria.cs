﻿using System;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public class AverageWeeklyFuelViewSearchCriteria
    {
        public string Criteria { get; set; }
        public long TenantId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
