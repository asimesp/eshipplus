﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    [Serializable]
    public class MiscReciptViewSearchCriteria
    {
        public List<ParameterColumn> Parameters { get; set; }

        public SearchField SortBy { get; set; }
        public bool SortAscending { get; set; }

        public MiscReciptViewSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }
    }
}
