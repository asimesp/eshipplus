﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IInvoiceView
    {
        User ActiveUser { get; }

        Dictionary<int, string> InvoiceDetailReferenceType { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<Invoice>> Save;
		event EventHandler<ViewEventArgs<Invoice>> Delete;
		event EventHandler<ViewEventArgs<Invoice>> Lock;
		event EventHandler<ViewEventArgs<Invoice>> UnLock;
		event EventHandler<ViewEventArgs<Invoice>> Search;
		event EventHandler<ViewEventArgs<string>> PrefixSearch;
		event EventHandler<ViewEventArgs<Invoice>> LoadAuditLog;
		event EventHandler<ViewEventArgs<string>> CustomerSearch;
        event EventHandler<ViewEventArgs<long>> LoadAndLockMiscReceiptsApplicableToPostedInvoice;
        event EventHandler<ViewEventArgs<Invoice, List<MiscReceiptApplication>>> SaveMiscReceiptApplicationsAndInvoice;
        event EventHandler<ViewEventArgs<Invoice, List<MiscReceipt>>> UnlockInvoiceAndMiscReceipts;

		void DisplayPrefix(Prefix prefix);
		void DisplayCustomer(Customer customer);
        void DisplayMiscReceipts(List<MiscReceiptViewSearchDto> miscReceipts);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);

        void FailedMiscReceiptLocks();
		void FailedLock(Lock @lock);
		void Set(Invoice invoice);

    }
}
