﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IPaymentProfileManagerView
    {
        User ActiveUser { get; }

        List<PaymentGatewayPaymentProfile> PaymentProfiles { set; }

        event EventHandler<ViewEventArgs<long>> LoadPaymentProfiles;
        event EventHandler<ViewEventArgs<long, string>> DeletePaymentProfile;
        event EventHandler<ViewEventArgs<long, PaymentGatewayPaymentProfile>> AddOrUpdatePaymentProfile;


        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
