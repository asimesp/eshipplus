﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	[Serializable]
	public class CommissionsToBePaidViewSearchCriteria
	{
		public bool OnlyCommsWithNoPayment { get; set; }
		public long ActiveUserId { get; set; }

		public List<ParameterColumn> Parameters { get; set; }
	}
}
