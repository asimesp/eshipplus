﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IInvoiceDashboardView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<InvoiceViewSearchCriteria>> Search;

        void DisplaySearchResult(List<InvoiceDashboardDto> invoices);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
