﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface IDocumentPackDownloadView
	{
		User ActiveUser { get; }

		List<DocumentTag> DocumentTags { set; }

		Dictionary<int, string> EntityTypes { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<DocumentPackEntityDtoSearchCriteria>> Search;

		void DisplaySearchResult(List<DocumentPackEntityDto> entities);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
