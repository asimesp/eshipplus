﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IVendorBillView
    {
        User ActiveUser { get; }

        Dictionary<int, string> VendorBillDetailReferenceType { set; }

        event EventHandler Loading;
		event EventHandler<ViewEventArgs<VendorBill>> Save;
		event EventHandler<ViewEventArgs<VendorBill>> Delete;
		event EventHandler<ViewEventArgs<VendorBill>> Lock;
		event EventHandler<ViewEventArgs<VendorBill>> UnLock;
		event EventHandler<ViewEventArgs<VendorBill>> Search;
		event EventHandler<ViewEventArgs<VendorBill>> LoadAuditLog;
		event EventHandler<ViewEventArgs<string>> VendorSearch;
        event EventHandler<ViewEventArgs<List<VendorBill>>> BatchSave; 
		
		void DisplayVendor(Vendor vendor);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		
		void FailedLock(Lock @lock);
		void Set(VendorBill bill);
    }
}
