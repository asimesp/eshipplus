﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public class PurchaseOrderViewSearchCriteria
    {
        public long CustomerId;

        public List<ParameterColumn> Parameters;

        public PurchaseOrderViewSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }
    }
}
