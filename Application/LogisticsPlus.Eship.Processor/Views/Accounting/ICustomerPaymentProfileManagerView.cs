﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface ICustomerPaymentProfileManagerView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<string>> CustomerSearch;

        void DisplayCustomer(Customer customer);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);

    }
}
