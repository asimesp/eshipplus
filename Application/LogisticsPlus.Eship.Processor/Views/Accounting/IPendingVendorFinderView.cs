﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface IPendingVendorFinderView
	{
		User ActiveUser { get; }

		bool OnlyApprovedResults { get; }

		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;

		void DisplaySearchResult(List<PendingVendor> pendingVendors);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
