﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IInvoicePaidAmountUpdatesView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<List<Invoice>>> Save;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayUpdatedRecords(List<InvoicePaidAmountUpdateDto> updatedInvoices);
    }
}
