﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public class CustomerViewSearchCriteria
    {
        public bool OnlyActiveCustomers { get; set; }

        public List<ParameterColumn> Parameters { get; set; }

        public CustomerViewSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }
    }
}