﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views.Operations;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface IShipmentsToBeInvoicedView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<List<Shipment>>> SetShipmentsReadyToInvoice;
		
		event EventHandler<ViewEventArgs<List<Shipment>>> SingleSave;
		event EventHandler<ViewEventArgs<List<Shipment>>> MultiSave;

		event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;

		void DisplaySearchResult(List<ShipmentDashboardDto> shipments);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}