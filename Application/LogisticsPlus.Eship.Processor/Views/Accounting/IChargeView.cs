﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Dto.Accounting;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IChargeView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<ChargeViewSearchCriteria>> Search;
        event EventHandler<ViewEventArgs<List<ChargeViewSearchDto>>> ConfirmCharges;
		event EventHandler<ViewEventArgs<List<ChargeViewSearchDto>>> DisputeCharges;

        void LogException(Exception ex);
        void OnConfirmChargesComplete();
	    void OnDisputeChargesComplete();
        void DisplaySearchResult(List<ChargeItemDto> charges);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void SendNotification(List<ChargeViewSearchDto> charges, ChargeNotificationType type);
    }
}
