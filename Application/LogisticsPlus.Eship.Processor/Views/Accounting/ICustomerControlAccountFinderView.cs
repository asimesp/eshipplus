﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface ICustomerControlAccountFinderView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<CustomerControlAccountSearchCriteria>> Search;

		void DisplaySearchResult(List<CustomerControlAccount> customerControlAccounts);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
