﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface IVendorView
	{
		User ActiveUser { get; }

		Dictionary<int, string> BrokerReferenceTypes { set; }
        Dictionary<int, string> DateUnits { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<Vendor>> Save;
		event EventHandler<ViewEventArgs<Vendor>> Delete;
		event EventHandler<ViewEventArgs<Vendor>> Lock;
		event EventHandler<ViewEventArgs<Vendor>> UnLock;
		event EventHandler<ViewEventArgs<string>> VendorServiceRepSearch;
		event EventHandler<ViewEventArgs<Vendor>> LoadAuditLog;

	    void LogException(Exception ex);
		void DisplayVendorServiceRep(User user);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(Vendor vendor);
	}
}
