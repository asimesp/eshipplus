﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IMiscReceiptApplicationView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<List<MiscReceiptApplication>>> ApplyMiscReceiptToInvoices;
        event EventHandler<ViewEventArgs<MiscReceiptApplication>> Save;
        event EventHandler<ViewEventArgs<MiscReceiptApplication>> Delete;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
