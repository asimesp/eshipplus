﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IInvoicePayView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<List<Invoice>>> LockInvoices;
        event EventHandler<ViewEventArgs<MiscReceipt, PaymentInformation>> ProcessPayment;

        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void FailedLock(List<string> invoices);
        void LogException(Exception ex);
    }
}
