﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface ICustomerControlAccountView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<CustomerControlAccount>> Save;
		event EventHandler<ViewEventArgs<CustomerControlAccount>> Delete;
		event EventHandler<ViewEventArgs<CustomerControlAccount>> Lock;
		event EventHandler<ViewEventArgs<CustomerControlAccount>> UnLock;
        event EventHandler<ViewEventArgs<CustomerControlAccountSearchCriteria>> Search;
		event EventHandler<ViewEventArgs<List<CustomerControlAccount>>> BatchImport;
		event EventHandler<ViewEventArgs<string>> CustomerSearch;

		void DisplaySearchResult(List<CustomerControlAccount> customerControlAccounts);
        void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayCustomer(Customer customer);
		void FailedLock(Lock @lock);
	}
}
