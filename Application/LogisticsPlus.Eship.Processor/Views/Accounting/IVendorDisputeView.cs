﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IVendorDisputeView
    {
        User ActiveUser { get; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<VendorDispute>> Save;
        event EventHandler<ViewEventArgs<VendorDispute>> Delete;
        event EventHandler<ViewEventArgs<VendorDispute>> Resolve;
        event EventHandler<ViewEventArgs<VendorDispute>> Lock;
        event EventHandler<ViewEventArgs<VendorDispute>> UnLock;
		event EventHandler<ViewEventArgs<VendorDispute>> LoadAuditLog;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void FailedLock(Lock @lock);
	    void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
        void Set(VendorDispute vendorDispute);
		void OnVendorDisputeResolvedComplete(VendorDispute vendorDispute);
    }
}