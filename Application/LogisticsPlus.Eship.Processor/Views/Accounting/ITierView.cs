﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	public interface ITierView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<Tier>> Save;
		event EventHandler<ViewEventArgs<Tier>> Delete;
		event EventHandler<ViewEventArgs<Tier>> Lock;
		event EventHandler<ViewEventArgs<Tier>> UnLock;
		event EventHandler<ViewEventArgs<Tier>> LoadAuditLog;

	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(Tier tier);
	}
}
