﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IPaymentRefundOrVoidView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<Shipment, List<MiscReceipt>>> RefundOrVoidMiscReceiptsForShipment;
        event EventHandler<ViewEventArgs<LoadOrder, List<MiscReceipt>>> RefundOrVoidMiscReceiptsForLoadOrder;
        event EventHandler<ViewEventArgs<Shipment, List<MiscReceipt>>> LockShipmentAndMiscReceipts;
        event EventHandler<ViewEventArgs<LoadOrder, List<MiscReceipt>>> LockLoadOrderAndMiscReceipts;
        event EventHandler<ViewEventArgs<List<MiscReceipt>>> UnlockMiscReceipts;
        
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> msgs);
        void FailedLock();
    }
}
