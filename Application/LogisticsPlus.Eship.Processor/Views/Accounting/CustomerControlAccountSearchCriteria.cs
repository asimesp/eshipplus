﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public class CustomerControlAccountSearchCriteria
    {
        public long CustomerId { get; set; }

		public List<ParameterColumn> Parameters { get; set; }


        public CustomerControlAccountSearchCriteria()
		{
			Parameters = new List<ParameterColumn>();
		} 
    }
}