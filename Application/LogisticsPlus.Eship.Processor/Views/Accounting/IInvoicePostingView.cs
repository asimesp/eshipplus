﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
    public interface IInvoicePostingView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<InvoiceViewSearchCriteria>> Search;
		event EventHandler<ViewEventArgs<List<Invoice>>> Post;
		event EventHandler<ViewEventArgs<List<Invoice>>> PostDownload;

    	void HandlePostedInvoicesAndMessages(IEnumerable<Invoice> invoices , IEnumerable<ValidationMessage> messages);
    	void ProcessNotifications(IEnumerable<Invoice> invoices);
        void DisplaySearchResult(List<InvoiceDashboardDto> invoices);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
