﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;

namespace LogisticsPlus.Eship.Processor.Views.Accounting
{
	[Serializable]
	public class VendorBillViewSearchCriteria
	{
		public long ActiveUserId { get; set; }
		public List<ParameterColumn> Parameters { get; set; }

        public SearchField SortBy { get; set; }
        public bool SortAscending { get; set; }

		public VendorBillViewSearchCriteria()
		{
			Parameters = new List<ParameterColumn>();
		}
	}
}
