﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface IAddressBookView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<AddressBook>> Save;
        event EventHandler<ViewEventArgs<AddressBook>> Delete;
        event EventHandler<ViewEventArgs<AddressBook>> Lock;
        event EventHandler<ViewEventArgs<AddressBook>> UnLock;
        event EventHandler<ViewEventArgs<AddressBook>> LoadAuditLog;
		event EventHandler<ViewEventArgs<string>> CustomerSearch;
		event EventHandler<ViewEventArgs<List<AddressBook>>> BatchImport;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    	void DisplayCustomer(Customer customer);
        void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
        void FailedLock(Lock @lock);
        void Set(AddressBook addressBook);
    }
}
