﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	[Serializable]
	public class BookingRequestViewSearchCriteria
	{
		public bool DisableNoRecordNotification { get; set; }


		public long ActiveUserId { get; set; }
		public List<ParameterColumn> Parameters { get; set; }

		public BookingRequestViewSearchCriteria()
		{
			Parameters = new List<ParameterColumn>();
		}
	}
}
