﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface ILoadOrderFinderView
	{
		User ActiveUser { get; }

        bool FilterForLoadOrderToExludeAttachedToJob { get; }

        event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;

		void DisplaySearchResult(List<ShipmentDashboardDto> loadOrders);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
