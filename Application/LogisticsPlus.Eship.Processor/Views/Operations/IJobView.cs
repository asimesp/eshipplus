﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IJobView
	{
        User ActiveUser { get; }
        Dictionary<int, string> StatusTypes { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<Job, List<JobComponentDto>>> Save;
        event EventHandler<ViewEventArgs<Job>> Delete;
        event EventHandler<ViewEventArgs<Job>> Lock;
        event EventHandler<ViewEventArgs<Job>> UnLock;
        event EventHandler<ViewEventArgs<Job>> LoadAuditLog;
        event EventHandler<ViewEventArgs<string>> CustomerSearch;

        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayCustomer(Customer customer);
        void LogException(Exception ex);
        void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
        void Set(Job job, List<JobComponentDto> jobComponentDtos);
        void FailedLock(Lock @lock);
    }
}