﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IServiceTicketView
	{
		User ActiveUser { get; }

		List<EquipmentType> EquipmentTypes { set; }
		List<ServiceViewSearchDto> Services { set; }
        List<Asset> Assets { set; }

		Dictionary<int, string> NoteTypes { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<ServiceTicket>> Save;
		event EventHandler<ViewEventArgs<ServiceTicket>> Delete;
		event EventHandler<ViewEventArgs<ServiceTicket>> Lock;
		event EventHandler<ViewEventArgs<ServiceTicket>> UnLock;
		event EventHandler<ViewEventArgs<ServiceTicket>> LoadAuditLog;

		event EventHandler<ViewEventArgs<string>> CustomerSearch;
		event EventHandler<ViewEventArgs<string>> AccountBucketSearch;
		event EventHandler<ViewEventArgs<string>> VendorSearch;
		event EventHandler<ViewEventArgs<string>> PrefixSearch;
        event EventHandler<ViewEventArgs<List<ServiceTicket>>> BatchImport;


		void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers);
		void DisplayAccountBucket(AccountBucket accountBucket);
		void DisplayPrefix(Prefix prefix);
		void DisplayVendor(Vendor vendor);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(ServiceTicket serviceTicket);
	}
}
