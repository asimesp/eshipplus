﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface ICustomerTLTenderingProfile
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> Save;
        event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> Delete;
        event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> Lock;
        event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> UnLock;
        event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> LoadAuditLog;

		event EventHandler<ViewEventArgs<string>> CustomerSearch;
		event EventHandler<ViewEventArgs<string>> VendorSearch;

	    void Set(CustomerTLTenderingProfile profile);

		void DisplayCustomer(Customer customer);
        void DisplayVendorForLane(Vendor vendor);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs);
		void FailedLock(Lock @lock);
	
	}
}
