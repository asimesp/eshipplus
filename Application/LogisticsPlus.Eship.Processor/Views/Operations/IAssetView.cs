﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface IAssetView
    {
        User ActiveUser { get; }

        Dictionary<int, string> SearchAssetTypes { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<Asset>> Save;
        event EventHandler<ViewEventArgs<Asset>> Delete;
        event EventHandler<ViewEventArgs<Asset>> Lock;
        event EventHandler<ViewEventArgs<Asset>> UnLock;
        event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        event EventHandler<ViewEventArgs<List<Asset>>> BatchImport;

        void DisplaySearchResult(List<Asset> assets);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);

        void FailedLock(Lock @lock);

    }
}
