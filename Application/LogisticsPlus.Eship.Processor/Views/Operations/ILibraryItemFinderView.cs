﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface ILibraryItemFinderView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<LibraryItemSearchCriteria>> Search;

		void DisplaySearchResult(List<LibraryItemViewSearchDto> libraryItems);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
