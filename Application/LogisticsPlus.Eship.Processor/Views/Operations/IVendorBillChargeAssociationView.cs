﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IVendorBillChargeAssociationView
	{
		User ActiveUser { get; }

        VendorBillAssosciationType ReferenceType { get; set; }
        long ChargeVendorId { get; set; }

	    event EventHandler<ViewEventArgs<List<ShipmentCharge>,VendorBill>> SaveToVendorBillForShipment;
	    event EventHandler<ViewEventArgs<List<ServiceTicketCharge>,VendorBill>> SaveToVendorBillForServiceTicket;

		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
	}
}
