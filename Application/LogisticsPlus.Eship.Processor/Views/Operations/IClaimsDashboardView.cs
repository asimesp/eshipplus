﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface IClaimsDashboardView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<ClaimViewSearchCriteria>> Search;

        void DisplaySearchResult(List<ClaimViewSearchDto> claims);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);

    }
}
