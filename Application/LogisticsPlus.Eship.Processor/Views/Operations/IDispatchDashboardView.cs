﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface IDispatchDashboardView
    {
        User ActiveUser { get; }

        bool LockFailed { get; set; }

        List<EquipmentType> EquipmentTypes { set; }

        Dictionary<int, string> LoadOrderStatuses { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<LoadOrder>> SaveLoadOrder;
        event EventHandler<ViewEventArgs<Shipment>> SaveShipment;
        event EventHandler<ViewEventArgs<LoadOrder>> LockLoadOrder;
        event EventHandler<ViewEventArgs<LoadOrder>> UnLockLoadOrder;
        event EventHandler<ViewEventArgs<Shipment>> LockShipment;
        event EventHandler<ViewEventArgs<Shipment>> UnLockShipment;
        event EventHandler<ViewEventArgs<string>> VendorSearch;

        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayVendor(Vendor vendor, bool checkVendor = true);
        void Set(Shipment shipment);
        void Set(LoadOrder loadOrder);
        void LogException(Exception ex);
    }
}
