﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface ICarrierPreferredLaneView
	{

        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<VendorPreferredLane>> Save;
        event EventHandler<ViewEventArgs<VendorPreferredLane>> Delete;
        event EventHandler<ViewEventArgs<VendorPreferredLane>> Lock;
        event EventHandler<ViewEventArgs<VendorPreferredLane>> UnLock;
        event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        event EventHandler<ViewEventArgs<string>> VendorSearch;
        event EventHandler<ViewEventArgs<List<VendorPreferredLane>>> BatchImport;

        void LogException(Exception ex);
        void DisplaySearchResult(List<VendorPreferredLaneViewSearchDto> vendorPreferredLanes);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayVendor(Vendor vendor);
        void FailedLock(Lock @lock);
	}
}
