﻿namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public enum VendorBillAssosciationType
    {
        Shipment = 0,
        ServiceTicket
    }
}
