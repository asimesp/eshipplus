﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface ITruckloadBid
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<TruckloadBid>> TlSave;
		event EventHandler<ViewEventArgs<TruckloadBid>> TlDelete;
		event EventHandler<ViewEventArgs<TruckloadBid>> TlLock;
		event EventHandler<ViewEventArgs<TruckloadBid>> TlUnLock;
		event EventHandler<ViewEventArgs<TruckloadBid>> TlLoadAuditLog;

	
		void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs);
		void FailedLock(Lock @lock);

	}
}
