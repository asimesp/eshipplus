﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IAddressBookFinderView
	{
		User ActiveUser { get; }

        event EventHandler<ViewEventArgs<AddressBookViewSearchCriteria>> Search;
        event EventHandler<ViewEventArgs<List<AddressBook>>> DeleteSelected;

		void DisplaySearchResult(List<AddressBookViewSearchDto> addressBooks);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void ProcessSuccessfulDelete();
        void LogException(Exception ex);
	}
}
