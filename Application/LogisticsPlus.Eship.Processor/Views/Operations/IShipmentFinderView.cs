﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IShipmentFinderView
	{
		User ActiveUser { get; }
		bool FilterForShipmentsToBeInvoicedOnly { get; }
        bool FilterForShipmentsToExludeAttachedToJob { get; }

        event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;

		void DisplaySearchResult(List<ShipmentDashboardDto> shipments);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
