﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface IShipmentDashboardView
    {
        User ActiveUser { get; }

        bool LockFailed { get; set; }

        Dictionary<int, string> NoteTypes { set; }

        event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;
        event EventHandler Loading;
        event EventHandler<ViewEventArgs<LoadOrder>> SaveLoadOrder;
        event EventHandler<ViewEventArgs<Shipment>> SaveShipment;
        event EventHandler<ViewEventArgs<LoadOrder>> LockLoadOrder;
        event EventHandler<ViewEventArgs<LoadOrder>> UnLockLoadOrder;
        event EventHandler<ViewEventArgs<Shipment>> LockShipment;
        event EventHandler<ViewEventArgs<Shipment>> UnLockShipment;

        void DisplaySearchResult(List<ShipmentDashboardDto> shipments);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);

        void LogException(Exception ex);
        void Set(Shipment shipment);
        void Set(LoadOrder loadOrder);
    }
}
