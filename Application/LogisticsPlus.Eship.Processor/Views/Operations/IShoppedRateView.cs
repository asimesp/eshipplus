﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface IShoppedRateView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<ShoppedRateSearchCriteria>> Search;

        void DisplaySearchResult(List<ShoppedRateViewSearchDto> shoppedRates);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
