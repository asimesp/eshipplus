﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface IDatLoadboardAssetView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<DatAssetSearchCriteria>> Search;
		event EventHandler<ViewEventArgs<DatLoadboardAssetPosting,bool>> CheckDatDelete;
	    event EventHandler<ViewEventArgs<LoadOrder>> CheckDatUpdate;

        void DisplaySearchResult(List<DatLoadboardAssetPosting> assets);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DatRemovalError();
    }
}
