﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IRateAndScheduleConfirmationView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<Shipment>> UpdateShipmentDocumentsAndTracking;
		event EventHandler<ViewEventArgs<Shipment>> UpdateShipmentCheckCalls;
		event EventHandler<ViewEventArgs<Shipment>> UpdateShipmentNotes;
		event EventHandler<ViewEventArgs<Project44DispatchResponse>> SaveProject44DispatchRequestResponse;
		event EventHandler<ViewEventArgs<SMC3TrackRequestResponse>> SaveSmc3TrackRequestResponse;
		event EventHandler<ViewEventArgs<SMC3DispatchRequestResponse>> SaveSmc3DispatchRequestResponse;

	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
