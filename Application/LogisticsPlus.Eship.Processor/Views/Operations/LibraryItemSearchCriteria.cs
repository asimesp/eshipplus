﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public class LibraryItemSearchCriteria
    {
        public long CustomerId { get; set; }
        public long ActiveUserId { get; set; }

        public List<ParameterColumn> Parameters { get; set; }

        public LibraryItemSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }

    }
}