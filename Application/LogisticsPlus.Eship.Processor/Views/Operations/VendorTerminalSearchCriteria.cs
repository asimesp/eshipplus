﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public class VendorTerminalSearchCriteria
    {
        public long VendorId;

        public List<ParameterColumn> Parameters;
 
        public VendorTerminalSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }
    }
}
