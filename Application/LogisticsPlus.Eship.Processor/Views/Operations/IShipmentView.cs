﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IShipmentView
	{
		User ActiveUser { get; }

        bool RefundOrVoidMiscReceipts { get; }
        bool SaveOriginToAddressBook { get; }
		bool SaveDestinationToAddressBook { get; }

		List<ServiceViewSearchDto> Services { set; }
		List<EquipmentType> EquipmentTypes { set; }
		List<Asset> Assets { set; }

	    Dictionary<int, string> DateUnits { set; }
        Dictionary<int, string> InDisputeReasons { set; }
        Dictionary<int, string> NoteTypes { set; }
		Dictionary<int, string> ServiceModes { set; }
		Dictionary<int, string> MileageEngines { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<Shipment>> Save;
		event EventHandler<ViewEventArgs<Shipment>> Delete;
		event EventHandler<ViewEventArgs<Shipment>> Lock;
		event EventHandler<ViewEventArgs<Shipment>> UnLock;
		event EventHandler<ViewEventArgs<Shipment>> LoadAuditLog;
		event EventHandler<ViewEventArgs<string>> CustomerSearch;
		event EventHandler<ViewEventArgs<string>> VendorSearch;
		event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;
		event EventHandler<ViewEventArgs<SMC3TrackRequestResponse>> SaveSmc3TrackRequestResponse;
	    event EventHandler<ViewEventArgs<Project44TrackingResponse>> SaveProject44TrackRequestResponse;

        void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers);
		void DisplayVendor(Vendor vendor);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(Shipment shipment);
		void Set(ShoppedRate shoppedRate);
	}
}
