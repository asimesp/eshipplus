﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IAssetFinderView
	{
		User ActiveUser { get; }

		bool ShowActiveRecordsOnly { get; }

		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;

		void DisplaySearchResult(List<Asset> assets);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
