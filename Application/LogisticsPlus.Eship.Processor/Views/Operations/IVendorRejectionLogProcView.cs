﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IVendorRejectionLogProcView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<VendorRejectionLog>> LogVendorRejection;

		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
	}
}
