﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	[Serializable]
	public class BidViewSearchCriteria
	{
		public long ActiveUserId { get; set; }
		public List<ParameterColumn> Parameters { get; set; }

		public BidViewSearchCriteria()
		{
			Parameters = new List<ParameterColumn>();
		}
	}
}