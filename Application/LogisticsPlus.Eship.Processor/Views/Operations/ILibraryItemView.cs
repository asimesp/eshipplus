﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface ILibraryItemView
    {
        User ActiveUser { get; }
        
        event EventHandler<ViewEventArgs<LibraryItem>> Save;
        event EventHandler<ViewEventArgs<LibraryItem>> Delete;
        event EventHandler<ViewEventArgs<LibraryItemSearchCriteria>> Search;
        event EventHandler<ViewEventArgs<LibraryItem>> Lock;
        event EventHandler<ViewEventArgs<LibraryItem>> UnLock;
        event EventHandler<ViewEventArgs<List<LibraryItem>>> BatchImport;
        event EventHandler<ViewEventArgs<string>> CustomerSearch;

        void DisplaySearchResult(List<LibraryItem> libraryItems);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayCustomer(Customer customer);
        void FailedLock(Lock @lock);
    }
}
