﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IServiceTicketFinderView
	{
		User ActiveUser { get; }
		bool FilterForServiceTicketsToBeInvoicedOnly { get; }
        bool FilterForServiceTicketsToExludeAttachedToJob { get; }

        event EventHandler<ViewEventArgs<ServiceTicketViewSearchCriteria>> Search;

		void DisplaySearchResult(List<ServiceTicketDashboardDto> serviceTickets);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
