﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface ILoadOrderView
	{
		User ActiveUser { get; }

        bool RefundOrVoidMiscReceipts { get; }
        bool SaveOriginToAddressBook { get; }
		bool SaveDestinationToAddressBook { get; }

		List<ServiceViewSearchDto> Services { set; }
		List<EquipmentType> EquipmentTypes { set; }

        Dictionary<int, string> DateUnits { set; }
		Dictionary<int, string> NoteTypes { set; }
		Dictionary<int, string> ServiceModes { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<LoadOrder>> Save;
        event EventHandler<ViewEventArgs<LoadOrder>> Delete;
        event EventHandler<ViewEventArgs<LoadOrder>> Lock;
        event EventHandler<ViewEventArgs<LoadOrder>> UnLock;
        event EventHandler<ViewEventArgs<LoadOrder>> LoadAuditLog;
		event EventHandler<ViewEventArgs<string>> CustomerSearch;
		event EventHandler<ViewEventArgs<string>> VendorSearch;
    	event EventHandler<ViewEventArgs<LoadOrder, Shipment>> SaveConvertToShipment;

		void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers);
		void DisplayVendor(Vendor vendor);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(LoadOrder loadOrder);
    	void FinalizeConvertToShipment(Shipment shipment);
	}
}
