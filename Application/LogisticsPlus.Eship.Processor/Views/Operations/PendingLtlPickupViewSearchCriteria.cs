﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    [Serializable]
    public class PendingLtlPickupViewSearchCriteria
    {
        public long ActiveUserId { get; set; }
		public List<ParameterColumn> Parameters { get; set; }

		public SearchField SortBy { get; set; }
		public bool SortAscending { get; set; }

        public PendingLtlPickupViewSearchCriteria()
		{
			Parameters = new List<ParameterColumn>();
		}
    }
}
