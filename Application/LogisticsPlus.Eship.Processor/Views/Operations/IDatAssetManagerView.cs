﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface IDatAssetManagerView
    {
        User ActiveUser { get; }

        Dictionary<int, string> RateBasedOnTypes { set; }

        event EventHandler Loading;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
