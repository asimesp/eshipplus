﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface ICarrierTerminalView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<VendorTerminal>> Save;
        event EventHandler<ViewEventArgs<VendorTerminal>> Delete;
        event EventHandler<ViewEventArgs<VendorTerminal>> Lock;
        event EventHandler<ViewEventArgs<VendorTerminal>> UnLock;
        event EventHandler<ViewEventArgs<VendorTerminalSearchCriteria>> Search;
        event EventHandler<ViewEventArgs<string>> VendorSearch;
        event EventHandler<ViewEventArgs<List<VendorTerminal>>> BatchImport;

        void LogException(Exception ex);
        void DisplaySearchResult(List<VendorTerminal> vendorTerminals);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayVendor(Vendor vendor);
        void FailedLock(Lock @lock);
    }
}
