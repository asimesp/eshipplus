﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public class AddressBookViewSearchCriteria
	{
		public long CustomerId { get; set; }
	    public long UserId { get; set; }

	    public List<ParameterColumn> Parameters { get; set; }

        public AddressBookViewSearchCriteria()
		{
			Parameters = new List<ParameterColumn>();
		}
	}
}
