﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface IPendingLtlPickupsDashboardView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<PendingLtlPickupViewSearchCriteria>> Search;
        event EventHandler<ViewEventArgs<Shipment>> UpdateShipment;
        event EventHandler<ViewEventArgs<Shipment>> Lock;
        event EventHandler<ViewEventArgs<Shipment>> UnLock;

        void DisplaySearchResult(List<PendingLtlPickupsDashboardDto> shipments);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void FailedLock(Lock @lock);
        void LogException(Exception ex);
    }
}
