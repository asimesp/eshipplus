﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IClaimView
	{
		User ActiveUser { get; }

		Dictionary<int, string> NoteTypes { set; }
        Dictionary<int, string> ClaimTypes { set; } 
        Dictionary<int, string> AmountClaimedTypes { set; } 

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<Claim>> Save;
		event EventHandler<ViewEventArgs<Claim>> Delete;
		event EventHandler<ViewEventArgs<Claim>> Lock;
		event EventHandler<ViewEventArgs<Claim>> UnLock;
		event EventHandler<ViewEventArgs<Claim>> LoadAuditLog;

		event EventHandler<ViewEventArgs<string>> CustomerSearch;

		void DisplayCustomer(Customer customer);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs);
		void FailedLock(Lock @lock);
		void Set(Claim claim);
	}
}
