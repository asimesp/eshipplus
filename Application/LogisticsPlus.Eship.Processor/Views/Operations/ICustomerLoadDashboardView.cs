﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface ICustomerLoadDashboardView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;
    	event EventHandler<ViewEventArgs<Shipment>> VoidShipment;

    	void ProcessVoidComplete(Shipment shipment);
        void DisplaySearchResult(List<ShipmentDashboardDto> shipments);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
