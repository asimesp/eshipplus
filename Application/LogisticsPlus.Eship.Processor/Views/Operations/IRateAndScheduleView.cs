﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IRateAndScheduleView
	{
		User ActiveUser { get; }

		bool SaveOriginToAddressBook { get; }
		bool SaveOriginWithServicesToAddressBook { get; }
		bool SaveDestinationToAddressBook { get; }
		bool SaveDestinationWithServicesToAddressBook { get; }

		List<ServiceViewSearchDto> Services { set; }

		event EventHandler Loading;
        event EventHandler<ViewEventArgs<Shipment, PaymentInformation>> SaveShipment;
        event EventHandler<ViewEventArgs<LoadOrder, PaymentInformation>> SaveLoadOrder;
		event EventHandler<ViewEventArgs<string>> CustomerSearch;
		event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;


		void DisplayCustomer(Customer customer);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void Set(Shipment shipment);
		void Set(LoadOrder loadOrder);
		void Set(ShoppedRate shoppedRate);
	}
}
