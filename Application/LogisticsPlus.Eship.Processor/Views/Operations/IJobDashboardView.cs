﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IJobDashboardView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<JobViewSearchCriteria>> Search;
		event EventHandler<ViewEventArgs<Tuple<List<Job>, List<Shipment>, List<LoadOrder>, List<ServiceTicket>>>> BatchImport;
		event EventHandler<ViewEventArgs<Tuple<List<Shipment>, List<LoadOrder>, List<ServiceTicket>>>> UnLinkEntitiesFromJob;

		void DisplaySearchResult(List<JobDashboardDto> shipments);
	    void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
	    void SearchForImportedJobs(string jobQuery);
	    void ProcessNotifications(List<Shipment> shipments, List<LoadOrder> loadOrders);
	}
}
