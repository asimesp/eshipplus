﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface ITruckloadBidView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<TruckloadBid, LoadOrder>> TruckloadBidSave;
		event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidDelete;
		event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidLock;
		event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidUnLock;
		event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidLoadAuditLog;

	
		void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs);
		void FailedLock(Lock @lock);

		List<ValidationMessage> TruckloadBidMsgs { get; set; }

	}
}
