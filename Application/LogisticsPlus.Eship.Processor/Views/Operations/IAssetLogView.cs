﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
    public interface IAssetLogView
    {
        User ActiveUser { get; }

        Dictionary<int, string> MileageEngines { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<AssetLog>> Save;
        event EventHandler<ViewEventArgs<AssetLog>> Delete;
        event EventHandler<ViewEventArgs<AssetLog>> Lock;
        event EventHandler<ViewEventArgs<AssetLog>> UnLock;
        event EventHandler<ViewEventArgs<long>> Search;
        event EventHandler<ViewEventArgs<string>> AssetSearch;

        void DisplaySearchResult(List<AssetLogDto> assetLogs);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayAsset(Asset asset);

        void FailedLock(Lock @lock);
    }
}
