﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Operations
{
	public interface IJobFinderView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<JobViewSearchCriteria>> Search;

		void DisplaySearchResult(List<JobDashboardDto> jobDashboardDtos);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void ProcessItemSelection(long jobId);
	}
}