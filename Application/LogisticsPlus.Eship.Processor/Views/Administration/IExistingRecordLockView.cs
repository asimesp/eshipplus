﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Administration
{
	public interface IExistingRecordLockView
	{
		User ActiveUser { get; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<long>> ReleaseLock;


		void DisplayExistingLocks(List<LockDto> locks);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
	}
}
