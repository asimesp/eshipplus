﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Administration
{
	public interface IUserView
	{
		User ActiveUser { get; }

		List<UserDepartment> Departments { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<string>> CustomerSearch;
		event EventHandler<ViewEventArgs<string>> VendorSearch;
		event EventHandler<ViewEventArgs<User>> Save;
		event EventHandler<ViewEventArgs<User>> Delete;
		event EventHandler<ViewEventArgs<User>> Lock;
		event EventHandler<ViewEventArgs<User>> UnLock;
		event EventHandler<ViewEventArgs<User>> LoadAuditLog;
		event EventHandler<ViewEventArgs<List<Group>>> UpdateGroupUsers;
		event EventHandler<ViewEventArgs<List<GroupUser>>> RemoveGroupUsers;
        event EventHandler<ViewEventArgs<List<User>>> BatchImport;

	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void DisplayCustomer(Customer customer);
	    void DisplayVendor(Vendor vendor);
		void FailedLock(Lock @lock);
		void Set(User user);
	}
}
