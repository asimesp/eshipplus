﻿namespace LogisticsPlus.Eship.Processor.Views.Administration
{
    public class TenantUserResetCriteria
    {
        public string TenantCode { get; set; }
        public string UserName { get; set; }
        public bool ResetPassword { get; set; }
        public bool EnableUser { get; set; }
    }
}
