﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Administration
{
	public interface IUserFinderView
	{
		User ActiveUser { get; }
		bool FilterForEmployees { get; }

		event EventHandler<ViewEventArgs<UserSearchCriteria>> Search;

		void DisplaySearchResult(List<UserDto> users);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
