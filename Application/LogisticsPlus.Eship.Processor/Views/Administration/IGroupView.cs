﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Administration
{
	public interface IGroupView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<Group>> Save;
		event EventHandler<ViewEventArgs<Group>> Delete;
		event EventHandler<ViewEventArgs<Group>> Lock;
		event EventHandler<ViewEventArgs<Group>> UnLock;
		event EventHandler<ViewEventArgs<Group>> LoadAuditLog;

	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
		void FailedLock(Lock @lock);
		void Set(Group group);
	}
}
