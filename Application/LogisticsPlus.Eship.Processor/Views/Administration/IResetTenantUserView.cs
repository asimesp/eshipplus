﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Administration
{
    public interface IResetTenantUserView
    {
        AdminUser ActiveSuperUser { get; }

        event EventHandler<ViewEventArgs<TenantUserResetCriteria>> Reset;

        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void LogException(Exception ex);
    }
}
