﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Administration
{
    public interface IAdminUserView
    {
		AdminUser ActiveSuperUser { get; }

        event EventHandler<ViewEventArgs<AdminUser>> Save;
        event EventHandler<ViewEventArgs<AdminUser>> Delete;
        event EventHandler<ViewEventArgs<AdminUser>> LoadAuditLog;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
        void Set(AdminUser adminUser);

    }
}
