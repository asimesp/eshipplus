﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Administration
{
    public interface IUserImpersonationView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<UserSearchCriteria>> Search;

        void DisplaySearchResult(List<UserDto> schedules);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}

