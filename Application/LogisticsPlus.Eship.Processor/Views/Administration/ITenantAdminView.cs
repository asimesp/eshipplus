﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Administration
{
    public interface ITenantAdminView
    {
        User ActiveUser { get; }

        List<string> TimeList { set; }

	    Dictionary<int, string> MileageEngines { set; }
		
        Dictionary<int, string> PaymentGatewayTypes { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<Tenant>> Save;
        event EventHandler<ViewEventArgs<Tenant>> Lock;
        event EventHandler<ViewEventArgs<Tenant>> UnLock;
        event EventHandler<ViewEventArgs<Tenant>> LoadAuditLog;
        event EventHandler<ViewEventArgs<string>> DefaultSystemUserSearch;

        void DisplayDefaultSystemUser(User user);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);

        void FailedLock(Lock @lock);
    }
}
