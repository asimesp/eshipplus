﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
	public interface IVendorPerformanceSummaryDisplayView
	{
		User ActiveUser { get; }

        Dictionary<int, string> DateUnits { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<string>> CustomerSearch;
		event EventHandler<ViewEventArgs<string>> VendorSearch;

		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayCustomer(Customer customer);
		void DisplayVendor(Vendor vendor);
	}
}
