﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public interface IReportTemplateView
    {
        AdminUser ActiveSuperUser { get; }

        event EventHandler<ViewEventArgs<ReportTemplate>> Save;
        event EventHandler<ViewEventArgs<ReportTemplate>> Delete;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void Set(ReportTemplate reportTemplate);
    }
}
