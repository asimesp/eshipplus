﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public interface ICustomerGroupView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<CustomerGroup>> Save;
        event EventHandler<ViewEventArgs<CustomerGroup>> Delete;
        event EventHandler<ViewEventArgs<CustomerGroup>> Lock;
        event EventHandler<ViewEventArgs<CustomerGroup>> UnLock;
        event EventHandler<ViewEventArgs<CustomerGroup>> LoadAuditLog;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
        void FailedLock(Lock @lock);
		void Set(CustomerGroup @group);
    }
}
