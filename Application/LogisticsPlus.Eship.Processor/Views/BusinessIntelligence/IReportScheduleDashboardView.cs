﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public interface IReportScheduleDashboardView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<ReportScheduleViewSearchCriteria>> Search;

        void DisplaySearchResult(List<ReportSchedule> schedules);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
