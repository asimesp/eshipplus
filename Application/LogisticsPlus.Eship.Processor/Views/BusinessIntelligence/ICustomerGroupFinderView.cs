﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public interface ICustomerGroupFinderView
    {

        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;

        void DisplaySearchResult(List<CustomerGroup> customerGroups);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
