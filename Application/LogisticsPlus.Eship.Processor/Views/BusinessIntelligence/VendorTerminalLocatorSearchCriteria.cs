﻿namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public class VendorTerminalLocatorSearchCriteria
    {
        public string PostalCode { get; set; }
        public long CountryId { get; set; }
        public decimal Radius { get; set; }
		public bool UseKilometers { get; set; }

    	public VendorTerminalLocatorSearchCriteria()
    	{
    		UseKilometers = false;
    	}
    }
}
