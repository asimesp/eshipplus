﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public interface IVendorTerminalLocatorView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<VendorTerminalLocatorSearchCriteria>> Search;

        void DisplaySearchResult(List<VendorTerminalLocatorDto> terminals);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
