﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public interface IVendorLaneRateHistoryView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<LaneHistorySearchCriteria>> Search;

        void DisplaySearchResult(List<LaneHistoryRateDto> laneHistoryRates);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
