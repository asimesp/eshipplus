﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public interface IQlikUserView
    {
        AdminUser ActiveSuperUser { get; }

        event EventHandler Search;
        event EventHandler<ViewEventArgs<QlikUser>> Save;
        event EventHandler<ViewEventArgs<QlikUser>> Delete;
        event EventHandler<ViewEventArgs<List<QlikUser>>> BatchImport;

        void DisplaySearchResult(List<QlikUser> qlikUsers);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void LogException(Exception ex);
    }
}
