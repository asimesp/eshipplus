﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public interface IReportSchedulerView
    {
        User ActiveUser { get; }

		Dictionary<int, string> ScheduleIntervals { set; }
		Dictionary<int, string> ExportFileExtension { set; }
    	List<string> Time { set; }


    	event EventHandler Loading;
		event EventHandler<ViewEventArgs<ReportSchedule>> Lock;
		event EventHandler<ViewEventArgs<ReportSchedule>> UnLock;
		event EventHandler<ViewEventArgs<ReportSchedule>> Search;
		event EventHandler<ViewEventArgs<ReportSchedule>> Save;
		event EventHandler<ViewEventArgs<ReportSchedule>> Delete;
		event EventHandler<ViewEventArgs<ReportSchedule>> LoadAuditLog;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs);
    	void FailedLock(Lock @lock);
		void Set(ReportSchedule reportSchedule);
    }
}