﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public interface IVendorGroupView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<VendorGroup>> Save;
        event EventHandler<ViewEventArgs<VendorGroup>> Delete;
        event EventHandler<ViewEventArgs<VendorGroup>> Lock;
        event EventHandler<ViewEventArgs<VendorGroup>> UnLock;
        event EventHandler<ViewEventArgs<VendorGroup>> LoadAuditLog;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
        void FailedLock(Lock @lock);
		void Set(VendorGroup @group);
    }
}
