﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.BusinessIntelligence
{
    public interface IReportConfigurationView
    {
        User ActiveUser { get; }

		Dictionary<int, string> SqlDbTypes { set; }
		Dictionary<int, string> ReportConfigurationVisibilities { set; }
		Dictionary<int, string> Operators { set; }
    	Dictionary<int, string> ReportChartTypes { set; }
    	Dictionary<int, string> SortDirections { set; }
    	Dictionary<int, string> ChartDataSourceTypes { set; }

    	event EventHandler Loading;
		event EventHandler<ViewEventArgs<ReportConfiguration>> Lock;
		event EventHandler<ViewEventArgs<ReportConfiguration>> UnLock;
		event EventHandler<ViewEventArgs<ReportConfiguration>> Search;
		event EventHandler<ViewEventArgs<ReportConfiguration>> Save;
		event EventHandler<ViewEventArgs<ReportConfiguration>> Delete;
		event EventHandler<ViewEventArgs<ReportConfiguration>> LoadAuditLog;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs);
    	void FailedLock(Lock @lock);
		void Set(ReportConfiguration reportConfiguration);
    }
}