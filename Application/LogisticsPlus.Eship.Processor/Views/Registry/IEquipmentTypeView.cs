﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IEquipmentTypeView
	{
		User ActiveUser { get; }

        Dictionary<int, string> DatEquipmentTypes { set; }
        Dictionary<int, string> Groups { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<EquipmentType>> Save;
		event EventHandler<ViewEventArgs<EquipmentType>> Delete;
		event EventHandler<ViewEventArgs<EquipmentType>> Lock;
		event EventHandler<ViewEventArgs<EquipmentType>> UnLock;
        event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
	    event EventHandler<ViewEventArgs<List<EquipmentType>>> BatchImport;

		void DisplaySearchResult(List<EquipmentType> equipmentType);
        void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}