﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IDocumentTemplateView
	{
		User ActiveUser { get; }

		bool ForceDefault { get; }

		List<string> DecimalPlaces { set; }
		Dictionary<int,string> Categories { set; }
		Dictionary<int, string> ServiceModes { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<DocumentTemplate>> Save;
		event EventHandler<ViewEventArgs<DocumentTemplate>> Delete;
		event EventHandler<ViewEventArgs<DocumentTemplate>> Lock;
		event EventHandler<ViewEventArgs<DocumentTemplate>> UnLock;
		event EventHandler<ViewEventArgs<string>> Search;

		void DisplaySearchResult(List<DocumentTemplate> documentTemplate);
        void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	    bool TemplateFileHasBeenSaved();

		void FailedLock(Lock @lock);
	}
}