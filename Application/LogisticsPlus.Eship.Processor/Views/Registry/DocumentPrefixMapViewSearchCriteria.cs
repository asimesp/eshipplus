﻿namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public class DocumentPrefixMapViewSearchCriteria
	{
		public long TenantId { get; set; }
		public long PrefixId { get; set; }
		public long DocumentTemplateId { get; set; }
	}
}
