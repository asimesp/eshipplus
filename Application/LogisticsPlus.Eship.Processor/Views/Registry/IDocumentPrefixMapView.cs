﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IDocumentPrefixMapView
	{
		User ActiveUser { get; }

		List<DocumentTemplate> DocumentTemplates { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<DocumentPrefixMap>> Save;
		event EventHandler<ViewEventArgs<DocumentPrefixMap>> Delete;
		event EventHandler<ViewEventArgs<DocumentPrefixMap>> Lock;
		event EventHandler<ViewEventArgs<DocumentPrefixMap>> UnLock;
		event EventHandler<ViewEventArgs<DocumentPrefixMapViewSearchCriteria>> Search;

		void DisplaySearchResult(List<DocumentPrefixMapViewSearchDto> documentPrefixMap);
        void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}
