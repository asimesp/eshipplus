﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IPostalCodeFinderView
	{
		event EventHandler<ViewEventArgs<PostalCodeViewSearchCriteria>> Search;

		void DisplaySearchResult(List<PostalCodeViewSearchDto> postalCodes);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
