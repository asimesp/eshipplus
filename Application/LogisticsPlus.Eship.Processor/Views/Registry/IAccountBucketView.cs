﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IAccountBucketView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<AccountBucket>> Save;
		event EventHandler<ViewEventArgs<AccountBucket>> Delete;
		event EventHandler<ViewEventArgs<AccountBucket>> Lock;
		event EventHandler<ViewEventArgs<AccountBucket>> UnLock;
		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        event EventHandler<ViewEventArgs<List<AccountBucket>>> BatchImport;

		void DisplaySearchResult(List<AccountBucket> accountBuckets);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void LogException(Exception ex);

		void FailedLock(Lock @lock);
	}
}