﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IPrefixView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<Prefix>> Save;
		event EventHandler<ViewEventArgs<Prefix>> Delete;
		event EventHandler<ViewEventArgs<Prefix>> Lock;
		event EventHandler<ViewEventArgs<Prefix>> UnLock;
		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        event EventHandler<ViewEventArgs<List<Prefix>>> BatchImport;

		void DisplaySearchResult(List<Prefix> prefix);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}
