﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IShipmentPriorityView
	{
		User ActiveUser { get; }

		bool ForceDefault { get; }

		event EventHandler<ViewEventArgs<ShipmentPriority>> Save;
		event EventHandler<ViewEventArgs<ShipmentPriority>> Delete;
		event EventHandler<ViewEventArgs<ShipmentPriority>> Lock;
		event EventHandler<ViewEventArgs<ShipmentPriority>> UnLock;
		event EventHandler<ViewEventArgs<string>> Search;
        event EventHandler<ViewEventArgs<List<ShipmentPriority>>> BatchImport;

		void DisplaySearchResult(List<ShipmentPriority> shipmentPriorities);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}
