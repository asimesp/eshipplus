﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IPermissionDetailView 
	{
		User ActiveUser { get; }

		Dictionary<int, string> Categories { set; }
		Dictionary<int, string> Types { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<PermissionDetail>> Save;
		event EventHandler<ViewEventArgs<PermissionDetail>> Delete;
		event EventHandler<ViewEventArgs<PermissionDetail>> Lock;
		event EventHandler<ViewEventArgs<PermissionDetail>> UnLock;
		event EventHandler<ViewEventArgs<string>> Search;
		event EventHandler<ViewEventArgs<List<PermissionDetail>>> BatchImport;

		void DisplaySearchResult(List<PermissionDetail> permissionDetails);
		void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}
