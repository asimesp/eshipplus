﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IMileageSourceView
	{
		User ActiveUser { get; }

		bool ForcePrimary { get; }

		Dictionary<int, string> MileageEngines { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<MileageSource>> Save;
		event EventHandler<ViewEventArgs<MileageSource>> Delete;
		event EventHandler<ViewEventArgs<MileageSource>> Lock;
		event EventHandler<ViewEventArgs<MileageSource>> UnLock;
		event EventHandler<ViewEventArgs<string>> Search;
        event EventHandler<ViewEventArgs<List<MileageSource>>> BatchImport;

		void DisplaySearchResult(List<MileageSource> mileageSources);
	    void LogException(Exception ex); 
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}
