﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IP44ServiceView
    {
		User ActiveUser { get; }

        Dictionary<int, string> Project44Codes { set; }
        List<Service> ServiceCodes { set; }

        event EventHandler Loading;
		event EventHandler<ViewEventArgs<P44ServiceMapping>> Save;
		event EventHandler<ViewEventArgs<P44ServiceMapping>> Delete;
		event EventHandler<ViewEventArgs<P44ServiceMapping>> Lock;
		event EventHandler<ViewEventArgs<P44ServiceMapping>> UnLock;
		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        event EventHandler<ViewEventArgs<List<P44ServiceMapping>>> BatchImport;

		void DisplaySearchResult(List<P44ServiceMapping> service);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FaildedLock(Lock @lock);
	}
}
