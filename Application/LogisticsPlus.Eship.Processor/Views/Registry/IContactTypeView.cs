﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IContactTypeView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<ContactType>> Save;
		event EventHandler<ViewEventArgs<ContactType>> Delete;
		event EventHandler<ViewEventArgs<ContactType>> Lock;
		event EventHandler<ViewEventArgs<ContactType>> UnLock;
		event EventHandler<ViewEventArgs<string>> Search;
        event EventHandler<ViewEventArgs<List<ContactType>>> BatchImport;

		void DisplaySearchResult(List<ContactType> contactType);
        void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}