﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
    public interface IUserDepartmentView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<UserDepartment>> Save;
        event EventHandler<ViewEventArgs<UserDepartment>> Delete;
        event EventHandler<ViewEventArgs<UserDepartment>> Lock;
        event EventHandler<ViewEventArgs<UserDepartment>> UnLock;
        event EventHandler<ViewEventArgs<string>> Search;
        event EventHandler<ViewEventArgs<List<UserDepartment>>> BatchImport;

        void DisplaySearchResult(List<UserDepartment> userDepartments);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);

        void FailedLock(Lock @lock);
    }
}
