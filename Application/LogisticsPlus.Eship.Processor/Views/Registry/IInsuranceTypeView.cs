﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IInsuranceTypeView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<InsuranceType>> Save;
		event EventHandler<ViewEventArgs<InsuranceType>> Delete;
		event EventHandler<ViewEventArgs<InsuranceType>> Lock;
		event EventHandler<ViewEventArgs<InsuranceType>> UnLock;
		event EventHandler<ViewEventArgs<string>> Search;
        event EventHandler<ViewEventArgs<List<InsuranceType>>> BatchImport;

		void DisplaySearchResult(List<InsuranceType> insuranceType);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}