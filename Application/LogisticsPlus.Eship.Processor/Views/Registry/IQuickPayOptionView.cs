﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IQuickPayOptionView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<QuickPayOption>> Save;
		event EventHandler<ViewEventArgs<QuickPayOption>> Delete;
		event EventHandler<ViewEventArgs<QuickPayOption>> Lock;
		event EventHandler<ViewEventArgs<QuickPayOption>> UnLock;
		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        event EventHandler<ViewEventArgs<List<QuickPayOption>>> BatchImport;

		void DisplaySearchResult(List<QuickPayOption> quickPayOption);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}