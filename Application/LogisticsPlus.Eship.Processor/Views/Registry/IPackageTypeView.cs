﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IPackageTypeView
	{
		User ActiveUser { get; }

	    Dictionary<int, string> Project44Codes { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<PackageType>> Save;
		event EventHandler<ViewEventArgs<PackageType>> Delete;
		event EventHandler<ViewEventArgs<PackageType>> Lock;
		event EventHandler<ViewEventArgs<PackageType>> UnLock;
		event EventHandler<ViewEventArgs<string>> Search;
        event EventHandler<ViewEventArgs<List<PackageType>>> BatchImport;

        void DisplaySearchResult(List<PackageType> packageType);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}
