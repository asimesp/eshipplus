﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
    public class PostalCodeViewSearchCriteria
    {
        public List<ParameterColumn> Parameters { get; set; }

        public SearchField SortBy { get; set; }
        public bool SortAscending { get; set; }
       
        public PostalCodeViewSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }
    }
}
