﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IPostalCodeView
	{
        AdminUser ActiveSuperUser { get; }

		bool ForcePrimary { get; }

		List<Country> Countries { set; }

		event EventHandler Loading;
		event EventHandler<ViewEventArgs<PostalCode>> Save;
		event EventHandler<ViewEventArgs<PostalCode>> Delete;
		event EventHandler<ViewEventArgs<List<PostalCode>>> BatchImport;
		event EventHandler<ViewEventArgs<PostalCodeViewSearchCriteria>> Search;
        event EventHandler ExportAllPostalCodes;

		void DisplaySearchResult(List<PostalCodeViewSearchDto> postalCodes);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	    void ExportPostalCodes(List<PostalCodeViewSearchDto> postalCodes);
	}
}
