﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IServiceView
	{
		User ActiveUser { get; }

		Dictionary<int, string> Categories { set; }
		Dictionary<int, string> DispatchFlag { set; }
		Dictionary<int, string> Project44Codes { set; }


		event EventHandler Loading;
		event EventHandler<ViewEventArgs<Service>> Save;
		event EventHandler<ViewEventArgs<Service>> Delete;
		event EventHandler<ViewEventArgs<Service>> Lock;
		event EventHandler<ViewEventArgs<Service>> UnLock;
		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        event EventHandler<ViewEventArgs<List<Service>>> BatchImport;

		void DisplaySearchResult(List<ServiceViewSearchDto> service);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FaildedLock(Lock @lock);
	}
}
