﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
    public interface ISettingView
    {
        User ActiveUser { get; }

        Dictionary<int, string> Codes { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<Setting>> Save;
        event EventHandler<ViewEventArgs<Setting>> Lock;
        event EventHandler<ViewEventArgs<Setting>> UnLock;
        event EventHandler<ViewEventArgs<string>> Search;

        void DisplaySearchResult(List<Setting> setting);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);

        void FailedLock(Lock @lock);
    }
}
