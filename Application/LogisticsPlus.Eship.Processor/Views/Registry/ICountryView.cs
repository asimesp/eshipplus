﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface ICountryView
	{
        AdminUser ActiveSuperUser { get; }

	    Dictionary<int, string> Project44CountryCodes { set; }


        event EventHandler Loading;
        event EventHandler<ViewEventArgs<Country>> Save;
		event EventHandler<ViewEventArgs<Country>> Delete;
		event EventHandler<ViewEventArgs<string>> Search;
        event EventHandler<ViewEventArgs<List<Country>>> BatchImport;

        void LogException(Exception ex);
		void DisplaySearchResult(List<Country> country);
		void DisplayMessages(IEnumerable<ValidationMessage> messages); 
	}
}