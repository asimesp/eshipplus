﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IP44ChargeCodeView
    {
		User ActiveUser { get; }

        Dictionary<int, string> Project44Codes { set; }

        event EventHandler Loading;
		event EventHandler<ViewEventArgs<P44ChargeCodeMapping>> Save;
		event EventHandler<ViewEventArgs<P44ChargeCodeMapping>> Delete;
		event EventHandler<ViewEventArgs<P44ChargeCodeMapping>> Lock;
		event EventHandler<ViewEventArgs<P44ChargeCodeMapping>> UnLock;
		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        event EventHandler<ViewEventArgs<List<P44ChargeCodeMapping>>> BatchImport;

		void DisplaySearchResult(List<P44ChargeCodeMapping> p44ChargeCode);
	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FaildedLock(Lock @lock);
	}
}
