﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IChargeCodeView
	{
		User ActiveUser { get; }

		Dictionary<int,string> Categories { set; }

	    Dictionary<int, string> Project44Codes { set; }

        event EventHandler Loading;
		event EventHandler<ViewEventArgs<ChargeCode>> Save;
		event EventHandler<ViewEventArgs<ChargeCode>> Delete;
		event EventHandler<ViewEventArgs<ChargeCode>> Lock;
		event EventHandler<ViewEventArgs<ChargeCode>> UnLock;
		event EventHandler<ViewEventArgs<string>> Search;
        event EventHandler<ViewEventArgs<List<ChargeCode>>> BatchImport;

		void DisplaySearchResult(List<ChargeCode> chargeCode);
        void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}