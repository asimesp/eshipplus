﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
    public interface IYrMthBusDayView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<YrMthBusDay>> Save;
        event EventHandler<ViewEventArgs<YrMthBusDay>> Delete;
        event EventHandler<ViewEventArgs<YrMthBusDay>> Lock;
        event EventHandler<ViewEventArgs<YrMthBusDay>> UnLock;
        event EventHandler<ViewEventArgs<string>> Search;

        void DisplaySearchResult(List<YrMthBusDay> mthBusDays);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void LogException(Exception ex);

        void FailedLock(Lock @lock); 
    }
}