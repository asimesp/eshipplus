﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IServiceFinderView
	{
		User ActiveUser { get; }
		int ServiceCategoryFilter { get; set; }

		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;

		void DisplaySearchResult(List<ServiceViewSearchDto> service);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
	}
}
