﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
	public interface IDocumentTagView
	{
		User ActiveUser { get; }

		event EventHandler<ViewEventArgs<DocumentTag>> Save;
		event EventHandler<ViewEventArgs<DocumentTag>> Delete;
		event EventHandler<ViewEventArgs<DocumentTag>> Lock;
		event EventHandler<ViewEventArgs<DocumentTag>> UnLock;
		event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        event EventHandler<ViewEventArgs<List<DocumentTag>>> BatchImport;

		void DisplaySearchResult(List<DocumentTag> documentTag);
	    void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);

		void FailedLock(Lock @lock);
	}
}