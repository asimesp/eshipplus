﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Registry
{
    public interface IFailureCodeView
    {
        User ActiveUser { get; }

        Dictionary<int, string> FailureCodeCategories { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<FailureCode>> Save;
        event EventHandler<ViewEventArgs<FailureCode>> Delete;
        event EventHandler<ViewEventArgs<FailureCode>> Lock;
        event EventHandler<ViewEventArgs<FailureCode>> UnLock;
        event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        event EventHandler<ViewEventArgs<List<FailureCode>>> BatchImport;

        void DisplaySearchResult(List<FailureCode> failureCodes);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void FaildedLock(Lock @lock);
    }
}
