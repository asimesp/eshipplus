﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Cms
{
	public interface INewsView
	{
		AdminUser ActiveSuperUser { get; }

		event EventHandler<ViewEventArgs<News>> Save;
		event EventHandler<ViewEventArgs<News>> Delete;

		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
		void Set(News news);
	}
}
