﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Cms
{
	public interface IContentView
	{
		AdminUser ActiveSuperUser { get; }

		event EventHandler<ViewEventArgs<Content>> Save;
		event EventHandler<ViewEventArgs<Content>> Delete;

		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void LogException(Exception ex);
		void Set(Content content);
	}
}
