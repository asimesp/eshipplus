﻿using System;

namespace LogisticsPlus.Eship.Processor.Views
{
	public class ViewEventArgs<T> : EventArgs
	{
		public T Argument { get; set; }

		public ViewEventArgs(T argument)
		{
			Argument = argument;
		}
	}

	public class ViewEventArgs<T, TK> : EventArgs
	{
		public T Argument { get; set; }
		public TK Argument2 { get; set; }

		public ViewEventArgs(T argument, TK argument2)
		{
			Argument = argument;
			Argument2 = argument2;
		}
	}
}
