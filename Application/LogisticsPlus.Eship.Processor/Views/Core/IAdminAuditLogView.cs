﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
	public interface IAdminAuditLogView
	{
		AdminUser ActiveSuperUser { get; }

        event EventHandler<ViewEventArgs<AdminAuditLogSearchCriteria>> Search;

        void DisplaySearchResult(List<AuditLogsViewSearchDto> adminAuditLogs);
	}
}
