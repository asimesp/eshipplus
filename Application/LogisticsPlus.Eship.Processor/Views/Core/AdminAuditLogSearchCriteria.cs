﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
    public class AdminAuditLogSearchCriteria
    {
       public List<ParameterColumn> Parameters { get; set; }

        public AdminAuditLogSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }
    }
}
