﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
    public interface IDeveloperAccessRequestView
    {
        User ActiveUser { get; }

        List<string> SupportAreas { set; }

        event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Save;
        event EventHandler Loading;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
