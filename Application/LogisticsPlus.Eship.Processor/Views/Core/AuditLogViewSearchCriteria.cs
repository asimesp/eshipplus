﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
    public class AuditLogViewSearchCriteria
    {
      public List<ParameterColumn> Parameters { get; set; }

      public AuditLogViewSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }
    }
}
