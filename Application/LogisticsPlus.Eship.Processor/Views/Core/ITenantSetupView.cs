﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
	public interface ITenantSetupView
	{
        AdminUser ActiveSuperUser { get; }

		Permission GroupPermission { get; }
		Permission UserPermission { get; }
		Permission TenantAdminPermission { get; }

		event EventHandler<ViewEventArgs<Tenant>> Save;
		event EventHandler<ViewEventArgs<Tenant>> Delete;

	    void LogException(Exception ex);
		void DisplayMessages(IEnumerable<ValidationMessage> messages);
		void SetId(long id);
	}
}
