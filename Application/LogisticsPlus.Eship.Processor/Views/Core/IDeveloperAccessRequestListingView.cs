﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
    public interface IDeveloperAccessRequestListingView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Save;
        event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Delete;
        event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Lock;
        event EventHandler<ViewEventArgs<DeveloperAccessRequest>> UnLock;
        event EventHandler<ViewEventArgs<DeveloperAccessRequest>> LoadAuditLog;

        void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void FailedLock(Lock @lock);
        void Set(DeveloperAccessRequest request);
    }
}
