﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
    public interface IAnnouncementView
    {
        User ActiveUser { get; }

        Dictionary<int, string> AnnouncementTypes { set; }

        event EventHandler Loading;
        event EventHandler<ViewEventArgs<Announcement>> Save;
        event EventHandler<ViewEventArgs<Announcement>> Delete;
        event EventHandler<ViewEventArgs<Announcement>> Lock;
        event EventHandler<ViewEventArgs<Announcement>> UnLock;
        event EventHandler<ViewEventArgs<AnnouncementViewSearchCriteria>> Search;

    	void DisplaySearchResult(List<Announcement> announcements);
        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void FaildedLock(Lock @lock);
    }
}
