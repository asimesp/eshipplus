﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
    public interface IVendorProMassUpdateView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<List<string[]>>> Save;
        event EventHandler<ViewEventArgs<List<string[]>>> OverrideSave;

        void LogException(Exception ex);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
        void DisplayUpdatedRecords(List<string[]> records);
	    void LogShipmentForDocImgRtrv(Shipment shipment);
    }
}
