﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Core;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
    public interface IAuditLogView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<AuditLogViewSearchCriteria>> Search;

		void DisplaySearchResult(List<AuditLogsViewSearchDto> auditLogs);
    }
}