﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
    public class AnnouncementViewSearchCriteria
    {
        public List<ParameterColumn> Parameters { get; set; }

        public AnnouncementViewSearchCriteria()
        {
            Parameters = new List<ParameterColumn>();
        }
    }
}
