﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Processor.Views.Core
{
    public interface IDeveloperAccessRequestFinderView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;

        void DisplaySearchResult(List<DeveloperAccessRequest> customers);
        void DisplayMessages(IEnumerable<ValidationMessage> messages);
    }
}
