﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;


namespace LogisticsPlus.Eship.Processor.Views.MassUpload
{
    public interface IShipmentMassUploadView
    {
        User ActiveUser { get; }

        event EventHandler<ViewEventArgs<ShipmentDocument>> Save;


        void LogException(Exception ex);
		void ProcessFailedShimpentDocumentSave(ShipmentDocument document, IEnumerable<ValidationMessage> messages);
    }
}

