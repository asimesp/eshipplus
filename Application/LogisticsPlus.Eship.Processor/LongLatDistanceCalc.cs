﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.PluginBase.Mileage;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor
{
	public class LongLatDistanceCalc : EntityBase, IMileagePlugin
	{
		public bool IsInitialized { get; private set; }

		public string LastErrorMessage { get; set; }

		

		public bool Initialize()
		{
			IsInitialized = true;
			return IsInitialized;
		}

		public bool Initialize<T>(T parameter)
		{
			throw new NotSupportedException("Parameterized initialization is not supported by this implementation.");
		}


		public double GetMiles(Point start, Point end)
		{
			if (start == null) throw new ArgumentNullException("start");
			if (end == null) throw new ArgumentNullException("end");
			if (start.Type != end.Type) throw new NotSupportedException("Point types do not match. Operation is not supported");

			switch (start.Type)
			{
				case PointType.LongitudeLatitude:
					return CalculateDistanceLongLat(start, end, false);
				case PointType.Address:
					return CalculateDistanceCustom(start, end, false);
				default:
					throw new NotSupportedException("Unknown type!");
			}
		}

		public double GetKilometers(Point start, Point end)
		{
			if (start == null) throw new ArgumentNullException("start");
			if (end == null) throw new ArgumentNullException("end");
			if (start.Type != end.Type) throw new NotSupportedException("Point types do not match. Operation is not supported");

			switch (start.Type)
			{
				case PointType.LongitudeLatitude:
					return CalculateDistanceLongLat(start, end, true);
				case PointType.Address:
					return CalculateDistanceCustom(start, end, true);
				default:
					throw new NotSupportedException("Unknown type!");
			}
		}


		public double GetMiles<T>(Point start, Point end, T setup)
		{
			throw new NotSupportedException("Parameterized mileage method is not supported by this implementation.");
		}

		public double GetKilometers<T>(Point start, Point end, T setup)
		{
			throw new NotSupportedException("Parameterized kilometer method is not supported by this implementation.");
		}


		private double CalculateDistanceLongLat(Point p1, Point p2, bool inKilometers)
		{
			try
			{

				var radius = inKilometers ? 6370.97327862273 : 3958.73926185; // radius of earth
				LastErrorMessage = string.Empty;
				return 2.0 * radius
					   *
					   Math.Asin(Math.Sqrt((Math.Pow(Math.Sin(((p2.Latitude - p1.Latitude) / 2.0).DegreeToRadian()), 2)) +
										   (Math.Cos(p1.Latitude.DegreeToRadian()) * Math.Cos(p2.Latitude.DegreeToRadian()) *
											Math.Pow(Math.Sin(((p2.Longitude - p1.Longitude) / 2.0).DegreeToRadian()), 2))));
			}
			catch (Exception e)
			{
				LastErrorMessage = e.Message;
				return PluginBase.PluginBaseConstants.ErrorMilesValue;
			}
		}

		private double CalculateDistanceCustom(Point p1, Point p2, bool inKilometers)
		{
			try
			{
				const string query =
					@"select ISNULL(Distance, -1) 'Distance' from 
					CalculatePostalCodesDistance(@PostalCode1, @CountryId1, @PostalCode2, @CountryId2, @UseKilometers)";
				var parameters = new Dictionary<string, object>
				                 	{
				                 		{"PostalCode1", p1.PostalCode},
				                 		{"CountryId1", p1.Country},
				                 		{"PostalCode2", p2.PostalCode},
				                 		{"CountryId2", p2.Country},
				                 		{"UseKilometers", inKilometers ? 1 : 0}
				                 	};
				var distance = PluginBase.PluginBaseConstants.ErrorMilesValue;
				using (var reader = GetReader(query, parameters))
					if (reader.Read())
						distance = reader.Value("Distance").ToDouble();
				Connection.Close();
				LastErrorMessage = distance < 0 ? "Possible invalid parameters" : string.Empty;
				return distance;
			}
			catch (Exception e)
			{
				LastErrorMessage = e.Message;
				return PluginBase.PluginBaseConstants.ErrorMilesValue;
			}
		}
	}
}
