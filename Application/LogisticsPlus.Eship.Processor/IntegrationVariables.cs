﻿using LogisticsPlus.Eship.MacroPoint;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.UpsPlugin;
using LogisticssPlus.FedExPlugin.Services;

namespace LogisticsPlus.Eship.Processor
{
	public class IntegrationVariables
	{
		public SmcServiceSettings RatewareParameters { get; set; }
		public SmcServiceSettings CarrierConnectParameters { get; set; }
		public ServiceParams FedExParameters { get; set; }
		public UpsServiceParams UpsParameters { get; set; }
		public MacroPointSettings MacroPointParams { get; set; }
        public Project44ServiceSettings Project44Settings { get; set; }
	}
}
