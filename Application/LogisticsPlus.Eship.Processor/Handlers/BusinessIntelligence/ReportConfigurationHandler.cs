﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Reports;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
	public class ReportConfigurationHandler : EntityBase
	{
		private readonly IReportConfigurationView _view;
		private readonly ReportConfigurationValidator _validator = new ReportConfigurationValidator();
		private readonly ReportCustomizationValidator _rcuValidator = new ReportCustomizationValidator();

		public ReportConfigurationHandler(IReportConfigurationView view)
		{
			_view = view;
		}


		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.LoadAuditLog += OnLoadAuditLog;
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.ReportChartTypes = ProcessorUtilities.GetAll<ReportChartType>();
			_view.Operators = ProcessorUtilities.GetAll<Operator>();
			_view.ReportConfigurationVisibilities = ProcessorUtilities.GetAll<ReportConfigurationVisibility>();
			_view.SqlDbTypes = ReportUtilities.ReportColumnDataTypes();
			_view.SortDirections = ProcessorUtilities.GetAll<SortDirection>();
		    _view.ChartDataSourceTypes = ProcessorUtilities.GetAll<ChartDataSourceType>();
		}

		private void OnSave(Object sender, ViewEventArgs<ReportConfiguration> e)
		{
			var reportConfiguration = e.Argument;

			if (!reportConfiguration.IsNew)
			{
				var @lock = reportConfiguration.RetrieveLock(_view.ActiveUser, reportConfiguration.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			var dbReportConfiguration = new ReportConfiguration(reportConfiguration.Id, false);
			dbReportConfiguration.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				reportConfiguration.Connection = Connection;
				reportConfiguration.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.IsValid(reportConfiguration))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (reportConfiguration.IsNew)
				{
					reportConfiguration.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, reportConfiguration.Name),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = reportConfiguration.EntityName(),
							EntityId = reportConfiguration.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if(reportConfiguration.HasChanges())
				{
					foreach (var change in reportConfiguration.Changes())
					{
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change.StripTags(),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = reportConfiguration.EntityName(),
							EntityId = reportConfiguration.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					}

					reportConfiguration.Save();
				}

				ProcessSelectUsers(reportConfiguration, dbReportConfiguration.SelectUsers);
			    ProcessSelectGroups(reportConfiguration, dbReportConfiguration.SelectGroups);
                ProcessQlikSheet(reportConfiguration, dbReportConfiguration.QlikSheets);

				CommitTransaction();
				_view.Set(reportConfiguration);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		

		private void OnDelete(object sender, ViewEventArgs<ReportConfiguration> e)
		{
			var reportConfiguration = e.Argument;

			if (reportConfiguration.IsNew)
			{
				_view.Set(new ReportConfiguration());
				return;
			}

			reportConfiguration.LoadCollections();

			var @lock = reportConfiguration.ObtainLock(_view.ActiveUser, reportConfiguration.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				reportConfiguration.Connection = Connection;
				reportConfiguration.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteReportConfiguration(reportConfiguration))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1}", AuditLogConstants.Delete, reportConfiguration.Name),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = reportConfiguration.EntityName(),
					EntityId = reportConfiguration.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				foreach (var user in reportConfiguration.SelectUsers)
				{
					user.Connection = Connection;
					user.Transaction = Transaction;

					user.Delete();
				}

			    foreach (var group in reportConfiguration.SelectGroups)
			    {
			        group.Connection = Connection;
			        group.Transaction = Transaction;

			        group.Delete();
			    }

                foreach (var sheet in reportConfiguration.QlikSheets)
				{
					sheet.Connection = Connection;
					sheet.Transaction = Transaction;

					sheet.Delete();
				}

				reportConfiguration.Delete();

				@lock.Delete();

				CommitTransaction();

				_view.Set(new ReportConfiguration());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnUnLock(object sender, ViewEventArgs<ReportConfiguration> e)
		{
			var reportConfiguration = e.Argument;

			var @lock = reportConfiguration.RetrieveLock(_view.ActiveUser, reportConfiguration.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<ReportConfiguration> e)
		{
			var reportConfiguration = e.Argument;
			var @lock = reportConfiguration.ObtainLock(_view.ActiveUser, reportConfiguration.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<ReportConfiguration> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
		}



		private void ProcessSelectUsers(ReportConfiguration reportConfiguration, IEnumerable<ReportCustomizationUser> dbSelectUsers)
		{
			var dbDelete = dbSelectUsers.Where(u => !reportConfiguration.SelectUsers.Select(gu => gu.UserId).Contains(u.UserId));

			foreach (var selectUser in dbDelete)
			{
				selectUser.Connection = Connection;
				selectUser.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Report Configuration:{2} User Id:{3}", AuditLogConstants.Delete, selectUser.EntityName(), reportConfiguration.Name, selectUser.UserId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = reportConfiguration.EntityName(),
						EntityId = reportConfiguration.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				selectUser.Delete();
			}

			foreach (var selectUser in reportConfiguration.SelectUsers)
				if (!_rcuValidator.SelectUserExists(selectUser))
				{
					selectUser.Connection = Connection;
					selectUser.Transaction = Transaction;

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Report Configuration:{2} User Id:{3}", AuditLogConstants.AddedNew, selectUser.EntityName(), reportConfiguration.Name, selectUser.UserId),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = reportConfiguration.EntityName(),
							EntityId = reportConfiguration.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					selectUser.Save();
				}
		}

        private void ProcessSelectGroups(ReportConfiguration reportConfiguration, IEnumerable<ReportCustomizationGroup> dbSelectGroups)
        {
            var dbDelete = dbSelectGroups.Where(g => !reportConfiguration.SelectGroups.Select(gu => gu.GroupId).Contains(g.GroupId));

            foreach (var selectGroup in dbDelete)
            {
                selectGroup.Connection = Connection;
                selectGroup.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Report Configuration:{2} Group Id:{3}", AuditLogConstants.Delete, selectGroup.EntityName(), reportConfiguration.Name, selectGroup.GroupId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = reportConfiguration.EntityName(),
                    EntityId = reportConfiguration.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                selectGroup.Delete();
            }

            foreach (var selectGroup in reportConfiguration.SelectGroups)
                if (!_rcuValidator.SelectGroupExists(selectGroup))
                {
                    selectGroup.Connection = Connection;
                    selectGroup.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Report Configuration:{2} Group Id:{3}", AuditLogConstants.AddedNew, selectGroup.EntityName(), reportConfiguration.Name, selectGroup.GroupId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = reportConfiguration.EntityName(),
                        EntityId = reportConfiguration.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    selectGroup.Save();
                }
        }

        private void ProcessQlikSheet(ReportConfiguration reportConfiguration, IEnumerable<QlikSheet> dbQlikSheets)
		{
			var dbDelete = dbQlikSheets.Where(s => !reportConfiguration.QlikSheets.Select(dbs => dbs.Id).Contains(s.Id));

			foreach (var sheet in dbDelete)
			{
				sheet.Connection = Connection;
				sheet.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Report Configuration:{2} Sheet Name:{3} Sheet Ref: {4}", AuditLogConstants.Delete, sheet.EntityName(), reportConfiguration.Name, sheet.Name, sheet.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = reportConfiguration.EntityName(),
					EntityId = reportConfiguration.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				sheet.Delete();
			}

			foreach (var sheet in reportConfiguration.QlikSheets)
			{
				sheet.Connection = Connection;
				sheet.Transaction = Transaction;

				if (sheet.IsNew)
				{
					sheet.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Sheet Name:{2} ", AuditLogConstants.AddedNew, sheet.EntityName(), sheet.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = reportConfiguration.EntityName(),
						EntityId = reportConfiguration.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (sheet.HasChanges())
				{
					foreach (var change in sheet.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Sheet Name:{1} {2}", sheet.EntityName(), sheet.Name, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = reportConfiguration.EntityName(),
							EntityId = reportConfiguration.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					sheet.Save();
				}
			}
		}
	}
}
