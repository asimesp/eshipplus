﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
    public class BackgroundReportRunRecordManager : EntityBase
    {
        public List<string> DeleteBackgroundReports(List<BackgroundReportRunRecord> records, out List<Exception> ex)
        {
            var filesToDelete = new List<string>();
            ex = new List<Exception>();

            foreach (var expiredRecord in records)
            {
                var fileName = expiredRecord.CompletedReportFileName;
                var user = expiredRecord.User;

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();

                try
                {
                    expiredRecord.Connection = Connection;
                    expiredRecord.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Background Report: {1}", AuditLogConstants.Delete, expiredRecord.ReportName),
                        TenantId = expiredRecord.TenantId,
                        User = user,
                        EntityCode = expiredRecord.EntityName(),
                        EntityId = expiredRecord.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    expiredRecord.Delete();
                    CommitTransaction();

                    filesToDelete.Add(fileName);
                }
                catch (Exception e)
                {
                    RollBackTransaction();
                    ex.Add(e);
                }
            }

            return filesToDelete;
        }

        public void DeleteBackgroundReport(BackgroundReportRunRecord record, User user, out Exception ex)
        {
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                record.Connection = Connection;
                record.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Background Report: {1}", AuditLogConstants.Delete, record.ReportName),
                    TenantId = record.TenantId,
                    User = user,
                    EntityCode = record.EntityName(),
                    EntityId = record.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                record.Delete();
                CommitTransaction();
                ex = null;
            }
            catch (Exception e)
            {
                RollBackTransaction();
                ex = e;
            }
        }

        public void SaveBackgroundReport(BackgroundReportRunRecord record, out Exception ex)
        {
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                record.Connection = Connection;
                record.Transaction = Transaction;

                if (record.IsNew)
                {
                    record.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Background Report: {1}", AuditLogConstants.AddedNew, record.ReportName),
                        TenantId = record.TenantId,
                        User = record.User,
                        EntityCode = record.EntityName(),
                        EntityId = record.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (record.HasChanges())
                {
                    foreach (var change in record.Changes())
                    {
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = record.TenantId,
                            User = record.User,
                            EntityCode = record.EntityName(),
                            EntityId = record.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    }

                    record.Save();
                }

                CommitTransaction();

                ex = null;
            }
            catch (Exception e)
            {
                RollBackTransaction();
                ex = e;
            }

        }
    }
}
