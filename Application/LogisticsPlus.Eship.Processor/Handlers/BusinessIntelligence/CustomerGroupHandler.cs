﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
    public class CustomerGroupHandler : EntityBase
    {
        private readonly ICustomerGroupView _view;
        private readonly CustomerGroupValidator _validator = new CustomerGroupValidator();

        public CustomerGroupHandler(ICustomerGroupView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.LoadAuditLog += OnLoadAuditLog;
        }

        private void OnLoadAuditLog(object sender, ViewEventArgs<CustomerGroup> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
        }

    	private void OnSave(object sender, ViewEventArgs<CustomerGroup> e)
        {
            var customerGroup = e.Argument;

    		if (!customerGroup.IsNew)
            {
                var @lock = customerGroup.RetrieveLock(_view.ActiveUser, customerGroup.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[]
                                              {
                                                  ValidationMessage.Error(
                                                      ProcessorVars.UnableToObtainLockErrMsg)
                                              });
                    return;
                }
            }

            // pull db record
            var dbCustomerGroup = new CustomerGroup(customerGroup.Id);
            dbCustomerGroup.LoadCollections();

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                customerGroup.Connection = Connection;
                customerGroup.Transaction = Transaction;


                if (!_validator.IsValid(customerGroup))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

            	if (customerGroup.IsNew)
            	{
            		customerGroup.Save();

            		new AuditLog
            			{
            				Connection = Connection,
            				Transaction = Transaction,
            				Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, customerGroup.Name),
            				TenantId = _view.ActiveUser.TenantId,
            				User = _view.ActiveUser,
            				EntityCode = customerGroup.EntityName(),
            				EntityId = customerGroup.Id.ToString(),
            				LogDateTime = DateTime.Now
            			}.Log();
            	}
				else if (customerGroup.HasChanges())
				{
					foreach (var change in customerGroup.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = customerGroup.EntityName(),
								EntityId = customerGroup.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					customerGroup.Save();
				}


            	ProcessCustomerGroupMap(customerGroup, dbCustomerGroup.CustomerGroupMaps);

                CommitTransaction();

            	_view.Set(customerGroup);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

    	private void OnDelete(object sender, ViewEventArgs<CustomerGroup> e)
        {
            var customerGroup = e.Argument;

            if (customerGroup.IsNew) return;
           
            customerGroup.LoadCollections();

            var @lock = customerGroup.ObtainLock(_view.ActiveUser, customerGroup.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
            	_view.DisplayMessages(new[]{ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                customerGroup.Connection = Connection;
                customerGroup.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

            	@lock.Connection = Connection;
            	@lock.Transaction = Transaction;

                _validator.Messages.Clear();
				if (!_validator.CanDeleteCustomerGroup(customerGroup))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1}", AuditLogConstants.Delete, customerGroup.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = customerGroup.EntityName(),
                        EntityId = customerGroup.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                foreach (var map in customerGroup.CustomerGroupMaps)
                {
                    map.CustomerGroup = customerGroup;
                    map.Connection = Connection;
                    map.Transaction = Transaction;
                    map.Delete();
                }

               
                customerGroup.Delete();

                @lock.Delete();
                CommitTransaction();

				_view.Set(new CustomerGroup());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

    	private void OnUnLock(object sender, ViewEventArgs<CustomerGroup> e)
        {
            var customerGroup = e.Argument;
            var @lock = customerGroup.RetrieveLock(_view.ActiveUser, customerGroup.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

    	private void OnLock(object sender, ViewEventArgs<CustomerGroup> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

    	private void ProcessCustomerGroupMap(CustomerGroup customerGroup, IEnumerable<CustomerGroupMap> dbCustomerGroupMaps)
    	{
    		var dbDelete = dbCustomerGroupMaps.Where(u => !customerGroup.CustomerGroupMaps.Select(csr => csr.CustomerId)
    		                                               	.Contains(u.CustomerId));


    		foreach (var groupMap in dbDelete)
    		{
    			groupMap.Connection = Connection;
    			groupMap.Transaction = Transaction;

    			new AuditLog
    				{
    					Connection = Connection,
    					Transaction = Transaction,
    					Description =
    						string.Format("{0} {1} Customer Group:{2} Customer Ref#:{3}", AuditLogConstants.Delete,
    						              groupMap.EntityName(), customerGroup.Name, groupMap.CustomerId),
    					TenantId = _view.ActiveUser.TenantId,
    					User = _view.ActiveUser,
    					EntityCode = customerGroup.EntityName(),
    					EntityId = customerGroup.Id.ToString(),
    					LogDateTime = DateTime.Now
    				}.Log();

    			groupMap.Delete();
    		}

    		foreach (var groupMap in customerGroup.CustomerGroupMaps)
    			if (!_validator.CustomerGroupMapExists(groupMap))
    			{
    				groupMap.Connection = Connection;
    				groupMap.Transaction = Transaction;

    				new AuditLog
    					{
    						Connection = Connection,
    						Transaction = Transaction,
    						Description =
    							string.Format("{0} {1} Customer Group:{2} Customer Ref#:{3}",
    							              AuditLogConstants.AddedNew,
    							              groupMap.EntityName(), customerGroup.Name, groupMap.CustomerId),
    						TenantId = _view.ActiveUser.TenantId,
    						User = _view.ActiveUser,
    						EntityCode = customerGroup.EntityName(),
    						EntityId = customerGroup.Id.ToString(),
    						LogDateTime = DateTime.Now
    					}.Log();

    				groupMap.Save();
    			}
    	}
    }
}
