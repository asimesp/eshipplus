﻿using LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
    public class VendorTerminalLocatorHandler:EntityBase
    {
        private readonly IVendorTerminalLocatorView _view;

		public VendorTerminalLocatorHandler(IVendorTerminalLocatorView view)
		{
			_view = view;
		}

        public void Initialize()
        {
            _view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<VendorTerminalLocatorSearchCriteria> e)
        {
            var results = new VendorTerminalLocatorDto().FetchVendorTerminals(e.Argument,_view.ActiveUser.TenantId);
            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
            else
                _view.DisplaySearchResult(results);
        }

    }
}
