﻿using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
	public class ReportScheduleDashboardHandler : EntityBase
	{
		private readonly IReportScheduleDashboardView _view;

		public ReportScheduleDashboardHandler(IReportScheduleDashboardView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}
        
        private void OnSearch(object sender, ViewEventArgs<ReportScheduleViewSearchCriteria> e)
		{
			var results = new ReportScheduleSearch().FetchReportSchedules(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
            if (results.Count == 0) _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
		}

	}
}
