﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
    public class VendorLaneRateHistoryHandler: EntityBase 
    {
        private readonly IVendorLaneRateHistoryView _view;

        public VendorLaneRateHistoryHandler(IVendorLaneRateHistoryView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<LaneHistorySearchCriteria> e)
        {
            var results  = new LaneHistoryRateDto().VendorLaneHistory(_view.ActiveUser.TenantId, e.Argument);

            if (results.Count == 0)
            {
                results = new List<LaneHistoryRateDto>();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
            }
            _view.DisplaySearchResult(results);
        }

    }
}
