﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
	public class VendorPerformanceSummaryDisplayHandler
	{
		private readonly IVendorPerformanceSummaryDisplayView _view;

		 public VendorPerformanceSummaryDisplayHandler(IVendorPerformanceSummaryDisplayView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.CustomerSearch += OnCustomerSearch;
            _view.VendorSearch += OnVendorSearch;
            _view.Loading += OnLoading;
        }

	    private void OnLoading(object sender, EventArgs e)
	    {
            _view.DateUnits = ProcessorUtilities.GetAll<DateUnit>();
	    }


	    private void OnVendorSearch(object sender, ViewEventArgs<string> e)
		{
			var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplayVendor(vendor ?? new Vendor());
			if (vendor == null) _view.DisplayMessages(new[] {ValidationMessage.Error("Vendor not found")});
		}

		private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
        {
            if (!_view.ActiveUser.TenantEmployee)
                FetchCustomerForNonEmployee(e.Argument);
            else
                FetchCustomerForEmployee(e.Argument);
        }


		private void FetchCustomerForNonEmployee(string customerNumber)
        {
            if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
            {
                _view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
                return;
            }
            var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
			var customer = shipAs == null ? null : shipAs.Customer;
        	_view.DisplayCustomer(customer);
            if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
        }

        private void FetchCustomerForEmployee(string customerNumber)
        {
            var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

        	var customer1 = customer == null || !customer.Active ? new Customer() : customer;
        	_view.DisplayCustomer(customer1);
            if (customer == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
        }
	}
}
