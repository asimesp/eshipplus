﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
    public class VendorGroupHandler : EntityBase
    {
        private readonly IVendorGroupView _view;
        private readonly VendorGroupValidator _validator = new VendorGroupValidator();

        public VendorGroupHandler(IVendorGroupView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.LoadAuditLog += OnLoadAuditLog;
        }

        private void OnLoadAuditLog(object sender, ViewEventArgs<VendorGroup> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
        }

    	private void OnSave(object sender, ViewEventArgs<VendorGroup> e)
        {
            var vendorGroup = e.Argument;

    		if (!vendorGroup.IsNew)
            {
                var @lock = vendorGroup.RetrieveLock(_view.ActiveUser, vendorGroup.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[]
                                              {
                                                  ValidationMessage.Error(
                                                    ProcessorVars.UnableToObtainLockErrMsg)
                                              });
                    return;
                }
            }

            // pull db record
            var dbVendorGroup = new VendorGroup(vendorGroup.Id);
            dbVendorGroup.LoadCollections();

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                vendorGroup.Connection = Connection;
                vendorGroup.Transaction = Transaction;


                if (!_validator.IsValid(vendorGroup))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

            	if (vendorGroup.IsNew)
            	{
            		vendorGroup.Save();

            		new AuditLog
            			{
            				Connection = Connection,
            				Transaction = Transaction,
            				Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, vendorGroup.Name),
            				TenantId = _view.ActiveUser.TenantId,
            				User = _view.ActiveUser,
            				EntityCode = vendorGroup.EntityName(),
            				EntityId = vendorGroup.Id.ToString(),
            				LogDateTime = DateTime.Now
            			}.Log();
            	}
				else if (vendorGroup.HasChanges())
				{
					foreach (var change in vendorGroup.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = vendorGroup.EntityName(),
								EntityId = vendorGroup.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					vendorGroup.Save();
				}

            	ProcessVendorGroupMap(vendorGroup, dbVendorGroup.VendorGroupMaps);

                CommitTransaction();

            	_view.Set(vendorGroup);
                _view.DisplayMessages(new[] { ValidationMessage.Information("Record Saved.") });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error("An error occurred while saving record.") });
            }
        }

    	private void OnDelete(object sender, ViewEventArgs<VendorGroup> e)
        {
            var vendorGroup = e.Argument;
            if (vendorGroup.IsNew) return;
            vendorGroup.LoadCollections();

            var @lock = vendorGroup.ObtainLock(_view.ActiveUser, vendorGroup.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[]
                                          {
                                              ValidationMessage.Error(
                                              ProcessorVars.UnableToObtainLockErrMsg)
                                          });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                vendorGroup.Connection = Connection;
                vendorGroup.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

            	@lock.Connection = Connection;
            	@lock.Transaction = Transaction;

                _validator.Messages.Clear();
				if (!_validator.CanDeleteVendorGroup(vendorGroup))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1}", AuditLogConstants.Delete, vendorGroup.Name),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = vendorGroup.EntityName(),
                    EntityId = vendorGroup.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                foreach (var map in vendorGroup.VendorGroupMaps)
                {
                    map.VendorGroup = vendorGroup;
                    map.Connection = Connection;
                    map.Transaction = Transaction;
                    map.Delete();
                }

                vendorGroup.Delete();

                @lock.Delete();
                CommitTransaction();

            	_view.Set(new VendorGroup());
                _view.DisplayMessages(new[] { ValidationMessage.Information("Record Deleted.") });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error("An error occurred while deleting record.") });
            }
        }

    	private void OnUnLock(object sender, ViewEventArgs<VendorGroup> e)
        {
            var vendorGroup = e.Argument;
            var @lock = vendorGroup.RetrieveLock(_view.ActiveUser, vendorGroup.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

    	private void OnLock(object sender, ViewEventArgs<VendorGroup> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

    	private void ProcessVendorGroupMap(VendorGroup vendorGroup, IEnumerable<VendorGroupMap> dbVendorGroupMaps)
    	{
    		var dbDelete = dbVendorGroupMaps.Where(u => !vendorGroup.VendorGroupMaps.Select(csr => csr.VendorId)
    		                                             	.Contains(u.VendorId));


    		foreach (var groupMap in dbDelete)
    		{
    			groupMap.Connection = Connection;
    			groupMap.Transaction = Transaction;

    			new AuditLog
    				{
    					Connection = Connection,
    					Transaction = Transaction,
    					Description =
    						string.Format("{0} {1} Vendor Group:{2} Vendor Ref#:{3}", AuditLogConstants.Delete,
    						              groupMap.EntityName(), vendorGroup.Name, groupMap.VendorId),
    					TenantId = _view.ActiveUser.TenantId,
    					User = _view.ActiveUser,
    					EntityCode = vendorGroup.EntityName(),
    					EntityId = vendorGroup.Id.ToString(),
    					LogDateTime = DateTime.Now
    				}.Log();

    			groupMap.Delete();
    		}

    		foreach (var groupMap in vendorGroup.VendorGroupMaps)
    			if (!_validator.VendorGroupMapExists(groupMap))
    			{
    				groupMap.Connection = Connection;
    				groupMap.Transaction = Transaction;

    				new AuditLog
    					{
    						Connection = Connection,
    						Transaction = Transaction,
    						Description =
    							string.Format("{0} {1} Vendor Group:{2} Vendor Ref#:{3}",
    							              AuditLogConstants.AddedNew,
    							              groupMap.EntityName(), vendorGroup.Name, groupMap.VendorId),
    						TenantId = _view.ActiveUser.TenantId,
    						User = _view.ActiveUser,
    						EntityCode = vendorGroup.EntityName(),
    						EntityId = vendorGroup.Id.ToString(),
    						LogDateTime = DateTime.Now
    					}.Log();

    				groupMap.Save();
    			}
    	}
    }
}
