﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
    public class ReportTemplateHandler : EntityBase
    {
        private readonly IReportTemplateView _view;
        private readonly ReportTemplateValidator _validator = new ReportTemplateValidator();

        public ReportTemplateHandler(IReportTemplateView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Delete += OnDelete;
        }

        private void OnSave(Object sender, ViewEventArgs<ReportTemplate> e)
        {
            var reportTemplate = e.Argument;
            
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                reportTemplate.Connection = Connection;
                reportTemplate.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.IsValid(reportTemplate))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (reportTemplate.IsNew)
                {
                    reportTemplate.Save();

                    new AdminAuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, reportTemplate.Name, reportTemplate.Description),
                        EntityCode = reportTemplate.EntityName(),
                        EntityId = reportTemplate.Id.ToString(),
                        LogDateTime = DateTime.Now,
                        AdminUser = _view.ActiveSuperUser
                    }.Log();
                }
                else if (reportTemplate.HasChanges())
                {
                    foreach (var change in reportTemplate.Changes())
                        new AdminAuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change.StripTags(),
                            EntityCode = reportTemplate.EntityName(),
                            EntityId = reportTemplate.Id.ToString(),
                            LogDateTime = DateTime.Now,
                            AdminUser = _view.ActiveSuperUser
                        }.Log();
                    reportTemplate.Save();
                }

                CommitTransaction();
                _view.Set(reportTemplate);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<ReportTemplate> e)
        {
            var reportTemplate = e.Argument;

            if (reportTemplate.IsNew) return;

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                reportTemplate.Connection = Connection;
                reportTemplate.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteTemplate(reportTemplate))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

                new AdminAuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} {2}", AuditLogConstants.Delete, reportTemplate.Name, reportTemplate.Description),
                    EntityCode = reportTemplate.EntityName(),
                    EntityId = reportTemplate.Id.ToString(),
                    LogDateTime = DateTime.Now,
                    AdminUser = _view.ActiveSuperUser
                }.Log();

                reportTemplate.Delete();

                CommitTransaction();

				_view.Set(new ReportTemplate { Query = string.Empty, DefaultCustomization = string.Empty });
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }
    }
}
