﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
	public class ReportSchedulerHandler : EntityBase
	{
		private readonly IReportSchedulerView _view;
		private readonly ReportScheduleValidator _validator = new ReportScheduleValidator();

		public ReportSchedulerHandler(IReportSchedulerView view)
		{
			_view = view;
		}


		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.LoadAuditLog += OnLoadAuditLog;
		}


		private void OnLoading(object sender, EventArgs e)
		{
			_view.ScheduleIntervals = ProcessorUtilities.GetAll<ScheduleInterval>();
			_view.ExportFileExtension = ProcessorUtilities.GetExportFileExtension();
			_view.Time = TimeUtility.BuildHourlyTimeList();
		}

		private void OnSave(Object sender, ViewEventArgs<ReportSchedule> e)
		{
			var reportSchedule = e.Argument;

			if (!reportSchedule.IsNew)
			{
				var @lock = reportSchedule.RetrieveLock(_view.ActiveUser, reportSchedule.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				reportSchedule.Connection = Connection;
				reportSchedule.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.IsValid(reportSchedule))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (reportSchedule.IsNew)
				{
					reportSchedule.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
                        Description = string.Format("{0} {1} Report Configuration: {2}", AuditLogConstants.AddedNew, reportSchedule.Id, reportSchedule.ReportConfiguration.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = reportSchedule.EntityName(),
						EntityId = reportSchedule.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (reportSchedule.HasChanges())
				{
					foreach (var change in reportSchedule.Changes())
					{
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = reportSchedule.EntityName(),
								EntityId = reportSchedule.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					}

					reportSchedule.Save();
				}

				CommitTransaction();
				_view.Set(reportSchedule);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDelete(object sender, ViewEventArgs<ReportSchedule> e)
		{
			var reportSchedule = e.Argument;

			if (reportSchedule.IsNew)
			{
				_view.Set(new ReportSchedule());
				return;
			}

			var @lock = reportSchedule.ObtainLock(_view.ActiveUser, reportSchedule.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				reportSchedule.Connection = Connection;
				reportSchedule.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
                    Description = string.Format("{0} {1} Report Configuration: {2}", AuditLogConstants.Delete, reportSchedule.Id, reportSchedule.ReportConfiguration.Name),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = reportSchedule.EntityName(),
					EntityId = reportSchedule.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				reportSchedule.Delete();

				@lock.Delete();

				CommitTransaction();

				_view.Set(new ReportSchedule());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnUnLock(object sender, ViewEventArgs<ReportSchedule> e)
		{
			var reportSchedule = e.Argument;

			var @lock = reportSchedule.RetrieveLock(_view.ActiveUser, reportSchedule.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<ReportSchedule> e)
		{
			var reportSchedule = e.Argument;
			var @lock = reportSchedule.ObtainLock(_view.ActiveUser, reportSchedule.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<ReportSchedule> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
		}
	}
}
