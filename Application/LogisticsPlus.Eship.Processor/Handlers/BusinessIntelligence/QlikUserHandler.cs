﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
    public class QlikUserHandler : EntityBase
    {
        private readonly IQlikUserView _view;
        private readonly QlikUserValidator _validator = new QlikUserValidator();

        public QlikUserHandler(IQlikUserView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.BatchImport += OnBatchImport;
        }

        private void OnBatchImport(object sender, ViewEventArgs<List<QlikUser>> e)
        {
            var messages = new ValidationMessageCollection();

            foreach (var qlikUser in e.Argument)
            {
                _validator.Messages.Clear();
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    qlikUser.Connection = Connection;
                    qlikUser.Transaction = Transaction;

                    if (!_validator.IsValid(qlikUser))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }

                    qlikUser.Save();

                    new AdminAuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1}", AuditLogConstants.AddedNew, qlikUser.UserId),
                        AdminUser = _view.ActiveSuperUser,
                        EntityCode = qlikUser.EntityName(),
                        EntityId = qlikUser.UserId,
                        LogDateTime = DateTime.Now
                    }.Log();

                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [User Id: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, qlikUser.UserId, ex.Message));

                }
            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }

        private void OnSearch(object sender, EventArgs e)
        {
            var results = new QlikUserSearch().FetchAllQlikUsers();
            _view.DisplaySearchResult(results);
        }

        private void OnDelete(object sender, ViewEventArgs<QlikUser> e)
        {
            var qlikUser = e.Argument;
            if (qlikUser.IsNew) return;

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                qlikUser.Connection = Connection;
                qlikUser.Transaction = Transaction;

                new AdminAuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, qlikUser.UserId, qlikUser.Name),
                    EntityCode = qlikUser.EntityName(),
                    EntityId = qlikUser.UserId,
                    LogDateTime = DateTime.Now,
                    AdminUser = _view.ActiveSuperUser
                }.Log();

                foreach (var qlikUserAttribute in qlikUser.QlikUserAttributes)
                {
                    qlikUserAttribute.Connection = Connection;
                    qlikUserAttribute.Transaction = Transaction;
                    qlikUserAttribute.Delete();
                }

                qlikUser.Delete();

                CommitTransaction();

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSave(object sender, ViewEventArgs<QlikUser> e)
        {
            var qlikUser = e.Argument;

            var dbQlikUser = new QlikUser(qlikUser.UserId);
            dbQlikUser.LoadQlikUserAttributes();

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                qlikUser.Connection = Connection;
                qlikUser.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.IsValid(qlikUser))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (qlikUser.IsNew)
                {
                    qlikUser.Save();

                    new AdminAuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, qlikUser.UserId, qlikUser.Name),
                        EntityCode = qlikUser.EntityName(),
                        EntityId = qlikUser.UserId,
                        LogDateTime = DateTime.Now,
                        AdminUser = _view.ActiveSuperUser
                    }.Log();
                }
                else if (qlikUser.HasChanges())
                {
                    foreach (var change in qlikUser.Changes())
                        new AdminAuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change.StripTags(),
                            EntityCode = qlikUser.EntityName(),
                            EntityId = qlikUser.UserId,
                            LogDateTime = DateTime.Now,
                            AdminUser = _view.ActiveSuperUser,
                        }.Log();
                    qlikUser.Save();
                }

                ProcessQlikUserAttributes(qlikUser, dbQlikUser.QlikUserAttributes);


                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }


        private void ProcessQlikUserAttributes(QlikUser qlikUser, IEnumerable<QlikUserAttribute> qlikUserAttributes)
        {
            var dbDelete = qlikUserAttributes.Where(attribute => qlikUser.QlikUserAttributes.All(qua => !(qua.Type == attribute.Type && qua.Value == attribute.Value && qua.UserId == attribute.UserId))).ToList();

            foreach (var qlikUserAttribute in dbDelete)
            {
                qlikUserAttribute.Connection = Connection;
                qlikUserAttribute.Transaction = Transaction;

                new AdminAuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Qlik User:{2} User Id:{3}", AuditLogConstants.Delete, qlikUserAttribute.EntityName(), qlikUser.Name, qlikUserAttribute.UserId),
                    AdminUser = _view.ActiveSuperUser,
                    EntityCode = qlikUser.EntityName(),
                    EntityId = qlikUser.UserId,
                    LogDateTime = DateTime.Now,
                }.Log();

                qlikUserAttribute.Delete();
            }

            foreach (var qlikUserAttribute in qlikUser.QlikUserAttributes.Where(qlikUserAttribute => !_validator.QlikUserAttributeExists(qlikUserAttribute)))
            {
                qlikUserAttribute.Connection = Connection;
                qlikUserAttribute.Transaction = Transaction;

                new AdminAuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Qlik User:{2} User Id:{3}", AuditLogConstants.AddedNew, qlikUserAttribute.EntityName(), qlikUser.Name, qlikUserAttribute.UserId),
                        AdminUser = _view.ActiveSuperUser,
                        EntityCode = qlikUser.EntityName(),
                        EntityId = qlikUser.UserId,
                        LogDateTime = DateTime.Now
                    }.Log();

                qlikUserAttribute.Save();
            }
        }
    }
}
