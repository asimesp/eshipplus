﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
    public class ReportConfigurationUpdateHandler : EntityBase
    {
        private List<ValidationMessage> GenerateRunOrExportAuditLog(long reportConfigurationId, User activeUser, out Exception exc, string exportFileType = "")
        {
            var configuration = new ReportConfiguration(reportConfigurationId);
            if (configuration.IsNew)
            {
                exc = null;
                return new List<ValidationMessage>() { ValidationMessage.Error("Cannot save audit log for non-existing Report Configuration") };
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                configuration.Connection = Connection;
                configuration.Transaction = Transaction;

                var auditLogDescription = exportFileType == string.Empty
                                              ? string.Format("Ran Report: '{0}'", configuration.Name)
                                              : string.Format("Exported Report: '{0}' Export Format: {1}",
                                                              configuration.Name, exportFileType);
                // generate run/export audit log
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = auditLogDescription,
                    TenantId = activeUser.TenantId,
                    User = activeUser,
                    EntityCode = configuration.EntityName(),
                    EntityId = configuration.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                // update last run time
                configuration.TakeSnapShot();
                configuration.LastRun = DateTime.Now;

                foreach (var change in configuration.Changes())
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = change,
                        TenantId = activeUser.TenantId,
                        User = activeUser,
                        EntityCode = configuration.EntityName(),
                        EntityId = configuration.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                configuration.Save();

                CommitTransaction();

                exc = null;
                return new List<ValidationMessage>();
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                exc = ex;
                return new List<ValidationMessage> { ValidationMessage.Error(ex.Message) };
            }
        }

        public List<ValidationMessage> GenerateRunAuditLog(long reportConfigurationId, User activeUser, out Exception ex)
        {
            return GenerateRunOrExportAuditLog(reportConfigurationId, activeUser, out ex);
        }

        public List<ValidationMessage> GenerateExportAuditLog(long reportConfigurationId, User activeUser, string exportFileType, out Exception ex)
        {
            return GenerateRunOrExportAuditLog(reportConfigurationId, activeUser, out ex, exportFileType);
        }
    }
}
