﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence
{
	public class ReportConfigurationFinderHandler : EntityBase
	{
		private readonly IReportConfigurationFinderView _view;

		public ReportConfigurationFinderHandler(IReportConfigurationFinderView view) { _view = view; }

		public void Initialize() { _view.Search += OnSearch; }

		private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			var results = new ReportConfigurationSearch()
				.FetchReportConfigurations(e.Argument, _view.ActiveUser.Id, _view.ActiveUser.TenantId, _view.BypassVisibilityFilter);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}
	}
}
