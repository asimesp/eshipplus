﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Core;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class DeveloperAccessRequestListingHandler : EntityBase
    {
        private readonly IDeveloperAccessRequestListingView _view;

        private readonly DeveloperAccessRequestValidator _validator = new DeveloperAccessRequestValidator();

        public DeveloperAccessRequestListingHandler(IDeveloperAccessRequestListingView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.UnLock += OnUnLock;
            _view.Lock += OnLock;
            _view.LoadAuditLog += OnLoadAuditLog;
        }

	    private void OnUnLock(object sender, ViewEventArgs<DeveloperAccessRequest> e)
        {
            var developerAccessRequest = e.Argument;
            var @lock = developerAccessRequest.RetrieveLock(_view.ActiveUser, developerAccessRequest.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<DeveloperAccessRequest> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }


        private void OnSave(Object sender, ViewEventArgs<DeveloperAccessRequest> e)
        {
            var developerAccessRequest = e.Argument;

            if (!developerAccessRequest.IsNew)
            {
                var @lock = developerAccessRequest.RetrieveLock(_view.ActiveUser, developerAccessRequest.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                //link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                developerAccessRequest.Connection = Connection;
                developerAccessRequest.Transaction = Transaction;

                //validate
                if (!_validator.IsValid(developerAccessRequest))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (developerAccessRequest.IsNew)
                {
                    developerAccessRequest.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Ref #:{1}", AuditLogConstants.AddedNew, developerAccessRequest.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = developerAccessRequest.EntityName(),
                        EntityId = developerAccessRequest.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }

                if (developerAccessRequest.HasChanges())
                {
                    foreach (var change in developerAccessRequest.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = developerAccessRequest.EntityName(),
                            EntityId = developerAccessRequest.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    developerAccessRequest.Save();
                }

                CommitTransaction();

                _view.Set(developerAccessRequest);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });

            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<DeveloperAccessRequest> e)
        {
            _validator.Messages.Clear();

            var request = e.Argument;

            if (request.IsNew) return;

            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                request.Connection = Connection;
                request.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, request.ContactName, request.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = request.EntityName(),
                    EntityId = request.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                request.Delete();
                @lock.Delete();

                CommitTransaction();
                _view.Set(new DeveloperAccessRequest());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
                
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

	    private void OnLoadAuditLog(object sender, ViewEventArgs<DeveloperAccessRequest> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
        }

    }
}
