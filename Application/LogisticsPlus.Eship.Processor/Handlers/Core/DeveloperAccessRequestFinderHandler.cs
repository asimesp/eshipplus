﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class DeveloperAccessRequestFinderHandler
    {
        private readonly IDeveloperAccessRequestFinderView _view;

        public DeveloperAccessRequestFinderHandler(IDeveloperAccessRequestFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {

            var results = new DeveloperAccessRequestSearch().FetchRequests(e.Argument, _view.ActiveUser.TenantId);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

            _view.DisplaySearchResult(results);
        }
    }
}
