﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Core;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.Processor.Views.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class TenantSetupHandler : EntityBase
    {
        private const string FirstTimeUserMessage = "First time tenant user"; 

        private readonly ITenantSetupView _view;
        private readonly TenantValidator _validator = new TenantValidator();

        public TenantSetupHandler(ITenantSetupView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Delete += OnDelete;
        }

        private void OnDelete(object sender, ViewEventArgs<Tenant> e)
        {
            var tenant = e.Argument;

            if (tenant.IsNew)
            {
                _view.SetId(default(long));
                return;
            }

            var criteria = new UserSearchCriteria
                {
                    Parameters = new List<ParameterColumn>()
                };
            var user = new User(new UserSearch().FetchUsers(criteria, tenant.Id)[0].Id); // there should only be one and only one user! SEE validation for details

            user.LoadCollections();
            tenant.LoadCollections();

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                tenant.Connection = Connection;
                tenant.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.CanDeleteTenant(tenant))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                user.Connection = Connection;
                user.Transaction = Transaction;
               
                foreach (var permission in user.UserPermissions)
                {
                    permission.Connection = Connection;
                    permission.Transaction = Transaction;

                    new AdminAuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} User:{2} {3} {4} Code:{5}", AuditLogConstants.Delete, permission.EntityName(), user.Username,
                                              user.FirstName,
                                              user.LastName, permission.Code),
                        EntityCode = permission.EntityName(),
                        EntityId = permission.Id.ToString(),
                        LogDateTime = DateTime.Now,
                        AdminUser = _view.ActiveSuperUser
                    }.Log();

                    permission.Delete();
                }

                new AdminAuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} {2} {3}", AuditLogConstants.Delete, user.Username, user.FirstName, user.LastName),
                    EntityCode = tenant.EntityName(),
                    EntityId = tenant.Id.ToString(),
                    LogDateTime = DateTime.Now,
                    AdminUser = _view.ActiveSuperUser
                }.Log();

                user.Delete();

                foreach (var autonumber in tenant.AutoNumbers)
                {
                    autonumber.Connection = Connection;
                    autonumber.Transaction = Transaction;
                    autonumber.Delete();
                }

                new AdminAuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} {2} ", AuditLogConstants.Delete, tenant.Name, tenant.Code),
                    EntityCode = tenant.EntityName(),
                    EntityId = tenant.Id.ToString(),
                    LogDateTime = DateTime.Now,
                    AdminUser = _view.ActiveSuperUser
                }.Log();

                tenant.Delete();

                CommitTransaction();

                _view.SetId(default(long));
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSave(object sender, ViewEventArgs<Tenant> e)
        {
            var tenant = e.Argument;

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                tenant.Connection = Connection;
                tenant.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.IsValid(tenant))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if(tenant.IsNew)
                {
                    tenant.Save();

                    new AdminAuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} {2} ", AuditLogConstants.AddedNew, tenant.Name, tenant.Code),
                        EntityCode = tenant.EntityName(),
                        EntityId = tenant.Id.ToString(),
                        LogDateTime = DateTime.Now,
                        AdminUser = _view.ActiveSuperUser
                    }.Log();

                     ProcessNewSetupUser(tenant);
                }
                else if (tenant.HasChanges())
                {
                    foreach (var change in tenant.Changes())
                        new AdminAuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            EntityCode = tenant.EntityName(),
                            EntityId = tenant.Id.ToString(),
                            LogDateTime = DateTime.Now,
                            AdminUser = _view.ActiveSuperUser
                        }.Log();
                    tenant.Save();
                }

                foreach (var autoNumber in tenant.AutoNumbers)
                {
                    autoNumber.Connection = Connection;
                    autoNumber.Transaction = Transaction;
                    autoNumber.Tenant = tenant;
                    autoNumber.Save();
                }

                CommitTransaction();

                _view.SetId(tenant.Id);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void ProcessNewSetupUser(Tenant tenant)
        {
            var user = NewSetupUser();
            
            user.Connection = Connection;
            user.Transaction = Transaction;
            user.Tenant = tenant;		
            
            user.Save();

            new AdminAuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} {2} {3} {4}", AuditLogConstants.AddedNew, FirstTimeUserMessage, user.Username, user.FirstName, user.LastName),
                    EntityCode = tenant.EntityName(),
                    EntityId = tenant.Id.ToString(),
                    LogDateTime = DateTime.Now,
                    AdminUser = _view.ActiveSuperUser
                }.Log();

            ProcessFirstTimeUserPermissions(user);
        }

        private void ProcessFirstTimeUserPermissions(User user)
        {
            foreach (var permission in user.UserPermissions)
            {
                permission.Connection = Connection;
                permission.Transaction = Transaction;
                permission.Tenant = user.Tenant;
                permission.User = user;
                
                permission.Save();

                new AdminAuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} User:{2} {3} {4} Code:{5}", AuditLogConstants.AddedNew, permission.EntityName(), user.Username,
                                          user.FirstName,
                                          user.LastName, permission.Code),
                    EntityCode = permission.EntityName(),
                    EntityId = permission.Id.ToString(),
                    LogDateTime = DateTime.Now,
                    AdminUser = _view.ActiveSuperUser
                }.Log();
            }
        }

        private User NewSetupUser()
        {
            var user = new User
                        {
                            City = "Default",
                            Country = ProcessorVars.RegistryCache.Countries.Values.ToList()[0],
                            Email = "default@default.ext",
                            Enabled = true,
                            FailedLoginAttempts = 0,
                            Fax = "000-000-0000",
                            FirstName = "Setup",
                            ForcePasswordReset = false,
                            LastName = "User",
                            Mobile = "000-000-0000",
                            Password = "default",
                            Phone = "000-000-0000",
                            PostalCode = "12345",
                            State = "Default",
                            Street1 = "Default",
                            Street2 = "Default",
                            TenantEmployee = true,
                            Username = "default",
							QlikUserId = string.Empty,
                            QlikUserDirectory = string.Empty,
                            DatLoadboardPassword = string.Empty,
                            DatLoadboardUsername = string.Empty,
							AdUserName = string.Empty,
                        };

            user.UserPermissions.AddRange(NewSetupPermissions());

            return user;
        }

        private IEnumerable<UserPermission> NewSetupPermissions()
        {
            return new[]
			       	{
			       		new UserPermission
			       			{
			       				Code = _view.GroupPermission.Code,
			       				Description = _view.GroupPermission.Description,
			       				DeleteApplicable = _view.GroupPermission.DeleteApplicable,
			       				Deny = _view.GroupPermission.Deny,
			       				Grant = _view.GroupPermission.Grant,
			       				Modify = _view.GroupPermission.Modify,
			       				ModifyApplicable = _view.GroupPermission.ModifyApplicable,
			       				Remove = _view.GroupPermission.Remove,
			       			},
			       		new UserPermission
			       			{
			       				Code = _view.UserPermission.Code,
			       				Description = _view.UserPermission.Description,
			       				DeleteApplicable = _view.UserPermission.DeleteApplicable,
			       				Deny = _view.UserPermission.Deny,
			       				Grant = _view.UserPermission.Grant,
			       				Modify = _view.UserPermission.Modify,
			       				ModifyApplicable = _view.UserPermission.ModifyApplicable,
			       				Remove = _view.UserPermission.Remove,
			       			},
			       		new UserPermission
			       			{
			       				Code = _view.TenantAdminPermission.Code,
			       				Description = _view.TenantAdminPermission.Description,
			       				DeleteApplicable = _view.TenantAdminPermission.DeleteApplicable,
			       				Deny = _view.TenantAdminPermission.Deny,
			       				Grant = _view.TenantAdminPermission.Grant,
			       				Modify = _view.TenantAdminPermission.Modify,
			       				ModifyApplicable = _view.TenantAdminPermission.ModifyApplicable,
			       				Remove = _view.TenantAdminPermission.Remove,
			       			}
			       	};
        }
    }
}
