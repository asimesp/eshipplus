﻿using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class AdminAuditLogHandler
    {
        private readonly IAdminAuditLogView _view;

        public AdminAuditLogHandler(IAdminAuditLogView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<AdminAuditLogSearchCriteria> e)
        {
            var results = new AdminAuditLogSearch().FetchAdminAuditLogs(e.Argument);
            _view.DisplaySearchResult(results);
        }
    }
}
