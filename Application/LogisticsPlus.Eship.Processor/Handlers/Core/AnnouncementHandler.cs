﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Core;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class AnnouncementHandler : EntityBase
    {
        private readonly IAnnouncementView _view;

        private readonly AnnouncementValidator _validator = new AnnouncementValidator();

        public AnnouncementHandler(IAnnouncementView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.UnLock += OnUnLock;
            _view.Lock += OnLock;
        }

    	private void OnUnLock(object sender, ViewEventArgs<Announcement> e)
        {
            var announcement = e.Argument;
            var @lock = announcement.RetrieveLock(_view.ActiveUser, announcement.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<Announcement> e )
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FaildedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnSearch(object sender, ViewEventArgs<AnnouncementViewSearchCriteria> e)
        {
            var search = new AnnouncementSearch();
            var results = search.FetchAnnouncements(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }

        private void OnDelete(object sender, ViewEventArgs<Announcement> e)
        {
            _validator.Messages.Clear();

            var announcement = e.Argument;

            if (announcement.IsNew) return;

            var @lock = announcement.ObtainLock(_view.ActiveUser, announcement.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                announcement.Connection = Connection;
                announcement.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} {2} {3}", AuditLogConstants.Delete, announcement.Name, announcement.Type, announcement.Id),
					TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = announcement.EntityName(),
                    EntityId = announcement.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                announcement.Delete();
                @lock.Delete();
                
                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

		private void OnSave(object sender, ViewEventArgs<Announcement> e)
		{
			var announcement = e.Argument;

			if (!announcement.IsNew)
			{
				var @lock = announcement.RetrieveLock(_view.ActiveUser, announcement.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				announcement.Connection = Connection;
				announcement.Transaction = Transaction;

				if (!_validator.IsValid(announcement))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (announcement.IsNew)
				{
					announcement.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} {2}{3}", AuditLogConstants.AddedNew, announcement.Name, announcement.Type,
								              announcement.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = announcement.EntityName(),
							EntityId = 0.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (announcement.HasChanges())
				{
					foreach (var change in announcement.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = announcement.EntityName(),
								EntityId = announcement.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					announcement.Save();
				}

				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

    	private void OnLoading(object sender, EventArgs e)
        {
            _view.AnnouncementTypes = ProcessorUtilities.GetAll<AnnouncementType>();
        }



    }
}
