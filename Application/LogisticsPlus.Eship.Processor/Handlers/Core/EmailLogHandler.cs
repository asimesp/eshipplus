﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class EmailLogHandler
    {
        private readonly IEmailLogView _view;

        public EmailLogHandler(IEmailLogView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var results = new EmailLogSearch().FetchEmailLogs(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }
    }
}
