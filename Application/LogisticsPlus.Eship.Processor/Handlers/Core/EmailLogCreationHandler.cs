﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Validation;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class EmailLogCreationHandler : EntityBase
    {
        public List<ValidationMessage> SaveEmailLog(EmailLog emailLog, out Exception ex)
        {
            if (emailLog.Id != default(long))
            {
                ex = null;
                return new List<ValidationMessage> { ValidationMessage.Error("Cannot update existing email log") };
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                emailLog.Connection = Connection;
                emailLog.Transaction = Transaction;

                emailLog.Save();

                foreach (var attachment in emailLog.EmailLogAttachments)
                {
                    attachment.Connection = Connection;
                    attachment.Transaction = Transaction;

                    attachment.Save();
                }

                // commit transaction
                CommitTransaction();

                ex = null;
                return new List<ValidationMessage>();
            }
            catch (Exception e)
            {
                RollBackTransaction();
                ex = e;
                return new List<ValidationMessage> { ValidationMessage.Error(ex.Message) };
            }
        }
    }
}
