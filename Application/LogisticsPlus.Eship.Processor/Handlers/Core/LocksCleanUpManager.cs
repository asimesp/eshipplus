﻿using System;
using System.Collections.Generic;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
	public class LocksCleanUpManager : EntityBase
	{
		public void PurgeExpiredTenantLocks()
		{
			const string format = "delete from EntityLock where LockDateTime < @c{0} and TenantId = @p{0} {1}";
			var query = string.Empty;
			var cnt = 0;
			var parameters = new Dictionary<string, object>();

			var date = DateTime.Now;

			foreach (var key in ProcessorVars.RecordLockLimit.Keys)
			{
				query += string.Format(format, cnt, Environment.NewLine);
				parameters.Add(string.Format("c{0}", cnt), date.AddSeconds(-1*ProcessorVars.RecordLockLimit[key]));
				parameters.Add(string.Format("p{0}", cnt), key);
				cnt++;
			}
			
			
			ExecuteNonQuery(query, parameters);
		}
	}
}
