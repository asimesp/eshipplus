﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Core;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class DeveloperAccessRequestHandler : EntityBase
    {
        private readonly IDeveloperAccessRequestView _view;
        private readonly DeveloperAccessRequestValidator _validator = new DeveloperAccessRequestValidator();

        public DeveloperAccessRequestHandler(IDeveloperAccessRequestView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Loading += OnLoading;
        }

        private void OnLoading(object sender, EventArgs e)
        {

            var status = new List<string> {"Code Support", "General Question", "Document Delivery", "Other"};
            _view.SupportAreas = status;
        }

        private void OnSave(Object sender, ViewEventArgs<DeveloperAccessRequest> e)
        {
            var developerAccessRequest = e.Argument;

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                //link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                developerAccessRequest.Connection = Connection;
                developerAccessRequest.Transaction = Transaction;

                //validate
                if (!_validator.IsValid(developerAccessRequest))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                developerAccessRequest.Save();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Ref #:{1}", AuditLogConstants.AddedNew, developerAccessRequest.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = developerAccessRequest.EntityName(),
                    EntityId = developerAccessRequest.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                CommitTransaction();

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });

            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }
    }
}

