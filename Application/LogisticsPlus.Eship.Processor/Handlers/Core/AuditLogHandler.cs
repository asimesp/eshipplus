﻿using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class AuditLogHandler : EntityBase
    {
        private readonly IAuditLogView _view;

        public AuditLogHandler(IAuditLogView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<AuditLogViewSearchCriteria> e)
        {
            var results = new AuditLogSearch().FetchAuditLogs(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }
    }
}
