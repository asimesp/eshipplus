﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class EmailProcessorHandler : EntityBase
    {
        public void UpdateEmailLog(EmailLog emailLog, User user, out Exception ex)
        {
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                emailLog.Connection = Connection;
                emailLog.Transaction = Transaction;

                if (emailLog.HasChanges())
                {
                    foreach (var change in emailLog.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            User = user,
                            EntityCode = emailLog.EntityName(),
                            EntityId = emailLog.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    emailLog.Save();
                }

                CommitTransaction();
                ex = null;
            }
            catch (Exception e)
            {
                RollBackTransaction();
                ex = e;
            }
        }
    }
}
