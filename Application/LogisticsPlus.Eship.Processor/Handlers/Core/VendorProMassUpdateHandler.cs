﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Core
{
    public class VendorProMassUpdateHandler : EntityBase
    {
        private readonly IVendorProMassUpdateView _view;

        public VendorProMassUpdateHandler(IVendorProMassUpdateView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.OverrideSave += OnOverrideSave;
        }

        private void OnSave(object sender, ViewEventArgs<List<string[]>> e)
        {
            var inputs = e.Argument;
            ProcessSave(inputs, false); //OVERRIDE NOT ENABLED
        }

        private void OnOverrideSave(object sender, ViewEventArgs<List<string[]>> e)
        {
            var inputs = e.Argument;
            ProcessSave(inputs, true); //OVERRIDE ENABLED
        }

        private void ProcessSave(IEnumerable<string[]> inputs, bool overrideCurrentValues)
        {
			const int arraySize = 7;

			// indices
        	const int shipmentNumber = 0;
        	const int scac = 1;
            const int vendorNumber = 2;
        	const int pro = 3;
        	const int puDate = 4;
        	const int delDate = 5;
        	const int message = 6;

            var records = new List<string[]>();
			try
			{
				var search = new ShipmentSearch();

				foreach (var input in inputs)
				{
					var shipmentDocRtrvLogRequiredIfApplicable = false;

					var record = new string[arraySize];
					record[shipmentNumber] = input[shipmentNumber]; // shipment number
					record[scac] = input[scac]; // scac
                    record[vendorNumber] = input[vendorNumber]; // vendor number 
					record[pro] = input[pro]; // pro
					record[puDate] = input[puDate]; //PU Date
					record[delDate] = input[delDate]; //Del. Date
					record[message] = string.Empty; //is reserved for messages

					var updated = false;

					// find shipment
					var shipment = search.FetchShipmentByShipmentNumber(input[shipmentNumber], _view.ActiveUser.TenantId);
					if (shipment == null)
					{
						record[message] = "Shipment not found!";
						records.Add(record);
						continue;
					}

					// find vendor
					var shipmentVendor = shipment.Vendors.FirstOrDefault(sv => sv.Vendor.Scac == input[scac] && !string.IsNullOrEmpty(input[scac])) ??
                                         shipment.Vendors.FirstOrDefault(sv => sv.Vendor.VendorNumber == input[vendorNumber] && !string.IsNullOrEmpty(input[vendorNumber]));
					if (shipmentVendor == null)
					{
						record[message] = "Vendor with Scac not found on shipment!";
						records.Add(record);
						continue;
					}

					// for logging
					shipment.TakeSnapShot();

					var newPickupDate = record[puDate].ToDateTime();
					//check for format
					if (!string.IsNullOrEmpty(record[puDate]) && newPickupDate <= DateUtility.SystemEarliestDateTime)
					{
						record[message] = "Pickup date is invalid. Please use 'YYYY-MM-DD' format";
						records.Add(record);
						continue;
					}
					//set new pickupdate to an actual date 
					newPickupDate = newPickupDate < DateUtility.SystemEarliestDateTime
					                	? DateUtility.SystemEarliestDateTime
					                	: record[puDate].ToDateTime();

					if (overrideCurrentValues || DateUtility.SystemEarliestDateTime == shipment.ActualPickupDate)
					{
						shipment.ActualPickupDate = newPickupDate;
						updated = true;
					}

					//validate Del Date
					var newDeliveryDate = record[delDate].ToDateTime();
					//check format
					if (!string.IsNullOrEmpty(record[delDate]) && newDeliveryDate <= DateUtility.SystemEarliestDateTime)
					{
						record[message] = "Delivery date is invalid. Please use 'YYYY-MM-DD' format";
						records.Add(record);
						continue;
					}

					//set new deliver date to an actual valid date
					newDeliveryDate = newDeliveryDate < DateUtility.SystemEarliestDateTime
					                  	? DateUtility.SystemEarliestDateTime
					                  	: record[delDate].ToDateTime();

					if (overrideCurrentValues || DateUtility.SystemEarliestDateTime == shipment.ActualDeliveryDate)
					{
						if (shipment.ActualDeliveryDate != newDeliveryDate) shipmentDocRtrvLogRequiredIfApplicable = true;
						shipment.ActualDeliveryDate = newDeliveryDate;
						updated = true;
					}

					if (shipment.ActualDeliveryDate < shipment.ActualPickupDate)
					{
						record[message] = "Shipment delivery date cannot occur before the shipment pickup date";
						records.Add(record);
						continue;
					}

					// validate Pro
					var proExists = !string.IsNullOrEmpty(shipmentVendor.ProNumber);

					if (overrideCurrentValues)
					{
						shipmentVendor.TakeSnapShot();
						if (shipmentVendor.ProNumber != input[pro]) shipmentDocRtrvLogRequiredIfApplicable = true;
						shipmentVendor.ProNumber = input[pro];
					}
					else if (proExists && shipmentVendor.ProNumber != input[pro])
					{
						record[message] = string.Format("Pro already exists. Existing value: {0}", shipmentVendor.ProNumber);
						records.Add(record);
						continue;
					}
					else if (!proExists && !string.IsNullOrEmpty(input[pro]))
					{
						shipmentVendor.TakeSnapShot();
						if (shipmentVendor.ProNumber != input[pro]) shipmentDocRtrvLogRequiredIfApplicable = true;
						shipmentVendor.ProNumber = input[pro];
						updated = true;
					}

					Connection = DatabaseConnection.DefaultConnection;
					BeginTransaction();
					try
					{
						// obtain lock/check lock
						var @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);
						if (!@lock.IsUserLock(_view.ActiveUser))
						{
							record[message] = ProcessorVars.UnableToObtainLockErrMsg;
							records.Add(record);
							RollBackTransaction();
							continue;
						}

						// chain in transaction
						@lock.Connection = Connection;
						@lock.Transaction = Transaction;

						shipmentVendor.Connection = Connection;
						shipmentVendor.Transaction = Transaction;

						shipment.Connection = Connection;
						shipment.Transaction = Transaction;

						shipment.Status = shipment.InferShipmentStatus(false);

						//update shipment record
						foreach (var change in shipment.Changes())
							new AuditLog
								{
									Connection = Connection,
									Transaction = Transaction,
									Description = string.Format("{0} Shipment # {1} {2}",
									                            shipment.EntityName(), shipment.ShipmentNumber, change),
									TenantId = _view.ActiveUser.TenantId,
									User = _view.ActiveUser,
									EntityCode = shipment.EntityName(),
									EntityId = shipment.Id.ToString(),
									LogDateTime = DateTime.Now
								}.Log();
						shipment.Save();

						foreach (var change in shipmentVendor.Changes())
							new AuditLog
								{
									Connection = Connection,
									Transaction = Transaction,
									Description = string.Format("{0} Shipment Vendor's Vendor Number:{1} {2}",
									                            shipmentVendor.EntityName(),
									                            shipmentVendor.Vendor.VendorNumber, change),
									TenantId = _view.ActiveUser.TenantId,
									User = _view.ActiveUser,
									EntityCode = shipment.EntityName(),
									EntityId = shipment.Id.ToString(),
									LogDateTime = DateTime.Now
								}.Log();
						shipmentVendor.Save();

						// release lock
						@lock.Delete();


						// commit transaction
						CommitTransaction();

						record[message] = updated ? "Successfully updated record." : "No update performed.";
						records.Add(record);
					}
					catch(Exception ex)
					{
						RollBackTransaction();
                        _view.LogException(ex);
                        record[message] = string.Format("Error updating record. Err: {0}", ex.Message);
						records.Add(record);
					}

					// log shipment for img rtrv
					if (shipmentDocRtrvLogRequiredIfApplicable) _view.LogShipmentForDocImgRtrv(shipment);
				}
			}
			catch(Exception ex)
			{
                _view.LogException(ex);
				_view.DisplayMessages(new[]
	                                      {
	                                          ValidationMessage.Error(
	                                              "An error occurred while processing records. Some records might have already been processed.")
	                                      });
			}

            _view.DisplayUpdatedRecords(records);
        }
    }
}
