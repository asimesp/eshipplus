﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class CustomerPurchaseOrderHandler : EntityBase
	{
		private readonly ICustomerPurchaseOrderView _view;

		private readonly CustomerPurchaseOrderValidator _validator = new CustomerPurchaseOrderValidator();

		public CustomerPurchaseOrderHandler(ICustomerPurchaseOrderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.UnLock += OnUnLock;
			_view.Lock += OnLock;
			_view.CustomerSearch += OnCustomerSearch;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<CustomerPurchaseOrder>> e)
		{
			var messages = new ValidationMessageCollection();

			foreach (var customerPurchaseOrder in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					customerPurchaseOrder.Connection = Connection;
					customerPurchaseOrder.Transaction = Transaction;

					if (!_validator.IsValid(customerPurchaseOrder))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					customerPurchaseOrder.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} Customer:{2}-{3}", AuditLogConstants.AddedNew, customerPurchaseOrder.PurchaseOrderNumber,
										  customerPurchaseOrder.Customer.CustomerNumber, customerPurchaseOrder.Customer.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = customerPurchaseOrder.EntityName(),
						EntityId = customerPurchaseOrder.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();
				}
				catch(Exception ex)
				{
					RollBackTransaction();
				    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [Code: {1}]. Err: {2}", ProcessorVars.RecordSaveErrMsg, customerPurchaseOrder.PurchaseOrderNumber, ex.Message));
				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnCustomerSearch(Object sender, ViewEventArgs<string> e)
		{
			if (!_view.ActiveUser.TenantEmployee)
				FetchCustomerForNonEmployee(e.Argument);
			else
				FetchCustomerForEmployee(e.Argument);
		}

		private void OnUnLock(object sender, ViewEventArgs<CustomerPurchaseOrder> e)
		{
			var customerPurchaseOrder = e.Argument;
			var @lock = customerPurchaseOrder.RetrieveLock(_view.ActiveUser, customerPurchaseOrder.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<CustomerPurchaseOrder> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<PurchaseOrderViewSearchCriteria> e)
		{
			var results = new CustomerPurchaseOrderSearch().FetchCustomerPurchaseOrders
								(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<CustomerPurchaseOrder> e)
		{
			_validator.Messages.Clear();

			var customerPurchaseOrder = e.Argument;

			if (customerPurchaseOrder.IsNew) return;

			var @lock = customerPurchaseOrder.ObtainLock(_view.ActiveUser, customerPurchaseOrder.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();

			try
			{
				customerPurchaseOrder.Connection = Connection;
				customerPurchaseOrder.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} Customer:{2}-{3}", AuditLogConstants.Delete, customerPurchaseOrder.PurchaseOrderNumber,
							              customerPurchaseOrder.Customer.CustomerNumber, customerPurchaseOrder.Customer.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = customerPurchaseOrder.EntityName(),
						EntityId = customerPurchaseOrder.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				customerPurchaseOrder.Delete();
				@lock.Delete();

				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<CustomerPurchaseOrder> e)
		{
			var customerPurchaseOrder = e.Argument;

			if (!customerPurchaseOrder.IsNew)
			{
				var @lock = customerPurchaseOrder.RetrieveLock(_view.ActiveUser, customerPurchaseOrder.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				customerPurchaseOrder.Connection = Connection;
				customerPurchaseOrder.Transaction = Transaction;

				if (!_validator.IsValid(customerPurchaseOrder))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (customerPurchaseOrder.IsNew)
				{
					customerPurchaseOrder.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} Customer:{2}-{3}", AuditLogConstants.AddedNew, customerPurchaseOrder.PurchaseOrderNumber,
										  customerPurchaseOrder.Customer.CustomerNumber, customerPurchaseOrder.Customer.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = customerPurchaseOrder.EntityName(),
						EntityId = customerPurchaseOrder.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (customerPurchaseOrder.HasChanges())
				{
					foreach (var change in customerPurchaseOrder.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = customerPurchaseOrder.EntityName(),
								EntityId = customerPurchaseOrder.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					customerPurchaseOrder.Save();
				}

				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}




		private void FetchCustomerForNonEmployee(string customerNumber)
		{
			if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
			{
				_view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
				return;
			}
			var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
			if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else _view.DisplayCustomer(shipAs.Customer);
		}

		private void FetchCustomerForEmployee(string customerNumber)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

			_view.DisplayCustomer(customer ?? new Customer());
			if (customer == null)
				_view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
		}
	}
}
