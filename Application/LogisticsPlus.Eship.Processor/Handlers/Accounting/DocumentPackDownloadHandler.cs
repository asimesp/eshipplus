﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class DocumentPackDownloadHandler
	{
		private readonly IDocumentPackDownloadView _view;

		public DocumentPackDownloadHandler(IDocumentPackDownloadView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<DocumentPackEntityDtoSearchCriteria> e)
		{
			var results = new DocumentPackEntityDto().RetrieveEntities(e.Argument, _view.ActiveUser.Id, _view.ActiveUser.TenantId);

			_view.DisplaySearchResult(results);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

		}

		private void OnLoading(object sender, EventArgs e)
		{
            _view.DocumentTags = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].DocumentTags;
			_view.EntityTypes = ProcessorUtilities.GetAll<DocumentPackEntityType>();
		}
	}
}
