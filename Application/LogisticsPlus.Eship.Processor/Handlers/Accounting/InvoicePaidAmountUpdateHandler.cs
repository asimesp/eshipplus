﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class InvoicePaidAmountUpdateHandler : EntityBase
    {
        private readonly IInvoicePaidAmountUpdatesView _view;
        private readonly InvoiceValidator _validator = new InvoiceValidator();

        public InvoicePaidAmountUpdateHandler(IInvoicePaidAmountUpdatesView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
        }

		private void OnSave(object sender, ViewEventArgs<List<Invoice>> e)
		{
			var messages = new List<ValidationMessage>();
			var updateList = new List<InvoicePaidAmountUpdateDto>();

			foreach (var invoice in e.Argument)
			{
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					// link up
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					invoice.Connection = Connection;
					invoice.Transaction = Transaction;

					//Check if Invoice exists
					if (invoice.IsNew)
					{
						updateList.Add(new InvoicePaidAmountUpdateDto(invoice)
						               	{Message = ValidationMessage.Error("Invoice does not exist!")});
						RollBackTransaction();
						continue;
					}

					var @lock = invoice.ObtainLock(_view.ActiveUser, invoice.Id);
					if (!@lock.IsUserLock(_view.ActiveUser))
					{
						updateList.Add(new InvoicePaidAmountUpdateDto(invoice)
						               	{Message = ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
						RollBackTransaction();
						continue;
					}
					// link up lock
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					// validate invoice

					_validator.Messages.Clear();
					if (!_validator.IsValid(invoice))
					{
						var msg = _validator.Messages
							.Select(m => string.Format("{0}: {1}", m.Type, m.Message))
							.ToArray()
							.CommaJoin();
						updateList.Add(new InvoicePaidAmountUpdateDto(invoice) {Message = ValidationMessage.Error(msg)});
						RollBackTransaction();
						@lock.Delete(); // release lock!
						continue;
					}

					// audit log invoice
					foreach (var change in invoice.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = invoice.EntityName(),
								EntityId = invoice.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					// save record 
					invoice.Save();

					// release locks
					@lock.Delete();

					// update message
					updateList.Add(new InvoicePaidAmountUpdateDto(invoice) { Message = ValidationMessage.Information("Invoice updated!") });

					// commit transaction
					CommitTransaction();
				}
				catch (Exception ex)
				{
					RollBackTransaction();
					_view.LogException(ex);
					updateList.Add(new InvoicePaidAmountUpdateDto
					               	{
					               		Message = ValidationMessage.Error("Error processing record! Error Notification Sent."),
					               		InvoiceNumber = invoice.InvoiceNumber,
					               		AmountPaid = invoice.PaidAmount
					               	});

				}
			}

			// handle messages
			messages.Insert(0, ValidationMessage.Information("Mass update process complete!"));
			_view.DisplayUpdatedRecords(updateList);
			_view.DisplayMessages(messages);
		}
    }
}
