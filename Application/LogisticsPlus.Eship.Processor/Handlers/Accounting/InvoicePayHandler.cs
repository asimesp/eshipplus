﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class InvoicePayHandler : EntityBase
    {
        private readonly IInvoicePayView _view;
        private readonly MiscReceiptValidator _mrValidator = new MiscReceiptValidator();
        private readonly InvoiceValidator _iValidator = new InvoiceValidator();

        public InvoicePayHandler(IInvoicePayView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.LockInvoices += OnLockInvoices;
            _view.ProcessPayment += OnProcessPayment;
        }

        private void OnProcessPayment(object sender, ViewEventArgs<MiscReceipt, PaymentInformation> e)
        {
            var miscReceipt = e.Argument;
            if (!miscReceipt.MiscReceiptApplications.Any())
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Must be saving one or more misc receipt applications.") });
                return;
            }

            if (miscReceipt.MiscReceiptApplications.Select(i => i.Invoice.CustomerLocation.Customer.CustomerNumber).Distinct().Count() > 1)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Only one customer's invoices may be paid for in one transaction. Please select one or more invoices from the same customer account.") });
                return;
            }

            var paymentInfo = e.Argument2;
            var transactionId = string.Empty;
            var gateway = _view.ActiveUser.Tenant.GetPaymentGatewayService();
            var failedLockInvoiceNumbers = new List<string>();
            var locks = new Dictionary<long, Lock>();

            foreach (var application in miscReceipt.MiscReceiptApplications)
            {
                var @lock = application.Invoice.RetrieveLock(_view.ActiveUser, application.Invoice.Id);
                if (!@lock.IsUserLock(_view.ActiveUser)) failedLockInvoiceNumbers.Add(application.Invoice.InvoiceNumber);
                else if(!locks.ContainsKey(application.Invoice.Id)) locks.Add(application.Invoice.Id, @lock);
            }

            if (failedLockInvoiceNumbers.Any())
            {
                _view.FailedLock(failedLockInvoiceNumbers);
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                // link up
                _iValidator.Connection = Connection;
                _iValidator.Transaction = Transaction;

                _mrValidator.Connection = Connection;
                _mrValidator.Transaction = Transaction;

                miscReceipt.Connection = Connection;
                miscReceipt.Transaction = Transaction;

                // pre authorize payment
                if (string.IsNullOrEmpty(paymentInfo.CustomerPaymentProfileId))
                    paymentInfo.CustomerPaymentProfileId = gateway.AddPaymentProfile(paymentInfo);

                var authResponse = gateway.AuthorizePayment(paymentInfo.CustomerPaymentGatewayKey, paymentInfo.CustomerPaymentProfileId, paymentInfo.ChargeAmount, paymentInfo.Description);
                transactionId = authResponse.TransactionId;
                miscReceipt.GatewayTransactionId = authResponse.TransactionId;

                if (!authResponse.Approved)
                {
                    RollBackTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Error(authResponse.ErrorMessage) });
                    return;
                }

                if (!_mrValidator.IsValid(miscReceipt))
                {
                    var msg = !string.IsNullOrEmpty(transactionId)
                                  ? VoidPaymentGatewayTransaction(gateway, transactionId)
                                  : null;
                    var msgs = new List<ValidationMessage>();
                    msgs.AddRange(_mrValidator.Messages);
                    if (msg != null) msgs.Add(msg);
                    RollBackTransaction();
                    _view.DisplayMessages(msgs);
                    return;
                }

                // save receipt
                miscReceipt.Save();
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Misc Receipt for Customer:{1} Amount:{2}",
                                                AuditLogConstants.AddedNew,
                                                miscReceipt.Customer.CustomerNumber,
                                                miscReceipt.AmountPaid),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = miscReceipt.EntityName(),
                    EntityId = miscReceipt.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                // save applications and update/save invoice
                foreach (var application in miscReceipt.MiscReceiptApplications)
                {
                    application.Connection = Connection;
                    application.Transaction = Transaction;
                    application.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} applied to Invoice: {2} for amount: {3}",
                                          AuditLogConstants.AddedNew, application.EntityName(),
                                          application.Invoice.InvoiceNumber, application.Amount),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = application.EntityName(),
                        EntityId = application.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    var invoice = application.Invoice;
                    invoice.Connection = Connection;
                    invoice.Transaction = Transaction;
                    invoice.TakeSnapShot();
                    invoice.PaidAmount += application.Amount;

                    _iValidator.Messages.Clear();
                    if (!_iValidator.IsValid(invoice))
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(_iValidator.Messages);
                        return;
                    }

                    if (!invoice.HasChanges()) continue;
                    foreach (var change in application.Invoice.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = application.Invoice.EntityName(),
                            EntityId = application.Invoice.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    invoice.Save();
                }

                var captureResponse = gateway.CapturePreauthorizedPayment(paymentInfo.CustomerPaymentGatewayKey,
                                                                          paymentInfo.CustomerPaymentProfileId,
                                                                          transactionId, paymentInfo.ChargeAmount);
                if (!captureResponse.Approved)
                {
                    var msg = !string.IsNullOrEmpty(transactionId)
                                  ? VoidPaymentGatewayTransaction(gateway, transactionId)
                                  : null;
                    var msgs = new List<ValidationMessage> { ValidationMessage.Error(captureResponse.ErrorMessage) };
                    if (msg != null) msgs.Add(msg);
                    RollBackTransaction();
                    _view.DisplayMessages(msgs);
                    return;
                }

                foreach (var @lock in locks.Values) @lock.Delete();

                // commit transaction
                CommitTransaction();
                _view.DisplayMessages(new []{ValidationMessage.Information("Your payment has been processed successfully.")});
            }
            catch (Exception ex)
            {
                var msgs = new List<ValidationMessage>();
                var msg = !string.IsNullOrEmpty(transactionId) ? VoidPaymentGatewayTransaction(gateway, transactionId) : null;
                if (msg != null) msgs.Add(msg);
                msgs.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));

                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(msgs);
            }
        }

        private void OnLockInvoices(object sender, ViewEventArgs<List<Invoice>> e)
        {
            var failedLocks = (from invoice in e.Argument
                               let iLock = invoice.ObtainLock(_view.ActiveUser, invoice.Id)
                               where !iLock.IsUserLock(_view.ActiveUser)
                               select invoice.InvoiceNumber).ToList();
            if (failedLocks.Any()) _view.FailedLock(failedLocks);
        }


        private static ValidationMessage VoidPaymentGatewayTransaction(PaymentGatewayService gateway, string transactionId)
        {
            ValidationMessage msg = null;
            try
            {
                gateway.Void(transactionId);
            }
            catch (Exception gException)
            {
                msg = ValidationMessage.Error(string.Format("Error processing void for payment gateway transaction: {0}", gException.Message));
            }
            return msg;
        }

    }
}
