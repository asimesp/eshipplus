﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class VendorChargeConfirmationHandler : EntityBase
    {
        private readonly IChargeView _view;
        private readonly ChargeValidator _chargeValidator = new ChargeValidator();
        private readonly VendorBillValidator _vendorBillValidator = new VendorBillValidator();
        private readonly VendorDisputeValidator _vendorDisputeValidator = new VendorDisputeValidator();

        public VendorChargeConfirmationHandler(IChargeView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
            _view.ConfirmCharges += OnConfirmCharges;
            _view.DisputeCharges += OnDisputeCharges;
        }

        private void OnSearch(object sender, ViewEventArgs<ChargeViewSearchCriteria> e)
        {
            var results = new ChargeSearch().FetchChargeGroups(e.Argument, _view.ActiveUser.TenantId, _view.ActiveUser.VendorPortalVendorId, _view.ActiveUser.CanSeeAllVendorsInVendorPortal);
            _view.DisplaySearchResult(results);
        }

        private void OnConfirmCharges(object sender, ViewEventArgs<List<ChargeViewSearchDto>> e)
        {
            _chargeValidator.Messages.Clear();
            var ungroupedCharges = e.Argument;

            if (ungroupedCharges.Count == 0)
            {
                _view.DisplayMessages(new[] {ValidationMessage.Information(ProcessorVars.NoSelectedItemsMsg)});
                _view.OnConfirmChargesComplete();
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                var chargeGroups = ungroupedCharges.GroupBy(g => new {g.Number, g.ChargeType, g.VendorId})
                                                   .Select(grp => grp.ToList())
                                                   .ToList();

                // Save Document
				//var documentsToSave = chargeGroups.Select(gr => gr.First()).ToList();
				//documentsToSave.ForEach(DocumentSave);


                foreach (var chargeGroup in chargeGroups)
                {
                    _chargeValidator.Connection = Connection;
                    _chargeValidator.Transaction = Transaction;

                    if (!_chargeValidator.ChargesAreValid(chargeGroup,true))
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(_chargeValidator.Messages);
                        _view.OnConfirmChargesComplete();
                        return;
                    }

                    // document number and date will be the same for all charges
                    var firstCharge = chargeGroup.First();
                    var documentNumber = firstCharge.DocumentNumber;
                    var documentDate = firstCharge.DocumentDate;
                    var vendorLocationId = firstCharge.VendorLocationId;
                    var user = firstCharge.ChargeType == ChargeType.ServiceTicket
                                   ? new ServiceTicketSearch().FetchServiceTicketByServiceTicketNumber(firstCharge.Number, _view.ActiveUser.TenantId).User
                                   : new ShipmentSearch().FetchShipmentByShipmentNumber(firstCharge.Number,  _view.ActiveUser.TenantId).User;

                    //Create VendorBill
                    var vendorBill = new VendorBill
                        {
                            DateCreated = DateTime.Now,
                            DocumentDate = documentDate,
                            Posted = false,
                            PostDate = DateUtility.SystemEarliestDateTime,
                            Exported = false,
                            ExportDate = DateUtility.SystemEarliestDateTime,
                            BillType = InvoiceType.Invoice,
                            ApplyToDocumentId = default(long),
                            Details = new List<VendorBillDetail>(),
                            TenantId = _view.ActiveUser.TenantId,
                            VendorLocationId = vendorLocationId,
                            User = _view.ActiveUser,
                            DocumentNumber = documentNumber,
                            Connection = Connection,
                            Transaction = Transaction,
                        };

                    // Create VendorBillDetails
                    var vendorBillDetails = chargeGroup.Select(charge => new VendorBillDetail()
                        {
                            TenantId = charge.TenantId,
                            VendorBill = vendorBill,
                            ChargeCodeId = charge.ChargeCodeId,
                            Quantity = charge.Quantity,
                            UnitBuy = charge.UnitBuy,
                            ReferenceNumber = charge.Number,
                            ReferenceType = charge.ChargeType == ChargeType.ServiceTicket
                                                ? DetailReferenceType.ServiceTicket
                                                : DetailReferenceType.Shipment,
                            AccountBucketId = user.DefaultCustomer.DefaultAccountBucketId,
                            Note = string.Empty
                        }).ToList();

                    vendorBill.Details = vendorBillDetails;
                    

                    _vendorBillValidator.Connection = Connection;
                    _vendorBillValidator.Transaction = Transaction;

                    _vendorBillValidator.Messages.Clear();
                    if (!_vendorBillValidator.IsValid(vendorBill))
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(_vendorBillValidator.Messages);
                        _view.OnConfirmChargesComplete();
                        return;
                    }

                    // audit log VendorBill
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Vendor Bill: {1}", AuditLogConstants.AddedNew, vendorBill.DocumentNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = vendorBill.EntityName(),
                        EntityId = vendorBill.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();


                    //Save VendorBill
                    vendorBill.Save();

					//var documentsToSave = chargeGroups.Select(gr => gr.First()).ToList();
					DocumentSave(firstCharge, vendorBill);

                    //VendorBillDetail Save
                    ProcessVendorBillDetails(vendorBill);

                    // Save Charges
                    var processChargesMessages = ProcessCharges(chargeGroup, vendorBill);

                    if (!processChargesMessages.Any()) continue;

                    RollBackTransaction();
                    _view.DisplayMessages(processChargesMessages);
                    _view.OnConfirmChargesComplete();
                    return;
                }

                CommitTransaction();
                chargeGroups.ForEach(s => _view.SendNotification(s, ChargeNotificationType.Confirm));
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
                _view.OnConfirmChargesComplete();
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message)});
                _view.OnConfirmChargesComplete();
            }
        }

        private void OnDisputeCharges(object sender, ViewEventArgs<List<ChargeViewSearchDto>> e)
        {
            _chargeValidator.Messages.Clear();
            var ungroupedCharges = e.Argument;

            if (ungroupedCharges.Count == 0)
            {
                _view.DisplayMessages(new [] { ValidationMessage.Information(ProcessorVars.NoSelectedItemsMsg) });
                return;
            }

            var messages = new List<ValidationMessage>();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                var chargeGroups = ungroupedCharges.GroupBy(g => new {g.Number, g.ChargeType, g.VendorId})
                                                    .Select(grp => grp.ToList())
                                                    .ToList();

                foreach (var chargeGroup in chargeGroups)
                {
                    _chargeValidator.Connection = Connection;
                    _chargeValidator.Transaction = Transaction;

                    if (!_chargeValidator.ChargesAreValid(chargeGroup))
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(_chargeValidator.Messages);
						return;
                    }

                    // NOTE: vendor Id same for all charges in a group at this point
                    var vendorDispute = new VendorDispute
                        {
                            TenantId = _view.ActiveUser.TenantId,
                            VendorId = chargeGroup.First().VendorId,
                            CreatedByUserId = _view.ActiveUser.Id,
                            DateCreated = DateTime.Now,
                            IsResolved = false,
                            ResolutionDate = DateUtility.SystemEarliestDateTime,
                            ResolutionComments = string.Empty,
                            ResolvedByUserId = 0,
							DisputeComments = chargeGroup.First().DisputeComments
							
                        };

                    // assign new numbers
                    if (vendorDispute.IsNew)
                    {
                        var nextNumber = new AutoNumberProcessor().NextNumber(AutoNumberCode.VendorDisputeNumber, _view.ActiveUser);
                        vendorDispute.DisputerReferenceNumber = nextNumber == default(long)
                                                                    ? string.Empty
                                                                    : nextNumber.GetString();
                    }

                    _vendorDisputeValidator.Connection = Connection;
                    _vendorDisputeValidator.Transaction = Transaction;

                    vendorDispute.Connection = Connection;
                    vendorDispute.Transaction = Transaction;

                    // validate VendorDispute
                    _vendorDisputeValidator.Messages.Clear();
                    if (!_vendorDisputeValidator.IsValid(vendorDispute))
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(_vendorDisputeValidator.Messages);
                        return;
                    }

                    if (_vendorDisputeValidator.Messages.Any())
                        messages.AddRange(_vendorDisputeValidator.Messages); // for warning messages

                    // save
                    vendorDispute.Save();

                    // audit log VendorDispute
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Vendor Dispute: {1}", AuditLogConstants.AddedNew, vendorDispute.DisputerReferenceNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = vendorDispute.EntityName(),
                        EntityId = vendorDispute.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    var vendorDisputeDetails = new List<VendorDisputeDetail>();
                    foreach (var vendorDisputeDetail in chargeGroup.Select(charge => new VendorDisputeDetail()
                        {
                            TenantId = charge.TenantId,
                            DisputedId = vendorDispute.Id,
                            OriginalChargeAmount = charge.UnitBuy,
                            RevisedChargeAmount = charge.UnitBuy,
                            ChargeLineId = charge.Id,
                            ChargeLineIdType = charge.ChargeType == ChargeType.ServiceTicket
                                                   ? ChargeLineType.ServiceTicket
                                                   : ChargeLineType.Shipment,
                            VendorDispute = vendorDispute
                        }))
                    {
                        vendorDisputeDetail.Connection = Connection;
                        vendorDisputeDetail.Transaction = Transaction;
                        vendorDisputeDetails.Add(vendorDisputeDetail);
                    }

                    vendorDispute.VendorDisputeDetails = vendorDisputeDetails;

					if (!_vendorDisputeValidator.IsValid(vendorDispute))
					{
						RollBackTransaction();
						_view.DisplayMessages(_vendorDisputeValidator.Messages);
						return;
					}

                    // Save VendorDisputeDetail
                    ProcessVendorDisputeDetails(vendorDispute);
					
                }


                    // display Succesfully message
                    messages.Add(new ValidationMessage { Message = "Charges Disputed.", Type = ValidateMessageType.Information});
                    CommitTransaction();
                    chargeGroups.ForEach(s => _view.SendNotification(s, ChargeNotificationType.Dispute));
					_view.OnDisputeChargesComplete();
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new [] {ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message)});
				_view.OnDisputeChargesComplete();
                return;
            }

            _view.DisplayMessages(messages);
			

        }

        private void ProcessVendorDisputeDetails(VendorDispute vendorDispute)
        {
            foreach (var vendorDisputeDetail in vendorDispute.VendorDisputeDetails)
            {
                vendorDisputeDetail.Connection = Connection;
                vendorDisputeDetail.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Vendor Dispute Number:{2} Detail Ref#:{3}",
                                                AuditLogConstants.AddedNew, vendorDisputeDetail.EntityName(), vendorDispute.DisputerReferenceNumber, vendorDisputeDetail.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = vendorDispute.EntityName(),
                    EntityId = vendorDispute.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                // save
                vendorDisputeDetail.Save();
            }
        }

        private void DocumentSave(ChargeViewSearchDto item, VendorBill vb)
        {
            item.Documents.ForEach(d =>
            {
                if (item.ChargeType == ChargeType.Shipment)
                {
                    var doc = new ShipmentDocument()
                    {
                        TenantId = item.TenantId,
                        ShipmentId = item.ParentId.ToLong(),
                        LocationPath = d.LocationPath,
                        DocumentTagId = d.DocumentTagId,
                        Name = d.Name,
                        Description = d.Description,
                        IsInternal = false,
                        FtpDelivered = false,
						VendorBillId = vb.Id
                    };

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Document Name:{3}",
                                                    AuditLogConstants.AddedNew, d.EntityName(), item.Number, d.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = new Shipment().EntityName(),
                        EntityId = item.ParentId.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();


                    doc.Connection = Connection;
                    doc.Transaction = Transaction;
                    doc.Save();
                }
                if (item.ChargeType == ChargeType.ServiceTicket)
                {
                    var doc = new ServiceTicketDocument()
                    {
                        TenantId = item.TenantId,
                        ServiceTicketId = item.ParentId.ToLong(),
                        LocationPath = d.LocationPath,
                        DocumentTagId = d.DocumentTagId,
                        Name = d.Name,
                        Description = d.Description,
                        IsInternal = false,
                        FtpDelivered = false
                    };

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Service Ticket Number:{2} Document Name:{3}",
                                                    AuditLogConstants.AddedNew, d.EntityName(), item.Number, d.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = new ServiceTicket().EntityName(),
                        EntityId = item.ParentId.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    doc.Connection = Connection;
                    doc.Transaction = Transaction;
                    doc.Save();
                }
            });
        }

        private void ProcessVendorBillDetails(VendorBill bill)
        {
            foreach (var i in bill.Details)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                i.Save();
                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Vendor Bill Number: {2} Detail Ref#:{3}",
                                                    AuditLogConstants.AddedNew, i.EntityName(), bill.DocumentNumber,
                                                    i.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = bill.EntityName(),
                        EntityId = bill.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
            }
        }

        private List<ValidationMessage> ProcessCharges(IEnumerable<ChargeViewSearchDto> charges, VendorBill bill)
        {
            foreach (var charge in charges)
            {
                _chargeValidator.Messages.Clear();
                if (!_chargeValidator.IsValid(charge))
                {
                    return _chargeValidator.Messages;
                }

                Lock @lock;
                switch (charge.ChargeType)
                {
                    case ChargeType.ServiceTicket:
                        var stCharge = new ServiceTicketCharge(charge.Id, true)
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                VendorBillId = bill.Id
                            };

                        @lock = stCharge.ObtainLock(_view.ActiveUser, stCharge.Id);
                        if (!@lock.IsUserLock(_view.ActiveUser))
                        {
                            return new List<ValidationMessage>
                                {
                                    ValidationMessage.Error("Could not obtain lock for Charge Code {0}, Entity Reference Number {0}: {1}", charge.ChargeCode.Code, charge.Number, ProcessorVars.UnableToObtainLockErrMsg)
                                };
                        }

                        foreach (var change in charge.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description =
                                    string.Format("{0} Charge Ref#:{1} {2}", stCharge.EntityName(),
                                                  stCharge.Id, change),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = stCharge.ServiceTicket.EntityName(),
                                EntityId = stCharge.ServiceTicket.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                        stCharge.Save();
                        @lock.Delete();
                        break;
                    case ChargeType.Shipment:
                        var shCharge = new ShipmentCharge(charge.Id, true)
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                VendorBillId = bill.Id
                            };

                        @lock = shCharge.ObtainLock(_view.ActiveUser, shCharge.Id);
                        if (!@lock.IsUserLock(_view.ActiveUser))
                        {
                            return new List<ValidationMessage>
                                {
                                    ValidationMessage.Error("Could not obtain lock for Charge Number {0}: {1}", charge.Number, ProcessorVars.UnableToObtainLockErrMsg)
                                };
                        }

                        foreach (var change in charge.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description =
                                    string.Format("{0} Charge Ref#:{1} {2}", shCharge.EntityName(),
                                                  shCharge.Id, change),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = shCharge.Shipment.EntityName(),
                                EntityId = shCharge.Shipment.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                        shCharge.Save();
                        @lock.Delete();
                        break;
                }
            }

            return new List<ValidationMessage>();
        }
    }
}