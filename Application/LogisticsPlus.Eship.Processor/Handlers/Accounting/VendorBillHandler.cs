﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class VendorBillHandler : EntityBase
    {
        private readonly IVendorBillView _view;
        private readonly VendorBillValidator _validator = new VendorBillValidator();

        public VendorBillHandler(IVendorBillView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.LoadAuditLog += OnLoadAuditLog;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.VendorSearch += OnVendorSearch;
            _view.BatchSave += OnBatchSave;
        }

        private void OnSave(object sender, ViewEventArgs<VendorBill> e)
        {
            var bill = e.Argument;

            // check locks
            if (!bill.IsNew)
            {
                var @lock = bill.RetrieveLock(_view.ActiveUser, bill.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            // retrieve db invoice for collection comparisons ...
            var dbVendorBill = new VendorBill(bill.Id, false);
            dbVendorBill.LoadCollections();

            var shipmentChargesToDelete = new List<ShipmentCharge>();
            var serviceTicketChargesToDelete = new List<ServiceTicketCharge>();
            var shipmentChargesToAdd = new List<ShipmentCharge>();
            var serviceTicketChargesToAdd = new List<ServiceTicketCharge>();



            // fetch charges that will need link removed
            foreach (var detail in dbVendorBill.Details.Where(d => d.ReferenceType == DetailReferenceType.Shipment))
            {
                if (bill.Details.Select(i => i.Id).Contains(detail.Id)) continue;

                var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(detail.ReferenceNumber,
                                                                                  detail.TenantId);

                var charges = shipment.Charges
                                     .Where(c => c.ChargeCodeId == detail.ChargeCodeId
                                                 && c.VendorBillId == bill.Id
                                                 && !shipmentChargesToDelete.Select(stc => stc.Id).Contains(c.Id));

                // only remove links if all details with this ChargeCodeId/ReferenceNumber/ReferenceType have been removed from the bill
                if (charges.Any() &&
                        bill.Details.Count(d => d.ChargeCodeId == detail.ChargeCodeId 
                            && d.ReferenceNumber == shipment.ShipmentNumber 
                            && d.ReferenceType == DetailReferenceType.Shipment) == 0)
                {
                    shipmentChargesToDelete.AddRange(charges);
                }
            }

            foreach (var detail in dbVendorBill.Details.Where(d => d.ReferenceType == DetailReferenceType.ServiceTicket))
            {
                if (bill.Details.Select(i => i.Id).Contains(detail.Id)) continue;

                var serviceTicket = new ServiceTicketSearch().FetchServiceTicketByServiceTicketNumber(detail.ReferenceNumber, detail.TenantId);

                var charges = serviceTicket.Charges
                                          .Where(c => c.ChargeCodeId == detail.ChargeCodeId
                                                               && c.VendorBillId == bill.Id
                                                               && !serviceTicketChargesToDelete.Select(stc => stc.Id).Contains(c.Id));

                if (charges.Any() &&
                        bill.Details.Count(d => d.ChargeCodeId == detail.ChargeCodeId
                            && d.ReferenceNumber == serviceTicket.ServiceTicketNumber
                            && d.ReferenceType == DetailReferenceType.ServiceTicket) == 0)
                {
                    serviceTicketChargesToDelete.AddRange(charges);
                }
            }

            // Ensure that we can fetch Vendor Applicable charges
            if (bill.VendorLocationId != default(long))
            {
                // fetch charges that will need link added

                foreach (var detail in bill.Details.Where(d => d.ReferenceType == DetailReferenceType.Shipment))
                {
                    if (dbVendorBill.Details.Select(i => i.Id).Contains(detail.Id)) continue;

                    var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(detail.ReferenceNumber, detail.TenantId);

                    var charge = shipment.VendorApplicableCharges(bill.VendorLocation.Vendor.Id)
                                         .FirstOrDefault(c => c.ChargeCodeId == detail.ChargeCodeId
                                                              && c.VendorBillId == default(long)
                                                              && c.Shipment.AccountBuckets.First(a => a.Primary).AccountBucket.Code == detail.AccountBucket.Code
                                                              && !shipmentChargesToAdd.Select(stc => stc.Id).Contains(c.Id));

                    if (charge != null) shipmentChargesToAdd.Add(charge);
                }

                foreach (var detail in bill.Details.Where(d => d.ReferenceType == DetailReferenceType.ServiceTicket))
                {
                    if (dbVendorBill.Details.Select(i => i.Id).Contains(detail.Id)) continue;

                    var serviceTicket =
                        new ServiceTicketSearch().FetchServiceTicketByServiceTicketNumber(detail.ReferenceNumber, detail.TenantId);

                    var charge = serviceTicket.VendorApplicableCharges(bill.VendorLocation.Vendor.Id)
                                              .FirstOrDefault(c => c.ChargeCodeId == detail.ChargeCodeId
                                                                   && c.VendorBillId == default(long)
                                                                   && c.ServiceTicket.AccountBucket.Code == detail.AccountBucket.Code
                                                                   && !serviceTicketChargesToAdd.Select(stc => stc.Id).Contains(c.Id));

                    if (charge != null) serviceTicketChargesToAdd.Add(charge);
                }
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                bill.Connection = Connection;
                bill.Transaction = Transaction;

                // validate invoice
                _validator.Messages.Clear();
                if (!_validator.IsValid(bill))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                // audit log vendor bill
                if (bill.IsNew)
                {
                    // save
                    bill.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Vendor Bill: {1}", AuditLogConstants.AddedNew, bill.DocumentNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = bill.EntityName(),
                        EntityId = bill.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (bill.HasChanges())
                {
                    foreach (var change in bill.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = bill.EntityName(),
                            EntityId = bill.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save
                    bill.Save();
                }

                // process rest of collections
                ProcessVendorBillDetails(bill, dbVendorBill.Details);

                // remove VendorBillId from removed charges
                var messages = ProcessVendorBillLink(shipmentChargesToDelete, serviceTicketChargesToDelete, default(long));
                if (messages.Any())
                {
                    RollBackTransaction();
                    _view.DisplayMessages(messages);
                    return;
                }

                // add VendorBillId to added charges
                messages.Clear();
                messages = ProcessVendorBillLink(shipmentChargesToAdd, serviceTicketChargesToAdd, bill.Id);
                if (messages.Any())
                {
                    RollBackTransaction();
                    _view.DisplayMessages(messages);
                    return;
                }

                // commit transaction
                CommitTransaction();

                _view.Set(bill);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<VendorBill> e)
        {
            var bill = e.Argument;

            if (bill.IsNew)
            {
                _view.Set(new VendorBill());
                return;
            }

            // load collections
            bill.LoadCollections();

            // obtain lock/check lock
            var @lock = bill.ObtainLock(_view.ActiveUser, bill.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            var shipmentCharges = new List<ShipmentCharge>();
            var serviceTicketCharges = new List<ServiceTicketCharge>();

            foreach (var detail in bill.Details.Where(d => d.ReferenceType == DetailReferenceType.Shipment))
            {
                var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(detail.ReferenceNumber, detail.TenantId);
				if(shipment == null) continue;
                shipmentCharges.AddRange(shipment.Charges.Where(c => c.VendorBillId == bill.Id && !shipmentCharges.Select(sc => sc.Id).Contains(c.Id)));
            }

            foreach (var detail in bill.Details.Where(d => d.ReferenceType == DetailReferenceType.ServiceTicket))
            {
                var serviceTicket = new ServiceTicketSearch().FetchServiceTicketByServiceTicketNumber(detail.ReferenceNumber, detail.TenantId);
				if (serviceTicket == null) continue;
                serviceTicketCharges.AddRange(serviceTicket.Charges.Where(c => c.VendorBillId == bill.Id && !serviceTicketCharges.Select(sc => sc.Id).Contains(c.Id)));
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                bill.Connection = Connection;
                bill.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.CanDeleteVendorBill(bill))
                {
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Vendor Bill Number: {1}", AuditLogConstants.Delete, bill.DocumentNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = bill.EntityName(),
                    EntityId = bill.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();


                // handle collection deletes
                foreach (var detail in bill.Details)
                {
                    detail.Connection = Connection;
                    detail.Transaction = Transaction;

                    detail.Delete();
                }


                // remove VendorBillId from associated charges
                var messages = ProcessVendorBillLink(shipmentCharges, serviceTicketCharges, default(long));
                if (messages.Any())
                {
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(messages);
                    return;
                }

                //delete bill and lock
                bill.Delete();

                // delete lock
                @lock.Delete();

                CommitTransaction();

                _view.Set(new VendorBill
                {
                    DateCreated = DateTime.Now,
                    DocumentDate = DateUtility.SystemEarliestDateTime,
                    PostDate = DateUtility.SystemEarliestDateTime,
                    ExportDate = DateUtility.SystemEarliestDateTime,
                    Details = new List<VendorBillDetail>(),
                });

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnBatchSave(object sender, ViewEventArgs<List<VendorBill>> e)
        {
            var messages = new List<ValidationMessage>();

            foreach (var bill in e.Argument)
            {

                var recordIdentifier = string.Format("Document Number: {0} Vendor Number: {1} ", bill.DocumentNumber,
                                           bill.VendorLocation == null
                                               ? "[Invalid]"
                                               : bill.VendorLocation.Vendor.VendorNumber);
                _validator.Messages.Clear();

                var shipmentChargesToAdd = new List<ShipmentCharge>();
                var serviceTicketChargesToAdd = new List<ServiceTicketCharge>();

                if (bill.VendorLocation != null)
                {
                    // fetch charges that will need link added
                    foreach (var detail in bill.Details.Where(d => d.ReferenceType == DetailReferenceType.Shipment))
                    {
                        var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(detail.ReferenceNumber, detail.TenantId);

                        var charge = shipment.VendorApplicableCharges(bill.VendorLocation.Vendor.Id)
                                             .FirstOrDefault(c => c.ChargeCodeId == detail.ChargeCodeId
                                                                  && c.VendorBillId == default(long)
                                                                  && c.Shipment.AccountBuckets.First(a => a.Primary).AccountBucket.Code == detail.AccountBucket.Code
                                                                  && !shipmentChargesToAdd.Select(stc => stc.Id).Contains(c.Id));

                        if (charge != null) shipmentChargesToAdd.Add(charge);
                    }

                    foreach (var detail in bill.Details.Where(d => d.ReferenceType == DetailReferenceType.ServiceTicket))
                    {
                        var serviceTicket = new ServiceTicketSearch().FetchServiceTicketByServiceTicketNumber(detail.ReferenceNumber, detail.TenantId);

                        var charge = serviceTicket.VendorApplicableCharges(bill.VendorLocation.Vendor.Id)
                                                  .FirstOrDefault(c => c.ChargeCodeId == detail.ChargeCodeId
                                                                       && c.VendorBillId == default(long)
                                                                       && c.ServiceTicket.AccountBucket.Code == detail.AccountBucket.Code
                                                                       && !serviceTicketChargesToAdd.Select(stc => stc.Id).Contains(c.Id));

                        if (charge != null) serviceTicketChargesToAdd.Add(charge);
                    }
                }

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    bill.Connection = Connection;
                    bill.Transaction = Transaction;

                    // validation
                    if (!_validator.IsValid(bill))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages.Select(m => new ValidationMessage { Message = recordIdentifier + m.Message, Type = m.Type }));
                        continue;
                    }

                    // save
                    bill.Save();

                    // audit log
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description =
                                string.Format("{0} Vendor Bill: {1} ", AuditLogConstants.AddedNew, bill.DocumentNumber),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = bill.EntityName(),
                            EntityId = bill.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // process items
                    ProcessVendorBillDetails(bill, new VendorBillDetail[0]);

                    // add VendorBillId to added charges
                    messages.Clear();
                    messages = ProcessVendorBillLink(shipmentChargesToAdd, serviceTicketChargesToAdd, bill.Id);
                    if (messages.Any())
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(messages);
                        return;
                    }

                    // commit transaction
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [Record: {1}] {2}", ProcessorVars.RecordSaveErrMsg,  recordIdentifier, ex.Message));
                }
            }

            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }


        private void OnVendorSearch(object sender, ViewEventArgs<string> e)
        {
            if (e.Argument == string.Empty)
            {
                _view.DisplayVendor(null);
                return;
            }

            var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);

            if (vendor == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
            _view.DisplayVendor(vendor);
        }


        private void OnUnLock(object sender, ViewEventArgs<VendorBill> e)
        {
            var request = e.Argument;
            var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }
            @lock.Delete();
        }

        private void OnLock(object sender, ViewEventArgs<VendorBill> e)
        {
            var request = e.Argument;
            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnLoadAuditLog(object sender, ViewEventArgs<VendorBill> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
                                                                             e.Argument.TenantId));
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.VendorBillDetailReferenceType = ProcessorUtilities.GetAll<DetailReferenceType>();
        }


        private void ProcessVendorBillDetails(VendorBill bill, IEnumerable<VendorBillDetail> dbDetails)
        {
            var dbDelete = dbDetails.Where(dbi => !bill.Details.Select(i => i.Id).Contains(dbi.Id));

            foreach (var i in dbDelete)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Vendor Bill Number:{2} Detail Ref#:{3}",
                                                AuditLogConstants.Delete, i.EntityName(), bill.DocumentNumber, i.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = bill.EntityName(),
                    EntityId = bill.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                i.Delete();
            }

            foreach (var i in bill.Details)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                if (i.IsNew)
                {
                    i.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Vendor Bill Number:{2} Detail Ref#:{3}",
                                                    AuditLogConstants.AddedNew, i.EntityName(), bill.DocumentNumber, i.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = bill.EntityName(),
                        EntityId = bill.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (i.HasChanges())
                {
                    foreach (var change in i.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Detail Ref#:{1} {2}", i.EntityName(), i.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = bill.EntityName(),
                            EntityId = bill.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    i.Save();
                }
            }

        }

        private List<ValidationMessage> ProcessVendorBillLink(IEnumerable<ShipmentCharge> shipmentCharges,IEnumerable<ServiceTicketCharge> serviceTicketCharges, long vendorBillId)
        {
            foreach (var shipmentCharge in shipmentCharges)
            {
                var shipment = new Shipment(shipmentCharge.Shipment.Id);

                var shipmentLock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);
                if (!shipmentLock.IsUserLock(_view.ActiveUser))
                {
                    return new List<ValidationMessage> { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) };
                }

                shipmentCharge.TakeSnapShot();
                shipmentCharge.Connection = Connection;
                shipmentCharge.Transaction = Transaction;

                shipmentCharge.VendorBillId = vendorBillId;

                foreach (var change in shipmentCharge.Changes())
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} Charge Ref#:{1} {2}", shipmentCharge.EntityName(),
                                          shipmentCharge.Id, change),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipmentCharge.Shipment.EntityName(),
                        EntityId = shipmentCharge.Shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                shipmentCharge.Save();
                shipmentLock.Delete();
            }

            foreach (var serviceTicketCharge in serviceTicketCharges)
            {
                var serviceTicket = new ServiceTicket(serviceTicketCharge.ServiceTicket.Id);

                var serviceTicketLock = serviceTicket.ObtainLock(_view.ActiveUser, serviceTicket.Id);
                if (!serviceTicketLock.IsUserLock(_view.ActiveUser))
                {
                    return new List<ValidationMessage> { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) };
                }

                serviceTicketCharge.TakeSnapShot();
                serviceTicketCharge.Connection = Connection;
                serviceTicketCharge.Transaction = Transaction;

                serviceTicketCharge.VendorBillId = vendorBillId;

                foreach (var change in serviceTicketCharge.Changes())
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} Charge Ref#:{1} {2}", serviceTicketCharge.EntityName(),
                                          serviceTicketCharge.Id, change),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicketCharge.ServiceTicket.EntityName(),
                        EntityId = serviceTicketCharge.ServiceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                serviceTicketCharge.Save();
                serviceTicketLock.Delete();
            }

            return new List<ValidationMessage>();
        }
    }
}
