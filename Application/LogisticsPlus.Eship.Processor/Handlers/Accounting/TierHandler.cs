﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class TierHandler : EntityBase
	{
		private readonly ITierView _view;
		private readonly TierValidator _validator = new TierValidator();
		private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

		public TierHandler(ITierView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.LoadAuditLog += OnLoadAuditLog;
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<Tier> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
		}

		private void OnSave(object sender, ViewEventArgs<Tier> e)
		{
			var tier = e.Argument;

			var isNew = tier.IsNew;

			if (!isNew)
			{
				var @lock = tier.RetrieveLock(_view.ActiveUser, tier.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			if (isNew)
			{
				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.TierNumber, _view.ActiveUser);
				tier.TierNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				tier.Connection = Connection;
				tier.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.IsValid(tier))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (tier.HasChanges())
					foreach (var change in tier.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = tier.EntityName(),
								EntityId = tier.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

				if (tier.IsNew || tier.HasChanges()) tier.Save();

				if (isNew)
				{
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, tier.Name, tier.TierNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = tier.EntityName(),
						EntityId = tier.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}

				CommitTransaction();

				_view.Set(tier);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDelete(object sender, ViewEventArgs<Tier> e)
		{
			var tier = e.Argument;

			if (tier.IsNew)
			{
				_view.Set(tier);
				return;
			}

			var @lock = tier.ObtainLock(_view.ActiveUser, tier.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				tier.Connection = Connection;
				tier.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if(!_validator.CanDeleteTier(tier))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, tier.Name, tier.TierNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = tier.EntityName(),
						EntityId = tier.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				tier.Delete();
				@lock.Delete();

				CommitTransaction();

				_view.Set(new Tier());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnUnLock(object sender, ViewEventArgs<Tier> e)
		{
			var tier = e.Argument;

			var @lock = tier.RetrieveLock(_view.ActiveUser, tier.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<Tier> e)
		{
			var tier = e.Argument;
			var @lock = tier.ObtainLock(_view.ActiveUser, tier.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}
	}
}
