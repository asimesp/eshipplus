﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class PaymentProfileManagerHandler : EntityBase
    {
        private const string DeletionOfPaymentProfileFailed = "Deletion of payment profile via the payment gateway has failed. This payment profile will need to be delete manually via the payment gateway's website";
        private const string FailedToLoadPaymentProfilesError = "Failed to load payment profiles from payment gateway: {0}";

        private readonly IPaymentProfileManagerView _view;
        private readonly PaymentGatewayValidator _pgValidator = new PaymentGatewayValidator();

        public PaymentProfileManagerHandler(IPaymentProfileManagerView view)
        {
            _view = view;
        }


        public void Initialize()
        {
            _view.LoadPaymentProfiles += OnLoadPaymentProfiles;
            _view.DeletePaymentProfile += OnDeletePaymentProfile;
            _view.AddOrUpdatePaymentProfile += OnAddOrUpdatePaymentProfile;
        }


        private void OnAddOrUpdatePaymentProfile(object sender, ViewEventArgs<long, PaymentGatewayPaymentProfile> e)
        {
            var customer = new Customer(e.Argument);
            if (customer.IsNew) return;

            var gateway = customer.Tenant.GetPaymentGatewayService();
            var profile = e.Argument2;

            // if string is null/empty, then add new otherwise update profile
            if (String.IsNullOrEmpty(profile.PaymentProfileId))
            {
                try
                {
                    var profileId = gateway.AddPaymentProfile(new PaymentInformation
                    {
                        BillingAddress = profile.BillingStreet,
                        BillingCity = profile.BillingCity,
                        BillingCountry = profile.BillingCountry,
                        BillingFirstName = profile.BillingFirstName,
                        BillingLastName = profile.BillingLastName,
                        BillingState = profile.BillingState,
                        BillingZip = profile.BillingZip,
                        CardNumber = profile.CreditCardNumber,
                        CustomerNumber = customer.CustomerNumber,
                        CustomerPaymentGatewayKey = customer.PaymentGatewayKey,
                        ExpMonth = profile.ExpirationDateMonth.ToInt(),
                        ExpYear = profile.ExpirationDateYear.ToInt(),
                        SecurityCode = profile.SecurityCode,
                    });

                    if (!String.IsNullOrEmpty(profileId))
                    {
                        var msgs = new List<ValidationMessage> { ValidationMessage.Information("Creation of payment profile via the payment gateway was successful") };
                        _view.PaymentProfiles = gateway.GetPaymentProfiles(customer.PaymentGatewayKey);
                        _view.DisplayMessages(msgs);
                    }
                    else
                    {
                        _view.DisplayMessages(new[] { ValidationMessage.Error("Creation of payment profile via the payment gateway has failed. This payment profile will need to be created manually via the payment gateway's website") });
                    }
                }
                catch (Exception ex)
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ex.Message) });
                }
            }
            else
            {
                try
                {
                    if (gateway.UpdatePaymentProfile(customer.PaymentGatewayKey, e.Argument2))
                    {
                        var msgs = new List<ValidationMessage> { ValidationMessage.Information("Updating of payment profile via the payment gateway was successful") };
                        _view.PaymentProfiles = gateway.GetPaymentProfiles(customer.PaymentGatewayKey);
                        _view.DisplayMessages(msgs);
                    }
                    else
                    {
                        _view.DisplayMessages(new[] { ValidationMessage.Error("Updating of payment profile via the payment gateway has failed. This payment profile will need to be updated manually via the payment gateway's website") });
                    }
                }
                catch (Exception ex)
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ex.Message) });
                }
            }
        }

        private void OnDeletePaymentProfile(object sender, ViewEventArgs<long, string> e)
        {
            var customer = new Customer(e.Argument);
            if (customer.IsNew) return;

            if (string.IsNullOrEmpty(customer.PaymentGatewayKey))
            {
                _view.PaymentProfiles = new List<PaymentGatewayPaymentProfile>();
                return;
            }

            _pgValidator.Messages.Clear();
            if (!_pgValidator.CanDeleteCustomerPaymentGatewayPaymentProfile(e.Argument2, _view.ActiveUser.TenantId))
            {
                _view.DisplayMessages(_pgValidator.Messages);
                return;
            }
            
            var gateway = customer.Tenant.GetPaymentGatewayService();

            try
            {
                if (gateway.RemovePaymentProfile(customer.PaymentGatewayKey, e.Argument2))
                {
                    _view.PaymentProfiles = gateway.GetPaymentProfiles(customer.PaymentGatewayKey);
                    _view.DisplayMessages(new List<ValidationMessage> { ValidationMessage.Information("Deletion of payment profile via the payment gateway was successful") });
                }
                else
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(DeletionOfPaymentProfileFailed) });
                }
            }
            catch (Exception ex)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(String.Format("{0}: {1}", DeletionOfPaymentProfileFailed, ex.Message)) });
            }
        }

        private void OnLoadPaymentProfiles(object sender, ViewEventArgs<long> e)
        {
            var customer = new Customer(e.Argument);
            if (customer.IsNew) return;

            if (String.IsNullOrEmpty(customer.PaymentGatewayKey))
            {
                _view.PaymentProfiles = new List<PaymentGatewayPaymentProfile>();
                return;
            }

            var gateway = customer.Tenant.GetPaymentGatewayService();
            try
            {
                _view.PaymentProfiles = gateway.GetPaymentProfiles(customer.PaymentGatewayKey);
            }
            catch (Exception ex)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(FailedToLoadPaymentProfilesError, ex.Message) });
            }
        }
    }
}
