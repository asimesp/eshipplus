﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class InvoicePostingHandler : EntityBase
	{
		private readonly IInvoicePostingView _view;
		private readonly InvoiceValidator _validator = new InvoiceValidator();

		public InvoicePostingHandler(IInvoicePostingView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
			_view.Post += OnPost;
			_view.PostDownload += OnPostDownload;
		}

		private void OnSearch(object sender, ViewEventArgs<InvoiceViewSearchCriteria> e)
		{
			var results = new InvoiceSearch().FetchInvoiceDashboardDtos(e.Argument, _view.ActiveUser.TenantId);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}

		private void OnPost(object sender, ViewEventArgs<List<Invoice>> e)
		{
			var messages = new List<ValidationMessage>();
			var postedInvoices = new List<Invoice>();

			ProcessingInvoicePosting(e, postedInvoices, messages);

			messages.Add(ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
			_view.ProcessNotifications(postedInvoices);
			if (messages.Any()) _view.DisplayMessages(messages);
		}

		private void OnPostDownload(object sender, ViewEventArgs<List<Invoice>> e)
		{
			var messages = new List<ValidationMessage>();
			var postedInvoices = new List<Invoice>();

			ProcessingInvoicePosting(e, postedInvoices, messages);

            messages.Add(ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
			_view.ProcessNotifications(postedInvoices);
			_view.HandlePostedInvoicesAndMessages(postedInvoices, messages);
		}


		private void ProcessingInvoicePosting(ViewEventArgs<List<Invoice>> e, ICollection<Invoice> postedInvoices, List<ValidationMessage> messages)
		{
			foreach (var invoice in e.Argument)
			{
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					// link up
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					//attempt lock
					var @lock = invoice.ObtainLock(_view.ActiveUser, invoice.Id);
					if (!@lock.IsUserLock(_view.ActiveUser))
					{
						RollBackTransaction();
						messages.Add(
							ValidationMessage.Error(string.Format("{0} on Invoice Number: {1}",
							                                      ProcessorVars.UnableToObtainLockErrMsg, invoice.InvoiceNumber)));
						continue;
					}

					// chain lock
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					// chain invoice
					invoice.Connection = Connection;
					invoice.Transaction = Transaction;

					invoice.TakeSnapShot();
					invoice.Posted = true;
					invoice.PostDate = DateTime.Now;
					invoice.DueDate = DateTime.Now.AddDays(invoice.CustomerLocation.Customer.InvoiceTerms);

					// test customer invoice process requirements for shipments
					_validator.Messages.Clear();
					if (!_validator.MeetsInvoicingRequirement(invoice))
					{
						RollBackTransaction();
						@lock.Delete();
						var suffix = string.Format(" [Inv. #: {0}]", invoice.InvoiceNumber);
						foreach (var m in _validator.Messages) m.Message += suffix;
						messages.AddRange(_validator.Messages);
						continue;
					}

					foreach (var change in invoice.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = invoice.EntityName(),
								EntityId = invoice.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					invoice.Save();
					postedInvoices.Add(invoice);

					@lock.Delete();

					// commit transaction
					CommitTransaction();
				}
				catch (Exception ex)
				{
					RollBackTransaction();
					_view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Inv. #: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, invoice.InvoiceNumber, ex.Message));
				}
			}
		}
	}
}
