﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class ServiceFinderHandler : EntityBase
	{
		private readonly IServiceFinderView _view;

		public ServiceFinderHandler(IServiceFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			var results = new ServiceSearch().FetchServices(e.Argument, _view.ActiveUser.TenantId);
			if (_view.ServiceCategoryFilter.EnumValueIsValid<ServiceCategory>() && results.Any())
			{
				var category = _view.ServiceCategoryFilter.ToEnum<ServiceCategory>();
				results = results.Where(r => r.Category == category).ToList();
			}

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}
	}
}
