﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class ShipmentsToBeInvoicedHandler : EntityBase
	{
		private readonly IShipmentsToBeInvoicedView _view;
		public ShipmentsToBeInvoicedHandler(IShipmentsToBeInvoicedView view) { _view = view; }
		private readonly InvoiceValidator _validator = new InvoiceValidator();
		private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

		public void Initialize()
		{
			_view.Search += OnSearch;
			_view.SingleSave += OnSingleSave;
			_view.MultiSave += OnMultiSave;
			_view.SetShipmentsReadyToInvoice += OnSetShipmentsReadyToInvoice;
		}


		private void OnSetShipmentsReadyToInvoice(object sender, ViewEventArgs<List<Shipment>> e)
		{
			var shipments = e.Argument;
			var messages = new List<ValidationMessage>();

			foreach (var shipment in shipments)
			{
				//attempt lock
				var @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
					continue;
				}

				if (shipment.Status == ShipmentStatus.Invoiced || shipment.Status == ShipmentStatus.Void)
				{
					messages.Add(
						ValidationMessage.Error(
							"Unable to process. Status is {0}. [Shipment #: {1}].", shipment.Status.FormattedString(), shipment.ShipmentNumber));
					@lock.Delete();
					continue;
				}

				if(shipment.AuditedForInvoicing) continue;	// do not duplicate process already done previously.

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					// link up
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					// chain lock
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					shipment.Connection = Connection;
					shipment.Transaction = Transaction;

					// save
					shipment.TakeSnapShot();
					shipment.AuditedForInvoicing = true;
				    shipment.InDispute = false;
                    shipment.InDisputeReason = InDisputeReason.NotApplicable;
					foreach (var change in shipment.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					shipment.Save();

					@lock.Delete();

					// commit transaction
					CommitTransaction();
				}
				catch (Exception ex)
				{
					RollBackTransaction();
					shipment.AuditedForInvoicing = false; // remove set flag!
					_view.LogException(ex);
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
				}
			}

			// display loaded shipment that were updated
		    _view.DisplaySearchResult(
		        shipments.Where(s => s.AuditedForInvoicing && s.Status != ShipmentStatus.Void && s.Status != ShipmentStatus.Invoiced)
		            .Select(s => new ShipmentDashboardDto(s))
		            .ToList());

			// display messages.
			messages.Insert(0, ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}


		private void OnSearch(object sender, ViewEventArgs<ShipmentViewSearchCriteria> e)
		{
			var results = new ShipmentSearch().FetchShipmentsToBeInvoiced(e.Argument, _view.ActiveUser.TenantId);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
			_view.DisplaySearchResult(results);
		}

		private void OnSingleSave(object sender, ViewEventArgs<List<Shipment>> e)
		{
			var shipments = e.Argument;

			// validate for same customer
			var resellerShipment = shipments.FirstOrDefault(s => s.ResellerAdditionId != default(long) && s.BillReseller); // reseller check
			var customerId = resellerShipment == null
								? shipments.First().Customer.Id
								: resellerShipment.ResellerAddition.ResellerCustomerAccountId;

            if (shipments.Any(s => (s.ResellerAdditionId != default(long) && s.BillReseller && s.ResellerAddition.ResellerCustomerAccountId != customerId) 
                                || (s.ResellerAdditionId == default(long) && !s.BillReseller && s.Customer.Id != customerId)))
            {
                _view.DisplayMessages(new[] {ValidationMessage.Error("All shipments for a combined invoices must bill to same customer.")});
                return;
            }

			if (shipments.Count > 1)
			{
				var communication = shipments.First().Customer.Communication;

				if (communication != null)
				{
					var ediOrFtpEnabled = (communication.FtpEnabled || communication.EdiEnabled);
					var ediOrFtpNotifications =
						(communication.Notifications.Any(
							n => n.NotificationMethod == NotificationMethod.Edi || n.NotificationMethod == NotificationMethod.Ftp));

					if (ediOrFtpEnabled && ediOrFtpNotifications)
					{
						_view.DisplayMessages(new[] { ValidationMessage.Error("Cannot process EDI or FTP notifications for combined invoices.") });
						return;
					}
				}
			}

			var customerLocation = resellerShipment == null
									? shipments.First().Customer.Locations.FirstOrDefault(l => l.BillToLocation && l.MainBillToLocation)
									: resellerShipment.ResellerAddition.ResellerCustomerAccount.Locations.FirstOrDefault(l => l.BillToLocation && l.MainBillToLocation);

			if (customerLocation == null)
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error("Unable to find applicable bill to location.  Please check customer setup.") });
				return;
			}

			var messages = new List<ValidationMessage>();
			var locks = new List<Lock>();

			foreach (var shipmentLock in shipments.Select(shipment => shipment.ObtainLock(_view.ActiveUser, shipment.Id)))
			{
				if (!shipmentLock.IsUserLock(_view.ActiveUser))
					messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
				else
					locks.Add(shipmentLock);
			}

			//check for already invoiced shipments
			messages.AddRange(shipments
			                  	.Where(s => s.Status == ShipmentStatus.Invoiced || s.Status == ShipmentStatus.Void)
			                  	.Select(shipment => ValidationMessage.Error("Can not process Shipment [{0}] because status is {1}.",
			                  		                                    shipment.ShipmentNumber,
			                  		                                    shipment.Status.FormattedString())));

			if (messages.Any())
			{
				_view.DisplayMessages(messages);

				foreach (var @lock in locks)
					@lock.Delete();

				return;
			}

			var invoice = new Invoice
							{
								InvoiceDate = DateTime.Now,
								CustomerLocation = customerLocation,
								DueDate = DateUtility.SystemEarliestDateTime,
								PostDate = DateUtility.SystemEarliestDateTime,
								Posted = false,
								ExportDate = DateUtility.SystemEarliestDateTime,
								Exported = false,
								TenantId = _view.ActiveUser.TenantId,
								InvoiceType = InvoiceType.Invoice,
								PaidAmount = 0,
								SpecialInstruction = string.Empty,
								User = _view.ActiveUser,
								CustomerControlAccountNumber = string.Empty,
								PrefixId = customerLocation.Customer.PrefixId,
								HidePrefix = customerLocation.Customer.HidePrefix
							};

			foreach (var detail in from shipment in shipments
								   from charge in shipment.Charges
								   select new InvoiceDetail
											{
												Invoice = invoice,
												ReferenceType = DetailReferenceType.Shipment,
												ReferenceNumber = shipment.ShipmentNumber,
												Quantity = charge.Quantity,
												UnitSell = charge.UnitSell -
												   (shipment.ResellerAdditionId != default(long) && shipment.BillReseller
														? charge.GetResellerAdditions(shipment.ResellerAddition)
														: 0),
												UnitDiscount = charge.UnitDiscount,
												TenantId = _view.ActiveUser.TenantId,
												ChargeCodeId = charge.ChargeCodeId,
												AccountBucketId = shipment.AccountBuckets.First().AccountBucketId,
												Comment = charge.Comment
											})
			{
				invoice.Details.Add(detail);
			}

			var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.InvoiceNumber, _view.ActiveUser);
			invoice.InvoiceNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
			invoice.AppendNumberSuffix();


			// test customer invoice process requirements for shipments
			_validator.Messages.Clear();
			if (!_validator.MeetsInvoicingRequirement(invoice, shipments))
			{
				foreach (var l in locks) l.Delete();
				_view.DisplayMessages(_validator.Messages);
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				foreach (var l in locks)
				{
					l.Connection = Connection;
					l.Transaction = Transaction;
				}

				invoice.Connection = Connection;
				invoice.Transaction = Transaction;

				// validate invoice
				_validator.Messages.Clear();
				if (!_validator.IsValid(invoice))
				{
					RollBackTransaction();

					foreach (var l in locks)
						l.Delete();

					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// save
				invoice.Save();

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Invoice: {1}", AuditLogConstants.AddedNew, invoice.InvoiceNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = invoice.EntityName(),
						EntityId = invoice.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// process rest of collections
				ProcessInvoiceDetails(invoice);

				foreach (var shipment in shipments)
				{
					shipment.TakeSnapShot();
					shipment.Status = ShipmentStatus.Invoiced;
					foreach (var change in shipment.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					shipment.Save();
				}

				foreach (var l in locks)
					l.Delete();

				// commit transaction
				CommitTransaction();

				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();

				foreach (var l in locks)
					l.Delete();
                _view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}


		}

		private void OnMultiSave(object sender, ViewEventArgs<List<Shipment>> e)
		{
			var shipments = e.Argument;

			var messages = new List<ValidationMessage>();

			foreach (var shipment in shipments)
			{
				//attempt lock
				var @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
					continue;
				}

				//find customer location
				var customerLocation = shipment.ResellerAdditionId != default(long) && shipment.BillReseller
										? shipment.ResellerAddition.ResellerCustomerAccount.Locations.FirstOrDefault(l => l.BillToLocation && l.MainBillToLocation)
										: shipment.Customer.Locations.FirstOrDefault(l => l.BillToLocation && l.MainBillToLocation);

				if (customerLocation == null)
				{
					messages.Add(
						ValidationMessage.Error(
							"Unable to find applicable bill to location.  Please check customer setup. [Shipment #: {0}]",
							shipment.ShipmentNumber));

					@lock.Delete();
					continue;
				}


				if (shipment.Status == ShipmentStatus.Invoiced || shipment.Status == ShipmentStatus.Void)
				{
					messages.Add(
						ValidationMessage.Error(
							"Unable to process. Status is {0}. [Shipment #: {1}].", shipment.Status.FormattedString(), shipment.ShipmentNumber));
					@lock.Delete();
					continue;
				}

				//generate invoice
				var invoice = new Invoice
				{
					InvoiceDate = DateTime.Now,
					CustomerLocation = customerLocation,
					DueDate = DateUtility.SystemEarliestDateTime,
					PostDate = DateUtility.SystemEarliestDateTime,
					Posted = false,
					ExportDate = DateUtility.SystemEarliestDateTime,
					Exported = false,
					TenantId = _view.ActiveUser.TenantId,
					InvoiceType = InvoiceType.Invoice,
					PaidAmount = 0,
					User = _view.ActiveUser,
					CustomerControlAccountNumber = string.Empty,
					SpecialInstruction = string.Empty,
					PrefixId = shipment.PrefixId,
					HidePrefix = shipment.HidePrefix
				};

				foreach (var charge in shipment.Charges)
				{
					var detail = new InvoiceDetail
						{
							Invoice = invoice,
							ReferenceType = DetailReferenceType.Shipment,
							ReferenceNumber = shipment.ShipmentNumber,
							Quantity = charge.Quantity,
							//UnitSell = charge.UnitSell,
							UnitSell = charge.UnitSell -
							           (shipment.ResellerAdditionId != default(long) && shipment.BillReseller
								            ? charge.GetResellerAdditions(shipment.ResellerAddition)
								            : 0),
							UnitDiscount = charge.UnitDiscount,
							TenantId = _view.ActiveUser.TenantId,
							ChargeCodeId = charge.ChargeCodeId,
							AccountBucketId = shipment.AccountBuckets.First(a => a.Primary).AccountBucketId,
							Comment = charge.Comment
						};
					invoice.Details.Add(detail);
				}

				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.InvoiceNumber, _view.ActiveUser);
				invoice.InvoiceNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
				invoice.AppendNumberSuffix();

				// test customer invoice process requirements for shipment
				_validator.Messages.Clear();
				if (!_validator.MeetsInvoicingRequirement(invoice, shipment))
				{
					@lock.Delete();
					messages.AddRange(_validator.Messages);
					continue;
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					// link up
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					// chain lock
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					invoice.Connection = Connection;
					invoice.Transaction = Transaction;

					// validate invoice
					_validator.Messages.Clear();
					if (!_validator.IsValid(invoice))
					{
						RollBackTransaction();

						@lock.Delete();

						messages.AddRange(_validator.Messages);
						continue;
					}

					// save
					invoice.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Invoice: {1}", AuditLogConstants.AddedNew, invoice.InvoiceNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = invoice.EntityName(),
						EntityId = invoice.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					// process rest of collections
					ProcessInvoiceDetails(invoice);

					shipment.TakeSnapShot();
					shipment.Status = ShipmentStatus.Invoiced;
					foreach (var change in shipment.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					shipment.Save();

					@lock.Delete();

					// commit transaction
					CommitTransaction();
				}
				catch (Exception ex)
				{
					RollBackTransaction();
					_view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Shipment #: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg,  shipment.ShipmentNumber, ex.Message));
				}
			}

			messages.Insert(0, ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}


		private void ProcessInvoiceDetails(Invoice invoice)
		{
			foreach (var i in invoice.Details)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				i.Save();
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Invoice Number:{2} Detail Ref#:{3}",
												AuditLogConstants.AddedNew, i.EntityName(), invoice.InvoiceNumber, i.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = invoice.EntityName(),
					EntityId = invoice.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			}
		}
	}
}
