﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class PaymentRefundOrVoidHandler : EntityBase
    {
        private readonly IPaymentRefundOrVoidView _view;
        private readonly MiscReceiptValidator _validator = new MiscReceiptValidator();

        public PaymentRefundOrVoidHandler(IPaymentRefundOrVoidView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.RefundOrVoidMiscReceiptsForShipment += OnRefundOrVoidMiscReceiptsForShipment;
            _view.RefundOrVoidMiscReceiptsForLoadOrder += OnRefundOrVoidMiscReceiptsForLoadOrder;
            _view.LockLoadOrderAndMiscReceipts += OnLockLoadOrderAndMiscReceipts;
            _view.LockShipmentAndMiscReceipts += OnLockShipmentAndMiscReceipts;
            _view.UnlockMiscReceipts += OnUnlockMiscReceipts;
        }

        

        private void OnLockShipmentAndMiscReceipts(object sender, ViewEventArgs<Shipment, List<MiscReceipt>> e)
        {
            var shipmentLock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);
            if (!shipmentLock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock();
                _view.DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }
            if (shipmentLock.LockIsExpired()) shipmentLock.Refresh();

            var miscReceipts = e.Argument2;

            var msgs = new List<ValidationMessage>();
            var locks = new List<Lock> { shipmentLock };
            foreach (var receipt in miscReceipts)
            {
                var @lock = receipt.ObtainLock(_view.ActiveUser, receipt.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    msgs.Add(ValidationMessage.Error("Misc Receipt with Gateway Transaction Id:{0} - {1}", receipt.GatewayTransactionId, ProcessorVars.UnableToObtainLockErrMsg));
                    continue;
                }
                if (@lock.LockIsExpired()) @lock.Refresh();
                locks.Add(@lock);
            }

            if (!msgs.Any()) return;
            
            foreach (var l in locks) l.Delete();
            _view.FailedLock();
            _view.DisplayMessages(msgs);
        }

        private void OnLockLoadOrderAndMiscReceipts(object sender, ViewEventArgs<LoadOrder, List<MiscReceipt>> e)
        {
            var loadOrderLock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);
            if (!loadOrderLock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock();
                _view.DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }
            if (loadOrderLock.LockIsExpired()) loadOrderLock.Refresh();

            var miscReceipts = e.Argument2;

            var msgs = new List<ValidationMessage>();
            var locks = new List<Lock> { loadOrderLock };
            foreach (var receipt in miscReceipts)
            {
                var @lock = receipt.ObtainLock(_view.ActiveUser, receipt.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    msgs.Add(ValidationMessage.Error("Misc Receipt for Load Order Number:{0} - {1}", receipt.LoadOrder.LoadOrderNumber, ProcessorVars.UnableToObtainLockErrMsg));
                    continue;
                }
                if (@lock.LockIsExpired()) @lock.Refresh();
                locks.Add(@lock);
            }

            if (!msgs.Any()) return;

            foreach (var l in locks) l.Delete();
            _view.FailedLock();
            _view.DisplayMessages(msgs);
        }

        private void OnUnlockMiscReceipts(object sender, ViewEventArgs<List<MiscReceipt>> e)
        {
            foreach (var miscReceiptLock in e.Argument.Select(miscReceipt => miscReceipt.RetrieveLock(_view.ActiveUser, miscReceipt.Id)).Where(miscReceiptLock => miscReceiptLock != null && miscReceiptLock.IsUserLock(_view.ActiveUser)))
                miscReceiptLock.Delete();
        }


        public void OnRefundOrVoidMiscReceiptsForShipment(object sender, ViewEventArgs<Shipment, List<MiscReceipt>> e)
        {
            if (!e.Argument2.Any())
            {
                _view.DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error("No misc receipts were selected to be voided or refunded.") });
                return;
            }

            var shipmentLock = e.Argument.RetrieveLock(_view.ActiveUser, e.Argument.Id);
            if (!shipmentLock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock();
                _view.DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }
            if (shipmentLock.LockIsExpired()) shipmentLock.Refresh();

            var msgs = new List<ValidationMessage>();
            var locks = new List<Lock> { shipmentLock };
            foreach (var receipt in e.Argument2.Select(mr => mr.OriginalMiscReceipt))
            {
                var @lock = receipt.RetrieveLock(_view.ActiveUser, receipt.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    msgs.Add(ValidationMessage.Error("Misc Receipt with Gateway Transaction Id:{0} - {1}", receipt.GatewayTransactionId, ProcessorVars.UnableToObtainLockErrMsg));
                    continue;
                }
                if (@lock.LockIsExpired()) @lock.Refresh();
                locks.Add(@lock);
            }

            if (msgs.Any())
            {
                foreach (var l in locks) l.Delete();
                _view.FailedLock();
                _view.DisplayMessages(msgs);
            }

            msgs.AddRange(ProcessRefundsForMiscReceipts(e.Argument2));

            if (!msgs.Any()) msgs.Add(ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
            _view.DisplayMessages(msgs);
        }

        private void OnRefundOrVoidMiscReceiptsForLoadOrder(object sender, ViewEventArgs<LoadOrder, List<MiscReceipt>> e)
        {
            if (!e.Argument2.Any())
            {
                _view.DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error("No misc receipts were selected to be voided or refunded.") });
                return;
            }

            var loadOrderLock = e.Argument.RetrieveLock(_view.ActiveUser, e.Argument.Id);
            if (!loadOrderLock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock();
                _view.DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }
            if (loadOrderLock.LockIsExpired()) loadOrderLock.Refresh();

            var msgs = new List<ValidationMessage>();
            var locks = new List<Lock> { loadOrderLock };
            foreach (var receipt in e.Argument2.Select(mr => mr.OriginalMiscReceipt))
            {
                var @lock = receipt.RetrieveLock(_view.ActiveUser, receipt.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    msgs.Add(ValidationMessage.Error("Misc Receipt for Load Order Number:{0} - {1}", receipt.LoadOrder.LoadOrderNumber, ProcessorVars.UnableToObtainLockErrMsg));
                    continue;
                }
                if (@lock.LockIsExpired()) @lock.Refresh();
                locks.Add(@lock);
            }

            if (msgs.Any())
            {
                foreach (var l in locks) l.Delete();
                _view.FailedLock();
                _view.DisplayMessages(msgs);
            }

            msgs.AddRange(ProcessRefundsForMiscReceipts(e.Argument2));

            if (!msgs.Any()) msgs.Add(ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
            _view.DisplayMessages(msgs);
        }

        private IEnumerable<ValidationMessage> ProcessRefundsForMiscReceipts(IEnumerable<MiscReceipt> refundOrVoidMiscReceipts)
        {
            var msgs = new List<ValidationMessage>();
            var gateway = new Tenant(_view.ActiveUser.TenantId).GetPaymentGatewayService();

            foreach (var refundOrVoidReceipt in refundOrVoidMiscReceipts)
            {
                var transactionId = string.Empty;

                if (refundOrVoidReceipt.AmountPaid <= 0) continue;

                if (refundOrVoidReceipt.PaymentGatewayType != refundOrVoidReceipt.Tenant.PaymentGatewayType && refundOrVoidReceipt.PaymentGatewayType != PaymentGatewayType.NotApplicable && refundOrVoidReceipt.PaymentGatewayType != PaymentGatewayType.Check)
                {
                    msgs.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptErrorPaymentGatewayForTenantChanged, refundOrVoidReceipt.AmountPaid));
                    continue;
                }

                var amountLeftToApplyFromOriginalMiscReceipt = refundOrVoidReceipt.OriginalMiscReceipt.AmountLeftToBeAppliedOrRefunded(refundOrVoidReceipt.OriginalMiscReceipt.TenantId);

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    refundOrVoidReceipt.Connection = Connection;
                    refundOrVoidReceipt.Transaction = Transaction;

                    if (!_validator.IsValid(refundOrVoidReceipt))
                    {
                        msgs.AddRange(_validator.Messages);
                        RollBackTransaction();
                        continue;
                    }

                    refundOrVoidReceipt.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Misc Receipt Shipment #:{1} Load Order #:{2} Customer:{3} Amount:{4}",
                                            AuditLogConstants.AddedNew,
                                            refundOrVoidReceipt.ShipmentId != default(long)
                                                ? refundOrVoidReceipt.Shipment.ShipmentNumber
                                                : string.Empty,
                                            refundOrVoidReceipt.LoadOrderId != default(long)
                                                ? refundOrVoidReceipt.LoadOrder.LoadOrderNumber
                                                : string.Empty,
                                            refundOrVoidReceipt.Customer.CustomerNumber,
                                            refundOrVoidReceipt.AmountPaid),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = refundOrVoidReceipt.EntityName(),
                        EntityId = refundOrVoidReceipt.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    if (refundOrVoidReceipt.PaymentGatewayType == PaymentGatewayType.NotApplicable || refundOrVoidReceipt.PaymentGatewayType == PaymentGatewayType.Check)
                    {
                        CommitTransaction();
                        msgs.Add(ValidationMessage.Information(ProcessorVars.RefundSuccessfullyProcessedMessage));
                        continue;
                    }

                    switch (refundOrVoidReceipt.OriginalMiscReceipt.GetTransactionStatusViaPaymentGateway())
                    {
                        case TransactionStatus.CommunicationError:
                        case TransactionStatus.ApprovedReview:
                        case TransactionStatus.GeneralError:
                        case TransactionStatus.FailedReview:
                        case TransactionStatus.SettlementError:
                        case TransactionStatus.UnderReview:
                        case TransactionStatus.FdsPendingReview:
                        case TransactionStatus.FdsAuthorizedPendingReview:
                        case TransactionStatus.ReturnedItem:
                        case TransactionStatus.NotApplicable:
                            RollBackTransaction();
                            msgs.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionNotApplicable, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.Declined:
                            RollBackTransaction();
                            msgs.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionDeclined, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.Expired:
                            RollBackTransaction();
                            msgs.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionExpired, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.Voided:
                            RollBackTransaction();
                            msgs.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionVoided, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.CouldNotVoid:
                            RollBackTransaction();
                            msgs.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionCouldNotVoid, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.RefundSettledSuccessfully:
                            RollBackTransaction();
                            msgs.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionRefundSettledSuccessfully, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.RefundPendingSettlement:
                            RollBackTransaction();
                            msgs.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionRefundPendingSettlement, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.AuthorizedPendingCapture:
                        case TransactionStatus.CapturedPendingSettlement:
                            if (amountLeftToApplyFromOriginalMiscReceipt < refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid)
                            {
                                msgs.Add(ValidationMessage.Error(ProcessorVars.MiscReceiptCannotVoidPartiallyApplied, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid));
                                continue;
                            }
                            var voidResponse = gateway.Void(refundOrVoidReceipt.OriginalMiscReceipt.GatewayTransactionId);
                            if (voidResponse.Approved)
                            {
                                refundOrVoidReceipt.TakeSnapShot();
                                refundOrVoidReceipt.GatewayTransactionId = voidResponse.TransactionId;
                                foreach (var change in refundOrVoidReceipt.Changes())
                                    new AuditLog
                                    {
                                        Connection = Connection,
                                        Transaction = Transaction,
                                        Description = change,
                                        TenantId = _view.ActiveUser.TenantId,
                                        User = _view.ActiveUser,
                                        EntityCode = refundOrVoidReceipt.EntityName(),
                                        EntityId = refundOrVoidReceipt.Id.ToString(),
                                        LogDateTime = DateTime.Now
                                    }.Log();
                                refundOrVoidReceipt.Save();
                                CommitTransaction();
                                msgs.Add(ValidationMessage.Information(ProcessorVars.VoidSuccessfullyProcessedMessage));
                            }
                            else
                            {
                                RollBackTransaction();
                                msgs.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptErrorVoidingTransaction, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid, voidResponse.ErrorMessage));
                            }
                            break;
                        case TransactionStatus.SettledSuccessfully:
                            var refundResponse = gateway.Refund(refundOrVoidReceipt.OriginalMiscReceipt.GatewayTransactionId, refundOrVoidReceipt.AmountPaid);
                            if (refundResponse.Approved)
                            {
                                refundOrVoidReceipt.TakeSnapShot();
                                transactionId = refundResponse.TransactionId;
                                refundOrVoidReceipt.GatewayTransactionId = refundResponse.TransactionId;
                                foreach (var change in refundOrVoidReceipt.Changes())
                                    new AuditLog
                                    {
                                        Connection = Connection,
                                        Transaction = Transaction,
                                        Description = change,
                                        TenantId = _view.ActiveUser.TenantId,
                                        User = _view.ActiveUser,
                                        EntityCode = refundOrVoidReceipt.EntityName(),
                                        EntityId = refundOrVoidReceipt.Id.ToString(),
                                        LogDateTime = DateTime.Now
                                    }.Log();
                                refundOrVoidReceipt.Save();
                                CommitTransaction();
                                msgs.Add(ValidationMessage.Information(ProcessorVars.RefundSuccessfullyProcessedMessage));
                            }
                            else
                            {
                                RollBackTransaction();
                                msgs.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptErrorRefundingTransaction, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid, refundResponse.ErrorMessage));
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    var messages = new List<ValidationMessage>();
                    if (!string.IsNullOrEmpty(transactionId))
                    {
                        try
                        {
                            gateway.Void(transactionId);
                        }
                        catch (Exception gException)
                        {
                            messages.Add(ValidationMessage.Information(gException.Message));
                        }
                    }
                    RollBackTransaction();
                    messages.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
                    _view.LogException(ex);
                }
            }

            return msgs;
        }
    }
}
