﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;
using LogisticsPlus.Eship.Processor.Searches.Accounting;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class VendorDisputeFinderHandler : EntityBase
    {
        private readonly IVendorDisputeFinderView _view;

        public VendorDisputeFinderHandler(IVendorDisputeFinderView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
        }


        private void OnSearch(object sender, ViewEventArgs<VendorDisputeViewSearchCriteria> e)
        {
            var results = new VendorDisputeSearch().FetchVendorDisputes(e.Argument, _view.ActiveUser.TenantId);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

            _view.DisplaySearchResult(results);
        }
    }
}