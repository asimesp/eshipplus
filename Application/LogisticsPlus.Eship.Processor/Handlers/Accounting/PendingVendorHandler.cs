﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class PendingVendorHandler : EntityBase
	{
		private readonly IPendingVendorView _view;
		private readonly PendingVendorValidator _validator = new PendingVendorValidator();
		private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

		public PendingVendorHandler(IPendingVendorView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
		}

		private void OnDelete(object sender, ViewEventArgs<PendingVendor> e)
		{
			var pendingVendor = e.Argument;

			if (pendingVendor.IsNew)
			{
				_view.Set(pendingVendor);
				return;
			}

			// load collections
			pendingVendor.LoadCollections();
			foreach (var location in pendingVendor.Locations)
				location.LoadCollections();

			// obtain lock/check lock
			var locks = new List<Lock>();
			if (!ObtainLocksForDelete(locks, pendingVendor))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				pendingVendor.Connection = Connection;
				pendingVendor.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				foreach (var @lock in locks)
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} #{2}", AuditLogConstants.Delete, pendingVendor.Name, pendingVendor.VendorNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = pendingVendor.EntityName(),
					EntityId = pendingVendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				// handle collection deletes
				foreach (var location in pendingVendor.Locations)
				{
					location.Connection = Connection;
					location.Transaction = Transaction;

					foreach (var contact in location.Contacts)
					{
						contact.Connection = Connection;
						contact.Transaction = Transaction;

						contact.Delete();
					}

					location.Delete();
				}
				foreach (var service in pendingVendor.Services)
				{
					service.Connection = Connection;
					service.Transaction = Transaction;

					service.Delete();
				}
				foreach (var equipment in pendingVendor.Equipments)
				{
					equipment.Connection = Connection;
					equipment.Transaction = Transaction;

					equipment.Delete();
				}
				foreach (var insurance in pendingVendor.Insurances)
				{
					insurance.Connection = Connection;
					insurance.Transaction = Transaction;

					insurance.Delete();
				}
				foreach (var document in pendingVendor.Documents)
				{
					document.Connection = Connection;
					document.Transaction = Transaction;

					document.Delete();
				}

				// delete PendingVendor and lock
				pendingVendor.Delete();

				foreach (var @lock in locks)
					@lock.Delete();

				CommitTransaction();

				_view.Set(new PendingVendor());
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				foreach (var @lock in locks) @lock.Delete();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<PendingVendor> e)
		{
			var pendingVendor = e.Argument;

			// check locks
			if (!pendingVendor.IsNew)
			{
				var @lock = pendingVendor.RetrieveLock(_view.ActiveUser, pendingVendor.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// assign new numbers
			if (pendingVendor.IsNew)
			{
				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.VendorNumber, _view.ActiveUser);
				pendingVendor.VendorNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
			}
			foreach (var location in pendingVendor.Locations)
				if (location.IsNew)
				{
					var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.VendorLocationNumber, _view.ActiveUser);
					location.LocationNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
				}

			// retrieve db PendingVendor for collection comparisons ...
			var dbPendingVendor = new PendingVendor(pendingVendor.Id, false);
			dbPendingVendor.LoadCollections();
			foreach (var location in dbPendingVendor.Locations) location.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				pendingVendor.Connection = Connection;
				pendingVendor.Transaction = Transaction;

				// validate PendingVendor
				_validator.Messages.Clear();
				if (!_validator.IsValid(pendingVendor))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// audit log new - need PendingVendor id so had to wait till after save
				if (pendingVendor.IsNew)
				{
					// save
					pendingVendor.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, pendingVendor.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = pendingVendor.EntityName(),
						EntityId = pendingVendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (pendingVendor.HasChanges())
				{
					// audit log PendingVendor
					foreach (var change in pendingVendor.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = pendingVendor.EntityName(),
							EntityId = pendingVendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// save
					pendingVendor.Save();
				}

				// process collections
				ProcessLocations(pendingVendor, dbPendingVendor);
				ProcessPendingVendorServices(pendingVendor, dbPendingVendor.Services);
				ProcessPendingVendorEquipments(pendingVendor, dbPendingVendor.Equipments);
				ProcessInsurances(pendingVendor, dbPendingVendor);
				ProcessDocuments(pendingVendor, dbPendingVendor.Documents);

				Vendor vendor = null;
				if (_view.CreateAndSaveVendor)
				{
					vendor = pendingVendor.ToVendor();

					var vvalidator = new VendorValidator();
					if (vvalidator.DuplicateVendorDot(vendor) || vvalidator.DuplicateVendorMc(vendor) ||
					    vvalidator.DuplicateVendorNumber(vendor))
					{
						RollBackTransaction();
						_view.DisplayMessages(vvalidator.Messages);
						return;
					}

					// save vendor
					vendor.Save();

					// process vendor collections
					ProcessLocations(vendor);
					ProcessVendorServices(vendor);
					ProcessVendorEquipments(vendor);
					ProcessInsurances(vendor);

					if (pendingVendor.Documents.Any())
					{
						foreach (var doc in pendingVendor.Documents.Where(doc => !string.IsNullOrEmpty(doc.LocationPath)))
							vendor.Documents.Add(new VendorDocument
								{
									Vendor = vendor,
									Tenant = vendor.Tenant,
									Name = doc.Name,
									Description = doc.Description,
									DocumentTagId = doc.DocumentTagId,
									IsInternal = doc.IsInternal,
									LocationPath = _view.CopyFileForVendor(vendor, doc.LocationPath),
									Connection = Connection,
									Transaction = Transaction
								});
						ProcessDocuments(vendor);
					}
				}

				// commit transaction
				CommitTransaction();

				// return id in case of new
				_view.Set(pendingVendor);
				if (vendor != null) _view.Set(vendor);
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnUnLock(object sender, ViewEventArgs<PendingVendor> e)
		{
			var customer = e.Argument;
			var @lock = customer.RetrieveLock(_view.ActiveUser, customer.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<PendingVendor> e)
		{
			var customer = e.Argument;
			var @lock = customer.ObtainLock(_view.ActiveUser, customer.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<PendingVendor> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.BrokerReferenceTypes = ProcessorUtilities.GetAll<BrokerReferenceNumberType>();
		}

		private void ProcessPendingVendorEquipments(PendingVendor pendingVendor, IEnumerable<PendingVendorEquipment> dbEquipments)
		{
			var dbDelete = dbEquipments.Where(u => !pendingVendor.Equipments.Select(e => e.EquipmentTypeId).Contains(u.EquipmentTypeId));

			foreach (var e in dbDelete)
			{
				e.Connection = Connection;
				e.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Pending Vendor:{2} Equipment Id:{3}", AuditLogConstants.Delete, e.EntityName(), pendingVendor.Name,
									  e.EquipmentTypeId),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = pendingVendor.EntityName(),
					EntityId = pendingVendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				e.Delete();
			}

			foreach (var e in pendingVendor.Equipments)
				if (!_validator.PendingVendorEquipmentExists(e))
				{
					e.Connection = Connection;
					e.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Pending Vendor:{2} Equipment Id:{3}", AuditLogConstants.AddedNew, e.EntityName(),
													pendingVendor.Name, e.EquipmentTypeId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = pendingVendor.EntityName(),
						EntityId = pendingVendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					e.Save();
				}
		}

		private void ProcessPendingVendorServices(PendingVendor pendingVendor, IEnumerable<PendingVendorService> dbServices)
		{
			var dbDelete = dbServices.Where(u => !pendingVendor.Services.Select(s => s.ServiceId).Contains(u.ServiceId));

			foreach (var s in dbDelete)
			{
				s.Connection = Connection;
				s.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Pending Vendor:{2} Service Id:{3}", AuditLogConstants.Delete, s.EntityName(), pendingVendor.Name, s.ServiceId),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = pendingVendor.EntityName(),
					EntityId = pendingVendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				s.Delete();
			}

			foreach (var s in pendingVendor.Services)
				if (!_validator.PendingVendorServiceExists(s))
				{
					s.Connection = Connection;
					s.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Pending Vendor:{2} Service Id:{3}", AuditLogConstants.AddedNew, s.EntityName(),
													pendingVendor.Name, s.ServiceId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = pendingVendor.EntityName(),
						EntityId = pendingVendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					s.Save();
				}
		}

		private void ProcessLocations(PendingVendor pendingVendor, PendingVendor dbPendingVendor)
		{
			var dbDelete = dbPendingVendor.Locations.Where(l => !pendingVendor.Locations.Select(vl => vl.Id).Contains(l.Id));

			foreach (var l in dbDelete)
			{
				foreach (var c in l.Contacts)
				{
					c.Connection = Connection;
					c.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Pending Vendor:{2} Location #:{3} Contact Ref: {4}",
													AuditLogConstants.Delete, c.EntityName(), pendingVendor.Name, l.LocationNumber, c.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = pendingVendor.EntityName(),
						EntityId = pendingVendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					c.Delete();
				}

				l.Connection = Connection;
				l.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Pending Vendor:{2} Location #:{3}",
												AuditLogConstants.Delete, l.EntityName(), pendingVendor.Name, l.LocationNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = pendingVendor.EntityName(),
					EntityId = pendingVendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				l.Delete();
			}

			foreach (var l in pendingVendor.Locations)
			{
				l.Connection = Connection;
				l.Transaction = Transaction;

				if (l.IsNew)
				{
					l.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Pending Vendor:{2} Location #:{3}",
													AuditLogConstants.AddedNew, l.EntityName(), pendingVendor.Name, l.LocationNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = pendingVendor.EntityName(),
						EntityId = pendingVendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (l.HasChanges())
				{
					foreach (var change in l.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} #{1} {2}", l.EntityName(), l.LocationNumber, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = pendingVendor.EntityName(),
							EntityId = pendingVendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					l.Save();
				}

				ProcessContacts(pendingVendor, l, dbPendingVendor.Locations.FirstOrDefault(dbl => dbl.Id == l.Id));
			}
		}

		private void ProcessContacts(PendingVendor pendingVendor, PendingVendorLocation location, PendingVendorLocation dbLocation)
		{
			var dbDelete = dbLocation != null
							? dbLocation.Contacts.Where(l => !location.Contacts.Select(vl => vl.Id).Contains(l.Id))
							: new List<PendingVendorContact>();

			foreach (var c in dbDelete)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Pending Vendor:{2} Location #:{3} Contact Ref: {4}",
												AuditLogConstants.Delete, c.EntityName(), pendingVendor.Name, location.LocationNumber, c.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = pendingVendor.EntityName(),
					EntityId = pendingVendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				c.Delete();
			}

			foreach (var c in location.Contacts)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				if (c.IsNew)
				{
					c.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Pending Vendor:{2} Location #:{3} Contact Ref: {4}",
													AuditLogConstants.AddedNew, c.EntityName(), pendingVendor.Name, location.LocationNumber,
													c.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = pendingVendor.EntityName(),
						EntityId = pendingVendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (c.HasChanges())
				{
					foreach (var change in c.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("Location #{0} {1} {2}", location.LocationNumber, c.EntityName(), change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = pendingVendor.EntityName(),
							EntityId = pendingVendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					c.Save();
				}
			}
		}

		private void ProcessInsurances(PendingVendor pendingVendor, PendingVendor dbPendingVendor)
		{
			var dbDelete = dbPendingVendor.Insurances.Where(i => !pendingVendor.Insurances.Select(vi => vi.Id).Contains(i.Id));

			foreach (var i in dbDelete)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Pending Vendor:{2} Insurance Ref #:{3} Policy #: {4}",
												AuditLogConstants.Delete, i.EntityName(), pendingVendor.Name, i.Id, i.PolicyNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = pendingVendor.EntityName(),
					EntityId = pendingVendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				i.Delete();
			}

			foreach (var i in pendingVendor.Insurances)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				if (i.IsNew)
				{
					i.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Pending Vendor:{2} Insurance Ref #:{3} Policy #: {4}",
													AuditLogConstants.AddedNew, i.EntityName(), pendingVendor.Name, i.Id, i.PolicyNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = pendingVendor.EntityName(),
						EntityId = pendingVendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (i.HasChanges())
				{
					foreach (var change in i.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref #{1} {2}", i.EntityName(), i.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = pendingVendor.EntityName(),
							EntityId = pendingVendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					i.Save();
				}
			}
		}

		private void ProcessDocuments(PendingVendor pendingVendor, IEnumerable<PendingVendorDocument> dbDocuments)
		{
			var dbDelete = dbDocuments.Where(dbs => !pendingVendor.Documents.Select(s => s.Id).Contains(dbs.Id));

			foreach (var d in dbDelete)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Pending Vendor Number:{2} Document Ref#: {3}", AuditLogConstants.Delete, d.EntityName(),
									  pendingVendor.VendorNumber, d.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = pendingVendor.EntityName(),
					EntityId = pendingVendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				d.Delete();
			}

			foreach (var d in pendingVendor.Documents)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				if (d.IsNew)
				{
					d.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Pending Vendor Number:{2} Document Ref#:{3}",
													AuditLogConstants.AddedNew, d.EntityName(), pendingVendor.VendorNumber, d.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = pendingVendor.EntityName(),
						EntityId = pendingVendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (d.HasChanges())
				{
					foreach (var change in d.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Document Ref#:{1} {2}", d.EntityName(), d.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = pendingVendor.EntityName(),
							EntityId = pendingVendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					d.Save();
				}
			}
		}

		private bool ObtainLocksForDelete(ICollection<Lock> locks, PendingVendor pendingVendor)
		{
			var @lock = pendingVendor.ObtainLock(_view.ActiveUser, pendingVendor.Id);
			if (@lock != null)
			{
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					return false;
				}
				locks.Add(@lock);
			}

			foreach (var pendingVendorService in pendingVendor.Services)
			{
				@lock = pendingVendorService.Service.ObtainLock(_view.ActiveUser, pendingVendorService.Service.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					return false;
				}
				if (@lock != null) locks.Add(@lock);
			}

			foreach (var pendingVendorEquipment in pendingVendor.Equipments)
			{
				@lock = pendingVendorEquipment.EquipmentType.ObtainLock(_view.ActiveUser, pendingVendorEquipment.EquipmentType.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					return false;
				}
				if (@lock != null) locks.Add(@lock);
			}

			return true;
		}



		private void ProcessVendorEquipments(Vendor vendor)
		{
			foreach (var e in vendor.Equipments)
			{
				e.Connection = Connection;
				e.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor:{2} Equipment Id:{3}", AuditLogConstants.AddedNew, e.EntityName(),
													vendor.Name, e.EquipmentTypeId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				e.Save();
			}
		}

		private void ProcessVendorServices(Vendor vendor)
		{
			foreach (var s in vendor.Services)
			{
				s.Connection = Connection;
				s.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor:{2} Service Id:{3}", AuditLogConstants.AddedNew, s.EntityName(),
													vendor.Name, s.ServiceId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				s.Save();
			}
		}

		private void ProcessInsurances(Vendor vendor)
		{
			foreach (var i in vendor.Insurances)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				i.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor:{2} Insurance Ref #:{3} Policy #: {4}",
												AuditLogConstants.AddedNew, i.EntityName(), vendor.Name, i.Id, i.PolicyNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = vendor.EntityName(),
					EntityId = vendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessLocations(Vendor vendor)
		{

			foreach (var l in vendor.Locations)
			{
				l.Connection = Connection;
				l.Transaction = Transaction;

				l.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor:{2} Location #:{3}",
												AuditLogConstants.AddedNew, l.EntityName(), vendor.Name, l.LocationNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = vendor.EntityName(),
					EntityId = vendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();


				ProcessContacts(vendor, l);
			}
		}

		private void ProcessContacts(Vendor vendor, VendorLocation location)
		{
			foreach (var c in location.Contacts)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				c.Save();

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor:{2} Location #:{3} Contact Ref: {4}",
													AuditLogConstants.AddedNew, c.EntityName(), vendor.Name, location.LocationNumber,
													c.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
			}
		}

		private void ProcessDocuments(Vendor vendor)
		{
			foreach (var d in vendor.Documents)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				d.Save();
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor Number:{2} Document Ref#:{3}",
												AuditLogConstants.AddedNew, d.EntityName(), vendor.VendorNumber, d.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = vendor.EntityName(),
					EntityId = vendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			}
		}
	}
}
