﻿using System;
using System.Linq;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class VendorDisputeHandler : EntityBase
    {
        private readonly IVendorDisputeView _view;
        private readonly VendorDisputeValidator _validator = new VendorDisputeValidator();
        private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

        public VendorDisputeHandler(IVendorDisputeView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.Save += OnSave;
            _view.Resolve += OnResolve;
            _view.Delete += OnDelete;
			_view.LoadAuditLog += OnLoadAuditLog;
        }

        private void OnSave(object sender, ViewEventArgs<VendorDispute> e)
        {
            var vendorDispute = e.Argument;

            // check locks
            if (!vendorDispute.IsNew)
            {
                var @lock = vendorDispute.RetrieveLock(_view.ActiveUser, vendorDispute.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            var msgs = new List<ValidationMessage>();

            try
            {
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();

                // try to save VendorDispute and related VendorDisputeDetails
                var validationMessages = SaveVendorDispute(vendorDispute, msgs);
				if (validationMessages.Any())
				{
					RollBackTransaction();
					_view.DisplayMessages(validationMessages);
					return;
				}

                CommitTransaction();

                msgs.Add(ValidationMessage.Information(ProcessorVars.RecordSaveMsg));

                //update VendorDispute on UI
                _view.Set(vendorDispute);
                _view.DisplayMessages(msgs);
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<VendorDispute> e)
        {
            var vendorDispute = e.Argument;

            if (vendorDispute.IsNew)
            {
                _view.Set(new VendorDispute());
                return;
            }

            // load collection
            var vendorDisputeDetails = vendorDispute.VendorDisputeDetails;

            // obtain lock/check lock
            var @lock = vendorDispute.ObtainLock(_view.ActiveUser, vendorDispute.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                vendorDispute.Connection = Connection;
                vendorDispute.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _validator.Messages.Clear();

                foreach (var vendorDisputeDetail in vendorDisputeDetails)
                {
                    vendorDisputeDetail.Connection = Connection;
                    vendorDisputeDetail.Transaction = Transaction;

                    vendorDisputeDetail.Delete();
                }

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Disputer Reference Number: {1}", AuditLogConstants.Delete, vendorDispute.DisputerReferenceNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = vendorDispute.EntityName(),
                    EntityId = vendorDispute.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                // delete vendor dispute and lock
                vendorDispute.Delete();

                @lock.Delete();

                CommitTransaction();

                _view.Set(new VendorDispute());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnResolve(object sender, ViewEventArgs<VendorDispute> e)
        {
            var vendorDispute = e.Argument;

            // check locks
            if (!vendorDispute.IsNew)
            {
                var @lock = vendorDispute.RetrieveLock(_view.ActiveUser, vendorDispute.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            var msgs = new List<ValidationMessage>();

            try
            {
                Connection = DatabaseConnection.DefaultConnection;

                BeginTransaction();
				
				var validationMessages = SaveVendorDispute(vendorDispute, msgs);
				if (validationMessages.Any())
				{
					RollBackTransaction();
					_view.DisplayMessages(validationMessages);
					return;
				}

                var shipments = new HashSet<Shipment>(); // HashSet - because one Shipment has many ShipmentCharges
                var shipmentCharges = new List<ShipmentCharge>();

                var serviceTickets = new HashSet<ServiceTicket>();
                var serviceTicketCharges = new List<ServiceTicketCharge>();

                foreach (var vendorDisputeDetil in vendorDispute.VendorDisputeDetails)
                {
                    // check where VendorDisputeDetil attached - Shipments or ServiceTicket
                    if (vendorDisputeDetil.ChargeLineIdType == ChargeLineType.Shipment)
                    {
                        var shipmentCharge = new ShipmentCharge(vendorDisputeDetil.ChargeLineId);
                        shipmentCharges.Add(shipmentCharge);

                        if (shipmentCharge.Shipment.Id != 0 && !shipments.Any(f => f.Id == shipmentCharge.Shipment.Id))
                        {
                            shipments.Add(shipmentCharge.Shipment);
                        }
                    }
                    else
                    {
                        var serviceTicketCharge = new ServiceTicketCharge(vendorDisputeDetil.ChargeLineId);
                        serviceTicketCharges.Add(serviceTicketCharge);

                        if (serviceTicketCharge.ServiceTicket.Id != 0 && !serviceTickets.Any(f => f.Id == serviceTicketCharge.ServiceTicket.Id))
                        {
                            serviceTickets.Add(serviceTicketCharge.ServiceTicket);
                        }
                    }
                }

                var locks = new List<Lock>();
                var messages = ObtainLocksForSaveOrDelete(locks, shipments, serviceTickets); // locks collections

                // check for errors
                if (messages.Any())
                {
                    foreach (var l in locks) l.Delete();
                    _view.DisplayMessages(messages);
                    return;
                }

                foreach (var shipment in shipments)
                {
                    // find ShipmentCharges attachet do Shipment
                    var sCharges = shipment.Charges.Where(p => shipmentCharges.Any(emp => p.Id == emp.Id));

                    foreach (var shipmentCharge in sCharges)
                    {
                        shipmentCharge.TakeSnapShot();

                        // set to UnitBuy new value from RevisedChargeAmount
                        var oldUnitBuy = shipmentCharge.UnitBuy;
                        shipmentCharge.UnitBuy = vendorDispute.VendorDisputeDetails.Find(f => f.ChargeLineId == shipmentCharge.Id).RevisedChargeAmount;

                        foreach (var change in shipmentCharge.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("{0} {1} applied to ShipmentCharge: {2} ShipmentRef#: {3}",
                                          AuditLogConstants.Update, change,
                                          shipmentCharge.Id, shipment.ShipmentNumber),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = shipmentCharge.EntityName(),
                                EntityId = shipmentCharge.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("Dispute resolved for ShipmentCharge: {0} Old UnitBuy: {1} New UnitBuy: {2}",
                                      shipmentCharge.Id, oldUnitBuy, shipmentCharge.UnitBuy),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                        // save
                        shipmentCharge.Save();
                    }
                }

                foreach (var serviceTicket in serviceTickets)
                {
                    // find ServiceTicketCharges attachet do ServiceTicket
                    var stCharges = serviceTicket.Charges.Where(p => serviceTicketCharges.Any(emp => p.Id == emp.Id));

                    foreach (var serviceTicketCharge in stCharges)
                    {
                        serviceTicketCharge.TakeSnapShot();

                        // set to UnitBuy new value from RevisedChargeAmount
                        var oldUnitBuy = serviceTicketCharge.UnitBuy;
                        serviceTicketCharge.UnitBuy = vendorDispute.VendorDisputeDetails.Find(f => f.ChargeLineId == serviceTicketCharge.Id).RevisedChargeAmount;

                        foreach (var change in serviceTicketCharge.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("{0} {1} applied to ServiceTicketCharge: {2} ServiceTicket: {3}",
                                          AuditLogConstants.Update, change,
                                          serviceTicketCharge.Id, serviceTicket.ServiceTicketNumber),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = serviceTicketCharge.EntityName(),
                                EntityId = serviceTicketCharge.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("Dispute resolved for ServiceTicketCharge: {0} Old UnitBuy: {1} New UnitBuy: {2}",
                                      serviceTicketCharge.Id, oldUnitBuy, serviceTicketCharge.UnitBuy),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                        // save
                        serviceTicketCharge.Save();
                    }
                }

                foreach (var l in locks) l.Delete();

                // commit transaction
                CommitTransaction();

                msgs.Add(ValidationMessage.Information(ProcessorVars.RecordSaveMsg));

                //update VendorDispute on UI
                _view.Set(vendorDispute);
                _view.DisplayMessages(msgs);
				_view.OnVendorDisputeResolvedComplete(vendorDispute);
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private IEnumerable<ValidationMessage> SaveVendorDispute(VendorDispute vendorDispute, List<ValidationMessage> msgs)
        {
	        var validationMessages = new List<ValidationMessage>();
            // assign new numbers
            if (vendorDispute.IsNew)
            {
                var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.VendorDisputeNumber, _view.ActiveUser);
                vendorDispute.DisputerReferenceNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
            }

            // link up
            _validator.Connection = Connection;
            _validator.Transaction = Transaction;

            vendorDispute.Connection = Connection;
            vendorDispute.Transaction = Transaction;

            // validate VendorDispute
            _validator.Messages.Clear();
            if (!_validator.IsValid(vendorDispute))
            {
                RollBackTransaction();
                validationMessages = _validator.Messages;
				return validationMessages;
            }
            if (_validator.Messages.Any()) msgs.AddRange(_validator.Messages); // for warning messages

            // audit log VendorDispute
            if (vendorDispute.IsNew)
            {
                // save
                vendorDispute.Save();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} VendorDispute: {1}", AuditLogConstants.AddedNew, vendorDispute.DisputerReferenceNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = vendorDispute.EntityName(),
                    EntityId = vendorDispute.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
            }
            else if (vendorDispute.HasChanges())
            {
                foreach (var change in vendorDispute.Changes())
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = change,
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = vendorDispute.EntityName(),
                        EntityId = vendorDispute.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                // save
                vendorDispute.Save();
            }

	        ProcessVendorDisputeDetails(vendorDispute);
			return validationMessages;
        }

		private void OnLoadAuditLog(object sender, ViewEventArgs<VendorDispute> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}

        private IEnumerable<ValidationMessage> ObtainLocksForSaveOrDelete(List<Lock> locks, HashSet<Shipment> shipments, HashSet<ServiceTicket> serviceTickets)
        {
            var messages = new List<ValidationMessage>();

            foreach (var shipment in shipments)
            {
                var shipmentLock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);

                if (!shipmentLock.IsUserLock(_view.ActiveUser))
                {
                    messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
                }
                else
                {
                    locks.Add(shipmentLock);
                }
            }

            foreach (var serviceTicket in serviceTickets)
            {
                var serviceTicketLock = serviceTicket.ObtainLock(_view.ActiveUser, serviceTicket.Id);

                if (!serviceTicketLock.IsUserLock(_view.ActiveUser))
                {
                    messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
                }
                else
                {
                    locks.Add(serviceTicketLock);
                }
            }

            return messages;
        }

        private void ProcessVendorDisputeDetails(VendorDispute vendorDispute)
        {
            foreach (var vendorDisputeDetail in vendorDispute.VendorDisputeDetails)
            {
				
                vendorDisputeDetail.Connection = Connection;
                vendorDisputeDetail.Transaction = Transaction;

                vendorDisputeDetail.DisputedId = vendorDispute.Id;

                foreach (var change in vendorDisputeDetail.Changes())
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = change,
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = vendorDisputeDetail.EntityName(),
                        EntityId = vendorDisputeDetail.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                // save
                vendorDisputeDetail.Save();
            }
	        
        }

        private void OnUnLock(object sender, ViewEventArgs<VendorDispute> e)
        {
            var request = e.Argument;
            var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }
            @lock.Delete();
        }

        private void OnLock(object sender, ViewEventArgs<VendorDispute> e)
        {
            var request = e.Argument;
            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }
    }
}