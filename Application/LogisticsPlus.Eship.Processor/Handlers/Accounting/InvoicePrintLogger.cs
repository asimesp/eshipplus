﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class InvoicePrintLogger : EntityBase
	{
		public bool LogPrint(Invoice invoice, User activeUser, out Exception ex)
		{
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				new InvoicePrintLog
					{
						Connection = Connection,
						Transaction = Transaction,
						TenantId = activeUser.TenantId,
						Invoice = invoice,
						LogDate = DateTime.Now,
						TenantEmployee = activeUser.TenantEmployee,
						UserDefaultCustomerName = activeUser.DefaultCustomer.Name,
						UserDefaultCustomerNumber = activeUser.DefaultCustomer.CustomerNumber,
						UserFirstName = activeUser.FirstName,
						UserLastName = activeUser.LastName,
						UserLogon = activeUser.Username
					}.Log();

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("Invoice Printed: {0}", invoice.InvoiceNumber),
						TenantId = activeUser.TenantId,
						User = activeUser,
						EntityCode = invoice.EntityName(),
						EntityId = invoice.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				CommitTransaction();
				ex = null;
				return true;
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
				return false;
			}
		}
	}
}
