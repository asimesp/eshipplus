﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class InvoiceHandler : EntityBase
	{
		private readonly IInvoiceView _view;
		private readonly InvoiceValidator _validator = new InvoiceValidator();
        private readonly MiscReceiptValidator _mrValidator = new MiscReceiptValidator();
		private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

		public InvoiceHandler(IInvoiceView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.PrefixSearch += OnPrefixSearch;
			_view.CustomerSearch += OnCustomerSearch;
		    _view.LoadAndLockMiscReceiptsApplicableToPostedInvoice += OnLoadAndLockMiscReceiptsApplicableToPostedInvoice;
		    _view.SaveMiscReceiptApplicationsAndInvoice += OnSaveMiscReceiptApplicationsAndInvoice;
		    _view.UnlockInvoiceAndMiscReceipts += OnUnlockInvoiceAndMiscReceipts;
		}
        

	    private void OnSave(object sender, ViewEventArgs<Invoice> e)
		{
			var invoice = e.Argument;

			// check locks
			if (!invoice.IsNew)
			{
				var @lock = invoice.RetrieveLock(_view.ActiveUser, invoice.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// grab shipments/service ticket locks
			var shipments = new List<Shipment>();
			var serviceTickets = new List<ServiceTicket>();
			var locks = new List<Lock>(); // locks collections
			var messages = ObtainLocksForSaveOrDelete(locks, invoice, shipments, serviceTickets).ToList();

			if (invoice.InvoiceType == InvoiceType.Invoice)
			{
				const string notInvoiceable = "Can not process {0} [{1}] because status is {2}.";
				const string notAudited = "Can not process {0} [{1}] because it is not marked as audited for invoicing.";

				var snumbers = invoice.Details
					.Where(d => d.ReferenceType == DetailReferenceType.Shipment && d.IsNew)
					.Select(d => d.ReferenceNumber)
					.Distinct();
				messages.AddRange(
					shipments
						.Where(s => snumbers.Contains(s.ShipmentNumber) && s.Status == ShipmentStatus.Invoiced)
						.Select(s => ValidationMessage.Error(notInvoiceable, "Shipment", s.ShipmentNumber, s.Status.FormattedString())));
				messages.AddRange(shipments
									.Where(s => snumbers.Contains(s.ShipmentNumber) && !s.AuditedForInvoicing)
									.Select(s => ValidationMessage.Error(notAudited, "Shipment", s.ShipmentNumber)));

				var tnumbers = invoice.Details
					.Where(d => d.ReferenceType == DetailReferenceType.ServiceTicket && d.IsNew)
					.Select(d => d.ReferenceNumber)
					.Distinct();
				messages.AddRange(
					serviceTickets
						.Where(t => tnumbers.Contains(t.ServiceTicketNumber) && t.Status == ServiceTicketStatus.Invoiced)
						.Select(
							t => ValidationMessage.Error(notInvoiceable, "Service Ticket", t.ServiceTicketNumber, t.Status.FormattedString())));
				messages.AddRange(
					serviceTickets
						.Where(t => tnumbers.Contains(t.ServiceTicketNumber) && !t.AuditedForInvoicing)
						.Select(t => ValidationMessage.Error(notAudited, "Service Ticket", t.ServiceTicketNumber)));
			}
			else
			{
				const string notInvoiced = "Cannot process {0} [{1}] because status is not Invoiced.";
				var snumbers = invoice.Details
					.Where(d => d.ReferenceType == DetailReferenceType.Shipment && d.IsNew)
					.Select(d => d.ReferenceNumber)
					.Distinct();
				messages.AddRange(
					shipments
						.Where(s => snumbers.Contains(s.ShipmentNumber) && s.Status != ShipmentStatus.Invoiced)
						.Select(s => ValidationMessage.Error(notInvoiced, "Shipment", s.ShipmentNumber)));

				var tnumbers = invoice.Details
					.Where(d => d.ReferenceType == DetailReferenceType.ServiceTicket && d.IsNew)
					.Select(d => d.ReferenceNumber)
					.Distinct();
				messages.AddRange(
					serviceTickets
						.Where(t => tnumbers.Contains(t.ServiceTicketNumber) && t.Status != ServiceTicketStatus.Invoiced)
						.Select(t => ValidationMessage.Error(notInvoiced, "Service Ticket", t.ServiceTicketNumber)));


				if (invoice.OriginalInvoice != null)
				{
					const string notPresentOnOrgInv = "Cannot process {0} [{1}] because it is not on the original invoice [{2}]";
					var orgInvoice = invoice.OriginalInvoice;
					messages.AddRange(snumbers
							.Select(n => new
										{
											Number = n,
											Present = orgInvoice.Details.Any(d => d.ReferenceType == DetailReferenceType.Shipment && d.ReferenceNumber == n)
										})
							.Where(i => !i.Present)
							.Select(i => ValidationMessage.Error(notPresentOnOrgInv, "Shipment", i.Number, orgInvoice.InvoiceNumber))
						);
					messages.AddRange(tnumbers
							.Select(n => new
										{
											Number = n,
											Present = orgInvoice.Details.Any(d => d.ReferenceType == DetailReferenceType.ServiceTicket && d.ReferenceNumber == n)
										})
							.Where(i => !i.Present)
							.Select(i => ValidationMessage.Error(notPresentOnOrgInv, "Service Ticket", i.Number, orgInvoice.InvoiceNumber))
						);
				}
			}

			if (messages.Any())
			{
				foreach (var l in locks) l.Delete();
				_view.DisplayMessages(messages);
				return;
			}

			// assign new numbers
			if (invoice.IsNew)
			{
				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.InvoiceNumber, _view.ActiveUser);
				invoice.InvoiceNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
				invoice.AppendNumberSuffix();
			}

			// retrieve db invoice for collection comparisons ...
			var dbInvoice = new Invoice(invoice.Id, false);
			dbInvoice.LoadCollections();

			// check for shipments and/or service tickets to uninvoice
			var shipmentsToUninvoice = new List<Shipment>();
			var ticketsToUninvoice = new List<ServiceTicket>();

			if (invoice.InvoiceType == InvoiceType.Invoice)
				messages = ObtainLocksForUninvoicing(locks, invoice, dbInvoice.Details, shipmentsToUninvoice, ticketsToUninvoice).ToList();

			if (messages.Any())
			{
				foreach (var l in locks) l.Delete();
				_view.DisplayMessages(messages);
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				foreach (var l in locks)
				{
					l.Connection = Connection;
					l.Transaction = Transaction;
				}

				invoice.Connection = Connection;
				invoice.Transaction = Transaction;

				// validate invoice
				_validator.Messages.Clear();
				if (!_validator.IsValid(invoice))
				{
					RollBackTransaction();
					foreach (var l in locks) l.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}
                   
				// test customer invoice process requirements for shipments
				_validator.Messages.Clear();
				if (!dbInvoice.Posted && invoice.Posted && !_validator.MeetsInvoicingRequirement(invoice))
				{
					RollBackTransaction();
					foreach (var l in locks) l.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

                _validator.Messages.Clear();
                if (!_validator.MeetsInvoicingRequirement(invoice, shipments))
                {
                    RollBackTransaction();
                    foreach (var l in locks) l.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

				// audit log invoice
				if (invoice.IsNew)
				{
					// save
					invoice.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Invoice: {1}", AuditLogConstants.AddedNew, invoice.InvoiceNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = invoice.EntityName(),
						EntityId = invoice.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (invoice.HasChanges())
				{
					foreach (var change in invoice.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = invoice.EntityName(),
							EntityId = invoice.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// save
					invoice.Save();
				}

				// process rest of collections
				ProcessInvoiceDetails(invoice, dbInvoice.Details);

				// save shipments/service tickets with invoiced status
				SetInvoicesStatus(serviceTickets, shipments);

                
				// uninvoice affected detail items (shipments/service tickets removed)
				if (shipmentsToUninvoice.Any()) ProcessShipmentUninvoicing(shipmentsToUninvoice);
				if (ticketsToUninvoice.Any()) ProcessServiceTicketUninvoicing(ticketsToUninvoice);

				// release locks
				foreach (var l in locks) l.Delete();

				// commit transaction
				CommitTransaction();

				_view.Set(invoice);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				foreach (var l in locks) l.Delete();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDelete(object sender, ViewEventArgs<Invoice> e)
		{
			var invoice = e.Argument;

			if (invoice.IsNew)
			{
				_view.Set(new Invoice());
				return;
			}

			// load collections
			invoice.LoadCollections();
			var printLogs = invoice.RetrieveInvoicePrintLogs(); // print logs

			// obtain lock/check lock
			var shipments = new List<Shipment>();
			var serviceTickets = new List<ServiceTicket>();

			var locks = new List<Lock>();
			var messages = ObtainLocksForSaveOrDelete(locks, invoice, shipments, serviceTickets);

			if (messages.Any())
			{
				_view.DisplayMessages(messages);
				return;
			}

			var @lock = invoice.ObtainLock(_view.ActiveUser, invoice.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}
			locks.Add(@lock);



			// process delete
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				invoice.Connection = Connection;
				invoice.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				foreach (var l in locks)
				{
					l.Connection = Connection;
					l.Transaction = Transaction;
				}

				_validator.Messages.Clear();
				if (!_validator.CanDeleteInvoice(invoice))
				{
					RollBackTransaction();
					foreach (var l in locks) l.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} Invoice Number: {1}", AuditLogConstants.Delete, invoice.InvoiceNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = invoice.EntityName(),
					EntityId = invoice.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();


				// uninvoice affected shipments/service tickets
				if (invoice.InvoiceType == InvoiceType.Invoice)
				{
					ProcessShipmentUninvoicing(shipments);
					ProcessServiceTicketUninvoicing(serviceTickets);
				}


				// handle collection deletes
				foreach (var detail in invoice.Details)
				{
					detail.Connection = Connection;
					detail.Transaction = Transaction;

					detail.Delete();
				}

				// handle print logs deletes
				foreach (var log in printLogs)
				{
					log.Connection = Connection;
					log.Transaction = Transaction;

					log.Delete();
				}

				// delete invoice
				invoice.Delete();

				// delete locks
				foreach (var l in locks) l.Delete();

				CommitTransaction();

				_view.Set(new Invoice
							{
								InvoiceDate = DateTime.Now,
								DueDate = DateUtility.SystemEarliestDateTime,
								PostDate = DateUtility.SystemEarliestDateTime,
								ExportDate = DateUtility.SystemEarliestDateTime,
								Details = new List<InvoiceDetail>(),
							});

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				foreach (var l in locks) l.Delete();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}



		private void OnPrefixSearch(object sender, ViewEventArgs<string> e)
		{
			var prefix = new PrefixSearch().FetchPrefixByCode(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplayPrefix(prefix == null || !prefix.Active ? new Prefix() : prefix);
			if (prefix == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Prefix not found") });
			else if (!prefix.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Prefix is not active") });
		}

		private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
		{
			if (e.Argument == string.Empty)
			{
				_view.DisplayCustomer(null);
				return;
			}

			if (!_view.ActiveUser.TenantEmployee)
				FetchCustomerForNonEmployee(e.Argument);
			else
				FetchCustomerForEmployee(e.Argument);
		}



		private void OnUnLock(object sender, ViewEventArgs<Invoice> e)
		{
			var request = e.Argument;
			var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<Invoice> e)
		{
			var request = e.Argument;
			var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

        private void OnLoadAuditLog(object sender, ViewEventArgs<Invoice> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.InvoiceDetailReferenceType = ProcessorUtilities.GetAll<DetailReferenceType>();
		}



		private void FetchCustomerForNonEmployee(string customerNumber)
		{
			if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
			{
				_view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
				return;
			}
			var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
			_view.DisplayCustomer(shipAs == null || !shipAs.Customer.Active ? null : shipAs.Customer);
			if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else if (!shipAs.Customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
		}

		private void FetchCustomerForEmployee(string customerNumber)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

			_view.DisplayCustomer(customer == null || !customer.Active ? new Customer() : customer);
			if (customer == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else if (!customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
		}



		private void ProcessInvoiceDetails(Invoice invoice, IEnumerable<InvoiceDetail> dbDetails)
		{
			var dbDelete = dbDetails.Where(dbi => !invoice.Details.Select(i => i.Id).Contains(dbi.Id));

			foreach (var i in dbDelete)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Invoice Number:{2} Detail Ref#:{3}",
												AuditLogConstants.Delete, i.EntityName(), invoice.InvoiceNumber, i.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = invoice.EntityName(),
					EntityId = invoice.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				i.Delete();
			}

			foreach (var i in invoice.Details)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				if (i.IsNew)
				{
					i.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Invoice Number:{2} Detail Ref#:{3}",
													AuditLogConstants.AddedNew, i.EntityName(), invoice.InvoiceNumber, i.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = invoice.EntityName(),
						EntityId = invoice.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (i.HasChanges())
				{
					foreach (var change in i.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Detail Ref#:{1} {2}", i.EntityName(), i.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = invoice.EntityName(),
							EntityId = invoice.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					i.Save();
				}
			}
		}

		private IEnumerable<ValidationMessage> ObtainLocksForSaveOrDelete(ICollection<Lock> locks, Invoice invoice, ICollection<Shipment> shipments,
			ICollection<ServiceTicket> serviceTickets)
		{
			var messages = new List<ValidationMessage>();

			foreach (var detail in invoice.Details)
				switch (detail.ReferenceType)
				{
					case DetailReferenceType.Shipment:
						var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(detail.ReferenceNumber, detail.TenantId);

						if (shipment == null)
						{
							messages.Add(ValidationMessage.Error(string.Format("Invoice Detail Reference Number {0} is invalid", detail.ReferenceNumber)));
							continue;
						}
						var shipmentLock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);

						if (!shipmentLock.IsUserLock(_view.ActiveUser))
							messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
						else
						{
							locks.Add(shipmentLock);
							if (shipments.All(s => s.Id != shipment.Id)) shipments.Add(shipment);
						}
						break;
					case DetailReferenceType.ServiceTicket:
						var serviceTicket = new ServiceTicketSearch().FetchServiceTicketByServiceTicketNumber(detail.ReferenceNumber, detail.TenantId);

						if (serviceTicket == null)
						{
							messages.Add(ValidationMessage.Error(string.Format("Invoice Detail Reference Number {0} is invalid", detail.ReferenceNumber)));
							continue;
						}

						var serviceTicketLock = serviceTicket.ObtainLock(_view.ActiveUser, serviceTicket.Id);
						if (!serviceTicketLock.IsUserLock(_view.ActiveUser))
							messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
						else
						{
							locks.Add(serviceTicketLock);
							if (serviceTickets.All(s => s.Id != serviceTicket.Id)) serviceTickets.Add(serviceTicket);
						}
						break;
				}


			return messages;
		}

		private IEnumerable<ValidationMessage> ObtainLocksForUninvoicing(ICollection<Lock> locks, Invoice invoice, IEnumerable<InvoiceDetail> dbDetails,
			List<Shipment> shipments, List<ServiceTicket> serviceTickets)
		{
			var messages = new List<ValidationMessage>();

			var currentIds = new List<long>();
			var currentShipments = new List<string>();
			var currentTickets = new List<string>();

			foreach (var detail in invoice.Details)
			{
				currentIds.Add(detail.Id);

				switch (detail.ReferenceType)
				{
					case DetailReferenceType.Shipment:
						if (!currentShipments.Contains(detail.ReferenceNumber)) currentShipments.Add(detail.ReferenceNumber);
						break;
					case DetailReferenceType.ServiceTicket:
						if (!currentTickets.Contains(detail.ReferenceNumber)) currentTickets.Add(detail.ReferenceNumber);
						break;
				}
			}
			var dbDelete = dbDetails.Where(dbi => !currentIds.Contains(dbi.Id));

			var shipmentSearch = new ShipmentSearch();
			shipments.AddRange(
				dbDelete.Where(d => d.ReferenceType == DetailReferenceType.Shipment && !currentShipments.Contains(d.ReferenceNumber))
					.Select(d => d.ReferenceNumber)
					.Distinct()
					.Select(n => shipmentSearch.FetchShipmentByShipmentNumber(n, invoice.TenantId))
					.Where(s => s != null)
					.ToList());

			var ticketSearch = new ServiceTicketSearch();
			serviceTickets.AddRange(
				dbDelete.Where(d => d.ReferenceType == DetailReferenceType.ServiceTicket && !currentTickets.Contains(d.ReferenceNumber))
					.Select(d => d.ReferenceNumber)
					.Distinct()
					.Select(n => ticketSearch.FetchServiceTicketByServiceTicketNumber(n, invoice.TenantId))
					.Where(s => s != null)
					.ToList());

			foreach (var shipment in shipments)
			{
				var @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
					messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
				else
				{
					locks.Add(@lock);
				}
			}
			foreach (var ticket in serviceTickets)
			{
				var @lock = ticket.ObtainLock(_view.ActiveUser, ticket.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
					messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
				else
				{
					locks.Add(@lock);
				}
			}

			return messages;
		}




		private void SetInvoicesStatus(IEnumerable<ServiceTicket> serviceTickets, IEnumerable<Shipment> shipments)
		{
			foreach (var shipment in shipments.Where(s => s.Status != ShipmentStatus.Invoiced))
			{
				shipment.TakeSnapShot();
				shipment.Status = ShipmentStatus.Invoiced;
				foreach (var change in shipment.Changes())
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				shipment.Save();
			}
			foreach (var serviceTicket in serviceTickets.Where(s => s.Status != ServiceTicketStatus.Invoiced))
			{
				serviceTicket.TakeSnapShot();
				serviceTicket.Status = ServiceTicketStatus.Invoiced;
				foreach (var change in serviceTicket.Changes())
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = serviceTicket.EntityName(),
							EntityId = serviceTicket.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				serviceTicket.Save();
			}
		}

		private void ProcessShipmentUninvoicing(IEnumerable<Shipment> shipmentsToUninvoice)
		{
			foreach (var shipment in shipmentsToUninvoice)
			{
				shipment.TakeSnapShot();

				shipment.Status = shipment.InferShipmentStatus(true);

				foreach (var change in shipment.Changes())
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				shipment.Save();
			}
		}

		private void ProcessServiceTicketUninvoicing(IEnumerable<ServiceTicket> tickets)
		{
			foreach (var ticket in tickets)
			{
				ticket.TakeSnapShot();

				ticket.Status = ServiceTicketStatus.Open;

				foreach (var change in ticket.Changes())
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = change,
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = ticket.EntityName(),
						EntityId = ticket.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				ticket.Save();
			}
		}


        private void OnSaveMiscReceiptApplicationsAndInvoice(object sender, ViewEventArgs<Invoice, List<MiscReceiptApplication>> e)
        {
            var invoice = e.Argument;
            var applications = e.Argument2;

            if (invoice.IsNew || !invoice.Posted)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot modify misc receipt applications to unsaved or unposted invoice") });
                return;
            }

            if (applications.Any(mra => mra.IsNew))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot create new misc receipt applications when updating amount paid") });
                return;
            }

            // validate if the modified misc receipt applications are greater than the misc receipt's paid amount
            var miscReceipts = new List<MiscReceipt>();
            foreach (var application in applications)
            {
                if (miscReceipts.Any(mr => mr.Id == application.MiscReceiptId)) continue;
                
                application.MiscReceipt.LoadCollections();
                miscReceipts.Add(application.MiscReceipt);
            }
            var msgs = new List<ValidationMessage>();
            foreach (var miscReceipt in miscReceipts)
            {
                for (var i = 0; i < miscReceipt.MiscReceiptApplications.Count; i++)
                    miscReceipt.MiscReceiptApplications[i] = applications.FirstOrDefault(a => a.Id == miscReceipt.MiscReceiptApplications[i].Id) ?? miscReceipt.MiscReceiptApplications[i];

                if (miscReceipt.MiscReceiptApplications.Sum(mra => mra.Amount) > miscReceipt.AmountPaid)
                    msgs.Add(ValidationMessage.Error("Cannot apply an amount greater than misc receipt's amount paid of: {0} to invoice for misc receipt on shipment {1}", miscReceipt.AmountPaid, miscReceipt.Shipment != null ? miscReceipt.Shipment.ShipmentNumber : string.Empty));
            }

            if (msgs.Any())
            {
                RollBackTransaction();
                _view.DisplayMessages(msgs);
                return;
            }

            // check that all misc receipt and invoice locks are the user's 
            var locks = new List<Lock>();
            foreach (var application in applications.Where(a => !a.IsNew))
            {
                var receiptLock = application.MiscReceipt.RetrieveLock(_view.ActiveUser, application.MiscReceiptId);
                if (!receiptLock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
                locks.Add(receiptLock);

                var appLock = application.RetrieveLock(_view.ActiveUser, application.Id);
                if (!appLock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
                locks.Add(appLock);
            }

            var @lock = invoice.RetrieveLock(_view.ActiveUser, invoice.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }
            locks.Add(@lock);

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                invoice.Connection = Connection;
                invoice.Transaction = Transaction;

                foreach (var l in locks)
                {
                    l.Connection = Connection;
                    l.Transaction = Transaction;
                }

                foreach (var application in applications)
                {
                    application.Connection = Connection;
                    application.Transaction = Transaction;
                }

                _validator.Messages.Clear();
                if (!_validator.IsValid(invoice))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (!_mrValidator.MiscReceiptApplicationsAreValid(applications.Where(a => a.Amount != 0)))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_mrValidator.Messages);
                    return;
                }
               
                foreach (var application in applications)
                {
                    if (application.Amount == 0)
                    {
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} applied to Invoice: {2} for Misc Receipt with Gateway Transaction Id: {3}", AuditLogConstants.Delete, application.EntityName(),
                                                application.Invoice.InvoiceNumber, application.MiscReceipt.GatewayTransactionId),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = application.EntityName(),
                            EntityId = application.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                        application.Delete();
                    }
                    else if (application.HasChanges())
				    {
                        foreach (var change in application.Changes())
						    new AuditLog
						    {
							    Connection = Connection,
							    Transaction = Transaction,
							    Description = change,
							    TenantId = _view.ActiveUser.TenantId,
							    User = _view.ActiveUser,
                                EntityCode = application.EntityName(),
                                EntityId = application.Id.ToString(),
							    LogDateTime = DateTime.Now
						    }.Log();
                        application.Save();
				    }
                }

                if (invoice.HasChanges())
                {
                    foreach (var change in invoice.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = invoice.EntityName(),
                            EntityId = invoice.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    invoice.Save();
                }

                foreach (var l in locks) l.Delete();

                CommitTransaction();

                _view.Set(invoice);
                _view.DisplayMessages(new[] { ValidationMessage.Information("Invoice amount paid updated") });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnLoadAndLockMiscReceiptsApplicableToPostedInvoice(object sender, ViewEventArgs<long> e)
        {
            var miscReceipts = new MiscReceiptSearch().FetchMiscReceiptsApplicableToPostedInvoice(e.Argument, _view.ActiveUser.TenantId);
            foreach (var miscReceipt in miscReceipts)
                miscReceipt.MiscReceiptApplications = miscReceipt.MiscReceiptApplications.Where(mra => mra.InvoiceId == e.Argument).ToList();
            miscReceipts = miscReceipts.Where(mr => mr.MiscReceiptApplications.Any()).ToList();

            var invoice = new Invoice(e.Argument);
            var invoiceLock = invoice.ObtainLock(_view.ActiveUser, invoice.Id);
            if (!invoiceLock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(invoiceLock);
                _view.FailedMiscReceiptLocks();
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }
            var locks = new List<Lock> {invoiceLock};

            var msgs = new List<ValidationMessage>();
            foreach (var receipt in miscReceipts.Select(mr => new MiscReceipt(mr.Id)))
            {
                var receiptLock = receipt.ObtainLock(_view.ActiveUser, receipt.Id);
                if (!receiptLock.IsUserLock(_view.ActiveUser))
                {
                    msgs.Add(ValidationMessage.Error("Misc Receipt with Gateway Transaction Id:{0} - {1}", receipt.GatewayTransactionId, ProcessorVars.UnableToObtainLockErrMsg));
                    continue;
                }
                if (receiptLock.LockIsExpired()) receiptLock.Refresh();
                locks.Add(receiptLock);

                foreach (var application in receipt.MiscReceiptApplications)
                {
                    var appLock = application.ObtainLock(_view.ActiveUser, application.Id);
                    if (!appLock.IsUserLock(_view.ActiveUser))
                    {
                        msgs.Add(ValidationMessage.Error("Misc Receipt Application with Gateway Transaction Id:{0} - {1}", receipt.GatewayTransactionId, ProcessorVars.UnableToObtainLockErrMsg));
                        continue;
                    }
                    if (appLock.LockIsExpired()) appLock.Refresh();
                    locks.Add(appLock);
                }
            }

            if (msgs.Any())
            {
                foreach (var l in locks) l.Delete();
                _view.FailedMiscReceiptLocks();
                _view.DisplayMessages(msgs);
                return;
            }
            
            _view.DisplayMiscReceipts(miscReceipts);
        }

        private void OnUnlockInvoiceAndMiscReceipts(object sender, ViewEventArgs<Invoice, List<MiscReceipt>> e)
        {
            var invoice = e.Argument;
            var miscReceipts = e.Argument2;

            var @lock = invoice.RetrieveLock(_view.ActiveUser, invoice.Id);
            if (@lock != null && @lock.IsUserLock(_view.ActiveUser))
                @lock.Delete();

            foreach (var miscReceipt in miscReceipts)
            {
                var miscReceiptLock = miscReceipt.RetrieveLock(_view.ActiveUser, miscReceipt.Id);
                if (miscReceiptLock != null && miscReceiptLock.IsUserLock(_view.ActiveUser))
                    miscReceiptLock.Delete();

                foreach (var application in miscReceipt.MiscReceiptApplications)
                {
                    var appLock = application.RetrieveLock(_view.ActiveUser, application.Id);
                    if (appLock != null && appLock.IsUserLock(_view.ActiveUser))
                        appLock.Delete();
                }
            }
        }
	}
}
