﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class CustomerPaymentProfileManagementHandler
    {
        private readonly ICustomerPaymentProfileManagerView _view;

        public CustomerPaymentProfileManagementHandler(ICustomerPaymentProfileManagerView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.CustomerSearch += OnCustomerSearch;
            
        }


        private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
        {
            if (!_view.ActiveUser.TenantEmployee)
                FetchCustomerForNonEmployee(e.Argument);
            else
                FetchCustomerForEmployee(e.Argument);
        }

        private void FetchCustomerForNonEmployee(string customerNumber)
        {
            if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
            {
                _view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
                return;
            }

            var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
            if (shipAs == null)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
                _view.DisplayCustomer(new Customer());
            }
            else _view.DisplayCustomer(shipAs.Customer);
        }

        private void FetchCustomerForEmployee(string customerNumber)
        {
            var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

            _view.DisplayCustomer(customer);
            if (customer == null)
                _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
        }
    }
}
