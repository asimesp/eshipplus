﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class CommissionsToBePaidHandler : EntityBase
	{
		private readonly ICommissionsToBePaidView _view;
		private readonly CommissionsToBePaidValidator _validator = new CommissionsToBePaidValidator();

		public CommissionsToBePaidHandler(ICommissionsToBePaidView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
			_view.Save += OnSave;
		    _view.BatchImport += OnImport;
		}

	    private void OnImport(object sender, ViewEventArgs<List<CommissionPayment>> e)
	    {
            var commissions = e.Argument;
	        var messages = new ValidationMessageCollection();

            foreach (var commission in commissions)
            {
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    _validator.Messages.Clear();
                    if (!_validator.IsValid(commission))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }

                    // audit log invoice
                    if (commission.IsNew)
                    {
                        commission.Connection = Connection;
                        commission.Transaction = Transaction;

                        // save
                        commission.Save();

                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Commission Payment: {1}", AuditLogConstants.AddedNew, commission.Id),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = commission.EntityName(),
                            EntityId = commission.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                        // commit transaction
                        CommitTransaction();
                    }

                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
                }
            }

            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);

	    }

	    private void OnSearch(object sender, ViewEventArgs<CommissionsToBePaidViewSearchCriteria> e)
		{
			var results = new CommissionsToBePaidSearch().FetchCommissionsToBePaid(e.Argument, _view.ActiveUser.TenantId);

			if (results.Count == 0)
				_view.DisplayMessages(new[] {ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg)});
			else
				_view.DisplaySearchResult(results);
		}

		private void OnSave(object sender, ViewEventArgs<List<CommissionPayment>> e)
		{
			var commissions = e.Argument;
			var msg = new List<ValidationMessage>();

			foreach (var commission in commissions)
			{
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					_validator.Messages.Clear();
					if (!_validator.IsValid(commission))
					{
						RollBackTransaction();
						msg.AddRange(_validator.Messages);
						continue;
					}

                    // audit log invoice
					if (commission.IsNew)
					{
						commission.Connection = Connection;
						commission.Transaction = Transaction;

                        // save
                        commission.Save();

						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Commission Payment: {1}", AuditLogConstants.AddedNew, commission.Id),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = commission.EntityName(),
								EntityId = commission.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

						// commit transaction
						CommitTransaction();
					}

				}
				catch (Exception ex)
				{
					RollBackTransaction();
					_view.LogException(ex);
					msg.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
				}
			}

			msg.Add(ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
		}

	}
}
