﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class MiscReceiptApplicationHandler : EntityBase
    {
        private const string AmountAppliedGreaterThanMiscReceiptAppliableAmount = "Cannot apply an amount greater than misc receipt's amount paid to invoices";

        private readonly IMiscReceiptApplicationView _view;

        private readonly MiscReceiptValidator _mrValidator = new MiscReceiptValidator();
        private readonly InvoiceValidator _iValidator = new InvoiceValidator();

        public MiscReceiptApplicationHandler(IMiscReceiptApplicationView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.ApplyMiscReceiptToInvoices += OnApplyMiscReceiptToInvoices;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
        }


        private void OnSave(object sender, ViewEventArgs<MiscReceiptApplication> e)
        {
            var application = e.Argument;
            if (application.IsNew)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot save new misc receipt appliation.") });
                return;
            }

            var applicationLock = application.ObtainLock(_view.ActiveUser, application.Id);
            if (!applicationLock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            var invoice = application.Invoice;
            var invoiceLock = invoice.ObtainLock(_view.ActiveUser, invoice.Id);
            if (!invoiceLock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Could not obtain Invoice lock: {0}", ProcessorVars.UnableToObtainLockErrMsg) });
                applicationLock.Delete();
                return;
            }

            _mrValidator.Messages.Clear();
            _iValidator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _mrValidator.Connection = Connection;
                _mrValidator.Transaction = Transaction;

                _iValidator.Connection = Connection;
                _iValidator.Transaction = Transaction;

                application.Connection = Connection;
                application.Transaction = Transaction;

                if (!_mrValidator.MiscReceiptApplicationsAreValid(new[]{application}))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_mrValidator.Messages);
                    return;
                }

                if (application.Amount > application.MiscReceipt.AmountLeftToBeAppliedOrRefunded(application.MiscReceipt.TenantId, application.Id))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Error(AmountAppliedGreaterThanMiscReceiptAppliableAmount) });
                    return;
                }

                var dbMiscReceiptApplication = new MiscReceiptApplication(application.Id);
                invoice.TakeSnapShot();
                invoice.PaidAmount -= dbMiscReceiptApplication.Amount;
                invoice.PaidAmount += application.Amount;

                _iValidator.Messages.Clear();
                if (!_iValidator.IsValid(invoice))
                {
                    RollBackTransaction();
                    applicationLock.Delete();
                    _view.DisplayMessages(_iValidator.Messages);
                    return;
                }

                if (application.HasChanges())
                {
                    foreach (var change in application.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = application.EntityName(),
                            EntityId = application.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    application.Save();
                }

                if (invoice.HasChanges())
                {
                    foreach (var change in invoice.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = invoice.EntityName(),
                            EntityId = invoice.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    invoice.Save();
                }

                CommitTransaction();

                applicationLock.Delete();
                invoiceLock.Delete();

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                applicationLock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<MiscReceiptApplication> e)
        {
            _iValidator.Messages.Clear();

            var application = e.Argument;

            if (application.IsNew) return;

            var invoice = application.Invoice;


            var applicationLock = application.ObtainLock(_view.ActiveUser, application.Id);
            if (!applicationLock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            var invoiceLock = invoice.ObtainLock(_view.ActiveUser, invoice.Id);
            if (!invoiceLock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Could not obtain Invoice lock: {0}", ProcessorVars.UnableToObtainLockErrMsg) });
                applicationLock.Delete();
                return;
            }

            invoice.TakeSnapShot();
            invoice.PaidAmount -= application.Amount;

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                application.Connection = Connection;
                application.Transaction = Transaction;

                _iValidator.Connection = Connection;
                _iValidator.Transaction = Transaction;

                applicationLock.Connection = Connection;
                applicationLock.Transaction = Transaction;

                _iValidator.Messages.Clear();
                if (!_iValidator.IsValid(invoice))
                {
                    RollBackTransaction();
                    invoiceLock.Delete();
                    applicationLock.Delete();
                    _view.DisplayMessages(_iValidator.Messages);
                    return;
                }

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} applied to Invoice: {2} for Misc Receipt Gateway Transaction Id: {3}", AuditLogConstants.Delete, application.EntityName(),
                                                application.Invoice.InvoiceNumber, application.MiscReceipt.GatewayTransactionId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = application.EntityName(),
                    EntityId = application.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                if (invoice.HasChanges())
                {
                    foreach (var change in invoice.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = invoice.EntityName(),
                            EntityId = invoice.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    invoice.Save();
                }

                application.Delete();
                applicationLock.Delete();
                invoiceLock.Delete();

                CommitTransaction();

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnApplyMiscReceiptToInvoices(object sender, ViewEventArgs<List<MiscReceiptApplication>> e)
        {
            var messages = new List<ValidationMessage>();

            _mrValidator.Messages.Clear();
            _iValidator.Messages.Clear();

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                _mrValidator.Connection = Connection;
                _mrValidator.Transaction = Transaction;


                if (!_mrValidator.MiscReceiptApplicationsAreValid(e.Argument))
                {
                    RollBackTransaction();
                    messages.AddRange(_mrValidator.Messages);
                    messages.Add(ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
                    _view.DisplayMessages(messages);
                    return;
                }

                if (e.Argument.Sum(m => m.Amount) > (e.Argument.Any() ? e.Argument.First().MiscReceipt.AmountLeftToBeAppliedOrRefunded(_view.ActiveUser.TenantId) : 0))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Error(AmountAppliedGreaterThanMiscReceiptAppliableAmount) });
                    return;
                }

                foreach (var application in e.Argument)
                {
                    application.Connection = Connection;
                    application.Transaction = Transaction;

                    var invoice = application.Invoice;
                    var @lock = invoice.ObtainLock(_view.ActiveUser, invoice.Id);
                    if (!@lock.IsUserLock(_view.ActiveUser))
                    {
                        RollBackTransaction();
                        messages.AddRange(new[] { ValidationMessage.Error("Could not obtain lock for Invoice Number {0}: {1}", invoice.InvoiceNumber, ProcessorVars.UnableToObtainLockErrMsg) });
                        messages.Add(ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
                        _view.DisplayMessages(messages);
                        return;
                    }

                    application.Save();
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description =
                                string.Format("{0} {1} applied to Invoice: {2} for amount: {3}",
                                              AuditLogConstants.AddedNew, application.EntityName(),
                                              application.Invoice.InvoiceNumber, application.Amount),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = application.EntityName(),
                            EntityId = application.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    invoice.TakeSnapShot();
                    invoice.PaidAmount += application.Amount;

                    _iValidator.Messages.Clear();
                    if (!_iValidator.IsValid(invoice))
                    {
                        RollBackTransaction();
                        @lock.Delete();
                        _view.DisplayMessages(_iValidator.Messages);
                        return;
                    }

                    if (invoice.HasChanges())
                    {
                        foreach (var change in invoice.Changes())
                            new AuditLog
                                {
                                    Connection = Connection,
                                    Transaction = Transaction,
                                    Description = change,
                                    TenantId = _view.ActiveUser.TenantId,
                                    User = _view.ActiveUser,
                                    EntityCode = invoice.EntityName(),
                                    EntityId = invoice.Id.ToString(),
                                    LogDateTime = DateTime.Now
                                }.Log();
                        invoice.Save();
                    }
                    @lock.Delete();
                }

                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                messages.Add(ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message));
            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }
    }
}
