﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class BatchPaymentApplicationHandler : EntityBase
    {
        private readonly IBatchPaymentApplicationView _view;
        private readonly MiscReceiptValidator _mrValidator = new MiscReceiptValidator();
        private readonly InvoiceValidator _iValidator = new InvoiceValidator();


        public BatchPaymentApplicationHandler(IBatchPaymentApplicationView view)
        {
            _view = view;
        }


        public void Initialize()
        {
            _view.Search += OnSearch;
            _view.ApplyMiscReceiptToInvoices += OnApplyMiscReceiptToInvoices;
        }

        private void OnSearch(object sender, ViewEventArgs<MiscReciptViewSearchCriteria> e)
        {
            var results = new MiscReceiptSearch().FetchUnappliedMiscReceipts(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }

        private void OnApplyMiscReceiptToInvoices(object sender, ViewEventArgs<List<MiscReceiptApplication>> e)
        {
            var messages = new List<ValidationMessage>();

            _mrValidator.Messages.Clear();
            _iValidator.Messages.Clear();

            Connection = DatabaseConnection.DefaultConnection;

            // if the sum of application amounts for a particular misc receipt are greater than the appliable amount
            if (e.Argument.Any(app => e.Argument.Where(mr => mr.MiscReceiptId == app.MiscReceiptId).Sum(mr => mr.Amount) > app.MiscReceipt.AmountLeftToBeAppliedOrRefunded(_view.ActiveUser.TenantId)))
            {
                messages.Add(ValidationMessage.Error("Cannot apply an amount greater than misc receipt's amount paid to invoices"));
                _view.DisplayMessages(messages);
                return;
            }

            BeginTransaction();

            try
            {
                _mrValidator.Connection = Connection;
                _mrValidator.Transaction = Transaction;

                if (!_mrValidator.MiscReceiptApplicationsAreValid(e.Argument))
                {
                    RollBackTransaction();
                    messages.AddRange(_mrValidator.Messages);
                    _view.DisplayMessages(messages);
                    return;
                }
                
                foreach (var application in e.Argument)
                {
                    application.Connection = Connection;
                    application.Transaction = Transaction;

                    var invoice = new Invoice(application.InvoiceId, false);
                    var @lock = invoice.ObtainLock(_view.ActiveUser, invoice.Id);
                    if (!@lock.IsUserLock(_view.ActiveUser))
                    {
                        RollBackTransaction();
                        messages.AddRange(new[] { ValidationMessage.Error("Could not obtain lock for Invoice Number {0}: {1}", invoice.InvoiceNumber, ProcessorVars.UnableToObtainLockErrMsg) });
                        _view.DisplayMessages(messages);
                        return;
                    }

                    application.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} applied to Invoice: {2} for amount: {3}",
                                          AuditLogConstants.AddedNew, application.EntityName(),
                                          application.Invoice.InvoiceNumber, application.Amount),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = application.EntityName(),
                        EntityId = application.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    invoice.TakeSnapShot();
                    invoice.PaidAmount += application.Amount;

                    _iValidator.Messages.Clear();
                    if (!_iValidator.IsValid(invoice))
                    {
                        RollBackTransaction();
                        @lock.Delete();
                        _view.DisplayMessages(_iValidator.Messages);
                        return;
                    }

                    if (invoice.HasChanges())
                    {
                        foreach (var change in invoice.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = invoice.EntityName(),
                                EntityId = invoice.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                        invoice.Save();
                    }

                    @lock.Delete();
                }

                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                messages.Add(ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message));
            }
            _view.DisplayMessages(messages);
        }
    }
}
