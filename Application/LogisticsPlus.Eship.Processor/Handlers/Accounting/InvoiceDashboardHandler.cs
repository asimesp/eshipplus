﻿using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class InvoiceDashboardHandler : EntityBase
    {
        private readonly IInvoiceDashboardView _view;

        public InvoiceDashboardHandler(IInvoiceDashboardView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<InvoiceViewSearchCriteria> e)
		{
			var results = new InvoiceSearch().FetchInvoiceDashboardDtos(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
			if (results.Count == 0 && !e.Argument.DisableNoRecordNotification)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
		}
    }
}
