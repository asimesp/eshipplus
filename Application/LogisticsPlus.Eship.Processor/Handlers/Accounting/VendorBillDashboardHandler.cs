﻿using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class VendorBillDashboardHandler : EntityBase
	{
		private readonly IVendorBillDashboardView _view;

		public VendorBillDashboardHandler(IVendorBillDashboardView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<VendorBillViewSearchCriteria> e)
		{

			var results = new VendorBillSearch().FetchVendorBillDashboardDtos(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
			if (results.Count == 0)
				_view.DisplayMessages(new[] {ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg)});
		}
	}
}
