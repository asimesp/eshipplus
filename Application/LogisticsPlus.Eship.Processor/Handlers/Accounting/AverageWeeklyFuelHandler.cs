﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class AverageWeeklyFuelHandler : EntityBase
	{
		private readonly IAverageWeeklyFuelView _view;

		private readonly AverageWeeklyFuelValidator _validator = new AverageWeeklyFuelValidator();

		public AverageWeeklyFuelHandler(IAverageWeeklyFuelView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<AverageWeeklyFuel>> e)
		{
			var messages = new ValidationMessageCollection();

			foreach (var averageWeeklyFuel in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					averageWeeklyFuel.Connection = Connection;
					averageWeeklyFuel.Transaction = Transaction;

					if (!_validator.IsValid(averageWeeklyFuel))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					averageWeeklyFuel.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, averageWeeklyFuel.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = averageWeeklyFuel.EntityName(),
							EntityId = averageWeeklyFuel.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					CommitTransaction();
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Name: {1}]. Err: {2}", ProcessorVars.RecordSaveErrMsg,  averageWeeklyFuel.Id, ex.Message));

				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<AverageWeeklyFuel> e)
		{
			var averageWeeklyFuel = e.Argument;
			var @lock = averageWeeklyFuel.RetrieveLock(_view.ActiveUser, averageWeeklyFuel.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<AverageWeeklyFuel> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<AverageWeeklyFuelViewSearchCriteria> e)
		{
			var results = new AverageWeeklyFuelSearch().FetchAverageWeeklyFuels(e.Argument.Criteria, e.Argument.StartDate,
			                                                                    e.Argument.EndDate, _view.ActiveUser.TenantId);

			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<AverageWeeklyFuel> e)
		{
			_validator.Messages.Clear();

			var averageWeeklyFuel = e.Argument;

			if (averageWeeklyFuel.IsNew) return;

			var @lock = averageWeeklyFuel.ObtainLock(_view.ActiveUser, averageWeeklyFuel.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				averageWeeklyFuel.Connection = Connection;
				averageWeeklyFuel.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, averageWeeklyFuel.ChargeCodeId, averageWeeklyFuel.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = averageWeeklyFuel.EntityName(),
						EntityId = averageWeeklyFuel.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				averageWeeklyFuel.Delete();
				@lock.Delete();

				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<AverageWeeklyFuel> e)
		{
			var averageWeeklyFuel = e.Argument;

			if (!averageWeeklyFuel.IsNew)
			{
				var @lock = averageWeeklyFuel.RetrieveLock(_view.ActiveUser, averageWeeklyFuel.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}
			
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				averageWeeklyFuel.Connection = Connection;
				averageWeeklyFuel.Transaction = Transaction;

				_validator.Messages.Clear();

				if (!_validator.IsValid(averageWeeklyFuel))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (averageWeeklyFuel.IsNew)
				{
					averageWeeklyFuel.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, averageWeeklyFuel.ChargeCodeId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = averageWeeklyFuel.EntityName(),
						EntityId = averageWeeklyFuel.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (averageWeeklyFuel.HasChanges())
				{
					foreach (var change in averageWeeklyFuel.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = averageWeeklyFuel.EntityName(),
								EntityId = averageWeeklyFuel.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					averageWeeklyFuel.Save();
				}

				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}
	}
}
