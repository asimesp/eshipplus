﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class CustomerHandler : EntityBase
    {
        private readonly ICustomerView _view;
        private readonly CustomerValidator _validator = new CustomerValidator();
        private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();
        private readonly UserValidator _userValidator = new UserValidator();
        private readonly PaymentGatewayValidator _pgValidator = new PaymentGatewayValidator();

        public CustomerHandler(ICustomerView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.LoadAuditLog += OnLoadAuditLog;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.SalesRepSearch += OnSalesRepSearch;
            _view.TierSearch += OnTierSearch;
            _view.PrefixSearch += OnPrefixSearch;
            _view.AccountBucketSearch += OnAccountBucketSearch;
            _view.AddCustomerUsers += OnAddCustomerUsers;
            _view.CreateCustomerInPaymentGateway += OnCreateCustomerInPaymentGateway;
            _view.DeleteCustomerInPaymentGateway += OnDeleteCustomerInPaymentGateway;
        }


        private void OnDeleteCustomerInPaymentGateway(object sender, ViewEventArgs<Customer> e)
        {
            var customer = e.Argument;

            // check locks
            if (!customer.IsNew)
            {
                var @lock = customer.RetrieveLock(_view.ActiveUser, customer.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }
            else if (customer.IsNew)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot delete customer in payment gateway that has not been saved yet") });
                return;
            }

            _pgValidator.Messages.Clear();
            if (!_pgValidator.CanRemoveCustomerProfileFromPaymentGateway(customer))
            {
                RollBackTransaction();
                _view.DisplayMessages(_pgValidator.Messages);
                return;
            }

            customer.LoadCollections();
            customer.TakeSnapShot();

            var gateway = customer.Tenant.GetPaymentGatewayService();

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                customer.Connection = Connection;
                customer.Transaction = Transaction;

                // validate customer
                _validator.Messages.Clear();
                if (!_validator.IsValid(customer))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (gateway.DeleteCustomerInformation(customer))
                {
                    customer.PaymentGatewayKey = string.Empty;
                    customer.CanPayByCreditCard = false;
                }
                else
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error("Unable to delete customer in payment gateway") });
                    RollBackTransaction();
                    return;
                }

                if (customer.HasChanges())
                {
                    // audit log customer
                    foreach (var change in customer.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.EntityName(),
                            EntityId = customer.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save
                    customer.Save();
                }

                // commit transaction
                CommitTransaction();

                _view.DisplayMessages(new[] { ValidationMessage.Information("Customer profile has been removed from the payment gateway") });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnCreateCustomerInPaymentGateway(object sender, ViewEventArgs<Customer> e)
        {
            var customer = e.Argument;

            // check locks
            if (!customer.IsNew)
            {
                var @lock = customer.RetrieveLock(_view.ActiveUser, customer.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }
            else if (customer.IsNew)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot create customer in payment gateway that has not been saved yet") });
                return;
            }

            customer.LoadCollections();
            customer.TakeSnapShot();

            var gateway = customer.Tenant.GetPaymentGatewayService();

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                customer.Connection = Connection;
                customer.Transaction = Transaction;

                // validate customer
                _validator.Messages.Clear();
                if (!_validator.IsValid(customer))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                // call payment gateway to create customer profile
                customer.PaymentGatewayKey = gateway.AddCustomerInformation(customer);

                if (customer.HasChanges())
                {
                    // audit log customer
                    foreach (var change in customer.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.EntityName(),
                            EntityId = customer.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save
                    customer.Save();
                }

                // commit transaction
                CommitTransaction();

                _view.DisplayMessages(new[] { ValidationMessage.Information("Customer profile in the payment gateway has been created successfully") });
            }
            catch (Exception ex)
            {
                var msgs = new List<ValidationMessage>();
                if (!string.IsNullOrEmpty(customer.PaymentGatewayKey))
                {
                    try
                    {
                        gateway.DeleteCustomerInformation(customer);
                    }
                    catch (Exception exception)
                    {
                        msgs.Add(ValidationMessage.Error(exception.Message));
                    }
                }

                msgs.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));

                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(msgs);
            }
        }


        private void OnAccountBucketSearch(object sender, ViewEventArgs<string> e)
        {
            var accountBucket = new AccountBucketSearch().FetchAccountBucketByCode(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplayAccountBucket(accountBucket == null || !accountBucket.Active ? new AccountBucket() : accountBucket);
            if (accountBucket == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Account Bucket not found") });
            else if (!accountBucket.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Account Bucket is not active") });
        }

        private void OnPrefixSearch(object sender, ViewEventArgs<string> e)
        {
            var prefix = new PrefixSearch().FetchPrefixByCode(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplayPrefix(prefix == null || !prefix.Active ? new Prefix() : prefix);
            if (prefix == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Prefix not found") });
            else if (!prefix.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Prefix is not active") });
        }

        private void OnTierSearch(object sender, ViewEventArgs<string> e)
        {
            var tier = new TierSearch().FetchTierByTierNumber(e.Argument, _view.ActiveUser.TenantId);

            if (tier == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer Tier not found") });

            _view.DisplayTier(tier ?? new Tier());
        }

        private void OnSalesRepSearch(Object sender, ViewEventArgs<string> e)
        {
            var representative = new SalesRepresentativeSearch().FetchSalesRepresentativesByNumber(e.Argument, _view.ActiveUser.TenantId);

            if (representative == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Sales Representative not found") });
            _view.DisplaySalesRep(representative ?? new SalesRepresentative());
        }



        private void OnDelete(object sender, ViewEventArgs<Customer> e)
        {
            var customer = e.Argument;

            if (customer.IsNew)
            {
                _view.SetId(default(long));
                return;
            }

            // load collections
            customer.LoadCollections();
            foreach (var location in customer.Locations)
                location.LoadCollections();

            // get all user ship as association for this customer
            var userShipAs = customer.RetrieveUserShipAs();

            // obtain lock/check lock
            var locks = new List<Lock>();
            if (!ObtainLocksForDelete(locks, customer, userShipAs))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                customer.Connection = Connection;
                customer.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                foreach (var @lock in locks)
                {
                    @lock.Connection = Connection;
                    @lock.Transaction = Transaction;
                }

                _validator.Messages.Clear();
                if (!_validator.CanDeleteCustomer(customer))
                {
                    RollBackTransaction();
                    foreach (var @lock in locks) @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }
                _validator.Messages.Clear();
                if (customer.Locations.Any(l => !_validator.CanDeleteCustomerLocation(l)))
                {
                    RollBackTransaction();
                    foreach (var @lock in locks) @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} #{2}", AuditLogConstants.Delete, customer.Name, customer.CustomerNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = customer.EntityName(),
                    EntityId = customer.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                // handle collection deletes
                foreach (var location in customer.Locations)
                {
                    location.Connection = Connection;
                    location.Transaction = Transaction;

                    foreach (var contact in location.Contacts)
                    {
                        contact.Connection = Connection;
                        contact.Transaction = Transaction;

                        contact.Delete();
                    }

                    location.Delete();
                }
                foreach (var tag in customer.RequiredInvoiceDocumentTags)
                {
                    tag.Connection = Connection;
                    tag.Transaction = Transaction;

                    tag.Delete();
                }
                foreach (var csr in customer.ServiceRepresentatives)
                {
                    csr.Connection = Connection;
                    csr.Transaction = Transaction;

                    csr.Delete();
                }
                foreach (var field in customer.CustomFields)
                {
                    field.Connection = Connection;
                    field.Transaction = Transaction;

                    field.Delete();
                }

				foreach (var field in customer.CustomChargeCodeMap)
				{
					field.Connection = Connection;
					field.Transaction = Transaction;

					field.Delete();
				}

                foreach (var document in customer.Documents)
                {
                    document.Connection = Connection;
                    document.Transaction = Transaction;

                    document.Delete();
                }

                // delete UserShipAs
                foreach (var shipAs in userShipAs)
                {
                    shipAs.Connection = Connection;
                    shipAs.Transaction = Transaction;

                    shipAs.Delete();
                }

                // delete customer and lock
                customer.Delete();

                foreach (var @lock in locks) @lock.Delete();

                CommitTransaction();

                _view.SetId(default(long));
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                foreach (var @lock in locks) @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSave(object sender, ViewEventArgs<Customer> e)
        {
            var customer = e.Argument;

            // check locks
            if (!customer.IsNew)
            {
                var @lock = customer.RetrieveLock(_view.ActiveUser, customer.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            // assign new numbers
            if (customer.IsNew)
            {
                var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.CustomerNumber, _view.ActiveUser);
                customer.CustomerNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
            }
            foreach (var location in customer.Locations)
                if (location.IsNew)
                {
                    var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.CustomerLocationNumber, _view.ActiveUser);
                    location.LocationNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
                }

            // retrieve db customer for collection comparisons ...
            var dbCustomer = new Customer(customer.Id, false);
            dbCustomer.LoadCollections();
            foreach (var location in dbCustomer.Locations) location.LoadCollections();

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                customer.Connection = Connection;
                customer.Transaction = Transaction;

                // validate customer
                _validator.Messages.Clear();
                if (!_validator.IsValid(customer))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                // audit log new - need customer id so had to wait till after save
                if (customer.IsNew)
                {
                    // save
                    customer.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, customer.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = customer.EntityName(),
                        EntityId = customer.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (customer.HasChanges())
                {
                    // audit log customer
                    foreach (var change in customer.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = customer.EntityName(),
                                EntityId = customer.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                    // save
                    customer.Save();
                }

                // process collections and check if validator has messages from location deletion failure
                ProcessLocations(customer, dbCustomer);
                if (_validator.Messages.Any())
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                ProcessServiceRepresentatives(customer, dbCustomer.ServiceRepresentatives);
                ProcessRequiredInvoiceDocumentTags(customer, dbCustomer.RequiredInvoiceDocumentTags);
                ProcessCustomFields(customer, dbCustomer.CustomFields);
                ProcessCustomerChargeCodeMap(customer, dbCustomer.CustomChargeCodeMap);
                ProcessDocuments(customer, dbCustomer.Documents);

                // commit transaction
                CommitTransaction();

                // return id in case of new
                _view.SetId(customer.Id);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }


        private void OnUnLock(object sender, ViewEventArgs<Customer> e)
        {
            var customer = e.Argument;
            var @lock = customer.RetrieveLock(_view.ActiveUser, customer.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }
            @lock.Delete();
        }

        private void OnLock(object sender, ViewEventArgs<Customer> e)
        {
            var customer = e.Argument;
            var @lock = customer.ObtainLock(_view.ActiveUser, customer.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnLoadAuditLog(object sender, ViewEventArgs<Customer> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
                                                                             e.Argument.TenantId));
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.CustomerTypes = ProcessorUtilities.GetAll<CustomerType>();
        }


        private void ProcessCustomFields(Customer customer, IEnumerable<CustomField> dbCustomFields)
        {
            var dbDelete = dbCustomFields.Where(field => !customer.CustomFields.Select(f => f.Id).Contains(field.Id));

            foreach (var f in dbDelete)
            {
                f.Connection = Connection;
                f.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Customer:{2} Field Name:{3}",
                                                AuditLogConstants.Delete, f.EntityName(), customer.Name, f.Name),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = customer.EntityName(),
                    EntityId = customer.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                f.Delete();
            }

            foreach (var f in customer.CustomFields)
            {
                f.Connection = Connection;
                f.Transaction = Transaction;

                if (f.IsNew)
                {
                    f.Save();

                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Customer:{2} Field Name:{3}",
                                                        AuditLogConstants.AddedNew, f.EntityName(), customer.Name, f.Name),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.EntityName(),
                            EntityId = customer.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (f.HasChanges())
                {
                    foreach (var change in f.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("{0} Ref#{1} {2}", f.EntityName(), f.Id, change),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = customer.EntityName(),
                                EntityId = customer.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                    f.Save();
                }
            }
        }

		private void ProcessCustomerChargeCodeMap(Customer customer, IEnumerable<CustomerChargeCodeMap> dbCustomChargeCodeMap)
		{
			var dbDelete = dbCustomChargeCodeMap.Where(field => !customer.CustomChargeCodeMap.Select(f => f.Id).Contains(field.Id));

			foreach (var f in dbDelete)
			{
				f.Connection = Connection;
				f.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} from Customer:{2}, Charge Code:{3}",
												AuditLogConstants.Delete, f.EntityName(), customer.Name, f.ChargeCode.Code),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = customer.EntityName(),
					EntityId = customer.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				f.Delete();
			}

			foreach (var f in customer.CustomChargeCodeMap)
			{
				f.Connection = Connection;
				f.Transaction = Transaction;

				if (f.IsNew)
				{
					f.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} to Customer:{2}, Charge Code:{3}",
													AuditLogConstants.AddedNew, f.EntityName(), customer.Name, f.ChargeCode.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = customer.EntityName(),
						EntityId = customer.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (f.HasChanges())
				{
					foreach (var change in f.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref#{1} {2}", f.EntityName(), f.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = customer.EntityName(),
							EntityId = customer.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					f.Save();
				}
			}
		}

        private void ProcessRequiredInvoiceDocumentTags(Customer customer, IEnumerable<RequiredInvoiceDocumentTag> dbTags)
        {
            var dbDelete = dbTags.Where(u => !customer.RequiredInvoiceDocumentTags.Select(tag => tag.DocumentTagId).Contains(u.DocumentTagId));

            foreach (var tag in dbDelete)
            {
                tag.Connection = Connection;
                tag.Transaction = Transaction;

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Customer:{2} Document Tag Ref:{3}",
                                                    AuditLogConstants.Delete, tag.EntityName(), customer.Name, tag.DocumentTagId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = customer.EntityName(),
                        EntityId = customer.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                tag.Delete();
            }

            foreach (var tag in customer.RequiredInvoiceDocumentTags)
                if (!_validator.CustomerRequiredInvoiceDocumentTagExist(tag))
                {
                    tag.Connection = Connection;
                    tag.Transaction = Transaction;

                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Customer:{2} Document Tag Ref:{3}",
                                                        AuditLogConstants.AddedNew, tag.EntityName(), customer.Name, tag.DocumentTagId),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.EntityName(),
                            EntityId = customer.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    tag.Save();
                }
        }

        private void ProcessServiceRepresentatives(Customer customer, IEnumerable<CustomerServiceRepresentative> dbRepresentatives)
        {
            var dbDelete = dbRepresentatives.Where(u => !customer.ServiceRepresentatives.Select(csr => csr.UserId).Contains(u.UserId));

            foreach (var csr in dbDelete)
            {
                csr.Connection = Connection;
                csr.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Customer:{2} User Id:{3}", AuditLogConstants.Delete, csr.EntityName(), customer.Name, csr.UserId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = customer.EntityName(),
                    EntityId = customer.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                csr.Delete();
            }

            foreach (var csr in customer.ServiceRepresentatives)
                if (!_validator.CustomerServiceRepresentativeExists(csr))
                {
                    csr.Connection = Connection;
                    csr.Transaction = Transaction;

                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description =
                                string.Format("{0} {1} Customer:{2} User Id:{3}", AuditLogConstants.AddedNew, csr.EntityName(), customer.Name,
                                              csr.UserId),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.EntityName(),
                            EntityId = customer.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    csr.Save();
                }
        }

        private void ProcessLocations(Customer customer, Customer dbCustomer)
        {
            var dbDelete = dbCustomer.Locations.Where(l => !customer.Locations.Select(cl => cl.Id).Contains(l.Id));

            foreach (var l in dbDelete)
                if (_validator.CanDeleteCustomerLocation(l))
                {
                    foreach (var c in l.Contacts)
                    {
                        c.Connection = Connection;
                        c.Transaction = Transaction;

                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Customer:{2} Location #:{3} Contact Ref: {4}",
                                                        AuditLogConstants.Delete, c.EntityName(), customer.Name, l.LocationNumber, c.Id),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.EntityName(),
                            EntityId = customer.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                        c.Delete();
                    }

                    l.Connection = Connection;
                    l.Transaction = Transaction;

                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Customer:{2} Location #:{3}",
                                                        AuditLogConstants.Delete, l.EntityName(), customer.Name, l.LocationNumber),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.EntityName(),
                            EntityId = customer.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    l.Delete();
                }

            foreach (var l in customer.Locations)
            {
                l.Connection = Connection;
                l.Transaction = Transaction;

                if (l.IsNew)
                {
                    l.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Customer:{2} Location #:{3}",
                                                    AuditLogConstants.AddedNew, l.EntityName(), customer.Name, l.LocationNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = customer.EntityName(),
                        EntityId = customer.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (l.HasChanges())
                {
                    foreach (var change in l.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("{0} #{1} {2}", l.EntityName(), l.LocationNumber, change),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = customer.EntityName(),
                                EntityId = customer.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                    l.Save();
                }

                ProcessContacts(customer, l, dbCustomer.Locations.FirstOrDefault(dbl => dbl.Id == l.Id));
            }
        }

        private void ProcessContacts(Customer customer, CustomerLocation location, CustomerLocation dbLocation)
        {
            var dbDelete = dbLocation != null
                            ? dbLocation.Contacts.Where(l => !location.Contacts.Select(cl => cl.Id).Contains(l.Id))
                            : new List<CustomerContact>();

            foreach (var c in dbDelete)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Customer:{2} Location #:{3} Contact Ref: {4}",
                                                    AuditLogConstants.Delete, c.EntityName(), customer.Name, location.LocationNumber, c.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = customer.EntityName(),
                        EntityId = customer.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                c.Delete();
            }

            foreach (var c in location.Contacts)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                if (c.IsNew)
                {
                    c.Save();

                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Customer:{2} Location #:{3} Contact Ref: {4}",
                                                        AuditLogConstants.AddedNew, c.EntityName(), customer.Name, location.LocationNumber,
                                                        c.Id),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.EntityName(),
                            EntityId = customer.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (c.HasChanges())
                {
                    foreach (var change in c.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("Location #{0} {1} {2}", location.LocationNumber, c.EntityName(), change),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = customer.EntityName(),
                                EntityId = customer.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                    c.Save();
                }
            }
        }

        private void ProcessDocuments(Customer customer, IEnumerable<CustomerDocument> dbDocuments)
        {
            var dbDelete = dbDocuments.Where(dbs => !customer.Documents.Select(s => s.Id).Contains(dbs.Id));

            foreach (var d in dbDelete)
            {
                d.Connection = Connection;
                d.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Customer Number:{2} Document Ref#: {3}", AuditLogConstants.Delete, d.EntityName(),
                                      customer.CustomerNumber, d.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = customer.EntityName(),
                    EntityId = customer.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                d.Delete();
            }

            foreach (var d in customer.Documents)
            {
                d.Connection = Connection;
                d.Transaction = Transaction;

                if (d.IsNew)
                {
                    d.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Customer Number:{2} Document Ref#:{3}",
                                                    AuditLogConstants.AddedNew, d.EntityName(), customer.CustomerNumber, d.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = customer.EntityName(),
                        EntityId = customer.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (d.HasChanges())
                {
                    foreach (var change in d.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Document Ref#:{1} {2}", d.EntityName(), d.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.EntityName(),
                            EntityId = customer.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    d.Save();
                }
            }
        }


        private bool ObtainLocksForDelete(ICollection<Lock> locks, Customer customer, IEnumerable<UserShipAs> userShipAs)
        {
            var @lock = customer.ObtainLock(_view.ActiveUser, customer.Id);
            if (@lock != null)
            {
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    foreach (var l in locks) l.Delete();
                    return false;
                }
                locks.Add(@lock);
            }

            foreach (var shipAs in userShipAs)
            {
                @lock = shipAs.User.ObtainLock(_view.ActiveUser, shipAs.User.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    foreach (var l in locks) l.Delete();
                    return false;
                }
            }

            return true;
        }


        private void OnAddCustomerUsers(object sender, ViewEventArgs<List<UserShipAs>> e)
        {
            var messages = new List<ValidationMessage>();

            foreach (var userShipAs in e.Argument)
            {
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    // obtain lock
                    var @lock = userShipAs.User.ObtainLock(_view.ActiveUser, userShipAs.User.Id);
                    if (!@lock.IsUserLock(_view.ActiveUser))
                    {
                        messages.Add(ValidationMessage.Error("User [{0}] record in use by: {1} Date: {2: yyyy-MM-dd hh:mm:ss}",
                                                            userShipAs.User.Username, @lock.User.Username, @lock.LockDateTime));
                        continue;
                    }

                    // chain to transaction
                    _userValidator.Connection = Connection;
                    _userValidator.Transaction = Transaction;

                    @lock.Connection = Connection;
                    @lock.Transaction = Transaction;

                    if (!_userValidator.UserShipAsExists(userShipAs) && userShipAs.User.CustomerId != userShipAs.CustomerId)
                    {
                        userShipAs.Connection = Connection;
                        userShipAs.Transaction = Transaction;

                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description =
                                string.Format("{0} {1} User:{2} Customer Id:{3}", AuditLogConstants.AddedNew, userShipAs.EntityName(), userShipAs.User.Username,
                                              userShipAs.CustomerId),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = userShipAs.User.EntityName(),
                            EntityId = userShipAs.User.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                        userShipAs.Save();
                    }

                    // delete lock
                    @lock.Delete();

                    // commit transaction
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Insert(0, ValidationMessage.Error("An error occurred processing record(s). Some records might not have been processed."));
                }
            }

            // complete transaction
            messages.Add(ValidationMessage.Information("Record(s) processing complete."));
            _view.DisplayMessages(messages);
        }
    }
}