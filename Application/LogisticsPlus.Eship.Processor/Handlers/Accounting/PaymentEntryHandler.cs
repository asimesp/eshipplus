﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class PaymentEntryHandler : EntityBase
    {
        private const string CustomerNotSetupInPaymentGatewayErrorMsg = "Customer is not setup within the payment gateway";

        private readonly IPaymentEntryView _view;
        private readonly MiscReceiptValidator _validator = new MiscReceiptValidator();

        public PaymentEntryHandler(IPaymentEntryView view)
        {
            _view = view;
        }

        
        public void Initialize()
        {
            _view.LoadPaymentProfiles += OnLoadPaymentProfiles;
            _view.LoadPaymentProfile += OnLoadPaymentProfile;
            _view.DeletePaymentProfile += OnDeletePaymentProfile;
            _view.AuthorizeAndCapturePayment += OnAuthorizeAndCapturePayment;
            _view.SaveMiscReceipt += OnSaveMiscReceipt;
        }

        
        private void OnDeletePaymentProfile(object sender, ViewEventArgs<long, string> e)
        {
            var customer = new Customer(e.Argument);
            if (customer.IsNew)
            {
                _view.DisplayMessages(new[] {ValidationMessage.Error("Customer must be set before attempting to delete payment profile")});
                return;
            }

            if (string.IsNullOrEmpty(customer.PaymentGatewayKey))
            {
                _view.DisplayMessages(new[] {ValidationMessage.Error(CustomerNotSetupInPaymentGatewayErrorMsg)});
                return;
            }

            var gateway = customer.Tenant.GetPaymentGatewayService();

            try
            {
                var success = gateway.RemovePaymentProfile(customer.PaymentGatewayKey, e.Argument2);
                if (success) 
                    _view.DisplayPaymentProfile(new PaymentGatewayPaymentProfile());
                else
                    _view.DisplayMessages(new[] {ValidationMessage.Error("Unable to delete payment profile within payment gateway. Payment profile must be manually removed via the payment gateway's website.")});
                    
            }
            catch (Exception ex)
            {
                _view.DisplayMessages(new[] {ValidationMessage.Error(ex.Message)});
            }
        }

        private void OnLoadPaymentProfile(object sender, ViewEventArgs<long, string> e)
        {
            var customer = new Customer(e.Argument);
            if (customer.IsNew)
            {
                _view.DisplayMessages(new[] {ValidationMessage.Error("Customer must be set before attempting to load payment profile")});
                return;
            }

            if (string.IsNullOrEmpty(customer.PaymentGatewayKey))
            {
                _view.DisplayMessages(new[] {ValidationMessage.Error(CustomerNotSetupInPaymentGatewayErrorMsg)});
                _view.DisplayPaymentProfile(new PaymentGatewayPaymentProfile());
                return;
            }

            var gateway = customer.Tenant.GetPaymentGatewayService();

            try
            {
                _view.DisplayPaymentProfile(gateway.GetExistingPaymentProfile(customer.PaymentGatewayKey,e.Argument2));
            }
            catch (Exception ex)
            {
                _view.DisplayMessages(new[] {ValidationMessage.Error(ex.Message)});
            }
        }

        private void OnLoadPaymentProfiles(object sender, ViewEventArgs<long> e)
        {
            var customer = new Customer(e.Argument);
            if (customer.IsNew)
            {
                _view.DisplayMessages(new []{ValidationMessage.Error("Customer must be set before attempting to load its payment profiles")});
                return;
            }

            if (string.IsNullOrEmpty(customer.PaymentGatewayKey))
            {
                _view.DisplayMessages(new[] {ValidationMessage.Error(CustomerNotSetupInPaymentGatewayErrorMsg)});
                _view.PaymentProfiles = new Dictionary<string, string>();
                return;
            }

            var gateway = customer.Tenant.GetPaymentGatewayService();

            try
            {
                _view.PaymentProfiles = gateway
               .GetPaymentProfiles(customer.PaymentGatewayKey)
               .ToDictionary(profile => profile.PaymentProfileId, profile => string.Format("{0} {1}/{2} {3} {4}",
                   profile.CreditCardNumber,
                   profile.ExpirationDateMonth,
                   profile.ExpirationDateYear,
                   profile.BillingFirstName,
                   profile.BillingLastName));
            }
            catch (Exception ex)
            {
                _view.DisplayMessages(new[] {ValidationMessage.Error(ex.Message)});
            }
        }


        private void OnAuthorizeAndCapturePayment(object sender, ViewEventArgs<MiscReceipt, PaymentInformation> e)
        {
            var miscReceipt = e.Argument;
            var paymentInfo = e.Argument2;
            var transactionId = string.Empty;

            if (!miscReceipt.IsNew)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot save existing misc receipt") });
                return;
            }

            var gateway = new Tenant(_view.ActiveUser.TenantId).GetPaymentGatewayService();
            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                miscReceipt.Connection = Connection;
                miscReceipt.Transaction = Transaction;

                if (!_validator.IsValid(miscReceipt) || (miscReceipt.ShipmentId != default(long) && !_validator.MiscReceiptShipmentExists(miscReceipt)) || (miscReceipt.LoadOrderId != default(long) && !_validator.MiscReceiptLoadOrderExists(miscReceipt)))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                var paymentProfileId = paymentInfo.CustomerPaymentProfileId;
                if (string.IsNullOrEmpty(paymentInfo.CustomerPaymentProfileId))
                    paymentProfileId = gateway.AddPaymentProfile(paymentInfo);

                var response = gateway.AuthorizeAndCapture(paymentInfo.CustomerPaymentGatewayKey, paymentProfileId, paymentInfo.ChargeAmount, paymentInfo.Description);
                transactionId = response.TransactionId;
                miscReceipt.PaymentProfileId = paymentProfileId;
                miscReceipt.GatewayTransactionId = transactionId;

                if (response.Approved)
                {
                    miscReceipt.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Misc Receipt Shipment #:{1} Load Order #:{2} Customer:{3} Amount:{4}",
                                        AuditLogConstants.AddedNew,
                                        miscReceipt.ShipmentId != default(long)
                                            ? miscReceipt.Shipment.ShipmentNumber
                                            : string.Empty,
                                        miscReceipt.LoadOrderId != default(long)
                                            ? miscReceipt.LoadOrder.LoadOrderNumber
                                            : string.Empty,
                                        miscReceipt.Customer.CustomerNumber,
                                        miscReceipt.AmountPaid),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = miscReceipt.EntityName(),
                        EntityId = miscReceipt.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    CommitTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Information("Payment has been successfully processed.") });
                }
                else
                {
                    RollBackTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Error(response.ErrorMessage) });
                }
            }
            catch (Exception ex)
            {
                var msgs = new List<ValidationMessage>();
                if (!string.IsNullOrEmpty(transactionId))
                {
                    try
                    {
                        gateway.Void(transactionId);
                    }
                    catch (Exception gException)
                    {
                         msgs.Add(ValidationMessage.Error(gException.Message));
                    }
                }

                msgs.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(msgs);
            }

        }

        private void OnSaveMiscReceipt(object sender, ViewEventArgs<MiscReceipt> e)
        {
            var miscReceipt = e.Argument;
            if (!miscReceipt.IsNew)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot save existing misc receipt") });
                return;
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                miscReceipt.Connection = Connection;
                miscReceipt.Transaction = Transaction;

                if (!_validator.IsValid(miscReceipt))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                miscReceipt.Save();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Misc Receipt Shipment #:{1} Load Order #:{2} Customer:{3} Amount:{4}",
                                      AuditLogConstants.AddedNew,
                                      miscReceipt.ShipmentId != default(long)
                                          ? miscReceipt.Shipment.ShipmentNumber
                                          : string.Empty,
                                      miscReceipt.LoadOrderId != default(long)
                                          ? miscReceipt.LoadOrder.LoadOrderNumber
                                          : string.Empty,
                                      miscReceipt.Customer.CustomerNumber,
                                      miscReceipt.AmountPaid),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = miscReceipt.EntityName(),
                    EntityId = miscReceipt.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }
    }
}
