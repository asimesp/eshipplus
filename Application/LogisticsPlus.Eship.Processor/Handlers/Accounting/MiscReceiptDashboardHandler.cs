﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class MiscReceiptDashboardHandler : EntityBase
    {
        private readonly IMiscReceiptDashboardView _view;

        private readonly MiscReceiptValidator _validator = new MiscReceiptValidator();

        public MiscReceiptDashboardHandler(IMiscReceiptDashboardView view)
        {
            _view = view;
        }


        public void Initialize()
        {
            _view.Search += OnSearch;
            _view.RefundMiscReceipt += OnRefundMiscReceipt;
            _view.VoidMiscReceipt += OnVoidMiscReceipt;
        }


        private void OnVoidMiscReceipt(object sender, ViewEventArgs<MiscReceipt> e)
        {
            var miscReceipt = e.Argument;

            if (miscReceipt.AmountLeftToBeAppliedOrRefunded(miscReceipt.TenantId) < miscReceipt.AmountPaid)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot void a receipt that has been applied to one or more invoices or has been partially refunded") });
                return;
            }

            var gateway = new Tenant(_view.ActiveUser.TenantId).GetPaymentGatewayService();

            var voidReceipt = new MiscReceipt
            {
                AmountPaid = miscReceipt.AmountPaid,
                CustomerId = miscReceipt.CustomerId,
                GatewayTransactionId = string.Empty,
                PaymentGatewayType = miscReceipt.PaymentGatewayType,
                PaymentDate = DateTime.Now,
                Reversal = true,
                ShipmentId = miscReceipt.ShipmentId,
                LoadOrderId = miscReceipt.LoadOrderId,
                TenantId = miscReceipt.TenantId,
                UserId = miscReceipt.UserId,
                OriginalMiscReceiptId = miscReceipt.Id,
                MiscReceiptApplications = new List<MiscReceiptApplication>(),
                NameOnCard = miscReceipt.NameOnCard,
                PaymentProfileId = string.Empty
            };

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                voidReceipt.Connection = Connection;
                voidReceipt.Transaction = Transaction;

                if (!_validator.IsValid(voidReceipt))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                voidReceipt.Save();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Misc Receipt Shipment #:{1} Load Order #:{2} Customer:{3} Amount:{4}",
                                      AuditLogConstants.AddedNew,
                                      voidReceipt.ShipmentId != default(long)
                                          ? voidReceipt.Shipment.ShipmentNumber
                                          : string.Empty,
                                      voidReceipt.LoadOrderId != default(long)
                                          ? voidReceipt.LoadOrder.LoadOrderNumber
                                          : string.Empty, 
                                      voidReceipt.Customer.CustomerNumber,
                                      voidReceipt.AmountPaid),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = voidReceipt.EntityName(),
                    EntityId = voidReceipt.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                var response = gateway.Void(miscReceipt.GatewayTransactionId);
               
                voidReceipt.TakeSnapShot();
                voidReceipt.GatewayTransactionId = response.TransactionId;

                if (response.Approved)
                {
                    if (voidReceipt.HasChanges())
                    {
                        foreach (var change in voidReceipt.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = voidReceipt.EntityName(),
                                EntityId = voidReceipt.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                        voidReceipt.Save();
                    }

                    CommitTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Information("Void has been successfully processed.") });
                }
                else
                {
                    RollBackTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Error("An error occurred when attempting to void receipt: {0}", response.ErrorMessage) }.ToList());
                }
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new List<ValidationMessage>{ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message)});
            }
        }

        private void OnRefundMiscReceipt(object sender, ViewEventArgs<MiscReceipt, decimal> e)
        {
            var miscReceipt = e.Argument;
            var refundAmount = e.Argument2;

            if (miscReceipt.AmountLeftToBeAppliedOrRefunded(miscReceipt.TenantId) < refundAmount)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot refund a receipt for more than is currently applied or refunded") });
                return;
            }

            var transactionId = string.Empty;
            var gateway = new Tenant(_view.ActiveUser.TenantId).GetPaymentGatewayService();


            var refundReceipt = new MiscReceipt
                {
                    AmountPaid = refundAmount,
                    CustomerId = miscReceipt.CustomerId,
                    GatewayTransactionId = string.Empty,
                    PaymentGatewayType = miscReceipt.PaymentGatewayType,
                    PaymentDate = DateTime.Now,
                    Reversal = true,
                    ShipmentId = miscReceipt.ShipmentId,
                    LoadOrderId = miscReceipt.LoadOrderId,
                    TenantId = miscReceipt.TenantId,
                    UserId = miscReceipt.UserId,
                    OriginalMiscReceiptId = miscReceipt.Id,
                    MiscReceiptApplications = new List<MiscReceiptApplication>(),
                    NameOnCard = miscReceipt.NameOnCard,
                    PaymentProfileId = string.Empty
                };

            if (miscReceipt.PaymentGatewayType != miscReceipt.Tenant.PaymentGatewayType && refundReceipt.PaymentGatewayType != PaymentGatewayType.NotApplicable && refundReceipt.PaymentGatewayType != PaymentGatewayType.Check)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.MiscReceiptErrorPaymentGatewayForTenantChanged, miscReceipt.AmountPaid) });
                return;
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                refundReceipt.Connection = Connection;
                refundReceipt.Transaction = Transaction;

                if (!_validator.IsValid(refundReceipt))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                refundReceipt.Save();
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Misc Receipt Shipment #:{1} Load Order #:{2} Customer:{3} Amount:{4}",
                                        AuditLogConstants.AddedNew,
                                        refundReceipt.ShipmentId != default(long)
                                            ? refundReceipt.Shipment.ShipmentNumber
                                            : string.Empty,
                                        refundReceipt.LoadOrderId != default(long)
                                            ? refundReceipt.LoadOrder.LoadOrderNumber
                                            : string.Empty,
                                        refundReceipt.Customer.CustomerNumber,
                                        refundReceipt.AmountPaid),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = refundReceipt.EntityName(),
                    EntityId = refundReceipt.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                if (refundReceipt.PaymentGatewayType == PaymentGatewayType.NotApplicable || refundReceipt.PaymentGatewayType == PaymentGatewayType.Check)
                {
                    CommitTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RefundSuccessfullyProcessedMessage) });
                    return;
                }

                var response = gateway.Refund(miscReceipt.GatewayTransactionId, refundReceipt.AmountPaid);
                if (response.Approved)
                {
                    refundReceipt.TakeSnapShot();
                    transactionId = response.TransactionId;
                    refundReceipt.GatewayTransactionId = transactionId;

                    if (refundReceipt.HasChanges())
                    {
                        foreach (var change in refundReceipt.Changes())
                            new AuditLog
                                {
                                    Connection = Connection,
                                    Transaction = Transaction,
                                    Description = change,
                                    TenantId = _view.ActiveUser.TenantId,
                                    User = _view.ActiveUser,
                                    EntityCode = refundReceipt.EntityName(),
                                    EntityId = refundReceipt.Id.ToString(),
                                    LogDateTime = DateTime.Now
                                }.Log();
                        refundReceipt.Save();
                    }

                    CommitTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RefundSuccessfullyProcessedMessage) });
                }
                else
                {
                    RollBackTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Error("An error occurred when attempting to refund receipt: {0}", response.ErrorMessage) }.ToList());
                }
            }
            catch (Exception ex)
            {
                var msgs = new List<ValidationMessage>();
                if (!string.IsNullOrEmpty(transactionId))
                {
                    try
                    {
                        gateway.Void(transactionId);
                    }
                    catch (Exception gException)
                    {
                        msgs.Add(ValidationMessage.Error(gException.Message));
                    }
                }

                msgs.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));

                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(msgs);
            }
        }

        private void OnSearch(object sender, ViewEventArgs<MiscReciptViewSearchCriteria> e)
        {
            var results = new MiscReceiptSearch().FetchMiscReceipts(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }
    }
}
