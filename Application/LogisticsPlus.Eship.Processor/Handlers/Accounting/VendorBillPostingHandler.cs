﻿using System;
using System.Collections.Generic;
using System.Threading;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class VendorBillPostingHandler : EntityBase
	{
		private readonly IVendorBillPostingView _view;
		public VendorBillPostingHandler(IVendorBillPostingView view)
		{
			_view = view;
		}
		private readonly VendorBillValidator _validator = new VendorBillValidator();

		public void Initialize()
		{
			_view.Search += OnSearch;
			_view.Post += OnPost;
		}

		private void OnSearch(object sender, ViewEventArgs<VendorBillViewSearchCriteria> e)
		{
			var results = new VendorBillSearch().FetchVendorBillDashboardDtos(e.Argument, _view.ActiveUser.TenantId);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}

		private void OnPost(object sender, ViewEventArgs<List<VendorBill>> e)
		{
			var messages = new List<ValidationMessage>();

			foreach (var vendorBill in e.Argument)
			{
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					// link up
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					//attempt lock
					var @lock = vendorBill.ObtainLock(_view.ActiveUser, vendorBill.Id);
					if (!@lock.IsUserLock(_view.ActiveUser))
					{
						messages.Add(ValidationMessage.Error(string.Format("{0} on Vendor Bill #: {1}", ProcessorVars.UnableToObtainLockErrMsg, vendorBill.DocumentNumber)));
						continue;
					}

					// chain lock
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					// chain invoice
					vendorBill.Connection = Connection;
					vendorBill.Transaction = Transaction;

					vendorBill.TakeSnapShot();
					vendorBill.Posted = true;
					vendorBill.PostDate = DateTime.Now;

					foreach (var change in vendorBill.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendorBill.EntityName(),
							EntityId = vendorBill.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					vendorBill.Save();

					@lock.Delete();

					// commit transaction
					CommitTransaction();

					
				}
				catch (Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}
	}
}
