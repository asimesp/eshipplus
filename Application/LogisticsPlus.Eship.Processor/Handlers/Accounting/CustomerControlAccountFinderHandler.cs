﻿using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class CustomerControlAccountFinderHandler
	{
		private readonly ICustomerControlAccountFinderView _view;

		public CustomerControlAccountFinderHandler(ICustomerControlAccountFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<CustomerControlAccountSearchCriteria> e)
		{
			var results = new CustomerControlAccountSearch().FetchCustomerControlAccounts(e.Argument, _view.ActiveUser.TenantId);

			_view.DisplaySearchResult(results);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
			
		}
	}
}
