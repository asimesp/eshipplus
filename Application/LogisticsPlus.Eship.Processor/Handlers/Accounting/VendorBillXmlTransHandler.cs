﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class VendorBillXmlTransHandler : EntityBase
	{
		private readonly VendorBillValidator _validator = new VendorBillValidator();

		public void DeleteVendorBills(VendorBill bill, out Exception e, User processingUser)
		{
			e = null;

			if (bill.IsNew) return;

			// load collections
			bill.LoadCollections();

			// obtain lock/check lock
			var @lock = bill.ObtainLock(processingUser, bill.Id);
			if (!@lock.IsUserLock(processingUser)) return;

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				bill.Connection = Connection;
				bill.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteVendorBill(bill))
				{
					RollBackTransaction();
					@lock.Delete();
					return;
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} Vendor Bill Number: {1}", AuditLogConstants.Delete, bill.DocumentNumber),
					TenantId = processingUser.TenantId,
					User = processingUser,
					EntityCode = bill.EntityName(),
					EntityId = bill.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();


				// handle collection deletes
				foreach (var detail in bill.Details)
				{
					detail.Connection = Connection;
					detail.Transaction = Transaction;

					detail.Delete();
				}

				//delete bill and lock
				bill.Delete();

				// delete lock
				@lock.Delete();

				CommitTransaction();
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
				e = ex;
			}
		}

		public bool SaveNewVendorBills(VendorBill bill, out Exception e, User processingUser)
		{
			if (!bill.IsNew || !bill.Details.All(d => d.IsNew))
			{
				e = null;
				return false;
			}

			_validator.Messages.Clear();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				bill.Connection = Connection;
				bill.Transaction = Transaction;

				// validation
				if (!_validator.IsValid(bill))
				{
					RollBackTransaction();
					e = null;
					return false;
				}

				// save
				bill.Save();

				// audit log
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} Vendor Bill: {1} ", AuditLogConstants.AddedNew, bill.DocumentNumber),
					TenantId = processingUser.TenantId,
					User = processingUser,
					EntityCode = bill.EntityName(),
					EntityId = bill.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				// process details
				ProcessVendorBillDetails(bill, processingUser);

				// commit transaction
				CommitTransaction();
			
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				e = ex;
				return false;
			}

			e = null;
			return true;
		}

		private void ProcessVendorBillDetails(VendorBill bill, User processingUser)
		{
			foreach (var i in bill.Details)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				i.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor Bill Number:{2} Detail Ref#:{3}",
												AuditLogConstants.AddedNew, i.EntityName(), bill.DocumentNumber, i.Id),
					TenantId = processingUser.TenantId,
					User = processingUser,
					EntityCode = bill.EntityName(),
					EntityId = bill.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			}
		}
	}
}
