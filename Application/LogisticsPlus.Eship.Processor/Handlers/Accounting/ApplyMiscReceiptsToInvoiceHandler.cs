﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class ApplyMiscReceiptsToInvoiceHandler : EntityBase
    {
        private readonly IApplyMiscReceiptToInvoiceView _view;
        private readonly InvoiceValidator _validator = new InvoiceValidator();
        private readonly MiscReceiptValidator _mrValidator = new MiscReceiptValidator();


        public ApplyMiscReceiptsToInvoiceHandler(IApplyMiscReceiptToInvoiceView view)
		{
			_view = view;
		}


        public void Initialize()
        {
            _view.SaveMiscReceiptApplicationsAndInvoice += OnSaveMiscReceiptApplicationsAndInvoice;
            _view.LoadAndLockMiscReceiptsApplicableToPostedInvoice += OnLoadAndLockMiscReceiptsApplicableToPostedInvoice;
            _view.UnlockInvoiceAndMiscReceipts += OnUnlockInvoiceAndMiscReceipts;
        }

        private void OnUnlockInvoiceAndMiscReceipts(object sender, ViewEventArgs<Invoice, List<MiscReceipt>> e)
        {
            var invoice = e.Argument;
            var miscReceipts = e.Argument2;

            var @lock = invoice.RetrieveLock(_view.ActiveUser, invoice.Id);
            if (@lock != null && @lock.IsUserLock(_view.ActiveUser))
                @lock.Delete();

            foreach (var miscReceipt in miscReceipts)
            {
                var miscReceiptLock = miscReceipt.RetrieveLock(_view.ActiveUser, miscReceipt.Id);
                if (miscReceiptLock != null && miscReceiptLock.IsUserLock(_view.ActiveUser))
                    miscReceiptLock.Delete();
            }
        }

        private void OnLoadAndLockMiscReceiptsApplicableToPostedInvoice(object sender, ViewEventArgs<Invoice> e)
        {
            var invoiceLock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);
            if (!invoiceLock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedMiscReceiptLocks();
                _view.DisplayMessages(new List<ValidationMessage>{ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
                return;
            }
            if (invoiceLock.LockIsExpired()) invoiceLock.Refresh();

            var miscReceipts = new MiscReceiptSearch().FetchMiscReceiptsApplicableToPostedInvoice(e.Argument.Id, _view.ActiveUser.TenantId);

            var msgs = new List<ValidationMessage>();
            var locks = new List<Lock> {invoiceLock};
            foreach (var receipt in miscReceipts.Select(mr => new MiscReceipt(mr.Id)))
            {
                var @lock = receipt.ObtainLock(_view.ActiveUser, receipt.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    msgs.Add(ValidationMessage.Error("Misc Receipt with Gateway Transaction Id:{0} - {1}", receipt.GatewayTransactionId, ProcessorVars.UnableToObtainLockErrMsg)); 
                    continue;
                }
                if (@lock.LockIsExpired()) @lock.Refresh();
                locks.Add(@lock);
            }

            if (msgs.Any())
            {
                foreach (var l in locks) l.Delete();
                _view.FailedMiscReceiptLocks();
                _view.DisplayMessages(msgs);
                return;
            }

            _view.DisplayMiscReceipts(miscReceipts);
        }

        private void OnSaveMiscReceiptApplicationsAndInvoice(object sender, ViewEventArgs<Invoice, List<MiscReceiptApplication>> e)
        {
            var invoice = e.Argument;
            var applications = e.Argument2;

            if (applications.Any(mra => !mra.IsNew))
            {
                _view.DisplayMessages(new []{ValidationMessage.Error("Cannot save misc receipt applications that are not new")});
                return;
            }

            if (invoice.IsNew || !invoice.Posted)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot apply misc receipts to unsaved or unposted invoice") });
                return;
            }

            // validate if the modified misc receipt applications are greater than the misc receipt's paid amount
            var msgs = (from application in applications
                        where
                            application.Amount > application.MiscReceipt.AmountLeftToBeAppliedOrRefunded(application.MiscReceipt.TenantId)
                        select
                            ValidationMessage.Error("Cannot apply an amount greater than misc receipt's amount paid of: {0} to invoice for misc receipt on shipment {1}",
                                application.MiscReceipt.AmountPaid,
                                application.MiscReceipt.Shipment != null
                                    ? application.MiscReceipt.Shipment.ShipmentNumber
                                    : string.Empty)).ToList();
            if (msgs.Any())
            {
                RollBackTransaction();
                _view.DisplayMessages(msgs);
                return;
            }

            // Check that all misc receipt and invoice locks are the user's 
            var locks = new List<Lock>();
            foreach (var application in applications)
            {
                var appLock = application.MiscReceipt.RetrieveLock(_view.ActiveUser, application.MiscReceipt.Id);
                if (!appLock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
                locks.Add(appLock);
            }

            var @lock = invoice.RetrieveLock(_view.ActiveUser, invoice.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }
            locks.Add(@lock);


            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                invoice.Connection = Connection;
                invoice.Transaction = Transaction;

                foreach (var l in locks)
                {
                    l.Connection = Connection;
                    l.Transaction = Transaction;
                }

                foreach (var application in applications)
                {
                    application.Connection = Connection;
                    application.Transaction = Transaction;
                }

                _validator.Messages.Clear();
                if (!_validator.IsValid(invoice))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (!_mrValidator.MiscReceiptApplicationsAreValid(applications))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_mrValidator.Messages);
                    return;
                }

                foreach (var application in applications)
                {
                    application.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} applied to Invoice: {2} for amount: {3}",
                                            AuditLogConstants.AddedNew, application.EntityName(),
                                            application.Invoice.InvoiceNumber, application.Amount),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = application.EntityName(),
                        EntityId = application.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }

                if (invoice.HasChanges())
                {
                    foreach (var change in invoice.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = invoice.EntityName(),
                            EntityId = invoice.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    invoice.Save();
                }

                foreach (var l in locks) l.Delete();

                CommitTransaction();

                _view.DisplayMessages(new[] { ValidationMessage.Information("Invoice amount paid updated") });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }
    }
}
