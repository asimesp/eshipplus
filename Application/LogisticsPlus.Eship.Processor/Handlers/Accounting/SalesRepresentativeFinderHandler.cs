﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class SalesRepresentativeFinderHandler : EntityBase
	{
		private readonly ISalesRepresentativeFinderView _view;

		public SalesRepresentativeFinderHandler(ISalesRepresentativeFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
            var results = new SalesRepresentativeSearch().FetchSalesRepresentatives(e.Argument, _view.ActiveUser.TenantId);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

            _view.DisplaySearchResult(results);
		}
	}
}
