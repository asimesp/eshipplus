﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class CustomerInvoiceDashboardHandler
    {
        private readonly ICustomerInvoiceDashboardView _view;

        public CustomerInvoiceDashboardHandler(ICustomerInvoiceDashboardView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<InvoiceViewSearchCriteria> e)
		{
			var results = new InvoiceSearch().FetchInvoiceDashboardDtos(e.Argument, _view.ActiveUser.TenantId);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.DocumentTags = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].DocumentTags
				.Where(t => t.NonEmployeeVisible)
				.ToList();
		}
    }
}