﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class VendorHandler : EntityBase
	{
		private readonly IVendorView _view;
		private readonly VendorValidator _validator = new VendorValidator();
		private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

		public VendorHandler(IVendorView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.VendorServiceRepSearch += OnVendorServiceRepSearch;
		}

		private void OnVendorServiceRepSearch(object sender, ViewEventArgs<string> e)
		{
			var user = new UserSearch().FetchUserByUsernameAndAccessCode(e.Argument, _view.ActiveUser.Tenant.Code);
			if (user != null && !user.TenantEmployee) user = null;
			_view.DisplayVendorServiceRep(user);
			if (user == null)
				_view.DisplayMessages(new[] {ValidationMessage.Error("Vendor service representative user [{0}] not found!", e.Argument)});

		}

		private void OnDelete(object sender, ViewEventArgs<Vendor> e)
		{
			var vendor = e.Argument;

			if (vendor.IsNew)
			{
				_view.Set(vendor);
				return;
			}

			// load collections
			vendor.LoadCollections();
			foreach (var location in vendor.Locations)
				location.LoadCollections();

			// obtain lock/check lock
			var locks = new List<Lock>();
			if (!ObtainLocksForDelete(locks, vendor))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				vendor.Connection = Connection;
				vendor.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				foreach (var @lock in locks)
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;
				}

				_validator.Messages.Clear();
				if (!_validator.CanDeleteVendor(vendor))
				{
					RollBackTransaction();
					foreach (var @lock in locks) @lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}
				_validator.Messages.Clear();
				if (vendor.Locations.Any(l => !_validator.CanDeleteVendorLocation(l)))
				{
					RollBackTransaction();
					foreach (var @lock in locks) @lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} #{2}", AuditLogConstants.Delete, vendor.Name, vendor.VendorNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// handle collection deletes
				foreach (var location in vendor.Locations)
				{
					location.Connection = Connection;
					location.Transaction = Transaction;

					foreach (var contact in location.Contacts)
					{
						contact.Connection = Connection;
						contact.Transaction = Transaction;

						contact.Delete();
					}

					location.Delete();
				}
				foreach (var insurance in vendor.Insurances)
				{
					insurance.Connection = Connection;
					insurance.Transaction = Transaction;

					insurance.Delete();
				}
				foreach (var vendorPackageCustomMapping in vendor.VendorPackageCustomMappings)
				{
					vendorPackageCustomMapping.Connection = Connection;
					vendorPackageCustomMapping.Transaction = Transaction;

					vendorPackageCustomMapping.Delete();
				}
				foreach (var service in vendor.Services)
				{
					service.Connection = Connection;
					service.Transaction = Transaction;

					service.Delete();
				}
				foreach (var equipment in vendor.Equipments)
				{
					equipment.Connection = Connection;
					equipment.Transaction = Transaction;

					equipment.Delete();
				}
				foreach (var noServiceDay in vendor.NoServiceDays)
				{
					noServiceDay.Connection = Connection;
					noServiceDay.Transaction = Transaction;

					noServiceDay.Delete();
				}

				foreach (var document in vendor.Documents)
				{
					document.Connection = Connection;
					document.Transaction = Transaction;

					document.Delete();
				}

				// purge vendor terminals & peferred lanes
				vendor.PurgeVendorTerminals();
				vendor.PurgeVendorPreferredLanes();

				// delete vendor and lock
				vendor.Delete();

				foreach (var @lock in locks)
					@lock.Delete();

				CommitTransaction();

				_view.Set(new Vendor());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				foreach (var @lock in locks) @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<Vendor> e)
		{
			var vendor = e.Argument;

			// check locks
			if (!vendor.IsNew)
			{
				var @lock = vendor.RetrieveLock(_view.ActiveUser, vendor.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// assign new numbers
			if (vendor.IsNew)
			{
				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.VendorNumber, _view.ActiveUser);
				vendor.VendorNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
			}
			foreach (var location in vendor.Locations)
				if (location.IsNew)
				{
					var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.VendorLocationNumber, _view.ActiveUser);
					location.LocationNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
				}

			// retrieve db vendor for collection comparisons ...
			var dbVendor = new Vendor(vendor.Id, false);
			dbVendor.LoadCollections();
			foreach (var location in dbVendor.Locations) location.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				vendor.Connection = Connection;
				vendor.Transaction = Transaction;

				// validate vendor
				_validator.Messages.Clear();
				if (!_validator.IsValid(vendor))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// audit log new - need vendor id so had to wait till after save
				if (vendor.IsNew)
				{
					// save
					vendor.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, vendor.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (vendor.HasChanges())
				{
					// audit log vendor
					foreach (var change in vendor.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = vendor.EntityName(),
								EntityId = vendor.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					// save
					vendor.Save();
				}

				// process collections
				ProcessInsurances(vendor, dbVendor);
				ProcessPackageCustomMappings(vendor, dbVendor);
				ProcessLocations(vendor, dbVendor);
				ProcessVendorServices(vendor, dbVendor.Services);
				ProcessVendorEquipments(vendor, dbVendor.Equipments);
				ProcessVendorNoServiceDays(vendor, dbVendor.NoServiceDays);
				ProcessDocuments(vendor, dbVendor.Documents);

				// commit transaction
				CommitTransaction();

				// return id in case of new
				_view.Set(vendor);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnUnLock(object sender, ViewEventArgs<Vendor> e)
		{
			var customer = e.Argument;
			var @lock = customer.RetrieveLock(_view.ActiveUser, customer.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<Vendor> e)
		{
			var customer = e.Argument;
			var @lock = customer.ObtainLock(_view.ActiveUser, customer.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<Vendor> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}

		private void OnLoading(object sender, EventArgs e)
		{
            _view.BrokerReferenceTypes = ProcessorUtilities.GetAll<BrokerReferenceNumberType>();
            _view.DateUnits = ProcessorUtilities.GetAll<DateUnit>();
		}

		private void ProcessVendorEquipments(Vendor vendor, IEnumerable<VendorEquipment> dbEquipments)
		{
			var dbDelete = dbEquipments.Where(u => !vendor.Equipments.Select(e => e.EquipmentTypeId).Contains(u.EquipmentTypeId));

			foreach (var e in dbDelete)
			{
				e.Connection = Connection;
				e.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} Vendor:{2} Equipment Id:{3}", AuditLogConstants.Delete, e.EntityName(), vendor.Name,
										  e.EquipmentTypeId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				e.Delete();
			}

			foreach (var e in vendor.Equipments)
				if (!_validator.VendorEquipmentExists(e))
				{
					e.Connection = Connection;
					e.Transaction = Transaction;

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor:{2} Equipment Id:{3}", AuditLogConstants.AddedNew, e.EntityName(),
														vendor.Name, e.EquipmentTypeId),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendor.EntityName(),
							EntityId = vendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					e.Save();
				}
		}

		private void ProcessVendorNoServiceDays(Vendor vendor, IEnumerable<VendorNoServiceDay> dbNoServiceDays)
		{
			var dbDelete = dbNoServiceDays.Where(u => !vendor.NoServiceDays.Select(e => e.NoServiceDayId).Contains(u.NoServiceDayId));

			foreach (var e in dbDelete)
			{
				e.Connection = Connection;
				e.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Vendor:{2} No Service Day Id:{3}", AuditLogConstants.Delete, e.EntityName(), vendor.Name,
									  e.NoServiceDayId),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = vendor.EntityName(),
					EntityId = vendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				e.Delete();
			}

			foreach (var e in vendor.NoServiceDays)
				if (!_validator.VendorNoServiceDayExists(e))
				{
					e.Connection = Connection;
					e.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor:{2} No Service Day Id:{3}", AuditLogConstants.AddedNew, e.EntityName(),
													vendor.Name, e.NoServiceDayId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					e.Save();
				}
		}

		private void ProcessVendorServices(Vendor vendor, IEnumerable<VendorService> dbServices)
		{
			var dbDelete = dbServices.Where(u => !vendor.Services.Select(s => s.ServiceId).Contains(u.ServiceId));

			foreach (var s in dbDelete)
			{
				s.Connection = Connection;
				s.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor:{2} Service Id:{3}", AuditLogConstants.Delete, s.EntityName(), vendor.Name, s.ServiceId),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = vendor.EntityName(),
					EntityId = vendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				s.Delete();
			}

			foreach (var s in vendor.Services)
				if (!_validator.VendorServiceExists(s))
				{
					s.Connection = Connection;
					s.Transaction = Transaction;

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor:{2} Service Id:{3}", AuditLogConstants.AddedNew, s.EntityName(),
														vendor.Name, s.ServiceId),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendor.EntityName(),
							EntityId = vendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					s.Save();
				}
		}

		private void ProcessInsurances(Vendor vendor, Vendor dbVendor)
		{
			var dbDelete = dbVendor.Insurances.Where(i => !vendor.Insurances.Select(vi => vi.Id).Contains(i.Id));

			foreach (var i in dbDelete)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor:{2} Insurance Ref #:{3} Policy #: {4}",
													AuditLogConstants.Delete, i.EntityName(), vendor.Name, i.Id, i.PolicyNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				i.Delete();
			}

			foreach (var i in vendor.Insurances)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				if (i.IsNew)
				{
					i.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor:{2} Insurance Ref #:{3} Policy #: {4}",
							                            AuditLogConstants.AddedNew, i.EntityName(), vendor.Name, i.Id, i.PolicyNumber),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendor.EntityName(),
							EntityId = vendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (i.HasChanges())
				{
					foreach (var change in i.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Ref #{1} {2}", i.EntityName(), i.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = vendor.EntityName(),
								EntityId = vendor.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					i.Save();
				}
			}
		}

		private void ProcessPackageCustomMappings(Vendor vendor, Vendor dbVendor)
		{
			var dbDelete = dbVendor.VendorPackageCustomMappings.Where(i => !vendor.VendorPackageCustomMappings.Select(vi => vi.Id).Contains(i.Id));

			foreach (var i in dbDelete)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor:{2} Package Type Ref #:{3} Code: {4}",
												AuditLogConstants.Delete, i.EntityName(), vendor.Name, i.Id, i.VendorCode),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = vendor.EntityName(),
					EntityId = vendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				i.Delete();
			}

			foreach (var i in vendor.VendorPackageCustomMappings)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				if (i.IsNew)
				{
					i.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor:{2} Package Type Ref #:{3} Code: {4}",
													AuditLogConstants.AddedNew, i.EntityName(), vendor.Name, i.Id, i.VendorCode),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (i.HasChanges())
				{
					foreach (var change in i.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref #{1} {2}", i.EntityName(), i.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendor.EntityName(),
							EntityId = vendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					i.Save();
				}
			}
		}

		private void ProcessLocations(Vendor vendor, Vendor dbVendor)
		{
			var dbDelete = dbVendor.Locations.Where(l => !vendor.Locations.Select(vl => vl.Id).Contains(l.Id));

			foreach (var l in dbDelete)
				if (_validator.CanDeleteVendorLocation(l))
				{
					foreach (var c in l.Contacts)
					{
						c.Connection = Connection;
						c.Transaction = Transaction;

						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor:{2} Location #:{3} Contact Ref: {4}",
														AuditLogConstants.Delete, c.EntityName(), vendor.Name, l.LocationNumber, c.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendor.EntityName(),
							EntityId = vendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

						c.Delete();
					}

					l.Connection = Connection;
					l.Transaction = Transaction;

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor:{2} Location #:{3}",
							                            AuditLogConstants.Delete, l.EntityName(), vendor.Name, l.LocationNumber),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendor.EntityName(),
							EntityId = vendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					l.Delete();
				}

			foreach (var l in vendor.Locations)
			{
				l.Connection = Connection;
				l.Transaction = Transaction;

				if (l.IsNew)
				{
					l.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor:{2} Location #:{3}",
													AuditLogConstants.AddedNew, l.EntityName(), vendor.Name, l.LocationNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (l.HasChanges())
				{
					foreach (var change in l.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} #{1} {2}", l.EntityName(), l.LocationNumber, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendor.EntityName(),
							EntityId = vendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					l.Save();
				}

				ProcessContacts(vendor, l, dbVendor.Locations.FirstOrDefault(dbl => dbl.Id == l.Id));
			}
		}

		private void ProcessContacts(Vendor vendor, VendorLocation location, VendorLocation dbLocation)
		{
			var dbDelete = dbLocation != null
							? dbLocation.Contacts.Where(l => !location.Contacts.Select(vl => vl.Id).Contains(l.Id))
							: new List<VendorContact>();

			foreach (var c in dbDelete)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor:{2} Location #:{3} Contact Ref: {4}",
												AuditLogConstants.Delete, c.EntityName(), vendor.Name, location.LocationNumber, c.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = vendor.EntityName(),
					EntityId = vendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				c.Delete();
			}

			foreach (var c in location.Contacts)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				if (c.IsNew)
				{
					c.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor:{2} Location #:{3} Contact Ref: {4}",
							                            AuditLogConstants.AddedNew, c.EntityName(), vendor.Name, location.LocationNumber,
							                            c.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendor.EntityName(),
							EntityId = vendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (c.HasChanges())
				{
					foreach (var change in c.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("Location #{0} {1} {2}", location.LocationNumber, c.EntityName(), change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = vendor.EntityName(),
								EntityId = vendor.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					c.Save();
				}
			}
		}

		private void ProcessDocuments(Vendor vendor, IEnumerable<VendorDocument> dbDocuments)
		{
			var dbDelete = dbDocuments.Where(dbs => !vendor.Documents.Select(s => s.Id).Contains(dbs.Id));

			foreach (var d in dbDelete)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Vendor Number:{2} Document Ref#: {3}", AuditLogConstants.Delete, d.EntityName(),
									  vendor.VendorNumber, d.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = vendor.EntityName(),
					EntityId = vendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				d.Delete();
			}

			foreach (var d in vendor.Documents)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				if (d.IsNew)
				{
					d.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor Number:{2} Document Ref#:{3}",
													AuditLogConstants.AddedNew, d.EntityName(), vendor.VendorNumber, d.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendor.EntityName(),
						EntityId = vendor.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (d.HasChanges())
				{
					foreach (var change in d.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Document Ref#:{1} {2}", d.EntityName(), d.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendor.EntityName(),
							EntityId = vendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					d.Save();
				}
			}
		}

		private bool ObtainLocksForDelete(ICollection<Lock> locks, Vendor vendor)
		{
			var @lock = vendor.ObtainLock(_view.ActiveUser, vendor.Id);
			if (@lock != null)
			{
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					return false;
				}
				locks.Add(@lock);
			}

			foreach (var vendorService in vendor.Services)
			{
				@lock = vendorService.Service.ObtainLock(_view.ActiveUser, vendorService.Service.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					return false;
				}
				if (@lock != null) locks.Add(@lock);
			}

			foreach (var vendorEquipment in vendor.Equipments)
			{
				@lock = vendorEquipment.EquipmentType.ObtainLock(_view.ActiveUser, vendorEquipment.EquipmentType.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					return false;
				}
				if (@lock != null) locks.Add(@lock);
			}

			return true;
		}
	}
}
