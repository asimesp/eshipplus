﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
    public class SalesRepresentativeHandler : EntityBase
    {
        private readonly ISalesRepresentativeView _view;
        private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

        private readonly SalesRepresentativeValidator _validator = new SalesRepresentativeValidator();

        public SalesRepresentativeHandler(ISalesRepresentativeView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.LoadAuditLog += OnLoadAuditLog;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.CustomerSearch += OnCustomerSearch;
            _view.BatchImport += OnBatchImport;
        }

        private void OnBatchImport(object sender, ViewEventArgs<List<SalesRepresentative>> e)
        {
            var messages = new ValidationMessageCollection();
            foreach (var salesRepresentative in e.Argument)
            {
                _validator.Messages.Clear();
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    salesRepresentative.Connection = Connection;
                    salesRepresentative.Transaction = Transaction;

                    if (!_validator.IsValid(salesRepresentative))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }
                    salesRepresentative.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, salesRepresentative.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = salesRepresentative.EntityName(),
                        EntityId = salesRepresentative.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    ProcessSalesRepCommTiers(salesRepresentative, new List<SalesRepCommTier>());

                    CommitTransaction();
                    
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
                }
            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }

        private void OnUnLock(object sender, ViewEventArgs<SalesRepresentative> e)
        {
            var salesRepresentative = e.Argument;
            var @lock = salesRepresentative.RetrieveLock(_view.ActiveUser, salesRepresentative.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.ServiceModes = ProcessorUtilities.GetAll<ServiceMode>();
        }

        private void OnLock(object sender, ViewEventArgs<SalesRepresentative> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnDelete(object sender, ViewEventArgs<SalesRepresentative> e)
        {
            _validator.Messages.Clear();

            var salesRepresentative = e.Argument;

            if (salesRepresentative.IsNew) return;

            //load collections
            salesRepresentative.LoadCollections();

            var @lock = salesRepresentative.ObtainLock(_view.ActiveUser, salesRepresentative.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                salesRepresentative.Connection = Connection;
                salesRepresentative.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.CanDeleteSalesRepresentative(salesRepresentative))
                {
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                foreach (var cTier in salesRepresentative.SalesRepCommTiers)
                {
                    cTier.Connection = Connection;
                    cTier.Transaction = Transaction;

                    cTier.Delete();
                }

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, salesRepresentative.Name, salesRepresentative.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = salesRepresentative.EntityName(),
                        EntityId = salesRepresentative.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                salesRepresentative.Delete();
                @lock.Delete();

                CommitTransaction();

                _view.Set(new SalesRepresentative());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSave(object sender, ViewEventArgs<SalesRepresentative> e)
        {
            var salesRepresentative = e.Argument;

            if (!salesRepresentative.IsNew)
            {
                var @lock = salesRepresentative.RetrieveLock(_view.ActiveUser, salesRepresentative.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }
            else
            {
                var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.SalesRepresentativeNumber, _view.ActiveUser);
                salesRepresentative.SalesRepresentativeNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
            }

            var dbSalesRep = new SalesRepresentative(salesRepresentative.Id, false);
            dbSalesRep.LoadCollections();

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                salesRepresentative.Connection = Connection;
                salesRepresentative.Transaction = Transaction;

                if (!_validator.IsValid(salesRepresentative))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (salesRepresentative.IsNew)
                {
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, salesRepresentative.Name),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = salesRepresentative.EntityName(),
                            EntityId = salesRepresentative.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    salesRepresentative.Save();
                }
                else if (salesRepresentative.HasChanges())
                {
                    foreach (var change in salesRepresentative.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = salesRepresentative.EntityName(),
                                EntityId = salesRepresentative.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                    salesRepresentative.Save();
                }

                ProcessSalesRepCommTiers(salesRepresentative, dbSalesRep.SalesRepCommTiers);

                CommitTransaction();
                _view.Set(salesRepresentative);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void ProcessSalesRepCommTiers(SalesRepresentative salesRep, IEnumerable<SalesRepCommTier> dbSalesRepCommTiers)
        {
            var dbDelete = dbSalesRepCommTiers.Where(cTier => !salesRep.SalesRepCommTiers.Select(ct => ct.Id).Contains(cTier.Id));
            foreach (var cTier in dbDelete)
            {
                cTier.Connection = Connection;
                cTier.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Sales Representative Ref#{2} Commission Tier Ref#{3}",
                                                AuditLogConstants.Delete, cTier.EntityName(), salesRep.Id, cTier.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = salesRep.EntityName(),
                    EntityId = salesRep.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                cTier.Delete();
            }

            foreach (var cTier in salesRep.SalesRepCommTiers)
            {
                cTier.Connection = Connection;
                cTier.Transaction = Transaction;

                if (cTier.IsNew)
                {
                    cTier.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Sales Representative Ref#{2} Commission Tier Ref#{3}",
                                                    AuditLogConstants.AddedNew, cTier.EntityName(), salesRep.Id, cTier.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = salesRep.EntityName(),
                        EntityId = salesRep.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (cTier.HasChanges())
                {
                    foreach (var change in cTier.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Ref#{1} {2}", cTier.EntityName(), cTier.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = salesRep.EntityName(),
                            EntityId = salesRep.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    cTier.Save();
                }
            }
        }

        private void OnCustomerSearch(Object sender, ViewEventArgs<string> e)
        {
            if (!_view.ActiveUser.TenantEmployee)
                FetchCustomerForNonEmployee(e.Argument);
            else
                FetchCustomerForEmployee(e.Argument);
        }

        private void OnLoadAuditLog(object sender, ViewEventArgs<SalesRepresentative> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
                                                                             e.Argument.TenantId));
        }

        private void FetchCustomerForNonEmployee(string customerNumber)
        {
            if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
            {
                _view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
                return;
            }
            var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
            if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
            else _view.DisplayCustomer(shipAs.Customer);
        }

        private void FetchCustomerForEmployee(string customerNumber)
        {
            var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

            _view.DisplayCustomer(customer ?? new Customer());
            if (customer == null)
                _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
        }
    }
}
