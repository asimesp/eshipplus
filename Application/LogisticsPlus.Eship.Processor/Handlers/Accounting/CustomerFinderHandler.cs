﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class CustomerFinderHandler : EntityBase
	{
		private readonly ICustomerFinderView _view;

		public CustomerFinderHandler(ICustomerFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

        private void OnSearch(object sender, ViewEventArgs<CustomerViewSearchCriteria> e)
		{
			var results = _view.ActiveUser.TenantEmployee
							? FetchCustomerForEmployee(e.Argument)
							: FetchCustomerForNonEmployee(e.Argument);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}

        private List<CustomerViewSearchDto> FetchCustomerForNonEmployee(CustomerViewSearchCriteria criteria)
        {
            var customers = new CustomerSearch().FetchCustomerViewSearchDtos(criteria, _view.ActiveUser.TenantId);

			var uCustomers = _view.ActiveUser.RetrieveShipAsCustomers();

			return customers
				.Where(c => uCustomers.Select(uc => uc.Id).Contains(c.Id) || c.Id == _view.ActiveUser.DefaultCustomer.Id)
				.ToList();
		}

        private List<CustomerViewSearchDto> FetchCustomerForEmployee(CustomerViewSearchCriteria criteria)
		{
            return new CustomerSearch().FetchCustomerViewSearchDtos(criteria, _view.ActiveUser.TenantId);
		}
	}
}
