﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Accounting
{
	public class InvoiceControlAccountUpdatesHandler : EntityBase
	{
		private readonly IInvoiceControlAccountUpdatesView _view;
        private readonly InvoiceValidator _validator = new InvoiceValidator();

        public InvoiceControlAccountUpdatesHandler(IInvoiceControlAccountUpdatesView view)
        {
            _view = view;
        }

        public void Initialize()
        {
			_view.RetrieveRecordsToUpdate += OnRetrieveRecordsToUpdate;
            _view.Save += OnSave;
        }

		private void OnRetrieveRecordsToUpdate(object sender, EventArgs e)
		{
			_view.DisplayRecordsToUpdate(new InvoiceSearch().FetchInvoicesNotPostedWithoutControlAccounts(_view.ActiveUser.TenantId));
		}

		private void OnSave(object sender, ViewEventArgs<List<Invoice>> e)
		{
			var messages = new List<ValidationMessage>();

			foreach (var invoice in e.Argument)
			{
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					// link up
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					invoice.Connection = Connection;
					invoice.Transaction = Transaction;

					//Check if Invoice exists
					if (invoice.IsNew)
					{
						messages.Add(ValidationMessage.Error("Error [Inv #: {0}] - Invoice does not exist!", invoice.InvoiceNumber));
						RollBackTransaction();
						continue;
					}

					var @lock = invoice.ObtainLock(_view.ActiveUser, invoice.Id);
					if (!@lock.IsUserLock(_view.ActiveUser))
					{
						messages.Add(ValidationMessage.Error("Error [Inv #: {0}] - {1}", invoice.InvoiceNumber,
						                                     ProcessorVars.UnableToObtainLockErrMsg));
						RollBackTransaction();
						continue;
					}
					// link up lock
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					// validate invoice
					_validator.Messages.Clear();
					if (!_validator.IsValid(invoice))
					{
						var msg = _validator.Messages
							.Select(m => string.Format("{0}: {1}", m.Type, m.Message))
							.ToArray()
							.CommaJoin();
						messages.Add(ValidationMessage.Error("Error [Inv #: {0}] - {1}", invoice.InvoiceNumber, msg));
						RollBackTransaction();
						@lock.Delete(); // release lock!
						continue;
					}
					if (invoice.Posted)
					{
						messages.Add(ValidationMessage.Error("Error [Inv #: {0}] - Invoice already posted!", invoice.InvoiceNumber));
						RollBackTransaction();
						@lock.Delete(); // release lock!
						continue;
					}

					// audit log invoice
					foreach (var change in invoice.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = invoice.EntityName(),
								EntityId = invoice.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					// save record 
					invoice.Save();

					// release locks
					@lock.Delete();

					// update message
					messages.Add(ValidationMessage.Information("Invoice [Inv #: {0}] updated!", invoice.InvoiceNumber));

					// commit transaction
					CommitTransaction();
				}
				catch (Exception ex)
				{
					RollBackTransaction();
					_view.LogException(ex);
					messages.Add(ValidationMessage.Error("Error [Inv #: {0}] - Error processing record! Error Notification Sent.",
					                                     invoice.InvoiceNumber));

				}
			}

			// handle messages
			_view.DisplayUpdateMessages(messages);
			_view.DisplayMessages(new[] {ValidationMessage.Information("Mass update process complete!")});
		}
	}
}
