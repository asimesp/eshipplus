﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Validation;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Cms
{
	public class NewsHiddenStatusUpdateHandler : EntityBase
	{
		private readonly AdminUser _user;

		public NewsHiddenStatusUpdateHandler(AdminUser user)
		{
			if (user==null)
				throw new ArgumentNullException("user");

			_user = user;
		}

		public ValidationMessage SetAndSaveNewsArchivedStatus(long newsId, bool status, out Exception ex)
		{
			var news = new News(newsId, newsId != default(long)) { Hidden = status };

			if (!news.KeyLoaded)
			{
				ex = null;
				return ValidationMessage.Error("Unable to find news with specified ID [value: {0}]", newsId);
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				news.Connection = Connection;
				news.Transaction = Transaction;

				if (news.HasChanges())
				{
					foreach(var change in news.Changes())
					new AdminAuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = change,
						EntityCode = news.EntityName(),
						EntityId = news.Id.ToString(),
						LogDateTime = DateTime.Now,
						AdminUser = _user,
					}.Log();
					news.Save();
				}

				CommitTransaction();
				ex = null;
				return ValidationMessage.Information("Record hidden status updated.");

			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
				return ValidationMessage.Error("An error occurred while updating record hidden status.");
			}
		}
	}
}
