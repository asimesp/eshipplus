﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Cms;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Cms;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Cms
{
	public class NewsHandler : EntityBase
	{
		private readonly INewsView _view;

		private readonly NewsValidator _validator = new NewsValidator();

		public NewsHandler(INewsView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSaveNews;
			_view.Delete += OnDeleteNews;
		}

		private void OnSaveNews(object sender, ViewEventArgs<News> e)
		{
			var news = e.Argument;

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				if (!_validator.IsValid(news))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}
				
				if (news.IsNew)
				{
					news.Save();

					new AdminAuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} news: Key = {1}", AuditLogConstants.AddedNew, news.NewsKey),
						EntityCode = news.EntityName(),
						EntityId = news.Id.ToString(),
						LogDateTime = DateTime.Now,
						AdminUser = _view.ActiveSuperUser,
					}.Log();
				}
				else if (news.HasChanges())
				{
						new AdminAuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} news: Key = {1}", AuditLogConstants.Update, news.NewsKey),
							EntityCode = news.EntityName(),
							EntityId = news.Id.ToString(),
							LogDateTime = DateTime.Now,
							AdminUser = _view.ActiveSuperUser,
						}.Log();
					news.Save();
				}

				CommitTransaction();

				_view.Set(news);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDeleteNews(object sender, ViewEventArgs<News> e)
		{
			var news = e.Argument;

			if (news.IsNew)
			{
				_view.Set(new News());
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				news.Connection = Connection;
				news.Transaction = Transaction;

				new AdminAuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} news: Key = {1}", AuditLogConstants.Delete, news.NewsKey),
					EntityCode = news.EntityName(),
					EntityId = news.Id.ToString(),
					LogDateTime = DateTime.Now,
					AdminUser = _view.ActiveSuperUser
				}.Log();

				news.Delete();

				CommitTransaction();
				_view.Set(new News());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });

			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}
	}
}
