﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Searches.Cms;
using LogisticsPlus.Eship.Processor.Validation;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Cms
{
	public class CurrentContentUpdateHandler : EntityBase
	{
		private readonly AdminUser _user;

		public CurrentContentUpdateHandler(AdminUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			_user = user;
		}

		public ValidationMessage SetandSaveContentCurrentStatus(long contentId, bool status, out Exception ex)
		{
			var content = new Content(contentId, contentId != default(long)) {Current = status};

			if (!content.KeyLoaded)
			{
				ex = null;
				return ValidationMessage.Error("Unable to find content with specified ID [value: {0}]", contentId);
			}

			// check for previous current record
			Content previousCurrent = null;
			if (content.Current)
				previousCurrent = new ContentSearch().FetchContentByIdentifierCode(content.IdentifierCode)
					.FirstOrDefault(c => c.Current && c.Id != content.Id);		

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				content.Connection = Connection;
				content.Transaction = Transaction;

				if (previousCurrent != null)
				{
					previousCurrent.Connection = Connection;
					previousCurrent.Transaction = Transaction;

					previousCurrent.TakeSnapShot();
					previousCurrent.Current = false;
					foreach (var change in previousCurrent.Changes())
						new AdminAuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							EntityCode = previousCurrent.EntityName(),
							EntityId = previousCurrent.Id.ToString(),
							LogDateTime = DateTime.Now,
							AdminUser = _user,
						}.Log();
					previousCurrent.Save();
				}

				if (content.HasChanges())
				{
					foreach (var change in content.Changes())
						new AdminAuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								EntityCode = content.EntityName(),
								EntityId = content.Id.ToString(),
								LogDateTime = DateTime.Now,
								AdminUser = _user,
							}.Log();
					content.Save();
				}

				CommitTransaction();
				ex = null;
				return ValidationMessage.Information("Record current status updated.");

			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
				return ValidationMessage.Error("An error occurred while updating record current status.");
			}
		}
	}
}
