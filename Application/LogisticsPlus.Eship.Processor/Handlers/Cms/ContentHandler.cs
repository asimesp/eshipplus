﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Searches.Cms;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Cms;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Cms;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Cms
{
	public class ContentHandler: EntityBase
	{
		private readonly IContentView _view;

		private readonly ContentValidator _validator = new ContentValidator();

		public ContentHandler(IContentView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSaveContent;
			_view.Delete += OnDeleteContent;
		}

		private void OnSaveContent(object sender, ViewEventArgs<Content> e)
		{
			var content = e.Argument;

			// check for previous current record
			Content previousCurrent = null;
			if (content.Current)
				previousCurrent = new ContentSearch().FetchContentByIdentifierCode(content.IdentifierCode)
					.FirstOrDefault(c => c.Current && c.Id != content.Id);

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				content.Connection = Connection;
				content.Transaction = Transaction;

				if (!_validator.IsValid(content))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (previousCurrent != null)
				{
					previousCurrent.Connection = Connection;
					previousCurrent.Transaction = Transaction;

					previousCurrent.TakeSnapShot();
					previousCurrent.Current = false;
					foreach (var change in previousCurrent.Changes())
						new AdminAuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								EntityCode = previousCurrent.EntityName(),
								EntityId = previousCurrent.Id.ToString(),
								LogDateTime = DateTime.Now,
								AdminUser = _view.ActiveSuperUser,
							}.Log();
					previousCurrent.Save();
				}

				if (content.IsNew)
				{
					content.Save();

					new AdminAuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} content: Key = {1}", AuditLogConstants.AddedNew, content.ContentKey),
						EntityCode = content.EntityName(),
						EntityId = content.Id.ToString(),
						LogDateTime = DateTime.Now,
						AdminUser = _view.ActiveSuperUser,
					}.Log();
				}
				else if (content.HasChanges())
				{
						new AdminAuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} content: Key = {1}", AuditLogConstants.Update, content.ContentKey),
							EntityCode = content.EntityName(),
							EntityId = content.Id.ToString(),
							LogDateTime = DateTime.Now,
							AdminUser = _view.ActiveSuperUser,
						}.Log();
					content.Save();
				}

				CommitTransaction();

				_view.Set(content);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });

			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });

			}
		}

		private void OnDeleteContent(object sender, ViewEventArgs<Content> e)
		{
			var content = e.Argument;
			
			if (content.IsNew)
			{
				_view.Set(new Content());
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				content.Connection = Connection;
				content.Transaction = Transaction;

				new AdminAuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} content: Key = {1}", AuditLogConstants.Delete, content.ContentKey),
						EntityCode = content.EntityName(),
						EntityId = content.Id.ToString(),
						LogDateTime = DateTime.Now,
						AdminUser = _view.ActiveSuperUser
					}.Log();

				content.Delete();

				CommitTransaction();
				_view.Set(new Content());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
				
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}
	}
}
