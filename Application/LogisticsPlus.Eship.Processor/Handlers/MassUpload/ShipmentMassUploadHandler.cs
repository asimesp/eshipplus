﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.MassUpload;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.MassUpload
{
	public class ShipmentMassUploadHandler : EntityBase
	{
		private readonly IShipmentMassUploadView _view;
		private readonly ShipmentValidator _validator = new ShipmentValidator();


		public ShipmentMassUploadHandler(IShipmentMassUploadView view)
		{
			_view = view;
		}


		public void Initialize()
		{
			_view.Save += OnSave;
		}


		private void OnSave(object sender, ViewEventArgs<ShipmentDocument> e)
		{
			var document = e.Argument;
			var shipment = document.Shipment;

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				var @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					RollBackTransaction();
					var msgs = new[]
						{
							ValidationMessage.Error(string.Format("{0} [Number: {1}]", ProcessorVars.UnableToObtainLockErrMsg,
							                                      shipment.ShipmentNumber))
						};
					_view.ProcessFailedShimpentDocumentSave(document, msgs);
					return;
				}

				document.Connection = Connection;
				document.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				if (!_validator.ShipmentDocumentsAreValid(new[] {document}))
				{
					RollBackTransaction();
					_view.ProcessFailedShimpentDocumentSave(document, _validator.Messages);
					return;
				}

				//save
				document.Save();

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Document Name:{3}",
						                            AuditLogConstants.AddedNew, document.EntityName(), shipment.ShipmentNumber,
						                            document.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				@lock.Delete();
				CommitTransaction();
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
				var msgs = new[] {ValidationMessage.Error("Error adding shipment document. [Shipment #: {0}, Error: {1}", shipment.ShipmentNumber, ex.Message)};
				_view.ProcessFailedShimpentDocumentSave(document, msgs);
			}
		}
	}
}
