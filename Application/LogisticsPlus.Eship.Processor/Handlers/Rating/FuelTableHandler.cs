﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
	public class FuelTableHandler : EntityBase
	{
		private readonly IFuelTableView _view;
		private readonly FuelTableValidator _validator = new FuelTableValidator();

		public FuelTableHandler(IFuelTableView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
		}

		private void OnDelete(object sender, ViewEventArgs<FuelTable> e)
		{
			var fuelTable = e.Argument;

			if (fuelTable.IsNew)
			{
				_view.Set(new FuelTable());
				return;
			}

			// load collections
			fuelTable.LoadCollections();

			// obtain lock/check lock
			var @lock = fuelTable.ObtainLock(_view.ActiveUser, fuelTable.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				fuelTable.Connection = Connection;
				fuelTable.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if(!_validator.CanDeleteFuelTable(fuelTable))
				{
					@lock.Delete();
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Ref#{2}", AuditLogConstants.Delete, fuelTable.Name, fuelTable.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = fuelTable.EntityName(),
						EntityId = fuelTable.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// handle collection deletes
				foreach (var index in fuelTable.FuelIndices)
				{
					index.Connection = Connection;
					index.Transaction = Transaction;

					index.Delete();
				}

				// delete fuel table and lock
				fuelTable.Delete();

				@lock.Delete();

				CommitTransaction();

				_view.Set(new FuelTable());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<FuelTable> e)
		{
			var fuelTable = e.Argument;

			// check locks
			if (!fuelTable.IsNew)
			{
				var @lock = fuelTable.RetrieveLock(_view.ActiveUser, fuelTable.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
					return;
				}
			}

			// retrieve db fuel table for collection comparisons ...
			var dbFuelTable = new FuelTable(fuelTable.Id, false);
			dbFuelTable.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				fuelTable.Connection = Connection;
				fuelTable.Transaction = Transaction;

				// validate fuel table
				_validator.Messages.Clear();
				if (!_validator.IsValid(fuelTable))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// audit log fuel table
				if (fuelTable.IsNew)
				{
					// save
					fuelTable.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, fuelTable.Name),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = fuelTable.EntityName(),
							EntityId = fuelTable.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (fuelTable.HasChanges())
				{
					foreach (var change in fuelTable.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = fuelTable.EntityName(),
								EntityId = fuelTable.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					// save
					fuelTable.Save();
				}


				// process collections
				ProcessIndices(fuelTable, dbFuelTable);

				// commit transaction
				CommitTransaction();

				// return id in case of new
				_view.Set(fuelTable);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnUnLock(object sender, ViewEventArgs<FuelTable> e)
		{
			var fuelTable = e.Argument;
			var @lock = fuelTable.RetrieveLock(_view.ActiveUser, fuelTable.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg)});
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<FuelTable> e)
		{
			var fuelTable = e.Argument;
			var @lock = fuelTable.ObtainLock(_view.ActiveUser, fuelTable.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<FuelTable> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
		}

		private void ProcessIndices(FuelTable fuelTable, FuelTable dbFuelTable)
		{
			var dbDelete = dbFuelTable.FuelIndices.Where(i => !fuelTable.FuelIndices.Select(dbi => dbi.Id).Contains(i.Id));

			foreach (var index in dbDelete)
			{
				index.Connection = Connection;
				index.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Fuel Table:{2} Ref #:{3}",
						                            AuditLogConstants.Delete, index.EntityName(), fuelTable.Name, index.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = fuelTable.EntityName(),
						EntityId = fuelTable.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				index.Delete();
			}

			foreach (var index in fuelTable.FuelIndices)
			{
				index.Connection = Connection;
				index.Transaction = Transaction;

				if (index.IsNew)
				{
					index.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Fuel Table:{2} Ref #:{3}",
							                            AuditLogConstants.AddedNew, index.EntityName(), fuelTable.Name, index.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = fuelTable.EntityName(),
							EntityId = fuelTable.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (index.HasChanges())
				{
					foreach (var change in index.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("Fuel Table {0} Ref #{1} {2}", index.EntityName(), index.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = fuelTable.EntityName(),
								EntityId = fuelTable.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					index.Save();
				}
			}
		}
	}
}
