﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.SmallPacks;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
	public class CustomerRatingHandler : EntityBase
	{
		private readonly ICustomerRatingView _view;
		private readonly CustomerRatingValidator _validator = new CustomerRatingValidator();

		public CustomerRatingHandler(ICustomerRatingView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.VendorSearch += OnVendorSearch;
			_view.CustomerSearch += OnCustomerSearch;
		}

		private void OnSave(object sender, ViewEventArgs<CustomerRating> e)
		{
			var rating = e.Argument;

			// check locks
			if (!rating.IsNew)
			{
				var @lock = rating.RetrieveLock(_view.ActiveUser, rating.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// retrieve db customer rating for collection comparisons ...
			var dbRating = new CustomerRating(rating.Id, false);
			dbRating.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				rating.Connection = Connection;
				rating.Transaction = Transaction;

				// validate customer rating
				_validator.Messages.Clear();
				if (!_validator.IsValid(rating))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// get lock on customer and link up
				var cLock = rating.Customer.ObtainLock(_view.ActiveUser, rating.Customer.Id);
				if (!cLock.IsUserLock(_view.ActiveUser))
				{
					RollBackTransaction();
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
				cLock.Connection = Connection;
				cLock.Transaction = Transaction;

				//get lock on dbRating customer (previous link up
				Lock cpLock = null;
				if (!rating.IsNew && dbRating.Customer.Id != rating.Customer.Id)
				{
					cpLock = dbRating.Customer.ObtainLock(_view.ActiveUser, dbRating.Customer.Id);
					if (!cpLock.IsUserLock(_view.ActiveUser))
					{
						RollBackTransaction();
						_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
						return;
					}
					cpLock.Connection = Connection;
					cpLock.Transaction = Transaction;
				}

				// audit log customer rating
				if (rating.IsNew)
				{
					// save
					rating.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Customer #:{1}", AuditLogConstants.AddedNew, rating.Customer.CustomerNumber),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (rating.HasChanges())
				{
					foreach (var change in rating.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.EntityName(),
								EntityId = rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					// save
					rating.Save();
				}

				// check change on previous link up
				if (cpLock != null)
				{
					dbRating.Customer.TakeSnapShot();
					dbRating.Customer.Connection = Connection;
					dbRating.Customer.Transaction = Transaction;
					dbRating.Customer.Rating = null;
					if (dbRating.Customer.HasChanges())
					{
						foreach (var change in dbRating.Customer.Changes())
							new AuditLog
								{
									Connection = Connection,
									Transaction = Transaction,
									Description = change,
									TenantId = _view.ActiveUser.TenantId,
									User = _view.ActiveUser,
									EntityCode = dbRating.Customer.EntityName(),
									EntityId = dbRating.Customer.Id.ToString(),
									LogDateTime = DateTime.Now
								}.Log();
						dbRating.Customer.Save();
					}

					// release lock (previous linkup)
					cpLock.Delete();
				}

				// ensure link is present with customer and save
				rating.Customer.TakeSnapShot();
				rating.Customer.Connection = Connection;
				rating.Customer.Transaction = Transaction;
				rating.Customer.Rating = rating;
				if (rating.Customer.HasChanges())
				{
					foreach (var change in rating.Customer.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.Customer.EntityName(),
								EntityId = rating.Customer.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					rating.Customer.Save();
				}

				// release customer lock
				cLock.Delete();

				// process collections
				ProcessLTLSellRates(rating, dbRating.LTLSellRates);
				ProcessTLSellRates(rating, dbRating.TLSellRates);
				ProcessSmallPackRates(rating, dbRating.SmallPackRates);
				ProcessCustomerServiceMarkups(rating, dbRating.CustomerServiceMarkups);

				// commit transaction
				CommitTransaction();

				// remove from cache to force reload
				ProcessorVars.RaterCache.Drop(rating);

				// return id in case of new
				_view.Set(rating);
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDelete(object sender, ViewEventArgs<CustomerRating> e)
		{
			var rating = e.Argument;

			if (rating.IsNew)
			{
				_view.Set(new CustomerRating());
				return;
			}

			// load collections
			rating.LoadCollections();

			// obtain lock/check lock
			var @lock = rating.ObtainLock(_view.ActiveUser, rating.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}
			var cLock = rating.Customer.ObtainLock(_view.ActiveUser, rating.Customer.Id);
			if (!cLock.IsUserLock(_view.ActiveUser))
			{
				@lock.Delete();
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				rating.Connection = Connection;
				rating.Transaction = Transaction;

				rating.Customer.Connection = Connection;
				rating.Customer.Transaction = Transaction;

				cLock.Connection = Connection;
				cLock.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Customer #{1} Rating Ref#{2}", AuditLogConstants.Delete,
													rating.Customer.CustomerNumber, rating.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = rating.EntityName(),
						EntityId = rating.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// handle collection deletes
				foreach (var rate in rating.LTLSellRates)
				{
					rate.Connection = Connection;
					rate.Transaction = Transaction;

					rate.Delete();
				}
				foreach (var rate in rating.TLSellRates)
				{
					rate.Connection = Connection;
					rate.Transaction = Transaction;

					rate.Delete();
				}
				foreach (var rate in rating.SmallPackRates)
				{
					rate.Connection = Connection;
					rate.Transaction = Transaction;

					rate.Delete();
				}
				foreach (var markup in rating.CustomerServiceMarkups)
				{
					markup.Connection = Connection;
					markup.Transaction = Transaction;

					markup.Delete();
				}

				// remove customer association
				rating.Customer.TakeSnapShot();
				rating.Customer.Rating = null;
				foreach (var change in rating.Customer.Changes())
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.Customer.EntityName(),
							EntityId = rating.Customer.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				rating.Customer.Save();


				// delete customer and lock
				rating.Delete();

				// remove locks
				@lock.Delete();
				cLock.Delete();

				CommitTransaction();

				// remove from cache to force reload
				ProcessorVars.RaterCache.Drop(rating);

				_view.Set(new CustomerRating());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(e.Argument, _view.ActiveUser.TenantId);

			_view.DisplayCustomer(customer ?? new Customer());
			if (customer == null)
				_view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
		}

		private void OnVendorSearch(object sender, ViewEventArgs<string> e)
		{
			var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);

			_view.DisplayVendor(vendor ?? new Vendor());
			if (vendor == null)
				_view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
		}

		private void OnUnLock(object sender, ViewEventArgs<CustomerRating> e)
		{
			var rating = e.Argument;
			var @lock = rating.RetrieveLock(_view.ActiveUser, rating.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<CustomerRating> e)
		{
			var rating = e.Argument;
			var @lock = rating.ObtainLock(_view.ActiveUser, rating.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<CustomerRating> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}

		private void OnLoading(object sender, EventArgs e)
		{
            _view.ResellerAdditions = new ResellerAdditionSearch().FetchResellerAdditions(new List<ParameterColumn>(), _view.ActiveUser.TenantId);
            _view.VendorRatings = new VendorRatingSearch().FetchVendorRatings(new VendorRatingViewSearchCriteria(), _view.ActiveUser.TenantId);

            _view.RateType = ProcessorUtilities.GetAll<RateType>();
			_view.SmallPackEngines = GetSmallPackEngineTypes();
			_view.SmallPackTypes = SmallPacksFactory.RetrieveServiceTypes();
            _view.TariffType = ProcessorUtilities.GetAll<TLTariffType>();
			_view.ValuePercentageTypes = ProcessorUtilities.GetAll<ValuePercentageType>();
			_view.WeightBreaks = ProcessorUtilities.GetAll<WeightBreak>();
		}

		private static Dictionary<int, string> GetSmallPackEngineTypes()
		{
			return new Dictionary<int, string>
			       	{
			       		{(int)SmallPackageEngine.FedEx, SmallPackageEngine.FedEx.ToString().FormattedString()},
			       		{(int)SmallPackageEngine.Ups, SmallPackageEngine.Ups.ToString().FormattedString()}
			       	};
		}

		private void ProcessLTLSellRates(CustomerRating rating, IEnumerable<LTLSellRate> dbRates)
		{
			var dbDelete = dbRates.Where(rate => !rating.LTLSellRates.Select(r => r.Id).Contains(rate.Id));

			foreach (var rate in dbDelete)
			{
				rate.Connection = Connection;
				rate.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Customer Rating Ref#{2} LTL Sell Rate Ref#{3}",
													AuditLogConstants.Delete, rate.EntityName(), rating.Id, rate.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = rating.EntityName(),
						EntityId = rating.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				rate.Delete();
			}

			foreach (var rate in rating.LTLSellRates)
			{
				rate.Connection = Connection;
				rate.Transaction = Transaction;

				if (rate.IsNew)
				{
					rate.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Customer Rating Ref#{2} LTL Sell Rate Ref#{3}",
														AuditLogConstants.AddedNew, rate.EntityName(), rating.Id, rate.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (rate.HasChanges())
				{
					foreach (var change in rate.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref#{1} {2}", rate.EntityName(), rate.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					rate.Save();
				}
			}
		}

		private void ProcessTLSellRates(CustomerRating rating, IEnumerable<TLSellRate> dbRates)
		{
			var dbDelete = dbRates.Where(rate => !rating.TLSellRates.Select(r => r.Id).Contains(rate.Id));

			foreach (var rate in dbDelete)
			{
				rate.Connection = Connection;
				rate.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Customer Rating Ref#{2} TL Sell Rate Ref#{3}",
												AuditLogConstants.Delete, rate.EntityName(), rating.Id, rate.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = rating.EntityName(),
					EntityId = rating.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				rate.Delete();
			}

			foreach (var rate in rating.TLSellRates)
			{
				rate.Connection = Connection;
				rate.Transaction = Transaction;

				if (rate.IsNew)
				{
					rate.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Customer Rating Ref#{2} TL Sell Rate Ref#{3}",
													AuditLogConstants.AddedNew, rate.EntityName(), rating.Id, rate.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = rating.EntityName(),
						EntityId = rating.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (rate.HasChanges())
				{
					foreach (var change in rate.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref#{1} {2}", rate.EntityName(), rate.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					rate.Save();
				}
			}
		}

		private void ProcessSmallPackRates(CustomerRating rating, IEnumerable<SmallPackRate> dbRates)
		{
			var dbDelete = dbRates.Where(rate => !rating.SmallPackRates.Select(r => r.Id).Contains(rate.Id));

			foreach (var rate in dbDelete)
			{
				rate.Connection = Connection;
				rate.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Customer Rating Ref#{2} Small Pack Rate Ref#{3}",
												AuditLogConstants.Delete, rate.EntityName(), rating.Id, rate.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = rating.EntityName(),
					EntityId = rating.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				rate.Delete();
			}

			foreach (var rate in rating.SmallPackRates)
			{
				rate.Connection = Connection;
				rate.Transaction = Transaction;

				if (rate.IsNew)
				{
					rate.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Customer Rating Ref#{2} Small Pack Rate Ref#{3}",
														AuditLogConstants.AddedNew, rate.EntityName(), rating.Id, rate.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (rate.HasChanges())
				{
					foreach (var change in rate.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Ref#{1} {2}", rate.EntityName(), rate.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.EntityName(),
								EntityId = rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					rate.Save();
				}
			}
		}

		private void ProcessCustomerServiceMarkups(CustomerRating rating, IEnumerable<CustomerServiceMarkup> dbMarkups)
		{
			var dbDelete = dbMarkups.Where(markup => !rating.CustomerServiceMarkups.Select(m => m.Id).Contains(markup.Id));

			foreach (var rate in dbDelete)
			{
				rate.Connection = Connection;
				rate.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Customer Rating Ref#{2} Customer Service Markup Ref#{3}",
												AuditLogConstants.Delete, rate.EntityName(), rating.Id, rate.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = rating.EntityName(),
					EntityId = rating.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				rate.Delete();
			}

			foreach (var markup in rating.CustomerServiceMarkups)
			{
				markup.Connection = Connection;
				markup.Transaction = Transaction;

				if (markup.IsNew)
				{
					markup.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Customer Rating Ref#{2} Customer Service Markup Ref#{3}",
														AuditLogConstants.AddedNew, markup.EntityName(), rating.Id, markup.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (markup.HasChanges())
				{
					foreach (var change in markup.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Ref#{1} {2}", markup.EntityName(), markup.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.EntityName(),
								EntityId = rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					markup.Save();
				}
			}
		}
	}
}
