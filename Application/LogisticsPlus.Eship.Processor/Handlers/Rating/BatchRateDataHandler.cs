﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.SmallPacks;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
	public class BatchRateDataHandler : EntityBase
	{
		private readonly IBatchRateDataView _view;
		private readonly BatchRateDataValidator _validator = new BatchRateDataValidator();

		public BatchRateDataHandler(IBatchRateDataView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
		    _view.RefreshVendorRatingProfiles += OnRefreshVendorRatingProfiles;
		}

		private void OnSearch(object sender, ViewEventArgs<ServiceMode> e)
		{
			PerformSearch(e.Argument);
		}

		private void OnDelete(object sender, ViewEventArgs<List<BatchRateData>> e)
		{
			var locks = new List<Lock>();
			var batchToProcess = e.Argument.Where(i => !i.IsNew).ToList();

			// obtain lock/check lock
			foreach (var data in batchToProcess)
			{
				var @lock = data.ObtainLock(_view.ActiveUser, data.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
					return;
				}
				locks.Add(@lock);
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				foreach (var @lock in locks)
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;
				}

				foreach (var data in batchToProcess)
				{
					data.Connection = Connection;
					data.Transaction = Transaction;

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Name: {1} ", AuditLogConstants.Delete, data.Name),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = data.EntityName(),
							EntityId = data.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// delete batch rate data and lock
					data.Delete();
				}

				// remove locks
				foreach (var @lock in locks)
					@lock.Delete();

				CommitTransaction();

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				foreach (var @lock in locks)
					@lock.Delete();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<List<BatchRateData>> e)
		{
			var msgs = new ValidationMessageCollection();		

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				foreach (var data in e.Argument)
				{
					data.Connection = Connection;
					data.Transaction = Transaction;

					// validate customer rating
					_validator.Messages.Clear();
					if (!_validator.IsValid(data))
					{
						msgs.AddRange(_validator.Messages);
						continue;
					}

					// audit log customer rating
					if (data.IsNew)
					{
						// save
						data.Save();

						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Name :{1}", AuditLogConstants.AddedNew, data.Name),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = data.EntityName(),
								EntityId = data.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					}
				}

				// commit transaction
				CommitTransaction();

				msgs.Add(ValidationMessage.Information(ProcessorVars.RecordSaveMsg));
				_view.DisplayMessages(msgs);
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
				msgs.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsg, ex.Message));
				_view.DisplayMessages(msgs);
			}
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.SmallPackEngines = GetSmallPackEngineTypes();
			_view.SmallPackTypes = SmallPacksFactory.RetrieveServiceTypes();
		}

	    private void OnRefreshVendorRatingProfiles(object sender, ViewEventArgs<VendorRatingViewSearchCriteria> e)
	    {
            _view.VendorRatingProfiles = new VendorRatingSearch().FetchVendorRatings(e.Argument, _view.ActiveUser.TenantId);
	    }

	    private static Dictionary<int, string> GetSmallPackEngineTypes()
		{
			return new Dictionary<int, string>
			       	{
			       		{(int)SmallPackageEngine.FedEx, SmallPackageEngine.FedEx.ToString().FormattedString()},
			       		{(int)SmallPackageEngine.Ups, SmallPackageEngine.Ups.ToString().FormattedString()}
			       	};
		}

		private void PerformSearch(ServiceMode mode)
		{
			_view.DisplayBatchRateDataSet(new BatchRateDataSearch().FetchBatchRateDataSet(mode, _view.ActiveUser.TenantId));
		}
	}
}
