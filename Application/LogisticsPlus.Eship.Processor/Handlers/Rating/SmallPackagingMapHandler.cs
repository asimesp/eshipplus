﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.SmallPacks;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
    public class SmallPackagingMapHandler : EntityBase
    {
        private readonly ISmallPackagingMapView _view;

        private readonly SmallPackagingMapValidator _validator = new SmallPackagingMapValidator();

        public SmallPackagingMapHandler(ISmallPackagingMapView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.UnLock += OnUnLock;
            _view.Lock += OnLock;
        }

        private void OnUnLock(object sender, ViewEventArgs<SmallPackagingMap> e)
        {
            var smallPackagingMap = e.Argument;
            var @lock = smallPackagingMap.RetrieveLock(_view.ActiveUser, smallPackagingMap.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<SmallPackagingMap> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnSearch(object sender, ViewEventArgs<string> e)
        {
            var results = new SmallPackagingMapSearch().FetchSmallPackagingMaps(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }

        private void OnDelete(object sender, ViewEventArgs<SmallPackagingMap> e)
        {
            _validator.Messages.Clear();

            var smallPackagingMap = e.Argument;

            if (smallPackagingMap.IsNew) return;

            var @lock = smallPackagingMap.ObtainLock(_view.ActiveUser, smallPackagingMap.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                smallPackagingMap.Connection = Connection;
                smallPackagingMap.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, smallPackagingMap.PackageType, smallPackagingMap.SmallPackageEngine),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = smallPackagingMap.EntityName(),
                    EntityId = smallPackagingMap.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                smallPackagingMap.Delete();
                @lock.Delete();

                CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[smallPackagingMap.TenantId].UpdateCache(smallPackagingMap, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSave(object sender, ViewEventArgs<SmallPackagingMap> e)
        {
            var smallPackagingMap = e.Argument;

            if (!smallPackagingMap.IsNew)
            {
                var @lock = smallPackagingMap.RetrieveLock(_view.ActiveUser, smallPackagingMap.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                smallPackagingMap.Connection = Connection;
                smallPackagingMap.Transaction = Transaction;

                if (!_validator.IsValid(smallPackagingMap))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (smallPackagingMap.IsNew)
                {
					smallPackagingMap.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, smallPackagingMap.PackageType, smallPackagingMap.SmallPackageEngine),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = smallPackagingMap.EntityName(),
                        EntityId = 0.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }

				else if (smallPackagingMap.HasChanges())
				{
					foreach (var change in smallPackagingMap.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = smallPackagingMap.EntityName(),
								EntityId = smallPackagingMap.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					smallPackagingMap.Save();
				}

            	CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[smallPackagingMap.TenantId].UpdateCache(smallPackagingMap);


                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnLoading(object sender, EventArgs e)
        {
        	_view.SmallPackageEngines = GetSmallPackEngines();
            _view.SmallPackageEnginesTypes = SmallPacksFactory.RetrievePackageTypes();
        }

		private static Dictionary<int, string> GetSmallPackEngines()
		{
			return new Dictionary<int, string>
			       	{
			       		{(int)SmallPackageEngine.FedEx, SmallPackageEngine.FedEx.ToString().FormattedString()},
			       		{(int)SmallPackageEngine.Ups, SmallPackageEngine.Ups.ToString().FormattedString()}
			       	};
		}
    }
}
