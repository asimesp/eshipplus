﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.Smc.Rates;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
	public class VendorRatingHandler : EntityBase
	{
		private readonly IVendorRatingView _view;
		private readonly VendorRatingValidator _validator = new VendorRatingValidator();

		public VendorRatingHandler(IVendorRatingView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.VendorSearch += OnVendorSearch;
		}

		private void OnSave(object sender, ViewEventArgs<VendorRating> e)
		{
			var rating = e.Argument;

			// check locks
			if (!rating.IsNew)
			{
				var @lock = rating.RetrieveLock(_view.ActiveUser, rating.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// retrieve db vendor rating for collection comparisons ...
			var dbRating = new VendorRating(rating.Id, false);
			dbRating.LoadCollections();
			foreach (var charge in dbRating.LTLAdditionalCharges) charge.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				rating.Connection = Connection;
				rating.Transaction = Transaction;

				// validate vendor rating
				_validator.Messages.Clear();
				if (!_validator.IsValid(rating))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// audit log vendor rating
				if (rating.IsNew)
				{
					// save
					rating.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, rating.Name),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (rating.HasChanges())
				{
					foreach (var change in rating.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.EntityName(),
								EntityId = rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					// save
					rating.Save();
				}

				// process collections
				ProcessLTLAdditionalCharges(rating, dbRating);
				ProcessLTLPcfToFcConversion(rating, dbRating.LTLPcfToFcConvTab);
				ProcessDiscountTiers(rating, dbRating.LTLDiscountTiers);
				ProcessLTLAccessorials(rating, dbRating.LTLAccessorials);
				ProcessLTLCubicFootCapacityRules(rating, dbRating.LTLCubicFootCapacityRules);
				ProcessLTLOverLengthRules(rating, dbRating.LTLOverLengthRules);
                ProcessLTLPackageSpecificRates(rating, dbRating.LTLPackageSpecificRates);
                ProcessLTLGuaranteedCharges(rating, dbRating.LTLGuaranteedCharges);

				// commit transaction
				CommitTransaction();

				// remove from cache to force reload
				ProcessorVars.RaterCache.Drop(rating);

				// return id in case of new
				_view.Set(rating);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}
        
	    private void OnDelete(object sender, ViewEventArgs<VendorRating> e)
		{
			var rating = e.Argument;

			if (rating.IsNew)
			{
				_view.Set(new VendorRating());
				return;
			}

			// load collections
			rating.LoadCollections();
			foreach (var charge in rating.LTLAdditionalCharges)
				charge.LoadCollections();

			// obtain lock/check lock
			var @lock = rating.ObtainLock(_view.ActiveUser, rating.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				rating.Connection = Connection;
				rating.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteVendorRating(rating))
				{
					@lock.Delete();
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor Ref#{2} Rating Ref#{3}", AuditLogConstants.Delete,
						                            rating.RatingName, rating.Vendor.VendorNumber, rating.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = rating.EntityName(),
						EntityId = rating.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// handle collection deletes
				foreach(var convTab in rating.LTLPcfToFcConvTab)
				{
					convTab.Connection = Connection;
					convTab.Transaction = Transaction;

					convTab.Delete();				
				}

				foreach (var tier in rating.LTLDiscountTiers)
				{
					tier.Connection = Connection;
					tier.Transaction = Transaction;

					tier.Delete();
				}
				
				foreach (var accessorial in rating.LTLAccessorials)
				{
					accessorial.Connection = Connection;
					accessorial.Transaction = Transaction;

					accessorial.Delete();
				}
				foreach (var charge in rating.LTLAdditionalCharges)
				{
					charge.Connection = Connection;
					charge.Transaction = Transaction;

					foreach (var index in charge.AdditionalChargeIndices)
					{
						index.Connection = Connection;
						index.Transaction = Transaction;

						index.Delete();
					}

					charge.Delete();
				}
				foreach (var rule in rating.LTLCubicFootCapacityRules)
				{
					rule.Connection = Connection;
					rule.Transaction = Transaction;

					rule.Delete();
				}
				foreach (var rule in rating.LTLOverLengthRules)
				{
					rule.Connection = Connection;
					rule.Transaction = Transaction;

					rule.Delete();
				}
                foreach (var psRate in rating.LTLPackageSpecificRates)
                {
                    psRate.Connection = Connection;
                    psRate.Transaction = Transaction;

                    psRate.Delete();
                }
                foreach (var gRate in rating.LTLGuaranteedCharges)
                {
                    gRate.Connection = Connection;
                    gRate.Transaction = Transaction;

                    gRate.Delete();
                }

				// delete customer and lock
				rating.Delete();

				@lock.Delete();

				CommitTransaction();

				// remove from cache to force reload
				ProcessorVars.RaterCache.Drop(rating);

				_view.Set(new VendorRating());
                _view.DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnVendorSearch(object sender, ViewEventArgs<string> e)
		{
			var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);

			_view.DisplayVendor(vendor ?? new Vendor());
			if (vendor == null)
				_view.DisplayMessages(new[] {ValidationMessage.Error("Vendor not found")});
		}

		private void OnUnLock(object sender, ViewEventArgs<VendorRating> e)
		{
			var rating = e.Argument;
			var @lock = rating.RetrieveLock(_view.ActiveUser, rating.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<VendorRating> e)
		{
			var rating = e.Argument;
			var @lock = rating.ObtainLock(_view.ActiveUser, rating.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<VendorRating> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.Smc3ServiceLevels = Smc3ServiceLevel
				.ServiceLevels
				.ToDictionary(i => i, i => i.FormattedString());
			_view.LTLTariffs = new Rateware2(_view.RatewareSettings, _view.CarrierConnectSettings)
				.ListAvailableTariffs()
				.ToDictionary(t => t, TariffNameFormatter.ReaderFriendlyTariff);
			_view.FuelIndexRegions = ProcessorUtilities.GetAll<FuelIndexRegion>();
			_view.DiscountTierRateBasis = ProcessorUtilities.GetAll<DiscountTierRateBasis>();
			_view.LTLAccessorialsRateTypes = GetLTLAccessorialRateTypes();
		    
            var rt = ProcessorUtilities.GetAll<RateType>();
		    rt.Remove(RateType.RatePerMile.ToInt());
            _view.LTLAdditionalChargeIndexRateTypes = rt;
            _view.LTLGuaranteedChargeRateTypes = rt.Where(r => r.Key != RateType.PerHundredWeight.ToInt()).ToDictionary(r => r.Key, r=> r.Value);

            _view.DaysOfWeek = ProcessorUtilities.GetAll<DayOfWeek>();

			var wb = ProcessorUtilities.GetAll<WeightBreak>();
			wb.Remove(WeightBreak.L5C.ToInt());
			_view.WeightBreaks = wb;

			_view.Regions = new RegionSearch().FetchRegions(new List<ParameterColumn>(), _view.ActiveUser.TenantId);
			_view.FuelTables = new FuelTableSearch().FetchFuelTables(new List<ParameterColumn>(), _view.ActiveUser.TenantId);
		}

		private static Dictionary<int, string> GetLTLAccessorialRateTypes()
		{
			return new Dictionary<int, string>
			       	{
			       		{(int)RateType.Flat, RateType.Flat.ToString().FormattedString()},
			       		{(int)RateType.PerHundredWeight, RateType.PerHundredWeight.ToString().FormattedString()}
			       	};
		}

		private void ProcessLTLOverLengthRules(VendorRating rating, IEnumerable<LTLOverLengthRule> dbRules)
		{
			var dbDelete = dbRules.Where(dbr => !rating.LTLOverLengthRules.Select(r => r.Id).Contains(dbr.Id));

			foreach (var rule in dbDelete)
			{
				rule.Connection = Connection;
				rule.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor Rating: {2} LTL Over Length Rule Ref#{3}",
												AuditLogConstants.Delete, rule.EntityName(), rating.Name, rule.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = rating.EntityName(),
					EntityId = rating.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				rule.Delete();
			}

			foreach (var a in rating.LTLOverLengthRules)
			{
				a.Connection = Connection;
				a.Transaction = Transaction;

				if (a.IsNew)
				{
					a.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor Rating:{2} LTL Over Length Ref#{3}",
							                            AuditLogConstants.AddedNew, a.EntityName(), rating.Name, a.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (a.HasChanges())
				{
					foreach (var change in a.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Ref#{1} {2}", a.EntityName(), a.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.EntityName(),
								EntityId = rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					a.Save();
				}
			}
		}

		private void ProcessLTLCubicFootCapacityRules(VendorRating rating, IEnumerable<LTLCubicFootCapacityRule> dbRules)
		{
			var dbDelete = dbRules.Where(dbr => !rating.LTLCubicFootCapacityRules.Select(r => r.Id).Contains(dbr.Id));

			foreach (var rule in dbDelete)
			{
				rule.Connection = Connection;
				rule.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor Rating: {2} LTL Cubic Capacity Rule Ref#{3}",
												AuditLogConstants.Delete, rule.EntityName(), rating.Name, rule.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = rating.EntityName(),
					EntityId = rating.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				rule.Delete();
			}

			foreach (var a in rating.LTLCubicFootCapacityRules)
			{
				a.Connection = Connection;
				a.Transaction = Transaction;

				if (a.IsNew)
				{
					a.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor Rating:{2} LTL Cubic Capacity Ref#{3}",
							                            AuditLogConstants.AddedNew, a.EntityName(), rating.Name, a.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (a.HasChanges())
				{
					foreach (var change in a.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Ref#{1} {2}", a.EntityName(), a.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.EntityName(),
								EntityId = rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					a.Save();
				}
			}
		}

		private void ProcessLTLAccessorials(VendorRating rating, IEnumerable<LTLAccessorial> dbAccessorials)
		{
			var dbDelete = dbAccessorials.Where(dba => !rating.LTLAccessorials.Select(a => a.Id).Contains(dba.Id));

			foreach (var a in dbDelete)
			{
				a.Connection = Connection;
				a.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor Rating: {2} LTL Accessorial Ref#{3}",
						                            AuditLogConstants.Delete, a.EntityName(), rating.Name, a.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = rating.EntityName(),
						EntityId = rating.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				a.Delete();
			}

			foreach (var a in rating.LTLAccessorials)
			{
				a.Connection = Connection;
				a.Transaction = Transaction;

				if (a.IsNew)
				{
					a.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor Rating:{2} LTL Accessorial Ref#{3}",
							                            AuditLogConstants.AddedNew, a.EntityName(), rating.Name, a.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if(a.HasChanges())
				{
					foreach (var change in a.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Ref#{1} {2}", a.EntityName(), a.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.EntityName(),
								EntityId = rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					a.Save();
				}
			}
		}

		private void ProcessDiscountTiers(VendorRating rating, IEnumerable<DiscountTier> dbTiers)
		{
			var dbDelete = dbTiers.Where(tier => !rating.LTLDiscountTiers.Select(t => t.Id).Contains(tier.Id));

			foreach (var tier in dbDelete)
			{
				tier.Connection = Connection;
				tier.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor Rating: {2} Discount Tier Ref#{3}",
						                            AuditLogConstants.Delete, tier.EntityName(), rating.Name, tier.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = rating.EntityName(),
						EntityId = rating.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				tier.Delete();
			}

			foreach (var tier in rating.LTLDiscountTiers)
			{
				tier.Connection = Connection;
				tier.Transaction = Transaction;

				if (tier.IsNew)
				{
					tier.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor Rating:{2} Discount Tier Ref#{3}",
							                            AuditLogConstants.AddedNew, tier.EntityName(), rating.Name, tier.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if(tier.HasChanges())
				{
					foreach (var change in tier.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Ref#{1} {2}", tier.EntityName(), tier.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.EntityName(),
								EntityId = rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					tier.Save();
				}
			}
		}

		private void ProcessLTLPcfToFcConversion(VendorRating rating, IEnumerable<LTLPcfToFcConversion> dbConvTabs)
		{
			var dbDelete = dbConvTabs.Where(convTab => !rating.LTLPcfToFcConvTab.Select(t => t.Id).Contains(convTab.Id));

			foreach (var convTab in dbDelete)
			{
				convTab.Connection = Connection;
				convTab.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor Rating: {2} LTL Pcf to Fc Conversion Ref#{3}",
												AuditLogConstants.Delete, convTab.EntityName(), rating.Name, convTab.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = rating.EntityName(),
					EntityId = rating.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				convTab.Delete();
			}

			foreach (var convTab in rating.LTLPcfToFcConvTab)
			{
				convTab.Connection = Connection;
				convTab.Transaction = Transaction;

				if (convTab.IsNew)
				{
					convTab.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor Rating:{2} LTL Pcf to Fc Conversion Ref#{3}",
													AuditLogConstants.AddedNew, convTab.EntityName(), rating.Name, convTab.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = rating.EntityName(),
						EntityId = rating.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (convTab.HasChanges())
				{
					foreach (var change in convTab.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref#{1} {2}", convTab.EntityName(), convTab.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					convTab.Save();
				}
			}
		}

		private void ProcessLTLAdditionalCharges(VendorRating rating, VendorRating dbRating)
		{
			var dbDelete = dbRating.LTLAdditionalCharges.Where(dbc => !rating.LTLAdditionalCharges.Select(c => c.Id).Contains(dbc.Id));

			foreach (var c in dbDelete)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				foreach(var index in c.AdditionalChargeIndices)
				{
					index.Connection = Connection;
					index.Transaction = Transaction;

					index.Delete();
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Vendor Rating:{2} Add'l Charge Ref#:{3}",
						                            AuditLogConstants.Delete, c.EntityName(), rating.Name, c.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = rating.EntityName(),
						EntityId = rating.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				c.Delete();
			}

			foreach (var c in rating.LTLAdditionalCharges)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				if (c.IsNew)
				{
					c.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor Rating:{2} Add'l Charge Ref#:{3}",
							                            AuditLogConstants.AddedNew, c.EntityName(), rating.Name, c.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (c.HasChanges())
				{
					foreach (var change in c.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Add'l Charge Ref#{1} {2}", c.EntityName(), c.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.EntityName(),
								EntityId = rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					c.Save();
				}

				ProcessAdditionalChargeIndices(rating, c, dbRating.LTLAdditionalCharges.FirstOrDefault(dbc => dbc.Id == c.Id));
			}
		}

		private void ProcessAdditionalChargeIndices(VendorRating rating, LTLAdditionalCharge charge, LTLAdditionalCharge dbCharge)
		{
			var dbDelete = dbCharge != null
			               	? dbCharge.AdditionalChargeIndices.Where(dbc => !charge.AdditionalChargeIndices.Select(c => c.Id).Contains(dbc.Id))
			               	: new List<AdditionalChargeIndex>();

			foreach (var c in dbDelete)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor Rating: {2} Add'l Charge Ref#{3} Index Ref# {4}",
												AuditLogConstants.Delete, c.EntityName(), rating.Name, charge.Id, c.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = rating.EntityName(),
					EntityId = rating.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				c.Delete();
			}

			foreach (var c in charge.AdditionalChargeIndices)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				if (c.IsNew)
				{
					c.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor Rating: {2} Add'l Charge Ref#{3} Index Ref# {4}",
							                            AuditLogConstants.AddedNew, c.EntityName(), rating.Name, charge.Id, c.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = rating.EntityName(),
							EntityId = rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (c.HasChanges())
				{
					foreach (var change in c.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("Add'l Charge: {0} {1} {2}", charge.Name, c.EntityName(), change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = rating.EntityName(),
								EntityId = rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					c.Save();
				}
			}
		}

        private void ProcessLTLPackageSpecificRates(VendorRating rating, IEnumerable<LTLPackageSpecificRate> dbPsRates)
        {
            var dbDelete = dbPsRates.Where(psRate => !rating.LTLPackageSpecificRates.Select(p => p.Id).Contains(psRate.Id));

            foreach (var psRate in dbDelete)
            {
                psRate.Connection = Connection;
                psRate.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Vendor Rating: {2} Package Specific Rate Ref#{3}",
                                                AuditLogConstants.Delete, psRate.EntityName(), rating.Name, psRate.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = rating.EntityName(),
                    EntityId = rating.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                psRate.Delete();
            }

            foreach (var psRate in rating.LTLPackageSpecificRates)
            {
                psRate.Connection = Connection;
                psRate.Transaction = Transaction;

                if (psRate.IsNew)
                {
                    psRate.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Vendor Rating:{2} Package Specific Rate Ref#{3}",
                                                    AuditLogConstants.AddedNew, psRate.EntityName(), rating.Name, psRate.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = rating.EntityName(),
                        EntityId = rating.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (psRate.HasChanges())
                {
                    foreach (var change in psRate.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Ref#{1} {2}", psRate.EntityName(), psRate.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = rating.EntityName(),
                            EntityId = rating.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    psRate.Save();
                }
            }
        }

        private void ProcessLTLGuaranteedCharges(VendorRating rating, IEnumerable<LTLGuaranteedCharge> dbGuaranteedRates)
        {
            var dbDelete = dbGuaranteedRates.Where(dba => !rating.LTLGuaranteedCharges.Select(a => a.Id).Contains(dba.Id));

            foreach (var g in dbDelete)
            {
                g.Connection = Connection;
                g.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Vendor Rating: {2} LTL Guaranteed Rate Ref#{3}",
                                                AuditLogConstants.Delete, g.EntityName(), rating.Name, g.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = rating.EntityName(),
                    EntityId = rating.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                g.Delete();
            }

            foreach (var g in rating.LTLGuaranteedCharges)
            {
                g.Connection = Connection;
                g.Transaction = Transaction;

                if (g.IsNew)
                {
                    g.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Vendor Rating:{2} LTL Guaranteed Rate Ref#{3}",
                                                    AuditLogConstants.AddedNew, g.EntityName(), rating.Name, g.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = rating.EntityName(),
                        EntityId = rating.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (g.HasChanges())
                {
                    foreach (var change in g.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Ref#{1} {2}", g.EntityName(), g.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = rating.EntityName(),
                            EntityId = rating.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    g.Save();
                }
            }
        }
	}
}
