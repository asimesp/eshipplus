﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
	public class BatchRateDataCleanUpManager : EntityBase
	{
		public void PurgeCompletedReadyForDeleteAfter()
		{
			const string format =
				@"delete from BatchRateData where DATEADD(Day, {0}, DateCompleted) <= GETDATE() and DateCompleted <> @DefaultDate and TenantId = @p{1} {2}";
			var query = string.Empty;
			var cnt = 0;
			var parameters = new Dictionary<string, object> {{"DefaultDate", DateUtility.SystemEarliestDateTime}};
			foreach (var key in ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays.Keys)
			{
				query += string.Format(format, ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays[key], cnt, Environment.NewLine);
				parameters.Add(string.Format("p{0}", cnt), key);
				cnt++;
			}


			ExecuteNonQuery(query, parameters);
		}
	}
}
