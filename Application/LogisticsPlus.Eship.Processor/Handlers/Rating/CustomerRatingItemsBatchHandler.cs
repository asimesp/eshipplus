﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
    public class CustomerRatingItemsBatchHandler : EntityBase
	{
		private readonly CustomerRatingValidator _validator = new CustomerRatingValidator();
		private readonly ICustomerRatingItemsBatchView _view;
        

		public CustomerRatingItemsBatchHandler(ICustomerRatingItemsBatchView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.InsertLTLSellRates += OnInsertLTLSellRates;
			_view.RemoveLTLSellRates += OnRemoveLTLSellRates;

			_view.InsertSmallPackRates += OnInsertSmallPackRates;
			_view.RemoveSmallPackRates += OnRemoveSmallPackRates;

			_view.InsertCustomerServiceMarkups += OnInsertCustomerServiceMarkups;
			_view.RemoveCustomerServiceMarkups += OnRemoveCustomerServiceMarkups;

		    _view.InsertTLSellRates += OnInsertTLSellRates;
            _view.RemoveTLSellRates += OnRemoveTLSellRates;

            _view.InsertProfitMarkups += OnInsertProfitMarkups;
		}
		
	    private void OnRemoveCustomerServiceMarkups(object sender, ViewEventArgs<List<string[]>> e)
		{
			var msg = new List<ValidationMessage>();
			var customerSearch = new CustomerSearch();

            if (e.Argument == null) return;

			foreach (var parts in e.Argument)
			{
				// find customer
				var customer = customerSearch.FetchCustomerByNumber(parts[0], _view.ActiveUser.TenantId);
				if (customer == null)
				{
					msg.Add(ValidationMessage.Error("Customer not found! Customer Number: {0}", parts[0]));
					continue;
				}

				// ensure rating
				if (customer.Rating == null)
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) has no rating profile", parts[0]));
					continue;
				}

				// get matching markups
				var markups = customer.Rating.CustomerServiceMarkups.Where(m => m.EffectiveDate.TimeToMinimum() == parts[1].ToDateTime().TimeToMinimum() &&  m.Service.Code == parts[2]).ToList();

				// if nothing, move on to next record
				if (!markups.Any()) continue;

				// obtain lock/check lock
				var @lock = customer.Rating.ObtainLock(_view.ActiveUser, customer.Rating.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					var value = string.Format("{0} -: {1}", ProcessorVars.UnableToObtainLockErrMsg, parts.TabJoin());
					msg.Add(ValidationMessage.Error(value));
					continue;
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					foreach (var markup in markups)
					{
						markup.Connection = Connection;
						markup.Transaction = Transaction;

						// audti log
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} {1} Customer Rating Ref#{2} Customer Service Markup Ref#{3}",
								                            AuditLogConstants.Delete, markup.EntityName(), customer.Rating.Id, markup.Id),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = customer.Rating.EntityName(),
								EntityId = customer.Rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

						//delete
						markup.Delete();
					}

					// delete lock
					@lock.Delete();

					// commit transaction
					CommitTransaction();

					// remove from cache to force reload
					ProcessorVars.RaterCache.Drop(customer.Rating);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					msg.Add(ValidationMessage.Error("Error processing record: {0} Err: {1}", parts.TabJoin(), ex.Message));
				}
			}

			// completion message
			if (msg.Any()) msg.Insert(0, ValidationMessage.Information("There were some errors during processing:"));
			msg.Insert(0, ValidationMessage.Information("Processing complete!"));

			_view.DisplayMessages(msg);
		}

		private void OnInsertCustomerServiceMarkups(object sender, ViewEventArgs<List<string[]>> e)
		{
			var msg = new List<ValidationMessage>();
			var customerSearch = new CustomerSearch();
			var serviceSearch = new ServiceSearch();

            if (e.Argument == null) return;

            foreach (var parts in e.Argument)
			{
				// find customer
				var customer = customerSearch.FetchCustomerByNumber(parts[0], _view.ActiveUser.TenantId);
				if (customer == null)
				{
					msg.Add(ValidationMessage.Error("Customer not found! Customer Number: {0}", parts[0]));
					continue;
				}

				// ensure rating
				if (customer.Rating == null)
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) has no rating profile", parts[0]));
					continue;
				}

				//create small package rate			
				var markup = new CustomerServiceMarkup
				             	{
				             		ServiceId = serviceSearch.FetchServiceIdByCode(parts[1], _view.ActiveUser.TenantId),
				             		MarkupPercent = parts[2].ToDecimal(),
				             		MarkupValue = parts[3].ToDecimal(),
				             		ServiceChargeCeiling = parts[4].ToDecimal(),
				             		ServiceChargeFloor = parts[5].ToDecimal(),
				             		EffectiveDate = parts[6].ToDateTime(),
				             		UseMinimum = parts[7].ToBoolean(),
				             		Active = parts[8].ToBoolean(),
				             		CustomerRating = customer.Rating,
				             		TenantId = customer.Rating.TenantId,
				             	};

				// validate
				customer.Rating.CustomerServiceMarkups.Add(markup);
				_validator.Messages.Clear();
				if (!_validator.IsValid(customer.Rating))
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) Customer Service Markup Error(s) start:", parts[0]));
					msg.AddRange(_validator.Messages);
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) Customer Service Markup Error(s) end", parts[0]));
					continue;
				}

				// obtain lock/check lock
				var @lock = customer.Rating.ObtainLock(_view.ActiveUser, customer.Rating.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					var value = string.Format("{0} -: {1}", ProcessorVars.UnableToObtainLockErrMsg, parts.TabJoin());
					msg.Add(ValidationMessage.Error(value));
					continue;
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					markup.Connection = Connection;
					markup.Transaction = Transaction;

					// save
					markup.Save();

					// audit log
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Customer Rating Ref#{2} Customer Service Markup Ref#{3}",
							                            AuditLogConstants.AddedNew, markup.EntityName(), customer.Rating.Id, markup.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = customer.Rating.EntityName(),
							EntityId = customer.Rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// delete lock
					@lock.Delete();

					// commit transaction
					CommitTransaction();

					// remove from cache to force reload
					ProcessorVars.RaterCache.Drop(customer.Rating);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					msg.Add(ValidationMessage.Error("Error processing record: {0}", parts.TabJoin()));
				}
			}

			// completion message
			if (msg.Any()) msg.Insert(0, ValidationMessage.Information("There were some errors during processing:"));
			msg.Insert(0, ValidationMessage.Information("Processing complete!"));

			_view.DisplayMessages(msg);
		}

		private void OnInsertProfitMarkups(object sender, ViewEventArgs<List<string[]>> e)
		{
			var msg = new List<ValidationMessage>();
            var msgTypeEnums = new List<ValidationMessage>();
			var customerSearch = new CustomerSearch();

            if (e.Argument == null) return;

            foreach (var parts in e.Argument)
			{
                msgTypeEnums.Clear();

				// find customer
				var customer = customerSearch.FetchCustomerByNumber(parts[0], _view.ActiveUser.TenantId);
				if (customer == null)
				{
					msg.Add(ValidationMessage.Error("Customer not found! Customer Number: {0}", parts[0]));
					continue;
				}

				// ensure rating
				if (customer.Rating == null)
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) has no rating profile", parts[0]));
					continue;
				}

                customer.Rating.NoLineHaulProfit = parts[1].ToBoolean();
                customer.Rating.LineHaulProfitCeilingType = parts[2].ToEnum<ValuePercentageType>();
                customer.Rating.LineHaulProfitCeilingValue = parts[3].ToDecimal();
                customer.Rating.LineHaulProfitCeilingPercentage = parts[4].ToDecimal();
                customer.Rating.UseLineHaulMinimumCeiling = parts[5].ToBoolean();
                customer.Rating.LineHaulProfitFloorType = parts[6].ToEnum<ValuePercentageType>();
                customer.Rating.LineHaulProfitFloorValue = parts[7].ToDecimal();
                customer.Rating.LineHaulProfitFloorPercentage = parts[8].ToDecimal();
                customer.Rating.UseLineHaulMinimumFloor = parts[9].ToBoolean();
                customer.Rating.NoFuelProfit = parts[10].ToBoolean();
                customer.Rating.FuelProfitCeilingType = parts[11].ToEnum<ValuePercentageType>();
                customer.Rating.FuelProfitCeilingValue = parts[12].ToDecimal();
                customer.Rating.FuelProfitCeilingPercentage = parts[13].ToDecimal();
                customer.Rating.UseFuelMinimumCeiling = parts[14].ToBoolean();
                customer.Rating.FuelProfitFloorType = parts[15].ToEnum<ValuePercentageType>();
                customer.Rating.FuelProfitFloorValue = parts[16].ToDecimal();
                customer.Rating.FuelProfitFloorPercentage = parts[17].ToDecimal();
                customer.Rating.UseFuelMinimumFloor = parts[18].ToBoolean();
                customer.Rating.NoAccessorialProfit = parts[19].ToBoolean();
                customer.Rating.AccessorialProfitCeilingType = parts[20].ToEnum<ValuePercentageType>();
                customer.Rating.AccessorialProfitCeilingValue = parts[21].ToDecimal();
                customer.Rating.AccessorialProfitCeilingPercentage = parts[22].ToDecimal();
                customer.Rating.UseAccessorialMinimumCeiling = parts[23].ToBoolean();
                customer.Rating.AccessorialProfitFloorType = parts[24].ToEnum<ValuePercentageType>();
                customer.Rating.AccessorialProfitFloorValue = parts[25].ToDecimal();
                customer.Rating.AccessorialProfitFloorPercentage = parts[26].ToDecimal();
                customer.Rating.UseAccessorialMinimumFloor = parts[27].ToBoolean();
                customer.Rating.NoServiceProfit = parts[28].ToBoolean();
                customer.Rating.ServiceProfitCeilingType = parts[29].ToEnum<ValuePercentageType>();
                customer.Rating.ServiceProfitCeilingValue = parts[30].ToDecimal();
                customer.Rating.ServiceProfitCeilingPercentage = parts[31].ToDecimal();
                customer.Rating.UseServiceMinimumCeiling = parts[32].ToBoolean();
                customer.Rating.ServiceProfitFloorType = parts[33].ToEnum<ValuePercentageType>();
                customer.Rating.ServiceProfitFloorValue = parts[34].ToDecimal();
                customer.Rating.ServiceProfitFloorPercentage = parts[35].ToDecimal();
                customer.Rating.UseServiceMinimumFloor = parts[36].ToBoolean();

				// validate
				_validator.Messages.Clear();
				if (!_validator.IsValid(customer.Rating))
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) Customer Service Markup Error(s) start:", parts[0]));
					msg.AddRange(_validator.Messages);
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) Customer Service Markup Error(s) end", parts[0]));
					continue;
				}

				// obtain lock/check lock
				var @lock = customer.Rating.ObtainLock(_view.ActiveUser, customer.Rating.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					var value = string.Format("{0} -: {1}", ProcessorVars.UnableToObtainLockErrMsg, parts.TabJoin());
					msg.Add(ValidationMessage.Error(value));
					continue;
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					customer.Rating.Connection = Connection;
					customer.Rating.Transaction = Transaction;

					// save
					customer.Rating.Save();

					// audit log
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Profit Ref#{2}",
							                            AuditLogConstants.Update, customer.Rating.EntityName(), customer.Rating.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = customer.Rating.EntityName(),
							EntityId = customer.Rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// delete lock
					@lock.Delete();

					// commit transaction
					CommitTransaction();

					// remove from cache to force reload
					ProcessorVars.RaterCache.Drop(customer.Rating);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					msg.Add(ValidationMessage.Error("Error processing record: {0}", parts.TabJoin()));
				}
			}

			// completion message
			if (msg.Any()) msg.Insert(0, ValidationMessage.Information("There were some errors during processing:"));
			msg.Insert(0, ValidationMessage.Information("Processing complete!"));

			_view.DisplayMessages(msg);
		}

		private void OnRemoveSmallPackRates(object sender, ViewEventArgs<List<string[]>> e)
		{
			var msg = new List<ValidationMessage>();
			var customerSearch = new CustomerSearch();

            if (e.Argument == null) return;

            foreach (var parts in e.Argument)
			{
				// find customer
				var customer = customerSearch.FetchCustomerByNumber(parts[0], _view.ActiveUser.TenantId);
				if (customer == null)
				{
					msg.Add(ValidationMessage.Error("Customer not found! Customer Number: {0}", parts[0]));
					continue;
				}

				// ensure rating
				if (customer.Rating == null)
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) has no rating profile", parts[0]));
					continue;
				}

				//get matching rates
				var rates = customer.Rating.SmallPackRates
					.Where(r => r.EffectiveDate.TimeToMinimum() == parts[1].ToDateTime().TimeToMinimum() && r.SmallPackageEngine == parts[2].ToEnum<SmallPackageEngine>() && r.SmallPackType == parts[3])
					.ToList();

				// if nothing, move on to next record
				if(!rates.Any()) continue;

				// obtain lock/check lock
				var @lock = customer.Rating.ObtainLock(_view.ActiveUser, customer.Rating.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					var value = string.Format("{0} -: {1}", ProcessorVars.UnableToObtainLockErrMsg, parts.TabJoin());
					msg.Add(ValidationMessage.Error(value));
					continue;
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					foreach (var rate in rates)
					{
						rate.Connection = Connection;
						rate.Transaction = Transaction;

						// audti log
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} {1} Customer Rating Ref#{2} Small Pack Rate Ref#{3}",
								                            AuditLogConstants.Delete, rate.EntityName(), customer.Rating.Id, rate.Id),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = customer.Rating.EntityName(),
								EntityId = customer.Rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

						// delete rate
						rate.Delete();
					}
					// delete lock
					@lock.Delete();

					// commit transaction
					CommitTransaction();

					// remove from cache to force reload
					ProcessorVars.RaterCache.Drop(customer.Rating);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					msg.Add(ValidationMessage.Error("Error processing record: {0}", parts.TabJoin()));
				}
			}

			// completion message
			if (msg.Any()) msg.Insert(0, ValidationMessage.Information("There were some errors during processing:"));
			msg.Insert(0, ValidationMessage.Information("Processing complete!"));

			_view.DisplayMessages(msg);
		}

		private void OnInsertSmallPackRates(object sender, ViewEventArgs<List<string[]>> e)
		{
			var msg = new List<ValidationMessage>();
			var customerSearch = new CustomerSearch();
			var vendorSearch = new VendorSearch();
			var chargeCodeSearch = new ChargeCodeSearch();

            if (e.Argument == null) return;
            
            foreach (var parts in e.Argument)
			{
				// find customer
				var customer = customerSearch.FetchCustomerByNumber(parts[0], _view.ActiveUser.TenantId);
				if (customer == null)
				{
					msg.Add(ValidationMessage.Error("Customer not found! Customer Number: {0}", parts[0]));
					continue;
				}

				// ensure rating
				if (customer.Rating == null)
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) has no rating profile", parts[0]));
					continue;
				}

				//create small package rate
				var rate = new SmallPackRate
				           	{
				           		Vendor = vendorSearch.FetchVendorByNumber(parts[1], _view.ActiveUser.TenantId),
				           		SmallPackageEngine = parts[2].ToEnum<SmallPackageEngine>(),
				           		SmallPackType = parts[3],
				           		MarkupPercent = parts[4].ToDecimal(),
				           		MarkupValue = parts[5].ToDecimal(),
				           		EffectiveDate = parts[6].ToDateTime(),
				           		UseMinimum = parts[7].ToBoolean(),
				           		ChargeCodeId = chargeCodeSearch.FetchChargeCodeIdByCode(parts[8], _view.ActiveUser.TenantId),
				           		Active = parts[9].ToBoolean(),
				           		CustomerRating = customer.Rating,
				           		TenantId = customer.Rating.TenantId,
				           	};

				// validate
				customer.Rating.SmallPackRates.Add(rate);
				_validator.Messages.Clear();
				if (!_validator.IsValid(customer.Rating))
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) Small Pack Rate Error(s) start:", parts[0]));
					msg.AddRange(_validator.Messages);
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) Small Pack Rate Error(s) end", parts[0]));
					continue;
				}

				// obtain lock/check lock
				var @lock = customer.Rating.ObtainLock(_view.ActiveUser, customer.Rating.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					var value = string.Format("{0} -: {1}", ProcessorVars.UnableToObtainLockErrMsg, parts.TabJoin());
					msg.Add(ValidationMessage.Error(value));
					continue;
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					rate.Connection = Connection;
					rate.Transaction = Transaction;

					// save
					rate.Save();

					// audti log
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Customer Rating Ref#{2} Small Pack Rate Ref#{3}",
							                            AuditLogConstants.AddedNew, rate.EntityName(), customer.Rating.Id, rate.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = customer.Rating.EntityName(),
							EntityId = customer.Rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// delete lock
					@lock.Delete();

					// commit transaction
					CommitTransaction();

					// remove from cache to force reload
					ProcessorVars.RaterCache.Drop(customer.Rating);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					msg.Add(ValidationMessage.Error("Error processing record: {0}", parts.TabJoin()));
				}
			}

			// completion message
			if (msg.Any()) msg.Insert(0, ValidationMessage.Information("There were some errors during processing:"));
			msg.Insert(0, ValidationMessage.Information("Processing complete!"));

			_view.DisplayMessages(msg);
		}

		private void OnRemoveLTLSellRates(object sender, ViewEventArgs<List<string[]>> e)
		{
			var msg = new List<ValidationMessage>();
			var customerSearch = new CustomerSearch();

            if (e.Argument == null) return;

            foreach (var parts in e.Argument)
			{
				// find customer
				var customer = customerSearch.FetchCustomerByNumber(parts[0], _view.ActiveUser.TenantId);
				if (customer == null)
				{
					msg.Add(ValidationMessage.Error("Customer not found! Customer Number: {0}", parts[0]));
					continue;
				}

				// ensure rating
				if (customer.Rating == null)
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) has no rating profile", parts[0]));
					continue;
				}

				//retrieve sell rates
				var rates = customer.Rating.LTLSellRates.Where(r => r.VendorRating.Name == parts[2] && r.EffectiveDate.TimeToMinimum() == parts[1].ToDateTime().TimeToMinimum()).ToList();

				// if nothing, move on to next record
				if (!rates.Any()) continue;

				// obtain lock/check lock
				var @lock = customer.Rating.ObtainLock(_view.ActiveUser, customer.Rating.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					var value = string.Format("{0} -: {1}", ProcessorVars.UnableToObtainLockErrMsg, parts.TabJoin());
					msg.Add(ValidationMessage.Error(value));
					continue;
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					foreach (var rate in rates)
					{
						rate.Connection = Connection;
						rate.Transaction = Transaction;

						// audti log
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} {1} Customer Rating Ref#{2} LTL Sell Rate Ref#{3}",
								                            AuditLogConstants.Delete, rate.EntityName(), customer.Rating.Id, rate.Id),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = customer.Rating.EntityName(),
								EntityId = customer.Rating.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

						// delete
						rate.Delete();
					}

					// delete lock
					@lock.Delete();

					// commit transaction
					CommitTransaction();

					// remove from cache to force reload
					ProcessorVars.RaterCache.Drop(customer.Rating);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					msg.Add(ValidationMessage.Error("Error processing record: {0}", parts.TabJoin()));
				}
			}

			// completion message
			if (msg.Any()) msg.Insert(0, ValidationMessage.Information("There were some errors during processing:"));
			msg.Insert(0, ValidationMessage.Information("Processing complete!"));

			_view.DisplayMessages(msg);
		}

		private void OnInsertLTLSellRates(object sender, ViewEventArgs<List<string[]>> e)
		{
			var msg = new ValidationMessageCollection();

			var customerSearch = new CustomerSearch();
			var vendorRatingSearch = new VendorRatingSearch();

            if(e.Argument == null) return;
            
            foreach(var parts in e.Argument)
			{
				// find customer
				var customer = customerSearch.FetchCustomerByNumber(parts[0], _view.ActiveUser.TenantId);
				if(customer == null)
				{
					msg.Add(ValidationMessage.Error("Customer not found! Customer Number: {0}", parts[0]));
					continue;
				}
				
				// ensure rating
				if (customer.Rating == null)
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) has no rating profile", parts[0]));
					continue;
				}

				//create sell rate
				var rate = new LTLSellRate
				           	{
				           		LTLIndirectPointEnabled = parts[1].ToBoolean(),
				           		EffectiveDate = parts[2].ToDateTime(),
				           		MarkupPercent = parts[3].ToDecimal(),
				           		MarkupValue = parts[4].ToDecimal(),
				           		VendorRatingId = vendorRatingSearch.FetchVendorRatingIdByName(parts[5], _view.ActiveUser.TenantId),
				           		UseMinimum = parts[6].ToBoolean(),
				           		Active = parts[7].ToBoolean(),
								StartOverrideWeightBreak = (WeightBreak) parts[8].ToInt(),
								OverrideMarkupPercentL5C = parts[9]!=null? parts[9].ToDecimal() : default(decimal),
								OverrideMarkupValueL5C = parts[10] != null ? parts[10].ToDecimal() : default(decimal),
								OverrideMarkupPercentM5C = parts[11] != null ? parts[11].ToDecimal() : default(decimal),
								OverrideMarkupValueM5C = parts[12] != null ? parts[12].ToDecimal() : default(decimal),
								OverrideMarkupPercentM1M = parts[13] != null ? parts[13].ToDecimal() : default(decimal),
								OverrideMarkupValueM1M = parts[14] != null ? parts[14].ToDecimal() : default(decimal),
								OverrideMarkupPercentM2M = parts[15] != null ? parts[15].ToDecimal() : default(decimal),
								OverrideMarkupValueM2M = parts[16] != null ? parts[16].ToDecimal() : default(decimal),
								OverrideMarkupPercentM5M = parts[17] != null ? parts[17].ToDecimal() : default(decimal),
								OverrideMarkupValueM5M = parts[18] != null ? parts[18].ToDecimal() : default(decimal),
								OverrideMarkupPercentM10M = parts[19] != null ? parts[19].ToDecimal() : default(decimal),
								OverrideMarkupValueM10M = parts[20] != null ? parts[20].ToDecimal() : default(decimal),
								OverrideMarkupPercentM20M = parts[21]!=null? parts[21].ToDecimal() : default(decimal),
								OverrideMarkupValueM20M = parts[22]!=null? parts[22].ToDecimal() : default(decimal),
								OverrideMarkupPercentM30M = parts[23] != null ? parts[23].ToDecimal() : default(decimal),		
								OverrideMarkupValueM30M = parts[24] != null ? parts[24].ToDecimal() : default(decimal),
								OverrideMarkupPercentM40M = parts[25] != null ? parts[25].ToDecimal() : default(decimal),
								OverrideMarkupValueM40M = parts[26] != null ? parts[26].ToDecimal() : default(decimal),
								OverrideMarkupVendorFloorEnabled = parts[27].ToBoolean(),
								OverrideMarkupPercentVendorFloor = parts[28] != null ? parts[28].ToDecimal() : default(decimal),
								OverrideMarkupValueVendorFloor = parts[29] != null ? parts[29].ToDecimal() : default(decimal),
				           		CustomerRating = customer.Rating,
				           		TenantId = customer.Rating.TenantId,
				           	};

				// validate
				customer.Rating.LTLSellRates.Add(rate);
				_validator.Messages.Clear();
				if(!_validator.IsValid(customer.Rating))
				{
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) Sell Rate Error(s) start:", parts[0]));
					msg.AddRange(_validator.Messages);
					msg.Add(ValidationMessage.Error("Customer (with Number: {0}) Sell Rate Error(s) end", parts[0]));
					continue;				
				}

				// obtain lock/check lock
				var @lock = customer.Rating.ObtainLock(_view.ActiveUser, customer.Rating.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					var value = string.Format("{0} -: {1}", ProcessorVars.UnableToObtainLockErrMsg, parts.TabJoin());
					msg.Add(ValidationMessage.Error(value));
					continue;
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					rate.Connection = Connection;
					rate.Transaction = Transaction;

					// save
					rate.Save();

					// audit log
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Customer Rating Ref#{2} LTL Sell Rate Ref#{3}",
							                            AuditLogConstants.AddedNew, rate.EntityName(), customer.Rating.Id, rate.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = customer.Rating.EntityName(),
							EntityId = customer.Rating.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// delete lock
					@lock.Delete();

					// commit transaction
					CommitTransaction();

					// remove from cache to force reload
					ProcessorVars.RaterCache.Drop(customer.Rating);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					msg.Add(ValidationMessage.Error("Error processing record: {0} Err: {1}", parts.TabJoin(), ex.Message));
				}
			}

			// completion message
			if (msg.Any()) msg.Insert(0, ValidationMessage.Information("There were some errors during processing:"));
			msg.Insert(0, ValidationMessage.Information("Processing complete!"));

			_view.DisplayMessages(msg);
		}

        private void OnRemoveTLSellRates(object sender, ViewEventArgs<List<string[]>> e)
        {
            var msg = new List<ValidationMessage>();
            var customerSearch = new CustomerSearch();

            if (e.Argument == null) return;

            foreach (var parts in e.Argument)
            {
                // find customer
                var customer = customerSearch.FetchCustomerByNumber(parts[0], _view.ActiveUser.TenantId);
                if (customer == null)
                {
                    msg.Add(ValidationMessage.Error("Customer not found! Customer Number: {0}", parts[0]));
                    continue;
                }

                // ensure rating
                if (customer.Rating == null)
                {
                    msg.Add(ValidationMessage.Error("Customer (with Number: {0}) has no rating profile", parts[0]));
                    continue;
                }

                // get matching tl sell rates
                var tlSellRates = customer.Rating.TLSellRates
                    .Where(t => t.EffectiveDate.TimeToMinimum() == parts[1].ToDateTime().TimeToMinimum()
						&& t.OriginCity.ToLower() == parts[2].ToLower() && t.OriginState.ToLower() == parts[3].ToLower() 
                        && (t.OriginCountry == null || t.OriginCountry.Code.ToLower() == parts[4].ToLower())
                        && t.OriginPostalCode.ToLower() == parts[5].ToLower() && t.DestinationCity.ToLower() == parts[6].ToLower() 
                        && t.DestinationState.ToLower() == parts[7].ToLower()
                        && (t.DestinationCountry == null || t.DestinationCountry.Code.ToLower() == parts[8].ToLower()) 
                        && t.DestinationPostalCode.ToLower() == parts[9].ToLower())
                    .ToList();

				// if nothing, move on to next record
				if (!tlSellRates.Any()) continue;

                // obtain lock/check lock
                var @lock = customer.Rating.ObtainLock(_view.ActiveUser, customer.Rating.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    var value = string.Format("{0} -: {1}", ProcessorVars.UnableToObtainLockErrMsg, parts.TabJoin());
                    msg.Add(ValidationMessage.Error(value));
                    continue;
                }

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    @lock.Connection = Connection;
                    @lock.Transaction = Transaction;

                    foreach (var tlSellRate in tlSellRates)
                    {
                        tlSellRate.Connection = Connection;
                        tlSellRate.Transaction = Transaction;

                        // audti log
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Customer Rating Ref#{2} TL Sell Rate Ref#{3}",
                                                        AuditLogConstants.Delete, tlSellRate.EntityName(), customer.Rating.Id, tlSellRate.Id),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.Rating.EntityName(),
                            EntityId = customer.Rating.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                        //delete
                        tlSellRate.Delete();
                    }

                    // delete lock
                    @lock.Delete();

                    // commit transaction
                    CommitTransaction();

                    // remove from cache to force reload
                    ProcessorVars.RaterCache.Drop(customer.Rating);
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    msg.Add(ValidationMessage.Error("Error processing record: {0} Err: {1}", parts.TabJoin(), ex.Message));
                }
            }

            // completion message
            if (msg.Any()) msg.Insert(0, ValidationMessage.Information("There were some errors during processing:"));
            msg.Insert(0, ValidationMessage.Information("Processing complete!"));

            _view.DisplayMessages(msg);
        }

        private void OnInsertTLSellRates(object sender, ViewEventArgs<List<string[]>> e)
        {
            var msg = new List<ValidationMessage>();
            var customerSearch = new CustomerSearch();

            if (e.Argument == null) return;

            foreach (var parts in e.Argument)
            {
                // find customer
                var customer = customerSearch.FetchCustomerByNumber(parts[0], _view.ActiveUser.TenantId);
                if (customer == null)
                {
                    msg.Add(ValidationMessage.Error("Customer not found! Customer Number: {0}", parts[0]));
                    continue;
                }

                // ensure rating
                if (customer.Rating == null)
                {
                    msg.Add(ValidationMessage.Error("Customer (with Number: {0}) has no rating profile", parts[0]));
                    continue;
                }

                var postalCodeSearch = new PostalCodeSearch();
                var originCountryId = !string.IsNullOrEmpty(parts[11]) ? ProcessorVars.RegistryCache.Countries.Values.First(c => c.Code == parts[11]).Id : default(long);
                var destCountryId = !string.IsNullOrEmpty(parts[15]) ? ProcessorVars.RegistryCache.Countries.Values.First(c => c.Code == parts[15]).Id : default(long);
                var mileageSourceId = !string.IsNullOrEmpty(parts[18]) ? ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].MileageSources.First(m => m.Code == parts[18]).Id : default(long);
                var originPostalCode = !string.IsNullOrEmpty(parts[12])
                                                   ? !string.IsNullOrEmpty(parts[11])
                                                         ? postalCodeSearch.FetchPostalCodeByCode(parts[12], originCountryId).Code
                                                         : parts[12]
                                                   : string.Empty;
                var destPostalCode = !string.IsNullOrEmpty(parts[16])
                                                   ? !string.IsNullOrEmpty(parts[15])
                                                         ? postalCodeSearch.FetchPostalCodeByCode(parts[16], destCountryId).Code
                                                         : parts[16]
                                                   : string.Empty; 

                //create sell rate
                var tlSellRate = new TLSellRate
                    {
                        EquipmentTypeId = !string.IsNullOrEmpty(parts[1]) ? ProcessorVars.RegistryCache[customer.TenantId].EquipmentTypes.First(eq => eq.Code == parts[1]).Id : default(long),
                        EffectiveDate = parts[2].ToDateTime(),
                        RateType = parts[3].ToEnum<RateType>(),
                        Rate = parts[4].ToDecimal(),
                        MinimumCharge = parts[5].ToDecimal(),
                        MinimumWeight = parts[6].ToDecimal(),
                        MaximumWeight = parts[7].ToDecimal(),
                        TariffType = parts[8].ToEnum<TLTariffType>(),
                        OriginCity = parts[9],
                        OriginState = parts[10],
                        OriginCountryId = originCountryId,
                        OriginPostalCode = originPostalCode,
                        DestinationCity = parts[13],
                        DestinationState = parts[14],
                        Mileage = parts[17].ToDecimal(),
                        MileageSourceId = mileageSourceId,
                        DestinationCountryId = destCountryId,
                        DestinationPostalCode = destPostalCode,

                        TenantId = customer.Rating.TenantId,
                        CustomerRating = customer.Rating
                    };
                
                // validate
                customer.Rating.TLSellRates.Add(tlSellRate);
                _validator.Messages.Clear();
                if (!_validator.IsValid(customer.Rating))
                {
                    msg.Add(ValidationMessage.Error("Customer (with Number: {0}) Sell Rate Error(s) start:", parts[0]));
                    msg.AddRange(_validator.Messages);
                    msg.Add(ValidationMessage.Error("Customer (with Number: {0}) Sell Rate Error(s) end", parts[0]));
                    continue;
                }

                // obtain lock/check lock
                var @lock = customer.Rating.ObtainLock(_view.ActiveUser, customer.Rating.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    var value = string.Format("{0} -: {1}", ProcessorVars.UnableToObtainLockErrMsg, parts.TabJoin());
                    msg.Add(ValidationMessage.Error(value));
                    continue;
                }

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    @lock.Connection = Connection;
                    @lock.Transaction = Transaction;

                    tlSellRate.Connection = Connection;
                    tlSellRate.Transaction = Transaction;

                    // save
                    tlSellRate.Save();

                    // audit log
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Customer Rating Ref#{2} TL Sell Rate Ref#{3}",
                                                    AuditLogConstants.AddedNew, tlSellRate.EntityName(), customer.Rating.Id, tlSellRate.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = customer.Rating.EntityName(),
                        EntityId = customer.Rating.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    // delete lock
                    @lock.Delete();

                    // commit transaction
                    CommitTransaction();

                    // remove from cache to force reload
                    ProcessorVars.RaterCache.Drop(customer.Rating);
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    msg.Add(ValidationMessage.Error("Error processing record: {0} Err: {1}", parts.TabJoin(), ex.Message));
                }
            }

            // completion message
            if (msg.Any()) msg.Insert(0, ValidationMessage.Information("There were some errors during processing:"));
            msg.Insert(0, ValidationMessage.Information("Processing complete!"));

            _view.DisplayMessages(msg);

        }
	}
}
