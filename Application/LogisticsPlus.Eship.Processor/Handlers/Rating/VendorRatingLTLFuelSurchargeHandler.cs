﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
	public class VendorRatingLTLFuelSurchargeHandler : EntityBase
	{
		private readonly IVendorRatingLTLFuelSurchargeView _view;

		public VendorRatingLTLFuelSurchargeHandler(IVendorRatingLTLFuelSurchargeView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Search += OnSearch;
			_view.Save += OnSave;
		}

		private void OnSave(object sender, ViewEventArgs<List<VendorRating>> e)
		{
			var messages = new List<ValidationMessage>();
			var toProcess = new List<VendorRating>();
			var locks = new List<Lock>();

			// obtain locks
			foreach (var rating in e.Argument)
			{
				var @lock = rating.ObtainLock(_view.ActiveUser, rating.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					messages.Add(ValidationMessage.Error("Could not update record [{0}]. {1}", rating.Name,
														 ProcessorVars.UnableToObtainLockErrMsg));
					continue;
				}

				// add for processing
			    locks.Add(@lock);
				toProcess.Add(rating);
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// put locks in transaction
				foreach (var @lock in locks)
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;
				}

				// process records
				foreach (var rating in toProcess)
				{
					rating.Connection = Connection;
					rating.Transaction = Transaction;

					// audit log
					if (rating.HasChanges())
						foreach (var change in rating.Changes())
							new AuditLog
								{
									Connection = Connection,
									Transaction = Transaction,
									Description = change,
									TenantId = _view.ActiveUser.TenantId,
									User = _view.ActiveUser,
									EntityCode = rating.EntityName(),
									EntityId = rating.Id.ToString(),
									LogDateTime = DateTime.Now
								}.Log();

					// save and delete lock
					rating.Save();
				}

				// delete locks
				foreach(var @lock in locks)
					@lock.Delete();

				// commit transaction
				CommitTransaction();

				// handle messages
				if (messages.Count > 0)
					messages.Insert(0, ValidationMessage.Warning("Some records where not successfully updated:"));
				messages.Insert(0, ValidationMessage.Information("Mass update process complete!"));
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error("An error occurred while deleting record. Err: {0}", ex.Message) });
			}
		}

		private void OnSearch(object sender, ViewEventArgs<DateTime> e)
		{
			var surcharges = new VendorRatingLTLFuelSurchargeUpdateDto().RetrieveListForUpdate(e.Argument, _view.ActiveUser.TenantId);

			if (surcharges.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
			else
				_view.DisplayVendorRatingSurcharges(surcharges);
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.FuelIndexRegions = ProcessorUtilities.GetAll<FuelIndexRegion>();
		}
	}
}
