﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
	public class RegionHandler : EntityBase
	{
		private readonly IRegionView _view;
		private readonly RegionValidator _validator = new RegionValidator();

		public RegionHandler(IRegionView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
		}

		private void OnDelete(object sender, ViewEventArgs<Region> e)
		{
			var region = e.Argument;

			if (region.IsNew)
			{
				_view.Set(region);
				return;
			}

			// load collections
			region.LoadCollections();

			// obtain lock/check lock
			var @lock = region.ObtainLock(_view.ActiveUser, region.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				region.Connection = Connection;
				region.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteRegion(region))
				{
					@lock.Delete();
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Ref#{2}", AuditLogConstants.Delete, region.Name, region.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = region.EntityName(),
						EntityId = region.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// handle collection deletes
				foreach (var index in region.Areas)
				{
					index.Connection = Connection;
					index.Transaction = Transaction;

					index.Delete();
				}

				// delete region and lock
				region.Delete();

				@lock.Delete();

				CommitTransaction();

				_view.Set(new Region());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<Region> e)
		{
			var region = e.Argument;

			// check locks
			if (!region.IsNew)
			{
				var @lock = region.RetrieveLock(_view.ActiveUser, region.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
					return;
				}
			}

			// retrieve db region for collection comparisons ...
			var dbRegion = new Region(region.Id, false);
			dbRegion.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				region.Connection = Connection;
				region.Transaction = Transaction;

				// validate region
				_validator.Messages.Clear();
				if (!_validator.IsValid(region))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// audit log region
				if (region.IsNew)
				{
					// save
					region.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, region.Name),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = region.EntityName(),
							EntityId = region.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (region.HasChanges())
				{
					foreach (var change in region.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = region.EntityName(),
								EntityId = region.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					// save
					region.Save();
				}


				// process collections
				ProcessAreas(region, dbRegion);

				// commit transaction
				CommitTransaction();

				// return id in case of new
				_view.Set(region);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnUnLock(object sender, ViewEventArgs<Region> e)
		{
			var fuelTable = e.Argument;
			var @lock = fuelTable.RetrieveLock(_view.ActiveUser, fuelTable.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg)});
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<Region> e)
		{
			var region = e.Argument;
			var @lock = region.ObtainLock(_view.ActiveUser, region.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<Region> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
			                                                                 e.Argument.TenantId));
		}

		private void ProcessAreas(Region region, Region dbRegion)
		{
			var dbDelete = dbRegion.Areas.Where(i => !region.Areas.Select(dbi => dbi.Id).Contains(i.Id));

			foreach (var index in dbDelete)
			{
				index.Connection = Connection;
				index.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Region:{2} Ref #:{3} {4}:{5}",
						                            AuditLogConstants.Delete, index.EntityName(), region.Name, index.Id,
						                            index.UseSubRegion ? "Sub Region" : "Postal Code",
						                            index.UseSubRegion ? index.SubRegion.Name : index.PostalCode),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = region.EntityName(),
						EntityId = region.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				index.Delete();
			}

			foreach (var index in region.Areas)
			{
				index.Connection = Connection;
				index.Transaction = Transaction;

				if (index.IsNew)
				{
					index.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Region:{2} Ref #:{3}  {4}:{5}",
							                            AuditLogConstants.AddedNew, index.EntityName(), region.Name, index.Id,
							                            index.UseSubRegion ? "Sub Region" : "Postal Code",
							                            index.UseSubRegion ? index.SubRegion.Name : index.PostalCode),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = region.EntityName(),
							EntityId = region.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (index.HasChanges())
				{
					foreach (var change in index.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("Region {0} Ref #{1} {2}", index.EntityName(), index.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = region.EntityName(),
								EntityId = region.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					index.Save();
				}
			}
		}
	}
}
