﻿using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
    public class CustomerTLTenderProfileFinderHandler
    {
        private readonly ICustomerTLTenderProfileFinderView _view;

        public CustomerTLTenderProfileFinderHandler(ICustomerTLTenderProfileFinderView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<CustomerTLTenderingProfileViewSearchCriteria> e)
        {
            var results = new CustomerTLTenderProfileSearch().FetchCustomerTLTenderProfiles(e.Argument, _view.ActiveUser.TenantId);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

            _view.DisplaySearchResult(results);
        }
    }
}
