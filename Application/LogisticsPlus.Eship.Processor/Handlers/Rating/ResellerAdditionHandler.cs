﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
    public class ResellerAdditionHandler : EntityBase
    {
        private readonly IResellerAdditionView _view;

        private readonly ResellerAdditionValidator _validator = new ResellerAdditionValidator();

        public ResellerAdditionHandler(IResellerAdditionView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoad;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.UnLock += OnUnLock;
            _view.Lock += OnLock;
            _view.LoadAuditLog += OnLoadAuditLog;
        	_view.CustomerSearch += OnCustomerSearch;
        }

		private void OnCustomerSearch(Object sender, ViewEventArgs<string> e)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(e.Argument, _view.ActiveUser.TenantId);

			_view.DisplayCustomer(customer ?? new Customer());
			if (customer == null)
				_view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
		}
		
        private void OnLoadAuditLog(object sender, ViewEventArgs<ResellerAddition> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
        }

        private void OnUnLock(object sender, ViewEventArgs<ResellerAddition> e)
        {
            var resellerAddition = e.Argument;
            var @lock = resellerAddition.RetrieveLock(_view.ActiveUser, resellerAddition.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<ResellerAddition> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnDelete(object sender, ViewEventArgs<ResellerAddition> e)
        {
            _validator.Messages.Clear();

            var resellerAddition = e.Argument;

            if (resellerAddition.IsNew) return;

            var @lock = resellerAddition.ObtainLock(_view.ActiveUser, resellerAddition.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                resellerAddition.Connection = Connection;
                resellerAddition.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteResellerAddition(resellerAddition))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, resellerAddition.Name, resellerAddition.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = resellerAddition.EntityName(),
                    EntityId = resellerAddition.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                resellerAddition.Delete();
                @lock.Delete();

                CommitTransaction();

				// remove from cache to force reload
				ProcessorVars.RaterCache.Drop(resellerAddition);

            	_view.Set(new ResellerAddition());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSave(object sender, ViewEventArgs<ResellerAddition> e)
        {
            var resellerAddition = e.Argument;

        	if (!resellerAddition.IsNew)
            {
                var @lock = resellerAddition.RetrieveLock(_view.ActiveUser, resellerAddition.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                resellerAddition.Connection = Connection;
                resellerAddition.Transaction = Transaction;

                if (!_validator.IsValid(resellerAddition))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

            	if (resellerAddition.IsNew)
            	{
            		resellerAddition.Save();
            		new AuditLog
            			{
            				Connection = Connection,
            				Transaction = Transaction,
            				Description = string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, resellerAddition.Name, resellerAddition.Id),
            				TenantId = _view.ActiveUser.TenantId,
            				User = _view.ActiveUser,
            				EntityCode = resellerAddition.EntityName(),
            				EntityId = resellerAddition.Id.ToString(),
            				LogDateTime = DateTime.Now
            			}.Log();
            	}
				else if (resellerAddition.HasChanges())
				{
					foreach (var change in resellerAddition.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = resellerAddition.EntityName(),
								EntityId = resellerAddition.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					resellerAddition.Save();
				}

            	CommitTransaction();

				// remove from cache to force reload
				ProcessorVars.RaterCache.Drop(resellerAddition);

            	_view.Set(resellerAddition);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnLoad(Object sender, EventArgs e)
        {
            _view.ValuePercentagTypes = ProcessorUtilities.GetAll<ValuePercentageType>();
        }
    }
}
