﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;

namespace LogisticsPlus.Eship.Processor.Handlers.Rating
{
	public class FuelTableFinderHandler
	{
		private readonly IFuelTableFinderView _view;

		public FuelTableFinderHandler(IFuelTableFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			var results = new FuelTableSearch().FetchFuelTables(e.Argument, _view.ActiveUser.TenantId);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}
	}
}
