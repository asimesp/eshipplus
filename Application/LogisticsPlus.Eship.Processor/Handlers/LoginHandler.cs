﻿using System;
using System.DirectoryServices.AccountManagement;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers
{
	public class LoginHandler : EntityBase
	{
		private readonly ILoginView _view;
		private const string ErrorAttemptingToLogin = "Error attempting to login";

		public LoginHandler(ILoginView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.AuthenticateUser += OnAuthenticateUser;
			_view.RetrieveUserPassword += OnRetrieveUserPassword;
			_view.PasswordChange += OnPasswordChanged;
		}

		private void OnPasswordChanged(object sender, ViewEventArgs<string> e)
		{
			try
			{
				var user = new UserSearch().FetchUserByUsernameAndAccessCode(_view.UserName, _view.AccessCode);

				if (user == null)
				{
					_view.NotifyUserNotFound();
					return;
				}

				if (!user.Enabled)
				{
					_view.NotifyDisabledUser();
					return;
				}

				if (!user.Tenant.Active)
				{
					_view.NotifyInActiveAccount();
					return;
				}

				if (user.FailedLoginAttempts >= ProcessorVars.MaxFailedLoginAttempts)
				{
					_view.NotifyLoginAttemptsExceeded();
					return;
				}

				if (user.ForcePasswordReset)
				{
					var @lock = user.ObtainLock(user, user.Id);
					if (!@lock.IsUserLock(user))
					{
						_view.DisplayMessages(new[] {ValidationMessage.Error(ErrorAttemptingToLogin)});
						return;
					}

					Connection = DatabaseConnection.DefaultConnection;
					BeginTransaction();
					try
					{
						user.Connection = Connection;
						user.Transaction = Transaction;

						@lock.Connection = Connection;
						@lock.Transaction = Transaction;

						user.TakeSnapShot();
						user.ForcePasswordReset = false;
						user.Password = e.Argument;
						user.FailedLoginAttempts = 0;

						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description =
									string.Format("Password Reset Username: {0} User: {1} {2}", user.Username,
									              user.FirstName, user.LastName),
								TenantId = user.TenantId,
								User = user,
								EntityCode = user.EntityName(),
								EntityId = user.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("User Authenticated Username: {0} User: {1} {2}", user.Username,
								                            user.FirstName, user.LastName),
								TenantId = user.TenantId,
								User = user,
								EntityCode = user.EntityName(),
								EntityId = user.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

						foreach (var change in user.Changes())
							new AuditLog
								{
									Connection = Connection,
									Transaction = Transaction,
									Description = change,
									TenantId = user.TenantId,
									User = user,
									EntityCode = user.EntityName(),
									EntityId = user.Id.ToString(),
									LogDateTime = DateTime.Now
								}.Log();

						user.Save();
						@lock.Delete();

						CommitTransaction();

						_view.NotifySuccesfulLogin(user);
					}
					catch
					{
						RollBackTransaction();
						_view.DisplayMessages(new[] {ValidationMessage.Error(ErrorAttemptingToLogin)});
					}
				}
			}
			catch (Exception ex)
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error("{0} Err: {1}", ErrorAttemptingToLogin, ex.Message) });
			}
		}

		private void OnAuthenticateUser(object sender, EventArgs e)
		{
			if (_view.AdminAuthentication) AuthenticateAdminUser();
			else AuthenticateTenantUser();
		}

		private void OnRetrieveUserPassword(object sender, EventArgs e)
		{
			if(_view.AdminAuthentication) RetrieveAdminUserPassword();
			else RetrieveTenantUserPassword();
		}

        private bool IsAdValidUser(string userName, string pass)
        {
            try
            {
                bool validUser;
                using (var pc = new PrincipalContext(ContextType.Domain, _view.Domain))
                {
                    validUser = pc.ValidateCredentials(userName, pass);
                }
                return validUser;
            }
            catch
            {
                return false;
            }
        }

		private void RetrieveAdminUserPassword()
		{
			var user = new AdminUserSearch().FetchUserByUsername(_view.UserName);

			if (user == null)
			{
				_view.NotifyUserNotFound();
				return;
			}

			new AdminAuditLog
				{
					Description = string.Format("Retrieved User Password for Login Username: {0} User: {1} {2}",
					                            user.Username, user.FirstName, user.LastName),
					AdminUser = user,
					EntityCode = user.EntityName(),
					EntityId = user.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			_view.ProcessPasswordRetrieval(user.Password, user.Email);
		}

		private void RetrieveTenantUserPassword()
		{
			var user = new UserSearch().FetchUserByUsernameAndAccessCode(_view.UserName, _view.AccessCode);

			if (user == null)
			{
				_view.NotifyUserNotFound();
				return;
			}

			new AuditLog
				{
					Description = string.Format("Retrieved User Password for Login Username: {0} User: {1} {2}",
					                            user.Username, user.FirstName, user.LastName),
					TenantId = user.TenantId,
					User = user,
					EntityCode = user.EntityName(),
					EntityId = user.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			_view.ProcessPasswordRetrieval(user.Password, user.Email);
		}



		private void AuthenticateAdminUser()
		{
			try
			{
				var user = new AdminUserSearch().FetchUserByUsername(_view.UserName);

				if (user == null)
				{
					_view.NotifyUserNotFound();
					return;
				}

				if (!user.Enabled)
				{
					_view.NotifyDisabledUser();
					return;
				}

				if (user.Password != _view.Password)
				{
					_view.NotifyInvalidPassword();
					return;
				}

				new AdminAuditLog
					{
						Description = string.Format("Admin User Authenticated Username: {0} User: {1} {2}", user.Username,
						                            user.FirstName, user.LastName),
						AdminUser = user,
						EntityCode = user.EntityName(),
						EntityId = user.Id.ToString(),
						LogDateTime = DateTime.Now,
					}.Log();

				_view.NotifySuccesfulLogin(user);
			}
			catch (Exception ex)
			{
                _view.DisplayMessages(new[] { ValidationMessage.Error("{0} Err: {1}", ErrorAttemptingToLogin, ex.Message) });
			}
		}

		private void AuthenticateTenantUser()
		{
			try
			{
				var user = _view.AuthenticationType == AuthenticationType.DomainAuthentication
					           ? new UserSearch().FetchUserByAdUsername(_view.UserName, _view.AccessCode) ??
					             new UserSearch().FetchUserByUsernameAndAccessCode(_view.UserName, _view.AccessCode)
					           : new UserSearch().FetchUserByUsernameAndAccessCode(_view.UserName, _view.AccessCode);

				if (user == null)
				{
					_view.NotifyUserNotFound();
					return;
				}

				if (!user.Enabled)
				{
					_view.NotifyDisabledUser();
					return;
				}

				if (!user.Tenant.Active || !user.DefaultCustomer.Active || !user.DefaultCustomer.Tier.Active)
				{
					_view.NotifyInActiveAccount();
					return;
				}

                var isValidatedUser = _view.AuthenticationType == AuthenticationType.DomainAuthentication
                                          ? IsAdValidUser(_view.UserName, _view.Password)
                                          : user.Password == _view.Password;

                if (!isValidatedUser)
				{
					var passwordLock = user.ObtainLock(user, user.Id);
					if (!passwordLock.IsUserLock(user))
					{
						_view.DisplayMessages(new[] {ValidationMessage.Error(ErrorAttemptingToLogin)});
						return;
					}

					Connection = DatabaseConnection.DefaultConnection;
					BeginTransaction();
					try
					{
						user.Connection = Connection;
						user.Transaction = Transaction;

						passwordLock.Connection = Connection;
						passwordLock.Transaction = Transaction;

						user.TakeSnapShot();
						user.FailedLoginAttempts++;

						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description =
									string.Format("Failed login attempt Username: {0} User: {1} {2}", user.Username,
									              user.FirstName, user.LastName),
								TenantId = user.TenantId,
								User = user,
								EntityCode = user.EntityName(),
								EntityId = user.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

						foreach (var change in user.Changes())
							new AuditLog
								{
									Connection = Connection,
									Transaction = Transaction,
									Description = change,
									TenantId = user.TenantId,
									User = user,
									EntityCode = user.EntityName(),
									EntityId = user.Id.ToString(),
									LogDateTime = DateTime.Now
								}.Log();

						user.Save();
						passwordLock.Delete();

						CommitTransaction();

						_view.NotifyInvalidPassword();
						return;
					}
					catch
					{
						RollBackTransaction();
						_view.DisplayMessages(new[] {ValidationMessage.Error(ErrorAttemptingToLogin)});
					}
				}

				if (user.FailedLoginAttempts >= ProcessorVars.MaxFailedLoginAttempts)
				{
					_view.NotifyLoginAttemptsExceeded();
					return;
				}

				if (user.ForcePasswordReset)
				{
					_view.ProcessPasswordReset();
					return;
				}

				var @lock = user.ObtainLock(user, user.Id);
				if (!@lock.IsUserLock(user))
				{
					_view.DisplayMessages(new[] {ValidationMessage.Error(ErrorAttemptingToLogin)});
					return;
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					user.Connection = Connection;
					user.Transaction = Transaction;

					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("User Authenticated Username: {0} User: {1} {2}", user.Username,
							                            user.FirstName, user.LastName),
							TenantId = user.TenantId,
							User = user,
							EntityCode = user.EntityName(),
							EntityId = user.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					user.TakeSnapShot();
					user.FailedLoginAttempts = 0;
					foreach (var change in user.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = user.TenantId,
								User = user,
								EntityCode = user.EntityName(),
								EntityId = user.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					user.Save();
					@lock.Delete();

					CommitTransaction();
				}
				catch
				{
					RollBackTransaction();
					_view.DisplayMessages(new[] {ValidationMessage.Error(ErrorAttemptingToLogin)});
				}
				_view.NotifySuccesfulLogin(user);
			}
			catch (Exception ex)
			{
                _view.DisplayMessages(new[] { ValidationMessage.Error("{0} Err: {1}", ErrorAttemptingToLogin, ex.Message) });
			}
		}
	}
}
