﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
	public class XmlTransmissionProcessHandler : EntityBase
	{
		public void SaveTransmission(XmlTransmission transmission, out Exception ex)
		{
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				var validator = new XmlTransmissionValidator {Connection = Connection, Transaction = Transaction};
				if (!validator.IsValid(transmission))
				{
					RollBackTransaction();
					var errors = validator.Messages.Select(m => string.Format("{0}: {1}", m.Type, m.Message));
					ex = new Exception(string.Join(Environment.NewLine, errors.ToArray()));
					return;
				}

				if (transmission.IsNew)
				{
					// save
					transmission.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} Xml Transmission: {1}", AuditLogConstants.AddedNew, transmission.TransmissionKey),
							TenantId = transmission.TenantId,
							User = transmission.User,
							EntityCode = transmission.EntityName(),
							EntityId = transmission.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

				}
				else if (transmission.HasChanges())
				{
					foreach (var change in transmission.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = transmission.TenantId,
								User = transmission.User,
								EntityCode = transmission.EntityName(),
								EntityId = transmission.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					// save
					transmission.Save();
				}

				// commit transaction
				CommitTransaction();

				ex = null;
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
			}
		}


		public void SaveTransmission(XmlConnect connectRecord, User processingUser, out Exception ex)
		{
			SaveTransmission(new List<XmlConnect> {connectRecord}, processingUser, out ex);
		}

		public void SaveTransmission(List<XmlConnect> connectRecords, User processingUser, out Exception ex)
		{
			ex = null;
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				foreach (var connect in connectRecords)
				{
					var validator = new XmlConnectValidator {Connection = Connection, Transaction = Transaction};
					if (!validator.IsValid(connect))
					{
						RollBackTransaction();
						var errors = validator.Messages.Select(m => string.Format("{0}: {1}", m.Type, m.Message));
						ex = new Exception(string.Join(Environment.NewLine, errors.ToArray()));
						return;
					}

					if (connect.IsNew)
					{
						// save
						connect.Save();

						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Xml Connect: Doc Type: {1} Direction: {2}",
								                            AuditLogConstants.AddedNew, connect.DocumentType, connect.Direction),
								TenantId = connect.TenantId,
								User = processingUser,
								EntityCode = connect.EntityName(),
								EntityId = connect.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					}
					else if (connect.HasChanges())
					{
						foreach (var change in connect.Changes())
							new AuditLog
								{
									Connection = Connection,
									Transaction = Transaction,
									Description = change,
									TenantId = connect.TenantId,
									User = processingUser,
									EntityCode = connect.EntityName(),
									EntityId = connect.Id.ToString(),
									LogDateTime = DateTime.Now
								}.Log();

						// save
						connect.Save();
					}
				}

				// commit transaction
				CommitTransaction();
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
			}
		}
	}
}
