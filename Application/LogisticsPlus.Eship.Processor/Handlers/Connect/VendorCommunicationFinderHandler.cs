﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
	public class VendorCommunicationFinderHandler
	{
		private readonly IVendorCommunicationFinderView _view;

		public VendorCommunicationFinderHandler(IVendorCommunicationFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
            var results = new VendorSearch().FetchVendorsWithCommunication(e.Argument, _view.ActiveUser.TenantId);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

            _view.DisplaySearchResult(results);
		}
	}
}
