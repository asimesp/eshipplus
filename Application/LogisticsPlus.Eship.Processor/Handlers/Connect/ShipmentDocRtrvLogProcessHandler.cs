﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
	public class ShipmentDocRtrvLogProcessHandler : EntityBase
	{
		public void InsertImgRtrvLog(ShipmentDocRtrvLog log, User processingUser, out Exception ex, Shipment s = null)
		{
			var shipment = s ?? log.RetrieveCorrespondingShipment();

			ex = null;
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				if (!log.IsNew)
				{
					RollBackTransaction();
					ex = new Exception("Cannot insert existing record. Record must be new!");
					return;
				}

				// save
				log.Save();

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("Added New {0} [Shipment #: {1}]", log.EntityName(), log.ShipmentNumber),
						TenantId = log.TenantId,
						User = processingUser,
						EntityCode = log.EntityName(),
						EntityId = log.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				if (shipment != null)
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = "Added New Image Document Retrieval Log",
							TenantId = log.TenantId,
							User = processingUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();


				// commit transaction
				CommitTransaction();
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
			}
		}

		public void UpdateImgRtrvLogAndShipmentDocument(ShipmentDocRtrvLog log, List<ShipmentDocument> documents, User processingUser, out Exception ex)
		{
			var validator = new ShipmentValidator();

			ex = null;
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				validator.Connection = Connection;
				validator.Transaction = Transaction;

				// validation
				if (log.IsNew)
				{
					RollBackTransaction();
					ex = new Exception("Cannot update non-existing record. Record must be exist!");
					return;
				}

				if (!validator.ShipmentDocumentsAreValid(documents))
				{
					RollBackTransaction();
					ex = new Exception(validator.Messages.Select(m => m.Message).ToArray().NewLineJoin());
					return;
				}


				// save

				// shipment documents
				foreach (var doc in documents)
				{
					doc.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Shipment Number:{2} Document Name:{3}",
							                            AuditLogConstants.AddedNew, doc.EntityName(), doc.Shipment.ShipmentNumber, doc.Name),
							TenantId = log.TenantId,
							User = processingUser,
							EntityCode = doc.Shipment.EntityName(),
							EntityId = doc.Shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}

				// log
				log.Save();

				foreach (var change in log.Changes())
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = log.TenantId,
							User = processingUser,
							EntityCode = log.EntityName(),
							EntityId = log.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

				


				// commit transaction
				CommitTransaction();
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
			}
		}
	}
}
