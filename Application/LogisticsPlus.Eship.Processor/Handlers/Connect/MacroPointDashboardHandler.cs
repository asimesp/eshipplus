﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
    public class MacroPointDashboardHandler : EntityBase
    {
        private readonly IMacroPointDashboardView _view;

        public MacroPointDashboardHandler(IMacroPointDashboardView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<MacroPointOrderSearchCriteria> e)
        {
            var results = new MacroPointOrderSearch().FetchMacroPointOrders(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }
    }
}
