﻿using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
    public class DocDeliveryLogHandler : EntityBase
    {
        private readonly IDocDeliveryLogView _view;

        public DocDeliveryLogHandler(IDocDeliveryLogView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<DocDeliveryLogSearchCriteria> e)
        {
            var results = new DocDeliveryLogSearch().FetchDocDeliveryLogs(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }
    }
}