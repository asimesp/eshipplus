﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
	public class CustomerCommunicationHandler : EntityBase
	{
		private readonly ICustomerCommunicationView _view;
		private readonly CustomerCommunicationValidator _validator = new CustomerCommunicationValidator();

		public CustomerCommunicationHandler(ICustomerCommunicationView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.CustomerSearch += OnCustomerSearch;
		}

		private void OnSave(object sender, ViewEventArgs<CustomerCommunication> e)
		{
			var customerCommunication = e.Argument;

			// check locks
			if (!customerCommunication.IsNew)
			{
				var @lock = customerCommunication.RetrieveLock(_view.ActiveUser, customerCommunication.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// retrieve db customer rating for collection comparisons ...
			var dbCustomerCommunication = new CustomerCommunication(customerCommunication.Id, false);
			dbCustomerCommunication.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				customerCommunication.Connection = Connection;
				customerCommunication.Transaction = Transaction;

				// validate customer rating
				_validator.Messages.Clear();
				if (!_validator.IsValid(customerCommunication))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// get lock on customer and link up
				var cLock = customerCommunication.Customer.ObtainLock(_view.ActiveUser, customerCommunication.Customer.Id);
				if (!cLock.IsUserLock(_view.ActiveUser))
				{
					RollBackTransaction();
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
				cLock.Connection = Connection;
				cLock.Transaction = Transaction;

				// audit log customer rating
				if (customerCommunication.IsNew)
				{
					// save
					customerCommunication.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Customer #:{1}", AuditLogConstants.AddedNew, customerCommunication.Customer.CustomerNumber),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = customerCommunication.EntityName(),
							EntityId = customerCommunication.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (customerCommunication.HasChanges())
				{
					foreach (var change in customerCommunication.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = customerCommunication.EntityName(),
								EntityId = customerCommunication.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					// save
					customerCommunication.Save();
				}

				// ensure link is present with customer and save
				customerCommunication.Customer.Communication = customerCommunication;
				customerCommunication.Customer.Save();

				// release customer lock
				cLock.Delete();

				// process collections
				ProcessNotifications(customerCommunication, dbCustomerCommunication.Notifications);
				ProcessDocDeliveryDocumentTags(customerCommunication, dbCustomerCommunication.DocDeliveryDocTags);

				// commit transaction
				CommitTransaction();

				// return id in case of new
				_view.Set(customerCommunication);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDelete(object sender, ViewEventArgs<CustomerCommunication> e)
		{
			var customerCommunication = e.Argument;

			if (customerCommunication.IsNew)
			{
				_view.Set(new CustomerCommunication());
				return;
			}

			// load collections
			customerCommunication.LoadCollections();

			// obtain lock/check lock
			var @lock = customerCommunication.ObtainLock(_view.ActiveUser, customerCommunication.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}
			var cLock = customerCommunication.Customer.ObtainLock(_view.ActiveUser, customerCommunication.Customer.Id);
			if (!cLock.IsUserLock(_view.ActiveUser))
			{
				@lock.Delete();
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				customerCommunication.Connection = Connection;
				customerCommunication.Transaction = Transaction;

				customerCommunication.Customer.Connection = Connection;
				customerCommunication.Customer.Transaction = Transaction;

				cLock.Connection = Connection;
				cLock.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteCustomerCommunication(customerCommunication))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Customer #{1} Communication Ref#{2}", AuditLogConstants.Delete,
													customerCommunication.Customer.CustomerNumber, customerCommunication.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = customerCommunication.EntityName(),
						EntityId = customerCommunication.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// handle collection deletes
				foreach (var notification in customerCommunication.Notifications)
				{
					notification.Connection = Connection;
					notification.Transaction = Transaction;

					notification.Delete();
				}
				foreach(var tag in customerCommunication.DocDeliveryDocTags)
				{
					tag.Connection = Connection;
					tag.Transaction = Transaction;

					tag.Delete();
				}

				// remove customer association
				customerCommunication.Customer.Communication = null;
				customerCommunication.Customer.Save();

				// delete customer and lock
				customerCommunication.Delete();

				// remove locks
				@lock.Delete();
				cLock.Delete();

				CommitTransaction();

				_view.Set(new CustomerCommunication());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(e.Argument, _view.ActiveUser.TenantId);

			_view.DisplayCustomer(customer ?? new Customer());
			if (customer == null)
				_view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
		}

		private void OnUnLock(object sender, ViewEventArgs<CustomerCommunication> e)
		{
			var rating = e.Argument;
			var @lock = rating.RetrieveLock(_view.ActiveUser, rating.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<CustomerCommunication> e)
		{
			var customerCommunication = e.Argument;
			var @lock = customerCommunication.ObtainLock(_view.ActiveUser, customerCommunication.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<CustomerCommunication> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}

		private void OnLoading(object sender, EventArgs e)
		{
            _view.DocumentTags = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].DocumentTags;
			_view.Milestones = ProcessorUtilities.GetAll<Milestone>();

			var notificationMethods = ProcessorUtilities.GetAll<NotificationMethod>();
			notificationMethods.Remove(NotificationMethod.Fax.ToInt());
			_view.NotificationMethods = notificationMethods;
		}

		private void ProcessNotifications(CustomerCommunication customerCommunication, IEnumerable<CustomerNotification> dbNotifications)
		{
			var dbDelete = dbNotifications.Where(markup => !customerCommunication.Notifications.Select(m => m.Id).Contains(markup.Id));

			foreach (var notification in dbDelete)
			{
				notification.Connection = Connection;
				notification.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Customer Communication Ref#{2} Notification Ref#{3}",
												AuditLogConstants.Delete, notification.EntityName(), customerCommunication.Id, notification.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = customerCommunication.EntityName(),
					EntityId = customerCommunication.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				notification.Delete();
			}

			foreach (var notification in customerCommunication.Notifications)
			{
				notification.Connection = Connection;
				notification.Transaction = Transaction;

				if (notification.IsNew)
				{
					notification.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Customer Communication Ref#{2} Notification Ref#{3}",
							                            AuditLogConstants.AddedNew, notification.EntityName(), customerCommunication.Id,
							                            notification.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = customerCommunication.EntityName(),
							EntityId = customerCommunication.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (notification.HasChanges())
				{
					foreach (var change in notification.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Ref#{1} {2}", notification.EntityName(), notification.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = customerCommunication.EntityName(),
								EntityId = customerCommunication.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					notification.Save();
				}
			}
		}

		private void ProcessDocDeliveryDocumentTags(CustomerCommunication customerCommunication, IEnumerable<DocDeliveryDocTag> dbTags)
		{
			var dbDelete = dbTags.Where(u => !customerCommunication.DocDeliveryDocTags.Select(tag => tag.DocumentTagId).Contains(u.DocumentTagId));

			foreach (var tag in dbDelete)
			{
				tag.Connection = Connection;
				tag.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Customer Communication Ref#{2} Document Tag Ref#{3}",
												AuditLogConstants.Delete, tag.EntityName(), customerCommunication.Id, tag.DocumentTagId),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = customerCommunication.EntityName(),
					EntityId = customerCommunication.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				tag.Delete();
			}

			foreach (var tag in customerCommunication.DocDeliveryDocTags)
				if (!_validator.CustomerCommunicationDocDeliveryDocumentTagExist(tag))
				{
					tag.Connection = Connection;
					tag.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Customer Communication Ref#{2} Document Tag Ref#{3}",
												AuditLogConstants.AddedNew, tag.EntityName(), customerCommunication.Id, tag.DocumentTagId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = customerCommunication.EntityName(),
						EntityId = customerCommunication.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					tag.Save();
				}
		}
	}
}
