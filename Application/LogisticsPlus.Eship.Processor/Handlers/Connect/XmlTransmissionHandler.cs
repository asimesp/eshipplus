﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
	public class XmlTransmissionHandler : EntityBase
	{
		private readonly IXmlTransmissionView _view;

		public XmlTransmissionHandler(IXmlTransmissionView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Search += OnSearch;
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.SendOkay = new Dictionary<int, string> {{0, "No"}, {1, "Yes"}};
			_view.Directions = ProcessorUtilities.GetAll<Direction>();
		}

		private void OnSearch(object sender, ViewEventArgs<XmlTransmissionViewSearchCriteria> e)
		{
			var results = new XmlTransmissionSearch().FetchXmlTransmissions(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}
	}
}
