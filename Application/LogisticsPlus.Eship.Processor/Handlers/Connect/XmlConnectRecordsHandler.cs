﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
	public class XmlConnectRecordsHandler: EntityBase
	{
		private readonly IXmlConnectRecordsView _view;

		private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();
		private readonly LoadOrderValidator _validator = new LoadOrderValidator();

		public XmlConnectRecordsHandler(IXmlConnectRecordsView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
			_view.ProcessLoadTenderAccept += OnProcessLoadTenderAccept;
		}

		private void OnProcessLoadTenderAccept(object sender, ViewEventArgs<XmlConnect, LoadOrder> e)
		{
			var xc = e.Argument;
			var load = e.Argument2;

			// check locks
			if (!load.IsNew)
			{
				_view.DisplayMessages(new[] {ValidationMessage.Error("Cannot save to existing load order")});
				return;
			}

			// assign new numbers
			var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ShipmentNumber, _view.ActiveUser);
			load.LoadOrderNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				load.Connection = Connection;
				load.Transaction = Transaction;

				// validate load order
				_validator.Messages.Clear();
				if (!_validator.IsValid(load))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (load.Status != LoadOrderStatus.Offered)
				{
					RollBackTransaction();
					_view.DisplayMessages(new[] { ValidationMessage.Error("Invalid load record for save. Incorrect status.") });
					return;
				}

				// validate purchase order if necessary
				if (load.Customer.ValidatePurchaseOrderNumber && !string.IsNullOrEmpty(load.PurchaseOrderNumber) &&
				    !PurchaseOrderNumberIsValid(load))
				{
					RollBackTransaction();
					return;
				}

				// save
				load.Save();

				// audit log load order
				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Quote: {1}", AuditLogConstants.AddedNew, load.LoadOrderNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = load.EntityName(),
						EntityId = load.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// locations
				ProcessLoadOrderLocations(load);

				// resave to ensure location id saved
				load.Save();

				// process rest of collections
				ProcessLoadOrderServices(load);
				ProcessLoadOrderReferences(load);
				ProcessLoadOrderItems(load);
				ProcessLoadOrderCharges(load);
				ProcessLoadOrderAccountBuckets(load);

				// commit transaction
				CommitTransaction();

				//finalize
				_view.FinalizeLoadOrderAccepted(xc, load);
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}


		private void OnSearch(object sender, ViewEventArgs<XmlConnectSearchCriteria> e)
		{
			var results = new XmlConnectSearch().FetchXmlConnectRecords(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}


		private void ProcessLoadOrderItems(LoadOrder loadOrder)
		{
			foreach (var i in loadOrder.Items)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				i.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Item Ref#:{3}",
												AuditLogConstants.AddedNew, i.EntityName(), loadOrder.LoadOrderNumber, i.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessLoadOrderReferences(LoadOrder loadOrder)
		{
			foreach (var r in loadOrder.CustomerReferences)
			{
				r.Connection = Connection;
				r.Transaction = Transaction;

				r.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Customer Reference:{3}",
												AuditLogConstants.AddedNew, r.EntityName(), loadOrder.LoadOrderNumber, r.Name),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessLoadOrderServices(LoadOrder loadOrder)
		{
			foreach (var brs in loadOrder.Services)
			{
				brs.Connection = Connection;
				brs.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Load Order Number:{2} Service Ref#:{3}", AuditLogConstants.AddedNew,
									  brs.EntityName(),
									  loadOrder.LoadOrderNumber, brs.ServiceId),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				brs.Save();
			}
		}

		private void ProcessLoadOrderLocations(LoadOrder loadOrder)
		{
			var locations = new List<LoadOrderLocation> { loadOrder.Origin, loadOrder.Destination };

			foreach (var l in locations)
			{
				l.Connection = Connection;
				l.Transaction = Transaction;

				l.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3}",
												AuditLogConstants.AddedNew, l.EntityName(), loadOrder.LoadOrderNumber, l.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();


				ProcessContacts(loadOrder, l);
			}
		}

		private void ProcessContacts(LoadOrder loadOrder, LoadOrderLocation location)
		{
			foreach (var c in location.Contacts)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				c.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3} Contact Ref#: {4}",
												AuditLogConstants.AddedNew, c.EntityName(), loadOrder.LoadOrderNumber, location.Id, c.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			}
		}

		private void ProcessLoadOrderCharges(LoadOrder loadOrder)
		{
			foreach (var charge in loadOrder.Charges)
			{
				charge.Connection = Connection;
				charge.Transaction = Transaction;

				charge.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Charge Ref#:{3}",
												AuditLogConstants.AddedNew, charge.EntityName(), loadOrder.LoadOrderNumber, charge.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessLoadOrderAccountBuckets(LoadOrder loadOrder)
		{

			foreach (var loab in loadOrder.AccountBuckets)
			{
				loab.Connection = Connection;
				loab.Transaction = Transaction;

				loab.Save();
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Account Bucket Code:{3}",
												AuditLogConstants.AddedNew, loab.EntityName(), loadOrder.LoadOrderNumber, loab.AccountBucket.Code),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}



		private bool PurchaseOrderNumberIsValid(LoadOrder loadOrder)
		{
			var purchaseOrder = new CustomerPurchaseOrderSearch()
				.FetchCustomerPurchaseOrder(loadOrder.TenantId, loadOrder.Customer.Id, loadOrder.PurchaseOrderNumber);

			if (purchaseOrder == null)
			{
				var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
							  ? ProcessorVars.MissingPOErrMsg
							  : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;

				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			var @lock = purchaseOrder.ObtainLock(_view.ActiveUser, loadOrder.Customer.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return false;
			}

			if (purchaseOrder.ExpirationDate.TimeToMaximum() < DateTime.Now.TimeToMaximum())
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
							  ? string.Format(ProcessorVars.ExpiredPOErrMsgDateFormat, purchaseOrder.ExpirationDate.FormattedShortDateAlt())
							  : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			if (purchaseOrder.ApplyOnOrigin && !(purchaseOrder.ValidPostalCode == loadOrder.Origin.PostalCode && purchaseOrder.CountryId == loadOrder.Origin.CountryId))
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
							? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "origin")
							: loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			if (purchaseOrder.ApplyOnDestination &&
				!(purchaseOrder.ValidPostalCode == loadOrder.Destination.PostalCode && purchaseOrder.CountryId == loadOrder.Destination.CountryId))
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
							? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "destination")
							: loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			purchaseOrder.TakeSnapShot();

			purchaseOrder.Connection = Connection;
			purchaseOrder.Transaction = Transaction;

			@lock.Connection = Connection;
			@lock.Transaction = Transaction;

			if (++purchaseOrder.CurrentUses > purchaseOrder.MaximumUses)
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.MaxUsePOErrMsgFormat, purchaseOrder.MaximumUses) });
				return false;
			}


			foreach (var change in purchaseOrder.Changes())
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} for load order [{1}]", change, loadOrder.LoadOrderNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = purchaseOrder.EntityName(),
					EntityId = purchaseOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			// save purchase order
			purchaseOrder.Save();

			// remove lock
			@lock.Delete();

			return true;
		}
	}
}
