﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
	public class VendorCommunicationHandler : EntityBase
	{
		private readonly IVendorCommunicationView _view;
		private readonly VendorCommunicationValidator _validator = new VendorCommunicationValidator();

		public VendorCommunicationHandler(IVendorCommunicationView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.VendorSearch += OnVendorSearch;
		}

		private void OnSave(object sender, ViewEventArgs<VendorCommunication> e)
		{
			var vendorCommunication = e.Argument;

			// check locks
			if (!vendorCommunication.IsNew)
			{
				var @lock = vendorCommunication.RetrieveLock(_view.ActiveUser, vendorCommunication.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// retrieve db vendor rating for collection comparisons ...
			var dbVendorCommunication = new VendorCommunication(vendorCommunication.Id, false);
			dbVendorCommunication.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				vendorCommunication.Connection = Connection;
				vendorCommunication.Transaction = Transaction;

				// validate vendor rating
				_validator.Messages.Clear();
				if (!_validator.IsValid(vendorCommunication))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// get lock on vendor and link up
				var cLock = vendorCommunication.Vendor.ObtainLock(_view.ActiveUser, vendorCommunication.Vendor.Id);
				if (!cLock.IsUserLock(_view.ActiveUser))
				{
					RollBackTransaction();
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
				cLock.Connection = Connection;
				cLock.Transaction = Transaction;

				// audit log vendor rating
				if (vendorCommunication.IsNew)
				{
					// save
					vendorCommunication.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Vendor #:{1}", AuditLogConstants.AddedNew, vendorCommunication.Vendor.VendorNumber),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendorCommunication.EntityName(),
							EntityId = vendorCommunication.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (vendorCommunication.HasChanges())
				{
					foreach (var change in vendorCommunication.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = vendorCommunication.EntityName(),
								EntityId = vendorCommunication.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					// save
					vendorCommunication.Save();
				}

				// ensure link is present with vendor and save
				vendorCommunication.Vendor.Communication = vendorCommunication;
				vendorCommunication.Vendor.Save();

				// release vendor lock
				cLock.Delete();

				// process collections
				ProcessNotifications(vendorCommunication, dbVendorCommunication.Notifications);

				// commit transaction
				CommitTransaction();

				// return id in case of new
				_view.Set(vendorCommunication);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDelete(object sender, ViewEventArgs<VendorCommunication> e)
		{
			var vendorCommunication = e.Argument;

			if (vendorCommunication.IsNew)
			{
				_view.Set(new VendorCommunication());
				return;
			}

			// load collections
			vendorCommunication.LoadCollections();

			// obtain lock/check lock
			var @lock = vendorCommunication.ObtainLock(_view.ActiveUser, vendorCommunication.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}
			var cLock = vendorCommunication.Vendor.ObtainLock(_view.ActiveUser, vendorCommunication.Vendor.Id);
			if (!cLock.IsUserLock(_view.ActiveUser))
			{
				@lock.Delete();
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				vendorCommunication.Connection = Connection;
				vendorCommunication.Transaction = Transaction;

				vendorCommunication.Vendor.Connection = Connection;
				vendorCommunication.Vendor.Transaction = Transaction;

				cLock.Connection = Connection;
				cLock.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteVendorCommunication(vendorCommunication))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Vendor #{1} Communication Ref#{2}", AuditLogConstants.Delete,
													vendorCommunication.Vendor.VendorNumber, vendorCommunication.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = vendorCommunication.EntityName(),
						EntityId = vendorCommunication.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// handle collection deletes
				foreach (var markup in vendorCommunication.Notifications)
				{
					markup.Connection = Connection;
					markup.Transaction = Transaction;

					markup.Delete();
				}

				// remove vendor association
				vendorCommunication.Vendor.Communication = null;
				vendorCommunication.Vendor.Save();

				// delete vendor and lock
				vendorCommunication.Delete();

				// remove locks
				@lock.Delete();
				cLock.Delete();

				CommitTransaction();

				_view.Set(new VendorCommunication());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnVendorSearch(object sender, ViewEventArgs<string> e)
		{
			var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);

			_view.DisplayVendor(vendor ?? new Vendor());
			if (vendor == null)
				_view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
		}

		private void OnUnLock(object sender, ViewEventArgs<VendorCommunication> e)
		{
			var rating = e.Argument;
			var @lock = rating.RetrieveLock(_view.ActiveUser, rating.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<VendorCommunication> e)
		{
			var vendorCommunication = e.Argument;
			var @lock = vendorCommunication.ObtainLock(_view.ActiveUser, vendorCommunication.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<VendorCommunication> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}

		private void OnLoading(object sender, EventArgs e)
		{
			var okayMilestones = new List<int> {Milestone.NewShipmentRequest.ToInt(), Milestone.ShipmentVoided.ToInt(), Milestone.TruckloadBidNotification.ToInt()};
			_view.Milestones = ProcessorUtilities
				.GetAll<Milestone>()
				.Where(i => okayMilestones.Contains(i.Key))
				.ToDictionary(i => i.Key, i => i.Value);
			_view.NotificationMethods = ProcessorUtilities.GetAll<NotificationMethod>();
		}

		private void ProcessNotifications(VendorCommunication vendorCommunication, IEnumerable<VendorNotification> dbNotifications)
		{
			var dbDelete = dbNotifications.Where(markup => !vendorCommunication.Notifications.Select(m => m.Id).Contains(markup.Id));

			foreach (var notification in dbDelete)
			{
				notification.Connection = Connection;
				notification.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor Communication Ref#{2} Notification Ref#{3}",
												AuditLogConstants.Delete, notification.EntityName(), vendorCommunication.Id, notification.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = vendorCommunication.EntityName(),
					EntityId = vendorCommunication.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				notification.Delete();
			}

			foreach (var notification in vendorCommunication.Notifications)
			{
				notification.Connection = Connection;
				notification.Transaction = Transaction;

				if (notification.IsNew)
				{
					notification.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Vendor Communication Ref#{2} Notification Ref#{3}",
							                            AuditLogConstants.AddedNew, notification.EntityName(), vendorCommunication.Id,
							                            notification.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = vendorCommunication.EntityName(),
							EntityId = vendorCommunication.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (notification.HasChanges())
				{
					foreach (var change in notification.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Ref#{1} {2}", notification.EntityName(), notification.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = vendorCommunication.EntityName(),
								EntityId = vendorCommunication.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					notification.Save();
				}
			}
		}
	}
}
