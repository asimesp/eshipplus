﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{

	/// <summary>
	/// Every method will save object that is passed to it.  Object should not be modified in any other way unless intended modificatio is validated and to be saved
	/// </summary>
	public class DocDeliveryProcessorHandler : EntityBase
	{
		public void LogInvoiceDelivery(Invoice invoice, User executingUser, out Exception ex)
		{
			// obtain record lock
			var @lock = invoice.ObtainLock(executingUser, invoice.Id);
			if (!@lock.IsUserLock(executingUser))
			{
				ex = new Exception(ProcessorVars.UnableToObtainLockErrMsg + " Invoice Record: " + invoice.InvoiceNumber);
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				invoice.FtpDelivered = true;	// set delivered flag

				foreach (var change in invoice.Changes())
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = change,
						TenantId = invoice.TenantId,
						User = executingUser,
						EntityCode = invoice.EntityName(),
						EntityId = invoice.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// save
				invoice.Save();

				// release record lock
				@lock.Delete();

				// commit transaction
				CommitTransaction();

				ex = null;
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
			}
		}

		public void LogShipmentBolDelivery(Shipment shipment, User executingUser, out Exception ex)
		{
			// obtain record lock
			var @lock = shipment.ObtainLock(executingUser, shipment.Id);
			if (!@lock.IsUserLock(executingUser))
			{
				ex = new Exception(ProcessorVars.UnableToObtainLockErrMsg + " Shipment Record: " + shipment.ShipmentNumber);
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				shipment.BolFtpDelivered = true;	// set delivered flag

				foreach (var change in shipment.Changes())
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = change,
						TenantId = shipment.TenantId,
						User = executingUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// save
				shipment.Save();

				// release record lock
				@lock.Delete();


				// commit transaction
				CommitTransaction();

				ex = null;
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
			}
		}

		public void LogShipmentStatementDelivery(Shipment shipment, User executingUser, out Exception ex)
		{
			// obtain record lock
			var @lock = shipment.ObtainLock(executingUser, shipment.Id);
			if (!@lock.IsUserLock(executingUser))
			{
				ex = new Exception(ProcessorVars.UnableToObtainLockErrMsg + " Shipment Record: " + shipment.ShipmentNumber);
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				shipment.StatementFtpDelivered = true;	// set delivered flag


				foreach (var change in shipment.Changes())
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = change,
						TenantId = shipment.TenantId,
						User = executingUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// save
				shipment.Save();

				// release record lock
				@lock.Delete();

				// commit transaction
				CommitTransaction();

				ex = null;
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
			}
		}

		public void LogShipmentDocumentDelivery(ShipmentDocument document, User executingUser, out Exception ex)
		{
			var shipment = document.Shipment;

			// obtain record lock
			var @lock = shipment.ObtainLock(executingUser, shipment.Id);
			if (!@lock.IsUserLock(executingUser))
			{
				ex = new Exception(ProcessorVars.UnableToObtainLockErrMsg + " Shipment Record: " + shipment.ShipmentNumber);
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				document.FtpDelivered = true;	// set delivered flag

				foreach (var change in document.Changes())
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Document Ref#:{1} {2}", document.EntityName(), document.Id, change),
						TenantId = document.TenantId,
						User = executingUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// save
				document.Save();

				// release record lock
				@lock.Delete();

				// commit transaction
				CommitTransaction();

				ex = null;
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
			}
		}


		public void SaveDocDeliveryLog(DocDeliveryLog log, out Exception ex)
		{
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				var validator = new DocDeliveryLogValidator { Connection = Connection, Transaction = Transaction };
				if (!validator.IsValid(log))
				{
					RollBackTransaction();
					var errors = validator.Messages.Select(m => string.Format("{0}: {1}", m.Type, m.Message));
					ex = new Exception(string.Join(Environment.NewLine, errors.ToArray()));
					return;
				}

				// save
				log.Save();

				// commit transaction
				CommitTransaction();

				ex = null;
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
			}
		}
	}
}
