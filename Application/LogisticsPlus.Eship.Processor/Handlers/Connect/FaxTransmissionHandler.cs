﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
	public class FaxTransmissionHandler : EntityBase
	{
		private readonly IFaxTransmissionView _view;
		private readonly FaxTransmissionValidator _validator = new FaxTransmissionValidator();

		public FaxTransmissionHandler(IFaxTransmissionView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Search += OnSearch;
			_view.UnLock += OnUnLock;
			_view.Lock += OnLock;
		}

		private void OnUnLock(object sender, ViewEventArgs<FaxTransmission> e)
		{
			var transmission = e.Argument;
			var @lock = transmission.RetrieveLock(_view.ActiveUser, transmission.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<FaxTransmission> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<FaxTransmissionSearchCriteria> e)
		{
			var results = new FaxTransmissionSearch().FetchFaxTransmissions(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnSave(object sender, ViewEventArgs<FaxTransmission> e)
		{
			var transmission = e.Argument;

			if (!transmission.IsNew)
			{
				var @lock = transmission.RetrieveLock(_view.ActiveUser, transmission.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				transmission.Connection = Connection;
				transmission.Transaction = Transaction;

				if (!_validator.IsValid(transmission))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (transmission.IsNew)
				{
					transmission.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Fax Transmission Shipment #:{1}", AuditLogConstants.AddedNew, transmission.ShipmentNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = transmission.EntityName(),
						EntityId = transmission.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (transmission.HasChanges())
				{
					foreach (var change in transmission.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = transmission.EntityName(),
								EntityId = transmission.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
				
					transmission.Save();
				}

				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}
	}
}
