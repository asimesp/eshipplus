﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Connect
{
	public class RESTTransmissionLogManager : EntityBase
	{
		public List<ValidationMessage> Log(string streamData, RESTTransmissionSource source, User user = null, string authenticationData = null)
		{
			try
			{
				new RESTTransmissionLog
				{
					LogDateTime = DateTime.Now,
					Source = source,
					StreamData = streamData,
					AuthenticationData = string.IsNullOrEmpty(authenticationData) ? string.Empty : authenticationData,
					User = user,
				}.Log();
			}
			catch (Exception ex)
			{
				return new List<ValidationMessage> { ValidationMessage.Error("Error Logging: [{0}]", ex.ToString()) };
			}

			return new List<ValidationMessage>();
		}
	}
}
