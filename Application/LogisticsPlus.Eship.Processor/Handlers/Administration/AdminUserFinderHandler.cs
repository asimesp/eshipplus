﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Administration
{
    public class AdminUserFinderHandler : EntityBase
    {
        private readonly IAdminUserFinderView _view;

        public AdminUserFinderHandler(IAdminUserFinderView view)
		{
			_view = view;
		}

        public void Initialize()
        {
            _view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var results = new AdminUserSearch().FetchAdminUsers(e.Argument);

            _view.DisplaySearchResult(results);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
        }
    }
}
