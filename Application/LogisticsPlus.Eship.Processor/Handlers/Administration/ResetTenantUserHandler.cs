﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using ObjToSql.Core;
using LogisticsPlus.Eship.Processor.Views.Administration;

namespace LogisticsPlus.Eship.Processor.Handlers.Administration
{
    public class ResetTenantUserHandler : EntityBase
    {
        private const string NoDefaultUserMessage = "No default tenant user";
        private const string PasswordResetMessage = "New password: ";
        private const string ResetFailedMessage = "Error attempting to reset Failed Login attempts";
        private const string ResetMessage = "Failed login attempts reset";
        private const string TenantUsernotFoundMessage = "Tenant user was not found";
        private const string UserActivatedMessage = "User account has been activated";

        private readonly IResetTenantUserView _view;

        public ResetTenantUserHandler(IResetTenantUserView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Reset += OnResetUser;
        }

        private void OnResetUser(object sender, ViewEventArgs<TenantUserResetCriteria> e)
        {
            var user = new UserSearch().FetchUserByUsernameAndAccessCode(e.Argument.UserName, e.Argument.TenantCode);
            var resetPassword = e.Argument.ResetPassword;
            var enableUser = e.Argument.EnableUser;

            if (user == null)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(TenantUsernotFoundMessage) });
                return;
            }
            user.TakeSnapShot();

            user.FailedLoginAttempts = default(int);

            if (resetPassword)
                user.Password = user.Username + DateTime.Now.ToString("ffff");

            var userEnabled = false;
            if (enableUser && !user.Enabled)
            {
                user.Enabled = true;
                userEnabled = true;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                user.Connection = Connection;
                user.Transaction = Transaction;

                if (user.Tenant.DefaultSystemUser == null)
                {
                    RollBackTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Error(NoDefaultUserMessage) });
                    return;
                }

                user.Save();

                if (user.HasChanges())
                {
                    foreach (var change in user.Changes())
                        new AdminAuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("sys [{0}]: {1}", user.Tenant.DefaultSystemUser.Username, change),
                                EntityCode = user.EntityName(),
                                EntityId = user.Id.ToString(),
                                AdminUser = _view.ActiveSuperUser,
                                LogDateTime = DateTime.Now
                            }.Log();
                }
                CommitTransaction();

                var messages = new List<ValidationMessage>{ValidationMessage.Information(ResetMessage)};
                
                if (resetPassword)
                    messages.Add(ValidationMessage.Information(string.Format("{0}{1}", PasswordResetMessage, user.Password)));

                if (userEnabled) messages.Add(ValidationMessage.Information(UserActivatedMessage));

                _view.DisplayMessages(messages.ToArray());

            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ResetFailedMessage) });
            }
        }
    }
}
