﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Administration
{
	public class UserHandler : EntityBase
	{
		private readonly IUserView _view;
		private readonly UserValidator _uValidator = new UserValidator();
		private readonly GroupUserValidator _guValidator = new GroupUserValidator();

		private readonly UserPermissionValidator _upValidator = new UserPermissionValidator();

		public UserHandler(IUserView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.CustomerSearch += OnCustomerSearch;
			_view.UpdateGroupUsers += OnUpdateGroupUsers;
			_view.RemoveGroupUsers += OnRemoveGroupUsers;
		    _view.BatchImport += OnBatchImport;
            _view.VendorSearch += OnVendorSearch;
		}

	    private void OnBatchImport(object sender, ViewEventArgs<List<User>> e)
	    {
            var messages = new List<ValidationMessage>();
            foreach (var user in e.Argument)
            {
                Lock @lock = null;
                if (!user.IsNew)
                {
                    @lock = user.ObtainLock(_view.ActiveUser, user.Id);
                    if (!@lock.IsUserLock(_view.ActiveUser))
                    {
                        _view.DisplayMessages(new[] { ValidationMessage.Error(string.Format("{0} User: {1}", ProcessorVars.UnableToObtainLockErrMsg, user.Username)) });
                        continue;
                    }
                }

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _uValidator.Connection = Connection;
                    _uValidator.Transaction = Transaction;
                    _guValidator.Connection = Connection;
                    _guValidator.Transaction = Transaction;

                    user.Connection = Connection;
                    user.Transaction = Transaction;

                    // validation
                    var validationMessages = Validate(user);
                    if (validationMessages.Count > 0)
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(validationMessages);
                        return;
                    }

                    var dbUser = user.IsNew ? new User() : new User(user.Id, false);
                    if(!dbUser.IsNew) dbUser.LoadCollections();

                    if (user.IsNew)
                    {
                        user.Save();

                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description =
                                string.Format("{0} {1} {2} {3}", AuditLogConstants.AddedNew, user.Username, user.FirstName, user.LastName),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = user.EntityName(),
                            EntityId = user.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    }
                    else if (user.HasChanges())
                    {
                        foreach (var change in user.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = user.EntityName(),
                                EntityId = user.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                        user.Save();
                    }


                    ProcessPermissions(user, dbUser.UserPermissions);
                    ProcessUserShipAs(user, dbUser.UserShipAs);
                    ProcessUserGroups(user, dbUser.UserGroups);

                    if(@lock != null) @lock.Delete();

                    // commit transaction
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [User: {1}] {2}", ProcessorVars.RecordSaveErrMsg, user.Username, ex.Message));
                }
            }

            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
	    }


	    private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(e.Argument, _view.ActiveUser.TenantId);

			_view.DisplayCustomer(customer ?? new Customer());
			if (customer == null)
				_view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
		}
        
        private void OnVendorSearch(object sender, ViewEventArgs<string> e)
		{
			var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);

			_view.DisplayVendor(vendor ?? new Vendor());
			if (vendor == null)
				_view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.Departments = new UserDepartmentSearch().FetchUserDepartments("%", _view.ActiveUser.TenantId);
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<User> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
		}

		private void OnSave(object sender, ViewEventArgs<User> e)
		{
			var user = e.Argument;

			if (!user.IsNew)
			{
				var @lock = user.RetrieveLock(_view.ActiveUser, user.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			var dbUser = new User(user.Id, false);
			dbUser.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_uValidator.Connection = Connection;
				_uValidator.Transaction = Transaction;
				_upValidator.Connection = Connection;
				_upValidator.Transaction = Transaction;

				user.Connection = Connection;
				user.Transaction = Transaction;

				var validationMessages = Validate(user);
				if (validationMessages.Count > 0)
				{
					RollBackTransaction();
					_view.DisplayMessages(validationMessages);
					return;
				}

				if (user.IsNew)
				{
					user.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} {2} {3}", AuditLogConstants.AddedNew, user.Username, user.FirstName, user.LastName),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = user.EntityName(),
							EntityId = user.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (user.HasChanges())
				{
					foreach (var change in user.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = user.EntityName(),
								EntityId = user.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					user.Save();
				}


				ProcessPermissions(user, dbUser.UserPermissions);
				ProcessUserShipAs(user, dbUser.UserShipAs);

				CommitTransaction();

				_view.Set(user);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDelete(object sender, ViewEventArgs<User> e)
		{
			var user = e.Argument;

			if (user.IsNew)
			{
				_view.Set(new User());
				return;
			}

			user.LoadCollections();

			var customersThatUserRepresents = user.RetrieveCustomersThatUserRepresents();
			var locks = new List<Lock>();
			if (!ObtainLocksForDelete(locks, user, customersThatUserRepresents))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				user.Connection = Connection;
				user.Transaction = Transaction;

				_uValidator.Connection = Connection;
				_uValidator.Transaction = Transaction;

				foreach (var @lock in locks)
				{
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;
				}

				_uValidator.Messages.Clear();
				if (!_uValidator.CanDeleteUser(user))
				{
					RollBackTransaction();
					foreach (var @lock in locks) @lock.Delete();
					_view.DisplayMessages(_uValidator.Messages);
					return;
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} {2} {3} {4}", AuditLogConstants.Delete, user.Username, user.FirstName, user.LastName, user.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = user.EntityName(),
					EntityId = user.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				foreach (var permission in user.UserPermissions)
				{
					permission.Connection = Connection;
					permission.Transaction = Transaction;

					permission.Delete();
				}
				foreach (var groupUser in user.UserGroups)
				{
					groupUser.Connection = Connection;
					groupUser.Transaction = Transaction;

					groupUser.Delete();
				}
				foreach (var shipAs in user.UserShipAs)
				{
					shipAs.Connection = Connection;
					shipAs.Transaction = Transaction;

					shipAs.Delete();
				}
				foreach (var rep in customersThatUserRepresents)
				{
					rep.Connection = Connection;
					rep.Transaction = Transaction;

					rep.Delete();
				}

				user.PurgeUserSearchProfileDefault();
				user.PurgeUserRecordLocks();
				user.Delete();

				foreach (var @lock in locks) @lock.Delete();

				CommitTransaction();
				_view.Set(new User());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				foreach (var @lock in locks) @lock.Delete();
			    _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnUnLock(object sender, ViewEventArgs<User> e)
		{
			var user = e.Argument;

			var @lock = user.RetrieveLock(_view.ActiveUser, user.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}

			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<User> e)
		{
			var user = e.Argument;
			var @lock = user.ObtainLock(_view.ActiveUser, user.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private bool ObtainLocksForDelete(ICollection<Lock> locks, User user, IEnumerable<CustomerServiceRepresentative> customersThatUserRepresents)
		{
			var @lock = user.ObtainLock(_view.ActiveUser, user.Id);
			if (@lock != null)
			{
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					return false;
				}
				locks.Add(@lock);
			}

			foreach (var groupUser in user.UserGroups)
			{
				@lock = groupUser.Group.ObtainLock(_view.ActiveUser, groupUser.Group.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					return false;
				}
				if (@lock != null) locks.Add(@lock);
			}

			foreach (var reps in customersThatUserRepresents)
			{
				@lock = reps.Customer.ObtainLock(_view.ActiveUser, reps.Customer.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					return false;
				}
				if (@lock != null) locks.Add(@lock);
			}

			return true;
		}

		private List<ValidationMessage> Validate(User user)
		{
			var messages = new List<ValidationMessage>();

			_uValidator.Messages.Clear();
			_upValidator.Messages.Clear();

			if (!_uValidator.IsValid(user))
			{
				RollBackTransaction();
				messages.AddRange(_uValidator.Messages);
			}

			if (user.UserPermissions.Any(up => !_upValidator.IsValid(up)))
			{
				RollBackTransaction();
				messages.AddRange(_upValidator.Messages);
			}

			return messages;
		}

		private void ProcessPermissions(User user, IEnumerable<UserPermission> dbPermissions)
		{
			var dbDelete = dbPermissions.Where(p => !user.UserPermissions.Select(up => up.Id).Contains(p.Id));

			foreach (var p in dbDelete)
			{
				p.Connection = Connection;
				p.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} User:{2} {3} {4} Code:{5}", AuditLogConstants.Delete, p.EntityName(), user.Username,
										  user.FirstName,
										  user.LastName, p.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = user.EntityName(),
						EntityId = user.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				p.Delete();
			}

			foreach (var p in user.UserPermissions)
			{
				p.Connection = Connection;
				p.Transaction = Transaction;

				if (p.IsNew)
				{
					p.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} User:{2} {3} {4} Code:{5}", AuditLogConstants.AddedNew, p.EntityName(), user.Username,
											  user.FirstName,
											  user.LastName, p.Code),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = user.EntityName(),
							EntityId = user.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (p.HasChanges())
				{
					foreach (var change in p.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} {1}", p.EntityName(), change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = user.EntityName(),
								EntityId = user.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					p.Save();
				}
			}
		}

		private void ProcessUserShipAs(User user, IEnumerable<UserShipAs> dbShipAs)
		{
			var dbDelete = dbShipAs.Where(u => !user.UserShipAs.Select(usa => usa.CustomerId).Contains(u.CustomerId));

			foreach (var usa in dbDelete)
			{
				usa.Connection = Connection;
				usa.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} User:{2} Customer Id:{3}", AuditLogConstants.Delete, usa.EntityName(), user.Username,
							              usa.CustomerId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = user.EntityName(),
						EntityId = user.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				usa.Delete();
			}

			foreach (var usa in user.UserShipAs)
				if (!_uValidator.UserShipAsExists(usa))
				{
					usa.Connection = Connection;
					usa.Transaction = Transaction;

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} User:{2} Customer Id:{3}", AuditLogConstants.AddedNew, usa.EntityName(), user.Username,
								              usa.CustomerId),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = user.EntityName(),
							EntityId = user.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					usa.Save();
				}
		}

        private void ProcessUserGroups(User user, IEnumerable<GroupUser> dbGroupUsers)
        {
            var dbDelete = dbGroupUsers.Where(u => !user.UserGroups.Select(usa => usa.GroupId).Contains(u.GroupId));

            foreach (var usg in dbDelete)
            {
                usg.Connection = Connection;
                usg.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} User:{2} Group Id:{3}", AuditLogConstants.Delete, usg.EntityName(), user.Username,
                                      usg.GroupId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = user.EntityName(),
                    EntityId = user.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                usg.Delete();
            }

            foreach (var usg in user.UserGroups)
                if (!_guValidator.GroupUserExists(usg))
                {
                    usg.Connection = Connection;
                    usg.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} User:{2} Group Id:{3}", AuditLogConstants.AddedNew, usg.EntityName(), user.Username,
                                          usg.GroupId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = user.EntityName(),
                        EntityId = user.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    usg.Save();
                }
        }

		private void OnUpdateGroupUsers(object sender, ViewEventArgs<List<Group>> e)
		{
			var messages = new List<ValidationMessage>();

			foreach (var @group in e.Argument)
			{
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					// obtain lock
					var @lock = @group.ObtainLock(_view.ActiveUser, @group.Id);
					if (!@lock.IsUserLock(_view.ActiveUser))
					{
						messages.Add(ValidationMessage.Error("Group [{0}] record in use by: {1} Date: {2: yyyy-MM-dd hh:mm:ss}",
															@group.Name, @lock.User.Username, @lock.LockDateTime));
						continue;
					}

					// chain to transaction
					_guValidator.Connection = Connection;
					_guValidator.Transaction = Transaction;

					@lock.Connection = Connection;
					@lock.Transaction = Transaction;


					foreach (var groupUser in @group.GroupUsers)
						if (!_guValidator.GroupUserExists(groupUser))
						{
							groupUser.Connection = Connection;
							groupUser.Transaction = Transaction;

							new AuditLog
								{
									Connection = Connection,
									Transaction = Transaction,
									Description =
										string.Format("{0} {1} Group:{2} User Id:{3}", AuditLogConstants.AddedNew, groupUser.EntityName(), @group.Name,
													  groupUser.UserId),
									TenantId = _view.ActiveUser.TenantId,
									User = _view.ActiveUser,
									EntityCode = @group.EntityName(),
									EntityId = @group.Id.ToString(),
									LogDateTime = DateTime.Now
								}.Log();

							groupUser.Save();
						}

					// delete lock
					@lock.Delete();

					// commit transaction
					CommitTransaction();

				}
				catch (Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Insert(0, ValidationMessage.Error("An error occurred processing record(s). Some records might not have been processed."));
				}
			}

			// complete transaction
			messages.Add(ValidationMessage.Information("Record(s) processing complete."));
			_view.DisplayMessages(messages);
		}

		private void OnRemoveGroupUsers(object sender, ViewEventArgs<List<GroupUser>> e)
		{
			var messages = new List<ValidationMessage>();

			foreach (var groupUser in e.Argument)
			{
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					// obtain lock
					var @lock = groupUser.Group.ObtainLock(_view.ActiveUser, groupUser.Group.Id);
					if (!@lock.IsUserLock(_view.ActiveUser))
					{
						messages.Add(ValidationMessage.Error("Group [{0}] record in use by: {1} Date: {2: yyyy-MM-dd hh:mm:ss}",
															groupUser.Group.Name, @lock.User.Username, @lock.LockDateTime));
						continue;
					}

					// chain to transaction
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					groupUser.Connection = Connection;
					groupUser.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Group:{2} User Id:{3}", AuditLogConstants.Delete, groupUser.EntityName(), groupUser.Group.Name, groupUser.UserId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = groupUser.Group.EntityName(),
						EntityId = groupUser.Group.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					// delete record
					groupUser.Delete();

					// delete lock
					@lock.Delete();

					// commit transaction
					CommitTransaction();
				}
				catch (Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Insert(0, ValidationMessage.Error("An error occurred processing record(s). Some records might not have been processed."));
				}
			}

			// complete transaction
			messages.Add(ValidationMessage.Information("Record(s) processing complete."));
			_view.DisplayMessages(messages);
		}
	}
}
