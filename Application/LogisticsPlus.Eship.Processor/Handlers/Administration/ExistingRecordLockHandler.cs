﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Administration
{
	public class ExistingRecordLockHandler : EntityBase
	{
		private readonly IExistingRecordLockView _view;

		public ExistingRecordLockHandler(IExistingRecordLockView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.ReleaseLock += OnReleaseLock;
		}


		private void OnReleaseLock(object sender, ViewEventArgs<long> e)
		{
			var @lock = new Lock(e.Argument) {Connection = Connection, Transaction = Transaction};

			if (@lock.KeyLoaded)
			{
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("Manually {0} Lock Key: {1} Entity Id: {2} Lock User Id: {3}",
								              AuditLogConstants.Delete,
								              @lock.LockKey, @lock.EntityId, @lock.User == null ? default(long) : @lock.User.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = @lock.EntityName(),
							EntityId = @lock.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// delete lock
					@lock.Delete();

					CommitTransaction();
				}
				catch (Exception ex)
				{
					RollBackTransaction();
					_view.LogException(ex);
					_view.DisplayMessages(new[] {ValidationMessage.Error("An error occurred while performing lock release. Err: {0}", ex.Message)});
				}
			}

			LoadLocks();
		}

		private void OnLoading(object sender, EventArgs e)
		{
			LoadLocks();
		}

		private void LoadLocks()
		{
			var locks = new LockSearch().FetchLocks(_view.ActiveUser.TenantId);
			_view.DisplayExistingLocks(locks);		
		}
	}
}
