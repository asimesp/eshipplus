﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Administration
{
	public class AdminUserHandler : EntityBase
	{
		private readonly IAdminUserView _view;
		private readonly AdminUserValidator _uValidator = new AdminUserValidator();

		private readonly AdminUserPermissionValidator _upValidator = new AdminUserPermissionValidator();

		public AdminUserHandler(IAdminUserView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.LoadAuditLog += OnLoadAuditLog;
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<AdminUser> e)
		{
			_view.DisplayAuditLogs(new AdminAuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id));
		}

		private void OnSave(object sender, ViewEventArgs<AdminUser> e)
		{
			var user = e.Argument;

			var dbUser = new AdminUser(user.Id, false);
			dbUser.LoadCollections();


			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_uValidator.Connection = Connection;
				_uValidator.Transaction = Transaction;

				_upValidator.Connection = Connection;
				_upValidator.Transaction = Transaction;

				user.Connection = Connection;
				user.Transaction = Transaction;

				var validationMessages = Validate(user);
				if (validationMessages.Count > 0)
				{
					RollBackTransaction();
					_view.DisplayMessages(validationMessages);
					return;
				}

				if (user.IsNew)
				{
					user.Save();

					new AdminAuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} {2} {3}", AuditLogConstants.AddedNew, user.Username, user.FirstName, user.LastName),
							EntityCode = user.EntityName(),
							EntityId = user.Id.ToString(),
							LogDateTime = DateTime.Now,
							AdminUser = _view.ActiveSuperUser
						}.Log();
				}
				else if (user.HasChanges())
				{
					foreach (var change in user.Changes())
						new AdminAuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								EntityCode = user.EntityName(),
								EntityId = user.Id.ToString(),
								LogDateTime = DateTime.Now,
                                AdminUser = _view.ActiveSuperUser
							}.Log();
					user.Save();
				}


				ProcessPermissions(user, dbUser.AdminUserPermissions);

				CommitTransaction();

				_view.Set(user);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDelete(object sender, ViewEventArgs<AdminUser> e)
		{
			var user = e.Argument;

			if (user.IsNew)
			{
				_view.Set(new AdminUser());
				return;
			}

			user.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				user.Connection = Connection;
				user.Transaction = Transaction;

				_uValidator.Connection = Connection;
				_uValidator.Transaction = Transaction;

				_uValidator.Messages.Clear();
				if (!_uValidator.CanDeleteUser(user))
				{
					RollBackTransaction();
					_view.DisplayMessages(_uValidator.Messages);
					return;
				}

				new AdminAuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} {2} {3} {4}", AuditLogConstants.Delete, user.Username, user.FirstName, user.LastName, user.Id),
					EntityCode = user.EntityName(),
					EntityId = user.Id.ToString(),
					LogDateTime = DateTime.Now,
                    AdminUser = _view.ActiveSuperUser
				}.Log();

				foreach (var permission in user.AdminUserPermissions)
				{
					permission.Connection = Connection;
					permission.Transaction = Transaction;

					permission.Delete();
				}
				user.Delete();

				CommitTransaction();
				_view.Set(new AdminUser());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private List<ValidationMessage> Validate(AdminUser user)
		{
			var messages = new List<ValidationMessage>();

			_uValidator.Messages.Clear();
			_upValidator.Messages.Clear();

			if (!_uValidator.IsValid(user))
			{
				RollBackTransaction();
				messages.AddRange(_uValidator.Messages);
			}

			if (user.AdminUserPermissions.Any(up => !_upValidator.IsValid(up)))
			{
				RollBackTransaction();
				messages.AddRange(_upValidator.Messages);
			}

			return messages;
		}

		private void ProcessPermissions(AdminUser user, IEnumerable<AdminUserPermission> dbPermissions)
		{
			var dbDelete = dbPermissions.Where(p => !user.AdminUserPermissions.Select(up => up.Id).Contains(p.Id));

			foreach (var p in dbDelete)
			{
				p.Connection = Connection;
				p.Transaction = Transaction;

				new AdminAuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} User:{2} {3} {4} Code:{5}", AuditLogConstants.Delete, p.EntityName(), user.Username,
										  user.FirstName,
										  user.LastName, p.Code),
						EntityCode = user.EntityName(),
						EntityId = user.Id.ToString(),
						LogDateTime = DateTime.Now,
                        AdminUser = _view.ActiveSuperUser
					}.Log();

				p.Delete();
			}

			foreach (var p in user.AdminUserPermissions)
			{
				p.Connection = Connection;
				p.Transaction = Transaction;

				if (p.IsNew)
				{
					p.Save();

					new AdminAuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} User:{2} {3} {4} Code:{5}", AuditLogConstants.AddedNew, p.EntityName(), user.Username,
											  user.FirstName,
											  user.LastName, p.Code),
							EntityCode = user.EntityName(),
							EntityId = user.Id.ToString(),
							LogDateTime = DateTime.Now,
                            AdminUser = _view.ActiveSuperUser
						}.Log();
				}
				else if (p.HasChanges())
				{
					foreach (var change in p.Changes())
						new AdminAuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} {1}", p.EntityName(), change),
								EntityCode = user.EntityName(),
								EntityId = user.Id.ToString(),
								LogDateTime = DateTime.Now,
                                AdminUser = _view.ActiveSuperUser
							}.Log();
					p.Save();
				}
			}
		}
	}
}
