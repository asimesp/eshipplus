﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Core;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Administration
{
	public class TenantAdminHandler : EntityBase
	{
		private readonly ITenantAdminView _view;
		private readonly TenantValidator _validator = new TenantValidator();

		public TenantAdminHandler(ITenantAdminView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.DefaultSystemUserSearch += OnDefaultSystemUserSearch;
		}

		private void OnDefaultSystemUserSearch(object sender, ViewEventArgs<string> e)
		{
			var user = new UserSearch().FetchUserByUsernameAndAccessCode(e.Argument, _view.ActiveUser.Tenant.Code);
			_view.DisplayDefaultSystemUser(user);
			if (user == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Default system user [{0}] not found!", e.Argument) });
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.TimeList = TimeUtility.BuildHourlyTimeList();
            _view.MileageEngines = ProcessorUtilities.GetAll<MileageEngine>();
            _view.PaymentGatewayTypes = ProcessorUtilities.GetAll<PaymentGatewayType>();
		}

		private void OnSave(object sender, ViewEventArgs<Tenant> e)
		{
			var tenant = e.Argument;

			if (tenant.IsNew)
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error("Processing of new tenant via this workflow is invalid.") });
				return;
			}

			var @lock = tenant.RetrieveLock(_view.ActiveUser, tenant.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
				return;
			}

			var dbTenant = new Tenant(tenant.Id);
			dbTenant.LoadCollections();
			dbTenant.MailingLocation.LoadCollections();
			dbTenant.BillingLocation.LoadCollections();


			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				tenant.Connection = Connection;
				tenant.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.IsValid(tenant))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (tenant.HasChanges())
				{
					foreach (var change in tenant.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change.StripTags(),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = tenant.EntityName(),
								EntityId = tenant.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					tenant.Save();
				}

				SaveTenantLocation(tenant.MailingLocation, tenant, dbTenant.MailingLocation.Contacts);
				SaveTenantLocation(tenant.BillingLocation, tenant, dbTenant.BillingLocation.Contacts);

				CommitTransaction();

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnLock(object sender, ViewEventArgs<Tenant> e)
		{
			var tenant = e.Argument;
			var @lock = tenant.ObtainLock(_view.ActiveUser, tenant.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnUnLock(object sender, ViewEventArgs<Tenant> e)
		{
			var tenant = e.Argument;

			var @lock = tenant.RetrieveLock(_view.ActiveUser, tenant.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg)});
				return;
			}

			@lock.Delete();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<Tenant> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.Id));
		}

		private void SaveTenantLocation(TenantLocation location, Tenant tenant, IEnumerable<TenantContact> dbContacts)
		{
			location.Connection = Connection;
			location.Transaction = Transaction;

			if (location.HasChanges())
			{
				foreach (var change in location.Changes())
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", location.EntityName(), change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = tenant.EntityName(),
							EntityId = tenant.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				location.Save();
			}

			// contacts

			var dbDelete = dbContacts.Where(c => !location.Contacts.Select(lc => lc.Id).Contains(c.Id));

			foreach (var contact in dbDelete)
			{
				contact.Connection = Connection;
				contact.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Tenant Code:{2} Contact Name:{3}", AuditLogConstants.Delete,
						                            contact.EntityName(), tenant.Code, contact.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = tenant.EntityName(),
						EntityId = tenant.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				contact.Delete();
			}

			foreach (var contact in location.Contacts)
			{
				contact.Connection = Connection;
				contact.Transaction = Transaction;

				if (contact.IsNew)
				{
					contact.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Tenant Code:{2} Contact Name:{3}", AuditLogConstants.AddedNew,
							                            contact.EntityName(), tenant.Code, contact.Name),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = tenant.EntityName(),
							EntityId = tenant.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (contact.HasChanges())
				{
					foreach (var change in contact.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} {1}", contact.EntityName(), change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = tenant.EntityName(),
								EntityId = tenant.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					contact.Save();
				}
			}
		}
	}
}
