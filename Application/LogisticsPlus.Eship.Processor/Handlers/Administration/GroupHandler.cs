﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Administration
{
    public class GroupHandler : EntityBase
    {
        private readonly IGroupView _view;
        private readonly GroupValidator _gValidator = new GroupValidator();
        private readonly GroupPermissionValidator _gpValidator = new GroupPermissionValidator();
        private readonly GroupUserValidator _guValidator = new GroupUserValidator();

        public GroupHandler(IGroupView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.LoadAuditLog += OnLoadAuditLog;
        }

        private void OnLoadAuditLog(object sender, ViewEventArgs<Group> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
        }

        private void OnSave(object sender, ViewEventArgs<Group> e)
        {
            var @group = e.Argument;

        	if (!@group.IsNew)
            {
                var @lock = @group.RetrieveLock(_view.ActiveUser, @group.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            var dbGroup = new Group(@group.Id, false);
            dbGroup.LoadCollections();

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _gValidator.Connection = Connection;
                _gValidator.Transaction = Transaction;
                _gpValidator.Connection = Connection;
                _gpValidator.Transaction = Transaction;
                _guValidator.Connection = Connection;
                _guValidator.Transaction = Transaction;

                @group.Connection = Connection;
                @group.Transaction = Transaction;

                var validationMessages = Validate(@group);
                if (validationMessages.Count > 0)
                {
                    RollBackTransaction();
                    _view.DisplayMessages(validationMessages);
                    return;
                }

            	if (@group.IsNew)
            	{
            		@group.Save();

            		new AuditLog
            			{
            				Connection = Connection,
            				Transaction = Transaction,
            				Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, @group.Name),
            				TenantId = _view.ActiveUser.TenantId,
            				User = _view.ActiveUser,
            				EntityCode = @group.EntityName(),
            				EntityId = @group.Id.ToString(),
            				LogDateTime = DateTime.Now
            			}.Log();
            	}
				else if (@group.HasChanges())
				{
					foreach (var change in @group.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = @group.EntityName(),
								EntityId = @group.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					@group.Save();
				}


            	ProcessPermissions(@group, dbGroup.Permissions);
                ProcessGroupUsers(@group, dbGroup.GroupUsers);

                CommitTransaction();

                _view.Set(@group);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<Group> e)
        {
            var @group = e.Argument;

            if (@group.IsNew)
            {
                _view.Set(group);
                return;
            }

            @group.LoadCollections();

            var @lock = @group.ObtainLock(_view.ActiveUser, @group.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                @group.Connection = Connection;
                @group.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _gValidator.Messages.Clear();
                if (!_gValidator.CanDeleteGroup(@group))
                {
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_gValidator.Messages);
                    return;
                }

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, @group.Name, @group.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = @group.EntityName(),
                        EntityId = @group.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                foreach (var permission in @group.Permissions)
                {
                    permission.Connection = Connection;
                    permission.Transaction = Transaction;

                    permission.Delete();
                }
                foreach (var groupUser in @group.GroupUsers)
                {
                    groupUser.Connection = Connection;
                    groupUser.Transaction = Transaction;

                    groupUser.Delete();
                }

                @group.Delete();

                @lock.Delete();

                CommitTransaction();

                _view.Set(new Group());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnUnLock(object sender, ViewEventArgs<Group> e)
        {
            var @group = e.Argument;

            var @lock = @group.RetrieveLock(_view.ActiveUser, @group.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }

            @lock.Delete();
        }

        private void OnLock(object sender, ViewEventArgs<Group> e)
        {
            var @group = e.Argument;
            var @lock = @group.ObtainLock(_view.ActiveUser, @group.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private List<ValidationMessage> Validate(Group @group)
        {
            var messages = new List<ValidationMessage>();

            _gValidator.Messages.Clear();
            _gpValidator.Messages.Clear();
            _guValidator.Messages.Clear();

            if (!_gValidator.IsValid(@group))
            {
                RollBackTransaction();
                messages.AddRange(_gValidator.Messages);
            }

            if (@group.Permissions.Any(gp => !_gpValidator.IsValid(gp)))
            {
                RollBackTransaction();
                messages.AddRange(_gpValidator.Messages);
            }

            if (@group.GroupUsers.Any(gu => !_guValidator.HasAllRequiredData(gu)))
            {
                RollBackTransaction();
                messages.AddRange(_guValidator.Messages);
            }

            return messages;
        }

        private void ProcessPermissions(Group @group, IEnumerable<GroupPermission> dbPermissions)
        {
            var dbDelete = dbPermissions.Where(p => !@group.Permissions.Select(gp => gp.Id).Contains(p.Id));

            foreach (var p in dbDelete)
            {
                p.Connection = Connection;
                p.Transaction = Transaction;

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Group:{2} Code:{3}", AuditLogConstants.Delete, p.EntityName(), @group.Name, p.Code),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = @group.EntityName(),
                        EntityId = @group.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                p.Delete();
            }

			foreach (var p in @group.Permissions)
			{
				p.Connection = Connection;
				p.Transaction = Transaction;

				if (p.IsNew)
				{
					p.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} Group:{2} Code:{3}", AuditLogConstants.AddedNew, p.EntityName(), @group.Name, p.Code),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = @group.EntityName(),
							EntityId = @group.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (p.HasChanges())
				{
					foreach (var change in p.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} {1}", p.EntityName(), change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = @group.EntityName(),
								EntityId = @group.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					p.Save();
				}
			}
        }

        private void ProcessGroupUsers(Group @group, IEnumerable<GroupUser> dbGroupUsers)
        {
            var dbDelete = dbGroupUsers.Where(u => !@group.GroupUsers.Select(gu => gu.UserId).Contains(u.UserId));

            foreach (var groupUser in dbDelete)
            {
                groupUser.Connection = Connection;
                groupUser.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Group:{2} User Id:{3}", AuditLogConstants.Delete, groupUser.EntityName(), @group.Name, groupUser.UserId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = @group.EntityName(),
                    EntityId = @group.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                groupUser.Delete();
            }

            foreach (var groupUser in @group.GroupUsers)
                if (!_guValidator.GroupUserExists(groupUser))
                {
                    groupUser.Connection = Connection;
                    groupUser.Transaction = Transaction;

                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Group:{2} User Id:{3}", AuditLogConstants.AddedNew, groupUser.EntityName(), @group.Name, groupUser.UserId),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = @group.EntityName(),
                            EntityId = @group.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    groupUser.Save();
                }
        }
    }
}