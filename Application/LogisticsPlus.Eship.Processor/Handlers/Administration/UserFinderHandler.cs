﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Administration
{
	public class UserFinderHandler : EntityBase
	{
		private readonly IUserFinderView _view;

		public UserFinderHandler(IUserFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<UserSearchCriteria> e)
		{
            var results = new UserSearch().FetchUsers(e.Argument, _view.ActiveUser.TenantId);

            _view.DisplaySearchResult(results);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
		}
	}
}
