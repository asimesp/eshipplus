﻿using System;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Handlers
{
	public class SearchProfileDefaultHandler
	{
		public Exception SaveSearchProfileDefaults(SearchProfileDefault @default)
		{
			try
			{
				@default.Save();

				return null;
			}
			catch (Exception ex)
			{
				return ex;
			}
		}
	}
}
