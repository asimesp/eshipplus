﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class QuickPayOptionHandler : EntityBase
	{
		private readonly IQuickPayOptionView _view;

		private readonly QuickPayOptionValidator _validator = new QuickPayOptionValidator();

		public QuickPayOptionHandler(IQuickPayOptionView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<QuickPayOption>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var quickPayOption in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					quickPayOption.Connection = Connection;
					quickPayOption.Transaction = Transaction;

					if (!_validator.IsValid(quickPayOption))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					quickPayOption.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1}", AuditLogConstants.AddedNew, quickPayOption.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = quickPayOption.EntityName(),
						EntityId = quickPayOption.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[quickPayOption.TenantId].UpdateCache(quickPayOption);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, quickPayOption.Code, ex.Message));

				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<QuickPayOption> e)
		{
			QuickPayOption quickPayOption = e.Argument;
			var @lock = quickPayOption.RetrieveLock(_view.ActiveUser, quickPayOption.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<QuickPayOption> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			_view.DisplaySearchResult(new QuickPayOptionSearch().FetchQuickPayOptions(e.Argument, _view.ActiveUser.TenantId));
		}

		private void OnDelete(object sender, ViewEventArgs<QuickPayOption> e)
		{
			_validator.Messages.Clear();

			var quickPayOption = e.Argument;

			if (quickPayOption.IsNew) return;

			var @lock = quickPayOption.ObtainLock(_view.ActiveUser, quickPayOption.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}


			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				quickPayOption.Connection = Connection;
				quickPayOption.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteQuickPayOption(quickPayOption))
				{
					RollBackTransaction();
                    @lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, quickPayOption.Code, quickPayOption.Description),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = quickPayOption.EntityName(),
						EntityId = quickPayOption.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				quickPayOption.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[quickPayOption.TenantId].UpdateCache(quickPayOption, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<QuickPayOption> e)
		{
			var quickPayOption = e.Argument;

			if (!quickPayOption.IsNew)
			{
				var @lock = quickPayOption.RetrieveLock(_view.ActiveUser, quickPayOption.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				quickPayOption.Connection = Connection;
				quickPayOption.Transaction = Transaction;

				if (!_validator.IsValid(quickPayOption) || _validator.HasWarnings)
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (quickPayOption.IsNew)
				{
					quickPayOption.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, quickPayOption.Code,
								              quickPayOption.Description),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = quickPayOption.EntityName(),
							EntityId = quickPayOption.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (quickPayOption.HasChanges())
				{
					foreach (var change in quickPayOption.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = quickPayOption.EntityName(),
								EntityId = quickPayOption.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					quickPayOption.Save();
				}


				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[quickPayOption.TenantId].UpdateCache(quickPayOption);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}
	}
}
