﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
    public class FailureCodeHandler : EntityBase
    {
        private readonly IFailureCodeView _view;

        private readonly FailureCodeValidator _validator = new FailureCodeValidator();

        public FailureCodeHandler(IFailureCodeView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.Save += OnSave;
            _view.BatchImport += OnBatchImport;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.UnLock += OnUnLock;
            _view.Lock += OnLock;
        }

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var search = new FailureCodeSearch();
            var results = search.FetchFailureCodes(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }

        private void OnBatchImport(object sender, ViewEventArgs<List<FailureCode>> e)
        {
            var messages = new ValidationMessageCollection();

            foreach (var failureCode in e.Argument)
            {
                _validator.Messages.Clear();
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    failureCode.Connection = Connection;
                    failureCode.Transaction = Transaction;

                    if (!_validator.IsValid(failureCode))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }

                    failureCode.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1}", AuditLogConstants.AddedNew, failureCode.Code),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = failureCode.EntityName(),
                        EntityId = failureCode.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[failureCode.TenantId].UpdateCache(failureCode);
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [Code: {1}] {2}", ProcessorVars.RecordSaveErrMsg, failureCode.Code, ex.Message));

                }
            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }

        private void OnSave(object sender, ViewEventArgs<FailureCode> e)
        {
            var failureCode = e.Argument;

            if (!failureCode.IsNew)
            {
                var @lock = failureCode.RetrieveLock(_view.ActiveUser, failureCode.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                failureCode.Connection = Connection;
                failureCode.Transaction = Transaction;

                if (!_validator.IsValid(failureCode))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (failureCode.IsNew)
                {
                    failureCode.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} {2} {3}", AuditLogConstants.AddedNew, failureCode.Code, failureCode.Category, failureCode.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = failureCode.EntityName(),
                        EntityId = failureCode.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (failureCode.HasChanges())
                {
                    foreach (var change in failureCode.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = failureCode.EntityName(),
                                EntityId = failureCode.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                    failureCode.Save();
                }

                CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[failureCode.TenantId].UpdateCache(failureCode);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<FailureCode> e)
        {
            _validator.Messages.Clear();

            var failureCode = e.Argument;

            if (failureCode.IsNew) return;

            var @lock = failureCode.ObtainLock(_view.ActiveUser, failureCode.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                failureCode.Connection = Connection;
                failureCode.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.CanDeleteFailureCode(failureCode))
                {
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, failureCode.Code, failureCode.Description),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = failureCode.EntityName(),
                        EntityId = failureCode.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                failureCode.Delete();
                @lock.Delete();

                CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[failureCode.TenantId].UpdateCache(failureCode, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.FailureCodeCategories = ProcessorUtilities.GetAll<FailureCodeCategory>();
        }

        private void OnUnLock(object sender, ViewEventArgs<FailureCode> e)
        {
            var failureCode = e.Argument;
            var @lock = failureCode.RetrieveLock(_view.ActiveUser, failureCode.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<FailureCode> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FaildedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

    }
}
