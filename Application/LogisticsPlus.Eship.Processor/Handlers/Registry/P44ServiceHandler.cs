﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
    public class P44ServiceHandler : EntityBase
	{
		private readonly IP44ServiceView _view;

		private readonly P44ServiceValidator _validator = new P44ServiceValidator();

		public P44ServiceHandler(IP44ServiceView view)
		{
			_view = view;
		}

		public void Initialize()
		{
            _view.Loading += OnLoading;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.BatchImport += OnBatchImport;
        }

		private void OnBatchImport(object sender, ViewEventArgs<List<P44ServiceMapping>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var p44ServiceMapping in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

                    p44ServiceMapping.Connection = Connection;
                    p44ServiceMapping.Transaction = Transaction;

					if (!_validator.IsValid(p44ServiceMapping))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

                    p44ServiceMapping.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = $"{AuditLogConstants.AddedNew} {p44ServiceMapping.Service.Code} {p44ServiceMapping.Project44Code}",
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = p44ServiceMapping.EntityName(),
						EntityId = p44ServiceMapping.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[p44ServiceMapping.TenantId].UpdateCache(p44ServiceMapping);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, p44ServiceMapping.Project44Code.GetString(), ex.Message));
				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<P44ServiceMapping> e)
		{
			var p44ServiceMapping = e.Argument;
			var @lock = p44ServiceMapping.RetrieveLock(_view.ActiveUser, p44ServiceMapping.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<P44ServiceMapping> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FaildedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			var results = new P44ServiceMappingSearch().FetchP44ServiceMappings(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<P44ServiceMapping> e)
		{
			_validator.Messages.Clear();

			var p44ServiceMapping = e.Argument;

			if (p44ServiceMapping.IsNew) return;

			var @lock = p44ServiceMapping.ObtainLock(_view.ActiveUser, p44ServiceMapping.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
                p44ServiceMapping.Connection = Connection;
                p44ServiceMapping.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;
				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =$"{AuditLogConstants.Delete} {p44ServiceMapping.Project44Code.GetString()} {p44ServiceMapping.Service.Code}",
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = p44ServiceMapping.EntityName(),
						EntityId = p44ServiceMapping.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

                p44ServiceMapping.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[p44ServiceMapping.TenantId].UpdateCache(p44ServiceMapping, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<P44ServiceMapping> e)
		{
			var p44ServiceMapping = e.Argument;

			if (!p44ServiceMapping.IsNew)
			{
				var @lock = p44ServiceMapping.RetrieveLock(_view.ActiveUser, p44ServiceMapping.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
            if (!_validator.IsValid(p44ServiceMapping))
            {
                RollBackTransaction();
                _view.DisplayMessages(_validator.Messages);
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
                p44ServiceMapping.Connection = Connection;
                p44ServiceMapping.Transaction = Transaction;

				if (p44ServiceMapping.IsNew)
				{
                    p44ServiceMapping.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = $"{AuditLogConstants.AddedNew} {p44ServiceMapping.Project44Code.GetString()} {p44ServiceMapping.Service.Code}",
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = p44ServiceMapping.EntityName(),
							EntityId = p44ServiceMapping.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (p44ServiceMapping.HasChanges())
				{
					foreach (var change in p44ServiceMapping.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = p44ServiceMapping.EntityName(),
								EntityId = p44ServiceMapping.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
                    p44ServiceMapping.Save();
				}


				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[p44ServiceMapping.TenantId].UpdateCache(p44ServiceMapping);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnLoading(object sender, EventArgs e)
        {
            _view.Project44Codes = ProcessorUtilities.GetAll<Project44ServiceCode>();
            _view.ServiceCodes = new ServiceSearch().FetchAllServices(_view.ActiveUser.TenantId.ToLong());
        }
	}
}
