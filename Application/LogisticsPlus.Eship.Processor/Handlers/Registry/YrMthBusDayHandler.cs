﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
    public class YrMthBusDayHandler : EntityBase
    {
        private readonly IYrMthBusDayView _view;
        private readonly YrMthBusDayValidator _validator = new YrMthBusDayValidator();

        public YrMthBusDayHandler(IYrMthBusDayView view)
		{
			_view = view;
		}

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.Search += OnSearch;
        }

        private void OnSave(object sender, ViewEventArgs<YrMthBusDay> e)
        {
            var yrMthBusDay = e.Argument;

            if (!yrMthBusDay.IsNew)
            {
                var @lock = yrMthBusDay.RetrieveLock(_view.ActiveUser, yrMthBusDay.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                yrMthBusDay.Connection = Connection;
                yrMthBusDay.Transaction = Transaction;

                if (!_validator.IsValid(yrMthBusDay))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (yrMthBusDay.IsNew)
                {
                    yrMthBusDay.Save();
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, yrMthBusDay.Year),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = yrMthBusDay.EntityName(),
                            EntityId = yrMthBusDay.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (yrMthBusDay.HasChanges())
                {
                    foreach (var change in yrMthBusDay.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = yrMthBusDay.EntityName(),
                                EntityId = yrMthBusDay.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                    yrMthBusDay.Save();
                }

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<YrMthBusDay> e)
        {
            var yrMthBusDay = e.Argument;

            if (yrMthBusDay.IsNew) return;

            var @lock = yrMthBusDay.ObtainLock(_view.ActiveUser, yrMthBusDay.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                yrMthBusDay.Connection = Connection;
                yrMthBusDay.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, yrMthBusDay.Year, yrMthBusDay.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = yrMthBusDay.EntityName(),
                        EntityId = yrMthBusDay.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                yrMthBusDay.Delete();
                @lock.Delete();

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnLock(object sender, ViewEventArgs<YrMthBusDay> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnUnLock(object sender, ViewEventArgs<YrMthBusDay> e)
        {
            var chargeCode = e.Argument;
            var @lock = chargeCode.RetrieveLock(_view.ActiveUser, chargeCode.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnSearch(object sender, ViewEventArgs<string> e)
        {
            var results = new YrMthBusDaySearch().FetchYrMthBusDays(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }
    }
}