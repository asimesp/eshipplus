﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class ShipmentPriorityHandler : EntityBase
	{
		private readonly IShipmentPriorityView _view;

		private readonly ShipmentPriorityValidator _validator = new ShipmentPriorityValidator();

		public ShipmentPriorityHandler(IShipmentPriorityView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<ShipmentPriority>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var shipmentPriority in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					shipmentPriority.Connection = Connection;
					shipmentPriority.Transaction = Transaction;

					if (!_validator.IsValid(shipmentPriority))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					shipmentPriority.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1}", AuditLogConstants.AddedNew, shipmentPriority.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipmentPriority.EntityName(),
						EntityId = shipmentPriority.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[shipmentPriority.TenantId].UpdateCache(shipmentPriority);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] {2}", ProcessorVars.RecordSaveErrMsg, shipmentPriority.Code, ex.Message));

				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<ShipmentPriority> e)
		{
			var shipmentPriority = e.Argument;
			var @lock = shipmentPriority.RetrieveLock(_view.ActiveUser, shipmentPriority.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<ShipmentPriority> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<string> e)
		{
			_view.DisplaySearchResult(new ShipmentPrioritySearch().FetchShipmentPriorities(e.Argument, _view.ActiveUser.TenantId));
		}

		private void OnDelete(object sender, ViewEventArgs<ShipmentPriority> e)
		{
			_validator.Messages.Clear();

			var shipmentPriority = e.Argument;

			if (shipmentPriority.IsNew) return;

			var @lock = shipmentPriority.ObtainLock(_view.ActiveUser, shipmentPriority.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				shipmentPriority.Connection = Connection;
				shipmentPriority.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteShipmentPriority(shipmentPriority))
				{
					RollBackTransaction();
                    @lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, shipmentPriority.Code, shipmentPriority.Description),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipmentPriority.EntityName(),
						EntityId = shipmentPriority.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				shipmentPriority.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[shipmentPriority.TenantId].UpdateCache(shipmentPriority, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<ShipmentPriority> e)
		{
			var shipmentPriority = e.Argument;

			if (!shipmentPriority.IsNew)
			{
				var @lock = shipmentPriority.RetrieveLock(_view.ActiveUser, shipmentPriority.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				shipmentPriority.Connection = Connection;
				shipmentPriority.Transaction = Transaction;

				if (shipmentPriority.Default)
				{
					var message = CheckForceDefault(shipmentPriority);
					if (message != null)
					{
						RollBackTransaction();
						_view.DisplayMessages(new[] { message });
						return;
					}
				}

				if (!_validator.IsValid(shipmentPriority))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (shipmentPriority.IsNew)
				{
					shipmentPriority.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, shipmentPriority.Code, shipmentPriority.Description),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipmentPriority.EntityName(),
							EntityId = shipmentPriority.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (shipmentPriority.HasChanges())
				{
					foreach (var change in shipmentPriority.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = shipmentPriority.EntityName(),
								EntityId = shipmentPriority.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					shipmentPriority.Save();
				}


				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[shipmentPriority.TenantId].UpdateCache(shipmentPriority);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private ValidationMessage CheckForceDefault(ShipmentPriority shipmentPriority)
		{
			var currentPrimary = new ShipmentPrioritySearch().CurrentDefaultShipmentPriority(shipmentPriority.TenantId);
			if (currentPrimary != null && currentPrimary.Id != shipmentPriority.Id)
			{
				if (_view.ForceDefault)
				{
					var @lock = currentPrimary.ObtainLock(_view.ActiveUser, currentPrimary.Id);
					if (!@lock.IsUserLock(_view.ActiveUser))
						return ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg);

					currentPrimary.Connection = Connection;
					currentPrimary.Transaction = Transaction;
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					currentPrimary.TakeSnapShot();
					currentPrimary.Default = false;
					foreach (var change in currentPrimary.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = currentPrimary.EntityName(),
								EntityId = currentPrimary.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					currentPrimary.Save();
					@lock.Delete();
				}
				else
				{
					shipmentPriority.Default = false;
				}
			}
			return null;
		}
	}
}
