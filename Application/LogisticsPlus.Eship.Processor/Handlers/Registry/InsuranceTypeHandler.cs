﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class InsuranceTypeHandler : EntityBase
	{
		private readonly IInsuranceTypeView _view;

		private readonly InsuranceTypeValidator _validator = new InsuranceTypeValidator();

		public InsuranceTypeHandler(IInsuranceTypeView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<InsuranceType>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var insuranceType in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					insuranceType.Connection = Connection;
					insuranceType.Transaction = Transaction;

					if (!_validator.IsValid(insuranceType))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					insuranceType.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1}", AuditLogConstants.AddedNew, insuranceType.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = insuranceType.EntityName(),
						EntityId = insuranceType.IsNew.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[insuranceType.TenantId].UpdateCache(insuranceType);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, insuranceType.Code, ex.Message));

				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<InsuranceType> e)
		{
			var insuranceType = e.Argument;
			var @lock = insuranceType.RetrieveLock(_view.ActiveUser, insuranceType.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<InsuranceType> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<string> e)
		{
			var results = new InsuranceTypeSearch().FetchInsuranceTypes(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<InsuranceType> e)
		{
			_validator.Messages.Clear();

			var insuranceType = e.Argument;

			if (insuranceType.IsNew) return;

			var @lock = insuranceType.ObtainLock(_view.ActiveUser, insuranceType.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				insuranceType.Connection = Connection;
				insuranceType.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteInsuranceType(insuranceType))
				{
					RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, insuranceType.Code, insuranceType.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = insuranceType.EntityName(),
						EntityId = insuranceType.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				insuranceType.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[insuranceType.TenantId].UpdateCache(insuranceType, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<InsuranceType> e)
		{
			var insuranceType = e.Argument;

			if (!insuranceType.IsNew)
			{
				var @lock = insuranceType.RetrieveLock(_view.ActiveUser, insuranceType.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				insuranceType.Connection = Connection;
				insuranceType.Transaction = Transaction;

				if (!_validator.IsValid(insuranceType))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}


				if (insuranceType.IsNew)
				{
					insuranceType.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, insuranceType.Code),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = insuranceType.EntityName(),
							EntityId = insuranceType.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (insuranceType.HasChanges())
				{
					foreach (var change in insuranceType.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = insuranceType.EntityName(),
								EntityId = insuranceType.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					insuranceType.Save();
				}

				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[insuranceType.TenantId].UpdateCache(insuranceType);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}
	}
}
