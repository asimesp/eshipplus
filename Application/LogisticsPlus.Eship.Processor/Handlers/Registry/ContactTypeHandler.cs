﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class ContactTypeHandler : EntityBase
	{
		private readonly IContactTypeView _view;

		private readonly ContactTypeValidator _validator = new ContactTypeValidator();

		public ContactTypeHandler(IContactTypeView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<ContactType>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var contactType in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					contactType.Connection = Connection;
					contactType.Transaction = Transaction;

					if (!_validator.IsValid(contactType))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					contactType.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1}", AuditLogConstants.AddedNew, contactType.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = contactType.EntityName(),
						EntityId = contactType.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[contactType.TenantId].UpdateCache(contactType);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, contactType.Code, ex.Message));

				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<ContactType> e)
		{
			var contactType = e.Argument;
			var @lock = contactType.RetrieveLock(_view.ActiveUser, contactType.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<ContactType> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<string> e)
		{
			List<ContactType> results = new ContactTypeSearch().FetchContactTypes(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<ContactType> e)
		{
			_validator.Messages.Clear();

			var contactType = e.Argument;

			if (contactType.IsNew) return;

			var @lock = contactType.ObtainLock(_view.ActiveUser, contactType.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				contactType.Connection = Connection;
				contactType.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteContactType(contactType))
				{
					RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, contactType.Code, contactType.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = contactType.EntityName(),
						EntityId = contactType.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				contactType.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[contactType.TenantId].UpdateCache(contactType, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<ContactType> e)
		{
			var contactType = e.Argument;

			if (!contactType.IsNew)
			{
				var @lock = contactType.RetrieveLock(_view.ActiveUser, contactType.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				contactType.Connection = Connection;
				contactType.Transaction = Transaction;

				if (!_validator.IsValid(contactType))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (contactType.IsNew)
				{
					contactType.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, contactType.Code),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = contactType.EntityName(),
							EntityId = contactType.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (contactType.HasChanges())
				{
					foreach (var change in contactType.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = contactType.EntityName(),
								EntityId = contactType.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					contactType.Save();
				}

				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[contactType.TenantId].UpdateCache(contactType);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}
	}
}
