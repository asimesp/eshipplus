﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
    public class AccountBucketHandler : EntityBase
    {
        private readonly IAccountBucketView _view;

        private readonly AccountBucketValidator _validator = new AccountBucketValidator();

        public AccountBucketHandler(IAccountBucketView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.BatchImport += OnBatchImport;
        }

        
        private void OnBatchImport(object sender, ViewEventArgs<List<AccountBucket>> e)
        {
            var messages = new ValidationMessageCollection();

            foreach (var accountBucket in e.Argument)
            {
                _validator.Messages.Clear();
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    accountBucket.Connection = Connection;
                    accountBucket.Transaction = Transaction;

                    if (!_validator.IsValid(accountBucket))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }

                    accountBucket.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1}", AuditLogConstants.AddedNew, accountBucket.Code),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = accountBucket.EntityName(),
                        EntityId = accountBucket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    CommitTransaction();

                    // cache item
                    ProcessorVars.RegistryCache[accountBucket.TenantId].UpdateCache(accountBucket);
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, accountBucket.Code, ex.Message));

                }


            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }

        private void OnUnLock(object sender, ViewEventArgs<AccountBucket> e)
        {
            var accountBucket = e.Argument;
            var @lock = accountBucket.RetrieveLock(_view.ActiveUser, accountBucket.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<AccountBucket> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var results = new AccountBucketSearch().FetchAccountBuckets(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }

        private void OnDelete(object sender, ViewEventArgs<AccountBucket> e)
        {
            _validator.Messages.Clear();

            var accountBucket = e.Argument;

            if (accountBucket.IsNew) return;

            var @lock = accountBucket.ObtainLock(_view.ActiveUser, accountBucket.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                accountBucket.Connection = Connection;
                accountBucket.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.CanDeleteAccountBucket(accountBucket))
                {
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, accountBucket.Code, accountBucket.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = accountBucket.EntityName(),
                        EntityId = accountBucket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                foreach (var accountBucketUnit in accountBucket.AccountBucketUnits)
                {
                    if (!_validator.CanDeleteAccountBucketUnit(accountBucketUnit))
                    {
                        RollBackTransaction();
                        @lock.Delete();
                        _view.DisplayMessages(_validator.Messages);
                        return;
                    }

                    accountBucketUnit.Connection = Connection;
                    accountBucketUnit.Transaction = Transaction;

                    accountBucketUnit.Delete();

                    // drop from cache
                    ProcessorVars.RegistryCache[accountBucket.TenantId].UpdateCache(accountBucketUnit, true);
                }

                accountBucket.Delete();
                @lock.Delete();

                CommitTransaction();

                // drop from cache
                ProcessorVars.RegistryCache[accountBucket.TenantId].UpdateCache(accountBucket, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }


        }

        private void OnSave(object sender, ViewEventArgs<AccountBucket> e)
        {
            var accountBucket = e.Argument;

            if (!accountBucket.IsNew)
            {
                var @lock = accountBucket.RetrieveLock(_view.ActiveUser, accountBucket.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            var dbAccountBucket = new AccountBucket(accountBucket.Id, false);
            dbAccountBucket.LoadAccountBucketUnits();

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                accountBucket.Connection = Connection;
                accountBucket.Transaction = Transaction;

                if (!_validator.IsValid(accountBucket))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }
                
                if (accountBucket.IsNew)
                {
                    accountBucket.Save();
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, accountBucket.Code),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = accountBucket.EntityName(),
                            EntityId = accountBucket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (accountBucket.HasChanges())
                {
                    foreach (var change in accountBucket.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = accountBucket.EntityName(),
                                EntityId = accountBucket.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                    accountBucket.Save();
                }

                // check if we can delete account bucket units that have been removed
                if(!ProcessAccountBucketUnits(accountBucket, dbAccountBucket.AccountBucketUnits))
                {
                    RollBackTransaction();
                    return;
                }

                CommitTransaction();

                // cache item
                ProcessorVars.RegistryCache[accountBucket.TenantId].UpdateCache(accountBucket);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }


        }


        private bool ProcessAccountBucketUnits(AccountBucket accountBucket, IEnumerable<AccountBucketUnit> dbAccountBucketUnits)
        {
            var dbDelete = dbAccountBucketUnits.Where(dbs => !accountBucket.AccountBucketUnits.Select(s => s.Id).Contains(dbs.Id));

            var valid = true;

            foreach (var a in dbDelete)
            {
                if (!_validator.CanDeleteAccountBucketUnit(a))
                {
                    valid =  false;
                    continue;
                }

                a.Connection = Connection;
                a.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Account Bucket:{2} Account Bucket Unit Ref#: {3}", AuditLogConstants.Delete, a.EntityName(),
                                      accountBucket.Code, a.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = accountBucket.EntityName(),
                    EntityId = accountBucket.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                a.Delete();

                // drop from cache
                ProcessorVars.RegistryCache[accountBucket.TenantId].UpdateCache(a, true);
            }

            if (!valid)
            {
                _view.DisplayMessages(_validator.Messages);
                return false;
            }
          
            foreach (var a in accountBucket.AccountBucketUnits)
            {
                a.Connection = Connection;
                a.Transaction = Transaction;

                if (a.IsNew)
                {
                    a.Save();
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Account Bucket:{2} Account Bucket Unit Ref#:{3}",
                                                        AuditLogConstants.AddedNew, a.EntityName(), accountBucket.Code,
                                                        a.Id),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = accountBucket.EntityName(),
                            EntityId = accountBucket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // cache item
                    ProcessorVars.RegistryCache[a.TenantId].UpdateCache(a);
                }
                else if (a.HasChanges())
                {
                    foreach (var change in a.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description =
                                    string.Format("{0} Account Bucket Unit Ref#:{1} {2}", a.EntityName(), a.Id, change),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = accountBucket.EntityName(),
                                EntityId = accountBucket.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                    a.Save();

                    // cache item
                    ProcessorVars.RegistryCache[a.TenantId].UpdateCache(a);
                }

                
            }

            return true;
        }
    }
}
