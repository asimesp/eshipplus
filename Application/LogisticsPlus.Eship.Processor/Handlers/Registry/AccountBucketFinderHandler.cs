﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class AccountBucketFinderHandler
	{
		private readonly IAccountBucketFinderView _view;

		public AccountBucketFinderHandler(IAccountBucketFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			var results = new AccountBucketSearch().FetchAccountBuckets(e.Argument, _view.ActiveUser.TenantId);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}
	}
}
