﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class DocumentPrefixMapHandler : EntityBase
	{
		private readonly IDocumentPrefixMapView _view;

		private readonly DocumentPrefixMapValidator _validator = new DocumentPrefixMapValidator();

		public DocumentPrefixMapHandler(IDocumentPrefixMapView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
		}

		private void OnUnLock(object sender, ViewEventArgs<DocumentPrefixMap> e)
		{
			var documentPrefixMap = e.Argument;
			var @lock = documentPrefixMap.RetrieveLock(_view.ActiveUser, documentPrefixMap.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<DocumentPrefixMap> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<DocumentPrefixMapViewSearchCriteria> e)
		{
			var results = new DocumentPrefixMapSearch().FetchDocumentPrefixMaps(e.Argument.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<DocumentPrefixMap> e)
		{
			_validator.Messages.Clear();

			var documentPrefixMap = e.Argument;

			if (documentPrefixMap.IsNew) return;

			var @lock = documentPrefixMap.ObtainLock(_view.ActiveUser, documentPrefixMap.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				documentPrefixMap.Connection = Connection;
				documentPrefixMap.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, documentPrefixMap.PrefixId, documentPrefixMap.DocumentTemplateId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = documentPrefixMap.EntityName(),
						EntityId = documentPrefixMap.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				documentPrefixMap.Delete();
				@lock.Delete();

				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<DocumentPrefixMap> e)
		{
			var documentPrefixMap = e.Argument;

			if (!documentPrefixMap.IsNew)
			{
				var @lock = documentPrefixMap.RetrieveLock(_view.ActiveUser, documentPrefixMap.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				documentPrefixMap.Connection = Connection;
				documentPrefixMap.Transaction = Transaction;

				if (!_validator.IsValid(documentPrefixMap))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (documentPrefixMap.IsNew)
				{
					documentPrefixMap.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, documentPrefixMap.PrefixId, documentPrefixMap.DocumentTemplateId),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = documentPrefixMap.EntityName(),
							EntityId = documentPrefixMap.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (documentPrefixMap.HasChanges())
				{
					foreach (var change in documentPrefixMap.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = documentPrefixMap.EntityName(),
								EntityId = documentPrefixMap.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					documentPrefixMap.Save();
				}

				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnLoading(object sender, EventArgs e)
		{
            _view.DocumentTemplates = new DocumentTemplateSearch().FetchDocumentTemplates(SearchUtilities.WildCard, _view.ActiveUser.TenantId);
		}
	}
}
