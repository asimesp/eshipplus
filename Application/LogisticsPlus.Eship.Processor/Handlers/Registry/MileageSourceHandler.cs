﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class MileageSourceHandler : EntityBase
	{
		private readonly IMileageSourceView _view;

		private readonly MileageSourceValidator _validator = new MileageSourceValidator();

		public MileageSourceHandler(IMileageSourceView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.BatchImport += OnBatchImport;
			_view.Loading += OnLoading;
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.MileageEngines = ProcessorUtilities.GetAll<MileageEngine>();
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<MileageSource>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var mileageSource in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					mileageSource.Connection = Connection;
					mileageSource.Transaction = Transaction;

					if (!_validator.IsValid(mileageSource))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					mileageSource.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1}", AuditLogConstants.AddedNew, mileageSource.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = mileageSource.EntityName(),
						EntityId = mileageSource.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[mileageSource.TenantId].UpdateCache(mileageSource);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg,  mileageSource.Code, ex.Message));

				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<MileageSource> e)
		{
			var mileageSource = e.Argument;
			var @lock = mileageSource.RetrieveLock(_view.ActiveUser, mileageSource.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<MileageSource> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<string> e)
		{
			List<MileageSource> results = new MileageSourceSearch().FetchMileageSources(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<MileageSource> e)
		{
			_validator.Messages.Clear();

			var mileageSource = e.Argument;

			if (mileageSource.IsNew) return;

			var @lock = mileageSource.ObtainLock(_view.ActiveUser, mileageSource.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				mileageSource.Connection = Connection;
				mileageSource.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteMileageSource(mileageSource))
				{
					RollBackTransaction();
                    @lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, mileageSource.Code, mileageSource.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = mileageSource.EntityName(),
						EntityId = mileageSource.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				mileageSource.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[mileageSource.TenantId].UpdateCache(mileageSource, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<MileageSource> e)
		{
			var mileageSource = e.Argument;

			if (!mileageSource.IsNew)
			{
				var @lock = mileageSource.RetrieveLock(_view.ActiveUser, mileageSource.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				mileageSource.Connection = Connection;
				mileageSource.Transaction = Transaction;

				if (mileageSource.Primary)
				{
					var message = CheckForcePrimary(mileageSource);
					if (message != null)
					{
						RollBackTransaction();
						_view.DisplayMessages(new[] { message });
						return;
					}
				}

				if (!_validator.IsValid(mileageSource))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (mileageSource.IsNew)
				{
					mileageSource.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, mileageSource.Code),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = mileageSource.EntityName(),
							EntityId = mileageSource.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (mileageSource.HasChanges())
				{
					foreach (var change in mileageSource.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = mileageSource.EntityName(),
								EntityId = mileageSource.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					mileageSource.Save();
				}

				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[mileageSource.TenantId].UpdateCache(mileageSource);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private ValidationMessage CheckForcePrimary(MileageSource mileageSource)
		{
			var currentPrimary = new MileageSourceSearch().CurrentPrimaryMileageSource(mileageSource.TenantId);
			if (currentPrimary != null && currentPrimary.Id != mileageSource.Id)
			{
				if (_view.ForcePrimary)
				{
					var @lock = currentPrimary.ObtainLock(_view.ActiveUser, currentPrimary.Id);
					if (!@lock.IsUserLock(_view.ActiveUser))
						return ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg);

					currentPrimary.Connection = Connection;
					currentPrimary.Transaction = Transaction;
					@lock.Connection = Connection;
					@lock.Transaction = Transaction;

					currentPrimary.TakeSnapShot();
					currentPrimary.Primary = false;
					foreach (var change in currentPrimary.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = currentPrimary.EntityName(),
								EntityId = currentPrimary.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					currentPrimary.Save();
					@lock.Delete();
				}
				else
				{
					mileageSource.Primary = false;
				}
			}
			return null;
		}
	}
}
