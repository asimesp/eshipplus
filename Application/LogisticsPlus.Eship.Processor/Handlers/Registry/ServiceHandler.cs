﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class ServiceHandler : EntityBase
	{
		private readonly IServiceView _view;

		private readonly ServiceValidator _validator = new ServiceValidator();

		public ServiceHandler(IServiceView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<Service>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var service in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					service.Connection = Connection;
					service.Transaction = Transaction;

					if (!_validator.IsValid(service))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					service.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1}", AuditLogConstants.AddedNew, service.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = service.EntityName(),
						EntityId = service.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[service.TenantId].UpdateCache(service);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, service.Code, ex.Message));
				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<Service> e)
		{
			var service = e.Argument;
			var @lock = service.RetrieveLock(_view.ActiveUser, service.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<Service> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FaildedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			var results = new ServiceSearch().FetchServices(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<Service> e)
		{
			_validator.Messages.Clear();

			var service = e.Argument;

			if (service.IsNew) return;

			var @lock = service.ObtainLock(_view.ActiveUser, service.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				service.Connection = Connection;
				service.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteService(service))
				{
					RollBackTransaction();
                    @lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, service.Code, service.ChargeCodeId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = service.EntityName(),
						EntityId = service.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				service.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[service.TenantId].UpdateCache(service, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<Service> e)
		{
			var service = e.Argument;

			if (!service.IsNew)
			{
				var @lock = service.RetrieveLock(_view.ActiveUser, service.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				service.Connection = Connection;
				service.Transaction = Transaction;

				if (!_validator.IsValid(service))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (service.IsNew)
				{
					service.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, service.Code, service.ChargeCodeId),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = service.EntityName(),
							EntityId = service.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (service.HasChanges())
				{
					foreach (var change in service.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = service.EntityName(),
								EntityId = service.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					service.Save();
				}


				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[service.TenantId].UpdateCache(service);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.Categories = ProcessorUtilities.GetAll<ServiceCategory>();
			_view.DispatchFlag = ProcessorUtilities.GetAll<ServiceDispatchFlag>();
			_view.Project44Codes = ProcessorUtilities.GetAll<Project44ServiceCode>();
		}
	}
}
