﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;
using P44SDK.V4.Model;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
    public class PackageTypeHandler : EntityBase
    {
        private readonly IPackageTypeView _view;

        private readonly PackageTypeValidator _validator = new PackageTypeValidator();

        public PackageTypeHandler(IPackageTypeView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.BatchImport += OnBatchImport;
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.Project44Codes = ProcessorUtilities.GetAll<LineItem.PackageTypeEnum>();
        }

        private void OnBatchImport(object sender, ViewEventArgs<List<PackageType>> e)
        {
            var messages = new ValidationMessageCollection();

            foreach (var packageType in e.Argument)
            {
                _validator.Messages.Clear();
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    packageType.Connection = Connection;
                    packageType.Transaction = Transaction;

                    if (!_validator.IsValid(packageType))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }

                    packageType.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1}", AuditLogConstants.AddedNew, packageType.TypeName),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = packageType.EntityName(),
                        EntityId = packageType.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[packageType.TenantId].UpdateCache(packageType);
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, packageType.TypeName, ex.Message));

                }
            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }

        private void OnUnLock(object sender, ViewEventArgs<PackageType> e)
        {
            var packageType = e.Argument;
            var @lock = packageType.RetrieveLock(_view.ActiveUser, packageType.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<PackageType> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnSearch(object sender, ViewEventArgs<string> e)
        {
            _view.DisplaySearchResult(new PackageTypeSearch().FetchPackageTypes(e.Argument, _view.ActiveUser.TenantId));
        }

        private void OnDelete(object sender, ViewEventArgs<PackageType> e)
        {
            _validator.Messages.Clear();

            var packageType = e.Argument;

            if (packageType.IsNew) return;

            var @lock = packageType.ObtainLock(_view.ActiveUser, packageType.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                packageType.Connection = Connection;
                packageType.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.CanDeletePackageType(packageType))
                {
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1}", AuditLogConstants.Delete, packageType.TypeName),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = packageType.EntityName(),
                        EntityId = packageType.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                packageType.Delete();
                @lock.Delete();

                CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[packageType.TenantId].UpdateCache(packageType, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSave(object sender, ViewEventArgs<PackageType> e)
        {
            var packageType = e.Argument;

            if (!packageType.IsNew)
            {
                var @lock = packageType.RetrieveLock(_view.ActiveUser, packageType.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                packageType.Connection = Connection;
                packageType.Transaction = Transaction;

                if (!_validator.IsValid(packageType) || _validator.HasWarnings)
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (packageType.IsNew)
                {
                    packageType.Save();
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, packageType.TypeName),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = packageType.EntityName(),
                            EntityId = packageType.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (packageType.HasChanges())
                {
                    foreach (var change in packageType.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = packageType.EntityName(),
                                EntityId = packageType.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                    packageType.Save();
                }

                CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[packageType.TenantId].UpdateCache(packageType);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }
    }
}
