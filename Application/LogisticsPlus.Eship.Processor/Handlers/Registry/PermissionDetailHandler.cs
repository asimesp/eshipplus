﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class PermissionDetailHandler : EntityBase
	{
		private readonly IPermissionDetailView _view;

		private readonly PermissionDetailValidator _validator = new PermissionDetailValidator();

		public PermissionDetailHandler(IPermissionDetailView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<PermissionDetail>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var detail in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					detail.Connection = Connection;
					detail.Transaction = Transaction;

					if (!_validator.IsValid(detail))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					detail.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1}", AuditLogConstants.AddedNew, detail.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = detail.EntityName(),
						EntityId = detail.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[detail.TenantId].UpdateCache(detail);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, detail.Code, ex.Message));

				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<PermissionDetail> e)
		{
			var chargeCode = e.Argument;
			var @lock = chargeCode.RetrieveLock(_view.ActiveUser, chargeCode.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<PermissionDetail> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<string> e)
		{
			var results = new PermissionDetailSearch().FetchPermissionDetails(_view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<PermissionDetail> e)
		{
			_validator.Messages.Clear();

			var detail = e.Argument;

			if (detail.IsNew) return;

			var @lock = detail.ObtainLock(_view.ActiveUser, detail.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				detail.Connection = Connection;
				detail.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, detail.Code, detail.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = detail.EntityName(),
						EntityId = detail.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				detail.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[detail.TenantId].UpdateCache(detail, true);

				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<PermissionDetail> e)
		{
			var detail = e.Argument;

			if (!detail.IsNew)
			{
				var @lock = detail.RetrieveLock(_view.ActiveUser, detail.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				detail.Connection = Connection;
				detail.Transaction = Transaction;

				if (!_validator.IsValid(detail))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (detail.IsNew)
				{
					detail.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, detail.Code),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = detail.EntityName(),
							EntityId = detail.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (detail.HasChanges())
				{
					foreach (var change in detail.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = detail.EntityName(),
								EntityId = detail.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					detail.Save();
				}

				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[detail.TenantId].UpdateCache(detail);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.Categories = ProcessorUtilities.GetAll<PermissionCategory>();
			_view.Types = ProcessorUtilities.GetAll<PermissionType>();
		}
	}
}
