﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class SettingHandler : EntityBase
	{
		private readonly ISettingView _view;

		private readonly SettingValidator _validator = new SettingValidator();

		public SettingHandler(ISettingView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
		}

	    private void OnUnLock(object sender, ViewEventArgs<Setting> e)
		{
			var setting = e.Argument;
			var @lock = setting.RetrieveLock(_view.ActiveUser, setting.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<Setting> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<string> e)
		{
			var results = new SettingSearch().FetchSettings(e.Argument, _view.ActiveUser.TenantId);

			var enumCount = Enum.GetValues(typeof(SettingCode)).Length;

			if (results.Count != enumCount)
			{
				var newSettings = ProcessorUtilities.GetAll<SettingCode>(false)
					.Where(i => !results.Select(r => r.Code.ToString()).Contains(i.Value))
					.Select(i => i.Key.ToEnum<SettingCode>())
					.Select(s => new Setting
					             	{
					             		Code = s,
										TenantId = _view.ActiveUser.TenantId,
					             		Value = string.Empty
					             	})
					.ToList();

				foreach(var settings in newSettings)
				{
					settings.Save();
					results.Add(settings);
				}

				results.OrderBy(s => s.Code);
			}

			_view.DisplaySearchResult(results);
		}

		private void OnSave(object sender, ViewEventArgs<Setting> e)
		{
			var setting = e.Argument;

			if (!setting.IsNew)
			{
				var @lock = setting.RetrieveLock(_view.ActiveUser, setting.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				setting.Connection = Connection;
				setting.Transaction = Transaction;

				if (!_validator.IsValid(setting))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (setting.IsNew)
				{
					setting.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, setting.Code),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = setting.EntityName(),
							EntityId = 0.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (setting.HasChanges())
				{
					foreach (var change in setting.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = setting.EntityName(),
								EntityId = setting.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					setting.Save();
				}


				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.Codes = ProcessorUtilities.GetAll<SettingCode>();
		}
	}
}
