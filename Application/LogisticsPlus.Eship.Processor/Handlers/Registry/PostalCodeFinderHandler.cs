﻿using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class PostalCodeFinderHandler : EntityBase
	{
		private readonly IPostalCodeFinderView _view;

		public PostalCodeFinderHandler(IPostalCodeFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<PostalCodeViewSearchCriteria> e)
		{
            var results = new PostalCodeSearch().FetchPostalCodes(e.Argument);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

            _view.DisplaySearchResult(results);
		}

	}
}
