﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
    public class UserDepartmentHandler : EntityBase
    {
        private readonly IUserDepartmentView _view;

		private readonly UserDepartmentValidator _validator = new UserDepartmentValidator();

        public UserDepartmentHandler(IUserDepartmentView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
		    _view.BatchImport += OnBatchImport;
		}

        private void OnBatchImport(object sender, ViewEventArgs<List<UserDepartment>> e)
        {
            var messages = new ValidationMessageCollection();

            foreach (var userDepartment in e.Argument)
            {
                _validator.Messages.Clear();
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    userDepartment.Connection = Connection;
                    userDepartment.Transaction = Transaction;

                    if (!_validator.IsValid(userDepartment))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }

                    userDepartment.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1}", AuditLogConstants.AddedNew, userDepartment.Code),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = userDepartment.EntityName(),
                        EntityId = userDepartment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, userDepartment.Code, ex.Message));
                }
            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }

        private void OnUnLock(object sender, ViewEventArgs<UserDepartment> e)
		{
			var userDepartment = e.Argument;
			var @lock = userDepartment.RetrieveLock(_view.ActiveUser, userDepartment.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

        private void OnLock(object sender, ViewEventArgs<UserDepartment> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<string> e)
		{
            _view.DisplaySearchResult(new UserDepartmentSearch().FetchUserDepartments(e.Argument, _view.ActiveUser.TenantId));
		}

        private void OnDelete(object sender, ViewEventArgs<UserDepartment> e)
		{
			_validator.Messages.Clear();

			var userDepartment = e.Argument;

			if (userDepartment.IsNew) return;

			var @lock = userDepartment.ObtainLock(_view.ActiveUser, userDepartment.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				userDepartment.Connection = Connection;
				userDepartment.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteDepartment(userDepartment))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, userDepartment.Code, userDepartment.Description),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = userDepartment.EntityName(),
					EntityId = userDepartment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				userDepartment.Delete();
				@lock.Delete();

				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

        private void OnSave(object sender, ViewEventArgs<UserDepartment> e)
		{
			var userDepartment = e.Argument;

			if (!userDepartment.IsNew)
			{
				var @lock = userDepartment.RetrieveLock(_view.ActiveUser, userDepartment.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				userDepartment.Connection = Connection;
				userDepartment.Transaction = Transaction;

				if (!_validator.IsValid(userDepartment))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (userDepartment.IsNew)
				{
					userDepartment.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, userDepartment.Code, userDepartment.Description),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = userDepartment.EntityName(),
							EntityId = userDepartment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (userDepartment.HasChanges())
				{
					foreach (var change in userDepartment.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = userDepartment.EntityName(),
								EntityId = userDepartment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					userDepartment.Save();
				}


				CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}
    }
}
