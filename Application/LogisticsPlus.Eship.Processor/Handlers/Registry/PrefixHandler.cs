﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class PrefixHandler : EntityBase
	{
		private readonly IPrefixView _view;

		private readonly PrefixValidator _validator = new PrefixValidator();

		public PrefixHandler(IPrefixView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<Prefix>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var prefix in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					prefix.Connection = Connection;
					prefix.Transaction = Transaction;

					if (!_validator.IsValid(prefix))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					prefix.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1}", AuditLogConstants.AddedNew, prefix.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = prefix.EntityName(),
						EntityId = prefix.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[prefix.TenantId].UpdateCache(prefix);

				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, prefix.Code, ex.Message));
				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<Prefix> e)
		{
			var prefix = e.Argument;
			var @lock = prefix.RetrieveLock(_view.ActiveUser, prefix.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<Prefix> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			_view.DisplaySearchResult(new PrefixSearch().FetchPrefixes(e.Argument, _view.ActiveUser.TenantId));
		}

		private void OnDelete(object sender, ViewEventArgs<Prefix> e)
		{
			_validator.Messages.Clear();

			var prefix = e.Argument;

			if (prefix.IsNew) return;

			var @lock = prefix.ObtainLock(_view.ActiveUser, prefix.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				prefix.Connection = Connection;
				prefix.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeletePrefix(prefix))
				{
					RollBackTransaction();
                    @lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, prefix.Code, prefix.Description),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = prefix.EntityName(),
					EntityId = prefix.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				prefix.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[prefix.TenantId].UpdateCache(prefix, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) }); 
			}
		}

		private void OnSave(object sender, ViewEventArgs<Prefix> e)
		{
			var prefix = e.Argument;

			if (!prefix.IsNew)
			{
				var @lock = prefix.RetrieveLock(_view.ActiveUser, prefix.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				prefix.Connection = Connection;
				prefix.Transaction = Transaction;

				if (!_validator.IsValid(prefix) || _validator.HasWarnings)
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (prefix.IsNew)
				{
					prefix.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, prefix.Code, prefix.Description),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = prefix.EntityName(),
							EntityId = prefix.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (prefix.HasChanges())
				{
					foreach (var change in prefix.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = prefix.EntityName(),
								EntityId = prefix.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					prefix.Save();
				}

				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[prefix.TenantId].UpdateCache(prefix);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}
	}
}
