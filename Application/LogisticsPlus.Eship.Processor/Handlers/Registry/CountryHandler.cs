﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class CountryHandler : EntityBase
	{
		private readonly ICountryView _view;

		private readonly CountryValidator _validator = new CountryValidator();

		public CountryHandler(ICountryView view)
		{
			_view = view;
		}

		public void Initialize()
		{
		    _view.Loading += OnLoading;
            _view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
            _view.BatchImport += OnBatchImport;
		}

	    private void OnLoading(object sender, EventArgs e)
	    {
	        _view.Project44CountryCodes = ProcessorUtilities.GetAll<Project44CountryCode>();
	    }

        private void OnBatchImport(object sender, ViewEventArgs<List<Country>> e)
        {
            var messages = new ValidationMessageCollection();

            foreach (var country in e.Argument)
            {
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					country.Connection = Connection;
					country.Transaction = Transaction;

					_validator.Messages.Clear();
					if (!_validator.IsValid(country))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

                    country.Save();

					new AdminAuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, country.Name, country.Code),
							EntityCode = country.EntityName(),
							EntityId = country.Id.ToString(),
							LogDateTime = DateTime.Now,
							AdminUser = _view.ActiveSuperUser
						}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache.UpdateCache(country);
				}
				catch (Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, country.Code, ex.Message));
				}
            }

            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }

		private void OnSearch(object sender, ViewEventArgs<string> e)
		{
			var results = new CountrySearch().FetchCountries(e.Argument);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<Country> e)
		{
			var country = e.Argument;

			if (country.IsNew) return;

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				country.Connection = Connection;
				country.Transaction = Transaction;

				new AdminAuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} {2}", AuditLogConstants.Delete, country.Name, country.Code),
						EntityCode = country.EntityName(),
						EntityId = country.Id.ToString(),
						LogDateTime = DateTime.Now,
						AdminUser = _view.ActiveSuperUser
					}.Log();

				country.Delete();

				CommitTransaction();

				// drop country
				ProcessorVars.RegistryCache.UpdateCache(country, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<Country> e)
		{
			var country = e.Argument;
			
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				country.Connection = Connection;
				country.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.IsValid(country))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}
				
                if (country.IsNew)
                {
                    country.Save();

                	new AdminAuditLog
                		{
                			Connection = Connection,
                			Transaction = Transaction,
                			Description =
                				string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, country.Name, country.Code),
                			EntityCode = country.EntityName(),
                			EntityId = country.Id.ToString(),
                			LogDateTime = DateTime.Now,
                			AdminUser = _view.ActiveSuperUser
                		}.Log();
                }
                else if (country.HasChanges())
                {
					foreach (var change in country.Changes())
						new AdminAuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								EntityCode = country.EntityName(),
								EntityId = country.Id.ToString(),
								LogDateTime = DateTime.Now,
								AdminUser = _view.ActiveSuperUser
							}.Log();
                    country.Save();
                }

				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache.UpdateCache(country);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}
	}
}
