﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
    public class EquipmentTypeHandler : EntityBase
    {
        private readonly IEquipmentTypeView _view;

        private readonly EquipmentTypeValidator _validator = new EquipmentTypeValidator();

        public EquipmentTypeHandler(IEquipmentTypeView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.BatchImport += OnBatchImport;
        }

        private void OnBatchImport(object sender, ViewEventArgs<List<EquipmentType>> e)
        {
            var messages = new ValidationMessageCollection();

            foreach (var equipmentType in e.Argument)
            {
                _validator.Messages.Clear();
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    equipmentType.Connection = Connection;
                    equipmentType.Transaction = Transaction;

                    if (!_validator.IsValid(equipmentType))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }

                    equipmentType.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, equipmentType.Code, equipmentType.TypeName),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = equipmentType.EntityName(),
                        EntityId = equipmentType.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[equipmentType.TenantId].UpdateCache(equipmentType);
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, equipmentType.Code, ex.Message));

                }
            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }

        private void OnUnLock(object sender, ViewEventArgs<EquipmentType> e)
        {
            var equipmentType = e.Argument;
            var @lock = equipmentType.RetrieveLock(_view.ActiveUser, equipmentType.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<EquipmentType> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var results = new EquipmentTypeSearch().FetchEquipmentTypes(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }

        private void OnDelete(object sender, ViewEventArgs<EquipmentType> e)
        {
            _validator.Messages.Clear();

            var equipmentType = e.Argument;

            if (equipmentType.IsNew) return;

            var @lock = equipmentType.ObtainLock(_view.ActiveUser, equipmentType.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }


            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                equipmentType.Connection = Connection;
                equipmentType.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.CanDeleteEquipmentType(equipmentType))
                {
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} {2} {3}", AuditLogConstants.Delete, equipmentType.Code,  equipmentType.TypeName, equipmentType.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = equipmentType.EntityName(),
                        EntityId = equipmentType.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                equipmentType.Delete();
                @lock.Delete();

                CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[equipmentType.TenantId].UpdateCache(equipmentType, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSave(object sender, ViewEventArgs<EquipmentType> e)
        {
            var equipmentType = e.Argument;

            if (!equipmentType.IsNew)
            {
                var @lock = equipmentType.RetrieveLock(_view.ActiveUser, equipmentType.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                equipmentType.Connection = Connection;
                equipmentType.Transaction = Transaction;

                if (!_validator.IsValid(equipmentType))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (equipmentType.IsNew)
                {
                    equipmentType.Save();
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} {2}", AuditLogConstants.AddedNew, equipmentType.Code, equipmentType.TypeName),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = equipmentType.EntityName(),
                            EntityId = equipmentType.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (equipmentType.HasChanges())
                {
                    foreach (var change in equipmentType.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = equipmentType.EntityName(),
                                EntityId = equipmentType.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                    equipmentType.Save();
                }

                CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[equipmentType.TenantId].UpdateCache(equipmentType);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.Groups = ProcessorUtilities.GetAll<EquipmentTypeGroup>();
            _view.DatEquipmentTypes = ProcessorUtilities.GetAll<DatEquipmentType>();
        }
    }
}
