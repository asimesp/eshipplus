﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class ChargeCodeHandler : EntityBase
	{
		private readonly IChargeCodeView _view;

		private readonly ChargeCodeValidator _validator = new ChargeCodeValidator();

		public ChargeCodeHandler(IChargeCodeView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<ChargeCode>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var chargeCode in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					chargeCode.Connection = Connection;
					chargeCode.Transaction = Transaction;

					if (!_validator.IsValid(chargeCode))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					chargeCode.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1}", AuditLogConstants.AddedNew, chargeCode.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = chargeCode.EntityName(),
						EntityId = chargeCode.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[chargeCode.TenantId].UpdateCache(chargeCode);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, chargeCode.Code, ex.Message));

				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<ChargeCode> e)
		{
			var chargeCode = e.Argument;
			var @lock = chargeCode.RetrieveLock(_view.ActiveUser, chargeCode.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<ChargeCode> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<string> e)
		{
			var results = new ChargeCodeSearch().FetchChargeCodes(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<ChargeCode> e)
		{
			_validator.Messages.Clear();

			var chargeCode = e.Argument;

			if (chargeCode.IsNew) return;

			var @lock = chargeCode.ObtainLock(_view.ActiveUser, chargeCode.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				chargeCode.Connection = Connection;
				chargeCode.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteChargeCode(chargeCode))
				{
					RollBackTransaction();
                    @lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, chargeCode.Code, chargeCode.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = chargeCode.EntityName(),
						EntityId = chargeCode.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				chargeCode.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[chargeCode.TenantId].UpdateCache(chargeCode, true);

				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<ChargeCode> e)
		{
			var chargeCode = e.Argument;

			if (!chargeCode.IsNew)
			{
				var @lock = chargeCode.RetrieveLock(_view.ActiveUser, chargeCode.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				chargeCode.Connection = Connection;
				chargeCode.Transaction = Transaction;

				if (!_validator.IsValid(chargeCode))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (chargeCode.IsNew)
				{
					chargeCode.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, chargeCode.Code),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = chargeCode.EntityName(),
							EntityId = chargeCode.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (chargeCode.HasChanges())
				{
					foreach (var change in chargeCode.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = chargeCode.EntityName(),
								EntityId = chargeCode.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					chargeCode.Save();
				}

				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[chargeCode.TenantId].UpdateCache(chargeCode);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.Categories = ProcessorUtilities.GetAll<ChargeCodeCategory>();
			_view.Project44Codes = ProcessorUtilities.GetAll<Project44ChargeCode>();
		}
	}
}
