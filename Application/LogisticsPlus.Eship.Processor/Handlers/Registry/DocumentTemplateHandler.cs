﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
    public class DocumentTemplateHandler : EntityBase
    {
        private readonly IDocumentTemplateView _view;

        private readonly DocumentTemplateValidator _validator = new DocumentTemplateValidator();

        public DocumentTemplateHandler(IDocumentTemplateView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
        }

        private void OnUnLock(object sender, ViewEventArgs<DocumentTemplate> e)
        {
            var documentTemplate = e.Argument;
            var @lock = documentTemplate.RetrieveLock(_view.ActiveUser, documentTemplate.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<DocumentTemplate> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnSearch(object sender, ViewEventArgs<string> e)
        {
            var results = new DocumentTemplateSearch().FetchDocumentTemplates(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }

        private void OnDelete(object sender, ViewEventArgs<DocumentTemplate> e)
        {
            _validator.Messages.Clear();

            var documentTemplate = e.Argument;

            if (documentTemplate.IsNew) return;

            var @lock = documentTemplate.ObtainLock(_view.ActiveUser, documentTemplate.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                documentTemplate.Connection = Connection;
                documentTemplate.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                if (!_validator.CanDeleteDocumentTemplate(documentTemplate))
                {
                    @lock.Delete();
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, documentTemplate.Code, documentTemplate.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = documentTemplate.EntityName(),
                        EntityId = documentTemplate.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                documentTemplate.Delete();
                @lock.Delete();

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSave(object sender, ViewEventArgs<DocumentTemplate> e)
        {
            var documentTemplate = e.Argument;

            if (!documentTemplate.IsNew)
            {
                var @lock = documentTemplate.RetrieveLock(_view.ActiveUser, documentTemplate.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                documentTemplate.Connection = Connection;
                documentTemplate.Transaction = Transaction;

                if (documentTemplate.Primary)
                {
                    var message = CheckForcePrimary(documentTemplate);
                    if (message != null)
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(new[] { message });
                        return;
                    }
                }

                if (!_validator.IsValid(documentTemplate))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (documentTemplate.IsNew)
                {
                    documentTemplate.Save();
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, documentTemplate.Code),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = documentTemplate.EntityName(),
                            EntityId = documentTemplate.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (documentTemplate.HasChanges())
                {
                    foreach (var change in documentTemplate.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = documentTemplate.EntityName(),
                                EntityId = documentTemplate.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                    documentTemplate.Save();
                }

                if (_view.TemplateFileHasBeenSaved())
                {
                    CommitTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
                }
                else
                {
                    RollBackTransaction();
                    _view.DisplayMessages(new[]  {ValidationMessage.Error("An error occrured writing the template to file")});
                }

            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.Categories = ProcessorUtilities.GetAll<DocumentTemplateCategory>();
            _view.ServiceModes = ProcessorUtilities.GetAll<ServiceMode>();
            _view.DecimalPlaces = DecimalPlacesUtility.DecimalPlaces();
        }

        private ValidationMessage CheckForcePrimary(DocumentTemplate documentTemplate)
        {
            var currentPrimary = new DocumentTemplateSearch()
                .CurrentPrimaryDocumentTemplate(documentTemplate.TenantId, documentTemplate.ServiceMode, documentTemplate.Category);
            if (currentPrimary != null && currentPrimary.Id != documentTemplate.Id)
            {
                if (_view.ForceDefault)
                {
                    var @lock = currentPrimary.ObtainLock(_view.ActiveUser, currentPrimary.Id);
                    if (!@lock.IsUserLock(_view.ActiveUser))
                        return ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg);

                    currentPrimary.Connection = Connection;
                    currentPrimary.Transaction = Transaction;
                    @lock.Connection = Connection;
                    @lock.Transaction = Transaction;

                    currentPrimary.TakeSnapShot();
                    currentPrimary.Primary = false;
                    foreach (var change in currentPrimary.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = currentPrimary.EntityName(),
                                EntityId = currentPrimary.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                    currentPrimary.Save();
                    @lock.Delete();
                }
                else
                {
                    documentTemplate.Primary = false;
                }
            }
            return null;
        }
    }
}
