﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class PostalCodeHandler : EntityBase
	{
		private readonly IPostalCodeView _view;

		private readonly PostalCodeValidator _validator = new PostalCodeValidator();

		public PostalCodeHandler(IPostalCodeView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.BatchImport += OnBatchImport;
		    _view.ExportAllPostalCodes += OnExportAllPostalCodes;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<PostalCode>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var postalCode in e.Argument)
			{
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					postalCode.Connection = Connection;
					postalCode.Transaction = Transaction;

					if (postalCode.Primary) CheckForcePrimary(postalCode);

					_validator.Messages.Clear();
					if (!_validator.IsValid(postalCode))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					postalCode.Save();

                    new AdminAuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} {2} {3}", AuditLogConstants.AddedNew, postalCode.City, postalCode.State, postalCode.Country.Code),
                        EntityCode = postalCode.EntityName(),
                        EntityId = postalCode.Id.ToString(),
                        LogDateTime = DateTime.Now,
                        AdminUser = _view.ActiveSuperUser
                    }.Log();

					CommitTransaction();
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("An error occurred while saving record [Code: {0}].", postalCode.Code));
				}
			}
			messages.Add(ValidationMessage.Information("Import process complete"));
            _view.DisplayMessages(messages);
		}

		private void OnSearch(object sender, ViewEventArgs<PostalCodeViewSearchCriteria> e)
		{
			var search = new PostalCodeSearch();
			var results = search.FetchPostalCodes(e.Argument);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<PostalCode> e)
		{
			var postalCode = e.Argument;

			if (postalCode.IsNew) return;

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				postalCode.Connection = Connection;
				postalCode.Transaction = Transaction;

                new AdminAuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} {2} {3} {4}", AuditLogConstants.Delete, postalCode.City, postalCode.State, postalCode.Country.Code, postalCode.Code),
                    EntityCode = postalCode.EntityName(),
                    EntityId = postalCode.Id.ToString(),
                    LogDateTime = DateTime.Now,
                    AdminUser = _view.ActiveSuperUser
                }.Log();

				postalCode.Delete();

				CommitTransaction();
				_view.DisplayMessages(new[] { ValidationMessage.Information("Record Deleted.") });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error("An error occurred while deleting record.") });
			}
		}

		private void OnSave(object sender, ViewEventArgs<PostalCode> e)
		{
			var postalCode = e.Argument;
			
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				postalCode.Connection = Connection;
				postalCode.Transaction = Transaction;

				if (postalCode.Primary) CheckForcePrimary(postalCode);

				_validator.Messages.Clear();
				if (!_validator.IsValid(postalCode))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

                if (postalCode.IsNew)
                {
                    postalCode.Save();

                    new AdminAuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                             string.Format("{0} {1} {2} {3} {4}", AuditLogConstants.AddedNew, postalCode.City, postalCode.State, postalCode.Country.Code, postalCode.Code),
                        EntityCode = postalCode.EntityName(),
                        EntityId = postalCode.Id.ToString(),
                        LogDateTime = DateTime.Now,
                        AdminUser = _view.ActiveSuperUser
                    }.Log();
                }
                else if (postalCode.HasChanges())
                {
                    foreach (var change in postalCode.Changes())
                        new AdminAuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            EntityCode = postalCode.EntityName(),
                            EntityId = postalCode.Id.ToString(),
                            LogDateTime = DateTime.Now,
                            AdminUser = _view.ActiveSuperUser
                        }.Log();
                    postalCode.Save();
                }

				CommitTransaction();
				_view.DisplayMessages(new[] { ValidationMessage.Information("Record Saved.") });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error("An error occurred while saving record.") });
			}
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.Countries = new CountrySearch().FetchCountriesThatRequirePostalCode("%");
		}

		private void CheckForcePrimary(PostalCode postalCode)
		{
			var currentPrimary = new PostalCodeSearch().CurrentPrimaryPostalCode(postalCode.Code, postalCode.CountryId);
			if (currentPrimary != null && currentPrimary.Id != postalCode.Id)
				if (_view.ForcePrimary)
				{
                    currentPrimary.TakeSnapShot();

					currentPrimary.Connection = Connection;
					currentPrimary.Transaction = Transaction;
					currentPrimary.Primary = false;

                    foreach (var change in currentPrimary.Changes())
                        new AdminAuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            EntityCode = postalCode.EntityName(),
                            EntityId = postalCode.Id.ToString(),
                            LogDateTime = DateTime.Now,
                            AdminUser = _view.ActiveSuperUser
                        }.Log();

					currentPrimary.Save();
				}
				else
				{
					postalCode.Primary = false;
				}
		}

        private void OnExportAllPostalCodes(object sender, EventArgs e)
        {
            var search = new PostalCodeSearch();
            var results = search.FetchAllPostalCodes();
            _view.ExportPostalCodes(results);
        }
	}
}
