﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Registry
{
	public class P44ChargeCodeHandler : EntityBase
	{
		private readonly IP44ChargeCodeView _view;

		private readonly P44ChargeCodeValidator _validator = new P44ChargeCodeValidator();

		public P44ChargeCodeHandler(IP44ChargeCodeView view)
		{
			_view = view;
		}

		public void Initialize()
		{
            _view.Loading += OnLoading;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.BatchImport += OnBatchImport;
        }

		private void OnBatchImport(object sender, ViewEventArgs<List<P44ChargeCodeMapping>> e)
		{
            var messages = new ValidationMessageCollection();

			foreach (var p44ChargeCodeMapping in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

                    p44ChargeCodeMapping.Connection = Connection;
                    p44ChargeCodeMapping.Transaction = Transaction;

					if (!_validator.IsValid(p44ChargeCodeMapping))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

                    p44ChargeCodeMapping.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = $"{AuditLogConstants.AddedNew} {p44ChargeCodeMapping.Project44Code.GetString()} {p44ChargeCodeMapping.ChargeCode.Code}",
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = p44ChargeCodeMapping.EntityName(),
						EntityId = p44ChargeCodeMapping.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[p44ChargeCodeMapping.TenantId].UpdateCache(p44ChargeCodeMapping);
				}
				catch(Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					messages.Add(ValidationMessage.Error("{0} [Code: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, p44ChargeCodeMapping.Project44Code.GetString(), ex.Message));
				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<P44ChargeCodeMapping> e)
		{
			var p44ChargeCodeMapping = e.Argument;
			var @lock = p44ChargeCodeMapping.RetrieveLock(_view.ActiveUser, p44ChargeCodeMapping.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<P44ChargeCodeMapping> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FaildedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
			var results = new P44ChargeCodeMappingSearch().FetchP44ChargeCodeMappings(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<P44ChargeCodeMapping> e)
		{
			var p44ChargeCodeMapping = e.Argument;

			if (p44ChargeCodeMapping.IsNew) return;

			var @lock = p44ChargeCodeMapping.ObtainLock(_view.ActiveUser, p44ChargeCodeMapping.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
                p44ChargeCodeMapping.Connection = Connection;
                p44ChargeCodeMapping.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							$"{AuditLogConstants.Delete} {p44ChargeCodeMapping.Project44Code.GetString()} {p44ChargeCodeMapping.ChargeCode.Code}",
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = p44ChargeCodeMapping.EntityName(),
						EntityId = p44ChargeCodeMapping.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

                p44ChargeCodeMapping.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[p44ChargeCodeMapping.TenantId].UpdateCache(p44ChargeCodeMapping, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<P44ChargeCodeMapping> e)
		{
			var p44ChargeCodeMapping = e.Argument;

			if (!p44ChargeCodeMapping.IsNew)
			{
				var @lock = p44ChargeCodeMapping.RetrieveLock(_view.ActiveUser, p44ChargeCodeMapping.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// in this implementation, we can validation outside transaction optimizing the execution flow.
			_validator.Messages.Clear();
			if (!_validator.IsValid(p44ChargeCodeMapping))
			{
				_view.DisplayMessages(_validator.Messages);
				return;
			}


			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
                p44ChargeCodeMapping.Connection = Connection;
                p44ChargeCodeMapping.Transaction = Transaction;

				if (p44ChargeCodeMapping.IsNew)
				{
                    p44ChargeCodeMapping.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								$"{AuditLogConstants.AddedNew} {p44ChargeCodeMapping.Project44Code.GetString()} {p44ChargeCodeMapping.ChargeCode.Code}",
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = p44ChargeCodeMapping.EntityName(),
							EntityId = p44ChargeCodeMapping.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (p44ChargeCodeMapping.HasChanges())
				{
					foreach (var change in p44ChargeCodeMapping.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = p44ChargeCodeMapping.EntityName(),
								EntityId = p44ChargeCodeMapping.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
                    p44ChargeCodeMapping.Save();
				}


				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[p44ChargeCodeMapping.TenantId].UpdateCache(p44ChargeCodeMapping);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnLoading(object sender, EventArgs e)
        {
            _view.Project44Codes = ProcessorUtilities.GetAll<Project44ChargeCode>();
        }
	}
}
