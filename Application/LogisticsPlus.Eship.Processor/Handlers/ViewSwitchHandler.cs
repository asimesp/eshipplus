﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Views;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers
{
    public class ViewSwitchHandler : EntityBase
    {
        private readonly IViewSwitchView _view;
        

        public ViewSwitchHandler(IViewSwitchView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.RecordViewSwitch += OnRecordViewSwitch;
        }

        private void OnRecordViewSwitch(object sender, ViewEventArgs<string> e)
        {
            new AuditLog
            {
                Description = e.Argument,
                TenantId = _view.ActiveUser.TenantId,
                User = _view.ActiveUser,
                EntityCode = _view.ActiveUser.EntityName(),
                LogDateTime = DateTime.Now,
                EntityId = _view.ActiveUser.Id.ToString()
            }.Log();
        }
    }
}
