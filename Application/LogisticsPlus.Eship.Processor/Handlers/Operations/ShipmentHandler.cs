﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Project44;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class ShipmentHandler : EntityBase
	{
		private readonly IShipmentView _view;
		private readonly ShipmentValidator _validator = new ShipmentValidator();
        private readonly MiscReceiptValidator _mrValidator = new MiscReceiptValidator();
        private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

		public ShipmentHandler(IShipmentView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.CustomerSearch += OnCustomerSearch;
			_view.VendorSearch += OnVendorSearch;
			_view.SaveShoppedRate += OnSaveShoppedRate;
			_view.SaveSmc3TrackRequestResponse += OnSaveSmc3TrackRequestResponse;
		    _view.SaveProject44TrackRequestResponse += OnSaveProject44TrackRequestResponse;
		}

		private void OnSaveSmc3TrackRequestResponse(object sender, ViewEventArgs<SMC3TrackRequestResponse> e)
		{
			
			//TODO: Should be check for existing record???  Chris look into this!
			//var existingRecords = new SMC3TrackRequestResponseSearch()
			//	.FetchTrackRequestResponseByShipmentId(e.Argument.ShipmentId, e.Argument.Scac);

			//if(existingRecords.Any() && existingRecords.First().)


			var validator = new SMC3RequestResponseValidator();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				validator.Connection = Connection;
				validator.Transaction = Transaction;

				if (!validator.IsValid(e.Argument))
				{
					_view.DisplayMessages(validator.Messages);
					RollBackTransaction();
				    return;
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("SMC3 EVA tracking auto initiated"),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = e.Argument.Shipment.EntityName(),
					EntityId = e.Argument.Shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				e.Argument.Save();

				// commit transaction
				CommitTransaction();
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error("Error processing shipment documents.") });
			}

		}


	    private void OnSaveProject44TrackRequestResponse(object sender, ViewEventArgs<Project44TrackingResponse> e)
	    {
	        var validator = new Project44ResponseValidator();
	        Connection = DatabaseConnection.DefaultConnection;
	        BeginTransaction();
	        try
	        {
	            validator.Connection = Connection;
	            validator.Transaction = Transaction;

	            if (!validator.IsValid(e.Argument))
	            {
	                _view.DisplayMessages(validator.Messages);
	                RollBackTransaction();
	                return;
	            }

	            new AuditLog
	            {
	                Connection = Connection,
	                Transaction = Transaction,
	                Description = string.Format("Project 44 tracking auto initiated"),
	                TenantId = _view.ActiveUser.TenantId,
	                User = _view.ActiveUser,
	                EntityCode = e.Argument.Shipment.EntityName(),
	                EntityId = e.Argument.Shipment.Id.ToString(),
	                LogDateTime = DateTime.Now
	            }.Log();

	            e.Argument.Save();

	            // commit transaction
	            CommitTransaction();
	        }
	        catch (Exception ex)
	        {
	            RollBackTransaction();
	            _view.LogException(ex);
	            _view.DisplayMessages(new[] { ValidationMessage.Error("Error processing shipment tracking.") });
	        }

	    }

        private void OnSave(object sender, ViewEventArgs<Shipment> e)
		{
			var shipment = e.Argument;

			// check locks
			if (!shipment.IsNew)
			{
				var @lock = shipment.RetrieveLock(_view.ActiveUser, shipment.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// assign new numbers
			if (shipment.IsNew)
			{
				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ShipmentNumber, _view.ActiveUser);
				shipment.ShipmentNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
			}

			// retrieve db shipment for collection comparisons ...
			var dbShipment = new Shipment(shipment.Id, false);
			dbShipment.LoadCollections();
			var dbLocations = dbShipment.Stops.ToList();
			if (!dbShipment.IsNew)
			{
				dbLocations.Add(dbShipment.Origin);
				dbLocations.Add(dbShipment.Destination);
			}
			foreach (var location in dbLocations) location.LoadCollections();

			// retrieve shipment historical data
			var historicalData = shipment.FetchHistoricalData() ??
									   new ShipmentHistoricalData
										   {
											   Shipment = shipment,
											   WasInDispute = shipment.InDispute,
                                               WasInDisputeReason = shipment.InDisputeReason,
                                               OriginalEstimatedDeliveryDate = shipment.EstimatedDeliveryDate,
											   OriginalEstimatedPickupDate = shipment.DesiredPickupDate,
										   };
			if (!historicalData.IsNew) historicalData.TakeSnapShot();
			if (shipment.InDispute) historicalData.WasInDispute = true;
            if (historicalData.WasInDisputeReason == InDisputeReason.NotApplicable)
                historicalData.WasInDisputeReason = shipment.InDisputeReason;
			if (historicalData.OriginalEstimatedPickupDate == DateUtility.SystemEarliestDateTime)
				historicalData.OriginalEstimatedPickupDate = shipment.DesiredPickupDate;
			if (historicalData.OriginalEstimatedDeliveryDate == DateUtility.SystemEarliestDateTime)
				historicalData.OriginalEstimatedDeliveryDate = shipment.EstimatedDeliveryDate;

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				shipment.Connection = Connection;
				shipment.Transaction = Transaction;

				historicalData.Connection = Connection;
				historicalData.Transaction = Transaction;

				var msgs = new List<ValidationMessage>();

				// validate shipment
				_validator.Messages.Clear();
				if (!_validator.IsValid(shipment))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}
				if (_validator.Messages.Any()) msgs.AddRange(_validator.Messages); // for warning messages

				//validate account bucket
				if (!shipment.IsNew)
				{
					var hasShipmentAccountBucketAnyChanges = shipment.AccountBuckets.Any(n => n.IsNew || n.HasChanges())
					                                         || dbShipment.AccountBuckets.Any(dbab => !shipment.AccountBuckets
						                                         .Select(v => v.Id).Contains(dbab.Id));

					if (hasShipmentAccountBucketAnyChanges && !shipment.ShipmentAccountBucketCanBeChanged())
					{
						RollBackTransaction();
						_view.DisplayMessages(
							new[] { ValidationMessage.Error(string.Format("Account bucket cannot be changed as shipment was created in a different account period {0}", shipment.DateCreated.ToString("MM-yyyy"))) });
						return;
					}
				}

				// check for need to lock on customer
				if (shipment.IsNew && shipment.Customer.EnableShipperBillOfLading && !ProcessCustomerShipperBOL(shipment))
				{
					RollBackTransaction();
					return;
				}

				// validate purchase order if necessary
				if (shipment.Customer.ValidatePurchaseOrderNumber && !string.IsNullOrEmpty(shipment.PurchaseOrderNumber) &&
					(shipment.PurchaseOrderNumber != dbShipment.PurchaseOrderNumber || shipment.IsNew) &&
					!PurchaseOrderNumberIsValid(shipment))
				{
					RollBackTransaction();
					return;
				}

				// audit log shipment
				if (shipment.IsNew)
				{
					// save
					shipment.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Shipment: {1}", AuditLogConstants.AddedNew, shipment.ShipmentNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (shipment.HasChanges())
				{
					foreach (var change in shipment.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					// save
					shipment.Save();
				}

				// locations
				ProcessShipmentLocations(shipment, dbLocations);

				// terminal information
				ProcessTerminal(shipment.OriginTerminal, dbShipment.OriginTerminal, shipment.EntityName(), shipment.Id);
				ProcessTerminal(shipment.DestinationTerminal, dbShipment.DestinationTerminal, shipment.EntityName(), shipment.Id);

				// resave to ensure location and terminal id saved
				shipment.Save();

				// process rest of collections
				ProcessShipmentServices(shipment, dbShipment.Services);
				ProcessShipmentEquipments(shipment, dbShipment.Equipments);
				ProcessShipmentReferences(shipment, dbShipment.CustomerReferences);
				ProcessShipmentItems(shipment, dbShipment.Items);
				ProcessShipmentCharges(shipment, dbShipment.Charges);
				ProcessShipmentNotes(shipment, dbShipment.Notes);
				ProcessShipmentVendors(shipment, dbShipment.Vendors);
				ProcessShipmentAccountBuckets(shipment, dbShipment.AccountBuckets);
				ProcessShipmentQuickPayOptions(shipment, dbShipment.QuickPayOptions);
				ProcessShipmentAssets(shipment, dbShipment.Assets);
				ProcessShipmentDocuments(shipment, dbShipment.Documents);
				ProcessShipmentAutoRatingAccessorials(shipment, dbShipment.AutoRatingAccessorials);
				ProcessCheckCalls(shipment, dbShipment.CheckCalls);


				// historical data handling
				if (historicalData.IsNew || historicalData.HasChanges()) historicalData.Save();

				// commit transaction
				CommitTransaction();

				//save addresses
				if (_view.SaveOriginToAddressBook) SaveOriginAddress(shipment);
				if (_view.SaveDestinationToAddressBook) SaveDestinationAddress(shipment);

				//if the shipment was a FedEx shipment and the shipment is being saved as voided, attempt to delete shipment at FedEx
				if (shipment.SmallPackageEngine == SmallPackageEngine.FedEx && shipment.Status == ShipmentStatus.Void)
				{
					var pmaps = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].SmallPackagingMaps;
					var smaps = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].SmallPackageServiceMaps;
					var variables = ProcessorVars.IntegrationVariables[_view.ActiveUser.TenantId];
					var rater = new Rater2(variables.RatewareParameters, variables.CarrierConnectParameters,
										  variables.FedExParameters, variables.UpsParameters, pmaps, smaps, variables.Project44Settings);

					var errMsgs = rater.DeleteFedexShipment(shipment);
					if (errMsgs.Any())
					{
						errMsgs.Insert(0, "This FedEx shipment has been successfully voided but due to the following reason(s), this shipment can't be cancelled via eShip:");
						errMsgs.Add("Please contact Fed Ex to handle the cancellation");
						msgs.AddRange(errMsgs.Select(m => ValidationMessage.Warning(m)));
					}
				}

                // refund or void any related misc receipts
                if (_view.RefundOrVoidMiscReceipts)
                    msgs.AddRange(RefundOrVoidMiscReceipts(shipment));

                msgs.Add(ValidationMessage.Information(ProcessorVars.RecordSaveMsg));

				_view.Set(shipment);
				_view.DisplayMessages(msgs);
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDelete(object sender, ViewEventArgs<Shipment> e)
		{
			var shipment = e.Argument;

			if (shipment.IsNew)
			{
				_view.Set(new Shipment());
				return;
			}

            // load collections
			shipment.LoadCollections();
			var locations = shipment.Stops.ToList();
			locations.Add(shipment.Origin);
			locations.Add(shipment.Destination);
			foreach (var stop in locations) stop.LoadCollections();

			// obtain lock/check lock
			var @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

            Lock loadOrderLock = null;
            var relatedLoadOrder = new LoadOrderSearch().FetchLoadOrderByLoadNumber(shipment.ShipmentNumber, shipment.TenantId);
            if (relatedLoadOrder != null)
            {
                loadOrderLock = relatedLoadOrder.ObtainLock(_view.ActiveUser, relatedLoadOrder.Id);
                if (!loadOrderLock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
                relatedLoadOrder.TakeSnapShot();
                relatedLoadOrder.Status = LoadOrderStatus.Covered;
            }

			// retrieve historical data for deletion
			var historicalData = shipment.FetchHistoricalData();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				shipment.Connection = Connection;
				shipment.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				if (historicalData != null)
				{
					historicalData.Connection = Connection;
					historicalData.Transaction = Transaction;
				}

				_validator.Messages.Clear();
				if (!_validator.CanDeleteShipment(shipment))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				//attempt to delete the shipment at FedEx if this is a FedEx shipment
				if (shipment.SmallPackageEngine == SmallPackageEngine.FedEx)
				{
					var pmaps = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].SmallPackagingMaps;
					var smaps = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].SmallPackageServiceMaps;
					var variables = ProcessorVars.IntegrationVariables[_view.ActiveUser.TenantId];

					var rater = new Rater2(variables.RatewareParameters, variables.CarrierConnectParameters,
										  variables.FedExParameters, variables.UpsParameters, pmaps, smaps, variables.Project44Settings);

					var errMsgs = rater.DeleteFedexShipment(shipment);
					if (errMsgs.Any())
					{
						errMsgs.Insert(0, "This FedEx shipment cannot be deleted due to the following reason(s):");
						errMsgs.Add("Please contact Fed Ex to handle the cancellation");
						RollBackTransaction();
						@lock.Delete();
						_view.DisplayMessages(errMsgs.Select(m => ValidationMessage.Error(m)));
						return;
					}
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} Shipment Number: {1}", AuditLogConstants.Delete, shipment.ShipmentNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				// handle collection deletes
				foreach (var location in locations)
				{
					location.Connection = Connection;
					location.Transaction = Transaction;

					foreach (var contact in location.Contacts)
					{
						contact.Connection = Connection;
						contact.Transaction = Transaction;

						contact.Delete();
					}

					location.Delete();
				}

				if (shipment.OriginTerminal != null)
				{
					shipment.OriginTerminal.Connection = Connection;
					shipment.OriginTerminal.Transaction = Transaction;
					shipment.OriginTerminal.Delete();
				}

				if (shipment.DestinationTerminal != null)
				{
					shipment.DestinationTerminal.Connection = Connection;
					shipment.DestinationTerminal.Transaction = Transaction;
					shipment.DestinationTerminal.Delete();
				}

				foreach (var reference in shipment.CustomerReferences)
				{
					reference.Connection = Connection;
					reference.Transaction = Transaction;

					reference.Delete();
				}
				foreach (var item in shipment.Items)
				{
					item.Connection = Connection;
					item.Transaction = Transaction;

					item.Delete();
				}
				foreach (var equipment in shipment.Equipments)
				{
					equipment.Connection = Connection;
					equipment.Transaction = Transaction;

					equipment.Delete();
				}
				foreach (var service in shipment.Services)
				{
					service.Connection = Connection;
					service.Transaction = Transaction;

					service.Delete();
				}
				foreach (var vendor in shipment.Vendors)
				{
					vendor.Connection = Connection;
					vendor.Transaction = Transaction;

					vendor.Delete();
				}

				foreach (var accountBucket in shipment.AccountBuckets)
				{
					accountBucket.Connection = Connection;
					accountBucket.Transaction = Transaction;

					accountBucket.Delete();
				}
				foreach (var charge in shipment.Charges)
				{
					charge.Connection = Connection;
					charge.Transaction = Transaction;

					charge.Delete();
				}
				foreach (var note in shipment.Notes)
				{
					note.Connection = Connection;
					note.Transaction = Transaction;

					note.Delete();
				}
				foreach (var quickPayOption in shipment.QuickPayOptions)
				{
					quickPayOption.Connection = Connection;
					quickPayOption.Transaction = Transaction;

					quickPayOption.Delete();
				}
				foreach (var asset in shipment.Assets)
				{
					asset.Connection = Connection;
					asset.Transaction = Transaction;

					asset.Delete();
				}
				foreach (var document in shipment.Documents)
				{
					document.Connection = Connection;
					document.Transaction = Transaction;

					document.Delete();
				}
				foreach (var autoRatingAccessorial in shipment.AutoRatingAccessorials)
				{
					autoRatingAccessorial.Connection = Connection;
					autoRatingAccessorial.Transaction = Transaction;

					autoRatingAccessorial.Delete();
				}
				foreach (var checkCall in shipment.CheckCalls)
				{
					checkCall.Connection = Connection;
					checkCall.Transaction = Transaction;

					checkCall.Delete();
				}

				// delete historical data
				if (historicalData != null) historicalData.Delete();

				// delete shipment and lock
				shipment.Delete();

                if (relatedLoadOrder != null && relatedLoadOrder.HasChanges())
                {
                    foreach (var change in relatedLoadOrder.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = relatedLoadOrder.EntityName(),
                            EntityId = relatedLoadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save
                    relatedLoadOrder.Save();
                    loadOrderLock.Delete();
                }


				@lock.Delete();

				CommitTransaction();

				_view.Set(new Shipment());
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSaveShoppedRate(object sender, ViewEventArgs<ShoppedRate> e)
		{
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				var shoppedRate = e.Argument;

				shoppedRate.Connection = Connection;
				shoppedRate.Transaction = Transaction;

				var isNew = shoppedRate.IsNew;

				// save
				shoppedRate.Save();

				// only insert services for new shopped rated.  If existing, then this is only and update
				// of reference to quote!
				if (isNew)
					foreach (var service in shoppedRate.Services)
					{
						service.Connection = Connection;
						service.Transaction = Transaction;

						service.Save();
					}

				// commit transaction
				CommitTransaction();
				_view.Set(shoppedRate);
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.Set(new ShoppedRate());
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error("An error occured while performing the last operation. Err: {0}", ex.Message) });
			}
		}


		private void OnVendorSearch(object sender, ViewEventArgs<string> e)
		{
			var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplayVendor(vendor == null || !vendor.Active ? new Vendor() : vendor);
			if (vendor == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
			else if (!vendor.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor is not active") });
		}

		private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
		{
			if (!_view.ActiveUser.TenantEmployee)
				FetchCustomerForNonEmployee(e.Argument);
			else
				FetchCustomerForEmployee(e.Argument);
		}


		private void OnUnLock(object sender, ViewEventArgs<Shipment> e)
		{
			var request = e.Argument;
			var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<Shipment> e)
		{
			var request = e.Argument;
			var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<Shipment> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.EquipmentTypes = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].EquipmentTypes
				.Where(eq => eq.Active)
				.ToList();
			_view.Services = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].Services;
			_view.Assets = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].Assets;

            _view.DateUnits = ProcessorUtilities.GetAll<DateUnit>();
            _view.InDisputeReasons = ProcessorUtilities.GetAll<InDisputeReason>();
			_view.NoteTypes = ProcessorUtilities.GetAll<NoteType>();
			_view.ServiceModes = ProcessorUtilities.GetAll<ServiceMode>();
			_view.MileageEngines = ProcessorUtilities.GetAll<MileageEngine>();
		}


		private void FetchCustomerForNonEmployee(string customerNumber)
		{
			if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
			{
				_view.DisplayCustomer(_view.ActiveUser.DefaultCustomer, true);
				return;
			}
			var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
			var customer = shipAs == null || !shipAs.Customer.Active ? null : shipAs.Customer;
			_view.DisplayCustomer(customer, true);
			if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else if (!shipAs.Customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
		}

		private void FetchCustomerForEmployee(string customerNumber)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

			var customer1 = customer == null || !customer.Active ? new Customer() : customer;
			_view.DisplayCustomer(customer1, true);
			if (customer == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else if (!customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
		}

		private void ProcessShipmentVendors(Shipment shipment, IEnumerable<ShipmentVendor> dbVendors)
		{
			var dbDelete = dbVendors.Where(dbv => !shipment.Vendors.Select(v => v.Id).Contains(dbv.Id));

			foreach (var sv in dbDelete)
			{
				sv.Connection = Connection;
				sv.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Vendor Number:{3}",
												AuditLogConstants.Delete, sv.EntityName(), shipment.ShipmentNumber, sv.Vendor.VendorNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				sv.Delete();
			}

			foreach (var sv in shipment.Vendors)
			{
				sv.Connection = Connection;
				sv.Transaction = Transaction;

				if (sv.IsNew)
				{
					sv.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Vendor Number:{3}",
													AuditLogConstants.AddedNew, sv.EntityName(), shipment.ShipmentNumber, sv.Vendor.VendorNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (sv.HasChanges())
				{
					foreach (var change in sv.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Shipment Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					sv.Save();
				}
			}
		}

		private void ProcessShipmentAccountBuckets(Shipment shipment, IEnumerable<ShipmentAccountBucket> dbAccountBuckets)
		{
			var dbDelete = dbAccountBuckets.Where(dbab => !shipment.AccountBuckets.Select(v => v.Id).Contains(dbab.Id));

			foreach (var qab in dbDelete)
			{
				qab.Connection = Connection;
				qab.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Account Bucket Code:{3}",
												AuditLogConstants.Delete, qab.EntityName(), shipment.ShipmentNumber, qab.AccountBucket.Code),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				qab.Delete();
			}

			foreach (var qab in shipment.AccountBuckets)
			{
				qab.Connection = Connection;
				qab.Transaction = Transaction;

				if (qab.IsNew)
				{
					qab.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Account Bucket Code:{3}",
													AuditLogConstants.AddedNew, qab.EntityName(), shipment.ShipmentNumber, qab.AccountBucket.Code),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (qab.HasChanges())
				{
					foreach (var change in qab.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Shipment Vendor's Account Bucket Code:{1} {2}", qab.EntityName(), qab.AccountBucket.Code, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					qab.Save();
				}
			}
		}

		private void ProcessShipmentNotes(Shipment shipment, IEnumerable<ShipmentNote> dbNotes)
		{
			var dbDelete = dbNotes.Where(dbn => !shipment.Notes.Select(n => n.Id).Contains(dbn.Id));

			foreach (var note in dbDelete)
			{
				note.Connection = Connection;
				note.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Note Ref#:{3}",
												AuditLogConstants.Delete, note.EntityName(), shipment.ShipmentNumber, note.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				note.Delete();
			}

			foreach (var note in shipment.Notes)
			{
				note.Connection = Connection;
				note.Transaction = Transaction;

				if (note.IsNew)
				{
					note.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Note Ref#:{3}",
													AuditLogConstants.AddedNew, note.EntityName(), shipment.ShipmentNumber, note.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (note.HasChanges())
				{
					foreach (var change in note.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Note Ref#:{1} {2}", note.EntityName(), note.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					note.Save();
				}
			}
		}

		private void ProcessShipmentCharges(Shipment shipment, IEnumerable<ShipmentCharge> dbCharges)
		{
			var dbDelete = dbCharges.Where(dbc => !shipment.Charges.Select(c => c.Id).Contains(dbc.Id));

			foreach (var charge in dbDelete)
			{
				charge.Connection = Connection;
				charge.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Charge Ref#:{3}",
												AuditLogConstants.Delete, charge.EntityName(), shipment.ShipmentNumber, charge.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				charge.Delete();
			}

			foreach (var charge in shipment.Charges)
			{
				charge.Connection = Connection;
				charge.Transaction = Transaction;

				if (charge.IsNew)
				{
					charge.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Charge Ref#:{3}",
													AuditLogConstants.AddedNew, charge.EntityName(), shipment.ShipmentNumber, charge.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (charge.HasChanges())
				{
					foreach (var change in charge.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Charge Ref#:{1} {2}", charge.EntityName(), charge.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					charge.Save();
				}
			}
		}

		private void ProcessShipmentItems(Shipment shipment, IEnumerable<ShipmentItem> dbItems)
		{
			var dbDelete = dbItems.Where(dbi => !shipment.Items.Select(i => i.Id).Contains(dbi.Id));

			foreach (var i in dbDelete)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Item Ref#:{3}",
												AuditLogConstants.Delete, i.EntityName(), shipment.ShipmentNumber, i.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				i.Delete();
			}

			foreach (var i in shipment.Items)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				if (i.IsNew)
				{
					i.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Item Ref#:{3}",
													AuditLogConstants.AddedNew, i.EntityName(), shipment.ShipmentNumber, i.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (i.HasChanges())
				{
					foreach (var change in i.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Item Ref#:{1} {2}", i.EntityName(), i.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					i.Save();
				}
			}
		}

		private void ProcessShipmentReferences(Shipment shipment, IEnumerable<ShipmentReference> dbReferences)
		{
			var dbDelete = dbReferences.Where(dbr => !shipment.CustomerReferences.Select(r => r.Id).Contains(dbr.Id));

			foreach (var r in dbDelete)
			{
				r.Connection = Connection;
				r.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Customer Reference:{3}",
													AuditLogConstants.Delete, r.EntityName(), shipment.ShipmentNumber, r.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				r.Delete();
			}

			foreach (var r in shipment.CustomerReferences)
			{
				r.Connection = Connection;
				r.Transaction = Transaction;

				if (r.IsNew)
				{
					r.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Shipment Number:{2} Customer Reference:{3}",
														AuditLogConstants.AddedNew, r.EntityName(), shipment.ShipmentNumber, r.Name),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (r.HasChanges())
				{
					foreach (var change in r.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Customer Reference:{1} {2}", r.EntityName(), r.Name, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					r.Save();
				}
			}
		}

		private void ProcessShipmentEquipments(Shipment shipment, IEnumerable<ShipmentEquipment> dbEquipments)
		{
			var dbDelete = dbEquipments.Where(dbe => !shipment.Equipments.Select(e => e.EquipmentTypeId).Contains(dbe.EquipmentTypeId));

			foreach (var s in dbDelete)
			{
				s.Connection = Connection;
				s.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Shipment Number:{2} Equipment Type Ref#: {3}", AuditLogConstants.Delete, s.EntityName(),
									  shipment.ShipmentNumber, s.EquipmentTypeId),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				s.Delete();
			}

			foreach (var equipment in shipment.Equipments)
				if (!_validator.ShipmentEquipmentExists(equipment))
				{
					equipment.Connection = Connection;
					equipment.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} Shipment Number:{2} Equipment Type Ref#:{3}", AuditLogConstants.AddedNew,
										  equipment.EntityName(), shipment.ShipmentNumber, equipment.EquipmentTypeId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					equipment.Save();
				}
		}

		private void ProcessShipmentServices(Shipment shipment, IEnumerable<ShipmentService> dbServices)
		{
			var dbDelete = dbServices.Where(dbs => !shipment.Services.Select(s => s.ServiceId).Contains(dbs.ServiceId));

			foreach (var s in dbDelete)
			{
				s.Connection = Connection;
				s.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Shipment Number:{2} Service Ref#: {3}", AuditLogConstants.Delete, s.EntityName(),
									  shipment.ShipmentNumber, s.ServiceId),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				s.Delete();
			}

			foreach (var service in shipment.Services)
				if (!_validator.ShipmentServiceExists(service))
				{
					service.Connection = Connection;
					service.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} Shipment Number:{2} Service Ref#:{3}", AuditLogConstants.AddedNew,
										  service.EntityName(),
										  shipment.ShipmentNumber, service.ServiceId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					service.Save();
				}
		}

		private void ProcessShipmentLocations(Shipment shipment, List<ShipmentLocation> dbShipmentLocations)
		{
			var locations = shipment.Stops.ToList();
			locations.Add(shipment.Origin);
			locations.Add(shipment.Destination);
			var dbDelete = dbShipmentLocations.Where(l => !locations.Select(cl => cl.Id).Contains(l.Id));

			foreach (var l in dbDelete)
			{
				foreach (var c in l.Contacts)
				{
					c.Connection = Connection;
					c.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3} Contact Ref#: {4}",
													AuditLogConstants.Delete, c.EntityName(), shipment.ShipmentNumber, l.Id, c.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					c.Delete();
				}

				l.Connection = Connection;
				l.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3}",
												AuditLogConstants.Delete, l.EntityName(), shipment.ShipmentNumber, l.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				l.Delete();
			}

			foreach (var l in locations)
			{
				l.Connection = Connection;
				l.Transaction = Transaction;

				if (l.IsNew)
				{
					l.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3}",
													AuditLogConstants.AddedNew, l.EntityName(), shipment.ShipmentNumber, l.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (l.HasChanges())
				{
					foreach (var change in l.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref#:{1} {2}", l.EntityName(), l.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					l.Save();
				}

				ProcessShipmentContacts(shipment, l, dbShipmentLocations.FirstOrDefault(dbl => dbl.Id == l.Id));
			}
		}

		private void ProcessShipmentContacts(Shipment shipment, ShipmentLocation location, ShipmentLocation dbLocation)
		{
			var dbDelete = dbLocation != null
							? dbLocation.Contacts.Where(l => !location.Contacts.Select(cl => cl.Id).Contains(l.Id))
							: new List<ShipmentContact>();

			foreach (var c in dbDelete)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3} Contact Ref#: {4}",
												AuditLogConstants.Delete, c.EntityName(), shipment.ShipmentNumber, location.Id, c.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				c.Delete();
			}

			foreach (var c in location.Contacts)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				if (c.IsNew)
				{
					c.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3} Contact Ref#: {4}",
													AuditLogConstants.AddedNew, c.EntityName(), shipment.ShipmentNumber, location.Id, c.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (c.HasChanges())
				{
					foreach (var change in c.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("Location Ref#{0} {1} {2}", location.Id, c.EntityName(), change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					c.Save();
				}
			}
		}

		private void ProcessShipmentQuickPayOptions(Shipment shipment, IEnumerable<ShipmentQuickPayOption> dbQuickPayOptions)
		{
			var dbDelete = dbQuickPayOptions.Where(dbi => !shipment.QuickPayOptions.Select(i => i.Id).Contains(dbi.Id));

			foreach (var o in dbDelete)
			{
				o.Connection = Connection;
				o.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Quick Pay Option Ref#:{3}",
												AuditLogConstants.Delete, o.EntityName(), shipment.ShipmentNumber, o.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				o.Delete();
			}

			foreach (var o in shipment.QuickPayOptions)
			{
				o.Connection = Connection;
				o.Transaction = Transaction;

				if (o.IsNew)
				{
					o.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Quick Pay Option Ref#:{3}",
													AuditLogConstants.AddedNew, o.EntityName(), shipment.ShipmentNumber, o.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (o.HasChanges())
				{
					foreach (var change in o.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Quick Pay Option Ref#:{1} {2}", o.EntityName(), o.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					o.Save();
				}
			}
		}

		private void ProcessShipmentAssets(Shipment shipment, IEnumerable<ShipmentAsset> dbAssets)
		{
			var dbDelete = dbAssets.Where(dba => !shipment.Assets.Select(a => a.Id).Contains(dba.Id));

			foreach (var sha in dbDelete)
			{
				sha.Connection = Connection;
				sha.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Shimpent Asset:{3}",
												AuditLogConstants.Delete, sha.EntityName(), shipment.ShipmentNumber, sha.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				sha.Delete();
			}

			foreach (var sha in shipment.Assets)
			{
				sha.Connection = Connection;
				sha.Transaction = Transaction;

				if (sha.IsNew)
				{
					sha.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Shipment Asset:{3}",
												AuditLogConstants.AddedNew, sha.EntityName(), shipment.ShipmentNumber, sha.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (sha.HasChanges())
				{
					foreach (var change in sha.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Shipment Asset's Asset:{1} {2}", sha.EntityName(), sha.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					sha.Save();
				}
			}
		}

		private void ProcessShipmentDocuments(Shipment shipment, IEnumerable<ShipmentDocument> dbDocuments)
		{
			var dbDelete = dbDocuments.Where(dbs => !shipment.Documents.Select(s => s.Id).Contains(dbs.Id));

			foreach (var d in dbDelete)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Shipment Number:{2} Document Name: {3}", AuditLogConstants.Delete, d.EntityName(),
									  shipment.ShipmentNumber, d.Name),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				d.Delete();
			}

			foreach (var d in shipment.Documents)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				if (d.IsNew)
				{
					d.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Shipment Number:{2} Document Name:{3}",
														AuditLogConstants.AddedNew, d.EntityName(), shipment.ShipmentNumber, d.Name),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (d.HasChanges())
				{
					foreach (var change in d.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Document Name:{1} {2}", d.EntityName(), d.Name, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					d.Save();
				}
			}
		}

		private void ProcessShipmentAutoRatingAccessorials(Shipment shipment, IEnumerable<ShipmentAutoRatingAccessorial> dbAutoratingAccessorials)
		{
			var dbDelete = dbAutoratingAccessorials.Where(dbara => !shipment.AutoRatingAccessorials.Select(ara => ara.Id).Contains(dbara.Id));

			foreach (var ara in dbDelete)
			{
				ara.Connection = Connection;
				ara.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Autorating Service Description:{3}",
												AuditLogConstants.Delete, ara.EntityName(), shipment.ShipmentNumber, ara.ServiceDescription),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				ara.Delete();
			}

			foreach (var ara in shipment.AutoRatingAccessorials)
			{
				ara.Connection = Connection;
				ara.Transaction = Transaction;

				if (ara.IsNew)
				{
					ara.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Autorating Service Description:{3}",
													AuditLogConstants.AddedNew, ara.EntityName(), shipment.ShipmentNumber, ara.ServiceDescription),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (ara.HasChanges())
				{
					foreach (var change in ara.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Autorating Service Description:{1} {2}", ara.EntityName(), ara.ServiceDescription, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					ara.Save();
				}
			}
		}

		private void ProcessTerminal(LTLTerminalInfo info, LTLTerminalInfo dbInfo, string entityName, long entityId)
		{
			// remove terminal information
			if (info == null && dbInfo != null)
			{
				dbInfo.Connection = Connection;
				dbInfo.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} LTL Terminal Info: {1}", AuditLogConstants.Delete, dbInfo.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = entityName,
						EntityId = entityId.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				dbInfo.Delete();
			}

			// return if info is null
			if (info == null) return;

			// remove previous terminal before saving new one
			if (dbInfo != null && !dbInfo.IsNew && dbInfo.Id != info.Id)
			{
				dbInfo.Connection = Connection;
				dbInfo.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} LTL Terminal Info: {1}", AuditLogConstants.Delete, info.Name),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = entityName,
					EntityId = entityId.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
				dbInfo.Delete();
			}

			info.Connection = Connection;
			info.Transaction = Transaction;

			// audit log shipment
			if (info.IsNew)
			{
				// save
				info.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} LTL Terminal Info: {1}", AuditLogConstants.AddedNew, info.Name),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = info.Shipment.EntityName(),
					EntityId = info.Shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
			else if (info.HasChanges())
			{
				foreach (var change in info.Changes())
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = change,
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = info.Shipment.EntityName(),
						EntityId = info.Shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// save
				info.Save();
			}
		}

		private void ProcessCheckCalls(Shipment shipment, IEnumerable<CheckCall> dbCheckCalls)
		{
			var dbDelete = dbCheckCalls.Where(dbcc => !shipment.CheckCalls.Select(e => e.Id).Contains(dbcc.Id));

			foreach (var checkCall in dbDelete)
			{
				checkCall.Connection = Connection;
				checkCall.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Shipment Number:{2} CheckCall Ref#: {3}", AuditLogConstants.Delete, checkCall.EntityName(),
									  shipment.ShipmentNumber, checkCall.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				checkCall.Delete();
			}

			foreach (var checkCall in shipment.CheckCalls)
			{
				checkCall.Connection = Connection;
				checkCall.Transaction = Transaction;

				if (checkCall.IsNew)
				{
					checkCall.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} Shipment Number:{2} CheckCall Ref#:{3}", AuditLogConstants.AddedNew,
										  checkCall.EntityName(), shipment.ShipmentNumber, checkCall.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();


				}
				else if (checkCall.HasChanges())
				{
					foreach (var change in checkCall.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description =
									string.Format("{0} Shipment Number: {1} Check Call Ref#:{2} {3}", checkCall.EntityName(),
												  shipment.ShipmentNumber, checkCall.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					checkCall.Save();
				}
			}
		}


		private bool ProcessCustomerShipperBOL(Shipment shipment)
		{
			var @lock = shipment.Customer.ObtainLock(_view.ActiveUser, shipment.Customer.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return false;
			}

			// change customer transaction
			shipment.Customer.Connection = Connection;
			shipment.Customer.Transaction = Transaction;

			@lock.Connection = Connection;
			@lock.Transaction = Transaction;

			// set shipper bol and increment customer seed
			shipment.Customer.TakeSnapShot();
			shipment.ShipperBol = string.Format("{0}{1}{2}", shipment.Customer.ShipperBillOfLadingPrefix,
												++shipment.Customer.ShipperBillOfLadingSeed,
												shipment.Customer.ShipperBillOfLadingSuffix);

			// audit log customer
			foreach (var change in shipment.Customer.Changes())
				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} on shipment [{1}]", change, shipment.ShipmentNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.Customer.EntityName(),
						EntityId = shipment.Customer.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

			// save customer
			shipment.Customer.Save();

			// remove customer lock
			@lock.Delete();

			return true;
		}

		private bool PurchaseOrderNumberIsValid(Shipment shipment)
		{
			var purchaseOrder = new CustomerPurchaseOrderSearch()
				.FetchCustomerPurchaseOrder(shipment.TenantId, shipment.Customer.Id, shipment.PurchaseOrderNumber);

			if (purchaseOrder == null)
			{
				var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
							  ? ProcessorVars.MissingPOErrMsg
							  : shipment.Customer.InvalidPurchaseOrderNumberMessage;

				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			var @lock = purchaseOrder.ObtainLock(_view.ActiveUser, shipment.Customer.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return false;
			}

			if (purchaseOrder.ExpirationDate.TimeToMaximum() < DateTime.Now.TimeToMaximum())
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
							  ? string.Format(ProcessorVars.ExpiredPOErrMsgDateFormat, purchaseOrder.ExpirationDate.FormattedShortDateAlt())
							  : shipment.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			if (purchaseOrder.ApplyOnOrigin && !(purchaseOrder.IsValidPostalCodeFormat(shipment.Origin.PostalCode) && purchaseOrder.CountryId == shipment.Origin.CountryId))
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
							? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "origin")
							: shipment.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			if (purchaseOrder.ApplyOnDestination && !(purchaseOrder.IsValidPostalCodeFormat(shipment.Destination.PostalCode) && purchaseOrder.CountryId == shipment.Destination.CountryId))
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
							? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "destination")
							: shipment.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			purchaseOrder.TakeSnapShot();

			purchaseOrder.Connection = Connection;
			purchaseOrder.Transaction = Transaction;

			@lock.Connection = Connection;
			@lock.Transaction = Transaction;

			if (++purchaseOrder.CurrentUses > purchaseOrder.MaximumUses)
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.MaxUsePOErrMsgFormat, purchaseOrder.MaximumUses) });
				return false;
			}


			foreach (var change in purchaseOrder.Changes())
				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} for shipment [{1}]", change, shipment.ShipmentNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = purchaseOrder.EntityName(),
						EntityId = purchaseOrder.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

			// save purchase order
			purchaseOrder.Save();

			// remove lock
			@lock.Delete();

			return true;
		}


		private void SaveOriginAddress(Shipment shipment)
		{
			// build
			var addressBook = new AddressBook
			{
				City = shipment.Origin.City,
				State = shipment.Origin.State,
				CountryId = shipment.Origin.CountryId,
				Street1 = shipment.Origin.Street1,
				Street2 = shipment.Origin.Street2,
				Customer = shipment.Customer,
				Description = shipment.Origin.Description,
				OriginSpecialInstruction = shipment.Origin.SpecialInstructions,
				Direction = shipment.Origin.Direction,
				GeneralInfo = shipment.Origin.GeneralInfo,
				DestinationSpecialInstruction = string.Empty,
				PostalCode = shipment.Origin.PostalCode,
				TenantId = shipment.Origin.TenantId,
				Contacts = new List<AddressBookContact>(),
				Services = new List<AddressBookService>()
			};

			addressBook.Contacts.AddRange(
				shipment.Origin.Contacts.Select(c =>
												new AddressBookContact
												{
													TenantId = c.TenantId,
													AddressBook = addressBook,
													Comment = c.Comment,
													ContactTypeId = c.ContactTypeId,
													Email = c.Email,
													Fax = c.Fax,
													Phone = c.Phone,
													Mobile = c.Mobile,
													Name = c.Name,
													Primary = c.Primary,
												}));

			SaveAddressBook(addressBook);
		}

		private void SaveDestinationAddress(Shipment shipment)
		{
			// build
			var addressBook = new AddressBook
			{
				City = shipment.Destination.City,
				State = shipment.Destination.State,
				CountryId = shipment.Destination.CountryId,
				Street1 = shipment.Destination.Street1,
				Street2 = shipment.Destination.Street2,
				Customer = shipment.Customer,
				Description = shipment.Destination.Description,
				DestinationSpecialInstruction = shipment.Destination.SpecialInstructions,
				OriginSpecialInstruction = string.Empty,
				Direction = shipment.Destination.Direction,
				GeneralInfo = shipment.Destination.GeneralInfo,
				PostalCode = shipment.Destination.PostalCode,
				TenantId = shipment.Destination.TenantId,
				Contacts = new List<AddressBookContact>(),
				Services = new List<AddressBookService>()
			};
			addressBook.Contacts.AddRange(
				shipment.Destination.Contacts.Select(c =>
												new AddressBookContact
												{
													TenantId = c.TenantId,
													AddressBook = addressBook,
													Comment = c.Comment,
													ContactTypeId = c.ContactTypeId,
													Email = c.Email,
													Fax = c.Fax,
													Phone = c.Phone,
													Mobile = c.Mobile,
													Name = c.Name,
													Primary = c.Primary,
												}));

			SaveAddressBook(addressBook);
		}

		private void SaveAddressBook(AddressBook addressBook)
		{
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// save and audit log
				addressBook.Connection = Connection;
				addressBook.Transaction = Transaction;
				addressBook.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} Ref #:{1}", AuditLogConstants.AddedNew, addressBook.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = addressBook.EntityName(),
					EntityId = addressBook.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				// contants
				foreach (var contact in addressBook.Contacts)
				{
					contact.Connection = Connection;
					contact.Transaction = Transaction;

					contact.AddressBook = addressBook;

					contact.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Contact Name: {2} ", AuditLogConstants.AddedNew,
													contact.EntityName(), contact.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = addressBook.EntityName(),
						EntityId = addressBook.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}

				// commit
				CommitTransaction();
			}
			catch
			{
				RollBackTransaction();
			}
		}


        private IEnumerable<ValidationMessage> RefundOrVoidMiscReceipts(Shipment shipment)
        {
            var miscReceipts = new MiscReceiptSearch().FetchMiscReceiptsRelatedToShipment(shipment.Id, shipment.TenantId);
            var gateway = new Tenant(_view.ActiveUser.TenantId).GetPaymentGatewayService();
            var messages = new List<ValidationMessage>();

            _mrValidator.Messages.Clear();
            var transactionId = string.Empty;


            foreach (var miscReceipt in miscReceipts.Where(mr => !mr.Reversal))
            {
                if (miscReceipt.PaymentGatewayType != miscReceipt.Tenant.PaymentGatewayType && miscReceipt.PaymentGatewayType != PaymentGatewayType.NotApplicable && miscReceipt.PaymentGatewayType != PaymentGatewayType.Check)
                {
                    messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptErrorPaymentGatewayForTenantChanged, miscReceipt.AmountPaid));
                    continue;
                }

                var refundOrVoidReceipt = new MiscReceipt
                {
                    AmountPaid = miscReceipt.AmountLeftToBeAppliedOrRefunded(miscReceipt.TenantId),
                    CustomerId = miscReceipt.CustomerId,
                    GatewayTransactionId = string.Empty,
                    PaymentGatewayType = miscReceipt.PaymentGatewayType,
                    PaymentDate = DateTime.Now,
                    Reversal = true,
                    ShipmentId = miscReceipt.ShipmentId,
                    LoadOrderId = miscReceipt.LoadOrderId,
                    TenantId = miscReceipt.TenantId,
                    UserId = miscReceipt.UserId,
                    OriginalMiscReceiptId = miscReceipt.Id,
                    MiscReceiptApplications = new List<MiscReceiptApplication>(),
                    NameOnCard = miscReceipt.NameOnCard,
                    PaymentProfileId = string.Empty
                };

                // do not refund a receipt that was already fully applied or refunded
                if (refundOrVoidReceipt.AmountPaid <= 0) continue;

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _mrValidator.Connection = Connection;
                    _mrValidator.Transaction = Transaction;

                    refundOrVoidReceipt.Connection = Connection;
                    refundOrVoidReceipt.Transaction = Transaction;

                    if (!_mrValidator.IsValid(refundOrVoidReceipt))
                    {
                        messages.AddRange(_mrValidator.Messages);
                        RollBackTransaction();
                        continue;
                    }

                    refundOrVoidReceipt.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Misc Receipt Shipment #:{1} Load Order #:{2} Customer:{3} Amount:{4}",
                                            AuditLogConstants.AddedNew,
                                            refundOrVoidReceipt.ShipmentId != default(long)
                                                ? refundOrVoidReceipt.Shipment.ShipmentNumber
                                                : string.Empty,
                                            refundOrVoidReceipt.LoadOrderId != default(long)
                                                ? refundOrVoidReceipt.LoadOrder.LoadOrderNumber
                                                : string.Empty,
                                            refundOrVoidReceipt.Customer.CustomerNumber,
                                            refundOrVoidReceipt.AmountPaid),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = refundOrVoidReceipt.EntityName(),
                        EntityId = refundOrVoidReceipt.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    if (refundOrVoidReceipt.PaymentGatewayType == PaymentGatewayType.NotApplicable || refundOrVoidReceipt.PaymentGatewayType == PaymentGatewayType.Check)
                    {
                        CommitTransaction();
                        messages.Add(ValidationMessage.Information(ProcessorVars.RefundSuccessfullyProcessedMessage));
                        continue;
                    }

                    switch (miscReceipt.GetTransactionStatusViaPaymentGateway())
                    {
                         case TransactionStatus.CommunicationError:
                        case TransactionStatus.ApprovedReview:
                        case TransactionStatus.GeneralError:
                        case TransactionStatus.FailedReview:
                        case TransactionStatus.SettlementError:
                        case TransactionStatus.UnderReview:
                        case TransactionStatus.FdsPendingReview:
                        case TransactionStatus.FdsAuthorizedPendingReview:
                        case TransactionStatus.ReturnedItem:
                        case TransactionStatus.NotApplicable:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionNotApplicable, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.Declined:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionDeclined, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.Expired:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionExpired, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.Voided:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionVoided, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.CouldNotVoid:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionCouldNotVoid, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.RefundSettledSuccessfully:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionRefundSettledSuccessfully, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.RefundPendingSettlement:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionRefundPendingSettlement, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.AuthorizedPendingCapture:
                        case TransactionStatus.CapturedPendingSettlement:
                            if (refundOrVoidReceipt.AmountPaid < miscReceipt.AmountPaid)
                            {
                                RollBackTransaction();
                                messages.Add(ValidationMessage.Error(ProcessorVars.MiscReceiptCannotVoidPartiallyApplied, refundOrVoidReceipt.OriginalMiscReceipt.AmountPaid));
                                continue;
                            }
                            var voidResponse = gateway.Void(miscReceipt.GatewayTransactionId);
                            if (voidResponse.Approved)
                            {
                                refundOrVoidReceipt.TakeSnapShot();
                                refundOrVoidReceipt.GatewayTransactionId = voidResponse.TransactionId;
                                foreach (var change in refundOrVoidReceipt.Changes())
                                    new AuditLog
                                    {
                                        Connection = Connection,
                                        Transaction = Transaction,
                                        Description = change,
                                        TenantId = _view.ActiveUser.TenantId,
                                        User = _view.ActiveUser,
                                        EntityCode = refundOrVoidReceipt.EntityName(),
                                        EntityId = refundOrVoidReceipt.Id.ToString(),
                                        LogDateTime = DateTime.Now
                                    }.Log();
                                refundOrVoidReceipt.Save();
                                CommitTransaction();
                                messages.Add(ValidationMessage.Information(ProcessorVars.VoidSuccessfullyProcessedMessage));
                            }
                            else
                            {
                                RollBackTransaction();
                                messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptErrorVoidingTransaction, refundOrVoidReceipt.AmountPaid, voidResponse.ErrorMessage));
                            }
                            break;
                        case TransactionStatus.SettledSuccessfully:
                            var refundResponse = gateway.Refund(miscReceipt.GatewayTransactionId, refundOrVoidReceipt.AmountPaid);
                            if (refundResponse.Approved)
                            {
                                refundOrVoidReceipt.TakeSnapShot();
                                transactionId = refundResponse.TransactionId;
                                refundOrVoidReceipt.GatewayTransactionId = refundResponse.TransactionId;
                                foreach (var change in refundOrVoidReceipt.Changes())
                                    new AuditLog
                                    {
                                        Connection = Connection,
                                        Transaction = Transaction,
                                        Description = change,
                                        TenantId = _view.ActiveUser.TenantId,
                                        User = _view.ActiveUser,
                                        EntityCode = refundOrVoidReceipt.EntityName(),
                                        EntityId = refundOrVoidReceipt.Id.ToString(),
                                        LogDateTime = DateTime.Now
                                    }.Log();
                                refundOrVoidReceipt.Save();
                                CommitTransaction();
                                messages.Add(ValidationMessage.Information(ProcessorVars.RefundSuccessfullyProcessedMessage));
                            }
                            else
                            {
                                RollBackTransaction();
                                messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptErrorRefundingTransaction, miscReceipt.AmountPaid, refundResponse.ErrorMessage));
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    var msgs = new List<ValidationMessage>();
                    if (!string.IsNullOrEmpty(transactionId))
                    {
                        try
                        {
                            gateway.Void(transactionId);
                        }
                        catch (Exception gException)
                        {
                            msgs.Add(ValidationMessage.Information(gException.Message));
                        }
                    }
                    RollBackTransaction();
                    msgs.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
                    _view.LogException(ex);
                }
            }

            return messages;
        }

	}
}
