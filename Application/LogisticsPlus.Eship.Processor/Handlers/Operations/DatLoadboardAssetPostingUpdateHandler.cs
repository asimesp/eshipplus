﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class DatLoadboardAssetPostingUpdateHandler : EntityBase
    {
        private readonly DatLoadboardAssetPostingValidator _validator = new DatLoadboardAssetPostingValidator();

        public List<ValidationMessage> SaveDatLoadboardAssetPosting(DatLoadboardAssetPosting posting, LoadOrder loadOrder ,out Exception ex, User activeUser = null)
        {
            Lock @lock = null;
            var user = activeUser ?? posting.Tenant.DefaultSystemUser;
            if (!posting.IsNew)
                try
                {
                    @lock = posting.ObtainLock(user, posting.Id);
                    if (!@lock.IsUserLock(user))
                    {
                        ex = new Exception(ProcessorVars.UnableToObtainLockErrMsg);
                        return new List<ValidationMessage> { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) };
                    }
                }
                catch (Exception e1)
                {
                    ex = e1;
                    return new List<ValidationMessage> { ValidationMessage.Error(ex.Message) };
                }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                posting.Connection = Connection;
                posting.Transaction = Transaction;

                // validate shipment
                _validator.Messages.Clear();
                if (!_validator.IsValid(posting))
                {
                    RollBackTransaction();
                    ex = new Exception(_validator.Messages.Select(m => m.Message).ToArray().NewLineJoin());
                    return _validator.Messages.ToList();
                }


                if (posting.IsNew)
                {
                    posting.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Added New [Asset Id #: {1}, ID #: {2}]", posting.EntityName(), posting.AssetId, posting.IdNumber),
                        TenantId = posting.TenantId,
                        User = user,
                        EntityCode = posting.EntityName(),
                        EntityId = posting.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    if (loadOrder != null)
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Added New [Asset Id #: {1}]", posting.EntityName(), posting.AssetId),
                            TenantId = posting.TenantId,
                            User = user,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (posting.HasChanges())
                {
                    foreach (var change in posting.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = posting.TenantId,
                            User = user,
                            EntityCode = posting.EntityName(),
                            EntityId = posting.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save
                    posting.Save();
                }

                // release lock
                if (@lock != null) @lock.Delete();

                // commit transaction
                CommitTransaction();

                ex = null;
                return new List<ValidationMessage>();
            }
            catch (Exception e)
            {
                RollBackTransaction();
                ex = e;
                return new List<ValidationMessage> { ValidationMessage.Error(ex.Message) };
            }
        }

        public List<ValidationMessage> DeleteDatLoadboardAssetPosting(DatLoadboardAssetPosting posting, LoadOrder loadOrder, out Exception ex, User activeUser = null)
        {
            activeUser = activeUser ?? posting.Tenant.DefaultSystemUser;
            if (posting.IsNew)
            {
                ex = null;
                return new List<ValidationMessage> { ValidationMessage.Error("Cannot delete unsaved DAT loadboard asset posting record.") };
            }

            var @lock = posting.ObtainLock(activeUser, posting.Id);
            if (!@lock.IsUserLock(activeUser))
            {
                ex = null;
                return new List<ValidationMessage>{ ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) };
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                posting.Connection = Connection;
                posting.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} {2}", AuditLogConstants.Delete, posting.AssetId, posting.Id),
                    TenantId = activeUser.TenantId,
                    User = activeUser,
                    EntityCode = posting.EntityName(),
                    EntityId = posting.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                if (loadOrder != null)
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} [Asset Id #: {2}]", posting.EntityName(), AuditLogConstants.Delete, posting.AssetId),
                        TenantId = posting.TenantId,
                        User = activeUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                posting.Delete();
                @lock.Delete();

                CommitTransaction();

                ex = null;
                return new List<ValidationMessage>();
            }
            catch (Exception e)
            {
                RollBackTransaction();
                @lock.Delete();
                ex = e;
                return new List<ValidationMessage> { ValidationMessage.Error(ex.Message) };
            }
        }
    }
}
