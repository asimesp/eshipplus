﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class MacroPointOrderUpdateHandler : EntityBase
	{
		private readonly MacroPointOrderValidator _validator = new MacroPointOrderValidator();

		public List<ValidationMessage> SaveMacroPointOrder(MacroPointOrder order, out Exception ex, User activeUser = null)
		{
			Lock @lock = null;
			var user = activeUser ?? order.Tenant.DefaultSystemUser;
			if(!order.IsNew)
				try
				{
					@lock = order.ObtainLock(user, order.Id);
					if (!@lock.IsUserLock(user))
					{
						ex = new Exception(ProcessorVars.UnableToObtainLockErrMsg);
						return new List<ValidationMessage> {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)};
					}
				}
				catch (Exception e1)
				{
					ex = e1;
					return new List<ValidationMessage> {ValidationMessage.Error(ex.Message)};
				}

			var shipment = order.RetrieveCorrespondingShipment();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				order.Connection = Connection;
				order.Transaction = Transaction;

				// validate shipment
				_validator.Messages.Clear();
				if (!_validator.IsValid(order))
				{
					RollBackTransaction();
					ex = new Exception(_validator.Messages.Select(m => m.Message).ToArray().NewLineJoin());
					return _validator.Messages.ToList();
				}


				if (order.IsNew)
				{
					order.Save();
				
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Added New [Order Id #: {1}, ID #: {2}]", order.EntityName(), order.OrderId, order.IdNumber),
							TenantId = order.TenantId,
							User = user,
							EntityCode = order.EntityName(),
							EntityId = order.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					if (shipment != null)
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Added New [Order Id #: {1}]", order.EntityName(), order.OrderId),
								TenantId = order.TenantId,
								User = user,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
				}
				else if (order.HasChanges())
				{
					foreach (var change in order.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = order.TenantId,
							User = user,
							EntityCode = order.EntityName(),
							EntityId = order.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// save
					order.Save();
				}

				// release lock
				if (@lock != null) @lock.Delete();

				// commit transaction
				CommitTransaction();

				ex = null;
				return new List<ValidationMessage>();
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
				return new List<ValidationMessage> { ValidationMessage.Error(ex.Message) };
			}
		}
	}
}
