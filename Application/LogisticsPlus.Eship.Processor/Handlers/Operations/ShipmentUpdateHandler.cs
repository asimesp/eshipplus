﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class ShipmentUpdateHandler : EntityBase
	{
		private readonly ShipmentValidator _validator = new ShipmentValidator();

		public List<ValidationMessage> UpdateShipmentTrackingInfo(Shipment shipment, out Exception ex, User activeUser = null)
		{
			if (shipment.IsNew)
			{
				ex = null;
				return new List<ValidationMessage> {ValidationMessage.Error("Cannot save non-existing shipment")};
			}

			Lock @lock;
			var user = activeUser ?? (shipment.Tenant == null ? null : shipment.Tenant.DefaultSystemUser);
			try
			{
				@lock = shipment.ObtainLock(user, shipment.Id);
				if (!@lock.IsUserLock(user))
				{
					ex = null;
					return new List<ValidationMessage> {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)};
				}
			}
			catch (Exception e1)
			{
				ex = e1;
				return new List<ValidationMessage> {ValidationMessage.Error(ex.Message)};
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				shipment.Connection = Connection;
				shipment.Transaction = Transaction;

				// validate shipment
				_validator.Messages.Clear();
				if (!_validator.HasAllRequiredData(shipment) || !_validator.ShipmentCheckCallsAreValid(shipment.CheckCalls) || !_validator.ShipmentVendorsAreValid(shipment.Vendors) || !_validator.CheckCallTimesAreValid(shipment.CheckCalls, shipment))
				{
					RollBackTransaction();
					ex = new Exception(string.Join(Environment.NewLine, _validator.Messages.Select(m => m.Message).ToArray()));
					return _validator.Messages.ToList();
				}

				if (shipment.HasChanges())
				{
					foreach (var change in shipment.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = shipment.TenantId,
								User = user,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					// save
					shipment.Save();
				}

				// origin and destination
				if (shipment.Origin.HasChanges())
				{
					foreach (var change in shipment.Origin.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref#:{1} {2}", shipment.Origin.EntityName(), shipment.Origin.Id, change),
							TenantId = shipment.TenantId,
							User = user,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					shipment.Origin.Save();
				}
				if (shipment.Destination.HasChanges())
				{
					foreach (var change in shipment.Destination.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref#:{1} {2}", shipment.Destination.EntityName(), shipment.Destination.Id, change),
							TenantId = shipment.TenantId,
							User = user,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					shipment.Destination.Save();
				}

				// vendor pro change
				foreach (var sv in shipment.Vendors)
					if (sv.HasChanges())
					{
						sv.Connection = Connection;
						sv.Transaction = Transaction;

						foreach (var change in sv.Changes())
							new AuditLog
								{
									Connection = Connection,
									Transaction = Transaction,
									Description =
										string.Format("{0} Shipment Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
									TenantId = shipment.TenantId,
									User = user,
									EntityCode = shipment.EntityName(),
									EntityId = shipment.Id.ToString(),
									LogDateTime = DateTime.Now
								}.Log();

						sv.Save();
					}

				// status check call additions
				foreach (var checkCall in shipment.CheckCalls)
				{
					checkCall.Connection = Connection;
					checkCall.Transaction = Transaction;

					if (checkCall.IsNew)
					{
						checkCall.Save();

						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} Shipment Number:{2} CheckCall Ref#:{3}", AuditLogConstants.AddedNew,
											  checkCall.EntityName(), shipment.ShipmentNumber, checkCall.Id),
							TenantId = shipment.TenantId,
							User = user,
							EntityCode = shipment.EntityName(),
							EntityId = shipment.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();


					}
					else if (checkCall.HasChanges())
					{
						foreach (var change in checkCall.Changes())
							new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description =
									string.Format("{0} Shipment Number: {1} Check Call Ref#:{2} {3}", checkCall.EntityName(),
												  shipment.ShipmentNumber, checkCall.Id, change),
								TenantId = shipment.TenantId,
								User = user,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

						checkCall.Save();
					}
				}

                // shipment document additions
                foreach (var d in shipment.Documents)
                {
                    d.Connection = Connection;
                    d.Transaction = Transaction;

                    if (d.IsNew)
                    {
                        d.Save();
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Shipment Number:{2} Document Name:{3}",
                                                        AuditLogConstants.AddedNew, d.EntityName(), shipment.ShipmentNumber,
                                                        d.Name),
                            TenantId = shipment.TenantId,
                            User = user,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    }
                    else if (d.HasChanges())
                    {
                        foreach (var change in d.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("{0} Document Name:{1} {2}", d.EntityName(), d.Name, change),
                                TenantId = shipment.TenantId,
                                User = user,
                                EntityCode = shipment.EntityName(),
                                EntityId = shipment.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                        d.Save();
                    }
                }

                // release lock
				@lock.Delete();

				// commit transaction
				CommitTransaction();

				ex = null;
				return new List<ValidationMessage>();
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
				return new List<ValidationMessage> {ValidationMessage.Error(ex.Message)};
			}
		}

		public List<ValidationMessage> UpdateShipmentTrackingInfo(Shipment shipment, User user)
		{

			Exception ex;
			return UpdateShipmentTrackingInfo(shipment, out ex, user);
		}

		public List<ValidationMessage> SaveShipmentCharge(ShipmentCharge charge, out Exception ex, User activeUser = null)
		{
			if (!charge.IsNew)
			{
				ex = null;
				return new List<ValidationMessage> {ValidationMessage.Error("Charge must be new!")};
			}

			var user = activeUser ?? (charge.Tenant == null ? null : charge.Tenant.DefaultSystemUser);

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				charge.Connection = Connection;
				charge.Transaction = Transaction;

				// validate shipment
				_validator.Messages.Clear();
				if (!_validator.ShipmentChargesAreValid(new List<ShipmentCharge> {charge}))
				{
					RollBackTransaction();
					ex = new Exception(string.Join(Environment.NewLine, _validator.Messages.Select(m => m.Message).ToArray()));
					return _validator.Messages.ToList();
				}

				// save charge
				charge.Save();
				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Shipment Number:{2} Charge Ref#:{3}",
						                            AuditLogConstants.AddedNew, charge.EntityName(), charge.Shipment.ShipmentNumber,
						                            charge.Id),
						TenantId = charge.TenantId,
						User = user,
						EntityCode = charge.Shipment.EntityName(),
						EntityId = charge.Shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// commit transaction
				CommitTransaction();

				ex = null;
				return new List<ValidationMessage>();
			}
			catch (Exception e)
			{
				RollBackTransaction();
				ex = e;
				return new List<ValidationMessage> {ValidationMessage.Error(ex.Message)};
			}
		}
	}
}
