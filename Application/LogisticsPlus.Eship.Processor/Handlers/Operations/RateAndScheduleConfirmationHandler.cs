﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;
using LogisticsPlus.Eship.Processor.Validation.Operations;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class RateAndScheduleConfirmationHandler : EntityBase
	{
		private readonly IRateAndScheduleConfirmationView _view;

		public RateAndScheduleConfirmationHandler(IRateAndScheduleConfirmationView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.UpdateShipmentDocumentsAndTracking += OnUpdateShipmentDocumentsAndTracking;
		    _view.UpdateShipmentCheckCalls += OnUpdateShipmentCheckCalls;
		    _view.UpdateShipmentNotes += OnUpdateShipmentNotes;
            _view.SaveSmc3TrackRequestResponse += OnSaveSmc3TrackRequestResponse;
			_view.SaveSmc3DispatchRequestResponse += OnSaveSmc3DispatchRequestResponse;
		    _view.SaveProject44DispatchRequestResponse += OnSaveProject44DispatchRequestResponse;
		}

		private void OnSaveSmc3DispatchRequestResponse(object sender, ViewEventArgs<SMC3DispatchRequestResponse> e)
		{
			var validator = new SMC3RequestResponseValidator();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				validator.Connection = Connection;
				validator.Transaction = Transaction;

				if (!validator.IsValid(e.Argument))
				{
					_view.DisplayMessages(validator.Messages);
					RollBackTransaction();
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("SMC3 EVA dispatch auto initiated"),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = e.Argument.Shipment.EntityName(),
					EntityId = e.Argument.Shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				e.Argument.Save();

				// commit transaction
				CommitTransaction();
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error("Error processing shipment dispatch request.") });
			}
		}

		private void OnSaveSmc3TrackRequestResponse(object sender, ViewEventArgs<SMC3TrackRequestResponse> e)
		{
			var validator = new SMC3RequestResponseValidator();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				validator.Connection = Connection;
				validator.Transaction = Transaction;

				if (!validator.IsValid(e.Argument))
				{
					_view.DisplayMessages(validator.Messages);
					RollBackTransaction();
				}

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("SMC3 EVA tracking auto initiated"),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = e.Argument.Shipment.EntityName(),
						EntityId = e.Argument.Shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				e.Argument.Save();

				// commit transaction
				CommitTransaction();
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
				_view.DisplayMessages(new[] {ValidationMessage.Error("Error processing shipment tracking request.")});
			}

		}

	    private void OnSaveProject44DispatchRequestResponse(object sender, ViewEventArgs<Project44DispatchResponse> e)
	    {
	        var validator = new Project44ResponseValidator();

	        Connection = DatabaseConnection.DefaultConnection;
	        BeginTransaction();
	        try
	        {
	            validator.Connection = Connection;
	            validator.Transaction = Transaction;

	            if (!validator.IsValid(e.Argument))
	            {
	                _view.DisplayMessages(validator.Messages);
	                RollBackTransaction();
	            }

	            new AuditLog
	            {
	                Connection = Connection,
	                Transaction = Transaction,
	                Description = string.Format("Project 44 dispatch auto initiated"),
	                TenantId = _view.ActiveUser.TenantId,
	                User = _view.ActiveUser,
	                EntityCode = e.Argument.Shipment.EntityName(),
	                EntityId = e.Argument.Shipment.Id.ToString(),
	                LogDateTime = DateTime.Now
	            }.Log();

	            e.Argument.Save();

	            // commit transaction
	            CommitTransaction();
	        }
	        catch (Exception ex)
	        {
	            RollBackTransaction();
	            _view.LogException(ex);
	            _view.DisplayMessages(new[] { ValidationMessage.Error("Error processing shipment dispatch request.") });
	        }
	    }

        private void OnUpdateShipmentDocumentsAndTracking(object sender, ViewEventArgs<Shipment> e)
		{
			var shipment = e.Argument;

			var @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Customer.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				// tracking
				foreach (var sv in shipment.Vendors)
				{
					foreach (var change in sv.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description =
									string.Format("{0} Shipment Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					sv.Save();
				}

				// documents
				foreach (var d in shipment.Documents)
				{
					d.Connection = Connection;
					d.Transaction = Transaction;

					if (d.IsNew)
					{
						d.Save();

						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Document Ref#:{1}", AuditLogConstants.AddedNew, d.Id),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = shipment.EntityName(),
								EntityId = shipment.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
					}
					else
					{
						foreach (var change in d.Changes())
							new AuditLog
								{
									Connection = Connection,
									Transaction = Transaction,
									Description = string.Format("{0} Document Ref#:{1} {2}", d.EntityName(), d.Id, change),
									TenantId = _view.ActiveUser.TenantId,
									User = _view.ActiveUser,
									EntityCode = shipment.EntityName(),
									EntityId = shipment.Id.ToString(),
									LogDateTime = DateTime.Now
								}.Log();

						d.Save();
					}
				}

				// delete lock
				@lock.Delete();

				// commit transaction
				CommitTransaction();
			}
            catch (Exception ex)
			{
                _view.LogException(ex);
				_view.DisplayMessages(new[] {ValidationMessage.Error("Error processing shipment documents.")});
			}
		}

        private void OnUpdateShipmentCheckCalls(object sender, ViewEventArgs<Shipment> e)
        {
            var shipment = e.Argument;

            var @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Customer.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                // check calls
                foreach (var checkCall in shipment.CheckCalls)
                {
                    foreach (var change in checkCall.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description =
                                string.Format("{0} {1} Shipment Number:{2} CheckCall Ref#: {3}", AuditLogConstants.Delete, checkCall.EntityName(),
                                    shipment.ShipmentNumber, checkCall.Id),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    checkCall.Save();
                }


                // delete lock
                @lock.Delete();

                // commit transaction
                CommitTransaction();
            }
            catch (Exception ex)
            {
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error("Error processing shipment documents.") });
            }
        }
        private void OnUpdateShipmentNotes(object sender, ViewEventArgs<Shipment> e)
        {
            var shipment = e.Argument;

            var @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Customer.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                // check calls
                foreach (var note in shipment.Notes)
                {
                    foreach (var change in note.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Shipment Number:{2} Note Ref#:{3}", AuditLogConstants.AddedNew, note.EntityName(), shipment.ShipmentNumber, note.Id),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    note.Save();
                }


                // delete lock
                @lock.Delete();

                // commit transaction
                CommitTransaction();
            }
            catch (Exception ex)
            {
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error("Error processing shipment notes.") });
            }
        }
    }
}
