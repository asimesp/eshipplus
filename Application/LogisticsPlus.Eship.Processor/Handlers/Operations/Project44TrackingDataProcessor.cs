﻿using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using ObjToSql.Core;
using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Project44.StatusUpdates;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class Project44TrackingDataProcessor : EntityBase
	{
        public List<ValidationMessage> SaveTrackingData(Project44AsyncTrackingResponse data, out Exception ex)
        {
            try
            {
                var trackingData = new Project44TrackingData
                {
                    DateCreated = DateTime.Now,
                    ResponseData = JsonConvert.SerializeObject(data),
                    Project44Id = data.latestShipmentStatus.shipment.id
                };

                var validator = new Project44TrackingDataValidator();
                if (!validator.IsValid(trackingData))
                {
                    ex = null;
                    return validator.Messages;
                }

                // Save TrackingData
                trackingData.Save();

                ex = null;
                return new List<ValidationMessage>();
            }
            catch (Exception e)
            {
                ex = e;
                return new List<ValidationMessage> { ValidationMessage.Error(e.Message) };
            }
        }
    }
}