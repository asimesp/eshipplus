﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class LoadOrderDeletionHandler : EntityBase
    {
        private readonly LoadOrderValidator _loadValidator = new LoadOrderValidator();
        private const string PurgeFailureMessage = "Cannot Purge Expired Load Order";

        public List<string> DeleteLoadOrders(List<LoadOrder> loadOrders)
        {
            var errorMessages = new List<string>();

            foreach (var loadOrder in loadOrders)
            {
                // load collections
                loadOrder.LoadCollections();
                var locations = loadOrder.Stops.ToList();
                locations.Add(loadOrder.Origin);
                locations.Add(loadOrder.Destination);
                foreach (var stop in locations) stop.LoadCollections();

                // retrieve historical data for deletion
                var historicalData = loadOrder.FetchHistoricalData();

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    loadOrder.Connection = Connection;
                    loadOrder.Transaction = Transaction;

                    _loadValidator.Connection = Connection;
                    _loadValidator.Transaction = Transaction;

                    if (historicalData != null)
                    {
                        historicalData.Connection = Connection;
                        historicalData.Transaction = Transaction;
                    }

                    _loadValidator.Messages.Clear();
                    if (!_loadValidator.CanDeleteLoadOrder(loadOrder))
                    {
                        // Do not add error message here; if the load is used elsewhere, it does not count as a purging error
                        RollBackTransaction();
                        continue;
                    }

                    new AuditLog
                    {
                        Description =
                            string.Format("{0} Expired Load Order Number: {1}", AuditLogConstants.Delete,
                                          loadOrder.LoadOrderNumber),
                        TenantId = loadOrder.TenantId,
                        User = loadOrder.Tenant.DefaultSystemUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    // handle collection deletes
                    foreach (var location in locations)
                    {
                        location.Connection = Connection;
                        location.Transaction = Transaction;

                        foreach (var contact in location.Contacts)
                        {
                            contact.Connection = Connection;
                            contact.Transaction = Transaction;

                            contact.Delete();
                        }

                        location.Delete();
                    }

                    foreach (var reference in loadOrder.CustomerReferences)
                    {
                        reference.Connection = Connection;
                        reference.Transaction = Transaction;

                        reference.Delete();
                    }

                    foreach (var item in loadOrder.Items)
                    {
                        item.Connection = Connection;
                        item.Transaction = Transaction;

                        item.Delete();
                    }

                    foreach (var equipment in loadOrder.Equipments)
                    {
                        equipment.Connection = Connection;
                        equipment.Transaction = Transaction;

                        equipment.Delete();
                    }

                    foreach (var service in loadOrder.Services)
                    {
                        service.Connection = Connection;
                        service.Transaction = Transaction;

                        service.Delete();
                    }

                    foreach (var vendor in loadOrder.Vendors)
                    {
                        vendor.Connection = Connection;
                        vendor.Transaction = Transaction;

                        vendor.Delete();
                    }

                    foreach (var accountBucket in loadOrder.AccountBuckets)
                    {
                        accountBucket.Connection = Connection;
                        accountBucket.Transaction = Transaction;

                        accountBucket.Delete();
                    }

                    foreach (var charge in loadOrder.Charges)
                    {
                        charge.Connection = Connection;
                        charge.Transaction = Transaction;

                        charge.Delete();
                    }

                    foreach (var note in loadOrder.Notes)
                    {
                        note.Connection = Connection;
                        note.Transaction = Transaction;

                        note.Delete();
                    }

                    foreach (var document in loadOrder.Documents)
                    {
                        document.Connection = Connection;
                        document.Transaction = Transaction;

                        document.Delete();
                    }

                    // delete historical data
                    if (historicalData != null) historicalData.Delete();

                    loadOrder.Delete();

                    CommitTransaction();
                }
                catch (Exception exc)
                {
                    RollBackTransaction();
                    errorMessages.Add(string.Format("{0} {1}: ", PurgeFailureMessage, loadOrder.LoadOrderNumber) + exc.Message);
                }
            }

            return errorMessages;
        }


    }
}
