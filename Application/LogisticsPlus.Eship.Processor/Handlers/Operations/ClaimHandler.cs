﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class ClaimHandler : EntityBase
	{
		private readonly IClaimView _view;
		private readonly ClaimValidator _validator = new ClaimValidator();
		private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

		public ClaimHandler(IClaimView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.LoadAuditLog += OnLoadAuditLog;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.CustomerSearch += OnCustomerSearch;
		}

		private void OnSave(object sender, ViewEventArgs<Claim> e)
		{
			var claim = e.Argument;

			// check locks
			if (!claim.IsNew)
			{
				var @lock = claim.RetrieveLock(_view.ActiveUser, claim.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// assign new numbers
			if (claim.IsNew)
			{
				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ClaimNumber, _view.ActiveUser);
				claim.ClaimNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
			}

			// retrieve db shipment for collection comparisons ...
			var dbClaim = new Claim(claim.Id, false);
			dbClaim.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				claim.Connection = Connection;
				claim.Transaction = Transaction;

				// validate shipment
				_validator.Messages.Clear();
				if (!_validator.IsValid(claim))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// audit log shipment
				if (claim.IsNew)
				{
					// save
					claim.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Claim: {1}", AuditLogConstants.AddedNew, claim.ClaimNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = claim.EntityName(),
						EntityId = claim.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (claim.HasChanges())
				{
					foreach (var change in claim.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = claim.EntityName(),
							EntityId = claim.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// save
					claim.Save();
				}

				// process rest of collections
				ProcessClaimNotes(claim, dbClaim.Notes);
				ProcessClaimVendors(claim, dbClaim.Vendors);
				ProcessClaimServiceTickets(claim, dbClaim.ServiceTickets);
				ProcessClaimShipment(claim, dbClaim.Shipments);
				ProcessClaimDocuments(claim, dbClaim.Documents);

				// commit transaction
				CommitTransaction();

				_view.Set(claim);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnDelete(object sender, ViewEventArgs<Claim> e)
		{
			var claim = e.Argument;

			if (claim.IsNew)
			{
				_view.Set(new Claim());
				return;
			}

			// load collections
			claim.LoadCollections();

			// obtain lock/check lock
			var @lock = claim.ObtainLock(_view.ActiveUser, claim.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				claim.Connection = Connection;
				claim.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} Claim Number: {1}", AuditLogConstants.Delete, claim.ClaimNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = claim.EntityName(),
					EntityId = claim.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				foreach (var vendor in claim.Vendors)
				{
					vendor.Connection = Connection;
					vendor.Transaction = Transaction;

					vendor.Delete();
				}

				foreach (var shipment in claim.Shipments)
				{
					shipment.Connection = Connection;
					shipment.Transaction = Transaction;

					shipment.Delete();
				}

				foreach (var note in claim.Notes)
				{
					note.Connection = Connection;
					note.Transaction = Transaction;

					note.Delete();
				}

				foreach (var serviceTicket in claim.ServiceTickets)
				{
					serviceTicket.Connection = Connection;
					serviceTicket.Transaction = Transaction;

					serviceTicket.Delete();
				}

				foreach (var document in claim.Documents)
				{
					document.Connection = Connection;
					document.Transaction = Transaction;

					document.Delete();
				}

				// delete claim and lock
				claim.Delete();

				@lock.Delete();

				CommitTransaction();

				_view.Set(new Claim());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}


		private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
		{
			if (!_view.ActiveUser.TenantEmployee)
				FetchCustomerForNonEmployee(e.Argument);
			else
				FetchCustomerForEmployee(e.Argument);
		}

		private void OnUnLock(object sender, ViewEventArgs<Claim> e)
		{
			var request = e.Argument;
			var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<Claim> e)
		{
			var request = e.Argument;
			var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<Claim> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.NoteTypes = ProcessorUtilities.GetAll<NoteType>();
		    _view.ClaimTypes = ProcessorUtilities.GetAll<ClaimType>();
		    _view.AmountClaimedTypes = ProcessorUtilities.GetAll<AmountClaimedType>();
		}


		private void FetchCustomerForNonEmployee(string customerNumber)
		{
			if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
			{
				_view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
				return;
			}
			var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
			_view.DisplayCustomer(shipAs == null || !shipAs.Customer.Active ? null : shipAs.Customer);
			if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else if (!shipAs.Customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
		}

		private void FetchCustomerForEmployee(string customerNumber)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

			_view.DisplayCustomer(customer == null || !customer.Active ? new Customer() : customer);
			if (customer == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else if (!customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
		}


		private void ProcessClaimVendors(Claim claim, IEnumerable<ClaimVendor> dbVendors)
		{
			var dbDelete = dbVendors.Where(dbv => !claim.Vendors.Select(v => v.Id).Contains(dbv.Id));

			foreach (var qv in dbDelete)
			{
				qv.Connection = Connection;
				qv.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Claim Number:{2} Vendor Number:{3}",
												AuditLogConstants.Delete, qv.EntityName(), claim.ClaimNumber, qv.Vendor.VendorNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = claim.EntityName(),
					EntityId = claim.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				qv.Delete();
			}

			foreach (var qv in claim.Vendors)
			{
			    qv.Connection = Connection;
			    qv.Transaction = Transaction;

			    if (qv.IsNew)
			    {
			        if (!_validator.ClaimVendorExists(qv))
			        {
			            qv.Save();
			            new AuditLog
			                {
			                    Connection = Connection,
			                    Transaction = Transaction,
			                    Description = string.Format("{0} {1} Claim Number:{2} Vendor Number:{3}",
			                                                AuditLogConstants.AddedNew, qv.EntityName(), claim.ClaimNumber,
			                                                qv.Vendor.VendorNumber),
			                    TenantId = _view.ActiveUser.TenantId,
			                    User = _view.ActiveUser,
			                    EntityCode = claim.EntityName(),
			                    EntityId = claim.Id.ToString(),
			                    LogDateTime = DateTime.Now
			                }.Log();
			        }
			    }
			    else if (qv.HasChanges())
			    {
			        foreach (var change in qv.Changes())
			            new AuditLog
			                {
			                    Connection = Connection,
			                    Transaction = Transaction,
			                    Description = string.Format("{0} Claim Vendor's Vendor Number:{1} {2}", qv.EntityName(), qv.Vendor.VendorNumber, change),
			                    TenantId = _view.ActiveUser.TenantId,
			                    User = _view.ActiveUser,
			                    EntityCode = claim.EntityName(),
			                    EntityId = claim.Id.ToString(),
			                    LogDateTime = DateTime.Now
			                }.Log();
			        qv.Save();
			    }
			}
		}

		private void ProcessClaimServiceTickets(Claim claim, IEnumerable<ClaimServiceTicket> dbServiceTickets)
		{
			var dbDelete = dbServiceTickets.Where(dbv => !claim.ServiceTickets.Select(v => v.ServiceTicketId).Contains(dbv.ServiceTicketId));

			foreach (var qv in dbDelete)
			{
				qv.Connection = Connection;
				qv.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Claim Number:{2} Service Ticket Number:{3}",
												AuditLogConstants.Delete, qv.EntityName(), claim.ClaimNumber, qv.ServiceTicket.ServiceTicketNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = claim.EntityName(),
					EntityId = claim.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				qv.Delete();
			}

			foreach (var qv in claim.ServiceTickets)
			{
				if (!_validator.ClaimServiceTicketExists(qv))
				{
					qv.Connection = Connection;
					qv.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Claim Number:{2} Service Ticket Number:{3}",
													AuditLogConstants.AddedNew, qv.EntityName(), claim.ClaimNumber, qv.ServiceTicket.ServiceTicketNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = claim.EntityName(),
						EntityId = claim.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					qv.Save();
				}
			}
		}

		private void ProcessClaimShipment(Claim claim, IEnumerable<ClaimShipment> dbShipments)
		{
			var dbDelete = dbShipments.Where(dbv => !claim.Shipments.Select(v => v.ShipmentId).Contains(dbv.ShipmentId));

			foreach (var qv in dbDelete)
			{
				qv.Connection = Connection;
				qv.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Claim Number:{2} Shipment Number:{3}",
												AuditLogConstants.Delete, qv.EntityName(), claim.ClaimNumber, qv.Shipment.ShipmentNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = claim.EntityName(),
					EntityId = claim.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				qv.Delete();
			}

			foreach (var qv in claim.Shipments)
			{
				if (!_validator.ClaimShipmentExists(qv))
				{
					qv.Connection = Connection;
					qv.Transaction = Transaction;

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Claim Number:{2} Shipment Number:{3}",
													AuditLogConstants.AddedNew, qv.EntityName(), claim.ClaimNumber, qv.Shipment.ShipmentNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = claim.EntityName(),
						EntityId = claim.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					qv.Save();
				}
			}
		}

		private void ProcessClaimNotes(Claim claim, IEnumerable<ClaimNote> dbNotes)
		{
			var dbDelete = dbNotes.Where(dbn => !claim.Notes.Select(n => n.Id).Contains(dbn.Id));

			foreach (var note in dbDelete)
			{
				note.Connection = Connection;
				note.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Claim Number:{2} Note Ref#:{3}",
												AuditLogConstants.Delete, note.EntityName(), claim.ClaimNumber, note.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = claim.EntityName(),
					EntityId = claim.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				note.Delete();
			}

			foreach (var note in claim.Notes)
			{
				note.Connection = Connection;
				note.Transaction = Transaction;

				if (note.IsNew)
				{
					note.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Claim Number:{2} Note Ref#:{3}",
													AuditLogConstants.AddedNew, note.EntityName(), claim.ClaimNumber, note.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = claim.EntityName(),
						EntityId = claim.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (note.HasChanges())
				{
					foreach (var change in note.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} Note Ref#:{1} {2}", note.EntityName(), note.Id, change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = claim.EntityName(),
								EntityId = claim.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					note.Save();
				}
			}
		}

		private void ProcessClaimDocuments(Claim claim, IEnumerable<ClaimDocument> dbDocuments)
		{
			var dbDelete = dbDocuments.Where(dbs => !claim.Documents.Select(s => s.Id).Contains(dbs.Id));

			foreach (var d in dbDelete)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Claim Number:{2} Document Ref#: {3}", AuditLogConstants.Delete, d.EntityName(),
									  claim.ClaimNumber, d.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = claim.EntityName(),
					EntityId = claim.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				d.Delete();
			}

			foreach (var d in claim.Documents)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				if (d.IsNew)
				{
					d.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Claim Number:{2} Document Ref#:{3}",
													AuditLogConstants.AddedNew, d.EntityName(), claim.ClaimNumber, d.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = claim.EntityName(),
						EntityId = claim.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (d.HasChanges())
				{
					foreach (var change in d.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Document Ref#:{1} {2}", d.EntityName(), d.Id, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = claim.EntityName(),
							EntityId = claim.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					d.Save();
				}
			}
		}
	}
}
