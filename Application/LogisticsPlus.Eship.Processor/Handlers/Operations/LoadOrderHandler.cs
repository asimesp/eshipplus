﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;
using Shipment = LogisticsPlus.Eship.Core.Operations.Shipment;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class LoadOrderHandler : EntityBase
    {
        private readonly ILoadOrderView _view;
        private readonly LoadOrderValidator _loadValidator = new LoadOrderValidator();
        private readonly ShipmentValidator _shipmentValidator = new ShipmentValidator();
        private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();
        private readonly MiscReceiptValidator _mrValidator = new MiscReceiptValidator();

        public LoadOrderHandler(ILoadOrderView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.LoadAuditLog += OnLoadAuditLog;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.CustomerSearch += OnCustomerSearch;
            _view.VendorSearch += OnVendorSearch;
            _view.SaveConvertToShipment += OnSaveConvertToShipment;
        }


        private void OnSave(object sender, ViewEventArgs<LoadOrder> e)
        {
            var loadOrder = e.Argument;

            // check locks
            if (!loadOrder.IsNew)
            {
                var @lock = loadOrder.RetrieveLock(_view.ActiveUser, loadOrder.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            // assign new numbers
            if (loadOrder.IsNew)
            {
                var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ShipmentNumber, _view.ActiveUser);
                loadOrder.LoadOrderNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
            }

            // retrieve db loadOrder for collection comparisons ...
            var dbLoadOrder = new LoadOrder(loadOrder.Id, false);
            dbLoadOrder.LoadCollections();
            var dbLocations = dbLoadOrder.Stops.ToList();
            if (!dbLoadOrder.IsNew)
            {
                dbLocations.Add(dbLoadOrder.Origin);
                dbLocations.Add(dbLoadOrder.Destination);
            }

            foreach (var location in dbLocations) location.LoadCollections();

            // retrieve load order historical data
            var historicalData = loadOrder.FetchHistoricalData() ??
                                 new LoadOrderHistoricalData
                                     {
                                         LoadOrder = loadOrder,
                                         WasAccepted = loadOrder.Status == LoadOrderStatus.Accepted,
                                         WasCancelled = loadOrder.Status == LoadOrderStatus.Cancelled,
                                         WasCovered = loadOrder.Status == LoadOrderStatus.Covered,
                                         WasLost = loadOrder.Status == LoadOrderStatus.Lost,
                                         WasOffered = loadOrder.Status == LoadOrderStatus.Offered,
                                         WasQuoted = loadOrder.Status == LoadOrderStatus.Quoted,
                                         WasShipment = loadOrder.Status == LoadOrderStatus.Shipment
                                     };

            if(!historicalData.IsNew) historicalData.TakeSnapShot();
            if (loadOrder.Status == LoadOrderStatus.Accepted) historicalData.WasAccepted = true;
            if (loadOrder.Status == LoadOrderStatus.Cancelled) historicalData.WasCancelled = true;
            if (loadOrder.Status == LoadOrderStatus.Covered) historicalData.WasCovered = true;
            if (loadOrder.Status == LoadOrderStatus.Lost) historicalData.WasLost = true;
            if (loadOrder.Status == LoadOrderStatus.Offered) historicalData.WasOffered = true;
            if (loadOrder.Status == LoadOrderStatus.Quoted) historicalData.WasQuoted = true;
            if (loadOrder.Status == LoadOrderStatus.Shipment) historicalData.WasShipment = true;

            var messages = new List<ValidationMessage>();

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _loadValidator.Connection = Connection;
                _loadValidator.Transaction = Transaction;

                loadOrder.Connection = Connection;
                loadOrder.Transaction = Transaction;

                historicalData.Connection = Connection;
                historicalData.Transaction = Transaction;

                // validate loadOrder
                _loadValidator.Messages.Clear();
                if (!_loadValidator.IsValid(loadOrder))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_loadValidator.Messages);
                    return;
                }

                // check for need to lock on customer
                if (loadOrder.Customer.EnableShipperBillOfLading && !ProcessCustomerShipperBOL(loadOrder))
                {
                    RollBackTransaction();
                    return;
                }

                // validate purchase order if necessary
				if (loadOrder.Customer.ValidatePurchaseOrderNumber && !string.IsNullOrEmpty(loadOrder.PurchaseOrderNumber) &&
					(loadOrder.PurchaseOrderNumber != dbLoadOrder.PurchaseOrderNumber || loadOrder.IsNew) && 
	                !PurchaseOrderNumberIsValid(loadOrder))
	            {
		            RollBackTransaction();
		            return;
	            }


                // audit log loadOrder
                if (loadOrder.IsNew)
                {
                    // save
                    loadOrder.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Load Order: {1}", AuditLogConstants.AddedNew, loadOrder.LoadOrderNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (loadOrder.HasChanges())
                {
                    foreach (var change in loadOrder.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save
                    loadOrder.Save();
                }
                
                

                // locations
                ProcessLoadOrderLocations(loadOrder, dbLocations);

                // resave to ensure location id saved
                loadOrder.Save();

                // process rest of collections
                ProcessLoadOrderServices(loadOrder, dbLoadOrder.Services);
                ProcessLoadOrderEquipments(loadOrder, dbLoadOrder.Equipments);
                ProcessLoadOrderReferences(loadOrder, dbLoadOrder.CustomerReferences);
                ProcessLoadOrderItems(loadOrder, dbLoadOrder.Items);
                ProcessLoadOrderCharges(loadOrder, dbLoadOrder.Charges);
                ProcessLoadOrderNotes(loadOrder, dbLoadOrder.Notes);
                ProcessLoadOrderVendors(loadOrder, dbLoadOrder.Vendors);
                ProcessLoadOrderAccountBuckets(loadOrder, dbLoadOrder.AccountBuckets);
                ProcessLoadOrderDocuments(loadOrder, dbLoadOrder.Documents);

                // historical data handling

                if (historicalData.IsNew || historicalData.HasChanges()) historicalData.Save();

                // commit transaction
                CommitTransaction();

                // refund or void any related misc receipts
                if (_view.RefundOrVoidMiscReceipts)
                    messages = RefundOrVoidMiscReceipts(loadOrder);

                //save addresses
                if (_view.SaveOriginToAddressBook) SaveOriginAddress(loadOrder);
                if (_view.SaveDestinationToAddressBook) SaveDestinationAddress(loadOrder);

                _view.Set(loadOrder);

                messages.Add(ValidationMessage.Information(ProcessorVars.RecordSaveMsg));
                _view.DisplayMessages(messages);
            }
            catch (Exception ex)
            {
                RollBackTransaction();

                _view.LogException(ex);

                messages.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
                _view.DisplayMessages(messages);
            }
        }

        private void OnDelete(object sender, ViewEventArgs<LoadOrder> e)
        {
            var loadOrder = e.Argument;

            if (loadOrder.IsNew)
            {
                _view.Set(new LoadOrder());
                return;
            }

            // load collections
            loadOrder.LoadCollections();
            var locations = loadOrder.Stops.ToList();
            locations.Add(loadOrder.Origin);
            locations.Add(loadOrder.Destination);
            foreach (var stop in locations) stop.LoadCollections();

            // obtain lock/check lock
            var @lock = loadOrder.ObtainLock(_view.ActiveUser, loadOrder.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            // retrieve historical data for deletion
            var historicalData = loadOrder.FetchHistoricalData();

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                loadOrder.Connection = Connection;
                loadOrder.Transaction = Transaction;

                _loadValidator.Connection = Connection;
                _loadValidator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                if (historicalData != null)
                {
                    historicalData.Connection = Connection;
                    historicalData.Transaction = Transaction;
                }

                _loadValidator.Messages.Clear();
                if (!_loadValidator.CanDeleteLoadOrder(loadOrder))
                {
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_loadValidator.Messages);
                    return;
                }


                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Load Order Number: {1}", AuditLogConstants.Delete, loadOrder.LoadOrderNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                // handle collection deletes
                foreach (var location in locations)
                {
                    location.Connection = Connection;
                    location.Transaction = Transaction;

                    foreach (var contact in location.Contacts)
                    {
                        contact.Connection = Connection;
                        contact.Transaction = Transaction;

                        contact.Delete();
                    }

                    location.Delete();
                }

                foreach (var reference in loadOrder.CustomerReferences)
                {
                    reference.Connection = Connection;
                    reference.Transaction = Transaction;

                    reference.Delete();
                }

                foreach (var item in loadOrder.Items)
                {
                    item.Connection = Connection;
                    item.Transaction = Transaction;

                    item.Delete();
                }

                foreach (var equipment in loadOrder.Equipments)
                {
                    equipment.Connection = Connection;
                    equipment.Transaction = Transaction;

                    equipment.Delete();
                }

                foreach (var service in loadOrder.Services)
                {
                    service.Connection = Connection;
                    service.Transaction = Transaction;

                    service.Delete();
                }

                foreach (var vendor in loadOrder.Vendors)
                {
                    vendor.Connection = Connection;
                    vendor.Transaction = Transaction;

                    vendor.Delete();
                }

                foreach (var accountBucket in loadOrder.AccountBuckets)
                {
                    accountBucket.Connection = Connection;
                    accountBucket.Transaction = Transaction;

                    accountBucket.Delete();
                }

                foreach (var charge in loadOrder.Charges)
                {
                    charge.Connection = Connection;
                    charge.Transaction = Transaction;

                    charge.Delete();
                }

                foreach (var note in loadOrder.Notes)
                {
                    note.Connection = Connection;
                    note.Transaction = Transaction;

                    note.Delete();
                }

                foreach (var document in loadOrder.Documents)
                {
                    document.Connection = Connection;
                    document.Transaction = Transaction;

                    document.Delete();
                }

                // delete historical data
                if(historicalData != null) historicalData.Delete();

                // delete loadOrder and lock
                loadOrder.Delete();

                @lock.Delete();

                CommitTransaction();

                _view.Set(new LoadOrder());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }


        private void OnVendorSearch(object sender, ViewEventArgs<string> e)
        {
            var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplayVendor(vendor == null || !vendor.Active ? new Vendor() : vendor);
            if (vendor == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
            else if (!vendor.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor is not active") });
        }

        private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
        {
            if (!_view.ActiveUser.TenantEmployee)
                FetchCustomerForNonEmployee(e.Argument);
            else
                FetchCustomerForEmployee(e.Argument);
        }


        private void OnUnLock(object sender, ViewEventArgs<LoadOrder> e)
        {
            var request = e.Argument;
            var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }
            @lock.Delete();
        }

        private void OnLock(object sender, ViewEventArgs<LoadOrder> e)
        {
            var request = e.Argument;
            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnLoadAuditLog(object sender, ViewEventArgs<LoadOrder> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.EquipmentTypes = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].EquipmentTypes
                .Where(eq => eq.Active)
                .ToList();
            _view.Services = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].Services;

            _view.DateUnits = ProcessorUtilities.GetAll<DateUnit>();
            _view.NoteTypes = ProcessorUtilities.GetAll<NoteType>();
            _view.ServiceModes = ProcessorUtilities.GetAll<ServiceMode>();
        }


        private void OnSaveConvertToShipment(object sender, ViewEventArgs<LoadOrder, Shipment> e)
        {

            var loadOrder = e.Argument;
            var shipment = e.Argument2;

            // check locks
            if (!loadOrder.IsNew)
            {
                var @lock = loadOrder.RetrieveLock(_view.ActiveUser, loadOrder.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            if (loadOrder.IsNew)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot convert load order that has not been previously saved!") });
                return;
            }

            if (!shipment.IsNew)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot convert load order to existing shipment.  Shipment must be new!") });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var locks = new List<Lock>();
            var miscReceiptsToUpdate = new MiscReceiptSearch().FetchMiscReceiptsRelatedToLoadOrder(loadOrder.Id, _view.ActiveUser.TenantId);
            foreach (var receipt in miscReceiptsToUpdate)
            {
                var receiptLock = receipt.ObtainLock(_view.ActiveUser, receipt.Id);
                if (!receiptLock.IsUserLock(_view.ActiveUser))
                {
                    msgs.Add(ValidationMessage.Error("Misc Receipt for Load Order Number:{0} - {1}", receipt.LoadOrder.LoadOrderNumber, ProcessorVars.UnableToObtainLockErrMsg));
                    continue;
                }
                if (receiptLock.LockIsExpired()) receiptLock.Refresh();
                locks.Add(receiptLock);
            }
            if (msgs.Any())
            {
                foreach (var l in locks) l.Delete();
                _view.DisplayMessages(msgs);
                return;
            }

            // retrieve db loadOrder for collection comparisons ...
            var dbLoadOrder = new LoadOrder(loadOrder.Id, false);
            dbLoadOrder.LoadCollections();
            var dbLocations = dbLoadOrder.Stops.ToList();
            if (!dbLoadOrder.IsNew)
            {
                dbLocations.Add(dbLoadOrder.Origin);
                dbLocations.Add(dbLoadOrder.Destination);
            }
            foreach (var location in dbLocations) location.LoadCollections();

            // retrieve load order historical data
            var historicalData = loadOrder.FetchHistoricalData() ??
                                 new LoadOrderHistoricalData
                                     {
                                         LoadOrder = loadOrder,
                                         WasShipment = true
                                     };
	        if (!historicalData.IsNew) historicalData.TakeSnapShot();
            historicalData.WasShipment = true;

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _shipmentValidator.Connection = Connection;
                _shipmentValidator.Transaction = Transaction;

                _loadValidator.Connection = Connection;
                _loadValidator.Transaction = Transaction;

                shipment.Connection = Connection;
                shipment.Transaction = Transaction;

                loadOrder.Connection = Connection;
                loadOrder.Transaction = Transaction;

                historicalData.Connection = Connection;
                historicalData.Transaction = Transaction;

                // validation
                _shipmentValidator.Messages.Clear();
                if (!_shipmentValidator.HasAllRequiredData(shipment))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_shipmentValidator.Messages);
                    return;
                }

                _loadValidator.Messages.Clear();
                if (!_loadValidator.IsValid(loadOrder))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_loadValidator.Messages);
                    return;
                }

                // saves


                // load order save
                if (loadOrder.HasChanges())
                {
                    foreach (var change in loadOrder.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    loadOrder.Save();
                }

                // load process rest of collections
                ProcessLoadOrderLocations(loadOrder, dbLocations);
                ProcessLoadOrderServices(loadOrder, dbLoadOrder.Services);
                ProcessLoadOrderEquipments(loadOrder, dbLoadOrder.Equipments);
                ProcessLoadOrderReferences(loadOrder, dbLoadOrder.CustomerReferences);
                ProcessLoadOrderItems(loadOrder, dbLoadOrder.Items);
                ProcessLoadOrderCharges(loadOrder, dbLoadOrder.Charges);
                ProcessLoadOrderNotes(loadOrder, dbLoadOrder.Notes);
                ProcessLoadOrderVendors(loadOrder, dbLoadOrder.Vendors);
                ProcessLoadOrderAccountBuckets(loadOrder, dbLoadOrder.AccountBuckets);
                ProcessLoadOrderDocuments(loadOrder, dbLoadOrder.Documents);


                // shipment save
                shipment.Save();

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Shipment: {1}", AuditLogConstants.AddedNew, shipment.ShipmentNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                ProcessMiscReceipts(miscReceiptsToUpdate, shipment.Id);

                // locations
                ProcessShipmentLocations(shipment);

                // resave to ensure location id saved
                shipment.Save();

                // process rest of collections
                ProcessShipmentServices(shipment);
                ProcessShipmentEquipments(shipment);
                ProcessShipmentReferences(shipment);
                ProcessShipmentItems(shipment);
                ProcessShipmentCharges(shipment);
                ProcessShipmentNotes(shipment);
                ProcessShipmentVendors(shipment);
                ProcessShipmentAccountBuckets(shipment);
                ProcessShipmentDocuments(shipment);

                // create initial historical data for shipment
                new ShipmentHistoricalData
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    ShipmentId = shipment.Id,
                    WasInDispute = shipment.InDispute,
                    WasInDisputeReason = shipment.InDisputeReason,
                    OriginalEstimatedDeliveryDate = shipment.EstimatedDeliveryDate,
                    OriginalEstimatedPickupDate = shipment.DesiredPickupDate,
                }.Save();


                // save historical data
                if(historicalData.IsNew || historicalData.HasChanges()) historicalData.Save();

                // commit transaction
                CommitTransaction();

                _view.FinalizeConvertToShipment(shipment);
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }


        private void FetchCustomerForNonEmployee(string customerNumber)
        {
            if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
            {
                _view.DisplayCustomer(_view.ActiveUser.DefaultCustomer, true);
                return;
            }
            var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
            var customer = shipAs == null || !shipAs.Customer.Active ? null : shipAs.Customer;
            _view.DisplayCustomer(customer, true);
            if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
            else if (!shipAs.Customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
        }

        private void FetchCustomerForEmployee(string customerNumber)
        {
            var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

            var customer1 = customer == null || !customer.Active ? new Customer() : customer;
            _view.DisplayCustomer(customer1, true);
            if (customer == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
            else if (!customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
        }

        private void ProcessLoadOrderVendors(LoadOrder loadOrder, IEnumerable<LoadOrderVendor> dbVendors)
        {
            var dbDelete = dbVendors.Where(dbv => !loadOrder.Vendors.Select(v => v.Id).Contains(dbv.Id));

            foreach (var sv in dbDelete)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Vendor Number:{3}",
                                                AuditLogConstants.Delete, sv.EntityName(), loadOrder.LoadOrderNumber, sv.Vendor.VendorNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                sv.Delete();
            }

            foreach (var sv in loadOrder.Vendors)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                if (sv.IsNew)
                {
                    sv.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Vendor Number:{3}",
                                                    AuditLogConstants.AddedNew, sv.EntityName(), loadOrder.LoadOrderNumber, sv.Vendor.VendorNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (sv.HasChanges())
                {
                    foreach (var change in sv.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Load Order Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    sv.Save();
                }
            }
        }

        private void ProcessLoadOrderAccountBuckets(LoadOrder loadOrder, IEnumerable<LoadOrderAccountBucket> dbAccountBuckets)
        {
            var dbDelete = dbAccountBuckets.Where(dbab => !loadOrder.AccountBuckets.Select(v => v.Id).Contains(dbab.Id));

            foreach (var qab in dbDelete)
            {
                qab.Connection = Connection;
                qab.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Account Bucket Code:{3}",
                                                AuditLogConstants.Delete, qab.EntityName(), loadOrder.LoadOrderNumber, qab.AccountBucket.Code),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                qab.Delete();
            }

            foreach (var qab in loadOrder.AccountBuckets)
            {
                qab.Connection = Connection;
                qab.Transaction = Transaction;

                if (qab.IsNew)
                {
                    qab.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Account Bucket Code:{3}",
                                                    AuditLogConstants.AddedNew, qab.EntityName(), loadOrder.LoadOrderNumber, qab.AccountBucket.Code),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (qab.HasChanges())
                {
                    foreach (var change in qab.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Load Order Vendor's Account Bucket Code:{1} {2}", qab.EntityName(), qab.AccountBucket.Code, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    qab.Save();
                }
            }
        }

        private void ProcessLoadOrderNotes(LoadOrder loadOrder, IEnumerable<LoadOrderNote> dbNotes)
        {
            var dbDelete = dbNotes.Where(dbn => !loadOrder.Notes.Select(n => n.Id).Contains(dbn.Id));

            foreach (var note in dbDelete)
            {
                note.Connection = Connection;
                note.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Note Ref#:{3}",
                                                AuditLogConstants.Delete, note.EntityName(), loadOrder.LoadOrderNumber, note.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                note.Delete();
            }

            foreach (var note in loadOrder.Notes)
            {
                note.Connection = Connection;
                note.Transaction = Transaction;

                if (note.IsNew)
                {
                    note.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Note Ref#:{3}",
                                                    AuditLogConstants.AddedNew, note.EntityName(), loadOrder.LoadOrderNumber, note.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (note.HasChanges())
                {
                    foreach (var change in note.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("{0} Note Ref#:{1} {2}", note.EntityName(), note.Id, change),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = loadOrder.EntityName(),
                                EntityId = loadOrder.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                    note.Save();
                }
            }
        }

        private void ProcessLoadOrderCharges(LoadOrder loadOrder, IEnumerable<LoadOrderCharge> dbCharges)
        {
            var dbDelete = dbCharges.Where(dbc => !loadOrder.Charges.Select(c => c.Id).Contains(dbc.Id));

            foreach (var charge in dbDelete)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Charge Ref#:{3}",
                                                AuditLogConstants.Delete, charge.EntityName(), loadOrder.LoadOrderNumber, charge.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                charge.Delete();
            }

            foreach (var charge in loadOrder.Charges)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                if (charge.IsNew)
                {
                    charge.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Charge Ref#:{3}",
                                                    AuditLogConstants.AddedNew, charge.EntityName(), loadOrder.LoadOrderNumber, charge.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (charge.HasChanges())
                {
                    foreach (var change in charge.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Charge Ref#:{1} {2}", charge.EntityName(), charge.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    charge.Save();
                }
            }
        }

        private void ProcessLoadOrderItems(LoadOrder loadOrder, IEnumerable<LoadOrderItem> dbItems)
        {
            var dbDelete = dbItems.Where(dbi => !loadOrder.Items.Select(i => i.Id).Contains(dbi.Id));

            foreach (var i in dbDelete)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Item Ref#:{3}",
                                                AuditLogConstants.Delete, i.EntityName(), loadOrder.LoadOrderNumber, i.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                i.Delete();
            }

            foreach (var i in loadOrder.Items)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                if (i.IsNew)
                {
                    i.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Item Ref#:{3}",
                                                    AuditLogConstants.AddedNew, i.EntityName(), loadOrder.LoadOrderNumber, i.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (i.HasChanges())
                {
                    foreach (var change in i.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Item Ref#:{1} {2}", i.EntityName(), i.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    i.Save();
                }
            }
        }

        private void ProcessLoadOrderReferences(LoadOrder loadOrder, IEnumerable<LoadOrderReference> dbReferences)
        {
            var dbDelete = dbReferences.Where(dbr => !loadOrder.CustomerReferences.Select(r => r.Id).Contains(dbr.Id));

            foreach (var r in dbDelete)
            {
                r.Connection = Connection;
                r.Transaction = Transaction;

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Customer Reference:{3}",
                                                    AuditLogConstants.Delete, r.EntityName(), loadOrder.LoadOrderNumber, r.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                r.Delete();
            }

            foreach (var r in loadOrder.CustomerReferences)
            {
                r.Connection = Connection;
                r.Transaction = Transaction;

                if (r.IsNew)
                {
                    r.Save();

                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Load Order Number:{2} Customer Reference:{3}",
                                                        AuditLogConstants.AddedNew, r.EntityName(), loadOrder.LoadOrderNumber, r.Name),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (r.HasChanges())
                {
                    foreach (var change in r.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("{0} Customer Reference:{1} {2}", r.EntityName(), r.Name, change),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = loadOrder.EntityName(),
                                EntityId = loadOrder.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                    r.Save();
                }
            }
        }

        private void ProcessLoadOrderEquipments(LoadOrder loadOrder, IEnumerable<LoadOrderEquipment> dbEquipments)
        {
            var dbDelete = dbEquipments.Where(dbe => !loadOrder.Equipments.Select(e => e.EquipmentTypeId).Contains(dbe.EquipmentTypeId));

            foreach (var s in dbDelete)
            {
                s.Connection = Connection;
                s.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Load Order Number:{2} Equipment Type Ref#: {3}", AuditLogConstants.Delete, s.EntityName(),
                                      loadOrder.LoadOrderNumber, s.EquipmentTypeId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                s.Delete();
            }

            foreach (var equipment in loadOrder.Equipments)
                if (!_loadValidator.LoadOrderEquipmentExists(equipment))
                {
                    equipment.Connection = Connection;
                    equipment.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Load Order Number:{2} Equipment Type Ref#:{3}", AuditLogConstants.AddedNew,
                                          equipment.EntityName(), loadOrder.LoadOrderNumber, equipment.EquipmentTypeId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    equipment.Save();
                }
        }

        private void ProcessLoadOrderServices(LoadOrder loadOrder, IEnumerable<LoadOrderService> dbServices)
        {
            var dbDelete = dbServices.Where(dbs => !loadOrder.Services.Select(s => s.ServiceId).Contains(dbs.ServiceId));

            foreach (var s in dbDelete)
            {
                s.Connection = Connection;
                s.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Load Order Number:{2} Service Ref#: {3}", AuditLogConstants.Delete, s.EntityName(),
                                      loadOrder.LoadOrderNumber, s.ServiceId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                s.Delete();
            }

            foreach (var service in loadOrder.Services)
                if (!_loadValidator.LoadOrderServiceExists(service))
                {
                    service.Connection = Connection;
                    service.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Load Order Number:{2} Service Ref#:{3}", AuditLogConstants.AddedNew,
                                          service.EntityName(),
                                          loadOrder.LoadOrderNumber, service.ServiceId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    service.Save();
                }
        }

        private void ProcessLoadOrderLocations(LoadOrder loadOrder, List<LoadOrderLocation> dbLoadOrderLocations)
        {
            var locations = loadOrder.Stops.ToList();
            locations.Add(loadOrder.Origin);
            locations.Add(loadOrder.Destination);

            var dbDelete = dbLoadOrderLocations.Where(l => !locations.Select(cl => cl.Id).Contains(l.Id));

            foreach (var l in dbDelete)
            {
                foreach (var c in l.Contacts)
                {
                    c.Connection = Connection;
                    c.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                    AuditLogConstants.Delete, c.EntityName(), loadOrder.LoadOrderNumber, l.Id, c.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    c.Delete();
                }

                l.Connection = Connection;
                l.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3}",
                                                AuditLogConstants.Delete, l.EntityName(), loadOrder.LoadOrderNumber, l.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                l.Delete();
            }

            foreach (var l in locations)
            {
                l.Connection = Connection;
                l.Transaction = Transaction;

                if (l.IsNew)
                {
                    l.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3}",
                                                    AuditLogConstants.AddedNew, l.EntityName(), loadOrder.LoadOrderNumber, l.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (l.HasChanges())
                {
                    foreach (var change in l.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Ref#:{1} {2}", l.EntityName(), l.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    l.Save();
                }

                ProcessLoadOrderContacts(loadOrder, l, dbLoadOrderLocations.FirstOrDefault(dbl => dbl.Id == l.Id));
            }
        }

        private void ProcessLoadOrderContacts(LoadOrder loadOrder, LoadOrderLocation location, LoadOrderLocation dbLocation)
        {
            var dbDelete = dbLocation != null
                            ? dbLocation.Contacts.Where(l => !location.Contacts.Select(cl => cl.Id).Contains(l.Id))
                            : new List<LoadOrderContact>();

            foreach (var c in dbDelete)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                AuditLogConstants.Delete, c.EntityName(), loadOrder.LoadOrderNumber, location.Id, c.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                c.Delete();
            }

            foreach (var c in location.Contacts)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                if (c.IsNew)
                {
                    c.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                    AuditLogConstants.AddedNew, c.EntityName(), loadOrder.LoadOrderNumber, location.Id, c.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (c.HasChanges())
                {
                    foreach (var change in c.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("Location Ref#{0} {1} {2}", location.Id, c.EntityName(), change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    c.Save();
                }
            }
        }

        private void ProcessLoadOrderDocuments(LoadOrder loadOrder, IEnumerable<LoadOrderDocument> dbDocuments)
        {
            var dbDelete = dbDocuments.Where(dbs => !loadOrder.Documents.Select(s => s.Id).Contains(dbs.Id));

            foreach (var d in dbDelete)
            {
                d.Connection = Connection;
                d.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Load Order Number:{2} Document Name: {3}", AuditLogConstants.Delete, d.EntityName(),
                                      loadOrder.LoadOrderNumber, d.Name),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                d.Delete();
            }

            foreach (var d in loadOrder.Documents)
            {
                d.Connection = Connection;
                d.Transaction = Transaction;

                if (d.IsNew)
                {
                    d.Save();
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} {1} Load Order Number:{2} Document Name:{3}",
                                                        AuditLogConstants.AddedNew, d.EntityName(), loadOrder.LoadOrderNumber, d.Name),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (d.HasChanges())
                {
                    foreach (var change in d.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("{0} Document Name:{1} {2}", d.EntityName(), d.Name, change),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = loadOrder.EntityName(),
                                EntityId = loadOrder.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                    d.Save();
                }
            }
        }
        

        private void SaveOriginAddress(LoadOrder loadOrder)
        {
            // build
            var addressBook = new AddressBook
            {
                City = loadOrder.Origin.City,
                State = loadOrder.Origin.State,
                CountryId = loadOrder.Origin.CountryId,
                Street1 = loadOrder.Origin.Street1,
                Street2 = loadOrder.Origin.Street2,
                Customer = loadOrder.Customer,
                Description = loadOrder.Origin.Description,
                OriginSpecialInstruction = loadOrder.Origin.SpecialInstructions,
                Direction = loadOrder.Origin.Direction,
                GeneralInfo = loadOrder.Origin.GeneralInfo,
                DestinationSpecialInstruction = string.Empty,
                PostalCode = loadOrder.Origin.PostalCode,
                TenantId = loadOrder.Origin.TenantId,
                Contacts = new List<AddressBookContact>(),
                Services = new List<AddressBookService>()
            };

            addressBook.Contacts.AddRange(
                loadOrder.Origin.Contacts.Select(c =>
                                                new AddressBookContact
                                                {
                                                    TenantId = c.TenantId,
                                                    AddressBook = addressBook,
                                                    Comment = c.Comment,
                                                    ContactTypeId = c.ContactTypeId,
                                                    Email = c.Email,
                                                    Fax = c.Fax,
                                                    Phone = c.Phone,
                                                    Mobile = c.Mobile,
                                                    Name = c.Name,
                                                    Primary = c.Primary,
                                                }));

            SaveAddressBook(addressBook);
        }

        private void SaveDestinationAddress(LoadOrder loadOrder)
        {
            // build
            var addressBook = new AddressBook
            {
                City = loadOrder.Destination.City,
                State = loadOrder.Destination.State,
                CountryId = loadOrder.Destination.CountryId,
                Street1 = loadOrder.Destination.Street1,
                Street2 = loadOrder.Destination.Street2,
                Customer = loadOrder.Customer,
                Description = loadOrder.Destination.Description,
                DestinationSpecialInstruction = loadOrder.Destination.SpecialInstructions,
                OriginSpecialInstruction = string.Empty,
                Direction = loadOrder.Destination.Direction,
                GeneralInfo = loadOrder.Destination.GeneralInfo,
                PostalCode = loadOrder.Destination.PostalCode,
                TenantId = loadOrder.Destination.TenantId,
                Contacts = new List<AddressBookContact>(),
                Services = new List<AddressBookService>()
            };
            addressBook.Contacts.AddRange(
                loadOrder.Destination.Contacts.Select(c =>
                                                new AddressBookContact
                                                {
                                                    TenantId = c.TenantId,
                                                    AddressBook = addressBook,
                                                    Comment = c.Comment,
                                                    ContactTypeId = c.ContactTypeId,
                                                    Email = c.Email,
                                                    Fax = c.Fax,
                                                    Phone = c.Phone,
                                                    Mobile = c.Mobile,
                                                    Name = c.Name,
                                                    Primary = c.Primary,
                                                }));

            SaveAddressBook(addressBook);
        }

        private void SaveAddressBook(AddressBook addressBook)
        {
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // save and audit log
                addressBook.Connection = Connection;
                addressBook.Transaction = Transaction;
                addressBook.Save();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Ref #:{1}", AuditLogConstants.AddedNew, addressBook.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = addressBook.EntityName(),
                    EntityId = addressBook.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                // contacts
                foreach (var contact in addressBook.Contacts)
                {
                    contact.Connection = Connection;
                    contact.Transaction = Transaction;

                    contact.AddressBook = addressBook;

                    contact.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Contact Name: {2} ", AuditLogConstants.AddedNew,
                                                    contact.EntityName(), contact.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = addressBook.EntityName(),
                        EntityId = addressBook.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }

                // commit
                CommitTransaction();
            }
            catch
            {
                RollBackTransaction();
            }
        }




        private void ProcessShipmentVendors(Shipment shipment)
        {
            foreach (var sv in shipment.Vendors)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                sv.Save();
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Vendor Number:{3}",
                                                AuditLogConstants.AddedNew, sv.EntityName(), shipment.ShipmentNumber, sv.Vendor.VendorNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
            }
        }

        private void ProcessShipmentAccountBuckets(Shipment shipment)
        {
            foreach (var sab in shipment.AccountBuckets)
            {
                sab.Connection = Connection;
                sab.Transaction = Transaction;

                sab.Save();
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Account Bucket Code:{3}",
                                                AuditLogConstants.AddedNew, sab.EntityName(), shipment.ShipmentNumber, sab.AccountBucket.Code),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
            }
        }

        private void ProcessShipmentNotes(Shipment shipment)
        {
            foreach (var note in shipment.Notes)
            {
                note.Connection = Connection;
                note.Transaction = Transaction;

                note.Save();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Note Ref#:{3}",
                                                AuditLogConstants.AddedNew, note.EntityName(), shipment.ShipmentNumber, note.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
            }
        }

        private void ProcessShipmentCharges(Shipment shipment)
        {
            foreach (var charge in shipment.Charges)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                charge.Save();
                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Charge Ref#:{3}",
                                                    AuditLogConstants.AddedNew, charge.EntityName(),
                                                    shipment.ShipmentNumber, charge.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
            }
        }

        private void ProcessShipmentItems(Shipment shipment)
        {
            foreach (var i in shipment.Items)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                i.Save();
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Item Ref#:{3}",
                                                AuditLogConstants.AddedNew, i.EntityName(), shipment.ShipmentNumber, i.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
            }
        }

        private void ProcessShipmentReferences(Shipment shipment)
        {
            foreach (var r in shipment.CustomerReferences)
            {
                r.Connection = Connection;
                r.Transaction = Transaction;

                r.Save();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Customer Reference:{3}",
                                                AuditLogConstants.AddedNew, r.EntityName(), shipment.ShipmentNumber, r.Name),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
            }
        }

        private void ProcessShipmentEquipments(Shipment shipment)
        {
            foreach (var equipment in shipment.Equipments.Where(equipment => !_shipmentValidator.ShipmentEquipmentExists(equipment)))
            {
                equipment.Connection = Connection;
                equipment.Transaction = Transaction;

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Shipment Number:{2} Equipment Type Ref#:{3}", AuditLogConstants.AddedNew,
                                          equipment.EntityName(), shipment.ShipmentNumber, equipment.EquipmentTypeId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                equipment.Save();
            }
        }

        private void ProcessShipmentServices(Shipment shipment)
        {
            foreach (var service in shipment.Services.Where(service => !_shipmentValidator.ShipmentServiceExists(service)))
            {
                service.Connection = Connection;
                service.Transaction = Transaction;

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Shipment Number:{2} Service Ref#:{3}", AuditLogConstants.AddedNew,
                                          service.EntityName(),
                                          shipment.ShipmentNumber, service.ServiceId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                service.Save();
            }
        }

        private void ProcessShipmentLocations(Shipment shipment)
        {
            var locations = shipment.Stops.ToList();
            locations.Add(shipment.Origin);
            locations.Add(shipment.Destination);


            foreach (var l in locations)
            {
                l.Connection = Connection;
                l.Transaction = Transaction;

                l.Save();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3}",
                                                AuditLogConstants.AddedNew, l.EntityName(), shipment.ShipmentNumber, l.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                ProcessShipmentContacts(shipment, l);
            }
        }

        private void ProcessShipmentContacts(Shipment shipment, ShipmentLocation location)
        {
            foreach (var c in location.Contacts)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                c.Save();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                AuditLogConstants.AddedNew, c.EntityName(), shipment.ShipmentNumber, location.Id, c.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
            }
        }

        private void ProcessShipmentDocuments(Shipment shipment)
        {
            foreach (var d in shipment.Documents)
            {
                d.Connection = Connection;
                d.Transaction = Transaction;

                d.Save();
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Document Ref#:{3}",
                                                AuditLogConstants.AddedNew, d.EntityName(), shipment.ShipmentNumber, d.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
            }
        }


        private bool ProcessCustomerShipperBOL(LoadOrder loadOrder)
        {
            var @lock = loadOrder.Customer.ObtainLock(_view.ActiveUser, loadOrder.Customer.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return false;
            }

            // change customer transaction
            loadOrder.Customer.Connection = Connection;
            loadOrder.Customer.Transaction = Transaction;

            @lock.Connection = Connection;
            @lock.Transaction = Transaction;

            // set shipper bol and increment customer seed
            loadOrder.Customer.TakeSnapShot();
            loadOrder.ShipperBol = string.Format("{0}{1}{2}", loadOrder.Customer.ShipperBillOfLadingPrefix,
                                                ++loadOrder.Customer.ShipperBillOfLadingSeed,
                                                loadOrder.Customer.ShipperBillOfLadingSuffix);

            // audit log customer
            foreach (var change in loadOrder.Customer.Changes())
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} on Load Order [{1}]", change, loadOrder.LoadOrderNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.Customer.EntityName(),
                    EntityId = loadOrder.Customer.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

            // save customer
            loadOrder.Customer.Save();

            // remove customer lock
            @lock.Delete();

            return true;
        }

        private bool PurchaseOrderNumberIsValid(LoadOrder loadOrder)
        {
            var purchaseOrder = new CustomerPurchaseOrderSearch()
                .FetchCustomerPurchaseOrder(loadOrder.TenantId, loadOrder.Customer.Id, loadOrder.PurchaseOrderNumber);

            if (purchaseOrder == null)
            {
                var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
                              ? ProcessorVars.MissingPOErrMsg
                              : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;

                _view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
                return false;
            }

            var @lock = purchaseOrder.ObtainLock(_view.ActiveUser, loadOrder.Customer.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return false;
            }

            if (purchaseOrder.ExpirationDate.TimeToMaximum() < DateTime.Now.TimeToMaximum())
            {
                @lock.Delete();
                var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
                              ? string.Format(ProcessorVars.ExpiredPOErrMsgDateFormat, purchaseOrder.ExpirationDate.FormattedShortDateAlt())
                              : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
                _view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
                return false;
            }

			if (purchaseOrder.ApplyOnOrigin && !(purchaseOrder.IsValidPostalCodeFormat(loadOrder.Origin.PostalCode) && purchaseOrder.CountryId == loadOrder.Origin.CountryId))
            {
                @lock.Delete();
                var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
                            ? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "origin")
                            : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
                _view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
                return false;
            }

			if (purchaseOrder.ApplyOnDestination && !(purchaseOrder.IsValidPostalCodeFormat(loadOrder.Destination.PostalCode) && purchaseOrder.CountryId == loadOrder.Destination.CountryId))
            {
                @lock.Delete();
                var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
                            ? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "destination")
                            : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
                _view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
                return false;
            }

            purchaseOrder.TakeSnapShot();

            purchaseOrder.Connection = Connection;
            purchaseOrder.Transaction = Transaction;

            @lock.Connection = Connection;
            @lock.Transaction = Transaction;

            if (++purchaseOrder.CurrentUses > purchaseOrder.MaximumUses)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.MaxUsePOErrMsgFormat, purchaseOrder.MaximumUses) });
                return false;
            }


            foreach (var change in purchaseOrder.Changes())
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} for load order [{1}]", change, loadOrder.LoadOrderNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = purchaseOrder.EntityName(),
                    EntityId = purchaseOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

            // save purchase order
            purchaseOrder.Save();

            // remove lock
            @lock.Delete();

            return true;
        }


        private void ProcessMiscReceipts(IEnumerable<MiscReceipt> miscReceiptsToUpdate, long shipmentId)
        {
            foreach (var miscReceipt in miscReceiptsToUpdate)
            {
                miscReceipt.Connection = Connection;
                miscReceipt.Transaction = Transaction;

                miscReceipt.TakeSnapShot();
                miscReceipt.ShipmentId = shipmentId;

                foreach (var change in miscReceipt.Changes())
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = miscReceipt.EntityName(),
                            EntityId = miscReceipt.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                miscReceipt.Save();
            }
        }


        private List<ValidationMessage> RefundOrVoidMiscReceipts(LoadOrder loadOrder)
        {
            var miscReceipts = new MiscReceiptSearch().FetchMiscReceiptsRelatedToLoadOrder(loadOrder.Id, loadOrder.TenantId);
            var gateway = new Tenant(_view.ActiveUser.TenantId).GetPaymentGatewayService();
            var messages = new List<ValidationMessage>();

            _mrValidator.Messages.Clear();
            var transactionId = string.Empty;


            foreach (var miscReceipt in miscReceipts.Where(mr => !mr.Reversal))
            {
                if (miscReceipt.PaymentGatewayType != miscReceipt.Tenant.PaymentGatewayType && miscReceipt.PaymentGatewayType != PaymentGatewayType.NotApplicable && miscReceipt.PaymentGatewayType != PaymentGatewayType.Check)
                {
                    messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptErrorPaymentGatewayForTenantChanged, miscReceipt.AmountPaid));
                    continue;
                }

                var refundOrVoidReceipt = new MiscReceipt
                {
                    AmountPaid = miscReceipt.AmountLeftToBeAppliedOrRefunded(miscReceipt.TenantId),
                    CustomerId = miscReceipt.CustomerId,
                    GatewayTransactionId = string.Empty,
                    PaymentGatewayType = miscReceipt.PaymentGatewayType,
                    PaymentDate = DateTime.Now,
                    Reversal = true,
                    ShipmentId = miscReceipt.ShipmentId,
                    LoadOrderId = miscReceipt.LoadOrderId,
                    TenantId = miscReceipt.TenantId,
                    UserId = miscReceipt.UserId,
                    OriginalMiscReceiptId = miscReceipt.Id,
                    MiscReceiptApplications = new List<MiscReceiptApplication>(),
                    NameOnCard = miscReceipt.NameOnCard,
                    PaymentProfileId = string.Empty
                };

                // do not refund a receipt that was already fully applied or refunded
                if(refundOrVoidReceipt.AmountPaid <= 0) continue;

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _mrValidator.Connection = Connection;
                    _mrValidator.Transaction = Transaction;

                    refundOrVoidReceipt.Connection = Connection;
                    refundOrVoidReceipt.Transaction = Transaction;

                    if (!_mrValidator.IsValid(refundOrVoidReceipt))
                    {
                        messages.AddRange(_mrValidator.Messages);
                        RollBackTransaction();
                        continue;
                    }

                    refundOrVoidReceipt.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Misc Receipt Shipment #:{1} Load Order #:{2} Customer:{3} Amount:{4}",
                                            AuditLogConstants.AddedNew,
                                            refundOrVoidReceipt.ShipmentId != default(long)
                                                ? refundOrVoidReceipt.Shipment.ShipmentNumber
                                                : string.Empty,
                                            refundOrVoidReceipt.LoadOrderId != default(long)
                                                ? refundOrVoidReceipt.LoadOrder.LoadOrderNumber
                                                : string.Empty,
                                            refundOrVoidReceipt.Customer.CustomerNumber,
                                            refundOrVoidReceipt.AmountPaid),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = refundOrVoidReceipt.EntityName(),
                        EntityId = refundOrVoidReceipt.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    if (refundOrVoidReceipt.PaymentGatewayType == PaymentGatewayType.NotApplicable || refundOrVoidReceipt.PaymentGatewayType == PaymentGatewayType.Check)
                    {
                        CommitTransaction();
                        messages.Add(ValidationMessage.Information(ProcessorVars.RefundSuccessfullyProcessedMessage));
                        continue;
                    }

                    switch (miscReceipt.GetTransactionStatusViaPaymentGateway())
                    {
                        case TransactionStatus.CommunicationError:
                        case TransactionStatus.ApprovedReview:
                        case TransactionStatus.GeneralError:
                        case TransactionStatus.FailedReview:
                        case TransactionStatus.SettlementError:
                        case TransactionStatus.UnderReview:
                        case TransactionStatus.FdsPendingReview:
                        case TransactionStatus.FdsAuthorizedPendingReview:
                        case TransactionStatus.ReturnedItem:
                        case TransactionStatus.NotApplicable:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionNotApplicable, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.Declined:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionDeclined, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.Expired:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionExpired, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.Voided:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionVoided, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.CouldNotVoid:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionCouldNotVoid, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.RefundSettledSuccessfully:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionRefundSettledSuccessfully, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.RefundPendingSettlement:
                            RollBackTransaction();
                            messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptTransactionRefundPendingSettlement, miscReceipt.AmountPaid));
                            break;
                        case TransactionStatus.AuthorizedPendingCapture:
                        case TransactionStatus.CapturedPendingSettlement:
                            var voidResponse = gateway.Void(miscReceipt.GatewayTransactionId);
                            if (voidResponse.Approved)
                            {
                                refundOrVoidReceipt.TakeSnapShot();
                                refundOrVoidReceipt.GatewayTransactionId = voidResponse.TransactionId;
                                foreach (var change in refundOrVoidReceipt.Changes())
                                    new AuditLog
                                    {
                                        Connection = Connection,
                                        Transaction = Transaction,
                                        Description = change,
                                        TenantId = _view.ActiveUser.TenantId,
                                        User = _view.ActiveUser,
                                        EntityCode = refundOrVoidReceipt.EntityName(),
                                        EntityId = refundOrVoidReceipt.Id.ToString(),
                                        LogDateTime = DateTime.Now
                                    }.Log();
                                refundOrVoidReceipt.Save();
                                CommitTransaction();
                                messages.Add(ValidationMessage.Information(ProcessorVars.VoidSuccessfullyProcessedMessage));
                            }
                            else
                            {
                                RollBackTransaction();
                                messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptErrorVoidingTransaction, refundOrVoidReceipt.AmountPaid, voidResponse.ErrorMessage));
                            }
                            break;
                        case TransactionStatus.SettledSuccessfully:
                            var refundResponse = gateway.Refund(miscReceipt.GatewayTransactionId, refundOrVoidReceipt.AmountPaid);
                            if (refundResponse.Approved)
                            {
                                refundOrVoidReceipt.TakeSnapShot();
                                transactionId = refundResponse.TransactionId;
                                refundOrVoidReceipt.GatewayTransactionId = refundResponse.TransactionId;
                                foreach (var change in refundOrVoidReceipt.Changes())
                                    new AuditLog
                                    {
                                        Connection = Connection,
                                        Transaction = Transaction,
                                        Description = change,
                                        TenantId = _view.ActiveUser.TenantId,
                                        User = _view.ActiveUser,
                                        EntityCode = refundOrVoidReceipt.EntityName(),
                                        EntityId = refundOrVoidReceipt.Id.ToString(),
                                        LogDateTime = DateTime.Now
                                    }.Log();
                                refundOrVoidReceipt.Save();
                                CommitTransaction();
                                messages.Add(ValidationMessage.Information(ProcessorVars.RefundSuccessfullyProcessedMessage));
                            }
                            else
                            {
                                RollBackTransaction();
                                messages.Add(ValidationMessage.Information(ProcessorVars.MiscReceiptErrorRefundingTransaction, miscReceipt.AmountPaid, refundResponse.ErrorMessage));
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    var msgs = new List<ValidationMessage>();
                    if (!string.IsNullOrEmpty(transactionId))
                    {
                        try
                        {
                            gateway.Void(transactionId);
                        }
                        catch (Exception gException)
                        {
                            msgs.Add(ValidationMessage.Information(gException.Message));
                        }
                    }
                    RollBackTransaction();
                    msgs.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
                    _view.LogException(ex);
                }
            }

            return messages;
        }
    }
}
