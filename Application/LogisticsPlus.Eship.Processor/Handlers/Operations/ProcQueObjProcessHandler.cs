﻿using System;
using LogisticsPlus.Eship.Core;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class ProcQueObjProcessHandler : EntityBase
	{
		public bool Save(ProcQueObj procQueObj, out Exception ex)
		{
			if (!procQueObj.DateCreated.IsValidSystemDateTime())
			{
				ex = null;
				return false;
			}

			try
			{
				procQueObj.Save();
			}
			catch (Exception e)
			{
				ex = e;
				return false;
			}
			ex = null;
			return true;
		}

		public bool Delete(ProcQueObj procQueObj, out Exception ex)
		{
			try
			{
				procQueObj.Delete();
			}
			catch (Exception e)
			{
				ex = e;
				return false;
			}
			ex = null;
			return true;
		}
	}
}
