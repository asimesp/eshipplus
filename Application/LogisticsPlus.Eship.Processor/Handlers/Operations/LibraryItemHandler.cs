﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class LibraryItemHandler : EntityBase
    {
        private readonly ILibraryItemView _view;

        private readonly LibraryItemValidator _validator = new LibraryItemValidator();

        public LibraryItemHandler(ILibraryItemView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.Lock += OnLock;
			_view.UnLock += OnUnLock;
            _view.BatchImport += OnBatchImport;
            _view.CustomerSearch += OnCustomerSearch;
		}

        private void OnBatchImport(object sender, ViewEventArgs<List<LibraryItem>> e)
        {
            var messages = new ValidationMessageCollection();

            foreach (var libraryItem in e.Argument)
            {
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    libraryItem.Connection = Connection;
                    libraryItem.Transaction = Transaction;

                    _validator.Messages.Clear();
                    if (!_validator.IsValid(libraryItem))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }


                    libraryItem.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, libraryItem.Description),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = libraryItem.EntityName(),
                        EntityId = libraryItem.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    CommitTransaction();
                }
                catch(Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
                }
            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }

        private void OnUnLock(object sender, ViewEventArgs<LibraryItem> e)
        {
            var libraryItem = e.Argument;
            var @lock = libraryItem.RetrieveLock(_view.ActiveUser, libraryItem.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<LibraryItem> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnSearch(object sender, ViewEventArgs<LibraryItemSearchCriteria> e)
        {
            var results = new LibraryItemSearch()
                .FetchLibraryItems(e.Argument, _view.ActiveUser.TenantId);

            _view.DisplaySearchResult(results);
        }

        private void OnDelete(object sender, ViewEventArgs<LibraryItem> e)
        {
            _validator.Messages.Clear();

            var libraryItem = e.Argument;

            if (libraryItem.IsNew) return;

            var @lock = libraryItem.ObtainLock(_view.ActiveUser, libraryItem.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                libraryItem.Connection = Connection;
                libraryItem.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _validator.Messages.Clear();
               
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Ref#: {2}", AuditLogConstants.Delete, libraryItem.Description, libraryItem.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = libraryItem.EntityName(),
                    EntityId = libraryItem.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                libraryItem.Delete();
                @lock.Delete();

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSave(object sender, ViewEventArgs<LibraryItem> e)
        {
            var libraryItem = e.Argument;

            if (!libraryItem.IsNew)
            {
                var @lock = libraryItem.RetrieveLock(_view.ActiveUser, libraryItem.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                libraryItem.Connection = Connection;
                libraryItem.Transaction = Transaction;

                if (!_validator.IsValid(libraryItem))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (libraryItem.IsNew)
                {
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, libraryItem.Description),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = libraryItem.EntityName(),
                        EntityId = libraryItem.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    libraryItem.Save();
                }
                else if (libraryItem.HasChanges())
                {
                    foreach (var change in libraryItem.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = libraryItem.EntityName(),
                            EntityId = libraryItem.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    libraryItem.Save();
                }

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnCustomerSearch(Object sender, ViewEventArgs<string> e)
        {
            if (!_view.ActiveUser.TenantEmployee)
                FetchCustomerForNonEmployee(e.Argument);
            else
                FetchCustomerForEmployee(e.Argument);
        }

        private void FetchCustomerForNonEmployee(string customerNumber)
        {
            if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
            {
                _view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
                return;
            }
            var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
            if (shipAs == null)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
                _view.DisplayCustomer(new Customer());

            }
            else _view.DisplayCustomer(shipAs.Customer);
        }

        private void FetchCustomerForEmployee(string customerNumber)
        {
            var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

            _view.DisplayCustomer(customer ?? new Customer());
            if (customer == null)
                _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
        }
    }
}
