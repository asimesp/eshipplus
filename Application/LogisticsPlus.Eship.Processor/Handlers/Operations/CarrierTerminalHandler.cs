﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class CarrierTerminalHandler : EntityBase
    {
        private readonly ICarrierTerminalView _view;

		private readonly VendorTerminalValidator _validator = new VendorTerminalValidator();

        public CarrierTerminalHandler(ICarrierTerminalView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.UnLock += OnUnLock;
			_view.Lock += OnLock;
			_view.VendorSearch += OnVendorSearch;
			_view.BatchImport += OnBatchImport;
		}

        private void OnSave(object sender, ViewEventArgs<VendorTerminal> e)
        {
            var vendorTerminal = e.Argument;

            if (!vendorTerminal.IsNew)
            {
                var @lock = vendorTerminal.RetrieveLock(_view.ActiveUser, vendorTerminal.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                vendorTerminal.Connection = Connection;
                vendorTerminal.Transaction = Transaction;

                if (!_validator.IsValid(vendorTerminal))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (vendorTerminal.IsNew)
                {
                    vendorTerminal.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Vendor:{2}-{3}", AuditLogConstants.AddedNew, vendorTerminal.Name,
                                          vendorTerminal.Vendor.VendorNumber, vendorTerminal.Vendor.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = vendorTerminal.EntityName(),
                        EntityId = vendorTerminal.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (vendorTerminal.HasChanges())
                {
                    foreach (var change in vendorTerminal.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = vendorTerminal.EntityName(),
                            EntityId = vendorTerminal.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    vendorTerminal.Save();
                }

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<VendorTerminal> e)
        {
            _validator.Messages.Clear();

            var vendorTerminal = e.Argument;

            if (vendorTerminal.IsNew) return;

            var @lock = vendorTerminal.ObtainLock(_view.ActiveUser, vendorTerminal.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                vendorTerminal.Connection = Connection;
                vendorTerminal.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Vendor:{2}-{3}", AuditLogConstants.Delete, vendorTerminal.Name,
                                      vendorTerminal.Vendor.VendorNumber, vendorTerminal.Vendor.Name),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = vendorTerminal.EntityName(),
                    EntityId = vendorTerminal.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                vendorTerminal.Delete();
                @lock.Delete();

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSearch(object sender, ViewEventArgs<VendorTerminalSearchCriteria> e)
        {
            var results = new VendorTerminalSearch().FetchVendorTerminals(e.Argument, _view.ActiveUser.TenantId);
            
            if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
            _view.DisplaySearchResult(results);
        }

        private void OnUnLock(object sender, ViewEventArgs<VendorTerminal> e)
        {
            var vendorTerminal = e.Argument;
            var @lock = vendorTerminal.RetrieveLock(_view.ActiveUser, vendorTerminal.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<VendorTerminal> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnVendorSearch(object sender, ViewEventArgs<string> e)
        {
            var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);

            _view.DisplayVendor(vendor ?? new Vendor());
            if (vendor == null)
                _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
        }

        private void OnBatchImport(object sender, ViewEventArgs<List<VendorTerminal>> e)
        {
            var messages = new ValidationMessageCollection();

            foreach (var vendorTerminal in e.Argument)
            {
                _validator.Messages.Clear();
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    vendorTerminal.Connection = Connection;
                    vendorTerminal.Transaction = Transaction;

                    if (!_validator.IsValid(vendorTerminal))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }

                    vendorTerminal.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Vendor:{2}-{3}", AuditLogConstants.AddedNew, vendorTerminal.Name,
                                          vendorTerminal.Vendor.VendorNumber, vendorTerminal.Vendor.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = vendorTerminal.EntityName(),
                        EntityId = vendorTerminal.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [Name: {1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, vendorTerminal.Name, ex.Message));
                }
            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }
    }
}
