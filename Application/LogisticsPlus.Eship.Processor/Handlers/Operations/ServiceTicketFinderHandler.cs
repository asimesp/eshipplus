﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class ServiceTicketFinderHandler : EntityBase
	{
		private readonly IServiceTicketFinderView _view;

		public ServiceTicketFinderHandler(IServiceTicketFinderView view) { _view = view; }

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<ServiceTicketViewSearchCriteria> e)
		{
            List<Dto.Operations.ServiceTicketDashboardDto> results = new List<Dto.Operations.ServiceTicketDashboardDto>();

            if (_view.FilterForServiceTicketsToExludeAttachedToJob)
            {
                results = new ServiceTicketSearch().FetchServiceTicketExludeAttachetToJob(e.Argument, _view.ActiveUser.TenantId);
            }
            else if (_view.FilterForServiceTicketsToBeInvoicedOnly)
            {
                results = new ServiceTicketSearch().FetchServiceTicketReadyForInvoicing(e.Argument, _view.ActiveUser.TenantId);
            }
            else
            {
                results = new ServiceTicketSearch().FetchServiceTicketDashboardDtos(e.Argument, _view.ActiveUser.TenantId); ;
            }
            
			_view.DisplaySearchResult(results);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
		}
	}
}
