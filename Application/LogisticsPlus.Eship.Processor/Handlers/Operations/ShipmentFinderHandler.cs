﻿using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class ShipmentFinderHandler : EntityBase
	{
		private readonly IShipmentFinderView _view;

		public ShipmentFinderHandler(IShipmentFinderView view) { _view = view; }

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<ShipmentViewSearchCriteria> e)
		{
            List<ShipmentDashboardDto> results;

            if (_view.FilterForShipmentsToExludeAttachedToJob)
            {
                results = new ShipmentSearch().FetchShipmentsExludeAttachetToJob(e.Argument, _view.ActiveUser.TenantId);
            }
            else if (_view.FilterForShipmentsToBeInvoicedOnly)
            {
                results = new ShipmentSearch().FetchShipmentsToBeInvoiced(e.Argument, _view.ActiveUser.TenantId);
            }
            else
            {
                results = new ShipmentSearch().FetchShipmentsDashboardDto(e.Argument, _view.ActiveUser.TenantId); ;
            }

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}
	}
}
