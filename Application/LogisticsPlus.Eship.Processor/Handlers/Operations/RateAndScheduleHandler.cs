﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class RateAndScheduleHandler : EntityBase
	{
        private const string DemoNumber = "Demo";

        private readonly IRateAndScheduleView _view;
		private readonly RateAndScheduleValidator _validator = new RateAndScheduleValidator();
        private readonly MiscReceiptValidator _mrValidator = new MiscReceiptValidator();
        private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

		public RateAndScheduleHandler(IRateAndScheduleView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.CustomerSearch += OnCustomerSearch;
			_view.SaveShoppedRate += OnSaveShoppedRate;
			_view.SaveShipment += OnSaveShipment;
			_view.SaveLoadOrder += OnSaveLoadOrder;
		}

        private void OnSaveLoadOrder(object sender, ViewEventArgs<LoadOrder, PaymentInformation> e)
		{
			var loadOrder = e.Argument;

			// check for demo first
			_validator.Messages.Clear();
			if (loadOrder.Customer != null && loadOrder.Customer.RateAndScheduleInDemo)
			{
			    loadOrder.LoadOrderNumber = DemoNumber;
				if (loadOrder.Status == LoadOrderStatus.Quoted)
					_validator.QuotedLoadOrderIsValid(loadOrder);
				else
					_validator.OfferedLoadOrderIsValid(loadOrder);
				var msgs = _validator.Messages.Any()
					           ? _validator.Messages
					           : new List<ValidationMessage> {ValidationMessage.Information("Demo load successfully validated!")};
				_view.DisplayMessages(msgs);
			    return;
			}


            
            var paymentInfo = e.Argument2 ?? new PaymentInformation();
            var paymentRequired = e.Argument2 != null;
            var transactionId = string.Empty;
            var gateway = loadOrder.Tenant.GetPaymentGatewayService();

            // assign new numbers
            var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ShipmentNumber, _view.ActiveUser);
            loadOrder.LoadOrderNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
            paymentInfo.Description = loadOrder.LoadOrderNumber;

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                _mrValidator.Connection = Connection;
                _mrValidator.Transaction = Transaction;

                loadOrder.Connection = Connection;
                loadOrder.Transaction = Transaction;

                // validate load order
                _validator.Messages.Clear();
                if (loadOrder.Status == LoadOrderStatus.Quoted && !_validator.QuotedLoadOrderIsValid(loadOrder))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (loadOrder.Status == LoadOrderStatus.Offered && !_validator.OfferedLoadOrderIsValid(loadOrder))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (loadOrder.Status != LoadOrderStatus.Quoted && loadOrder.Status != LoadOrderStatus.Offered)
                {
                    RollBackTransaction();
                    _view.DisplayMessages(new[] { ValidationMessage.Error("Invalid load record for save. Incorrect status.") });
                    return;
                }

                // pre authorize payment
                if (paymentRequired)
                {
                    if (loadOrder.Status != LoadOrderStatus.Quoted)
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot process payments for loads that aren't quoted.") });
                        return;
                    }

                    if (string.IsNullOrEmpty(paymentInfo.CustomerPaymentProfileId))
                        paymentInfo.CustomerPaymentProfileId = gateway.AddPaymentProfile(paymentInfo);

                    var authResponse = gateway.AuthorizePayment(paymentInfo.CustomerPaymentGatewayKey, paymentInfo.CustomerPaymentProfileId,
                                                                paymentInfo.ChargeAmount, paymentInfo.Description);
                    transactionId = authResponse.TransactionId;

                    if (!authResponse.Approved)
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(new[] { ValidationMessage.Error(authResponse.ErrorMessage) });
                        return;
                    }
                }

                // check for need to lock on customer
                if (loadOrder.Customer.EnableShipperBillOfLading && !ProcessCustomerShipperBOL(loadOrder))
                {
                    var msg = !string.IsNullOrEmpty(transactionId) ? VoidPaymentGatewayTransaction(gateway, transactionId) : null;
                    RollBackTransaction();
                    if (msg != null) _view.DisplayMessages(new[] { msg });
                    return;
                }

                // validate purchase order if necessary
				if (loadOrder.Customer.ValidatePurchaseOrderNumber && !string.IsNullOrEmpty(loadOrder.PurchaseOrderNumber) && !PurchaseOrderNumberIsValid(loadOrder))
                {
                    var msg = !string.IsNullOrEmpty(transactionId) ? VoidPaymentGatewayTransaction(gateway, transactionId) : null;
                    RollBackTransaction();
                    if (msg != null) _view.DisplayMessages(new[] { msg });
                    return;
                }


                // save
                loadOrder.Save();

                // audit log load order
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Quote: {1}", AuditLogConstants.AddedNew, loadOrder.LoadOrderNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                // locations
                ProcessLoadOrderLocations(loadOrder);

                // resave to ensure location id saved
                loadOrder.Save();

                // process rest of collections
                ProcessLoadOrderServices(loadOrder);
                ProcessLoadOrderReferences(loadOrder);
                ProcessLoadOrderItems(loadOrder);
                ProcessLoadOrderCharges(loadOrder);
                ProcessLoadOrderAccountBuckets(loadOrder);
                ProcessLoadOrderEquipments(loadOrder);
                ProcessLoadOrderVendors(loadOrder);

                // create initial historical data for load order
                new LoadOrderHistoricalData
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        LoadOrderId = loadOrder.Id,
                        WasOffered = loadOrder.Status == LoadOrderStatus.Offered,
                        WasQuoted = loadOrder.Status == LoadOrderStatus.Quoted
                    }.Save();

                // authorize and capture payment
                if (paymentRequired)
                {
                    var miscReceipt = new MiscReceipt
                    {
                        AmountPaid = paymentInfo.ChargeAmount,
                        PaymentDate = DateTime.Now,
                        CustomerId = loadOrder.Customer.Id,
                        PaymentGatewayType = loadOrder.Tenant.PaymentGatewayType,
                        LoadOrderId = loadOrder.Id,
                        ShipmentId = default(long),
                        TenantId = loadOrder.TenantId,
                        Reversal = false,
                        UserId = _view.ActiveUser.Id,
                        GatewayTransactionId = transactionId,
                        MiscReceiptApplications = new List<MiscReceiptApplication>(),
                        NameOnCard = paymentInfo.NameOnCard,
                        Connection = Connection,
                        Transaction = Transaction,
                        PaymentProfileId = paymentInfo.CustomerPaymentProfileId,
                        OriginalMiscReceiptId = default(long)
                    };

                    if (!_mrValidator.IsValid(miscReceipt) || !_mrValidator.MiscReceiptLoadOrderExists(miscReceipt))
                    {
                        var msg = !string.IsNullOrEmpty(transactionId)
                                      ? VoidPaymentGatewayTransaction(gateway, transactionId)
                                      : null;
                        var msgs = new List<ValidationMessage>();
                        msgs.AddRange(_mrValidator.Messages);
                        if (msg != null) msgs.Add(msg);
                        RollBackTransaction();
                        _view.DisplayMessages(msgs);
                        return;
                    }
                    miscReceipt.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format(
                                "{0} Misc Receipt Shipment #:{1} Load Order #:{2} Customer:{3} Amount:{4}",
                                AuditLogConstants.AddedNew,
                                string.Empty,
                                loadOrder.LoadOrderNumber,
                                miscReceipt.Customer.CustomerNumber,
                                miscReceipt.AmountPaid),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = miscReceipt.EntityName(),
                        EntityId = miscReceipt.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    var captureResponse = gateway.CapturePreauthorizedPayment(paymentInfo.CustomerPaymentGatewayKey, paymentInfo.CustomerPaymentProfileId, transactionId, paymentInfo.ChargeAmount);
                    transactionId = captureResponse.TransactionId;

                    if (!captureResponse.Approved)
                    {
                        var msg = !string.IsNullOrEmpty(transactionId)
                                      ? VoidPaymentGatewayTransaction(gateway, transactionId)
                                      : null;
                        var msgs = new List<ValidationMessage> { ValidationMessage.Error(captureResponse.ErrorMessage) };
                        if (msg != null) msgs.Add(msg);
                        RollBackTransaction();
                        _view.DisplayMessages(msgs);
                        return;
                    }
                }

                // commit transaction
                CommitTransaction();

                // save addresses
                if (_view.SaveOriginToAddressBook || _view.SaveOriginWithServicesToAddressBook) SaveOriginAddress(loadOrder);
                if (_view.SaveDestinationToAddressBook || _view.SaveDestinationWithServicesToAddressBook) SaveDestinationAddress(loadOrder);

                _view.Set(loadOrder);
            }
            catch (Exception ex)
            {
                var msgs = new List<ValidationMessage>();
                var msg = !string.IsNullOrEmpty(transactionId) ? VoidPaymentGatewayTransaction(gateway, transactionId) : null;
                if (msg != null) msgs.Add(msg);
                msgs.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(msgs);
            }
		}

        private void OnSaveShipment(object sender, ViewEventArgs<Shipment, PaymentInformation> e)
		{
            var shipment = e.Argument;

			// check for demo first
			_validator.Messages.Clear();
			if (shipment.Customer != null && shipment.Customer.RateAndScheduleInDemo)
			{
                shipment.ShipmentNumber = DemoNumber;
                _validator.ShipmentIsValid(shipment);
				var msgs = _validator.Messages.Any()
							   ? _validator.Messages
							   : new List<ValidationMessage> { ValidationMessage.Information("Demo shipment successfully validated!") };
				_view.DisplayMessages(msgs);
			    return;
			}

            var paymentInfo = e.Argument2 ?? new PaymentInformation();
            var paymentRequired = e.Argument2 != null;
            var transactionId = string.Empty;
            var gateway = shipment.Tenant.GetPaymentGatewayService();

            // assign new numbers
            var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ShipmentNumber, _view.ActiveUser);
            shipment.ShipmentNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
            paymentInfo.Description = shipment.ShipmentNumber;

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                _mrValidator.Connection = Connection;
                _mrValidator.Transaction = Transaction;

                shipment.Connection = Connection;
                shipment.Transaction = Transaction;

                // validate shipment
                _validator.Messages.Clear();
                if (!_validator.ShipmentIsValid(shipment))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                // pre authorize payment
                if (paymentRequired)
                {
                    if (string.IsNullOrEmpty(paymentInfo.CustomerPaymentProfileId))
                        paymentInfo.CustomerPaymentProfileId = gateway.AddPaymentProfile(paymentInfo);

                    var authResponse = gateway.AuthorizePayment(paymentInfo.CustomerPaymentGatewayKey, paymentInfo.CustomerPaymentProfileId,
                                                                paymentInfo.ChargeAmount, paymentInfo.Description);
                    transactionId = authResponse.TransactionId;

                    if (!authResponse.Approved)
                    {
                        RollBackTransaction();
                        _view.DisplayMessages(new[] { ValidationMessage.Error(authResponse.ErrorMessage) });
                        return;
                    }
                }

                // check for need to lock on customer
                if (shipment.Customer.EnableShipperBillOfLading && !ProcessCustomerShipperBOL(shipment))
                {
                    var msg = !string.IsNullOrEmpty(transactionId) ? VoidPaymentGatewayTransaction(gateway, transactionId) : null;
                    RollBackTransaction();
                    if (msg != null) _view.DisplayMessages(new[] { msg });
                    return;
                }

                // validate purchase order if necessary
                if (shipment.Customer.ValidatePurchaseOrderNumber && !string.IsNullOrEmpty(shipment.PurchaseOrderNumber) && !PurchaseOrderNumberIsValid(shipment))
                {
                    var msg = !string.IsNullOrEmpty(transactionId) ? VoidPaymentGatewayTransaction(gateway, transactionId) : null;
                    RollBackTransaction();
                    if (msg != null) _view.DisplayMessages(new[] { msg });
                    return;
                }

                // save
                shipment.Save();

                // audit log shipment
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Shipment: {1}", AuditLogConstants.AddedNew, shipment.ShipmentNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();


                // locations
                ProcessShipmentLocations(shipment);

                // terminal information
                if (shipment.OriginTerminal != null) ProcessTerminal(shipment.OriginTerminal);
                if (shipment.DestinationTerminal != null) ProcessTerminal(shipment.DestinationTerminal);

                // resave to ensure location/terminal id saved
                shipment.Save();

                // process rest of collections
                ProcessShipmentServices(shipment);
                ProcessShipmentReferences(shipment);
                ProcessShipmentItems(shipment);
                ProcessShipmentCharges(shipment);
                ProcessShipmentVendors(shipment);
                ProcessShipmentAccountBuckets(shipment);
                ProcessShipmentAutoRatingAccessorials(shipment);

                // create initial historical data for shipment
                new ShipmentHistoricalData
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    ShipmentId = shipment.Id,
                    WasInDispute = shipment.InDispute,
                    WasInDisputeReason = shipment.InDisputeReason,
                    OriginalEstimatedDeliveryDate = shipment.EstimatedDeliveryDate,
                    OriginalEstimatedPickupDate = shipment.DesiredPickupDate,
                }.Save();


                if (paymentRequired)
                {
                    // capture payment
                    var miscReceipt = new MiscReceipt
                    {
                        AmountPaid = paymentInfo.ChargeAmount,
                        PaymentDate = DateTime.Now,
                        CustomerId = shipment.Customer.Id,
                        PaymentGatewayType = shipment.Tenant.PaymentGatewayType,
                        ShipmentId = shipment.Id,
                        LoadOrderId = default(long),
                        TenantId = shipment.TenantId,
                        Reversal = false,
                        UserId = _view.ActiveUser.Id,
                        GatewayTransactionId = transactionId,
                        MiscReceiptApplications = new List<MiscReceiptApplication>(),
                        NameOnCard = paymentInfo.NameOnCard,
                        Connection = Connection,
                        Transaction = Transaction,
                        PaymentProfileId = paymentInfo.CustomerPaymentProfileId,
                        OriginalMiscReceiptId = default(long),
                    };

                    if (!_mrValidator.IsValid(miscReceipt) || !_mrValidator.MiscReceiptShipmentExists(miscReceipt))
                    {
                        var msg = !string.IsNullOrEmpty(transactionId)
                                      ? VoidPaymentGatewayTransaction(gateway, transactionId)
                                      : null;
                        var msgs = new List<ValidationMessage>();
                        msgs.AddRange(_mrValidator.Messages);
                        if (msg != null) msgs.Add(msg);
                        RollBackTransaction();
                        _view.DisplayMessages(msgs);
                        return;
                    }
                    miscReceipt.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Misc Receipt Shipment #:{1} Load Order #:{2} Customer:{3} Amount:{4}",
                                                    AuditLogConstants.AddedNew,
                                                    shipment.ShipmentNumber,
                                                    string.Empty,
                                                    miscReceipt.Customer.CustomerNumber,
                                                    miscReceipt.AmountPaid),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = miscReceipt.EntityName(),
                        EntityId = miscReceipt.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    var captureResponse = gateway.CapturePreauthorizedPayment(paymentInfo.CustomerPaymentGatewayKey,
                                                                              paymentInfo.CustomerPaymentProfileId,
                                                                              transactionId, paymentInfo.ChargeAmount);
                    if (!captureResponse.Approved)
                    {
                        var msg = !string.IsNullOrEmpty(transactionId)
                                      ? VoidPaymentGatewayTransaction(gateway, transactionId)
                                      : null;
                        var msgs = new List<ValidationMessage> { ValidationMessage.Error(captureResponse.ErrorMessage) };
                        if (msg != null) msgs.Add(msg);
                        RollBackTransaction();
                        _view.DisplayMessages(msgs);
                        return;
                    }
                }

                // commit transaction
                CommitTransaction();

                // save addresses
                if (_view.SaveOriginToAddressBook || _view.SaveOriginWithServicesToAddressBook) SaveOriginAddress(shipment);
                if (_view.SaveDestinationToAddressBook || _view.SaveDestinationWithServicesToAddressBook) SaveDestinationAddress(shipment);

                _view.Set(shipment);
            }
            catch (Exception ex)
            {
                var msgs = new List<ValidationMessage>();
                var msg = !string.IsNullOrEmpty(transactionId) ? VoidPaymentGatewayTransaction(gateway, transactionId) : null;
                if (msg != null) msgs.Add(msg);
                msgs.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));

                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(msgs);
            }
		}

		private void OnSaveShoppedRate(object sender, ViewEventArgs<ShoppedRate> e)
		{
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				var shoppedRate = e.Argument;

				shoppedRate.Connection = Connection;
				shoppedRate.Transaction = Transaction;

				var isNew = shoppedRate.IsNew;

				// save
				shoppedRate.Save();

				// only insert services for new shopped rated.  If existing, then this is only and update
				// of reference to quote!
				if (isNew)
					foreach (var service in shoppedRate.Services)
					{
						service.Connection = Connection;
						service.Transaction = Transaction;

						service.Save();
					}

				// commit transaction
				CommitTransaction();
				_view.Set(shoppedRate);
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				_view.Set(new ShoppedRate());
                _view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error("An error occured while performing the last operation. Err: {0}", ex.Message) });
			}
		}


		private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
		{
			if (!_view.ActiveUser.TenantEmployee)
				FetchCustomerForNonEmployee(e.Argument);
			else
				FetchCustomerForEmployee(e.Argument);
		}

		private void OnLoading(object sender, EventArgs e)
		{
			_view.Services = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId]
				.Services
				.Where(s => s.Category == ServiceCategory.Accessorial)
				.ToList();
		}



		private void FetchCustomerForNonEmployee(string customerNumber)
		{
			if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
			{
				_view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
				return;
			}
			var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
			_view.DisplayCustomer(shipAs == null || !shipAs.Customer.Active ? null : shipAs.Customer);
			if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else if (!shipAs.Customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
		}

		private void FetchCustomerForEmployee(string customerNumber)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

			_view.DisplayCustomer(customer == null || !customer.Active ? new Customer() : customer);
			if (customer == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else if (!customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
		}



		private void ProcessLoadOrderItems(LoadOrder loadOrder)
		{
			foreach (var i in loadOrder.Items)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				i.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Item Ref#:{3}",
												AuditLogConstants.AddedNew, i.EntityName(), loadOrder.LoadOrderNumber, i.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessLoadOrderReferences(LoadOrder loadOrder)
		{
			foreach (var r in loadOrder.CustomerReferences)
			{
				r.Connection = Connection;
				r.Transaction = Transaction;

				r.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Customer Reference:{3}",
												AuditLogConstants.AddedNew, r.EntityName(), loadOrder.LoadOrderNumber, r.Name),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessLoadOrderServices(LoadOrder loadOrder)
		{
			foreach (var brs in loadOrder.Services)
			{
				brs.Connection = Connection;
				brs.Transaction = Transaction;

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} {1} Load Order Number:{2} Service Ref#:{3}", AuditLogConstants.AddedNew,
										  brs.EntityName(),
										  loadOrder.LoadOrderNumber, brs.ServiceId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = loadOrder.EntityName(),
						EntityId = loadOrder.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				brs.Save();
			}
		}

		private void ProcessLoadOrderLocations(LoadOrder loadOrder)
		{
			var locations = new List<LoadOrderLocation> { loadOrder.Origin, loadOrder.Destination };

			foreach (var l in locations)
			{
				l.Connection = Connection;
				l.Transaction = Transaction;

				l.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3}",
												AuditLogConstants.AddedNew, l.EntityName(), loadOrder.LoadOrderNumber, l.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();


				ProcessContacts(loadOrder, l);
			}
		}

		private void ProcessContacts(LoadOrder loadOrder, LoadOrderLocation location)
		{
			foreach (var c in location.Contacts)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				c.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3} Contact Ref#: {4}",
												AuditLogConstants.AddedNew, c.EntityName(), loadOrder.LoadOrderNumber, location.Id, c.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			}
		}

        private void ProcessLoadOrderVendors(LoadOrder loadOrder)
        {
            foreach (var sv in loadOrder.Vendors)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                sv.Save();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Vendor Number:{3}",
                                                AuditLogConstants.AddedNew, sv.EntityName(),
                                                loadOrder.LoadOrderNumber, sv.Vendor.VendorNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
            }
        }

		private void ProcessLoadOrderCharges(LoadOrder loadOrder)
		{
			foreach (var charge in loadOrder.Charges)
			{
				charge.Connection = Connection;
				charge.Transaction = Transaction;

				charge.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Load Order Number:{2} Charge Ref#:{3}",
												AuditLogConstants.AddedNew, charge.EntityName(), loadOrder.LoadOrderNumber, charge.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.EntityName(),
					EntityId = loadOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

        private void ProcessLoadOrderAccountBuckets(LoadOrder loadOrder)
        {

            foreach (var loab in loadOrder.AccountBuckets)
            {
                loab.Connection = Connection;
                loab.Transaction = Transaction;

                    loab.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Account Bucket Code:{3}",
                                                    AuditLogConstants.AddedNew, loab.EntityName(), loadOrder.LoadOrderNumber, loab.AccountBucket.Code),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
            }
        }

        private void ProcessLoadOrderEquipments(LoadOrder loadOrder)
        {
            foreach (var equipment in loadOrder.Equipments)
            {
                equipment.Connection = Connection;
                equipment.Transaction = Transaction;

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Load Order Number:{2} Equipment Type Ref#:{3}",
                                          AuditLogConstants.AddedNew,
                                          equipment.EntityName(), loadOrder.LoadOrderNumber, equipment.EquipmentTypeId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                equipment.Save();
            }
        }


		private void ProcessShipmentVendors(Shipment shipment)
		{
			foreach (var qv in shipment.Vendors)
			{
				qv.Connection = Connection;
				qv.Transaction = Transaction;

				qv.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Vendor Number:{3}",
												AuditLogConstants.AddedNew, qv.EntityName(), shipment.ShipmentNumber, qv.Vendor.VendorNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessShipmentAccountBuckets(Shipment shipment)
		{
			foreach (var qab in shipment.AccountBuckets)
			{
				qab.Connection = Connection;
				qab.Transaction = Transaction;

				qab.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Account Bucket Code:{3}",
												AuditLogConstants.AddedNew, qab.EntityName(), shipment.ShipmentNumber, qab.AccountBucket.Code),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessShipmentCharges(Shipment shipment)
		{
			foreach (var charge in shipment.Charges)
			{
				charge.Connection = Connection;
				charge.Transaction = Transaction;

				charge.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Charge Ref#:{3}",
												AuditLogConstants.AddedNew, charge.EntityName(), shipment.ShipmentNumber, charge.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessShipmentItems(Shipment shipment)
		{
			foreach (var i in shipment.Items)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				i.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Item Ref#:{3}",
												AuditLogConstants.AddedNew, i.EntityName(), shipment.ShipmentNumber, i.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			}
		}

		private void ProcessShipmentReferences(Shipment shipment)
		{
			foreach (var r in shipment.CustomerReferences)
			{
				r.Connection = Connection;
				r.Transaction = Transaction;

				r.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Customer Reference:{3}",
												AuditLogConstants.AddedNew, r.EntityName(), shipment.ShipmentNumber, r.Name),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			}
		}

		private void ProcessShipmentServices(Shipment shipment)
		{
			foreach (var service in shipment.Services)
			{
				service.Connection = Connection;
				service.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Shipment Number:{2} Service Ref#:{3}", AuditLogConstants.AddedNew,
									  service.EntityName(),
									  shipment.ShipmentNumber, service.ServiceId),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				service.Save();
			}
		}

		private void ProcessShipmentLocations(Shipment shipment)
		{
			var locations = new List<ShipmentLocation> { shipment.Origin, shipment.Destination };

			foreach (var l in locations)
			{
				l.Connection = Connection;
				l.Transaction = Transaction;

				l.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3}",
												AuditLogConstants.AddedNew, l.EntityName(), shipment.ShipmentNumber, l.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				ProcessShipmentContacts(shipment, l);
			}
		}

		private void ProcessShipmentContacts(Shipment shipment, ShipmentLocation location)
		{
			foreach (var c in location.Contacts)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;

				c.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3} Contact Ref#: {4}",
												AuditLogConstants.AddedNew, c.EntityName(), shipment.ShipmentNumber, location.Id, c.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessShipmentAutoRatingAccessorials(Shipment shipment)
		{
			foreach (var ara in shipment.AutoRatingAccessorials)
			{
				ara.Connection = Connection;
				ara.Transaction = Transaction;

				ara.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Shipment Number:{2} Autorating Service Description:{3}",
												AuditLogConstants.AddedNew, ara.EntityName(), shipment.ShipmentNumber, ara.ServiceDescription),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.EntityName(),
					EntityId = shipment.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessTerminal(LTLTerminalInfo info)
		{
			info.Connection = Connection;
			info.Transaction = Transaction;

			// save
			info.Save();

			// audit log shipment
			new AuditLog
			{
				Connection = Connection,
				Transaction = Transaction,
				Description = string.Format("{0} LTL Terminal Info: {1}", AuditLogConstants.AddedNew, info.Name),
				TenantId = _view.ActiveUser.TenantId,
				User = _view.ActiveUser,
				EntityCode = info.Shipment.EntityName(),
				EntityId = info.Shipment.Id.ToString(),
				LogDateTime = DateTime.Now
			}.Log();
		}


		private bool ProcessCustomerShipperBOL(Shipment shipment)
		{
			var @lock = shipment.Customer.ObtainLock(_view.ActiveUser, shipment.Customer.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return false;
			}

			// change customer transaction
			shipment.Customer.Connection = Connection;
			shipment.Customer.Transaction = Transaction;

			@lock.Connection = Connection;
			@lock.Transaction = Transaction;

			// set shipper bol and increment customer seed
			shipment.Customer.TakeSnapShot();
			shipment.ShipperBol = string.Format("{0}{1}{2}", shipment.Customer.ShipperBillOfLadingPrefix,
												++shipment.Customer.ShipperBillOfLadingSeed,
												shipment.Customer.ShipperBillOfLadingSuffix);

			// audit log customer
			foreach (var change in shipment.Customer.Changes())
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} on shipment [{1}]", change, shipment.ShipmentNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = shipment.Customer.EntityName(),
					EntityId = shipment.Customer.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			// save customer
			shipment.Customer.Save();

			// remove customer lock
			@lock.Delete();

			return true;
		}

		private bool PurchaseOrderNumberIsValid(Shipment shipment)
		{
			var purchaseOrder = new CustomerPurchaseOrderSearch()
				.FetchCustomerPurchaseOrder(shipment.TenantId, shipment.Customer.Id, shipment.PurchaseOrderNumber);

			if (purchaseOrder == null)
			{
				var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
							  ? "Purchase order number is not found. Customer requires a valid purchase order number."
							  : shipment.Customer.InvalidPurchaseOrderNumberMessage;

				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			var @lock = purchaseOrder.ObtainLock(_view.ActiveUser, shipment.Customer.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return false;
			}

			if (purchaseOrder.ExpirationDate.TimeToMaximum() < DateTime.Now.TimeToMaximum())
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
				          	? string.Format("Purchase order number is expired as of {0:yyyy-MM-dd}", purchaseOrder.ExpirationDate)
				          	: shipment.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			if (purchaseOrder.ApplyOnOrigin && !(purchaseOrder.IsValidPostalCodeFormat(shipment.Origin.PostalCode) && purchaseOrder.CountryId == shipment.Origin.CountryId))
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
							? "Purchase order number is invalid for origin location"
							: shipment.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			if (purchaseOrder.ApplyOnDestination && !(purchaseOrder.IsValidPostalCodeFormat(shipment.Destination.PostalCode) && purchaseOrder.CountryId == shipment.Destination.CountryId))
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
							? "Purchase order number is invalid for destination location"
							: shipment.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			purchaseOrder.TakeSnapShot();

			purchaseOrder.Connection = Connection;
			purchaseOrder.Transaction = Transaction;

			@lock.Connection = Connection;
			@lock.Transaction = Transaction;

			if (++purchaseOrder.CurrentUses > purchaseOrder.MaximumUses)
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error("Purchase order number has previously reached maximum uses of {0}", purchaseOrder.MaximumUses) });
				return false;
			}


			foreach (var change in purchaseOrder.Changes())
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} for shipment [{1}]", change, shipment.ShipmentNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = purchaseOrder.EntityName(),
					EntityId = purchaseOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			// save purchase order
			purchaseOrder.Save();

			// remove lock
			@lock.Delete();

			return true;
		}

		private bool ProcessCustomerShipperBOL(LoadOrder loadOrder)
		{
			var @lock = loadOrder.Customer.ObtainLock(_view.ActiveUser, loadOrder.Customer.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return false;
			}

			// change customer transaction
			loadOrder.Customer.Connection = Connection;
			loadOrder.Customer.Transaction = Transaction;

			@lock.Connection = Connection;
			@lock.Transaction = Transaction;

			// set shipper bol and increment customer seed
			loadOrder.Customer.TakeSnapShot();
			loadOrder.ShipperBol = string.Format("{0}{1}{2}", loadOrder.Customer.ShipperBillOfLadingPrefix,
												++loadOrder.Customer.ShipperBillOfLadingSeed,
												loadOrder.Customer.ShipperBillOfLadingSuffix);

			// audit log customer
			foreach (var change in loadOrder.Customer.Changes())
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} on Load Order [{1}]", change, loadOrder.LoadOrderNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = loadOrder.Customer.EntityName(),
					EntityId = loadOrder.Customer.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			// save customer
			loadOrder.Customer.Save();

			// remove customer lock
			@lock.Delete();

			return true;
		}

		private bool PurchaseOrderNumberIsValid(LoadOrder loadOrder)
		{
			var purchaseOrder = new CustomerPurchaseOrderSearch()
				.FetchCustomerPurchaseOrder(loadOrder.TenantId, loadOrder.Customer.Id, loadOrder.PurchaseOrderNumber);

			if (purchaseOrder == null)
			{
				var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
							  ? ProcessorVars.MissingPOErrMsg
							  : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;

				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			var @lock = purchaseOrder.ObtainLock(_view.ActiveUser, loadOrder.Customer.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return false;
			}

			if (purchaseOrder.ExpirationDate.TimeToMaximum() < DateTime.Now.TimeToMaximum())
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
							  ? string.Format(ProcessorVars.ExpiredPOErrMsgDateFormat, purchaseOrder.ExpirationDate.FormattedShortDateAlt())
							  : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			if (purchaseOrder.ApplyOnOrigin && !(purchaseOrder.IsValidPostalCodeFormat(loadOrder.Origin.PostalCode) && purchaseOrder.CountryId == loadOrder.Origin.CountryId))
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
							? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "origin")
							: loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			if (purchaseOrder.ApplyOnDestination && !(purchaseOrder.IsValidPostalCodeFormat(loadOrder.Destination.PostalCode) && purchaseOrder.CountryId == loadOrder.Destination.CountryId))
			{
				@lock.Delete();
				var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
							? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "destination")
							: loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
				_view.DisplayMessages(new[] { ValidationMessage.Error(msg) });
				return false;
			}

			purchaseOrder.TakeSnapShot();

			purchaseOrder.Connection = Connection;
			purchaseOrder.Transaction = Transaction;

			@lock.Connection = Connection;
			@lock.Transaction = Transaction;

			if (++purchaseOrder.CurrentUses > purchaseOrder.MaximumUses)
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.MaxUsePOErrMsgFormat, purchaseOrder.MaximumUses) });
				return false;
			}


			foreach (var change in purchaseOrder.Changes())
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} for load order [{1}]", change, loadOrder.LoadOrderNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = purchaseOrder.EntityName(),
					EntityId = purchaseOrder.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			// save purchase order
			purchaseOrder.Save();

			// remove lock
			@lock.Delete();

			return true;
		}



		private void SaveOriginAddress(LoadOrder loadOrder)
		{
			// build
			var addressBook = new AddressBook
			{
				City = loadOrder.Origin.City,
				State = loadOrder.Origin.State,
				CountryId = loadOrder.Origin.CountryId,
				Street1 = loadOrder.Origin.Street1,
				Street2 = loadOrder.Origin.Street2,
				Customer = loadOrder.Customer,
				Description = loadOrder.Origin.Description,
				OriginSpecialInstruction = loadOrder.Origin.SpecialInstructions,
				DestinationSpecialInstruction = string.Empty,
				Direction = loadOrder.Origin.Direction,
				GeneralInfo = loadOrder.Origin.GeneralInfo,
				PostalCode = loadOrder.Origin.PostalCode,
				TenantId = loadOrder.Origin.TenantId,
				Contacts = new List<AddressBookContact>(),
				Services = new List<AddressBookService>()
			};
			addressBook.Contacts.AddRange(
				loadOrder.Origin.Contacts.Select(c =>
												new AddressBookContact
												{
													TenantId = c.TenantId,
													AddressBook = addressBook,
													Comment = c.Comment,
													ContactTypeId = c.ContactTypeId,
													Email = c.Email,
													Fax = c.Fax,
													Phone = c.Phone,
													Mobile = c.Mobile,
													Name = c.Name,
													Primary = c.Primary,
												}));
			if (_view.SaveOriginWithServicesToAddressBook && loadOrder.Services.Any())
				addressBook.Services.AddRange(
					loadOrder.Services.Select(s => new AddressBookService
					{
						AddressBook = addressBook,
						TenantId = s.TenantId,
						ServiceId = s.ServiceId,
					}));
			SaveAddressBook(addressBook);

		}

        private void SaveDestinationAddress(LoadOrder loadOrder)
		{
			// build
			var addressBook = new AddressBook
			{
				City = loadOrder.Destination.City,
				State = loadOrder.Destination.State,
				CountryId = loadOrder.Destination.CountryId,
				Street1 = loadOrder.Destination.Street1,
				Street2 = loadOrder.Destination.Street2,
				Customer = loadOrder.Customer,
				Description = loadOrder.Destination.Description,
				DestinationSpecialInstruction = loadOrder.Destination.SpecialInstructions,
				OriginSpecialInstruction = string.Empty,
				Direction = loadOrder.Destination.Direction,
				GeneralInfo = loadOrder.Destination.GeneralInfo,
				PostalCode = loadOrder.Destination.PostalCode,
				TenantId = loadOrder.Destination.TenantId,
				Contacts = new List<AddressBookContact>(),
				Services = new List<AddressBookService>()
			};
			addressBook.Contacts.AddRange(
				loadOrder.Destination.Contacts.Select(c =>
												new AddressBookContact
												{
													TenantId = c.TenantId,
													AddressBook = addressBook,
													Comment = c.Comment,
													ContactTypeId = c.ContactTypeId,
													Email = c.Email,
													Fax = c.Fax,
													Phone = c.Phone,
													Mobile = c.Mobile,
													Name = c.Name,
													Primary = c.Primary,
												}));
			if (_view.SaveDestinationWithServicesToAddressBook && loadOrder.Services.Any())
				addressBook.Services.AddRange(
					loadOrder.Services.Select(s => new AddressBookService
					{
						AddressBook = addressBook,
						TenantId = s.TenantId,
						ServiceId = s.ServiceId,
					}));
			SaveAddressBook(addressBook);

		}

		private void SaveOriginAddress(Shipment shipment)
		{
			// build
			var addressBook = new AddressBook
							{
								City = shipment.Origin.City,
								State = shipment.Origin.State,
								CountryId = shipment.Origin.CountryId,
								Street1 = shipment.Origin.Street1,
								Street2 = shipment.Origin.Street2,
								Customer = shipment.Customer,
								Description = shipment.Origin.Description,
								OriginSpecialInstruction = shipment.Origin.SpecialInstructions,
								DestinationSpecialInstruction = string.Empty,
								Direction = shipment.Origin.Direction,
								GeneralInfo = shipment.Origin.GeneralInfo,
								PostalCode = shipment.Origin.PostalCode,
								TenantId = shipment.Origin.TenantId,
								Contacts = new List<AddressBookContact>(),
								Services = new List<AddressBookService>()
							};
			addressBook.Contacts.AddRange(
				shipment.Origin.Contacts.Select(c =>
												new AddressBookContact
													{
														TenantId = c.TenantId,
														AddressBook = addressBook,
														Comment = c.Comment,
														ContactTypeId = c.ContactTypeId,
														Email = c.Email,
														Fax = c.Fax,
														Phone = c.Phone,
														Mobile = c.Mobile,
														Name = c.Name,
														Primary = c.Primary,
													}));
			if (_view.SaveOriginWithServicesToAddressBook && shipment.Services.Any())
				addressBook.Services.AddRange(
					shipment.Services.Select(s => new AddressBookService
													{
														AddressBook = addressBook,
														TenantId = s.TenantId,
														ServiceId = s.ServiceId,
													}));
			SaveAddressBook(addressBook);

		}

		private void SaveDestinationAddress(Shipment shipment)
		{
			// build
			var addressBook = new AddressBook
			{
				City = shipment.Destination.City,
				State = shipment.Destination.State,
				CountryId = shipment.Destination.CountryId,
				Street1 = shipment.Destination.Street1,
				Street2 = shipment.Destination.Street2,
				Customer = shipment.Customer,
				Description = shipment.Destination.Description,
				DestinationSpecialInstruction = shipment.Destination.SpecialInstructions,
				OriginSpecialInstruction = string.Empty,
				Direction = shipment.Destination.Direction,
				GeneralInfo = shipment.Destination.GeneralInfo,
				PostalCode = shipment.Destination.PostalCode,
				TenantId = shipment.Destination.TenantId,
				Contacts = new List<AddressBookContact>(),
				Services = new List<AddressBookService>()
			};
			addressBook.Contacts.AddRange(
				shipment.Destination.Contacts.Select(c =>
												new AddressBookContact
												{
													TenantId = c.TenantId,
													AddressBook = addressBook,
													Comment = c.Comment,
													ContactTypeId = c.ContactTypeId,
													Email = c.Email,
													Fax = c.Fax,
													Phone = c.Phone,
													Mobile = c.Mobile,
													Name = c.Name,
													Primary = c.Primary,
												}));
			if (_view.SaveDestinationWithServicesToAddressBook && shipment.Services.Any())
				addressBook.Services.AddRange(
					shipment.Services.Select(s => new AddressBookService
					{
						AddressBook = addressBook,
						TenantId = s.TenantId,
						ServiceId = s.ServiceId,
					}));
			SaveAddressBook(addressBook);

		}

		private void SaveAddressBook(AddressBook addressBook)
		{
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// save and audit log
				addressBook.Connection = Connection;
				addressBook.Transaction = Transaction;
				addressBook.Save();

				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Ref #:{1}", AuditLogConstants.AddedNew, addressBook.Id),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = addressBook.EntityName(),
						EntityId = addressBook.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// contants
				foreach (var contact in addressBook.Contacts)
				{
					contact.Connection = Connection;
					contact.Transaction = Transaction;

					contact.AddressBook = addressBook;

					contact.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Contact Name: {2} ", AuditLogConstants.AddedNew,
														contact.EntityName(), contact.Name),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = addressBook.EntityName(),
							EntityId = addressBook.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}

				// services

				foreach (var service in addressBook.Services)
				{
					service.Connection = Connection;
					service.Transaction = Transaction;

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} Service Ref #: {2} ", AuditLogConstants.AddedNew,
											  service.EntityName(), service.ServiceId),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = addressBook.EntityName(),
							EntityId = addressBook.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					service.Save();
				}


				// commit
				CommitTransaction();
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
			}
		}


        private ValidationMessage VoidPaymentGatewayTransaction(PaymentGatewayService gateway, string transactionId)
        {
            ValidationMessage msg = null;
            try
            {
                gateway.Void(transactionId);
            }
            catch (Exception gException)
            {
                msg = ValidationMessage.Error(string.Format("Error processing void for payment gateway transaction: {0}", gException.Message));
            }
            return msg;
        }
	}
}
