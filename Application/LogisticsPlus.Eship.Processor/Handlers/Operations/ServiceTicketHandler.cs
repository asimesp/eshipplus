﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class ServiceTicketHandler : EntityBase
    {
        private readonly IServiceTicketView _view;
        private readonly ServiceTicketValidator _validator = new ServiceTicketValidator();
        private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

        public ServiceTicketHandler(IServiceTicketView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.LoadAuditLog += OnLoadAuditLog;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.CustomerSearch += OnCustomerSearch;
            _view.VendorSearch += OnVendorSearch;
            _view.AccountBucketSearch += OnAccountBucketSearch;
            _view.PrefixSearch += OnPrefixSearch;
            _view.BatchImport += OnBatchImport;
        }

        private void OnBatchImport(object sender, ViewEventArgs<List<ServiceTicket>> e)
        {
            var messages = new ValidationMessageCollection();
            foreach (var serviceTicket in e.Argument)
            {
                // assign new numbers
                var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ServiceTicketNumber, _view.ActiveUser);
                serviceTicket.ServiceTicketNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    // link up
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    serviceTicket.Connection = Connection;
                    serviceTicket.Transaction = Transaction;

                    // validate service Ticket
                    _validator.Messages.Clear();
                    if (!_validator.IsValid(serviceTicket))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }

                    // save
                    serviceTicket.Save();

                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description =
                                string.Format("{0} Service Ticket: {1}", AuditLogConstants.AddedNew,
                                              serviceTicket.ServiceTicketNumber),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();


                    // process rest of collections
                    ProcessServiceTicketItems(serviceTicket, new List<ServiceTicketItem>());
                    ProcessServiceTicketCharges(serviceTicket, new List<ServiceTicketCharge>());
                    ProcessServiceTicketVendors(serviceTicket, new List<ServiceTicketVendor>());
                    ProcessServiceTicketNotes(serviceTicket, new List<ServiceTicketNote>());
                    ProcessServiceTicketServices(serviceTicket, new List<ServiceTicketService>());
                    ProcessServiceTicketEquipments(serviceTicket, new List<ServiceTicketEquipment>());

                    // commit transaction
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
                }
            }

            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }

        private void OnSave(object sender, ViewEventArgs<ServiceTicket> e)
        {
            var serviceTicket = e.Argument;

            // check locks
            if (!serviceTicket.IsNew)
            {
                var @lock = serviceTicket.RetrieveLock(_view.ActiveUser, serviceTicket.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            // assign new numbers
            if (serviceTicket.IsNew)
            {
                var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ServiceTicketNumber, _view.ActiveUser);
                serviceTicket.ServiceTicketNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
            }

            // retrieve db shipment for collection comparisons ...
            var dbServiceTicket = new ServiceTicket(serviceTicket.Id, false);
            dbServiceTicket.LoadCollections();

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                serviceTicket.Connection = Connection;
                serviceTicket.Transaction = Transaction;

                // validate service Ticket
                _validator.Messages.Clear();
                if (!_validator.IsValid(serviceTicket))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                // audit log shipment
                if (serviceTicket.IsNew)
                {
                    // save
                    serviceTicket.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Service Ticket: {1}", AuditLogConstants.AddedNew, serviceTicket.ServiceTicketNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (serviceTicket.HasChanges())
                {
                    foreach (var change in serviceTicket.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save
                    serviceTicket.Save();
                }

                // resave to ensure location id saved
                serviceTicket.Save();

                // process rest of collections
                ProcessServiceTicketServices(serviceTicket, dbServiceTicket.Services);
                ProcessServiceTicketEquipments(serviceTicket, dbServiceTicket.Equipments);
                ProcessServiceTicketItems(serviceTicket, dbServiceTicket.Items);
                ProcessServiceTicketCharges(serviceTicket, dbServiceTicket.Charges);
                ProcessServiceTicketNotes(serviceTicket, dbServiceTicket.Notes);
                ProcessServiceTicketVendors(serviceTicket, dbServiceTicket.Vendors);
                ProcessServiceTicketAssets(serviceTicket, dbServiceTicket.Assets);
				ProcessServiceTicketDocuments(serviceTicket, dbServiceTicket.Documents);

                // commit transaction
                CommitTransaction();

                _view.Set(serviceTicket);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<ServiceTicket> e)
        {
            var serviceTicket = e.Argument;

            if (serviceTicket.IsNew)
            {
                _view.Set(new ServiceTicket());
                return;
            }

            // load collections
            serviceTicket.LoadCollections();

            // obtain lock/check lock
            var @lock = serviceTicket.ObtainLock(_view.ActiveUser, serviceTicket.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                serviceTicket.Connection = Connection;
                serviceTicket.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _validator.Messages.Clear();
                if (!_validator.CanDeleteServiceTicket(serviceTicket))
                {
                    RollBackTransaction();
                    @lock.Delete();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Service Ticket Number: {1}", AuditLogConstants.Delete, serviceTicket.ServiceTicketNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = serviceTicket.EntityName(),
                    EntityId = serviceTicket.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                foreach (var item in serviceTicket.Items)
                {
                    item.Connection = Connection;
                    item.Transaction = Transaction;

                    item.Delete();
                }

                foreach (var equipment in serviceTicket.Equipments)
                {
                    equipment.Connection = Connection;
                    equipment.Transaction = Transaction;

                    equipment.Delete();
                }

                foreach (var service in serviceTicket.Services)
                {
                    service.Connection = Connection;
                    service.Transaction = Transaction;

                    service.Delete();
                }

                foreach (var vendor in serviceTicket.Vendors)
                {
                    vendor.Connection = Connection;
                    vendor.Transaction = Transaction;

                    vendor.Delete();
                }

                foreach (var charge in serviceTicket.Charges)
                {
                    charge.Connection = Connection;
                    charge.Transaction = Transaction;

                    charge.Delete();
                }

                foreach (var note in serviceTicket.Notes)
                {
                    note.Connection = Connection;
                    note.Transaction = Transaction;

                    note.Delete();
                }

				foreach (var document in serviceTicket.Documents)
				{
					document.Connection = Connection;
					document.Transaction = Transaction;

					document.Delete();
				}

                foreach (var asset in serviceTicket.Assets)
                {
                    asset.Connection = Connection;
                    asset.Transaction = Transaction;

                    asset.Delete();
                }

                // delete Service Ticket and lock
                serviceTicket.Delete();

                @lock.Delete();

                CommitTransaction();

                _view.Set(new ServiceTicket());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }


        private void OnPrefixSearch(object sender, ViewEventArgs<string> e)
        {
            var prefix = new PrefixSearch().FetchPrefixByCode(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplayPrefix(prefix == null || !prefix.Active ? new Prefix() : prefix);
            if (prefix == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Prefix not found") });
            else if (!prefix.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Prefix is not active") });
        }

        private void OnVendorSearch(object sender, ViewEventArgs<string> e)
        {
            var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplayVendor(vendor == null || !vendor.Active ? new Vendor() : vendor);
            if (vendor == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
            else if (!vendor.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor is not active") });
        }

        private void OnAccountBucketSearch(object sender, ViewEventArgs<string> e)
        {
            var accountBucket = new AccountBucketSearch().FetchAccountBucketByCode(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplayAccountBucket(accountBucket == null || !accountBucket.Active ? new AccountBucket() : accountBucket);
            if (accountBucket == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Account Bucket not found") });
            else if (!accountBucket.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Account Bucket is not active") });
        }

        private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
        {
            if (!_view.ActiveUser.TenantEmployee)
                FetchCustomerForNonEmployee(e.Argument);
            else
                FetchCustomerForEmployee(e.Argument);
        }


        private void OnUnLock(object sender, ViewEventArgs<ServiceTicket> e)
        {
            var request = e.Argument;
            var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }
            @lock.Delete();
        }

        private void OnLock(object sender, ViewEventArgs<ServiceTicket> e)
        {
            var request = e.Argument;
            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnLoadAuditLog(object sender, ViewEventArgs<ServiceTicket> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
                                                                             e.Argument.TenantId));
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.EquipmentTypes = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].EquipmentTypes;
            _view.Services = new ServiceSearch().FetchServicesByCategory(ServiceCategory.Accessorial, _view.ActiveUser.TenantId);
            _view.NoteTypes = ProcessorUtilities.GetAll<NoteType>();
            _view.Assets = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].Assets;
        }


        private void FetchCustomerForNonEmployee(string customerNumber)
        {
            if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
            {
                _view.DisplayCustomer(_view.ActiveUser.DefaultCustomer, true);
                return;
            }
            var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
            var customer = shipAs == null || !shipAs.Customer.Active ? null : shipAs.Customer;
            _view.DisplayCustomer(customer, true);
            if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
            else if (!shipAs.Customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
        }

        private void FetchCustomerForEmployee(string customerNumber)
        {
            var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

            var customer1 = customer == null || !customer.Active ? new Customer() : customer;
            _view.DisplayCustomer(customer1, true);
            if (customer == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
            else if (!customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
        }


	    private void ProcessServiceTicketVendors(ServiceTicket serviceTicket, IEnumerable<ServiceTicketVendor> dbVendors)
        {
            var dbDelete = dbVendors.Where(dbv => !serviceTicket.Vendors.Select(v => v.Id).Contains(dbv.Id));

            foreach (var qv in dbDelete)
            {
                qv.Connection = Connection;
                qv.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Service Ticket Number:{2} Vendor Number:{3}",
                                                AuditLogConstants.Delete, qv.EntityName(), serviceTicket.ServiceTicketNumber, qv.Vendor.VendorNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = serviceTicket.EntityName(),
                    EntityId = serviceTicket.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                qv.Delete();
            }

            foreach (var qv in serviceTicket.Vendors)
            {
                qv.Connection = Connection;
                qv.Transaction = Transaction;

                if (qv.IsNew)
                {
                    qv.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Service Ticket Number:{2} Vendor Number:{3}",
                                                    AuditLogConstants.AddedNew, qv.EntityName(), serviceTicket.ServiceTicketNumber, qv.Vendor.VendorNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (qv.HasChanges())
                {
                    foreach (var change in qv.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Service Ticket Vendor's Vendor Number:{1} {2}", qv.EntityName(), qv.Vendor.VendorNumber, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    qv.Save();
                }
            }
        }

        private void ProcessServiceTicketNotes(ServiceTicket serviceTicket, IEnumerable<ServiceTicketNote> dbNotes)
        {
            var dbDelete = dbNotes.Where(dbn => !serviceTicket.Notes.Select(n => n.Id).Contains(dbn.Id));

            foreach (var note in dbDelete)
            {
                note.Connection = Connection;
                note.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Service Ticket Number:{2} Note Ref#:{3}",
                                                AuditLogConstants.Delete, note.EntityName(), serviceTicket.ServiceTicketNumber, note.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = serviceTicket.EntityName(),
                    EntityId = serviceTicket.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                note.Delete();
            }

            foreach (var note in serviceTicket.Notes)
            {
                note.Connection = Connection;
                note.Transaction = Transaction;

                if (note.IsNew)
                {
                    note.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Service Ticket Number:{2} Note Ref#:{3}",
                                                    AuditLogConstants.AddedNew, note.EntityName(), serviceTicket.ServiceTicketNumber, note.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (note.HasChanges())
                {
                    foreach (var change in note.Changes())
                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = string.Format("{0} Note Ref#:{1} {2}", note.EntityName(), note.Id, change),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = serviceTicket.EntityName(),
                                EntityId = serviceTicket.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                    note.Save();
                }
            }
        }

        private void ProcessServiceTicketCharges(ServiceTicket serviceTicket, IEnumerable<ServiceTicketCharge> dbCharges)
        {
            var dbDelete = dbCharges.Where(dbc => !serviceTicket.Charges.Select(c => c.Id).Contains(dbc.Id));

            foreach (var charge in dbDelete)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Service Ticket Number:{2} Charge Ref#:{3}",
                                                AuditLogConstants.Delete, charge.EntityName(), serviceTicket.ServiceTicketNumber, charge.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = serviceTicket.EntityName(),
                    EntityId = serviceTicket.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                charge.Delete();
            }

            foreach (var charge in serviceTicket.Charges)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                if (charge.IsNew)
                {
                    charge.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Service Ticket Number:{2} Charge Ref#:{3}",
                                                    AuditLogConstants.AddedNew, charge.EntityName(), serviceTicket.ServiceTicketNumber, charge.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (charge.HasChanges())
                {
                    foreach (var change in charge.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Charge Ref#:{1} {2}", charge.EntityName(), charge.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    charge.Save();
                }
            }
        }

        private void ProcessServiceTicketItems(ServiceTicket serviceTicket, IEnumerable<ServiceTicketItem> dbItems)
        {
            var dbDelete = dbItems.Where(dbi => !serviceTicket.Items.Select(i => i.Id).Contains(dbi.Id));

            foreach (var i in dbDelete)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Service Ticket Number:{2} Item Ref#:{3}",
                                                AuditLogConstants.Delete, i.EntityName(), serviceTicket.ServiceTicketNumber, i.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = serviceTicket.EntityName(),
                    EntityId = serviceTicket.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                i.Delete();
            }

            foreach (var i in serviceTicket.Items)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                if (i.IsNew)
                {
                    i.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Service Ticket Number:{2} Item Ref#:{3}",
                                                    AuditLogConstants.AddedNew, i.EntityName(), serviceTicket.ServiceTicketNumber, i.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (i.HasChanges())
                {
                    foreach (var change in i.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Item Ref#:{1} {2}", i.EntityName(), i.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    i.Save();
                }
            }
        }

        private void ProcessServiceTicketEquipments(ServiceTicket serviceTicket, IEnumerable<ServiceTicketEquipment> dbEquipments)
        {
            var dbDelete = dbEquipments.Where(dbe => !serviceTicket.Equipments.Select(e => e.EquipmentTypeId).Contains(dbe.EquipmentTypeId));

            foreach (var s in dbDelete)
            {
                s.Connection = Connection;
                s.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Service Ticket Number:{2} Equipment Type Ref#: {3}", AuditLogConstants.Delete, s.EntityName(),
                                      serviceTicket.ServiceTicketNumber, s.EquipmentTypeId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = serviceTicket.EntityName(),
                    EntityId = serviceTicket.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                s.Delete();
            }

            foreach (var equipment in serviceTicket.Equipments)
                if (!_validator.ServiceTicketEquipmentExists(equipment))
                {
                    equipment.Connection = Connection;
                    equipment.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Service Ticket Number:{2} Equipment Type Ref#:{3}", AuditLogConstants.AddedNew,
                                          equipment.EntityName(), serviceTicket.ServiceTicketNumber, equipment.EquipmentTypeId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    equipment.Save();
                }
        }

        private void ProcessServiceTicketServices(ServiceTicket serviceTicket, IEnumerable<ServiceTicketService> dbServices)
        {
            var dbDelete = dbServices.Where(dbs => !serviceTicket.Services.Select(s => s.ServiceId).Contains(dbs.ServiceId));

            foreach (var s in dbDelete)
            {
                s.Connection = Connection;
                s.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Service Ticket Number:{2} Service Ref#: {3}", AuditLogConstants.Delete, s.EntityName(),
                                      serviceTicket.ServiceTicketNumber, s.ServiceId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = serviceTicket.EntityName(),
                    EntityId = serviceTicket.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                s.Delete();
            }

            foreach (var service in serviceTicket.Services)
                if (!_validator.ServiceTicketServiceExists(service))
                {
                    service.Connection = Connection;
                    service.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} ServiceTicket Number:{2} Service Ref#:{3}", AuditLogConstants.AddedNew,
                                          service.EntityName(),
                                          serviceTicket.ServiceTicketNumber, service.ServiceId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    service.Save();
                }
        }

        private void ProcessServiceTicketAssets(ServiceTicket serviceTicket, IEnumerable<ServiceTicketAsset> dbAssets)
        {
            var dbDelete = dbAssets.Where(dbs => !serviceTicket.Assets.Select(s => s.Id).Contains(dbs.Id));

            foreach (var a in dbDelete)
            {
                a.Connection = Connection;
                a.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Service Ticket Number:{2} Asset Ref#: {3}", AuditLogConstants.Delete, a.EntityName(),
                                      serviceTicket.ServiceTicketNumber, a.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = serviceTicket.EntityName(),
                    EntityId = serviceTicket.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                a.Delete();
            }

            foreach (var a in serviceTicket.Assets)
            {
                a.Connection = Connection;
                a.Transaction = Transaction;

                if (a.IsNew)
                {
                    a.Save();
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description =
                                string.Format("{0} {1} Service Ticket Number:{2} Asset Ref#:{3}",
                                              AuditLogConstants.AddedNew,
                                              a.EntityName(),
                                              serviceTicket.ServiceTicketNumber, a.Id),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                }
                else if (a.HasChanges())
                {
                    foreach (var change in a.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Service Ticket Asset's Asset:{1} {2}", a.EntityName(), a.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    a.Save();

                }
            }
        }

		private void ProcessServiceTicketDocuments(ServiceTicket serviceTicket, IEnumerable<ServiceTicketDocument> dbDocuments)
		{
			var dbDelete = dbDocuments.Where(dbs => !serviceTicket.Documents.Select(s => s.Id).Contains(dbs.Id));

			foreach (var d in dbDelete)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description =
						string.Format("{0} {1} Service Ticket Number:{2} Document Name: {3}", AuditLogConstants.Delete, d.EntityName(),
									  serviceTicket.ServiceTicketNumber, d.Name),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = serviceTicket.EntityName(),
					EntityId = serviceTicket.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				d.Delete();
			}

			foreach (var d in serviceTicket.Documents)
			{
				d.Connection = Connection;
				d.Transaction = Transaction;

				if (d.IsNew)
				{
					d.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Service Ticket Number:{2} Document Name:{3}",
													AuditLogConstants.AddedNew, d.EntityName(), serviceTicket.ServiceTicketNumber, d.Name),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = serviceTicket.EntityName(),
						EntityId = serviceTicket.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (d.HasChanges())
				{
					foreach (var change in d.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Document Name:{1} {2}", d.EntityName(), d.Name, change),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = serviceTicket.EntityName(),
							EntityId = serviceTicket.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					d.Save();
				}
			}
		}
    }
}
