﻿using System;
using System.Linq;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class JobHandler : EntityBase
    {
        private readonly IJobView _view;
        private readonly JobValidator _validator = new JobValidator();
        private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

        public JobHandler(IJobView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.LoadAuditLog += OnLoadAuditLog;
            _view.Save += OnSave;
            _view.CustomerSearch += OnCustomerSearch;
            _view.Delete += OnDelete;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.StatusTypes = ProcessorUtilities.GetAll<JobStatus>();
        }

        private void OnLoadAuditLog(object sender, ViewEventArgs<Job> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
                                                                             e.Argument.TenantId));
        }

        private void OnSave(object sender, ViewEventArgs<Job, List<JobComponentDto>> e)
        {
            var job = e.Argument;
            var jobComponentDtos = e.Argument2;

            // check locks
            if (!job.IsNew)
            {
                var @lock = job.RetrieveLock(_view.ActiveUser, job.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            // assign new numbers
            if (job.IsNew)
            {
                var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.JobNumber, _view.ActiveUser);
                job.JobNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
            }

            // retrieve db job for collection comparisons ...
            var dbJob = new Job(job.Id, false);
            var documents = dbJob.Documents;

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                job.Connection = Connection;
                job.Transaction = Transaction;

                var msgs = new List<ValidationMessage>();

                // validate job
                _validator.Messages.Clear();
                if (!_validator.IsValid(job))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }
                if (_validator.Messages.Any()) msgs.AddRange(_validator.Messages); // for warning messages

                // audit log job
                if (job.IsNew)
                {
                    // save
                    job.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Job: {1}", AuditLogConstants.AddedNew, job.JobNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = job.EntityName(),
                        EntityId = job.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (job.HasChanges())
                {
                    foreach (var change in job.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = job.EntityName(),
                            EntityId = job.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save
                    job.Save();
                }

                ProcessJobDocuments(job, documents);

                if (jobComponentDtos != null && jobComponentDtos.Count > 0)
                {
                    var messages = ProcessJobComponentDtos(job, dbJob, jobComponentDtos);
                    if (messages.Any())
                    {
                        msgs.Add(ValidationMessage.Warning(ProcessorVars.UnableToObtainLockErrMsg));
                        msgs.AddRange(messages);
                    }
                }

                // commit transaction
                CommitTransaction();

                msgs.Add(ValidationMessage.Information(ProcessorVars.RecordSaveMsg));

                _view.Set(job, jobComponentDtos);
                _view.DisplayMessages(msgs);
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
        {
            if (!_view.ActiveUser.TenantEmployee)
                FetchCustomerForNonEmployee(e.Argument);
            else
                FetchCustomerForEmployee(e.Argument);
        }

        private void OnDelete(object sender, ViewEventArgs<Job> e)
        {
            var job = e.Argument;

            if (job.IsNew)
            {
                _view.Set(new Job(), new List<JobComponentDto>());
                return;
            }

            // load collections
            var documents = job.Documents;

            // obtain lock/check lock
            var @lock = job.ObtainLock(_view.ActiveUser, job.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                job.Connection = Connection;
                job.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                _validator.Messages.Clear();

                foreach (var document in documents)
                {
                    document.Connection = Connection;
                    document.Transaction = Transaction;

                    document.Delete();
                }

                var dbJobComponentDto = new JobComponentDto(job);
                dbJobComponentDto.LoadCollections();

                var dbDeleteShipments = dbJobComponentDto.Shipments;
                var dbDeleteLoadOrders = dbJobComponentDto.LoadOrders;
                var dbDeleteServiceTickets = dbJobComponentDto.ServiceTickets;

                var locks = new List<Lock>(); // locks collections
                var messages = ObtainLocksForDelete(locks, dbDeleteShipments, dbDeleteLoadOrders, dbDeleteServiceTickets);

                if (messages.Any())
                {
                    foreach (var l in locks) l.Delete();
                    _view.DisplayMessages(messages);
                    RollBackTransaction();
                    return;
                }

                var msg = RemoveJobComponentDtos(job, dbDeleteShipments, dbDeleteServiceTickets, dbDeleteLoadOrders);
                if (msg.Type == ValidateMessageType.Warning) messages.Add(msg);

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Job. Number: {1}", AuditLogConstants.Delete, job.JobNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = job.EntityName(),
                    EntityId = job.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                // delete job and lock
                job.Delete();

                // release locks
                foreach (var l in locks) l.Delete();

                @lock.Delete();

                CommitTransaction();

                _view.Set(new Job(), new List<JobComponentDto>());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnUnLock(object sender, ViewEventArgs<Job> e)
        {
            var request = e.Argument;
            var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }
            @lock.Delete();
        }

        private void OnLock(object sender, ViewEventArgs<Job> e)
        {
            var request = e.Argument;
            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private ValidationMessage RemoveJobComponentDtos(Job job, List<Shipment> deleteShipments, IEnumerable<ServiceTicket> deleteServiceTickets, IEnumerable<LoadOrder> deleteLoadOrders)
        {
            var dbJobComponentDto = new JobComponentDto(job);
            dbJobComponentDto.LoadCollections(); // load Shipments, ServiceTickets and LoadOrders from DB

            var shipmentNumbers = deleteShipments.Select(s => s.ShipmentNumber).ToList();
            var dbRelatedLoadOrders = new JobSearch().RelatedLoadOrders(shipmentNumbers, job.TenantId); // load Load Orders related to Shipments

            foreach (var deleteShipment in deleteShipments)
            {
                deleteShipment.Connection = Connection;
                deleteShipment.Transaction = Transaction;

                deleteShipment.TakeSnapShot();

                deleteShipment.JobId = default(long);   // remove link between Job and Shipment
                deleteShipment.JobStep = default(int);

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1}, Number: {2}", AuditLogConstants.Delete, deleteShipment.EntityName(), deleteShipment.ShipmentNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = job.EntityName(),
                    EntityId = job.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
                
                var dbUpdateLoadOrder = dbRelatedLoadOrders.FirstOrDefault(dbr => dbr.LoadOrderNumber == deleteShipment.ShipmentNumber);
                if (dbUpdateLoadOrder != null)
                {
                    // remove link between Job and Load Orders which related to Shipment
                    var message = UpdateRelatedLoadOrder(job, deleteShipment, dbUpdateLoadOrder);
                    if (message.Type == ValidateMessageType.Warning) return message;
                }

                // save Shipment and Delete lock
                deleteShipment.Save();
            }

            foreach (var deleteServiceTicket in deleteServiceTickets)
            {
                deleteServiceTicket.Connection = Connection;
                deleteServiceTicket.Transaction = Transaction;

                deleteServiceTicket.TakeSnapShot();

                deleteServiceTicket.JobId = default(long);    // remove link between Job and Service Ticket
                deleteServiceTicket.JobStep = default(int);

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1}, Number: {2}", AuditLogConstants.Delete, deleteServiceTicket.EntityName(), deleteServiceTicket.ServiceTicketNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = job.EntityName(),
                    EntityId = job.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
                
                // save Service Ticket and Delete lock
                deleteServiceTicket.Save();
            }

            foreach (var deleteLoadOrder in deleteLoadOrders)
            {
                deleteLoadOrder.Connection = Connection;
                deleteLoadOrder.Transaction = Transaction;

                deleteLoadOrder.TakeSnapShot();

                deleteLoadOrder.JobId = default(long);   // remove link between Job and Load Order
                deleteLoadOrder.JobStep = default(int);

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1}, Number: {2}", AuditLogConstants.Delete, deleteLoadOrder.EntityName(), deleteLoadOrder.LoadOrderNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = job.EntityName(),
                    EntityId = job.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();
                
                // save Load Order and Delete lock
                deleteLoadOrder.Save();
            }

            return ValidationMessage.Information(string.Empty);
        }

        private List<ValidationMessage> ProcessJobComponentDtos(Job job, Job dbJob, List<JobComponentDto> jobComponentDtos)
        {
            var dbJobComponentDto = new JobComponentDto(dbJob);
            dbJobComponentDto.LoadCollections();

            var newShipments = jobComponentDtos.Where(jsd => jsd.ComponentType == ComponentType.Shipment).ToList();
            var newLoadOrders = jobComponentDtos.Where(jsd => jsd.ComponentType == ComponentType.LoadOrder).ToList();
            var newServiceTickets = jobComponentDtos.Where(jsd => jsd.ComponentType == ComponentType.ServiceTicket).ToList();

            var shipmentNumbers = newShipments.Select(s => s.ComponentNumber).ToList();
            var dbRelatedLoadOrders = new JobSearch().RelatedLoadOrders(shipmentNumbers, job.TenantId);

            var dbDeleteShipments = dbJobComponentDto.Shipments.Where(dbs => !newShipments.Select(s => s.ComponentNumber).Contains(dbs.ShipmentNumber)).ToList();
            var dbDeleteLoadOrders = dbJobComponentDto.LoadOrders.Where(dbs => !newLoadOrders.Select(s => s.ComponentNumber).Contains(dbs.LoadOrderNumber)).ToList();
            var dbDeleteServiceTickets = dbJobComponentDto.ServiceTickets.Where(dbs => !newServiceTickets.Select(s => s.ComponentNumber).Contains(dbs.ServiceTicketNumber)).ToList();

            var locks = new List<Lock>(); // locks collections
            var messages = ObtainLocksForDelete(locks, dbDeleteShipments, dbDeleteLoadOrders, dbDeleteServiceTickets);

            if (messages.Any())
            {
                foreach (var l in locks) l.Delete();
                return messages;
            }

            var msg = RemoveJobComponentDtos(dbJob, dbDeleteShipments, dbDeleteServiceTickets, dbDeleteLoadOrders);
            if (msg.Type == ValidateMessageType.Warning) messages.Add(msg);

            // release locks
            foreach (var l in locks) l.Delete();

            //var messages = new List<ValidationMessage>();

            foreach (var newShipment in newShipments)
            {
                var shipment = new Shipment(newShipment.ComponentId, true);
                var shipmentLock = shipment.ObtainLock(_view.ActiveUser, shipment.Id); // try to get lock

                // if no lock just show message for user and continue work
                if (!shipmentLock.IsUserLock(_view.ActiveUser))
                {
                    messages.Add(ValidationMessage.Warning(string.Format("Shipment, Number: {0} skipped.", shipment.ShipmentNumber)));
                    continue;
                }

                shipment.TakeSnapShot();

                var isNew = shipment.JobId != job.Id;
                var oldJobStep = shipment.JobStep;

                shipment.JobId = job.Id;
                shipment.JobStep = newShipment.ComponentJobStep;

                if (shipment.HasChanges())
                {
                    var dbUpdateLoadOrder = dbRelatedLoadOrders.FirstOrDefault(dbr => dbr.LoadOrderNumber == shipment.ShipmentNumber);

                    if (dbUpdateLoadOrder != null)
                    {
                        var dbUpdateLoadOrderLock = dbUpdateLoadOrder.ObtainLock(_view.ActiveUser, shipment.Id); // try to get lock

                        // if no lock just show message for user and continue work
                        if (!dbUpdateLoadOrderLock.IsUserLock(_view.ActiveUser))
                        {
                            messages.Add(ValidationMessage.Warning(string.Format("Load Order, Number: {0} skipped.", dbUpdateLoadOrder.LoadOrderNumber)));
                            continue;
                        }

                        // remove link between Job and Load Orders which related to Shipment
                        UpdateRelatedLoadOrder(job, shipment, dbUpdateLoadOrder);

                        // remove lock
                        dbUpdateLoadOrderLock.Delete();
                    }

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1}, Number: {2} Step: {3}", 
                                        isNew ? AuditLogConstants.AddedNew : AuditLogConstants.Update, 
                                        shipment.EntityName(), 
                                        shipment.ShipmentNumber,
                                        isNew ? shipment.JobStep.ToString() : string.Format("Changed from {0} to {1}", oldJobStep, shipment.JobStep)),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = job.EntityName(),
                        EntityId = job.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }

                foreach (var change in shipment.Changes())
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = change,
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                // save
                shipment.Save();

                // remove lock 
                shipmentLock.Delete();
            }

            foreach (var newServiceTicket in newServiceTickets)
            {
                var serviceTicket = new ServiceTicket(newServiceTicket.ComponentId, true);
                var serviceTicketLock = serviceTicket.ObtainLock(_view.ActiveUser, serviceTicket.Id); // try to get lock

                // if no lock just show message for user and continue work
                if (!serviceTicketLock.IsUserLock(_view.ActiveUser))
                {
                    messages.Add(ValidationMessage.Warning(string.Format("Service Ticket, Number: {0} skipped.", serviceTicket.ServiceTicketNumber)));
                    continue;
                }

                serviceTicket.TakeSnapShot();

                var isNew = serviceTicket.JobId != job.Id;
                var oldJobStep = serviceTicket.JobStep;

                serviceTicket.JobId = job.Id;
                serviceTicket.JobStep = newServiceTicket.ComponentJobStep;

                if (serviceTicket.HasChanges())
                {
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1}, Number: {2} Step: {3}",
                                        isNew ? AuditLogConstants.AddedNew : AuditLogConstants.Update,
                                        serviceTicket.EntityName(),
                                        serviceTicket.ServiceTicketNumber,
                                        isNew ? serviceTicket.JobStep.ToString() : string.Format("Changed from {0} to {1}", oldJobStep, serviceTicket.JobStep)),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = job.EntityName(),
                        EntityId = job.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }

                foreach (var change in serviceTicket.Changes())
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = change,
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                // save
                serviceTicket.Save();

                // remove lock 
                serviceTicketLock.Delete();
            }

            foreach (var newLoadOrder in newLoadOrders)
            {
                var loadOrder = new LoadOrder(newLoadOrder.ComponentId, true);

                var loadOrderLock = loadOrder.ObtainLock(_view.ActiveUser, loadOrder.Id); // try to get lock

                // if no lock just show message for user and continue work
                if (!loadOrderLock.IsUserLock(_view.ActiveUser))
                {
                    messages.Add(ValidationMessage.Warning(string.Format("Load Order, Number: {0} skipped.", loadOrder.LoadOrderNumber)));
                    continue;
                }

                loadOrder.TakeSnapShot();

                var isNew = loadOrder.JobId != job.Id;
                var oldJobStep = loadOrder.JobStep;

                loadOrder.JobId = job.Id;
                loadOrder.JobStep = newLoadOrder.ComponentJobStep;

                if (loadOrder.HasChanges())
                {
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1}, Number: {2} Step: {3}",
                                        isNew ? AuditLogConstants.AddedNew : AuditLogConstants.Update,
                                        loadOrder.EntityName(),
                                        loadOrder.LoadOrderNumber,
                                        isNew ? loadOrder.JobStep.ToString() : string.Format("Changed from {0} to {1}", oldJobStep, loadOrder.JobStep)),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = job.EntityName(),
                        EntityId = job.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }

                foreach (var change in loadOrder.Changes())
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = change,
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                // save
                loadOrder.Save();

                // remove lock 
                loadOrderLock.Delete();
            }

            return messages;
        }

        private ValidationMessage UpdateRelatedLoadOrder(Job job, Shipment shipment, LoadOrder dbRelatedLoadOrder)
        {
            var @lock = dbRelatedLoadOrder.ObtainLock(_view.ActiveUser, dbRelatedLoadOrder.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                return ValidationMessage.Warning(ProcessorVars.UnableToObtainLockErrMsg);
            }

            dbRelatedLoadOrder.TakeSnapShot();

            //var isNew = dbRelatedLoadOrder.JobStep != shipment.JobStep;

            dbRelatedLoadOrder.JobStep = shipment.JobStep;
            dbRelatedLoadOrder.JobId = shipment.JobId;

            foreach (var change in dbRelatedLoadOrder.Changes())
                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = change,
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = dbRelatedLoadOrder.EntityName(),
                    EntityId = dbRelatedLoadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

            // save
            dbRelatedLoadOrder.Save();

            @lock.Delete();

            return ValidationMessage.Information(string.Empty);
        }

        private List<ValidationMessage> ObtainLocksForDelete(List<Lock> locks, List<Shipment> dbShipments, List<LoadOrder> dbLoadOrders, List<ServiceTicket> dbServiceTickets)
        {
            var messages = new List<ValidationMessage>();

            foreach (var shipment in dbShipments)
            {
                var shipmentLock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);

                if (!shipmentLock.IsUserLock(_view.ActiveUser))
                {
                    messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
                }
                else
                {
                    locks.Add(shipmentLock);
                }
            }

            foreach (var loadOrder in dbLoadOrders)
            {
                var loadOrderLock = loadOrder.ObtainLock(_view.ActiveUser, loadOrder.Id);

                if (!loadOrderLock.IsUserLock(_view.ActiveUser))
                {
                    messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
                }
                else
                {
                    locks.Add(loadOrderLock);
                }
            }

            foreach (var serviceTicket in dbServiceTickets)
            {
                var serviceTicketLock = serviceTicket.ObtainLock(_view.ActiveUser, serviceTicket.Id);

                if (!serviceTicketLock.IsUserLock(_view.ActiveUser))
                {
                    messages.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
                }
                else
                {
                    locks.Add(serviceTicketLock);
                }
            }

            return messages;
        }

        private void ProcessJobDocuments(Job job, IEnumerable<JobDocument> dbDocuments)
        {
            var dbDelete = dbDocuments.Where(dbs => !job.Documents.Select(s => s.Id).Contains(dbs.Id));

            foreach (var deteteDocument in dbDelete)
            {
                deteteDocument.Connection = Connection;
                deteteDocument.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1}. Job Number: {2}. Document Ref#: {3}", AuditLogConstants.Delete, deteteDocument.EntityName(),
                                      job.JobNumber, deteteDocument.Name),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = job.EntityName(),
                    EntityId = job.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                deteteDocument.Delete();
            }

            foreach (var processDocument in job.Documents)
            {
                processDocument.Connection = Connection;
                processDocument.Transaction = Transaction;

                if (processDocument.IsNew)
                {
                    processDocument.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Job Number: {2} Document Ref#: {3}",
                                                    AuditLogConstants.AddedNew, processDocument.EntityName(), job.JobNumber, processDocument.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = job.EntityName(),
                        EntityId = job.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (processDocument.HasChanges())
                {
                    foreach (var change in processDocument.Changes())
                    {
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Document Ref#: {1} {2}", processDocument.EntityName(), processDocument.Name, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = job.EntityName(),
                            EntityId = job.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                        processDocument.Save();
                    }
                }
            }
        }

        private void FetchCustomerForNonEmployee(string customerNumber)
        {
            if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
            {
                _view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
                return;
            }
            var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
            _view.DisplayCustomer(shipAs == null || !shipAs.Customer.Active ? null : shipAs.Customer);
            if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
            else if (!shipAs.Customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
        }

        private void FetchCustomerForEmployee(string customerNumber)
        {
            var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

            _view.DisplayCustomer(customer == null || !customer.Active ? new Customer() : customer);
            if (customer == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
            else if (!customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
        }
    }
}