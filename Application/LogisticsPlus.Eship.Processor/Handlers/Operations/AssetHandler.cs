﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class AssetHandler : EntityBase
	{
		private readonly IAssetView _view;
		private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();
		private readonly AssetValidator _validator = new AssetValidator();

		public AssetHandler(IAssetView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Loading += OnLoading;
			_view.Save += OnSave;
			_view.Delete += OnDelete;
			_view.Search += OnSearch;
			_view.UnLock += OnUnLock;
			_view.Lock += OnLock;
			_view.BatchImport += OnBatchImport;
		}

		private void OnBatchImport(object sender, ViewEventArgs<List<Asset>> e)
		{
			var messages = new ValidationMessageCollection();

			foreach (var asset in e.Argument)
			{
				// assign new number
				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.AssetNumber, _view.ActiveUser);
				asset.AssetNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();

				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					asset.Connection = Connection;
					asset.Transaction = Transaction;

					if (!_validator.IsValid(asset))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					asset.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description =
							string.Format("{0} Asset#:{1} Type:{2} Desc:{3}", AuditLogConstants.AddedNew, asset.AssetNumber, asset.AssetType, asset.Description),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = asset.EntityName(),
						EntityId = asset.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					CommitTransaction();

					// cache item
					ProcessorVars.RegistryCache[asset.TenantId].UpdateCache(asset);
				}
				catch (Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
				    messages.Add(ValidationMessage.Error("{0} [Asset: {1} {2}] Err: {3}.",
				                                         ProcessorVars.RecordSaveErrMsg,
				                                         asset.Description,
				                                         asset.AssetType,
				                                         ex.Message));
				}
			}
			messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
			_view.DisplayMessages(messages);
		}

		private void OnUnLock(object sender, ViewEventArgs<Asset> e)
		{
			var asset = e.Argument;
			var @lock = asset.RetrieveLock(_view.ActiveUser, asset.Id);
			if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
		}

		private void OnLock(object sender, ViewEventArgs<Asset> e)
		{
			var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}

			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
		{
            var results = new AssetSearch().FetchAssets(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void OnDelete(object sender, ViewEventArgs<Asset> e)
		{
			_validator.Messages.Clear();

			var asset = e.Argument;

			if (asset.IsNew) return;

			var @lock = asset.ObtainLock(_view.ActiveUser, asset.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();

			try
			{
				asset.Connection = Connection;
				asset.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteAsset(asset))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} Asset #:{1} Type:{2} Desc:{3}", AuditLogConstants.Delete, asset.AssetNumber, asset.AssetType, asset.Description),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = asset.EntityName(),
					EntityId = asset.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				asset.Delete();
				@lock.Delete();

				CommitTransaction();

				// drop from cache
				ProcessorVars.RegistryCache[asset.TenantId].UpdateCache(asset, true);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnSave(object sender, ViewEventArgs<Asset> e)
		{
			var asset = e.Argument;

			if (!asset.IsNew)
			{
				var @lock = asset.RetrieveLock(_view.ActiveUser, asset.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// assign new numbers
			if (asset.IsNew)
			{
				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.AssetNumber, _view.ActiveUser);
				asset.AssetNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
			}

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				asset.Connection = Connection;
				asset.Transaction = Transaction;

				if (!_validator.IsValid(asset))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (asset.IsNew)
				{
					asset.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Asset #:{1} Type:{2} Desc:{3}", AuditLogConstants.AddedNew, asset.AssetNumber, asset.AssetType, asset.Description),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = asset.EntityName(),
						EntityId = asset.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (asset.HasChanges())
				{
					foreach (var change in asset.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = asset.EntityName(),
								EntityId = asset.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();
				
					asset.Save();
				}

				CommitTransaction();

				// cache item
				ProcessorVars.RegistryCache[asset.TenantId].UpdateCache(asset);

                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnLoading(object sender, EventArgs e)
		{
			var rateTypes = ProcessorUtilities.GetAll<RateType>();
			rateTypes.Remove(RateType.PerHundredWeight.ToInt());
			rateTypes.Remove(RateType.LineHaulPercentage.ToInt());

			_view.SearchAssetTypes = ProcessorUtilities.GetAll<AssetType>();
		}
	}
}
