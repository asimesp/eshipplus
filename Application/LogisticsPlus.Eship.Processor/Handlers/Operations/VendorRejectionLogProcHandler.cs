﻿using System;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class VendorRejectionLogProcHandler : EntityBase
	{
		private readonly IVendorRejectionLogProcView _view;
		private readonly VendorRejectionLogValidator _validator = new VendorRejectionLogValidator();

		public VendorRejectionLogProcHandler(IVendorRejectionLogProcView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.LogVendorRejection += OnLogVendorRejection;
		}

		private void OnLogVendorRejection(object sender, ViewEventArgs<VendorRejectionLog> e)
		{
			var log = e.Argument;

			if (!log.IsNew)
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error("Vendor rejection log must be new!") });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				log.Connection = Connection;
				log.Transaction = Transaction;

				// validate loadOrder
				_validator.Messages.Clear();
				if (!_validator.IsValid(log))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// save
				log.Save();

				// commit transaction
				CommitTransaction();

				_view.DisplayMessages(new[] {ValidationMessage.Information("Vendor rejection logged.")});
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}
	}
}
