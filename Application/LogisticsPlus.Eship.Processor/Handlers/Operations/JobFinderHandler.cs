﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class JobFinderHandler : EntityBase
    {
        private readonly IJobFinderView _view;

        public JobFinderHandler(IJobFinderView view) { _view = view; }

        public void Initialize()
        {
            _view.Search += OnSearch;
           
        }

        private void OnSearch(object sender, ViewEventArgs<JobViewSearchCriteria> e)
        {
            var results = new JobSearch().FetchJobDashboardDtos(e.Argument, _view.ActiveUser.TenantId);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

            _view.DisplaySearchResult(results);
        }

       
    }
}