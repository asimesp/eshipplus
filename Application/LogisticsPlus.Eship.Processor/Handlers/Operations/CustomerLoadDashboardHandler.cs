﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class CustomerLoadDashboardHandler : EntityBase
	{
		private readonly ICustomerLoadDashboardView _view;

		public CustomerLoadDashboardHandler(ICustomerLoadDashboardView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
			_view.VoidShipment += OnVoidShipment;
		}

		private void OnVoidShipment(object sender, ViewEventArgs<Shipment> e)
		{
			var shipment = e.Argument;

			// check locks
			var @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[]
				                      	{
				                      		ValidationMessage.Error("Unable to obtain exclusive access to shipment #{0} for cancel process.",
				                      			e.Argument.ShipmentNumber)
				                      	});
				return;
			}

			// void process
			shipment.TakeSnapShot();
			shipment.Status = ShipmentStatus.Void;

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				shipment.Connection = Connection;
				shipment.Transaction = Transaction;

				foreach (var change in shipment.Changes())
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = change,
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = shipment.EntityName(),
						EntityId = shipment.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

				// save
				shipment.Save();

				// commit transaction
				CommitTransaction();

				_view.ProcessVoidComplete(shipment);
			}
			catch (Exception ex)
			{
				RollBackTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

		private void OnSearch(object sender, ViewEventArgs<ShipmentViewSearchCriteria> e)
		{
			var results = new ShipmentSearch().FetchLoadsShipmentsDashboardDto(e.Argument, _view.ActiveUser.TenantId, true);
            _view.DisplaySearchResult(results);
            if (results.Count == 0 && !e.Argument.DisableNoRecordNotification)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
		}
	}
}
