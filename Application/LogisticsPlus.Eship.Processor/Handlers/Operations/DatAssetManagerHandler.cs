﻿using System;
using LogisticsPlus.Eship.Dat;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class DatAssetManagerHandler : EntityBase
    {
        private readonly IDatAssetManagerView _view;

        public DatAssetManagerHandler(IDatAssetManagerView view)
		{
			_view = view;
		}

		public void Initialize()
		{
		    _view.Loading += OnLoading;
		}

        private void OnLoading(object sender, EventArgs e)
        {
            _view.RateBasedOnTypes = ProcessorUtilities.GetAll<RateBasedOnType>();
        }

    }
}
