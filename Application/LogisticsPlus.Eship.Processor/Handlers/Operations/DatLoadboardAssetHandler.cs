﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Dat;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class DatLoadboardAssetHandler : EntityBase
	{
		private readonly IDatLoadboardAssetView _view;

        public DatLoadboardAssetHandler(IDatLoadboardAssetView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
			_view.CheckDatDelete += CheckAssetAreDeletedFromDat;
			_view.CheckDatUpdate += CheckAssetAreUpdatedInDat;
		}


		private void OnSearch(object sender, ViewEventArgs<DatAssetSearchCriteria> e)
		{
            var results = new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssets(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
		}

		private void CheckAssetAreDeletedFromDat(object sender, ViewEventArgs<DatLoadboardAssetPosting,bool> e)
		{
			var datPosting = e.Argument;
		    var forceDelete = e.Argument2;

			if (!datPosting.IsNew)
			{

			    if (!_view.ActiveUser.HasDatCredentials)
			    {
			        _view.DisplayMessages(new[]
			            {
			                ValidationMessage.Error("You do not have permission to delete DAT Asset Id {0}.",
			                                        datPosting.AssetId)
			            });
			    }

			    else
                {
                    var @lock = datPosting.ObtainLock(_view.ActiveUser, datPosting.Id);

                    if (!@lock.IsUserLock(_view.ActiveUser))
                    {
                        var ex = new Exception(ProcessorVars.UnableToObtainLockErrMsg);
                        _view.DisplayMessages(new[]
			                    {
			                        ValidationMessage.Error(ex.Message)
			                    });
                        return;
                    }

                    Connection = DatabaseConnection.DefaultConnection;
                    BeginTransaction();
			        try
			        {
			            datPosting.Connection = Connection;
			            datPosting.Transaction = Transaction;

			            @lock.Connection = Connection;
			            @lock.Transaction = Transaction;

			            var connexionService = new Connexion(_view.ActiveUser.DatLoadboardUsername,
			                                                 _view.ActiveUser.DatLoadboardPassword);
			            var response = connexionService.DeleteAsset(new DeleteAssetRequest
			                {
			                    deleteAssetOperation =
			                        new DeleteAssetOperation
			                            {
			                                Item = new DeleteAssetsByAssetIds {assetIds = new[] {datPosting.AssetId}}
			                            }
			                });

			            if (response.HasError && !forceDelete)
			            {
                            RollBackTransaction();
                            @lock.Delete();
			                _view.DatRemovalError();
			            }
			            else
			            {
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description =
                                    string.Format("{0} DAT Asset Id: {1}", AuditLogConstants.Delete, datPosting.AssetId),
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = datPosting.EntityName(),
                                EntityId = datPosting.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();


                            datPosting.Delete();
                            @lock.Delete();
                            CommitTransaction();
			            }
			        }
                    catch (Exception ex)
                    {
                        RollBackTransaction();
                        @lock.Delete();
                        _view.LogException(ex);
                        _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
                    }
			    }
			}
		}

		private void CheckAssetAreUpdatedInDat(object sender, ViewEventArgs<LoadOrder> e)
		{
			var loadOrder = e.Argument;
			var datPosting = new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(loadOrder.LoadOrderNumber, loadOrder.TenantId);

			    if (datPosting != null)
                {
                    if (!_view.ActiveUser.HasDatCredentials)
                        _view.DisplayMessages(new[]
                            {
                                ValidationMessage.Error("You do not have permission to update DAT Asset Id {0}.",
                                                        datPosting.AssetId)
                            });

                    else
                    {
                        var @lock = datPosting.ObtainLock(_view.ActiveUser, datPosting.Id);
                        if (!@lock.IsUserLock(_view.ActiveUser))
                        {
                            _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                            return;
                        }

                        Connection = DatabaseConnection.DefaultConnection;
                        BeginTransaction();
                        try
                        {
                            datPosting.Connection = Connection;
                            datPosting.Transaction = Transaction;

                            var service = new Connexion(_view.ActiveUser.DatLoadboardUsername,
                                                        _view.ActiveUser.DatLoadboardPassword);

                            var requestRate = new ShipmentRate
                                {
                                    rateMiles = loadOrder.Mileage.ToInt()
                                };

                            var request = new UpdateAssetRequest
                                {
                                    updateAssetOperation = new UpdateAssetOperation
                                        {
                                            Item = datPosting.AssetId,
                                            ItemElementName = ItemChoiceType.assetId,
                                            Item1 = new ShipmentUpdate
                                                {
                                                    ltl = loadOrder.ServiceMode == ServiceMode.LessThanTruckload,
                                                    ltlSpecified = true,
                                                    stops = 1 + loadOrder.Stops.Count,
                                                    stopsSpecified = true,
                                                    rate = requestRate,
                                                    comments = datPosting.Comments.ToArray()
                                                },

                                        }
                                };

                            var response = service.UpdateAsset(request);

                            if (response.HasError)
                            {
                                RollBackTransaction();
                                @lock.Delete();
                                _view.DisplayMessages(new[]
			                        {
			                            ValidationMessage.Error("{0}: {1}", response.Error.message, response.Error.detailedMessage)
			                        });
                                
                            }
                            else
                            {
                                datPosting.Mileage = loadOrder.Mileage;

                                foreach (var change in datPosting.Changes())
                                    new AuditLog
                                    {
                                        Connection = Connection,
                                        Transaction = Transaction,
                                        Description = change,
                                        TenantId = _view.ActiveUser.TenantId,
                                        User = _view.ActiveUser,
                                        EntityCode = datPosting.EntityName(),
                                        EntityId = datPosting.Id.ToString(),
                                        LogDateTime = DateTime.Now
                                    }.Log();

                                datPosting.Save();

                                // release lock
                                @lock.Delete();

                                CommitTransaction();
                            }
                        }
                        catch (Exception ex)
                        {
                            RollBackTransaction();
                            _view.LogException(ex);
                            _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
                        }
                    }
                }
		}

	}
}
