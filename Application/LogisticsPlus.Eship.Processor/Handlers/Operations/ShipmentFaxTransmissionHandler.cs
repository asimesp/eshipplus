﻿using System;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class ShipmentFaxTransmissionHandler : EntityBase
	{
		public ValidationMessage[] SaveFaxTransmission(FaxTransmission transmission)
		{
			try
			{
				var validator = new FaxTransmissionValidator();
				if (!validator.IsValid(transmission)) return validator.Messages.ToArray();
				transmission.Save();
				return new ValidationMessage[0];
			}
			catch (Exception ex)
			{
				return new[] {ValidationMessage.Error("Processing Error: Update Transmission. Err: {0}", ex.Message)};
			}
		}
	}
}
