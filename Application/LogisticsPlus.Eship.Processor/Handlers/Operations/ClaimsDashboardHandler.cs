﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class ClaimsDashboardHandler : EntityBase
    {
         private readonly IClaimsDashboardView _view;

         public ClaimsDashboardHandler(IClaimsDashboardView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<ClaimViewSearchCriteria> e)
		{
			var results = new ClaimSearch().FetchClaimDtos(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);
			if (results.Count == 0)
				_view.DisplayMessages(new[] {ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg)});
		}
    }
}
