﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class AddressBookHandler : EntityBase
    {
        private readonly IAddressBookView _view;
        private readonly AddressBookValidator _validator = new AddressBookValidator();

        public AddressBookHandler(IAddressBookView view)
        {
            _view = view;
        }
		
        public void Initialize()
        {
            _view.LoadAuditLog += OnLoadAuditLog;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
        	_view.BatchImport += OnBatchImport;
        	_view.CustomerSearch += OnCustomerSearch;
        }

		private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
		{
			if (!_view.ActiveUser.TenantEmployee)
				FetchCustomerForNonEmployee(e.Argument);
			else
				FetchCustomerForEmployee(e.Argument);
		}

        private void OnBatchImport(object sender, ViewEventArgs<List<AddressBook>> e)
		{
			var messages = new ValidationMessageCollection();

			foreach (var addressBook in e.Argument)
			{
				_validator.Messages.Clear();
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					_validator.Connection = Connection;
					_validator.Transaction = Transaction;

					addressBook.Connection = Connection;
					addressBook.Transaction = Transaction;
                                      
                    
					if (!_validator.IsValid(addressBook))
					{
						RollBackTransaction();
						messages.AddRange(_validator.Messages);
						continue;
					}

					addressBook.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref #:{1}", AuditLogConstants.AddedNew, addressBook.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = addressBook.EntityName(),
							EntityId = addressBook.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					ProcessAddressBookContacts(addressBook, new List<AddressBookContact>());
					ProcessAddressBookServices(addressBook, new List<AddressBookService>());

					CommitTransaction();
				}
				catch (Exception ex)
				{
					RollBackTransaction();
                    _view.LogException(ex);
					_view.DisplayMessages(new[] { ValidationMessage.Error("An error occurred while saving record.") });
				}
			}
			messages.Add(ValidationMessage.Information("Import process complete"));
			_view.DisplayMessages(messages);
		}

        private void OnLoadAuditLog(object sender, ViewEventArgs<AddressBook> e)
        {
            _view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id, e.Argument.TenantId));
        }

		private void OnSave(Object sender, ViewEventArgs<AddressBook> e)
		{
			var addressBook = e.Argument;

			if (!addressBook.IsNew)
			{
				var @lock = addressBook.RetrieveLock(_view.ActiveUser, addressBook.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg)});
					return;
				}
			}

			//pull db record
			var dbAddressBook = new AddressBook(addressBook.Id);
			dbAddressBook.LoadCollections();

			_validator.Messages.Clear();
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				addressBook.Connection = Connection;
				addressBook.Transaction = Transaction;

				if (!_validator.IsValid(addressBook))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				if (addressBook.IsNew)
				{
					addressBook.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Ref #:{1}", AuditLogConstants.AddedNew, addressBook.Id),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = addressBook.EntityName(),
							EntityId = addressBook.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (addressBook.HasChanges())
				{
					foreach (var change in addressBook.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = change,
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = addressBook.EntityName(),
								EntityId = addressBook.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					addressBook.Save();
				}

				ProcessAddressBookContacts(addressBook, dbAddressBook.Contacts);
				ProcessAddressBookServices(addressBook, dbAddressBook.Services);

				CommitTransaction();

				_view.Set(addressBook);
				_view.DisplayMessages(new[] {ValidationMessage.Information("Record Saved")});

			}
			catch (Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
				_view.DisplayMessages(new[] {ValidationMessage.Error("An error occurred while saving record.")});
			}

		}

    	private void OnDelete(object sender, ViewEventArgs<AddressBook> e)
        {
            var addressBook = e.Argument;

            if (addressBook.IsNew) return;

            addressBook.LoadCollections();

            var @lock = addressBook.ObtainLock(_view.ActiveUser, addressBook.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                addressBook.Connection = Connection;
                addressBook.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                _validator.Messages.Clear();

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
					Description = string.Format("{0} Ref #:{1} Description: {2}", AuditLogConstants.Delete, addressBook.Id, addressBook.Description),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = addressBook.EntityName(),
                    EntityId = addressBook.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                foreach (var service in addressBook.Services)
                {
                    service.Connection = Connection;
                    service.Transaction = Transaction;
                    service.Delete();
                }

                foreach (var contact in addressBook.Contacts)
                {
                    contact.Connection = Connection;
                    contact.Transaction = Transaction;
                    contact.Delete();
                }

                addressBook.Delete();

                @lock.Delete();
                CommitTransaction();

                _view.Set(new AddressBook());
                _view.DisplayMessages(new[] { ValidationMessage.Information("Record Deleted.") });
            }
            catch(Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error("An error occurred while deleting record.") });
            }
        }

    	private void OnUnLock(object sender, ViewEventArgs<AddressBook> e)
        {
            var addressBook = e.Argument;
            var @lock = addressBook.RetrieveLock(_view.ActiveUser, addressBook.Id);
            if (@lock == null) return;
			if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
			else _view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg)});
        }

    	private void OnLock(object sender, ViewEventArgs<AddressBook> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }


		private void FetchCustomerForNonEmployee(string customerNumber)
		{
			if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
			{
				_view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
				return;
			}
			var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
			if (shipAs == null)
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
				_view.DisplayCustomer(new Customer());
			}
			else _view.DisplayCustomer(shipAs.Customer);
		}

		private void FetchCustomerForEmployee(string customerNumber)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

			_view.DisplayCustomer(customer);
			if (customer == null)
				_view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
		}


    	private void ProcessAddressBookServices(AddressBook addressBook, IEnumerable<AddressBookService> dbServices)
    	{
    		var dbDelete = dbServices.Where(u => !addressBook.Services.Select(c => c.ServiceId).Contains(u.ServiceId));

    		foreach (var service in dbDelete)
    		{
    			service.Connection = Connection;
    			service.Transaction = Transaction;

    			new AuditLog
    				{
    					Connection = Connection,
    					Transaction = Transaction,
    					Description =
    						string.Format("{0} {1} Service Ref #: {2} ", AuditLogConstants.Delete,
    						              service.EntityName(), service.ServiceId),
    					TenantId = _view.ActiveUser.TenantId,
    					User = _view.ActiveUser,
    					EntityCode = addressBook.EntityName(),
    					EntityId = addressBook.Id.ToString(),
    					LogDateTime = DateTime.Now
    				}.Log();

    			service.Delete();
    		}

    		foreach (var service in addressBook.Services)
				if (!_validator.AddressBookServiceExists(service))
				{
					service.Connection = Connection;
					service.Transaction = Transaction;

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description =
								string.Format("{0} {1} Service Ref #: {2} ", AuditLogConstants.AddedNew,
								              service.EntityName(), service.ServiceId),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = addressBook.EntityName(),
							EntityId = addressBook.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					service.Save();
				}
    	}

    	private void ProcessAddressBookContacts(AddressBook addressBook, IEnumerable<AddressBookContact> dbAddressBookContacts)
    	{
    		var dbDelete = dbAddressBookContacts.Where(c => !addressBook.Contacts.Select(csr => csr.Id).Contains(c.Id));

    		foreach (var contact in dbDelete)
    		{
    			contact.Connection = Connection;
    			contact.Transaction = Transaction;

    			new AuditLog
    				{
    					Connection = Connection,
    					Transaction = Transaction,
    					Description = string.Format("{0} {1} Contact Name: {2} ", AuditLogConstants.Delete,
    					                            contact.EntityName(), contact.Name),
    					TenantId = _view.ActiveUser.TenantId,
    					User = _view.ActiveUser,
    					EntityCode = addressBook.EntityName(),
    					EntityId = addressBook.Id.ToString(),
    					LogDateTime = DateTime.Now
    				}.Log();

    			contact.Delete();
    		}

			foreach (var contact in addressBook.Contacts)
			{
				contact.Connection = Connection;
				contact.Transaction = Transaction;

				contact.AddressBook = addressBook;

				if (contact.IsNew)
				{
					contact.Save();

					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1} Contact Name: {2} ", AuditLogConstants.AddedNew,
							                            contact.EntityName(), contact.Name),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = addressBook.EntityName(),
							EntityId = addressBook.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (contact.HasChanges())
				{
					foreach (var change in contact.Changes())
						new AuditLog
							{
								Connection = Connection,
								Transaction = Transaction,
								Description = string.Format("{0} {1}", contact.EntityName(), change),
								TenantId = _view.ActiveUser.TenantId,
								User = _view.ActiveUser,
								EntityCode = addressBook.EntityName(),
								EntityId = addressBook.Id.ToString(),
								LogDateTime = DateTime.Now
							}.Log();

					contact.Save();
				}
			}
    	}
    }
}

