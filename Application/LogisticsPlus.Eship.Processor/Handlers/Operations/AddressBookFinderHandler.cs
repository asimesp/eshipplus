﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class AddressBookFinderHandler : EntityBase
	{
		private readonly IAddressBookFinderView _view;

		public AddressBookFinderHandler(IAddressBookFinderView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
		    _view.DeleteSelected += OnDeleteSelected;
		}

	    private void OnDeleteSelected(object sender, ViewEventArgs<List<AddressBook>> e)
	    {
	        var messages = new List<ValidationMessage>();
	        foreach (var addressBook in e.Argument)
	        {
                if (addressBook.IsNew) return;

                addressBook.LoadCollections();

                var @lock = addressBook.ObtainLock(_view.ActiveUser, addressBook.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    messages.Add(ValidationMessage.Error(string.Format("{0} Record Description: {1}", ProcessorVars.UnableToObtainLockErrMsg, addressBook.Description )));
                    continue;
                }

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    addressBook.Connection = Connection;
                    addressBook.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} Ref #:{1} Description: {2}", AuditLogConstants.Delete, addressBook.Id, addressBook.Description),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = addressBook.EntityName(),
                        EntityId = addressBook.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    foreach (var service in addressBook.Services)
                    {
                        service.Connection = Connection;
                        service.Transaction = Transaction;
                        service.Delete();
                    }

                    foreach (var contact in addressBook.Contacts)
                    {
                        contact.Connection = Connection;
                        contact.Transaction = Transaction;
                        contact.Delete();
                    }

                    addressBook.Delete();

                    @lock.Delete();
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(
                        ValidationMessage.Error(
                            "An error occurred while deleting record. [Ref #: {0} Record Description: {1}] Err: {2}",
                            addressBook.Id,
                            addressBook.Description,
                            ex.Message));
                }
	        }

	        messages.Add(ValidationMessage.Information("Record(s) Deleted"));
	        _view.ProcessSuccessfulDelete();
	        _view.DisplayMessages(messages);
	    }

	   private void OnSearch(object sender, ViewEventArgs<AddressBookViewSearchCriteria> e)
		{
			var results = new AddressBookViewSearchDto()
				.FetchAddressBookDtos(_view.ActiveUser.TenantId, e.Argument);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}
	}
}
