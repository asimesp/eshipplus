﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class ServiceTicketDashboardHandler : EntityBase
    {
        private readonly IServiceTicketDashboardView _view;

        public ServiceTicketDashboardHandler(IServiceTicketDashboardView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<ServiceTicketViewSearchCriteria> e)
        {
            var results = new ServiceTicketSearch().FetchServiceTicketDashboardDtos(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
        }
    }
}
