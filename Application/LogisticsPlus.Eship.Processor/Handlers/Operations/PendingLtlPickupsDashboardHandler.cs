﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class PendingLtlPickupsDashboardHandler : EntityBase
    {
        private readonly IPendingLtlPickupsDashboardView _view;
        private readonly ShipmentValidator _validator = new ShipmentValidator();

        public PendingLtlPickupsDashboardHandler(IPendingLtlPickupsDashboardView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
            _view.UpdateShipment += OnUpdateShipment;
            _view.Lock += OnLock;
            _view.UnLock += OnUnLock;
        }

        private void OnLock(object sender, ViewEventArgs<Shipment> e)
        {
            var request = e.Argument;
            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnUnLock(object sender, ViewEventArgs<Shipment> e)
        {
            var request = e.Argument;
            var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] {ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg)});
                return;
            }
            
            @lock.Delete();
        }

        private void OnUpdateShipment(object sender, ViewEventArgs<Shipment> e)
        {
            var shipment = e.Argument;

            if (shipment.IsNew)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot save non-existing shipment") });
                return;
            }

            Lock @lock;
            try
            {
                @lock = shipment.ObtainLock(_view.ActiveUser, shipment.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.FailedLock(@lock);
                    return;
                }
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                shipment.Connection = Connection;
                shipment.Transaction = Transaction;

                // validate shipment
                _validator.Messages.Clear();
                if (!_validator.ShipmentCheckCallsAreValid(shipment.CheckCalls) || !_validator.CheckCallTimesAreValid(shipment.CheckCalls, shipment))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                // check call addition
                foreach (var checkCall in shipment.CheckCalls)
                {
                    checkCall.Connection = Connection;
                    checkCall.Transaction = Transaction;

                    if (checkCall.IsNew)
                    {
                        checkCall.Save();

                        new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description =
                                    string.Format("{0} {1} Shipment Number:{2} CheckCall Ref#:{3}",
                                                  AuditLogConstants.AddedNew,
                                                  checkCall.EntityName(), shipment.ShipmentNumber, checkCall.Id),
                                TenantId = shipment.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = shipment.EntityName(),
                                EntityId = shipment.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();
                    }
                }

                // release lock
                @lock.Delete();

                // commit transaction
                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnSearch(object sender, ViewEventArgs<PendingLtlPickupViewSearchCriteria> e)
        {
            var results = new ShipmentSearch().FetchShipmentPickupDashboardDto(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
            if (results.Count == 0) _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
        }


    }
}
