﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class JobDashboardHandler : EntityBase
    {
        private readonly IJobDashboardView _view;
        private readonly JobValidator _jobValidator = new JobValidator();
        private readonly ShipmentValidator _shipmentValidator = new ShipmentValidator();
        private readonly LoadOrderValidator _loadValidator = new LoadOrderValidator();
        private readonly ServiceTicketValidator _serviceTicketValidator = new ServiceTicketValidator();


        private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

        private List<ValidationMessage> ErrorMessages = new List<ValidationMessage>();

        private List<Customer> alteredCustomers = new List<Customer>();
        private List<CustomerPurchaseOrder> alteredPurchaseOrders = new List<CustomerPurchaseOrder>();

        public JobDashboardHandler(IJobDashboardView view)
        {
            _view = view;
        }

        public void Initialize()
        {
            _view.Search += OnSearch;
            _view.BatchImport += OnBatchImport;
            _view.UnLinkEntitiesFromJob += OnUnLinkEntities;
        }

        private void OnSearch(object sender, ViewEventArgs<JobViewSearchCriteria> e)
        {
            var results = new JobSearch().FetchJobDashboardDtos(e.Argument, _view.ActiveUser.TenantId);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

            _view.DisplaySearchResult(results);
        }

        private void OnBatchImport(object sender, ViewEventArgs<Tuple<List<Job>, List<Shipment>, List<LoadOrder>, List<ServiceTicket>>> e)
        {
            var customerLocks = new List<Lock>();
            var purchaseOrderLocks = new List<Lock>();
            var jobLocks = new List<Lock>();

            try
            {
                var batchTuple = e.Argument;

                var jobs = batchTuple.Item1;
                var shipments = batchTuple.Item2;
                var shipmentsWithHistoricalData = new List<Tuple<Shipment, ShipmentHistoricalData>>();
                var loadOrders = batchTuple.Item3;
                var loadOrdersWithHistoricalData = new List<Tuple<LoadOrder, LoadOrderHistoricalData>>();
                var serviceTickets = batchTuple.Item4;

                // Process auto-numbers (we can't do this inside the "Process.." methods as a second BeginTransaction() would be called)
                foreach (var job in jobs)
                {
                    if (job.IsNew)
                    {
                        var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.JobNumber, _view.ActiveUser);
                        job.JobNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
                    }
                    else
                    {
                        var jobLock = job.ObtainLock(_view.ActiveUser, job.Id);

                        if (!jobLock.IsUserLock(_view.ActiveUser))
                        {
                            ErrorMessages.Insert(0, ValidationMessage.Error("An error occurred while saving Jobs: "));
                            _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg), });
                            return;
                        }

                        jobLocks.Add(jobLock);
                    }
                }

                foreach (var shipment in shipments)
                {
                    var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ShipmentNumber, _view.ActiveUser);
                    shipment.ShipmentNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();

                    // retrieve shipment historical data
                    var historicalData = new ShipmentHistoricalData
                                         {
                                             Shipment = shipment,
                                             WasInDispute = shipment.InDispute,
                                             WasInDisputeReason = shipment.InDisputeReason,
                                             OriginalEstimatedDeliveryDate = shipment.EstimatedDeliveryDate,
                                             OriginalEstimatedPickupDate = shipment.DesiredPickupDate,
                                         };
                    shipmentsWithHistoricalData.Add(new Tuple<Shipment, ShipmentHistoricalData>(shipment, historicalData));
                }

                foreach (var loadOrder in loadOrders)
                {
                    var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ShipmentNumber, _view.ActiveUser);
                    loadOrder.LoadOrderNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();

                    // retrieve load order historical data
                    var historicalData = new LoadOrderHistoricalData
                                         {
                                             LoadOrder = loadOrder,
                                             WasAccepted = loadOrder.Status == LoadOrderStatus.Accepted,
                                             WasCancelled = loadOrder.Status == LoadOrderStatus.Cancelled,
                                             WasCovered = loadOrder.Status == LoadOrderStatus.Covered,
                                             WasLost = loadOrder.Status == LoadOrderStatus.Lost,
                                             WasOffered = loadOrder.Status == LoadOrderStatus.Offered,
                                             WasQuoted = loadOrder.Status == LoadOrderStatus.Quoted,
                                             WasShipment = loadOrder.Status == LoadOrderStatus.Shipment
                                         };

                    loadOrdersWithHistoricalData.Add(new Tuple<LoadOrder, LoadOrderHistoricalData>(loadOrder, historicalData));
                }

                foreach (var serviceTicket in serviceTickets)
                {
                    var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.ServiceTicketNumber, _view.ActiveUser);
                    serviceTicket.ServiceTicketNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
                }

                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();

                // link up validators
                _jobValidator.Connection = Connection;
                _jobValidator.Transaction = Transaction;

                _shipmentValidator.Connection = Connection;
                _shipmentValidator.Transaction = Transaction;

                _loadValidator.Connection = Connection;
                _loadValidator.Transaction = Transaction;

                _serviceTicketValidator.Connection = Connection;
                _serviceTicketValidator.Transaction = Transaction;

                ProcessJobs(jobs, shipments, loadOrders, serviceTickets);
                if (ErrorMessages.Any())
                {
                    RollBackTransaction();

                    ErrorMessages.Insert(0, ValidationMessage.Error("An error occurred while saving Jobs: "));
                    _view.DisplayMessages(ErrorMessages);
                    return;
                }

                ProcessShipments(shipmentsWithHistoricalData);
                if (ErrorMessages.Any())
                {
                    RollBackTransaction();

                    ErrorMessages.Insert(0, ValidationMessage.Error("An error occurred while saving Shipments: "));
                    _view.DisplayMessages(ErrorMessages);
                    return;
                }

                ProcessLoadOrders(loadOrdersWithHistoricalData);
                if (ErrorMessages.Any())
                {
                    RollBackTransaction();

                    ErrorMessages.Insert(0, ValidationMessage.Error("An error occurred while saving Load Orders: "));
                    _view.DisplayMessages(ErrorMessages);
                    return;
                }

                ProcessServiceTickets(serviceTickets);
                if (ErrorMessages.Any())
                {
                    RollBackTransaction();

                    ErrorMessages.Insert(0, ValidationMessage.Error("An error occurred while saving Service Tickets: "));
                    _view.DisplayMessages(ErrorMessages);
                    return;
                }

                customerLocks = alteredCustomers.Select(c => c.ObtainLock(_view.ActiveUser, c.Id)).ToList();
                if (customerLocks.Any(c => !c.IsUserLock(_view.ActiveUser)))
                {
                    RollBackTransaction();
                    ErrorMessages.Insert(0, ValidationMessage.Error("An error occurred while saving Customer records: "));
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg), });
                    return;
                }

                purchaseOrderLocks = alteredPurchaseOrders.Select(po => po.ObtainLock(_view.ActiveUser, po.Customer.Id)).ToList();
                if (purchaseOrderLocks.Any(c => !c.IsUserLock(_view.ActiveUser)))
                {
                    RollBackTransaction();
                    ErrorMessages.Insert(0, ValidationMessage.Error("An error occurred while saving Customer records: "));
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg), });
                    return;
                }

                // Save customers whose ShipperBillOfLadingSeed was altered
                foreach (var customer in alteredCustomers)
                {
                    // audit log customer
                    foreach (var change in customer.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = customer.EntityName(),
                            EntityId = customer.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save customer
                    customer.Save();
                }

                // Save purchase orders whose CurrentUses was altered
                foreach (var purchaseOrder in alteredPurchaseOrders)
                {
                    // audit log purchase order
                    foreach (var change in purchaseOrder.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = purchaseOrder.EntityName(),
                            EntityId = purchaseOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save purchase order
                    purchaseOrder.Save();
                }

                customerLocks.ForEach(l => l.Delete());
                purchaseOrderLocks.ForEach(l => l.Delete());
                jobLocks.ForEach(l => l.Delete());

                // commit transaction
                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg) });


                _view.ProcessNotifications(shipments, loadOrders);

                var jobString = jobs.Select(j => j.JobNumber.WrapSingleQuotes()).ToArray().CommaJoin();
                _view.SearchForImportedJobs(jobString);
            }
            catch (Exception ex)
            {
                customerLocks.ForEach(l => l.Delete());
                purchaseOrderLocks.ForEach(l => l.Delete());
                jobLocks.ForEach(l => l.Delete());

                _view.DisplayMessages(new[] { ValidationMessage.Error(ex.Message) });
            }
        }

        private void OnUnLinkEntities(object sender, ViewEventArgs<Tuple<List<Shipment>, List<LoadOrder>, List<ServiceTicket>>> e)
        {
            var unlinkTuple = e.Argument;

            var shipments = unlinkTuple.Item1;
            var loadOrders = unlinkTuple.Item2;
            var serviceTickets = unlinkTuple.Item3;
            try
            {
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();

                // link up validators
                _shipmentValidator.Connection = Connection;
                _shipmentValidator.Transaction = Transaction;

                _loadValidator.Connection = Connection;
                _loadValidator.Transaction = Transaction;

                _serviceTicketValidator.Connection = Connection;
                _serviceTicketValidator.Transaction = Transaction;

                ProcessShipments(shipments.Select(s => new Tuple<Shipment,ShipmentHistoricalData>(s, null)).ToList());
                if (ErrorMessages.Any())
                {
                    RollBackTransaction();

                    ErrorMessages.Insert(0, ValidationMessage.Error("An error occurred while saving Shipments: "));
                    _view.DisplayMessages(ErrorMessages);
                    return;
                }

                ProcessLoadOrders(loadOrders.Select(l => new Tuple<LoadOrder,LoadOrderHistoricalData>(l, null)).ToList());
                if (ErrorMessages.Any())
                {
                    RollBackTransaction();

                    ErrorMessages.Insert(0, ValidationMessage.Error("An error occurred while saving Load Orders: "));
                    _view.DisplayMessages(ErrorMessages);
                    return;
                }

                ProcessServiceTickets(serviceTickets);
                if (ErrorMessages.Any())
                {
                    RollBackTransaction();

                    ErrorMessages.Insert(0, ValidationMessage.Error("An error occurred while saving Service Tickets: "));
                    _view.DisplayMessages(ErrorMessages);
                    return;
                }

                // commit transaction
                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information("Successfully unlinked entities") });
            }
            catch (Exception ex)
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ex.Message) });
            }
        }

        private void ProcessJobs(List<Job> jobs, List<Shipment> shipments, List<LoadOrder> loadOrders, List<ServiceTicket> serviceTickets)
        {
            foreach (var job in jobs)
            {
                // save
                if (job.IsNew)
                {
                    try
                    {
                        job.Connection = Connection;
                        job.Transaction = Transaction;

                        // validate job
                        _jobValidator.Messages.Clear();
                        if (!_jobValidator.IsValid(job))
                        {
                            ErrorMessages.AddRange(_jobValidator.Messages);
                            return;
                        }

                        // determine which entities need updated
                        var shipmentsToUpdate = shipments.Where(s => s.JobId == job.Id).ToList();
                        var loadOrdersToUpdate = loadOrders.Where(s => s.JobId == job.Id).ToList();
                        var serviceTicketsToUpdate = serviceTickets.Where(s => s.JobId == job.Id).ToList();


                        job.ResetId();
                        job.Save();

                        shipmentsToUpdate.ForEach(s => s.JobId = job.Id);
                        loadOrdersToUpdate.ForEach(s => s.JobId = job.Id);
                        serviceTicketsToUpdate.ForEach(s => s.JobId = job.Id);

                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Job: {1}", AuditLogConstants.AddedNew, job.JobNumber),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = job.EntityName(),
                            EntityId = job.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();


                    }
                    catch (Exception ex)
                    {
                        _view.LogException(ex);
                        ErrorMessages.Add(ValidationMessage.Error(ex.Message));
                        return;
                    }
                }
            }
        }

        private void ProcessShipments(List<Tuple<Shipment,ShipmentHistoricalData>> shipments)
        {
            foreach (var ship in shipments)
            {
                var shipment = ship.Item1;
                var historicalData = ship.Item2;

                try
                {
                    shipment.Connection = Connection;
                    shipment.Transaction = Transaction;

                    if (historicalData != null)
                    {
                        historicalData.Connection = Connection;
                        historicalData.Transaction = Transaction;
                    }

                    // validate shipment
                    _shipmentValidator.Messages.Clear();
                    if (!_shipmentValidator.IsValid(shipment))
                    {
                        ErrorMessages.AddRange(_shipmentValidator.Messages);
                        return;
                    }

                    // check for need to lock on customer
                    if (shipment.IsNew && shipment.Customer.EnableShipperBillOfLading)
                    {
                        var shipperBol = ProcessCustomerShipperBOL(shipment.Customer);

                        if (string.IsNullOrEmpty(shipperBol))
                        {
                            ErrorMessages.Add(ValidationMessage.Error("Could not Process Customer Shipper BOL for Shipment with Shipper Ref #: {0}", shipment.ShipperReference));
                            return;
                        }

                        shipment.ShipperBol = shipperBol;
                    }

                    // validate purchase order if necessary
                    if (shipment.Customer.ValidatePurchaseOrderNumber && !string.IsNullOrEmpty(shipment.PurchaseOrderNumber))
                    {
                        var msg = ShipmentPurchaseOrderNumberIsValid(shipment);
                        if (msg != null)
                        {
                            ErrorMessages.Add(msg);
                            return;
                        }
                    }

                    // audit log shipment
                    if (shipment.IsNew)
                    {
                        // save
                        shipment.Save();

                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Shipment: {1}", AuditLogConstants.AddedNew, shipment.ShipmentNumber),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    }
                    else if (shipment.HasChanges())
                    {
                        foreach (var change in shipment.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = shipment.EntityName(),
                                EntityId = shipment.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                        // save
                        shipment.Save();
                    }

                    ProcessShipmentLocations(shipment);

                    // resave to ensure location and terminal id saved
                    shipment.Save();

                    // process collections
                    ProcessShipmentServices(shipment);
                    ProcessShipmentEquipments(shipment);
                    ProcessShipmentItems(shipment);
                    ProcessShipmentCharges(shipment);
                    ProcessShipmentVendors(shipment);
                    ProcessShipmentAccountBuckets(shipment);
                    ProcessShipmentReferences(shipment);


                    // historical data handling
                    if (historicalData != null && (historicalData.IsNew || historicalData.HasChanges())) historicalData.Save();
                }
                catch (Exception ex)
                {
                    _view.LogException(ex);
                    ErrorMessages.Add(ValidationMessage.Error(ex.Message));
                    return;
                }
            }
        }

        private void ProcessLoadOrders(List<Tuple<LoadOrder,LoadOrderHistoricalData>> loadOrders)
        {
            foreach (var load in loadOrders)
            {
                try
                {
                    var loadOrder = load.Item1;
                    var historicalData = load.Item2;

                    loadOrder.Connection = Connection;
                    loadOrder.Transaction = Transaction;

                    if (historicalData != null)
                    {
                        historicalData.Connection = Connection;
                        historicalData.Transaction = Transaction;
                    }

                    // validate loadOrder
                    _loadValidator.Messages.Clear();
                    if (!_loadValidator.IsValid(loadOrder))
                    {
                        ErrorMessages.AddRange(_loadValidator.Messages);
                        return;
                    }

                    // check for need to lock on customer
                    if (loadOrder.IsNew && loadOrder.Customer.EnableShipperBillOfLading)
                    {
                        var shipperBol = ProcessCustomerShipperBOL(loadOrder.Customer);

                        if (string.IsNullOrEmpty(shipperBol))
                        {
                            ErrorMessages.Add(ValidationMessage.Error("Could not Process Customer Shipper BOL for Load Order with Shipper Ref #: {0}", loadOrder.ShipperReference));
                            return;
                        }

                        loadOrder.ShipperBol = shipperBol;
                    }

                    // validate purchase order if necessary
                    if (loadOrder.Customer.ValidatePurchaseOrderNumber && !string.IsNullOrEmpty(loadOrder.PurchaseOrderNumber))
                    {
                        var msg = LoadOrderPurchaseOrderNumberIsValid(loadOrder);

                        if (msg != null)
                        {
                            ErrorMessages.Add(msg);
                            return;
                        }
                    }

                    // audit log load order
                    if (loadOrder.IsNew)
                    {
                        // save
                        loadOrder.Save();

                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Load Order: {1}", AuditLogConstants.AddedNew, loadOrder.LoadOrderNumber),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    }
                    else if (loadOrder.HasChanges())
                    {
                        foreach (var change in loadOrder.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = loadOrder.EntityName(),
                                EntityId = loadOrder.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                        // save
                        loadOrder.Save();
                    }

                    ProcessLoadOrderLocations(loadOrder);

                    // resave to ensure location and terminal id saved
                    loadOrder.Save();

                    // process collections
                    ProcessLoadOrderServices(loadOrder);
                    ProcessLoadOrderEquipments(loadOrder);
                    ProcessLoadOrderItems(loadOrder);
                    ProcessLoadOrderCharges(loadOrder);
                    ProcessLoadOrderVendors(loadOrder);
                    ProcessLoadOrderAccountBuckets(loadOrder);


                    // historical data handling
                    if (historicalData != null && (historicalData.IsNew || historicalData.HasChanges())) historicalData.Save();
                }
                catch (Exception ex)
                {
                    _view.LogException(ex);
                    ErrorMessages.Add(ValidationMessage.Error(ex.Message));
                    return;
                }
            }
        }

        private void ProcessServiceTickets(List<ServiceTicket> serviceTickets)
        {
            foreach (var serviceTicket in serviceTickets)
            {
                try
                {
                    serviceTicket.Connection = Connection;
                    serviceTicket.Transaction = Transaction;

                    // validate service Ticket
                    _serviceTicketValidator.Messages.Clear();
                    if (!_serviceTicketValidator.IsValid(serviceTicket))
                    {
                        ErrorMessages.AddRange(_serviceTicketValidator.Messages);
                        return;
                    }

                    // audit log service ticket
                    if (serviceTicket.IsNew)
                    {
                        // save
                        serviceTicket.Save();

                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Service Ticket: {1}", AuditLogConstants.AddedNew, serviceTicket.ServiceTicketNumber),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    }
                    else if (serviceTicket.HasChanges())
                    {
                        foreach (var change in serviceTicket.Changes())
                            new AuditLog
                            {
                                Connection = Connection,
                                Transaction = Transaction,
                                Description = change,
                                TenantId = _view.ActiveUser.TenantId,
                                User = _view.ActiveUser,
                                EntityCode = serviceTicket.EntityName(),
                                EntityId = serviceTicket.Id.ToString(),
                                LogDateTime = DateTime.Now
                            }.Log();

                        // save
                        serviceTicket.Save();
                    }

                    // process collections
                    ProcessServiceTicketServices(serviceTicket);
                    ProcessServiceTicketEquipments(serviceTicket);
                    ProcessServiceTicketItems(serviceTicket);
                    ProcessServiceTicketCharges(serviceTicket);
                    ProcessServiceTicketVendors(serviceTicket);
                }
                catch (Exception ex)
                {
                    _view.LogException(ex);
                    ErrorMessages.Add(ValidationMessage.Error(ex.Message));
                    return;
                }
            }
        }


        // Shipment Helper Methods 
        private string ProcessCustomerShipperBOL(Customer customer)
        {
            var latestCustomer = alteredCustomers.FirstOrDefault(c => c.Id == customer.Id);
            if (latestCustomer == null)
            {
                alteredCustomers.Add(customer);
            }

            // set shipper bol and increment customer seed
            if (latestCustomer == null)
            {
                customer.Connection = Connection;
                customer.Transaction = Transaction;

                // Only take one snapshot; we do this to show original value before import and final value after import succeeds
                customer.TakeSnapShot();
            }

            var shipperBol = string.Format("{0}{1}{2}", customer.ShipperBillOfLadingPrefix, ++(latestCustomer ?? customer).ShipperBillOfLadingSeed, customer.ShipperBillOfLadingSuffix);

            return shipperBol;
        }

        private ValidationMessage ShipmentPurchaseOrderNumberIsValid(Shipment shipment)
        {
            var purchaseOrder = new CustomerPurchaseOrderSearch()
                .FetchCustomerPurchaseOrder(shipment.TenantId, shipment.Customer.Id, shipment.PurchaseOrderNumber);

            if (purchaseOrder == null)
            {
                var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
                              ? ProcessorVars.MissingPOErrMsg
                              : shipment.Customer.InvalidPurchaseOrderNumberMessage;

                return ValidationMessage.Error(msg);
            }

            var matchingPurchaseOrder = alteredPurchaseOrders.FirstOrDefault(po => po.Id == purchaseOrder.Id);
            if (matchingPurchaseOrder == null)
            {
                alteredPurchaseOrders.Add(purchaseOrder);
            }
            else
            {
                purchaseOrder = matchingPurchaseOrder;
            }

            if (purchaseOrder.ExpirationDate.TimeToMaximum() < DateTime.Now.TimeToMaximum())
            {
                var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
                              ? string.Format(ProcessorVars.ExpiredPOErrMsgDateFormat, purchaseOrder.ExpirationDate.FormattedShortDateAlt())
                              : shipment.Customer.InvalidPurchaseOrderNumberMessage;
                return ValidationMessage.Error(msg);
            }

            if (purchaseOrder.ApplyOnOrigin && !(purchaseOrder.IsValidPostalCodeFormat(shipment.Origin.PostalCode) && purchaseOrder.CountryId == shipment.Origin.CountryId))
            {
                var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
                            ? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "origin")
                            : shipment.Customer.InvalidPurchaseOrderNumberMessage;
                return ValidationMessage.Error(msg);
            }

            if (purchaseOrder.ApplyOnDestination && !(purchaseOrder.IsValidPostalCodeFormat(shipment.Destination.PostalCode) && purchaseOrder.CountryId == shipment.Destination.CountryId))
            {
                var msg = string.IsNullOrEmpty(shipment.Customer.InvalidPurchaseOrderNumberMessage)
                            ? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "destination")
                            : shipment.Customer.InvalidPurchaseOrderNumberMessage;
                return ValidationMessage.Error(msg);
            }

            if (matchingPurchaseOrder == null)
            {
                purchaseOrder.Connection = Connection;
                purchaseOrder.Transaction = Transaction;

                purchaseOrder.TakeSnapShot();
            }

            if (++purchaseOrder.CurrentUses > purchaseOrder.MaximumUses)
            {
                return ValidationMessage.Error(ProcessorVars.MaxUsePOErrMsgFormat, purchaseOrder.MaximumUses);
            }

            return null;
        }

        private void ProcessShipmentVendors(Shipment shipment)
        {
            foreach (var sv in shipment.Vendors)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                if (sv.IsNew)
                {
                    sv.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Vendor Number:{3}",
                                                    AuditLogConstants.AddedNew, sv.EntityName(), shipment.ShipmentNumber, sv.Vendor.VendorNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (sv.HasChanges())
                {
                    foreach (var change in sv.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Shipment Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    sv.Save();
                }
            }
        }

        private void ProcessShipmentAccountBuckets(Shipment shipment)
        {
            foreach (var qab in shipment.AccountBuckets)
            {
                qab.Connection = Connection;
                qab.Transaction = Transaction;

                if (qab.IsNew)
                {
                    qab.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Account Bucket Code:{3}",
                                                    AuditLogConstants.AddedNew, qab.EntityName(), shipment.ShipmentNumber, qab.AccountBucket.Code),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (qab.HasChanges())
                {
                    foreach (var change in qab.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Shipment Vendor's Account Bucket Code:{1} {2}", qab.EntityName(), qab.AccountBucket.Code, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    qab.Save();
                }
            }
        }

        private void ProcessShipmentCharges(Shipment shipment)
        {
            foreach (var charge in shipment.Charges)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                if (charge.IsNew)
                {
                    charge.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Charge Ref#:{3}",
                                                    AuditLogConstants.AddedNew, charge.EntityName(), shipment.ShipmentNumber, charge.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (charge.HasChanges())
                {
                    foreach (var change in charge.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Charge Ref#:{1} {2}", charge.EntityName(), charge.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    charge.Save();
                }
            }
        }

        private void ProcessShipmentItems(Shipment shipment)
        {
            foreach (var i in shipment.Items)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                if (i.IsNew)
                {
                    i.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Item Ref#:{3}",
                                                    AuditLogConstants.AddedNew, i.EntityName(), shipment.ShipmentNumber, i.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (i.HasChanges())
                {
                    foreach (var change in i.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Item Ref#:{1} {2}", i.EntityName(), i.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    i.Save();
                }
            }
        }

        private void ProcessShipmentEquipments(Shipment shipment)
        {
            foreach (var equipment in shipment.Equipments)
                if (!_shipmentValidator.ShipmentEquipmentExists(equipment))
                {
                    equipment.Connection = Connection;
                    equipment.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Shipment Number:{2} Equipment Type Ref#:{3}", AuditLogConstants.AddedNew,
                                          equipment.EntityName(), shipment.ShipmentNumber, equipment.EquipmentTypeId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    equipment.Save();
                }
        }

        private void ProcessShipmentServices(Shipment shipment)
        {
            foreach (var service in shipment.Services)
                if (!_shipmentValidator.ShipmentServiceExists(service))
                {
                    service.Connection = Connection;
                    service.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Shipment Number:{2} Service Ref#:{3}", AuditLogConstants.AddedNew,
                                          service.EntityName(),
                                          shipment.ShipmentNumber, service.ServiceId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    service.Save();
                }
        }

        private void ProcessShipmentLocations(Shipment shipment)
        {
            var locations = shipment.Stops.ToList();
            locations.Add(shipment.Origin);
            locations.Add(shipment.Destination);

            foreach (var l in locations)
            {
                l.Connection = Connection;
                l.Transaction = Transaction;

                if (l.IsNew)
                {
                    l.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3}",
                                                    AuditLogConstants.AddedNew, l.EntityName(), shipment.ShipmentNumber, l.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (l.HasChanges())
                {
                    foreach (var change in l.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Ref#:{1} {2}", l.EntityName(), l.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    l.Save();
                }

                ProcessShipmentContacts(shipment, l);
            }
        }

        private void ProcessShipmentContacts(Shipment shipment, ShipmentLocation location)
        {
            foreach (var c in location.Contacts)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                if (c.IsNew)
                {
                    c.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                    AuditLogConstants.AddedNew, c.EntityName(), shipment.ShipmentNumber, location.Id, c.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (c.HasChanges())
                {
                    foreach (var change in c.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("Location Ref#{0} {1} {2}", location.Id, c.EntityName(), change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    c.Save();
                }
            }
        }

        private void ProcessShipmentReferences(Shipment shipment)
        {
            foreach (var reference in shipment.CustomerReferences)
            {
                reference.Connection = Connection;
                reference.Transaction = Transaction;

                if (reference.IsNew)
                {
                    reference.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Customer Ref:{3}",
                            AuditLogConstants.AddedNew, reference.EntityName(), shipment.ShipmentNumber, reference.Name),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (reference.HasChanges())
                {
                    foreach (var change in reference.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Customer Ref:{1} {2}", reference.EntityName(), reference.Name, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    reference.Save();
                }
            }

        }



        // Load Order Helper Methods
        private ValidationMessage LoadOrderPurchaseOrderNumberIsValid(LoadOrder loadOrder)
        {
            var purchaseOrder = new CustomerPurchaseOrderSearch()
                .FetchCustomerPurchaseOrder(loadOrder.TenantId, loadOrder.Customer.Id, loadOrder.PurchaseOrderNumber);

            if (purchaseOrder == null)
            {
                var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
                              ? ProcessorVars.MissingPOErrMsg
                              : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;

                return ValidationMessage.Error(msg);
            }

            var matchingPurchaseOrder = alteredPurchaseOrders.FirstOrDefault(po => po.Id == purchaseOrder.Id);
            if (matchingPurchaseOrder == null)
            {
                alteredPurchaseOrders.Add(purchaseOrder);
            }
            else
            {
                purchaseOrder = matchingPurchaseOrder;
            }

            if (purchaseOrder.ExpirationDate.TimeToMaximum() < DateTime.Now.TimeToMaximum())
            {
                var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
                              ? string.Format(ProcessorVars.ExpiredPOErrMsgDateFormat, purchaseOrder.ExpirationDate.FormattedShortDateAlt())
                              : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
                return ValidationMessage.Error(msg);
            }

            if (purchaseOrder.ApplyOnOrigin && !(purchaseOrder.IsValidPostalCodeFormat(loadOrder.Origin.PostalCode) && purchaseOrder.CountryId == loadOrder.Origin.CountryId))
            {
                var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
                            ? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "origin")
                            : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
                return ValidationMessage.Error(msg);
            }

            if (purchaseOrder.ApplyOnDestination && !(purchaseOrder.IsValidPostalCodeFormat(loadOrder.Destination.PostalCode) && purchaseOrder.CountryId == loadOrder.Destination.CountryId))
            {
                var msg = string.IsNullOrEmpty(loadOrder.Customer.InvalidPurchaseOrderNumberMessage)
                            ? string.Format(ProcessorVars.InvalidLocationPOErrMsgFormat, "destination")
                            : loadOrder.Customer.InvalidPurchaseOrderNumberMessage;
                return ValidationMessage.Error(msg);
            }

            if (matchingPurchaseOrder == null)
            {
                purchaseOrder.Connection = Connection;
                purchaseOrder.Transaction = Transaction;

                purchaseOrder.TakeSnapShot();
            }


            if (++purchaseOrder.CurrentUses > purchaseOrder.MaximumUses)
            {
                return ValidationMessage.Error(ProcessorVars.MaxUsePOErrMsgFormat, purchaseOrder.MaximumUses);
            }

            return null;
        }

        private void ProcessLoadOrderVendors(LoadOrder loadOrder)
        {
            foreach (var sv in loadOrder.Vendors)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                if (sv.IsNew)
                {
                    sv.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Vendor Number:{3}",
                                                    AuditLogConstants.AddedNew, sv.EntityName(), loadOrder.LoadOrderNumber, sv.Vendor.VendorNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (sv.HasChanges())
                {
                    foreach (var change in sv.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Load Order Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    sv.Save();
                }
            }
        }

        private void ProcessLoadOrderAccountBuckets(LoadOrder loadOrder)
        {
            foreach (var qab in loadOrder.AccountBuckets)
            {
                qab.Connection = Connection;
                qab.Transaction = Transaction;

                if (qab.IsNew)
                {
                    qab.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Account Bucket Code:{3}",
                                                    AuditLogConstants.AddedNew, qab.EntityName(), loadOrder.LoadOrderNumber, qab.AccountBucket.Code),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (qab.HasChanges())
                {
                    foreach (var change in qab.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Load Order Vendor's Account Bucket Code:{1} {2}", qab.EntityName(), qab.AccountBucket.Code, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    qab.Save();
                }
            }
        }

        private void ProcessLoadOrderCharges(LoadOrder loadOrder)
        {
            foreach (var charge in loadOrder.Charges)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                if (charge.IsNew)
                {
                    charge.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Charge Ref#:{3}",
                                                    AuditLogConstants.AddedNew, charge.EntityName(), loadOrder.LoadOrderNumber, charge.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (charge.HasChanges())
                {
                    foreach (var change in charge.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Charge Ref#:{1} {2}", charge.EntityName(), charge.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    charge.Save();
                }
            }
        }

        private void ProcessLoadOrderItems(LoadOrder loadOrder)
        {
            foreach (var i in loadOrder.Items)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                if (i.IsNew)
                {
                    i.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Item Ref#:{3}",
                                                    AuditLogConstants.AddedNew, i.EntityName(), loadOrder.LoadOrderNumber, i.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (i.HasChanges())
                {
                    foreach (var change in i.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Item Ref#:{1} {2}", i.EntityName(), i.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    i.Save();
                }
            }
        }

        private void ProcessLoadOrderEquipments(LoadOrder loadOrder)
        {
            foreach (var equipment in loadOrder.Equipments)
                if (!_loadValidator.LoadOrderEquipmentExists(equipment))
                {
                    equipment.Connection = Connection;
                    equipment.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Load Order Number:{2} Equipment Type Ref#:{3}", AuditLogConstants.AddedNew,
                                          equipment.EntityName(), loadOrder.LoadOrderNumber, equipment.EquipmentTypeId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    equipment.Save();
                }
        }

        private void ProcessLoadOrderServices(LoadOrder loadOrder)
        {
            foreach (var service in loadOrder.Services)
                if (!_loadValidator.LoadOrderServiceExists(service))
                {
                    service.Connection = Connection;
                    service.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Load Order Number:{2} Service Ref#:{3}", AuditLogConstants.AddedNew,
                                          service.EntityName(),
                                          loadOrder.LoadOrderNumber, service.ServiceId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    service.Save();
                }
        }

        private void ProcessLoadOrderLocations(LoadOrder loadOrder)
        {
            var locations = loadOrder.Stops.ToList();
            locations.Add(loadOrder.Origin);
            locations.Add(loadOrder.Destination);

            foreach (var l in locations)
            {
                l.Connection = Connection;
                l.Transaction = Transaction;

                if (l.IsNew)
                {
                    l.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3}",
                                                    AuditLogConstants.AddedNew, l.EntityName(), loadOrder.LoadOrderNumber, l.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (l.HasChanges())
                {
                    foreach (var change in l.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Ref#:{1} {2}", l.EntityName(), l.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    l.Save();
                }

                ProcessLoadOrderContacts(loadOrder, l);
            }
        }

        private void ProcessLoadOrderContacts(LoadOrder loadOrder, LoadOrderLocation location)
        {
            foreach (var c in location.Contacts)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                if (c.IsNew)
                {
                    c.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                    AuditLogConstants.AddedNew, c.EntityName(), loadOrder.LoadOrderNumber, location.Id, c.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (c.HasChanges())
                {
                    foreach (var change in c.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("Location Ref#{0} {1} {2}", location.Id, c.EntityName(), change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    c.Save();
                }
            }
        }



        // Service Ticket Helper Methods
        private void ProcessServiceTicketVendors(ServiceTicket serviceTicket)
        {
            foreach (var sv in serviceTicket.Vendors)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                if (sv.IsNew)
                {
                    sv.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Service Ticket Number:{2} Vendor Number:{3}",
                                                    AuditLogConstants.AddedNew, sv.EntityName(), serviceTicket.ServiceTicketNumber, sv.Vendor.VendorNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (sv.HasChanges())
                {
                    foreach (var change in sv.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Service Ticket Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    sv.Save();
                }
            }
        }

        private void ProcessServiceTicketCharges(ServiceTicket serviceTicket)
        {
            foreach (var charge in serviceTicket.Charges)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                if (charge.IsNew)
                {
                    charge.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Service Ticket Number:{2} Charge Ref#:{3}",
                                                    AuditLogConstants.AddedNew, charge.EntityName(), serviceTicket.ServiceTicketNumber, charge.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (charge.HasChanges())
                {
                    foreach (var change in charge.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Charge Ref#:{1} {2}", charge.EntityName(), charge.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    charge.Save();
                }
            }
        }

        private void ProcessServiceTicketItems(ServiceTicket serviceTicket)
        {
            foreach (var i in serviceTicket.Items)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                if (i.IsNew)
                {
                    i.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Service Ticket Number:{2} Item Ref#:{3}",
                                                    AuditLogConstants.AddedNew, i.EntityName(), serviceTicket.ServiceTicketNumber, i.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (i.HasChanges())
                {
                    foreach (var change in i.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Item Ref#:{1} {2}", i.EntityName(), i.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = serviceTicket.EntityName(),
                            EntityId = serviceTicket.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    i.Save();
                }
            }
        }

        private void ProcessServiceTicketEquipments(ServiceTicket serviceTicket)
        {
            foreach (var equipment in serviceTicket.Equipments)
                if (!_serviceTicketValidator.ServiceTicketEquipmentExists(equipment))
                {
                    equipment.Connection = Connection;
                    equipment.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Service Ticket Number:{2} Equipment Type Ref#:{3}", AuditLogConstants.AddedNew,
                                          equipment.EntityName(), serviceTicket.ServiceTicketNumber, equipment.EquipmentTypeId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    equipment.Save();
                }
        }

        private void ProcessServiceTicketServices(ServiceTicket serviceTicket)
        {
            foreach (var service in serviceTicket.Services)
                if (!_serviceTicketValidator.ServiceTicketServiceExists(service))
                {
                    service.Connection = Connection;
                    service.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Service Ticket Number:{2} Service Ref#:{3}", AuditLogConstants.AddedNew,
                                          service.EntityName(),
                                          serviceTicket.ServiceTicketNumber, service.ServiceId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = serviceTicket.EntityName(),
                        EntityId = serviceTicket.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    service.Save();
                }
        }

    }
}
