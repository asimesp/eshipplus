﻿using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class ShoppedRateHandler : EntityBase
    {
        private readonly IShoppedRateView _view;

        public ShoppedRateHandler(IShoppedRateView view)
        {
            _view = view;
        }

        public void Initialize()
        {

            _view.Search += OnSearch;
        }

        private void OnSearch(object sender, ViewEventArgs<ShoppedRateSearchCriteria> e)
        {
            var results = new ShoppedRateViewSearchDto().FetchShoppedRateDtos(e.Argument, _view.ActiveUser.TenantId);

            if (results.Count == 0)
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

            _view.DisplaySearchResult(results);
        }
    }
}
