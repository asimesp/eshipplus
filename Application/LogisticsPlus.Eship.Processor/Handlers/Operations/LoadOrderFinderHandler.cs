﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class LoadOrderFinderHandler : EntityBase
	{
        private readonly ILoadOrderFinderView _view;

        public LoadOrderFinderHandler(ILoadOrderFinderView view) { _view = view; }

		public void Initialize()
		{
			_view.Search += OnSearch;
		}

		private void OnSearch(object sender, ViewEventArgs<ShipmentViewSearchCriteria> e)
		{
			var results = _view.FilterForLoadOrderToExludeAttachedToJob
				                                     ? new LoadOrderSearch().FetchLoadsDashboardDtoExludeAttachedToJob(e.Argument,_view.ActiveUser.TenantId)
				                                     : new LoadOrderSearch().FetchLoadsDashboardDto(e.Argument,_view.ActiveUser.TenantId);

			if (results.Count == 0)
				_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });

			_view.DisplaySearchResult(results);
		}
	}
}
