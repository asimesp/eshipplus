﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class AssetLogHandler : EntityBase
    {
        private readonly IAssetLogView _view;
        private readonly AssetLogValidator _validator = new AssetLogValidator();
		
        public AssetLogHandler(IAssetLogView view)
		{
			_view = view;
		}

        public void Initialize()
        {
            _view.Loading += OnLoading;
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.AssetSearch += OnAssetSearch;
            _view.UnLock += OnUnLock;
            _view.Lock += OnLock;
        }

        private void OnAssetSearch(object sender, ViewEventArgs<string> e)
        {
            var asset = new AssetSearch().FetchAssetByAssetNumber(e.Argument, _view.ActiveUser.TenantId);

            _view.DisplayAsset(asset ?? new Asset());
            if (asset == null)
                _view.DisplayMessages(new[] { ValidationMessage.Error("Asset not found") });       
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.MileageEngines = ProcessorUtilities.GetAll<MileageEngine>();
        }

        private void OnSave(object sender, ViewEventArgs<AssetLog> e)
        {
            var assetLog = e.Argument;

            if (!assetLog.IsNew)
            {
                var @lock = assetLog.RetrieveLock(_view.ActiveUser, assetLog.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                assetLog.Connection = Connection;
                assetLog.Transaction = Transaction;

                if (!_validator.IsValid(assetLog))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (assetLog.IsNew)
                {
					assetLog.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description  = string.Format("{0} Asset Number: {1} Log Date: {2} Date Created: {3} Postal Code: {4}",
                                        AuditLogConstants.AddedNew,
                                        assetLog.Asset.AssetNumber,
                                        assetLog.LogDate,
                                        assetLog.DateCreated,
                                        assetLog.PostalCode),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = assetLog.EntityName(),
                        EntityId = assetLog.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (assetLog.HasChanges())
                {
                    foreach (var change in assetLog.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = assetLog.EntityName(),
                            EntityId = assetLog.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    assetLog.Save();
                }

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }     
        }

        private void OnDelete(object sender, ViewEventArgs<AssetLog> e)
        {
            _validator.Messages.Clear();

            var assetLog = e.Argument;

            if (assetLog.IsNew) return;

            var @lock = assetLog.ObtainLock(_view.ActiveUser, assetLog.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                assetLog.Connection = Connection;
                assetLog.Transaction = Transaction;

                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} Asset Number: {1} Log Date: {2} Date Created: {3} Postal Code: {4}", 
                                        AuditLogConstants.Delete, 
                                        assetLog.Asset.AssetNumber, 
                                        assetLog.LogDate, 
                                        assetLog.DateCreated, 
                                        assetLog.PostalCode),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = assetLog.EntityName(),
                    EntityId = assetLog.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                assetLog.Delete();
                @lock.Delete();

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

		private void OnSearch(object sender, ViewEventArgs<long> e)
		{
			var results = new AssetLogSearch().FetchAssetLogs(e.Argument, _view.ActiveUser.TenantId);
			_view.DisplaySearchResult(results);

			if (results.Count == 0)
				_view.DisplayMessages(new[] {ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg)});
		}

    	private void OnUnLock(object sender, ViewEventArgs<AssetLog> e)
        {
            var assetLog = e.Argument;
            var @lock = assetLog.RetrieveLock(_view.ActiveUser, assetLog.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<AssetLog> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }
    }
}
