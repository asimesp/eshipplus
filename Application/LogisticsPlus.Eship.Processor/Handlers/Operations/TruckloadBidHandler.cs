﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class TruckloadBidHandler: EntityBase
	{

		private readonly ITruckloadBidView _view;
		private readonly TruckloadBidValidator _validator = new TruckloadBidValidator();

		public TruckloadBidHandler(ITruckloadBidView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.TruckloadBidLoadAuditLog += OnLoadAuditLog;
			_view.TruckloadBidLock += OnLock;
			_view.TruckloadBidUnLock += OnUnLock;
			_view.TruckloadBidSave += OnSave;
			_view.TruckloadBidDelete += OnDelete;
			
		}
		
		private void OnSave(object sender, ViewEventArgs<TruckloadBid,LoadOrder> e)
		{
			var bid = e.Argument;

			 //check locks
			if (!bid.IsNew)
			{
				var @lock = bid.RetrieveLock(_view.ActiveUser, bid.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}
		
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				bid.Connection = Connection;
				bid.Transaction = Transaction;
				
				_validator.Messages.Clear();


				if (!_validator.IsValid(bid))
				{
					RollBackTransaction();
					_view.TruckloadBidMsgs.AddRange(_validator.Messages);
					return;
				}

				// audit log 
				if (bid.IsNew)
				{
					// save
					bid.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Truckload Bid for Load Order: {1}", AuditLogConstants.AddedNew, bid.LoadOrderId),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = bid.EntityName(),
						EntityId = bid.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (bid.HasChanges())
				{
					foreach (var change in bid.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = bid.EntityName(),
							EntityId = bid.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// save
					bid.Save();
				}
				// resave to ensure id saved
				bid.Save();
			
				// commit transaction
				CommitTransaction();
				_view.DisplayMessages(new[] { ValidationMessage.Information("Truckload bid initiated.") });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}


		private void OnDelete(object sender, ViewEventArgs<TruckloadBid> e)
		{
			var bid = e.Argument;

			if (bid.IsNew)
			{
				return;
			}

			// obtain lock/check lock
			var @lock = bid.ObtainLock(_view.ActiveUser, bid.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.TruckloadBidMsgs.Add(ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg));
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				bid.Connection = Connection;
				bid.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} Truckload bid for Load Order {1}", AuditLogConstants.Delete, bid.LoadOrderId),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = bid.EntityName(),
					EntityId = bid.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				// delete bid and lock
				bid.Delete();

				@lock.Delete();

				CommitTransaction();
				_view.DisplayMessages(new[] { ValidationMessage.Information("Truckload bid cancelled.") });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				@lock.Delete();
				_view.LogException(ex);
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}

		private void OnUnLock(object sender, ViewEventArgs<TruckloadBid> e)
		{
			var request = e.Argument;
			var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

		private void OnLock(object sender, ViewEventArgs<TruckloadBid> e)
		{
			var request = e.Argument;
			var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

		private void OnLoadAuditLog(object sender, ViewEventArgs<TruckloadBid> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}


	}
}
