﻿using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Smc.Eva;
using ObjToSql.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class Smc3EvaDataProcessor : EntityBase
	{
		public List<ValidationMessage> SaveDispatchData(DispatchData data, out Exception ex)
		{
			//validate TrackingDate
			try
			{
				var dispatchData = new SMC3DispatchData
					{
						DateCreated = DateTime.Now,
						TransactionId = data.transactionID,
						Scac = data.scac,
						PickupNumber = data.pickupNumber,
						BarcodeNumber = data.barcodeNumber,
						DateAccepted = data.dateAccepted,
						Identifiers = data.identifiers.Select(i => string.Format("{0}-{1}", i.referenceNumber, i.referenceType)).ToArray().CommaJoin(),
						ResponseStatus = data.responseStatus,
						ResponseCode = data.responseCode,
						ResponseMessage = data.responseMessage
					};

				var validator = new SMC3DispatchDataValidator();
                if (!validator.IsValid(dispatchData))
				{
					ex = null;
					return validator.Messages;
				}

				// Save TrackingData
                dispatchData.Save();

				ex = null;
				return new List<ValidationMessage>();
			}
			catch (Exception e)
			{
				ex = e;
				return new List<ValidationMessage> { ValidationMessage.Error(e.Message) };
			}
		}

		public List<ValidationMessage> SaveTrackingData(TrackingData data, out Exception ex)
		{
			try
			{
				var trackingData = new SMC3TrackingData
					{
						DateCreated = DateTime.Now,
						AccountToken = data.accountToken.Truncate(200),
						AppointmentDate = data.appointmentDate.Truncate(10),
						AppointmentScheduled = data.appointmentScheduled.ToBoolean(),
						AppointmentTime = data.appointmentTime,
						AppointmentType = data.appointmentType,
						BillToAccount = data.billToAccount.Truncate(100),
						BillToAddress1 = data.billToAddress1.Truncate(50),
						BillToAddress2 = data.billToAddress2.Truncate(50),
						BillToCity = data.billToCity.Truncate(50),
						BillToCountry = data.billToCountry.Truncate(50),
						BillToName = data.billToName.Truncate(50),
						BillToPhone = data.billToPhone.Truncate(25),
						BillToPostalCode = data.billToPostalCode.Truncate(10),
						BillToStateProvince = data.billToStateProvince.Truncate(50),
						Bol = data.bol.Truncate(50),
						CCXLCalendarDays = data.ccxlCalendarDays.Truncate(15),
						CCXLDestinationDate = data.ccxlDestinationDate.Truncate(10),
						CCXLDestinationTime = data.cxclDestinationTime.Truncate(8),
						DeliveryDate = data.deliveryDate.Truncate(10),
						DeliveryTime = data.deliveryTime.Truncate(8),
						DestinationAccount = data.destinationAccount.Truncate(100),
						DestinationAddress1 = data.destinationAddress1.Truncate(50),
						DestinationAddress2 = data.destinationAddress2.Truncate(50),
						DestinationCity = data.destinationCity.Truncate(50),
						DestinationCountry = data.destinationCountry.Truncate(50),
						DestinationName = data.destinationName.Truncate(50),
						DestinationPhone = data.destinationPhone.Truncate(25),
						DestinationPostalCode = data.destinationPostalCode.Truncate(10),
						DestinationStateProvince = data.destinationStateProvince.Truncate(50),
						DestinationTerminalCity = data.destinationTerminalCity.Truncate(50),
						DestinationTerminalCode = data.destinationTerminalCode.Truncate(50),
						DestinationTerminalContactName = data.destinationTerminalContactName.Truncate(50),
						DestinationTerminalCountry = data.destinationTerminalCountry.Truncate(50),
                        DestinationTerminalEmail = data.destinationTerminalEmail.Truncate(200),
						DestinationTerminalFax = data.destinationTerminalFax.Truncate(25),
						DestinationTerminalPhone = data.destinationTerminalPhone.Truncate(25),
						DestinationTerminalPostalCode = data.destinationTerminalPostalCode.Truncate(10),
						DestinationTerminalStateProvince = data.destinationTerminalStateProvince.Truncate(50),
						DestinationTerminalTollFree = data.destinationTerminalTollFree.Truncate(25),
						ETADestinationDate = data.etaDestinationDate.Truncate(10),
						ETADestinationTime = data.etaDestinationTime.Truncate(8),
						EquipmentId = data.equipmentID.Truncate(50),
						EquipmentType = data.equipmentType.Truncate(15),
						HazardousMaterial = data.hazardousMaterial.ToBoolean(),
						InterlineContactPhone = data.interlineContactPhone.Truncate(25),
						InterlineNotes = data.interlineNotes.Truncate(500),
						InterlinePartnerName = data.interlinePartnerName.Truncate(50),
						InterlinePro = data.interlinePro.Truncate(50),
						InterlineSCAC = data.interlineScac.Truncate(10),
						OriginAccount = data.originAccount.Truncate(100),
						OriginAddress1 = data.originAddress1.Truncate(50),
						OriginAddress2 = data.originAddress2.Truncate(50),
						OriginCity = data.originCity.Truncate(50),
						OriginCountry = data.originCountry.Truncate(50),
						OriginName = data.originName.Truncate(50),
						OriginPhone = data.originPhone.Truncate(25),
						OriginPostalCode = data.originPostalCode.Truncate(10),
						OriginStateProvince = data.originStateProvince.Truncate(50),
						OriginTerminalCity = data.originTerminalCity.Truncate(50),
						OriginTerminalCode = data.originTerminalCode.Truncate(50),
						OriginTerminalContactName = data.originTerminalContactName.Truncate(50),
						OriginTerminalCountry = data.originTerminalCountry.Truncate(50),
                        OriginTerminalEmail = data.originTerminalEmail.Truncate(200),
						OriginTerminalFax = data.originTerminalFax.Truncate(25),
						OriginTerminalPhone = data.originTerminalPhone.Truncate(25),
						OriginTerminalPostalCode = data.originTerminalPostalCode.Truncate(10),
						OriginTerminalStateProvince = data.originTerminalStateProvince.Truncate(50),
						OriginTerminalTollFree = data.originTerminalTollFree.Truncate(25),
						PO = data.po.Truncate(50),
						PackagingType = data.packagingType.Truncate(20),
						PaymentTerms = data.paymentTerms.Truncate(10),
						PickupDate = data.pickupDate.Truncate(10),
						PickupTime = data.pickupTime.Truncate(8),
						Pieces = data.pieces.ToInt(),
						Pro = data.pro.Truncate(50),
						ReferenceNumber = data.referenceNumber.Truncate(50),
						ReferenceType = data.referenceType.Truncate(10),
						ResponseCode = data.responseCode.Truncate(50),
						ResponseMessage = data.responseMessage.Truncate(500),
						ResponseStatus = data.responseStatus.Truncate(50),
						SCAC = data.scac.Truncate(10),
						StatusCity = data.statusCity.Truncate(50),
						StatusCode = data.statusCode.Truncate(50),
						StatusCountry = data.statusCountry.Truncate(50),
						StatusDate = data.statusDate.Truncate(10),
						StatusDescription = data.statusDescription.Truncate(500),
						StatusEventType = data.statusEventType.Truncate(2),
						StatusPostalCode = data.statusPostalCode.Truncate(10),
						StatusStateProvince = data.statusStateProvince.Truncate(50),
						StatusTime = data.statusTime.Truncate(8),
						TransactionId = data.transactionID.Truncate(200),
						Weight = data.weight.ToDecimal(),
						WeightType = data.weightType.Truncate(20),
						WeightUnit = data.weightUnit.Truncate(10)
					};

				var validator = new SMC3TrackingDataValidator();
				if (!validator.IsValid(trackingData))
				{
					ex = null;
					return validator.Messages;
				}

				// Save TrackingData
				trackingData.Save();

				ex = null;
				return new List<ValidationMessage>();
			}
			catch (Exception e)
			{
				ex = e;
				return new List<ValidationMessage> {ValidationMessage.Error(e.Message)};
			}
		}
	}
}