﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class ShipmentDashboardHandler : EntityBase
    {
        private readonly IShipmentDashboardView _view;

        private readonly LoadOrderValidator _lValidator = new LoadOrderValidator();
        private readonly ShipmentValidator _sValidator = new ShipmentValidator();


        public ShipmentDashboardHandler(IShipmentDashboardView view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.Search += OnSearch;
            _view.Loading += OnLoading;
            _view.LockShipment += OnLockShipment;
            _view.UnLockShipment += OnUnLockShipment;
            _view.LockLoadOrder += OnLockLoadOrder;
            _view.UnLockLoadOrder += OnUnLockLoadOrder;
            _view.SaveShipment += OnSaveShipment;
            _view.SaveLoadOrder += OnSaveLoadOrder;
		}


    	private void OnSearch(object sender, ViewEventArgs<ShipmentViewSearchCriteria> e)
    	{
    		var results = new ShipmentSearch().FetchLoadsShipmentsDashboardDto(e.Argument, _view.ActiveUser.TenantId, true);
    		_view.DisplaySearchResult(results);
    		if (results.Count == 0 && !e.Argument.DisableNoRecordNotification)
    			_view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
    	}

        private void OnLoading(object sender, EventArgs e)
        {
            _view.NoteTypes = ProcessorUtilities.GetAll<NoteType>();
        }

        
        private void OnUnLockLoadOrder(object sender, ViewEventArgs<LoadOrder> e)
        {
            var request = e.Argument;
            var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }
            @lock.Delete();
        }

        private void OnLockLoadOrder(object sender, ViewEventArgs<LoadOrder> e)
        {
            var request = e.Argument;
            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.LockFailed = true;
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnUnLockShipment(object sender, ViewEventArgs<Shipment> e)
        {
            var request = e.Argument;
            var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }
            @lock.Delete();
        }

        private void OnLockShipment(object sender, ViewEventArgs<Shipment> e)
        {
            var request = e.Argument;
            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.LockFailed = true;
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }


        private void OnSaveShipment(object sender, ViewEventArgs<Shipment> e)
        {
            var shipment = e.Argument;
            if (!shipment.IsNew)
            {
                var @lock = shipment.RetrieveLock(_view.ActiveUser, shipment.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }
            else
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot update unsaved shipment") });
                return;
            }

            // retrieve shipment historical data
            var historicalData = shipment.FetchHistoricalData() ??
                                       new ShipmentHistoricalData
                                       {
                                           Shipment = shipment,
                                           WasInDispute = shipment.InDispute,
                                           WasInDisputeReason = shipment.InDisputeReason,
                                           OriginalEstimatedDeliveryDate = shipment.EstimatedDeliveryDate,
                                           OriginalEstimatedPickupDate = shipment.DesiredPickupDate,
                                       };
            if (!historicalData.IsNew) historicalData.TakeSnapShot();
            if (shipment.InDispute) historicalData.WasInDispute = true;
            if (historicalData.WasInDisputeReason == InDisputeReason.NotApplicable)
                historicalData.WasInDisputeReason = shipment.InDisputeReason;
            if (historicalData.OriginalEstimatedPickupDate == DateUtility.SystemEarliestDateTime)
                historicalData.OriginalEstimatedPickupDate = shipment.DesiredPickupDate;
            if (historicalData.OriginalEstimatedDeliveryDate == DateUtility.SystemEarliestDateTime)
                historicalData.OriginalEstimatedDeliveryDate = shipment.EstimatedDeliveryDate;

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _sValidator.Connection = Connection;
                _sValidator.Transaction = Transaction;

                shipment.Connection = Connection;
                shipment.Transaction = Transaction;

                historicalData.Connection = Connection;
                historicalData.Transaction = Transaction;

                var msgs = new List<ValidationMessage>();

                _sValidator.Messages.Clear();
                if (!_sValidator.IsValid(shipment))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_sValidator.Messages);
                    return;
                }
                if (_sValidator.Messages.Any()) msgs.AddRange(_sValidator.Messages); // for warning messages

                if (shipment.HasChanges())
                {
                    foreach (var change in shipment.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    shipment.Save();
                }

                ProcessShipmentCheckCalls(shipment);
                ProcessShipmentVendors(shipment);
                ProcessShipmentNotes(shipment);

                // historical data handling
                if (historicalData.IsNew || historicalData.HasChanges()) historicalData.Save();

                CommitTransaction();

                _view.Set(shipment);
                _view.DisplayMessages(msgs);
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void ProcessShipmentCheckCalls(Shipment shipment)
        {
            foreach (var checkCall in shipment.CheckCalls.Where(checkCall => checkCall.IsNew))
            {
                checkCall.Connection = Connection;
                checkCall.Transaction = Transaction;

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Shipment Number:{2} CheckCall Ref#:{3}", AuditLogConstants.AddedNew,
                                          checkCall.EntityName(), shipment.ShipmentNumber, checkCall.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                checkCall.Save();
            }
        }

        private void ProcessShipmentNotes(Shipment shipment)
        {
            foreach (var note in shipment.Notes.Where(note => note.IsNew))
            {
                note.Connection = Connection;
                note.Transaction = Transaction;

                new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Note Ref#:{3}",
                                                    AuditLogConstants.AddedNew, note.EntityName(), shipment.ShipmentNumber, note.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                note.Save();
            }
        }

        private void ProcessShipmentVendors(Shipment shipment)
        {
            foreach (var sv in shipment.Vendors.Where(sv => sv.HasChanges()))
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                foreach (var change in sv.Changes())
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Shipment Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                sv.Save();
            }
        }


        private void OnSaveLoadOrder(object sender, ViewEventArgs<LoadOrder> e)
        {
            var loadOrder = e.Argument;
            if (!loadOrder.IsNew)
            {
                var @lock = loadOrder.RetrieveLock(_view.ActiveUser, loadOrder.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            } 
            else
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot update unsaved load order") });
                return;
            }

            var messages = new List<ValidationMessage>();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _lValidator.Connection = Connection;
                _lValidator.Transaction = Transaction;

                loadOrder.Connection = Connection;
                loadOrder.Transaction = Transaction;

                _lValidator.Messages.Clear();
                if (!_lValidator.IsValid(loadOrder))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_lValidator.Messages);
                    return;
                }

                if (loadOrder.HasChanges())
                {
                    foreach (var change in loadOrder.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    loadOrder.Save();
                }

                ProcessLoadOrderNotes(loadOrder);
                ProcessLoadOrderVendors(loadOrder);

                CommitTransaction();

                _view.Set(loadOrder);
                _view.DisplayMessages(messages);
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);

                messages.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
                _view.DisplayMessages(messages);
            }
        }

        private void ProcessLoadOrderNotes(LoadOrder loadOrder)
        {
            foreach (var note in loadOrder.Notes.Where(note => note.IsNew))
            {
                note.Connection = Connection;
                note.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Note Ref#:{3}",
                                                AuditLogConstants.AddedNew, note.EntityName(), loadOrder.LoadOrderNumber, note.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                note.Save();
            }
        }

        private void ProcessLoadOrderVendors(LoadOrder loadOrder)
        {
            foreach (var sv in loadOrder.Vendors.Where(sv => sv.HasChanges()))
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                foreach (var change in sv.Changes())
                    new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Load Order Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                sv.Save();
            }
        }
    }
}