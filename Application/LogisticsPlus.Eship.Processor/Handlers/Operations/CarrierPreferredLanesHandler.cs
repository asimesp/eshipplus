﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class CarrierPreferredLanesHandler : EntityBase
    {

        private readonly ICarrierPreferredLaneView _view;

        private readonly VendorPreferredLaneValidator _validator = new VendorPreferredLaneValidator();


        public CarrierPreferredLanesHandler(ICarrierPreferredLaneView view)
		{
			_view = view;
		}

        public void Initialize()
        {
            _view.Save += OnSave;
            _view.Delete += OnDelete;
            _view.Search += OnSearch;
            _view.UnLock += OnUnLock;
            _view.Lock += OnLock;
            _view.VendorSearch += OnVendorSearch;
            _view.BatchImport += OnBatchImport;
        }

        private void OnSave(object sender, ViewEventArgs<VendorPreferredLane> e)
        {
            var lane = e.Argument;

            if (!lane.IsNew)
            {
                var @lock = lane.RetrieveLock(_view.ActiveUser, lane.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }

            _validator.Messages.Clear();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _validator.Connection = Connection;
                _validator.Transaction = Transaction;

                lane.Connection = Connection;
                lane.Transaction = Transaction;

                if (!_validator.IsValid(lane))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_validator.Messages);
                    return;
                }

                if (lane.IsNew)
                {
                    lane.Save();

                	new AuditLog
                		{
                			Connection = Connection,
                			Transaction = Transaction,
                			Description =
                				string.Format("{0} Vendor: {1} - {2} Lane: {3}", AuditLogConstants.AddedNew,
                				              lane.Vendor.VendorNumber, lane.Vendor.Name,
                				              lane.FullLane),
                			TenantId = _view.ActiveUser.TenantId,
                			User = _view.ActiveUser,
                			EntityCode = lane.EntityName(),
                			EntityId = lane.Id.ToString(),
                			LogDateTime = DateTime.Now
                		}.Log();
                }
                else if (lane.HasChanges())
                {
                    foreach (var change in lane.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = lane.EntityName(),
                            EntityId = lane.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    lane.Save();
                }

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }

        private void OnDelete(object sender, ViewEventArgs<VendorPreferredLane> e)
        {
            _validator.Messages.Clear();

            var lane = e.Argument;

            if (lane.IsNew) return;

            var @lock = lane.ObtainLock(_view.ActiveUser, lane.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                return;
            }

            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();

            try
            {
                lane.Connection = Connection;
                lane.Transaction = Transaction;

                @lock.Connection = Connection;
                @lock.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
					   string.Format("{0} Vendor: {1} - {2} Lane: {3}", AuditLogConstants.Delete,
											  lane.Vendor.VendorNumber, lane.Vendor.Name,
											  lane.FullLane),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = lane.EntityName(),
                    EntityId = lane.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                lane.Delete();
                @lock.Delete();

                CommitTransaction();
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
            }
        }

        private void OnSearch(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var results = new VendorPreferredLaneSearch().FetchVendorPreferredLanes(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplaySearchResult(results);
			if (results.Count == 0)
				_view.DisplayMessages(new[] {ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg)});
        }

        private void OnUnLock(object sender, ViewEventArgs<VendorPreferredLane> e)
        {
            var vendorPreferredLane = e.Argument;
            var @lock = vendorPreferredLane.RetrieveLock(_view.ActiveUser, vendorPreferredLane.Id);
            if (@lock == null) return;
            if (@lock.IsUserLock(_view.ActiveUser)) @lock.Delete();
            else _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
        }

        private void OnLock(object sender, ViewEventArgs<VendorPreferredLane> e)
        {
            var @lock = e.Argument.ObtainLock(_view.ActiveUser, e.Argument.Id);

            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.FailedLock(@lock);
                return;
            }

            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnVendorSearch(object sender, ViewEventArgs<string> e)
        {
            var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplayVendor(vendor);
            if (vendor == null)
                _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
        }

        private void OnBatchImport(object sender, ViewEventArgs<List<VendorPreferredLane>> e)
        {
            var messages = new ValidationMessageCollection();

            foreach (var lane in e.Argument)
            {
                _validator.Messages.Clear();
                Connection = DatabaseConnection.DefaultConnection;
                BeginTransaction();
                try
                {
                    _validator.Connection = Connection;
                    _validator.Transaction = Transaction;

                    lane.Connection = Connection;
                    lane.Transaction = Transaction;

                    if (!_validator.IsValid(lane))
                    {
                        RollBackTransaction();
                        messages.AddRange(_validator.Messages);
                        continue;
                    }

                    lane.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
							string.Format("{0} Vendor: {1} - {2} Lane: {3}", AuditLogConstants.AddedNew,
											  lane.Vendor.VendorNumber, lane.Vendor.Name,
											  lane.FullLane),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = lane.EntityName(),
                        EntityId = lane.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollBackTransaction();
                    _view.LogException(ex);
                    messages.Add(ValidationMessage.Error("{0} [{1}] Err: {2}", ProcessorVars.RecordSaveErrMsg, lane.FullLane, ex.Message));
                }
            }
            messages.Add(ValidationMessage.Information(ProcessorVars.ImportProcessCompleteMsg));
            _view.DisplayMessages(messages);
        }
    }
}
