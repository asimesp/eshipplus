﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;
using Shipment = LogisticsPlus.Eship.Core.Operations.Shipment;

namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
    public class DispatchDashboardHandler : EntityBase
    {
        private readonly IDispatchDashboardView _view;
        private readonly LoadOrderValidator _lValidator = new LoadOrderValidator();
        private readonly ShipmentValidator _sValidator = new ShipmentValidator();

        public DispatchDashboardHandler(IDispatchDashboardView view)
		{
			_view = view;
		}

		public void Initialize()
		{
		    _view.Loading += OnLoading;
            _view.LockShipment += OnLockShipment;
            _view.UnLockShipment += OnUnLockShipment;
		    _view.LockLoadOrder += OnLockLoadOrder;
		    _view.UnLockLoadOrder += OnUnLockLoadOrder;
            _view.SaveShipment += OnSaveShipment;
            _view.SaveLoadOrder += OnSaveLoadOrder;
		    _view.VendorSearch += OnVendorSearch;
		}


        private void OnVendorSearch(object sender, ViewEventArgs<string> e)
        {
            var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);
            _view.DisplayVendor(vendor == null || !vendor.Active ? new Vendor() : vendor);
            if (vendor == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
            else if (!vendor.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor is not active") });
        }

        private void OnLoading(object sender, EventArgs e)
        {
            _view.EquipmentTypes = ProcessorVars.RegistryCache[_view.ActiveUser.TenantId].EquipmentTypes
                .Where(eq => eq.Active)
                .ToList();
            _view.LoadOrderStatuses = ProcessorUtilities.GetAll<LoadOrderStatus>()
                .Where(s => s.Value != LoadOrderStatus.Shipment.ToString())
                .ToDictionary(s => s.Key, s => s.Value);
        }


        private void OnSaveLoadOrder(object sender, ViewEventArgs<LoadOrder> e)
        {
            var loadOrder = e.Argument;
            // check locks
            if (!loadOrder.IsNew)
            {
                var @lock = loadOrder.RetrieveLock(_view.ActiveUser, loadOrder.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            } 
            else
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error("Cannot update unsaved load order") });
                return;
            }

            // retrieve db loadOrder for collection comparisons ...
            var dbLoadOrder = new LoadOrder(loadOrder.Id, false);
            dbLoadOrder.LoadCollections();
            var dbLocations = dbLoadOrder.Stops.ToList();
            dbLocations.Add(dbLoadOrder.Origin);
            dbLocations.Add(dbLoadOrder.Destination);

            foreach (var location in dbLocations) location.LoadCollections();

            var messages = new List<ValidationMessage>();
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                _lValidator.Connection = Connection;
                _lValidator.Transaction = Transaction;

                loadOrder.Connection = Connection;
                loadOrder.Transaction = Transaction;

                _lValidator.Messages.Clear();
                if (!_lValidator.IsValid(loadOrder))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_lValidator.Messages);
                    return;
                }

                if (loadOrder.HasChanges())
                {
                    foreach (var change in loadOrder.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    loadOrder.Save();
                }

                // locations
                ProcessLoadOrderLocations(loadOrder, dbLocations);

                // resave to ensure location id saved
                loadOrder.Save();

                // process rest of collections
                ProcessLoadOrderEquipments(loadOrder, dbLoadOrder.Equipments);
                ProcessLoadOrderItems(loadOrder, dbLoadOrder.Items);
                ProcessLoadOrderCharges(loadOrder, dbLoadOrder.Charges);
                ProcessLoadOrderVendors(loadOrder, dbLoadOrder.Vendors);
                ProcessLoadOrderServices(loadOrder, dbLoadOrder.Services);

                // commit transaction
                CommitTransaction();

                _view.Set(loadOrder);
                _view.DisplayMessages(messages);
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);

                messages.Add(ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message));
                _view.DisplayMessages(messages);
            }
        }


        private void ProcessLoadOrderEquipments(LoadOrder loadOrder, IEnumerable<LoadOrderEquipment> dbEquipments)
        {
            var dbDelete = dbEquipments.Where(dbe => !loadOrder.Equipments.Select(e => e.EquipmentTypeId).Contains(dbe.EquipmentTypeId));

            foreach (var s in dbDelete)
            {
                s.Connection = Connection;
                s.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Load Order Number:{2} Equipment Type Ref#: {3}", AuditLogConstants.Delete, s.EntityName(),
                                      loadOrder.LoadOrderNumber, s.EquipmentTypeId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                s.Delete();
            }

            foreach (var equipment in loadOrder.Equipments)
                if (!_lValidator.LoadOrderEquipmentExists(equipment))
                {
                    equipment.Connection = Connection;
                    equipment.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Load Order Number:{2} Equipment Type Ref#:{3}", AuditLogConstants.AddedNew,
                                          equipment.EntityName(), loadOrder.LoadOrderNumber, equipment.EquipmentTypeId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    equipment.Save();
                }
        }

        private void ProcessLoadOrderLocations(LoadOrder loadOrder, IEnumerable<LoadOrderLocation> dbLoadOrderLocations)
        {
            var locations = loadOrder.Stops.ToList();
            locations.Add(loadOrder.Origin);
            locations.Add(loadOrder.Destination);

            var dbDelete = dbLoadOrderLocations.Where(l => !locations.Select(cl => cl.Id).Contains(l.Id));

            foreach (var l in dbDelete)
            {
                foreach (var c in l.Contacts)
                {
                    c.Connection = Connection;
                    c.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                    AuditLogConstants.Delete, c.EntityName(), loadOrder.LoadOrderNumber, l.Id, c.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    c.Delete();
                }

                l.Connection = Connection;
                l.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3}",
                                                AuditLogConstants.Delete, l.EntityName(), loadOrder.LoadOrderNumber, l.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                l.Delete();
            }

            foreach (var l in locations)
            {
                l.Connection = Connection;
                l.Transaction = Transaction;

                if (l.IsNew)
                {
                    l.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3}",
                                                    AuditLogConstants.AddedNew, l.EntityName(), loadOrder.LoadOrderNumber, l.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (l.HasChanges())
                {
                    foreach (var change in l.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Ref#:{1} {2}", l.EntityName(), l.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    l.Save();
                }

                ProcessLoadOrderContacts(loadOrder, l, dbLoadOrderLocations.FirstOrDefault(dbl => dbl.Id == l.Id));
            }
        }

        private void ProcessLoadOrderContacts(LoadOrder loadOrder, LoadOrderLocation location, LoadOrderLocation dbLocation)
        {
            var dbDelete = dbLocation != null
                            ? dbLocation.Contacts.Where(l => !location.Contacts.Select(cl => cl.Id).Contains(l.Id))
                            : new List<LoadOrderContact>();

            foreach (var c in dbDelete)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                AuditLogConstants.Delete, c.EntityName(), loadOrder.LoadOrderNumber, location.Id, c.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                c.Delete();
            }

            foreach (var c in location.Contacts)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                if (c.IsNew)
                {
                    c.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                    AuditLogConstants.AddedNew, c.EntityName(), loadOrder.LoadOrderNumber, location.Id, c.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (c.HasChanges())
                {
                    foreach (var change in c.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("Location Ref#{0} {1} {2}", location.Id, c.EntityName(), change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    c.Save();
                }
            }
        }

        private void ProcessLoadOrderCharges(LoadOrder loadOrder, IEnumerable<LoadOrderCharge> dbCharges)
        {
            var dbDelete = dbCharges.Where(dbc => !loadOrder.Charges.Select(c => c.Id).Contains(dbc.Id));

            foreach (var charge in dbDelete)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Charge Ref#:{3}",
                                                AuditLogConstants.Delete, charge.EntityName(), loadOrder.LoadOrderNumber, charge.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                charge.Delete();
            }

            foreach (var charge in loadOrder.Charges)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                if (charge.IsNew)
                {
                    charge.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Charge Ref#:{3}",
                                                    AuditLogConstants.AddedNew, charge.EntityName(), loadOrder.LoadOrderNumber, charge.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (charge.HasChanges())
                {
                    foreach (var change in charge.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Charge Ref#:{1} {2}", charge.EntityName(), charge.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    charge.Save();
                }
            }
        }

        private void ProcessLoadOrderItems(LoadOrder loadOrder, IEnumerable<LoadOrderItem> dbItems)
        {
            var dbDelete = dbItems.Where(dbi => !loadOrder.Items.Select(i => i.Id).Contains(dbi.Id));

            foreach (var i in dbDelete)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Item Ref#:{3}",
                                                AuditLogConstants.Delete, i.EntityName(), loadOrder.LoadOrderNumber, i.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                i.Delete();
            }

            foreach (var i in loadOrder.Items)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                if (i.IsNew)
                {
                    i.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Item Ref#:{3}",
                                                    AuditLogConstants.AddedNew, i.EntityName(), loadOrder.LoadOrderNumber, i.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (i.HasChanges())
                {
                    foreach (var change in i.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Item Ref#:{1} {2}", i.EntityName(), i.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    i.Save();
                }
            }
        }

        private void ProcessLoadOrderVendors(LoadOrder loadOrder, IEnumerable<LoadOrderVendor> dbVendors)
        {
            var dbDelete = dbVendors.Where(dbv => !loadOrder.Vendors.Select(v => v.Id).Contains(dbv.Id));

            foreach (var sv in dbDelete)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Load Order Number:{2} Vendor Number:{3}",
                                                AuditLogConstants.Delete, sv.EntityName(), loadOrder.LoadOrderNumber, sv.Vendor.VendorNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                sv.Delete();
            }

            foreach (var sv in loadOrder.Vendors)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                if (sv.IsNew)
                {
                    sv.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Load Order Number:{2} Vendor Number:{3}",
                                                    AuditLogConstants.AddedNew, sv.EntityName(), loadOrder.LoadOrderNumber, sv.Vendor.VendorNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = loadOrder.EntityName(),
                        EntityId = loadOrder.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (sv.HasChanges())
                {
                    foreach (var change in sv.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Load Order Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = loadOrder.EntityName(),
                            EntityId = loadOrder.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    sv.Save();
                }
            }
        }

        private void ProcessLoadOrderServices(LoadOrder loadOrder, IEnumerable<LoadOrderService> dbServices)
        {
            var dbDelete = dbServices.Where(dbs => !loadOrder.Services.Select(s => s.ServiceId).Contains(dbs.ServiceId));

            foreach (var s in dbDelete)
            {
                s.Connection = Connection;
                s.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Load Order Number:{2} Service Ref#: {3}", AuditLogConstants.Delete, s.EntityName(),
                                      loadOrder.LoadOrderNumber, s.ServiceId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = loadOrder.EntityName(),
                    EntityId = loadOrder.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                s.Delete();
            }
        }


        private void OnSaveShipment(object sender, ViewEventArgs<Shipment> e)
        {
            var shipment = e.Argument;

            // check locks
            if (!shipment.IsNew)
            {
                var @lock = shipment.RetrieveLock(_view.ActiveUser, shipment.Id);
                if (!@lock.IsUserLock(_view.ActiveUser))
                {
                    _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
                    return;
                }
            }
            else
            {
                _view.DisplayMessages(new [] {ValidationMessage.Error("Cannot update unsaved shipment")});
                return;
            }

            // retrieve db shipment for collection comparisons ...
            var dbShipment = new Shipment(shipment.Id, false);
            dbShipment.LoadCollections();
            var dbLocations = dbShipment.Stops.ToList();
            dbLocations.Add(dbShipment.Origin);
            dbLocations.Add(dbShipment.Destination);
            
            foreach (var location in dbLocations) location.LoadCollections();

            // retrieve shipment historical data
            var historicalData = shipment.FetchHistoricalData() ??
                                       new ShipmentHistoricalData
                                       {
                                           Shipment = shipment,
                                           WasInDispute = shipment.InDispute,
                                           WasInDisputeReason = shipment.InDisputeReason,
                                           OriginalEstimatedDeliveryDate = shipment.EstimatedDeliveryDate,
                                           OriginalEstimatedPickupDate = shipment.DesiredPickupDate,
                                       };
            if (!historicalData.IsNew) historicalData.TakeSnapShot();
            if (shipment.InDispute) historicalData.WasInDispute = true;
            if (historicalData.WasInDisputeReason == InDisputeReason.NotApplicable)
                historicalData.WasInDisputeReason = shipment.InDisputeReason;
            if (historicalData.OriginalEstimatedPickupDate == DateUtility.SystemEarliestDateTime)
                historicalData.OriginalEstimatedPickupDate = shipment.DesiredPickupDate;
            if (historicalData.OriginalEstimatedDeliveryDate == DateUtility.SystemEarliestDateTime)
                historicalData.OriginalEstimatedDeliveryDate = shipment.EstimatedDeliveryDate;
            
            Connection = DatabaseConnection.DefaultConnection;
            BeginTransaction();
            try
            {
                // link up
                _sValidator.Connection = Connection;
                _sValidator.Transaction = Transaction;

                shipment.Connection = Connection;
                shipment.Transaction = Transaction;

                historicalData.Connection = Connection;
                historicalData.Transaction = Transaction;

                var msgs = new List<ValidationMessage>();

                // validate shipment
                _sValidator.Messages.Clear();
                if (!_sValidator.IsValid(shipment))
                {
                    RollBackTransaction();
                    _view.DisplayMessages(_sValidator.Messages);
                    return;
                }
                if (_sValidator.Messages.Any()) msgs.AddRange(_sValidator.Messages); // for warning messages

                // audit log shipment
                if (shipment.HasChanges())
                {
                    foreach (var change in shipment.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    // save
                    shipment.Save();
                }

                // locations
                ProcessShipmentLocations(shipment, dbLocations);

                // resave to ensure location and terminal id saved
                shipment.Save();

                // process rest of collections
                ProcessShipmentEquipments(shipment, dbShipment.Equipments);
                ProcessShipmentItems(shipment, dbShipment.Items);
                ProcessShipmentCharges(shipment, dbShipment.Charges);
                ProcessShipmentVendors(shipment, dbShipment.Vendors);
                ProcessShipmentServices(shipment, dbShipment.Services);

                // historical data handling
                if (historicalData.IsNew || historicalData.HasChanges()) historicalData.Save();

                // commit transaction
                CommitTransaction();

                _view.Set(shipment);
                _view.DisplayMessages(msgs);
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
            }
        }


        private void ProcessShipmentItems(Shipment shipment, IEnumerable<ShipmentItem> dbItems)
        {
            var dbDelete = dbItems.Where(dbi => !shipment.Items.Select(i => i.Id).Contains(dbi.Id));

            foreach (var i in dbDelete)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Item Ref#:{3}",
                                                AuditLogConstants.Delete, i.EntityName(), shipment.ShipmentNumber, i.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                i.Delete();
            }

            foreach (var i in shipment.Items)
            {
                i.Connection = Connection;
                i.Transaction = Transaction;

                if (i.IsNew)
                {
                    i.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Item Ref#:{3}",
                                                    AuditLogConstants.AddedNew, i.EntityName(), shipment.ShipmentNumber, i.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (i.HasChanges())
                {
                    foreach (var change in i.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Item Ref#:{1} {2}", i.EntityName(), i.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    i.Save();
                }
            }
        }

        private void ProcessShipmentEquipments(Shipment shipment, IEnumerable<ShipmentEquipment> dbEquipments)
        {
            var dbDelete = dbEquipments.Where(dbe => !shipment.Equipments.Select(e => e.EquipmentTypeId).Contains(dbe.EquipmentTypeId));

            foreach (var s in dbDelete)
            {
                s.Connection = Connection;
                s.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Shipment Number:{2} Equipment Type Ref#: {3}", AuditLogConstants.Delete, s.EntityName(),
                                      shipment.ShipmentNumber, s.EquipmentTypeId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                s.Delete();
            }

            foreach (var equipment in shipment.Equipments)
                if (!_sValidator.ShipmentEquipmentExists(equipment))
                {
                    equipment.Connection = Connection;
                    equipment.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description =
                            string.Format("{0} {1} Shipment Number:{2} Equipment Type Ref#:{3}", AuditLogConstants.AddedNew,
                                          equipment.EntityName(), shipment.ShipmentNumber, equipment.EquipmentTypeId),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    equipment.Save();
                }
        }

        private void ProcessShipmentCharges(Shipment shipment, IEnumerable<ShipmentCharge> dbCharges)
        {
            var dbDelete = dbCharges.Where(dbc => !shipment.Charges.Select(c => c.Id).Contains(dbc.Id));

            foreach (var charge in dbDelete)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Charge Ref#:{3}",
                                                AuditLogConstants.Delete, charge.EntityName(), shipment.ShipmentNumber, charge.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                charge.Delete();
            }

            foreach (var charge in shipment.Charges)
            {
                charge.Connection = Connection;
                charge.Transaction = Transaction;

                if (charge.IsNew)
                {
                    charge.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Charge Ref#:{3}",
                                                    AuditLogConstants.AddedNew, charge.EntityName(), shipment.ShipmentNumber, charge.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (charge.HasChanges())
                {
                    foreach (var change in charge.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Charge Ref#:{1} {2}", charge.EntityName(), charge.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    charge.Save();
                }
            }
        }

        private void ProcessShipmentLocations(Shipment shipment, List<ShipmentLocation> dbShipmentLocations)
        {
            var locations = shipment.Stops.ToList();
            locations.Add(shipment.Origin);
            locations.Add(shipment.Destination);
            var dbDelete = dbShipmentLocations.Where(l => !locations.Select(cl => cl.Id).Contains(l.Id));

            foreach (var l in dbDelete)
            {
                foreach (var c in l.Contacts)
                {
                    c.Connection = Connection;
                    c.Transaction = Transaction;

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                    AuditLogConstants.Delete, c.EntityName(), shipment.ShipmentNumber, l.Id, c.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();

                    c.Delete();
                }

                l.Connection = Connection;
                l.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3}",
                                                AuditLogConstants.Delete, l.EntityName(), shipment.ShipmentNumber, l.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                l.Delete();
            }

            foreach (var l in locations)
            {
                l.Connection = Connection;
                l.Transaction = Transaction;

                if (l.IsNew)
                {
                    l.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3}",
                                                    AuditLogConstants.AddedNew, l.EntityName(), shipment.ShipmentNumber, l.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (l.HasChanges())
                {
                    foreach (var change in l.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Ref#:{1} {2}", l.EntityName(), l.Id, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    l.Save();
                }

                ProcessShipmentContacts(shipment, l, dbShipmentLocations.FirstOrDefault(dbl => dbl.Id == l.Id));
            }
        }

        private void ProcessShipmentContacts(Shipment shipment, ShipmentLocation location, ShipmentLocation dbLocation)
        {
            var dbDelete = dbLocation != null
                            ? dbLocation.Contacts.Where(l => !location.Contacts.Select(cl => cl.Id).Contains(l.Id))
                            : new List<ShipmentContact>();

            foreach (var c in dbDelete)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                AuditLogConstants.Delete, c.EntityName(), shipment.ShipmentNumber, location.Id, c.Id),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                c.Delete();
            }

            foreach (var c in location.Contacts)
            {
                c.Connection = Connection;
                c.Transaction = Transaction;

                if (c.IsNew)
                {
                    c.Save();

                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Location Ref#:{3} Contact Ref#: {4}",
                                                    AuditLogConstants.AddedNew, c.EntityName(), shipment.ShipmentNumber, location.Id, c.Id),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (c.HasChanges())
                {
                    foreach (var change in c.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("Location Ref#{0} {1} {2}", location.Id, c.EntityName(), change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    c.Save();
                }
            }
        }

        private void ProcessShipmentVendors(Shipment shipment, IEnumerable<ShipmentVendor> dbVendors)
        {
            var dbDelete = dbVendors.Where(dbv => !shipment.Vendors.Select(v => v.Id).Contains(dbv.Id));

            foreach (var sv in dbDelete)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description = string.Format("{0} {1} Shipment Number:{2} Vendor Number:{3}",
                                                AuditLogConstants.Delete, sv.EntityName(), shipment.ShipmentNumber, sv.Vendor.VendorNumber),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                sv.Delete();
            }

            foreach (var sv in shipment.Vendors)
            {
                sv.Connection = Connection;
                sv.Transaction = Transaction;

                if (sv.IsNew)
                {
                    sv.Save();
                    new AuditLog
                    {
                        Connection = Connection,
                        Transaction = Transaction,
                        Description = string.Format("{0} {1} Shipment Number:{2} Vendor Number:{3}",
                                                    AuditLogConstants.AddedNew, sv.EntityName(), shipment.ShipmentNumber, sv.Vendor.VendorNumber),
                        TenantId = _view.ActiveUser.TenantId,
                        User = _view.ActiveUser,
                        EntityCode = shipment.EntityName(),
                        EntityId = shipment.Id.ToString(),
                        LogDateTime = DateTime.Now
                    }.Log();
                }
                else if (sv.HasChanges())
                {
                    foreach (var change in sv.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = string.Format("{0} Shipment Vendor's Vendor Number:{1} {2}", sv.EntityName(), sv.Vendor.VendorNumber, change),
                            TenantId = _view.ActiveUser.TenantId,
                            User = _view.ActiveUser,
                            EntityCode = shipment.EntityName(),
                            EntityId = shipment.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();
                    sv.Save();
                }
            }
        }

        private void ProcessShipmentServices(Shipment shipment, IEnumerable<ShipmentService> dbServices)
        {
            var dbDelete = dbServices.Where(dbs => !shipment.Services.Select(s => s.ServiceId).Contains(dbs.ServiceId));

            foreach (var s in dbDelete)
            {
                s.Connection = Connection;
                s.Transaction = Transaction;

                new AuditLog
                {
                    Connection = Connection,
                    Transaction = Transaction,
                    Description =
                        string.Format("{0} {1} Shipment Number:{2} Service Ref#: {3}", AuditLogConstants.Delete, s.EntityName(),
                                      shipment.ShipmentNumber, s.ServiceId),
                    TenantId = _view.ActiveUser.TenantId,
                    User = _view.ActiveUser,
                    EntityCode = shipment.EntityName(),
                    EntityId = shipment.Id.ToString(),
                    LogDateTime = DateTime.Now
                }.Log();

                s.Delete();
            }
        }

        private void OnUnLockLoadOrder(object sender, ViewEventArgs<LoadOrder> e)
        {
            var request = e.Argument;
            var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }
            @lock.Delete();
        }

        private void OnLockLoadOrder(object sender, ViewEventArgs<LoadOrder> e)
        {
            var request = e.Argument;
            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.LockFailed = true;
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }

        private void OnUnLockShipment(object sender, ViewEventArgs<Shipment> e)
        {
            var request = e.Argument;
            var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
            if (@lock == null) return;
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
                return;
            }
            @lock.Delete();
        }

        private void OnLockShipment(object sender, ViewEventArgs<Shipment> e)
        {
            var request = e.Argument;
            var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
            if (!@lock.IsUserLock(_view.ActiveUser))
            {
                _view.LockFailed = true;
                return;
            }
            if (@lock.LockIsExpired()) @lock.Refresh();
        }
    }
}
