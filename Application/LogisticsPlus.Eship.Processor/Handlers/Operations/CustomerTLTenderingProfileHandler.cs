﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using ObjToSql.Core;


namespace LogisticsPlus.Eship.Processor.Handlers.Operations
{
	public class CustomerTLTenderingProfileHandler : EntityBase
	{
		private readonly ICustomerTLTenderingProfile _view;
        private readonly CustomerTLTenderingProfileValidator _validator = new CustomerTLTenderingProfileValidator();
		
        public CustomerTLTenderingProfileHandler(ICustomerTLTenderingProfile view)
		{
			_view = view;
		}

		public void Initialize()
		{
			_view.TLProfileLoadAuditLog += OnLoadAuditLog;
			_view.TLProfileLock += OnLock;
			_view.TLProfileUnLock += OnUnLock;
			_view.TLProfileSave += OnSave;
			_view.TLProfileDelete += OnDelete;
			_view.CustomerSearch += OnCustomerSearch;
		    _view.VendorSearch += OnVendorSearch;
			_view.TLProfileCanDeleteLane += OnDeleteLane;
		}



		private void OnDeleteLane(object sender, ViewEventArgs<long> e)
		{
			
			  if (!_validator.CanDeleteLane(e.Argument))
			  {
				  _view.DisplayMessages(_validator.Messages);
				  return;
			  }
		}

		private void OnSave(object sender, ViewEventArgs<CustomerTLTenderingProfile> e)
		{
			var profile = e.Argument;

			// check locks
			if (!profile.IsNew)
			{
				var @lock = profile.RetrieveLock(_view.ActiveUser, profile.Id);
				if (!@lock.IsUserLock(_view.ActiveUser))
				{
					_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
					return;
				}
			}

			// retrieve db shipment for collection comparisons ...
			var dbprofile = new CustomerTLTenderingProfile(profile.Id, false);
			dbprofile.LoadCollections();

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				// link up
				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				profile.Connection = Connection;
				profile.Transaction = Transaction;

				// validate profile
				_validator.Messages.Clear();
				

				if (!_validator.IsValid(profile))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}

				// audit log profile
				if (profile.IsNew)
				{
					// save
					profile.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Customer TL Tendering Profile For Customer #: {1}", AuditLogConstants.AddedNew, profile.Customer.CustomerNumber),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = profile.EntityName(),
						EntityId = profile.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (profile.HasChanges())
				{
					foreach (var change in profile.Changes())
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = change,
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = profile.EntityName(),
							EntityId = profile.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

					// save
					profile.Save();
				}

				// resave to ensure location id saved
				profile.Save();


				var dnLanes = dbprofile.Lanes;
				var dbDelete = dnLanes.Where(dbn => !profile.Lanes.Select(n => n.Id).Contains(dbn.Id));
				if (dbDelete.Any(lane => !_validator.CanDeleteLane(lane.Id)))
				{
					RollBackTransaction();
					_view.DisplayMessages(_validator.Messages);
					return;
				}
				// process rest of collections
				ProcessProfileVendors(profile, dbprofile.Vendors);
                ProcessProfileLanes(profile, dbprofile.Lanes);

				// commit transaction
				CommitTransaction();

				_view.Set(profile);
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordSaveMsg) });
			}
			catch (Exception ex)
			{
				RollBackTransaction();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message) });
			}
		}

        private void OnDelete(object sender, ViewEventArgs<CustomerTLTenderingProfile> e)
		{
			var profile = e.Argument;

			if (profile.IsNew)
			{
				_view.Set(new CustomerTLTenderingProfile());
				return;
			}

			// load collections
			profile.LoadCollections();

			// obtain lock/check lock
			var @lock = profile.ObtainLock(_view.ActiveUser, profile.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.UnableToObtainLockErrMsg) });
				return;
			}

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				profile.Connection = Connection;
				profile.Transaction = Transaction;

				_validator.Connection = Connection;
				_validator.Transaction = Transaction;

				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				_validator.Messages.Clear();
				if (!_validator.CanDeleteCustomerTLProfile(profile))
				{
					RollBackTransaction();
					@lock.Delete();
					_view.DisplayMessages(_validator.Messages);
					return;
				}
				_validator.Messages.Clear();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} Customer TL Tendering Profile For Customer #: {1}", AuditLogConstants.Delete, profile.Customer.CustomerNumber),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = profile.EntityName(),
					EntityId = profile.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				foreach (var vendor in profile.Vendors)
				{
					vendor.Connection = Connection;
					vendor.Transaction = Transaction;

					vendor.Delete();
				}

				foreach (var lane in profile.Lanes)
				{
                    lane.Connection = Connection;
                    lane.Transaction = Transaction;

                    lane.Delete();
				}

				// delete claim and lock
				profile.Delete();

				@lock.Delete();

				CommitTransaction();

				_view.Set(new CustomerTLTenderingProfile());
                _view.DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.RecordDeleteMsg) });
			}
			catch(Exception ex)
			{
				RollBackTransaction();
                @lock.Delete();
                _view.LogException(ex);
                _view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.RecordDeleteErrMsgFormat, ex.Message) });
			}
		}


        private void OnVendorSearch(object sender, ViewEventArgs<string> e)
        {
            var vendor = new VendorSearch().FetchVendorByNumber(e.Argument, _view.ActiveUser.TenantId);

            _view.DisplayVendorForLane(vendor == null || !vendor.Active ? new Vendor() : vendor);
            if (vendor == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor not found") });
            else if (!vendor.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Vendor is not active") });
        }

		private void OnCustomerSearch(object sender, ViewEventArgs<string> e)
		{
			if (!_view.ActiveUser.TenantEmployee)
				FetchCustomerForNonEmployee(e.Argument);
			else
				FetchCustomerForEmployee(e.Argument);
		}

        private void OnUnLock(object sender, ViewEventArgs<CustomerTLTenderingProfile> e)
		{
			var request = e.Argument;
			var @lock = request.RetrieveLock(_view.ActiveUser, request.Id);
			if (@lock == null) return;
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.DisplayMessages(new[] { ValidationMessage.Error(ProcessorVars.DeleteLockErrMsg) });
				return;
			}
			@lock.Delete();
		}

        private void OnLock(object sender, ViewEventArgs<CustomerTLTenderingProfile> e)
		{
			var request = e.Argument;
			var @lock = request.ObtainLock(_view.ActiveUser, request.Id);
			if (!@lock.IsUserLock(_view.ActiveUser))
			{
				_view.FailedLock(@lock);
				return;
			}
			if (@lock.LockIsExpired()) @lock.Refresh();
		}

        private void OnLoadAuditLog(object sender, ViewEventArgs<CustomerTLTenderingProfile> e)
		{
			_view.DisplayAuditLogs(new AuditLogSearch().FetchEntityAuditLogs(e.Argument.EntityName(), e.Argument.Id,
																			 e.Argument.TenantId));
		}



		private void FetchCustomerForNonEmployee(string customerNumber)
		{
			if (_view.ActiveUser.DefaultCustomer.CustomerNumber == customerNumber)
			{
				_view.DisplayCustomer(_view.ActiveUser.DefaultCustomer);
				return;
			}
			var shipAs = _view.ActiveUser.UserShipAs.FirstOrDefault(s => s.Customer.CustomerNumber == customerNumber);
			_view.DisplayCustomer(shipAs == null || !shipAs.Customer.Active ? null : shipAs.Customer);
			if (shipAs == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else if (!shipAs.Customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
		}

		private void FetchCustomerForEmployee(string customerNumber)
		{
			var customer = new CustomerSearch().FetchCustomerByNumber(customerNumber, _view.ActiveUser.TenantId);

			_view.DisplayCustomer(customer == null || !customer.Active ? new Customer() : customer);
			if (customer == null) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer not found") });
			else if (!customer.Active) _view.DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
		}


		private void ProcessProfileVendors(CustomerTLTenderingProfile profile, IEnumerable<CustomerTLTenderingProfileVendor> dbVendors)
		{
			var idInprofile = profile.Vendors.Select(p => p.Vendor.Id).ToList();
			var dbVendorId = dbVendors.Select(p => p.VendorId).ToList();
			var dbDelete = dbVendors.Where(vendor => !idInprofile.Contains(vendor.VendorId)).ToList();
			var dbAdd = profile.Vendors.Where(vendor => !dbVendorId.Contains(vendor.Vendor.Id)).ToList();

			foreach (var qv in dbDelete)
			{
				qv.Connection = Connection;
				qv.Transaction = Transaction;

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0}, Vendor Number:{1} from Truckload Tendering Profile Id:{2} ",
                                                AuditLogConstants.Delete, qv.Vendor.VendorNumber, profile.Id),
					TenantId = _view.ActiveUser.TenantId,
					User = _view.ActiveUser,
					EntityCode = profile.EntityName(),
                    EntityId = profile.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				qv.Delete();
			}

			foreach (var qv in dbAdd)
			{
			    qv.Connection = Connection;
			    qv.Transaction = Transaction;


				if (!_validator.TenderingProfileVendorExists(qv))
				{
					qv.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Vendor Number:{1} Truckload Tendering Profile",
														AuditLogConstants.AddedNew, qv.Vendor.VendorNumber),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = profile.EntityName(),
							EntityId = profile.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
				}
				else if (qv.HasChanges())
			    {
			        foreach (var change in qv.Changes())
			            new AuditLog
			                {
			                    Connection = Connection,
			                    Transaction = Transaction,
			                    Description = string.Format("{0} Truckload Tendering Proflie Vendor's Vendor Number:{1} {2}", qv.EntityName(), qv.Vendor.VendorNumber, change),
			                    TenantId = _view.ActiveUser.TenantId,
			                    User = _view.ActiveUser,
			                    EntityCode = profile.EntityName(),
                                EntityId = profile.Id.ToString(),
			                    LogDateTime = DateTime.Now
			                }.Log();
			        qv.Save();
			    }
			}
		}

		private void ProcessProfileLanes(CustomerTLTenderingProfile profile, IEnumerable<CustomerTLTenderingProfileLane> dbLanes)
		{
			var dbDelete = dbLanes.Where(dbn => !profile.Lanes.Select(n => n.Id).Contains(dbn.Id));
			
			foreach (var lane in dbDelete)
			{
				//Checking if Lane can be deleted
				 if (!_validator.CanDeleteLane(lane.Id)){
					 
					 _view.DisplayMessages(new[] {ValidationMessage.Error("Cannot remove Lane: {0} as it is referenced in truckload tender",lane.FullLane)});
					return;
				 }
				
					 lane.Connection = Connection;
					 lane.Transaction = Transaction;

					 new AuditLog
					 {
						 Connection = Connection,
						 Transaction = Transaction,
						 Description = string.Format("{0} from {1}, Lane:{2}",
													 AuditLogConstants.Delete, lane.EntityName(), lane.FullLane),
						 TenantId = _view.ActiveUser.TenantId,
						 User = _view.ActiveUser,
						 EntityCode = profile.EntityName(),
						 EntityId = profile.Id.ToString(),
						 LogDateTime = DateTime.Now
					 }.Log();

					 lane.Delete();
				
			
			}

			foreach (var lane in profile.Lanes)
			{
				lane.Connection = Connection;
				lane.Transaction = Transaction;

				if (lane.IsNew)
				{
					lane.DateCreated = DateUtility.SystemLatestDateTime;
					lane.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Truckload Tendering Profile Id:{2} Lane:{3}",
													AuditLogConstants.AddedNew, lane.EntityName(), profile.Id, lane.FullLane),
						TenantId = _view.ActiveUser.TenantId,
						User = _view.ActiveUser,
						EntityCode = profile.EntityName(),
						EntityId = profile.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
				}
				else if (lane.HasChanges())
				{
					lane.Save();
					foreach (var change in lane.Changes())
					{
						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0}, Lane:{1}, {2}", lane.EntityName(),AuditLogConstants.Update, lane.FullLane),
							TenantId = _view.ActiveUser.TenantId,
							User = _view.ActiveUser,
							EntityCode = profile.EntityName(),
							EntityId = profile.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					}
					
				}
			}
		}
	}
}
