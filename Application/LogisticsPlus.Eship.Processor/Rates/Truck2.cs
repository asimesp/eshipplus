﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Processor.Rates
{
	public static class Truck2
	{
		public static bool FitsLinearFootage(this VendorRating rating, Shipment shipment)
		{
			// if bypass set, return true. NOTE: by passing may result items not being able to fit in truck!
			if (shipment.LinearFootRuleBypassed) return true;

			// test to see that individual dimensions will fit in truck first. maxLength == 0 means no overlengths!
			var maxLength = rating.LTLOverLengthRules.Any() ? rating.LTLOverLengthRules.Max(r => r.UpperLengthBound) : 0;
			if (maxLength != 0 &&
			    !shipment.Items.All(
				    unit =>
				    unit.ActualLength < maxLength && unit.ActualWidth <= rating.LTLTruckWidth &&
				    unit.ActualHeight <= rating.LTLTruckHeight))
				return false;
			if (maxLength == 0 &&
			    !shipment.Items.All(
				    unit =>
				    unit.ActualLength <= rating.LTLTruckLength && unit.ActualWidth <= rating.LTLTruckWidth &&
				    unit.ActualHeight <= rating.LTLTruckHeight))
				return false;

			// get shipment items as individual i.e. where qty > 1 break apart grouping
			var items = new List<ShipmentItem>();
			foreach (var item in shipment.Items)
				for (var i = 0; i < item.Quantity; i++)
					items.Add(new ShipmentItem
						{
							ActualLength = item.ActualLength > rating.LTLTruckLength ? rating.LTLTruckLength : item.ActualLength,
							ActualWidth = item.ActualWidth,
							ActualHeight = item.ActualHeight,
							IsStackable = item.IsStackable
						});
			var individualItems = items
				.OrderByDescending(i => i.ActualLength)
				.ThenBy(i => i.ActualWidth)
				.ToList();

			var allBoxes = new List<TruckBox>();
			foreach (var item in individualItems)
			{
				var stackHeight = item.IsStackable ? item.ActualHeight : rating.LTLTruckHeight;

				// find next box
				var box = !allBoxes.Any()
					          ? TruckBox.StartBox(rating.LTLTruckLength, rating.LTLTruckWidth, rating.LTLTruckHeight)
					          : allBoxes // already gone through at least once
						            .Where(i => !i.IsFull)
						            .OrderBy(i => i.Length)
						            .Where(i => i.Length >= item.ActualLength)
						            .OrderBy(i => i.Width)
						            .Where(i => i.Width >= item.ActualWidth)
						            .OrderBy(i => i.Height)
						            .FirstOrDefault(i => i.Height >= stackHeight);

				if (box == null) break; // no box, break out for this item

				var boxes = box.BreakDown(new TruckBox
					{
						IsFull = true,
						Length = item.ActualLength,
						Height = item.ActualHeight,
						Width = item.ActualWidth,
						StackHeight = stackHeight
					});

				if (!boxes.Any()) continue;

				allBoxes.RemoveAll(i => i.Key == box.Key); // remove box (area) broken down
				allBoxes.AddRange(boxes); // add new boxes (broken down areas)
			}

			// count of full boxes must equal individual items count
			if (allBoxes.Count(b => b.IsFull) != individualItems.Count) return false;


			// handle stacking: group by stack area
			var stacks = allBoxes.Where(b => b.IsFull)
			                     .GroupBy(b => b.StackArea)
			                     .Select(i => new {i.Key, Values = i})
			                     .Select(i => new ShipmentItem
				                     {
					                     ActualLength = i.Values.Max(x => x.Length),
					                     ActualHeight = i.Values.Sum(x => x.Height),
					                     ActualWidth = i.Values.Max(x => x.Width)
				                     })
			                     .ToList();

			// check square inches verifying that lxw will fit truck
			var sqInches = stacks.Sum(i => (i.ActualLength > rating.LTLTruckLength ? rating.LTLTruckLength : i.ActualLength)*i.ActualWidth);
			var maxSqInches = (rating.LTLTruckLength*rating.LTLTruckWidth);
			if (sqInches > maxSqInches) return false;

			return true;
		}

		public static bool CanHoldWeightOf(this VendorRating rating, Shipment shipment)
		{
			var weight = shipment.Items.TotalActualWeight();
			return rating.LTLMinPickupWeight <= weight && weight <= rating.LTLTruckWeight &&
				   !shipment.Items.Any(
					i => (i.Quantity > 1 ? i.ActualWeight / i.Quantity : i.ActualWeight) > rating.LTLTruckUnitWeight);
		}

        public static bool ItemsWithinLimits(this VendorRating rating, Shipment shipment)
        {
            if (shipment.Items.Sum(i => i.Quantity) > rating.MaxPackageQuantity && rating.MaxPackageQuantityApplies) 
                return false;

            return !rating.IndividualItemLimitsApply || shipment.Items.All(item => item.ActualWidth <= rating.MaxItemWidth && item.ActualLength <= rating.MaxItemLength && item.ActualHeight <= rating.MaxItemHeight);
        }

		public static List<RatingItem> ConfigureRatingItems(this VendorRating rating, Shipment shipment, bool breakdownhierarchy = false)
		{
			var allItems = new List<RatingItem>();
				foreach (var si in shipment.Items)
					for (var i = 0; i < si.Quantity; i++)
						allItems.Add(new RatingItem(si, rating));
			var boxes = new List<RatingItem>();

			// handle stackable items
			var sortedboxes = allItems.Where(i => i.Stackable).OrderByDescending(i => i.Height).ToList();

			RatingItem item;
			while ((item = sortedboxes.Count > 0 ? sortedboxes[0] : null) != null)
			{
				sortedboxes.Remove(item);

				// find suitable stack match(es)
				var i = 0;
				while (i < sortedboxes.Count)
				{
					var canStack = (item.Height + sortedboxes[i].Height) <= rating.LTLTruckHeight &&
					               ((item.Length <= sortedboxes[i].Length && item.Width <= sortedboxes[i].Width) ||
					                (item.Length > sortedboxes[i].Length && item.Width > sortedboxes[i].Width));
					if (canStack)
					{
						// combined item becomes new item to test against
						var newItem = new RatingItem(item, sortedboxes[i], true);
						sortedboxes.Remove(sortedboxes[i]); // remove item used in combination
						item = newItem;
					}
					else i++;
				}

				boxes.Add(item);
			}

			// add non stackable items and sort for side by side logic
			boxes.AddRange(allItems.Where(i => !i.Stackable));
			sortedboxes = boxes.OrderBy(i => i.Length).ThenByDescending(i => i.Width).ToList();
			boxes.Clear(); // clear out since now in sorted boxes;

			// handle side by side items (width columns)
			while ((item = sortedboxes.Count > 0 ? sortedboxes[0] : null) != null)
			{
				sortedboxes.Remove(item); // remove from list

				// find column stack match(es)
				var i = 0;
				while (i < sortedboxes.Count)
					if ((item.Width + sortedboxes[i].Width) <= rating.LTLTruckWidth)
					{
						// combined item becomes new item to test against
						var newItem = new RatingItem(item, sortedboxes[i], false);
						sortedboxes.Remove(sortedboxes[i]); // remove item used in combination
						item = newItem;
					}
					else i++;

				boxes.Add(item);
			}

			// set rated height and width for configured boxes
			foreach (var box in boxes)
			{
				box.RatedWidth = box.Width*2 > rating.LTLTruckWidth ? rating.CubicCapacityPenaltyWidth : box.Width;
				box.RatedHeight = box.Height*2 > rating.LTLTruckHeight || !box.Stackable ? rating.CubicCapacityPenaltyHeight : box.Height;
			}

			return breakdownhierarchy ? boxes.SelectMany(b => b.RetrieveComponentHierarchy()).ToList() : boxes;
		}


		public static int InferQuantityMultiplierAfterStacks2(this ShipmentItem item, VendorRating rating)
		{
			// get shipment items as individual i.e. where qty > 1 break apart grouping
			var individualItems = new List<ShipmentItem>();

			// get individuals items
			for (var i = 0; i < item.Quantity; i++)
				individualItems.Add(new ShipmentItem
					{
						ActualLength = item.ActualLength > rating.LTLTruckLength ? rating.LTLTruckLength : item.ActualLength,
						ActualWidth = item.ActualWidth,
						ActualHeight = item.ActualHeight,
						IsStackable = item.IsStackable
					});


			var allBoxes = new List<TruckBox>();
			foreach (var ii in individualItems)
			{
				var stackHeight = ii.IsStackable ? ii.ActualHeight : rating.LTLTruckHeight;

				// find next box
				var box = !allBoxes.Any()
					          ? TruckBox.StartBox(rating.LTLTruckLength, rating.LTLTruckWidth, rating.LTLTruckHeight)
					          : allBoxes // already gone through at least once
						            .Where(i => !i.IsFull)
						            .OrderBy(i => i.Length)
						            .Where(i => i.Length >= ii.ActualLength)
						            .OrderBy(i => i.Width)
						            .Where(i => i.Width >= ii.ActualWidth)
						            .OrderBy(i => i.Height)
						            .FirstOrDefault(i => i.Height >= stackHeight);

				if (box == null) break; // no box, break out for this item

				var boxes = box.BreakDown(new TruckBox
					{
						IsFull = true,
						Length = ii.ActualLength,
						Height = ii.ActualHeight,
						Width = ii.ActualWidth,
						StackHeight = stackHeight
					});

				if (!boxes.Any()) continue;

				allBoxes.RemoveAll(i => i.Key == box.Key); // remove box (area) broken down
				allBoxes.AddRange(boxes); // add new boxes (broken down areas)
			}

			// handle stack counts
			return allBoxes.Where(b => b.IsFull).GroupBy(b => b.StackArea).Count();
		}
	}
}
