﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Calculations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.Smc.Rates;
using ObjToSql.Core;
using P44SDK.V4.Model;

namespace LogisticsPlus.Eship.Processor.Rates
{
    public class VendorRateAnalysisLTL : EntityBase
    {
        private const string VendorRatingOriginRegion =
            @"exec dbo.[MatchingVendorRatingOriginRegionIds] @VendorRatingId, @TenantId, @PostalCode, @CountryId";

        private const string VendorRatingDestRegion =
            @"exec dbo.[MatchingVendorRatingDestinationRegionIds] @VendorRatingId, @TenantId, @PostalCode, @CountryId";

        private const string RatingIdKey = "VendorRatingId";
        private const string PostalCodeKey = "PostalCode";
        private const string TenantKey = "TenantId";
        private const string CountryId = "CountryId";

        private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>
                                                                      {
                                                                          {RatingIdKey, null},
                                                                          {PostalCodeKey, null},
                                                                          {TenantKey, null}
                                                                      };

        private readonly Dictionary<string, List<long>> _originRegionIds = new Dictionary<string, List<long>>();
        private readonly Dictionary<string, List<long>> _destRegionIds = new Dictionary<string, List<long>>();

        private readonly List<Country> _countries;
        private readonly Rateware2 _rateware;

        /// <summary>
        ///		Contructor
        /// </summary>
        /// <param name="ratewareSettings">
        ///		Rateware settings.  Must be present for LTL rating (if applicable per rating profile)
        /// </param>
        /// <param name="carrierConnectSettings">
        ///		Carrier Connect settings.  Must be present for LTL rating (if applicable per rating profile)
        /// </param>
        public VendorRateAnalysisLTL(SmcServiceSettings ratewareSettings, SmcServiceSettings carrierConnectSettings)
        {
            // set rateware engine
            _rateware = ratewareSettings == null ? null : new Rateware2(ratewareSettings, carrierConnectSettings);
            _countries = ProcessorVars.RegistryCache.Countries.Values.ToList();
        }


        public bool RunSmc3Analysis(VendorRating rating, DateTime effectiveDate, List<Lane> lanes, bool ltlIndirectPointEnabled)
        {
            if (rating == null) return false;
            if (_rateware == null) return false;

            // clear cached results
            _originRegionIds.Clear();
            _destRegionIds.Clear();

            var smc3Requests = new Dictionary<string, RateRequest>();
            var cnt = 0; // delay counter

            foreach (var lane in lanes)
            {
                if (++cnt >= ProcessorVars.CycleCountLimit)
                {
                    cnt = 0;
                    Thread.Sleep(ProcessorVars.SleepTimeOutInMilliSeconds);
                }

                try
                {
                    // retrieve  Region Id's
                    LoadOriginDestinationRegionIds(rating, lane);

                    // LTL Rate reqeusts
                    var request = ProcessLTLRates(rating, lane, effectiveDate, ltlIndirectPointEnabled);
                    if (request == null) lane.Comment = "Unable to rate lane. No matching discount program.";
                    else if (!smc3Requests.ContainsKey(request.ShipmentId)) smc3Requests.Add(request.ShipmentId, request);

                }
                catch (Exception e)
                {
                    if (Connection != null && Connection.State != ConnectionState.Closed) Connection.Close();
                    lane.Comment = string.Format("Error processing lane: {0}", e.Message);
                }
            }

            // run rates
            var smcRates = _rateware.GetShippingRates2(smc3Requests);

            foreach (var lane in lanes)
            {
                if (smcRates.ContainsKey(lane.LineId))
                {
                    var rate = smcRates[lane.LineId];
                    if (!ltlIndirectPointEnabled && !rate.DirectPointRate)
                    {
                        // if invalid rate, set all return values to zero!
                        lane.FreightCostFloor = 0;
                        lane.FreightCostCeiling = 0;
                        lane.DiscountPercent = 0;
                        lane.FuelPercent = 0;
                        lane.BilledWeight = 0;
                        lane.BilledWeightBreak = string.Empty;
                        lane.OriginalValue = 0;
                        lane.RatedFreightClass = 0;
                        lane.CurrentTransitDays = 0;

                        lane.Comment = "Indirect point rate not allowed.";
                    }
                    else
                    {
                        lane.BilledWeight = rate == null ? default(decimal) : rate.BilledWeight.ToDecimal();
                        lane.BilledWeightBreak = rate == null ? string.Empty : lane.BilledWeight.GetBilledWeightBreak().GetString();
                        lane.OriginalValue = rate == null ? default(decimal) : rate.LineHaulCharge;
                        lane.CurrentTransitDays = rate == null ? 0 : rate.TransitDays;
                    }
                }
            }


            return true;
        }


        private RateRequest ProcessLTLRates(VendorRating rating, Lane lane, DateTime effectiveDate, bool lTLIndirectPointEnabled)
        {
            // region filter and ordered priority
            var filteredTiers = rating.LTLDiscountTiers.Where(t => t.EffectiveDate <= effectiveDate.TimeToMaximum())
                .Where(d =>
                       _originRegionIds[lane.OriginKey].Contains(d.OriginRegionId) &&
                       _destRegionIds[lane.DestinationKey].Contains(d.DestinationRegionId))
                .OrderBy(t => t.TierPriority)
                .ToList();
            if (!filteredTiers.Any()) return null;

            // get applicable tier
            var tier = filteredTiers.First();

            // get rate classes (FAK)
            lane.RatedFreightClass = tier.FetchFAK(lane.ActualFreightClass);

            // build SMC rate shipment request
            var rateRequest = new RateRequest
            {
                DestinationCountry = _countries.FirstOrDefault(c => c.Id == lane.DestinationCountryId).GetSmcRatingCountry(),
                DestinationPostalCode = lane.DestinationPostalCode,
                OriginCountry = _countries.FirstOrDefault(c => c.Id == lane.OriginCountryId).GetSmcRatingCountry(),
                OriginPostalCode = lane.OriginPostalCode,
                ShipmentId = lane.LineId,
                LTLTariff = rating.LTLTariff,
                Smc3ServiceLevel = rating.Smc3ServiceLevel,
                IndirectPoint = lTLIndirectPointEnabled,
                Mode = ServiceMode.LessThanTruckload,
                RateBasis = tier.RateBasis,
                StopAlternationWeight = tier.WeightBreak.GetWeightBreakValue(),
                Scac = tier.VendorRating.Vendor.Scac,
                Items = new[]
                        {
                            new RateRequestItem
                                {
                                    NmfcClass = lane.RatedFreightClass.ToString(),
                                    Weight = ((int) Math.Ceiling(lane.ActualWeight)).ToString(),
                                    Length = ((int) Math.Ceiling(lane.Length)).ToString(),
                                    Width = ((int) Math.Ceiling(lane.Width)).ToString(),
                                    Height = ((int) Math.Ceiling(lane.Height)).ToString()
                                }
                        }
                        .ToList(),
                EarlyPickup = DateTime.Now.SetTime(TimeUtility.DefaultOpen), //set to current date for transit
                LatePickup = DateTime.Now.SetTime(TimeUtility.DefaultClose),
            };

            // lane values
            lane.FreightCostFloor = tier.FloorValue;
            lane.FreightCostCeiling = tier.CeilingValue;
            lane.DiscountPercent = tier.DiscountPercent;
            lane.FuelPercent = rating.CurrentLTLFuelMarkup.ApplyManipulations(rating.FuelMarkupFloor, rating.FuelMarkupCeiling);

            return rateRequest;
        }

        private void LoadOriginDestinationRegionIds(VendorRating rating, Lane lane)
        {
            _parameters[RatingIdKey] = rating.Id;
            _parameters[PostalCodeKey] = lane.OriginPostalCode;
            _parameters[TenantKey] = rating.TenantId;
            _parameters[CountryId] = lane.OriginCountryId;

            if (!_originRegionIds.ContainsKey(lane.OriginKey))
            {
                _originRegionIds.Add(lane.OriginKey, new List<long>());
                using (var reader = GetReader(VendorRatingOriginRegion, _parameters))
                    while (reader.Read())
                        _originRegionIds[lane.OriginKey].Add(reader.GetValue(reader.GetOrdinal("Id")).ToLong());
                Connection.Close();
            }


            if (!_destRegionIds.ContainsKey(lane.DestinationKey))
            {
                _parameters[PostalCodeKey] = lane.DestinationPostalCode;
                _parameters[CountryId] = lane.DestinationCountryId;
                _destRegionIds.Add(lane.DestinationKey, new List<long>());
                using (var reader = GetReader(VendorRatingDestRegion, _parameters))
                    while (reader.Read())
                        _destRegionIds[lane.DestinationKey].Add(reader.GetValue(reader.GetOrdinal("Id")).ToLong());
                Connection.Close();
            }
        }
    }
}
