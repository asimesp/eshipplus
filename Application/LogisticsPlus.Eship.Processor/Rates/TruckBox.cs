﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Processor.Rates
{
	public class TruckBox
	{
		public Guid Key { get; private set; }

		public decimal Length { get; set; }
		public decimal Width { get; set; }
		public decimal Height { get; set; }
		public decimal StackHeight { get; set; }

		public string StackArea { get; private set; }

		public bool IsFull { get; set; }
		public bool IsGroundLevel { get; private set; }


		public static TruckBox StartBox(decimal length, decimal width, decimal height)
		{
			return new TruckBox
				{
					Length = length,
					Width = width,
					Height = height,
					IsFull = false,
					IsGroundLevel = true,
				};
		}

		public TruckBox()
		{
			Key = Guid.NewGuid();
			StackArea = null;
		}

		/// <summary>
		/// Method will take a box and create boxes smaller boxes
		/// with space left after using up space for firstBox.
		/// firstBox is placed in a corner (assumed best utilization of space)
		/// </summary>
		/// <param name="firstBox">first box to place in space</param>
		/// <returns>List of smaller boxes if breakdown occured else empty list</returns>
		public List<TruckBox> BreakDown(TruckBox firstBox)
		{
			if (firstBox.Length > Length || firstBox.Width > Width || firstBox.Height > Height) return new List<TruckBox>();

			if (IsGroundLevel) StackArea = Guid.NewGuid().GetString();

			firstBox.StackArea = StackArea;
			firstBox.IsGroundLevel = false;

			// add box
			var boxes = new List<TruckBox> {firstBox};

			// create boxes for the rest of the space
			var heightDiff = Height - firstBox.StackHeight;
			var lengthDiff = Length - firstBox.Length;
			var widthDiff = Width - firstBox.Width;

			if (lengthDiff > 0)
				boxes.Add(new TruckBox
					{
						IsFull = false,
						Height = Height,
						Width = Width,
						Length = lengthDiff,
						StackArea = IsGroundLevel ? null : StackArea,
						IsGroundLevel = IsGroundLevel
					});
			if (widthDiff > 0)
				boxes.Add(new TruckBox
					{
						IsFull = false,
						Height = Height,
						Width = widthDiff,
						Length = firstBox.Length,
						StackArea = IsGroundLevel ? null : StackArea,
						IsGroundLevel = IsGroundLevel
					});
			if (heightDiff > 0)
				boxes.Add(new TruckBox
					{
						IsFull = false,
						Height = heightDiff,
						Width = firstBox.Width,
						Length = firstBox.Length,
						StackArea = StackArea,
						IsGroundLevel = false
					});

			return boxes;
		}
	}
}
