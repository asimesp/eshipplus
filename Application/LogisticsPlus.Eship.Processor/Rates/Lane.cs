﻿using LogisticsPlus.Eship.Core.Calculations;

namespace LogisticsPlus.Eship.Processor.Rates
{
	public class Lane
	{
		// keys
		public string OriginKey { get { return string.Format("{0}-{1}", OriginPostalCode, OriginCountryId); } }
		public string DestinationKey { get { return string.Format("{0}-{1}", DestinationPostalCode, DestinationCountryId); } }

		// input fields
		public string LineId { get; set; }
		public string OriginPostalCode { get; set; }
		public long OriginCountryId { get; set; }
		public string DestinationPostalCode { get; set; }
		public long DestinationCountryId { get; set; }

		public decimal ActualWeight { get; set; }
		public double ActualFreightClass { get; set; }

		public decimal Length { get; set; }
		public decimal Width { get; set; }
		public decimal Height { get; set; }

		// return values after rating
		public decimal OriginalValue { get; set; }
		public decimal DiscountPercent { get; set; }
		public decimal FreightCostFloor { get; set; }
		public decimal FreightCostCeiling { get; set; }
		public decimal BilledWeight { get; set; }
		public decimal FuelPercent { get; set; }
		public double RatedFreightClass { get; set; }
		public decimal FreightCost { get { return OriginalValue.ApplyManipulations(FreightCostFloor, FreightCostCeiling, DiscountPercent); } }
		public decimal FuelCost { get { return FuelPercent.CalculateLineHaulPercentageRate(FreightCost); } }
		public int CurrentTransitDays { get; set; }
		public string BilledWeightBreak { get; set; }


		public string Comment { get; set; }
	}
}
