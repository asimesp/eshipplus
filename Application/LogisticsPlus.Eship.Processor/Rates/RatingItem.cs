﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Calculations;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Processor.Rates
{
	public class RatingItem
	{
		private decimal _height;
		private decimal _length;
		private decimal _actualLength;
		private decimal _width;
		private double _freightClass;

		public decimal Height
		{
			get { return HasComponents ? Stacked ? Components.Sum(c => c.Height) : Components.Max(c => c.Height) : _height; }
			private set { _height = value; }
		}

		public decimal Length
		{
			get { return HasComponents ? Components.Max(c => c.Length) : _length; }
			private set { _length = value; }
		}

		public decimal ActualLength
		{
			get { return HasComponents ? Components.Max(c => c.ActualLength) : _actualLength; }
			private set { _actualLength = value; }
		}


		public decimal Width
		{
			get { return HasComponents ? Stacked ? Components.Max(c => c.Width) : Components.Sum(c => c.Width) : _width; }
			private set { _width = value; }
		}

		public double ActualFreightClass
		{
			get { return HasComponents ? Components.Max(c => c.ActualFreightClass) : _freightClass; }
			private set { _freightClass = value; }
		}


		public bool Stacked { get; private set; }
		public bool Stackable { get; private set; }

		public decimal RatedHeight { get; set; }
		public decimal RatedWidth { get; set; }
		public decimal RatedLength { get { return Length; } }
		public decimal RatingCubicFeet { get { return RatedHeight*RatedLength*RatedWidth/CubicFootCalculations.CubicFoot; } }

		public bool HasComponents { get { return Components != null; } }

		public RatingItem Component1
		{
			get { return Components != null ? Components[0] : null; }
		}
		public RatingItem Component2
		{
			get { return Components != null ? Components[1] : null; }
		}

		private List<RatingItem> Components { get; set; }

		public RatingItem(RatingItem comp1, RatingItem comp2, bool stacked)
		{
			if(comp1 == null)
				throw new ArgumentNullException("comp1");
			if (comp2 == null)
				throw new ArgumentNullException("comp2");

			Stacked = stacked;
			Stackable = stacked;
			Components = new List<RatingItem> {comp1, comp2};		
		}

		public RatingItem(ShipmentItem item, VendorRating rating)
		{
			Stackable = item.IsStackable;
			Height = item.ActualHeight;
			Length = item.ActualLength > rating.LTLTruckLength ? rating.LTLTruckLength : item.ActualLength;
			ActualLength = item.ActualLength;
			Width = item.ActualWidth;
			ActualFreightClass = item.ActualFreightClass;
			Stacked = false;
			Components = null;
		}

		public List<RatingItem> RetrieveComponentHierarchy()
		{
			if (!HasComponents) return new List<RatingItem> {this};

			var comps = new List<RatingItem>();

			Component1.RatedHeight = RatedHeight * (Stacked ? Component1.Height / Height : (!Component1.Stackable || Component1.Height * 2 > RatedHeight ? 1 : Component1.Height / RatedHeight));
			Component2.RatedHeight = RatedHeight * (Stacked ? Component2.Height / Height : (!Component2.Stackable || Component2.Height * 2 > RatedHeight ? 1 : Component2.Height / RatedHeight));

			Component1.RatedWidth = RatedWidth*(Stacked ? 1 : Component1.Width/Width);
			Component2.RatedWidth = RatedWidth*(Stacked ? 1 : Component2.Width/Width);

			comps.Add(Component1);
			comps.Add(Component2);

			do
			{
				var clist = comps.Where(c => c.HasComponents).ToList();
				foreach (var comp in clist)
				{
					comps.Remove(comp);
					comps.AddRange(comp.RetrieveComponentHierarchy());
				}
			} while (comps.Any(c => c.HasComponents));

			return comps;
		}
	}
}
