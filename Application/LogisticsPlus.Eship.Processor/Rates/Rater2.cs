﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Calculations;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.SmallPacks;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.Smc.Rates;
using LogisticsPlus.UpsPlugin;
using LogisticssPlus.FedExPlugin.Services;
using ObjToSql.Core;
using P44SDK.V4.Model;
using Charge = LogisticsPlus.Eship.Core.Accounting.Charge;
using Location = LogisticsPlus.Eship.Core.Location;
using Shipment = LogisticsPlus.Eship.Core.Operations.Shipment;

namespace LogisticsPlus.Eship.Processor.Rates
{
    public class Rater2 : EntityBase
    {
        private const string CfcRuleSuffix = "CFC";

        private const string CustomerRatingOriginRegion =
            @"exec dbo.[RatingOriginRegionIds] @CustomerRatingId, @TenantId, @PostalCode, @CountryId";

        private const string CustomerRatingDestRegion =
            @"exec dbo.[RatingDestinationRegionIds] @CustomerRatingId, @TenantId, @PostalCode, @CountryId";

        private const string RatingIdKey = "CustomerRatingId";
        private const string PostalCodeKey = "PostalCode";
        private const string TenantKey = "TenantId";
        private const string CountryId = "CountryId";

        private const string LatestEarlyPickupTimeForGuaranteedServices = "15:00";
        private const string LatestLatePickupTimeForGuaranteedServices = "17:00";

        private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>
            {
                {RatingIdKey, null},
                {PostalCodeKey, null},
                {TenantKey, null}
            };

        private readonly SmcServiceSettings _ratewareSettings;
        private readonly Project44ServiceSettings _project44Settings;
        private readonly SmcServiceSettings _carrierConnectSettings;
        private readonly ServiceParams _fedExServiceParams;
        private readonly UpsServiceParams _upsServiceParams;
        private readonly List<SmallPackagingMap> _packagingMaps;
        private readonly List<SmallPackageServiceMap> _serviceMaps;
        private readonly List<long> _originRegionIds = new List<long>();
        private readonly List<long> _destRegionIds = new List<long>();

        ///  <summary>
        /// 		Contructor
        ///  </summary>
        ///  <param name="ratewareSettings">
        /// 		Rateware settings.  Must be present for LTL rating (if applicable per rating profile)
        ///  </param>
        ///  <param name="carrierConnectSettings">
        /// 		Carrier Connect settins. Must be present for LTL rating (if applicable per rating profile)
        ///  </param>
        ///  <param name="fedExServiceParams">
        /// 		Ups small package parameters.  Must be present to get UPS rates  (if applicable per rating profile)
        ///  </param>
        ///  <param name="upsServiceParams">
        /// 		FedEx small package parameters.  Must be rpesent to get FedEx rates (if applicable per rating profile)
        ///  </param>
        ///  <param name="packagingMaps">
        /// 		Small package types maps mapping system packaging to engine packaging.  Must be present for small package rates  (if applicable per rating profile)
        ///  </param>
        ///  <param name="serviceMaps">
        /// 		Small package service maps mapping system service to engine services.  Must be present for small package rates  (if applicable per rating profile)
        ///  </param>
        /// <param name="project44Settings">
        ///     Project 44 Settings. 
        /// </param>
        public Rater2(SmcServiceSettings ratewareSettings, SmcServiceSettings carrierConnectSettings,
                      ServiceParams fedExServiceParams,
                      UpsServiceParams upsServiceParams, List<SmallPackagingMap> packagingMaps,
                      List<SmallPackageServiceMap> serviceMaps,
                      Project44ServiceSettings project44Settings = null)
        {
            _ratewareSettings = ratewareSettings;
            _carrierConnectSettings = carrierConnectSettings;
            _fedExServiceParams = fedExServiceParams;
            _upsServiceParams = upsServiceParams;
            _packagingMaps = packagingMaps;
            _serviceMaps = serviceMaps;
            _project44Settings = project44Settings;
        }


        public List<Rate> GetRates(Shipment shipment)
        {
            var rates = new List<Rate>();

            if (shipment.Customer == null) return rates;
            if (shipment.Customer.RatingId == default(long)) return rates;

            var rating = shipment.Customer.Rating;

            // retrieve  Region Id's
            LoadOriginDestinationRegionIds(rating, shipment.Origin.PostalCode, shipment.Origin.CountryId,
                                           shipment.Destination.PostalCode,
                                           shipment.Destination.CountryId, shipment.TenantId);



            // TL Rates
            if (rating.TLSellRates.Any()) rates.AddRange(ProcessTLRates(shipment));

            // LTL Rates
            if (_ratewareSettings != null && _carrierConnectSettings != null) rates.AddRange(ProcessLTLRates(shipment));

            // LTL Package Specific Rates
            if (_carrierConnectSettings != null) rates.AddRange(ProcessLTLPackageSpecificRates(shipment));

            // Small Package Rate
            if (rating.SmallPackRates.Any()) rates.AddRange(ProcessSmallPackageRates(shipment));

            // clean rates
            for (var i = rates.Count - 1; i >= 0; i--)
                if (!rates[i].IsValid) rates.RemoveAt(i);

            // Profit Calculations
            ProcessProfits(rates, rating);

            // reseller additions
            if (rating.ResellerAddition != null)
                ProcessRellerAdditions(rates, rating.ResellerAddition);

            // process insurance
            if (rating.InsuranceEnabled && !shipment.DeclineInsurance)
                ProcessInsurancePurchase(rates, shipment);

            return rates;
        }


        public List<string> BookShipment(Shipment shipment)
        {
            var error = new List<string> { "Internal Error: service parameters missing!" };
            switch (shipment.SmallPackageEngine)
            {
                case SmallPackageEngine.FedEx:
                    return _fedExServiceParams == null ? error : BookFedExShipment(shipment);
                case SmallPackageEngine.Ups:
                    return new List<string> { "Internal Error: disabled engine" };
                default:
                    return new List<string> { "Internal Error: unknown engine" };
            }
        }



        public static void UpdateShipmentItemRatedCalculations(Shipment shipment, LTLCubicFootCapacityRule rule,
                                                               DiscountTier tier)
        {
            if (shipment.ServiceMode != ServiceMode.LessThanTruckload) return;

            var rating = tier == null ? null : tier.VendorRating;
            if (rating != null && rating.EnableLTLPcfToFcConversion) UpdateShipmentItemClassByPcf(shipment, rating);

            foreach (var item in shipment.Items)
                item.RatedFreightClass = tier.GetNmfcClass(item.ActualFreightClass, rule).ToDouble();

        }

        public static void UpdateShipmentAutoRatingAccessorilas(Shipment shipment, LTLSellRate sellRate)
        {
            var serviceIds = shipment.Services.Select(s => s.ServiceId);
            var markups = shipment.Customer.Rating != null
                              ? GetEffectiveCustomerServiceMarkups(shipment.Customer.Rating, shipment.DesiredPickupDate)
                              : new List<CustomerServiceMarkup>();

            foreach (var accessorial in GetEffectiveAccessorials(sellRate.VendorRating, shipment.DesiredPickupDate))
                if (serviceIds.Contains(accessorial.ServiceId))
                {
                    var markup = markups.FirstOrDefault(m => m.ServiceId == accessorial.ServiceId) ?? new CustomerServiceMarkup();

                    shipment.AutoRatingAccessorials.Add(
                        new ShipmentAutoRatingAccessorial
                        {
                            BuyFloor = accessorial.FloorValue,
                            BuyCeiling = accessorial.CeilingValue,
                            Rate = accessorial.Rate,
                            RateType = accessorial.RateType,
                            Shipment = shipment,
                            TenantId = shipment.TenantId,
                            ServiceDescription = string.Format("{0} - {1}", accessorial.Service.Code, accessorial.Service.Description),
                            SellCeiling = markup.ServiceChargeCeiling,
                            SellFloor = markup.ServiceChargeFloor,
                            UseLowerSell = markup.UseMinimum,
                            MarkupPercent = markup.MarkupPercent,
                            MarkupValue = markup.MarkupValue,
                        });
                }

        }


        public List<string> DeleteFedexShipment(Shipment shipment)
        {
            var handler = new FedExPluginHandler(_fedExServiceParams, _packagingMaps, _serviceMaps);
            var confirmation = handler.DeleteShipment(shipment);

            return confirmation.Errors;
        }



        private List<string> BookFedExShipment(Shipment shipment)
        {
            var handler = new FedExPluginHandler(_fedExServiceParams, _packagingMaps, _serviceMaps);
            var confirmation = handler.BookShipment(shipment);

            return ProcessConfirmation(shipment, confirmation);
        }

        private static List<string> ProcessConfirmation(Shipment shipment, BookingConfirmation confirmation)
        {
            if (!confirmation.Errors.Any())
            {
                var i = 0; // index to keep doc names unique for this shipment.
                foreach (var doc in confirmation.Documents)
                {
                    var file = Path.GetTempFileName();

                    using (var stream = new FileStream(file, FileMode.Create))
                    using (var writer = new BinaryWriter(stream))
                        writer.Write(doc.Content);

                    shipment.Documents.Add(new ShipmentDocument
                    {
                        TenantId = shipment.TenantId,
                        Name = string.Format("{0}_{1}", ++i, doc.Name),
                        LocationPath = file,
                        Description = doc.Name,
                        Shipment = shipment,
                        IsInternal = false
                    });
                }
            }

            return confirmation.Errors;
        }


        private static void ProcessInsurancePurchase(IEnumerable<Rate> rates, Shipment shipment)
        {
            foreach (var rate in rates)
            {
                var freightRevenue = rate.Charges
                                         .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight)
                                         .Sum(c => c.AmountDue);
                var declaredValue = shipment.Items.Sum(i => i.Value);

                var cost = (freightRevenue + declaredValue) * shipment.Tenant.InsuranceCostTotalPercentage / 100 *
                           shipment.Tenant.InsuranceCostPerDollar;
                var revenue = (freightRevenue + declaredValue) * shipment.Tenant.InsuranceRevenueTotalPercentage / 100 *
                              shipment.Tenant.InsuranceRevenueRatePerDollar;

                rate.Charges.Add(new RateCharge
                {
                    ChargeCodeId = shipment.Customer.Rating.InsuranceChargeCodeId,
                    UnitBuy = Math.Max(cost, shipment.Tenant.InsuranceCostPurchaseFloor),
                    UnitDiscount = 0,
                    UnitSell = Math.Max(revenue, shipment.Customer.Rating.InsurancePurchaseFloor),
                    Quantity = 1
                });
            }
        }



        private static void ProcessProfits(IEnumerable<Rate> rates, CustomerRating rating)
        {
            foreach (var rate in rates)
                if (rate.Mode == ServiceMode.LessThanTruckload || rate.Mode == ServiceMode.SmallPackage)
                {
                    // check for no profit lines and force applicable profit (minimum test first, then maximum) before adjustment calculations
                    foreach (var charge in rate.Charges)
                        if (charge.Profit == 0 && charge.ChargeCode.Project44Code != Project44ChargeCode.DSC) charge.ApplyApplicableProfitForNoProfitCharge(rating);

                    rate.NoFreightProfit = rating.NoLineHaulProfit;
                    rate.FreightProfitAdjustment = rating.NoLineHaulProfit
                                                       ? 0m
                                                       : rating.GetAdjustmentFator(ChargeCodeCategory.Freight, rate.Charges);

                    rate.NoFuelProfit = rating.NoFuelProfit;
                    rate.FuelProfitAdjustment = rating.NoFuelProfit
                                                    ? 0m
                                                    : rating.GetAdjustmentFator(ChargeCodeCategory.Fuel, rate.Charges);

                    rate.NoAccessorialProfit = rating.NoAccessorialProfit;
                    rate.AccessorialProfitAdjustment = rating.NoAccessorialProfit
                                                           ? 0m
                                                           : rating.GetAdjustmentFator(ChargeCodeCategory.Accessorial, rate.Charges);

                    rate.NoServiceProfit = rating.NoServiceProfit;
                    rate.ServiceProfitAdjustment = rating.NoServiceProfit
                                                       ? 0m
                                                       : rating.GetAdjustmentFator(ChargeCodeCategory.Service, rate.Charges);

                    foreach (var charge in rate.Charges)
                    {
                        // We do not want to adjust discounts from Project 44.
                        if (charge.ChargeCode.Project44Code == Project44ChargeCode.DSC)
                            continue;

                        var adjustment = 1.0000m;
                        switch (charge.ChargeCode.Category)
                        {
                            case ChargeCodeCategory.Freight:
                                adjustment = rate.FreightProfitAdjustment;
                                break;
                            case ChargeCodeCategory.Fuel:
                                adjustment = rate.FuelProfitAdjustment;
                                break;
                            case ChargeCodeCategory.Service:
                                adjustment = rate.ServiceProfitAdjustment;
                                break;
                            case ChargeCodeCategory.Accessorial:
                                adjustment = rate.AccessorialProfitAdjustment;
                                break;
                        }
                        var profit = charge.Profit;
                        charge.UnitSell += (adjustment * profit) - profit;
                    }
                }
        }



        private static void ProcessRellerAdditions(IEnumerable<Rate> rates, ResellerAddition addition)
        {
            foreach (var rate in rates)
            {
                // addition: percent or value
                SetRellerFreightAddition(addition, rate);
                SetRellerFuelAddition(addition, rate);
                SetRellerAccessorialAddition(addition, rate);
                SetRellerServiceAddition(addition, rate);

                foreach (var charge in rate.Charges)
                {
                    var value = 0.0000m;
                    switch (charge.ChargeCode.Category)
                    {
                        case ChargeCodeCategory.Freight:
                            value = rate.ResellerFreightMarkupIsPercent
                                        ? charge.UnitSell * (rate.ResellerFreightMarkup / 100)
                                        : rate.ResellerFreightMarkup;
                            break;
                        case ChargeCodeCategory.Fuel:
                            value = rate.ResellerFuelMarkupIsPercent
                                        ? charge.UnitSell * (rate.ResellerFuelMarkup / 100)
                                        : rate.ResellerFuelMarkup;
                            break;
                        case ChargeCodeCategory.Accessorial:
                            value = rate.ResellerAccessorialMarkupIsPercent
                                        ? charge.UnitSell * (rate.ResellerAccessorialMarkup / 100)
                                        : rate.ResellerAccessorialMarkup;
                            break;
                        case ChargeCodeCategory.Service:
                            value = rate.ResellerServiceMarkupIsPercent
                                        ? charge.UnitSell * (rate.ResellerServiceMarkup / 100)
                                        : rate.ResellerServiceMarkup;
                            break;
                    }
                    charge.UnitSell += value;
                }
            }
        }

        private static void SetRellerFreightAddition(ResellerAddition addition, Rate rate)
        {
            var freight = rate.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight).Sum(c => c.AmountDue);
            if (addition.LineHaulType == ValuePercentageType.Value)
            {
                rate.ResellerFreightMarkup = addition.LineHaulValue;
                rate.ResellerFreightMarkupIsPercent = false;
            }
            else if (addition.LineHaulType == ValuePercentageType.Percentage)
            {
                rate.ResellerFreightMarkup = addition.LineHaulPercentage;
                rate.ResellerFreightMarkupIsPercent = true;
            }
            else
            {
                if (addition.UseLineHaulMinimum)
                {
                    if ((freight * addition.LineHaulPercentage / 100) < addition.LineHaulValue)
                    {
                        rate.ResellerFreightMarkup = addition.LineHaulPercentage;
                        rate.ResellerFreightMarkupIsPercent = true;
                    }
                    else
                    {
                        rate.ResellerFreightMarkup = addition.LineHaulValue;
                        rate.ResellerFreightMarkupIsPercent = false;
                    }
                }
                else
                {
                    if ((freight * addition.LineHaulPercentage / 100) > addition.LineHaulValue)
                    {
                        rate.ResellerFreightMarkup = addition.LineHaulPercentage;
                        rate.ResellerFreightMarkupIsPercent = true;
                    }
                    else
                    {
                        rate.ResellerFreightMarkup = addition.LineHaulValue;
                        rate.ResellerFreightMarkupIsPercent = false;
                    }
                }
            }
        }

        private static void SetRellerFuelAddition(ResellerAddition addition, Rate rate)
        {
            var fuel = rate.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Fuel).Sum(c => c.AmountDue);
            if (addition.FuelType == ValuePercentageType.Value)
            {
                rate.ResellerFuelMarkup = addition.FuelValue;
                rate.ResellerFuelMarkupIsPercent = false;
            }
            else if (addition.FuelType == ValuePercentageType.Percentage)
            {
                rate.ResellerFuelMarkup = addition.FuelPercentage;
                rate.ResellerFuelMarkupIsPercent = true;
            }
            else
            {
                if (addition.UseFuelMinimum)
                {
                    if ((fuel * addition.FuelPercentage / 100) < addition.FuelValue)
                    {
                        rate.ResellerFuelMarkup = addition.FuelPercentage;
                        rate.ResellerFuelMarkupIsPercent = true;
                    }
                    else
                    {
                        rate.ResellerFuelMarkup = addition.FuelValue;
                        rate.ResellerFuelMarkupIsPercent = false;
                    }
                }
                else
                {
                    if ((fuel * addition.FuelPercentage / 100) > addition.FuelValue)
                    {
                        rate.ResellerFuelMarkup = addition.FuelPercentage;
                        rate.ResellerFuelMarkupIsPercent = true;
                    }
                    else
                    {
                        rate.ResellerFuelMarkup = addition.FuelValue;
                        rate.ResellerFuelMarkupIsPercent = false;
                    }
                }
            }
        }

        private static void SetRellerAccessorialAddition(ResellerAddition addition, Rate rate)
        {
            var fuel = rate.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Accessorial).Sum(c => c.AmountDue);
            if (addition.AccessorialType == ValuePercentageType.Value)
            {
                rate.ResellerAccessorialMarkup = addition.AccessorialValue;
                rate.ResellerAccessorialMarkupIsPercent = false;
            }
            else if (addition.AccessorialType == ValuePercentageType.Percentage)
            {
                rate.ResellerAccessorialMarkup = addition.AccessorialPercentage;
                rate.ResellerAccessorialMarkupIsPercent = true;
            }
            else
            {
                if (addition.UseAccessorialMinimum)
                {
                    if ((fuel * addition.AccessorialPercentage / 100) < addition.AccessorialValue)
                    {
                        rate.ResellerAccessorialMarkup = addition.AccessorialPercentage;
                        rate.ResellerAccessorialMarkupIsPercent = true;
                    }
                    else
                    {
                        rate.ResellerAccessorialMarkup = addition.AccessorialValue;
                        rate.ResellerAccessorialMarkupIsPercent = false;
                    }
                }
                else
                {
                    if ((fuel * addition.AccessorialPercentage / 100) > addition.AccessorialValue)
                    {
                        rate.ResellerAccessorialMarkup = addition.AccessorialPercentage;
                        rate.ResellerAccessorialMarkupIsPercent = true;
                    }
                    else
                    {
                        rate.ResellerAccessorialMarkup = addition.AccessorialValue;
                        rate.ResellerAccessorialMarkupIsPercent = false;
                    }
                }
            }
        }

        private static void SetRellerServiceAddition(ResellerAddition addition, Rate rate)
        {
            var fuel = rate.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Service).Sum(c => c.AmountDue);
            if (addition.ServiceType == ValuePercentageType.Value)
            {
                rate.ResellerServiceMarkup = addition.ServiceValue;
                rate.ResellerServiceMarkupIsPercent = false;
            }
            else if (addition.ServiceType == ValuePercentageType.Percentage)
            {
                rate.ResellerServiceMarkup = addition.ServicePercentage;
                rate.ResellerServiceMarkupIsPercent = true;
            }
            else
            {
                if (addition.UseServiceMinimum)
                {
                    if ((fuel * addition.ServicePercentage / 100) < addition.ServiceValue)
                    {
                        rate.ResellerServiceMarkup = addition.ServicePercentage;
                        rate.ResellerServiceMarkupIsPercent = true;
                    }
                    else
                    {
                        rate.ResellerServiceMarkup = addition.ServiceValue;
                        rate.ResellerServiceMarkupIsPercent = false;
                    }
                }
                else
                {
                    if ((fuel * addition.ServicePercentage / 100) > addition.ServiceValue)
                    {
                        rate.ResellerServiceMarkup = addition.ServicePercentage;
                        rate.ResellerServiceMarkupIsPercent = true;
                    }
                    else
                    {
                        rate.ResellerServiceMarkup = addition.ServiceValue;
                        rate.ResellerServiceMarkupIsPercent = false;
                    }
                }
            }
        }




        private IEnumerable<Rate> ProcessTLRates(Shipment shipment)
        {
            var locations = shipment.Stops.OrderBy(l => l.StopOrder).Cast<Location>().ToList();
            locations.Insert(0, shipment.Origin);
            locations.Add(shipment.Destination);

            // order by stops; expected: Origin = 1st ..... Destination = Last
            locations = locations.Where(l => l != null).ToList();

            if (locations.Count < 2) return new List<Rate>();

            // copy collection as method will modifiy collection
            var rates = GetTLRates(shipment, locations.Copy(), TLTariffType.OneWay);

            if (locations.Count == 3) rates.AddRange(GetTLRates(shipment, locations, TLTariffType.RoundTrip));

            return rates;
        }

        private IEnumerable<Rate> ProcessSmallPackageRates(Shipment shipment)
        {
            var rates = new List<Rate>();

            // FedEx
            if (_fedExServiceParams != null &&
                shipment.Customer.Rating.SmallPackRates.Any(r => r.SmallPackageEngine == SmallPackageEngine.FedEx))
            {
                var fHandler = new FedExPluginHandler(_fedExServiceParams, _packagingMaps, _serviceMaps);
                rates.AddRange(fHandler.GetRates(shipment));
            }

            // Ups
            if (_upsServiceParams != null &&
                shipment.Customer.Rating.SmallPackRates.Any(r => r.SmallPackageEngine == SmallPackageEngine.Ups))
            {
                var uHandler = new UpsPluginHandler(_upsServiceParams, _packagingMaps, _serviceMaps);
                rates.AddRange(uHandler.GetRates(shipment));
            }

            // update rates with sell and charge codes ...
            var effectiveMarkup = GetEffectiveSmallPackRates(shipment.Customer.Rating, shipment.DesiredPickupDate.TimeToMaximum());
            for (var i = rates.Count - 1; i >= 0; i--)
            {
                var rate = rates[i];
                var markup = effectiveMarkup
                    .FirstOrDefault(m => m.SmallPackageEngine == rate.SmallPackEngine && m.SmallPackType == rate.SmallPackServiceType);

                if (markup == null)
                {
                    rates.RemoveAt(i); // remove what is not found!
                }
                else
                {
                    rate.Vendor = markup.Vendor; // set vendor
                    foreach (var charge in rate.Charges)
                    {
                        charge.ChargeCodeId = markup.ChargeCodeId;
                        charge.UnitSell = charge.UnitBuy.CalculateMarkedRate(markup.MarkupPercent, markup.MarkupValue, markup.UseMinimum);
                    }
                }
            }

            return rates;
        }

        private IEnumerable<Rate> ProcessLTLRates(Shipment shipmentToRate)
        {
            var shipment = shipmentToRate.Duplicate();
            // to preserve original shipment values (NOTE: shipemntToRate is not changed)
            shipment.LinearFootRuleBypassed = shipmentToRate.LinearFootRuleBypassed;
            // NOTE: this is not capture in the duplicate function
            shipment.DesiredPickupDate = shipmentToRate.DesiredPickupDate;

            var retVal = new Dictionary<string, Rate>();

            // filter for effective dates + vendor ratings + indirect points if necessary
            var serviceIds = shipment.Services.Select(qs => qs.ServiceId).ToList();

            // region filter
            var rFilter = GetEffectiveSellRates(shipment.Customer.Rating, shipment.DesiredPickupDate.TimeToMaximum())
                .Where(r => r.VendorRating.Project44Profile || r.VendorRating.LTLDiscountTiers
                             .Any(d => _originRegionIds.Contains(d.OriginRegionId) && _destRegionIds.Contains(d.DestinationRegionId)))
                .ToList();


            var aFilter = rFilter.Where(r =>
            {
                var accessorialServiceIds = r.VendorRating.LTLAccessorials.Select(s => s.ServiceId).ToList();
                return r.VendorRating.Project44Profile || !serviceIds.Any() || serviceIds.All(accessorialServiceIds.Contains);
            }) // accessorial filter
                                 .ToList();

            var filteredSellRates = aFilter
                .Where(r => r.VendorRating.Project44Profile || r.VendorRating.FitsLinearFootage(shipment) && r.VendorRating.CanHoldWeightOf(shipment) && r.VendorRating.ItemsWithinLimits(shipment))
                .ToList();


            if (!filteredSellRates.Any()) return new Rate[0]; // return empty result set if no ratings!

            // setup rateware service
            var rateware = new Rateware2(_ratewareSettings, _carrierConnectSettings);

            // setup p44 wrapper
            var project44Wrapper = _project44Settings != null
                ? new Project44Wrapper(_project44Settings)
                : null;

            // process each vendor rating profile
            var smcRateRequests = new List<RateRequest>();
            var project44RateScacs = new List<string>();

            var refreshShipment = false; // if updated by pcf to fc process
            foreach (var sellRate in filteredSellRates)
            {
                if (refreshShipment)
                {
                    shipment = shipmentToRate.Duplicate();
                    shipment.LinearFootRuleBypassed = shipmentToRate.LinearFootRuleBypassed;
                    // NOTE: this is not captured in the duplicate function
                    shipment.DesiredPickupDate = shipmentToRate.DesiredPickupDate;
                    refreshShipment = false;
                }

                if (sellRate.VendorRating.Project44Profile && project44Wrapper != null)
                {
                    var p44PartnerCodeOrVendorScac = string.IsNullOrWhiteSpace(sellRate.VendorRating.Project44TradingPartnerCode)
                        ? sellRate.VendorRating.Vendor.Scac
                        : sellRate.VendorRating.Project44TradingPartnerCode;
                    project44RateScacs.Add(p44PartnerCodeOrVendorScac);
                    retVal.Add(p44PartnerCodeOrVendorScac, new Rate
                    {
                        ApplyDiscount = false,
                        LTLSellRate = sellRate,
                        DiscountTier = null,
                        CFCRule = null,
                        RatedCubicFeet = shipment.Items.TotalActualCubitFeet(),
                        RatedWeight = shipment.Items.Sum(i => Math.Ceiling(i.ActualWeight).ToInt())
                    });
                    continue;
                }

                var rating = sellRate.VendorRating;

                // check for Pcf to Fc conversion
                if (rating.EnableLTLPcfToFcConversion)
                {
                    // if limit for cubic foot reach, break out
                    if (shipment.Items.TotalActualCubitFeet() > rating.LTLMaxCfForPcfConv) continue;

                    UpdateShipmentItemClassByPcf(shipment, rating);
                    refreshShipment = true;
                }

                // filter for effective dates
                var tier = GetEffectiveDiscountTier(rating, shipment.DesiredPickupDate.TimeToMaximum());

                // if no valid tier, continue to next record
                if (tier == null) continue;

                // cubic capacity rule (if applicable)
                var consolidatedRatingItems = rating.ConfigureRatingItems(shipment);
                var totalRatingCubitFeet = consolidatedRatingItems.Sum(i => i.RatingCubicFeet);
                var rule = rating.HasLTLCubicFootCapacityRules && totalRatingCubitFeet != 0
                               ? rating.GetCFCRule(totalRatingCubitFeet, shipment.Items.TotalActualWeight() / totalRatingCubitFeet,
                                                   shipment.DesiredPickupDate.TimeToMaximum())
                               : null;
                if (rule != null)
                {
                    // build SMC rate shipment request (CFC rule)
                    var shipmentId = rating.Id.ToString() + CfcRuleSuffix;
                    var rateRequestCFC = new RateRequest
                    {
                        DestinationCountry = shipment.Destination.Country.GetSmcRatingCountry(),
                        DestinationPostalCode = shipment.Destination.PostalCode,
                        OriginCountry = shipment.Origin.Country.GetSmcRatingCountry(),
                        OriginPostalCode = shipment.Origin.PostalCode,
                        ShipmentId = shipmentId,
                        LTLTariff = rating.LTLTariff,
                        Smc3ServiceLevel = rating.Smc3ServiceLevel,
                        IndirectPoint = sellRate.LTLIndirectPointEnabled,
                        Mode = ServiceMode.LessThanTruckload,
                        RateBasis = tier.RateBasis,
                        StopAlternationWeight = tier.WeightBreak.GetWeightBreakValue(),
                        Scac = sellRate.VendorRating.Vendor.Scac,

                        EarlyPickup = shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup),
                        LatePickup = shipment.DesiredPickupDate.SetTime(shipment.LatePickup),

                        Items = consolidatedRatingItems
                                .Select(i => new RateRequestItem
                                {
                                    Height = Math.Ceiling(i.RatedHeight).ToInt().GetString(),
                                    Length = Math.Ceiling(i.ActualLength).ToInt().GetString(),
                                    Width = Math.Ceiling(i.RatedWidth).ToInt().GetString(),
                                    NmfcClass = tier.GetNmfcClass(i.ActualFreightClass, rule),
                                    Weight = Math.Ceiling(rule.GetRatedWeight(i)).ToInt().GetString(),
                                })
                                .ToList()
                    };

                    smcRateRequests.Add(rateRequestCFC);
                    retVal.Add(shipmentId, new Rate
                    {
                        ApplyDiscount = rule.ApplyDiscount,
                        LTLSellRate = sellRate,
                        DiscountTier = tier,
                        CFCRule = rule,
                        RatedCubicFeet = totalRatingCubitFeet,
                        RatedWeight = rateRequestCFC.Items.Sum(i => i.Weight.ToInt())
                    });
                }

                // build SMC rate shipment request (non-CFC rule)
                var rateRequest = new RateRequest
                {
                    DestinationCountry = shipment.Destination.Country.GetSmcRatingCountry(),
                    DestinationPostalCode = shipment.Destination.PostalCode,
                    OriginCountry = shipment.Origin.Country.GetSmcRatingCountry(),
                    OriginPostalCode = shipment.Origin.PostalCode,
                    ShipmentId = rating.Id.ToString(),
                    LTLTariff = rating.LTLTariff,
                    Smc3ServiceLevel = rating.Smc3ServiceLevel,
                    IndirectPoint = sellRate.LTLIndirectPointEnabled,
                    Mode = ServiceMode.LessThanTruckload,
                    RateBasis = tier.RateBasis,
                    StopAlternationWeight = tier.WeightBreak.GetWeightBreakValue(),
                    Scac = sellRate.VendorRating.Vendor.Scac,

                    EarlyPickup = shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup),
                    LatePickup = shipment.DesiredPickupDate.SetTime(shipment.LatePickup),

                    Items = shipment.Items
                                        .Select(i => new RateRequestItem
                                        {
                                            Height = Math.Ceiling(i.ActualHeight).ToInt().GetString(),
                                            Length = Math.Ceiling(i.ActualLength).ToInt().GetString(),
                                            Width = Math.Ceiling(i.ActualWidth).ToInt().GetString(),
                                            NmfcClass = tier.GetNmfcClass(i.ActualFreightClass),
                                            Weight = Math.Ceiling(i.ActualWeight).ToInt().GetString()
                                        })
                                        .ToList()
                };

                smcRateRequests.Add(rateRequest);
                retVal.Add(rating.Id.ToString(), new Rate
                {
                    ApplyDiscount = true,
                    LTLSellRate = sellRate,
                    DiscountTier = tier,
                    CFCRule = null,
                    RatedCubicFeet = shipment.Items.TotalActualCubitFeet(),
                    RatedWeight = rateRequest.Items.Sum(i => i.Weight.ToInt())
                });


            }

            // SMC Rates
            var smcRates = rateware.GetShippingRates2(smcRateRequests.ToDictionary(r => r.ShipmentId, r => r));

            // Project 44 Rates
            var p44Rates = new Dictionary<string, List<RateQuote>>();

            if (project44RateScacs.Any() && project44Wrapper != null)
            {
                var rates = project44Wrapper.GetRates(shipment, project44RateScacs, shipment.Customer.Rating.Project44AccountGroup);

                if (rates == null)
                {
                    project44RateScacs.ForEach(s => retVal.Remove(s));
                }
                else
                {
                    var filteredRates = rates
                        .Where(r => (r.ErrorMessages == null || !r.ErrorMessages.Any()) && r.RateQuoteDetail != null).GroupBy(r => r.CarrierCode)
                        .Select(r => new KeyValuePair<string, List<RateQuote>>(r.Key, r.ToList())).ToList();
                    var rateDictionary = new Dictionary<string, List<RateQuote>>();
                    filteredRates.ForEach(r => rateDictionary.Add(r.Key, r.Value));
                    p44Rates = rateDictionary;

                    foreach (var excludedScac in project44RateScacs.Where(r => !rateDictionary.Keys.Contains(r)))
                    {
                        retVal.Remove(excludedScac);
                    }
                }
            }


            // filter out CFC application items with their non CFC rule counterpart.  Select that with higher original cost
            foreach (var cfcKey in smcRates.Keys.Where(k => k.EndsWith(CfcRuleSuffix)).ToList())
            {
                var cleanKey = cfcKey.Replace(CfcRuleSuffix, string.Empty);

                if (!smcRates.ContainsKey(cleanKey)) retVal.Remove(cleanKey);
                if (!smcRates.ContainsKey(cfcKey)) retVal.Remove(cfcKey);
                if (!retVal.ContainsKey(cleanKey) || !retVal.ContainsKey(cfcKey)) continue;

                if (smcRates[cleanKey].LineHaulCharge > smcRates[cfcKey].LineHaulCharge)
                    retVal.Remove(cfcKey);
                else if (smcRates[cleanKey].LineHaulCharge <= smcRates[cfcKey].LineHaulCharge)
                    retVal.Remove(cleanKey);
            }

            // process SMC3 Rates
            foreach (var key in smcRates.Keys)
            {
                if (retVal.ContainsKey(key))
                {
                    // rate basics
                    var rate = retVal[key];

                    var sellRate = rate.LTLSellRate;
                    var vRating = rate.LTLSellRate.VendorRating;

                    // Keep track of whether or not we should allow guar. delivery (if any accessorial/overlength charges, then no)
                    var guaranteedDeliveryAllowed = true;


                    rate.DirectPointRate = smcRates[key].DirectPointRate;
                    rate.Mode = ServiceMode.LessThanTruckload;
                    rate.OriginalRateValue = smcRates[key].LineHaulCharge;
                    rate.TransitDays = smcRates[key].TransitDays;
                    rate.EstimatedDeliveryDate = smcRates[key].EstimatedDeliveryDate;
                    rate.BilledWeight = smcRates[key].BilledWeight.ToDecimal();

                    rate.OriginTerminalCode = smcRates[key].OriginTerminalCode;
                    rate.DestinationTerminalCode = smcRates[key].DestinationTerminalCode;

                    // freight/line haul charges
                    var dt = rate.DiscountTier;
                    var freightCost = rate.ApplyDiscount
                                          ? rate.OriginalRateValue.ApplyManipulations(dt.FloorValue, dt.CeilingValue, dt.DiscountPercent)
                                          : rate.OriginalRateValue;

                    var useVendorMinimum = freightCost == dt.FloorValue && sellRate.OverrideMarkupVendorFloorEnabled;
                    var totalWeight = shipment.Items.TotalActualWeight();
                    var markupPercent = useVendorMinimum ? sellRate.OverrideMarkupPercentVendorFloor : sellRate.GetMarkupPercent(totalWeight);
                    var markupValue = useVendorMinimum ? sellRate.OverrideMarkupValueVendorFloor : sellRate.GetMarkupValue(totalWeight);

                    rate.MarkupPercent = markupPercent;
                    rate.MarkupValue = markupValue;
                    rate.TotalActualShipmentWeight = totalWeight;

                    rate.Charges.Add(new RateCharge
                    {
                        ChargeCodeId = dt.FreightChargeCodeId,
                        UnitBuy = freightCost,
                        UnitDiscount = 0,
                        UnitSell = freightCost.CalculateMarkedRate(markupPercent, markupValue, sellRate.UseMinimum),
                        Quantity = 1,
                    });

                    var totalLinehaulCost = rate.Charges.Sum(c => c.FinalBuy); // for calculations

                    // additional charges
                    if (vRating.HasAdditionalCharges)
                    {
                        var search = new VendorRatingSearch();
                        foreach (var charge in GetEffectiveAdditionalCharges(vRating, shipment.DesiredPickupDate.TimeToMaximum()))
                        {
                            var index = search.ApplicableAdditionalChargeIndex(
                                charge.TenantId,
                                charge.Id,
                                shipment.Origin.CountryId,
                                shipment.Origin.PostalCode,
                                shipment.Destination.CountryId,
                                shipment.Destination.PostalCode);

                            if (index != null)
                            {
                                var cost = index.RateType
                                                .CalculateRate(index.Rate, shipment.Mileage, shipment.Items.TotalActualWeight(), totalLinehaulCost)
                                                .ApplyManipulations(index.ChargeFloor, index.ChargeCeiling);
                                rate.Charges.Add(new RateCharge
                                {
                                    ChargeCodeId = charge.ChargeCodeId,
                                    UnitBuy = cost,
                                    Quantity = 1,
                                    UnitDiscount = 0,
                                    UnitSell = cost
                                });
                            }
                        }
                    }

                    // fuel
                    rate.FuelMarkupPercent = vRating.CurrentLTLFuelMarkup
                                                    .ApplyManipulations(vRating.FuelMarkupFloor, vRating.FuelMarkupCeiling);
                    rate.Charges.Add(new RateCharge
                    {
                        ChargeCodeId = vRating.FuelChargeCodeId,
                        UnitBuy = vRating.CurrentLTLFuelMarkup
                                         .ApplyManipulations(vRating.FuelMarkupFloor, vRating.FuelMarkupCeiling)
                                         .CalculateLineHaulPercentageRate(rate.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight).Sum(c => c.FinalBuy)),
                        UnitDiscount = 0,
                        Quantity = 1,
                        UnitSell = vRating.CurrentLTLFuelMarkup
                                          .ApplyManipulations(vRating.FuelMarkupFloor, vRating.FuelMarkupCeiling)
                                          .CalculateLineHaulPercentageRate(rate.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight).Sum(c => c.FinalSell))
                    });


                    // Accessorial charges
                    if (serviceIds.Any())
                    {
                        var markups = GetEffectiveCustomerServiceMarkups(shipment.Customer.Rating,
                                                                         shipment.DesiredPickupDate.TimeToMaximum());
                        foreach (var charge in GetEffectiveAccessorials(vRating, shipment.DesiredPickupDate.TimeToMaximum()))
                            if (serviceIds.Contains(charge.ServiceId))
                            {
                                var cost = charge.RateType
                                                 .CalculateRate(charge.Rate, 0, shipment.Items.TotalActualWeight(), 0)
                                                 .ApplyManipulations(charge.FloorValue, charge.CeilingValue);
                                var c = new RateCharge
                                {
                                    ChargeCodeId = charge.Service.ChargeCodeId,
                                    UnitBuy = cost,
                                    Quantity = 1,
                                    UnitDiscount = 0,
                                    UnitSell = cost
                                };
                                var markup = markups.FirstOrDefault(m => m.ServiceId == charge.ServiceId);
                                if (markup != null)
                                    c.UnitSell = cost
                                        .CalculateMarkedRate(markup.MarkupPercent, markup.MarkupValue, markup.UseMinimum)
                                        .ApplyManipulations(markup.ServiceChargeFloor, markup.ServiceChargeCeiling);
                                rate.Charges.Add(c);
                                guaranteedDeliveryAllowed = false;
                            }
                    }

                    // overlength rules
                    if (rate.CFCRule == null && vRating.HasLTLOverLengthRules)
                    {
                        var rules = from r in GetEffectiveOverlengthRules(vRating, shipment.DesiredPickupDate.TimeToMaximum())
                                    from i in
                                        shipment.Items.Where(
                                            qi => r.LowerLengthBound <= qi.ActualLength && qi.ActualLength < r.UpperLengthBound)
                                    select r;

                        if (rules.Any()) rate.OverlengthRuleApplied = true;

                        foreach (var rule in rules)
                        {
                            guaranteedDeliveryAllowed = false;
                            rate.Charges.Add(new RateCharge
                            {
                                ChargeCodeId = rule.ChargeCodeId,
                                UnitBuy = rule.Charge,
                                Quantity = 1,
                                UnitDiscount = 0,
                                UnitSell = rule.Charge
                            });
                        }

                    }


                    // If Guaranteed Service Level, and we have accessorials, don't include the rate
                    if (rate.LTLSellRate.VendorRating.Smc3ServiceLevel.IsGuaranteedService() && !guaranteedDeliveryAllowed)
                    {
                        retVal.Remove(key);
                        continue;
                    }


                    // guaranteed charges (NOTE: must occur after fuel calculation as all guaranteed charges are added in to the list so as not to skew fuel)
                    if (shipment.ServiceMode == ServiceMode.LessThanTruckload
                        && !shipment.Services.Any()
                        && vRating.LTLGuaranteedCharges.Any()
                        && shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup) <= shipment.DesiredPickupDate.SetTime(LatestEarlyPickupTimeForGuaranteedServices)
                        && shipment.DesiredPickupDate.SetTime(shipment.LatePickup) <= shipment.DesiredPickupDate.SetTime(LatestLatePickupTimeForGuaranteedServices)
                        && guaranteedDeliveryAllowed)
                    {
                        foreach (var gCharge in vRating.LTLGuaranteedCharges.Where(c => c.EffectiveDate <= shipment.DesiredPickupDate.TimeToMaximum()))
                        {
                            var unitBuy = gCharge.RateType == RateType.Flat ? gCharge.Rate : totalLinehaulCost * (gCharge.Rate / 100);
                            if (unitBuy > gCharge.CeilingValue) unitBuy = gCharge.CeilingValue;
                            if (unitBuy < gCharge.FloorValue) unitBuy = gCharge.FloorValue;

                            rate.Charges.Add(new RateCharge
                            {
                                ChargeCodeId = gCharge.ChargeCodeId,
                                LTLGuaranteedChargeId = gCharge.Id,
                                Quantity = 1,
                                UnitBuy = unitBuy,
                                UnitDiscount = 0,
                                UnitSell = unitBuy.CalculateMarkedRate(markupPercent, 0, false)
                            });
                        }
                    }
                }
            }

            // process project 44 rates
            foreach (var p44RateGroup in p44Rates)
            {
                if (retVal.ContainsKey(p44RateGroup.Key))
                {
                    var standardP44Rate = p44RateGroup.Value.FirstOrDefault(r => r.ServiceLevel.Code == ServiceLevel.CodeEnum.STD);

                    if (standardP44Rate == null)
                    {
                        retVal.Remove(p44RateGroup.Key);
                        continue;
                    }

                    // rate basics
                    var rate = retVal[p44RateGroup.Key];
                    var chargeSearch = new P44ChargeCodeMappingSearch();//new ChargeCodeSearch();
                    var sellRate = rate.LTLSellRate;

                    rate.IsProject44Rate = true;
                    rate.Project44QuoteNumber = standardP44Rate.CapacityProviderQuoteNumber;
                    rate.DirectPointRate = standardP44Rate.LaneType == RateQuote.LaneTypeEnum.DIRECT;
                    rate.Mode = ServiceMode.LessThanTruckload;
                    rate.OriginalRateValue = standardP44Rate.RateQuoteDetail.Total.ToDecimal();

                    rate.TransitDays = standardP44Rate.TransitDays.ToInt();
                    rate.EstimatedDeliveryDate = standardP44Rate.DeliveryDateTime.ToDateTime();

                    // freight/line haul charges
                    var totalWeight = shipment.Items.TotalActualWeight();
                    var markupPercent = sellRate.MarkupPercent;
                    var markupValue = sellRate.MarkupValue;

                    rate.MarkupPercent = markupPercent;
                    rate.MarkupValue = markupValue;
                    rate.TotalActualShipmentWeight = totalWeight;

                    // freight  & fuel charges
                    var p44Charges = standardP44Rate.RateQuoteDetail.Charges.Where(c =>
                        !Project44Constants.GuaranteedServiceLevels.Select(l => l.ToString()).Contains(c.Code)).Select(
                        c =>
                        {
                            //TODO: use p44 charge code mapping to get correct charge code
                            var p44CcId = chargeSearch.FetchP44ChargeCodeIdByProject44Code(c.Code.ToEnum<Project44ChargeCode>().ToInt(), sellRate.TenantId);

                            return new
                            {
                                c.Code,
                                c.Amount,
                                ChargeCode = p44CcId == default(long) ? new ChargeCode(sellRate.Tenant.DefaultUnmappedChargeCodeId) : new ChargeCode(p44CcId),
                                Comment = p44CcId != default(long) ? string.Empty : $"{c.Code}-{c.Description}"
                            };
                        }).ToList();

                    // we cannot map P44 charges if charge codes are not setup properly, and we cannot simply exclude unmapped charges
                    if (p44Charges.Any(c => c.ChargeCode.Id == default(long)))
                        continue;

                    rate.Charges.AddRange(p44Charges
                        .Where(c => (c.ChargeCode.Category == ChargeCodeCategory.Fuel ||
                                     c.ChargeCode.Category == ChargeCodeCategory.Freight) &&
                                    c.Code != Project44ChargeCode.DSC.ToString()).Select(c => new RateCharge
                                    {
                                        ChargeCodeId = c.ChargeCode.Id,
                                        UnitBuy = c.Amount.ToDecimal(),
                                        UnitDiscount = default(decimal),
                                        UnitSell = c.Amount.ToDecimal()
                                .CalculateMarkedRate(markupPercent, markupValue, sellRate.UseMinimum),
                                        Quantity = 1,
                                        Comment = c.Comment
                                    }));


                    // discount charge
                    var discountCharge = p44Charges.FirstOrDefault(c => c.Code == Project44ChargeCode.DSC.ToString());
                    if (discountCharge != null)
                    {
                        rate.Charges.Add(new RateCharge
                        {
                            ChargeCodeId = discountCharge.ChargeCode.Id,
                            UnitBuy = discountCharge.Amount.ToDecimal(),
                            UnitDiscount = discountCharge.Amount.ToDecimal() * -1,
                            UnitSell = default(decimal),
                            Quantity = 1,
                        });
                    }

                    // Accessorials & services
                    var markups = GetEffectiveCustomerServiceMarkups(shipment.Customer.Rating, shipment.DesiredPickupDate.TimeToMaximum());

                    rate.Charges.AddRange(p44Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Accessorial || c.ChargeCode.Category == ChargeCodeCategory.Service).Select(c =>
                    {
                        var markup = markups.FirstOrDefault(m => m.Service.ChargeCodeId == c.ChargeCode.Id);

                        return new RateCharge
                        {
                            ChargeCodeId = c.ChargeCode.Id,
                            UnitBuy = c.Amount.ToDecimal(),
                            UnitDiscount = 0,
                            UnitSell = markup != null
                                            ? c.Amount.ToDecimal()
                                               .CalculateMarkedRate(markup.MarkupPercent, markup.MarkupValue, markup.UseMinimum)
                                               .ApplyManipulations(markup.ServiceChargeFloor, markup.ServiceChargeCeiling)
                                            : c.Amount.ToDecimal(),
                            Quantity = 1
                        };
                    }));

                    // guaranteed charges
                    var guaranteedCharges = new List<RateCharge>();
                    foreach (var guarRate in p44RateGroup.Value.Where(r => Project44Constants.GuaranteedServiceLevels.Contains(r.ServiceLevel.Code)))
                    {
                        var gCharges = guarRate.RateQuoteDetail.Charges.Where(c => !rate.Charges.Select(chrg => chrg.ChargeCode.Project44Code.ToString()).Contains(c.Code));
                        guaranteedCharges.AddRange(gCharges.Select(c => new RateCharge
                        {
                            ChargeCodeId = chargeSearch.FetchP44ChargeCodeIdByProject44Code(c.Code.ToEnum<Project44ChargeCode>().ToInt(), sellRate.TenantId),
                            Quantity = 1,
                            UnitBuy = c.Amount.ToDecimal(),
                            UnitDiscount = 0,
                            UnitSell = c.Amount.ToDecimal().CalculateMarkedRate(markupPercent, 0, false),
                            IsProject44GuaranteedCharge = true,
                            IsProject44ExpeditedServiceCharge = false,
                            Project44QuoteNumber = guarRate.CapacityProviderQuoteNumber,
                            Comment = guarRate.DeliveryDateTime != null ? guarRate.DeliveryDateTime.ToString() : string.Empty
                        }));
                    }
                    rate.Charges.AddRange(guaranteedCharges.Where(c => c.ChargeCodeId != default(long)));


                    // expedited service
                    var expeditedCharges = new List<RateCharge>();
                    foreach (var expediteRate in p44RateGroup.Value.Where(r => Project44Constants.ExpeditedServiceLevels.Contains(r.ServiceLevel.Code)))
                    {
                        var expediteCharges = expediteRate.RateQuoteDetail.Charges.Where(c => !rate.Charges.Select(chrg => chrg.ChargeCode.Project44Code.ToString()).Contains(c.Code));
                        expeditedCharges.AddRange(expediteCharges.Select(c => new RateCharge
                        {
                            ChargeCodeId = chargeSearch.FetchP44ChargeCodeIdByProject44Code(c.Code.ToEnum<Project44ChargeCode>().ToInt(), sellRate.TenantId),
                            Quantity = 1,
                            UnitBuy = c.Amount.ToDecimal(),
                            UnitDiscount = 0,
                            UnitSell = c.Amount.ToDecimal().CalculateMarkedRate(markupPercent, 0, false),
                            IsProject44GuaranteedCharge = false,
                            IsProject44ExpeditedServiceCharge = true,
                            Project44QuoteNumber = expediteRate.CapacityProviderQuoteNumber,
                            Comment = expediteRate.DeliveryDateTime != null ? expediteRate.DeliveryDateTime.ToString() : string.Empty
                        }));
                    }
                    rate.Charges.AddRange(expeditedCharges.Where(c => c.ChargeCodeId != default(long)));
                }
            }

            return retVal.Values;
        }

        private IEnumerable<Rate> ProcessLTLPackageSpecificRates(Shipment shipmentToRate)
        {
            var shipment = shipmentToRate;

            if (shipment.Items.Select(i => i.PackageTypeId).Distinct().Count() != 1)
                return new Rate[0]; // must only have one and only one package type

            var packageTypeId = shipment.Items.First().PackageTypeId;
            var quantity = shipment.Items.Sum(i => i.Quantity);

            // filter for effective dates + vendor ratings + indirect points if necessary
            var serviceIds = shipment.Services.Select(qs => qs.ServiceId).ToList();

            // region filter
            var rFilter = GetEffectiveSellRates(shipment.Customer.Rating, shipment.DesiredPickupDate.TimeToMaximum())
                .Where(r => r.VendorRating.EnableLTLPackageSpecificRates &&
                            r.VendorRating.LTLPackageSpecificRatePackageTypeId == packageTypeId &&
                            r.VendorRating.LTLPackageSpecificRates.Any(d => _originRegionIds.Contains(d.OriginRegionId) &&
                                                                            _destRegionIds.Contains(d.DestinationRegionId) &&
                                                                            d.PackageQuantity == quantity))
                .ToList();

            var aFilter = rFilter
                .Where(r =>
                {
                    var accessorialServiceIds = r.VendorRating.LTLAccessorials.Select(s => s.ServiceId).ToList();
                    return !serviceIds.Any() || serviceIds.All(accessorialServiceIds.Contains);
                }) // accessorial filter
                .ToList();

            var filteredSellRates = aFilter
                .Where(r => r.VendorRating.FitsLinearFootage(shipment) && r.VendorRating.CanHoldWeightOf(shipment) && r.VendorRating.ItemsWithinLimits(shipment))
                .ToList();


            if (!filteredSellRates.Any()) return new Rate[0]; // return empty result set if no ratings!

            // setup rateware service
            var rateware = new Rateware2(_carrierConnectSettings);

            // get transit days
            var transitDaysRequests = filteredSellRates
                .Select(fsr => new TransitDaysRequest
                {
                    DestinationCountry = shipment.Destination.Country.GetSmcRatingCountry(),
                    DestinationPostalCode = shipment.Destination.PostalCode,
                    OriginCountry = shipment.Origin.Country.GetSmcRatingCountry(),
                    OriginPostalCode = shipment.Origin.PostalCode,
                    ShipmentId = fsr.VendorRating.Id.ToString(),
                    IndirectPoint = fsr.LTLIndirectPointEnabled,
                    Mode = ServiceMode.LessThanTruckload,
                    Scac = fsr.VendorRating.Vendor.Scac,
                    Smc3ServiceLevel = fsr.VendorRating.Smc3ServiceLevel,
                    EarlyPickup = shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup),
                    LatePickup = shipment.DesiredPickupDate.SetTime(shipment.LatePickup),
                    Key = fsr.Id.GetString(),
                })
                .ToList();
            var transitInfos = rateware.GetTransitDays(transitDaysRequests);

            // process each vendor rating profile
            var rates = new List<Rate>();
            foreach (var sellRate in filteredSellRates)
            {
                var rating = sellRate.VendorRating;

                // filter for effective dates
                var psrate = GetEffectiveLTLPackageSpecificRate(rating, shipment.DesiredPickupDate.TimeToMaximum(), quantity);

                // if no valid tier, continue to next record
                if (psrate == null) continue;

                // get transit info. If null continue to next record
                var transitInfo = transitInfos.FirstOrDefault(ti => ti.Key == sellRate.Id.GetString());
                if (transitInfo == null) continue;

                // rate 
                var rate = new Rate
                {
                    ApplyDiscount = false,
                    LTLSellRate = sellRate,
                    CFCRule = null,
                    IsLTLPackageSpecificRate = true,
                    DirectPointRate = true,
                    Mode = ServiceMode.LessThanTruckload,
                    OriginalRateValue = psrate.Rate,
                    TransitDays = transitInfo.TransitDays,
                    EstimatedDeliveryDate = transitInfo.EstimatedDeliveryDate,
                    BilledWeight = shipment.Items.Sum(i => i.ActualWeight),
                    RatedWeight = shipment.Items.Sum(i => i.ActualWeight),
                    RatedCubicFeet = shipment.Items.TotalActualCubitFeet(),
                    OriginTerminalCode = transitInfo.OriginTerminalCode,
                    DestinationTerminalCode = transitInfo.DestinationTerminalCode,
                };


                // freight/line haul charges
                var freightCost = psrate.Rate;

                var totalWeight = shipment.Items.TotalActualWeight();
                var markupPercent = sellRate.GetMarkupPercent(totalWeight);
                var markupValue = sellRate.GetMarkupValue(totalWeight);

                rate.TotalActualShipmentWeight = totalWeight;
                rate.MarkupValue = markupValue;
                rate.MarkupPercent = markupPercent;

                rate.Charges.Add(new RateCharge
                {
                    ChargeCodeId = rating.LTLPackageSpecificFreightChargeCodeId,
                    UnitBuy = freightCost,
                    UnitDiscount = 0,
                    UnitSell = freightCost.CalculateMarkedRate(markupPercent, markupValue, sellRate.UseMinimum),
                    Quantity = 1
                });

                var totalFreightCharge = rate.Charges.Sum(c => c.FinalSell); // for calculations
                var totalFreightCost = rate.Charges.Sum(c => c.FinalBuy);

                // fuel
                rate.FuelMarkupPercent = rating.CurrentLTLFuelMarkup
                                               .ApplyManipulations(rating.FuelMarkupFloor, rating.FuelMarkupCeiling);
                rate.Charges.Add(new RateCharge
                {
                    ChargeCodeId = rating.FuelChargeCodeId,
                    UnitBuy = rating.CurrentLTLFuelMarkup
                                        .ApplyManipulations(rating.FuelMarkupFloor, rating.FuelMarkupCeiling)
                                        .CalculateLineHaulPercentageRate(totalFreightCost),
                    UnitDiscount = 0,
                    Quantity = 1,
                    UnitSell = rating.CurrentLTLFuelMarkup
                                         .ApplyManipulations(rating.FuelMarkupFloor, rating.FuelMarkupCeiling)
                                         .CalculateLineHaulPercentageRate(totalFreightCharge)
                });

                // Accessorial charges
                if (serviceIds.Any())
                {
                    var markups = GetEffectiveCustomerServiceMarkups(shipment.Customer.Rating,
                                                                     shipment.DesiredPickupDate.TimeToMaximum());
                    foreach (var charge in GetEffectiveAccessorials(rating, shipment.DesiredPickupDate.TimeToMaximum()))
                        if (serviceIds.Contains(charge.ServiceId))
                        {
                            var cost = charge.RateType
                                             .CalculateRate(charge.Rate, 0, shipment.Items.TotalActualWeight(), 0)
                                             .ApplyManipulations(charge.FloorValue, charge.CeilingValue);
                            var c = new RateCharge
                            {
                                ChargeCodeId = charge.Service.ChargeCodeId,
                                UnitBuy = cost,
                                Quantity = 1,
                                UnitDiscount = 0,
                                UnitSell = cost
                            };
                            var markup = markups.FirstOrDefault(m => m.ServiceId == charge.ServiceId);
                            if (markup != null)
                                c.UnitSell = cost
                                    .CalculateMarkedRate(markup.MarkupPercent, markup.MarkupValue, markup.UseMinimum)
                                    .ApplyManipulations(markup.ServiceChargeFloor, markup.ServiceChargeCeiling);
                            rate.Charges.Add(c);
                        }
                }

                // overlength rules
                if (rating.HasLTLOverLengthRules)
                {
                    var rules = from r in GetEffectiveOverlengthRules(rating, shipment.DesiredPickupDate.TimeToMaximum())
                                from i in
                                    shipment.Items.Where(
                                        qi => r.LowerLengthBound <= qi.ActualLength && qi.ActualLength < r.UpperLengthBound)
                                select r;

                    if (rules.Any()) rate.OverlengthRuleApplied = true;

                    foreach (var rule in rules)
                        rate.Charges.Add(new RateCharge
                        {
                            ChargeCodeId = rule.ChargeCodeId,
                            UnitBuy = rule.Charge,
                            Quantity = 1,
                            UnitDiscount = 0,
                            UnitSell = rule.Charge
                        });
                }

                // additional charges
                if (rating.HasAdditionalCharges)
                {
                    var search = new VendorRatingSearch();
                    foreach (var charge in GetEffectiveAdditionalCharges(rating, shipment.DesiredPickupDate.TimeToMaximum()))
                    {
                        var index = search.ApplicableAdditionalChargeIndex(
                            charge.TenantId,
                            charge.Id,
                            shipment.Origin.CountryId,
                            shipment.Origin.PostalCode,
                            shipment.Destination.CountryId,
                            shipment.Destination.PostalCode);

                        if (index != null)
                        {
                            var cost = index.RateType
                                            .CalculateRate(index.Rate, shipment.Mileage, shipment.Items.TotalActualWeight(),
                                                           totalFreightCharge)
                                            .ApplyManipulations(index.ChargeFloor, index.ChargeCeiling);
                            rate.Charges.Add(new RateCharge
                            {
                                ChargeCodeId = charge.ChargeCodeId,
                                UnitBuy = cost,
                                Quantity = 1,
                                UnitDiscount = 0,
                                UnitSell = cost
                            });
                        }
                    }
                }

                // add rate to rates
                rates.Add(rate);
            }

            return rates;
        }



        private static IEnumerable<SmallPackRate> GetEffectiveSmallPackRates(CustomerRating rating,
                                                                             DateTime effectiveDateCutOff)
        {
            var rates = new Dictionary<string, List<SmallPackRate>>();
            foreach (var s in rating.SmallPackRates.Where(r => r.Active))
            {
                var key = string.Format("{0}-{1}", s.SmallPackageEngine, s.SmallPackType);
                if (!rates.ContainsKey(key)) rates.Add(key, new List<SmallPackRate>());
                if (s.EffectiveDate <= effectiveDateCutOff) rates[key].Add(s);
            }
            var filteredRates = rates.Keys
                                     .Select(k => rates[k].OrderByDescending(a => a.EffectiveDate).FirstOrDefault())
                                     .Where(c => c != null)
                                     .ToList();
            return filteredRates;
        }

        private static IEnumerable<LTLAdditionalCharge> GetEffectiveAdditionalCharges(VendorRating rating, DateTime effectiveDateCutOff)
        {
            var rules = new Dictionary<string, List<LTLAdditionalCharge>>();
            foreach (var c in rating.LTLAdditionalCharges)
            {
                if (!rules.ContainsKey(c.Name)) rules.Add(c.Name, new List<LTLAdditionalCharge>());
                if (c.EffectiveDate <= effectiveDateCutOff) rules[c.Name].Add(c);
            }
            var filteredCharges = rules.Keys
                                       .Select(k => rules[k].OrderByDescending(a => a.EffectiveDate).FirstOrDefault())
                                       .Where(c => c != null)
                                       .ToList();
            return filteredCharges;
        }

        private static IEnumerable<LTLOverLengthRule> GetEffectiveOverlengthRules(VendorRating rating,
                                                                                  DateTime effectiveDateCutOff)
        {
            var rules = new Dictionary<string, List<LTLOverLengthRule>>();
            foreach (var r in rating.LTLOverLengthRules)
            {
                var key = string.Format("{0}-{1}", r.LowerLengthBound, r.UpperLengthBound);
                if (!rules.ContainsKey(key)) rules.Add(key, new List<LTLOverLengthRule>());
                if (r.EffectiveDate <= effectiveDateCutOff) rules[key].Add(r);
            }
            var filteredRules = rules.Keys
                                     .Select(k => rules[k].OrderByDescending(a => a.EffectiveDate).FirstOrDefault())
                                     .Where(a => a != null)
                                     .ToList();
            return filteredRules;
        }

        private static IEnumerable<CustomerServiceMarkup> GetEffectiveCustomerServiceMarkups(CustomerRating rating,
                                                                                             DateTime effectiveDateCutOff)
        {
            var markups = new Dictionary<long, List<CustomerServiceMarkup>>();
            foreach (var a in rating.CustomerServiceMarkups.Where(r => r.Active))
            {
                if (!markups.ContainsKey(a.ServiceId)) markups.Add(a.ServiceId, new List<CustomerServiceMarkup>());
                if (a.EffectiveDate <= effectiveDateCutOff) markups[a.ServiceId].Add(a);
            }
            var filteredMarkups = markups.Keys
                                         .Select(k => markups[k].OrderByDescending(a => a.EffectiveDate).FirstOrDefault())
                                         .Where(a => a != null)
                                         .ToList();
            return filteredMarkups;
        }

        private static IEnumerable<LTLAccessorial> GetEffectiveAccessorials(VendorRating rating, DateTime effectiveDateCutOff)
        {
            var accessorials = new Dictionary<long, List<LTLAccessorial>>();
            foreach (var a in rating.LTLAccessorials)
            {
                if (!accessorials.ContainsKey(a.ServiceId)) accessorials.Add(a.ServiceId, new List<LTLAccessorial>());
                if (a.EffectiveDate <= effectiveDateCutOff) accessorials[a.ServiceId].Add(a);
            }
            var filteredAccessorials = accessorials.Keys
                                                   .Select(
                                                       k => accessorials[k].OrderByDescending(a => a.EffectiveDate).FirstOrDefault())
                                                   .Where(a => a != null)
                                                   .ToList();
            return filteredAccessorials;
        }

        private DiscountTier GetEffectiveDiscountTier(VendorRating rating, DateTime effectiveDateCutOff)
        {
            var list = new Dictionary<string, List<DiscountTier>>();
            foreach (var t in rating.LTLDiscountTiers)
            {
                var key = string.Format("{0}-{1}", t.OriginRegionId.ToString(), t.DestinationRegionId.ToString());
                if (!list.ContainsKey(key)) list.Add(key, new List<DiscountTier>());
                if (t.EffectiveDate <= effectiveDateCutOff) list[key].Add(t);
            }

            var tier = list.Keys
                           .Select(k => list[k].OrderByDescending(r => r.EffectiveDate).FirstOrDefault())
                           .Where(t => t != null)
                           .OrderBy(t => t.TierPriority)
                           .FirstOrDefault(
                               t => _originRegionIds.Contains(t.OriginRegionId) && _destRegionIds.Contains(t.DestinationRegionId));
            return tier;
        }

        private LTLPackageSpecificRate GetEffectiveLTLPackageSpecificRate(VendorRating rating, DateTime effectiveDateCutOff,
                                                                          int packageQuantity)
        {
            var list = new Dictionary<string, List<LTLPackageSpecificRate>>();
            foreach (var t in rating.LTLPackageSpecificRates.Where(t => t.PackageQuantity == packageQuantity))
            {
                var key = string.Format("{0}-{1}", t.OriginRegionId.ToString(), t.DestinationRegionId.ToString());
                if (!list.ContainsKey(key)) list.Add(key, new List<LTLPackageSpecificRate>());
                if (t.EffectiveDate <= effectiveDateCutOff) list[key].Add(t);
            }

            var tier = list.Keys
                           .Select(k => list[k].OrderByDescending(r => r.EffectiveDate).FirstOrDefault())
                           .Where(t => t != null)
                           .OrderBy(t => t.RatePriority)
                           .FirstOrDefault(
                               t => _originRegionIds.Contains(t.OriginRegionId) && _destRegionIds.Contains(t.DestinationRegionId));
            return tier;
        }

        private static IEnumerable<LTLSellRate> GetEffectiveSellRates(CustomerRating customerRating, DateTime effectiveDateCutOff)
        {
            var sellRateList = new Dictionary<long, List<LTLSellRate>>();
            foreach (var rate in customerRating.LTLSellRates.Where(r => r.VendorRating.Active && r.VendorRating.Vendor.Active && r.Active))
            {
                if (!sellRateList.ContainsKey(rate.VendorRatingId)) sellRateList.Add(rate.VendorRatingId, new List<LTLSellRate>());
                if (rate.EffectiveDate <= effectiveDateCutOff && rate.VendorRating.ExpirationDate >= effectiveDateCutOff) sellRateList[rate.VendorRatingId].Add(rate);
            }
            var filteredSellRates = sellRateList.Keys
                                                .Select(
                                                    k => sellRateList[k].OrderByDescending(r => r.EffectiveDate).FirstOrDefault())
                                                .Where(i => i != null)
                                                .ToList();
            return filteredSellRates;
        }


        private static void UpdateShipmentItemClassByPcf(Shipment shipment, VendorRating rating)
        {
            var cuffOffDate = shipment.DesiredPickupDate;
            var convTab = rating.LTLPcfToFcConvTab
                                .Where(t => t.EffectiveDate <= cuffOffDate)
                                .OrderByDescending(t => t.EffectiveDate)
                                .FirstOrDefault();
            if (convTab == null) return;

            foreach (var item in shipment.Items) convTab.SetItemActualFreightClass(item);
        }



        private List<Rate> GetTLRates(Shipment shipment, IList<Location> locations, TLTariffType tlTariff)
        {
            // variable/commons
            var tenantId = shipment.TenantId;
            var ratingId = shipment.Customer.RatingId;
            var totalWeight = shipment.Items.TotalActualWeight();
            var equipId = shipment.Equipments.Any() ? shipment.Equipments[0].EquipmentTypeId : default(long);
            var effDate = shipment.DesiredPickupDate.TimeToMaximum();
            var milesageSourceId = shipment.Customer.RequiredMileageSourceId;

            // first segment setup
            var prates = new List<PhantomRate> { new PhantomRate(locations[0], locations[1]) };
            locations.RemoveAt(0); // remove 1st stop
            do
            {
                locations.RemoveAt(0); // remove current stop (destination)

                var newRates = new List<PhantomRate>();
                foreach (var prate in prates)
                {
                    var sellRates = RetrieveApplicableTLSellRates(tlTariff, prate.Origin, prate.Destination, totalWeight,
                                                                  tenantId, ratingId, effDate, milesageSourceId, equipId)
                        .Select(r => new
                        {
                            Key = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}", r.OriginCity, r.OriginState, r.OriginPostalCode,
                                                    r.OriginCountryId, r.DestinationCity, r.DestinationState, r.DestinationPostalCode,
                                                    r.DestinationCountryId, r.EquipmentTypeId, r.TariffType.ToInt()),
                            Rate = r
                        })
                        .GroupBy(r => r.Key)
                        .Select(i => i.OrderByDescending(x => x.Rate.EffectiveDate).First().Rate)
                        .ToList();

                    if (sellRates.Count() == 1)
                    {
                        var r = sellRates[0].ToRate(shipment);
                        prate.Charges.AddRange(r.Charges.Select(c => (Charge)c));
                        prate.Mileage += sellRates[0].Mileage;
                        prate.TLRateDetails.Add(new TLRateDetail
                        {
                            OriginFullAddress = prate.Origin.FullAddress,
                            DestinationFullAddress = prate.Destination.FullAddress,
                            Mileage = r.Mileage,
                            Rate = sellRates[0].Rate,
                            RateType = sellRates[0].RateType,
                            TotalWeight = totalWeight,
                            TariffType = sellRates[0].TariffType
                        });
                        newRates.Add(prate);
                    }
                    else if (sellRates.Count() > 1)
                    {
                        foreach (var sellRate in sellRates)
                        {
                            var r = sellRate.ToRate(shipment);
                            var nrate = new PhantomRate(prate);
                            nrate.Charges.AddRange(r.Charges.Select(c => (Charge)c));
                            nrate.Mileage += r.Mileage;
                            nrate.TLRateDetails.Add(new TLRateDetail
                            {
                                OriginFullAddress = nrate.Origin.FullAddress,
                                DestinationFullAddress = nrate.Destination.FullAddress,
                                Mileage = r.Mileage,
                                Rate = sellRate.Rate,
                                RateType = sellRate.RateType,
                                TotalWeight = totalWeight,
                                TariffType = sellRate.TariffType
                            });
                            newRates.Add(nrate);

                        }
                    }
                }

                // reset prates for next stop
                prates.Clear();
                if (!locations.Any()) prates = newRates;
                else
                {
                    foreach (var nrate in newRates)
                    {
                        nrate.NextSegment(locations[0]);
                        prates.Add(nrate);
                    }
                }
            } while (locations.Any());

            return prates.Select(pr => pr.ToRate(shipment)).ToList();
        }



        private void LoadOriginDestinationRegionIds(CustomerRating rating, string originPostalCode, long originCountryId,
                                                    string destinationPostalCode, long destinationCountryId, long tenantId)
        {
            _parameters[RatingIdKey] = rating.Id;
            _parameters[PostalCodeKey] = originPostalCode;
            _parameters[TenantKey] = tenantId;
            _parameters[CountryId] = originCountryId;


            _originRegionIds.Clear();
            using (var reader = GetReader(CustomerRatingOriginRegion, _parameters))
                while (reader.Read())
                    _originRegionIds.Add(reader.GetValue(reader.GetOrdinal("Id")).ToLong());
            Connection.Close();

            _parameters[PostalCodeKey] = destinationPostalCode;
            _parameters[CountryId] = destinationCountryId;
            _destRegionIds.Clear();
            using (var reader = GetReader(CustomerRatingDestRegion, _parameters))
                while (reader.Read())
                    _destRegionIds.Add(reader.GetValue(reader.GetOrdinal("Id")).ToLong());
            Connection.Close();
        }

        private IEnumerable<TLSellRate> RetrieveApplicableTLSellRates(TLTariffType tariffType, Location origin, Location destination,
                                                               decimal totalWeight, long tenantId, long customerRatingId, DateTime shipmentDate,
                                                               long mileageSourceId, long equipmentTypeId = default(long))
        {
            if (((string.IsNullOrEmpty(origin.City) || string.IsNullOrEmpty(origin.State)) && string.IsNullOrEmpty(origin.PostalCode)) ||
                ((string.IsNullOrEmpty(destination.City) || string.IsNullOrEmpty(destination.State)) && string.IsNullOrEmpty(destination.PostalCode)))
                return new List<TLSellRate>();

            var sellRates = new List<TLSellRate>();

            const string query = @"Select
							*
						From
							TLSellRate
						Where
							TenantId = @TenantId
							And MinimumWeight <= @Weight 
							And @Weight <= MaximumWeight
							And TariffType = @TariffType
							And EquipmentTypeId = @EquipmentTypeId
							And CustomerRatingId = @CustomerRatingId
							And (@OriginCity = '' Or OriginCity = @OriginCity)
							And (@OriginState = '' Or OriginState = @OriginState)
							And (@OriginPostalCode = '' Or OriginPostalCode = @OriginPostalCode)
							And OriginCountryId = @OriginCountryId
							And (@DestinationCity = '' Or DestinationCity = @DestinationCity)
							And (@DestinationState = '' Or DestinationState = @DestinationState)
							And (@DestinationPostalCode = '' Or DestinationPostalCode = @DestinationPostalCode)
							And DestinationCountryId = @DestinationCountryId
							And MileageSourceId = @MileageSourceId
							And EffectiveDate <= @ShipmentDate";

            var parameters = new Dictionary<string, object>
                {
                    {"TenantId", tenantId},
                    {"Weight", totalWeight},
                    {"TariffType", tariffType.ToInt()},
                    {"EquipmentTypeId", equipmentTypeId},
                    {"CustomerRatingId", customerRatingId},
                    {"OriginCity", origin.City.GetString()},
                    {"OriginState", origin.State.GetString()},
                    {"OriginPostalCode", origin.PostalCode.GetString()},
                    {"OriginCountryId", origin.CountryId},
                    {"DestinationCity", destination.City.GetString()},
                    {"DestinationState", destination.State.GetString()},
                    {"DestinationPostalCode", destination.PostalCode.GetString()},
                    {"DestinationCountryId", destination.CountryId},
                    {"MileageSourceId", mileageSourceId},
                    {"ShipmentDate", shipmentDate}
                };


            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    sellRates.Add(new TLSellRate(reader));
            Connection.Close();

            return sellRates;
        }

    }
}