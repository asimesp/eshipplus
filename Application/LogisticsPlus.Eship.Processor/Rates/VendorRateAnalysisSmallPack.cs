﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.SmallPacks;
using LogisticsPlus.UpsPlugin;
using LogisticssPlus.FedExPlugin.Services;

namespace LogisticsPlus.Eship.Processor.Rates
{
	public class VendorRateAnalysisSmallPack
	{
		private readonly ServiceParams _fedExServiceParams;
		private readonly UpsServiceParams _upsServiceParams;
		private readonly List<SmallPackagingMap> _packagingMaps;

		public VendorRateAnalysisSmallPack(ServiceParams fedexServiceParameters, UpsServiceParams upsServiceParams, List<SmallPackagingMap> packagingMaps)
		{
			_fedExServiceParams = fedexServiceParameters;
			_upsServiceParams = upsServiceParams;
			_packagingMaps = packagingMaps;
		}

		public void RunAnalysis(SmallPackageEngine engine, string engineServiceType, PackageType packageType, List<Lane> lanes, DateTime effectiveDate)
		{
			switch (engine)
			{
				case SmallPackageEngine.FedEx:
					RunFedExAnalysis(lanes, engineServiceType, packageType, effectiveDate);
					break;
				case SmallPackageEngine.Ups:
					RunUpsAnalysis(lanes, engineServiceType, packageType, effectiveDate);
					break;
			}
		}

		private void RunFedExAnalysis(IEnumerable<Lane> lanes, string engineServicetype, PackageType packageType, DateTime effectiveDate)
		{
			if (_fedExServiceParams == null) return;

			var handler = new FedExPluginHandler(_fedExServiceParams, _packagingMaps, new List<SmallPackageServiceMap>());

			foreach (var lane in lanes)
			{
				try
				{
					var shipment = new Shipment
									{
										DesiredPickupDate = effectiveDate,
										Origin = new ShipmentLocation
													{
														PostalCode = lane.OriginPostalCode,
														CountryId = lane.OriginCountryId
													},
										Destination = new ShipmentLocation
														{
															PostalCode = lane.DestinationPostalCode,
															CountryId = lane.DestinationCountryId
														},
										Items = new List<ShipmentItem>
					               		        	{
					               		        		new ShipmentItem
					               		        			{
					               		        				ActualLength = lane.Length,
					               		        				ActualWidth = lane.Width,
					               		        				ActualHeight = lane.Height,
					               		        				ActualWeight = lane.ActualWeight,
					               		        				Quantity = 1,
																PieceCount = 1,
																PackageType = packageType
					               		        			}
					               		        	},
									};

					var rates = handler.GetRates(shipment);
					var rate = rates.FirstOrDefault(r => r.SmallPackServiceType == engineServicetype);
					
					if (rate == null)
					{
						lane.OriginalValue = 0;
						lane.BilledWeight = 0;
						lane.DiscountPercent = 0;
						lane.FuelPercent = 0;
						lane.Comment = "No rate found";
						lane.CurrentTransitDays = 0;
					}
					else
					{
						lane.OriginalValue = rate.OriginalRateValue;
						lane.BilledWeight = rate.BilledWeight;
						lane.DiscountPercent = 0;
						lane.FuelPercent = 0;
						lane.Comment = string.Empty;
						lane.CurrentTransitDays = rate.TransitDays;
					}
				}
				catch (Exception ex)
				{
					lane.Comment = ex.Message;
				}
			}
		}

		private void RunUpsAnalysis(IEnumerable<Lane> lanes, string engineServicetype, PackageType packageType, DateTime effectiveDate)
		{
			if (_upsServiceParams == null) return;

			var handler = new UpsPluginHandler(_upsServiceParams, _packagingMaps, new List<SmallPackageServiceMap>());

			foreach (var lane in lanes)
			{
				try
				{
					var shipment = new Shipment
					{
						DesiredPickupDate = effectiveDate,
						Origin = new ShipmentLocation
						{
							PostalCode = lane.OriginPostalCode,
							CountryId = lane.OriginCountryId
						},
						Destination = new ShipmentLocation
						{
							PostalCode = lane.DestinationPostalCode,
							CountryId = lane.DestinationCountryId
						},
						Items = new List<ShipmentItem>
					               		        	{
					               		        		new ShipmentItem
					               		        			{
					               		        				ActualLength = lane.Length,
					               		        				ActualWidth = lane.Width,
					               		        				ActualHeight = lane.Height,
					               		        				ActualWeight = lane.ActualWeight,
					               		        				Quantity = 1,
																PieceCount = 1,
																PackageType = packageType
					               		        			}
					               		        	},
					};

					var rates = handler.GetRates(shipment);
					var rate = rates.FirstOrDefault(r => r.SmallPackServiceType == engineServicetype);

					if (rate == null)
					{
						lane.OriginalValue = 0;
						lane.BilledWeight = 0;
						lane.DiscountPercent = 0;
						lane.FuelPercent = 0;
						lane.Comment = "No rate found";
						lane.CurrentTransitDays = 0;
					}
					else
					{
						lane.OriginalValue = rate.OriginalRateValue;
						lane.BilledWeight = rate.BilledWeight;
						lane.DiscountPercent = 0;
						lane.FuelPercent = 0;
						lane.Comment = string.Empty;
						lane.CurrentTransitDays = rate.TransitDays;
					}
				}
				catch (Exception ex)
				{
					lane.Comment = ex.Message;
				}
			}
		}
	}
}
