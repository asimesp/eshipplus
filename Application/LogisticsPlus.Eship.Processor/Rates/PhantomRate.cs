﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Processor.Rates
{
	public class PhantomRate
	{
		public List<Charge> Charges { get; set; }

		public decimal Mileage { get; set; }
		public TLTariffType TariffType { get; set; }
		public Location Origin { get; set; }
		public Location Destination { get; set; }

        public List<TLRateDetail> TLRateDetails { get; set; }

		public PhantomRate(PhantomRate rate)
		{
			Charges = rate.Charges.Copy();
		    TLRateDetails = rate.TLRateDetails.Select(r => new TLRateDetail(r)).ToList();
			TariffType = rate.TariffType;
			Mileage = rate.Mileage;

			// No deep copy required as we will not change values only references.
			Origin = rate.Origin;
			Destination = rate.Destination;
		}

		public PhantomRate(Location origin, Location destination)
		{
			Origin = origin;
			Destination = destination;

			Charges = new List<Charge>();
            TLRateDetails = new List<TLRateDetail>();
		}

		public void NextSegment(Location location)
		{
			Origin = Destination;
			Destination = location;
		}

		public PhantomRate Copy()
		{
			return new PhantomRate(this);
		}
	}
}
