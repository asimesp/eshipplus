﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core;
using System;

namespace LogisticsPlus.Eship.Processor
{
	public class AutoNumberProcessor : EntityBase
	{
        private const long BaseSeedValue = 10000;

        public long NextNumber(AutoNumberCode code, User user)
		{
			return NextNumber(user.TenantId, code, user);
		}

		public long NextNumber(long tenantId, AutoNumberCode code, User user)
		{
			if (!code.ToInt().EnumValueIsValid<AutoNumberCode>()) return default(long);
			if (user == null) return default(long);
            
			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				const string query = "select [Id] from AutoNumber where Code = @Code and TenantId = @TenantId";
				var parameters = new Dictionary<string, object> {{"Code", code}, {"TenantId", tenantId}};
				var autoNumber = new AutoNumber(ExecuteScalar(query, parameters).ToLong());
                autoNumber.Tenant = new Tenant(tenantId, false);

                if (!autoNumber.KeyLoaded)
				{
                    autoNumber.NextNumber = BaseSeedValue;
                    autoNumber.Code = code;
                    autoNumber.Save();
                }

				var @lock = autoNumber.ObtainLock(user, autoNumber.Id);
				if (!@lock.IsUserLock(user))
				{
					RollBackTransaction();
					return default(long);
				}
				@lock.Connection = Connection;
				@lock.Transaction = Transaction;

				var value = autoNumber.NextNumber++;

				autoNumber.Save();
				@lock.Delete();

				CommitTransaction();
				return value;
			}
			catch
			{
				RollBackTransaction();
				return default(long);
			}
		}
	}
}
