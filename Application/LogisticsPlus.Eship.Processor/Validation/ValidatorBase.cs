﻿using System.Collections.Generic;
using System.Linq;
using ObjToSql.Core;
using System;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.Processor.Validation
{
	public class ValidatorBase : EntityBase
	{
		private readonly List<ValidationMessage> _messages = new List<ValidationMessage>();

		public List<ValidationMessage> Messages
		{
			get { return _messages; }
		}

		public bool HasErrors { get { return _messages.Count(m => m.Type == ValidateMessageType.Error) > 0; } }
		public bool HasWarnings { get { return _messages.Count(m => m.Type == ValidateMessageType.Warning) > 0; } }
		public bool HasInformation { get { return _messages.Count(m => m.Type == ValidateMessageType.Information) > 0; } }
    }
}
