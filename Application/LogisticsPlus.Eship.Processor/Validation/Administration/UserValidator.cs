﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.Processor.Validation.Administration
{
	public class UserValidator : ValidatorBase
	{
		public bool IsValid(User user)
		{
			bool valid = HasAllRequiredData(user);

			if (DuplicateUserName(user)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(User user)
		{
			var valid = true;

			if (string.IsNullOrEmpty(user.Username))
			{
				Messages.Add(ValidationMessage.Error("User requires a user login name"));
				valid = false;
			}
			else if (user.Username.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("User login name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(user.AdUserName) && user.AdUserName.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("User Active Directory User Name cannot exceed 100 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(user.Password))
			{
				Messages.Add(ValidationMessage.Error("User requires a user password"));
				valid = false;
			}
			else if (user.Password.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("User password cannot exceed 50 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(user.FirstName))
			{
				Messages.Add(ValidationMessage.Error("User requires a user first name"));
				valid = false;
			}
			else if (user.FirstName.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("User first name cannot exceed 50 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(user.LastName))
			{
				Messages.Add(ValidationMessage.Error("User requires a user last name"));
				valid = false;
			}
			else if (user.LastName.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("User last name cannot exceed 50 characters"));
				valid = false;
			}

			if (user.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("User requires a tenant association"));
				valid = false;
			}

			if (user.DefaultCustomer == null)
			{
				Messages.Add(ValidationMessage.Error("User requires a default customer association"));
				valid = false;
			}

			if (user.CountryId == default(long))
			{
				Messages.Add(ValidationMessage.Error("User requires a country association"));
				valid = false;
			}

            if (!string.IsNullOrEmpty(user.QlikUserId) && user.QlikUserId.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("User qlik user id cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(user.QlikUserDirectory) && user.QlikUserDirectory.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("User qlik user directory cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(user.DatLoadboardUsername) && user.DatLoadboardUsername.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("User DAT loadboard username cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(user.DatLoadboardPassword) && user.DatLoadboardPassword.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("User DAT loadboard password cannot exceed 50 characters"));
                valid = false;
            }

		    if (!string.IsNullOrEmpty(user.Phone) && user.Phone.Length > 50)
		    {
                Messages.Add(ValidationMessage.Error("User Phone cannot exceed 50 characters"));
		        valid = false;
		    }

		    if (!string.IsNullOrEmpty(user.Mobile) && user.Mobile.Length > 50)
		    {
                Messages.Add(ValidationMessage.Error("User Mobile cannot exceed 50 characters"));
		        valid = false;
		    }

		    if (!string.IsNullOrEmpty(user.Fax) && user.Fax.Length > 50)
		    {
                Messages.Add(ValidationMessage.Error("User Fax cannot exceed 50 characters"));
		        valid = false;
		    }

		    if (!string.IsNullOrEmpty(user.Email) && user.Email.Length > 100)
		    {
                Messages.Add(ValidationMessage.Error("User Email cannot exceed 100 characters"));
		        valid = false;
		    }

			return valid;
		}

		public bool DuplicateUserName(User user)
		{
			const string query = "Select count(*) from [User] where [Username] = @Username and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Username", user.Username},
			                 		{"TenantId", user.TenantId},
			                 		{"Id", user.Id}
			                 	};
			var duplicateGroupName = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateGroupName) Messages.Add(ValidationMessage.Error("Username is already in use"));
			return duplicateGroupName;
		}

		public bool CanDeleteUser(User user)
		{
			const string query = "select dbo.UserIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", user.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("User is referenced one or more times and cannot be deleted."));
			return !used;
		}

		public bool UserShipAsExists(UserShipAs userShipAs)
		{
			const string query =
				"Select count(*) from UserShipAs where UserId = @UserId and CustomerId = @CustomerId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"UserId", userShipAs.UserId},
			                 		{"TenantId", userShipAs.TenantId},
			                 		{"CustomerId", userShipAs.CustomerId}
			                 	};
			var duplicateCsr = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateCsr)
				Messages.Add(ValidationMessage.Error("User can already ship as this customer"));
			return duplicateCsr;
		}
	}
}
