﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.Processor.Validation.Administration
{
	public class GroupUserValidator : ValidatorBase
	{
		public bool HasAllRequiredData(GroupUser groupUser)
		{
			var valid = true;

			if (groupUser.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Group user requires tenant association"));
				valid = false;
			}

			if (groupUser.User == null)
			{
				Messages.Add(ValidationMessage.Error("Group user requires user association"));
				valid = false;
			}

			if (groupUser.Group == null)
			{
				Messages.Add(ValidationMessage.Error("Group user requires group association"));
				valid = false;
			}

			return valid;
		}

		public bool GroupUserExists(GroupUser groupUser)
		{
			const string query =
				"Select count(*) from GroupUser where UserId = @UserId and GroupId = @GroupId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"UserId", groupUser.UserId},
			                 		{"TenantId", groupUser.TenantId},
			                 		{"GroupId", groupUser.Group.Id}
			                 	};
			var duplicateGroupUser = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateGroupUser) Messages.Add(ValidationMessage.Error("Group user is already in group"));
			return duplicateGroupUser;
		}
	}
}
