﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.Processor.Validation.Administration
{
    public class AdminUserPermissionValidator : ValidatorBase
    {
        public bool IsValid(AdminUserPermission adminUserPermission)
        {
            var valid = true;

            if (!HasAllRequiredData(adminUserPermission)) valid = false;
            if (DuplicateUserPermission(adminUserPermission)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(AdminUserPermission adminUserPermission)
        {
            var valid = true;

            if (adminUserPermission.AdminUser == null)
            {
                Messages.Add(ValidationMessage.Error("Admin user permission [{0}] requires an admin user association",
                                                     adminUserPermission.Code.FormattedString()));
                valid = false;
            }

            if (string.IsNullOrEmpty(adminUserPermission.Code))
            {
                Messages.Add(ValidationMessage.Error("Admin user permission requires a code"));
                valid = false;
            }
            else if (adminUserPermission.Code.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Admin user Permission Code cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(adminUserPermission.Description) && adminUserPermission.Description.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("User Permission description cannot exceed 200 characters"));
                valid = false;
            }

            if (!adminUserPermission.Grant && !adminUserPermission.Deny && !adminUserPermission.Modify && !adminUserPermission.Remove)
            {
                Messages.Add(
                    ValidationMessage.Error("Admin user permission [{0}] requires at least one set value [Grant | Deny | Modify | Delete]",
                                            adminUserPermission.Code.FormattedString()));
                valid = false;
            }

            return valid;
        }

        public bool DuplicateUserPermission(AdminUserPermission adminUserPermission)
        {
            const string query =
                "SELECT COUNT(*) FROM AdminUserPermission WHERE Code = @Code AND AdminUserId = @UserId AND Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", adminUserPermission.Code},
			                 		{"UserId", adminUserPermission.AdminUser == null ? default(long) : adminUserPermission.AdminUser.Id},
			                 		{"Id", adminUserPermission.Id}
			                 	};
            var duplicateGroupPermission = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateGroupPermission)
                Messages.Add(ValidationMessage.Error("User already has permission [{0}].", adminUserPermission.Code.FormattedString()));
            return duplicateGroupPermission;
        }
    }
}
