﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.Processor.Validation.Administration
{
	public class GroupPermissionValidator : ValidatorBase
	{
		public bool IsValid(GroupPermission groupPermission)
		{
			var valid = true;

			if (!HasAllRequiredData(groupPermission)) valid = false;
			if (DuplicateGroupPermission(groupPermission)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(GroupPermission groupPermission)
		{
			var valid = true;

			if(groupPermission.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Group permission [{0}] requires tenant association",
				                                     groupPermission.Code.FormattedString()));
				valid = false;
			}

			if(groupPermission.Group == null)
			{
				Messages.Add(ValidationMessage.Error("Group permission [{0}] requires a group association",
				                                     groupPermission.Code.FormattedString()));
				valid = false;
			}

			if(string.IsNullOrEmpty(groupPermission.Code))
			{
				Messages.Add(ValidationMessage.Error("Group permission requires a code"));
				valid = false;
			}
			else if (groupPermission.Code.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Group Permission Code cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(groupPermission.Description) && groupPermission.Description.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Group Permission description cannot exceed 200 characters"));
				valid = false;
			}


			if(!groupPermission.Grant && !groupPermission.Deny && !groupPermission.Modify && !groupPermission.Remove)
			{
				Messages.Add(
					ValidationMessage.Error("Group permission [{0}] requires at least one set value [Grant | Deny | Modify | Delete]",
					                        groupPermission.Code.FormattedString()));
				valid = false;
			}

			return valid;
		}

		public bool DuplicateGroupPermission(GroupPermission permission)
		{
			const string query =
				"Select count(*) from GroupPermission where Code = @Code and GroupId = @GroupId and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", permission.Code},
			                 		{"TenantId", permission.TenantId},
			                 		{"GroupId", permission.Group == null ? default(long) : permission.Group.Id},
			                 		{"Id", permission.Id}
			                 	};
			var duplicateGroupPermission = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateGroupPermission)
				Messages.Add(ValidationMessage.Error("Permission [{0}] is already in group", permission.Code.FormattedString()));
			return duplicateGroupPermission;
		}
	}
}
