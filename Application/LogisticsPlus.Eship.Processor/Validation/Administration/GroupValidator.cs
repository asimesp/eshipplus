﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.Processor.Validation.Administration
{
	public class GroupValidator : ValidatorBase
	{
		public bool IsValid(Group group)
		{
			var valid = true;

			if(!HasAllRequiredData(group)) valid = false;
			if(DuplicateGroupName(group)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(Group group)
		{
			var valid = true;

			if(string.IsNullOrEmpty(group.Name))
			{
				Messages.Add(ValidationMessage.Error("Permission group requires a name"));
				valid = false;
			}
			else if (group.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Permission group name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(group.Description) && group.Description.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Permission group description cannot exceed 200 characters"));
				valid = false;
			}

			if(group.Permissions.Count == 0)
			{
				Messages.Add(ValidationMessage.Error("Permission group must have at least one permission"));
				valid = false;
			}

			if (group.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Permission group requires a tenant association"));
				valid = false;
			}

			return valid;
		}
	    public bool CanDeleteGroup(Group group)
	    {
	        const string query = "select dbo.GroupIdUsageCount(@Id)";
	        var parameters = new Dictionary<string, object> { { "Id", group.Id } };
	        var used = ExecuteScalar(query, parameters).ToInt() > 0;
	        if (used) Messages.Add(ValidationMessage.Error("Group is referenced one or more times and cannot be deleted."));
	        return !used;
	    }

        public bool DuplicateGroupName(Group group)
		{
			const string query = "Select count(*) from [Group] where [Name] = @Name and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Name", group.Name},
			                 		{"TenantId", group.TenantId},
			                 		{"Id", group.Id}
			                 	};
			var duplicateGroupName = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateGroupName) Messages.Add(ValidationMessage.Error("Permission group name is already in use"));
			return duplicateGroupName;
		}
	}
}
