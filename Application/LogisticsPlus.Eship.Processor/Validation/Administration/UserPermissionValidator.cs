﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.Processor.Validation.Administration
{
	public class UserPermissionValidator : ValidatorBase
	{
		public bool IsValid(UserPermission userPermission)
		{
			var valid = true;

			if (!HasAllRequiredData(userPermission)) valid = false;
			if (DuplicateUserPermission(userPermission)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(UserPermission userPermission)
		{
			var valid = true;

			if (userPermission.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("User permission [{0}] requires tenant association",
													 userPermission.Code.FormattedString()));
				valid = false;
			}

			if (userPermission.User == null)
			{
				Messages.Add(ValidationMessage.Error("User permission [{0}] requires a user association",
													 userPermission.Code.FormattedString()));
				valid = false;
			}

			if (string.IsNullOrEmpty(userPermission.Code))
			{
				Messages.Add(ValidationMessage.Error("User permission requires a code"));
				valid = false;
			}
			else if (userPermission.Code.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("User Permission Code cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(userPermission.Description) && userPermission.Description.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("User Permission description cannot exceed 200 characters"));
				valid = false;
			}

			if (!userPermission.Grant && !userPermission.Deny && !userPermission.Modify && !userPermission.Remove)
			{
				Messages.Add(
					ValidationMessage.Error("User permission [{0}] requires at least one set value [Grant | Deny | Modify | Delete]",
											userPermission.Code.FormattedString()));
				valid = false;
			}

			return valid;
		}

		public bool DuplicateUserPermission(UserPermission permission)
		{
			const string query =
				"Select count(*) from UserPermission where Code = @Code and UserId = @UserId and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", permission.Code},
			                 		{"TenantId", permission.TenantId},
			                 		{"UserId", permission.User == null ? default(long) : permission.User.Id},
			                 		{"Id", permission.Id}
			                 	};
			var duplicateGroupPermission = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateGroupPermission)
				Messages.Add(ValidationMessage.Error("Permission [{0}] is already in user", permission.Code.FormattedString()));
			return duplicateGroupPermission;
		}
	}
}
