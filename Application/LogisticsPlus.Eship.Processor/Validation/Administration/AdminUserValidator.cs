﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.Processor.Validation.Administration
{
    public class AdminUserValidator : ValidatorBase
    {
        public bool IsValid(AdminUser adminUser)
        {
	        bool valid = UsernameIsUnique(adminUser);

	        if (!HasAllRequiredData(adminUser)) valid = false;
            return valid;
        }

        private bool HasAllRequiredData(AdminUser adminUser)
        {
            var valid = true;

            if (string.IsNullOrEmpty(adminUser.Username))
            {
                Messages.Add(ValidationMessage.Error("User requires a login name"));
                valid = false;
            }
            else if (adminUser.Username.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("User login name cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(adminUser.Email))
            {
                Messages.Add(ValidationMessage.Error("User requires an email address"));
                valid = false;
            }
            else if (adminUser.Email.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("User email address cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(adminUser.Email) && !adminUser.Email.EmailIsValid())
            {
                Messages.Add(ValidationMessage.Error("User email address is invalid"));
                valid = false;
            }

            if (string.IsNullOrEmpty(adminUser.Password))
            {
                Messages.Add(ValidationMessage.Error("User requires a password"));
                valid = false;
            }
            else if (adminUser.Password.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("User password cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(adminUser.FirstName))
            {
                Messages.Add(ValidationMessage.Error("User requires a first name"));
                valid = false;
            }
            else if (adminUser.FirstName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("User first name cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(adminUser.LastName))
            {
                Messages.Add(ValidationMessage.Error("User requires a last name"));
                valid = false;
            }
            else if (adminUser.LastName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("User last name cannot exceed 50 characters"));
                valid = false;
            }

            return valid;
        }

        public bool UsernameIsUnique(AdminUser adminUser)
        {
            const string query = "SELECT COUNT(*) from AdminUser WHERE Username = @Username AND Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Username", adminUser.Username},
			                 		{"Id", adminUser.Id}
			                 	};
            var duplicateGroupName = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateGroupName) Messages.Add(ValidationMessage.Error("Username is already in use"));
            return !duplicateGroupName;
        }

		public bool CanDeleteUser(AdminUser adminUser)
		{
			const string query = "SELECT dbo.AdminUserIdUsageCount(@Id)";
            var parameters = new Dictionary<string, object> { { "Id", adminUser.Id } };
            var used = ExecuteScalar(query, parameters).ToInt() > 0;
            if (used) Messages.Add(ValidationMessage.Error("Admin user is referenced one or more times and cannot be deleted."));
            return !used;
		}
    }
}
