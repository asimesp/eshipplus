﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class ClaimValidator : ValidatorBase
    {
        public bool ClaimVendorExists(ClaimVendor claimVendor)
        {
            const string query =
                "Select count(*) from ClaimVendor where VendorId = @VendorId and ClaimId = @ClaimId and TenantId = @TenantId";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"VendorId", claimVendor.VendorId},
			                 		{"TenantId", claimVendor.TenantId},
			                 		{"ClaimId", claimVendor.ClaimId}
			                 	};
            var duplicateService = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateService)
                Messages.Add(ValidationMessage.Error("Vendor is already included in Claim vendors"));
            return duplicateService;
        }

		public bool ClaimShipmentExists(ClaimShipment claimShipment)
		{
			const string query =
				"Select count(*) from ClaimShipment where ShipmentId = @ShipmentId and ClaimId = @ClaimId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"ShipmentId", claimShipment.ShipmentId},
			                 		{"TenantId", claimShipment.TenantId},
			                 		{"ClaimId", claimShipment.ClaimId}
			                 	};
			var duplicateService = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateService)
				Messages.Add(ValidationMessage.Error("Shipment is already included in Claim shipments"));
			return duplicateService;
		}
		
		public bool ClaimServiceTicketExists(ClaimServiceTicket claimServiceTicket)
		{
			const string query =
				"Select count(*) from ClaimServiceTicket where ServiceTicketId = @ServiceTicketId and ClaimId = @ClaimId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"ServiceTicketId", claimServiceTicket.ServiceTicketId},
			                 		{"TenantId", claimServiceTicket.TenantId},
			                 		{"ClaimId", claimServiceTicket.ClaimId}
			                 	};
			var duplicateService = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateService)
				Messages.Add(ValidationMessage.Error("Shipment is already included in Claim shipments"));
			return duplicateService;
		}

        
        public bool IsValid(Claim claim)
        {
            var valid = true;

            if (!HasAllRequiredData(claim)) valid = false;
            if (DuplicateClaimNumber(claim)) valid = false;

            return valid;
        }


        public bool DuplicateClaimNumber(Claim claim)
        {
			const string query = "SELECT COUNT(*) FROM Claim WHERE ClaimNumber = @ClaimNumber AND TenantId = @TenantId AND Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"ClaimNumber", claim.ClaimNumber},
			                 		{"TenantId", claim.TenantId},
			                 		{"Id", claim.Id}
			                 	};
            var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateNumber) Messages.Add(ValidationMessage.Error("Claim number is already in use"));
            return duplicateNumber;
        }

		public bool HasAllRequiredData(Claim claim)
        {
            var valid = true;

            if (claim.TenantId == default(long))
            {
				Messages.Add(ValidationMessage.Error("Claim requires a tenant association"));
                valid = false;
            }

            if (claim.Customer == null)
            {
				Messages.Add(ValidationMessage.Error("Claim requires a customer association"));
                valid = false;
            }
            
            if (claim.User == null)
            {
				Messages.Add(ValidationMessage.Error("Claim requires a user association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(claim.ClaimNumber))
            {
				Messages.Add(ValidationMessage.Error("Claim requires a Claim number"));
                valid = false;
            }
			else if (claim.ClaimNumber.Length > 50)
            {
				Messages.Add(ValidationMessage.Error("Claim number cannot exceed 50 characters"));
                valid = false;
            }

            if (claim.EstimatedRepairCost < 0)
            {
                Messages.Add(ValidationMessage.Error("Estimated repair cost must be greater than or equal to zero"));
                valid = false;
            }

			if(!string.IsNullOrEmpty(claim.ClaimDetail) && claim.ClaimDetail.Length > 1900)
			{
				Messages.Add(ValidationMessage.Error("Claim detail cannot exceed 1900 characters"));
				valid = false;
			}

            if (!string.IsNullOrEmpty(claim.ClaimantReferenceNumber) && claim.ClaimantReferenceNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Claimant reference number cannot exceed 50 characters"));
                valid = false;
            }

            if (claim.AmountClaimed < 0)
            {
                Messages.Add(ValidationMessage.Error("Amount claimed must be greater than or equal to zero"));
                valid = false;
            }

			if (!claim.Status.ToInt().EnumValueIsValid<ClaimStatus>())
            {
				Messages.Add(ValidationMessage.Error("Claim must have a valid status"));
                valid = false;
            }

            if (!claim.ClaimType.ToInt().EnumValueIsValid<ClaimType>())
            {
                Messages.Add(ValidationMessage.Error("Claim must have a valid type"));
                valid = false;
            }

            if (!claim.AmountClaimedType.ToInt().EnumValueIsValid<AmountClaimedType>())
            {
                Messages.Add(ValidationMessage.Error("Claim must have a valid amount claimed type"));
                valid = false;
            }

            if (!claim.DateCreated.IsValidSystemDateTime())
            {
				Messages.Add(ValidationMessage.Error("Claim date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!claim.Vendors.Any())
            {
				Messages.Add(ValidationMessage.Error("Claim must have at least one vendor"));
                valid = false;
            }
            else
            {
                if (!ClaimVendorsAreValid(claim.Vendors)) valid = false;
            }

            if (claim.CheckAmount < 0)
            {
                Messages.Add(ValidationMessage.Error("Check amount must be greater than or equal to zero"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(claim.CheckNumber) && claim.CheckNumber.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Check Number number cannot exceed 100 characters"));
                valid = false;
            }

            if (!claim.DateLpAcctToPayCarrier.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("'Date LP To Pay Carrier' must be between {0} and {1}",
                    DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!claim.Acknowledged.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("'Acknowledged Date' must be between {0} and {1}",
                    DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (claim.Notes.Count > 0 && !ClaimNotesAreValid(claim.Notes)) valid = false;
            if (claim.Documents.Count > 0 && !ClaimDocumentsAreValid(claim.Documents)) valid = false;
            if (claim.ServiceTickets.Count > 0 && !ClaimServiceTicketsAreValid(claim.ServiceTickets)) valid = false;
            if (claim.Shipments.Count > 0 && !ClaimShipmentsAreValid(claim.Shipments)) valid = false;

            return valid;
        }


		private bool ClaimVendorsAreValid(IEnumerable<ClaimVendor> vendors)
        {
            var valid = true;

            foreach (var vendor in vendors)
            {
                if (vendor.TenantId == default(long))
                {
					Messages.Add(ValidationMessage.Error("Claim vendor requires a tenant association"));
                    valid = false;
                }

                if (vendor.Claim == null)
                {
					Messages.Add(ValidationMessage.Error("Claim vendor requires a claim association"));
                    valid = false;
                }

                if (vendor.VendorId == default(long))
                {
					Messages.Add(ValidationMessage.Error("Claim vendor requires a vendor association"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(vendor.ProNumber) && vendor.ProNumber.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Vendor Pro Number cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(vendor.FreightBillNumber) && vendor.FreightBillNumber.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Vendor Freight Bill Number cannot exceed 50 characters"));
                    valid = false;
                }
            }

            return valid;
        }

		private bool ClaimServiceTicketsAreValid(IEnumerable<ClaimServiceTicket> serviceTickets)
		{
			var valid = true;

			foreach (var ticket in serviceTickets)
			{
				if (ticket.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Claim service ticket requires a tenant association"));
					valid = false;
				}

				if (ticket.Claim == null)
				{
					Messages.Add(ValidationMessage.Error("Claim service ticket requires a claim association"));
					valid = false;
				}

				if (ticket.ServiceTicketId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Claim service ticket requires a service ticket association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool ClaimShipmentsAreValid(IEnumerable<ClaimShipment> shipments)
		{
			var valid = true;

			foreach (var shipment in shipments)
			{
				if (shipment.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Claim shipment requires a tenant association"));
					valid = false;
				}

				if (shipment.Claim == null)
				{
					Messages.Add(ValidationMessage.Error("Claim shipment requires a claim association"));
					valid = false;
				}

				if (shipment.ShipmentId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Claim shipment requires a shipment association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool ClaimNotesAreValid(IEnumerable<ClaimNote> notes)
        {
            var valid = true;

            foreach (var note in notes)
            {
                if (note.TenantId == default(long))
                {
					Messages.Add(ValidationMessage.Error("Claim note requires a tenant association"));
                    valid = false;
                }

				if (note.Claim == null)
                {
					Messages.Add(ValidationMessage.Error("Claim note requires a claim association"));
                    valid = false;
                }

                if (note.User == null)
                {
					Messages.Add(ValidationMessage.Error("Claim note requires a user association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(note.Message))
                {
					Messages.Add(ValidationMessage.Error("Claim note requires a message"));
                    valid = false;
                }
                else if (note.Message.Length > 1000)
                {
					Messages.Add(ValidationMessage.Error("Claim note message cannot exceed 1000 characters"));
                    valid = false;
                }
                if (note.SendReminder && note.SendReminderEmails == string.Empty)
                {
                    Messages.Add(ValidationMessage.Error("Claim note reminder requires emails"));
                    valid = false;
                }
                if (!string.IsNullOrEmpty(note.SendReminderEmails) && note.SendReminderEmails.Length > 1500)
                {
                    Messages.Add(ValidationMessage.Error("Claim note reminder emails cannot exceed 1500 characters"));
                    valid = false;
                }
                if (note.SendReminder && !string.IsNullOrEmpty(note.SendReminderEmails) && note.SendReminderEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
                {
                    Messages.Add(ValidationMessage.Error("Claim note reminder emails contains one or more invalid addresses"));
                    valid = false;
                }
                if (!note.Type.ToInt().EnumValueIsValid<NoteType>())
                {
					Messages.Add(ValidationMessage.Error("Claim note must have a valid type"));
                    valid = false;
                }
                if (!note.SendReminderOn.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Claim Note Reminder Date must be between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }
                if (!note.DateCreated.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Claim Note Date Created must be between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }
            }

            return valid;
        }

		private bool ClaimDocumentsAreValid(IEnumerable<ClaimDocument> documents)
		{
			var valid = true;

			foreach (var document in documents)
			{
				if (document.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Claim document requires a tenant association"));
					valid = false;
				}

				if (document.Claim == null)
				{
					Messages.Add(ValidationMessage.Error("Claim document requires a claim association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Name))
				{
					Messages.Add(ValidationMessage.Error("Claim document requires a name"));
					valid = false;
				}
				else if (document.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Claim document name cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Description))
				{
					Messages.Add(ValidationMessage.Error("Claim document requires a description"));
					valid = false;
				}
				else if (document.Description.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Claim document description cannot exceed 500 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.LocationPath))
				{
					Messages.Add(ValidationMessage.Error("Claim document requires a location path"));
					valid = false;
				}
				else if (document.LocationPath.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Claim document location path cannot exceed 200 characters"));
					valid = false;
				}
			}
			return valid;
		}
    }
}
