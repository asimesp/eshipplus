﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class AssetValidator : ValidatorBase
    {
    	public bool CanDeleteAsset(Asset asset)
    	{
    		const string query = "select dbo.AssetIdUsageCount(@Id)";
    		var parameters = new Dictionary<string, object> { { "Id", asset.Id } };
    		var used = ExecuteScalar(query, parameters).ToInt() > 0;
    		if (used) Messages.Add(ValidationMessage.Error("Asset is referenced one or more times and cannot be deleted."));
    		return !used;
    	}

    	public bool IsValid(Asset asset)
        {
            Messages.Clear();

            var valid = true;

            if (!HasAllRequiredData(asset)) valid = false;
			if (DuplicateAssetNumber(asset)) valid = false;

            return valid;
        }

    	public bool HasAllRequiredData(Asset asset)
        {
            var valid = true;

            if (asset.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Assets require a tenant association"));
                valid = false;
            }

			if(!asset.AssetType.ToInt().EnumValueIsValid<AssetType>())
			{
				Messages.Add(ValidationMessage.Error("Asset type is invalid"));
				valid = false;
			}

			if (!asset.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Asset date created must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if(string.IsNullOrEmpty(asset.AssetNumber))
			{
				Messages.Add(ValidationMessage.Error("Asset requires an asset number"));
				valid = false;
			}
			else if (asset.AssetNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Asset number cannot exceed 50 characters"));
				valid = false;
			}

        	if (!string.IsNullOrEmpty(asset.Description) && asset.Description.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Asset Description cannot exceed 100 characters"));
				valid = false;
			}

        	return valid;
        }

		public bool DuplicateAssetNumber(Asset asset)
		{
			const string query = "Select count(*) from Asset where AssetNumber = @AssetNumber and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"AssetNumber", asset.AssetNumber},
			                 		{"TenantId", asset.TenantId},
			                 		{"Id", asset.Id}
			                 	};
			var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateNumber) Messages.Add(ValidationMessage.Error("Asset number is already in use"));
			return duplicateNumber;
		}
    }
}
