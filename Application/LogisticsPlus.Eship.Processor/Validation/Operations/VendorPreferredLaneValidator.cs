﻿using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class VendorPreferredLaneValidator : ValidatorBase
    {
        public bool IsValid(VendorPreferredLane vendorPreferredLane)
        {
            var valid = true;

            if (!HasAllRequiredData(vendorPreferredLane)) valid = false;

            return valid;
        }

        private bool HasAllRequiredData(VendorPreferredLane vendorPreferredLane)
        {

            var valid = true;
           
            if (vendorPreferredLane.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor preferred lane requires a tenant association"));
                valid = false;
            }

            if (vendorPreferredLane.VendorId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor preferred lane requires a vendor association"));
                valid = false;
            }

            if(vendorPreferredLane.DestinationCountryId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor preferred lane requires a destination country association"));
                valid = false;
            }

            if (vendorPreferredLane.OriginCountryId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor preferred lane requires an origin country association"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(vendorPreferredLane.OriginCity) && vendorPreferredLane.OriginCity.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor preferred lane origin city cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(vendorPreferredLane.DestinationCity) && vendorPreferredLane.DestinationCity.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor preferred lane destination city cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(vendorPreferredLane.OriginState) && vendorPreferredLane.OriginState.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor preferred lane origin state cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(vendorPreferredLane.DestinationState) && vendorPreferredLane.DestinationState.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor preferred lane destination state cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(vendorPreferredLane.OriginPostalCode) && vendorPreferredLane.OriginPostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Vendor preferred lane origin postal code cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(vendorPreferredLane.DestinationPostalCode) && vendorPreferredLane.DestinationPostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Vendor preferred lane origin postal code cannot exceed 10 characters"));
                valid = false;
            }
            return valid;
        }
    }
}
