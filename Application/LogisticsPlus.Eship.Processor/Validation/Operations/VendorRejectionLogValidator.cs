﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
	public class VendorRejectionLogValidator : ValidatorBase
	{
		public bool IsValid(VendorRejectionLog log)
		{
			var valid = true;

			if (!HasAllRequiredData(log)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(VendorRejectionLog log)
		{
			var valid = true;

			if (!string.IsNullOrEmpty(log.ShipmentNumber) && log.ShipmentNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log load/shipment number cannot exceed 50 characters"));
				valid = false;
			}

			if (!log.ShipmentDateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log load/shipment date created must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime,
													 DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!log.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log date created must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime,
													 DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (log.VendorId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log requires a vendor association"));
				valid = false;
			}

			if (log.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log requires a tenant association"));
				valid = false;
			}

			if (log.UserId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log requires a user association"));
				valid = false;
			}

			if(!string.IsNullOrEmpty(log.OriginCity) && log.OriginCity.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log origin city cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(log.OriginState) && log.OriginState.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log origin state cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(log.OriginPostalCode) && log.OriginPostalCode.Length > 10)
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log origin postalc code cannot exceed 10 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(log.OriginCountryCode) && log.OriginCountryCode.Length > 2)
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log origin city cannot exceed 2 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(log.DestinationCity) && log.DestinationCity.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log destination city cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(log.DestinationState) && log.DestinationState.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log destination state cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(log.DestinationPostalCode) && log.DestinationPostalCode.Length > 10)
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log destination postalc code cannot exceed 10 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(log.DestinationCountryCode) && log.DestinationCountryCode.Length > 2)
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log destination city cannot exceed 2 characters"));
				valid = false;
			}

			if (!log.ServiceMode.ToInt().EnumValueIsValid<ServiceMode>())
			{
				Messages.Add(ValidationMessage.Error("Vendor rejection log must have a valid service mode"));
				valid = false;
			}

			return valid;
		}
	}
}
