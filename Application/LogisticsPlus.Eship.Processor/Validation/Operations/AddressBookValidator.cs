﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
	public class AddressBookValidator : ValidatorBase
	{
		public bool AddressBookServiceExists(AddressBookService addressBookService)
		{
			const string query =
				@"SELECT COUNT(*) FROM AddressBookService WHERE ServiceId = @ServiceId AND TenantId = @TenantId AND AddressBookId = @AddressBookId";

			var parameters = new Dictionary<string, object>
    		                 	{
    		                 		{"ServiceId", addressBookService.ServiceId},
    		                 		{"TenantId", addressBookService.TenantId},
    		                 		{"AddressBookId", addressBookService.AddressBookId}
    		                 	};

			var duplicateMapping = ExecuteScalar(query, parameters).ToInt() > 0;

			if (!duplicateMapping) Messages.Add(ValidationMessage.Error("Address book service already Exists"));
			return duplicateMapping;
		}

		public bool IsValid(AddressBook addressBook)
		{
			var valid = true;

			if (!HasAllRequiredData(addressBook)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(AddressBook addressBook)
		{
			var valid = true;

			if (addressBook.TenantId == default(long))
			{
                Messages.Add(ValidationMessage.Error("Address book requires a tenant association"));
				valid = false;
			}

			if (addressBook.CountryId == default(long))
			{
                Messages.Add(ValidationMessage.Error("Address book requires a country association"));
				valid = false;
			}

			if (addressBook.Customer == null)
			{
                Messages.Add(ValidationMessage.Error("Address book requires a customer association"));
				valid = false;
			}

            if (string.IsNullOrEmpty(addressBook.Description))
            {
                Messages.Add(ValidationMessage.Error("Address book requires a description"));
                valid = false;
            }
            else if (addressBook.Description.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Address book description cannot exceed 50 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(addressBook.OriginSpecialInstruction) && addressBook.OriginSpecialInstruction.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Address book Origin Special Instruction cannot exceed 500 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(addressBook.DestinationSpecialInstruction) && addressBook.DestinationSpecialInstruction.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Address book Destination Special Instruction cannot exceed 500 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(addressBook.GeneralInfo) && addressBook.GeneralInfo.Length > 2000)
			{
				Messages.Add(ValidationMessage.Error("Address book General Info cannot exceed 2000 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(addressBook.Direction) && addressBook.Direction.Length > 2000)
			{
				Messages.Add(ValidationMessage.Error("Address book direction cannot exceed 2000 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(addressBook.Street1) && addressBook.Street1.Length > 50)
			{
                Messages.Add(ValidationMessage.Error("Address book street1 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(addressBook.Street2) && addressBook.Street2.Length > 50)
			{
                Messages.Add(ValidationMessage.Error("Address book street2 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(addressBook.City) && addressBook.City.Length > 50)
			{
                Messages.Add(ValidationMessage.Error("Address book city cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(addressBook.State) && addressBook.State.Length > 50)
			{
                Messages.Add(ValidationMessage.Error("Address book state cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(addressBook.PostalCode) && addressBook.PostalCode.Length > 10)
			{
                Messages.Add(ValidationMessage.Error("Address book postal Code cannot exceed 10 characters"));
				valid = false;
			}

			if (addressBook.Contacts.Count > 0)
			{
				if (!HasValidLocationContactInformation(addressBook.Contacts)) valid = false;
				if (addressBook.Contacts.Count(c => c.Primary) != 1)
				{
                    Messages.Add(ValidationMessage.Error("Address book should have one (and only one) primary contact"));
					valid = false;
				}
			}

			return valid;
		}

		private bool HasValidLocationContactInformation(IEnumerable<AddressBookContact> contacts)
		{
			var valid = true;

			foreach (var contact in contacts)
			{
				if (contact.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Address book contact requires a tenant association"));
					valid = false;
				}

				if (contact.AddressBook == null)
				{
					Messages.Add(ValidationMessage.Error("Address book contact requires a Address book location association"));
					valid = false;
				}

				if (contact.ContactTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Address book contact requires a contact type association"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Name) && contact.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Address book contact name cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Phone) && contact.Phone.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Address book contact phone cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Mobile) && contact.Mobile.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Address book contact mobile cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Fax) && contact.Fax.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Address book contact fax cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Email) && contact.Email.Length > 100)
				{
					Messages.Add(ValidationMessage.Error("Address book contact email cannot exceed 100 characters"));
					valid = false;
				}

			    if (!string.IsNullOrEmpty(contact.Email) && !contact.Email.EmailIsValid())
			    {
			        Messages.Add(ValidationMessage.Error("Address book contact email is invalid"));
			        valid = false;
			    }

                if (!string.IsNullOrEmpty(contact.Comment) && contact.Comment.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Address book contact comment cannot exceed 200 characters"));
					valid = false;
				}
			}

			return valid;
		}
	}
}
