﻿using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
	public class TruckloadBidValidator : ValidatorBase
	{
		public bool BidLoadOrderIsUnique(TruckloadBid bid)
		{
			var dbBid = new TruckloadBidSearch().FetchTruckloadBidByLoadOrderIdAndStatus(bid.LoadOrderId,
			                                                                             TruckloadBidStatus.Processing.ToInt(),
			                                                                             bid.TenantId);
			var isUnique = dbBid == null || dbBid.Id == bid.Id;
			if (!isUnique)
				Messages.Add(ValidationMessage.Error("Bid for this load order already exists."));
			return isUnique;
		}
	
		public bool IsValid(TruckloadBid bid)
		{
			var valid = HasAllRequiredData(bid);
			if (!BidLoadOrderIsUnique(bid)) valid = false;

			return valid;
		}


		public bool HasAllRequiredData(TruckloadBid bid)
		{
			var valid = true;

			if (bid.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires a tenant association"));
				valid = false;
			}
			
			if (bid.LoadOrderId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires a loadorder association"));
				valid = false;
			}
			else
			{
				if (bid.LoadOrder.Status != LoadOrderStatus.Offered)
				{
					Messages.Add(ValidationMessage.Error("Load Order status should be {0} for tendering the load.",LoadOrderStatus.Offered));
					valid = false;
				}

				if (bid.LoadOrder.ServiceMode != ServiceMode.Truckload)
				{
					Messages.Add(ValidationMessage.Error("Service mode for the load order should be {0} for tendering the load.",ServiceMode.Truckload));
					valid = false;
				}
				
			}

			if (bid.TruckloadTenderingProfile == null)
			{
				Messages.Add(ValidationMessage.Error("Customer on load order should have a customer truckload tendering profile."));
				valid = false;
			}
			else
			{
				var matchingLane = bid.TruckloadTenderingProfile.RetrieveMatchingLane(bid.LoadOrder);
				if (matchingLane == null)
				{
					Messages.Add(ValidationMessage.Error("There is no matching lane for this LoadOrder in the Customer Profile."));
					valid = false;
				}
				else
				{
					if (bid.TLLaneId == default(long))
					{
						Messages.Add(ValidationMessage.Error("Tendering bid requires a lane association"));
						valid = false;
					}
					if (bid.FirstPreferredVendorId == default(long))
					{
						Messages.Add(ValidationMessage.Error("Tendering required first preferred vendor."));
						valid = false;
					}
					else
					{
						if (!bid.TimeLoadTendered1.IsValidSystemDateTime())
						{
							Messages.Add(ValidationMessage.Error("Tendering bid requires time when the load was tenedered to 1st vendor"));
							valid = false;
						}

						if (!bid.ExpirationTime1.IsValidSystemDateTime())
						{
							Messages.Add(ValidationMessage.Error("Tendering bid requires time when the load tenedered to 1st vendor expires"));
							valid = false;
						}
					}

					var loadOrderVendor = bid.LoadOrder.Vendors.FirstOrDefault(v => v.Primary);

					if (loadOrderVendor == null || (bid.IsNew && loadOrderVendor.VendorId != matchingLane.FirstPreferredVendorId))
					{
						Messages.Add(
							ValidationMessage.Error(
								"Primary vendor on the Load Order should be same as first preferred vendor on the matching lane for the customer truckload tendering profile for the customer on the load order."));
						valid = false;
					}
				}

				if (bid.LoadOrder.Customer.Id != bid.TruckloadTenderingProfile.CustomerId)
				{
					Messages.Add(ValidationMessage.Error("Customer on truckload bid should same as customer for the load order"));
					valid = false;
				}
				
			}


			

			if (bid.SecondPreferredVendorId != default(long) && bid.FirstPreferredVendorId == default(long))
			{
				Messages.Add(ValidationMessage.Error("First Preferred Vendor should be added to the bid before Second Preferred Vendor is added."));
				valid = false;
			}
			else
			{
				if (!bid.TimeLoadTendered2.IsValidSystemDateTime())
				{
					Messages.Add(ValidationMessage.Error("Tendering bid requires time when the load was tendered to 2nd vendor"));
					valid = false;
				}

				if (!bid.ExpirationTime2.IsValidSystemDateTime())
				{
					Messages.Add(ValidationMessage.Error("Tendering bid requires time when the load tenedered to 2nd vendor expires"));
					valid = false;
				}
			}

			if (bid.ThirdPreferredVendorId != default(long) && (bid.FirstPreferredVendorId == default(long) || bid.SecondPreferredVendorId == default(long)))
			{
				Messages.Add(ValidationMessage.Error("Second and Third Preferred Vendor should be added to the bid before Third Preferred Vendor is added."));
				valid = false;
			}
			else
			{
				if (!bid.TimeLoadTendered2.IsValidSystemDateTime())
				{
					Messages.Add(ValidationMessage.Error("Tendering bid requires time when the load was tenedered to 3rd vendor"));
					valid = false;
				}

				if (!bid.ExpirationTime2.IsValidSystemDateTime())
				{
					Messages.Add(ValidationMessage.Error("Tendering bid requires time when the load tenedered to 3rd vendor expires"));
					valid = false;
				}
			}
			
			if (bid.UserId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires a current user association."));
				valid = false;
			}

			
			

			return valid;
		}
	}
}
