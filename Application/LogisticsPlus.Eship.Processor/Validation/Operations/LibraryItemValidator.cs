﻿using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class LibraryItemValidator : ValidatorBase
    {
        public bool IsValid(LibraryItem libraryItem)
        {
            var valid = true;

            if (!HasAllRequiredData(libraryItem)) valid = false;

            return valid;
        }

        private bool HasAllRequiredData(LibraryItem libraryItem)
        {
            var valid = true;

            if (libraryItem.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Library Item requires a Tenant association"));
                valid = false;
            }

            if (libraryItem.Customer == null)
            {
                Messages.Add(ValidationMessage.Error("Library Item requires a Customer association"));
                valid = false;
            }

            if(string.IsNullOrEmpty(libraryItem.Description))
            {
                Messages.Add(ValidationMessage.Error("Library Item Description is Required"));
                valid = false;
            }
            else if (libraryItem.Description.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Library Item Description cannot exceed 100 characters"));
                valid = false;
            }

            if (libraryItem.PieceCount < 0)
            {
                Messages.Add(ValidationMessage.Error("Library Item Piece Count must be greater than or equal to zero"));
                valid = false;
            }          

            if (libraryItem.Quantity < 0)
            {
				Messages.Add(ValidationMessage.Error("Library Item Quantity must be greater than or equal to zero"));
                valid = false;
            }

            if (libraryItem.Weight < 0)
            {
				Messages.Add(ValidationMessage.Error("Library Item Weight must be greater than or equal to zero"));
                valid = false;
            }

            if (libraryItem.Length < 0)
            {
				Messages.Add(ValidationMessage.Error("Library Item Length must be greater than or equal to zero"));
                valid = false;
            }

            if (libraryItem.Width < 0)
            {
				Messages.Add(ValidationMessage.Error("Library Item Width must be greater than or equal to zero"));
                valid = false;
            }

            if (libraryItem.Height < 0)
            {
				Messages.Add(ValidationMessage.Error("Library Item Height must be greater than or equal to zero"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(libraryItem.NMFCCode) && libraryItem.NMFCCode.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Library Item NMFC Code cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(libraryItem.HTSCode) && libraryItem.HTSCode.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Library Item HTS Code cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(libraryItem.Comment) && libraryItem.Comment.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Library Item Comment cannot exceed 50 characters"));
                valid = false;
            }

            return valid;
        }
    }
}
