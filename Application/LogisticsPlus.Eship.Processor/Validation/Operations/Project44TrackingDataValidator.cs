﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class Project44TrackingDataValidator : ValidatorBase
    {
        public bool IsValid(Project44TrackingData track)
        {
	        bool valid = HasAllRequiredData(track);

	        return valid;
        }

        private bool HasAllRequiredData(Project44TrackingData track)
        {
            var valid = true;

            if (string.IsNullOrEmpty(track.Project44Id))
            {
                Messages.Add(ValidationMessage.Error("Project 44 Tracking Data requires a Project 44 Id"));
                valid = false;
            }
            else if (track.Project44Id.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Project 44 Id cannot exceed 100 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(track.ResponseData))
            {
                Messages.Add(ValidationMessage.Error("Project 44 Tracking Data requires Response Data"));
                valid = false;
            }

            if (!track.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Project 44 Tracking Data date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }
                
            return valid;
        }
    }
}
