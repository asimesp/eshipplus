﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class VendorTerminalValidator : ValidatorBase
    {
        public bool IsValid(VendorTerminal vendorTerminal)
        {
            var valid = true;

            if (!HasAllRequiredData(vendorTerminal)) valid = false;

            return valid;
        }

        private bool HasAllRequiredData(VendorTerminal terminal)
        {
            var valid = true;

            if (string.IsNullOrEmpty(terminal.Name))
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal requires a name"));
                valid = false;
            }
            else if (terminal.Name.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal name cannot exceed 50 characters"));
                valid = false;
            }

            if (!terminal.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal must have a valid creation date between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.Street1) && terminal.Street1.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal street1 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.Street2) && terminal.Street2.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal street2 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.City) && terminal.City.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal city cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.State) && terminal.State.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal state cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.PostalCode) && terminal.PostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal postal code cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.Phone) && terminal.Phone.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal phone cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.Fax) && terminal.Fax.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal fax cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.Mobile) && terminal.Mobile.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal mobile cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.Email) && terminal.Email.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal email cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.Email) && !terminal.Email.EmailIsValid())
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal email is invalid"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.ContactName) && terminal.ContactName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal contact Name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(terminal.Comment) && terminal.Comment.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal comment cannot exceed 50 characters"));
                valid = false;
            }

            if (terminal.CountryId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal requires a country association"));
                valid = false;
            }

            if (terminal.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal requires a tenant association"));
                valid = false;
            }

            if (terminal.Vendor == null)
            {
                Messages.Add(ValidationMessage.Error("Vendor terminal requires a vendor association"));
                valid = false;
            }

            return valid;
        }
    }
}
