﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Calculations;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
	public class RateAndScheduleValidator : ValidatorBase
	{
		public bool OfferedLoadOrderIsValid(LoadOrder loadOrder)
		{
			bool valid = HasAllRequiredDataForOfferedLoad(loadOrder);

			if (DuplicateLoadNumber(loadOrder)) valid = false;

			return valid;
		}

        private bool HasAllRequiredDataForOfferedLoad(LoadOrder loadOrder)
		{
			var valid = true;

			if (loadOrder.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Load order requires a organizational association"));
				valid = false;
			}

			if (loadOrder.Customer == null)
			{
                Messages.Add(ValidationMessage.Error("Load order requires a customer association"));
				valid = false;
			}

			if (loadOrder.User == null)
			{
                Messages.Add(ValidationMessage.Error("Load order requires a user association"));
				valid = false;
			}

			if (loadOrder.Origin == null)
			{
                Messages.Add(ValidationMessage.Error("Load order requires an origin location"));
				valid = false;
			}
            else if (!HasValidLocationInformation(new[] { loadOrder.Origin }, true))
			{
                Messages.Add(ValidationMessage.Error("Load order origin location has some invalid data"));
				valid = false;
			}

			if (loadOrder.Destination == null)
			{
                Messages.Add(ValidationMessage.Error("Load order requires an destination location"));
				valid = false;
			}
			else if (!HasValidLocationInformation(new[] { loadOrder.Destination }, true))
			{
                Messages.Add(ValidationMessage.Error("Load order destination location has some invalid data"));
				valid = false;
			}

			if (string.IsNullOrEmpty(loadOrder.LoadOrderNumber))
			{
                Messages.Add(ValidationMessage.Error("Load order requires a load order number"));
				valid = false;
			}
            else if (loadOrder.LoadOrderNumber.Length > 50)
			{
                Messages.Add(ValidationMessage.Error("Load order number cannot exceed 50 characters"));
				valid = false;
			}

			if (!loadOrder.ServiceMode.ToInt().EnumValueIsValid<ServiceMode>())
			{
                Messages.Add(ValidationMessage.Error("Load order must have a valid service mode"));
				valid = false;
			}

			if (!loadOrder.DateCreated.IsValidSystemDateTime())
			{
                Messages.Add(ValidationMessage.Error("Load order date created date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!loadOrder.DesiredPickupDate.IsValidSystemDateTime())
			{
                Messages.Add(ValidationMessage.Error("Load order desired pickup date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!loadOrder.EarlyPickup.IsValidTimeString())
			{
                Messages.Add(ValidationMessage.Error("Load order early pickup time is invalid"));
				valid = false;
			}

			if (!loadOrder.LatePickup.IsValidTimeString())
			{
                Messages.Add(ValidationMessage.Error("Load order late pickup time is invalid"));
				valid = false;
			}

			var earlyPickup = DateTime.Now.SetTime(loadOrder.EarlyPickup);
			var latePickup = DateTime.Now.SetTime(loadOrder.LatePickup);

			if (earlyPickup > latePickup)
			{
				Messages.Add(ValidationMessage.Error("Load order early pickup time is later than late pickup time"));
				valid = false;
			}

			if (!loadOrder.EstimatedDeliveryDate.IsValidSystemDateTime())
			{
                Messages.Add(ValidationMessage.Error("Load order estimated delivery date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!loadOrder.EarlyDelivery.IsValidTimeString())
			{
                Messages.Add(ValidationMessage.Error("Load order early delivery time is invalid"));
				valid = false;
			}

			if (!loadOrder.LateDelivery.IsValidTimeString())
			{
                Messages.Add(ValidationMessage.Error("Load order late delivery time is invalid"));
				valid = false;
			}

            if (loadOrder.HazardousMaterial && !loadOrder.Items.Any(i => i.HazardousMaterial))
            {
                Messages.Add(ValidationMessage.Error("Load Order cannot be marked as Hazmat if no items are marked as Hazmat"));
                valid = false;
            }

            if (loadOrder.Status != LoadOrderStatus.Offered)
            {
                Messages.Add(ValidationMessage.Error("Load order must of a status of {0}", LoadOrderStatus.Offered));
                valid = false;
            }

			var earlyDelivery = DateTime.Now.SetTime(loadOrder.EarlyDelivery);
			var lateDelivery = DateTime.Now.SetTime(loadOrder.LateDelivery);

			if (earlyDelivery > lateDelivery)
			{
				Messages.Add(ValidationMessage.Error("Load order early delivery time is later than late delivery time"));
				valid = false;
			}

			if (loadOrder.HazardousMaterial && string.IsNullOrEmpty(loadOrder.HazardousMaterialContactName))
			{
                Messages.Add(ValidationMessage.Error("Load order is marked hazardous and therefore requires a hazardous material contact name"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactName) && loadOrder.HazardousMaterialContactName.Length > 50)
			{
                Messages.Add(ValidationMessage.Error("Load order hazardous material contact name cannot exceed 50 characters"));
				valid = false;
			}

			if (loadOrder.HazardousMaterial && string.IsNullOrEmpty(loadOrder.HazardousMaterialContactPhone))
			{
                Messages.Add(ValidationMessage.Error("Load order is marked hazardous and therefore requires a hazardous material contact phone"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactPhone) && loadOrder.HazardousMaterialContactPhone.Length > 50)
			{
                Messages.Add(ValidationMessage.Error("Load order hazardous material contact phone cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactMobile) && loadOrder.HazardousMaterialContactMobile.Length > 50)
			{
                Messages.Add(ValidationMessage.Error("Load order hazardous material contact mobile cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactEmail) && loadOrder.HazardousMaterialContactEmail.Length > 100)
			{
                Messages.Add(ValidationMessage.Error("Load order hazardous material contact email cannot exceed 100 characters"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactEmail) && !loadOrder.HazardousMaterialContactEmail.EmailIsValid())
		    {
		        Messages.Add(ValidationMessage.Error("Load order hazardous material contact email is invalid"));
		        valid = false;
		    }

            if (!string.IsNullOrEmpty(loadOrder.PurchaseOrderNumber) && loadOrder.PurchaseOrderNumber.Length > 50)
			{
                Messages.Add(ValidationMessage.Error("Load order purchase order number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(loadOrder.ShipperReference) && loadOrder.ShipperReference.Length > 50)
			{
                Messages.Add(ValidationMessage.Error("Load order shipper reference cannot exceed 50 characters"));
				valid = false;
			}

			if (!loadOrder.Status.ToInt().EnumValueIsValid<LoadOrderStatus>())
			{
                Messages.Add(ValidationMessage.Error("Load order must have a valid status"));
				valid = false;
			}


			if (loadOrder.Equipments.Count > 0 && !LoadOrderEquipmentsAreValid(loadOrder.Equipments)) valid = false;
			if (loadOrder.Services.Count > 0 && !LoadOrderServicesAreValid(loadOrder.Services)) valid = false;
			var lastStopIndex = loadOrder.Destination == null ? default(long) : loadOrder.Destination.StopOrder;
            if (loadOrder.Items.Count > 0 && !LoadOrderItemsAreValid(loadOrder.Items, lastStopIndex, true)) valid = false;
			if (loadOrder.CustomerReferences.Count > 0 && !LoadOrderReferencesAreValid(loadOrder.CustomerReferences, loadOrder.Customer)) valid = false;

			return valid;
		}


        public bool QuotedLoadOrderIsValid(LoadOrder loadOrder)
        {
	        var valid = HasAllRequiredDataForQuotedLoad(loadOrder);

	        if (DuplicateLoadNumber(loadOrder)) valid = false;

            return valid;
        }

        private bool HasAllRequiredDataForQuotedLoad(LoadOrder loadOrder)
        {
            var valid = true;

            if (loadOrder.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Load order requires a organizational association"));
                valid = false;
            }

            if (loadOrder.Customer == null)
            {
                Messages.Add(ValidationMessage.Error("Load order requires a customer association"));
                valid = false;
            }
            else
            {

                if (loadOrder.Customer.RequiredMileageSourceId != default(long) && loadOrder.Customer.RequiredMileageSourceId != loadOrder.MileageSourceId)
                {
                    Messages.Add(ValidationMessage.Error("Customer requires mileage from mileage source {0}-{1}.",
                                                         loadOrder.Customer.MileageSource.Code, loadOrder.Customer.MileageSource.Description));
                    valid = false;
                }
            }

            var accountBucket = loadOrder.AccountBuckets.FirstOrDefault(ab => ab.Primary);
            if (accountBucket == null)
            {
                Messages.Add(ValidationMessage.Error("Load order requires an account bucket association"));
                valid = false;
            }

            if (loadOrder.User == null)
            {
                Messages.Add(ValidationMessage.Error("Load order requires a user association"));
                valid = false;
            }

            if (loadOrder.Origin == null)
            {
                Messages.Add(ValidationMessage.Error("Load order requires an origin location"));
                valid = false;
            }
            else if (!HasValidLocationInformation(new[] { loadOrder.Origin }))
            {
                Messages.Add(ValidationMessage.Error("Load order origin location has some invalid data"));
                valid = false;
            }

            if (loadOrder.Destination == null)
            {
                Messages.Add(ValidationMessage.Error("Load order requires an destination location"));
                valid = false;
            }
            else if (!HasValidLocationInformation(new[] { loadOrder.Destination }))
            {
                Messages.Add(ValidationMessage.Error("Load order destination location has some invalid data"));
                valid = false;
            }

            if (string.IsNullOrEmpty(loadOrder.LoadOrderNumber))
            {
                Messages.Add(ValidationMessage.Error("Load order requires a Load order number"));
                valid = false;
            }
            else if (loadOrder.LoadOrderNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Load order number cannot exceed 50 characters"));
                valid = false;
            }

            if (!loadOrder.ServiceMode.ToInt().EnumValueIsValid<ServiceMode>())
            {
                Messages.Add(ValidationMessage.Error("Load order must have a valid service mode"));
                valid = false;
            }

            if (!loadOrder.DesiredPickupDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Load order desired pickup date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!loadOrder.EarlyPickup.IsValidTimeString())
            {
                Messages.Add(ValidationMessage.Error("Load order early pickup time is invalid"));
                valid = false;
            }

            if (!loadOrder.LatePickup.IsValidTimeString())
            {
                Messages.Add(ValidationMessage.Error("Load order late pickup time is invalid"));
                valid = false;
            }

            var earlyPickup = DateTime.Now.SetTime(loadOrder.EarlyPickup);
            var latePickup = DateTime.Now.SetTime(loadOrder.LatePickup);

            if (earlyPickup > latePickup)
            {
                Messages.Add(ValidationMessage.Error("Load order early pickup time is later than late pickup time"));
                valid = false;
            }
            else if (loadOrder.ServiceMode == ServiceMode.LessThanTruckload && (latePickup - earlyPickup).Hours < 2)
            {
                Messages.Add(ValidationMessage.Error("Load order early pickup time cannot be within 2 hours of the late pickup time for Less Than Truckload load"));
                valid = false;
            }

            if (!loadOrder.EstimatedDeliveryDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Load order estimated delivery date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!loadOrder.EarlyDelivery.IsValidTimeString())
            {
                Messages.Add(ValidationMessage.Error("Load order early delivery time is invalid"));
                valid = false;
            }

            if (!loadOrder.LateDelivery.IsValidTimeString())
            {
                Messages.Add(ValidationMessage.Error("Load order late delivery time is invalid"));
                valid = false;
            }

            var earlyDelivery = DateTime.Now.SetTime(loadOrder.EarlyDelivery);
            var lateDelivery = DateTime.Now.SetTime(loadOrder.LateDelivery);

            if (loadOrder.HazardousMaterial && !loadOrder.Items.Any(i => i.HazardousMaterial))
            {
                Messages.Add(ValidationMessage.Error("Load Order cannot be marked as Hazmat if no items are marked as Hazmat"));
                valid = false;
            }

            if (earlyDelivery > lateDelivery)
            {
                Messages.Add(ValidationMessage.Error("Load order early delivery time is later than late delivery time"));
                valid = false;
            }

            if (loadOrder.Status != LoadOrderStatus.Quoted)
            {
                Messages.Add(ValidationMessage.Error("Load order must of a status of {0}", LoadOrderStatus.Quoted));
                valid = false;
            }

            if (!loadOrder.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Load order date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (loadOrder.HazardousMaterial && string.IsNullOrEmpty(loadOrder.HazardousMaterialContactName))
            {
                Messages.Add(ValidationMessage.Error("Load order is marked hazardous and therefore requires a hazardous material contact name"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactName) && loadOrder.HazardousMaterialContactName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Load order hazardous material contact name cannot exceed 50 characters"));
                valid = false;
            }

            if (loadOrder.HazardousMaterial && string.IsNullOrEmpty(loadOrder.HazardousMaterialContactPhone))
            {
                Messages.Add(ValidationMessage.Error("Load order is marked hazardous and therefore requires a hazardous material contact phone"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactPhone) && loadOrder.HazardousMaterialContactPhone.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Load order hazardous material contact phone cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactMobile) && loadOrder.HazardousMaterialContactMobile.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Load order hazardous material contact mobile cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactEmail) && loadOrder.HazardousMaterialContactEmail.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Load order hazardous material contact email cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.PurchaseOrderNumber) && loadOrder.PurchaseOrderNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Load order purchase order number cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.ShipperReference) && loadOrder.ShipperReference.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Load order shipper reference cannot exceed 50 characters"));
                valid = false;
            }

            if (loadOrder.Charges.Count > 0 && !ChargesAreValidForQuotedLoadOrder(loadOrder.Charges)) valid = false;
            if (loadOrder.Services.Count > 0 && !LoadOrderServicesAreValid(loadOrder.Services)) valid = false;
            var lastStopIndex = loadOrder.Destination == null ? default(long) : loadOrder.Destination.StopOrder;
            if (loadOrder.Items.Count > 0 && !LoadOrderItemsAreValid(loadOrder.Items, lastStopIndex)) valid = false;
            if (loadOrder.CustomerReferences.Count > 0 && !LoadOrderReferencesAreValid(loadOrder.CustomerReferences, loadOrder.Customer)) valid = false;

            return valid;
        }

        private bool ChargesAreValidForQuotedLoadOrder(IEnumerable<LoadOrderCharge> charges)
        {
            var valid = true;

            foreach (var charge in charges)
            {
                if (charge.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order charge requires a organizational association"));
                    valid = false;
                }

                if (charge.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order charge requires a Load order association"));
                    valid = false;
                }

                if (charge.ChargeCodeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order charge requires a charge code association"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(charge.Comment) && charge.Comment.Length > 100)
                {
                    Messages.Add(ValidationMessage.Error("Load order charge comment cannot exceed 100 characters"));
                    valid = false;
                }

                if (charge.Quantity <= 0)
                {
                    Messages.Add(ValidationMessage.Error("Load order charge quantity must be greater than zero"));
                    valid = false;
                }

                // We need to allow negative UnitBuys for P44 discounts to reflect them in the total UnitBuy
                if (charge.UnitBuy < 0 && charge.ChargeCode.Project44Code != Project44ChargeCode.DSC)
                {
                    Messages.Add(ValidationMessage.Error("Load order charge unit buy must be greater than or equal to zero"));
                    valid = false;
                }

                if (charge.UnitSell < 0)
                {
                    Messages.Add(ValidationMessage.Error("Load order charge unit sell must be greater than or equal to zero"));
                    valid = false;
                }

                if (charge.UnitDiscount < 0)
                {
                    Messages.Add(ValidationMessage.Error("Load order charge unit discount must be greater than or equal to zero"));
                    valid = false;
                }
            }

            return valid;
        }

        
        private bool DuplicateLoadNumber(LoadOrder loadOrder)
        {
            var validator = new LoadOrderValidator();
            var duplicateLoadNumber = validator.DuplicateLoadNumber(loadOrder);
            Messages.AddRange(validator.Messages);
            return duplicateLoadNumber;
        }

        private bool HasValidLocationInformation(IEnumerable<LoadOrderLocation> locations, bool isOfferedLoad = false)
		{
			var valid = true;

			foreach (var location in locations)
			{
				if (string.IsNullOrEmpty(location.Description) && !isOfferedLoad)
				{
                    Messages.Add(ValidationMessage.Error("Load order location requires a description"));
					valid = false;
				}
				else if (!string.IsNullOrEmpty(location.Description) && location.Description.Length > 50)
				{
                    Messages.Add(ValidationMessage.Error("Load order location Description cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.Street1) && location.Street1.Length > 50)
				{
                    Messages.Add(ValidationMessage.Error("Load order location Street 1 cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.Street2) && location.Street2.Length > 50)
				{
                    Messages.Add(ValidationMessage.Error("Load order location Street 2 cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.City) && location.City.Length > 50)
				{
                    Messages.Add(ValidationMessage.Error("Load order location City cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.State) && location.State.Length > 50)
				{
                    Messages.Add(ValidationMessage.Error("Load order location State cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.PostalCode) && location.PostalCode.Length > 10)
				{
                    Messages.Add(ValidationMessage.Error("Load order location Postal Code cannot exceed 10 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.SpecialInstructions) && location.SpecialInstructions.Length > 500)
				{
                    Messages.Add(ValidationMessage.Error("Load order location special instruction cannot exceed 500 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.GeneralInfo) && location.GeneralInfo.Length > 2000)
				{
                    Messages.Add(ValidationMessage.Error("Load order location general info cannot exceed 2000 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.Direction) && location.Direction.Length > 2000)
				{
                    Messages.Add(ValidationMessage.Error("Load order location direction cannot exceed 2000 characters"));
					valid = false;
				}

				if (location.MilesFromPreceedingStop < 0)
				{
					Messages.Add(ValidationMessage.Error("Load order location miles from preceeding stop cannot be less than zero"));
					valid = false;
				}

				if (location.CountryId == default(long))
				{
                    Messages.Add(ValidationMessage.Error("Load order location requires a country association"));
					valid = false;
				}

				if (location.TenantId == default(long))
				{
                    Messages.Add(ValidationMessage.Error("Load order location requires a organizational association"));
					valid = false;
				}

				if (location.LoadOrder == null)
				{
                    Messages.Add(ValidationMessage.Error("Load order location requires a load order association"));
					valid = false;
				}

				if (location.Contacts.Count > 0)
				{
					if (!HasValidLocationContactInformation(location.Contacts, isOfferedLoad)) valid = false;
					if (location.Contacts.Count(c => c.Primary) != 1)
					{
                        Messages.Add(ValidationMessage.Error("Load order location should have one (and only one) primary contact"));
						valid = false;
					}
				}

                if (!location.AppointmentDateTime.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Load order location appointment date/time must be between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }
			}

			return valid;
		}

        private bool HasValidLocationContactInformation(IEnumerable<LoadOrderContact> contacts, bool isOfferedLoad = false)
		{
			var valid = true;

			foreach (var contact in contacts)
			{
				if (contact.TenantId == default(long))
				{
                    Messages.Add(ValidationMessage.Error("Load order contact requires a organizational association"));
					valid = false;
				}

				if (contact.Location == null)
				{
                    Messages.Add(ValidationMessage.Error("Load order contact requires a location association"));
					valid = false;
				}

				if (contact.ContactTypeId == default(long))
				{
                    Messages.Add(ValidationMessage.Error("Load order contact requires a contact type association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(contact.Name) && !isOfferedLoad)
				{
                    Messages.Add(ValidationMessage.Error("Load order contact name is required"));
					valid = false;
				}
				else if (!string.IsNullOrEmpty(contact.Name) && contact.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Load order contact name cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Phone) && contact.Phone.Length > 50)
				{
                    Messages.Add(ValidationMessage.Error("Load order contact phone cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Mobile) && contact.Mobile.Length > 50)
				{
                    Messages.Add(ValidationMessage.Error("Load order contact mobile cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Fax) && contact.Fax.Length > 50)
				{
                    Messages.Add(ValidationMessage.Error("BLoad order contact fax cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Email) && contact.Email.Length > 100)
				{
                    Messages.Add(ValidationMessage.Error("Load order contact email cannot exceed 100 characters"));
					valid = false;
				}

			    if (!string.IsNullOrEmpty(contact.Email) && !contact.Email.EmailIsValid())
			    {
			        Messages.Add(ValidationMessage.Error("Load order contact email is invalid"));
			        valid = false;
			    }

                if (!string.IsNullOrEmpty(contact.Comment) && contact.Comment.Length > 200)
				{
                    Messages.Add(ValidationMessage.Error("Load order contact comment cannot exceed 200 characters"));
					valid = false;
				}
			}

			return valid;
		}

		private bool LoadOrderReferencesAreValid(IEnumerable<LoadOrderReference> customerReferences, Customer customer)
		{
			var valid = true;

			var requiredReferences = customer != null
										? customer.CustomFields
											.Where(c => c.Required)
											.Select(c => c.Name)
											.Distinct()
											.ToDictionary(c => c, c => c)
										: new Dictionary<string, string>();

			foreach (var reference in customerReferences)
			{
				if (reference.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Load order reference requires a organizational association"));
					valid = false;
				}

				if (reference.LoadOrder == null)
				{
					Messages.Add(ValidationMessage.Error("Load order reference requires a load order association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(reference.Name))
				{
					Messages.Add(ValidationMessage.Error("Load order reference must have a reference name"));
					valid = false;
				}
				else
				{
					if (requiredReferences.ContainsKey(reference.Name)) requiredReferences.Remove(reference.Name);

					if (reference.Name.Length > 50)
					{
						Messages.Add(ValidationMessage.Error("Load order reference name cannot exceed 100 characters"));
						valid = false;
					}
				}

				if (reference.Required && string.IsNullOrEmpty(reference.Value))
				{
					Messages.Add(ValidationMessage.Error("Load order reference is required and must therefore have a value"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(reference.Value) && reference.Value.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Load order reference value cannot exceed 50 characters"));
					valid = false;
				}
			}

			foreach (var key in requiredReferences.Keys)
			{
				Messages.Add(ValidationMessage.Error("Customer requires {0} reference field on load order.", key));
				valid = false;
			}

			return valid;
		}

        private bool LoadOrderItemsAreValid(IEnumerable<LoadOrderItem> items, long lastStopIndex, bool isOfferedLoad = false)
		{
			var valid = true;

			foreach (var item in items)
			{
				if (item.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Load order item requires a organizational association"));
					valid = false;
				}

				if (item.LoadOrder == null)
				{
					Messages.Add(ValidationMessage.Error("Load order item requires a load order association"));
					valid = false;
				}

				if (item.PackageTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Load order item requires a package type association"));
					valid = false;
				}

				if (item.Pickup < 0 || item.Pickup >= lastStopIndex)
				{
					Messages.Add(ValidationMessage.Error("Load order item pickup location index is invalid"));
					valid = false;
				}

				if (item.Delivery <= 0 || item.Delivery > lastStopIndex)
				{
					Messages.Add(ValidationMessage.Error("Load order item delivery location index is invalid"));
					valid = false;
				}

				if (string.IsNullOrEmpty(item.Description) && !isOfferedLoad)
				{
					Messages.Add(ValidationMessage.Error("Load order item must have a description"));
					valid = false;
				}
				else if (!string.IsNullOrEmpty(item.Description) && item.Description.Length > 100)
				{
					Messages.Add(ValidationMessage.Error("Load order item description cannot exceed 100 characters"));
					valid = false;
				}

				if (item.PieceCount < 0)
				{
					Messages.Add(ValidationMessage.Error("Load order item must have a piece count of zero or greater"));
					valid = false;
				}

				if (item.Quantity <= 0)
				{
					Messages.Add(ValidationMessage.Error("Load order item must have quantity greater than zero"));
					valid = false;
				}

				if (item.Weight <= 0 || item.Weight > 99999999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Load order item weight must be greater than zero and less than 100000000"));
					valid = false;
				}

				if (item.Length < 0 || item.Length > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Load order item length must be greater than or equal to zero and less than 10000"));
					valid = false;
				}
				if (item.Width < 0 || item.Width > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Load order item width must be greater than or equal to zero and less than 10000"));
					valid = false;
				}

				if (item.Height < 0 || item.Height > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Load order item height must be greater than or equal to zero and less than 10000"));
					valid = false;
				}

				if (item.IsOutsideAllowablePcf())
				{
					Messages.Add(ValidationMessage.Error("Load order item pounds per cubic feet not in allowable limits of {0} - {1} lb/ft3", CubicFootCalculations.MinPcf, CubicFootCalculations.MaxPcf));
					valid = false;
				}

				if (item.Value < 0)
				{
					Messages.Add(ValidationMessage.Error("Load order item value must be greater than zero"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(item.NMFCCode) && item.NMFCCode.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Load order item NMFC code cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(item.HTSCode) && item.HTSCode.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Load order item HTS code cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(item.Comment) && item.Comment.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Load order item comment cannot exceed 50 characters"));
					valid = false;
				}
			}

			return valid;
		}

		private bool LoadOrderEquipmentsAreValid(IEnumerable<LoadOrderEquipment> equipments)
		{
			var valid = true;

			foreach (var equipment in equipments)
			{
				if (equipment.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Load order equipment requires am organizational association"));
					valid = false;
				}

				if (equipment.LoadOrder == null)
				{
					Messages.Add(ValidationMessage.Error("Load order equipment requires a load order association"));
					valid = false;
				}

				if (equipment.EquipmentTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Load order equipment requires an equipment association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool LoadOrderServicesAreValid(IEnumerable<LoadOrderService> services)
		{
			var valid = true;

			foreach (var service in services)
			{
				if (service.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Load order service requires an organizational association"));
					valid = false;
				}

				if (service.LoadOrder == null)
				{
					Messages.Add(ValidationMessage.Error("Load order service requires a load order association"));
					valid = false;
				}

				if (service.Service == null)
				{
					Messages.Add(ValidationMessage.Error("Load order service requires a service association"));
					valid = false;
				}
			}

			return valid;
		}



		public bool ShipmentIsValid(Shipment shipment)
		{
			return HasAllRequiredData(shipment) && !DuplicateShipmentNumber(shipment);
		}

		private bool DuplicateShipmentNumber(Shipment shipment)
		{
			var validator = new ShipmentValidator();
			var duplicateShipmentNumber = validator.DuplicateShipmentNumber(shipment);
			Messages.AddRange(validator.Messages);
			return duplicateShipmentNumber;
		}

		private bool HasAllRequiredData(Shipment shipment)
		{
			var valid = true;

			if (shipment.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Shipment requires a organizational association"));
				valid = false;
			}

			var customer = shipment.Customer;
			if (customer == null)
			{
				Messages.Add(ValidationMessage.Error("Shipment requires a customer association"));
				valid = false;
			}
			else
			{
				if (customer.ShipmentRequiresPurchaseOrderNumber && string.IsNullOrEmpty(shipment.PurchaseOrderNumber))
				{
					Messages.Add(ValidationMessage.Error("Customer requires purchase order number on shipment"));
					valid = false;
				}

				if (customer.InvoiceRequiresShipperReference && string.IsNullOrEmpty(shipment.ShipperReference))
				{
					Messages.Add(ValidationMessage.Error("Customer requires shipper reference number on shipment"));
					valid = false;
				}
			}

			if (shipment.User == null)
			{
				Messages.Add(ValidationMessage.Error("Shipment requires a user association"));
				valid = false;
			}

			if (shipment.Origin == null)
			{
				Messages.Add(ValidationMessage.Error("Shipment requires an origin location"));
				valid = false;
			}
			else if (!HasValidLocationInformation(new[] { shipment.Origin }))
			{
				Messages.Add(ValidationMessage.Error("Shipment origin location has some invalid data"));
				valid = false;
			}

			if (shipment.Destination == null)
			{
				Messages.Add(ValidationMessage.Error("Shipment requires an destination location"));
				valid = false;
			}
			else if (!HasValidLocationInformation(new[] { shipment.Destination }))
			{
				Messages.Add(ValidationMessage.Error("Shipment destination location has some invalid data"));
				valid = false;
			}

			if (string.IsNullOrEmpty(shipment.ShipmentNumber))
			{
				Messages.Add(ValidationMessage.Error("Shipment requires a Shipment number"));
				valid = false;
			}
			else if (shipment.ShipmentNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Shipment number cannot exceed 50 characters"));
				valid = false;
			}

			if (!shipment.ServiceMode.ToInt().EnumValueIsValid<ServiceMode>())
			{
				Messages.Add(ValidationMessage.Error("Shipment must have a valid service mode"));
				valid = false;
			}

			if (!shipment.EarlyPickup.IsValidTimeString())
			{
				Messages.Add(ValidationMessage.Error("Shipment early pickup time is invalid"));
				valid = false;
			}

			if (!shipment.LatePickup.IsValidTimeString())
			{
				Messages.Add(ValidationMessage.Error("Shipment late pickup time is invalid"));
				valid = false;
			}

			var earlyPickup = DateTime.Now.SetTime(shipment.EarlyPickup);
			var latePickup = DateTime.Now.SetTime(shipment.LatePickup);

			if (earlyPickup > latePickup)
			{
				Messages.Add(ValidationMessage.Error("Shipment early pickup time is later than late pickup time"));
				valid = false;
			}
			else if (shipment.ServiceMode == ServiceMode.LessThanTruckload && (latePickup - earlyPickup).Hours < 2)
			{
				Messages.Add(ValidationMessage.Error("Shipment early pickup time cannot be within 2 hours of the late pickup time for Less Than Truckload shipment"));
				valid = false;
			}

			if (!shipment.EarlyDelivery.IsValidTimeString())
			{
				Messages.Add(ValidationMessage.Error("Shipment early delivery time is invalid"));
				valid = false;
			}

			if (!shipment.LateDelivery.IsValidTimeString())
			{
				Messages.Add(ValidationMessage.Error("Shipment late delivery time is invalid"));
				valid = false;
			}

			var earlyDelivery = DateTime.Now.SetTime(shipment.EarlyDelivery);
			var lateDelivery = DateTime.Now.SetTime(shipment.LateDelivery);

			if (earlyDelivery > lateDelivery)
			{
				Messages.Add(ValidationMessage.Error("Shipment early delivery time is later than late delivery time"));
				valid = false;
			}
			
			if (!shipment.Status.ToInt().EnumValueIsValid<ShipmentStatus>())
			{
				Messages.Add(ValidationMessage.Error("Shipment must have a valid status"));
				valid = false;
			}

			if (!shipment.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Shipment date created must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

            if (shipment.HazardousMaterial && !shipment.Items.Any(i => i.HazardousMaterial))
            {
                Messages.Add(ValidationMessage.Error("Shipment cannot be marked as Hazmat if no items are marked as Hazmat"));
                valid = false;
            }

			if (shipment.HazardousMaterial && string.IsNullOrEmpty(shipment.HazardousMaterialContactName))
			{
				Messages.Add(ValidationMessage.Error("Shipment is marked hazardous and therefore requires a hazardous material contact name"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(shipment.HazardousMaterialContactName) && shipment.HazardousMaterialContactName.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Shipment hazardous material contact name cannot exceed 50 characters"));
				valid = false;
			}

			if (shipment.HazardousMaterial && string.IsNullOrEmpty(shipment.HazardousMaterialContactPhone))
			{
				Messages.Add(ValidationMessage.Error("Shipment is marked hazardous and therefore requires a hazardous material contact phone"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(shipment.HazardousMaterialContactPhone) && shipment.HazardousMaterialContactPhone.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Shipment hazardous material contact phone cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(shipment.HazardousMaterialContactMobile) && shipment.HazardousMaterialContactMobile.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Shipment hazardous material contact mobile cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(shipment.HazardousMaterialContactEmail) && shipment.HazardousMaterialContactEmail.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Shipment hazardous material contact email cannot exceed 100 characters"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(shipment.HazardousMaterialContactEmail) && !shipment.HazardousMaterialContactEmail.EmailIsValid())
		    {
		        Messages.Add(ValidationMessage.Error("Shipment hazardous material contact email is invalid"));
		        valid = false;
		    }

            if (!string.IsNullOrEmpty(shipment.PurchaseOrderNumber) && shipment.PurchaseOrderNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Shipment purchase order number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(shipment.ShipperReference) && shipment.ShipperReference.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Shipment shipper reference cannot exceed 50 characters"));
				valid = false;
			}


			if (!shipment.Vendors.Any())
			{
				Messages.Add(ValidationMessage.Error("Shipment must have at least one vendor"));
				valid = false;
			}
			else
			{
				if (!ShipmentVendorsAreValid(shipment.Vendors)) valid = false;

				if (shipment.Vendors.Count(v => v.Primary) != 1)
				{
					Messages.Add(ValidationMessage.Error("Shipment should have one (and only one) primary vendor"));
					valid = false;
				}
			}

			if (!shipment.AccountBuckets.Any())
			{
				Messages.Add(ValidationMessage.Error("Shipment must have at least one account bucket designation"));
				valid = false;
			}
			else
			{
				if (!ShipmentAccountBucketsAreValid(shipment.AccountBuckets)) valid = false;

				if (shipment.AccountBuckets.Count(a => a.Primary) != 1)
				{
					Messages.Add(ValidationMessage.Error("Shipment should have one (and only one) primary account bucket designation"));
					valid = false;
				}
			}

			if (shipment.OriginalRateValue < 0)
			{
				Messages.Add(ValidationMessage.Error("Shipment original rate value buy must be greater than or equal to zero"));
				valid = false;
			}

			if (shipment.VendorDiscountPercentage < 0 || shipment.VendorDiscountPercentage > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Shipment vendor discount percentage must be greater than or equal to zero and less than 10000"));
				valid = false;
			}

			if (shipment.VendorFreightFloor < 0)
			{
				Messages.Add(ValidationMessage.Error("Shipment vendor freight floor buy must be greater than or equal to zero"));
				valid = false;
			}

			if (shipment.VendorFuelPercent < 0 || shipment.VendorFuelPercent > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Shipment vendor fuel percent mmust be greater than or equal to zero and less than 10000"));
				valid = false;
			}

			if (shipment.CustomerFreightMarkupValue < 0)
			{
				Messages.Add(ValidationMessage.Error("Shipment customer freight markup value must be greater than or equal to zero"));
				valid = false;
			}

			if (shipment.CustomerFreightMarkupPercent < 0)
			{
				Messages.Add(ValidationMessage.Error("Shipment customer freight markup percent must be greater than or equal to zero"));
				valid = false;
			}

			if (shipment.BilledWeight < 0)
			{
				Messages.Add(ValidationMessage.Error("Shipment billed weight must be greater than or equal to zero"));
				valid = false;
			}

			if (shipment.RatedWeight < 0)
			{
				Messages.Add(ValidationMessage.Error("Shipment rated weight must be greater than or equal to zero"));
				valid = false;
			}

            if (shipment.RatedCubicFeet < 0)
            {
                Messages.Add(ValidationMessage.Error("Shipment rated cubic feet must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.RatedPcf < 0)
            {
                Messages.Add(ValidationMessage.Error("Shipment rated pounds per cubic feet must be greater than or equal to zero"));
                valid = false;
            }

			if (shipment.LineHaulProfitAdjustmentRatio < 0 || shipment.LineHaulProfitAdjustmentRatio > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Shipment line haul profit adjustment ratio must be greater than or equal to zero and less than 10000"));
				valid = false;
			}

			if (shipment.FuelProfitAdjustmentRatio < 0 || shipment.FuelProfitAdjustmentRatio > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Shipment fuel profit adjustment ratio must be greater than or equal to zero and less than 10000"));
				valid = false;
			}

			if (shipment.AccessorialProfitAdjustmentRatio < 0 || shipment.AccessorialProfitAdjustmentRatio > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Shipment accessorial profit adjustment ratio must be greater than or equal to zero and less than 10000"));
				valid = false;
			}

			if (shipment.ServiceProfitAdjustmentRatio < 0 || shipment.ServiceProfitAdjustmentRatio > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Shipment service profit adjustment ratio must be greater than or equal to zero and less than 10000"));
				valid = false;
			}

			if (shipment.ResellerAdditionalFreightMarkup < 0)
			{
				Messages.Add(ValidationMessage.Error("Shipment reseller additional freight markup must be greater than or equal to zero"));
				valid = false;
			}

			if (shipment.ResellerAdditionalFuelMarkup < 0)
			{
				Messages.Add(ValidationMessage.Error("Shipment reseller additional fuel markup must be greater than or equal to zero"));
				valid = false;
			}

			if (shipment.ResellerAdditionalAccessorialMarkup < 0)
			{
				Messages.Add(ValidationMessage.Error("Shipment reseller additional accessorial markup must be greater than or equal to zero"));
				valid = false;
			}

			if (shipment.ResellerAdditionalServiceMarkup < 0)
			{
				Messages.Add(ValidationMessage.Error("Shipment reseller additional service markup must be greater than or equal to zero"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(shipment.SmallPackType) && shipment.SmallPackType.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Shipment small pack service type cannot exceed 50 characters"));
				valid = false;
			}

			if (!shipment.SmallPackageEngine.ToInt().EnumValueIsValid<SmallPackageEngine>())
			{
				Messages.Add(ValidationMessage.Error("Shipment small pack engine is invalid"));
				valid = false;
			}

			if (shipment.SalesRepresentativeCommissionPercent < 0 || shipment.SalesRepresentativeCommissionPercent > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Shipment sales representative commission percent must be greater than zero and less than 10000"));
				valid = false;
			}

			if (shipment.OriginTerminal != null && !LTLTerminalInfoIsValid(shipment.OriginTerminal))
			{
				Messages.Add(ValidationMessage.Error("LTL Origin Terminal has some invalid information"));
				valid = false;
			}

			if (shipment.DestinationTerminal != null && !LTLTerminalInfoIsValid(shipment.DestinationTerminal))
			{
				Messages.Add(ValidationMessage.Error("LTL Destination Terminal has some invalid information"));
				valid = false;
			}

			if (shipment.Charges.Count > 0 && !ShipmentChargesAreValid(shipment.Charges)) valid = false;
			if (shipment.Services.Count > 0 && !ShipmentServicesAreValid(shipment.Services)) valid = false;

			var lastStopIndex = shipment.Destination == null ? default(long) : shipment.Destination.StopOrder;
			var requiresNMFCCode = customer != null && customer.ShipmentRequiresNMFC;

			if (shipment.Items.Count > 0 && !ShipmentItemsAreValid(shipment.Items, lastStopIndex, requiresNMFCCode)) valid = false;
			if (shipment.CustomerReferences.Count > 0 && !ShipmentReferencesAreValid(shipment.CustomerReferences, customer)) valid = false;
			if (shipment.AutoRatingAccessorials.Count > 0 && !ShipmentAutoRatingAccessorialsAreValid(shipment.AutoRatingAccessorials)) valid = false;
			if (shipment.Documents.Count > 0 && !ShipmentDocumentsAreValid(shipment.Documents)) valid = false;

			return valid;
		}

		private bool LTLTerminalInfoIsValid(LTLTerminalInfo info)
		{
			var valid = true;

			if (info.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info requires a tenant association "));
				valid = false;
			}

			if (info.Shipment == null)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info requires a shipment association "));
				valid = false;
			}

			if (string.IsNullOrEmpty(info.Code))
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info requires a code "));
				valid = false;
			}
			else if (info.Code.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info code cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.Name) && info.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info Name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.Phone) && info.Phone.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info phone number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.TollFree) && info.TollFree.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info toll free number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.Fax) && info.Fax.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info fax number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.Email) && info.Email.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info email address cannot exceed 100 characters"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(info.Email) && !info.Email.EmailIsValid())
		    {
		        Messages.Add(ValidationMessage.Error("LTL Terminal Info email is invalid"));
		        valid = false;
		    }

            if (!string.IsNullOrEmpty(info.ContactName) && info.ContactName.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info contact name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.ContactTitle) && info.ContactTitle.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info contact title cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.Street1) && info.Street1.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info street1 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.Street2) && info.Street2.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info street2 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.City) && info.City.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info city cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.State) && info.State.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info state cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(info.PostalCode) && info.PostalCode.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info postal code cannot exceed 50 characters"));
				valid = false;
			}

			if (info.CountryId == default(long))
			{
				Messages.Add(ValidationMessage.Error("LTL Terminal Info requires a country association"));
				valid = false;
			}


			return valid;
		}

		private bool ShipmentDocumentsAreValid(IEnumerable<ShipmentDocument> documents)
		{
			var valid = true;

			foreach (var document in documents)
			{
				if (document.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment document requires a organizational association"));
					valid = false;
				}

				if (document.Shipment == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment document requires a shipment association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Name))
				{
					Messages.Add(ValidationMessage.Error("Shipment document requires a name"));
					valid = false;
				}
				else if (document.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment document name cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Description))
				{
					Messages.Add(ValidationMessage.Error("Shipment document requires a description"));
					valid = false;
				}
				else if (document.Description.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Shipment document description cannot exceed 500 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.LocationPath))
				{
					Messages.Add(ValidationMessage.Error("Shipment document requires a location path"));
					valid = false;
				}
				else if (document.LocationPath.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Shipment document location path cannot exceed 200 characters"));
					valid = false;
				}
			}
			return valid;
		}

		private bool HasValidLocationInformation(IEnumerable<ShipmentLocation> locations)
		{
			var valid = true;

			foreach (var location in locations)
			{
				if (string.IsNullOrEmpty(location.Description))
				{
					Messages.Add(ValidationMessage.Error("Shipment location requires a description"));
					valid = false;
				}
				else if (location.Description.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment location Description cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(location.Street1))
				{
					Messages.Add(ValidationMessage.Error("Shipment location requires a street1 address"));
					valid = false;
				}
				else if (location.Street1.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment location Street1 cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.Street2) && location.Street2.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment location Street2 cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(location.City))
				{
					Messages.Add(ValidationMessage.Error("Shipment location requires a city"));
					valid = false;
				}
				else if (location.City.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment location City cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(location.State))
				{
					Messages.Add(ValidationMessage.Error("Shipment location requires a state"));
					valid = false;
				}
				else if (location.State.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment location State cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(location.PostalCode))
				{
					Messages.Add(ValidationMessage.Error("Shipment location requires a postal code"));
					valid = false;
				}
				else if (location.PostalCode.Length > 10)
				{
					Messages.Add(ValidationMessage.Error("Shipment location Postal Code cannot exceed 10 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.SpecialInstructions) && location.SpecialInstructions.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Shipment location special instruction cannot exceed 500 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.GeneralInfo) && location.GeneralInfo.Length > 2000)
				{
					Messages.Add(ValidationMessage.Error("Shipment location general info cannot exceed 2000 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.Direction) && location.Direction.Length > 2000)
				{
					Messages.Add(ValidationMessage.Error("Shipment location direction cannot exceed 2000 characters"));
					valid = false;
				}

				if (location.CountryId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment location requires a country association"));
					valid = false;
				}

				if (location.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment location requires a organizational association"));
					valid = false;
				}

                if (!location.AppointmentDateTime.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Shipment location appointment date/time must be between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime,
                                                         DateUtility.SystemLatestDateTime));
                    valid = false;
                }

				if (location.MilesFromPreceedingStop < 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment location miles from preceeding stop cannot be less than zero"));
					valid = false;
				}

				if (location.Shipment == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment location requires a Shipment association"));
					valid = false;
				}

				if (location.Contacts.Count > 0)
				{
					if (!HasValidLocationContactInformation(location.Contacts)) valid = false;
					if (location.Contacts.Count(c => c.Primary) != 1)
					{
						Messages.Add(ValidationMessage.Error(" Shipment location should have one (and only one) primary contact"));
						valid = false;
					}
				}
			}

			return valid;
		}

		private bool HasValidLocationContactInformation(IEnumerable<ShipmentContact> contacts)
		{
			var valid = true;

			foreach (var contact in contacts)
			{
				if (contact.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment contact requires a organizational association"));
					valid = false;
				}

				if (contact.Location == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment contact requires a Shipment location association"));
					valid = false;
				}

				if (contact.ContactTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment contact requires a contact type association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(contact.Name))
				{
					Messages.Add(ValidationMessage.Error("Shipment contact name is required"));
					valid = false;
				}
				else if (contact.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment contact name cannot exceed 50 characters"));
					valid = false;
				}

				if(string.IsNullOrEmpty(contact.Phone))
				{
					Messages.Add(ValidationMessage.Error("Shipment contact phone is required"));
					valid = false;
				}
				else if (contact.Phone.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment contact phone cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Mobile) && contact.Mobile.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment contact mobile cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Fax) && contact.Fax.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment contact fax cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Email) && contact.Email.Length > 100)
				{
					Messages.Add(ValidationMessage.Error("Shipment contact email cannot exceed 100 characters"));
					valid = false;
				}

			    if (!string.IsNullOrEmpty(contact.Email) && !contact.Email.EmailIsValid())
			    {
			        Messages.Add(ValidationMessage.Error("Shipment contact email is invalid"));
			        valid = false;
			    }


                if (!string.IsNullOrEmpty(contact.Comment) && contact.Comment.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Shipment contact comment cannot exceed 200 characters"));
					valid = false;
				}
			}

			return valid;
		}

		private bool ShipmentVendorsAreValid(IEnumerable<ShipmentVendor> vendors)
		{
			var valid = true;

			foreach (var vendor in vendors)
			{
				if (vendor.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment vendor requires a organizational association"));
					valid = false;
				}

				if (vendor.Shipment == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment vendor requires a Shipment association"));
					valid = false;
				}

				if (vendor.Vendor == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment vendor requires a service association"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(vendor.FailureComments) && vendor.FailureComments.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Shipment vendor failure comment cannot exceed 500 characters"));
					valid = false;
				}
			}

			return valid;
		}

		private bool ShipmentAccountBucketsAreValid(IEnumerable<ShipmentAccountBucket> accountBuckets)
		{
			var valid = true;

			foreach (var bucket in accountBuckets)
			{
				if (bucket.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment account bucket requires a organizational association"));
					valid = false;
				}

				if (bucket.Shipment == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment account bucket requires a shipment association"));
					valid = false;
				}

				if (bucket.AccountBucket == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment account bucket requires an account bucket association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool ShipmentAutoRatingAccessorialsAreValid(IEnumerable<ShipmentAutoRatingAccessorial> accessorials)
		{
			var valid = true;

			foreach (var accessory in accessorials)
			{
				if (accessory.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial requires a organizational association"));
					valid = false;
				}

				if (accessory.Shipment == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial requires a shipment association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(accessory.ServiceDescription))
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial requires a description"));
					valid = false;
				}
				else if (accessory.ServiceDescription.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial description cannot exceed 200 characters"));
					valid = false;
				}

				if (!accessory.RateType.ToInt().EnumValueIsValid<RateType>())
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial rate type is invalid"));
					valid = false;
				}

				if (accessory.Rate < 0 || accessory.Rate > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial rate must be between zero and 9999.9999"));
					valid = false;
				}

				if (accessory.BuyFloor < 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial buy floor value must be greater than zero"));
					valid = false;
				}

				if (accessory.BuyCeiling < 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial buy ceiling value must be greater than zero"));
					valid = false;
				}

				if (accessory.SellFloor < 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial sell floor value must be greater than zero"));
					valid = false;
				}

				if (accessory.SellCeiling < 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial sell ceiling value must be greater than zero"));
					valid = false;
				}

				if (accessory.MarkupValue < 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial markup value must be greater than zero"));
					valid = false;
				}

				if (accessory.MarkupPercent < 0 || accessory.MarkupPercent > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial rate must be between zero and 9999.9999"));
					valid = false;
				}
			}

			return valid;
		}

		private bool ShipmentChargesAreValid(IEnumerable<ShipmentCharge> charges)
		{
			var valid = true;

			foreach (var charge in charges)
			{
				if (charge.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment charge requires a organizational association"));
					valid = false;
				}

				if (charge.Shipment == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment charge requires a Shipment association"));
					valid = false;
				}

				if (charge.ChargeCodeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment charge requires a charge code association"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(charge.Comment) && charge.Comment.Length > 100)
				{
					Messages.Add(ValidationMessage.Error("Shipment charge comment cannot exceed 100 characters"));
					valid = false;
				}

				if (charge.Quantity <= 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment charge quantity must be greater than zero"));
					valid = false;
				}

			    // We need to allow negative UnitBuys for P44 discounts to reflect them in the total UnitBuy
                if (charge.UnitBuy < 0 && charge.ChargeCode.Project44Code != Project44ChargeCode.DSC)
				{
					Messages.Add(ValidationMessage.Error("Shipment charge unit buy must be greater than or equal to zero"));
					valid = false;
				}

				if (charge.UnitSell < 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment charge unit sell must be greater than or equal to zero"));
					valid = false;
				}

				if (charge.UnitDiscount < 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment charge unit discount must be greater than or equal to zero"));
					valid = false;
				}
			}

			return valid;
		}

		private bool ShipmentReferencesAreValid(IEnumerable<ShipmentReference> customerReferences, Customer customer)
		{
			var valid = true;

			var requiredReferences = customer != null
										? customer.CustomFields
											.Where(c => c.Required)
											.Select(c => c.Name)
											.Distinct()
											.ToDictionary(c => c, c => c)
										: new Dictionary<string, string>();

			foreach (var reference in customerReferences)
			{
				if (reference.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment reference requires a organizational association"));
					valid = false;
				}

				if (reference.Shipment == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment reference requires a Shipment association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(reference.Name))
				{
					Messages.Add(ValidationMessage.Error("Shipment reference must have a reference name"));
					valid = false;
				}
				else
				{
					if (requiredReferences.ContainsKey(reference.Name)) requiredReferences.Remove(reference.Name);

					if (reference.Name.Length > 50)
					{
						Messages.Add(ValidationMessage.Error("Shipment reference name cannot exceed 100 characters"));
						valid = false;
					}
				}

				if (reference.Required && string.IsNullOrEmpty(reference.Value))
				{
					Messages.Add(ValidationMessage.Error("Shipment reference is required and must therefore have a value"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(reference.Value) && reference.Value.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment reference value cannot exceed 50 characters"));
					valid = false;
				}
			}

			foreach (var key in requiredReferences.Keys)
			{
				Messages.Add(ValidationMessage.Error("Customer requires {0} reference field on Shipment.", key));
				valid = false;
			}

			return valid;
		}

		private bool ShipmentItemsAreValid(IEnumerable<ShipmentItem> items, long lastStopIndex, bool requiresNMFCCode)
		{
			var valid = true;

			foreach (var item in items)
			{
				if (item.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment item requires a organizational association"));
					valid = false;
				}

				if (item.Shipment == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment item requires a Shipment association"));
					valid = false;
				}

				if (item.PackageTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment item requires a package type association"));
					valid = false;
				}

				if (item.Pickup < 0 || item.Pickup >= lastStopIndex)
				{
					Messages.Add(ValidationMessage.Error("Shipment item pickup location index is invalid"));
					valid = false;
				}

				if (item.Delivery <= 0 || item.Delivery > lastStopIndex)
				{
					Messages.Add(ValidationMessage.Error("Shipment item delivery location index is invalid"));
					valid = false;
				}

				if (string.IsNullOrEmpty(item.Description))
				{
					Messages.Add(ValidationMessage.Error("Shipment item must have a description"));
					valid = false;
				}
				else if (item.Description.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Shipment item description cannot exceed 200 characters"));
					valid = false;
				}

				if (item.PieceCount < 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment item must have a piece count of zero or greater"));
					valid = false;
				}

				if (item.Quantity <= 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment item must have quantity greater than zero"));
					valid = false;
				}

				if (item.ActualWeight <= 0 || item.ActualWeight > 99999999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Shipment actual item weight must be greater than zero and less than 100000000"));
					valid = false;
				}

				if (item.ActualLength < 0 || item.ActualLength > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Shipment actual item length must be greater than or equal to zero and less than 10000"));
					valid = false;
				}
				if (item.ActualWidth < 0 || item.ActualWidth > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Shipment actual item width must be greater than or equal to zero and less than 10000"));
					valid = false;
				}

				if (item.ActualHeight < 0 || item.ActualHeight > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Shipment actual item height must be greater than or equal to zero and less than 10000"));
					valid = false;
				}

				if (item.IsOutsideAllowablePcfActual())
				{
					Messages.Add(ValidationMessage.Error("Shipment actual item pounds per cubic feet not in allowable limits of {0} - {1} lb/ft3", CubicFootCalculations.MinPcf, CubicFootCalculations.MaxPcf));
					valid = false;
				}

				if (item.Value < 0)
				{
					Messages.Add(ValidationMessage.Error("Shipment item value must be greater than zero"));
					valid = false;
				}

				if (requiresNMFCCode && string.IsNullOrEmpty(item.NMFCCode))
				{
					Messages.Add(ValidationMessage.Error("Customer requires item NMFC code on all items"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(item.NMFCCode) && item.NMFCCode.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment item NMFC code cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(item.HTSCode) && item.HTSCode.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment item HTS code cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(item.Comment) && item.Comment.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Shipment item comment cannot exceed 50 characters"));
					valid = false;
				}
			}

			return valid;
		}

		private bool ShipmentServicesAreValid(IEnumerable<ShipmentService> services)
		{
			var valid = true;

			foreach (var service in services)
			{
				if (service.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment service requires a organizational association"));
					valid = false;
				}

				if (service.Shipment == null)
				{
					Messages.Add(ValidationMessage.Error("Shipment service requires a Shipment association"));
					valid = false;
				}

				if (service.ServiceId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Shipment service requires a service association"));
					valid = false;
				}
			}

			return valid;
		}
	}
}
