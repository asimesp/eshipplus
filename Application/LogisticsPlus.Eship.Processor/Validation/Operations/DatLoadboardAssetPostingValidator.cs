﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Dat;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class DatLoadboardAssetPostingValidator : ValidatorBase
    {
        public bool IsValid(DatLoadboardAssetPosting posting)
        {
            var valid = HasAllRequiredData(posting);

            if (DuplicateIdNumber(posting)) valid = false;
            if (DuplicateAssetId(posting)) valid = false;

            return valid;
        }

        public bool DuplicateIdNumber(DatLoadboardAssetPosting posting)
        {
            const string query = @"SELECT COUNT(*) FROM DatLoadboardAssetPosting WHERE IdNumber = @IdNumber AND TenantId = @TenantId AND Id <> @Id";
            var parameters = new Dictionary<string, object>
                                 {
                                     {"IdNumber", posting.IdNumber},
                                     {"TenantId", posting.TenantId},
                                     {"Id", posting.Id}
                                 };
            var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateNumber) Messages.Add(ValidationMessage.Error("Id Number is already in use"));
            return duplicateNumber;
        }

        public bool DuplicateAssetId(DatLoadboardAssetPosting posting)
        {
            const string query = @"SELECT COUNT(*) FROM DatLoadboardAssetPosting WHERE AssetId = @AssetId AND TenantId = @TenantId AND Id <> @Id";
            var parameters = new Dictionary<string, object>
                                 {
                                     {"AssetId", posting.AssetId},
                                     {"TenantId", posting.TenantId},
                                     {"Id", posting.Id}
                                 };
            var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateNumber) Messages.Add(ValidationMessage.Error("Asset Id is already in use"));
            return duplicateNumber;
        }

        public bool HasAllRequiredData(DatLoadboardAssetPosting posting)
        {
            var valid = true;

            if (posting.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting requires a Tenant association"));
                valid = false;
            }

            if (posting.User == null)
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting requires a User association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(posting.IdNumber))
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting requires an id number"));
                valid = false;
            }
            else if (posting.IdNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting id number cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(posting.AssetId))
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting requires an asset id"));
                valid = false;
            }
            else if (posting.AssetId.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting asset id cannot exceed 50 characters"));
                valid = false;
            }


            if (posting.Mileage < 0 || posting.Mileage > 9999)
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting mileage must be greater than or equal to zero and less than 9999"));
                valid = false;
            }

            if (!posting.RateType.ToInt().EnumValueIsValid<RateBasedOnType>())
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting must have a valid rate type"));
                valid = false;
            }

            if (posting.BaseRate < 0 || posting.BaseRate > 99999.99m)
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting base rate must be greater than or equal to zero and less than 99999.99"));
                valid = false;
            }

            if (!posting.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!posting.ExpirationDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("DAT Loadboard Asset Posting expiration date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            return valid;
        }
    }
}
