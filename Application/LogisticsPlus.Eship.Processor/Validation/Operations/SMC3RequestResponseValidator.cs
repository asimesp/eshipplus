﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class SMC3RequestResponseValidator : ValidatorBase
    {
        public bool IsValid(SMC3TrackRequestResponse smc)
        {
	        bool valid = HasAllRequiredData(smc);

	        return valid;
        }

		public bool IsValid(SMC3DispatchRequestResponse smc)
		{
			bool valid = HasAllRequiredData(smc);

			return valid;
		}
        
        private bool HasAllRequiredData(SMC3RequestResponse smctrr)
        {
            var valid = true;

            if (string.IsNullOrEmpty(smctrr.TransactionId))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Request Response requires a Transaction Id"));
                valid = false;
            }
            else if (smctrr.TransactionId.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Transaction Id cannot exceed 200 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(smctrr.ResponseData))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Request Response requires Response Data"));
                valid = false;
            }
            else if (smctrr.ResponseData.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Response Data cannot exceed 500 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(smctrr.Scac))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Request Response requires Scac"));
                valid = false;
            }
            else if (smctrr.Scac.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Scac cannot exceed 10 characters"));
                valid = false;
            }


            if (!smctrr.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("SMC3 Data date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (smctrr.ShipmentId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Shipment Id required"));
                valid = false;
            }
            
            return valid;
        }

        
    }
}
