﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class AssetLogValidator : ValidatorBase
    {
        public bool IsValid(AssetLog log)
        {
            return HasAllRequiredData(log);
        }

        private bool HasAllRequiredData(AssetLog log)
        {
            var valid = true;

            if (log.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Asset log requires a tenant association"));
                valid = false;
            }

            if (log.AssetId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Asset log requires an asset association"));
                valid = false;
            }

            if(!string.IsNullOrEmpty(log.Comment) && log.Comment.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Asset log comment cannot exceed 200 characters"));
                valid = false;
            }

            if (!log.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Asset log date created must be between {0} and {1}",
                                                 DateUtility.SystemEarliestDateTime,
                                                 DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!log.LogDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Asset log date must be between {0} and {1}",
                                                 DateUtility.SystemEarliestDateTime,
                                                 DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!log.MileageEngine.ToInt().EnumValueIsValid<MileageEngine>())
            {
                Messages.Add(ValidationMessage.Error("Asset log mileage engine is invalid"));
                valid = false;
            }

			if (log.MilesRun < 0)
			{
				Messages.Add(ValidationMessage.Error("Asset log mileage must be greater than or equal to zero"));
				valid = false;
			}

        	if (!string.IsNullOrEmpty(log.Street1) && log.Street1.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Asset log Street1 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(log.Street2) && log.Street2.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Asset log Street2 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(log.City) && log.City.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Asset log location City cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(log.State) && log.State.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Asset log location State cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(log.PostalCode))
            {
                Messages.Add(ValidationMessage.Error("Asset log  requires a postal code"));
                valid = false;
            }
            else if(log.PostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Asset log postal code cannot exceed 10 characters"));
                valid = false;
            }

            if (log.CountryId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Asset log requires a country association"));
                valid = false;
            }

            return valid;
        }

    }
}
