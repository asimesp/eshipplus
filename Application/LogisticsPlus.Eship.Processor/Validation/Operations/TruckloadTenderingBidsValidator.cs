﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
	public class TruckloadBidValidator : ValidatorBase
	{
		
		public bool IsValid(TruckloadBid bids)
		{
			bool valid = HasAllRequiredData(bids);

			return valid;
		}


		public bool HasAllRequiredData(TruckloadBid bids)
		{
			var valid = true;

			if (bids.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires a tenant association"));
				valid = false;
			}
			
			if (bids.LoadOrderId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires a loadorder association"));
				valid = false;
			}
			if (bids.TLTenderingProfileId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires a lane association"));
				valid = false;
			}
			if (bids.FirstPreferredVendorId == default(long))
			{
				Messages.Add(ValidationMessage.Error("There is no First Preferred Vendor association for customer tendering profile for customer on this Load order."));
				valid = false;
			}
			if (bids.TLLaneId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires a lane association"));
				valid = false;
			}
			if (bids.TimeLoadTendered1 == DateUtility.SystemEarliestDateTime)
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires time when the load was tenedered to 1st vendor"));
				valid = false;
			}
			if (bids.TimeLoadTendered1 == DateUtility.SystemEarliestDateTime)
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires time when the load was tenedered to 1st vendor"));
				valid = false;
			}
			if (bids.ExpirationTime1 == DateUtility.SystemEarliestDateTime)
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires time when the load tenedered to 1st vendor expires"));
				valid = false;
			}
			if (bids.UserId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tendering bid requires a current user association."));
				valid = false;
			}
			
			return valid;
		}
		

		public bool HasValidData(TruckloadBid bid, LoadOrder loadOrder)
		{
			var valid = true;
			var tlProfile = new CustomerTLTenderingProfile(bid.TLTenderingProfileId);
			var checkIfLoadTendered = new TruckloadBidSearch().FetchTruckloadBidByLoadOrderId(loadOrder.Id, loadOrder.TenantId);
			if (loadOrder.Customer.Id != tlProfile.CustomerId)
			{
				Messages.Add(ValidationMessage.Error("Customer on truckload bid should same as customer for the load order"));
				valid = false;
			}
			if (loadOrder.Status != LoadOrderStatus.Offered)
			{
				Messages.Add(ValidationMessage.Error("Load Order status should be 'Offered' for tendering the load."));
				valid = false;
			}
			if (loadOrder.ServiceMode != ServiceMode.Truckload)
			{
				Messages.Add(ValidationMessage.Error("Service mode for the load order should be 'Truckload' for tendering the load."));
				valid = false;
			}
			if (checkIfLoadTendered != null)
			{
				Messages.Add(ValidationMessage.Error("Bid for this load order already exists."));
				valid = false;
			}
			if (ProcessorUtilities.HasMatchingLane(tlProfile, loadOrder.Id) == default(long))
			{
				Messages.Add(ValidationMessage.Error("There is no matching lane for this LoadOrder in the Customer Profile."));
				valid = false;
			}

			return valid;

		}
		
	}
}
