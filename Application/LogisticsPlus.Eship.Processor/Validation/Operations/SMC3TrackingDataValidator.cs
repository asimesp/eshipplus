﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class SMC3TrackingDataValidator : ValidatorBase
    {
        public bool IsValid(SMC3TrackingData smc)
        {
	        bool valid = HasAllRequiredData(smc);

	        return valid;
        }

        private bool HasAllRequiredData(SMC3TrackingData smctd)
        {
            var valid = true;

            if (string.IsNullOrEmpty(smctd.TransactionId))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Tracking Data requires a Transaction Id"));
                valid = false;
            }
            else if (smctd.TransactionId.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Transaction Id cannot exceed 200 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(smctd.AccountToken))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Tracking Data requires an Account Token"));
                valid = false;
            }
            else if (smctd.AccountToken.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Account Token cannot exceed 200 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(smctd.ReferenceNumber))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Tracking Data requires a Reference Number"));
                valid = false;
            }
            else if (smctd.ReferenceNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Reference Number cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(smctd.ReferenceType))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Tracking Data requires a Reference Type"));
                valid = false;
            }
            else if (smctd.ReferenceNumber.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Reference Type cannot exceed 10 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(smctd.SCAC))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Tracking Data requires a SCAC"));
                valid = false;
            }
            else if (smctd.SCAC.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("SCAC cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.Pro) && smctd.Pro.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Pro cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.Bol) && smctd.Bol.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Bol cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.PO) && smctd.PO.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("PO cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.StatusCode) && smctd.StatusCode.Length > 2)
            {
                Messages.Add(ValidationMessage.Error("Status Code cannot exceed 2 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(smctd.StatusEventType) && smctd.StatusEventType.Length > 2)
            {
                Messages.Add(ValidationMessage.Error("Status Event Type cannot exceed 2 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.StatusDate) && smctd.StatusDate.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Status Date cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.StatusDescription) && smctd.StatusDescription.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Status Description cannot exceed 500 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.StatusTime) && smctd.StatusTime.Length > 8)
            {
                Messages.Add(ValidationMessage.Error("Status Time cannot exceed 8 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.StatusCity) && smctd.StatusCity.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Status City cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.StatusCountry) && smctd.StatusCountry.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Status Country cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.StatusStateProvince) && smctd.StatusStateProvince.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Status State Province cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.StatusPostalCode) && smctd.StatusPostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Status Postal Code cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.AppointmentDate) && smctd.AppointmentDate.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Appointment Date cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.AppointmentTime) && smctd.AppointmentTime.Length > 8)
            {
                Messages.Add(ValidationMessage.Error("Appointment Time cannot exceed 8 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.AppointmentType) && smctd.AppointmentType.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Appointment Type cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.PickupDate) && smctd.PickupDate.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Pickup Date cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.PickupTime) && smctd.PickupTime.Length > 8)
            {
                Messages.Add(ValidationMessage.Error("Pickup Time cannot exceed 8 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DeliveryDate) && smctd.DeliveryDate.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Delivery Date cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DeliveryTime) && smctd.DeliveryTime.Length > 8)
            {
                Messages.Add(ValidationMessage.Error("Delivery Time cannot exceed 8 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.ETADestinationDate) && smctd.ETADestinationDate.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("ETA Destination Date cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.ETADestinationTime) && smctd.ETADestinationTime.Length > 8)
            {
                Messages.Add(ValidationMessage.Error("ETA Destination Time cannot exceed 8 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.CCXLDestinationDate) && smctd.CCXLDestinationDate.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("CCXL Destination Date cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.CCXLDestinationTime) && smctd.CCXLDestinationTime.Length > 8)
            {
                Messages.Add(ValidationMessage.Error("CCXL Destination Time cannot exceed 8 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.CCXLCalendarDays) && smctd.CCXLCalendarDays.Length > 15)
            {
                Messages.Add(ValidationMessage.Error("CCXL Calendar Days cannot exceed 15 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.InterlineContactPhone) && smctd.InterlineContactPhone.Length > 25)
            {
                Messages.Add(ValidationMessage.Error("Interline Contact Phone cannot exceed 25 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.InterlinePartnerName) && smctd.InterlinePartnerName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Interline Partner Name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.InterlineNotes) && smctd.InterlineNotes.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Interline Notes cannot exceed 500 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.InterlinePro) && smctd.InterlinePro.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Interline Pro cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.InterlineSCAC) && smctd.InterlineSCAC.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Interline SCAC cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginTerminalCode) && smctd.OriginTerminalCode.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin Terminal Code cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginTerminalPhone) && smctd.OriginTerminalPhone.Length > 25)
            {
                Messages.Add(ValidationMessage.Error("Origin Terminal Phone cannot exceed 25 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginTerminalFax) && smctd.OriginTerminalFax.Length > 25)
            {
                Messages.Add(ValidationMessage.Error("Origin Terminal Fax cannot exceed 25 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginTerminalTollFree) && smctd.OriginTerminalTollFree.Length > 25)
            {
                Messages.Add(ValidationMessage.Error("Origin Terminal Toll Free cannot exceed 25 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginTerminalContactName) && smctd.OriginTerminalContactName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin Terminal Contact Name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginTerminalCity) && smctd.OriginTerminalCity.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin Terminal City cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginTerminalCountry) && smctd.OriginTerminalCountry.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin Terminal Country cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginTerminalStateProvince) && smctd.OriginTerminalStateProvince.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin Terminal State Province cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginTerminalPostalCode) && smctd.OriginTerminalPostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Origin Terminal Postal Code cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationTerminalCode) && smctd.DestinationTerminalCode.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination Terminal Code cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationTerminalPhone) && smctd.DestinationTerminalPhone.Length > 25)
            {
                Messages.Add(ValidationMessage.Error("Destination Terminal Phone cannot exceed 25 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationTerminalFax) && smctd.DestinationTerminalFax.Length > 25)
            {
                Messages.Add(ValidationMessage.Error("Destination Terminal Fax cannot exceed 25 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationTerminalTollFree) && smctd.DestinationTerminalTollFree.Length > 25)
            {
                Messages.Add(ValidationMessage.Error("Destination Terminal Toll Free cannot exceed 25 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationTerminalContactName) && smctd.DestinationTerminalContactName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination Terminal Contact Name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationTerminalCity) && smctd.DestinationTerminalCity.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination Terminal City cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationTerminalCountry) && smctd.DestinationTerminalCountry.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination Terminal Country cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationTerminalStateProvince) && smctd.DestinationTerminalStateProvince.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination Terminal State Province cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationTerminalPostalCode) && smctd.DestinationTerminalPostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Destination Terminal Postal Code cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.EquipmentId) && smctd.EquipmentId.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Equipment Id cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.EquipmentType) && smctd.EquipmentType.Length > 15)
            {
                Messages.Add(ValidationMessage.Error("Equipment Type cannot exceed 15 characters"));
                valid = false;
            }

            if (smctd.Weight < 0)
            {
                Messages.Add(ValidationMessage.Error("Weight must be greater than or equal to zero"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.WeightType) && smctd.WeightType.Length > 20)
            {
                Messages.Add(ValidationMessage.Error("Weight Type cannot exceed 20 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.WeightUnit) && smctd.WeightUnit.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Weight Unit cannot exceed 10 characters"));
                valid = false;
            }

            if (smctd.Pieces < 0)
            {
                Messages.Add(ValidationMessage.Error("Pieces must be greater than or equal to zero"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.PackagingType) && smctd.PackagingType.Length > 20)
            {
                Messages.Add(ValidationMessage.Error("Packaging Type cannot exceed 20 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.BillToAccount) && smctd.BillToAccount.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Bill To Account cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.BillToAddress1) && smctd.BillToAddress1.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Bill To Address 1 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.BillToAddress2) && smctd.BillToAddress2.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Bill To Address 2 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.BillToCity) &&  smctd.BillToCity.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Bill To City cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.BillToCountry) && smctd.BillToCountry.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Bill To Country cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.BillToName) && smctd.BillToName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Bill To Name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.BillToPhone) && smctd.BillToPhone.Length > 25)
            {
                Messages.Add(ValidationMessage.Error("Bill To Phone cannot exceed 25 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.BillToStateProvince) && smctd.BillToStateProvince.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Bill To Phone cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.BillToPostalCode) && smctd.BillToPostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Bill To Postal Code cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.PaymentTerms) && smctd.PaymentTerms.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Payment Terms cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationAccount) && smctd.DestinationAccount.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Destination Account cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationAddress1) && smctd.DestinationAddress1.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination Address 1 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationAddress2) && smctd.DestinationAddress2.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination Address 2 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationCity) && smctd.DestinationCity.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination City cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationCountry) && smctd.DestinationCountry.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination Country cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationName) && smctd.DestinationName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination Name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationPhone) && smctd.DestinationPhone.Length > 25)
            {
                Messages.Add(ValidationMessage.Error("Destination Phone cannot exceed 25 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationStateProvince) && smctd.DestinationStateProvince.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Destination State Province cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.DestinationPostalCode) && smctd.DestinationPostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Destination Postal Code cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginAccount) && smctd.OriginAccount.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Origin Account cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginAddress1) && smctd.OriginAddress1.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin Address 1 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginAddress2) && smctd.OriginAddress2.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin Address 2 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginCity) && smctd.OriginCity.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin City cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginCountry) && smctd.OriginCountry.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin Country cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginName) && smctd.OriginName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin Name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginPhone) && smctd.OriginPhone.Length > 25)
            {
                Messages.Add(ValidationMessage.Error("Origin Phone cannot exceed 25 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginStateProvince) && smctd.OriginStateProvince.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Origin State Province cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.OriginPostalCode) && smctd.OriginPostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Origin Postal Code cannot exceed 10 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(smctd.ResponseStatus))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Tracking Data requires a Response Status"));
                valid = false;
            }
            else if (smctd.ResponseStatus.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Response Status cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(smctd.ResponseCode))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Tracking Data requires a Response Code"));
                valid = false;
            }
            else if (smctd.ResponseCode.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Response Code cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smctd.ResponseMessage) && smctd.ResponseMessage.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Response Message cannot exceed 500 characters"));
                valid = false;
            }

            if (!smctd.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("SMC3 Tracking Data date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }
                
            return valid;
        }
    }
}
