﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class LoadOrderValidator : ValidatorBase
    {
        public bool LoadOrderServiceExists(LoadOrderService loadOrderService)
        {
            const string query =
                "SELECT COUNT(*) FROM LoadOrderService WHERE ServiceId = @ServiceId AND LoadOrderId = @LoadOrderId AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object>
                                 {
                                     {"ServiceId", loadOrderService.ServiceId},
                                     {"LoadOrderId", loadOrderService.LoadOrderId},
                                     {"TenantId", loadOrderService.TenantId}

                                 };
            var duplicateService = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateService)
                Messages.Add(ValidationMessage.Error("Service is already included in Load Order Services"));
            return duplicateService;
        }

        public bool LoadOrderEquipmentExists(LoadOrderEquipment loadOrderEquipment)
        {
            const string query =
                "SELECT COUNT(*) FROM LoadOrderEquipment WHERE EquipmentTypeId = @EquipmentTypeId AND LoadOrderId = @LoadOrderId AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object>
                                 {
                                     {"EquipmentTypeId", loadOrderEquipment.EquipmentTypeId},
                                     {"TenantId", loadOrderEquipment.TenantId},
                                     {"LoadOrderId", loadOrderEquipment.LoadOrderId}
                                 };
            var duplicateEquipment = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateEquipment)
                Messages.Add(ValidationMessage.Error("Equipment type is already included in Load Order Equipments"));
            return duplicateEquipment;
        }

		public bool CanDeleteLoadOrder(LoadOrder loadOrder)
		{
            const string query = "select dbo.LoadOrderUsageIdCount(@LoadOrderId)";
		    var parameters = new Dictionary<string, object> {{"LoadOrderId", loadOrder.Id}};
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Load order is referenced one or more times and cannot be deleted."));
			return !used;
		}

    	public bool IsValid(LoadOrder loadOrder)
        {
    		bool valid = HasAllRequiredData(loadOrder);

    		if (DuplicateLoadNumber(loadOrder)) valid = false;

            return valid;
        }

        public bool DuplicateLoadNumber(LoadOrder loadOrder)
        {
        	var query = loadOrder.IsNew
        	            	? @"SELECT 
								(SELECT COUNT(*) FROM LoadOrder WHERE LoadOrderNumber = @Number AND TenantId = @TenantId AND Id <> @Id) + 
								(SELECT COUNT(*) FROM Shipment WHERE ShipmentNumber = @Number AND TenantId = @TenantId) "
        	            	: @"SELECT COUNT(*) FROM LoadOrder WHERE LoadOrderNumber = @Number AND TenantId = @TenantId AND Id <> @Id";
           
            var parameters = new Dictionary<string, object>
                                 {
                                     {"Number", loadOrder.LoadOrderNumber},
                                     {"TenantId", loadOrder.TenantId},
                                     {"Id", loadOrder.Id},

                                 };
            var used = ExecuteScalar(query, parameters).ToInt() > 0;
            if (used)
                Messages.Add(ValidationMessage.Error("Load order number is already in use."));
            return used;
        }

        public bool HasAllRequiredData(LoadOrder loadOrder)
        {
            var valid = true;

            if (loadOrder.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Load order requires a tenant association"));
                valid = false;
            }

            if (loadOrder.Customer == null)
            {
                Messages.Add(ValidationMessage.Error("Load order requires a customer association"));
                valid = false;
            }

            if (loadOrder.Origin != null)
            {
                if (!HasValidLocationInformation(new[] { loadOrder.Origin }))
                {
                    Messages.Add(ValidationMessage.Error("Load order origin location has some invalid data"));
                    valid = false;
                }
            }

            if (loadOrder.Destination != null)
            {
                if (!HasValidLocationInformation(new[] { loadOrder.Destination }))
                {
                    Messages.Add(ValidationMessage.Error("Load order destination location has some invalid data"));
                    valid = false;
                }
            }

            if (string.IsNullOrEmpty(loadOrder.LoadOrderNumber))
            {
                Messages.Add(ValidationMessage.Error("Load order requires a load order number"));
                valid = false;
            }
            else if (loadOrder.LoadOrderNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Load order number cannot exceed 50 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(loadOrder.Description) && loadOrder.Description.Length > 250)
			{
				Messages.Add(ValidationMessage.Error("Load order description cannot exceed 250 characters"));
				valid = false;
			}

            if (!loadOrder.ServiceMode.ToInt().EnumValueIsValid<ServiceMode>())
            {
                Messages.Add(ValidationMessage.Error("Load order must have a valid service mode"));
                valid = false;
            }

            if (!loadOrder.DesiredPickupDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Load Order desired pickup date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!loadOrder.EstimatedDeliveryDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Load order estimated delivery date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!loadOrder.Status.ToInt().EnumValueIsValid<LoadOrderStatus>())
            {
                Messages.Add(ValidationMessage.Error("Load order must have a valid status"));
                valid = false;
            }

            if (loadOrder.User == null)
            {
                Messages.Add(ValidationMessage.Error("Load order requires a user association"));
                valid = false;
            }

            if (!loadOrder.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Load order date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (loadOrder.HazardousMaterial && !loadOrder.Items.Any(i => i.HazardousMaterial))
            {
                Messages.Add(ValidationMessage.Error("Load Order cannot be marked as Hazmat if no items are marked as Hazmat"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactName) &&
                loadOrder.HazardousMaterialContactName.Length > 50)
            {
                Messages.Add(
                    ValidationMessage.Error("Load order hazardous material contact name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactPhone) &&
                loadOrder.HazardousMaterialContactPhone.Length > 50)
            {
                Messages.Add(
                    ValidationMessage.Error("Load order hazardous material contact phone cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactMobile) &&
                loadOrder.HazardousMaterialContactMobile.Length > 50)
            {
                Messages.Add(
                    ValidationMessage.Error("Load order hazardous material contact mobile cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactEmail) &&
                loadOrder.HazardousMaterialContactEmail.Length > 100)
            {
                Messages.Add(
                    ValidationMessage.Error("Load order hazardous material contact email cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.HazardousMaterialContactEmail) && !loadOrder.HazardousMaterialContactEmail.EmailIsValid())
            {
                Messages.Add(ValidationMessage.Error("Load order hazardous material contact email is invalid"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.PurchaseOrderNumber) && loadOrder.PurchaseOrderNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Load order purchase order number cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.ShipperReference) && loadOrder.ShipperReference.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Load order shipper reference cannot exceed 50 characters"));
                valid = false;
            }

            if (loadOrder.Vendors.Any())
            {
                if (!LoadOrderVendorsAreValid(loadOrder.Vendors)) valid = false;
            }

            if (loadOrder.AccountBuckets.Any())
            {
                if (!LoadOrderAccountBucketsAreValid(loadOrder.AccountBuckets)) valid = false;
            }

            if (loadOrder.SalesRepresentativeCommissionPercent < 0)
            {
                Messages.Add(
                    ValidationMessage.Error("Load order sales representative commission percent must be greater than zero"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.GeneralBolComments) && loadOrder.GeneralBolComments.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("General BOL comments cannot exceed 200 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.CriticalBolComments) && loadOrder.CriticalBolComments.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Critical BOL comments cannot exceed 200 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.MiscField1) && loadOrder.MiscField1.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Load order misc. field 1 cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(loadOrder.MiscField2) && loadOrder.MiscField2.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Load order misc. field 2 cannot exceed 100 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(loadOrder.DriverName) && loadOrder.DriverName.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Load order driver name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(loadOrder.DriverPhoneNumber) && loadOrder.DriverPhoneNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Load order driver phone number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(loadOrder.DriverTrailerNumber) && loadOrder.DriverTrailerNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Load order driver trailer number cannot exceed 50 characters"));
				valid = false;
			}

            if (loadOrder.Notes.Count > 0 && !LoadOrderNotesAreValid(loadOrder.Notes)) valid = false;
            if (loadOrder.Charges.Count > 0 && !LoadOrderChargesAreValid(loadOrder.Charges)) valid = false;
            if (loadOrder.Services.Count > 0 && !LoadOrderServicesAreValid(loadOrder.Services)) valid = false;

			if (loadOrder.Equipments.Count > 0)
			{
				if (loadOrder.Equipments.Count() != 1)
				{
					Messages.Add(ValidationMessage.Error("Load order can utilize one, and only one, equipment at a time"));
					valid = false;
				}
				if (!LoadOrderEquipmentsAreValid(loadOrder.Equipments)) valid = false;
			}

			var lastStopIndex = loadOrder.Destination == null ? default(long) : loadOrder.Destination.StopOrder;

            if (loadOrder.Items.Count > 0 && !LoadOrderItemsAreValid(loadOrder.Items, lastStopIndex))valid = false;
            if (loadOrder.CustomerReferences.Count > 0 &&
                !LoadOrderReferencesAreValid(loadOrder.CustomerReferences)) valid = false;
            if (loadOrder.Stops.Count > 0 && !HasValidLocationInformation(loadOrder.Stops)) valid = false;
            if (loadOrder.Documents.Count > 0 && !LoadOrderDocumentsAreValid(loadOrder.Documents)) valid = false;
            return valid;
        }

        
        
        private bool HasValidLocationInformation(IEnumerable<LoadOrderLocation> locations)
        {
            var valid = true;

            foreach (var location in locations)
            {
                if (!string.IsNullOrEmpty(location.Description) && location.Description.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order location Description cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.Street1) && location.Street1.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order location Street1 cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.Street2) && location.Street2.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order location Street2 cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.City) && location.City.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order location City cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.State) && location.State.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order location State cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.PostalCode) && location.PostalCode.Length > 10)
                {
                    Messages.Add(ValidationMessage.Error("Load order location Postal Code cannot exceed 10 characters"));
                    valid = false;

					
                }
				if (!string.IsNullOrEmpty(location.PostalCode) && location.Country != null && !string.IsNullOrEmpty(location.Country.PostalCodeValidation)
						&& !location.Country.IsValidPostalCodeFormat(location.PostalCode))
				{
					Messages.Add(ValidationMessage.Error("Load order location Postal Code is not in required format for country {0}. Expected Format: {1}",
														 location.Country.Name, location.Country.PostalCodeValidation));
					valid = false;
				}
				

                if (!string.IsNullOrEmpty(location.SpecialInstructions) && location.SpecialInstructions.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Load order location special instruction cannot exceed 500 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.GeneralInfo) && location.GeneralInfo.Length > 2000)
                {
                    Messages.Add(ValidationMessage.Error("Load order location general info cannot exceed 2000 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.Direction) && location.Direction.Length > 2000)
                {
                    Messages.Add(ValidationMessage.Error("Load order location direction cannot exceed 2000 characters"));
                    valid = false;
                }

				if (location.MilesFromPreceedingStop < 0)
				{
					Messages.Add(ValidationMessage.Error("Load order location miles from preceeding stop cannot be less than zero"));
					valid = false;
				}

                if (location.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order location requires a tenant association"));
                    valid = false;
                }

                if (location.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order location requires a load order association"));
                    valid = false;
                }

                if (!location.AppointmentDateTime.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Load order location appointment date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                    valid = false;
                }

                if (location.Contacts.Count <= 0) continue;
                if (!HasValidLocationContactInformation(location.Contacts)) valid = false;
            }

            return valid;
        }

        private bool HasValidLocationContactInformation(IEnumerable<LoadOrderContact> contacts)
        {
            var valid = true;

            foreach (var contact in contacts)
            {
                if (contact.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order contact requires a tenant association"));
                    valid = false;
                }

                if (contact.Location == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order contact requires a load order location association"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Name) && contact.Name.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order contact name cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Phone) && contact.Phone.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order contact phone cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Mobile) && contact.Mobile.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order contact mobile cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Fax) && contact.Fax.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order contact fax cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Email) && contact.Email.Length > 100)
                {
                    Messages.Add(ValidationMessage.Error("Load order contact email cannot exceed 100 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Email) && !contact.Email.EmailIsValid())
                {
                    Messages.Add(ValidationMessage.Error("Load Order contact email is invalid"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Comment) && contact.Comment.Length > 200)
                {
                    Messages.Add(ValidationMessage.Error("Load order contact comment cannot exceed 200 characters"));
                    valid = false;
                }
            }

            return valid;
        }


        private bool LoadOrderVendorsAreValid(IEnumerable<LoadOrderVendor> vendors)
        {
            var valid = true;

            foreach (var vendor in vendors)
            {
                if (vendor.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order vendor requires a tenant association"));
                    valid = false;
                }

                if (vendor.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order vendor requires a load order association"));
                    valid = false;
                }

                if (vendor.VendorId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order vendor requires a vendor association"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(vendor.ProNumber) && vendor.ProNumber.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order vendor pro number cannot exceed 50 characters"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool LoadOrderAccountBucketsAreValid(IEnumerable<LoadOrderAccountBucket> accountBuckets)
        {
            var valid = true;

            foreach (var bucket in accountBuckets)
            {
                if (bucket.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order account bucket requires a tenant association"));
                    valid = false;
                }

                if (bucket.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order account bucket requires a load order association"));
                    valid = false;
                }

                if (bucket.AccountBucket == null)
                {
                    Messages.Add(
                        ValidationMessage.Error("Load order account bucket requires an account bucket association"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool LoadOrderNotesAreValid(IEnumerable<LoadOrderNote> notes)
        {
            var valid = true;

            foreach (var note in notes)
            {
                if (note.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order note requires a tenant association"));
                    valid = false;
                }

                if (note.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order note requires a load order association"));
                    valid = false;
                }

                if (note.User == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order note requires a user association"));
                    valid = false;
                }

                if (note.Message.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Load order note message cannot exceed 500 characters"));
                    valid = false;
                }

                if (!note.Type.ToInt().EnumValueIsValid<NoteType>())
                {
                    Messages.Add(ValidationMessage.Error("Load order note must have a valid type"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool LoadOrderChargesAreValid(IEnumerable<LoadOrderCharge> charges)
        {
            var valid = true;

            if (charges.Sum(c => c.UnitSell) < 0)
            {
                Messages.Add(ValidationMessage.Error("Total of load order charges unit sell must be greater than or equal to zero"));
                valid = false;
            }

            if (charges.Sum(c => c.UnitBuy) < 0)
            {
                Messages.Add(ValidationMessage.Error("Total of load order charges unit buy must be greater than or equal to zero"));
                valid = false;
            }

            foreach (var charge in charges)
            {
                if (charge.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order charge requires a tenant association"));
                    valid = false;
                }

                if (charge.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order charge requires a load order association"));
                    valid = false;
                }

                if (charge.ChargeCodeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order charge requires a charge code association"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(charge.Comment) && charge.Comment.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order charge comment cannot exceed 50 characters"));
                    valid = false;
                }

                if (charge.Quantity < 0)
                {
                    Messages.Add(ValidationMessage.Error("Load order charge quantity must be greater than  or equal to zero"));
                    valid = false;
                }

                if (charge.UnitDiscount < 0)
                {
                    Messages.Add(
                        ValidationMessage.Error("Load order charge unit discount must be greater than or equal to zero"));
                    valid = false;
                }

            }

            return valid;
        }

        private bool LoadOrderServicesAreValid(IEnumerable<LoadOrderService> services)
        {
            var valid = true;

            foreach (var service in services)
            {
                if (service.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order service requires a tenant association"));
                    valid = false;
                }

                if (service.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order service requires a load order association"));
                    valid = false;
                }

                if (service.ServiceId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order service requires a service association"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool LoadOrderEquipmentsAreValid(IEnumerable<LoadOrderEquipment> equipments)
        {
            var valid = true;

            foreach (var equipment in equipments)
            {
                if (equipment.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order equipment requires a tenant association"));
                    valid = false;
                }

                if (equipment.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order equipment requires a load order association"));
                    valid = false;
                }

                if (equipment.EquipmentTypeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order equipment requires an equipment association"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool LoadOrderItemsAreValid(IEnumerable<LoadOrderItem> items, long lastStopIndex)
        {
            var valid = true;

            foreach (var item in items)
            {
                if (item.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order item requires a tenant association"));
                    valid = false;
                }

                if (item.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order item requires a load order association"));
                    valid = false;
                }

				if (item.PackageTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Load order requires a package type association"));
					valid = false;
				}

				if (item.Pickup < 0 || item.Pickup >= lastStopIndex)
				{
					Messages.Add(ValidationMessage.Error("Load order item pickup location index is invalid"));
					valid = false;
				}

				if (item.Delivery <= 0 || item.Delivery > lastStopIndex)
				{
					Messages.Add(ValidationMessage.Error("Load order item delivery location index is invalid"));
					valid = false;
				}

                if (item.Description.Length > 200)
                {
                    Messages.Add(ValidationMessage.Error("Load order item description cannot exceed 200 characters"));
                    valid = false;
                }

                if (item.PieceCount < 0)
                {
                    Messages.Add(ValidationMessage.Error("Load order item must have a piece count of zero or greater"));
                    valid = false;
                }

                if (item.Quantity < 0)
                {
                    Messages.Add(ValidationMessage.Error("Load order item must have quantity of zero or greater"));
                    valid = false;
                }


                if (item.Weight < 0 || item.Weight > 99999999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Load order item weight must be greater than or equal to zero and less than 100000000"));
                    valid = false;
                }

                if (item.Length < 0 || item.Length > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Load order item length must be greater than or equal to zero and less than 10000"));
                    valid = false;
                }

                if (item.Width < 0 || item.Width > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Load order actual item width must be greater than or equal to zero and less than 10000"));
                    valid = false;
                }
                if (item.Height < 0 || item.Height > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Load order actual item height must be greater than or equal to zero and less than 10000"));
                    valid = false;
                }

                if (item.Value < 0)
                {
                    Messages.Add(ValidationMessage.Error("Load order item value must be greater than zero"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(item.NMFCCode) && item.NMFCCode.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order item NMFC code cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(item.HTSCode) && item.HTSCode.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order item HTS code cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(item.Comment) && item.Comment.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order item comment cannot exceed 50 characters"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool LoadOrderReferencesAreValid(IEnumerable<LoadOrderReference> customerReferences)
        {
            var valid = true;

            foreach (var reference in customerReferences)
            {
                if (reference.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order reference requires a tenant association"));
                    valid = false;
                }

                if (reference.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order reference requires a load order association"));
                    valid = false;
                }


                if (!string.IsNullOrEmpty(reference.Name) && reference.Name.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order reference name cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(reference.Value) && reference.Value.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order reference value cannot exceed 50 characters"));
                    valid = false;
                }
            }

            return valid;
        }

        public bool LoadOrderDocumentsAreValid(IEnumerable<LoadOrderDocument> documents)
        {
            var valid = true;

            foreach (var document in documents)
            {
                if (document.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Load order document requires a tenant association"));
                    valid = false;
                }

                if (document.LoadOrder == null)
                {
                    Messages.Add(ValidationMessage.Error("Load order document requires a load order association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(document.Name))
                {
                    Messages.Add(ValidationMessage.Error("Load order document requires a name"));
                    valid = false;
                }
                else if (document.Name.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Load order document name cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(document.Description) &&  document.Description.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Load order document description cannot exceed 500 characters"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(document.LocationPath))
                {
                    Messages.Add(ValidationMessage.Error("Load order document requires a location path"));
                    valid = false;
                }
                else if (document.LocationPath.Length > 200)
                {
                    Messages.Add(ValidationMessage.Error("Load order document location path cannot exceed 200 characters"));
                    valid = false;
                }
            }
            return valid;
        }


    }
}