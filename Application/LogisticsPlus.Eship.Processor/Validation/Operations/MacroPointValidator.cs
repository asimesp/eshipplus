﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class MacroPointOrderValidator : ValidatorBase
    {
        public bool IsValid(MacroPointOrder order)
        {
	        bool valid = HasAllRequiredData(order);

	        if (DuplicateIdNumber(order)) valid = false;
            if (DuplicateOrderId(order)) valid = false;

            return valid;
        }

        public bool DuplicateIdNumber(MacroPointOrder order)
        {
            const string query = @"SELECT COUNT(*) FROM MacroPointOrderViewSearch WHERE IdNumber = @IdNumber AND TenantId = @TenantId AND Id <> @Id";
            var parameters = new Dictionary<string, object>
                                 {
                                     {"IdNumber", order.IdNumber},
                                     {"TenantId", order.TenantId},
                                     {"Id", order.Id}
                                 };
            var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateNumber) Messages.Add(ValidationMessage.Error("Id Number is already in use"));
            return duplicateNumber;
        }

        public bool DuplicateOrderId(MacroPointOrder order)
        {
            const string query = @"SELECT COUNT(*) FROM MacroPointOrderViewSearch WHERE OrderId = @OrderId AND TenantId = @TenantId AND Id <> @Id";
            var parameters = new Dictionary<string, object>
                                 {
                                     {"OrderId", order.OrderId},
                                     {"TenantId", order.TenantId},
                                     {"Id", order.Id}
                                 };
            var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateNumber) Messages.Add(ValidationMessage.Error("Order Id is already in use"));
            return duplicateNumber;
        }

        public bool HasAllRequiredData(MacroPointOrder order)
        {
            var valid = true;

            if (order.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order requires a Tenant association"));
                valid = false;
            }
            if (order.User == null)
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order requires a User association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(order.IdNumber))
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order requires an Id Number"));
                valid = false;
            }
            else if (order.IdNumber.Length > 50)
            {
                Messages.Add(
                    ValidationMessage.Error("Macro Point id number cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(order.OrderId))
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order requires an Order Id"));
                valid = false;
            }
            else if (order.OrderId.Length > 50)
            {
                Messages.Add(
                    ValidationMessage.Error("Macro Point Order order id cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(order.TrackingStatusCode) && order.TrackingStatusCode.Length > 10)
            {
                Messages.Add(
                    ValidationMessage.Error("Macro Point Order tracking status code cannot exceed 10 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(order.TrackingStatusDescription) && order.TrackingStatusDescription.Length > 50)
            {
                Messages.Add(
                    ValidationMessage.Error("Macro Point Order tracking status description cannot exceed 50 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(order.EmailCopiesOfUpdatesTo) && order.EmailCopiesOfUpdatesTo.Length > 500)
			{
				Messages.Add(
					ValidationMessage.Error("Macro Point Order email copies of update to cannot exceed 500 characters"));
				valid = false;
			}

            if (!string.IsNullOrEmpty(order.EmailCopiesOfUpdatesTo) && order.EmailCopiesOfUpdatesTo.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order email copies of update to contains one or more invalid addresses"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(order.Notes) && order.Notes.Length > 500)
			{
				Messages.Add(
					ValidationMessage.Error("Macro Point Order notes cannot exceed 500 characters"));
				valid = false;
			}

            if (string.IsNullOrEmpty(order.NumberType))
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order requires a number type"));
                valid = false;
            }
            else if (order.NumberType.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order number type cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(order.Number))
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order requires a number"));
                valid = false;
            }
            else if (order.Number.Length > 50)
            {
                Messages.Add(
                    ValidationMessage.Error("Macro Point Order number cannot exceed 50 characters"));
                valid = false;
            }

            if (!order.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }
            if (!order.DateStopRequested.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order date stop requested must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }
            if (!order.StartTrackDateTime.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Macro Point Order start track date time must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (order.TrackCost < 0 || order.TrackCost > 99999999999999.9999m)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Macro Point Order track cost must be greater than or equal to zero and less than 100000000000000"));
                valid = false;
            }

            return valid;
        }
    }
}
