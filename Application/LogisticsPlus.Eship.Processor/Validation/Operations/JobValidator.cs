﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
	public class JobValidator : ValidatorBase
	{

		public bool IsValid(Job job)
		{
			var valid = HasAllRequiredData(job);
			if (DuplicateJobNumber(job)) valid = false;
			return valid;
		}

		public bool DuplicateJobNumber(Job job)
		{
			const string query = "SELECT COUNT(*) FROM Job WHERE JobNumber = @JobNumber AND TenantId = @TenantId AND Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"JobNumber", job.JobNumber},
									{"TenantId", job.TenantId},
			                 		{"Id", job.Id}
			                 	};
			var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateNumber) Messages.Add(ValidationMessage.Error("Job number is already in use"));
			return duplicateNumber;
		}

		public bool HasAllRequiredData(Job job)
		{
			var valid = true;

			if (string.IsNullOrEmpty(job.JobNumber))
			{
				Messages.Add(ValidationMessage.Error("Job requires a Job number"));
				valid = false;
			}
			else if (job.JobNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Job number cannot exceed 50 characters"));
				valid = false;
			}

			if (!job.Status.ToInt().EnumValueIsValid<JobStatus>())
			{
				Messages.Add(ValidationMessage.Error("Job must have a valid status"));
				valid = false;
			}

			if (!job.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Job date created must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime,
													 DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (job.CreatedByUser == null)
			{
				Messages.Add(ValidationMessage.Error("Job requires a user association"));
				valid = false;
			}

			if (job.Customer == null)
			{
				Messages.Add(ValidationMessage.Error("Job requires a customer association"));
				valid = false;
			}

			if (job.Tenant == null)
			{
				Messages.Add(ValidationMessage.Error("Job requires a tenant association"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(job.ExternalReference1) && job.ExternalReference1.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Job external reference 1 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(job.ExternalReference2) && job.ExternalReference2.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Job external reference 2 cannot exceed 50 characters"));
				valid = false;
			}


			return valid;
		}

		public bool CanDeleteJob(Job job)
		{
			const string query = "select dbo.JobIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", job.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Job is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}
