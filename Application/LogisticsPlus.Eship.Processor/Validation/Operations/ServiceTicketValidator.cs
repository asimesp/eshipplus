﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class ServiceTicketValidator : ValidatorBase
    {
        public bool ServiceTicketServiceExists(ServiceTicketService serviceTicketService)
        {
            const string query =
                "Select count(*) from ServiceTicketService where ServiceId = @ServiceId and ServiceTicketId = @ServiceTicketId and TenantId = @TenantId";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"ServiceId", serviceTicketService.ServiceId},
			                 		{"TenantId", serviceTicketService.TenantId},
			                 		{"ServiceTicketId", serviceTicketService.ServiceTicketId}
			                 	};
            var duplicateService = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateService)
                Messages.Add(ValidationMessage.Error("Service is already included in Service Ticket Services"));
            return duplicateService;
        }

        public bool ServiceTicketEquipmentExists(ServiceTicketEquipment serviceTicketEquipment)
        {
            const string query =
                "Select count(*) from ServiceTicketEquipment where EquipmentTypeId = @EquipmentTypeId and ServiceTicketId = @ServiceTicketId and TenantId = @TenantId";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"EquipmentTypeId", serviceTicketEquipment.EquipmentTypeId},
			                 		{"TenantId", serviceTicketEquipment.TenantId},
			                 		{"ServiceTicketId", serviceTicketEquipment.ServiceTicketId}
			                 	};
            var duplicateService = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateService)
                Messages.Add(ValidationMessage.Error("Equipment type is already included in Service Ticket Equipments"));
            return duplicateService;
        }

        public bool CanDeleteServiceTicket(ServiceTicket serviceTicket)
        {
            const string query = "select dbo.ServiceTicketIdUsageCount(@Id)";
            var parameters = new Dictionary<string, object> { { "Id", serviceTicket.Id } };
            var used = ExecuteScalar(query, parameters).ToInt() > 0;
            if (used) Messages.Add(ValidationMessage.Error("Service Ticket is referenced one or more times and cannot be deleted."));
            return !used;
        }


        public bool IsValid(ServiceTicket serviceTicket)
        {
	        bool valid = HasAllRequiredData(serviceTicket);

	        if (DuplicateServiceTicketNumber(serviceTicket)) valid = false;

            return valid;
        }


        public bool DuplicateServiceTicketNumber(ServiceTicket serviceTicket)
        {
            const string query = "SELECT COUNT(*) FROM ServiceTicket WHERE ServiceTicketNumber = @ServiceTicketNumber AND TenantId = @TenantId AND Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"ServiceTicketNumber", serviceTicket.ServiceTicketNumber},
			                 		{"TenantId", serviceTicket.TenantId},
			                 		{"Id", serviceTicket.Id}
			                 	};
            var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateNumber) Messages.Add(ValidationMessage.Error("Service Ticket number is already in use"));
            return duplicateNumber;
        }


        public bool HasAllRequiredData(ServiceTicket serviceTicket)
        {
            var valid = true;

            if (serviceTicket.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Service Ticket requires a tenant association"));
                valid = false;
            }

            if (serviceTicket.Customer == null)
            {
                Messages.Add(ValidationMessage.Error("Service Ticket requires a customer association"));
                valid = false;
            }

			if (serviceTicket.OverrideCustomerLocation != null && serviceTicket.OverrideCustomerLocation.Customer.Id != serviceTicket.Customer.Id)
	        {
		        Messages.Add(ValidationMessage.Error("Service Ticket has invalid customer to customer association"));
				valid = false;
			}

            if (serviceTicket.User == null)
            {
                Messages.Add(ValidationMessage.Error("Service Ticket requires a user association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(serviceTicket.ServiceTicketNumber))
            {
                Messages.Add(ValidationMessage.Error("Service Ticket requires a Service Ticket number"));
                valid = false;
            }
            else if (serviceTicket.ServiceTicketNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Service Ticket number cannot exceed 50 characters"));
                valid = false;
            }

            if (!serviceTicket.Status.ToInt().EnumValueIsValid<ServiceTicketStatus>())
            {
                Messages.Add(ValidationMessage.Error("Service Ticket must have a valid status"));
                valid = false;
            }

            if (!serviceTicket.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Service Ticket date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

			if (!string.IsNullOrEmpty(serviceTicket.ExternalReference1) && serviceTicket.ExternalReference1.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Service Ticket external reference 1 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(serviceTicket.ExternalReference2) && serviceTicket.ExternalReference2.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Service Ticket external reference 2 cannot exceed 50 characters"));
				valid = false;
			}

            if (!serviceTicket.Vendors.Any())
            {
				Messages.Add(ValidationMessage.Error("Service Ticket must have at least one vendor"));
                valid = false;
            }
            else
            {
                if (!ServiceTicketVendorsAreValid(serviceTicket.Vendors)) valid = false;

                if (serviceTicket.Vendors.Count(v => v.Primary) != 1)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket should have one (and only one) primary vendor"));
                    valid = false;
                }
            }

            if (serviceTicket.BillReseller && serviceTicket.ResellerAddition == null)
            {
                Messages.Add(ValidationMessage.Error("Service Ticket must have reseller addition association"));
                valid = false;
            }

            if (serviceTicket.Notes.Count > 0 && !ServiceTicketNotesAreValid(serviceTicket.Notes)) valid = false;
            if (serviceTicket.Charges.Count > 0 && !ServiceTicketChargesAreValid(serviceTicket.Charges)) valid = false;
            if (serviceTicket.Services.Count > 0 && !ServiceTicketServicesAreValid(serviceTicket.Services)) valid = false;
            if (serviceTicket.Equipments.Count > 0 && !ServiceTicketEquipmentsAreValid(serviceTicket.Equipments)) valid = false;
            if (serviceTicket.Items.Count > 0 && !ServiceTicketItemsAreValid(serviceTicket.Items)) valid = false;
			if (serviceTicket.Documents.Count > 0 && !ServiceTicketDocumentsAreValid(serviceTicket.Documents)) valid = false;
            if (serviceTicket.Assets.Count > 0)
            {
                if (!ServiceTicketAssetsAreValid(serviceTicket.Assets)) valid = false;
                if (serviceTicket.Assets.Count(a => a.Primary) != 1)
                {
                    Messages.Add(ValidationMessage.Error("Service ticket assets should have one (and only one) primary asset"));
                    valid = false;
                }
            }
            return valid;
        }

        private bool ServiceTicketItemsAreValid(IEnumerable<ServiceTicketItem> items)
        {
            var valid = true;

            foreach (var item in items)
            {
                if (item.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket item requires a tenant association"));
                    valid = false;
                }

                if (item.ServiceTicket == null)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket item requires a Service Ticket association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(item.Description))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket item must have a description"));
                    valid = false;
                }
                else if (item.Description.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket item description cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(item.Comment) && item.Comment.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket item comment cannot exceed 50 characters"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ServiceTicketEquipmentsAreValid(IEnumerable<ServiceTicketEquipment> equipments)
        {
            var valid = true;

            foreach (var equipment in equipments)
            {
                if (equipment.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket equipment requires a tenant association"));
                    valid = false;
                }

                if (equipment.ServiceTicket == null)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket equipment requires a service ticket association"));
                    valid = false;
                }

                if (equipment.EquipmentTypeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket equipment requires an equipment association"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ServiceTicketServicesAreValid(IEnumerable<ServiceTicketService> services)
        {
            var valid = true;

            foreach (var service in services)
            {
                if (service.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket service requires a tenant association"));
                    valid = false;
                }

                if (service.ServiceTicket == null)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket service requires a Service Ticket association"));
                    valid = false;
                }

                if (service.ServiceId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket service requires a service association"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ServiceTicketVendorsAreValid(IEnumerable<ServiceTicketVendor> vendors)
        {
            var valid = true;

            foreach (var vendor in vendors)
            {
                if (vendor.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket vendor requires a tenant association"));
                    valid = false;
                }

                if (vendor.ServiceTicket == null)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket vendor requires a service ticket association"));
                    valid = false;
                }

                if (vendor.Vendor == null)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket vendor requires a service association"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ServiceTicketChargesAreValid(IEnumerable<ServiceTicketCharge> charges)
        {
            var valid = true;

            foreach (var charge in charges)
            {
                if (charge.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket charge requires a tenant association"));
                    valid = false;
                }

                if (charge.ServiceTicket == null)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket charge requires a Service Ticket association"));
                    valid = false;
                }

                if (charge.ChargeCodeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket charge requires a charge code association"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(charge.Comment) && charge.Comment.Length > 100)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket charge comment cannot exceed 100 characters"));
                    valid = false;
                }

                if (charge.Quantity <= 0)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket charge quantity must be greater than zero"));
                    valid = false;
                }

                if (charge.UnitBuy < 0)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket charge unit buy must be greater than or equal to zero"));
                    valid = false;
                }

                if (charge.UnitSell < 0)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket charge unit sell must be greater than or equal to zero"));
                    valid = false;
                }

                if (charge.UnitDiscount < 0)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket charge unit discount must be greater than or equal to zero"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ServiceTicketNotesAreValid(IEnumerable<ServiceTicketNote> notes)
        {
            var valid = true;

            foreach (var note in notes)
            {
                if (note.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket note requires a tenant association"));
                    valid = false;
                }

                if (note.ServiceTicket == null)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket note requires a Service Ticket association"));
                    valid = false;
                }

                if (note.User == null)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket note requires a user association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(note.Message))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket note requires a message"));
                    valid = false;
                }
                else if (note.Message.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket note message cannot exceed 500 characters"));
                    valid = false;
                }

                if (!note.Type.ToInt().EnumValueIsValid<NoteType>())
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket note must have a valid type"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ServiceTicketAssetsAreValid(IEnumerable<ServiceTicketAsset> assets)
        {
            var valid = true;

            foreach (var asset in assets)
            {
                if (asset.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket asset requires a tenant association"));
                    valid = false;
                }

                if (asset.ServiceTicket == null)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket asset requires a service ticket association"));
                    valid = false;
                }

                if (asset.DriverAssetId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket asset requires a driver asset association"));
                    valid = false;
                }

                if (asset.MilesRun <= 0)
                {
                    Messages.Add(ValidationMessage.Error("Service Ticket asset miles run must be greater than zero"));
                    valid = false;
                }
            }

            return valid;
        }

		public bool ServiceTicketDocumentsAreValid(IEnumerable<ServiceTicketDocument> documents)
		{
			var valid = true;

			foreach (var document in documents)
			{
				if (document.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Service Ticket document requires a tenant association"));
					valid = false;
				}

				if (document.ServiceTicket == null)
				{
					Messages.Add(ValidationMessage.Error("Service Ticket document requires a ServiceTicket association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Name))
				{
					Messages.Add(ValidationMessage.Error("Service Ticket document requires a name"));
					valid = false;
				}
				else if (document.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Service Ticket document name cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Description))
				{
					Messages.Add(ValidationMessage.Error("Service Ticket document requires a description"));
					valid = false;
				}
				else if (document.Description.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Service Ticket document description cannot exceed 500 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.LocationPath))
				{
					Messages.Add(ValidationMessage.Error("Service Ticket document requires a location path"));
					valid = false;
				}
				else if (document.LocationPath.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Service Ticket document location path cannot exceed 200 characters"));
					valid = false;
				}
			}
			return valid;
		}
    }
}
