﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class Project44ResponseValidator : ValidatorBase
    {
		public bool IsValid(Project44TrackingResponse smc)
		{
			bool valid = HasAllRequiredData(smc);

			return valid;
		}

        public bool IsValid(Project44DispatchResponse smc)
        {
            bool valid = HasAllRequiredData(smc);

            return valid;
        }

        private bool HasAllRequiredData(Project44Response p44Response)
        {
            var valid = true;

            if (string.IsNullOrEmpty(p44Response.Project44Id))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Request Response requires a Transaction Id"));
                valid = false;
            }
            else if (p44Response.Project44Id.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Transaction Id cannot exceed 200 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(p44Response.ResponseData))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Request Response requires Response Data"));
                valid = false;
            }

            if (string.IsNullOrEmpty(p44Response.Scac))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Request Response requires Scac"));
                valid = false;
            }
            else if (p44Response.Scac.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Scac cannot exceed 10 characters"));
                valid = false;
            }


            if (!p44Response.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("SMC3 Data date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (p44Response.ShipmentId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Shipment Id required"));
                valid = false;
            }
            
            return valid;
        }

        
    }
}
