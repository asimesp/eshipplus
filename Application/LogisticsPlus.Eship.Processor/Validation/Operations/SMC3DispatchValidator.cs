﻿using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class SMC3DispatchDataValidator : ValidatorBase
    {
        public bool IsValid(SMC3DispatchData smc)
        {
	        bool valid = HasAllRequiredData(smc);

	        return valid;
        }

        private bool HasAllRequiredData(SMC3DispatchData smcdd)
        {
            var valid = true;

            if (string.IsNullOrEmpty(smcdd.TransactionId))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Dispatch Data requires a Transaction Id"));
                valid = false;
            }
            else if (smcdd.TransactionId.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Transaction Id cannot exceed 200 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(smcdd.Scac))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Dispatch Data requires a SCAC"));
                valid = false;
            }
            else if (smcdd.Scac.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("SCAC cannot exceed 200 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(smcdd.Identifiers) && smcdd.Identifiers.Length > 1000)
            {
                Messages.Add(ValidationMessage.Error("Identifiers cannot exceed 1000 characters"));
                valid = false;
            }


            if (string.IsNullOrEmpty(smcdd.ResponseStatus))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Dispatch Data requires a Response Status"));
                valid = false;
            }
            else if (smcdd.ResponseStatus.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Response Status cannot exceed 50 characters"));
                valid = false;
            }
            
            if (string.IsNullOrEmpty(smcdd.ResponseCode))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Dispatch Data requires a Response Code"));
                valid = false;
            }
            else if (smcdd.ResponseCode.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Respond code cannot exceed 50 characters"));
                valid = false;
            }
            
            if (string.IsNullOrEmpty(smcdd.ResponseMessage))
            {
                Messages.Add(ValidationMessage.Error("SMC3 Dispatch Data requires a Response Message"));
                valid = false;
            }
            else if (smcdd.ResponseMessage.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Response Message cannot exceed 500 characters"));
                valid = false;
            }
                
            return valid;
        }
    }
}
