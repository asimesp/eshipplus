﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
	public class CustomerTLTenderingProfileValidator : ValidatorBase
	{
		public bool TenderingProfileVendorExists(CustomerTLTenderingProfileVendor tenderingProfileVendor)
		{
			const string query =
				"Select count(*) from CustomerTLTenderingProfileVendor where VendorId = @VendorId and TLTenderingProfileId = @TLTenderingProfileId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
				{
					{"VendorId", tenderingProfileVendor.VendorId},
					{"TLTenderingProfileId", tenderingProfileVendor.Id},
					{"TenantId", tenderingProfileVendor.TenantId}
				};
			bool duplicateVendor = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateVendor)
				Messages.Add(ValidationMessage.Error("Vendor is already included in Tender Profile Vendors"));
			return duplicateVendor;
		}

		public bool TenderingProfileAlreadyExistsForCustomer(CustomerTLTenderingProfile tenderingProfile)
		{
			const string query =
				"Select count(*) from CustomerTLTenderingProfile where CustomerId = @CustomerId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
				{
					{"CustomerId", tenderingProfile.CustomerId},
					{"TenantId", tenderingProfile.TenantId},
				};

			bool duplicateProfileForCustomer = ExecuteScalar(query, parameters).ToInt() > 0;

			return duplicateProfileForCustomer;
		}

		public bool IsValid(CustomerTLTenderingProfile profile)
		{
			bool valid = HasAllRequiredData(profile);

			return valid;
		}


		public bool HasAllRequiredData(CustomerTLTenderingProfile profile)
		{
			var valid = true;

			if (profile.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tendering Profile requires a tenant association"));
				valid = false;
			}

			if (profile.Customer == null)
			{
				Messages.Add(ValidationMessage.Error("Tendering Profile requires a customer association"));
				valid = false;
			}
			else if (profile.IsNew && TenderingProfileAlreadyExistsForCustomer(profile))
			{
				Messages.Add(ValidationMessage.Error("Tendering Profile already exists for this customer."));
				valid = false;
			}
			

			if (profile.MaxWaitTime <= 0)
			{
				Messages.Add(ValidationMessage.Error("Tendering Profile Max Wait Time must be greater than or equal to zero"));
				valid = false;
			}

			if (!VendorsAreValid(profile.Vendors)) valid = false;
			if (!LanesAreValid(profile.Lanes)) valid = false;

			// NOTE: Should we enforce this?
			if (profile.Lanes.Select(s => s.FullLane).Distinct().Count() != profile.Lanes.Count)
			{
				Messages.Add(ValidationMessage.Error("Tendering Profile cannot have duplicate lanes."));
				valid = false;
			}

			return valid;
		}

		private bool VendorsAreValid(IEnumerable<CustomerTLTenderingProfileVendor> vendors)
		{
			var valid = true;

			foreach (var vendor in vendors)
			{
				if (vendor.Vendor.IsNew)
				{
					Messages.Add(ValidationMessage.Error("Vendors for TL Tendering Profile cannot be new", vendor.Vendor.VendorNumber));
					valid = false;
				}

				if (vendor.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor {0} requires a tenant assosciation", vendor.Vendor.VendorNumber));
					valid = false;
				}

				if (vendor.TLTenderingProfile == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor {0} requires a TL Tendering Profile Assosication",
					                                     vendor.Vendor.VendorNumber));
					valid = false;
				}

				if (vendor.VendorId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor {0} requires a Vendor Assosciation", vendor.Vendor.VendorNumber));
					valid = false;
				}

				if (vendor.CommunicationType == NotificationMethod.Edi &&
				    !vendor.Vendor.Communication.EdiEnabled)
				{
					Messages.Add(ValidationMessage.Error("Vendor {0} does not support EDI as a Communication Type",
					                                     vendor.Vendor.VendorNumber));
					valid = false;
				}
			}

			return valid;
		}

		private bool LanesAreValid(IEnumerable<CustomerTLTenderingProfileLane> lanes)
		{
			var valid = true;

			foreach (var lane in lanes)
			{
				if (lane.OriginPostalCode == string.Empty &&
				    (lane.OriginCity == string.Empty || lane.OriginState == string.Empty))
				{
					Messages.Add(ValidationMessage.Error("If you do not provide an Origin Postal code you must provide an Origin City and State for Lane {0}", lane.FullLane));
					valid = false;
				}

				if (lane.DestinationPostalCode == string.Empty &&
				    (lane.DestinationCity == string.Empty || lane.DestinationState == string.Empty))
				{
					Messages.Add(ValidationMessage.Error("If you do not provide a Destination Postal code you must provide an Destination City and State for Lane {0}",lane.FullLane));
					valid = false;
				}

				if (lane.OriginCountryId == default(long))
				{
					Messages.Add(ValidationMessage.Error("You must specify an Origin Country for Lane {0}", lane.FullLane));
					valid = false;
				}

				if (lane.DestinationCountryId == default(long))
				{
					Messages.Add(ValidationMessage.Error("You must specify a Destination Country for Lane {0}", lane.FullLane));
					valid = false;
				}

				if (lane.FirstPreferredVendorId != default(long) && lane.SecondPreferredVendorId == default(long) &&
				    lane.ThirdPreferredVendorId != default(long))
				{
					Messages.Add(ValidationMessage.Error("You must specify a Second Preferred Vendor for Lane {0}", lane.FullLane));
					valid = false;
				}

				if (lane.FirstPreferredVendorId == default(long) && (lane.SecondPreferredVendorId != default(long) ||
				                                                     lane.ThirdPreferredVendorId != default(long)))
				{
					Messages.Add(ValidationMessage.Error("You must specify a First Preferred Vendor for Lane {0}", lane.FullLane));
					valid = false;
				}

				//if (lane.TLTenderingProfile == null)
				//{
				//	Messages.Add(ValidationMessage.Error("Lane {0} requires a TL Tendering Profile Assosciation", lane.Index));
				//	valid = false;
				//}


				var vendorSearch = new VendorSearch();
				if (lane.FirstPreferredVendor != null)
				{
					if (new Vendor(vendorSearch.FetchVendorByNumber(lane.FirstPreferredVendor.VendorNumber, lane.TenantId).Id)
						.IsNew)
					{
						Messages.Add(ValidationMessage.Error("Invalid Vendor Number for First Preferred Vendor for Lane {0}",
						                                     lane.FullLane));
						valid = false;
					}
				}

				if (lane.SecondPreferredVendor != null)
				{
					if (new Vendor(vendorSearch.FetchVendorByNumber(lane.SecondPreferredVendor.VendorNumber, lane.TenantId).Id)
						.IsNew)
					{
						Messages.Add(ValidationMessage.Error("Invalid Vendor Number for Second Preferred Vendor for Lane {0}",
						                                     lane.FullLane));
						valid = false;
					}
				}

				if (lane.ThirdPreferredVendor != null)
				{
					if (new Vendor(vendorSearch.FetchVendorByNumber(lane.ThirdPreferredVendor.VendorNumber, lane.TenantId).Id)
						.IsNew)
					{
						Messages.Add(ValidationMessage.Error("Invalid Vendor Number for Third Preferred Vendor for Lane {0}",
						                                     lane.FullLane));
						valid = false;
					}
				}
			}

			return valid;
		}

		public bool CanDeleteCustomerTLProfile(CustomerTLTenderingProfile profile)
		{
			const string query = "select dbo.CustomerTLTenderingProfileUsageIdCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", profile.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used)
				Messages.Add(ValidationMessage.Error("CustomerTLTenderingProfile is referenced one or more times and cannot be deleted."));
			return !used;
		}

		public bool CanDeleteLane(long id)
		{
			const string query = "select dbo.CustomerTLTenderingProfileLaneUsageIdCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used)
				Messages.Add(ValidationMessage.Error("Lane with ID"+id+"referenced one or more times and cannot be deleted."));
			return !used;
		}
		
	}
}
