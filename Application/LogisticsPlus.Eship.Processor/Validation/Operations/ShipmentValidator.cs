﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Calculations;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Operations
{
    public class ShipmentValidator : ValidatorBase
    {
        public bool ShipmentServiceExists(ShipmentService shipmentService)
        {
            const string query =
                "Select count(*) from ShipmentService where ServiceId = @ServiceId and ShipmentId = @ShipmentId and TenantId = @TenantId";
            var parameters = new Dictionary<string, object>
                                 {
                                     {"ServiceId", shipmentService.ServiceId},
                                     {"TenantId", shipmentService.TenantId},
                                     {"ShipmentId", shipmentService.ShipmentId}
                                 };
            var duplicateService = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateService)
                Messages.Add(ValidationMessage.Error("Service is already included in Shipment Services"));
            return duplicateService;
        }

        public bool ShipmentEquipmentExists(ShipmentEquipment shipmentEquipment)
        {
            const string query =
                "Select count(*) from ShipmentEquipment where EquipmentTypeId = @EquipmentTypeId and ShipmentId = @ShipmentId and TenantId = @TenantId";
            var parameters = new Dictionary<string, object>
                                 {
                                     {"EquipmentTypeId", shipmentEquipment.EquipmentTypeId},
                                     {"TenantId", shipmentEquipment.TenantId},
                                     {"ShipmentId", shipmentEquipment.ShipmentId}
                                 };
            var duplicateEquipment = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateEquipment)
                Messages.Add(ValidationMessage.Error("Equipment type is already included in Shipment Equipments"));
            return duplicateEquipment;
        }


        public bool CanDeleteShipment(Shipment shipment)
        {
            const string query = "select dbo.ShipmentIdUsageCount(@Id)";
            var parameters = new Dictionary<string, object> { { "Id", shipment.Id } };
            var used = ExecuteScalar(query, parameters).ToInt() > 0;
            if (used)
                Messages.Add(ValidationMessage.Error("Shipment is referenced one or more times and cannot be deleted."));
            return !used;
        }


        public bool IsValid(Shipment shipment)
        {
            var valid = HasAllRequiredData(shipment);

            if (DuplicateShipmentNumber(shipment)) valid = false;

            return valid;
        }


        public bool DuplicateShipmentNumber(Shipment shipment)
        {
            var query = shipment.IsNew
                            ? @"SELECT 
								(SELECT COUNT(*) FROM Shipment WHERE ShipmentNumber = @Number AND TenantId = @TenantId AND Id <> @Id) +
								(SELECT COUNT(*) FROM LoadOrder WHERE LoadOrderNumber = @Number AND TenantId = @TenantId)"
                            : @"SELECT COUNT(*) FROM Shipment WHERE ShipmentNumber = @Number AND TenantId = @TenantId AND Id <> @Id";
            var parameters = new Dictionary<string, object>
                                 {
                                     {"Number", shipment.ShipmentNumber},
                                     {"TenantId", shipment.TenantId},
                                     {"Id", shipment.Id}
                                 };
            var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateNumber) Messages.Add(ValidationMessage.Error("Shipment number is already in use"));
            return duplicateNumber;
        }


        public bool HasAllRequiredData(Shipment shipment)
        {
            var valid = true;

            if (shipment.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Shipment requires a tenant association"));
                valid = false;
            }

            var customer = shipment.Customer;
            if (customer == null)
            {
                Messages.Add(ValidationMessage.Error("Shipment requires a customer association"));
                valid = false;
            }
            else
            {

                var wStatus = new[] { ShipmentStatus.Invoiced, ShipmentStatus.Void };
                if (customer.RequiredMileageSourceId != default(long) &&
                    customer.RequiredMileageSourceId != shipment.MileageSourceId &&
                    (shipment.ServiceMode == ServiceMode.Truckload || shipment.ServiceMode == ServiceMode.Rail))
                {
                    var msg = ValidationMessage.Error("Customer requires mileage from mileage source {0}-{1}",
                                                      customer.MileageSource.Code, customer.MileageSource.Description);
                    if (wStatus.Contains(shipment.Status)) msg.Type = ValidateMessageType.Warning;
                    else valid = false;
                    Messages.Add(msg);

                }

                if (customer.ShipmentRequiresPurchaseOrderNumber && string.IsNullOrEmpty(shipment.PurchaseOrderNumber))
                {
                    var msg = ValidationMessage.Error("Customer requires purchase order number on shipment");
                    if (wStatus.Contains(shipment.Status)) msg.Type = ValidateMessageType.Warning;
                    else valid = false;
                    Messages.Add(msg);
                }

                if (customer.InvoiceRequiresShipperReference && string.IsNullOrEmpty(shipment.ShipperReference))
                {
                    var msg = ValidationMessage.Error("Customer requires shipper reference number on shipment");
                    if (wStatus.Contains(shipment.Status)) msg.Type = ValidateMessageType.Warning;
                    else valid = false;
                    Messages.Add(msg);
                }
            }

            if (shipment.User == null)
            {
                Messages.Add(ValidationMessage.Error("Shipment requires a user association"));
                valid = false;
            }

            if (shipment.Origin == null)
            {
                Messages.Add(ValidationMessage.Error("Shipment requires an origin location"));
                valid = false;
            }
            else if (!HasValidLocationInformation(new[] { shipment.Origin }))
            {
                Messages.Add(ValidationMessage.Error("Shipment origin location has some invalid data"));
                valid = false;
            }

            if (shipment.Destination == null)
            {
                Messages.Add(ValidationMessage.Error("Shipment requires an destination location"));
                valid = false;
            }
            else if (!HasValidLocationInformation(new[] { shipment.Destination }))
            {
                Messages.Add(ValidationMessage.Error("Shipment destination location has some invalid data"));
                valid = false;
            }

            if (string.IsNullOrEmpty(shipment.ShipmentNumber))
            {
                Messages.Add(ValidationMessage.Error("Shipment requires a Shipment number"));
                valid = false;
            }
            else if (shipment.ShipmentNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Shipment number cannot exceed 50 characters"));
                valid = false;
            }

            if (!shipment.ServiceMode.ToInt().EnumValueIsValid<ServiceMode>())
            {
                Messages.Add(ValidationMessage.Error("Shipment must have a valid service mode"));
                valid = false;
            }
            else if (shipment.ServiceMode.ToInt().ToEnum<ServiceMode>() == ServiceMode.NotApplicable)
            {
                Messages.Add(ValidationMessage.Error("Shipment cannot have a service mode that is not applicable"));
                valid = false;
            }
            else
            {
                if (shipment.ServiceMode == ServiceMode.Truckload && shipment.ShipmentCoordinatorId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment requires a Shipment Coordinator "));
                    valid = false;
                }

                if (shipment.ServiceMode == ServiceMode.Truckload && shipment.CarrierCoordinatorId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment requires a Carrier Coordinator "));
                    valid = false;
                }
            }


            if (!shipment.DesiredPickupDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Shipment desired pickup date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime && shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime)
            {
                Messages.Add(ValidationMessage.Error("Shipment requires Actual Pickup Date before setting Actual Delivery Date",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!shipment.ActualPickupDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Shipment actual pickup date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!shipment.ActualDeliveryDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Shipment actual delivery date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!shipment.EstimatedDeliveryDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Shipment estimated delivery date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!shipment.EarlyPickup.IsValidTimeString())
            {
                Messages.Add(ValidationMessage.Error("Shipment early pickup time is invalid"));
                valid = false;
            }

            if (!shipment.LatePickup.IsValidTimeString())
            {
                Messages.Add(ValidationMessage.Error("Shipment late pickup time is invalid"));
                valid = false;
            }

            var earlyPickup = DateTime.Now.SetTime(shipment.EarlyPickup);
            var latePickup = DateTime.Now.SetTime(shipment.LatePickup);

            if (earlyPickup > latePickup)
            {
                Messages.Add(ValidationMessage.Error("Shipment early pickup time is later than late pickup time"));
                valid = false;
            }
            else if (shipment.ServiceMode == ServiceMode.LessThanTruckload && (latePickup - earlyPickup).Hours < 2)
            {
                Messages.Add(ValidationMessage.Error("Shipment early pickup time cannot be within 2 hours of the late pickup time for Less Than Truckload shipment"));
                valid = false;
            }

            if (!shipment.EarlyDelivery.IsValidTimeString())
            {
                Messages.Add(ValidationMessage.Error("Shipment early delivery time is invalid"));
                valid = false;
            }

            if (!shipment.LateDelivery.IsValidTimeString())
            {
                Messages.Add(ValidationMessage.Error("Shipment late delivery time is invalid"));
                valid = false;
            }

            var earlyDelivery = DateTime.Now.SetTime(shipment.EarlyDelivery);
            var lateDelivery = DateTime.Now.SetTime(shipment.LateDelivery);

            if (earlyDelivery > lateDelivery)
            {
                Messages.Add(ValidationMessage.Error("Shipment early delivery time is later than late delivery time"));
                valid = false;
            }

            if (!shipment.Status.ToInt().EnumValueIsValid<ShipmentStatus>())
            {
                Messages.Add(ValidationMessage.Error("Shipment must have a valid status"));
                valid = false;
            }

            if (!shipment.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Shipment date created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }
            if (shipment.HazardousMaterial && !shipment.Items.Any(i => i.HazardousMaterial))
            {
                Messages.Add(ValidationMessage.Error("Shipment cannot be marked as Hazmat if no items are marked as Hazmat"));
                valid = false;
            }

            if (shipment.HazardousMaterial && string.IsNullOrEmpty(shipment.HazardousMaterialContactName))
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment is marked hazardous and therefore requires a hazardous material contact name"));
                valid = false;
            }
            if (shipment.HazardousMaterial && string.IsNullOrEmpty(shipment.HazardousMaterialContactPhone))
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment is marked hazardous and therefore requires a hazardous material contact phone"));
                valid = false;
            }



            if (!string.IsNullOrEmpty(shipment.HazardousMaterialContactName) &&
                    shipment.HazardousMaterialContactName.Length > 50)
            {
                Messages.Add(
                    ValidationMessage.Error("Shipment hazardous material contact name cannot exceed 50 characters"));
                valid = false;
            }



            if (!string.IsNullOrEmpty(shipment.HazardousMaterialContactPhone) &&
                shipment.HazardousMaterialContactPhone.Length > 50)
            {
                Messages.Add(
                    ValidationMessage.Error("Shipment hazardous material contact phone cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.HazardousMaterialContactMobile) &&
                shipment.HazardousMaterialContactMobile.Length > 50)
            {
                Messages.Add(
                    ValidationMessage.Error("Shipment hazardous material contact mobile cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.HazardousMaterialContactEmail) &&
                shipment.HazardousMaterialContactEmail.Length > 100)
            {
                Messages.Add(
                    ValidationMessage.Error("Shipment hazardous material contact email cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.HazardousMaterialContactEmail) && !shipment.HazardousMaterialContactEmail.EmailIsValid())
            {
                Messages.Add(ValidationMessage.Error("Shipment hazardous material contact email is invalid"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.PurchaseOrderNumber) && shipment.PurchaseOrderNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Shipment purchase order number cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.ShipperReference) && shipment.ShipperReference.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Shipment shipper reference cannot exceed 50 characters"));
                valid = false;
            }


            if (!shipment.Vendors.Any())
            {
                Messages.Add(ValidationMessage.Error("Shipment must have at least one vendor"));
                valid = false;
            }
            else
            {
                if (!ShipmentVendorsAreValid(shipment.Vendors)) valid = false;

                if (shipment.Vendors.Count(v => v.Primary) != 1)
                {
                    Messages.Add(ValidationMessage.Error("Shipment should have one (and only one) primary vendor"));
                    valid = false;
                }
            }

            if (!shipment.AccountBuckets.Any())
            {
                Messages.Add(ValidationMessage.Error("Shipment must have at least one account bucket designation"));
                valid = false;
            }
            else
            {
                if (!ShipmentAccountBucketsAreValid(shipment.AccountBuckets)) valid = false;

                if (shipment.AccountBuckets.Count(a => a.Primary) != 1)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment should have one (and only one) primary account bucket designation"));
                    valid = false;
                }
            }

            if (shipment.OriginalRateValue < 0)
            {
                Messages.Add(
                    ValidationMessage.Error("Shipment original rate value buy must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.VendorDiscountPercentage < 0 || shipment.VendorDiscountPercentage > 9999.9999m)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment vendor discount percentage must be greater than or equal to zero and less than 10000"));
                valid = false;
            }

            if (shipment.VendorFreightFloor < 0)
            {
                Messages.Add(
                    ValidationMessage.Error("Shipment vendor freight floor buy must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.VendorFuelPercent < 0 || shipment.VendorFuelPercent > 9999.9999m)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment vendor fuel percent must be greater than or equal to zero and less than 10000"));
                valid = false;
            }

            if (shipment.CustomerFreightMarkupValue < 0)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment customer freight markup value must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.CustomerFreightMarkupPercent < 0)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment customer freight markup percent must be greater than or equal to zero"));
                valid = false;
            }


            if (shipment.BilledWeight < 0)
            {
                Messages.Add(ValidationMessage.Error("Shipment billed weight must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.RatedWeight < 0)
            {
                Messages.Add(ValidationMessage.Error("Shipment rated weight must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.RatedCubicFeet < 0)
            {
                Messages.Add(ValidationMessage.Error("Shipment rated cubic feet must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.RatedPcf < 0)
            {
                Messages.Add(ValidationMessage.Error("Shipment rated pounds per cubic feet must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.LineHaulProfitAdjustmentRatio < 0 || shipment.LineHaulProfitAdjustmentRatio > 9999.9999m)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment line haul profit adjustment ratio must be greater than or equal to zero and less than 10000"));
                valid = false;
            }

            if (shipment.FuelProfitAdjustmentRatio < 0 || shipment.FuelProfitAdjustmentRatio > 9999.9999m)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment fuel profit adjustment ratio must be greater than or equal to zero and less than 10000"));
                valid = false;
            }

            if (shipment.AccessorialProfitAdjustmentRatio < 0 || shipment.AccessorialProfitAdjustmentRatio > 9999.9999m)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment accessorial profit adjustment ratio must be greater than or equal to zero and less than 10000"));
                valid = false;
            }

            if (shipment.ServiceProfitAdjustmentRatio < 0 || shipment.ServiceProfitAdjustmentRatio > 9999.9999m)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment service profit adjustment ratio must be greater than or equal to zero and less than 10000"));
                valid = false;
            }

            if (shipment.ResellerAdditionalFreightMarkup < 0)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment reseller additional freight markup must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.ResellerAdditionalFuelMarkup < 0)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment reseller additional fuel markup must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.ResellerAdditionalAccessorialMarkup < 0)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment reseller additional accessorial markup must be greater than or equal to zero"));
                valid = false;
            }

            if (shipment.ResellerAdditionalServiceMarkup < 0)
            {
                Messages.Add(
                    ValidationMessage.Error(
                        "Shipment reseller additional service markup must be greater than or equal to zero"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.SmallPackType) && shipment.SmallPackType.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Shipment small pack service type cannot exceed 50 characters"));
                valid = false;
            }

            if (!shipment.SmallPackageEngine.ToInt().EnumValueIsValid<SmallPackageEngine>())
            {
                Messages.Add(ValidationMessage.Error("Shipment small pack engine is invalid"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.VendorRatingOverrideAddress) &&
                shipment.VendorRatingOverrideAddress.Length > 100)
            {
                Messages.Add(
                    ValidationMessage.Error("Shipment vendor rating override address cannot exceed 100 characters"));
                valid = false;
            }

            if (shipment.OriginTerminal != null && !LTLTerminalInfoIsValid(shipment.OriginTerminal))
            {
                Messages.Add(ValidationMessage.Error("LTL Origin Terminal has some invalid information"));
                valid = false;
            }

            if (shipment.DestinationTerminal != null && !LTLTerminalInfoIsValid(shipment.DestinationTerminal))
            {
                Messages.Add(ValidationMessage.Error("LTL Destination Terminal has some invalid information"));
                valid = false;
            }

            if (shipment.SalesRepresentativeCommissionPercent < 0 ||
                shipment.SalesRepresentativeCommissionPercent > 9999.9999m)
            {
                Messages.Add(
                    ValidationMessage.Error("Shipment sales representative commission percent must be greater than zero and less than 10000"));
                valid = false;
            }

            if (shipment.BillReseller && shipment.ResellerAddition == null)
            {
                Messages.Add(ValidationMessage.Error("Shipment must have reseller addition association"));
                valid = false;
            }

            if (shipment.Mileage < 0)
            {
                Messages.Add(ValidationMessage.Error("Shipment miles cannot be less than zero"));
                valid = false;
            }

            if (shipment.EmptyMileage < 0)
            {
                Messages.Add(ValidationMessage.Error("Shipment empty miles cannot be less than zero"));
                valid = false;
            }

            if (!shipment.Charges.Any())
            {
                Messages.Add(ValidationMessage.Error("Shipment must have at least 1 charge associated"));
                valid = false;
            }

            if (!shipment.Items.Any())
            {
                Messages.Add(ValidationMessage.Error("Shipment must have at least 1 item associated"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.GeneralBolComments) && shipment.GeneralBolComments.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("General BOL Comments cannot exceed 200 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.CriticalBolComments) && shipment.CriticalBolComments.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Critical BOL Comments cannot exceed 200 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.MiscField1) && shipment.MiscField1.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Shipment Misc. Field 1 cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.MiscField2) && shipment.MiscField2.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Shipment Misc. Field 2 cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.DriverName) && shipment.DriverName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Shipment Driver Name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.DriverPhoneNumber) && shipment.DriverPhoneNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Shipment Driver Phone Number cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.DriverTrailerNumber) && shipment.DriverTrailerNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Shipment Driver Trailer Number cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(shipment.Project44QuoteNumber) && shipment.Project44QuoteNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Project 44 Quote Number cannot exceed 50 characters"));
                valid = false;
            }

            if (shipment.Notes.Count > 0 && !ShipmentNotesAreValid(shipment.Notes)) valid = false;
            if (shipment.Charges.Count > 0 && !ShipmentChargesAreValid(shipment.Charges)) valid = false;
            if (shipment.Services.Count > 0 && !ShipmentServicesAreValid(shipment.Services)) valid = false;
            if (shipment.Equipments.Count > 0)
            {
                if (shipment.Equipments.Count() != 1)
                {
                    Messages.Add(ValidationMessage.Error("Shipment can utilize one, and only one, equipment at a time"));
                    valid = false;
                }
                if (!ShipmentEquipmentsAreValid(shipment.Equipments)) valid = false;
            }

            var lastStopIndex = shipment.Destination == null ? default(long) : shipment.Destination.StopOrder;
            var requiresNMFCCode = customer != null && customer.ShipmentRequiresNMFC;

            if (shipment.Items.Count > 0 && !ShipmentItemsAreValid(shipment.Items, lastStopIndex, requiresNMFCCode))
                valid = false;
            if (!ShipmentReferencesAreValid(shipment.CustomerReferences, customer)) valid = false;
            if (shipment.Stops.Count > 0 && !HasValidLocationInformation(shipment.Stops)) valid = false;
            if (shipment.QuickPayOptions.Count > 0 && !ShipmentQuickPayOptionsAreValid(shipment.QuickPayOptions))
                valid = false;
            if (shipment.Assets.Count > 0)
            {
                if (!ShipmentAssetsAreValid(shipment.Assets)) valid = false;
                if (shipment.Assets.Count(a => a.Primary) != 1)
                {
                    Messages.Add(ValidationMessage.Error("Shipment assets should have one (and only one) primary asset"));
                    valid = false;
                }
            }


            if (shipment.Documents.Count > 0 && !ShipmentDocumentsAreValid(shipment.Documents)) valid = false;
            if (shipment.AutoRatingAccessorials.Count > 0 &&
                !ShipmentAutoRatingAccessorialsAreValid(shipment.AutoRatingAccessorials)) valid = false;
            if (shipment.CheckCalls.Count > 0 && (!CheckCallTimesAreValid(shipment.CheckCalls, shipment) || !ShipmentCheckCallsAreValid(shipment.CheckCalls))) valid = false;
            return valid;
        }


        private bool LTLTerminalInfoIsValid(LTLTerminalInfo info)
        {
            var valid = true;

            if (info.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info requires a tenant association "));
                valid = false;
            }

            if (info.Shipment == null)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info requires a shipment association "));
                valid = false;
            }

            if (string.IsNullOrEmpty(info.Code))
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info requires a code "));
                valid = false;
            }
            else if (info.Code.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info code cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.Name) && info.Name.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info Name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.Phone) && info.Phone.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info phone number cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.TollFree) && info.TollFree.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info toll free number cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.Fax) && info.Fax.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info fax number cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.Email) && info.Email.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info email address cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.Email) && !info.Email.EmailIsValid())
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info email address is invalid"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.ContactName) && info.ContactName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info contact name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.ContactTitle) && info.ContactTitle.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info contact title cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.Street1) && info.Street1.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info street1 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.Street2) && info.Street2.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info street2 cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.City) && info.City.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info city cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.State) && info.State.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info state cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(info.PostalCode) && info.PostalCode.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("LTL Terminal Info postal code cannot exceed 50 characters"));
                valid = false;
            }

            return valid;
        }


        private bool HasValidLocationInformation(IEnumerable<ShipmentLocation> locations)
        {
            var valid = true;

            foreach (var location in locations)
            {
                if (string.IsNullOrEmpty(location.Description))
                {
                    Messages.Add(ValidationMessage.Error("Shipment location requires a description"));
                    valid = false;
                }
                else if (location.Description.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment location Description cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.Street1) && location.Street1.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment location Street1 cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.Street2) && location.Street2.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment location Street2 cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.City) && location.City.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment location City cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.State) && location.State.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment location State cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.PostalCode) && location.PostalCode.Length > 10)
                {
                    Messages.Add(ValidationMessage.Error("Shipment location Postal Code cannot exceed 10 characters"));
                    valid = false;
                }
                if (!string.IsNullOrEmpty(location.PostalCode) && location.Country != null && !string.IsNullOrEmpty(location.Country.PostalCodeValidation)
                        && !location.Country.IsValidPostalCodeFormat(location.PostalCode))
                {
                    Messages.Add(ValidationMessage.Error("Shipment location Postal Code is not in required format for country {0}. Expected Format: {1}",
                                                         location.Country.Name, location.Country.PostalCodeValidation));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.SpecialInstructions) && location.SpecialInstructions.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Shipment location special instruction cannot exceed 500 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.GeneralInfo) && location.GeneralInfo.Length > 2000)
                {
                    Messages.Add(ValidationMessage.Error("Shipment location general info cannot exceed 2000 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(location.Direction) && location.Direction.Length > 2000)
                {
                    Messages.Add(ValidationMessage.Error("shipment location direction cannot exceed 2000 characters"));
                    valid = false;
                }

                if (location.CountryId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment location requires a country association"));
                    valid = false;
                }

                if (!location.AppointmentDateTime.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Shipment location appointment date/time must be between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime,
                                                         DateUtility.SystemLatestDateTime));
                    valid = false;
                }

                if (location.MilesFromPreceedingStop < 0)
                {
                    Messages.Add(ValidationMessage.Error("Shipment location miles from preceeding stop cannot be less than zero"));
                    valid = false;
                }

                if (location.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment location requires a tenant association"));
                    valid = false;
                }

                if (location.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment location requires a Shipment association"));
                    valid = false;
                }

                if (location.Contacts.Count > 0)
                {
                    if (!HasValidLocationContactInformation(location.Contacts)) valid = false;
                    if (location.Contacts.Count(c => c.Primary) != 1)
                    {
                        Messages.Add(
                            ValidationMessage.Error(" Shipment location should have one (and only one) primary contact"));
                        valid = false;
                    }
                }
            }

            return valid;
        }

        private bool HasValidLocationContactInformation(IEnumerable<ShipmentContact> contacts)
        {
            var valid = true;

            foreach (var contact in contacts)
            {
                if (contact.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment contact requires a tenant association"));
                    valid = false;
                }

                if (contact.Location == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment contact requires a Shipment location association"));
                    valid = false;
                }

                if (contact.ContactTypeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment contact requires a contact type association"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Name) && contact.Name.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment contact name cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Phone) && contact.Phone.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment contact phone cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Mobile) && contact.Mobile.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment contact mobile cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Fax) && contact.Fax.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment contact fax cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Email) && contact.Email.Length > 100)
                {
                    Messages.Add(ValidationMessage.Error("Shipment contact email cannot exceed 100 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Email) && !contact.Email.EmailIsValid())
                {
                    Messages.Add(ValidationMessage.Error("Shipment contact email is invalid"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(contact.Comment) && contact.Comment.Length > 200)
                {
                    Messages.Add(ValidationMessage.Error("Shipment contact comment cannot exceed 200 characters"));
                    valid = false;
                }
            }

            return valid;
        }


        private bool ShipmentReferencesAreValid(IEnumerable<ShipmentReference> customerReferences, Customer customer)
        {
            var valid = true;

            var requiredReferences = customer != null
                                         ? customer.CustomFields
                                               .Where(c => c.Required)
                                               .Select(c => c.Name)
                                               .Distinct()
                                               .ToDictionary(c => c, c => c)
                                         : new Dictionary<string, string>();

            foreach (var reference in customerReferences)
            {
                if (reference.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment reference requires a tenant association"));
                    valid = false;
                }

                if (reference.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment reference requires a Shipment association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(reference.Name))
                {
                    Messages.Add(ValidationMessage.Error("Shipment reference must have a reference name"));
                    valid = false;
                }
                else
                {
                    if (requiredReferences.ContainsKey(reference.Name))
                    {
                        reference.Required = true;
                        requiredReferences.Remove(reference.Name);
                    }

                    if (reference.Name.Length > 50)
                    {
                        Messages.Add(ValidationMessage.Error("Shipment reference name cannot exceed 50 characters"));
                        valid = false;
                    }
                }

                if (reference.Required && string.IsNullOrEmpty(reference.Value))
                {
                    Messages.Add(
                        ValidationMessage.Error("Shipment reference [{0}] is required by customer [{1}] and must therefore have a value", reference.Name, customer.CustomerNumber));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(reference.Value) && reference.Value.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment reference value cannot exceed 50 characters"));
                    valid = false;
                }
            }

            foreach (var key in requiredReferences.Keys)
            {
                Messages.Add(ValidationMessage.Error("Customer [{0}] requires {1} field on Shipment.", customer.Name, key));
                valid = false;
            }

            return valid;
        }

        private bool ShipmentItemsAreValid(IEnumerable<ShipmentItem> items, long lastStopIndex, bool requiresNMFCCode)
        {
            var valid = true;

            foreach (var item in items)
            {
                if (item.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment item requires a tenant association"));
                    valid = false;
                }

                if (item.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment item requires a Shipment association"));
                    valid = false;
                }

                if (item.PackageTypeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment item requires a package type association"));
                    valid = false;
                }

                if (item.Pickup < 0 || item.Pickup >= lastStopIndex)
                {
                    Messages.Add(ValidationMessage.Error("Shipment item pickup location index is invalid"));
                    valid = false;
                }

                if (item.Delivery <= 0 || item.Delivery > lastStopIndex)
                {
                    Messages.Add(ValidationMessage.Error("Shipment item delivery location index is invalid"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(item.Description))
                {
                    Messages.Add(ValidationMessage.Error("Shipment item must have a description"));
                    valid = false;
                }
                else if (item.Description.Length > 200)
                {
                    Messages.Add(ValidationMessage.Error("Shipment item description cannot exceed 200 characters"));
                    valid = false;
                }

                if (item.PieceCount < 0)
                {
                    Messages.Add(ValidationMessage.Error("Shipment item must have a piece count of zero or greater"));
                    valid = false;
                }

                if (item.Quantity <= 0)
                {
                    Messages.Add(ValidationMessage.Error("Shipment item must have quantity greater than zero"));
                    valid = false;
                }

                if (item.ActualWeight <= 0 || item.ActualWeight > 99999999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment actual item weight must be greater than zero and less than 100000000"));
                    valid = false;
                }

                if (item.ActualLength < 0 || item.ActualLength > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment actual item length must be greater than or equal to zero and less than 10000"));
                    valid = false;
                }
                if (item.ActualWidth < 0 || item.ActualWidth > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment actual item width must be greater than or equal to zero and less than 10000"));
                    valid = false;
                }
                if (item.ActualHeight < 0 || item.ActualHeight > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment actual item height must be greater than or equal to zero and less than 10000"));
                    valid = false;
                }

                if (item.IsOutsideAllowablePcfActual())
                {
                    Messages.Add(ValidationMessage.Error("Shipment actual item pounds per cubic feet not in allowable limits of {0} - {1} lb/ft3", CubicFootCalculations.MinPcf, CubicFootCalculations.MaxPcf));
                    valid = false;
                }

                if (item.Value < 0)
                {
                    Messages.Add(ValidationMessage.Error("Shipment item value must be greater than zero"));
                    valid = false;
                }

                if (requiresNMFCCode && string.IsNullOrEmpty(item.NMFCCode))
                {
                    Messages.Add(ValidationMessage.Error("Customer requires item NMFC code on all items"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(item.NMFCCode) && item.NMFCCode.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment item NMFC code cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(item.HTSCode) && item.HTSCode.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment item HTS code cannot exceed 50 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(item.Comment) && item.Comment.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment item comment cannot exceed 50 characters"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ShipmentEquipmentsAreValid(IEnumerable<ShipmentEquipment> equipments)
        {
            var valid = true;

            foreach (var equipment in equipments)
            {
                if (equipment.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment equipment requires a tenant association"));
                    valid = false;
                }

                if (equipment.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment equipment requires a Shipment association"));
                    valid = false;
                }

                if (equipment.EquipmentTypeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment equipment requires an equipment association"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ShipmentServicesAreValid(IEnumerable<ShipmentService> services)
        {
            var valid = true;

            foreach (var service in services)
            {
                if (service.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment service requires a tenant association"));
                    valid = false;
                }

                if (service.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment service requires a Shipment association"));
                    valid = false;
                }

                if (service.ServiceId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment service requires a service association"));
                    valid = false;
                }
            }

            return valid;
        }

        public bool ShipmentVendorsAreValid(IEnumerable<ShipmentVendor> vendors)
        {
            var valid = true;

            foreach (var vendor in vendors)
            {
                if (vendor.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment vendor requires a tenant association"));
                    valid = false;
                }

                if (vendor.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment vendor requires a Shipment association"));
                    valid = false;
                }

                if (vendor.VendorId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment vendor requires a vendor association"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(vendor.FailureComments) && vendor.FailureComments.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Shipment vendor failure comment cannot exceed 500 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(vendor.ProNumber) && vendor.ProNumber.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment vendor pro number cannot exceed 50 characters"));
                    valid = false;
                }

            }

            return valid;
        }

        public bool ShipmentChargesAreValid(IEnumerable<ShipmentCharge> charges)
        {
            var valid = true;

            if (charges.Sum(c => c.UnitSell) < 0)
            {
                Messages.Add(ValidationMessage.Error("Total of shipment charges unit sell must greater than or equal to zero"));
                valid = false;
            }

            if (charges.Sum(c => c.UnitBuy) < 0)
            {
                Messages.Add(ValidationMessage.Error("Total of shipment charges unit buy must greater than or equal to zero"));
                valid = false;
            }

            foreach (var charge in charges)
            {
                if (charge.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment charge requires a tenant association"));
                    valid = false;
                }

                if (charge.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment charge requires a Shipment association"));
                    valid = false;
                }

                if (charge.ChargeCodeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment charge requires a charge code association"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(charge.Comment) && charge.Comment.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment charge comment cannot exceed 50 characters"));
                    valid = false;
                }

                if (charge.Quantity <= 0)
                {
                    Messages.Add(ValidationMessage.Error("Shipment charge quantity must be greater than zero"));
                    valid = false;
                }

                if (charge.UnitDiscount < 0)
                {
                    Messages.Add(
                        ValidationMessage.Error("Shipment charge unit discount must be greater than or equal to zero"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ShipmentNotesAreValid(IEnumerable<ShipmentNote> notes)
        {
            var valid = true;

            foreach (var note in notes)
            {
                if (note.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment note requires a tenant association"));
                    valid = false;
                }

                if (note.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment note requires a Shipment association"));
                    valid = false;
                }

                if (note.User == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment note requires a user association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(note.Message))
                {
                    Messages.Add(ValidationMessage.Error("Shipment note requires a message"));
                    valid = false;
                }
                else if (note.Message.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Shipment note message cannot exceed 500 characters"));
                    valid = false;
                }

                if (!note.Type.ToInt().EnumValueIsValid<NoteType>())
                {
                    Messages.Add(ValidationMessage.Error("Shipment note must have a valid type"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ShipmentQuickPayOptionsAreValid(IEnumerable<ShipmentQuickPayOption> options)
        {
            var valid = true;

            foreach (var option in options)
            {
                if (option.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment quick pay option requires a tenant association"));
                    valid = false;
                }

                if (option.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment quick pay option requires a Shipment association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(option.Description))
                {
                    Messages.Add(ValidationMessage.Error("Shipment quick pay option requires a description"));
                    valid = false;
                }
                else if (option.Description.Length > 500)
                {
                    Messages.Add(
                        ValidationMessage.Error("Shipment quick pay option description cannot exceed 500 characters"));
                    valid = false;
                }

                if (option.DiscountPercent <= 0 || option.DiscountPercent > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment quick pay option discount percentage must be greater than zero and less than 10000"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool ShipmentAssetsAreValid(IEnumerable<ShipmentAsset> assets)
        {
            var valid = true;

            foreach (var asset in assets)
            {
                if (asset.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment asset requires a tenant association"));
                    valid = false;
                }

                if (asset.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment asset requires a shipment association"));
                    valid = false;
                }

                if (asset.DriverAssetId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment drivers asset requires an asset association"));
                    valid = false;
                }

                if (asset.MilesRun <= 0)
                {
                    Messages.Add(ValidationMessage.Error("Shipment asset miles run must be greater than zero"));
                    valid = false;
                }

                if (!asset.MileageEngine.ToInt().EnumValueIsValid<MileageEngine>())
                {
                    Messages.Add(ValidationMessage.Error("Shipment asset mileage engine is invalid"));
                    valid = false;
                }

            }

            return valid;
        }

        private bool ShipmentAccountBucketsAreValid(IEnumerable<ShipmentAccountBucket> accountBuckets)
        {
            var valid = true;

            foreach (var bucket in accountBuckets)
            {
                if (bucket.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment account bucket requires a tenant association"));
                    valid = false;
                }

                if (bucket.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment account bucket requires a shipment association"));
                    valid = false;
                }

                if (bucket.AccountBucket == null)
                {
                    Messages.Add(
                        ValidationMessage.Error("Shipment account bucket requires an account bucket association"));
                    valid = false;
                }
            }

            return valid;
        }

        public bool ShipmentDocumentsAreValid(IEnumerable<ShipmentDocument> documents)
        {
            var valid = true;

            foreach (var document in documents)
            {
                if (document.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Shipment document requires a tenant association"));
                    valid = false;
                }

                if (document.Shipment == null)
                {
                    Messages.Add(ValidationMessage.Error("Shipment document requires a shipment association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(document.Name))
                {
                    Messages.Add(ValidationMessage.Error("Shipment document requires a name"));
                    valid = false;
                }
                else if (document.Name.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Shipment document name cannot exceed 50 characters"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(document.Description))
                {
                    Messages.Add(ValidationMessage.Error("Shipment document requires a description"));
                    valid = false;
                }
                else if (document.Description.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Shipment document description cannot exceed 500 characters"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(document.LocationPath))
                {
                    Messages.Add(ValidationMessage.Error("Shipment document requires a location path"));
                    valid = false;
                }
                else if (document.LocationPath.Length > 200)
                {
                    Messages.Add(ValidationMessage.Error("Shipment document location path cannot exceed 200 characters"));
                    valid = false;
                }
            }
            return valid;
        }

        private bool ShipmentAutoRatingAccessorialsAreValid(IEnumerable<ShipmentAutoRatingAccessorial> accessorials)
        {
            var valid = true;

            foreach (var accessory in accessorials)
            {
                if (accessory.TenantId == default(long))
                {
                    Messages.Add(
                        ValidationMessage.Error("Shipment auto rating accessorial requires a tenant association"));
                    valid = false;
                }

                if (accessory.Shipment == null)
                {
                    Messages.Add(
                        ValidationMessage.Error("Shipment auto rating accessorial requires a shipment association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(accessory.ServiceDescription))
                {
                    Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial requires a description"));
                    valid = false;
                }
                else if (accessory.ServiceDescription.Length > 200)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment auto rating accessorial description cannot exceed 200 characters"));
                    valid = false;
                }

                if (!accessory.RateType.ToInt().EnumValueIsValid<RateType>())
                {
                    Messages.Add(ValidationMessage.Error("Shipment auto rating accessorial rate type is invalid"));
                    valid = false;
                }

                if (accessory.Rate < 0 || accessory.Rate > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment auto rating accessorial rate must be between zero and 9999.9999"));
                    valid = false;
                }

                if (accessory.BuyFloor < 0)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment auto rating accessorial buy floor value must be greater than zero"));
                    valid = false;
                }

                if (accessory.BuyCeiling < 0)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment auto rating accessorial buy ceiling value must be greater than zero"));
                    valid = false;
                }

                if (accessory.SellFloor < 0)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment auto rating accessorial sell floor value must be greater than zero"));
                    valid = false;
                }

                if (accessory.SellCeiling < 0)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment auto rating accessorial sell ceiling value must be greater than zero"));
                    valid = false;
                }

                if (accessory.MarkupValue < 0)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment auto rating accessorial markup value must be greater than zero"));
                    valid = false;
                }

                if (accessory.MarkupPercent < 0 || accessory.MarkupPercent > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Shipment auto rating accessorial rate must be between zero and 9999.9999"));
                    valid = false;
                }
            }

            return valid;
        }


        public bool CheckCallTimesAreValid(IEnumerable<CheckCall> checkCalls, Shipment shipment)
        {
            return true;

            bool valid = true;
            foreach (var cc in checkCalls)
                if (cc.IsNew)
                {
                    if (cc.EdiStatusCode == ProcessorVars.EdiStatusCodeEstimatedDelivery)
                    {
                        DateTime earlyTime = shipment.EstimatedDeliveryDate.Add(Convert.ToDateTime(shipment.EarlyDelivery).TimeOfDay);
                        DateTime lateTime = shipment.EstimatedDeliveryDate.Add(Convert.ToDateTime(shipment.LateDelivery).TimeOfDay);

                        if (!(cc.EventDate >= earlyTime && cc.EventDate <= lateTime))
                        {
                            Messages.Add(ValidationMessage.Error("Event Date for code AG must fall within range of Estimated Delivery Date."));
                            valid = false;
                        }
                    }

                    if (cc.EdiStatusCode == ProcessorVars.EdiStatusCodePickupAppointment)
                    {
                        if (cc.EventDate != shipment.Origin.AppointmentDateTime)
                        {
                            Messages.Add(ValidationMessage.Error("Event Date for code AA and Origin Appointment Date must match."));
                            valid = false;
                        }
                    }

                    if (cc.EdiStatusCode == ProcessorVars.EdiStatusCodeDeliveryAppointment)
                    {
                        if (cc.EventDate != shipment.Destination.AppointmentDateTime)
                        {
                            Messages.Add(ValidationMessage.Error("Event Date for code AB and Destination Appointment Date must match."));
                            valid = false;
                        }
                    }

                    if (cc.EdiStatusCode == ProcessorVars.EdiStatusCodeActualPickup)
                    {
                        if (cc.EventDate.Date != shipment.ActualPickupDate.Date)
                        {
                            Messages.Add(ValidationMessage.Error("Event Date for code AF and Actual Pickup Date must match."));
                            valid = false;
                        }
                    }

                    if (cc.EdiStatusCode == ProcessorVars.EdiStatusCodeFinalDelivery)
                    {
                        if (cc.EventDate.Date != shipment.ActualDeliveryDate.Date)
                        {
                            Messages.Add(ValidationMessage.Error("Event Date for code D1 and Actual Delivery Date must match."));
                            valid = false;
                        }
                    }
                }

            return valid;
        }

        public bool ShipmentCheckCallsAreValid(IEnumerable<CheckCall> checkCalls)
        {
            var valid = true;

            foreach (var checkCall in checkCalls)
            {
                if (checkCall.TenantId == default(long))
                {
                    Messages.Add(
                        ValidationMessage.Error("Shipment check call requires a tenant association"));
                    valid = false;
                }

                if (checkCall.User == null)
                {
                    Messages.Add(
                        ValidationMessage.Error("Shipment check call requires a user association"));
                    valid = false;
                }
                if (checkCall.Shipment == null)
                {
                    Messages.Add(
                        ValidationMessage.Error("Shipment check call requires a shipment association"));
                    valid = false;
                }

                if (!checkCall.CallDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Shipment check call date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                    valid = false;
                }

                if (!checkCall.EventDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Shipment check call event date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(checkCall.CallNotes) && checkCall.CallNotes.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Shipment check call note cannot exceed 500 characters"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(checkCall.EdiStatusCode) && checkCall.EdiStatusCode.Length > 2)
                {
                    Messages.Add(ValidationMessage.Error("Shipment check call edi status code cannot exceed 2 characters"));
                    valid = false;
                }

            }

            return valid;
        }
    }
}
