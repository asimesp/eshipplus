﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;

namespace LogisticsPlus.Eship.Processor.Validation.Connect
{
	public class VendorCommunicationValidator : ValidatorBase
	{
		public bool IsValid(VendorCommunication communication)
		{
			bool valid = HasAllRequiredData(communication);

			if (!NotificationsAreValid(communication)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(VendorCommunication communication)
		{
			var valid = true;

			if (communication.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication requires a tenant association"));
				valid = false;
			}

			if (communication.Vendor == null)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication requires a vendor association"));
				valid = false;
			}
			else
			{
				if (communication.Vendor.Communication != null && communication.Vendor.Communication.Id != communication.Id)
				{
					Messages.Add(ValidationMessage.Error("Vendor Communication vendor association is invalid.  Vendor is already associated with another Communication profile"));
					valid = false;
				}
			}

			if (communication.EdiEnabled && string.IsNullOrEmpty(communication.EdiCode))
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Edi is enabled, and requires an Edi Code"));
				valid = false;
			}

			if (communication.EdiEnabled && string.IsNullOrEmpty(communication.EdiVANUrl))
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Edi is enabled, and requires an Edi Van Url"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiVANEnvelopePath) && communication.EdiVANEnvelopePath.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Edi Van Envelope path must be less than or equal to 200 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiCode) && communication.EdiCode.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication EdiCode must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiVANUrl) && communication.EdiVANUrl.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Edi VAN Url must be less than or equal to 100 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiVANUsername) && communication.EdiVANUsername.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Edi VAN Username must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiVANPassword) && communication.EdiVANPassword.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Edi VAN Password must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiVANDefaultFolder) && communication.EdiVANDefaultFolder.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Edi VAN Default Folder must be less than or equal to 50 characters"));
				valid = false;
			}


			if (communication.FtpEnabled && string.IsNullOrEmpty(communication.FtpUrl))
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Ftp is enabled, and requires an Ftp Url"));
				valid = false;
			}

			if (communication.FtpEnabled && string.IsNullOrEmpty(communication.FtpUsername))
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Ftp is enabled, and requires an Ftp Username"));
				valid = false;
			}

			if (communication.FtpEnabled && string.IsNullOrEmpty(communication.FtpPassword))
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Ftp is enabled, and requires an Ftp Password"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.FtpUrl) && communication.FtpUrl.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Ftp Url must be less than or equal to 100 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.FtpUsername) && communication.FtpUsername.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Ftp Username must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.FtpPassword) && communication.FtpPassword.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Ftp Password must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.FtpDefaultFolder) && communication.FtpDefaultFolder.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Ftp Default Folder must be less than or equal to 50 characters"));
				valid = false;
			}

			if (communication.LoadTenderExpAllowance < 0)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication load tender expiration allowance (mins) must be greater than or equal to zero"));
				valid = false;
			}

			if ((communication.ImgRtrvEnabled || communication.ShipmentDispatchEnabled) && string.IsNullOrEmpty(communication.CarrierIntegrationEngine))
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Image Retrieval Engine must be present as function is enabled"));
				valid = false;
			}
			if (!string.IsNullOrEmpty(communication.CarrierIntegrationEngine) && communication.CarrierIntegrationEngine.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Image Retrieval Engine must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.CarrierIntegrationUsername) && communication.CarrierIntegrationUsername.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Image Retrieval Username must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.CarrierIntegrationPassword) && communication.CarrierIntegrationPassword.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Communication Image Retrieval Password must be less than or equal to 50 characters"));
				valid = false;
			}

            if (!string.IsNullOrEmpty(communication.SMC3ProductionAccountToken) && communication.SMC3ProductionAccountToken.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Vendor Communication SMC3 Production Account Token must be less than or equal to 200 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(communication.SMC3TestAccountToken) && communication.SMC3TestAccountToken.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Vendor Communication SMC3 Test Account Token must be less than or equal to 200 characters"));
                valid = false;
            }

			return valid;
		}

		private bool NotificationsAreValid(VendorCommunication vendorCommunication)
		{
			var valid = true;

			foreach (var notification in vendorCommunication.Notifications)
			{
				if (notification.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor Communication notification requires a tenant association"));
					valid = false;
				}

				if (notification.VendorCommunication == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor Communication notification requires a Vendor Communication association"));
					valid = false;
				}

				if (notification.NotificationMethod == NotificationMethod.Edi)
				{
					if (string.IsNullOrEmpty(vendorCommunication.EdiCode))
					{
						Messages.Add(ValidationMessage.Error("Vendor Communication EdiCode must be present"));
						valid = false;
					}

					if (string.IsNullOrEmpty(vendorCommunication.EdiVANUrl))
					{
						Messages.Add(ValidationMessage.Error("Vendor Communication Edi VAN Url must be present"));
						valid = false;
					}

					if (string.IsNullOrEmpty(vendorCommunication.EdiVANUsername))
					{
						Messages.Add(ValidationMessage.Error("Vendor Communication Edi VAN Username must be present"));
						valid = false;
					}

					if (string.IsNullOrEmpty(vendorCommunication.EdiVANPassword))
					{
						Messages.Add(ValidationMessage.Error("Vendor Communication Edi VAN Password must be present"));
						valid = false;
					}
				}

				if (notification.NotificationMethod == NotificationMethod.Ftp)
				{
					if (string.IsNullOrEmpty(vendorCommunication.FtpUrl))
					{
						Messages.Add(ValidationMessage.Error("Vendor Communication Ftp Url must be present"));
						valid = false;
					}

					if (string.IsNullOrEmpty(vendorCommunication.FtpUsername))
					{
						Messages.Add(ValidationMessage.Error("Vendor Communication Ftp Username must be present"));
						valid = false;
					}

					if (string.IsNullOrEmpty(vendorCommunication.FtpPassword))
					{
						Messages.Add(ValidationMessage.Error("Vendor Communication Ftp Password must be present"));
						valid = false;
					}
				}

				if (notification.NotificationMethod == NotificationMethod.Fax)
				{
					if (string.IsNullOrEmpty(notification.Fax))
					{
						Messages.Add(ValidationMessage.Error("Vendor Notification Fax must be present"));
						valid = false;
					}
				}

				if (notification.NotificationMethod == NotificationMethod.Email)
				{
					if (string.IsNullOrEmpty(notification.Email))
					{
						Messages.Add(ValidationMessage.Error("Vendor Notification Email must be present"));
						valid = false;
					}
					else if (notification.Email.Length > 500)
					{
						Messages.Add(ValidationMessage.Error("Vendor Notification Email cannot exceed 500 characters"));
						valid = false;
					}

				    if (!string.IsNullOrEmpty(notification.Email) && 
				        notification.Email.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
				    {
				        Messages.Add(ValidationMessage.Error("Vendor Notification Email contains one or more invalid addresses"));
				        valid = false;
				    }
                }

				if (notification.NotificationMethod == NotificationMethod.Edi || notification.NotificationMethod == NotificationMethod.Ftp)
				{
					if (notification.Milestone != Milestone.NewShipmentRequest &&
						notification.Milestone != Milestone.ShipmentVoided)
					{
						Messages.Add(ValidationMessage.Error("Vendor Communication Milestone can not be processed via EDI or FTP"));
						valid = false;
					}
				}
			}

			return valid;
		}

		public bool CanDeleteVendorCommunication(VendorCommunication vendorCommunication)
		{
			const string query =
				@"SELECT 
					(SELECT COUNT(*) FROM XmlTransmission WHERE CommunicationReferenceType = @CommunicationReferenceType and CommunicationReferenceId = @CommunicationReferenceId) +
					(SELECT COUNT(*) FROM ShipmentDocRtrvLog WHERE CommunicationId = @CommunicationReferenceId)";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"CommunicationReferenceType", CommunicationReferenceType.Vendor},
			                 		{"CommunicationReferenceId", vendorCommunication.Id}
			                 	};
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Vendor Communication is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}