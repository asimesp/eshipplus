﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;

namespace LogisticsPlus.Eship.Processor.Validation.Connect
{
    public class DocDeliveryLogValidator : ValidatorBase
    {
        public bool IsValid(DocDeliveryLog docDeliveryLog)
        {
            var valid = true;

            if (!HasAllRequiredData(docDeliveryLog)) valid = false;

            return valid;
        }

        private bool HasAllRequiredData(DocDeliveryLog docDeliveryLog)
        {
            var valid = true;

            if (docDeliveryLog.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Document delivery log requires a tenant association"));
                valid = false;
            }

			if (docDeliveryLog.UserId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Document delivery log requires a user association"));
				valid = false;
			}

            if (docDeliveryLog.EntityId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Document delivery log requires an entity association"));
                valid = false;
            }

            if (!docDeliveryLog.EntityType.ToInt().EnumValueIsValid<DocDeliveryEntityType>())
            {
                Messages.Add(ValidationMessage.Error("Document delivery log must have a valid entity type"));
                valid = false;
            }
            else if(docDeliveryLog.EntityType == DocDeliveryEntityType.ShipmentDocument)
            {
                if ( docDeliveryLog.DocumentTagId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Document delivery log with an entity type of {0} requires a document tag association", DocDeliveryEntityType.ShipmentDocument.FormattedString()));
                    valid = false;
                }
                
                if (string.IsNullOrEmpty(docDeliveryLog.LocationPath))
                {
                    Messages.Add(ValidationMessage.Error("Document delivery log with an entity type of {0} requires a location path", DocDeliveryEntityType.ShipmentDocument.FormattedString()));
                    valid = false;
                }
                else if(docDeliveryLog.LocationPath.Length > 200)
                {
                    Messages.Add(ValidationMessage.Error("Document delivery log location path cannot exceed 200 characters"));
                    valid = false;
                }
            }

            if (string.IsNullOrEmpty(docDeliveryLog.DocumentName))
            {
                Messages.Add(ValidationMessage.Error("Document delivery log must have document name"));
                valid = false;
            }
            else if (docDeliveryLog.DocumentName.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Document delivery log document name cannot exceed 50 characters"));
                valid = false;
            }

            if (!docDeliveryLog.LogDateTime.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Document delivery log date time must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if(!docDeliveryLog.DeliveryWasSuccessful)
            {
                if(string.IsNullOrEmpty(docDeliveryLog.FailedDeliveryMessage))
                {
                    Messages.Add(ValidationMessage.Error("Document delivery log must have a failed delivery message if delivery was not succesful"));
                    valid = false;
                }
                else if(docDeliveryLog.FailedDeliveryMessage.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Document delivery log failed delivery message cannot exceed 500 characters"));
                    valid = false;
                }
            }

            return valid;
        }
    }
}