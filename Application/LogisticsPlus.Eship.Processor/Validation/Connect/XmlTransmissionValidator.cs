﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;

namespace LogisticsPlus.Eship.Processor.Validation.Connect
{
	public class XmlTransmissionValidator : ValidatorBase
	{
		public bool IsValid(XmlTransmission transmission)
		{
			Messages.Clear();

			var valid = true;

			if (!HasAllRequiredData(transmission)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(XmlTransmission transmission)
		{
			var valid = true;

			if (transmission.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission require a tenant association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(transmission.ReferenceNumber))
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission requires a Reference number"));
				valid = false;
			}
			else if (transmission.ReferenceNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission reference number cannot exceed 50 characters"));
				valid = false;
			}

			if (!transmission.ReferenceNumberType.ToInt().EnumValueIsValid<XmlTransmissionReferenceNumberType>())
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission reference number type is invalid"));
				valid = false;
			}

			if (string.IsNullOrEmpty(transmission.Xml))
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission xml body cannot be empty"));
				valid = false;
			}

			if (string.IsNullOrEmpty(transmission.DocumentType))
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission document type cannot be empty"));
				valid = false;
			}
			else if (transmission.DocumentType.Length > 5)
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission document type cannot exceed 5 characters"));
				valid = false;
			}

			if (!transmission.CommunicationReferenceType.ToInt().EnumValueIsValid<CommunicationReferenceType>())
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission communication reference type is invalid"));
				valid = false;
			}

			if(transmission.CommunicationReferenceId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission requires a communication reference association"));
				valid = false;
			}

			if (!transmission.TransmissionDateTime.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission transmission date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime,
													 DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!transmission.AcknowledgementDateTime.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission acknowledgement date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime,
													 DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!string.IsNullOrEmpty(transmission.AcknowledgementMessage) && transmission.AcknowledgementMessage.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission acknowledgement message cannot exceed 100 characters"));
				valid = false;
			}

			if (!transmission.NotificationMethod.ToInt().EnumValueIsValid<NotificationMethod>())
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission notification method is invalid"));
				valid = false;
			}

			if (transmission.TransmissionKey == Guid.Empty)
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission transmission key can not be empty"));
				valid = false;
			}

			if (!transmission.Direction.ToInt().EnumValueIsValid<Direction>())
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission direction type is invalid"));
				valid = false;
			}
			else if (transmission.Direction == Direction.Outbound && transmission.User == null)
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission require a user association"));
				valid = false;
			}

			return valid;
		}
	}
}