﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;

namespace LogisticsPlus.Eship.Processor.Validation.Connect
{
	public class CustomerCommunicationValidator : ValidatorBase
	{
		public bool IsValid(CustomerCommunication communication)
		{
			var valid = HasAllRequiredData(communication);

			if (!NotificationsAreValid(communication)) valid = false;
			if (!DocDeliveryDocumentTagsAreValid(communication)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(CustomerCommunication communication)
		{
			var valid = true;

			if (communication.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Customer Communication requires a tenant association"));
				valid = false;
			}

			if (communication.Customer == null)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication requires a customer association"));
				valid = false;
			}
			else
			{
				if (communication.Customer.Communication != null && communication.Customer.Communication.Id != communication.Id)
				{
					Messages.Add(ValidationMessage.Error("Customer Communication customer association is invalid.  Customer is already associated with another Communication profile"));
					valid = false;
				}
			}

			if (!communication.StartDocDeliveriesFrom.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Customer Communication must have a valid start delivery date between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}


			if (communication.EdiEnabled && string.IsNullOrEmpty(communication.EdiCode))
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Edi is enabled, and requires an Edi Code"));
				valid = false;
			}

			if (communication.EdiEnabled && string.IsNullOrEmpty(communication.EdiVANUrl))
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Edi is enabled, and requires an Edi Van Url"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiVANEnvelopePath) && communication.EdiVANEnvelopePath.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Edi Van Envelope path must be less than or equal to 200 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiCode) && communication.EdiCode.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication EdiCode must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiVANUrl) && communication.EdiVANUrl.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Edi VAN Url must be less than 100 or equal to characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiVANUsername) && communication.EdiVANUsername.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Edi VAN Username must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiVANPassword) && communication.EdiVANPassword.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Edi VAN Password must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.EdiVANDefaultFolder) && communication.EdiVANDefaultFolder.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Edi VAN Default Folder must be less than or equal to 50 characters"));
				valid = false;
			}



			if (communication.FtpEnabled && string.IsNullOrEmpty(communication.FtpUrl))
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Ftp is enabled, and requires an Ftp Url"));
				valid = false;
			}

			if (communication.FtpEnabled && string.IsNullOrEmpty(communication.FtpUsername))
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Ftp is enabled, and requires an Ftp Username"));
				valid = false;
			}

			if (communication.FtpEnabled && string.IsNullOrEmpty(communication.FtpPassword))
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Ftp is enabled, and requires an Ftp Password"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.FtpUrl) && communication.FtpUrl.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Ftp Url must be less than or equal to 100 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.FtpUsername) && communication.FtpUsername.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Ftp Username must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.FtpPassword) && communication.FtpPassword.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Ftp Password must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.FtpDefaultFolder) && communication.FtpDefaultFolder.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Ftp Default Folder must be less than or equal to 50 characters"));
				valid = false;
			}



			if (communication.FtpDocDeliveryEnabled && string.IsNullOrEmpty(communication.DocDeliveryFtpUrl))
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Document Delivery is enabled, and requires a document delivery Ftp Url"));
				valid = false;
			}

			if (communication.FtpDocDeliveryEnabled && string.IsNullOrEmpty(communication.DocDeliveryFtpUsername))
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Document Delivery is enabled, and requires a document delivery Ftp Username"));
				valid = false;
			}

			if (communication.FtpDocDeliveryEnabled && string.IsNullOrEmpty(communication.DocDeliveryFtpPassword))
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Document Delivery is enabled, and requires a document delivery Ftp Password"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.DocDeliveryFtpUrl) && communication.DocDeliveryFtpUrl.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Document Delivery Ftp Url must be less than or equal to 100 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.DocDeliveryFtpUsername) && communication.DocDeliveryFtpUsername.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Document Delivery Ftp Username must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.DocDeliveryFtpPassword) && communication.DocDeliveryFtpPassword.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Document Delivery Ftp Password must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(communication.DocDeliveryFtpDefaultFolder) && communication.DocDeliveryFtpDefaultFolder.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Communication Document Delivery Ftp Default Folder must be less than or equal to 50 characters"));
				valid = false;
			}

			return valid;
		}

		private bool DocDeliveryDocumentTagsAreValid(CustomerCommunication customerCommunication)
		{
			var valid = true;

			foreach (var tag in customerCommunication.DocDeliveryDocTags)
			{
				if (tag.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer Communication doc delivery document tag requires a tenant association"));
					valid = false;
				}

				if (tag.CustomerCommunication == null)
				{
					Messages.Add(ValidationMessage.Error("Customer Communication doc delivery invoice document tag requires a customer association"));
					valid = false;
				}

				if (tag.DocumentTag == null)
				{
					Messages.Add(ValidationMessage.Error("Customer Communication doc dlivery document tag requires a document tag association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool NotificationsAreValid(CustomerCommunication customerCommunication)
		{
			var valid = true;

			foreach (var notification in customerCommunication.Notifications)
			{
				if (notification.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer Communication notification requires a tenant association"));
					valid = false;
				}

				if (notification.CustomerCommunication == null)
				{
					Messages.Add(ValidationMessage.Error("Customer Communication notification requires a Customer Communication association"));
					valid = false;
				}

				if (notification.NotificationMethod == NotificationMethod.Edi)
				{
					if (string.IsNullOrEmpty(customerCommunication.EdiCode))
					{
						Messages.Add(ValidationMessage.Error("Customer Communication EdiCode must be present"));
						valid = false;
					}

					if (string.IsNullOrEmpty(customerCommunication.EdiVANUrl))
					{
						Messages.Add(ValidationMessage.Error("Customer Communication Edi VAN Url must be present"));
						valid = false;
					}

					if (string.IsNullOrEmpty(customerCommunication.EdiVANUsername))
					{
						Messages.Add(ValidationMessage.Error("Customer Communication Edi VAN Username must be present"));
						valid = false;
					}

					if (string.IsNullOrEmpty(customerCommunication.EdiVANPassword))
					{
						Messages.Add(ValidationMessage.Error("Customer Communication Edi VAN Password must be present"));
						valid = false;
					}
				}

				if (notification.NotificationMethod == NotificationMethod.Ftp)
				{
					if (string.IsNullOrEmpty(customerCommunication.FtpUrl))
					{
						Messages.Add(ValidationMessage.Error("Customer Communication Ftp Url must be present"));
						valid = false;
					}

					if (string.IsNullOrEmpty(customerCommunication.FtpUsername))
					{
						Messages.Add(ValidationMessage.Error("Customer Communication Ftp Username must be present"));
						valid = false;
					}

					if (string.IsNullOrEmpty(customerCommunication.FtpPassword))
					{
						Messages.Add(ValidationMessage.Error("Customer Communication Ftp Password must be present"));
						valid = false;
					}
				}

				if (notification.NotificationMethod == NotificationMethod.Fax)
				{
					if (string.IsNullOrEmpty(notification.Fax))
					{
						Messages.Add(ValidationMessage.Error("Customer Notification Fax must be present"));
						valid = false;
					}
				}

				if (notification.NotificationMethod == NotificationMethod.Email)
				{
					if (string.IsNullOrEmpty(notification.Email))
					{
						Messages.Add(ValidationMessage.Error("Customer Notification Email must be present"));
						valid = false;
					}
					else if (notification.Email.Length > 500)
					{
						Messages.Add(ValidationMessage.Error("Customer Notification Email cannot exceed 500 characters"));
						valid = false;
					}

				    if (!string.IsNullOrEmpty(notification.Email) && 
				        notification.Email.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
				    {
				        Messages.Add(ValidationMessage.Error("Customer Notification Email contains one or more invalid addresses"));
				        valid = false;
				    }
                }

				if (notification.NotificationMethod == NotificationMethod.Edi || notification.NotificationMethod == NotificationMethod.Ftp)
				{
					if (notification.Milestone != Milestone.ShipmentInTransit &&
						notification.Milestone != Milestone.ShipmentDelivered &&
						notification.Milestone != Milestone.InvoicePosted &&
						notification.Milestone != Milestone.ShipmentCheckCallUpdate)
					{
						Messages.Add(ValidationMessage.Error("Customer Communication Milestone can not be processed via EDI or FTP"));
						valid = false;
					}
				}
			}

			return valid;
		}

		public bool CustomerCommunicationDocDeliveryDocumentTagExist(DocDeliveryDocTag tag)
		{
			const string query =
				"Select count(*) from DocDeliveryDocTag where DocumentTagId = @DocumentTagId and CustomerCommunicationId = @CustomerCommunicationId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"DocumentTagId", tag.DocumentTagId},
			                 		{"TenantId", tag.TenantId},
			                 		{"CustomerCommunicationId", tag.CustomerCommunicationId}
			                 	};
			var duplicateTag = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateTag)
				Messages.Add(ValidationMessage.Error("Document tag is already required for invoicing on this customer"));
			return duplicateTag;
		}

		public bool CanDeleteCustomerCommunication(CustomerCommunication customerCommunication)
		{
			const string query = "SELECT COUNT(*) FROM XmlTransmission WHERE CommunicationReferenceType = @CommunicationReferenceType and CommunicationReferenceId = @CommunicationReferenceId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"CommunicationReferenceType", CommunicationReferenceType.Customer},
			                 		{"CommunicationReferenceId", customerCommunication.Id}
			                 	};
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Customer Communication is referenced one or more times and cannot be deleted."));
			return !used;
		}


	}
}
