﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;

namespace LogisticsPlus.Eship.Processor.Validation.Connect
{
    public class FaxTransmissionValidator : ValidatorBase
    {
    	public bool IsValid(FaxTransmission transmission)
        {
            Messages.Clear();

    		bool valid = HasAllRequiredData(transmission);

    		return valid;
        }

		public bool HasAllRequiredData(FaxTransmission transmission)
        {
            var valid = true;

            if (transmission.TenantId == default(long))
            {
				Messages.Add(ValidationMessage.Error("Fax Transmission require a tenant association"));
                valid = false;
            }

			if (string.IsNullOrEmpty(transmission.ShipmentNumber))
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission requires a Shipment number"));
				valid = false;
			}
			else if (transmission.ShipmentNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission shipment number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(transmission.ExternalReference) && transmission.ExternalReference.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission external reference cannot exceed 50 characters"));
				valid = false;
			}

			if(!string.IsNullOrEmpty(transmission.Message) && transmission.Message.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission message cannot exceed 200 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(transmission.ResolutionComment) && transmission.ResolutionComment.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission resolution comment cannot exceed 200 characters"));
				valid = false;
			}

			if (!transmission.SendDateTime.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission send date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime,
													 DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!transmission.ResponseDateTime.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission response date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime,
													 DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!transmission.LastStatusCheckDateTime.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission last status check date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime,
													 DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!transmission.Status.ToInt().EnumValueIsValid<FaxTransmissionStatus>())
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission status is invalid"));
				valid = false;
			}

			if (transmission.TransmissionKey == Guid.Empty)
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission transmission key can not be empty"));
				valid = false;
			}

			if (!transmission.ServiceProvider.ToInt().EnumValueIsValid<ServiceProvider>())
			{
				Messages.Add(ValidationMessage.Error("Fax Transmission must have a valid service provider"));
				valid = false;
			}

			return valid;
        }
    }
}
