﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;

namespace LogisticsPlus.Eship.Processor.Validation.Connect
{
	public class XmlConnectValidator : ValidatorBase
	{
		public bool IsValid(XmlConnect connect)
		{
			Messages.Clear();

			bool valid = HasAllRequiredData(connect);

			return valid;
		}

		public bool HasAllRequiredData(XmlConnect connect)
		{
			var valid = true;

			if (connect.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Xml Transmission require a tenant association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(connect.Xml))
			{
				Messages.Add(ValidationMessage.Error("Xml connect missing xml content"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.ShipmentIdNumber) && connect.ShipmentIdNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect shipment Id number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.ControlNumber) && connect.ControlNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect control number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.OriginStreet1) && connect.OriginStreet1.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect origin street1 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.OriginStreet2) && connect.OriginStreet2.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect origin street2 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.OriginCity) && connect.OriginCity.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect origin city cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.OriginState) && connect.OriginState.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect origin state cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.OriginCountryCode) && connect.OriginCountryCode.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect origin country code cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.OriginCountryName) && connect.OriginCountryName.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect origin country name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.OriginPostalCode) && connect.OriginPostalCode.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect origin postal code cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.DestinationStreet1) && connect.DestinationStreet1.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect destination street1 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.DestinationStreet2) && connect.DestinationStreet2.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect destination street2 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.DestinationCity) && connect.DestinationCity.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect destination city cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.DestinationState) && connect.DestinationState.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect destination state cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.DestinationCountryCode) && connect.DestinationCountryCode.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect destination country code cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.DestinationCountryName) && connect.DestinationCountryName.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect destination country name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.DestinationPostalCode) && connect.DestinationPostalCode.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect destination postal code cannot exceed 50 characters"));
				valid = false;
			}
			
			if (!connect.ExpirationDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Xml connect expiration date must be between {0} and {1}",
												 DateUtility.SystemEarliestDateTime,
												 DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!connect.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Xml connect date created must be between {0} and {1}",
												 DateUtility.SystemEarliestDateTime,
												 DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!connect.ReceiptDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Xml connect receipt date must be between {0} and {1}",
												 DateUtility.SystemEarliestDateTime,
												 DateUtility.SystemLatestDateTime));
				valid = false;
			}
			
			if (!connect.Direction.ToInt().EnumValueIsValid<Direction>())
			{
				Messages.Add(ValidationMessage.Error("Xml Connect document direction is invalid"));
				valid = false;
			}

			if (!connect.Status.ToInt().EnumValueIsValid<XmlConnectStatus>())
			{
				Messages.Add(ValidationMessage.Error("Xml Connect status is invalid"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.StatusMessage) && connect.StatusMessage.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect status message cannot exceed 200 characters"));
				valid = false;
			}

			if (!connect.DocumentType.ToInt().EnumValueIsValid<EdiDocumentType>())
			{
				Messages.Add(ValidationMessage.Error("Xml Connect document type is invalid"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.CustomerNumber) && connect.CustomerNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect customer number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.VendorNumber) && connect.VendorNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect vendor number cannot exceed 50 characters"));
				valid = false;
			}

            if (!string.IsNullOrEmpty(connect.VendorPro) && connect.VendorPro.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Xml Connect vendor pro cannot exceed 50 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(connect.VendorScac) && connect.VendorScac.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect vendor scac cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.PurchaseOrderNumber) && connect.PurchaseOrderNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect purchase order number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.ShipperReference) && connect.ShipperReference.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect shipper reference cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(connect.EquipmentDescriptionCode) && connect.EquipmentDescriptionCode.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect equipment description code cannot exceed 50 characters"));
				valid = false;
			}

			if (connect.TotalStops < 0)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect total stops must be greater than zero"));
				valid = false;
			}

			if (connect.TotalWeight < 0)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect total weight must be greater than zero"));
				valid = false;
			}

			if (connect.TotalPackages < 0)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect total packages must be greater than zero"));
				valid = false;
			}

			if (connect.TotalPieces < 0)
			{
				Messages.Add(ValidationMessage.Error("Xml Connect total pieces must be greater than zero"));
				valid = false;
			}

			return valid;
		}
	}
}