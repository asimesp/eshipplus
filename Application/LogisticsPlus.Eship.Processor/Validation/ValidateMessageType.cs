﻿namespace LogisticsPlus.Eship.Processor.Validation
{
	public enum ValidateMessageType
	{
		Error = 0,
		Warning,
		Information
	}
}
