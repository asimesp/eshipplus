﻿using LogisticsPlus.Eship.Core.Cms;

namespace LogisticsPlus.Eship.Processor.Validation.Cms
{
	public class ContentValidator : ValidatorBase
	{
		public bool IsValid(Content content)
		{
			var valid = true;
			if (!HasAllRequiredData(content)) valid = false;
			return valid;
		}

		private bool HasAllRequiredData(Content content)
		{
			var valid = true;

			if (content.FullText == null)
			{
				Messages.Add(ValidationMessage.Error("Content full text cannot be null"));
				valid = false;
			}

			if (string.IsNullOrEmpty(content.ShortName))
			{
				Messages.Add(ValidationMessage.Error("Content requires a Short Name"));
				valid = false;
			}
			else if (content.ShortName.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Short Name cannot exceed 50 characters"));
				valid = false;
			}
			
			if (content.ShortText == null)
			{
				Messages.Add(ValidationMessage.Error("Short Text cannot be null"));
				valid = false;
			}
			else if (content.ShortText.Length > 250)
			{
				Messages.Add(ValidationMessage.Error("Short Text cannot exceed 250 characters"));
				valid = false;
			}

			return valid;
		}
	}
}
