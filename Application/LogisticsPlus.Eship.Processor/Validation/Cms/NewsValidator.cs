﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Cms;

namespace LogisticsPlus.Eship.Processor.Validation.Cms
{
	public class NewsValidator : ValidatorBase
	{
		public bool IsValid(News news)
		{
			var valid = true;
			if (!HasAllRequiredData(news)) valid = false;
			return valid;
		}

		private bool HasAllRequiredData(News news)
		{
			var valid = true;
			if (news.FullText == null)
			{
				Messages.Add(ValidationMessage.Error("News full text cannot be null"));
				valid = false;
			}

			if (string.IsNullOrEmpty(news.Title))
			{
				Messages.Add(ValidationMessage.Error("News requires a title"));
				valid = false;
			}
			else if (news.Title.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("News title cannot exceed 50 characters"));
			}
			
            if (news.ShortText == null)
			{
				Messages.Add(ValidationMessage.Error("News short text cannot be null"));
				valid = false;
			}
            else if (news.ShortText.Length > 250)
            {
                Messages.Add(ValidationMessage.Error("News short text cannot exceed 250 characters"));
            }

            if (!news.ExpirationDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("News expiration date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime,
                                                     DateUtility.SystemLatestDateTime));
                valid = false;
            }

			return valid;
		}
	}
}
