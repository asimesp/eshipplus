﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Processor.Validation.Rating
{
    public class SmallPackageServiceMapValidator:ValidatorBase
    {
        public bool IsValid(SmallPackageServiceMap smallPackageServiceMap)
        {
            Messages.Clear();

            var valid = true;

            if (!IsUnique(smallPackageServiceMap)) valid = false;
            if (!HasAllRequiredData(smallPackageServiceMap)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(SmallPackageServiceMap smallPackageServiceMap)
        {
            var valid = true;

            if (smallPackageServiceMap.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Small Package Service Mapping requires a tenant association"));
                valid = false;
            }

            if (smallPackageServiceMap.ServiceId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Small Package Service Mapping requires a Package Type association"));
                valid = false;
            }

			if (!smallPackageServiceMap.SmallPackageEngine.ToInt().EnumValueIsValid<SmallPackageEngine>())
			{
				Messages.Add(ValidationMessage.Error("Small Package Service Mapping must have a valid small pack engine"));
				valid = false;
			}

			if (smallPackageServiceMap.SmallPackageEngine == SmallPackageEngine.None)
			{
				Messages.Add(ValidationMessage.Error("Small Package Service Mapping must have a valid small pack engine"));
				valid = false;
			}

            if (string.IsNullOrEmpty(smallPackageServiceMap.SmallPackEngineService))
            {
                Messages.Add(ValidationMessage.Error("Small Package Service Mapping requires a Small Package Engine Service"));
                valid = false;
            }
            else if (smallPackageServiceMap.SmallPackEngineService.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Small Package Service Mapping Service cannot exceed 50 characters"));
                valid = false;
            }
            return valid;
        }

        public bool IsUnique(SmallPackageServiceMap smallPackageServiceMap)
        {
            var query = smallPackageServiceMap.IsNew
                            ? "SELECT COUNT(*) FROM SmallPackageServiceMap WHERE ServiceID = @ServiceID AND SmallPackageEngine = @SmallPackageEngine AND TenantId = @TenantId"
                            : "SELECT COUNT(*) FROM SmallPackageServiceMap WHERE ServiceID= @ServiceID AND SmallPackageEngine = @SmallPackageEngine AND TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"ServiceID", smallPackageServiceMap.ServiceId},
			                 		{"SmallPackageEngine", smallPackageServiceMap.SmallPackageEngine},
			                 		{"TenantId", smallPackageServiceMap.TenantId},
			                 	};
            if (!smallPackageServiceMap.IsNew) parameters.Add("Id", smallPackageServiceMap.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Small Package Service Mapping is not unique"));
            return isUnique;
        }
    }
}
