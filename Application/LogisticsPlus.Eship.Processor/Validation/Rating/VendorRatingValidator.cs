﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Processor.Validation.Rating
{
    public class VendorRatingValidator : ValidatorBase
    {
        public bool CanDeleteVendorRating(VendorRating vendorRating)
        {
            const string query = "select dbo.VendorRatingIdUsageCount(@Id)";
            var parameters = new Dictionary<string, object> { { "Id", vendorRating.Id } };
            var used = ExecuteScalar(query, parameters).ToInt() > 0;
            if (used) Messages.Add(ValidationMessage.Error("Vendor rating is referenced one or more times and cannot be deleted."));
            return !used;
        }

        public bool IsValid(VendorRating rating)
        {
            bool valid = HasAllRequiredData(rating);

            if (DuplicateVendorRatingnName(rating)) valid = false;

            return valid;
        }

        public bool DuplicateVendorRatingnName(VendorRating rating)
        {
            const string query = "Select count(*) from VendorRating where [Name] = @Name and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Name", rating.Name},
			                 		{"TenantId", rating.TenantId},
			                 		{"Id", rating.Id}
			                 	};
            var duplicateName = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateName) Messages.Add(ValidationMessage.Error("Vendor rating name is already in use"));
            return duplicateName;
        }
	

        public bool HasAllRequiredData(VendorRating rating)
        {
            var valid = true;

            if (rating.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor rating requires a tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(rating.Name))
            {
                Messages.Add(ValidationMessage.Error("Vendor rating requires a name"));
                valid = false;
            }
            else if (rating.Name.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(rating.DisplayName) && rating.DisplayName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating display name cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(rating.OverrideAddress) && rating.OverrideAddress.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating override address cannot exceed 100 characters"));
                valid = false;
            }

            if (!((int)rating.FuelIndexRegion).EnumValueIsValid<FuelIndexRegion>())
            {
                Messages.Add(ValidationMessage.Error("Vendor rating fuel index region is invalid"));
                valid = false;
            }

            if (rating.VendorId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor rating requires a vendor association"));
                valid = false;
            }

            if (rating.CurrentLTLFuelMarkup < 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating current LTL Fuel Markup must be greater than or equal to zero"));
                valid = false;
            }

            if (rating.FuelMarkupCeiling < 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL Fuel Markup ceiling must be greater than or equal to zero"));
                valid = false;
            }

            if (rating.FuelMarkupFloor < 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL Fuel Markup floor must be greater than or equal to zero"));
                valid = false;
            }

            if (rating.FuelMarkupFloor > rating.FuelMarkupCeiling)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL Fuel Markup floor cannot be greater than Fuel Markup ceiling"));
                valid = false;
            }

            if (!((int)rating.FuelUpdatesOn).EnumValueIsValid<DayOfWeek>())
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL day of fuel update is invalid"));
                valid = false;
            }

            if (rating.FuelChargeCodeId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor rating requires a charge code association for fuel"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(rating.LTLTariff) && rating.LTLTariff.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL tarriff cannot exceed 50 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(rating.Smc3ServiceLevel) && rating.Smc3ServiceLevel.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor rating Smc 3 Service Level cannot exceed 50 characters"));
				valid = false;
			}

            if (rating.LTLTruckLength <= 0 || rating.LTLTruckLength > 9999.9999m)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL truck maximum length must be between zero and 9999.9999"));
                valid = false;
            }

            if (rating.LTLTruckWidth <= 0 || rating.LTLTruckWidth > 9999.9999m)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL truck maximum width must be between zero and 9999.9999"));
                valid = false;
            }

            if (rating.LTLTruckHeight <= 0 || rating.LTLTruckHeight > 9999.9999m)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL truck maximum height must be between zero and 9999.9999"));
                valid = false;
            }

            if (rating.LTLTruckWeight <= 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL truck maximum weight must be greater than zero"));
                valid = false;
            }

            if (rating.LTLMinPickupWeight < 0 || rating.LTLMinPickupWeight > 9999.9999m)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL minimum pickup weight must be between zero and 9999.9999"));
                valid = false;
            }

            if (rating.LTLTruckUnitWeight <= 0 || rating.LTLTruckUnitWeight > 9999.9999m)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL individual unit maximum weight must be between zero and 9999.9999"));
                valid = false;
            }

            if (rating.LTLMaxCfForPcfConv < 0 || rating.LTLMaxCfForPcfConv > 9999.9999m)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL maximum cubic foot for Pcf conversion must be greater than zero and less than 9999.9999"));
                valid = false;
            }

            if (rating.EnableLTLPcfToFcConversion && rating.LTLMaxCfForPcfConv <= 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating LTL maximum cubic foot for Pcf conversion must be greater than zero  when conversion function is enabled"));
                valid = false;
            }

            if (rating.HasAdditionalCharges && rating.LTLAdditionalCharges.Count == 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating setup indicates additional charges but none have been added"));
                valid = false;
            }

            if (rating.HasLTLCubicFootCapacityRules)
            {
                if (rating.LTLCubicFootCapacityRules.Count == 0)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating setup indicates LTL cubic capacity rules but none have been added"));
                    valid = false;
                }


                if (rating.CubicCapacityPenaltyWidth <= 0 || rating.CubicCapacityPenaltyWidth > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL truck cubic capacity rules penalty width must be greater than zero but less than or equal to 9999.9999"));
                    valid = false;
                }

                if (rating.CubicCapacityPenaltyHeight <= 0 || rating.CubicCapacityPenaltyHeight > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL truck cubic capacity rules penalty height must be greater than zero but less than or equal to 9999.9999"));
                    valid = false;
                }
            }
            else
            {
                if (rating.CubicCapacityPenaltyWidth < 0 || rating.CubicCapacityPenaltyWidth > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL truck cubic capacity rules penalty width must be between zero and 9999.9999"));
                    valid = false;
                }

                if (rating.CubicCapacityPenaltyHeight < 0 || rating.CubicCapacityPenaltyHeight > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL truck cubic capacity rules penalty height must be between zero and 9999.9999"));
                    valid = false;
                }
            }

            if (rating.HasLTLOverLengthRules && rating.LTLOverLengthRules.Count == 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating setup indicates LTL overlength rules but none have been added"));
                valid = false;
            }

            if (rating.LTLOverLengthRules.Count > 0)
            {
                if (!LTLOverLengthRulesAreValid(rating.LTLOverLengthRules)) valid = false;
            }

            if (rating.EnableLTLPackageSpecificRates && rating.LTLPackageSpecificRatePackageTypeId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor rating package specific rates package type must be chosen when package specific rates are enabled"));
                valid = false;
            }

            if (rating.EnableLTLPackageSpecificRates && rating.LTLPackageSpecificFreightChargeCodeId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor rating package specific rates freight charge code must be chosen when package specific rates are enabled"));
                valid = false;
            }
            if (rating.MaxPackageQuantity < 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating max package quantity must be greater than or equal to zero"));
                valid = false;
            }
            if (rating.MaxItemLength < 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating max item length must be greater than or equal to zero"));
                valid = false;
            }
            if (rating.MaxItemWidth < 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating max item width must be greater than or equal to zero"));
                valid = false;
            }
            if (rating.MaxItemHeight < 0)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating max item height must be greater than or equal to zero"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(rating.RatingDetailNotes) && rating.RatingDetailNotes.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Vendor rating rating anecdotes cannot exceed 500 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(rating.CriticalBOLNotes) && rating.CriticalBOLNotes.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor rating rating critical bill of lading notes cannot exceed 50 characters"));
				valid = false;
			}

            if (!string.IsNullOrEmpty(rating.Project44TradingPartnerCode) && rating.Project44TradingPartnerCode.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Project44 Trading Partner Code cannot exceed 50 characters"));
                valid = false;
            }

            if (rating.LTLCubicFootCapacityRules.Count > 0 && !LTLCubicFootCapacityRulesAreValid(rating.LTLCubicFootCapacityRules)) valid = false;
            if (rating.LTLAdditionalCharges.Count > 0 && !LTLAdditionalChargesAreValid(rating.LTLAdditionalCharges)) valid = false;
            if (rating.LTLDiscountTiers.Count > 0 && !DiscountTiersAreValid(rating.LTLDiscountTiers)) valid = false;
            if (rating.LTLAccessorials.Count > 0 && !LTLAccessorialsAreValid(rating.LTLAccessorials)) valid = false;
            if (rating.LTLPcfToFcConvTab.Count > 0 && !LTLPcfToFcConvTabsAreValid(rating.LTLPcfToFcConvTab)) valid = false;
            if (rating.LTLPackageSpecificRates.Count > 0 && !LTLPackageSpecificRatesAreValid(rating.LTLPackageSpecificRates)) valid = false;
            if (rating.LTLGuaranteedCharges.Count > 0 && !LTLGuaranteedChargesAreValid(rating.LTLGuaranteedCharges)) valid = false;

            return valid;
        }

        private bool LTLOverLengthRulesAreValid(IEnumerable<LTLOverLengthRule> ltlOverLengthRules)
        {
            var valid = true;

            foreach (var rule in ltlOverLengthRules)
            {
                if (rule.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Overlength rule requires a tenant association"));
                    valid = false;
                }

                if (rule.VendorRating == null)
                {
                    Messages.Add(
                        ValidationMessage.Error("Vendor rating LTL Overlength rule requires a vendor rating association"));
                    valid = false;
                }

                if (rule.LowerLengthBound < 0 || rule.LowerLengthBound > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error("Vendor rating LTL Overlength rule lower length bound must be between 0 and 9999.9999"));
                    valid = false;
                }

                if (rule.UpperLengthBound < 0 || rule.UpperLengthBound > 9999.9999m)
                {
                    Messages.Add(
                        ValidationMessage.Error("Vendor rating LTL Overlength rule upper length bound must be between 0 and 9999.9999"));
                    valid = false;
                }

                if (rule.LowerLengthBound > rule.UpperLengthBound)
                {
                    Messages.Add(
                        ValidationMessage.Error(
                            "Vendor rating LTL Overlength rule lower length bound cannot be greater than upper length bound"));
                    valid = false;
                }

                if (rule.Charge < 0)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Overlength rule charge cannot be less than zero"));
                    valid = false;
                }

                if (rule.ChargeCodeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Overlength rule requires a charge code association"));
                    valid = false;
                }

                if (!rule.EffectiveDate.IsValidSystemDateTime())
                {
                    Messages.Add(
                        ValidationMessage.Error("Vendor rating LTL Overlength rule must have a valid effective date between {0} and {1}",
                                                DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }
            }

            return valid;
        }

        private bool LTLCubicFootCapacityRulesAreValid(IEnumerable<LTLCubicFootCapacityRule> ltlCubicFootCapacityRules)
        {
            var valid = true;

            foreach (var rule in ltlCubicFootCapacityRules)
            {
                if (rule.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Cubic Foot Capacity rule requires a tenant association"));
                    valid = false;
                }

                if (rule.VendorRating == null)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Cubic Foot Capacity rule requires a vendor rating association"));
                    valid = false;
                }

                if (rule.LowerBound < 0 || rule.LowerBound > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Cubic Foot Capacity rule lower bound must be between 0 and 9999.9999"));
                    valid = false;
                }

                if (rule.UpperBound < 0 || rule.UpperBound > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Cubic Foot Capacity rule upper bound must be between 0 and 9999.9999"));
                    valid = false;
                }

                if (rule.LowerBound > rule.UpperBound)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Cubic Foot Capacity rule lower bound cannot be greater than  rule upper bound"));
                    valid = false;
                }

                if (rule.AveragePoundPerCubicFootLimit < 0 || rule.AveragePoundPerCubicFootLimit > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Cubic Foot Capacity rule avg lb/cubic foot limit must be between 0 and 9999.9999"));
                    valid = false;
                }

                if (rule.PenaltyPoundPerCubicFoot < 0 || rule.PenaltyPoundPerCubicFoot > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Cubic Foot Capacity rule penalty lb/cubic foot must be between 0 and 9999.9999"));
                    valid = false;
                }


                if (!rule.EffectiveDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Cubic Foot Capacity rule must have a valid effective date between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }

            }

            return valid;
        }

        private bool LTLAdditionalChargesAreValid(IEnumerable<LTLAdditionalCharge> ltlAdditionalCharges)
        {
            var valid = true;
			var ltlAdditionalChargesNames = new List<string>();

			if (ltlAdditionalCharges != null && ltlAdditionalCharges.Any())
			{
				ltlAdditionalChargesNames = ltlAdditionalCharges.Select(c => c.Name.ToLower().Trim()).ToList();
			}

            foreach (var charge in ltlAdditionalCharges)
            {
                if (charge.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge requires a tenant association"));
                    valid = false;
                }

                if (charge.VendorRating == null)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge requires a vendor rating association"));
                    valid = false;
                }

                if (charge.ChargeCodeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge requires a charge code association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(charge.Name))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge requires a name"));
                    valid = false;
                }
                else if (charge.Name.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge name cannot exceed 50 characters"));
                    valid = false;
                }
				else if ((ltlAdditionalChargesNames.Count(h => h == charge.Name.ToLower().Trim()) > 1))
                {
					Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge Name should be unique."));
					valid = false;
                }

                if (!charge.EffectiveDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge must have a valid effective date between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }

                if (charge.AdditionalChargeIndices.Count > 0 && !LTLAdditionalChargeIndicesAreValid(charge.AdditionalChargeIndices))
                    valid = false;
            }

            return valid;
        }

        private bool LTLAdditionalChargeIndicesAreValid(IEnumerable<AdditionalChargeIndex> additionalChargeIndices)
        {
            var valid = true;

            foreach (var index in additionalChargeIndices)
            {
                if (index.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge index requires a tenant association"));
                    valid = false;
                }

                if (index.LTLAdditionalCharge == null)
                {
                    Messages.Add(
                        ValidationMessage.Error("Vendor rating LTL additional charge index requires a LTL additional charge association"));
                    valid = false;
                }

                if (index.UsePostalCode && string.IsNullOrEmpty(index.PostalCode))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge index requires a postal code"));
                    valid = false;
                }

                if (!string.IsNullOrEmpty(index.PostalCode) && index.PostalCode.Length > 10)
                {
                    Messages.Add(
                        ValidationMessage.Error("Vendor rating LTL additional charge index postal code cannot exceed 10 characters"));
                    valid = false;
                }

                if (!index.UsePostalCode && index.RegionId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge index requires a region association"));
                    valid = false;
                }

                if (index.UsePostalCode && index.CountryId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL additional charge index requires a country association"));
                    valid = false;
                }

                if (!((int)index.RateType).EnumValueIsValid<RateType>() ||
                    (index.RateType != RateType.Flat && index.RateType != RateType.PerHundredWeight &&
                     index.RateType != RateType.LineHaulPercentage))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL charge index rate type is invalid"));
                    valid = false;
                }

                if (index.Rate < 0)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL charge rate must be greater than or equal to zero"));
                    valid = false;
                }

                if (index.ChargeFloor < 0)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL charge rate floor value must be greater than or equal to zero"));
                    valid = false;
                }

                if (index.ChargeCeiling < 0)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL charge rate ceiling value must be greater than or equal to zero"));
                    valid = false;
                }

                if (index.ChargeFloor > index.ChargeCeiling)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL charge rate floor value cannot be greater than ceiling value"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool LTLAccessorialsAreValid(IEnumerable<LTLAccessorial> ltlAccessorials)
        {
            var valid = true;

            foreach (var item in ltlAccessorials)
            {
                if (item.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL accessorial requires a tenant association"));
                    valid = false;
                }

                if (item.VendorRating == null)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL accessorial requires a vendor rating association"));
                    valid = false;
                }

                if (item.ServiceId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL accessorial requires a service association"));
                    valid = false;
                }

                if (!((int)item.RateType).EnumValueIsValid<RateType>() || (item.RateType != RateType.Flat && item.RateType != RateType.PerHundredWeight))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL accessorial rate type is invalid"));
                    valid = false;
                }

                if (item.Rate < 0)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL accessorial rate must be greater than or equal to zero"));
                    valid = false;
                }

                if (item.FloorValue < 0)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL accessorial rate floor value must be greater than or equal to zero"));
                    valid = false;
                }

                if (item.CeilingValue < 0)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL accessorial rate ceiling value must be greater than or equal to zero"));
                    valid = false;
                }

                if (item.FloorValue > item.CeilingValue)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL accessorial rate floor value cannot be greater than ceiling value"));
                    valid = false;
                }

                if (!item.EffectiveDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL accessorial must have a valid effective date between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }
            }

            return valid;
        }

        private bool LTLPcfToFcConvTabsAreValid(IEnumerable<LTLPcfToFcConversion> ltlPcfToFcConvs)
        {
            var valid = true;

            foreach (var item in ltlPcfToFcConvs)
            {
                if (item.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Pcf to Fc Conv. requires a tenant association"));
                    valid = false;
                }

                if (item.VendorRating == null)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Pcf to Fc Conv. requires a vendor rating association"));
                    valid = false;
                }

                if (!item.EffectiveDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL accessorial must have a valid effective date between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }

                var convTab = item.ConversionTable;
                if (convTab == null)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Pcf to Fc Conv. requires a conversion table"));
                    valid = false;
                }
                else
                {
                    if (convTab.Length == 0)
                    {
                        Messages.Add(ValidationMessage.Error("Vendor rating LTL Pcf to Fc Conv. is invalid"));
                        valid = false;
                        continue;
                    }
                    if (convTab.Length < 1)
                    {
                        Messages.Add(ValidationMessage.Error("Vendor rating LTL Pcf to Fc Conv. must have a header line and at least one detail line"));
                        valid = false;
                        continue;
                    }

                    for (var i = 0; i < convTab.Length; i++)
                    {
                        var parts = convTab[i];

                        if (parts.Length < 3)
                        {
                            Messages.Add(ValidationMessage.Error("Vendor rating LTL Pcf to Fc Conv. table line {0} is invalid.", i + 1));
                            valid = false;
                            continue;
                        }

                        if (parts[LTLPcfToFcConversion.PcfLowerIndex].ToDecimal() < 0 || parts[LTLPcfToFcConversion.PcfLowerIndex].ToDecimal() > 999.9999m)
                        {
                            Messages.Add(ValidationMessage.Error("Vendor rating LTL Pcf to Fc Conv. table line {0} must have a Pcf Lower between 0 and 999.9999", i + 1));
                            valid = false;
                        }

                        if (parts[LTLPcfToFcConversion.PcfUpperIndex].ToDecimal() < 0 || parts[LTLPcfToFcConversion.PcfUpperIndex].ToDecimal() > 999.9999m)
                        {
                            Messages.Add(ValidationMessage.Error("Vendor rating LTL Pcf to Fc Conv. table line {0} must have a Pcf Upper between 0 and 999.9999", i + 1));
                            valid = false;
                        }

                        if (parts[LTLPcfToFcConversion.PcfLowerIndex].ToDecimal() > parts[LTLPcfToFcConversion.PcfUpperIndex].ToDecimal())
                        {
                            Messages.Add(ValidationMessage.Error("Vendor rating LTL Pcf to Fc Conv. table line {0} has Pcf Lower greater than Pcf Upper", i + 1));
                            valid = false;
                        }
                    }

                }
            }

            return valid;
        }

        private bool DiscountTiersAreValid(IEnumerable<DiscountTier> ltlDiscountTiers)
        {
            var valid = true;

            foreach (var tier in ltlDiscountTiers)
            {
                if (tier.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier requires a tenant association"));
                    valid = false;
                }

                if (tier.VendorRating == null)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier requires a vendor rating association"));
                    valid = false;
                }

                if (tier.OriginRegionId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier requires origin region if postal code is not to be used"));
                    valid = false;
                }
                if (tier.DestinationRegionId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier requires destination region if postal code is not to be used"));
                    valid = false;
                }

                if (tier.DiscountPercent < 0 || tier.DiscountPercent > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier must have a discount percent between 0 and 9999.9999"));
                    valid = false;
                }

                if (tier.FloorValue < 0)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier must have a floor value greater than zero"));
                    valid = false;
                }

                if (tier.CeilingValue < 0)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier must have a floor value greater than zero"));
                    valid = false;
                }

                if (tier.FloorValue > tier.CeilingValue)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier floor value cannot be greater than ceiling value"));
                    valid = false;
                }

                if (tier.FreightChargeCodeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier requires a freight charge code association"));
                    valid = false;
                }

                if (!tier.EffectiveDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier must have a valid effective date between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }

                if (!tier.RateBasis.ToInt().EnumValueIsValid<DiscountTierRateBasis>())
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier rate basis is invalid"));
                    valid = false;
                }

                if (!tier.WeightBreak.ToInt().EnumValueIsValid<WeightBreak>())
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL discount tier discount applies up to weight break is invalid"));
                }
            }

            return valid;
        }

        private bool LTLPackageSpecificRatesAreValid(IEnumerable<LTLPackageSpecificRate> psRates)
        {
            var valid = true;

            foreach (var psRate in psRates)
            {
                if (psRate.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Package Specific Rate requires a tenant association"));
                    valid = false;
                }
                if (psRate.OriginRegionId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Package Specific Rate requires origin region association"));
                    valid = false;
                }
                if (psRate.DestinationRegionId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Package Specific Rate requires destination region association"));
                    valid = false;
                }
                if (psRate.VendorRating == null)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Package Specific Rate requires a vendor rating association"));
                    valid = false;
                }

                if (psRate.Rate < 0 || psRate.Rate > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Package Specific Rate must have a flat rate between 0 and 9999.9999"));
                    valid = false;
                }
                if (psRate.PackageQuantity < 0 || psRate.PackageQuantity > 9999)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL Package Specific Rate must have a package quantity between 0 and 9999"));
                    valid = false;
                }
                if (!psRate.EffectiveDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating  LTL Package Specific Rate must have a valid effective date between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }
            }

            return valid;
        }

        private bool LTLGuaranteedChargesAreValid(IEnumerable<LTLGuaranteedCharge> guaranteedCharges)
        {
	        var valid = DuplicateGuaranteedChargeTimes(guaranteedCharges.ToList());

            foreach (var item in guaranteedCharges)
            {
                if (item.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge requires a tenant association"));
                    valid = false;
                }

                if (item.VendorRating == null)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge requires a vendor rating association"));
                    valid = false;
                }

                if (item.ChargeCodeId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge requires a charge code association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(item.Description))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge requires a description"));
                    valid = false;
                }
                else if (item.Description.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge descripton cannot exceed 50 characters"));
                    valid = false;
                }

                if (!((int)item.RateType).EnumValueIsValid<RateType>() || (item.RateType != RateType.Flat && item.RateType != RateType.LineHaulPercentage))
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge rate type is invalid"));
                    valid = false;
                }

                if (item.Rate < 0 || item.Rate > 999999999999999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge rate must be between 0 and 99999999999999.9999"));
                    valid = false;
                }

                if (item.FloorValue < 0 || item.FloorValue > 999999999999999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge rate floor value must be between 0 and 99999999999999.9999"));
                    valid = false;
                }

                if (item.CeilingValue < 0 || item.CeilingValue > 999999999999999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge rate ceiling value must be between 0 and 99999999999999.9999"));
                    valid = false;
                }

                if (item.FloorValue > item.CeilingValue)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge rate floor value cannot be greater than ceiling value"));
                    valid = false;
                }

                if (!item.EffectiveDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge must have a valid effective date between {0} and {1}",
                                                         DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }

                if (!item.Time.IsValidFreeFormTimeString())
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge time is invalid"));
                    valid = false;
                }
                if (!string.IsNullOrEmpty(item.CriticalNotes) && item.CriticalNotes.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge critical notes cannot exceed 50 characters"));
                    valid = false;
                }
            }

            return valid;
        }

        private bool DuplicateGuaranteedChargeTimes(List<LTLGuaranteedCharge> guaranteedCharges)
        {
            var valid = true;

            foreach (var gCharge in guaranteedCharges)
                if (guaranteedCharges.Count(c => c.Time == gCharge.Time) > 1)
                    valid = false;
                
            if (!valid) Messages.Add(ValidationMessage.Error("Vendor rating LTL guaranteed charge times must be unique for this vendor rating"));

            return valid;
        }
    }
}