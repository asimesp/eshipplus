﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Processor.Validation.Rating
{
	public class RegionValidator : ValidatorBase
	{
		public bool CanDeleteRegion(Region region)
		{
			const string query = "select dbo.RegionIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", region.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Region is referenced one or more times and cannot be deleted."));
			return !used;
		}

		public bool IsValid(Region region)
		{
			var valid = true;

			if (!HasAllRequiredData(region)) valid = false;
			if (DuplicateRegionName(region)) valid = false;
			if (!HasNoRecursiveRegionDependencies(region.Areas.Where(a => a.SubRegionId != default(long)).Select(a => a.SubRegion).ToList(), new List<long>()))
			{
				Messages.Add(ValidationMessage.Error(
					"Endless recursive sub region nesting detected: One of the area sub regions has this region contained as a sub region within it"));
				valid = false;
			}


			return valid;
		}

		public bool DuplicateRegionName(Region region)
		{
			const string query = "SELECT COUNT(*) FROM [Region] WHERE [Name] = @Name AND TenantId = @TenantId AND Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Name", region.Name},
			                 		{"TenantId", region.TenantId},
			                 		{"Id", region.Id}
			                 	};
			var duplicateRegionName = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateRegionName) Messages.Add(ValidationMessage.Error("Region name is already in use"));
			return duplicateRegionName;
		}

		public bool HasAllRequiredData(Region region)
		{
			var valid = true;

			if (string.IsNullOrEmpty(region.Name))
			{
				Messages.Add(ValidationMessage.Error("Region requires a name"));
				valid = false;
			}
			else if (region.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Region name cannot exceed 50 characters"));
				valid = false;
			}

			if (region.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Region requires a tenant association"));
				valid = false;
			}

			if(region.Areas.Count == 0)
			{
				Messages.Add(ValidationMessage.Error("Region must have at least one area"));
				valid = false;
			}

			if (!HasValidAreas(region.Areas)) valid = false;
			
			return valid;
		}

		private bool HasValidAreas(IEnumerable<Area> areas)
		{
			var valid = true;

			foreach(var index in areas)
			{
				if (index.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Region area requires a tenant association"));
					valid = false;
				}

				if (index.Region == null)
				{
					Messages.Add(ValidationMessage.Error("Region area requires a Region association"));
					valid = false;
				}

				if(index.UseSubRegion && index.SubRegionId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Region area required a sub region"));
					valid = false;
				}

				if (!index.UseSubRegion && (String.IsNullOrEmpty(index.PostalCode) || index.CountryId == default(long)))
				{
					Messages.Add(ValidationMessage.Error("Region area required a postal code and country"));
					valid = false;
				}
			}

			return valid;
		}

		private bool HasNoRecursiveRegionDependencies(ICollection<Region> subRegions, List<long> nestedRegionIds)
		{
			if (subRegions.Count == 0) return true;	// end of recursion

			// if any subregion Id is previous nested return false
			if (subRegions.Any(r => nestedRegionIds.Contains(r.Id))) return false;

			// passes, added check id's to nesting loop
			nestedRegionIds.AddRange(subRegions.Select(r => r.Id));

			var regions = new List<Region>();
			foreach (var subRegion in subRegions)
				regions.AddRange(subRegion.Areas.Where(a => a.SubRegionId != default(long)).Select(a => a.SubRegion));
			

			return HasNoRecursiveRegionDependencies(regions, nestedRegionIds);
		}
	}
}
