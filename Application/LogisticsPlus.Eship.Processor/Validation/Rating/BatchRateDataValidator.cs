﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Rating
{
	public class BatchRateDataValidator : ValidatorBase
	{
		public bool IsValid(BatchRateData data)
		{
			var valid = true;

			if (!HasAllRequiredData(data)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(BatchRateData data)
		{
			var valid = true;

			if (data.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Batch rate data requires a tenant association"));
				valid = false;
			}

			if (data.SubmittedByUserId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Batch rate data requires a customer association"));
				valid = false;
			}

			if (!data.ServiceMode.ToInt().EnumValueIsValid<ServiceMode>())
			{
				Messages.Add(ValidationMessage.Error("Batch rate data must have a valid service mode"));
				valid = false;
			}
			else
			{
				switch (data.ServiceMode)
				{
					case ServiceMode.LessThanTruckload:
						if (data.VendorRatingId == default(long))
						{
							Messages.Add(ValidationMessage.Error("Batch rate data requires a vendor rating profile for LTL rate analysis"));
							valid = false;
						}
						break;
					case ServiceMode.Truckload:
						if (data.VendorRatingId == default(long))
						{
							Messages.Add(ValidationMessage.Error("Batch rate data requires a vendor rating profile for TL rate analysis"));
							valid = false;
						}
						if (data.ChargeCodeId == default(long))
						{
							Messages.Add(ValidationMessage.Error("Batch rate data requires a charge code for LTL rate analysis"));
							valid = false;
						}
						break;
					case ServiceMode.SmallPackage:
						if (data.PackageTypeId == default(long))
						{
							Messages.Add(ValidationMessage.Error("Batch rate data requires a package type for small pack rate analysis"));
							valid = false;
						}
						if (string.IsNullOrEmpty(data.SmallPackageEngineType))
						{
							Messages.Add(ValidationMessage.Error("Batch rate data requires a small package engine type for small pack rate analysis"));
							valid = false;
						}
						break;
				}
			}

			if (!data.SmallPackageEngine.ToInt().EnumValueIsValid<SmallPackageEngine>())
			{
				Messages.Add(ValidationMessage.Error("Batch rate data must have a valid small package engine"));
				valid = false;
			}

			if (!data.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Batch rate data date created must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!data.AnalysisEffectiveDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Batch rate data analysis effective date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!data.DateCompleted.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Batch rate data date completed must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!string.IsNullOrEmpty(data.SmallPackageEngineType) && data.SmallPackageEngineType.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Batch rate data small package engine type cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(data.Name) && data.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Batch rate data name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(data.GroupCode) && data.GroupCode.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Batch rate group code cannot exceed 50 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(data.LaneData))
			{
				Messages.Add(ValidationMessage.Error("Batch rate data must have lane data present"));
				valid = false;
			}

			return valid;
		}
	}
}
