﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Processor.Validation.Rating
{
    public class SmallPackagingMapValidator : ValidatorBase
    {
        public bool IsValid(SmallPackagingMap smallPackagingMap)
        {
            Messages.Clear();

            var valid = true;

            if (!IsUnique(smallPackagingMap)) valid = false;
            if (!HasAllRequiredData(smallPackagingMap)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(SmallPackagingMap smallPackagingMap)
        {
            var valid = true;

            if (smallPackagingMap.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Small Package Mapping requires a tenant association"));
                valid = false;
            }

            if (smallPackagingMap.PackageTypeId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Small Package Mapping requires a Package Type association"));
                valid = false;
            }

			if (!smallPackagingMap.SmallPackageEngine.ToInt().EnumValueIsValid<SmallPackageEngine>())
			{
				Messages.Add(ValidationMessage.Error("Small Package Mapping must have a valid small pack engine"));
				valid = false;
			}

			if (smallPackagingMap.SmallPackageEngine == SmallPackageEngine.None)
			{
				Messages.Add(ValidationMessage.Error("Small Package Mapping must have a valid small pack engine"));
				valid = false;
			}

            if (string.IsNullOrEmpty(smallPackagingMap.SmallPackEngineType))
            {
                Messages.Add(ValidationMessage.Error("Small Package Mapping requires a Small Package Engine Type"));
                valid = false;
            }
            else if (smallPackagingMap.SmallPackEngineType.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Small Package Mapping Engine Type cannot exceed 50 characters"));
                valid = false;
            }
            return valid;
        }

        public bool IsUnique(SmallPackagingMap smallPackagingMap)
        {
            var query = smallPackagingMap.IsNew
                            ? "SELECT COUNT(*) FROM SmallPackagingMap WHERE PackageTypeID = @PackageTypeID AND SmallPackageEngine = @SmallPackageEngine AND TenantId = @TenantId"
                            : "SELECT COUNT(*) FROM SmallPackagingMap WHERE PackageTypeID = @PackageTypeID AND SmallPackageEngine = @SmallPackageEngine AND TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"PackageTypeID", smallPackagingMap.PackageTypeId},
			                 		{"SmallPackageEngine", smallPackagingMap.SmallPackageEngine},
			                 		{"TenantId", smallPackagingMap.TenantId},
			                 	};
            if (!smallPackagingMap.IsNew) parameters.Add("Id", smallPackagingMap.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Small Packaging Map is not unique"));
            return isUnique;
        }
    }
}
