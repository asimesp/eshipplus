﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Processor.Validation.Rating
{
    public class ResellerAdditionValidator : ValidatorBase
    {
		public bool CanDeleteResellerAddition(ResellerAddition resellerAddition)
		{
			const string query = "select dbo.ResellerAdditionIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", resellerAddition.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Reseller Addition is referenced one or more times and cannot be deleted."));
			return !used;
		}

        public bool IsValid(ResellerAddition resellerAddition)
        {
            Messages.Clear();

            var valid = true;

            if (!HasAllRequiredData(resellerAddition)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(ResellerAddition resellerAddition)
        {
            var valid = true;

            if (string.IsNullOrEmpty(resellerAddition.Name))
            {
                Messages.Add(ValidationMessage.Error("Reseller Addition requires a Name"));
                valid = false;
            }
            else if (resellerAddition.Name.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Reseller Addition Name cannot exceed 50 characters"));
                valid = false;
            }

			if (resellerAddition.ResellerCustomerAccountId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Reseller Addition requires a customer account association"));
				valid = false;
			}

            if (resellerAddition.LineHaulPercentage < 0)
            {
                Messages.Add(ValidationMessage.Error("Reseller Addition Line Haul Percentage cannot be less than zero"));
                valid = false;
            }

            if (resellerAddition.LineHaulValue < 0)
            {
                Messages.Add(ValidationMessage.Error("Reseller Addition Line Haul Value cannot be less than zero"));
                valid = false;
            }

            if (resellerAddition.FuelPercentage < 0)
            {
                Messages.Add(ValidationMessage.Error("Reseller Addition Fuel Percentage cannot be less than zero"));
                valid = false;
            }

            if (resellerAddition.FuelValue < 0)
            {
                Messages.Add(ValidationMessage.Error("Reseller Addition Fuel Value cannot be less than zero"));
                valid = false;
            }

            if (resellerAddition.AccessorialPercentage < 0)
            {
                Messages.Add(ValidationMessage.Error("Reseller Addition Accessorial Percentage cannot be less than zero"));
                valid = false;
            }

            if (resellerAddition.AccessorialValue < 0)
            {
                Messages.Add(ValidationMessage.Error("Reseller Addition Accessorial Value cannot be less than zero"));
                valid = false;
            }

            if (resellerAddition.ServicePercentage < 0)
            {
                Messages.Add(ValidationMessage.Error("Reseller Addition Service Percentage cannot be less than zero"));
                valid = false;
            }

            if (resellerAddition.ServiceValue < 0)
            {
                Messages.Add(ValidationMessage.Error("Reseller Addition Service Value cannot be less than zero"));
                valid = false;
            }

            return valid;
        }

        public bool IsUnique(ResellerAddition resellerAddition)
        {
            var query = resellerAddition.IsNew
                ? "SELECT COUNT(*) FROM ResellerAddition WHERE Name = @Name AND TenantId = @TenantId"
                : "SELECT COUNT(*) FROM ResellerAddition WHERE Name = @Name AND TenantId = @TenantId and Id <> @Id";
           
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{ "Name", resellerAddition.Name },
									{ "TenantId", resellerAddition.TenantId }
								};
            if (!resellerAddition.IsNew) parameters.Add("Id", resellerAddition.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Reseller Addition is not unique"));
            return isUnique;
        }
    }
}