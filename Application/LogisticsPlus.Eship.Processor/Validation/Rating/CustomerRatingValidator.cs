﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.SmallPacks;

namespace LogisticsPlus.Eship.Processor.Validation.Rating
{
	public class CustomerRatingValidator : ValidatorBase
	{
		public bool IsValid(CustomerRating rating)
		{
			bool valid = HasAllRequiredData(rating);

			return valid;
		}

		public bool HasAllRequiredData(CustomerRating rating)
		{
			var valid = true;

			if (rating.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Customer rating requires a tenant association"));
				valid = false;
			}

			if (rating.Customer == null)
			{
				Messages.Add(ValidationMessage.Error("Customer rating requires a customer association"));
				valid = false;
			}
			else
			{
				if(rating.Customer.Rating != null && rating.Customer.Rating.Id != rating.Id)
				{
					Messages.Add(ValidationMessage.Error("Customer rating customer association is invalid.  Customer is already associated with another rating profile"));
					valid = false;
				}
			}

			if (rating.InsuranceEnabled)
			{
				if (rating.InsurancePurchaseFloor < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating insurance purchase floor value must be greater than zero"));
					valid = false;
				}

				if (rating.InsuranceChargeCodeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer rating requires an insurance charge code association"));
					valid = false;
				}
			}

			if (rating.FuelProfitCeilingPercentage < 0 || rating.FuelProfitCeilingPercentage > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Customer rating fuel profit ceiling percentage must be between zero and 9999.9999"));
				valid = false;
			}

			if (rating.FuelProfitCeilingValue < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer rating fuel profit ceiling value must be greater than or equal to zero"));
				valid = false;
			}

			if (!rating.FuelProfitCeilingType.ToInt().EnumValueIsValid<ValuePercentageType>())
			{
				Messages.Add(ValidationMessage.Error("Customer rating fuel profit ceiling type is invalid"));
				valid = false;
			}

			if (rating.FuelProfitFloorPercentage < 0 || rating.FuelProfitFloorPercentage > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Customer rating fuel profit floor percentage must be between zero and 9999.9999"));
				valid = false;
			}

			if (rating.FuelProfitFloorValue < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer rating fuel profit floor value must be greater than or equal to zero"));
				valid = false;
			}

			if (!rating.FuelProfitFloorType.ToInt().EnumValueIsValid<ValuePercentageType>())
			{
				Messages.Add(ValidationMessage.Error("Customer rating fuel profit floor type is invalid"));
				valid = false;
			}

			if (rating.LineHaulProfitCeilingPercentage < 0 || rating.LineHaulProfitCeilingPercentage > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Customer rating line haul profit ceiling percentage must be between zero and 9999.9999"));
				valid = false;
			}

			if (rating.LineHaulProfitCeilingValue < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer rating line haul profit ceiling value must be greater than or equal to zero"));
				valid = false;
			}

			if (!rating.LineHaulProfitCeilingType.ToInt().EnumValueIsValid<ValuePercentageType>())
			{
				Messages.Add(ValidationMessage.Error("Customer rating line haul profit ceiling type is invalid"));
				valid = false;
			}

			if (rating.LineHaulProfitFloorPercentage < 0 || rating.LineHaulProfitFloorPercentage > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Customer rating line haul profit floor percentage must be between zero and 9999.9999"));
				valid = false;
			}

			if (rating.LineHaulProfitFloorValue < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer rating line haul profit floor value must be greater than or equal to zero"));
				valid = false;
			}

			if (!rating.LineHaulProfitFloorType.ToInt().EnumValueIsValid<ValuePercentageType>())
			{
				Messages.Add(ValidationMessage.Error("Customer rating line haul profit floor type is invalid"));
				valid = false;
			}

			if (rating.AccessorialProfitCeilingPercentage < 0 || rating.AccessorialProfitCeilingPercentage > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Customer rating accessorial profit ceiling percentage must be between zero and 9999.9999"));
				valid = false;
			}

			if (rating.AccessorialProfitCeilingValue < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer rating accessorial profit ceiling value must be greater than or equal to zero"));
				valid = false;
			}

			if (!rating.AccessorialProfitCeilingType.ToInt().EnumValueIsValid<ValuePercentageType>())
			{
				Messages.Add(ValidationMessage.Error("Customer rating accessorial profit ceiling type is invalid"));
				valid = false;
			}

			if (rating.AccessorialProfitFloorPercentage < 0 || rating.AccessorialProfitFloorPercentage > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Customer rating accessorial profit floor percentage must be between zero and 9999.9999"));
				valid = false;
			}

			if (rating.AccessorialProfitFloorValue < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer rating accessorial profit floor value must be greater than or equal to zero"));
				valid = false;
			}

			if (!rating.AccessorialProfitFloorType.ToInt().EnumValueIsValid<ValuePercentageType>())
			{
				Messages.Add(ValidationMessage.Error("Customer rating accessorial profit floor type is invalid"));
				valid = false;
			}

			if (rating.ServiceProfitCeilingPercentage < 0 || rating.ServiceProfitCeilingPercentage > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Customer rating service profit ceiling percentage must be between zero and 9999.9999"));
				valid = false;
			}

			if (rating.ServiceProfitCeilingValue < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer rating service profit ceiling value must be greater than or equal to zero"));
				valid = false;
			}

			if (!rating.ServiceProfitCeilingType.ToInt().EnumValueIsValid<ValuePercentageType>())
			{
				Messages.Add(ValidationMessage.Error("Customer rating service profit ceiling type is invalid"));
				valid = false;
			}

			if (rating.ServiceProfitFloorPercentage < 0 || rating.ServiceProfitFloorPercentage > 9999.9999m)
			{
				Messages.Add(ValidationMessage.Error("Customer rating service profit floor percentage must be between zero and 9999.9999"));
				valid = false;
			}

			if (rating.ServiceProfitFloorValue < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer rating service profit floor value must be greater than or equal to zero"));
				valid = false;
			}

			if (!rating.ServiceProfitFloorType.ToInt().EnumValueIsValid<ValuePercentageType>())
			{
				Messages.Add(ValidationMessage.Error("Customer rating service profit floor type is invalid"));
				valid = false;
			}

			if(rating.BillReseller && rating.ResellerAdditionId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Customer rating is setup to bill reseller and therefore requires a reseller association"));
				valid = false;
			}

			if (!rating.TLFuelRateType.ToInt().EnumValueIsValid<RateType>())
			{
				Messages.Add(ValidationMessage.Error("Customer rating TL fuel rate type is invalid"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(rating.Project44AccountGroup) && rating.Project44AccountGroup.Length > 50)
		    {
		        Messages.Add(ValidationMessage.Error("Customer rating Project 44 Account Group cannot exceed 50 characters"));
		        valid = false;
		    }


            if (rating.LTLSellRates.Count > 0 && !LTLSellRatesAreValid(rating.LTLSellRates)) valid = false;
			if (rating.TLSellRates.Count > 0 && !TLSellRatesAreValid(rating.TLSellRates)) valid = false;
			if (rating.SmallPackRates.Count > 0 && !LTLSmallPackRatesAreValid(rating.SmallPackRates)) valid = false;
			if (rating.CustomerServiceMarkups.Count > 0 && !CustomerServiceMarkupsAreValid(rating.CustomerServiceMarkups)) valid = false;

			return valid;
		}

		private bool CustomerServiceMarkupsAreValid(IEnumerable<CustomerServiceMarkup> customerServiceMarkups)
		{
			var valid = true;
			var checkDupDictionary = new Dictionary<DateTime, List<long>>();

			foreach (var markup in customerServiceMarkups)
			{
				if (markup.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer rating service markup requires a tenant association"));
					valid = false;
				}

				if (markup.CustomerRating == null)
				{
					Messages.Add(ValidationMessage.Error("Customer rating service markup requires a customer rating association"));
					valid = false;
				}

				if (markup.ServiceId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer rating service markup requires a service association"));
					valid = false;
				}

				if (!markup.EffectiveDate.IsValidSystemDateTime())
				{
					Messages.Add(
						ValidationMessage.Error("Customer rating service markup must have a valid effective date between {0} and {1}",
												DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
					valid = false;
				}

				if (markup.MarkupPercent < 0 || markup.MarkupPercent > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating service markup percentage must be between zero and 9999.9999"));
					valid = false;
				}

				if (markup.MarkupValue < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating service markup value must be greater than or equal to zero"));
					valid = false;
				}

				if (markup.ServiceChargeCeiling < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating service markup charge ceiling value must be greater than or equal to zero"));
					valid = false;
				}

				if (markup.ServiceChargeFloor < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating service markup charge floor value must be greater than or equal to zero"));
					valid = false;
				}

				if (!checkDupDictionary.ContainsKey(markup.EffectiveDate)) checkDupDictionary.Add(markup.EffectiveDate, new List<long>());
				if (!checkDupDictionary[markup.EffectiveDate].Contains(markup.ServiceId)) 
					checkDupDictionary[markup.EffectiveDate].Add(markup.ServiceId);
				else
				{
					Messages.Add(ValidationMessage.Error("Customer rating service markup has duplicate markup record {0} - {1}",
						markup.EffectiveDate.FormattedLongDateAlt(), markup.Service.FormattedString()));
					valid = false;
				}

			}

			return valid;
		}

		private bool LTLSmallPackRatesAreValid(IEnumerable<SmallPackRate> smallPackRates)
		{
			var valid = true;
			var checkDupDictionary = new Dictionary<DateTime, List<string>>();

			foreach (var rate in smallPackRates)
			{
				if (rate.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer rating small pack rate requires a tenant association"));
					valid = false;
				}

				if (rate.CustomerRating == null)
				{
					Messages.Add(ValidationMessage.Error("Customer rating small pack rate requires a customer rating association"));
					valid = false;
				}

                if (rate.VendorId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Customer rating small pack rate requires a vendor association"));
                    valid = false;
                }

				if(rate.ChargeCodeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer rating small pack rate requires a charge code association"));
					valid = false;
				}

				if (!((int)rate.SmallPackageEngine).EnumValueIsValid<SmallPackageEngine>() || rate.SmallPackageEngine == 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating small pack rate engine is invalid"));
					valid = false;
				}
				else
				{
					var engine = rate.SmallPackageEngine;
					var types = engine == SmallPackageEngine.None ? new List<string>() : SmallPacksFactory.RetrieveServiceTypes()[engine];
					if (types.Count > 0 && !types.Contains(rate.SmallPackType))
					{
						Messages.Add(ValidationMessage.Error("Customer rating small pack rate type is invalid"));
						valid = false;
					}
				}

				if (!rate.EffectiveDate.IsValidSystemDateTime())
				{
					Messages.Add(
						ValidationMessage.Error("Customer rating small pack rate must have a valid effective date between {0} and {1}",
												DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
					valid = false;
				}

				if (rate.MarkupPercent < 0 || rate.MarkupPercent > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating small pack rate markup percentage must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.MarkupValue < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating small pack rate markup value must be greater than or equal to zero"));
					valid = false;
				}

				var valueInDictionary = String.Concat(rate.SmallPackageEngine.ToString(), rate.SmallPackType, rate.VendorId.ToString());
				if (!checkDupDictionary.ContainsKey(rate.EffectiveDate)) checkDupDictionary.Add(rate.EffectiveDate, new List<string>());

				if (!checkDupDictionary[rate.EffectiveDate].Contains(valueInDictionary))
				{
					checkDupDictionary[rate.EffectiveDate].Add(valueInDictionary);
				}
				else
				{
					Messages.Add(ValidationMessage.Error("Small Pack Rates has duplicate record {0} - {1} - {2} - {3}",
						rate.EffectiveDate.FormattedLongDateAlt(), rate.SmallPackageEngine, rate.ChargeCode.Description, rate.Vendor.Name));
					valid = false;
				}

			}

			return valid;
		}

		private bool LTLSellRatesAreValid(IEnumerable<LTLSellRate> ltlSellRates)
		{
			var valid = true;
			var checkDupDictionary = new Dictionary<DateTime, List<long>>();

			foreach(var rate in ltlSellRates)
			{
				if (rate.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate requires a tenant association"));
					valid = false;
				}

				if (rate.CustomerRating == null)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate requires a customer rating association"));
					valid = false;
				}

				if (rate.VendorRatingId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate requires a vendor rating association"));
					valid = false;
				}

				if (!rate.EffectiveDate.IsValidSystemDateTime())
				{
					Messages.Add(
						ValidationMessage.Error("Customer rating ltl sell rate must have a valid effective date between {0} and {1}",
						                        DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
					valid = false;
				}

				if(rate.MarkupPercent < 0 || rate.MarkupPercent > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate markup percentage must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.MarkupValue < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate markup value must be greater than or equal to zero"));
					valid = false;
				}


				if (!rate.StartOverrideWeightBreak.ToInt().EnumValueIsValid<WeightBreak>())
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate start override weight break is invalid"));
				}


				if (rate.OverrideMarkupPercentL5C < 0 || rate.OverrideMarkupPercentL5C > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup percentage L5C must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.OverrideMarkupValueL5C < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup value L5C must be greater than or equal to zero"));
					valid = false;
				}

				if (rate.OverrideMarkupPercentM5C < 0 || rate.OverrideMarkupPercentM5C > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup percentage M5C must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.OverrideMarkupValueM5C < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup value M5C must be greater than or equal to zero"));
					valid = false;
				}

				if (rate.OverrideMarkupPercentM1M < 0 || rate.OverrideMarkupPercentM1M > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup percentage M1M must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.OverrideMarkupValueM1M < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup value M1M must be greater than or equal to zero"));
					valid = false;
				}

				if (rate.OverrideMarkupPercentM2M < 0 || rate.OverrideMarkupPercentM2M > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup percentage M2M must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.OverrideMarkupValueM2M < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup value M2M must be greater than or equal to zero"));
					valid = false;
				}

				if (rate.OverrideMarkupPercentM5M < 0 || rate.OverrideMarkupPercentM5M > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup percentage M5M must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.OverrideMarkupValueM5M < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup value M5M must be greater than or equal to zero"));
					valid = false;
				}

				if (rate.OverrideMarkupPercentM10M < 0 || rate.OverrideMarkupPercentM10M > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup percentage M10M must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.OverrideMarkupValueM10M < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup value M10M must be greater than or equal to zero"));
					valid = false;
				}

				if (rate.OverrideMarkupPercentM20M < 0 || rate.OverrideMarkupPercentM20M > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup percentage M20M must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.OverrideMarkupValueM20M < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup value M20M must be greater than or equal to zero"));
					valid = false;
				}

				if (rate.OverrideMarkupPercentM30M < 0 || rate.OverrideMarkupPercentM30M > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup percentage M30M must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.OverrideMarkupValueM30M < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup value M30M must be greater than or equal to zero"));
					valid = false;
				}

				if (rate.OverrideMarkupPercentM40M < 0 || rate.OverrideMarkupPercentM40M > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup percentage M40M must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.OverrideMarkupValueM40M < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup value M40M must be greater than or equal to zero"));
					valid = false;
				}

                if (rate.OverrideMarkupPercentVendorFloor < 0)
                {
                    Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup percent vendor floor must be greater than or equal to zero"));
                    valid = false;
                }

                if (rate.OverrideMarkupValueVendorFloor < 0)
                {
                    Messages.Add(ValidationMessage.Error("Customer rating ltl sell rate override markup value vendor floor must be greater than or equal to zero"));
                    valid = false;
                }


				if (!checkDupDictionary.ContainsKey(rate.EffectiveDate)) checkDupDictionary.Add(rate.EffectiveDate, new List<long>());
				if (!checkDupDictionary[rate.EffectiveDate].Contains(rate.VendorRatingId))
					checkDupDictionary[rate.EffectiveDate].Add(rate.VendorRatingId);
				else
				{
					Messages.Add(ValidationMessage.Error("LTL Sell Rates has duplicate record {0} - {1}",
						rate.EffectiveDate.FormattedLongDateAlt(), rate.VendorRating.DisplayName));
					valid = false;
				}
			}

			return valid;
		}

		private bool TLSellRatesAreValid(IEnumerable<TLSellRate> tlSellRates)
		{
			var valid = true;
			var checkDupDictionary = new Dictionary<DateTime, List<string>>();

			foreach (var rate in tlSellRates)
			{
				if (rate.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer rating tl sell rate requires a tenant association"));
					valid = false;
				}

				if (rate.CustomerRating == null)
				{
					Messages.Add(ValidationMessage.Error("Customer rating tl sell rate requires a customer rating association"));
					valid = false;
				}

				if (!rate.EffectiveDate.IsValidSystemDateTime())
				{
					Messages.Add(
						ValidationMessage.Error("Customer rating tl sell rate must have a valid effective date between {0} and {1}",
												DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
					valid = false;
				}

				if (!rate.RateType.ToInt().EnumValueIsValid<RateType>())
				{
					Messages.Add(ValidationMessage.Error("Customer rating TL sell rate type is invalid"));
					valid = false;
				}

				if (rate.Rate < 0 || rate.Rate > 9999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating tl sell rate rate must be between zero and 9999.9999"));
					valid = false;
				}

				if (rate.MinimumCharge < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating tl sell rate minimum charge must be greater than or equal to zero"));
					valid = false;
				}

				if (rate.MinimumWeight < 0 || rate.MinimumWeight > 99999999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating tl sell rate minimum weight must be between zero and 99999999.9999"));
					valid = false;
				}

				if (rate.MaximumWeight < 0 || rate.MaximumWeight > 99999999.9999m)
				{
					Messages.Add(ValidationMessage.Error("Customer rating tl sell rate minimum weight must be between zero and 99999999.9999"));
					valid = false;
				}

				if (rate.MinimumWeight > rate.MaximumWeight)
				{
					Messages.Add(ValidationMessage.Error("Customer rating tl sell rate minimum weight cannot be greater than maximum weight"));
					valid = false;
				}

				if (!rate.TariffType.ToInt().EnumValueIsValid<TLTariffType>())
				{
					Messages.Add(ValidationMessage.Error("Customer rating TL sell rate type is invalid"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(rate.OriginCity) && rate.OriginCity.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer rating TL sell rate origin city cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(rate.OriginState) && rate.OriginState.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer rating TL sell rate origin state cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(rate.OriginPostalCode) && rate.OriginPostalCode.Length > 10)
				{
					Messages.Add(ValidationMessage.Error("Customer rating TL sell rate origin postal code cannot exceed 10 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(rate.DestinationCity) && rate.DestinationCity.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer rating TL sell rate destination city cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(rate.DestinationState) && rate.DestinationState.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer rating TL sell rate destination state cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(rate.DestinationPostalCode) && rate.DestinationPostalCode.Length > 10)
				{
					Messages.Add(ValidationMessage.Error("Customer rating TL sell rate destination postal code cannot exceed 10 characters"));
					valid = false;
				}

				if (rate.Mileage < 0)
				{
					Messages.Add(ValidationMessage.Error("Customer rating tl sell rate mileage must be greater than or equal to zero"));
					valid = false;
				}

				var valueInDictionary = String.Concat(rate.OriginPostalCode, rate.OriginState, rate.OriginCity,
				                                      rate.OriginCountryId.GetString(), rate.DestinationPostalCode,
				                                      rate.DestinationState, rate.DestinationCity,
				                                      rate.DestinationCountryId.GetString());
													 
				if (!checkDupDictionary.ContainsKey(rate.EffectiveDate)) checkDupDictionary.Add(rate.EffectiveDate, new List<string>());

				if (!checkDupDictionary[rate.EffectiveDate].Contains(valueInDictionary))
				{
					checkDupDictionary[rate.EffectiveDate].Add(valueInDictionary);
				}
				else
				{
					Messages.Add(
						ValidationMessage.Error(
							@"TL Sell Rates has duplicate record for date {0} - Origin: {1}, {2}, {3}, {4} - Destination: {5}, {6}, {7}, {8}",
							rate.EffectiveDate.FormattedLongDateAlt(), rate.OriginState, rate.OriginCity, rate.OriginPostalCode,
							(rate.OriginCountry ?? new Country()).Code,
							rate.DestinationState, rate.DestinationCity, rate.DestinationPostalCode,
							(rate.DestinationCountry ?? new Country()).Code));
					valid = false;
				}
			}

			return valid;
		}
	}
}
