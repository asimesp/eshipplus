﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Processor.Validation.Rating
{
	public class FuelTableValidator : ValidatorBase
	{
		public bool CanDeleteFuelTable(FuelTable fuelTable)
		{
			const string query = "select dbo.FuelTableIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", fuelTable.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Fuel table is referenced one or more times and cannot be deleted."));
			return !used;
		}

		public bool IsValid(FuelTable fuelTable)
		{
			var valid = true;

			if (!HasAllRequiredData(fuelTable)) valid = false;
			if (DuplicateFuelTableName(fuelTable)) valid = false;

			return valid;
		}

		public bool DuplicateFuelTableName(FuelTable fuelTable)
		{
			const string query = "Select count(*) from [FuelTable] where [Name] = @Name and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Name", fuelTable.Name},
			                 		{"TenantId", fuelTable.TenantId},
			                 		{"Id", fuelTable.Id}
			                 	};
			var duplicateFuelTableName = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateFuelTableName) Messages.Add(ValidationMessage.Error("Fuel table name is already in use"));
			return duplicateFuelTableName;
		}

		public bool HasAllRequiredData(FuelTable fuelTable)
		{
			var valid = true;

			if (string.IsNullOrEmpty(fuelTable.Name))
			{
				Messages.Add(ValidationMessage.Error("Fuel Table requires a name"));
				valid = false;
			}
			else if (fuelTable.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Fuel Table name cannot exceed 50 characters"));
				valid = false;
			}

			if (fuelTable.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Fuel Table requires a tenant association"));
				valid = false;
			}

			if(fuelTable.FuelIndices.Count == 0)
			{
				Messages.Add(ValidationMessage.Error("Fuel Table must have at least one index"));
				valid = false;
			}

			if (!HasValidFuelIndeces(fuelTable.FuelIndices)) valid = false;

			return valid;
		}

		private bool HasValidFuelIndeces(IEnumerable<FuelIndex> fuelIndices)
		{
			var valid = true;

			foreach(var index in fuelIndices)
			{
				if (index.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Fuel table index requires a tenant association"));
					valid = false;
				}

				if (index.FuelTable == null)
				{
					Messages.Add(ValidationMessage.Error("Fuel table index requires a fuel table association"));
					valid = false;
				}

				if(index.Surcharge < 0)
				{
					Messages.Add(ValidationMessage.Error("Fuel table index surcharge must be greater than zero"));
					valid = false;
				}

				if (index.Surcharge >= new decimal(9999.9999))
				{
					Messages.Add(ValidationMessage.Error("Fuel table index surcharge must be less than 9999.9999"));
					valid = false;
				}

				if (index.LowerBound < 0)
				{
					Messages.Add(ValidationMessage.Error("Fuel table index lower bound must be greater than zero"));
					valid = false;
				}

				if (index.UpperBound < 0)
				{
					Messages.Add(ValidationMessage.Error("Fuel table index uppper bound must be greater than zero"));
					valid = false;
				}

				if(index.LowerBound > index.UpperBound)
				{
					Messages.Add(ValidationMessage.Error("Fuel table index lower bound must be less than or equal to uppper bound"));
					valid = false;
				}
			}

			return valid;
		}
	}
}
