﻿namespace LogisticsPlus.Eship.Processor.Validation
{
	public class ValidationMessage
	{
		public ValidateMessageType Type { get; set; }
		public string Message { get; set; }

		public static ValidationMessage Warning(string value, params object[] args)
		{
			return new ValidationMessage { Message = string.Format(value, args), Type = ValidateMessageType.Warning };
		}

		public static ValidationMessage Information(string value, params object[] args)
		{
			return new ValidationMessage { Message = string.Format(value, args), Type = ValidateMessageType.Information };
		}

		public static ValidationMessage Error(string value, params object[] args)
		{
			return new ValidationMessage {Message = string.Format(value, args), Type = ValidateMessageType.Error};
		}
	}
}
