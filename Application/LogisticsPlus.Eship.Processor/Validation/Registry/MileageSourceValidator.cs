﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class MileageSourceValidator : ValidatorBase
    {
        public bool IsValid(MileageSource mileageSource)
        {
            Messages.Clear();

            var valid = true;

            if (!IsUnique(mileageSource)) valid = false;
            if (!HasAllRequiredData(mileageSource)) valid = false;
            if (!PassesPrimary(mileageSource)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(MileageSource mileageSource)
        {
            var valid = true;

            if (mileageSource.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Mileage source requires a tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(mileageSource.Code))
            {
                Messages.Add(ValidationMessage.Error("Mileage source requires a code"));
                valid = false;
            }
            else if (mileageSource.Code.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Mileage Source Code cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(mileageSource.Description))
            {
                Messages.Add(ValidationMessage.Error("Mileage source requires a description"));
                valid = false;
            }
            else if (mileageSource.Description.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Mileage Source Description cannot exceed 200 characters"));
                valid = false;
            }

			if (!mileageSource.MileageEngine.ToInt().EnumValueIsValid<MileageEngine>())
			{
				Messages.Add(ValidationMessage.Error("Mileage source mileage engine is invalid"));
				valid = false;
			}


            return valid;
        }

        public bool IsUnique(MileageSource mileageSource)
        {
            var query = mileageSource.IsNew
                ? "Select count(*) from MileageSource where Code = @Code and TenantId = @TenantId"
                : "Select count(*) from MileageSource where Code = @Code and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", mileageSource.Code},
			                 		{"TenantId", mileageSource.TenantId}
			                 	};
            if (!mileageSource.IsNew) parameters.Add("Id", mileageSource.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Mileage Source is not unique"));
            return isUnique;
        }

        public bool PassesPrimary(MileageSource mileageSource)
        {
            const string query = "Select count(*) from MileageSource where TenantId = @TenantId and [Primary] = @Primary and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"TenantId", mileageSource.TenantId},
			                 		{"Id", mileageSource.Id},
			                 		{"Primary", true}
			                 	};
            bool noPrimary = ExecuteScalar(query, parameters).ToInt() <= 0;
            var valid = true;
            if (noPrimary && !mileageSource.Primary)
            {
                Messages.Add(ValidationMessage.Error("Mileage Source must be made primary."));
                valid = false;
            }
            if (!noPrimary && mileageSource.Primary)
            {
                Messages.Add(ValidationMessage.Error("Mileage Source cannot be primary because another Mileage Source is primary"));
                valid = false;
            }

            return valid;
        }

		public bool CanDeleteMileageSource(MileageSource mileageSource)
		{
			const string query = "select dbo.MileageSourceIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", mileageSource.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Mileage Source is referenced one or more times and cannot be deleted."));
			return !used;
		}
    }
}
