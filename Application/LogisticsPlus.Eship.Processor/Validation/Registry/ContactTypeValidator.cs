﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class ContactTypeValidator : ValidatorBase
    {
        public bool IsValid(ContactType contactType)
        {
            Messages.Clear();

            var valid = true;

            if (!IsUnique(contactType)) valid = false;
            if (!HasAllRequiredData(contactType)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(ContactType contactType)
        {
            var valid = true;

            if (contactType.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Contact Type requires a tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(contactType.Code))
            {
                Messages.Add(ValidationMessage.Error("Contact Type requires a code"));
                valid = false;
            }
            else if (contactType.Code.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Contact Type Code cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(contactType.Description))
            {
                Messages.Add(ValidationMessage.Error("Contact Type requires a description"));
                valid = false;
            }
            else if (contactType.Description.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Contact Type Description cannot exceed 200 characters"));
            }

            return valid;
        }

        public bool IsUnique(ContactType contactType)
        {
            var query = contactType.IsNew
                ? "Select count(*) from ContactType where Code = @Code and TenantId = @TenantId"
                : "Select count(*) from ContactType where Code = @Code and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", contactType.Code},
			                 		{"TenantId", contactType.TenantId}
			                 	};
            if (!contactType.IsNew) parameters.Add("Id", contactType.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Contact Type is not unique"));
            return isUnique;
        }

		public bool CanDeleteContactType(ContactType contactType)
		{
			const string query = "select dbo.ContactTypeIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", contactType.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Contact Type is referenced one or more times and cannot be deleted."));
			return !used;
		}
    }
}
