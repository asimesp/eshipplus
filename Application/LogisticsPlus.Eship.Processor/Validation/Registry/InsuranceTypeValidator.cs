﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class InsuranceTypeValidator : ValidatorBase
    {
        public bool IsValid(InsuranceType insuranceType)
        {
            Messages.Clear();

            var valid = true;

            if (!IsUnique(insuranceType)) valid = false;
            if (!HasAllRequiredData(insuranceType)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(InsuranceType insuranceType)
        {
            var valid = true;

            if (insuranceType.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Insurance Type requires a tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(insuranceType.Code))
            {
                Messages.Add(ValidationMessage.Error("Insurance Type requires a code"));
                valid = false;
            }
            else if (insuranceType.Code.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Insurance Type Code cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(insuranceType.Description))
            {
                Messages.Add(ValidationMessage.Error("Insurance Type requires a description"));
                valid = false;
            }
            else if (insuranceType.Description.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Insurance Type Description cannot exceed 200 characters"));
                valid = false;
            }

            return valid;
        }

        public bool IsUnique(InsuranceType insuranceType)
        {
            var query = insuranceType.IsNew
                            ? "Select count(*) from InsuranceType where Code = @Code and TenantId = @TenantId"
                            : "Select count(*) from InsuranceType where Code = @Code and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", insuranceType.Code},
			                 		{"TenantId", insuranceType.TenantId}
			                 	};
            if (!insuranceType.IsNew) parameters.Add("Id", insuranceType.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Insurance Type is not unique"));
            return isUnique;
        }

		public bool CanDeleteInsuranceType(InsuranceType insuranceType)
		{
			const string query = "select dbo.InsuranceTypeIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", insuranceType.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Insurance Type is referenced one or more times and cannot be deleted."));
			return !used;
		}
    }
}
