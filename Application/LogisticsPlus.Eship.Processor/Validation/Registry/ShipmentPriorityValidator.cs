﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class ShipmentPriorityValidator : ValidatorBase
	{
		public bool IsValid(ShipmentPriority shipmentPriority)
		{
			Messages.Clear();

			var valid = true;

			if(!PassesDefault(shipmentPriority)) valid = false;
			if(AttemptToMakeDefaultInActive(shipmentPriority)) valid = false;
			if(!IsUnique(shipmentPriority)) valid = false;
			if(!HasAllRequiredData(shipmentPriority)) valid = false;

			return valid;
		}

		public bool PassesDefault(ShipmentPriority shipmentPriority)
		{
			const string query =
				@"Select count(*) from ShipmentPriority where ShipmentPriority.[Default] = @Default 
					and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Default", true},
			                 		{"TenantId", shipmentPriority.TenantId},
									{"Id", shipmentPriority.Id}
			                 	};
			var noDefaults = ExecuteScalar(query, parameters).ToInt() <= 0;
			var valid = true;

			if (noDefaults && !shipmentPriority.Default)
			{
				Messages.Add(ValidationMessage.Error("Shipment Priority must be made default."));
				valid = false;
			}

			if (!noDefaults && shipmentPriority.Default)
			{
				Messages.Add(ValidationMessage.Error("Shipment Priority cannot be default because another Shipment Priority is default."));
				valid = false;

			}

			return valid;
		}

		public bool AttemptToMakeDefaultInActive(ShipmentPriority shipmentPriority)
		{
			if (shipmentPriority.Default && !shipmentPriority.Active)
			{
				Messages.Add(ValidationMessage.Error("Attempting to make default shipment priority inactive."));
				return true;
			}
			return false;
		}

		public bool HasAllRequiredData(ShipmentPriority shipmentPriority)
		{
			var valid = true;

			if (shipmentPriority.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Shipment Priority requires a tenant association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(shipmentPriority.Code))
			{
				Messages.Add(ValidationMessage.Error("Shipment Priority requires a code."));
				valid = false;
			}
			else if (shipmentPriority.Code.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Shipment Priority Code cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(shipmentPriority.Description))
			{
				if (shipmentPriority.Description.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Shipment Priority Description cannot exceed 500 characters"));
					valid = false;
				}
			}

			if(string.IsNullOrEmpty(shipmentPriority.PriorityColor))
			{
				Messages.Add(ValidationMessage.Error("Shipment Priority requires priority color"));
				valid = false;
			}
			else if (shipmentPriority.PriorityColor.Length > 6)
			{
				Messages.Add(ValidationMessage.Error("Shipment Priority color cannot exceed 6 characters"));
			}

			return valid;
		}

		public bool IsUnique(ShipmentPriority shipmentPriority)
		{
			var query = shipmentPriority.IsNew
							? "Select count(*) from ShipmentPriority where Code = @Code and TenantId = @TenantId"
							: "Select count(*) from ShipmentPriority where Code = @Code and Id <> @Id and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", shipmentPriority.TenantId }, { "Code", shipmentPriority.Code } };
			if (!shipmentPriority.IsNew) parameters.Add("Id", shipmentPriority.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Shipment Priority Code is not unique."));
			return isUnique;
		}

		public bool CanDeleteShipmentPriority(ShipmentPriority shipmentPriority)
		{
			const string query = "select dbo.ShipmentPriorityIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", shipmentPriority.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Shipment Priority is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}
