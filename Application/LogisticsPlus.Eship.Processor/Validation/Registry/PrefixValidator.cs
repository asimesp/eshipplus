﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class PrefixValidator : ValidatorBase
    {
        public bool IsValid(Prefix prefix)
        {
            Messages.Clear();

            var valid = true;

            if (!IsUnique(prefix)) valid = false;
            if (!HasAllRequiredData(prefix)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(Prefix prefix)
        {
            var valid = true;
            if (prefix.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Prefix requires a tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(prefix.Code))
            {
                Messages.Add(ValidationMessage.Error("Prefix requires a code"));
                valid = false;
            }
            else if (prefix.Code.Length > 5)
            {
                Messages.Add(ValidationMessage.Error("Prefix Code cannot exceed 5 characters"));
                valid = false;
            }

            if(!string.IsNullOrEmpty(prefix.Description))
            {
                if(prefix.Description.Length>200)
                {
                    Messages.Add(ValidationMessage.Error("Prefix Description cannot exceed 200 characters"));
                    valid = false;
                }
            }

            return valid;
        }

        public bool IsUnique(Prefix prefix)
        {
            var query = prefix.IsNew
                            ? "Select count(*) from Prefix where Code = @Code and TenantId = @TenantId"
                            : "Select count(*) from Prefix where Code = @Code and TenantId = @TenantId and Id <> @Id";

            var parameters = new Dictionary<string, object>
			                 	{
			                 		{ "Code", prefix.Code }, 
									{ "TenantId", prefix.TenantId }
			                 	};
            if (!prefix.IsNew) parameters.Add("Id", prefix.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Prefix Code is not unique"));
            return isUnique;
        }

		public bool CanDeletePrefix(Prefix prefix)
		{
			const string query = "select dbo.PrefixIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", prefix.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Prefix is referenced one or more times and cannot be deleted."));
			return !used;
		}
    }
}
