﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class SettingValidator : ValidatorBase
    {
        public bool IsValid(Setting setting)
        {
            Messages.Clear();

            var valid = true;
            if (!IsUnique(setting)) valid = false;
            if (!HasAllRequiredData(setting)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(Setting setting)
        {
            var valid = true;

            if (setting.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Setting requires a tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(setting.Value))
            {
                Messages.Add(ValidationMessage.Error("Setting requires a value"));
                valid = false;
            }
            else if (setting.Value.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Setting Value cannot exceed 200 characters"));
                valid = false;
            }

            return valid;
        }

        public bool IsUnique(Setting setting)
        {
            var query = setting.IsNew
                ? "Select count(*) from Setting where Code = @Code and TenantId = @TenantId"
                : "Select count(*) from Setting where Code = @Code and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{ "Code", setting.Code },
									{ "TenantId", setting.TenantId }
								};
            if (!setting.IsNew) parameters.Add("Id", setting.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Setting is not unique"));
            return isUnique;
        }
    }
}
