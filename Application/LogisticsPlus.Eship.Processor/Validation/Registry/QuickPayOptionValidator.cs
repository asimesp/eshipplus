﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class QuickPayOptionValidator : ValidatorBase
	{
		public bool IsValid(QuickPayOption quickPayOption)
		{
			Messages.Clear();

			var valid = true;

			if (!IsUnique(quickPayOption)) valid = false;
			if (!HasAllRequiredData(quickPayOption)) valid = false;

			return valid;
		}

		public bool IsUnique(QuickPayOption quickPayOption)
		{
			var query = quickPayOption.IsNew
							? "Select count(*) from QuickPayOption where Code = @Code and TenantId = @TenantId"
							: "Select count(*) from QuickPayOption where Code = @Code and Id <> @Id and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", quickPayOption.TenantId }, { "Code", quickPayOption.Code } };
			if (!quickPayOption.IsNew) parameters.Add("Id", quickPayOption.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Quick Pay Code is not unique"));
			return isUnique;
		}

		public bool HasAllRequiredData(QuickPayOption quickPayOption)
		{
			var valid = true;

			if (quickPayOption.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Quickpay Option requires a tenant association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(quickPayOption.Code))
			{
				Messages.Add(ValidationMessage.Error("Quickpay Option requires a code"));
				valid = false;
			}
			else if (quickPayOption.Code.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Quickpay Option Code cannot exceed 50 characters"));
				valid = false;
			}

			if (quickPayOption.DiscountPercent <= 0)
			{
				Messages.Add(ValidationMessage.Error("Quickpay Option discount percent must be greater than zero"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(quickPayOption.Description))
			{
				if (quickPayOption.Description.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Quickpay Description cannot exceed 500 characters"));
					valid = false;
				}
			}

			return valid;
		}

		public bool CanDeleteQuickPayOption(QuickPayOption quickPayOption)
		{
			const string query = "select dbo.QuickPayOptionIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> {{"Id", quickPayOption.Id}};
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used)
				Messages.Add(ValidationMessage.Error("Quick pay option is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}
