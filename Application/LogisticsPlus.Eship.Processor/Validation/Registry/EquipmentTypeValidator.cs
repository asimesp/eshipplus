﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class EquipmentTypeValidator : ValidatorBase
	{
		public bool IsValid(EquipmentType equipmentType)
		{
			Messages.Clear();

			var valid = true;

			if (!IsUnique(equipmentType)) valid = false;
			if (!HasAllRequiredData(equipmentType)) valid = false;

			return valid;
		}

		public bool IsUnique(EquipmentType equipmentType)
		{
			var query = equipmentType.IsNew
				? "Select count(*) from EquipmentType where Code = @Code and TenantId = @TenantId"
				: "Select count(*) from EquipmentType where Code = @Code and Id <> @Id and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", equipmentType.TenantId }, { "Code", equipmentType.Code } };
			if (!equipmentType.IsNew) parameters.Add("Id", equipmentType.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Equipment Type is not unique"));
			return isUnique;
		}

		public bool HasAllRequiredData(EquipmentType equipmentType)
		{
			var valid = true;

			if (equipmentType.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Equipment Type requires a tenant association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(equipmentType.Code))
			{
				Messages.Add(ValidationMessage.Error("Equipment Type requires a Code"));
				valid = false;
			}
			else if (equipmentType.Code.Length > 4)
			{
				Messages.Add(ValidationMessage.Error("Equipment Type Code cannot exceed 4 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(equipmentType.TypeName))
			{
				Messages.Add(ValidationMessage.Error("Equipment Type requires a type name"));
				valid = false;
			}
			else if (equipmentType.TypeName.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Equipment Type Name cannot exceed 50 characters"));
				valid = false;
			}

			if (!equipmentType.Group.ToInt().EnumValueIsValid<EquipmentTypeGroup>())
			{
				Messages.Add(ValidationMessage.Error("Invalid Equipment Type Group"));
				valid = false;
			}

            if (!equipmentType.DatEquipmentType.ToInt().EnumValueIsValid<DatEquipmentType>())
            {
                Messages.Add(ValidationMessage.Error("Invalid DAT Equipment Type"));
                valid = false;
            }

			return valid;
		}

		public bool CanDeleteEquipmentType(EquipmentType equipmentType)
		{
			const string query = "select dbo.EquipmentTypeIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> {{"Id", equipmentType.Id}};
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Equipment Type is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}
