﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class ServiceValidator : ValidatorBase
	{
		public bool IsValid(Service service)
		{
			Messages.Clear();

			var valid = IsUnique(service);

			if (!HasAllRequiredData(service)) valid = false;

			return valid;
		}

		public bool IsUnique(Service service)
		{
			var query = service.IsNew
							? "Select count(*) from Service where Code = @Code and TenantId = @TenantId"
							: "Select count(*) from Service where Code = @Code and Id <> @Id and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", service.TenantId }, { "Code", service.Code } };
			if (!service.IsNew) parameters.Add("Id", service.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Service is not unique"));
			return isUnique;
		}

		public bool HasAllRequiredData(Service service)
		{
			var valid = true;

			if (service.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Service requires a tenant association"));
				valid = false;
			}

			if (service.ChargeCodeId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Service requires a charge code"));
				valid = false;
			}

			if (string.IsNullOrEmpty(service.Code))
			{
				Messages.Add(ValidationMessage.Error("Service requires a code"));
				valid = false;
			}
			else if (service.Code.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Service Code cannot be greater than 50 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(service.Description))
			{
				Messages.Add(ValidationMessage.Error("Service requires a description"));
				valid = false;
			}
			else if (service.Description.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Service Descripton cannot exceed 200 characters"));
				valid = false;
			}

			if (!service.Category.ToInt().EnumValueIsValid<ServiceCategory>())
			{
				Messages.Add(ValidationMessage.Error("Invalid Service Category"));
				valid = false;
			}

			if (!service.DispatchFlag.ToInt().EnumValueIsValid<ServiceDispatchFlag>())
			{
				Messages.Add(ValidationMessage.Error("Invalid Dispatch Flag"));
				valid = false;
			}

			return valid;
		}

		public bool CanDeleteService(Service service)
		{
			const string query = "select dbo.ServiceIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", service.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Service is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}
