﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class AccountBucketUnitValidator : ValidatorBase
    {
        public bool IsValid(AccountBucketUnit accountBucketUnit)
        {
            var valid = (IsUnique(accountBucketUnit) && (HasAllRequiredData(accountBucketUnit)));

            return valid;
        }

        public bool CanDeleteAccountBucketUnit(AccountBucketUnit accountBucketUnit)
        {
            const string query = "select dbo.AccountBucketUnitIdUsageCount(@Id)";
            var parameters = new Dictionary<string, object> { { "Id", accountBucketUnit.Id } };
            var used = ExecuteScalar(query, parameters).ToInt() > 0;
            if (used) Messages.Add(ValidationMessage.Error("Account bucket unit is referenced one or more times and cannot be deleted."));
            return !used;
        }

        private bool IsUnique(AccountBucketUnit accountBucketUnit)
        {
            var query = accountBucketUnit.IsNew
                            ? "SELECT COUNT(*) FROM AccountBucketUnit WHERE Name = @Name AND TenantId = @TenantId AND AccountBucketId = @AccountBucketId"
                            : "SELECT COUNT(*) FROM AccountBucketUnit WHERE Name = @Name AND TenantId = @TenantId AND AccountBucketId = @AccountBucketId AND Id <> @Id";

            var parameters = new Dictionary<string, object>
                                 {
                                     {"Name", accountBucketUnit.Name}, 
                                     {"AccountBucketId", accountBucketUnit.AccountBucketId},
                                     {"TenantId", accountBucketUnit.TenantId}
                                 };

            if (!accountBucketUnit.IsNew) parameters.Add("Id", accountBucketUnit.Id);

            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;

            if (!isUnique) Messages.Add(ValidationMessage.Error("Account bucket unit is not unique"));
            return isUnique;
        }

        private bool HasAllRequiredData(AccountBucketUnit accountBucketUnit)
        {
            var valid = true;

            if (accountBucketUnit.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Account bucket unit requires a tenant association"));
                valid = false;
            }

			if (accountBucketUnit.AccountBucket == null)
			{
				Messages.Add(ValidationMessage.Error("Account bucket unit requires an account bucket association"));
				valid = false;
			}

            if (string.IsNullOrEmpty(accountBucketUnit.Name))
            {
                Messages.Add(ValidationMessage.Error("Account bucket unit requires a name"));
                valid = false;
            }
            else if (accountBucketUnit.Name.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Account bucket name cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(accountBucketUnit.Description))
            {
                Messages.Add(ValidationMessage.Error("Account bucket unit requires a description"));
                valid = false;
            }
            else if (accountBucketUnit.Description.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Account bucket unit Description cannot exceed 50 characters"));
                valid = false;
            }
            return valid;
        }
    }
}
