﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class PermissionDetailValidator : ValidatorBase
	{
		public bool IsValid(PermissionDetail permissionDetail)
		{
			Messages.Clear();

			var valid = IsUnique(permissionDetail);

			if (!HasAllRequiredData(permissionDetail)) valid = false;

			return valid;
		}

		public bool IsUnique(PermissionDetail permissionDetail)
		{
			var query = permissionDetail.IsNew
				? "Select count(*) from PermissionDetail where Code = @Code and TenantId = @TenantId"
				: "Select count(*) from PermissionDetail where Code = @Code and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{ "Code", permissionDetail.Code }, 
									{ "TenantId", permissionDetail.TenantId }
			                 	};
			if (!permissionDetail.IsNew) parameters.Add("Id", permissionDetail.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Permission detail code is not unique"));
			return isUnique;
		}

		public bool HasAllRequiredData(PermissionDetail permissionDetail)
		{
			var valid = true;

			if (permissionDetail.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Permission detail Code requires a tenant association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(permissionDetail.Code))
			{
				Messages.Add(ValidationMessage.Error("Permission detail Code requires a code"));
				valid = false;
			}
			else if (permissionDetail.Code.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Permission detail Code cannot exceed 50 characters"));
			}

			if (string.IsNullOrEmpty(permissionDetail.DisplayCode))
			{
				Messages.Add(ValidationMessage.Error("Permission detail requires a display code"));
				valid = false;
			}
			else if (permissionDetail.DisplayCode.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Permission detail display code cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(permissionDetail.NavigationUrl) && permissionDetail.NavigationUrl.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Permission detail navigation URL cannot exceed 500 characters"));
				valid = false;
			}

			if (!permissionDetail.Category.ToInt().EnumValueIsValid<PermissionCategory>())
			{
				Messages.Add(ValidationMessage.Error("Invalid permission detail category"));
				valid = false;
			}
			if (!permissionDetail.Type.ToInt().EnumValueIsValid<PermissionType>())
			{
				Messages.Add(ValidationMessage.Error("Invalid permission detail type"));
				valid = false;
			}

			return valid;
		}
	}
}
