﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class PostalCodeValidator : ValidatorBase
    {
        public bool IsValid(PostalCode postalCode)
        {
            Messages.Clear();

            var valid = true;

            if (!PassesPrimary(postalCode)) valid = false;
            if (!IsUnique(postalCode)) valid = false;
            if (!HasAllRequiredData(postalCode)) valid = false;

            return valid;
        }

        public bool IsUnique(PostalCode postalCode)
        {
            var query = postalCode.IsNew
                            ? "Select count(*) from PostalCode where Code = @Code and CityAlias = @CityAlias and CountryId = @CountryId"
                            : "Select count(*) from PostalCode where Code = @Code and CityAlias = @CityAlias and CountryId = @CountryId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", postalCode.Code},
			                 		{"CountryId", postalCode.CountryId},
			                 		{"CityAlias", postalCode.CityAlias}
			                 	};
            if (!postalCode.IsNew) parameters.Add("Id", postalCode.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Postal Code is not unique"));
            return isUnique;
        }

        public bool PassesPrimary(PostalCode postalCode)
        {
            const string query =
                @"Select count(*) from PostalCode where PostalCode.Code = @Code and PostalCode.CountryId = @CountryId and PostalCode.[Primary] = @Primary and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", postalCode.Code},
			                 		{"CountryId", postalCode.CountryId},
			                 		{"Primary", true},
									{"Id", postalCode.Id}
			                 	};
            bool noPrimary = ExecuteScalar(query, parameters).ToInt() <= 0;
            var valid = true;
            if (noPrimary && !postalCode.Primary)
            {
                Messages.Add(ValidationMessage.Error("Postal Code must be made primary."));
                valid = false;
            }
            if (!noPrimary && postalCode.Primary)
            {
                Messages.Add(ValidationMessage.Error("Postal Code cannot be primary because another postal code having same code in country is primary"));
                valid = false;
            }

            return valid;
        }

        public bool HasAllRequiredData(PostalCode postalCode)
        {
            var valid = true;

            if (string.IsNullOrEmpty(postalCode.Code))
            {
                Messages.Add(ValidationMessage.Error("Postal code requires a code"));
                valid = false;
            }
            else if (postalCode.Code.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Postal code cannot exceed 10 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(postalCode.City))
            {
                Messages.Add(ValidationMessage.Error("Postal code requires a city"));
                valid = false;
            }
            else if (postalCode.City.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Postal Code City cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(postalCode.CityAlias))
            {
                Messages.Add(ValidationMessage.Error("Postal code requires a city alias"));
                valid = false;
            }
            else if (postalCode.CityAlias.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Postal Code City Alias cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(postalCode.State))
            {
                Messages.Add(ValidationMessage.Error("Postal code requires a state"));
                valid = false;
            }
            else if(postalCode.State.Length>50)
            {
                Messages.Add(ValidationMessage.Error("Postal Code State cannot exceed 50 characters"));
                valid = false;
            }

            if (postalCode.CountryId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Postal code requires a country association"));
                valid = false;
            }

            return valid;
        }
    }
}
