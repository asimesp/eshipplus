﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class AccountBucketValidator : ValidatorBase
    {
        public bool IsValid(AccountBucket accountBucket)
        {
            Messages.Clear();

            var valid = IsUnique(accountBucket);
            if (!HasAllRequiredData(accountBucket)) valid = false;

            return valid;
        }


        public bool IsUnique(AccountBucket accountBucket)
        {
            var query = accountBucket.IsNew
                            ? "Select count(*) from AccountBucket where Code = @Code and TenantId = @TenantId"
                            : "Select count(*) from AccountBucket where Code = @Code and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object> { { "Code", accountBucket.Code }, { "TenantId", accountBucket.TenantId } };
            if (!accountBucket.IsNew) parameters.Add("Id", accountBucket.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Account Bucket is not unique"));
            return isUnique;
        }

        private bool IsAccountBucketUnitUnique(AccountBucketUnit accountBucketUnit)
        {
            var query = accountBucketUnit.IsNew
                            ? "SELECT COUNT(*) FROM AccountBucketUnit WHERE Name = @Name AND TenantId = @TenantId AND AccountBucketId = @AccountBucketId"
                            : "SELECT COUNT(*) FROM AccountBucketUnit WHERE Name = @Name AND TenantId = @TenantId AND AccountBucketId = @AccountBucketId AND Id <> @Id";

            var parameters = new Dictionary<string, object>
                                 {
                                     {"Name", accountBucketUnit.Name}, 
                                     {"AccountBucketId", accountBucketUnit.AccountBucketId},
                                     {"TenantId", accountBucketUnit.TenantId}
                                 };

            if (!accountBucketUnit.IsNew) parameters.Add("Id", accountBucketUnit.Id);

            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;

            if (!isUnique) Messages.Add(ValidationMessage.Error("Account bucket unit is not unique"));
            return isUnique;
        }


        public bool HasAllRequiredData(AccountBucket accountBucket)
        {
            var valid = true;

            if (accountBucket.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Account Bucket requires a tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(accountBucket.Code))
            {
                Messages.Add(ValidationMessage.Error("Account Bucket requires a code"));
                valid = false;
            }
            else if (accountBucket.Code.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Account Bucket Code cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(accountBucket.Description))
            {
                Messages.Add(ValidationMessage.Error("Account Bucket requires a description"));
                valid = false;
            }
            else if (accountBucket.Description.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Account Bucket Description cannot exceed 200 characters"));
                valid = false;
            }

            if (accountBucket.AccountBucketUnits.Count > 0 && !AccountBucketUnitsAreValid(accountBucket.AccountBucketUnits))
                valid = false;

            return valid;
        }

        private bool AccountBucketUnitsAreValid(IEnumerable<AccountBucketUnit> accountBucketUnits)
        {
            var valid = true;
            foreach (var accountBucketUnit in accountBucketUnits)
            {

                if (accountBucketUnit.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Account bucket unit requires a tenant association"));
                    valid = false;
                }

                if (accountBucketUnit.AccountBucket == null)
                {
                    Messages.Add(ValidationMessage.Error("Account bucket unit requires an account bucket association"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(accountBucketUnit.Name))
                {
                    Messages.Add(ValidationMessage.Error("Account bucket unit requires a name"));
                    valid = false;
                }
                else if (accountBucketUnit.Name.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Account bucket name cannot exceed 50 characters"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(accountBucketUnit.Description))
                {
                    Messages.Add(ValidationMessage.Error("Account bucket unit requires a description"));
                    valid = false;
                }
                else if (accountBucketUnit.Description.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Account bucket unit Description cannot exceed 50 characters"));
                    valid = false;
                }
                if (!IsAccountBucketUnitUnique(accountBucketUnit)) valid = false;

            }
            return valid;

        }
        

        public bool CanDeleteAccountBucket(AccountBucket accountBucket)
        {
            const string query = "select dbo.AccountBucketIdUsageCount(@Id)";
            var parameters = new Dictionary<string, object> { { "Id", accountBucket.Id } };
            var used = ExecuteScalar(query, parameters).ToInt() > 0;
            if (used) Messages.Add(ValidationMessage.Error("Account bucket is referenced one or more times and cannot be deleted."));
            return !used;
        }

        public bool CanDeleteAccountBucketUnit(AccountBucketUnit accountBucketUnit)
        {
            const string query = "select dbo.AccountBucketUnitIdUsageCount(@Id)";
            var parameters = new Dictionary<string, object> { { "Id", accountBucketUnit.Id } };
            var used = ExecuteScalar(query, parameters).ToInt() > 0;
            if (used) Messages.Add(ValidationMessage.Error("Account bucket unit is referenced one or more times and cannot be deleted."));
            return !used;
        }
    }
}