﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class PackageTypeValidator : ValidatorBase
	{
		public bool IsValid(PackageType packageType)
		{
			Messages.Clear();

			var valid = IsUnique(packageType);
			if (!HasAllRequiredData(packageType)) valid = false;

			return valid;
		}

		public bool IsUnique(PackageType packageType)
		{
			var query = packageType.IsNew
							? "Select count(*) from PackageType where TypeName = @TypeName and TenantId = @TenantId"
							: "Select count(*) from PackageType where TypeName = @TypeName and Id <> @Id and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", packageType.TenantId }, { "TypeName", packageType.TypeName } };
			if (!packageType.IsNew) parameters.Add("Id", packageType.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Package Type is not unique"));
			return isUnique;
		}

		public bool HasAllRequiredData(PackageType packageType)
		{
			var valid = true;

			if (packageType.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Package Type requires a tenant association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(packageType.TypeName))
			{
				Messages.Add(ValidationMessage.Error("Package Type requires a type name"));
				valid = false;
			}
			else if (packageType.TypeName.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Package Type Name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(packageType.EdiOid) && packageType.EdiOid.Length > 2)
			{
				Messages.Add(ValidationMessage.Error("Package Type EDI Order Identification Details Code (EdiOid) cannot exceed 2 characters"));
				valid = false;
			}

			if (packageType.DefaultHeight < 0)
			{
				Messages.Add(ValidationMessage.Error("Package Type Default Height must be greater than or equal to zero"));
				valid = false;
			}

			if (packageType.DefaultLength < 0)
			{
				Messages.Add(ValidationMessage.Error("Package Type Default Length must be greater than or equal to zero"));
				valid = false;
			}

			if (packageType.DefaultWidth < 0)
			{
				Messages.Add(ValidationMessage.Error("Package Type Default Width must be greater than or equal to zero"));
				valid = false;
			}

			if (packageType.SortWeight < 0)
			{
				Messages.Add(ValidationMessage.Error("Package Type Sort Weight must be greater than or equal to zero"));
				valid = false;
			}

			return valid;
		}

		public bool CanDeletePackageType(PackageType packageType)
		{
			const string query = "select dbo.PackageTypeIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", packageType.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Package Type is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}
