﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class UserDepartmentValidator : ValidatorBase
    {
        public bool IsValid(UserDepartment userDepartment)
        {
             Messages.Clear();

            var valid = true;

            if (!IsUnique(userDepartment)) valid = false;
            if (!HasAllRequiredData(userDepartment)) valid = false;

            return valid;
        }

        private bool HasAllRequiredData(UserDepartment userDepartment)
        {
            var valid = true;
            if (userDepartment.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("User department requires a tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(userDepartment.Code))
            {
                Messages.Add(ValidationMessage.Error("User department requires a code"));
                valid = false;
            }
            else if(userDepartment.Code.Length > 50)
            {
                    Messages.Add(ValidationMessage.Error("User department code cannot exceed 50 characters"));
                    valid = false;
            }

            if(string.IsNullOrEmpty(userDepartment.Description))
            {
                Messages.Add(ValidationMessage.Error("User department requires a description"));
                valid = false;
            }
            else if(userDepartment.Description.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("User department description cannot exceed 200 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(userDepartment.Phone) && userDepartment.Phone.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("User department phone number cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(userDepartment.Fax) && userDepartment.Fax.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("User department fax number cannot exceed 50 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(userDepartment.Email) && userDepartment.Email.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("User department email cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(userDepartment.Email) && !userDepartment.Email.EmailIsValid())
            {
                Messages.Add(ValidationMessage.Error("User department email is invalid"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(userDepartment.Comments) && userDepartment.Comments.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("User department comments cannot exceed 500 characters"));
                valid = false;
            }

            return valid;
        }

        private bool IsUnique(UserDepartment userDepartment)
        {
            var query = userDepartment.IsNew
                            ? "Select count(*) from UserDepartment where Code = @Code and TenantId = @TenantId"
                            : "Select count(*) from UserDepartment where Code = @Code and TenantId = @TenantId and Id <> @Id";

            var parameters = new Dictionary<string, object>
			                 	{
			                 		{ "Code", userDepartment.Code }, 
									{ "TenantId", userDepartment.TenantId }
			                 	};
            if (!userDepartment.IsNew) parameters.Add("Id", userDepartment.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("User department code is not unique"));
            return isUnique;
        }

		public bool CanDeleteDepartment(UserDepartment department)
		{
			const string query = "select count(*) from [User] where UserDepartmentId = @Id";
			var parameters = new Dictionary<string, object> { { "Id", department.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("User department is referenced one or more times and cannot be deleted."));
			return !used;
		}
    }
}
