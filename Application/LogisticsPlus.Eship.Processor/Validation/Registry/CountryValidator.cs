﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class CountryValidator : ValidatorBase
	{
		public bool IsValid(Country country)
		{
			Messages.Clear();

			bool valid = IsUnique(country);
			if (!HasAllRequiredData(country)) valid = false;

			return valid;
		}

		public bool IsUnique(Country country)
		{
			var query = country.IsNew
				? "Select count(*) from Country where Code = @Code"
				: "Select count(*) from Country where Code = @Code and Id <> @Id";

			var parameters = new Dictionary<string, object> { { "Code", country.Code } };
			if (!country.IsNew) parameters.Add("Id", country.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Country is not unique"));
			return isUnique;
		}

		public bool HasAllRequiredData(Country country)
		{
			var valid = true;

			if (string.IsNullOrEmpty(country.Code))
			{
				Messages.Add(ValidationMessage.Error("Country requires a code"));
				valid = false;
			}
            else if(country.Code.Length > 2)
            {
                Messages.Add(ValidationMessage.Error("Country Code cannot exceed 2 characters"));
                valid = false;
            }

			if (string.IsNullOrEmpty(country.Name))
			{
				Messages.Add(ValidationMessage.Error("Country requires a name"));
				valid = false;
			}
            else if(country.Name.Length > 50)
		    {
		        Messages.Add(ValidationMessage.Error("Country Name cannot exceed 50 characters"));
		        valid = false;
		    }

            if (country.SortWeight < 0)
            {
                Messages.Add(ValidationMessage.Error("Country Sort Weight must be greater than or equal to zero"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(country.PostalCodeValidation) && country.PostalCodeValidation.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Country Postal Code Validation cannot exceed 200 characters"));
                valid = false;
            }

			return valid;
		}
	}
}
