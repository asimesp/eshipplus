﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class DocumentTemplateValidator : ValidatorBase
    {
        public bool IsValid(DocumentTemplate documentTemplate)
        {
            Messages.Clear();

            var valid = true;
            if (!IsUnique(documentTemplate)) valid = false;
            if (!HasAllRequiredData(documentTemplate)) valid = false;
            if (!PassesPrimary(documentTemplate)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(DocumentTemplate documentTemplate)
        {
            var valid = true;

            if (documentTemplate.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Document Template requires a tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(documentTemplate.Code))
            {
                Messages.Add(ValidationMessage.Error("Document Template requires a code"));
                valid = false;
            }
            else if (documentTemplate.Code.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Document Template Code cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(documentTemplate.TemplatePath))
            {
                Messages.Add(ValidationMessage.Error("Document Template requires a template path"));
                valid = false;
            }
            else if (documentTemplate.TemplatePath.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Document Template Path cannot exceed 500 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(documentTemplate.Description))
            {
                if (documentTemplate.Description.Length > 500)
                {
                    Messages.Add(ValidationMessage.Error("Document Templaste Descripton cannot exceed 500 characters"));
                    valid = false;
                }
            }

			if (!documentTemplate.Category.ToInt().EnumValueIsValid<DocumentTemplateCategory>())
            {
                Messages.Add(ValidationMessage.Error("Invalid Document Template Category"));
                valid = false;
            }

			if (!documentTemplate.ServiceMode.ToInt().EnumValueIsValid<ServiceMode>())
            {
                Messages.Add(ValidationMessage.Error("Invalid Document Template Service Mode"));
                valid = false;
            }

			if (string.IsNullOrEmpty(documentTemplate.WeightDecimals))
			{
				Messages.Add(ValidationMessage.Error("Document Template requires number of decimal places for rounding of weight values"));
				valid = false;
			}
			else if (!documentTemplate.WeightDecimals.DecimalPlaceIsValid())
			{
				Messages.Add(ValidationMessage.Error("Document Template number of decimal places for rounding of weight values is invalid"));
				valid = false;
			}

			if (string.IsNullOrEmpty(documentTemplate.DimensionDecimals))
			{
				Messages.Add(ValidationMessage.Error("Document Template requires number of decimal places for rounding of dimension values"));
				valid = false;
			}
			else if (!documentTemplate.DimensionDecimals.DecimalPlaceIsValid())
			{
				Messages.Add(ValidationMessage.Error("Document Template number of decimal places for rounding of dimension values is invalid"));
				valid = false;
			}

			if (string.IsNullOrEmpty(documentTemplate.CurrencyDecimals))
			{
				Messages.Add(ValidationMessage.Error("Document Template requires number of decimal places for rounding of currency values"));
				valid = false;
			}
			else if (!documentTemplate.CurrencyDecimals.DecimalPlaceIsValid())
			{
				Messages.Add(ValidationMessage.Error("Document Template number of decimal places for rounding of currency values is invalid"));
				valid = false;
			}

            return valid;
        }

        public bool IsUnique(DocumentTemplate documentTemplate)
        {
            var query = documentTemplate.IsNew
                ? "Select count(*) from DocumentTemplate where Code = @Code and TenantId = @TenantId"
                : "Select count(*) from DocumentTemplate where Code = @Code and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{ "Code", documentTemplate.Code },
									{ "TenantId", documentTemplate.TenantId }
								};
            if (!documentTemplate.IsNew) parameters.Add("Id", documentTemplate.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Document Template is not unique"));
            return isUnique;
        }

    	public bool PassesPrimary(DocumentTemplate documentTemplate)
        {
            const string query =
                @"Select count(*) from DocumentTemplate where [Primary] = @Primary and ServiceMode = @ServiceMode
					and Category = @Category and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Primary", true},
			                 		{"ServiceMode", documentTemplate.ServiceMode},
			                 		{"Category", documentTemplate.Category},
			                 		{"TenantId", documentTemplate.TenantId},
			                 		{"Id", documentTemplate.Id}
			                 	};
            var noPrimary = ExecuteScalar(query, parameters).ToInt() <= 0;
            var valid = true;

            if (noPrimary && !documentTemplate.Primary)
            {
                Messages.Add(ValidationMessage.Error("Document Template must be made primary."));
                valid = false;
            }

            if (!noPrimary && documentTemplate.Primary)
            {
                Messages.Add(ValidationMessage.Error("Document Template cannot be primary because another document template is primary."));
                valid = false;
            }

            return valid;
        }

		public bool CanDeleteDocumentTemplate(DocumentTemplate documentTemplate)
		{
			const string query = "select dbo.DocumentTemplateIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", documentTemplate.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Document Template is referenced one or more times and cannot be deleted."));
			return !used;
		}
    }
}
