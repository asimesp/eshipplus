﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class YrMthBusDayValidator : ValidatorBase
    {
        private const int JanDays = 31;
        private const int FebDays = 29;
        private const int MarDays = 31;
        private const int AprilDays = 30;
        private const int MayDays = 31;
        private const int JunDays = 30;
        private const int JulDays = 31;
        private const int AugDays = 31;
        private const int SeptDays = 30;
        private const int OctDays = 31;
        private const int NovDays = 30;
        private const int DecDays = 31;

        public bool IsValid(YrMthBusDay yrMthBusDay)
        {
            var valid = true;
            if (DuplicateYear(yrMthBusDay)) valid = false;
            if (!HasAllRequiredData(yrMthBusDay)) valid = false;

            return valid;
        }

        public bool DuplicateYear(YrMthBusDay yrMthBusDay)
        {
            const string query = "SELECT COUNT(*) FROM YrMthBusDay WHERE Year = @Year AND Id <> @Id AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "Year", yrMthBusDay.Year }, { "Id", yrMthBusDay.Id }, { "TenantId", yrMthBusDay.TenantId } };
            var duplicateYear = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateYear) Messages.Add(ValidationMessage.Error("Year already exists"));
            return duplicateYear;
        }

        private bool HasAllRequiredData(YrMthBusDay yrMthBusDay)
        {
            var valid = true;


            if (yrMthBusDay.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Mth Bus Day requires a tenant association"));
                valid = false;
            }

            if (yrMthBusDay.January < 1 || yrMthBusDay.January > JanDays)
            {
                Messages.Add(ValidationMessage.Error("Januarys value must greater than zero and less than {0}", JanDays));
                valid = false;
            }

            if (yrMthBusDay.February < 1 || yrMthBusDay.February > FebDays)
            {
                Messages.Add(ValidationMessage.Error("Februarys value must greater than zero and less than {0}", FebDays));
                valid = false;
            }

            if (yrMthBusDay.March < 1 || yrMthBusDay.March > MarDays)
            {
                Messages.Add(ValidationMessage.Error("March value must greater than zero and less than {0}", MarDays));
                valid = false;
            }

            if (yrMthBusDay.April < 1 || yrMthBusDay.April > AprilDays)
            {
                Messages.Add(ValidationMessage.Error("Aprils value must greater than zero and less than {0}", AprilDays));
                valid = false;
            }

            if (yrMthBusDay.May < 1 || yrMthBusDay.May > MayDays)
            {
                Messages.Add(ValidationMessage.Error("Mays value must greater than zero and less than {0}", MayDays));
                valid = false;
            }

            if (yrMthBusDay.June < 1 || yrMthBusDay.June > JunDays)
            {
                Messages.Add(ValidationMessage.Error("Junes value must be greater than zero and less than {0}", JunDays));
                valid = false;
            }

            if (yrMthBusDay.July < 1 || yrMthBusDay.July > JulDays)
            {
                Messages.Add(ValidationMessage.Error("Julys value must greater than zero or less than {0}", JulDays));
                valid = false;
            }

            if (yrMthBusDay.August < 1 || yrMthBusDay.August > AugDays)
            {
                Messages.Add(ValidationMessage.Error("Augusts value must be greater than zero and less than {0}", AugDays));
                valid = false;
            }

            if (yrMthBusDay.September < 1 || yrMthBusDay.September > SeptDays)
            {
                Messages.Add(ValidationMessage.Error("Septembers value must be greater than zero and less than {0}", SeptDays));
                valid = false;
            }

            if (yrMthBusDay.October < 1 || yrMthBusDay.October > OctDays)
            {
                Messages.Add(ValidationMessage.Error("Octobers value must be greater than  zero and less than {0}", OctDays));
                valid = false;
            }

            if (yrMthBusDay.November < 1 || yrMthBusDay.November > NovDays)
            {
                Messages.Add(ValidationMessage.Error("Novembers value must be greater than zero and less than {0}", NovDays));
                valid = false;
            }

            if (yrMthBusDay.December < 1 || yrMthBusDay.December > DecDays)
            {
                Messages.Add(ValidationMessage.Error("Decembers value must be greater than zero and less than {0}", DecDays));
                valid = false;
            }

            if (string.IsNullOrEmpty(yrMthBusDay.Year))
            {
                Messages.Add(ValidationMessage.Error("Year value is required"));
                valid = false;
            }
            else if (yrMthBusDay.Year.Length != 4)
            {
                Messages.Add(ValidationMessage.Error("Year value must be exactly four characters long"));
                valid = false;
            }


            var year = yrMthBusDay.Year.ToInt();
            if (year == default(int))
            {
                Messages.Add(ValidationMessage.Error("Year value must be numeric"));
                valid = false;
            }
            else
            {
                var yearDate = string.Format("{0}/01/01", year).ToDateTime();
                if (!yearDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Year must be between {0} and {1}",
                                                      DateUtility.SystemEarliestDateTime.Year,
                                                      DateUtility.SystemLatestDateTime.Year));
                    valid = false;
                }
            }

            return valid;
        }
    }
}