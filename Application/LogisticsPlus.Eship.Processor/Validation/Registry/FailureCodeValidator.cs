﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class FailureCodeValidator : ValidatorBase
    {
        public bool IsValid(FailureCode failureCode)
        {
            Messages.Clear();

            var valid = true;

            if (!HasAllRequiredData(failureCode)) valid = false;
            if (!IsUnique(failureCode)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(FailureCode failureCode)
        {
            var valid = true;

            if (string.IsNullOrEmpty(failureCode.Code))
            {
                Messages.Add(ValidationMessage.Error("Failure Code Requires a Code"));
                valid = false;
            }
            else if (failureCode.Code.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Failure Code Code cannot exceed 10 characters"));
                valid = false;
            }

            if (failureCode.Description != null && failureCode.Description.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Failure Code Description cannot exceed 500 characters"));
                valid = false;
            }

            return valid;
        }

        public bool IsUnique(FailureCode failureCode)
        {
            var query = failureCode.IsNew
                ? "Select count(*) from FailureCode where Code = @Code and TenantId = @TenantId"
                : "Select count(*) from FailureCode where Code = @Code and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{ "Code", failureCode.Code },
									{ "TenantId", failureCode.TenantId }
								};
            if (!failureCode.IsNew) parameters.Add("Id", failureCode.Id);
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Failure Code is not unique"));
            return isUnique;
        }

        public bool CanDeleteFailureCode(FailureCode failureCode)
        {
            const string query = "SELECT dbo.FailureCodeIdUsageCount(@Id)";
            var parameters = new Dictionary<string, object> { { "Id", failureCode.Id } };
            var used = ExecuteScalar(query, parameters).ToInt() > 0;
            if (used)
                Messages.Add(ValidationMessage.Error("Failure code is referenced one or more times and cannot be deleted."));
            return !used;
        }
    }
}
