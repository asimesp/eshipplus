﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class DocumentPrefixMapValidator : ValidatorBase
	{
		public bool IsValid(DocumentPrefixMap documentPrefixMap)
		{
			Messages.Clear();

			var valid = true;

			if (!IsUnique(documentPrefixMap)) valid = false;
			if (!HasAllRequiredData(documentPrefixMap)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(DocumentPrefixMap documentPrefixMap)
		{
			var valid = true;

			if (documentPrefixMap.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Document to Prefix Mapping requires a tenant association"));
				valid = false;
			}

			if (documentPrefixMap.PrefixId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Document to Prefix Mapping requires a prefix association"));
				valid = false;
			}

			if (documentPrefixMap.DocumentTemplateId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Document to Prefix Mapping requires a document template association"));
				valid = false;
			}

			return valid;
		}

		public bool IsUnique(DocumentPrefixMap documentPrefixMap)
		{
			var query = documentPrefixMap.IsNew
							? "SELECT COUNT(*) FROM DocumentPrefixMap WHERE PrefixId = @PrefixId AND DocumentTemplateId = @DocumentTemplateId AND TenantId = @TenantId"
							: "SELECT COUNT(*) FROM DocumentPrefixMap WHERE PrefixId = @PrefixId AND DocumentTemplateId = @DocumentTemplateId AND TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"PrefixId", documentPrefixMap.PrefixId},
			                 		{"DocumentTemplateId", documentPrefixMap.DocumentTemplateId},
			                 		{"TenantId", documentPrefixMap.TenantId},
			                 	};
			if (!documentPrefixMap.IsNew) parameters.Add("Id", documentPrefixMap.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Document to Prefix Mapping is not unique"));
			return isUnique;
		}
	}
}
