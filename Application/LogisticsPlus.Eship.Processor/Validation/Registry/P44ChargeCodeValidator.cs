﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class P44ChargeCodeValidator : ValidatorBase
	{
		public bool IsValid(P44ChargeCodeMapping p44ChargeCodeMapping)
		{
			Messages.Clear();

			var valid = IsUnique(p44ChargeCodeMapping);

			if (!HasAllRequiredData(p44ChargeCodeMapping)) valid = false;

			return valid;
		}

        public bool IsUnique(P44ChargeCodeMapping p44ChargeCodeMapping)
		{
			var query = "Select count(*) from P44ChargeCodeMapping where Project44Code = @Project44Code and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
            {
                { "Project44Code", p44ChargeCodeMapping.Project44Code },
                { "ChargeCodeId", p44ChargeCodeMapping.ChargeCodeId },
                { "TenantId", p44ChargeCodeMapping.TenantId },
                { "Id", p44ChargeCodeMapping.Id }
            };
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("P44 Charge Code Mapping is not unique"));
			return isUnique;
		}

        public bool HasAllRequiredData(P44ChargeCodeMapping p44ChargeCodeMapping)
		{
			var valid = true;

			if (p44ChargeCodeMapping.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Project 44 Charge Code Mapping  requires a tenant association"));
				valid = false;
			}

			if (p44ChargeCodeMapping.ChargeCodeId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Project 44 Charge Code Mapping requires a charge code Id"));
				valid = false;
			}

            if (!p44ChargeCodeMapping.Project44Code.ToInt().EnumValueIsValid<Project44ChargeCode>())
            {
                Messages.Add(ValidationMessage.Error("Project 44 Charge Code Mapping must have a valid code type"));
                valid = false;
            }

            return valid;
		}
    }
}
