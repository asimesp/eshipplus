﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class NoServiceDayValidator : ValidatorBase
	{
		public bool IsValid(NoServiceDay documentTag)
		{
			Messages.Clear();

			var valid = true;

			if (!IsUnique(documentTag)) valid = false;
			if (!HasAllRequiredData(documentTag)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(NoServiceDay noServiceDay)
		{
			var valid = true;

			if (noServiceDay.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("No Service Day requires a tenant association"));
				valid = false;
			}

			if (!noServiceDay.DateOfNoService.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("No Service Day date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}
			
			if (string.IsNullOrEmpty(noServiceDay.Description))
			{
				Messages.Add(ValidationMessage.Error("No Service Day requires a description"));
				valid = false;
			}
            else if(noServiceDay.Description.Length>200)
            {
				Messages.Add(ValidationMessage.Error("No Service Day description cannot exceed 200 characters"));
                valid = false;
            }
			return valid;
		}

		public bool IsUnique(NoServiceDay noServiceDay)
		{
			var query = noServiceDay.IsNew
				? "Select count(*) from NoServiceDay where DateOfNoService = @Date and TenantId = @TenantId"
				: "Select count(*) from NoServiceDay where DateOfNoService = @Date and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Date", noServiceDay.DateOfNoService},
			                 		{"TenantId", noServiceDay.TenantId}
			                 	};
			if (!noServiceDay.IsNew) parameters.Add("Id", noServiceDay.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("No Service Day is not unique"));
			return isUnique;
		}

		public bool CanDeleteNoServiceDay(NoServiceDay noServiceDay)
		{
			const string query = "select dbo.NoServiceDayIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", noServiceDay.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("No Service Day is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}
