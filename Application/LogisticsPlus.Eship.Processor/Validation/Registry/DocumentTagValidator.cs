﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class DocumentTagValidator : ValidatorBase
	{
		public bool IsValid(DocumentTag documentTag)
		{
			Messages.Clear();

			var valid = true;

			if (!IsUnique(documentTag)) valid = false;
			if (!HasAllRequiredData(documentTag)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(DocumentTag documentTag)
		{
			var valid = true;

			if (documentTag.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Document Tag requires a tenant association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(documentTag.Code))
			{
				Messages.Add(ValidationMessage.Error("Document Tag requires a code"));
				valid = false;
			}
            else if(documentTag.Code.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Document Tag Code cannot exceed 50 characters"));
                valid = false;
            }

			if (string.IsNullOrEmpty(documentTag.Description))
			{
				Messages.Add(ValidationMessage.Error("Documetn Tag requires a description"));
				valid = false;
			}
            else if(documentTag.Description.Length>200)
            {
                Messages.Add(ValidationMessage.Error("Documen Tag Description cannot exceed 200 characters"));
                valid = false;
            }
			return valid;
		}

		public bool IsUnique(DocumentTag documentTag)
		{
			var query = documentTag.IsNew
				? "Select count(*) from DocumentTag where Code = @Code and TenantId = @TenantId"
				: "Select count(*) from DocumentTag where Code = @Code and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Code", documentTag.Code},
			                 		{"TenantId", documentTag.TenantId}
			                 	};
			if (!documentTag.IsNew) parameters.Add("Id", documentTag.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Document Tag is not unique"));
			return isUnique;
		}

		public bool CanDeleteDocumentTag(DocumentTag documentTag)
		{
			const string query = "select dbo.DocumentTagIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", documentTag.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Document Tag is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}
