﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
	public class ChargeCodeValidator : ValidatorBase
	{
		public bool IsValid(ChargeCode chargeCode)
		{
			Messages.Clear();

			bool valid = IsUnique(chargeCode);

			if (!HasAllRequiredData(chargeCode)) valid = false;

			return valid;
		}

		public bool IsUnique(ChargeCode chargeCode)
		{
			var query = chargeCode.IsNew
				? "Select count(*) from ChargeCode where Code = @Code and TenantId = @TenantId"
				: "Select count(*) from ChargeCode where Code = @Code and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{ "Code", chargeCode.Code }, 
									{ "TenantId", chargeCode.TenantId }
			                 	};
			if (!chargeCode.IsNew) parameters.Add("Id", chargeCode.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Charge Code is not unique"));
			return isUnique;
		}

		public bool HasAllRequiredData(ChargeCode chargeCode)
		{
			var valid = true;

			if (chargeCode.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Charge Code requires a tenant association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(chargeCode.Code))
			{
				Messages.Add(ValidationMessage.Error("Charge Code requires a code"));
				valid = false;
			}
            else if (chargeCode.Code.Length>50)
            {
                Messages.Add(ValidationMessage.Error("Charge Code cannot exceed 50 characters"));
            }

			if (string.IsNullOrEmpty(chargeCode.Description))
			{
				Messages.Add(ValidationMessage.Error("Charge Code requires a description"));
				valid = false;
			}
            else if (chargeCode.Description.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Charge Code Descripton cannot exceed 200 characters"));
                valid = false;
            }
            if (!chargeCode.Category.ToInt().EnumValueIsValid<ChargeCodeCategory>())
            {
                Messages.Add(ValidationMessage.Error("Invalid Charge Code Category"));
                valid = false;
            }

			return valid;
		}

		public bool CanDeleteChargeCode(ChargeCode chargeCode)
		{
			const string query = "select dbo.ChargeCodeIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", chargeCode.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Charge Code is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}
