﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Registry
{
    public class P44ServiceValidator : ValidatorBase
    {
        public bool IsValid(P44ServiceMapping p44ServiceMapping)
        {
            Messages.Clear();

            var valid = IsUnique(p44ServiceMapping);

            if (!HasAllRequiredData(p44ServiceMapping)) valid = false;

            return valid;
        }

        public bool IsUnique(P44ServiceMapping p44ServiceMapping)
        {
            var query = "Select count(*) from P44ServiceMapping where Project44Code = @Project44Code and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
            {
                { "Project44Code", p44ServiceMapping.Project44Code },
                { "ServiceId", p44ServiceMapping.ServiceId },
                { "TenantId", p44ServiceMapping.TenantId },
                { "Id", p44ServiceMapping.Id }
            };
            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("P44 Service Mapping is not unique"));
            return isUnique;
        }

        public bool HasAllRequiredData(P44ServiceMapping p44ServiceMapping)
        {
            var valid = true;

            if (p44ServiceMapping.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Project 44 Service Mapping requires a tenant association"));
                valid = false;
            }

            if (p44ServiceMapping.ServiceId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Project 44 Service Mapping requires a service Id"));
                valid = false;
            }

            if (!p44ServiceMapping.Project44Code.ToInt().EnumValueIsValid<Project44ServiceCode>())
            {
                Messages.Add(ValidationMessage.Error("Project 44 Service Mapping must have a valid code type"));
                valid = false;
            }

            return valid;
        }
    }
}
