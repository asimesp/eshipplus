﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.Processor.Validation.Core
{
    public class DeveloperAccessRequestValidator : ValidatorBase
    {
        public bool IsValid(DeveloperAccessRequest developerAccessRequest)
        {
            var valid = HasAllRequiredData(developerAccessRequest);

            return valid;
        }

        private bool HasAllRequiredData(DeveloperAccessRequest developerAccessRequest)
        {
            var valid = true;

            if (developerAccessRequest.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Developer access request requires a tenant association"));
                valid = false;
            }

            if(developerAccessRequest.Customer == null)
            {
                Messages.Add(ValidationMessage.Error("Developer access request requires a customer association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(developerAccessRequest.ContactName))
            {
                Messages.Add(ValidationMessage.Error("Developer access request requires a contact name"));
                valid = false;
            }
            else if (developerAccessRequest.ContactName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Developer access request contact name cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(developerAccessRequest.ContactEmail))
            {
                Messages.Add(ValidationMessage.Error("Developer access request requires a contact email"));
                valid = false;
            }
            else if(developerAccessRequest.ContactEmail.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Developer access request contact email cannot exceed 100 characters"));
                valid = false; 
            }

            if (!string.IsNullOrEmpty(developerAccessRequest.ContactEmail) && !developerAccessRequest.ContactEmail.EmailIsValid())
            {
                Messages.Add(ValidationMessage.Error("Developer access request contact email is invalid"));
                valid = false;
            }

            if (!developerAccessRequest.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Developer Access Request created must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            return valid;
        }

    }
}
