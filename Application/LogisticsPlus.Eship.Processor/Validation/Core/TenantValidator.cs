﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Core
{
	public class TenantValidator : ValidatorBase
	{
		public bool IsValid(Tenant tenant)
		{
			Messages.Clear();

			bool valid = IsUnique(tenant);

			if (!HasAllRequiredData(tenant)) valid = false;

			return valid;
		}

		public bool IsUnique(Tenant tenant)
		{
			var query = tenant.IsNew
			            	? "Select count(*) from Tenant where Code = @Code"
			            	: "Select count(*) from Tenant where Code = @Code and Id <> @Id";
			var parameters = new Dictionary<string, object> {{"Code", tenant.Code}};
			if (!tenant.IsNew) parameters.Add("Id", tenant.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Tenant Code is not unique"));
			return isUnique;
		}

		public bool HasAllRequiredData(Tenant tenant)
		{
			var valid = true;

			if (string.IsNullOrEmpty(tenant.Code))
			{
				Messages.Add(ValidationMessage.Error("Tenant requires a Tenant Code"));
				valid = false;
			}
			else if (tenant.Code.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Tenant Code cannot exceed 50 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(tenant.Name))
			{
				Messages.Add(ValidationMessage.Error("Tenant requires a Tenant Name"));
				valid = false;
			}
			else if (tenant.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Tenant Name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tenant.TenantScac) && tenant.TenantScac.Length > 10)
			{
				Messages.Add(ValidationMessage.Error("Tenant SCAC cannot exceed 10 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tenant.LogoUrl) && tenant.LogoUrl.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Tenant Logo URL cannot exceed 200 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tenant.TermsAndConditionsFile) && tenant.TermsAndConditionsFile.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Tenant terms and conditions file length cannot excced 100 characters"));
				valid = false;
			}

			var seeds = ProcessorUtilities.GetAll<AutoNumberCode>();
			if (tenant.AutoNumbers.Count != seeds.Count)
			{
				Messages.Add(ValidationMessage.Error("System error: auto number seed count does not match system count."));
				valid = false;
			}
			else
			{
				foreach (var autonumber in tenant.AutoNumbers)
				{
					if (!seeds.ContainsKey(autonumber.Code.ToInt()))
					{
						Messages.Add(ValidationMessage.Error("Invalid auto number seed code [{0}]", autonumber.Code));
						valid = false;
					}
					if (autonumber.NextNumber <= 0)
					{
						Messages.Add(ValidationMessage.Error("Invalid auto number value [Code: {0} Value: {1}]", autonumber.Code,
						                                     autonumber.NextNumber));
						valid = false;
					}
				}
			}

			if(tenant.InsuranceCostPerDollar < 0)
			{
				Messages.Add(ValidationMessage.Error("Tenant insurance cost per dollar must be greater than 0"));
				valid = false;
			}

			if (tenant.InsuranceCostPurchaseFloor < 0)
			{
				Messages.Add(ValidationMessage.Error("Tenant minimum insurance purchase must be greater than 0"));
				valid = false;
			}

			if (tenant.InsuranceCostTotalPercentage < 0)
			{
				Messages.Add(ValidationMessage.Error("Tenant insurance cost total percentage must be greater than 0"));
				valid = false;
			}

			if (tenant.InsuranceRevenueRatePerDollar < 0)
			{
				Messages.Add(ValidationMessage.Error("Tenant insurance revenue rate per dollar must be greater than 0"));
				valid = false;
			}

			if (tenant.InsuranceRevenueTotalPercentage < 0)
			{
				Messages.Add(ValidationMessage.Error("Tenant insurance revenue total percentage must be greater than 0"));
				valid = false;
			}

			if(tenant.SMCRateWareEnabled && string.IsNullOrEmpty(tenant.SMCRateWareParameterFile))
			{
				Messages.Add(ValidationMessage.Error("Tenant SMC Rateware is enabled but missing a parameter file path"));
				valid = false;
			}
			
			if(!string.IsNullOrEmpty(tenant.SMCRateWareParameterFile) && tenant.SMCRateWareParameterFile.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Tenant SMC Rateware parameter file path cannot exceed 100 characters"));
				valid = false;
			}

			if (tenant.SMCCarrierConnectEnabled && string.IsNullOrEmpty(tenant.SMCCarrierConnectParameterFile))
			{
				Messages.Add(ValidationMessage.Error("Tenant SMC carrier connect is enabled but missing a parameter file path"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tenant.SMCCarrierConnectParameterFile) && tenant.SMCCarrierConnectParameterFile.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Tenant SMC carrier connect parameter file path cannot exceed 100 characters"));
				valid = false;
			}

			if (tenant.FedExSmallPackEnabled && string.IsNullOrEmpty(tenant.FedExSmallPackParameterFile))
			{
				Messages.Add(ValidationMessage.Error("Tenant FedEx small pack is enabled but missing a parameter file path"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tenant.FedExSmallPackParameterFile) && tenant.FedExSmallPackParameterFile.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Tenant FedEx small pack parameter file path cannot exceed 100 characters"));
				valid = false;
			}

			if (tenant.UpsSmallPackEnabled && string.IsNullOrEmpty(tenant.UpsSmallPackParameterFile))
			{
				Messages.Add(ValidationMessage.Error("Tenant Ups small pack is enabled but missing a parameter file path"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tenant.UpsSmallPackParameterFile) && tenant.UpsSmallPackParameterFile.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Tenant Ups small pack parameter file path cannot exceed 100 characters"));
				valid = false;
			}


			if (tenant.MacroPointEnabled && string.IsNullOrEmpty(tenant.MacroPointParameterFile))
			{
				Messages.Add(ValidationMessage.Error("Tenant Macro point is enabled but missing a parameter file path"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tenant.MacroPointParameterFile) && tenant.MacroPointParameterFile.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Tenant Macro Point parameter file path cannot exceed 100 characters"));
				valid = false;
			}

			if(!tenant.BatchRatingAnalysisStartTime.IsValidTimeString())
			{
				Messages.Add(ValidationMessage.Error("Tenant batch rating analysis start time is not valid"));
				valid = false;
			}

			if (!tenant.BatchRatingAnalysisEndTime.IsValidTimeString())
			{
				Messages.Add(ValidationMessage.Error("Tenant batch rating analysis end time is not valid"));
				valid = false;
			}

			if (!tenant.DefaultMileageEngine.ToInt().EnumValueIsValid<MileageEngine>())
			{
				Messages.Add(ValidationMessage.Error("Tenant mileage engine is invalid"));
				valid = false;
			}

			if (tenant.TruckloadDeliveryTolerance < 0)
			{
				Messages.Add(ValidationMessage.Error("Tenant Truckload Delivery Tolerance cannot be less than zero"));
				valid = false;
			}

			if (tenant.TruckloadPickupTolerance < 0)
			{
				Messages.Add(ValidationMessage.Error("Tenant Truckload Pickup Tolerance cannot be less than zero"));
				valid = false;
			}

			if (tenant.ShipmentDocImgRtrvAllowance < 0)
			{
				Messages.Add(ValidationMessage.Error("Tenant Shipemnt Document Image Retrieval Allowance cannot be less than zero"));
				valid = false;
			}

            if (!tenant.PaymentGatewayType.ToInt().EnumValueIsValid<PaymentGatewayType>())
            {
                Messages.Add(ValidationMessage.Error("Tenant payment gateway type is invalid"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(tenant.PaymentGatewayLoginId) && tenant.PaymentGatewayLoginId.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Tenant payment gateway login id cannot exceed 500 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(tenant.PaymentGatewaySecret) && tenant.PaymentGatewaySecret.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Tenant payment gateway secret cannot exceed 500 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(tenant.PaymentGatewayTransactionId) && tenant.PaymentGatewayTransactionId.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Tenant payment gateway transaction id cannot exceed 500 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(tenant.AutoNotificationSubjectPrefix) && tenant.AutoNotificationSubjectPrefix.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Tenant auto notification subject prefix cannot exceed 50 characters"));
				valid = false;
			}



			if (tenant.BillingLocation != null && !HasValidLocationInformation(tenant.BillingLocation)) valid = false;
			if (tenant.BillingLocation != null && !HasValidLocationContactInformation(tenant.BillingLocation.Contacts)) valid = false;
			if (tenant.MailingLocation != null && !HasValidLocationInformation(tenant.MailingLocation)) valid = false;
			if (tenant.MailingLocation != null && !HasValidLocationContactInformation(tenant.MailingLocation.Contacts)) valid = false;

			return valid;
		}

		public bool CanDeleteTenant(Tenant tenant)
		{
			const string query = "select dbo.TenantIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> {{"Id", tenant.Id}};
			var used = ExecuteScalar(query, parameters).ToInt() > (4 + ProcessorUtilities.GetAll<AutoNumberCode>().Count);
			if (used) Messages.Add(ValidationMessage.Error("Tenant is referenced one or more times and cannot be deleted."));
			return !used;
		}

		private bool HasValidLocationInformation(TenantLocation location)
		{
			var valid = true;

			if (!string.IsNullOrEmpty(location.Street1) && location.Street1.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Tenant location Street1 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(location.Street2) && location.Street2.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Tenant location Street2 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(location.City) && location.City.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Tenant location City cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(location.State) && location.State.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Tenant location State cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(location.PostalCode) && location.PostalCode.Length > 10)
			{
				Messages.Add(ValidationMessage.Error("Tenant location Postal Code cannot exceed 10 characters"));
				valid = false;
			}

			if(location.CountryId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tenant location requires a country association"));
				valid = false;
			}

			if (location.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tenant location requires a tenant association"));
				valid = false;
			}
            if (location.Contacts.Count > 0)
            {
                if (!HasValidLocationContactInformation(location.Contacts)) valid = false;
                if (location.Contacts.Count(c => c.Primary) != 1)
                {
                    Messages.Add(ValidationMessage.Error("Tenant location should have one (and only one) primary contact"));
                    valid = false;
                }
            }

			return valid;
		}

		private bool HasValidLocationContactInformation(IEnumerable<TenantContact> contacts)
		{
			var valid = true;

			foreach(var contact in contacts)
			{
				if(contact.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Tenant contact requires a tenant association"));
					valid = false;
				}

				if(contact.ContactTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Tenant contact requires a contact type association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(contact.Name))
				{
					Messages.Add(ValidationMessage.Error("Tenant contact name is required"));
					valid = false;
				}
				else if (contact.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Tenant contact name cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Phone) && contact.Phone.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Tenant contact phone cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Mobile) && contact.Mobile.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Tenant contact mobile cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Fax) && contact.Fax.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Tenant contact fax cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Email) && contact.Email.Length > 100)
				{
					Messages.Add(ValidationMessage.Error("Tenant contact email cannot exceed 100 characters"));
					valid = false;
				}

			    if (!string.IsNullOrEmpty(contact.Email) && !contact.Email.EmailIsValid())
			    {
			        Messages.Add(ValidationMessage.Error("Tenant contact email is invalid"));
			        valid = false;
			    }

                if (!string.IsNullOrEmpty(contact.Comment) && contact.Comment.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Tenant contact comment cannot exceed 200 characters"));
					valid = false;
				}
			}

			return valid;
		}
	}
}
