﻿using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.Processor.Validation.Core
{
    public class AnnouncementValidator : ValidatorBase
    {
        public bool IsValid(Announcement announcement)
        {
            Messages.Clear();

            bool valid = HasAllRequiredData(announcement);

            return valid;
        }

        public bool HasAllRequiredData(Announcement announcement)
        {
            var valid = true;

            if (!announcement.Type.ToInt().EnumValueIsValid<AnnouncementType>())
            {
                Messages.Add(ValidationMessage.Error("Announcement type is invalid"));
                valid = false;
            }

            if (string.IsNullOrEmpty(announcement.Name))
            {
                Messages.Add(ValidationMessage.Error("Announcement requires a Name"));
                valid = false;
            }
            else if (announcement.Name.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Announcement Name cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(announcement.Message))
            {
                Messages.Add(ValidationMessage.Error("Announcement requires a message"));
                valid = false;
            }
            else if (announcement.Message.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Announcement Message cannot exceed 500 characters"));
                valid = false;
            }
            return valid;
        }
    }
}
