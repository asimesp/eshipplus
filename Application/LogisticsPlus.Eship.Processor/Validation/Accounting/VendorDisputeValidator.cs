﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
    public class VendorDisputeValidator : ValidatorBase
    {
        public bool IsValid(VendorDispute vendorDispute)
        {
            return HasAllRequiredData(vendorDispute);
        }

        public bool DuplicateDisputerReferenceNumber(VendorDispute vendorDispute)
        {
            const string query = "SELECT COUNT(*) FROM VendorDispute WHERE DisputerReferenceNumber = @DisputerReferenceNumber AND TenantId = @TenantId AND Id <> @Id";
            var parameters = new Dictionary<string, object>
                                 {
                                     {"DisputerReferenceNumber", vendorDispute.DisputerReferenceNumber},
                                     {"TenantId", vendorDispute.TenantId},
                                     {"Id", vendorDispute.Id}
                                 };
            var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateNumber) Messages.Add(ValidationMessage.Error("Disputer reference number is already in use"));
            return duplicateNumber;
        }

		public bool DuplicateDisputeCharge(VendorDisputeDetail vendorDisputeDetail)
		{
			const string query =
				"SELECT COUNT(*) FROM VendorDisputeDetail vdd LEFT JOIN VendorDispute vd ON vd.Id = vdd.DisputedId WHERE vdd.ChargeLineId = @ChargeLineId AND vdd.ChargeLineIdType = @ChargeLineIdType AND vdd.TenantId = @TenantId AND vdd.Id <> @Id AND vd.IsResolved = 0";
			var parameters = new Dictionary<string, object>
                                 {
                                     {"ChargeLineId", vendorDisputeDetail.ChargeLineId},
                                     {"ChargeLineIdType", vendorDisputeDetail.ChargeLineIdType},
                                     {"TenantId", vendorDisputeDetail.TenantId},
                                     {"Id", vendorDisputeDetail.Id}
                                 };
			var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
		
			return duplicateNumber;
		}

        public bool HasAllRequiredData(VendorDispute vendorDispute)
        {
            var valid = true;

            if (string.IsNullOrEmpty(vendorDispute.DisputerReferenceNumber))
            {
                Messages.Add(ValidationMessage.Error("Vendor Dispute requires a disputer reference number"));
                valid = false;
            }
            else if (vendorDispute.DisputerReferenceNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Disputer reference number cannot exceed 50 characters"));
                valid = false;
            }

            else if (!vendorDispute.DateCreated.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Vendor Dispute must have a valid creation date between {0} and {1}",
                                                      DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (vendorDispute.CreatedByUser == null)
            {
                Messages.Add(ValidationMessage.Error("Vendor Dispute requires a user association"));
                valid = false;
            }

            if (vendorDispute.Vendor == null)
            {
                Messages.Add(ValidationMessage.Error("Vendor Dispute requires a vendor association"));
                valid = false;
            }
		
            if (vendorDispute.IsResolved)
            {
                if (vendorDispute.ResolvedByUser == null)
                {
                    Messages.Add(ValidationMessage.Error("Vendor Dispute requires a resolved user"));
                    valid = false;
                }

                if (!vendorDispute.ResolutionDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Vendor Dispute must have a valid resolution date between {0} and {1}",
                                                         vendorDispute.DateCreated, DateUtility.SystemLatestDateTime));
                    valid = false;
                }
            }

            if (vendorDispute.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor requires a tenant association"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(vendorDispute.ResolutionComments) && vendorDispute.ResolutionComments.Length > 1000)
            {
                Messages.Add(ValidationMessage.Error("Vendor Dispute resolution comments cannot exceed 1000 characters"));
                valid = false;
            }

            if (vendorDispute.VendorDisputeDetails != null && vendorDispute.VendorDisputeDetails.Count > 0)
            {
                valid = VendorDisputeDetailsAreValid(vendorDispute.VendorDisputeDetails);
            }

			if (string.IsNullOrEmpty(vendorDispute.DisputeComments))
			{
				Messages.Add(ValidationMessage.Error("Vendor Dispute comment is required"));
				valid = false;
			}
			else if (vendorDispute.DisputeComments.Length > 1000)
			{
				Messages.Add(ValidationMessage.Error("Vendor Dispute comment cannot exceet 1000 characters"));
				valid = false;
			}
			
            return valid;
        }

        private bool VendorDisputeDetailsAreValid(List<VendorDisputeDetail> vendorDisputeDetails)
        {
            var valid = true;

            foreach (var vendorDisputeDetail in vendorDisputeDetails)
            {
                if (vendorDisputeDetail.VendorDispute == null)
                {
                    Messages.Add(ValidationMessage.Error("Vendor dispute detail requires a dispute association"));
                    valid = false;
                    break;
                }

                if (vendorDisputeDetail.OriginalChargeAmount < 0)
                {
                    Messages.Add(ValidationMessage.Error("Original charge amount cannot be less than 0"));
                    valid = false;
                    break;
                }

                if (vendorDisputeDetail.RevisedChargeAmount < 0)
                {
                    Messages.Add(ValidationMessage.Error("Revised charge amount cannot be less than 0"));
                    valid = false;
                    break;
                }

                if (vendorDisputeDetail.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Vendor dispute detail requires a tenant association"));
                    valid = false;
                    break;
                }

				if (DuplicateDisputeCharge(vendorDisputeDetail))
				{
					Messages.Add(ValidationMessage.Error("Some of selected charges are already disputed"));
					valid = false;
					break;
				}
            }

            return valid;
        }
    }
}