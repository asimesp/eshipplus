﻿using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
	public class CustomerControlAccountValidator : ValidatorBase
	{
		public bool IsValid(CustomerControlAccount customerControlAccount)
		{
			Messages.Clear();

			var valid = true;

			if (!HasAllRequiredData(customerControlAccount)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(CustomerControlAccount account)
		{
			var valid = true;

			if (account.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Control Account requires a Tenant association"));
				valid = false;
			}

			if (account.Customer == null)
			{
				Messages.Add(ValidationMessage.Error("Control Account must have a Customer Association"));
				valid = false;
			}

			if (account.Country == null)
			{
				Messages.Add(ValidationMessage.Error("Control Account must have a Country Association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(account.AccountNumber))
			{
				Messages.Add(ValidationMessage.Error("Control Account requires an Account Number"));
				valid = false;
			}
			else if (account.AccountNumber.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Control Account Number cannot exceed 100 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(account.GenericCategory) && account.GenericCategory.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Control Account generic category cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(account.Description) && account.Description.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Control Account Description cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(account.Street1) && account.Street1.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Control Account Street1 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(account.Street2) && account.Street2.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Control Account Street2 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(account.City) && account.City.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Control Account City cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(account.State) && account.State.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Control Account State cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(account.PostalCode) && account.PostalCode.Length > 10)
			{
				Messages.Add(ValidationMessage.Error("Control Account Postal Code cannot exceed 10 characters"));
				valid = false;
			}


			return valid;
		}
	}
}
