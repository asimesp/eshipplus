﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
    public class MiscReceiptValidator : ValidatorBase
    {
        public bool IsValid(MiscReceipt miscReceipt)
        {
            return HasAllRequiredData(miscReceipt);
        }

        public bool HasAllRequiredData(MiscReceipt miscReceipt)
		{
            var valid = true;

            if (miscReceipt.UserId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Misc Receipt requires a user association"));
                valid = false;
            }

            if (miscReceipt.CustomerId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Misc Receipt requires a customer association"));
                valid = false;
            }

            if (!miscReceipt.Reversal && miscReceipt.OriginalMiscReceiptId != default(long))
            {
                Messages.Add(ValidationMessage.Error("Misc Receipt must be a reversal is there exists an original misc receipt association"));
                valid = false;
            }

            if (miscReceipt.Reversal && miscReceipt.OriginalMiscReceiptId != default(long) && miscReceipt.MiscReceiptApplications.Any())
            {
                Messages.Add(ValidationMessage.Error("Misc Receipt cannot have misc receipt applications when it is a reversal of another misc receipt"));
                valid = false;
            }

            if (miscReceipt.AmountPaid <= 0 || miscReceipt.AmountPaid > 99999999999999.9999m)
            {
                Messages.Add(ValidationMessage.Error("Misc Receipt amount paid must be greater than zero and less than 100,000,000,000,000"));
                valid = false;
            }

            if (!miscReceipt.PaymentDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Misc Receipt payment date must be between {0} and {1}", DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

            if (!string.IsNullOrEmpty(miscReceipt.GatewayTransactionId) && miscReceipt.GatewayTransactionId.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Misc Receipt gateway transaction id cannot exceed 100 characters"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(miscReceipt.PaymentProfileId) && miscReceipt.PaymentProfileId.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Misc Receipt payment profile id cannot exceed 100 characters"));
                valid = false;
            }

            if (!miscReceipt.PaymentGatewayType.ToInt().EnumValueIsValid<PaymentGatewayType>())
            {
                Messages.Add(ValidationMessage.Error("Misc Receipt must have a valid payment gateway type"));
                valid = false;
            }

            if (miscReceipt.MiscReceiptApplications.Count > 0 && !MiscReceiptApplicationsAreValid(miscReceipt.MiscReceiptApplications)) valid = false;

            return valid;
		}

        public bool MiscReceiptApplicationsAreValid(IEnumerable<MiscReceiptApplication> applications)
        {
            var valid = true;

            foreach (var application in applications)
            {
                if (application.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Misc Receipt Application requires a tenant association"));
                    valid = false;
                }

                if (application.MiscReceipt == null)
                {
                    Messages.Add(ValidationMessage.Error("Misc Receipt Application requires an misc receipt association"));
                    valid = false;
                }

                if (application.InvoiceId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Misc Receipt Application requires an invoice association"));
                    valid = false;
                }

                if (application.Invoice.InvoiceType == InvoiceType.Credit)
                {
                    Messages.Add(ValidationMessage.Error("Misc Receipt cannot be applied to a credit invoice"));
                    valid = false;
                }

                if (!application.ApplicationDate.IsValidSystemDateTime())
                {
                    Messages.Add(ValidationMessage.Error("Misc Receipt Application application date must be between {0} and {1}", DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }

                if (application.Amount <= 0)
                {
                    Messages.Add(ValidationMessage.Error("Misc Receipt Application amount must be greater than zero"));
                    valid = false;
                }
            }
            return valid;
        }

        public bool MiscReceiptShipmentExists(MiscReceipt miscReceipt)
        {
            const string query = "Select Count(*) From Shipment Where Id = @ShipmentId And TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "ShipmentId", miscReceipt.ShipmentId }, { "TenantId", miscReceipt.TenantId } };
            var shipmentExists = ExecuteScalar(query, parameters).ToInt() > 0;
            if (!shipmentExists) Messages.Add(ValidationMessage.Error("Misc Receipt Shipment reference is invalid."));
            return shipmentExists;
        }

        public bool MiscReceiptLoadOrderExists(MiscReceipt miscReceipt)
        {
            const string query = "Select Count(*) From LoadOrder Where Id = @LoadOrderId And TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "LoadOrderId", miscReceipt.LoadOrderId }, { "TenantId", miscReceipt.TenantId } };
            var shipmentExists = ExecuteScalar(query, parameters).ToInt() > 0;
            if (!shipmentExists) Messages.Add(ValidationMessage.Error("Misc Receipt Load Order reference is invalid."));
            return shipmentExists;
        }
    }
}
