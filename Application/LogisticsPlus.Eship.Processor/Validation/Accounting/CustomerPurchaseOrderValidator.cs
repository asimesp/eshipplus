﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
    public class CustomerPurchaseOrderValidator : ValidatorBase
    {
        public bool IsValid(CustomerPurchaseOrder customerPurchaseOrder)
        {
            Messages.Clear();

	        var valid = IsUnique(customerPurchaseOrder);
	        if (!HasAllRequiredData(customerPurchaseOrder)) valid = false;

            return valid;
        }

        public bool IsUnique(CustomerPurchaseOrder customerPurchaseOrder)
        {
            const string query = "SELECT count(*) FROM CustomerPurchaseOrder WHERE PurchaseOrderNumber = @PurchaseOrderNumber " +
                                 "AND TenantId = @TenantId " +
                                 "AND CustomerId = @CustomerID " +
                                 "AND Id <> @Id";

            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"PurchaseOrderNumber", customerPurchaseOrder.PurchaseOrderNumber}, 
									{"TenantId", customerPurchaseOrder.TenantId },
                                    {"CustomerID", customerPurchaseOrder.Customer.Id},
                                    {"Id", customerPurchaseOrder.Id}
			                 	};

            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
            if (!isUnique) Messages.Add(ValidationMessage.Error("Customer Purchase Order Number is not Unique"));

            return isUnique;
        }

        public bool HasAllRequiredData(CustomerPurchaseOrder customerPurchaseOrder)
        {
            var valid = true;

            if (customerPurchaseOrder.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Purchase Order requires a Tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(customerPurchaseOrder.PurchaseOrderNumber))
            {
                Messages.Add(ValidationMessage.Error("Purchase Order requires a Purchase Order Number"));
                valid = false;
            }
            else if (customerPurchaseOrder.PurchaseOrderNumber.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Purchase Order Number cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(customerPurchaseOrder.ValidPostalCode))
            {
                Messages.Add(ValidationMessage.Error("Purchase Order requires a Valid Postal Code"));
                valid = false;
            }
            else if (customerPurchaseOrder.ValidPostalCode.Length > 10)
            {
                Messages.Add(ValidationMessage.Error("Purchase Order Valid Postal Code cannot exceed 10 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(customerPurchaseOrder.AdditionalPurchaseOrderNotes) && customerPurchaseOrder.AdditionalPurchaseOrderNotes.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Purchase Order additional notes cannot exceed 50 characters"));
				valid = false;
			}

        	if (customerPurchaseOrder.Customer == null)
            {
                Messages.Add(ValidationMessage.Error("Purchase Order must have a Customer Association"));
                valid = false;
            }

            if (!customerPurchaseOrder.ExpirationDate.IsValidSystemDateTime())
            {
                Messages.Add(ValidationMessage.Error("Purchase Order Expiration Date must be between {0} and {1}",
                                                     DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                valid = false;
            }

			if (!customerPurchaseOrder.ApplyOnOrigin && !customerPurchaseOrder.ApplyOnDestination)
			{
				Messages.Add(ValidationMessage.Error("Purchase Order must be applied to one of the following: origin or destination"));
				valid = false;
			}

            if (customerPurchaseOrder.ApplyOnOrigin)
            {
                if (customerPurchaseOrder.ApplyOnDestination)
                {
                    Messages.Add(ValidationMessage.Error("Purchase Order can only be applied to one of the following: origin or destination"));
                    valid = false;
                }
            }
		

            if(customerPurchaseOrder.Country == null)
            {
                Messages.Add(ValidationMessage.Error("Purchase Order must have a Country Association"));
                valid = false;
            }

	        if (customerPurchaseOrder.Country != null 
				&& !string.IsNullOrEmpty(customerPurchaseOrder.ValidPostalCode) 
				&& !string.IsNullOrEmpty(customerPurchaseOrder.Country.PostalCodeValidation))
	        {
		        var postalCodeValidatioFormatResult = customerPurchaseOrder.Country.IsValidPostalCodeFormat(customerPurchaseOrder.ValidPostalCode, true);
		        if (!postalCodeValidatioFormatResult)
		        {
			        Messages.Add(ValidationMessage.Error("Postal code \"{0}\" does not match format \"{1}\"", customerPurchaseOrder.ValidPostalCode, customerPurchaseOrder.Country.PostalCodeValidation));
			        valid = false;
		        }
	        }

            return valid;
        }
    }
}
