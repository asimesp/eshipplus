﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
	public class AverageWeeklyFuelValidator : ValidatorBase
	{
		public bool IsValid(AverageWeeklyFuel averageWeeklyFuel)
		{
			Messages.Clear();

			var valid = true;

			if (!HasAllRequiredData(averageWeeklyFuel)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(AverageWeeklyFuel averageWeeklyFuel)
		{
			var valid = true;

			if (averageWeeklyFuel.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel requires a tenant association"));
				valid = false;
			}

			if (averageWeeklyFuel.ChargeCodeId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel requires a charge code association"));
				valid = false;
			}

			if (!averageWeeklyFuel.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel Date Created must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!averageWeeklyFuel.EffectiveDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel Effective Date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}
			
			if (averageWeeklyFuel.EastCoastCost < 0)
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel East Coast Cost must be greater than or equal to zero"));
				valid = false;
			}

			if (averageWeeklyFuel.NewEnglandCost < 0)
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel New England Cost must be greater than or equal to zero"));
				valid = false;
			}

			if (averageWeeklyFuel.CentralAtlanticCost < 0)
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel Central Atlantic Cost must be greater than or equal to zero"));
				valid = false;
			}

			if (averageWeeklyFuel.LowerAtlanticCost < 0)
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel Lower Atlantic Cost must be greater than or equal to zero"));
				valid = false;
			}

			if (averageWeeklyFuel.MidwestCost < 0)
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel Midwest Cost must be greater than or equal to zero"));
				valid = false;
			}

			if (averageWeeklyFuel.GulfCoastCost < 0)
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel Gulf Coast Cost must be greater than or equal to zero"));
				valid = false;
			}

			if (averageWeeklyFuel.RockyMountainCost < 0)
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel Rocky Mountain Cost must be greater than or equal to zero"));
				valid = false;
			}

			if (averageWeeklyFuel.WestCoastCost < 0)
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel West Coast Cost must be greater than or equal to zero"));
				valid = false;
			}

			if(averageWeeklyFuel.WestCoastLessCaliforniaCost < 0)
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel West Coast less California Cost must be greater than or equal to zero"));
			}

			if (averageWeeklyFuel.CaliforniaCost < 0)
			{
				Messages.Add(ValidationMessage.Error("Average Weekly Fuel California Cost must be greater than or equal to zero"));
				valid = false;
			}

			return valid;
		}
	}
}
