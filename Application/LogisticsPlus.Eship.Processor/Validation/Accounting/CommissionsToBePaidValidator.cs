﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
	public class CommissionsToBePaidValidator : ValidatorBase
	{
		public bool IsValid(CommissionPayment commission)
		{
			Messages.Clear();

			bool valid = HasAllRequiredData(commission);

			return valid;
		}

		public bool HasAllRequiredData(CommissionPayment commission)
		{
			var valid = true;

			if (commission.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Commission Payment require a tenant association"));
				valid = false;
			}

			if (commission.SalesRepresentativeId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Commission Payment require a sales representative association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(commission.ReferenceNumber))
			{
				Messages.Add(ValidationMessage.Error("Commission Payment requires a reference number"));
				valid = false;
			}
			else if (commission.ReferenceNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Commission Payment reference number cannot exceed 50 characters"));
				valid = false;
			}

			if (!commission.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Commission Payment date created must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!commission.PaymentDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Commission Payment payment date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (commission.AmountPaid < 0)
			{
				Messages.Add(ValidationMessage.Error("Commission Payment amount paid must be greater than or equal to zero"));
				valid = false;
			}

			return valid;
		}
	}
}
