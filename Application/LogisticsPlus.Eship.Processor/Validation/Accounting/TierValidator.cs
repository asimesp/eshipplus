﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
	public class TierValidator : ValidatorBase
	{
		public bool IsValid(Tier tier)
		{
			var valid = true;

			if (!HasAllRequiredData(tier)) valid = false;
			if (DuplicateTierNumber(tier)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(Tier tier)
		{
			var valid = true;

			if (string.IsNullOrEmpty(tier.Name))
			{
				Messages.Add(ValidationMessage.Error("Tier requires a name"));
				valid = false;
			}
			else if (tier.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Tier name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tier.LogoUrl) && tier.LogoUrl.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Tier logo url cannot exceed 200 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(tier.TierNumber))
			{
				Messages.Add(ValidationMessage.Error("Tier requires a tier number"));
				valid = false;
			}
			else if (tier.TierNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Tier number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tier.AdditionalBillOfLadingText) && tier.AdditionalBillOfLadingText.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Tier additional bill of lading text cannot exceed 500 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tier.LTLNotificationEmails) && tier.LTLNotificationEmails.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Tier LTL notification emails cannot exceed 500 characters"));
				valid = false;
			}
           

		    if (!string.IsNullOrEmpty(tier.LTLNotificationEmails) && tier.LTLNotificationEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
		    {
		        Messages.Add(ValidationMessage.Error("Tier LTL notification emails contains one or more invalid addresses"));
		        valid = false;
		    }

            if (!string.IsNullOrEmpty(tier.FTLNotificationEmails) && tier.FTLNotificationEmails.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Tier FTL notification emails cannot exceed 500 characters"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(tier.FTLNotificationEmails) && tier.FTLNotificationEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
		    {
		        Messages.Add(ValidationMessage.Error("Tier FTL notification emails contains one or more invalid addresses"));
		        valid = false;
		    }

            if (!string.IsNullOrEmpty(tier.SPNotificationEmails) && tier.SPNotificationEmails.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Tier small pack notification emails cannot exceed 500 characters"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(tier.SPNotificationEmails) && tier.SPNotificationEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
		    {
		        Messages.Add(ValidationMessage.Error("Tier small pack notification emails contains one or more invalid addresses"));
		        valid = false;
		    }

            if (!string.IsNullOrEmpty(tier.RailNotificationEmails) && tier.RailNotificationEmails.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Tier rail notification emails cannot exceed 500 characters"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(tier.RailNotificationEmails) && tier.RailNotificationEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
		    {
		        Messages.Add(ValidationMessage.Error("Tier rail notification emails contains one or more invalid addresses"));
		        valid = false;
		    }

            if (!string.IsNullOrEmpty(tier.AirNotificationEmails) && tier.AirNotificationEmails.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Tier air notification emails cannot exceed 500 characters"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(tier.AirNotificationEmails) && tier.AirNotificationEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
		    {
		        Messages.Add(ValidationMessage.Error("Tier air notification emails contains one or more invalid addresses"));
		        valid = false;
		    }

            if (!string.IsNullOrEmpty(tier.GeneralNotificationEmails) && tier.GeneralNotificationEmails.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Tier general notification emails cannot exceed 500 characters"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(tier.GeneralNotificationEmails) && tier.GeneralNotificationEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
		    {
		        Messages.Add(ValidationMessage.Error("Tier general notification emails contains one or more invalid addresses"));
		        valid = false;
		    }

            if (!string.IsNullOrEmpty(tier.PendingVendorNotificationEmails) && tier.PendingVendorNotificationEmails.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Tier pending vendor notification emails cannot exceed 500 characters"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(tier.PendingVendorNotificationEmails) && tier.PendingVendorNotificationEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
		    {
		        Messages.Add(ValidationMessage.Error("Tier pending vendor notification emails contains one or more invalid addresses"));
		        valid = false;
		    }

            if (!string.IsNullOrEmpty(tier.AccountPayableDisputeNotificationEmails) && tier.AccountPayableDisputeNotificationEmails.Length > 500)
            {
                Messages.Add(ValidationMessage.Error("Tier account payable dispute notificatione emails cannot exceed 500 characters"));
                valid = false;
            }

		    if (!string.IsNullOrEmpty(tier.AccountPayableDisputeNotificationEmails) && tier.AccountPayableDisputeNotificationEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
		    {
		        Messages.Add(ValidationMessage.Error("Tier account payable dispute notification emails contains one or more invalid addresses"));
		        valid = false;
		    }

            if (string.IsNullOrEmpty(tier.TollFreeContactNumber))
			{
				Messages.Add(ValidationMessage.Error("Tier requires a toll free contact number"));
				valid = false;
			}
			else if (tier.TollFreeContactNumber.Length > 25)
			{
				Messages.Add(ValidationMessage.Error("Tier toll free contact number cannot exceed 25 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(tier.SupportTollFree) && tier.SupportTollFree.Length > 25)
			{
				Messages.Add(ValidationMessage.Error("Tier support toll free cannot exceed 200 characters"));
				valid = false;
			}


			if (!string.IsNullOrEmpty(tier.TierSupportEmails) && tier.TierSupportEmails.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Tier support emails cannot exceed 500 characters"));
				valid = false;
			}

		    if (!string.IsNullOrEmpty(tier.TierSupportEmails) && tier.TierSupportEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
		    {
		        Messages.Add(ValidationMessage.Error("Tier support emails contains one or more invalid addresses"));
		        valid = false;
		    }


            if (tier.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Tier requires a tenant association"));
				valid = false;
			}

			return valid;
		}

		public bool DuplicateTierNumber(Tier tier)
		{
			const string query = "Select count(*) from Tier where TierNumber = @TierNumber and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
	                            {
	                                {"TierNumber", tier.TierNumber},
	                                {"TenantId", tier.TenantId},
	                                {"Id", tier.Id}
	                            };
			var duplicateTier = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateTier) Messages.Add(ValidationMessage.Error("Tier number is already in use"));
			return duplicateTier;
		}

		public bool CanDeleteTier(Tier tier)
		{
			const string query = "select dbo.TierIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", tier.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Tier is referenced one or more times and cannot be deleted."));
			return !used;
		}
	}
}
