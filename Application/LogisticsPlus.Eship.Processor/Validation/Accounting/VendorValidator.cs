﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
	public class VendorValidator : ValidatorBase
	{
		public bool CanDeleteVendor(Vendor vendor)
		{
			const string query = "select dbo.VendorIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", vendor.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Vendor is referenced one or more times and cannot be deleted."));
			return !used;
		}

		public bool CanDeleteVendorLocation(VendorLocation location)
		{
			const string query = "select COUNT(*) from VendorBill where VendorLocationId = @Id and Tenantid = @TenantId";
			var parameters = new Dictionary<string, object> { { "Id", location.Id }, { "TenantId", location.TenantId } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used)
				Messages.Add(ValidationMessage.Error("Vendor location [#{0}] is referenced one or more times and cannot be deleted.",
											location.LocationNumber));
			return !used;
		}

		public bool VendorServiceExists(VendorService vendorService)
		{
			const string query =
				"Select count(*) from VendorService where ServiceId = @ServiceId and VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"ServiceId", vendorService.ServiceId},
			                 		{"TenantId", vendorService.TenantId},
			                 		{"VendorId", vendorService.VendorId}
			                 	};
			var duplicateService = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateService)
				Messages.Add(ValidationMessage.Error("Service is already included in list of vendor services"));
			return duplicateService;
		}

		public bool VendorEquipmentExists(VendorEquipment vendorEquipment)
		{
			const string query =
				"Select count(*) from VendorEquipment where EquipmentTypeId = @EquipmentTypeId and VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"EquipmentTypeId", vendorEquipment.EquipmentTypeId},
			                 		{"TenantId", vendorEquipment.TenantId},
			                 		{"VendorId", vendorEquipment.VendorId}
			                 	};
			var duplicateCsr = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateCsr)
				Messages.Add(ValidationMessage.Error("Equipment is already included in list of vendor equipments"));
			return duplicateCsr;
		}

		public bool VendorNoServiceDayExists(VendorNoServiceDay vendorNoServiceDay)
		{
			const string query =
				"Select count(*) from VendorNoServiceDay where NoServiceDayId = @NoServiceDayId and VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"NoServiceDayId", vendorNoServiceDay.NoServiceDayId},
			                 		{"TenantId", vendorNoServiceDay.TenantId},
			                 		{"VendorId", vendorNoServiceDay.VendorId}
			                 	};
			var duplicationNsd = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicationNsd)
				Messages.Add(ValidationMessage.Error("NoServiceDay is already included in list of vendor no service days"));
			return duplicationNsd;
		}

		public bool IsValid(Vendor vendor)
		{
			var valid = HasAllRequiredData(vendor);

			if (DuplicateVendorNumber(vendor)) valid = false;
            if (DuplicateVendorMc(vendor)) valid = false;
            if (DuplicateVendorDot(vendor)) valid = false;

			return valid;
		}

		public bool DuplicateVendorNumber(Vendor vendor)
		{
			const string query = @"Select count(*) from Vendor where VendorNumber = @VendorNumber and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"VendorNumber", vendor.VendorNumber},
			                 		{"TenantId", vendor.TenantId},
			                 		{"Id", vendor.Id}
			                 	};
			var duplicateGroupName = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateGroupName) Messages.Add(ValidationMessage.Error("Vendor number is already in use"));
			return duplicateGroupName;
		}

		public bool DuplicateVendorLocationNumber(VendorLocation location)
		{
			const string query = @"Select count(*) from VendorLocation where LocationNumber = @LocationNumber and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"LocationNumber", location.LocationNumber},
			                 		{"TenantId", location.TenantId},
			                 		{"Id", location.Id}
			                 	};
			var duplicateGroupName = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateGroupName) Messages.Add(ValidationMessage.Error("Vendor Location number is already in use"));
			return duplicateGroupName;
		}

        public bool DuplicateVendorMc(Vendor vendor)
        {
            if (string.IsNullOrEmpty(vendor.MC)) return false;
            const string query = "Select count(*) from Vendor where MC = @MC and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"MC", vendor.MC},
			                 		{"TenantId", vendor.TenantId},
			                 		{"Id", vendor.Id}
			                 	};
            var duplicateGroupName = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateGroupName) Messages.Add(ValidationMessage.Error("Vendor MC is already in use"));
            return duplicateGroupName;
        }

        public bool DuplicateVendorDot(Vendor vendor)
        {
            if (string.IsNullOrEmpty(vendor.DOT)) return false;
            const string query = "Select count(*) from Vendor where DOT = @DOT and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"DOT", vendor.DOT},
			                 		{"TenantId", vendor.TenantId},
			                 		{"Id", vendor.Id}
			                 	};
            var duplicateGroupName = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateGroupName) Messages.Add(ValidationMessage.Error("Vendor DOT is already in use"));
            return duplicateGroupName;
        }

		public bool HasAllRequiredData(Vendor vendor)
		{
			var valid = true;

			if (string.IsNullOrEmpty(vendor.Name))
			{
				Messages.Add(ValidationMessage.Error("Vendor requires a name"));
				valid = false;
			}
			else if (vendor.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor name cannot exceed 50 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(vendor.VendorNumber))
			{
				Messages.Add(ValidationMessage.Error("Vendor requires a vendor number"));
				valid = false;
			}
			else if (vendor.VendorNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.Notation) && vendor.Notation.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Vendor notation (special notes) cannot exceed 100 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.Notes) && vendor.Notes.Length > 1000)
			{
				Messages.Add(ValidationMessage.Error("Vendor quick reference notes cannot exceed 1000 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.CustomField1) && vendor.CustomField1.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor custom field 1 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.CustomField2) && vendor.CustomField2.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor custom field 2 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.CustomField3) && vendor.CustomField3.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor custom field 3 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.CustomField4) && vendor.CustomField4.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor custom field 4 cannot exceed 50 characters"));
				valid = false;
			}

			if (vendor.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Vendor requires a tenant association"));
				valid = false;
			}

			if (!vendor.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Vendor must have a valid creation date between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.LogoUrl) && vendor.LogoUrl.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Logo url path length cannot exceed 200 characters"));
				valid = false;
			}

			if (!vendor.LastAuditDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Vendor must have a valid last audit date between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (vendor.HandlesLessThanTruckload)
			{
				if (string.IsNullOrEmpty(vendor.Scac))
				{
					Messages.Add(ValidationMessage.Error("Vendor that handles LTL shipment must have a vendor SCAC code"));
					valid = false;
				}
				else if (vendor.Scac.Length > 4)
				{
					Messages.Add(ValidationMessage.Error("Vendor SCAC code cannot exceed 4 characters"));
					valid = false;
				}
			}

			if (!string.IsNullOrEmpty(vendor.CTPATNumber) && vendor.CTPATNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor CTPAT number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.PIPNumber) && vendor.PIPNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor PIP number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.FederalIDNumber) && vendor.FederalIDNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor federal ID number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.BrokerReferenceNumber) && vendor.BrokerReferenceNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor broker reference number cannot exceed 50 characters"));
				valid = false;
			}

			if(!((int)vendor.BrokerReferenceNumberType).EnumValueIsValid<BrokerReferenceNumberType>())
			{
				Messages.Add(ValidationMessage.Error("Vendor broker reference number type is invalid"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.MC) && vendor.MC.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor MC cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.DOT) && vendor.DOT.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor DOT cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(vendor.TrackingUrl) && vendor.TrackingUrl.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Vendor tracking URL cannot exceed 200 characters"));
				valid = false;
			}

			if (vendor.Locations.Count == 0)
			{
				Messages.Add(ValidationMessage.Error("Vendor must have at least one location"));
				valid = false;
			}
			else
			{
				if (!HasValidLocationInformation(vendor.Locations)) valid = false;
				if (vendor.Locations.Count(l => l.Primary) != 1)
				{
					Messages.Add(ValidationMessage.Error("Vendor must have at least one, and only one, primary location"));
					valid = false;
				}
                if (vendor.Locations.Any(l => l.RemitToLocation) && vendor.Locations.Count(l => l.RemitToLocation && l.MainRemitToLocation) != 1)
                {
                    Messages.Add(ValidationMessage.Error("If a vendor has a remit to location, one and only one remit to location must be the main remit to location"));
                    valid = false;
                }
			}

			if (vendor.Insurances.Count > 0 && !VendorInsurancesAreValid(vendor.Insurances)) valid = false;
			if (vendor.VendorPackageCustomMappings.Count > 0 && !VendorPackageCustomMappingValid(vendor.VendorPackageCustomMappings)) valid = false;
			if (vendor.Services.Count > 0 && !VendorServicesAreValid(vendor.Services)) valid = false;
			if (vendor.Equipments.Count > 0 && !VendorEquipmentsAreValid(vendor.Equipments)) valid = false;
			if (vendor.NoServiceDays.Count > 0 && !HasValidNoServiceDays(vendor.NoServiceDays)) valid = false;
			if (vendor.Documents.Count > 0 && !VendorDocumentsAreValid(vendor.Documents)) valid = false;

			return valid;
		}

		private bool HasValidNoServiceDays(IEnumerable<VendorNoServiceDay> noServiceDays)
		{
			var valid = true;

			foreach (var noServiceDay in noServiceDays)
			{
				if (noServiceDay.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor no service day requires a tenant association"));
					valid = false;
				}

				if (noServiceDay.Vendor == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor no service day requires a vendor association"));
					valid = false;
				}

				if (noServiceDay.NoServiceDay == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor no service day requires a no service day association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool HasValidLocationInformation(IEnumerable<VendorLocation> locations)
		{
			var valid = true;

			foreach (var location in locations)
			{
				if (!string.IsNullOrEmpty(location.LocationNumber) && DuplicateVendorLocationNumber(location))
				{
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.Street1) && location.Street1.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor location Street1 cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.Street2) && location.Street2.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor location Street2 cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.City) && location.City.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor location City cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.State) && location.State.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor location State cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.PostalCode) && location.PostalCode.Length > 10)
				{
					Messages.Add(ValidationMessage.Error("Vendor location Postal Code cannot exceed 10 characters"));
					valid = false;
				}

				if (location.CountryId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor location requires a country association"));
					valid = false;
				}

				if (location.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor location requires a tenant association"));
					valid = false;
				}

				if (location.Vendor == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor location requires a vendor association"));
					valid = false;
				}

				if (location.Contacts.Count > 0)
				{
					if (!HasValidLocationContactInformation(location.Contacts)) valid = false;
					if (location.Contacts.Count(c => c.Primary) != 1)
					{
						Messages.Add(ValidationMessage.Error("Vendor location should have one (and only one) primary contact"));
						valid = false;
					}
				}
			}
			return valid;
		}

		private bool HasValidLocationContactInformation(IEnumerable<VendorContact> contacts)
		{
			var valid = true;

			foreach (var contact in contacts)
			{
				if (contact.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor contact requires a tenant association"));
					valid = false;
				}

				if (contact.VendorLocation == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor contact requires a vendor location association"));
					valid = false;
				}

				if (contact.ContactTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor contact requires a contact type association"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Name) && contact.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor contact name cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Phone) && contact.Phone.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor contact phone cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Mobile) && contact.Mobile.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor contact mobile cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Fax) && contact.Fax.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor contact fax cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Email) && contact.Email.Length > 100)
				{
					Messages.Add(ValidationMessage.Error("Vendor contact email cannot exceed 100 characters"));
					valid = false;
				}

			    if (!string.IsNullOrEmpty(contact.Email) && !contact.Email.EmailIsValid())
			    {
			        Messages.Add(ValidationMessage.Error("Vendor contact email is invalid"));
			        valid = false;
			    }

                if (!string.IsNullOrEmpty(contact.Comment) && contact.Comment.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Vendor contact comment cannot exceed 200 characters"));
					valid = false;
				}
			}

			return valid;
		}

		private bool VendorEquipmentsAreValid(IEnumerable<VendorEquipment> equipments)
		{
			var valid = true;

			foreach (var equipment in equipments)
			{
				if (equipment.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor equipment requires a tenant association"));
					valid = false;
				}

				if (equipment.Vendor == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor equipment requires a vendor association"));
					valid = false;
				}

				if (equipment.EquipmentType == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor equipment requires a equipment type association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool VendorServicesAreValid(IEnumerable<VendorService> services)
		{
			var valid = true;

			foreach (var service in services)
			{
				if (service.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor service requires a tenant association"));
					valid = false;
				}

				if (service.Vendor == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor service requires a vendor association"));
					valid = false;
				}

				if (service.Service == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor service requires a service association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool VendorInsurancesAreValid(IEnumerable<VendorInsurance> insurances)
		{
			var valid = true;

			foreach (var insurance in insurances)
			{
				if (insurance.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor insurnace requires a tenant association"));
					valid = false;
				}

				if (insurance.Vendor == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance filed requires a vendor association"));
					valid = false;
				}

				if (insurance.InsuranceTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor insurnace requires an insurance type association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(insurance.CarrierName))
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance requires an insurance carrier name"));
					valid = false;
				}
				else if (insurance.CarrierName.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance carrier name cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(insurance.PolicyNumber))
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance requires an insurance policy number"));
					valid = false;
				}
				else if (insurance.PolicyNumber.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance policy number cannot exceed 50 characters"));
					valid = false;
				}

				if (!insurance.EffectiveDate.IsValidSystemDateTime())
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance must have a valid effective date between {0} and {1}",
														 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
					valid = false;
				}

				if (!insurance.ExpirationDate.IsValidSystemDateTime())
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance must have a valid expiration date between {0} and {1}",
														 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
					valid = false;
				}

				if(insurance.EffectiveDate > insurance.ExpirationDate)
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance effective date cannot be after expiration date"));
					valid = false;
				}

				if (string.IsNullOrEmpty(insurance.CertificateHolder))
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance requires an insurance certificate holder name"));
					valid = false;
				}
				else if (insurance.CertificateHolder.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance certificate holder name cannot exceed 50 characters"));
					valid = false;
				}

				if(insurance.CoverageAmount < 0)
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance coverage amount cannot be less than zero"));
					valid = false;
				}

				if(!string.IsNullOrEmpty(insurance.SpecialInstruction) && insurance.SpecialInstruction.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Vendor insurance special instructions cannot exceed 500 characters"));
					valid = false;
				}
			}

			return valid;
		}

		private bool VendorPackageCustomMappingValid(IEnumerable<VendorPackageCustomMapping> vendorPackageCustomMappings)
		{
			var valid = true;

			foreach (var vendorPackageCustomMapping in vendorPackageCustomMappings)
			{
				if (vendorPackageCustomMapping.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor package custom mapping requires a tenant association"));
					valid = false;
				}

				if (vendorPackageCustomMapping.Vendor == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor package custom mapping filed requires a vendor association"));
					valid = false;
				}

				if (vendorPackageCustomMapping.PackageTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor package custom mapping requires an package type association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(vendorPackageCustomMapping.VendorCode))
				{
					Messages.Add(ValidationMessage.Error("Vendor package custom mapping requires a vendor code"));
					valid = false;
				}
				else if (vendorPackageCustomMapping.VendorCode.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor package custom mapping vendor code cannot exceed 50 characters"));
					valid = false;
				}
			}

			var errs = vendorPackageCustomMappings
				.Where(i => i.PackageTypeId != default(long))
				.GroupBy(i => i.PackageTypeId)
				.Where(g => g.Count() > 1)
				.Select(i => ValidationMessage
					             .Error("Vendor package custom mapping has duplicate mapping for package type [{0}]",
					                    i.First().PackageType.TypeName))
				.ToList();

			if (errs.Any())
			{
				Messages.AddRange(errs);
				valid = false;
			}

			return valid;
		}

		private bool VendorDocumentsAreValid(IEnumerable<VendorDocument> documents)
		{
			var valid = true;

			foreach (var document in documents)
			{
				if (document.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor document requires a tenant association"));
					valid = false;
				}

				if (document.Vendor == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor document requires a vendor association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Name))
				{
					Messages.Add(ValidationMessage.Error("Vendor document requires a name"));
					valid = false;
				}
				else if (document.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor document name cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Description))
				{
					Messages.Add(ValidationMessage.Error("Vendor document requires a description"));
					valid = false;
				}
				else if (document.Description.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Vendor document description cannot exceed 500 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.LocationPath))
				{
					Messages.Add(ValidationMessage.Error("Vendor document requires a location path"));
					valid = false;
				}
				else if (document.LocationPath.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Vendor document location path cannot exceed 200 characters"));
					valid = false;
				}
			}
			return valid;
		}
	}
}

