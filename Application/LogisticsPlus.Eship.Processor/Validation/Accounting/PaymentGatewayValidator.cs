﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
    public class PaymentGatewayValidator : ValidatorBase
    {
        public bool CanDeleteCustomerPaymentGatewayPaymentProfile(string paymentProfileId, long tenantId)
        {
            const string query = "select dbo.CanDeletePaymentGatewayPaymentProfile(@PaymentProfileId, @TenantId)";
            var parameters = new Dictionary<string, object> { { "PaymentProfileId", paymentProfileId }, { "TenantId", tenantId } };
            var canDelete = ExecuteScalar(query, parameters).ToBoolean();
            if (!canDelete) Messages.Add(ValidationMessage.Error("Shipments paid for using this payment profile have not all been invoiced and settled. Cannot delete payment profile."));
            return canDelete;
        }

        public bool CanRemoveCustomerProfileFromPaymentGateway(Customer customer)
        {
            if (customer.IsNew || string.IsNullOrEmpty(customer.PaymentGatewayKey)) return true;

            var profiles = customer.Tenant.GetPaymentGatewayService().GetPaymentProfiles(customer.PaymentGatewayKey);

            const string query = "select dbo.CanDeletePaymentGatewayPaymentProfile(@PaymentProfileId, @TenantId)";
            foreach (var profile in profiles)
            {
                var parameters = new Dictionary<string, object> { { "PaymentProfileId", profile.PaymentProfileId }, { "TenantId", customer.TenantId } };
                if (ExecuteScalar(query, parameters).ToBoolean()) continue;
                
                Messages.Add(ValidationMessage.Error("Shipments paid for using one of this customer's payment profiles have not all been invoiced and settled. Cannot remove customer from payment gateway."));
                return false;
            }

            return true;
        }
    }
}
