﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
	public class InvoiceValidator : ValidatorBase
	{
		public bool CanDeleteInvoice(Invoice invoice)
		{
			const string query = "select COUNT(*) from Invoice where OriginalInvoiceId = @OriginalInvoiceId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "OriginalInvoiceId", invoice.Id }, { "TenantId", invoice.TenantId } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Invoice is referenced one or more times and cannot be deleted."));
			return !used;
		}

		public bool IsValid(Invoice invoice)
		{
			Messages.Clear();

			var valid = true;

			if (!HasAllRequiredData(invoice)) valid = false;
			if (DuplicateInvoiceNumber(invoice)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(Invoice invoice)
		{
			var valid = true;
		   

			if (invoice.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Invoice requires a tenant association"));
				valid = false;
			}

			if (invoice.User == null)
			{
				Messages.Add(ValidationMessage.Error("Invoice requires a user association"));
				valid = false;
			}

			if (invoice.CustomerLocation == null)
			{
				Messages.Add(ValidationMessage.Error("Invoice requires a customer location association"));
				valid = false;
			}
         

			if (string.IsNullOrEmpty(invoice.InvoiceNumber))
			{
				Messages.Add(ValidationMessage.Error("Invoice requires an invoice number"));
				valid = false;
			}
			else if (invoice.InvoiceNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Invoice number cannot exceed 50 characters"));
				valid = false;
			}
			else if (invoice.NumberIsEqualToSuffix())
			{
				Messages.Add(ValidationMessage.Error("Invoice number is invalid. Possible invoice number generation error"));
				valid = false;
			}

			if (!invoice.InvoiceType.ToInt().EnumValueIsValid<InvoiceType>())
			{
				Messages.Add(ValidationMessage.Error("Invoice must have a valid type"));
				valid = false;
			}
			else if ((invoice.InvoiceType == InvoiceType.Supplemental || invoice.InvoiceType == InvoiceType.Credit))
			{
				if (invoice.OriginalInvoiceId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Invoice of type '{0}' must have original invoice reference",
					                                     invoice.InvoiceType.FormattedString()));
					valid = false;
				}
				else if (!invoice.OriginalInvoice.Posted)
				{
					Messages.Add(ValidationMessage.Error("Invoice of type '{0}' must have posted original invoice",
														 invoice.InvoiceType.FormattedString()));
					valid = false;
				}
                else if (invoice.OriginalInvoice.InvoiceType != InvoiceType.Invoice)
                {
                    Messages.Add(ValidationMessage.Error("Invoice of type '{0}' must have original invoice be of type 'Invoice'",
                                                         invoice.InvoiceType.FormattedString()));
                    valid = false;
                }
			}

			if (!invoice.InvoiceDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Invoice date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!invoice.DueDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Invoice due date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!invoice.PostDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Invoice post date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!invoice.ExportDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Invoice exported date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (invoice.PaidAmount < 0)
			{
				Messages.Add(ValidationMessage.Error("Invoice paid amount must be greater than or equal to zero"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(invoice.SpecialInstruction) && invoice.SpecialInstruction.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Invoice special instructions cannot exceed 500 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(invoice.CustomerControlAccountNumber) && invoice.CustomerControlAccountNumber.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Invoice customer control account number cannot exceed 100 characters"));
				valid = false;
			}

			if (!invoice.Details.Any())
			{
				Messages.Add(ValidationMessage.Error("Invoice must have at least one associated detail"));
				valid = false;
            }
            else if (invoice.Details.Sum(d => d.AmountDue) < 0)
            {
                Messages.Add(ValidationMessage.Error("Invoice total amount must be 0 or greater"));
                valid = false;
            }

			if (invoice.Details.Count > 0 && !InvoiceDetailsAreValid(invoice)) valid = false;
          
			return valid;
		}

		public bool DuplicateInvoiceNumber(Invoice invoice)
		{
			const string query = "Select count(*) from Invoice where InvoiceNumber = @InvoiceNumber and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"InvoiceNumber", invoice.InvoiceNumber},
			                 		{"TenantId", invoice.TenantId},
			                 		{"Id", invoice.Id}
			                 	};
			var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateNumber) Messages.Add(ValidationMessage.Error("Invoice number is already in use"));
			return duplicateNumber;
		}

		public bool MeetsInvoicingRequirement(Invoice invoice)
		{
			return MeetsInvoicingRequirement(invoice, new List<Shipment>());
		}

		public bool MeetsInvoicingRequirement(Invoice invoice, Shipment shipment)
		{
			return MeetsInvoicingRequirement(invoice, new List<Shipment> {shipment});
		}

		public bool MeetsInvoicingRequirement(Invoice invoice, List<Shipment> shipments)
		{
			Messages.Clear();
			var valid = true;

			var customer = invoice.CustomerLocation == null ? null : invoice.CustomerLocation.Customer;
			if (customer == null)
			{
				Messages.Add(ValidationMessage.Error("Invoice requires a customer location association. Cannot test requirement without a customer"));
				valid = false;
			}
			else
			{
				if (customer.InvoiceRequiresControlAccount && invoice.Posted && string.IsNullOrEmpty(invoice.CustomerControlAccountNumber))
				{
					Messages.Add(ValidationMessage.Error("Posted invoice requires a customer control account per customer setup"));
					valid = false;
				}

				foreach(var shipment in shipments)
				{
					if(customer.InvoiceRequiresShipperReference && string.IsNullOrEmpty(shipment.ShipperReference))
					{
						Messages.Add(ValidationMessage.Error("Invoice requires a shipper reference number per customer setup on shipment {0}",
							                        shipment.ShipmentNumber));
						valid = false;
					}

                    if (shipment.ActualDeliveryDate == DateUtility.SystemEarliestDateTime && !shipment.Customer.CanInvoiceNonDeliveredShipment)
                    {
                        Messages.Add(ValidationMessage.Error("Invoice requires Actual Delivery Date on shipment {0}",
                                                              shipment.ShipmentNumber));
                                                   
                        valid = false;
                    }

					if(customer.InvoiceRequiresCarrierProNumber)
					{
						var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
						if(sv == null)
						{
							Messages.Add(ValidationMessage.Error(
								"Invoice requires carrier pro number per customer setup. Cannot test requirement without primary vendor on shipment {0}",
								shipment.ShipmentNumber));
							valid = false;
						}
						else if (string.IsNullOrEmpty(sv.ProNumber))
						{
							Messages.Add(ValidationMessage.Error("Invoice requires carrier pro number per customer setup on shipment {0}",
							                                     shipment.ShipmentNumber));
							valid = false;
						}
					}
				}

			}
			return valid;
		}

		private bool InvoiceDetailsAreValid(Invoice invoice)
		{
			var valid = true;

			foreach (var detail in invoice.Details)
			{
				if (detail.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Invoice detail requires a tenant association"));
					valid = false;
				}

				if (detail.Invoice == null)
				{
					Messages.Add(ValidationMessage.Error("Invoice detail requires an invoice association"));
					valid = false;
				}

				if (detail.AccountBucket == null)
				{
					Messages.Add(ValidationMessage.Error("Invoice detail requires an account bucket association"));
					valid = false;
				}

				if (detail.ChargeCodeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Invoice detail requires a charge code association"));
					valid = false;
				}

				if (detail.Quantity <= 0)
				{
					Messages.Add(ValidationMessage.Error("Invoice detail quantity must be greater than zero"));
					valid = false;
				}

				if (detail.UnitDiscount < 0)
				{
					Messages.Add(ValidationMessage.Error("Invoice detail unit discount must be greater than or equal to zero"));
					valid = false;
				}

				if (!detail.ReferenceType.ToInt().EnumValueIsValid<DetailReferenceType>())
				{
					Messages.Add(ValidationMessage.Error("Invoice detail must have a valid detail reference type"));
					valid = false;
				}
				else
				{
					if (detail.ReferenceType != DetailReferenceType.Miscellaneous && string.IsNullOrEmpty(detail.ReferenceNumber))
					{
						Messages.Add(
							ValidationMessage.Error("Invoice detail must have a valid detail reference number to match reference type"));
						valid = false;
					}
				}

				if (!string.IsNullOrEmpty(detail.ReferenceNumber) && detail.ReferenceNumber.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Invoice detail reference number cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(detail.Comment) && detail.Comment.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Invoice detail comment cannot exceed 50 characters"));
					valid = false;
				}
			}
			return valid;
		}
	}
}
