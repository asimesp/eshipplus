﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
	public class VendorBillValidator : ValidatorBase
	{
		public bool CanDeleteVendorBill(VendorBill bill)
		{
			const string query = "select dbo.VendorBillIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", bill.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Vendor Bill is referenced one or more times and cannot be deleted."));
			return !used;
		}

        public bool IsValid(VendorBill bill)
		{
			Messages.Clear();

	        var valid = HasAllRequiredData(bill);

	        if (DuplicateVendorBillNumber(bill)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(VendorBill bill)
		{
			var valid = true;

			if (bill.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill require a tenant association"));
				valid = false;
			}

			if (bill.User == null)
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill requires a user association"));
				valid = false;
			}

			if (bill.VendorLocation == null)
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill requires a vendor location association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(bill.DocumentNumber))
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill requires a document number"));
				valid = false;
			}
			else if (bill.DocumentNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill document number cannot exceed 50 characters"));
				valid = false;
			}

			if (!bill.BillType.ToInt().EnumValueIsValid<InvoiceType>())
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill must have a valid type"));
				valid = false;
			}
			else
			{
				if(bill.BillType == InvoiceType.Supplemental)
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill of type 'supplemental' is invalid for vendor bill"));
					valid = false;
				}

				if (bill.BillType == InvoiceType.Credit && bill.ApplyToDocumentId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill of type 'credit' must have a valid original invoice reference"));
					valid = false;
				}
			}

			if (!bill.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill date created must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

		    if (bill.DocumentDate <= DateUtility.SystemEarliestDateTime)
		    {
                Messages.Add(ValidationMessage.Error("Vendor bill requires a document date"));
		        valid = false;
		    }

			if (!bill.DocumentDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill document date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!bill.PostDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill post date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!bill.ExportDate.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill exported date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!bill.Details.Any())
			{
				Messages.Add(ValidationMessage.Error("Vendor Bill must have at least 1 associated detail"));
				valid = false;
			}

			if (bill.Details.Count > 0 && !VendorBillDetailsAreValid(bill)) valid = false;

			return valid;
		}

		public bool DuplicateVendorBillNumber(VendorBill bill)
		{
			if (bill.VendorLocation == null || bill.VendorLocation.Vendor == null)
			{
				Messages.Add(ValidationMessage.Error("Assumed vendor bill number is already in use as unable to test due to missing vendor link"));
				return true;
			}

			const string query = "Select count(*) from VendorBill b, VendorLocation l where b.VendorLocationId = l.Id and  b.DocumentNumber = @DocumentNumber and l.VendorId = @VendorId and b.TenantId = @TenantId and b.Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"DocumentNumber", bill.DocumentNumber},
			                 		{"TenantId", bill.TenantId},
									{"VendorId", bill.VendorLocation.Vendor.Id},
			                 		{"Id", bill.Id}
			                 	};
			var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateNumber) Messages.Add(ValidationMessage.Error("Vendor Bill number is already in use"));
			return duplicateNumber;
		}

        public bool VendorBillDetailAreValid(VendorBillDetail detail)
        {
            var vendorBill = new VendorBill() { Details = new List<VendorBillDetail>() { detail } };

            return VendorBillDetailsAreValid(vendorBill);
        }


        private bool VendorBillDetailsAreValid(VendorBill bill)
		{
			var valid = true;

			foreach (var detail in bill.Details)
			{
				if (detail.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill detail requires a tenant association"));
					valid = false;
				}

				if (detail.VendorBill == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill detail requires an invoice association"));
					valid = false;
				}

				if (detail.AccountBucket == null)
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill detail requires an account bucket association"));
					valid = false;
				}

				if (detail.ChargeCodeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill detail requires a charge code association"));
					valid = false;
				}

				if (detail.Quantity <= 0)
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill detail quantity must be greater than zero"));
					valid = false;
				}

				if (detail.UnitBuy < 0)
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill detail unit buy must be greater than or equal to zero"));
					valid = false;
				}

				if (!detail.ReferenceType.ToInt().EnumValueIsValid<DetailReferenceType>())
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill detail must have a valid detail reference type"));
					valid = false;
				}
				else
				{
					if (detail.ReferenceType != DetailReferenceType.Miscellaneous && string.IsNullOrEmpty(detail.ReferenceNumber))
					{
						Messages.Add(
							ValidationMessage.Error("Vendor Bill detail must have a valid detail reference number to match reference type"));
						valid = false;
					}
				}

				if (!string.IsNullOrEmpty(detail.ReferenceNumber) && detail.ReferenceNumber.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill detail reference number cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(detail.Note) && detail.Note.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Vendor Bill detail note cannot exceed 50 characters"));
					valid = false;
				}
			}


			return valid;
		}
	}
}
