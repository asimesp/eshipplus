﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
	public class CustomerValidator : ValidatorBase
	{
		public bool CanDeleteCustomer(Customer customer)
		{
			const string query = "select dbo.CustomerIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", customer.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Customer is referenced one or more times and cannot be deleted."));
			return !used;
		}

		public bool CanDeleteCustomerLocation(CustomerLocation location)
		{
			const string query = "select dbo.CustomerLocationIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> {{"Id", location.Id}};
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used)
				Messages.Add(ValidationMessage.Error("Customer location [#{0}] is referenced one or more times and cannot be deleted.",
					                        location.LocationNumber));
			return !used;
		}

		public bool CustomerServiceRepresentativeExists(CustomerServiceRepresentative customerServiceRepresentative)
		{
			const string query =
				"Select count(*) from CustomerServiceRepresentative where UserId = @UserId and CustomerId = @CustomerId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"UserId", customerServiceRepresentative.UserId},
			                 		{"TenantId", customerServiceRepresentative.TenantId},
			                 		{"CustomerId", customerServiceRepresentative.CustomerId}
			                 	};
			var duplicateCsr = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateCsr)
				Messages.Add(ValidationMessage.Error("User is already assigned as a customer service representative to this customer"));
			return duplicateCsr;
		}

		public bool CustomerRequiredInvoiceDocumentTagExist(RequiredInvoiceDocumentTag requiredInvoiceDocumentTag)
		{
			const string query =
				"Select count(*) from RequiredInvoiceDocumentTag where DocumentTagId = @DocumentTagId and CustomerId = @CustomerId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"DocumentTagId", requiredInvoiceDocumentTag.DocumentTagId},
			                 		{"TenantId", requiredInvoiceDocumentTag.TenantId},
			                 		{"CustomerId", requiredInvoiceDocumentTag.CustomerId}
			                 	};
			var duplicateTag = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateTag)
				Messages.Add(ValidationMessage.Error("Document tag is already required for invoicing on this customer"));
			return duplicateTag;
		}

		public bool IsValid(Customer customer)
		{
			var valid = HasAllRequiredData(customer);

			if (DuplicateCustomerNumber(customer)) valid = false;

			return valid;
		}

		public bool DuplicateCustomerNumber(Customer customer)
		{
			const string query = "Select count(*) from Customer where CustomerNumber = @CustomerNumber and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"CustomerNumber", customer.CustomerNumber},
			                 		{"TenantId", customer.TenantId},
			                 		{"Id", customer.Id}
			                 	};
			var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateNumber) Messages.Add(ValidationMessage.Error("Customer number is already in use"));
			return duplicateNumber;
		}

		public bool DuplicateCustomerLocationNumber(CustomerLocation location)
		{
			const string query = "Select count(*) from CustomerLocation where LocationNumber = @LocationNumber and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"LocationNumber", location.LocationNumber},
			                 		{"TenantId", location.TenantId},
			                 		{"Id", location.Id}
			                 	};
			var duplicateNumber = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateNumber) Messages.Add(ValidationMessage.Error("Customer Location number [{0}] is already in use", location.LocationNumber));
			return duplicateNumber;
		}

		public bool HasAllRequiredData(Customer customer)
		{
			var valid = true;

			if (string.IsNullOrEmpty(customer.Name))
			{
				Messages.Add(ValidationMessage.Error("Customer requires a name"));
				valid = false;
			}
			else if (customer.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer name cannot exceed 50 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(customer.CustomerNumber))
			{
				Messages.Add(ValidationMessage.Error("Customer requires a customer number"));
				valid = false;
			}
			else if (customer.CustomerNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer number cannot exceed 50 characters"));
				valid = false;
			}

			if(!string.IsNullOrEmpty(customer.Notes) && customer.Notes.Length > 1000)
			{
				Messages.Add(ValidationMessage.Error("Customer quick reference notes cannot exceed 1000 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customer.CustomField1) && customer.CustomField1.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer custom field 1 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customer.CustomField2) && customer.CustomField2.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer custom field 2 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customer.CustomField3) && customer.CustomField3.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer custom field 3 cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customer.CustomField4) && customer.CustomField4.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer custom field 4 cannot exceed 50 characters"));
				valid = false;
			}

            if (!string.IsNullOrEmpty(customer.PaymentGatewayKey) && customer.PaymentGatewayKey.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Customer payment gateway key cannot exceed 100 characters"));
                valid = false;
            }

			if(customer.Tier == null)
			{
				Messages.Add(ValidationMessage.Error("Customer requires a tier association"));
				valid = false;
			}

			if (customer.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Customer requires a tenant association"));
				valid = false;
			}

			if (customer.DefaultAccountBucketId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Customer requires a default account bucket association"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customer.AdditionalBillOfLadingText) && customer.AdditionalBillOfLadingText.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Additional bill of lading text cannot exceed 500 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customer.InvalidPurchaseOrderNumberMessage) && customer.InvalidPurchaseOrderNumberMessage.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Invalid purchase order number message cannot exceed 200 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customer.CustomVendorSelectionMessage) && customer.CustomVendorSelectionMessage.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("Custom vendor selection message cannot exceed 500 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customer.LogoUrl) && customer.LogoUrl.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Logo url path length cannot exceed 200 characters"));
				valid = false;
			}

			if(customer.InvoiceTerms < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer invoice terms must be equal to or greater than zero"));
				valid = false;
			}

			if (customer.ShipperBillOfLadingSeed < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer shipper bill of lading seed must be equal to or greater than zero"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customer.ShipperBillOfLadingPrefix) && customer.ShipperBillOfLadingPrefix.Length > 10)
			{
				Messages.Add(ValidationMessage.Error("Shipper bill of lading prefix cannot exceed 10 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customer.ShipperBillOfLadingSuffix) && customer.ShipperBillOfLadingSuffix.Length > 10)
			{
				Messages.Add(ValidationMessage.Error("Shipper bill of lading suffix cannot exceed 10 characters"));
				valid = false;
			}

			if (!((int)customer.CustomerType).EnumValueIsValid<CustomerType>())
			{
				Messages.Add(ValidationMessage.Error("Customer must have a valid customer type"));
				valid = false;
			}

			if (!customer.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Customer must have a valid creation date between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

            if (!string.IsNullOrEmpty(customer.AuditInstructions) && customer.AuditInstructions.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Customer audit instructions cannot exceed 100 characters"));
                valid = false;
            }

			if (customer.TruckloadDeliveryTolerance < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer Truckload Delivery Tolerance cannot be less than zero"));
				valid = false;
			}

			if (customer.TruckloadPickupTolerance < 0)
			{
				Messages.Add(ValidationMessage.Error("Customer Truckload Pickup Tolerance cannot be less than zero"));
				valid = false;
			}

			if (customer.Locations.Count == 0)
			{
				Messages.Add(ValidationMessage.Error("Customer must have at least one location"));
				valid = false;
			}
			else
			{
				if(!HasValidLocationInformation(customer.Locations)) valid = false;
				if(customer.Locations.Count(l => l.Primary) != 1)
				{
					Messages.Add(ValidationMessage.Error("Customer must have one, and only one, primary location"));
					valid = false;
				}
                if(customer.Locations.Any(l => l.BillToLocation) && customer.Locations.Count(l => l.BillToLocation && l.MainBillToLocation) != 1 )
                {
                    Messages.Add(ValidationMessage.Error("If a customer has a bill to location, one and only one bill to location must be the main bill to location"));
                    valid = false;
                }
			}

            if (customer.CanPayByCreditCard && string.IsNullOrEmpty(customer.PaymentGatewayKey))
            {
                Messages.Add(ValidationMessage.Error("Customer must have a payment gateway key if they can pay by credit card"));
                valid = false;
            }


			if(customer.Documents.Count > 0 && !CustomerDocumentsAreValid(customer.Documents)) valid = false;
			if(customer.CustomFields.Count > 0 && !CustomFieldsAreValid(customer.CustomFields)) valid = false;
			if(customer.CustomChargeCodeMap.Count > 0 && !CustomChargeCodemapsAreValid(customer.CustomChargeCodeMap)) valid = false;
			if(customer.RequiredInvoiceDocumentTags.Count > 0 && !RequiredDocumentTagsAreValid(customer.RequiredInvoiceDocumentTags))
				valid = false;
			if(customer.ServiceRepresentatives.Count > 0 && !ServiceRepresentativesAreValid(customer.ServiceRepresentatives))
				valid = false;
			
			return valid;
		}

		private bool HasValidLocationInformation(IEnumerable<CustomerLocation> locations)
		{
			var valid = true;

			foreach (var location in locations)
			{
				if (!string.IsNullOrEmpty(location.LocationNumber) && DuplicateCustomerLocationNumber(location))
				{
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.Street1) && location.Street1.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer location Street1 cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.Street2) && location.Street2.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer location Street2 cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.City) && location.City.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer location City cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.State) && location.State.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer location State cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.PostalCode) && location.PostalCode.Length > 10)
				{
					Messages.Add(ValidationMessage.Error("Customer location Postal Code cannot exceed 10 characters"));
					valid = false;
				}

				if (location.CountryId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer location requires a country association"));
					valid = false;
				}

				if (location.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer location requires a tenant association"));
					valid = false;
				}

				if (location.Customer == null)
				{
					Messages.Add(ValidationMessage.Error("Customer location requires a customer association"));
					valid = false;
				}

				if (location.Contacts.Count > 0)
				{
					if (!HasValidLocationContactInformation(location.Contacts)) valid = false;
					if (location.Contacts.Count(c => c.Primary) != 1)
					{
						Messages.Add(ValidationMessage.Error("Customer location should have one (and only one) primary contact"));
						valid = false;
					}
				}
			}
			return valid;
		}

		private bool HasValidLocationContactInformation(IEnumerable<CustomerContact> contacts)
		{
			var valid = true;

			foreach (var contact in contacts)
			{
				if (contact.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer contact requires a tenant association"));
					valid = false;
				}

				if (contact.CustomerLocation == null)
				{
					Messages.Add(ValidationMessage.Error("Customer contact requires a customer location association"));
					valid = false;
				}

				if (contact.ContactTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer contact requires a contact type association"));
					valid = false;
				}

				if(string.IsNullOrEmpty(contact.Name))
				{
					Messages.Add(ValidationMessage.Error("Customer contact name is required"));
					valid = false;
				}
				else if (contact.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer contact name cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Phone) && contact.Phone.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer contact phone cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Mobile) && contact.Mobile.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer contact mobile cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Fax) && contact.Fax.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer contact fax cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Email) && contact.Email.Length > 100)
				{
					Messages.Add(ValidationMessage.Error("Customer contact email cannot exceed 100 characters"));
					valid = false;
				}

			    if (!string.IsNullOrEmpty(contact.Email) && !contact.Email.EmailIsValid())
			    {
			        Messages.Add(ValidationMessage.Error("Customer contact email is invalid"));
			        valid = false;
			    }

                if (!string.IsNullOrEmpty(contact.Comment) && contact.Comment.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Customer contact comment cannot exceed 200 characters"));
					valid = false;
				}
			}

			return valid;
		}

		private bool ServiceRepresentativesAreValid(IEnumerable<CustomerServiceRepresentative> serviceRepresentatives)
		{
			var valid = true;

			foreach (var representative in serviceRepresentatives)
			{
				if (representative.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer service requires a tenant association"));
					valid = false;
				}

				if (representative.Customer == null)
				{
					Messages.Add(ValidationMessage.Error("Customer service requires a customer association"));
					valid = false;
				}

				if (representative.User == null)
				{
					Messages.Add(ValidationMessage.Error("Customer service requires a user association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool RequiredDocumentTagsAreValid(IEnumerable<RequiredInvoiceDocumentTag> requiredInvoiceDocumentTags)
		{
			var valid = true;

			foreach (var tag in requiredInvoiceDocumentTags)
			{
				if (tag.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer requried invoice document tag requires a tenant association"));
					valid = false;
				}

				if (tag.Customer == null)
				{
					Messages.Add(ValidationMessage.Error("Customer requried invoice document tag requires a customer association"));
					valid = false;
				}

				if (tag.DocumentTag == null)
				{
					Messages.Add(ValidationMessage.Error("Customer requried invoice document tag requires a document tag association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool CustomerDocumentsAreValid(IEnumerable<CustomerDocument> documents)
		{
			var valid = true;

			foreach (var document in documents)
			{
				if (document.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer document requires a tenant association"));
					valid = false;
				}

				if (document.Customer == null)
				{
					Messages.Add(ValidationMessage.Error("Customer document requires a customer association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Name))
				{
					Messages.Add(ValidationMessage.Error("Customer document requires a name"));
					valid = false;
				}
				else if (document.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer document name cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Description))
				{
					Messages.Add(ValidationMessage.Error("Customer document requires a description"));
					valid = false;
				}
				else if (document.Description.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Customer document description cannot exceed 500 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.LocationPath))
				{
					Messages.Add(ValidationMessage.Error("Customer document requires a location path"));
					valid = false;
				}
				else if (document.LocationPath.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Customer document location path cannot exceed 200 characters"));
					valid = false;
				}
			}
			return valid;
		}


		private bool CustomFieldsAreValid(IEnumerable<CustomField> customFields)
		{
			var valid = true;

			foreach(var field in customFields)
			{
				if(field.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer custom field requires a tenant association"));
					valid = false;
				}

				if(field.Customer == null)
				{
					Messages.Add(ValidationMessage.Error("Customer custom field requires a customer association"));
					valid = false;
				}

				if(string.IsNullOrEmpty(field.Name))
				{
					Messages.Add(ValidationMessage.Error("Customer custom field requires a field name"));
					valid = false;
				}
				else if (field.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer custom field name cannot exceed 50 characters"));
					valid = false;
				}
			}

			return valid;
		}


		private bool CustomChargeCodemapsAreValid(IEnumerable<CustomerChargeCodeMap> customChargeCodeMap)
		{
			var valid = true;
			var charegCodeIds = customChargeCodeMap.Select(c => c.ChargeCodeId).ToList();

			foreach (var field in customChargeCodeMap)
			{
				if (field.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Customer charge code map requires a tenant association"));
					valid = false;
				}

				if (field.Customer == null)
				{
					Messages.Add(ValidationMessage.Error("Customer charge code map requires a customer association"));
					valid = false;
				}

				if (field.ChargeCode == null)
				{
					Messages.Add(ValidationMessage.Error("Customer charge code map requires a customer association"));
					valid = false;
				}
				else if (charegCodeIds.Count(c => c == field.ChargeCodeId) > 1)
				{
					Messages.Add(ValidationMessage.Error("Charge code in Customer charge code map should be unique."));
					valid = false;
				}

				if (string.IsNullOrEmpty(field.ExternalCode))
				{
					Messages.Add(ValidationMessage.Error("Customer charge code map requires a External Code"));
					valid = false;
				}
				else if (field.ExternalCode.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Customer charge code map, External code cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(field.ExternalDescription))
				{
					Messages.Add(ValidationMessage.Error("Customer charge code map requires a External Description."));
					valid = false;
				}
				else if (field.ExternalDescription.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Customer charge code map, External Description cannot exceed 200 characters"));
					valid = false;
				}
			}

			return valid;
		}
	}
}
