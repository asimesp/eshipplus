﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
	public class PendingVendorValidator : ValidatorBase
	{
		public bool PendingVendorServiceExists(PendingVendorService pendingVendorService)
		{
			const string query =
				"Select count(*) from PendingVendorService where ServiceId = @ServiceId and PendingVendorId = @PendingVendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"ServiceId", pendingVendorService.ServiceId},
			                 		{"TenantId", pendingVendorService.TenantId},
			                 		{"PendingVendorId", pendingVendorService.PendingVendorId}
			                 	};
			var duplicateService = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateService)
				Messages.Add(ValidationMessage.Error("Service is already included in list of pending vendor services"));
			return duplicateService;
		}

		public bool PendingVendorEquipmentExists(PendingVendorEquipment pendingVendorEquipment)
		{
			const string query =
				"Select count(*) from PendingVendorEquipment where EquipmentTypeId = @EquipmentTypeId and PendingVendorId = @PendingVendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"EquipmentTypeId", pendingVendorEquipment.EquipmentTypeId},
			                 		{"TenantId", pendingVendorEquipment.TenantId},
			                 		{"PendingVendorId", pendingVendorEquipment.PendingVendorId}
			                 	};
			var duplicateCsr = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateCsr)
				Messages.Add(ValidationMessage.Error("Equipment is already included in list of pending vendor equipments"));
			return duplicateCsr;
		}

		public bool IsValid(PendingVendor pendingVendor)
		{
			var valid = HasAllRequiredData(pendingVendor);

			if (DuplicatePendingVendorNumber(pendingVendor)) valid = false;
            if (DuplicatePendingVendorMc(pendingVendor)) valid = false;
            if (DuplicatePendingVendorDot(pendingVendor)) valid = false;

			return valid;
		}

		public bool DuplicatePendingVendorNumber(PendingVendor pendingVendor)
		{
			const string query = @"Select count(*) from PendingVendor where VendorNumber = @VendorNumber and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"VendorNumber", pendingVendor.VendorNumber},
			                 		{"TenantId", pendingVendor.TenantId},
			                 		{"Id", pendingVendor.Id}
			                 	};
			var duplicateGroupName = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateGroupName) Messages.Add(ValidationMessage.Error("Pending vendor number is already in use"));
			return duplicateGroupName;
		}

        public bool DuplicatePendingVendorMc(PendingVendor pendingVendor)
        {
            if (string.IsNullOrEmpty(pendingVendor.MC)) return false;
            const string query = @"Select count(*) from PendingVendor where MC = @MC and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"MC", pendingVendor.MC},
			                 		{"TenantId", pendingVendor.TenantId},
			                 		{"Id", pendingVendor.Id}
			                 	};
            var duplicateMc = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateMc) Messages.Add(ValidationMessage.Error("Pending vendor MC is already in use"));
            return duplicateMc;
        }

        public bool DuplicatePendingVendorDot(PendingVendor pendingVendor)
        {
            if (string.IsNullOrEmpty(pendingVendor.DOT)) return false;
            const string query = @"Select count(*) from PendingVendor where DOT = @DOT and TenantId = @TenantId and Id <> @Id";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"DOT", pendingVendor.DOT},
			                 		{"TenantId", pendingVendor.TenantId},
			                 		{"Id", pendingVendor.Id}
			                 	};
            var duplicateDot = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateDot) Messages.Add(ValidationMessage.Error("Pending vendor DOT is already in use"));
            return duplicateDot;
        }

		public bool HasAllRequiredData(PendingVendor pendingVendor)
		{
			var valid = true;

			if (string.IsNullOrEmpty(pendingVendor.Name))
			{
				Messages.Add(ValidationMessage.Error("Pending vendor requires a name"));
				valid = false;
			}
			else if (pendingVendor.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor name cannot exceed 50 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(pendingVendor.VendorNumber))
			{
				Messages.Add(ValidationMessage.Error("Pending vendor requires a PendingVendor number"));
				valid = false;
			}
			else if (pendingVendor.VendorNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(pendingVendor.Notation) && pendingVendor.Notation.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor notation (special notes) cannot exceed 100 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(pendingVendor.Notes) && pendingVendor.Notes.Length > 1000)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor quick reference notes cannot exceed 1000 characters"));
				valid = false;
			}

			if (pendingVendor.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Pending vendor requires a tenant association"));
				valid = false;
			}

            if (pendingVendor.CreatedByUserId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Pending vendor requires a created by user association"));
                valid = false;
            }

			if (!pendingVendor.DateCreated.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Pending vendor must have a valid creation date between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (pendingVendor.HandlesLessThanTruckload)
			{
				if (string.IsNullOrEmpty(pendingVendor.Scac))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor that handles LTL shipment must have a PendingVendor SCAC code"));
					valid = false;
				}
				else if (pendingVendor.Scac.Length > 4)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor SCAC code cannot exceed 4 characters"));
					valid = false;
				}
			}

			if (pendingVendor.Approved && pendingVendor.Rejected)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor cannot be approved and rejected at the same time"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(pendingVendor.CTPATNumber) && pendingVendor.CTPATNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor CTPAT number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(pendingVendor.PIPNumber) && pendingVendor.PIPNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor PIP number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(pendingVendor.FederalIDNumber) && pendingVendor.FederalIDNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor federal ID number cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(pendingVendor.BrokerReferenceNumber) && pendingVendor.BrokerReferenceNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor broker reference number cannot exceed 50 characters"));
				valid = false;
			}

			if (!((int)pendingVendor.BrokerReferenceNumberType).EnumValueIsValid<BrokerReferenceNumberType>())
			{
				Messages.Add(ValidationMessage.Error("Pending vendor broker reference number type is invalid"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(pendingVendor.MC) && pendingVendor.MC.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor MC cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(pendingVendor.DOT) && pendingVendor.DOT.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor DOT cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(pendingVendor.TrackingUrl) && pendingVendor.TrackingUrl.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor tracking URL cannot exceed 200 characters"));
				valid = false;
			}

			if (pendingVendor.Locations.Count == 0)
			{
				Messages.Add(ValidationMessage.Error("Pending vendor must have at least one location"));
				valid = false;
			}
			else
			{
				if (!HasValidLocationInformation(pendingVendor.Locations)) valid = false;
				if (pendingVendor.Locations.Count(l => l.Primary) != 1)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor must have at least one, and only one, primary location"));
					valid = false;
				}
				if (pendingVendor.Locations.Any(l => l.RemitToLocation) && pendingVendor.Locations.Count(l => l.RemitToLocation && l.MainRemitToLocation) != 1)
				{
					Messages.Add(ValidationMessage.Error("If a Pending vendor has a remit to location, one and only one remit to location must be the main remit to location"));
					valid = false;
				}
			}

			if (pendingVendor.Insurances.Count > 0 && !PendingVendorInsurancesAreValid(pendingVendor.Insurances)) valid = false;
			if (pendingVendor.Services.Count > 0 && !PendingVendorServicesAreValid(pendingVendor.Services)) valid = false;
			if (pendingVendor.Equipments.Count > 0 && !PendingVendorEquipmentsAreValid(pendingVendor.Equipments)) valid = false;
			if (pendingVendor.Documents.Count > 0 && !PendingVendorDocumentsAreValid(pendingVendor.Documents)) valid = false;

			return valid;
		}

		private bool HasValidLocationInformation(IEnumerable<PendingVendorLocation> locations)
		{
			var valid = true;

			foreach (var location in locations)
			{
				if (!string.IsNullOrEmpty(location.Street1) && location.Street1.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor location Street1 cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.Street2) && location.Street2.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor location Street2 cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.City) && location.City.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor location City cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.State) && location.State.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor location State cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(location.PostalCode) && location.PostalCode.Length > 10)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor location Postal Code cannot exceed 10 characters"));
					valid = false;
				}

				if (location.CountryId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor location requires a country association"));
					valid = false;
				}

				if (location.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor location requires a tenant association"));
					valid = false;
				}

				if (location.PendingVendor == null)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor location requires a PendingVendor association"));
					valid = false;
				}

				if (location.Contacts.Count > 0)
				{
					if (!HasValidLocationContactInformation(location.Contacts)) valid = false;
					if (location.Contacts.Count(c => c.Primary) != 1)
					{
						Messages.Add(ValidationMessage.Error("Pending vendor location should have one (and only one) primary contact"));
						valid = false;
					}
				}
			}
			return valid;
		}

		private bool HasValidLocationContactInformation(IEnumerable<PendingVendorContact> contacts)
		{
			var valid = true;

			foreach (var contact in contacts)
			{
				if (contact.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor contact requires a tenant association"));
					valid = false;
				}

				if (contact.PendingVendorLocation == null)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor contact requires a PendingVendor location association"));
					valid = false;
				}

				if (contact.ContactTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor contact requires a contact type association"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Name) && contact.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor contact name cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Phone) && contact.Phone.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor contact phone cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Mobile) && contact.Mobile.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor contact mobile cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Fax) && contact.Fax.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor contact fax cannot exceed 50 characters"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(contact.Email) && contact.Email.Length > 100)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor contact email cannot exceed 100 characters"));
					valid = false;
				}

			    if (!string.IsNullOrEmpty(contact.Email) && !contact.Email.EmailIsValid())
			    {
			        Messages.Add(ValidationMessage.Error("Pending Vendor contact email is invalid"));
			        valid = false;
			    }

                if (!string.IsNullOrEmpty(contact.Comment) && contact.Comment.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor contact comment cannot exceed 200 characters"));
					valid = false;
				}
			}

			return valid;
		}

		private bool PendingVendorEquipmentsAreValid(IEnumerable<PendingVendorEquipment> equipments)
		{
			var valid = true;

			foreach (var equipment in equipments)
			{
				if (equipment.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor equipment requires a tenant association"));
					valid = false;
				}

				if (equipment.PendingVendor == null)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor equipment requires a PendingVendor association"));
					valid = false;
				}

				if (equipment.EquipmentType == null)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor equipment requires a equipment type association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool PendingVendorServicesAreValid(IEnumerable<PendingVendorService> services)
		{
			var valid = true;

			foreach (var service in services)
			{
				if (service.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor service requires a tenant association"));
					valid = false;
				}

				if (service.PendingVendor == null)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor service requires a PendingVendor association"));
					valid = false;
				}

				if (service.Service == null)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor service requires a service association"));
					valid = false;
				}
			}

			return valid;
		}

		private bool PendingVendorInsurancesAreValid(IEnumerable<PendingVendorInsurance> insurances)
		{
			var valid = true;

			foreach (var insurance in insurances)
			{
				if (insurance.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurnace requires a tenant association"));
					valid = false;
				}

				if (insurance.PendingVendor == null)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance filed requires a PendingVendor association"));
					valid = false;
				}

				if (insurance.InsuranceTypeId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurnace requires an insurance type association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(insurance.CarrierName))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance requires an insurance carrier name"));
					valid = false;
				}
				else if (insurance.CarrierName.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance carrier name cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(insurance.PolicyNumber))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance requires an insurance policy number"));
					valid = false;
				}
				else if (insurance.PolicyNumber.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance policy number cannot exceed 50 characters"));
					valid = false;
				}

				if (!insurance.EffectiveDate.IsValidSystemDateTime())
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance must have a valid effective date between {0} and {1}",
														 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
					valid = false;
				}

				if (!insurance.ExpirationDate.IsValidSystemDateTime())
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance must have a valid expiration date between {0} and {1}",
														 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
					valid = false;
				}

				if (insurance.EffectiveDate > insurance.ExpirationDate)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance effective date cannot be after expiration date"));
					valid = false;
				}

				if (string.IsNullOrEmpty(insurance.CertificateHolder))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance requires an insurance certificate holder name"));
					valid = false;
				}
				else if (insurance.CertificateHolder.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance certificate holder name cannot exceed 50 characters"));
					valid = false;
				}

				if (insurance.CoverageAmount < 0)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance coverage amount cannot be less than zero"));
					valid = false;
				}

				if (!string.IsNullOrEmpty(insurance.SpecialInstruction) && insurance.SpecialInstruction.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor insurance special instructions cannot exceed 500 characters"));
					valid = false;
				}
			}

			return valid;
		}

		private bool PendingVendorDocumentsAreValid(IEnumerable<PendingVendorDocument> documents)
		{
			var valid = true;

			foreach (var document in documents)
			{
				if (document.TenantId == default(long))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor document requires a tenant association"));
					valid = false;
				}

				if (document.PendingVendor == null)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor document requires a PendingVendor association"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Name))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor document requires a name"));
					valid = false;
				}
				else if (document.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor document name cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.Description))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor document requires a description"));
					valid = false;
				}
				else if (document.Description.Length > 500)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor document description cannot exceed 500 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(document.LocationPath))
				{
					Messages.Add(ValidationMessage.Error("Pending vendor document requires a location path"));
					valid = false;
				}
				else if (document.LocationPath.Length > 200)
				{
					Messages.Add(ValidationMessage.Error("Pending vendor document location path cannot exceed 200 characters"));
					valid = false;
				}
			}
			return valid;
		}
	}
}
