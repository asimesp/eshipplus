﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
	public class SalesRepresentativeValidator : ValidatorBase
	{
		public bool IsValid(SalesRepresentative salesRepresentative)
		{
			Messages.Clear();

			bool valid = IsUnique(salesRepresentative);

			if (!HasAllRequiredData(salesRepresentative)) valid = false;

			return valid;
		}

		public bool IsUnique(SalesRepresentative salesRepresentative)
		{
			var query = salesRepresentative.IsNew
							? "Select count(*) from SalesRepresentative where SalesRepresentativeNumber = @SalesRepresentativeNumber and TenantId = @TenantId"
							: "Select count(*) from SalesRepresentative where SalesRepresentativeNumber = @SalesRepresentativeNumber and TenantId = @TenantId and Id <> @Id";
			var parameters = new Dictionary<string, object> { { "SalesRepresentativeNumber", salesRepresentative.SalesRepresentativeNumber }, { "TenantId", salesRepresentative.TenantId } };
			if (!salesRepresentative.IsNew) parameters.Add("Id", salesRepresentative.Id);
			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;
			if (!isUnique) Messages.Add(ValidationMessage.Error("Sales Representative is not unique"));
			return isUnique;
		}

		public bool HasAllRequiredData(SalesRepresentative salesRepresentative)
		{
			var valid = true;

			if (salesRepresentative.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Sales Representative requires a tenant association"));
				valid = false;
			}

			if (salesRepresentative.CountryId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Sales Representative requires a country association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(salesRepresentative.Name))
			{
				Messages.Add(ValidationMessage.Error("Sales Representative requires a name"));
				valid = false;
			}
			else if (salesRepresentative.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Sales Representative Name cannot exceed 50 characters"));
				valid = false;
			}
            if (!string.IsNullOrEmpty(salesRepresentative.Phone) && salesRepresentative.Phone.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative phone number cannot exceed 50 characters"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(salesRepresentative.Mobile) && salesRepresentative.Mobile.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative mobile number cannot exceed 50 characters"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(salesRepresentative.Fax) && salesRepresentative.Fax.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative fax number cannot exceed 50 characters"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(salesRepresentative.Email) && salesRepresentative.Email.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative email address cannot exceed 100 characters"));
                valid = false;
            }
		    if (!string.IsNullOrEmpty(salesRepresentative.Email) && !salesRepresentative.Email.EmailIsValid())
		    {
		        Messages.Add(ValidationMessage.Error("Sales Representative email address is invalid"));
		        valid = false;
		    }
            if (!string.IsNullOrEmpty(salesRepresentative.CompanyName) && salesRepresentative.CompanyName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative company name cannot exceed 50 characters"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(salesRepresentative.CompanyName) && salesRepresentative.CompanyName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative company name cannot exceed 50 characters"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(salesRepresentative.AdditionalEntityName) && salesRepresentative.AdditionalEntityName.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative additional entity name cannot exceed 50 characters"));
                valid = false;
            }
            if (salesRepresentative.AdditionalEntityCommPercent < 0 || salesRepresentative.AdditionalEntityCommPercent > 9999.9999m)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative additonal entity commission percent must be between zero and 9999.9999"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(salesRepresentative.Street1) && salesRepresentative.Street1.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative street one cannot exceed 50 characters"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(salesRepresentative.Street2) && salesRepresentative.Street2.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative street two cannot exceed 50 characters"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(salesRepresentative.City) && salesRepresentative.City.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative city cannot exceed 50 characters"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(salesRepresentative.State) && salesRepresentative.State.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative state cannot exceed 50 characters"));
                valid = false;
            }
            if (!string.IsNullOrEmpty(salesRepresentative.PostalCode) && salesRepresentative.PostalCode.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Sales Representative postal cannot exceed 50 characters"));
                valid = false;
            }
            
			if (string.IsNullOrEmpty(salesRepresentative.SalesRepresentativeNumber))
			{
				Messages.Add(ValidationMessage.Error("Sales Representative requires a number"));
				valid = false;
			}
			else if (salesRepresentative.SalesRepresentativeNumber.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Sales Representative Number cannot exceed 50 characters"));
				valid = false;
			}

            if (salesRepresentative.SalesRepCommTiers.Count > 0 && !SalesRepCommTiersAreValid(salesRepresentative.SalesRepCommTiers)) valid = false;

			return valid;
		}

		public bool CanDeleteSalesRepresentative(SalesRepresentative salesRepresentative)
		{
			const string query = "select dbo.SalesRepresentativeIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", salesRepresentative.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Sales Representative is referenced one or more times and cannot be deleted."));
			return !used;
		}

        public bool SalesRepCommTiersAreValid(IEnumerable<SalesRepCommTier> salesRepCommTiers)
        {
            var valid = true;

            foreach (var cTier in salesRepCommTiers)
            {
                if (cTier.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Sales Representative Commission Tier requires a tenant association"));
                    valid = false;
                }

                if (cTier.SalesRepresentative == null)
                {
                    Messages.Add(ValidationMessage.Error("Sales Representative Commission Tier requires a sales representative association"));
                    valid = false;
                }

                if (cTier.CommissionPercent < 0 || cTier.CommissionPercent > 9999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Sales Representative Commission Tier commission percentage must be between zero and 9999.9999"));
                    valid = false;
                }

                if (cTier.CeilingValue < 0 || cTier.CeilingValue > 99999999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Sales Representative Commission Tier ceiling value must be between zero and 99999999.9999"));
                    valid = false;
                }
                if (cTier.FloorValue < 0 || cTier.FloorValue > 99999999.9999m)
                {
                    Messages.Add(ValidationMessage.Error("Sales Representative Commission Tier floor value must be between zero and 99999999.9999"));
                    valid = false;
                }
				if (cTier.CeilingValue < cTier.FloorValue)
				{
					Messages.Add(ValidationMessage.Error("Sales Representative Commission Tier ceiling cannot be less than floor value"));
					valid = false;
				}

                if (!((int)cTier.ServiceMode).EnumValueIsValid<ServiceMode>())
                {
                    Messages.Add(ValidationMessage.Error("Sales Representative Commission Tier service mode type is invalid"));
                    valid = false;
                }

                if (!cTier.EffectiveDate.IsValidSystemDateTime())
                {
                    Messages.Add(
                        ValidationMessage.Error("Sales Representative Commission Tier must have a valid effective date between {0} and {1}",
                                                DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }

                if (!cTier.ExpirationDate.IsValidSystemDateTime())
                {
                    Messages.Add(
                        ValidationMessage.Error("Sales Representative Commission Tier must have a valid expiration date between {0} and {1}",
                                                DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
                    valid = false;
                }
            }

            return valid;
        }
	}
}
