﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Accounting;

namespace LogisticsPlus.Eship.Processor.Validation.Accounting
{
    public class ChargeValidator : ValidatorBase
    {
        public bool IsValid(ChargeViewSearchDto charge)
        {
            return HasAllRequiredData(charge);
        }

        public bool HasAllRequiredData(ChargeViewSearchDto charge)
		{
            var valid = true;

            if (charge.VendorId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Charge requires a Vendor association"));
                valid = false;
            }

            if (charge.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Charge requires a tenant association"));
                valid = false;
            }
            
            if (charge.ChargeCodeId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Charge requires a charge code association"));
                valid = false;
            }

            if (!string.IsNullOrEmpty(charge.Comment) && charge.Comment.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Charge comment cannot exceed 100 characters"));
                valid = false;
            }

            if (charge.Quantity <= 0)
            {
                Messages.Add(ValidationMessage.Error("Charge quantity must be greater than zero"));
                valid = false;
            }

            if (charge.UnitBuy < 0)
            {
                Messages.Add(ValidationMessage.Error("Charge unit buy must be greater than or equal to zero"));
                valid = false;
            }

            if (charge.UnitSell < 0)
            {
                Messages.Add(ValidationMessage.Error("Charge unit sell must be greater than or equal to zero"));
                valid = false;
            }

            if (charge.UnitDiscount < 0)
            {
                Messages.Add(ValidationMessage.Error("Charge unit discount must be greater than or equal to zero"));
                valid = false;
            }
            
            return valid;
		}

        public bool ChargesAreValid(IEnumerable<ChargeViewSearchDto> charges, bool requiresDocumentFields = false)
        {
            var valid = true;

            foreach (var charge in charges)
            {
                if (charge.TenantId == default(long))
                {
                    Messages.Add(ValidationMessage.Error("Charges require a tenant association"));
                    valid = false;
                }

                if (!HasAllRequiredData(charge))
                {
                    valid = false;
                }
            }

            if (charges.Select(c => c.ChargeType).Distinct().Count() > 1)
            {
                Messages.Add(ValidationMessage.Error("Charges must have the same charge type"));
                valid = false;
            }

            if (charges.Select(c => c.Number).Distinct().Count() > 1)
            {
                Messages.Add(ValidationMessage.Error("Charges must have the same reference number"));
                valid = false;
            }
            
            if (charges.Select(c => c.VendorId).Distinct().Count() > 1)
            {
                Messages.Add(ValidationMessage.Error("Charges must have the same vendor"));
                valid = false;
            }

            // prevent duplicate Charge Codes from the same entity from being added to the new vendor bill
            if (charges.GroupBy(x => x.ChargeCodeId).Any(g => g.Count() > 1))
            {
                Messages.Add(ValidationMessage.Error("Vendor Bill cannot have two charges with the same charge code from the same entity"));
                valid = false;
            }

            if (requiresDocumentFields)
            {
                // all charges guaranteed to have same document number and date
                if (string.IsNullOrEmpty(charges.First().DocumentNumber))
                {
                    Messages.Add(ValidationMessage.Error("Document number is required"));
                    valid = false;
                }

                if (charges.First().DocumentDate <= DateUtility.SystemEarliestDateTime)
                {
                    Messages.Add(ValidationMessage.Error("A valid Document date is required."));
                    valid = false;
                }
            }

			if (!requiresDocumentFields)
			{
				// all charges guaranteed to have same document number and date
				if (string.IsNullOrEmpty(charges.First().DisputeComments))
				{
					Messages.Add(ValidationMessage.Error("Dispute comments are required"));
					valid = false;
				}
				
			}

            return valid;
        }


    }
}
