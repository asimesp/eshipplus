﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LogisticsPlus.Eship.Processor.Validation
{

	public class ValidationMessageCollection : IEnumerable<ValidationMessage>
	{
		private readonly int _limit;

		private readonly List<ValidationMessage> _msgs = new List<ValidationMessage>();

		public ValidationMessageCollection(int limit = 200)
		{
			_limit = limit;
		}

		public void Insert(int index, ValidationMessage msg)
		{
			if (_msgs.Count + 1 > _limit) _msgs.RemoveAt(0);
			_msgs.Insert(index, msg);
		}

		public void Add(ValidationMessage msg)
		{
			_msgs.Add(msg);
			if (_msgs.Count > _limit) _msgs.RemoveAt(0);
		}

		public void AddRange(IEnumerable<ValidationMessage> msgs)
		{
			_msgs.AddRange(msgs);
			if (_msgs.Count > _limit) _msgs.RemoveRange(_limit, _msgs.Count - _limit);
		}

		public IEnumerator<ValidationMessage> GetEnumerator()
		{
			return _msgs.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
