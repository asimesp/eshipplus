﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence
{
	public class ReportCustomizationValidator : ValidatorBase
	{
		public bool HasAllRequiredData(ReportCustomizationUser selectUser)
		{
			var valid = true;

			if (selectUser.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Report Customization user requires tenant association"));
				valid = false;
			}

			if (selectUser.User == null)
			{
				Messages.Add(ValidationMessage.Error("Report Customization user requires user association"));
				valid = false;
			}

			if (selectUser.ReportConfiguration == null)
			{
				Messages.Add(ValidationMessage.Error("Report Customization user requires report configuration association"));
				valid = false;
			}

			return valid;
		}

		public bool SelectUserExists(ReportCustomizationUser selectUser)
		{
			const string query =
				"SELECT COUNT(*) FROM ReportCustomizationUser WHERE UserId = @UserId AND ReportConfigurationId = @ReportConfigurationId AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"UserId", selectUser.User.Id},
			                 		{"TenantId", selectUser.TenantId},
			                 		{"ReportConfigurationId", selectUser.ReportConfigurationId}
			                 	};
			var duplicateGroupUser = ExecuteScalar(query, parameters).ToInt() > 0;
			if (duplicateGroupUser) Messages.Add(ValidationMessage.Error("Report Customization user is already in Report Configuration"));
			return duplicateGroupUser;
		}

	    public bool SelectGroupExists(ReportCustomizationGroup selectGroup)
	    {
	        const string query =
                "SELECT COUNT(*) FROM ReportCustomizationGroup WHERE GroupId = @GroupId AND ReportConfigurationId = @ReportConfigurationId AND TenantId = @TenantId";
	        var parameters = new Dictionary<string, object>
	        {
	            {"GroupId", selectGroup.GroupId},
	            {"TenantId", selectGroup.TenantId},
                {"ReportConfigurationId", selectGroup.ReportConfigurationId}
	        };
	        var duplicateGroup = ExecuteScalar(query, parameters).ToInt() > 0;
	        if (duplicateGroup) Messages.Add(ValidationMessage.Error("Report Customization group is already in Report Configuration"));
	        return duplicateGroup;
	    }
    }
}
