﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence
{
	public class ReportScheduleValidator : ValidatorBase
	{
		public bool IsValid(ReportSchedule reportSchedule)
		{
			var valid = true;

			if (!HasAllRequiredData(reportSchedule)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(ReportSchedule reportSchedule)
		{
			var valid = true;

			if (!reportSchedule.ScheduleInterval.ToInt().EnumValueIsValid<ScheduleInterval>())
			{
				Messages.Add(ValidationMessage.Error("Report Schedule interval type is invalid"));
				valid = false;
			}

			if (!reportSchedule.Extension.ToInt().EnumValueIsValid<ReportExportExtension>())
			{
				Messages.Add(ValidationMessage.Error("Report Schedule export file extension is invalid"));
				valid = false;
			}

			if (reportSchedule.Notify && string.IsNullOrEmpty(reportSchedule.NotifyEmails))
			{
				Messages.Add(ValidationMessage.Error("Report Schedule requires an email list"));
				valid = false;
			}
			else if (reportSchedule.NotifyEmails.Length > 1000)
			{
				Messages.Add(ValidationMessage.Error("Report Schedule notification email list cannot exceed 1000 characters"));
				valid = false;
			}

		    if (reportSchedule.Notify && !string.IsNullOrEmpty(reportSchedule.NotifyEmails) && 
		        reportSchedule.NotifyEmails.Split(CoreUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries).Any(e => !e.EmailIsValid()))
		    {
		        Messages.Add(ValidationMessage.Error("Report Schedule notification email list contains one or more invalid addresses"));
		        valid = false;
		    }

            if (!reportSchedule.Start.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Report Schedule start date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!reportSchedule.End.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Report Schedule end date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (!reportSchedule.LastRun.IsValidSystemDateTime())
			{
				Messages.Add(ValidationMessage.Error("Report Schedule last run date must be between {0} and {1}",
													 DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime));
				valid = false;
			}

			if (reportSchedule.ReportConfigurationId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Report Schedule requires a report configuration association"));
				valid = false;
			}

			if (reportSchedule.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Report Schedule requires a tenant association"));
				valid = false;
			}

			if (reportSchedule.UserId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Report Schedule requires a user association"));
				valid = false;
			}

			if (reportSchedule.FtpEnabled && string.IsNullOrEmpty(reportSchedule.FtpUrl))
			{
				Messages.Add(ValidationMessage.Error("Report Schedule Ftp is enabled, and requires an Ftp Url"));
				valid = false;
			}

			if (reportSchedule.FtpEnabled && string.IsNullOrEmpty(reportSchedule.FtpUsername))
			{
				Messages.Add(ValidationMessage.Error("Report Schedule Ftp is enabled, and requires an Ftp Username"));
				valid = false;
			}

			if (reportSchedule.FtpEnabled && string.IsNullOrEmpty(reportSchedule.FtpPassword))
			{
				Messages.Add(ValidationMessage.Error("EReport Schedule Ftp is enabled, and requires an Ftp Password"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(reportSchedule.FtpUrl) && reportSchedule.FtpUrl.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Report Schedule Ftp Url must be less than or equal to 100 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(reportSchedule.FtpUsername) && reportSchedule.FtpUsername.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Report Schedule Ftp Username must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(reportSchedule.FtpPassword) && reportSchedule.FtpPassword.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Report Schedule Ftp Password must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(reportSchedule.FtpDefaultFolder) && reportSchedule.FtpDefaultFolder.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Report Schedule Ftp Default Folder must be less than or equal to 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(reportSchedule.DeliveryFilename) && reportSchedule.DeliveryFilename.Length > 100)
			{
				Messages.Add(ValidationMessage.Error("Report Schedule Delivery Filename must be less than or equal to 100 characters"));
				valid = false;
			}

			return valid;
		}
	}
}
