﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence
{
	public class CustomerGroupValidator : ValidatorBase
	{
		public bool CanDeleteCustomerGroup(CustomerGroup customerGroup)
		{
			const string query = "select dbo.CustomerGroupIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", customerGroup.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Customer group is referenced one or more times and cannot be deleted."));
			return !used;
		}

		public bool IsValid(CustomerGroup customerGroup)
		{
			var valid = true;

			if (!HasAllRequiredData(customerGroup)) valid = false;

			if (!IsUnique(customerGroup)) valid = false;

			return valid;
		}

		public bool HasAllRequiredData(CustomerGroup customerGroup)
		{
			var valid = true;

			if (customerGroup.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Customer Group requires a tenant association"));
				valid = false;
			}

			if (string.IsNullOrEmpty(customerGroup.Name))
			{
				Messages.Add(ValidationMessage.Error("Customer Group requires a Name"));
				valid = false;
			}
			else if (customerGroup.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Customer Group Name cannot exceed 50 characters"));
				valid = false;
			}

			if (!string.IsNullOrEmpty(customerGroup.Description) && customerGroup.Description.Length > 200)
			{
				Messages.Add(ValidationMessage.Error("Customer Group Description cannot exceed 50 characters"));
				valid = false;
			}

			if (customerGroup.CustomerGroupMaps == null || customerGroup.CustomerGroupMaps.Count < 1)
			{
				Messages.Add(ValidationMessage.Error("Customer Group must have at least one customer assigned"));
				valid = false;
			}
			return valid;
		}

		public bool IsUnique(CustomerGroup customerGroup)
		{
			var query = customerGroup.IsNew
							 ? "SELECT COUNT(*) FROM CustomerGroup WHERE Name = @Name AND TenantId = @TenantId"
							 : "SELECT COUNT(*) FROM CustomerGroup WHERE Name = @Name AND TenantId = @TenantId AND Id <> @Id";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Name", customerGroup.Name},
			                 		{"TenantId", customerGroup.TenantId},
			                 	};
			if (!customerGroup.IsNew) parameters.Add("Id", customerGroup.Id);

			var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;

			if (!isUnique) Messages.Add(ValidationMessage.Error("Customer Group name is already in use"));
			return isUnique;
		}

		public bool CustomerGroupMapExists(CustomerGroupMap customerGroupMap)
		{
			const string query = "SELECT COUNT(*) FROM CustomerGroupMap " +
								 "WHERE CustomerGroupID = @CustomerGroupID " +
								 "AND TenantId = @TenantId " +
								 "AND CustomerId = @CustomerId";

			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"CustomerGroupID", customerGroupMap.CustomerGroupId},
			                 		{"TenantId", customerGroupMap.TenantId},
                                    {"CustomerId", customerGroupMap.CustomerId}
			                 	};

			var duplicateMapping = ExecuteScalar(query, parameters).ToInt() > 0;

			if (!duplicateMapping) Messages.Add(ValidationMessage.Error("Customer Group Mapping already Exists"));
			return duplicateMapping;
		}
	}
}
