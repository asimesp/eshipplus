﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence
{
    public class ReportTemplateValidator : ValidatorBase
    {
		public bool CanDeleteTemplate(ReportTemplate template)
		{
			const string query = "select count(*) from ReportConfiguration where ReportTemplateId = @Id";
			var parameters = new Dictionary<string, object> { { "Id", template.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Report template is referenced one or more times and cannot be deleted."));
			return !used;
		}

        public bool IsValid(ReportTemplate reportTemplate)
        {
            var valid = HasAllRequiredData(reportTemplate);

            return valid;
        }

        public bool HasAllRequiredData(ReportTemplate reportTemplate)
        {
            var valid = true;


            if (string.IsNullOrEmpty(reportTemplate.Name))
            {
                Messages.Add(ValidationMessage.Error("Report template requires a name"));
                valid = false;
            }
            else if (reportTemplate.Name.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Report template name cannot exceed 50 characters"));
                valid = false;
            }

            if(!string.IsNullOrEmpty(reportTemplate.Description) && reportTemplate.Description.Length>200)
            {
                Messages.Add(ValidationMessage.Error("Report template description cannot exceed 200 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(reportTemplate.Query))
            {
                Messages.Add(ValidationMessage.Error("Report template requires a query "));
                valid = false;
            }

            if (string.IsNullOrEmpty(reportTemplate.DefaultCustomization))
            {
                Messages.Add(ValidationMessage.Error("Report template requires a default customization"));
                valid = false;
            }
           
            return valid;
        }
    }
}
