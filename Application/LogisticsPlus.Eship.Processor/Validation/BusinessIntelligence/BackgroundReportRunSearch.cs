﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence
{
    public class BackgroundReportRunSearch : EntityBase
    {
        public List<BackgroundReportRunRecord> FetchBackgroundReportRunRecordsForUser(long userId)
        {
            const string query = "SELECT * FROM BackgroundReportRunRecord WHERE UserId = @UserId";

            var records = new List<BackgroundReportRunRecord>();

            var parameters = new Dictionary<string, object>
                                 {
                                     {"UserId", userId}
                                 };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    records.Add(new BackgroundReportRunRecord(reader));
            Connection.Close();

            return records;
        }

        public List<BackgroundReportRunRecord> FetchBackgroundReportsToRun()
        {
            const string query = "SELECT * FROM BackgroundReportRunRecord WHERE ExpirationDate = @MinDate";

            var records = new List<BackgroundReportRunRecord>();

            var parameters = new Dictionary<string, object>
            {
                {"MinDate", DateUtility.SystemEarliestDateTime}
            };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    records.Add(new BackgroundReportRunRecord(reader));
            Connection.Close();

            return records;
        }

        public List<BackgroundReportRunRecord> FetchExpiredBackgroundReportRunRecords()
        {
            const string query = "SELECT * FROM BackgroundReportRunRecord WHERE ExpirationDate <= @CurrentDate AND ExpirationDate > @MinDate";

            var records = new List<BackgroundReportRunRecord>();

            var parameters = new Dictionary<string, object>
            {
                {"CurrentDate", DateTime.Now},
                {"MinDate", DateUtility.SystemEarliestDateTime},
            };

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    records.Add(new BackgroundReportRunRecord(reader));
            Connection.Close();

            return records;
        }
    }
}
