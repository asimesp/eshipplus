﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence
{
    public class VendorGroupValidator : ValidatorBase
    {
		public bool CanDeleteVendorGroup(VendorGroup vendorGroup)
		{
			const string query = "select dbo.VendorGroupIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", vendorGroup.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Vendor group is referenced one or more times and cannot be deleted."));
			return !used;
		}

        public bool IsValid(VendorGroup vendorGroup)
        {
            var valid = true;

            if (!HasAllRequiredData(vendorGroup)) valid = false;

            if (!IsUnique(vendorGroup)) valid = false;

            return valid;
        }

        public bool HasAllRequiredData(VendorGroup vendorGroup)
        {
            var valid = true;

            if (vendorGroup.TenantId == default(long))
            {
                Messages.Add(ValidationMessage.Error("Vendor Group requires a tenant association"));
                valid = false;
            }

            if (string.IsNullOrEmpty(vendorGroup.Name))
            {
                Messages.Add(ValidationMessage.Error("Vendor Group requires a Name"));
                valid = false;
            }
            else if (vendorGroup.Name.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Vendor Group Name cannot exceed 50 characters"));
                valid = false;
            }

			if (!string.IsNullOrEmpty(vendorGroup.Description) && vendorGroup.Description.Length > 200)
            {
                Messages.Add(ValidationMessage.Error("Vendor Group Description cannot exceed 50 characters"));
                valid = false;
            }

			if (vendorGroup.VendorGroupMaps == null || vendorGroup.VendorGroupMaps.Count < 1)
            {
                Messages.Add(ValidationMessage.Error("Vendor Group must have at least one vendor assigned"));
                valid = false;
            }
            return valid;
        }

        public bool IsUnique(VendorGroup vendorGroup)
        {
            var query = vendorGroup.IsNew
                             ? "SELECT COUNT(*) FROM VendorGroup WHERE Name = @Name AND TenantId = @TenantId"
                             : "SELECT COUNT(*) FROM VendorGroup WHERE Name = @Name AND TenantId = @TenantId AND Id <> @Id";

            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Name", vendorGroup.Name},
			                 		{"TenantId", vendorGroup.TenantId},
			                 	};

            if (!vendorGroup.IsNew) parameters.Add("Id", vendorGroup.Id);

            var isUnique = ExecuteScalar(query, parameters).ToInt() <= 0;

            if (!isUnique) Messages.Add(ValidationMessage.Error("Vendor Mappping is not unique"));
            return isUnique;
        }

        public bool VendorGroupMapExists(VendorGroupMap vendorGroupMap)
        {
            const string query = "SELECT COUNT(*) FROM VendorGroupMap " +
                                 "WHERE VendorGroupID = @VendorGroupID " +
                                 "AND TenantId = @TenantId " +
                                 "AND VendorId = @VendorId";

            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"VendorGroupID", vendorGroupMap.VendorGroupId},
			                 		{"TenantId", vendorGroupMap.TenantId},
                                    {"VendorId", vendorGroupMap.VendorId}
			                 	};

            var duplicateMapping = ExecuteScalar(query, parameters).ToInt() > 0;

            if (!duplicateMapping) Messages.Add(ValidationMessage.Error("Vendor Group Mapping already exists"));
            return duplicateMapping;
        }

    }
}
