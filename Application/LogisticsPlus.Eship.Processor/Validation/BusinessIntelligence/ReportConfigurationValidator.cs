﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence
{
	public class ReportConfigurationValidator : ValidatorBase
	{
		public bool CanDeleteReportConfiguration(ReportConfiguration configuration)
		{
			const string query = "select dbo.ReportConfigurationIdUsageCount(@Id)";
			var parameters = new Dictionary<string, object> { { "Id", configuration.Id } };
			var used = ExecuteScalar(query, parameters).ToInt() > 0;
			if (used) Messages.Add(ValidationMessage.Error("Report Configuration is referenced one or more times and cannot be deleted."));
			return !used;
		}

		public bool IsValid(ReportConfiguration reportConfiguration)
		{
			var valid = HasAllRequiredData(reportConfiguration);
			
			return valid;
		}

		public bool HasAllRequiredData(ReportConfiguration reportConfiguration)
		{
			var valid = true;

			if (string.IsNullOrEmpty(reportConfiguration.Name))
			{
				Messages.Add(ValidationMessage.Error("Report Configuration requires a name"));
				valid = false;
			}
			else if (reportConfiguration.Name.Length > 50)
			{
				Messages.Add(ValidationMessage.Error("Report Configuration name cannot exceed 50 characters"));
				valid = false;
			}

			if (string.IsNullOrEmpty(reportConfiguration.Description))
			{
				Messages.Add(ValidationMessage.Error("Report Configuration requires a description"));
				valid = false;
			}
			else if (reportConfiguration.Description.Length > 500)
			{
				Messages.Add(ValidationMessage.Error("ReportConfiguration number cannot exceed 500 characters"));
				valid = false;
			}

			if (reportConfiguration.ReportTemplateId == default(long) && !reportConfiguration.QlikConfiguration)
			{
				Messages.Add(ValidationMessage.Error("Report Configuration requires a report template association or it must be a qlik configuration"));
				valid = false;
			}

			if (reportConfiguration.QlikConfiguration && !reportConfiguration.QlikSheets.Any())
			{
				Messages.Add(ValidationMessage.Error("Report Configuration requires at least one qlik sheet when marked as a qlik configuration."));
				valid = false;
			}

			if (reportConfiguration.TenantId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Report Configuration requires a tenant association"));
				valid = false;
			}

			if (reportConfiguration.UserId == default(long))
			{
				Messages.Add(ValidationMessage.Error("Report Configuration requires a user association"));
				valid = false;
			}

			if (reportConfiguration.QlikSheets.Count > 0 && !QlikSheetsAreValid(reportConfiguration.QlikSheets)) valid = false;

			return valid;
		}

        public bool NamesAreValid(List<string> dataColumns, List<string> pivotTableNames, List<string> chartNames, string configName)
        {
            var valid = true;

            if (dataColumns.Count != dataColumns.Distinct().Count())
            {
                valid = false;
                Messages.Add(ValidationMessage.Error("Report Configuration cannot have duplicate data columns"));
            }

            if (pivotTableNames.Count != pivotTableNames.Distinct().Count() || pivotTableNames.Contains(string.Empty))
            {
                valid = false;
                Messages.Add(ValidationMessage.Warning("One or more pivot tables have an invalid or duplicate name"));
            }

            if (pivotTableNames.Any(p => p == configName))
            {
                valid = false;
                Messages.Add(ValidationMessage.Warning("Pivot table names cannot be the Report Configuration name"));
            }

            if (chartNames.Count != chartNames.Distinct().Count() || chartNames.Contains(string.Empty))
            {
                valid = false;
                Messages.Add(ValidationMessage.Warning("One or more charts have an invalid or duplicate name "));
            }

            if (chartNames.Any(pivotTableNames.Contains))
            {
                valid = false;
                Messages.Add(ValidationMessage.Warning("Chart Titles cannot match the names of pivot tables"));
            }

            if (chartNames.Any(p => p == configName))
            {
                valid = false;
                Messages.Add(ValidationMessage.Warning("Chart Titles cannot be the Report Configuration name"));
            }

            return valid;
        }

		private bool QlikSheetsAreValid(IEnumerable<QlikSheet> qlikSheets)
		{
			var valid = true;

			foreach (var sheet in qlikSheets)
			{
				if (string.IsNullOrEmpty(sheet.Name))
				{
					Messages.Add(ValidationMessage.Error("Report Configuration qlick sheet requires a name"));
					valid = false;
				}
				else if (sheet.Name.Length > 50)
				{
					Messages.Add(ValidationMessage.Error("Report Configuration qlick sheet name cannot exceed 50 characters"));
					valid = false;
				}

				if (string.IsNullOrEmpty(sheet.Link))
				{
					Messages.Add(ValidationMessage.Error("Report Configuration qlick sheet requires a link"));
					valid = false;
				}
				else if (sheet.Link.Length > 2000)
				{
					Messages.Add(ValidationMessage.Error("Report Configuration qlick sheet link cannot exceed 2000 characters"));
					valid = false;
				}
			}

			return valid;
		}
	}
}
