﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence
{
    public class QlikUserValidator : ValidatorBase
    {
        public bool IsValid(QlikUser qlikUser)
        {
            var valid = HasAllRequiredData(qlikUser);
            if (DuplicateUserId(qlikUser)) valid = false;
            return valid;
        }

        public bool HasAllRequiredData(QlikUser qlikUser)
        {
            var valid = true;

            if (string.IsNullOrEmpty(qlikUser.UserId))
            {
                Messages.Add(ValidationMessage.Error("Qlik user requires a user id"));
                valid = false;
            }
            else if (qlikUser.UserId.Length > 50)
            {
                Messages.Add(ValidationMessage.Error("Qlik user user id cannot exceed 50 characters"));
                valid = false;
            }

            if (string.IsNullOrEmpty(qlikUser.Name))
            {
                Messages.Add(ValidationMessage.Error("Qlik user requires a name"));
                valid = false;
            }
            else if (qlikUser.Name.Length > 100)
            {
                Messages.Add(ValidationMessage.Error("Qlik user name cannot exceed 100 characters"));
                valid = false;
            }

            if (qlikUser.QlikUserAttributes.Count > 0 && !QlikUserAttributesAreValid(qlikUser.QlikUserAttributes))
                valid = false;

            return valid;
        }

        private bool QlikUserAttributesAreValid(IEnumerable<QlikUserAttribute> qlikUserAttributes)
        {
            var valid = true;
            foreach (var qlikUserAttribute in qlikUserAttributes)
            {
                if (qlikUserAttribute.QlikUser == null)
                {
                    Messages.Add(ValidationMessage.Error("Qlik user attribute requires a qlik user reference"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(qlikUserAttribute.Type))
                {
                    Messages.Add(ValidationMessage.Error("Qlik user attribute requires a type"));
                    valid = false;
                }
                else if (qlikUserAttribute.Type.Length > 50)
                {
                    Messages.Add(ValidationMessage.Error("Qlik user attribute type cannot exceed 50 characters"));
                    valid = false;
                }

                if (string.IsNullOrEmpty(qlikUserAttribute.Value))
                {
                    Messages.Add(ValidationMessage.Error("Qlik user attribute requires a value"));
                    valid = false;
                }
                else if (qlikUserAttribute.Value.Length > 200)
                {
                    Messages.Add(ValidationMessage.Error("Qlik user attribute value cannot exceed 200 characters"));
                    valid = false;
                }
            }
            return valid;

        }


        public bool DuplicateUserId(QlikUser qlikUser)
        {
            if (!qlikUser.IsNew) return false;

            const string query = "Select count(*) from QlikUsers where userid = @UserId";
            var parameters = new Dictionary<string, object> {{"UserId", qlikUser.UserId}};
            var duplicateGroupName = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateGroupName) Messages.Add(ValidationMessage.Error("Qlik user id is already in use"));
            return duplicateGroupName;
        }

        public bool QlikUserAttributeExists(QlikUserAttribute qlikUserAttribute)
        {
            const string query = "Select count(*) from QlikUserAttributes where userid = @UserId and [type] = @Type and [value] = @Value";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"UserId", qlikUserAttribute.UserId},
			                 		{"Type", qlikUserAttribute.Type},
			                 		{"Value", qlikUserAttribute.Value}
			                 	};
            var duplicateQlikAttribute = ExecuteScalar(query, parameters).ToInt() > 0;
            if (duplicateQlikAttribute) Messages.Add(ValidationMessage.Error("Qlik User Attribute is already in Qlik User"));
            return duplicateQlikAttribute;
        }
    }
}
