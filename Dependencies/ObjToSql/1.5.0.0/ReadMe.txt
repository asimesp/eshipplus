﻿Library:	ObjToSql - Object to Relational Database. (version 1.4.2.0)
Brief:		A simple ORM model.
Author:		Christopher Oyesiku.

INTRO:
	ObjToSql is aimed at simplifying the mapping of an Object to a relational database entity.  Speed of executiong and ease of use
	have been taken into account.  While this solution does not exhaust all possibilities, I believe it will at least gives a simple
	foundation, allowing for increased speed of execution with greatly reduced overhead.

GETTING STARTED:
	There are two main classes to consider.  
		1. DatabaseConnection
		2. EntityBase

	DatabaseConnection is a static class in which default connection string and database type can be set.  Setting these values here
	eliminates the need to set them on every class object unless the developer has a specific reason to do e.g. accessing and alternative
	database.

	EntityBase is the base class from which all objects being mapped should inherit.  This class has the methods for inserting, updating,
	deleting, loading single record, and executing a data reader for multiple records.  Any inheriting class using EntityBase should be
	marked with the Entity attribute in ObjToSql.Core.Attributes.  Properties in the inheriting class that correspond to database columns
	should be marked with the Property attribute in the same namespace.
	
	Please see ObjToSql.xml for ObjToSql documentation.
	
NOTES:
	When utilizing transaction, it is essential that only the originator starts and commits the transaction. An originator is 
	consider the principle calling method from which the save will occur.  For example, in a parent-child relationship where we 
	want to save the parent and all the children in one transaction, the parent should be the originator in the form (Psuedo Code):
	
	Parent Method Save()
		Set Parent Connection
		Call Begin Transaction
		For each child of parent
			Set Child Connection
			Set Child Transaction
		End For each
		Call Parent Sql function (Insert, Update, Delete, etc) // do this first; values from parent might need to child
		For each child of parent
			Call child Sql function
		End For each
		Commit Transaction
		For each child in parent
			Clean Transaction
			Clean Connection
		End for each
	End Method
	
	If not executing in a transaction, the connection is automatically closed after the method call except in the case of the 
	GetReader(...) calls.
	
	When utilizing the GetReader(...) function, it is important to close the Connection after execution with the reader is complete.
	The reader connection will equal the EntityBase.Connection field.  There are two scenarios
		1. There is a shared Connection value previously set which the reader will then use, OR
		2. A new connection (from the DatabaseConnection.DefaultConnection) field is created, set to both the Entity and the reader.
		
	When using stored procedures as the EntitySource, the insert, delete, update and load stored procedures will be referenced as
	
			[EntityAttribute.DbSource][Insert | Delete | Update | Load]
			
	For example assume we have a class
	
	[ObjToSql.Core.Attributes.Entity("SimpleClass", Source = EntitySource.StoredProcedure, SourceName = "SimpleObject")]
	public class SimpleClass : EntityBase { }
	
	The insert stored procedure is expected to be "SimpleObjectInsert".
	
	When intent on using the Diff() method to retrieve a list of property value changes, ensure the GetSnapShot() method is called
	prior to your updates i.e. order of calls:
	
		GetSnapShot()
		... some updates here ....
		Diff()
		
	ObjToSql utilizes the SCOPE_IDENTITY() and LAST_INSERT_ID() MsSql and Mysql functions respectively.  It is suggested that if you 
	have entities with composite keys requiring multiple auto-inserts, you utilize store procedures  for such entities if using ObjToSql.
	
	With version 1.3.2.0, a LastGenratedQuery variable has been added to the EntityBase object to hold the last auto generated query
	executed.  This might be handy for debugging purposes.
	
	With version 1.5.0.0, object mapping cache is introduces to reduce processing requirements but will increase memory footprint.  NOTE
	on usage: when an object is first accessed for mapping, it's cache is built and stored. It is then referenced from that store on
	subsequent access calls.
	
REFERENCES:
	This library is freely provided for use.  Please cite author and any other references.  Thank you.

SAMPLE CODE (c#):

// set default settings at start up or are your desired location:

DatabaseConnection.DefaultConnectionString = "Server=[myServer];database=[myDatabase];user=[myUser];password=[myPassword]";
DatabaseConnection.DatabaseType = DatabaseType.MsSql;

// sample class setup

[ObjToSql.Core.Attributes.Entity("SimpleClass", Source = EntitySource.TableView, SourceName = "SimpleTable")]
public class SimpleClass : EntityBase
{
	private int id;

	[ObjToSql.Core.Attributes.Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.Int, Key = true)]
	private int Id { get { return id; }	set { id = value; } }

	[ObjToSql.Core.Attributes.EnableSnapShot("NameWho", Description = "Simple Class Name")]
	[ObjToSql.Core.Attributes.Property("NameWho", Column = "Name", DataType = SqlDbType.VarChar)]
	public string NameWho { get; set; }

	[ObjToSql.Core.Attributes.Property("Date", DataType = SqlDbType.DateTime)]
	public DateTime Date { get; set; }

	public SimpleClass(string connectionString, int id)
		: base(connectionString)
	{
		this.id = id;
		Load();
	}

	public SimpleClass(int id) : this(id, false)
	{
	}
	
	public SimpleClass(int id, bool withSnapShot)
	{
		Id = id;
		Load();

		if (withSnapShot) GetSnapShot();
	}

	public SimpleClass(DbDataReader reader)
	{
		Load(reader);
	}

	public SimpleClass()
	{
	}

	public void Save()
	{
		if (id == 0) Insert();
		else Update();
	}

	public void SaveInTransaction()
	{
		Connection = DatabaseConnection.DefaultConnection;
		BeginTransaction();
		if (id == 0) Insert();
		else Update();
		CommitTransaction();
	}

	public void SaveInTransactionWithOther(SimpleClass s)
	{
		Connection = DatabaseConnection.DefaultConnection;
		BeginTransaction();
		
		// other setup
		s.Connection = Connection;
		s.Transaction = Transaction;

		if (id == 0) Insert();
		else Update();

		s.Save(); // save other

		CommitTransaction();
		
		// clean up other
		s.Connection = null;
		s.Transaction = null;
	}

	public new void Delete()
	{
		base.Delete();
	}

	public List<SimpleClass> FetchAll()
	{
		const string query = "select * from SimpleTable";
		var list = new List<SimpleClass>();
		using( reader = GetReader(query, null))		
			while(reader.Read())
			{
				var simpleClass = new SimpleClass(reader);
				list.Add(simpleClass);
			}
		Connection.Close();	// NOTE: must close the reader connection here!
		return list;
	}
	
	public int UpdateAllDate()
	{
		const string query = "update SimpleTable set Date = @date";
		return ExecuteNonQuery(query, new Dictionary<string, object> {{"date", DateTime.Now}});
	}

	public int GetRecordCount()
	{
		const string query = "select count(*) from SimpleTable";
		return (int) ExecuteScalar(query, null);
	}
	
	public List<string> ListChanges()
	{
		return Diff();
	}
}