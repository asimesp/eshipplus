NOTE:

This DLL and the associated source archive IS NOT the official EPPLus 4.0.1.2. This version merely contains a fix/addition that allows you to specify the Format for a ExcelPivotTableField object.

Refer to ExcelPivotTableField.cs, lines 551-593 to see the fix. This fix was directly copied from ExcelPivotTableDataField.cs, lines 139-167.