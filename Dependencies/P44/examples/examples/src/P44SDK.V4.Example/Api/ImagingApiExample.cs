using System;

using System.Collections.Generic;
using P44SDK.V4.Client;
using P44SDK.V4.Api;
using P44SDK.V4.Model;

namespace P44SDK.V4.Example
{
    public class ImagingApiExample : IApiExample
    {
        //<summary>
        // This method makes an request to the project44 MultimodalImaging API and prints the results to the console.
        //</summary>
        public void execute(string username,
            string password,
            string hostname){

            // Intitialize the CapacityProviderAccountGroupManagementApi client, passing in the appropriate hostname
            var apiClient = new MultiModalImageRetrievalApi (hostname);
            apiClient.Configuration.Username = username;
            apiClient.Configuration.Password = password;
            apiClient.Configuration.AddDefaultHeader( "Content-Type", "application/json" );

            try {

                var listOfShipmentIdentifiers = new List<ShipmentIdentifier>();
                listOfShipmentIdentifiers.Add( new ShipmentIdentifier( ShipmentIdentifier.TypeEnum.PRO, "TEST_PRO" ) );

                var listOfAccounts = new List<CapacityProviderAccount>();
                listOfAccounts.Add( new CapacityProviderAccount( "CNWY" ) );

                var query = new MultiModalImageQuery( new CapacityProviderAccountGroup( listOfAccounts ),
                                                      listOfShipmentIdentifiers,
                                                      new Address( "60622" ),
                                                      MultiModalImageQuery.DocumentTypeEnum.BILLOFLADING,
                                                      MultiModalImageQuery.ImageFormatEnum.PNG,
                                                      MultiModalImageQuery.ModeEnum.LTL );

                Console.WriteLine("");
                Console.WriteLine( "Invoking Query on Imaging API..." );
                
                var imageReference = apiClient.QueryImages( query );

                Console.WriteLine("");
                Console.WriteLine("Processing Query Results...");

                processOutput( imageReference );
            }
            catch(ApiException ex){
                Console.Write (ex.ToString());
            }
        }


        private void processOutput( MultiModalImageReference reference )
        {
            Console.WriteLine ("");
            Console.WriteLine(String.Format("Image for document type {0} may be retrieved from {1} in {2} format.",
                                             reference.DocumentType,
                                             reference.ImageUrl,
                                             reference.ImageFormat ) );
        }
    }
}