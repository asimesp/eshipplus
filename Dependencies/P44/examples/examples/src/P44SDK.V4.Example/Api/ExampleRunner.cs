﻿using System;
using CommandLine;
using CommandLine.Text;


namespace P44SDK.V4.Example
{

	//<summary>
	//Simple class to collect and parse command line arguments.
	//</summary>
	class Options  {
		[Option('u', "username", Required = true,
			HelpText = "project44 username")]
		public string Username { get; set; }

		[Option('p', "password", Required = true,
			HelpText = "project44 password")]
		public string Password { get; set; }


		[Option('h', "hostname", Required = true,
			HelpText = "hostname of the p44 API server (ex. https://cloud.p-44.com)")]
		public string Hostname { get; set; }

		[Option('t', "type", Required = false,
			HelpText = "optional type indicator,  where type indicator is one of ltl_rate_quote, vltl_rate_quote")]
		public string Type { get; set; }

		[ParserState]
		public IParserState LastParserState { get; set; }

		[HelpOption]
		public string GetUsage() {
			var help = new HelpText {

				AdditionalNewLineAfterOption = true,
				AddDashesToOption = true };


			help.AddOptions(this);
			return help;
		}
	}


	public class ExampleRunner
	{
		static void Main(string[] args)

		{

			var options = new Options();
			if (CommandLine.Parser.Default.ParseArguments (args, options)) {

				IApiExample example;
				if (null == options.Type || "ltl_rate_quote".Equals (options.Type)) {
					example = new P44SDK.V4.Example.LTLQuotesApiExample ();
				} else if ("vltl_rate_quote".Equals (options.Type)) {
					example = new P44SDK.V4.Example.VolumeLTLQuotesApiExample ();
				} else if ("account_group_management".Equals (options.Type)) {
					example = new P44SDK.V4.Example.CapacityProviderAccountGroupApiExample ();
				} else if ("account_management".Equals (options.Type)) {
					example = new P44SDK.V4.Example.CapacityProviderAccountApiExample ();
				} else if ("location_management".Equals (options.Type)) {
					example = new P44SDK.V4.Example.LocationManagementApiExample ();
				} else if ("tl_tracking".Equals (options.Type)) {
					example = new P44SDK.V4.Example.TLTrackingApiExample ();
				} else if ("imaging".Equals (options.Type)) {
					example = new P44SDK.V4.Example.ImagingApiExample ();
				} else {
					throw new ArgumentException ("Unknown example type \""+options.Type+"\"");
				}

		    	example.execute (options.Username, options.Password, options.Hostname);

			}
		} 
	}
}

