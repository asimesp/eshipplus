/* 
 * project44 REST API
 *
 * *Advancing transportation technology through API connections*  # Introduction The Core Services API is designed to be easy to use and simple to test to help project44's customers and partners get up and running quickly. Use the API endpoints to connect to your Carriers and other Capacity Providers to obtain rate quotes, dispatch and track shipments and retrieve the documents needed for invoicing and reconciling payments.  The current version of the API is **v2**, which is **not backward compatible**. Any organization who went live before 2017 will require a *version bump* to use the new APIs. Contact your project44 representative to learn more about upgrading to v2.  We will make available a printable copy of the v2 APIs in the near future.   # Getting Started  The project44 API is a RESTful API. This means that the API is designed to allow you to work with objects using familiar HTTP verbs like so: * read = GET * create = POST * update = PUT * delete = DELETE  All production API requests are made to:  `https://cloud.p-44.com/api/v2`  There is also a testing sandbox to use when developing and testing applications, with requests being made to:  `https://test.p-44.com/api/v2`  The Core Services API is available to project44 customers and integration partners and all organizations must acquire valid credentials prior to using the web services. All the API endpoints require authentication. Please see the *Authentication* section below for additional information.  # Authentication The project44 API offers authentication through HTTP Basic over HTTPS. Most client software provides a mechanism for supplying a user name and password and will build the required authentication header automatically.  **Don't have support in your client software?** In the case where you need to manually construct and send basic auth headers, perform the following steps: 1. Build a string of the form username:password 2. Base64 encode the string 3. Supply an \"Authorization\" header with content \"Basic \" followed by the encoded string. For example, the string \"john:john\" encodes to \"am9objpqb2hu\" in base64,  so you would add the following HTTP header to your request: `Authorization: Basic am9objpqb2hu`   # Supported Data Formats The project44 API uses JSON as the default format.  **Why JSON?** JSON is inherently more efficient than XML as fewer bits are being passed across the wire, and less machine time is required to process the data on either end. JSON is a string representation of a key/value pair where the values can be arrays, strings, integers or even full objects. The key names are relevant and enable JSON parsers to quickly scan through the records and convert them into objects developers can interact with in code. JSON is also lightweight, which means existing objects and collections can be serialized and deserialized quickly and easily.  **Don't have support for JSON?** In the case where you need for your application to work with data in XML format, you can request that APIs use XML data using one of the following methods: * For POST/PUT requests, if the request body is in XML, set the following HTTP header: `Content-Type: application/xml` * For API responses, to have objects returned as XML, set the following HTTP header: `Accept: application/xml`   # Software Development Kits (SDKs) While you can use project44 APIs by making direct HTTP requests, we provide client library code for all our APIs that  make it easier to access them from your favorite languages.   Currently we support Java and C# client libraries.   Additionally, if your existing tooling requires the use of XML, we can supply XSD files to assist with your implementation.   These can be obtained by contacting your project44 representative.  # Sample Requests  ## Curl Sample Rate Quote Request: ```bash curl -X POST - -basic -u YOUR_USERNAME - -header 'Content-Type: application/json' \\   - -header 'Accept: application/json' -d '{ \\   \"originAddress\": { \\     \"postalCode\": \"60606\" \\    }, \\   \"destinationAddress\": { \\      \"postalCode\": \"90210\" \\   }, \\   \"lineItems\": [ \\      { \\       \"freightClass\": \"70\", \\        \"totalWeight\": 10,        \"packageDimensions\": { \\          \"length\": 40, \\         \"width\": 40, \\         \"height\": 30 \\       } \\     } \\   ] \\   }' https://cloud.p-44.com/api/v2/quotes/rates/query ```  ## Java Sample Rate Quote Request: ```java          final ApiClient defaultClient = Configuration.getDefaultApiClient();         defaultClient.setBasePath( \"https://cloud.p-44.com\" );          // Configure HTTP basic authorization: basicAuth         final HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication( \"basicAuth\" );         basicAuth.setUsername( YOUR_USERNAME );         basicAuth.setPassword( YOUR_PASSWORD );          final LTLQuotesApi apiInstance = new LTLQuotesApi( defaultClient );          final RateQuoteQuery rateQuoteQuery = new RateQuoteQuery();          // Set the pickup address         final Address origin = new Address();         origin.setPostalCode( \"60606\" );         rateQuoteQuery.setOriginAddress( origin );          // Set the destination address         final Address destination = new Address();         destination.setPostalCode( \"90210\" );         rateQuoteQuery.setDestinationAddress( destination );          // Add a line item         final LineItem lineItem = new LineItem();         lineItem.setFreightClass( LineItem.FreightClassEnum._70 );         lineItem.setTotalWeight( new BigDecimal( \"10\" ) );         final CubicDimension cubicDimension = new CubicDimension();         cubicDimension.setLength( new BigDecimal( \"40\" ) );         cubicDimension.setWidth( new BigDecimal( \"40\" ) );         cubicDimension.setHeight( new BigDecimal( \"40\" ) );         lineItem.setPackageDimensions( cubicDimension );         rateQuoteQuery.addLineItemsItem( lineItem );          // Invoke the API         final RateQuoteCollection result = apiInstance.queryRateQuotes( rateQuoteQuery );          // ... process the result ...         final List<RateQuote> rateQuotes = result.getRateQuotes();         for ( final RateQuote rateQuote : rateQuotes )         {             // Check for any issues             final List<Message> errors = rateQuote.getErrorMessages();             if ( null != errors && !errors.isEmpty() )             {                 logger.log( Level.SEVERE, \"Issues from vendor: \" + rateQuote.getCapacityProviderAccountGroup().getAccounts() );                 for ( final Message error : errors )                 {                     logger.log( Level.SEVERE, error.toString() );                 }                 System.exit( 0 );             }              // Check for any information/notes returned by the capacity provider             final List<Message> infos = rateQuote.getInfoMessages();             if ( null != infos && !infos.isEmpty() )             {                 logger.info( \"Information from vendor: \" + rateQuote.getCapacityProviderAccountGroup().getAccounts() );                 for ( final Message info : infos )                  {                     logger.info( info.toString() );                 }             }              // Process the quote             final BigDecimal total = rateQuote.getRateQuoteDetail().getTotal();             // ... more result processing ...         }         catch ( final ApiException e )         {             // ... handle errors ...             final Gson gson = new Gson();             final ApiError apiError = gson.fromJson( e.getResponseBody(), ApiError.class );             logger.log( Level.SEVERE, \"Exception when calling QuotesApi#rate : \" + apiError );             logger.log( Level.SEVERE, \"HTTP STATUS: \" + e.getCode() );         }           ```              ## C# Sample Rate Quote Request: ```csharp     LtlQuotesApi apiClient = new LtlQuotesApi (\"https://cloud.p-44.com\");     // Configure HTTP Basic authentication    apiClient.Configuration.Username = \"YOUR_USERNAME\";    apiClient.Configuration.Password = \"YOUR_PASSWORD\";     // Create the origin and destination addresses    var originAddress = new Address (\"60606\");    var destinationAddress = new Address (\"90210\");     // Create a line item (freight class 70, 30 lbs, 40x40x20 inches)    var lineItem = new LineItem (LineItem.FreightClassEnum._70,                                 30,                                 new CubicDimension (40, 40, 20));    var lineItems = new List<LineItem> ();    lineItems.Add (lineItem);     RateQuoteQuery rateQuoteQuery = new RateQuoteQuery (originAddress,                                                      destinationAddress,                                                     lineItems);     try {     // Invoke the API     var rateQuoteCollection = apiClient.QueryRateQuotes (rateQuoteQuery);      // Process results     var rateQuotes = rateQuoteCollection.RateQuotes;      foreach (var rateQuote in rateQuotes) {                 // Check for any issues      var errors = rateQuote.ErrorMessages;      if (null != errors) {       System.Console.Error.Write (\"Errors from vendor: \" + rateQuote.CapacityProviderAccountGroup.Account.Code);       foreach (var error in errors) {        System.Console.Error.Write (error);       }      }       // Check for any informational/warning messages returned by the capacity provider      var infos = rateQuote.InfoMessages;      if (null != infos) {       System.Console.Error.Write (\"Information from vendor: \" + rateQuote.CapacityProviderAccountGroup.Account.Code);       foreach (var info in infos) {        System.Console.Write (info);       }      }       var total = rateQuote.RateQuoteDetail.Total;      System.Console.Write (\"Quote total from vendor: \" + rateQuote.CapacityProviderAccountGroup.Account.Code + \" = \" + total);      // ... more result processing ...     }    } catch (ApiException e) {     System.Console.Error.Write (\"Exception when calling QuotesApi#queryRateQuotes : \" + e.ErrorContent);     System.Console.Error.WriteLine ();     System.Console.Error.Write (\"HTTP STATUS : \" + e.ErrorCode);    }    ``` 
 *
 * OpenAPI spec version: 2.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;



using P44SDK.V4.Client;
using P44SDK.V4.Api;
using P44SDK.V4.Model;

namespace P44SDK.V4.Example
{



	public class CapacityProviderAccountGroupApiExample : IApiExample
	{


		//<summary>
		// This method makes an request to the project44 CapacityProviderAccountGroupManagement API and prints the results to the console.
		//</summary>
		public void execute(string username,
			string password,
			string hostname){

			// Intitialize the CapacityProviderAccountGroupManagementApi client, passing in the appropriate hostname
			var apiClient = new CapacityProviderAccountGroupManagementApi (hostname);
			apiClient.Configuration.Username = username;
			apiClient.Configuration.Password = password;
			apiClient.Configuration.AddDefaultHeader( "Content-Type", "application/json" );

			try {
				// Create
				// Initialize the info for the account group that's going to be created
				// Name: ExampleGroup Code: ExampleGroup
				var accountGroupInfo = new CapacityProviderAccountGroupInfo("ExampleGroup", "ExampleGroup");

				Console.WriteLine( "Invoking Create on CapacityProviderAccountGroup API..." );
				var createResult = apiClient.CreateCapacityProviderAccountGroupInfo ( accountGroupInfo );
				writeAccountGroupInfo( createResult );

				// Query
				// Get the full list of account groups that are available for use
				Console.WriteLine("");
				Console.WriteLine( "Invoking Query on CapacityProviderAccountGroup API..." );
				var queryResult = apiClient.QueryCapacityProviderAccountGroup();

				foreach( var accountGroup in queryResult.Groups ) {
					writeAccountGroupInfo( accountGroup );
				}
				Console.WriteLine("");

				// Delete
				// Delete the group that was created above
				Console.WriteLine( "Invoking Delete on CapacityProviderAccountGroup API..." );
				apiClient.DeleteCapacityProviderAccountGroupInfo( createResult.Id );

				Console.WriteLine( "Done!" );
			}
			catch(ApiException ex){

				Console.Write (ex.ToString());
			}

		}

		private void writeAccountGroupInfo( CapacityProviderAccountGroupInfo accountGroupInfo){
			
			var name = accountGroupInfo.Name;
			var code = accountGroupInfo.Code;
			var id = accountGroupInfo.Id;
			Console.WriteLine("");
			Console.WriteLine( "Account group created with: ");
			Console.WriteLine( "Name: " + name);
			Console.WriteLine( "Code: " + code);
			Console.WriteLine( "Id: " + id);

		}
	}
}
