# P44SDK.V4.Model.TruckloadShipmentStatus
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Shipment** | [**TruckloadShipment**](TruckloadShipment.md) | The shipment to which this status applies, including project44-calculated shipment stop details and the project44 id of the shipment. | [optional] 
**LatestStatusUpdate** | [**TruckloadShipmentStatusUpdate**](TruckloadShipmentStatusUpdate.md) | The most recent shipment status update available. | [optional] 
**StatusUpdates** | [**List&lt;TruckloadShipmentStatusUpdate&gt;**](TruckloadShipmentStatusUpdate.md) | If requested, all available shipment status updates. | [optional] 
**LatestStopStatuses** | [**List&lt;TruckloadShipmentStopStatus&gt;**](TruckloadShipmentStopStatus.md) | The most up-to-date statuses of the vehicle in relation to each stop. One and only one status will always be returned for each stop. If the shipment does not have status &#39;COMPLETED&#39; or &#39;DISPATCHED&#39;, this list will always contain one and only one stop status with a code of either &#39;EN_ROUTE&#39; or &#39;ARRIVED&#39;. | [optional] 
**MapUrl** | **string** | Publicly accessible URL to the shipment details page. | [optional] 
**InfoMessages** | [**List&lt;Message&gt;**](Message.md) | System messages and messages from the capacity provider with severity &#39;INFO&#39; or &#39;WARNING&#39;. No messages with severity &#39;ERROR&#39; will be returned here. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

