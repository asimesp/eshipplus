# P44SDK.V4.Model.CapacityProviderAccountInfos
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | The code identifier for the account set. This must be unique to the capacity provider account group this account set resides in. | 
**CapacityProviderIdentifier** | [**CapacityProviderIdentifier**](CapacityProviderIdentifier.md) | The capacity provider id. This is usually the SCAC for carriers and some logical project44-created identifier for brokers, as defined in the capacity provider metadata. | 
**Group** | [**CapacityProviderAccountGroupInfo**](CapacityProviderAccountGroupInfo.md) | The group to which the set belongs. If no group is provided, the new capacity provider account will be added to the \&quot;Default\&quot; group, which will be reflected in the response. | [optional] 
**Accounts** | [**List&lt;CapacityProviderAccountInfo&gt;**](CapacityProviderAccountInfo.md) | Only one account for each of the capacity provider&#39;s account definitions may be provided. On update, accounts in this list may reference ids of existing accounts or may be completely new accounts without ids if replacement is desired. | 
**Id** | **long?** | The project44-generated id of the capacity provider account set. This id may not be provided when creating a capacity provider account set, but it is required when updating one. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

