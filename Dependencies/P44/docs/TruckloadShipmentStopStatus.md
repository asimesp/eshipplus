# P44SDK.V4.Model.TruckloadShipmentStopStatus
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StopNumber** | **int?** | The number of the shipment stop to which this status pertains. | [optional] 
**StatusCode** | **string** | The status of the vehicle relative to this stop - - whether it is presumed to be currently en route to the stop or whether it has been recorded as having arrived or departed the stop. | [optional] 
**ArrivalEstimate** | [**TruckloadArrivalEstimate**](TruckloadArrivalEstimate.md) | Estimate of when the vehicle will arrive at this stop. The startDateTime is the earliest the vehicle could arrive barring any unknown delays. Only computed if the vehicle has not yet arrived. | [optional] 
**ArrivalCode** | **string** | The approximate arrival status of the vehicle at the stop, as recorded. The arrival status is relative to the user-defined appointment window for the stop. | [optional] 
**ArrivalDateTime** | **string** | The recorded arrival time of the vehicle at this stop, if available. This time will always be in the time zone of the stop, and a time zone offset will always be provided (format: yyyy-MM-dd&#39;T&#39;HH:mm:ssZ). | [optional] 
**DepartureDateTime** | **string** | The recorded departure time of the vehicle from this stop, if available. This time will always be in the time zone of the stop, and a time zone offset will always be provided (format: yyyy-MM-dd&#39;T&#39;HH:mm:ssZ). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

