# P44SDK.V4.Model.LtlImageQuery
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CapacityProviderAccountGroup** | [**CapacityProviderAccountGroup**](CapacityProviderAccountGroup.md) | Capacity provider account group, containing the account to be used for authentication with the capacity provider&#39;s imaging API. | 
**ProNumber** | **string** | PRO number identifying the shipment related to the requested image. | 
**DestinationAddress** | [**Address**](Address.md) | The destination address of the shipment related to the requested image. Postal code and country code are necessary for some capacity providers to authorize image access or look up the image. | 
**DocumentType** | **string** | A code for the requested image&#39;s document type. | 
**ImageFormat** | **string** | A code for the format of the image to be returned. The capacity provider&#39;s image will be converted to this format. (default: &#39;PNG&#39;) | [optional] 
**ApiConfiguration** | [**ImageApiConfiguration**](ImageApiConfiguration.md) | Fields for configuring the behavior of this API. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

