# P44SDK.V4.Model.ParcelShipmentIdentifier
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | Parcel shipment identifier type. | 
**Value** | **string** | Parcel shipment identifier type. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

