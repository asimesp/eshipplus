# P44SDK.V4.Model.TruckloadShipmentStatusReason
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | The reason code for the status of the status update. A status of &#39;DISPATCHED&#39; will have one of the following reason codes: &#39;SCHEDULED&#39;, &#39;PENDING_APPROVAL&#39;, or &#39;ACQUIRING_LOCATION&#39;. A status of &#39;IN_TRANSIT&#39; will have one of the following reason codes: &#39;IN_MOTION&#39; or &#39;IDLE&#39;. A status of &#39;AT_STOP&#39; will not have a reason. A status of &#39;COMPLETED&#39; will have one of the following reason codes: &#39;APPROVAL_DENIED&#39;, &#39;TIMED_OUT&#39;, &#39;CANCELED&#39;, or &#39;DEPARTED_FINAL_STOP&#39;. | [optional] 
**Description** | **string** | A description of the reason for the status of the status update. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

