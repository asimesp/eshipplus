# P44SDK.V4.Model.RequestedAccessorialService
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | The code for the requested accessorial service. A list of accessorial service codes supported by project44 is provided in the API reference data section. | [optional] 
**Status** | **string** | The status of the requested accessorial service - - whether or not it is reflected in the total. An outcome of &#39;ACCEPTED&#39; means the capacity provider accepted the requested accessorial service, included it in their quote total, and provided a charge breakdown. An outcome of &#39;ACCEPTED_UNITEMIZED&#39; means the capacity provider most likely accepted the requested accessorial service and included it in their quote total, but did not provide a verifiable charge breakdown. An outcome of &#39;UNACCEPTED&#39; means the capacity provider did not accept the requested accessorial and it is not reflected in the quote total. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

