# P44SDK.V4.Model.LtlShipmentArrivalEstimate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EstimatedArrivalWindow** | [**SplitOffsetDateTimeWindow**](SplitOffsetDateTimeWindow.md) | The approximate arrival window of the shipment or vehicle. These times will always be in the time zone of the applicable location when possible. If the location is unknown, the time zone will be UTC. Unfortunately, given limited information from capacity providers, the times in this field may be null, may consist of dates only, or may consist of dates and times only with no time zones. If no time zones are provided, these times should be assumed local to the applicable location. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

