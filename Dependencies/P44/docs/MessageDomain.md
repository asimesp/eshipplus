# P44SDK.V4.Model.MessageDomain
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Diagnostic** | **string** |  | [optional] 
**Message** | **string** |  | [optional] 
**Severity** | **string** |  | [optional] 
**Source** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

