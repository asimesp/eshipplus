# P44SDK.V4.Model.ApiError
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HttpStatusCode** | **int?** | The value of the HTTP status code. | [optional] 
**HttpMessage** | **string** | Description of the HTTP status code. | [optional] 
**ErrorMessage** | **string** | Description of the error. | [optional] 
**Errors** | [**List&lt;Message&gt;**](Message.md) | (Optional) Collection of Message objects which provide further information as to the cause of this error. | [optional] 
**SupportReferenceId** | **string** | A reference identifier used by project44 support to assist with certain error messages. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

