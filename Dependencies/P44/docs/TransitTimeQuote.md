# P44SDK.V4.Model.TransitTimeQuote
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CapacityProviderAccountGroup** | [**CapacityProviderAccountGroup**](CapacityProviderAccountGroup.md) | The requested capacity provider account group containing the account that was used for authentication with the capacity provider&#39;s rate quote API. | [optional] 
**CarrierCode** | **string** | SCAC of the carrier to which this rate quote applies. This is used to differentiate among rate quotes from capacity providers that support multiple SCACs. | [optional] 
**TransitDays** | **int?** | The number of service days to deliver the shipment being quoted after it is picked up. | [optional] 
**DeliveryDateTime** | **DateTime?** | The delivery date and time, if provided. (format: yyyy-MM-dd&#39;T&#39;HH:mm:ss) | [optional] 
**InfoMessages** | [**List&lt;Message&gt;**](Message.md) | System messages and messages from the capacity provider with severity &#39;INFO&#39; or &#39;WARNING&#39;. No messages with severity &#39;ERROR&#39; will be returned here. | [optional] 
**ErrorMessages** | [**List&lt;Message&gt;**](Message.md) | System messages and messages from the capacity provider with severity &#39;ERROR&#39;. No messages with severity &#39;INFO&#39; or &#39;WARNING&#39; will be returned here. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

