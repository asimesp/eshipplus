# P44SDK.V4.Model.ParcelShipmentStatusUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StatusCode** | **string** | The status code for this status update. This field will always be populated. | [optional] 
**StatusReason** | [**ParcelShipmentStatusReason**](ParcelShipmentStatusReason.md) | Deeper detail as to why the current status code is what it is. | [optional] 
**Timestamp** | **string** | The datetime the carrier provided signifying when a status event occurred. | [optional] 
**RetrievalDateTime** | **string** | The dateTime that p44 received the status event from the carrier. | [optional] 
**Address** | [**Address**](Address.md) | Location information of where the status event took place. | [optional] 
**StopType** | **string** | The type of stop that the status event occurred at. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

