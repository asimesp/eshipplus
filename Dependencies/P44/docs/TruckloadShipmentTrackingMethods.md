# P44SDK.V4.Model.TruckloadShipmentTrackingMethods
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TrackingMethods** | [**List&lt;TruckloadShipmentTrackingMethod&gt;**](TruckloadShipmentTrackingMethod.md) | An ordered list of tracking methods that can be used to track a shipment, with the most robust method available listed first. When creating a shipment, provide as many shipment identifiers and equipment identifiers as possible that match the ones in the tracking methods in this list, preferring ones first in the list over others. Each tracking method will contain either a shipment identifier or an equipment identifier, or both. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

