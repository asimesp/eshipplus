# P44SDK.V4.Model.RailLocation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Address** | [**RailAddress**](RailAddress.md) | Address of the location. | 
**Id** | **string** | The project44-generated id for this location. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

