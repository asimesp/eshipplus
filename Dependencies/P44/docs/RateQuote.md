# P44SDK.V4.Model.RateQuote
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CapacityProviderAccountGroup** | [**CapacityProviderAccountGroup**](CapacityProviderAccountGroup.md) | The requested capacity provider account group containing the account that was used for authentication with the capacity provider&#39;s rate quote API. | [optional] 
**CarrierCode** | **string** | SCAC of the carrier to which this rate quote applies. This is used to differentiate among rate quotes from capacity providers that support multiple SCACs. | [optional] 
**ContractId** | **string** | This field is returned only for rate quotes from some brokers. It identifies the contract between the broker and capacity provider used to generate this rate quote. This can be used by other brokers or customers to gain access to that rate with the same capacity provider. | [optional] 
**CapacityProviderQuoteNumber** | **string** | The capacity provider&#39;s identifier for this rate quote, if provided. | [optional] 
**ServiceLevel** | [**ServiceLevel**](ServiceLevel.md) | The service level (e.g., standard or guaranteed) of this rate quote. | [optional] 
**TransitDays** | **int?** | The number of service days to deliver the shipment being quoted after it is picked up, if provided. | [optional] 
**DeliveryDateTime** | **DateTime?** | The delivery date and time for this rate quote in the timezone of the destination location, if provided. (format: yyyy-MM-dd&#39;T&#39;HH:mm:ss) | [optional] 
**CurrencyCode** | **string** | The currency code for all monetary values in this rate quote. | [optional] 
**RateQuoteDetail** | [**RateQuoteDetail**](RateQuoteDetail.md) | Pricing details for this rate quote, including the total, subtotal, and a breakdown of charges, if provided. | [optional] 
**AlternateRateQuotes** | [**List&lt;AlternateRateQuote&gt;**](AlternateRateQuote.md) | A list of alternate rate quotes from this capacity provider, if provided. These are typically rate quotes for other service levels the provider offers (e.g., guaranteed service levels). | [optional] 
**LaneType** | **string** | Whether the shipment being quoted will stay within the carrier&#39;s network (&#39;DIRECT&#39; for a direct lane) or be tendered to another carrier before reaching its destination (&#39;INTERLINE&#39; for an interline lane), if indicated. (default: &#39;UNSPECIFIED&#39;) | [optional] 
**RequestedAccessorialServices** | [**List&lt;RequestedAccessorialService&gt;**](RequestedAccessorialService.md) | A list of all accessorial services that were requested and their outcomes in this rate quote. | [optional] 
**QuoteEffectiveDateTime** | **DateTime?** | The effective date and time for this quote, if provided.(format: yyyy-MM-dd&#39;T&#39;HH:mm:ss) | [optional] 
**QuoteExpirationDateTime** | **DateTime?** | The date and time this quote expires with the capacity provider, if provided. (format:yyyy-MM-dd&#39;T&#39;HH:mm:ss) | [optional] 
**InfoMessages** | [**List&lt;Message&gt;**](Message.md) | System messages and messages from the capacity provider with severity &#39;INFO&#39; or &#39;WARNING&#39;. No messages with severity &#39;ERROR&#39; will be returned here. | [optional] 
**ErrorMessages** | [**List&lt;Message&gt;**](Message.md) | System messages and messages from the capacity provider with severity &#39;ERROR&#39;. No messages with severity &#39;INFO&#39; or &#39;WARNING&#39; will be returned here. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

