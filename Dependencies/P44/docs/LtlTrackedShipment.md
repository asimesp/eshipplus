# P44SDK.V4.Model.LtlTrackedShipment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CapacityProviderAccountGroup** | [**CapacityProviderAccountGroup**](CapacityProviderAccountGroup.md) | Capacity provider account group, containing the account to be used for authentication with the capacity provider&#39;s LTL tracking API. One and only one capacity provider account must be provided. | 
**ShipmentIdentifiers** | [**List&lt;LtlShipmentIdentifier&gt;**](LtlShipmentIdentifier.md) | A user-defined list of shipment identifiers that, with the exception of the EXTERNAL type, can be used to track the shipment with the capacity provider. | 
**ShipmentStops** | [**List&lt;LtlTrackedShipmentStop&gt;**](LtlTrackedShipmentStop.md) | A list of the shipment stops for the shipment. Two and only two stops must be provided - - one of type &#39;ORIGIN&#39; and one of type &#39;DESTINATION&#39;. Stops of type &#39;TERMINAL&#39; may be added as the shipment is tracked and they are discovered via capacity provider information.  Note that &#39;TERMINAL&#39; will be ignored if provided as part of a PUT. | 
**ApiConfiguration** | [**LtlTrackedShipmentApiConfiguration**](LtlTrackedShipmentApiConfiguration.md) | Fields for configuring the behavior of this API. | [optional] 
**Id** | **long?** | The project44-generated id of the LTL shipment. This may be provided when creating a tracked shipment only if a tracked shipment does not exist with this id but a shipment record does exist in project44&#39;s system with this id. This enables linking a dispatched shipment that initially failed to track to its tracking information by passing the id from dispatch. This id will always be returned in the shipment confirmation and may be used in subsequent API calls to reference a shipment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

