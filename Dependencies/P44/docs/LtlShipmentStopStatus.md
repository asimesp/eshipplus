# P44SDK.V4.Model.LtlShipmentStopStatus
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StopType** | **string** | The type of the shipment stop to which this status pertains. | [optional] 
**StopId** | **long?** | The id of the shipment stop to which this status pertains. | [optional] 
**StopNumber** | **int?** | The number of the shipment stop to which this status pertains. | [optional] 
**StatusCode** | **string** | The status of the vehicle relative to this stop - - whether it is presumed to be currently en route to the stop or whether it has been recorded as having arrived or departed the stop. | [optional] 
**ArrivalEstimate** | [**LtlShipmentArrivalEstimate**](LtlShipmentArrivalEstimate.md) | Estimate of when the vehicle will arrive at this stop. The startDateTime is the earliest the vehicle could arrive barring any unknown delays. Only computed if the vehicle has not yet arrived. | [optional] 
**ArrivalCode** | **string** | The approximate arrival status of the vehicle at the stop, as recorded. The arrival status is relative to the user-defined appointment window for the stop. | [optional] 
**ArrivalDateTime** | [**SplitOffsetDateTime**](SplitOffsetDateTime.md) | The recorded arrival time of the vehicle at this stop, if available. This time will always be in the time zone of the stop when possible. If the stop&#39;s locations is unknown, the time zone will be UTC. Unfortunately, given limited information from capacity providers, this field may be null, may consist of a date only, or may consist of a date and time only with no time zone. If no time zone is provided, this time should be assumed local to the location of the stop. | [optional] 
**DepartureDateTime** | [**SplitOffsetDateTime**](SplitOffsetDateTime.md) | The recorded departure time of the vehicle at this stop, if available. This time will always be in the time zone of the stop when possible. If the stop&#39;s locations is unknown, the time zone will be UTC. Unfortunately, given limited information from capacity providers, this field may be null, may consist of a date only, or may consist of a date and time only with no time zone. If no time zone is provided, this time should be assumed local to the location of the stop. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

