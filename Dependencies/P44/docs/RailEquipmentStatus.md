# P44SDK.V4.Model.RailEquipmentStatus
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Equipment** | [**RailEquipment**](RailEquipment.md) | The equipment to which this status applies | [optional] 
**LatestStatusUpdate** | [**RailEquipmentStatusUpdate**](RailEquipmentStatusUpdate.md) | The most recent equipment status update available. | [optional] 
**StatusUpdates** | [**List&lt;RailEquipmentStatusUpdate&gt;**](RailEquipmentStatusUpdate.md) | If requested, all available equipment status updates. | [optional] 
**LatestStopStatuses** | [**List&lt;RailEquipmentStopStatus&gt;**](RailEquipmentStopStatus.md) | The most up-to-date statuses of the equipment in relation to each stop. One and only one status will always be returned for each stop. If the equipment does not have status &#39;COMPLETED&#39; or &#39;DISPATCHED&#39;, this list will always contain one and only one stop status with a code of either &#39;EN_ROUTE&#39; or &#39;ARRIVED&#39;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

