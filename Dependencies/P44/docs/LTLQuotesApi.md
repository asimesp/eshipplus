# P44SDK.V4.Api.LTLQuotesApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**QueryRateQuotes**](LTLQuotesApi.md#queryratequotes) | **POST** /api/v4/ltl/quotes/rates/query | POST: Query for rate quotes.
[**QueryTransitTimeQuotes**](LTLQuotesApi.md#querytransittimequotes) | **POST** /api/v4/ltl/quotes/transittimes/query | POST: Query for transit time quotes.


<a name="queryratequotes"></a>
# **QueryRateQuotes**
> RateQuoteCollection QueryRateQuotes (RateQuoteQuery rateQuoteQuery)

POST: Query for rate quotes.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class QueryRateQuotesExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LTLQuotesApi();
            var rateQuoteQuery = new RateQuoteQuery(); // RateQuoteQuery | rateQuoteQuery

            try
            {
                // POST: Query for rate quotes.
                RateQuoteCollection result = apiInstance.QueryRateQuotes(rateQuoteQuery);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LTLQuotesApi.QueryRateQuotes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rateQuoteQuery** | [**RateQuoteQuery**](RateQuoteQuery.md)| rateQuoteQuery | 

### Return type

[**RateQuoteCollection**](RateQuoteCollection.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="querytransittimequotes"></a>
# **QueryTransitTimeQuotes**
> TransitTimeQuoteCollection QueryTransitTimeQuotes (TransitTimeQuoteQuery transitTimeQuoteQuery)

POST: Query for transit time quotes.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class QueryTransitTimeQuotesExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LTLQuotesApi();
            var transitTimeQuoteQuery = new TransitTimeQuoteQuery(); // TransitTimeQuoteQuery | transitTimeQuoteQuery

            try
            {
                // POST: Query for transit time quotes.
                TransitTimeQuoteCollection result = apiInstance.QueryTransitTimeQuotes(transitTimeQuoteQuery);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LTLQuotesApi.QueryTransitTimeQuotes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transitTimeQuoteQuery** | [**TransitTimeQuoteQuery**](TransitTimeQuoteQuery.md)| transitTimeQuoteQuery | 

### Return type

[**TransitTimeQuoteCollection**](TransitTimeQuoteCollection.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

