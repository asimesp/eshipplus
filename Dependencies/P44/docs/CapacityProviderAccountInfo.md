# P44SDK.V4.Model.CapacityProviderAccountInfo
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountDefinitionIdentifier** | **string** | Capacity provider account definition identifier is used to distinguish capacity provider accounts when a specific capacity provider has more then one account definition for different services. | 
**Username1** | **string** | The username of the account, if this capacity provider account type has a username. | [optional] 
**Credential1** | **string** | The first password, authentication key, or other credentials with the capacity provider, if this capacity provider account type has one of these.  This field will not appear in any response object. | [optional] 
**Credential2** | **string** | The second password, authentication key, or other credentials with the capacity provider, if this capacity provider account type has one of these. | [optional] 
**Credential3** | **string** | The third password, authentication key, or other credentials with the capacity provider, if this capacity provider account type has one of these. | [optional] 
**AccountNumber1** | **string** | The first account number with the capacity provider if this capacity provider account type has account numbers. This may be a bill-to account number, a shipper account number, a consignee account number, or some other account number. | [optional] 
**AccountNumber2** | **string** | The second account number with the capacity provider if this capacity provider account type has account numbers. This may be a bill-to account number, a shipper account number, a consignee account number, or some other account number. | [optional] 
**AccountFlag1** | **bool?** | A boolean flag related to the your account with the capacity provider, defined by the applicable capacity provider account definition (e.g., for UPGF, this is &#39;enable density-based rating&#39;). | [optional] 
**DirectionCode** | **string** | The direction or shipment terms of the your account with the capacity provider - - the party responsible for the shipment. | [optional] 
**PaymentTermsCode** | **string** | The payment terms of the customer&#39;s account with the capacity provider - - the party responsible for payment. | [optional] 
**EnableDirectionOverride** | **bool?** | Whether or not direction for this capacity provider account can be overridden in an API request. The direction for some capacity provider accounts should never be overridden, while others need to be changed depending on the origin and destination of a shipment or other factors.  Whether this override should be enabled depends primarily on the capacity provider. (default: &#39;false&#39;) | [optional] 
**EnablePaymentTermsOverride** | **bool?** | Whether or not payment terms for this capacity provider account can be overridden in an API request. The payment terms for some capacity provider accounts should never be overridden, while others need to be changed depending on the origin and destination of a shipment or other factors.  Whether this override is enabled depends primarily on the capacity provider. (default: &#39;false&#39;) | [optional] 
**BillToLocationId** | **string** | The project44-generated id of the bill-to location. Locations can be created, updated, and retrieved through the location management API. | [optional] 
**Id** | **long?** | The project44-generated id of the capacity provider account. This id may not be provided when creating a capacity provider account, but it is required when updating one. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

