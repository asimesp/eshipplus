# P44SDK.V4.Model.ProNumberIdentifier
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProNumber** | **string** | The generated ProNumber. | [optional] 
**VendorId** | **string** | The name of the vendor for which the ProNumber was generated. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

