# P44SDK.V4.Model.TruckloadShipmentStop
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StopNumber** | **int?** | The user-defined stop number, where &#39;1&#39; is for the origin and the destination has the largest number, with any stops inbetween ordered by appointment time. | 
**AppointmentWindow** | [**ZonedDateTimeWindow**](ZonedDateTimeWindow.md) | The user-defined appointment window of this stop - - i.e., the shipment&#39;s expected window of arrival at this stop. This may be used to report whether a shipment is on time to a stop. The start and end times are required for &#39;appointmentWindow&#39; - - the default start time does not apply to this field. The time zone identifier (e.g., &#39;America/Chicago&#39;) for this date time window is an optional field. If not provided the system will default to the timezone local to the stop. Please note, we will always report back the timezone for the appointment window as local to the stop. | 
**Location** | [**Location**](Location.md) | The user-defined location of this stop, including a contact and an optional address. | 
**StopName** | **string** | The optionally user-defined name of this stop. This is a freeform field. Project44 will default the name of each stop based on the stop number, if no name is provided. | [optional] 
**GeoCoordinates** | [**GeoCoordinates**](GeoCoordinates.md) | The latitude and longitude of this stop, determined by geocoding the user-defined address. This field may not be provided when creating a shipment, but will always be returned. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

