# P44SDK.V4.Model.ShipmentApiConfiguration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NoteConfiguration** | [**ShipmentNoteConfiguration**](ShipmentNoteConfiguration.md) | Configuration of the pickup note that will be constructed by project44. Pickup note construction is used to send some requested accessorial services to capacity providers through their API when no other API field is available for these services. It is also used to send the delivery note through the capacity provider&#39;s pickup note API field. If no note configuration is provided, the default note configuration will be used, which will send the following note sections in order and will not enable truncation: pickup note, priority accessorials, pickup accessorials, dimensions, delivery note, delivery accessorials, and other accessorials. | [optional] 
**AllowUnsupportedAccessorials** | **bool?** | If set to &#39;true&#39;, accessorial services that are not known to be supported by the capacity provider will be allowed and will be sent through the capacity provider&#39;s pickup note API field, according to the shipment note configuration. This is useful when the customer knows that a capacity provider supports an accessorial service that they have not documented, or when the customer has a special agreement with the capacity provider. (default: &#39;false&#39;) | [optional] 
**EnableUnitConversion** | **bool?** | If set to &#39;true&#39;, weight and length values in this shipment request will be converted when necessary to the capacity provider&#39;s supported units. When &#39;false&#39;, an error will be returned and the shipment will not be placed with the capacity provider if the capacity provider does not support the provided weight and length units. (default: &#39;false&#39;) | [optional] 
**FallBackToDefaultAccountGroup** | **bool?** | If set to &#39;true&#39; and the provided capacity provider account group code is invalid, the default capacity provider account group will be used. When &#39;false&#39;, an error will be returned if the provided capacity provider account group code is invalid. (default: &#39;false&#39;) | [optional] 
**PreScheduledPickup** | **bool?** | If set to &#39;true&#39;, will identify the pickup for this shipment as being already scheduled, and will only transmit BOL information to the carrier. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

