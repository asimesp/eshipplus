# P44SDK.V4.Model.ParcelShipmentStatus
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LatestStatusUpdate** | [**ParcelShipmentStatusUpdate**](ParcelShipmentStatusUpdate.md) |  | [optional] 
**Shipment** | [**ParcelShipmentState**](ParcelShipmentState.md) |  | [optional] 
**StatusUpdates** | [**List&lt;ParcelShipmentStatusUpdate&gt;**](ParcelShipmentStatusUpdate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

