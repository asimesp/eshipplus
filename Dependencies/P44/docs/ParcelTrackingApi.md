# P44SDK.V4.Api.ParcelTrackingApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateShipment**](ParcelTrackingApi.md#createshipment) | **POST** /api/v4/parcel/shipments | POST: Initialize parcel shipment for tracking.
[**DeleteShipment**](ParcelTrackingApi.md#deleteshipment) | **DELETE** /api/v4/parcel/shipments/{id} | DELETE: Delete an existing parcel shipment using the unique system id.
[**ParcelStatusById**](ParcelTrackingApi.md#parcelstatusbyid) | **GET** /api/v4/parcel/shipments/{id}/statuses | GET: Get the status of a parcel shipment using the unique system id.


<a name="createshipment"></a>
# **CreateShipment**
> ParcelShipmentConfirmation CreateShipment (ParcelShipment parcelShipment)

POST: Initialize parcel shipment for tracking.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new ParcelTrackingApi();
            var parcelShipment = new ParcelShipment(); // ParcelShipment | parcelShipment

            try
            {
                // POST: Initialize parcel shipment for tracking.
                ParcelShipmentConfirmation result = apiInstance.CreateShipment(parcelShipment);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParcelTrackingApi.CreateShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parcelShipment** | [**ParcelShipment**](ParcelShipment.md)| parcelShipment | 

### Return type

[**ParcelShipmentConfirmation**](ParcelShipmentConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteshipment"></a>
# **DeleteShipment**
> ParcelShipmentDeleteConfirmation DeleteShipment (long? id)

DELETE: Delete an existing parcel shipment using the unique system id.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class DeleteShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new ParcelTrackingApi();
            var id = 789;  // long? | id

            try
            {
                // DELETE: Delete an existing parcel shipment using the unique system id.
                ParcelShipmentDeleteConfirmation result = apiInstance.DeleteShipment(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParcelTrackingApi.DeleteShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| id | 

### Return type

[**ParcelShipmentDeleteConfirmation**](ParcelShipmentDeleteConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="parcelstatusbyid"></a>
# **ParcelStatusById**
> ParcelShipmentStatus ParcelStatusById (long? id, bool? includeStatusHistory = null)

GET: Get the status of a parcel shipment using the unique system id.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class ParcelStatusByIdExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new ParcelTrackingApi();
            var id = 789;  // long? | id
            var includeStatusHistory = true;  // bool? | includeStatusHistory (optional) 

            try
            {
                // GET: Get the status of a parcel shipment using the unique system id.
                ParcelShipmentStatus result = apiInstance.ParcelStatusById(id, includeStatusHistory);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParcelTrackingApi.ParcelStatusById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| id | 
 **includeStatusHistory** | **bool?**| includeStatusHistory | [optional] 

### Return type

[**ParcelShipmentStatus**](ParcelShipmentStatus.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

