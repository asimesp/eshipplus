# P44SDK.V4.Model.ShipmentIdentifier
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | The type of this shipment identifier or reference number. | 
**Value** | **string** | The value of this shipment identifier or reference number. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

