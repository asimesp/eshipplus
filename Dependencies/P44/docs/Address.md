# P44SDK.V4.Model.Address
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PostalCode** | **string** | The ZIP or postal code. US ZIP codes must have exactly 5 digits. Spaces are not accepted. | 
**AddressLines** | **List&lt;string&gt;** | Street name, number, direction, PO box, etc. Only three address lines are currently permitted. | [optional] 
**City** | **string** | Name of city, town, etc. | [optional] 
**State** | **string** | Abbreviation of state, province, district, etc. | [optional] 
**Country** | **string** | Abbreviation of country (using ISO 3166 standards). (default: &#39;US&#39;) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

