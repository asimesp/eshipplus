# P44SDK.V4.Model.RailEquipmentStatusUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Timestamp** | **string** | The timestamp of this status update in the time zone of the location. If this status update has no location, this timestamp will be in UTC. A time zone offset will always be provided. This field will always be populated. | [optional] 
**StatusCode** | **string** | The status code for this status update. This field will always be populated. | [optional] 
**StatusReason** | [**RailStatusReason**](RailStatusReason.md) | The reason for the status, providing an additional level of detail for the status. This field will not be provided if no additional detail is available. | [optional] 
**GeoCoordinates** | [**GeoCoordinates**](GeoCoordinates.md) | The approximate latitude and longitude of the shipment&#39;s location at the time of this status update, if available. | [optional] 
**Address** | [**RailAddress**](RailAddress.md) | The approximate address of the shipment&#39;s location at the time of this status update, if available. | [optional] 
**StopNumber** | **int?** | The stop number to which this status update corresponds, if any. This will be populated if the location of this update matches the location for a stop. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

