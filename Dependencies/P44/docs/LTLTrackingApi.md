# P44SDK.V4.Api.LTLTrackingApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateShipment**](LTLTrackingApi.md#createshipment) | **POST** /api/v4/ltl/trackedshipments | POST: Initialize an LTL shipment for tracking.
[**GetShipmentStatus**](LTLTrackingApi.md#getshipmentstatus) | **GET** /api/v4/ltl/trackedshipments/{id}/statuses | GET: Get the status of an LTL shipment using the unique system id.
[**GetShipmentStatusesByIdentifier**](LTLTrackingApi.md#getshipmentstatusesbyidentifier) | **GET** /api/v4/ltl/trackedshipments/statuses | GET: Find shipments by identifier and get status
[**UpdateShipment**](LTLTrackingApi.md#updateshipment) | **PUT** /api/v4/ltl/trackedshipments/{id} | PUT: Update a shipment.


<a name="createshipment"></a>
# **CreateShipment**
> LtlTrackedShipmentConfirmation CreateShipment (LtlTrackedShipment shipment)

POST: Initialize an LTL shipment for tracking.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LTLTrackingApi();
            var shipment = new LtlTrackedShipment(); // LtlTrackedShipment | shipment

            try
            {
                // POST: Initialize an LTL shipment for tracking.
                LtlTrackedShipmentConfirmation result = apiInstance.CreateShipment(shipment);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LTLTrackingApi.CreateShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipment** | [**LtlTrackedShipment**](LtlTrackedShipment.md)| shipment | 

### Return type

[**LtlTrackedShipmentConfirmation**](LtlTrackedShipmentConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getshipmentstatus"></a>
# **GetShipmentStatus**
> LtlShipmentStatus GetShipmentStatus (long? id, bool? includeStatusHistory = null)

GET: Get the status of an LTL shipment using the unique system id.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GetShipmentStatusExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LTLTrackingApi();
            var id = 789;  // long? | The project44-generated id of the LTL tracked or dispatched shipment being queried.
            var includeStatusHistory = true;  // bool? | Whether shipment status history ('statusUpdates') should be included in the response. (optional)  (default to false)

            try
            {
                // GET: Get the status of an LTL shipment using the unique system id.
                LtlShipmentStatus result = apiInstance.GetShipmentStatus(id, includeStatusHistory);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LTLTrackingApi.GetShipmentStatus: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| The project44-generated id of the LTL tracked or dispatched shipment being queried. | 
 **includeStatusHistory** | **bool?**| Whether shipment status history (&#39;statusUpdates&#39;) should be included in the response. | [optional] [default to false]

### Return type

[**LtlShipmentStatus**](LtlShipmentStatus.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getshipmentstatusesbyidentifier"></a>
# **GetShipmentStatusesByIdentifier**
> LtlShipmentStatusCollection GetShipmentStatusesByIdentifier (string shipmentIdentifierType = null, string shipmentIdentifierValue = null, string shipmentIdentifierSource = null, bool? includeStatusHistory = null)

GET: Find shipments by identifier and get status

This may return multiple shipments if the identifier is used for multiple shipments such as with a pickup number.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GetShipmentStatusesByIdentifierExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LTLTrackingApi();
            var shipmentIdentifierType = shipmentIdentifierType_example;  // string | The type of the identifier to search for. (optional) 
            var shipmentIdentifierValue = shipmentIdentifierValue_example;  // string | The identifier value to search for. (optional) 
            var shipmentIdentifierSource = shipmentIdentifierSource_example;  // string | The source system which created the identifier. (optional) 
            var includeStatusHistory = true;  // bool? | Whether shipment status history ('statusUpdates') should be included in the response. (optional)  (default to false)

            try
            {
                // GET: Find shipments by identifier and get status
                LtlShipmentStatusCollection result = apiInstance.GetShipmentStatusesByIdentifier(shipmentIdentifierType, shipmentIdentifierValue, shipmentIdentifierSource, includeStatusHistory);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LTLTrackingApi.GetShipmentStatusesByIdentifier: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipmentIdentifierType** | **string**| The type of the identifier to search for. | [optional] 
 **shipmentIdentifierValue** | **string**| The identifier value to search for. | [optional] 
 **shipmentIdentifierSource** | **string**| The source system which created the identifier. | [optional] 
 **includeStatusHistory** | **bool?**| Whether shipment status history (&#39;statusUpdates&#39;) should be included in the response. | [optional] [default to false]

### Return type

[**LtlShipmentStatusCollection**](LtlShipmentStatusCollection.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateshipment"></a>
# **UpdateShipment**
> LtlTrackedShipmentConfirmation UpdateShipment (long? id, LtlTrackedShipment shipment)

PUT: Update a shipment.

This will delete the current shipment and create a new shipment to begin tracking on. This updates how we track the shipment but will not affect the dispatch with the carrier. You must pass the entire details for the shipment as you would on a POST.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class UpdateShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LTLTrackingApi();
            var id = 789;  // long? | The project44-generated id of the LTL tracked or dispatched shipment being queried.
            var shipment = new LtlTrackedShipment(); // LtlTrackedShipment | shipment

            try
            {
                // PUT: Update a shipment.
                LtlTrackedShipmentConfirmation result = apiInstance.UpdateShipment(id, shipment);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LTLTrackingApi.UpdateShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| The project44-generated id of the LTL tracked or dispatched shipment being queried. | 
 **shipment** | [**LtlTrackedShipment**](LtlTrackedShipment.md)| shipment | 

### Return type

[**LtlTrackedShipmentConfirmation**](LtlTrackedShipmentConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

