# P44SDK.V4.Model.TransitTimeQuoteApiConfiguration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Timeout** | **int?** | Number of seconds to wait for a capacity provider to return a transit time quote. (default: &#39;10&#39;) | [optional] 
**FallBackToDefaultAccountGroup** | **bool?** | If set to &#39;true&#39; and the provided capacity provider account group code is invalid, the default capacity provider account group will be used. When &#39;false&#39;, an error will be returned if the provided capacity provider account group code is invalid. (default: &#39;false&#39;) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

