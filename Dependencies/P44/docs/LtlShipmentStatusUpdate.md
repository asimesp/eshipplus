# P44SDK.V4.Model.LtlShipmentStatusUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Timestamp** | [**SplitOffsetDateTime**](SplitOffsetDateTime.md) | When available, the timestamp of this status update in the time zone of the location. If this status update has no location, the time zone will be UTC. Unfortunately, given limited information from capacity providers, this field may be null, may consist of a date only, or may consist of a date and time only with no time zone. If no time zone is provided, this time should be assumed local to the location of the shipment. | [optional] 
**RetrievalDateTime** | **string** | Date and time project44 retrieved this status update from the capacity provider in UTC. This does not represent the time this status was reported by the capacity provider. (format: yyyy-MM-dd&#39;T&#39;HH:mm:ssZ) | [optional] 
**StatusCode** | **string** | The project44 status code for this status update. This field will always be populated. | [optional] 
**StatusReason** | [**LtlShipmentStatusReason**](LtlShipmentStatusReason.md) | The reason for the status, providing additional details for the status. This field will not be provided if no additional detail is available. | [optional] 
**Address** | [**Address**](Address.md) | The approximate postal address of the shipment or vehicle&#39;s location at the time of this status update, if available. | [optional] 
**StopId** | **long?** | The id of the shipment stop to which this status update corresponds, if any. For example, a status update with code &#39;AT_STOP&#39; will always have this field populated with the stop id the vehicle is at. | [optional] 
**StopType** | **string** | The type of the shipment stop to which this status update corresponds, if any. For example, a status update with code &#39;AT_STOP&#39; will always have this field populated with the stop type the vehicle is at. | [optional] 
**StopNumber** | **int?** | The stop number to which this status update corresponds, if any. For example, a status update with code &#39;AT_STOP&#39; will always have this field populated with the stop number the vehicle is at. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

