# P44SDK.V4.Model.RailShipmentStatus
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Shipment** | [**RailShipment**](RailShipment.md) | The shipment to which this status applies, including project44-calculated shipment stop details and the project44 id of the shipment. | [optional] 
**EquipmentStatuses** | [**List&lt;RailEquipmentStatus&gt;**](RailEquipmentStatus.md) | Status for each piece of equipment attached to this shipment. | [optional] 
**InfoMessages** | [**List&lt;Message&gt;**](Message.md) | System messages and messages from the capacity provider with severity &#39;INFO&#39; or &#39;WARNING&#39;. No messages with severity &#39;ERROR&#39; will be returned here. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

