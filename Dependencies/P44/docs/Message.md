# P44SDK.V4.Model.Message
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Severity** | **string** | The severity of this message. | [optional] 
**_Message** | **string** | Message informational text. | [optional] 
**Diagnostic** | **string** | Diagnostic information, often originating from the capacity provider. | [optional] 
**Source** | **string** | The originator of this message - - either project44 (the system) or the capacity provider. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

