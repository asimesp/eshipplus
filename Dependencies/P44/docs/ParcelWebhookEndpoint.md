# P44SDK.V4.Model.ParcelWebhookEndpoint
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | **string** | URL to which a POST request will be made with the content of the status update for a specific parcel shipment. | 
**Username** | **string** | Specify username if endpoint requires authentication. | [optional] 
**Password** | **string** | Specify password if endpoint requires authentication. | [optional] 
**ShowLatestUpdate** | **bool?** | Separately show latest update | [optional] 
**IncludeFullHistory** | **bool?** | Include full update history | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

