# P44SDK.V4.Api.AddressValidationApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ValidateAddress**](AddressValidationApi.md#validateaddress) | **POST** /api/v4/address/validate | POST: Validate specified address.  If validation is successful, returned address will update any/all fields that needed correction.  Additionally, correctly resolved address will also include an indication if the address is residential, its geo-location (lat/long) as well as the timezone. Alternatively, if an address is not validated correctly, a list with problems is returned along the original address.


<a name="validateaddress"></a>
# **ValidateAddress**
> Address ValidateAddress (Address address)

POST: Validate specified address.  If validation is successful, returned address will update any/all fields that needed correction.  Additionally, correctly resolved address will also include an indication if the address is residential, its geo-location (lat/long) as well as the timezone. Alternatively, if an address is not validated correctly, a list with problems is returned along the original address.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class ValidateAddressExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new AddressValidationApi();
            var address = new Address(); // Address | address

            try
            {
                // POST: Validate specified address.  If validation is successful, returned address will update any/all fields that needed correction.  Additionally, correctly resolved address will also include an indication if the address is residential, its geo-location (lat/long) as well as the timezone. Alternatively, if an address is not validated correctly, a list with problems is returned along the original address.
                Address result = apiInstance.ValidateAddress(address);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AddressValidationApi.ValidateAddress: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | [**Address**](Address.md)| address | 

### Return type

[**Address**](Address.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

