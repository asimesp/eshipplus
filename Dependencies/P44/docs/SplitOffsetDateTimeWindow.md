# P44SDK.V4.Model.SplitOffsetDateTimeWindow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EndDateTime** | [**SplitOffsetDateTime**](SplitOffsetDateTime.md) | Start date and/or time of this window with optional time zone offset. | [optional] 
**StartDateTime** | [**SplitOffsetDateTime**](SplitOffsetDateTime.md) | Start date and/or time of this window with optional time zone offset. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

