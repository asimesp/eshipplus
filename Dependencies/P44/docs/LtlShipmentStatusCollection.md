# P44SDK.V4.Model.LtlShipmentStatusCollection
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ShipmentStatuses** | [**List&lt;LtlShipmentStatus&gt;**](LtlShipmentStatus.md) | A list of shipment statuses that match the query | [optional] 
**InfoMessages** | [**List&lt;Message&gt;**](Message.md) | Any system messages that occurred processing this request. Severity will be &#39;INFO&#39; or &#39;WARNING&#39;. No messages with severity &#39;ERROR&#39; will be returned here. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

