# P44SDK.V4.Model.Location
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Address** | [**Address**](Address.md) | Postal address of the location. | 
**Contact** | [**Contact**](Contact.md) | Contact information at the location. | [optional] 
**Id** | **string** | The project44-generated id for this location. This id is required when updating a location through the location management API. It cannot be provided in any other API calls. This id can be provided in the capacity provider account management API to assign relevant capacity provider account addresses. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

