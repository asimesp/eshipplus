# P44SDK.V4.Api.CapacityProviderAccountGroupManagementApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateCapacityProviderAccountGroupInfo**](CapacityProviderAccountGroupManagementApi.md#createcapacityprovideraccountgroupinfo) | **POST** /api/v4/capacityprovideraccountgroups | POST: Create a new capacity provider account group.
[**DeleteCapacityProviderAccountGroupInfo**](CapacityProviderAccountGroupManagementApi.md#deletecapacityprovideraccountgroupinfo) | **DELETE** /api/v4/capacityprovideraccountgroups/{id} | DELETE: Delete the capacity provider account group.
[**QueryCapacityProviderAccountGroup**](CapacityProviderAccountGroupManagementApi.md#querycapacityprovideraccountgroup) | **GET** /api/v4/capacityprovideraccountgroups | GET: Get the current list of all available capacity provider account groups.


<a name="createcapacityprovideraccountgroupinfo"></a>
# **CreateCapacityProviderAccountGroupInfo**
> CapacityProviderAccountGroupInfo CreateCapacityProviderAccountGroupInfo (CapacityProviderAccountGroupInfo capacityProviderAccountGroupInfo)

POST: Create a new capacity provider account group.

The capacity provider account group requires a unique code and may use an optional name. The response will include a unique id that can be used to reference the capacity provider account group in other APIs.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateCapacityProviderAccountGroupInfoExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new CapacityProviderAccountGroupManagementApi();
            var capacityProviderAccountGroupInfo = new CapacityProviderAccountGroupInfo(); // CapacityProviderAccountGroupInfo | capacityProviderAccountGroupInfo

            try
            {
                // POST: Create a new capacity provider account group.
                CapacityProviderAccountGroupInfo result = apiInstance.CreateCapacityProviderAccountGroupInfo(capacityProviderAccountGroupInfo);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CapacityProviderAccountGroupManagementApi.CreateCapacityProviderAccountGroupInfo: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **capacityProviderAccountGroupInfo** | [**CapacityProviderAccountGroupInfo**](CapacityProviderAccountGroupInfo.md)| capacityProviderAccountGroupInfo | 

### Return type

[**CapacityProviderAccountGroupInfo**](CapacityProviderAccountGroupInfo.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletecapacityprovideraccountgroupinfo"></a>
# **DeleteCapacityProviderAccountGroupInfo**
> void DeleteCapacityProviderAccountGroupInfo (long? id)

DELETE: Delete the capacity provider account group.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class DeleteCapacityProviderAccountGroupInfoExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new CapacityProviderAccountGroupManagementApi();
            var id = 789;  // long? | id

            try
            {
                // DELETE: Delete the capacity provider account group.
                apiInstance.DeleteCapacityProviderAccountGroupInfo(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CapacityProviderAccountGroupManagementApi.DeleteCapacityProviderAccountGroupInfo: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| id | 

### Return type

void (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="querycapacityprovideraccountgroup"></a>
# **QueryCapacityProviderAccountGroup**
> CapacityProviderAccountGroupInfoCollection QueryCapacityProviderAccountGroup ()

GET: Get the current list of all available capacity provider account groups.

Each capacity provider account group returned in the response includes a unique id that can be used as reference in other APIs.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class QueryCapacityProviderAccountGroupExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new CapacityProviderAccountGroupManagementApi();

            try
            {
                // GET: Get the current list of all available capacity provider account groups.
                CapacityProviderAccountGroupInfoCollection result = apiInstance.QueryCapacityProviderAccountGroup();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CapacityProviderAccountGroupManagementApi.QueryCapacityProviderAccountGroup: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CapacityProviderAccountGroupInfoCollection**](CapacityProviderAccountGroupInfoCollection.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

