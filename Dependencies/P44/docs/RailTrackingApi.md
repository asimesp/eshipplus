# P44SDK.V4.Api.RailTrackingApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateShipment**](RailTrackingApi.md#createshipment) | **POST** /api/v4/rail/shipments/ | POST: Initialize a shipment for tracking.
[**DeleteShipment**](RailTrackingApi.md#deleteshipment) | **DELETE** /api/v4/rail/shipments/{id} | DELETE: Delete a shipment by system id.
[**GetShipmentStatus**](RailTrackingApi.md#getshipmentstatus) | **GET** /api/v4/rail/shipments/statuses | GET: Get a shipment status by shipment identifier.
[**GetShipmentStatus_0**](RailTrackingApi.md#getshipmentstatus_0) | **GET** /api/v4/rail/shipments/{id}/statuses | GET: Get a shipment status by system id.


<a name="createshipment"></a>
# **CreateShipment**
> RailShipmentConfirmation CreateShipment (RailShipment shipment)

POST: Initialize a shipment for tracking.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new RailTrackingApi();
            var shipment = new RailShipment(); // RailShipment | shipment

            try
            {
                // POST: Initialize a shipment for tracking.
                RailShipmentConfirmation result = apiInstance.CreateShipment(shipment);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RailTrackingApi.CreateShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipment** | [**RailShipment**](RailShipment.md)| shipment | 

### Return type

[**RailShipmentConfirmation**](RailShipmentConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteshipment"></a>
# **DeleteShipment**
> void DeleteShipment (long? id)

DELETE: Delete a shipment by system id.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class DeleteShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new RailTrackingApi();
            var id = 789;  // long? | The project44-generated id of the shipment being queried.

            try
            {
                // DELETE: Delete a shipment by system id.
                apiInstance.DeleteShipment(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RailTrackingApi.DeleteShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| The project44-generated id of the shipment being queried. | 

### Return type

void (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getshipmentstatus"></a>
# **GetShipmentStatus**
> RailShipmentStatus GetShipmentStatus (string shipmentIdentifierType, string shipmentIdentifierValue, bool? includeStatusHistory = null)

GET: Get a shipment status by shipment identifier.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GetShipmentStatusExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new RailTrackingApi();
            var shipmentIdentifierType = shipmentIdentifierType_example;  // string | The type of the shipment identifier.
            var shipmentIdentifierValue = shipmentIdentifierValue_example;  // string | The value of the shipment identifier.
            var includeStatusHistory = true;  // bool? | Whether shipment status history ('statusUpdates') should be included in the response. (optional)  (default to false)

            try
            {
                // GET: Get a shipment status by shipment identifier.
                RailShipmentStatus result = apiInstance.GetShipmentStatus(shipmentIdentifierType, shipmentIdentifierValue, includeStatusHistory);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RailTrackingApi.GetShipmentStatus: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipmentIdentifierType** | **string**| The type of the shipment identifier. | 
 **shipmentIdentifierValue** | **string**| The value of the shipment identifier. | 
 **includeStatusHistory** | **bool?**| Whether shipment status history (&#39;statusUpdates&#39;) should be included in the response. | [optional] [default to false]

### Return type

[**RailShipmentStatus**](RailShipmentStatus.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getshipmentstatus_0"></a>
# **GetShipmentStatus_0**
> RailShipmentStatus GetShipmentStatus_0 (long? id, bool? includeStatusHistory = null)

GET: Get a shipment status by system id.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GetShipmentStatus_0Example
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new RailTrackingApi();
            var id = 789;  // long? | The project44-generated id of the shipment being queried.
            var includeStatusHistory = true;  // bool? | Whether shipment status history ('statusUpdates') should be included in the response. (optional)  (default to false)

            try
            {
                // GET: Get a shipment status by system id.
                RailShipmentStatus result = apiInstance.GetShipmentStatus_0(id, includeStatusHistory);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RailTrackingApi.GetShipmentStatus_0: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| The project44-generated id of the shipment being queried. | 
 **includeStatusHistory** | **bool?**| Whether shipment status history (&#39;statusUpdates&#39;) should be included in the response. | [optional] [default to false]

### Return type

[**RailShipmentStatus**](RailShipmentStatus.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

