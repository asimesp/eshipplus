# P44SDK.V4.Model.CapacityProviderAccountGroupInfoCollection
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Groups** | [**List&lt;CapacityProviderAccountGroupInfo&gt;**](CapacityProviderAccountGroupInfo.md) | A collection of details necessary to get or instantiate a new capacity provider group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

