# P44SDK.V4.Model.RailAddress
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PointName** | **string** | Name of this point. Usually a city name but may also be the name of the junction, yard, or facility | [optional] 
**State** | **string** | Abbreviation of state, province, district, etc. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

