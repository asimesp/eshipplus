# P44SDK.V4.Model.RailEquipmentStopStatus
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StopNumber** | **int?** | The number of the shipment stop to which this status pertains. | [optional] 
**StopStatusCode** | **string** | The status of the shipment relative to this stop - - whether it is presumed to be currently en route to the stop or whether it has been recorded as having arrived or departed the stop. | [optional] 
**ArrivalEstimate** | [**RailArrivalEstimate**](RailArrivalEstimate.md) | Disclaimer: This functionality returns data but is still being enhanced and may be unreliable at times. Estimate of the dateTime and arrivalCode of the equipment when it will arrive at this stop. Only computed if the equipment has not yet arrived. | [optional] 
**ArrivalCode** | **string** | The approximate arrival status of the shipment at the stop, as recorded. | [optional] 
**ArrivalDateTime** | **string** | The recorded arrival time of the shipment at this stop, if available. This time will always be in the time zone of the stop, and a time zone offset will always be provided. | [optional] 
**DepartureDateTime** | **string** | The recorded departure time of the shipment from this stop, if available. This time will always be in the time zone of the stop, and a time zone offset will always be provided. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

