# P44SDK.V4.Model.RateQuoteCollection
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RateQuotes** | [**List&lt;RateQuote&gt;**](RateQuote.md) | List of rate quotes from all capacity providers requested. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

