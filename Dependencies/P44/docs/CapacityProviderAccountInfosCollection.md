# P44SDK.V4.Model.CapacityProviderAccountInfosCollection
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Accounts** | [**List&lt;CapacityProviderAccountInfos&gt;**](CapacityProviderAccountInfos.md) | List of account definitions for a capacity provider. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

