# P44SDK.V4.Model.LtlShipmentIdentifier
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | The type of the shipment identifier. | 
**Value** | **string** | The value of the shipment identifier. | 
**PrimaryForType** | **bool?** | Only used in LTL tracking. Whether this is the primary shipment identifier for this type for the given shipment. The primary shipment identifier for a type will always be attempted before other identifiers of that type with the capacity provider. | [optional] 
**Source** | **string** | Only used in LTL tracking. Whether this shipment identifier originated from the customer or the capacity provider. If the customer has provided this identifier at any point during the shipment&#39;s lifetime, this source will be &#39;CUSTOMER&#39;. Otherwise, the source will be &#39;CAPACITY_PROVIDER&#39;. This field is useful for ensuring uniqueness across your shipment identifiers - - you can&#39;t control whether duplicates exist among the capacity-provider-sourced identifiers, but you can ensure on your end that all of your provided identifiers are unique and query for unique shipments accordingly. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

