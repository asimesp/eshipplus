# P44SDK.V4.Model.CapacityProviderAccount
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | The code for the requested capacity provider account that was used for authentication with the capacity provider&#39;s API. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

