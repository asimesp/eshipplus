# P44SDK.V4.Api.CapacityProviderAccountManagementApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateCapacityProviderAccountInfos**](CapacityProviderAccountManagementApi.md#createcapacityprovideraccountinfos) | **POST** /api/v4/capacityprovideraccounts | POST: Create a capacity provider account.
[**DeleteCapacityProviderAccountInfos**](CapacityProviderAccountManagementApi.md#deletecapacityprovideraccountinfos) | **DELETE** /api/v4/capacityprovideraccounts/{id} | DELETE: Delete capacity provider account infos.
[**QueryListCapacityProviderAccountInfos**](CapacityProviderAccountManagementApi.md#querylistcapacityprovideraccountinfos) | **GET** /api/v4/capacityprovideraccounts | GET: Get the current list of all available capacity provider account infos.
[**QuerySingleCapacityProviderAccountInfos**](CapacityProviderAccountManagementApi.md#querysinglecapacityprovideraccountinfos) | **GET** /api/v4/capacityprovideraccounts/{id} | GET: Get the infos object for a single capacity provider.
[**UpdateCapacityProviderAccountInfos**](CapacityProviderAccountManagementApi.md#updatecapacityprovideraccountinfos) | **PUT** /api/v4/capacityprovideraccounts/{id} | PUT: Update the capacity provider account.


<a name="createcapacityprovideraccountinfos"></a>
# **CreateCapacityProviderAccountInfos**
> CapacityProviderAccountInfos CreateCapacityProviderAccountInfos (CapacityProviderAccountInfos capacityProviderAccountInfos)

POST: Create a capacity provider account.

The capacity provider account uses the capacity provider account infos object.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateCapacityProviderAccountInfosExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new CapacityProviderAccountManagementApi();
            var capacityProviderAccountInfos = new CapacityProviderAccountInfos(); // CapacityProviderAccountInfos | capacityProviderAccountInfos

            try
            {
                // POST: Create a capacity provider account.
                CapacityProviderAccountInfos result = apiInstance.CreateCapacityProviderAccountInfos(capacityProviderAccountInfos);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CapacityProviderAccountManagementApi.CreateCapacityProviderAccountInfos: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **capacityProviderAccountInfos** | [**CapacityProviderAccountInfos**](CapacityProviderAccountInfos.md)| capacityProviderAccountInfos | 

### Return type

[**CapacityProviderAccountInfos**](CapacityProviderAccountInfos.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletecapacityprovideraccountinfos"></a>
# **DeleteCapacityProviderAccountInfos**
> void DeleteCapacityProviderAccountInfos (long? id)

DELETE: Delete capacity provider account infos.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class DeleteCapacityProviderAccountInfosExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new CapacityProviderAccountManagementApi();
            var id = 789;  // long? | id

            try
            {
                // DELETE: Delete capacity provider account infos.
                apiInstance.DeleteCapacityProviderAccountInfos(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CapacityProviderAccountManagementApi.DeleteCapacityProviderAccountInfos: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| id | 

### Return type

void (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="querylistcapacityprovideraccountinfos"></a>
# **QueryListCapacityProviderAccountInfos**
> CapacityProviderAccountInfosCollection QueryListCapacityProviderAccountInfos ()

GET: Get the current list of all available capacity provider account infos.

Each capacity provider account infos object returned in the response includes a unique id that can be used as reference in other APIs.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class QueryListCapacityProviderAccountInfosExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new CapacityProviderAccountManagementApi();

            try
            {
                // GET: Get the current list of all available capacity provider account infos.
                CapacityProviderAccountInfosCollection result = apiInstance.QueryListCapacityProviderAccountInfos();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CapacityProviderAccountManagementApi.QueryListCapacityProviderAccountInfos: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CapacityProviderAccountInfosCollection**](CapacityProviderAccountInfosCollection.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="querysinglecapacityprovideraccountinfos"></a>
# **QuerySingleCapacityProviderAccountInfos**
> CapacityProviderAccountInfos QuerySingleCapacityProviderAccountInfos (long? id)

GET: Get the infos object for a single capacity provider.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class QuerySingleCapacityProviderAccountInfosExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new CapacityProviderAccountManagementApi();
            var id = 789;  // long? | id

            try
            {
                // GET: Get the infos object for a single capacity provider.
                CapacityProviderAccountInfos result = apiInstance.QuerySingleCapacityProviderAccountInfos(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CapacityProviderAccountManagementApi.QuerySingleCapacityProviderAccountInfos: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| id | 

### Return type

[**CapacityProviderAccountInfos**](CapacityProviderAccountInfos.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatecapacityprovideraccountinfos"></a>
# **UpdateCapacityProviderAccountInfos**
> CapacityProviderAccountInfos UpdateCapacityProviderAccountInfos (long? id, CapacityProviderAccountInfos capacityProviderAccountInfos)

PUT: Update the capacity provider account.

The capacity provider account uses the capacity provider account infos object.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class UpdateCapacityProviderAccountInfosExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new CapacityProviderAccountManagementApi();
            var id = 789;  // long? | id
            var capacityProviderAccountInfos = new CapacityProviderAccountInfos(); // CapacityProviderAccountInfos | capacityProviderAccountInfos

            try
            {
                // PUT: Update the capacity provider account.
                CapacityProviderAccountInfos result = apiInstance.UpdateCapacityProviderAccountInfos(id, capacityProviderAccountInfos);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CapacityProviderAccountManagementApi.UpdateCapacityProviderAccountInfos: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| id | 
 **capacityProviderAccountInfos** | [**CapacityProviderAccountInfos**](CapacityProviderAccountInfos.md)| capacityProviderAccountInfos | 

### Return type

[**CapacityProviderAccountInfos**](CapacityProviderAccountInfos.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

