# P44SDK.V4.Model.AccessorialServiceConfiguration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FetchAllServiceLevels** | **bool?** | If set to &#39;true&#39;, rate quotes for all possible service levels (e.g., guaranteed or expedited) will be retrieved from each capacity provider, based on what each provider offers. (default: &#39;false&#39;) | [optional] 
**FetchAllGuaranteed** | **bool?** | If set to &#39;true&#39;, rate quotes for all possible guaranteed service levels (e.g., guaranteed by noon or guaranteed by 3pm) will be retrieved from each capacity provider, based on what each provider offers. (default: &#39;false&#39;) | [optional] 
**FetchAllExcessiveLength** | **bool?** | If set to &#39;true&#39;, rate quotes for all possible excessive length accessorial services will be retrieved from each capacity provider, based on what each provider offers. (default: &#39;false&#39;) | [optional] 
**FetchAllInsideDelivery** | **bool?** | If set to &#39;true&#39;, rate quotes for all possible inside delivery accessorial services will be retrieved from each capacity provider, based on what each provider offers. (default: &#39;false&#39;) | [optional] 
**AllowUnacceptedAccessorials** | **bool?** | If set to &#39;true&#39;, rate quotes will be allowed from capacity providers that do not accept one or more requested accessorial services. The default behavior is to reject a rate quote with an error from any capacity provider that does not accept every requested accessorial service in the query. (default: &#39;false&#39;) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

