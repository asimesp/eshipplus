# P44SDK.V4.Model.ParcelShipmentState
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActualDeliveryTimestamp** | **string** |  | [optional] 
**CreatedAtTimestamp** | **string** |  | [optional] 
**EstimatedDeliveryTimestamp** | **string** |  | [optional] 
**Id** | **long?** |  | [optional] 
**PickedUpTimestamp** | **string** |  | [optional] 
**ShipmentIdentifiers** | [**List&lt;ParcelShipmentIdentifier&gt;**](ParcelShipmentIdentifier.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

