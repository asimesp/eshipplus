# P44SDK.V4.Model.LtlImageReference
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ImageUrl** | **string** | The web URL for accessing this image. | [optional] 
**DocumentType** | **string** | A code for this image&#39;s document type, matching what was requested. | [optional] 
**ImageFormat** | **string** | A code for this image&#39;s format, matching what was requested. | [optional] 
**ProNumber** | **string** | PRO number identifying the shipment related to the requested image. This may be slightly different than the requested PRO number, depending on the capacity provider (e.g., leading zeros may be missing). | [optional] 
**InfoMessages** | [**List&lt;Message&gt;**](Message.md) | System messages and messages from the capacity provider with severity &#39;INFO&#39; or &#39;WARNING&#39;. No messages with severity &#39;ERROR&#39; will be returned here. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

