# P44SDK.V4.Model.LtlTrackedShipmentImageReference
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ImageUrl** | **string** | The web URL for accessing this image. | [optional] 
**DocumentType** | **string** | A code for this image&#39;s document type. | [optional] 
**ImageFormat** | **string** | A code for this image&#39;s format. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

