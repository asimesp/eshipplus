# P44SDK.V4.Model.TruckloadShipmentTruckDetails
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TruckDimensions** | [**TruckloadShipmentTruckDimensions**](TruckloadShipmentTruckDimensions.md) | Optional information for the dimensions of the truck. | [optional] 
**Weight** | **int?** | Indicates the weight of the truck where excessive weight may require the driver to choose alternative routes. | [optional] 
**WeightUnitOfMeasure** | **string** | Weight measurement units (default: &#39;LB&#39;). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

