# P44SDK.V4.Model.TransitTimeQuoteQuery
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OriginAddress** | [**Address**](Address.md) | The origin address of the shipment to be quoted. City and state will be automatically looked up for American and Canadian postal codes if not provided. Note, however, some postal codes correspond to multiple cities. | 
**DestinationAddress** | [**Address**](Address.md) | The destination address of the shipment to be quoted. City and state will be automatically looked up for American and Canadian postal codes if not provided. Note, however, some postal codes correspond to multiple cities. | 
**CapacityProviderAccountGroup** | [**CapacityProviderAccountGroup**](CapacityProviderAccountGroup.md) | Capacity provider account group, containing all accounts to be used for authentication with the capacity providers&#39; transit time quote APIs. | [optional] 
**PickupDate** | **string** | The pickup date in the timezone of the origin location of the shipment being quoted. (default: current date; format: yyyy-MM-dd) | [optional] 
**ApiConfiguration** | [**TransitTimeQuoteApiConfiguration**](TransitTimeQuoteApiConfiguration.md) | Fields for configuring the behavior of this API. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

