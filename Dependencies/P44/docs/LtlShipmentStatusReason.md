# P44SDK.V4.Model.LtlShipmentStatusReason
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | The reason code for the status of the status update. | [optional] 
**Description** | **string** | A description of the reason for the status of the status update. This description may be provided by the capacity provider. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

