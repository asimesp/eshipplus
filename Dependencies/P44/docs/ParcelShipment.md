# P44SDK.V4.Model.ParcelShipment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CapacityProviderAccountGroup** | [**CapacityProviderAccountGroup**](CapacityProviderAccountGroup.md) | Fields for configuring the behavior of this API. | 
**ShipmentIdentifiers** | [**List&lt;ParcelShipmentIdentifier&gt;**](ParcelShipmentIdentifier.md) | A user-defined list of shipment identifiers that are unique to this shipment and carrier identifier. These may be used in subsequent API calls along with the carrier identifier to reference a shipment. They may also be used by project44 to track the shipment. Only one shipment identifier of each type may be provided. Currently, parcel tracking only accepts shipment identifier with type &#39;TRACKING_NUMBER&#39;. | 
**ApiConfiguration** | [**ParcelShipmentApiConfiguration**](ParcelShipmentApiConfiguration.md) | Fields for configuring the behavior of this API. | [optional] 
**Id** | **long?** | The project44-generated id for the shipment. This may not be provided when creating a shipment, but will always be returned in the shipment confirmation. This id may be used in subsequent API calls to reference a shipment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

