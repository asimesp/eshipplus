# P44SDK.V4.Model.ServiceLevel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | The project44 code for this service level. | [optional] 
**Description** | **string** | A description of this service level, often originating from the capacity provider. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

