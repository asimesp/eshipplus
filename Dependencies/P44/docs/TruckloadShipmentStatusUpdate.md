# P44SDK.V4.Model.TruckloadShipmentStatusUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Timestamp** | **string** | The timestamp of this status update in the time zone of the location. If this status update has no location, this timestamp will be in UTC. A time zone offset will always be provided. This field will always be populated (format: yyyy-MM-dd&#39;T&#39;HH:mm:ssZ). | [optional] 
**StatusCode** | **string** | The status code for this status update. This field will always be populated. | [optional] 
**StatusReason** | [**TruckloadShipmentStatusReason**](TruckloadShipmentStatusReason.md) | The reason for the status, providing an additional level of detail for the status. This field will not be provided if no additional detail is available. | [optional] 
**GeoCoordinates** | [**GeoCoordinates**](GeoCoordinates.md) | The approximate latitude and longitude of the shipment&#39;s location at the time of this status update, if available. | [optional] 
**Address** | [**Address**](Address.md) | The approximate postal address of the shipment&#39;s location at the time of this status update, if available. | [optional] 
**StopNumber** | **int?** | The stop number to which this status update corresponds, if any. For example, a status update with status code &#39;AT_STOP&#39; will always have this field populated with the stop number the vehicle is at. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

