# P44SDK.V4.Model.RailShipment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ShipmentIdentifiers** | [**List&lt;RailShipmentIdentifier&gt;**](RailShipmentIdentifier.md) | A user-defined list of shipment identifiers that are unique to this shipment. You must pass in either a shipment identifier or a tracking start date time. | [optional] 
**EquipmentIdentifiers** | [**List&lt;RailEquipmentIdentifier&gt;**](RailEquipmentIdentifier.md) | A user-defined list of equipment identifiers identifying rail equipment to be tracked as part of this shipment. This includes container ids and rail car ids | 
**ShipmentStops** | [**List&lt;RailShipmentStop&gt;**](RailShipmentStop.md) | Contains the shipment&#39;s origin and destination. | [optional] 
**TrackingStartDateTime** | **string** | Tracking for this shipment will begin at this time. This should be set to the time when your shipment has been loaded into the container or rail car. Cannot be more than 60 days in the past or 60 days in the future. You must pass in either a shipment identifier or a tracking start date time. | [optional] 
**Id** | **long?** | The project44-generated id for the shipment. This may not be provided when creating a shipment, but will always be returned in the shipment confirmation. This id may be used in subsequent API calls to reference a shipment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

