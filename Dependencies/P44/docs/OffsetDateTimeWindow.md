# P44SDK.V4.Model.OffsetDateTimeWindow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StartDateTime** | **string** | Start time of this window with timezone offset. (format: yyyy-MM-dd&#39;T&#39;HH:mm:ssZ) | [optional] 
**EndDateTime** | **string** | End time of this window with timezone offset. (format: yyyy-MM-dd&#39;T&#39;HH:mm:ssZ) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

