# P44SDK.V4.Model.TruckloadShippingDetails
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MultipleDrivers** | **bool?** | If set to &#39;true&#39;, indicates that the shipment is using a team of drivers and is therefore not constrained by the \&quot;11 hours of service\&quot; limit for an individual driver. This allows project44 to change its estimations around when a shipment can, and cannot be delivered. | [optional] 
**TruckDetails** | [**TruckloadShipmentTruckDetails**](TruckloadShipmentTruckDetails.md) | Optional information about the truck&#39;s size and weight that may adjust ETA and other tracking-related calculations. | [optional] 
**HazmatDetails** | [**TruckloadShipmentHazmatDetails**](TruckloadShipmentHazmatDetails.md) | Optional hazmat information about the truckload shipment that may adjust ETA and other tracking-related calculations. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

