# P44SDK.V4.Model.ParcelShipmentConfirmation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InfoMessages** | [**List&lt;MessageDomain&gt;**](MessageDomain.md) |  | [optional] 
**Shipment** | [**ParcelShipment**](ParcelShipment.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

