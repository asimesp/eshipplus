# P44SDK.V4.Model.LineItem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TotalWeight** | **decimal?** | Total weight of all packages composing this line item. | 
**PackageDimensions** | [**CubicDimension**](CubicDimension.md) | Dimensions of each package in this line item. | 
**FreightClass** | **string** | Freight class of all packages composing this item.  Required for LTL quotes and shipments only. | [optional] 
**PackageType** | **string** | Type of packages composing this line item. (default: &#39;PLT&#39;) | [optional] 
**TotalPackages** | **int?** | The number of packages composing this line item. (default: &#39;1&#39;) | [optional] 
**TotalPieces** | **int?** | The total number of pieces across all packages composing this line item. (default: &#39;1&#39;) | [optional] 
**Description** | **string** | Readable description of this line item. | [optional] 
**Stackable** | **bool?** | Whether the packages composing this line item are stackable. (default: &#39;false&#39;) | [optional] 
**NmfcItemCode** | **string** | NMFC prefix code for all packages composing this line item. | [optional] 
**NmfcSubCode** | **string** | NMFC suffix code for all packages composing this line item. | [optional] 
**HazmatDetail** | [**LineItemHazmatDetail**](LineItemHazmatDetail.md) | Not available in rating (send the hazmat accessorial instead). Required for shipment if this line item contains hazardous materials. Provides important information about the hazardous materials to be transported, as required by the US Department of Transportation (DOT). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

