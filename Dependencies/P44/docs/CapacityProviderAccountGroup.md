# P44SDK.V4.Model.CapacityProviderAccountGroup
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | The code for the capacity provider account group that contains all accounts against which an operation is to be performed. Capacity provider account groups are set up through the project44 Self-Service Portal. If no key is specified, the &#39;Default&#39; account group will be used. | [optional] 
**Accounts** | [**List&lt;CapacityProviderAccount&gt;**](CapacityProviderAccount.md) | Capacity provider accounts used for authentication with the capacity providers&#39; APIs. For quoting, defaults to all accounts within the account group. For shipment, shipment status, and image, one and only one account is required. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

