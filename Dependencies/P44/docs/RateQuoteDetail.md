# P44SDK.V4.Model.RateQuoteDetail
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | **decimal?** | The final total for this quote. | [optional] 
**Subtotal** | **decimal?** | Subtotal prior to discounts, if provided. | [optional] 
**Charges** | [**List&lt;Charge&gt;**](Charge.md) | A list of individual charges making up the total, if provided. Not returned for alternate rate quotes. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

