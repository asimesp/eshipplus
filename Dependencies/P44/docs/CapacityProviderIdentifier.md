# P44SDK.V4.Model.CapacityProviderIdentifier
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | Capacity provider identifier type. | 
**Value** | **string** | Capacity provider identifier value. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

