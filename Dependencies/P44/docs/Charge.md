# P44SDK.V4.Model.Charge
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Code that identifies this line item charge. A list of charge codes used by project44 is provided in the API reference data section. | [optional] 
**Description** | **string** | Readable description of the line item charge, often originating from the capacity provider. | [optional] 
**Amount** | **decimal?** | The actual amount charged by the capacity provider (adds into the total). This amount may be negative (e.g., for discounts). | [optional] 
**Rate** | **decimal?** | Rate used by the capacity provider to calculate this charge (e.g., an amount charged per lb, per piece, per mile, etc.). This rate may be negative (e.g., for discounts). | [optional] 
**ItemFreightClass** | **string** | If this is an item charge, the freight class of the line item, if provided. This field is not returned for volume LTL quotes. | [optional] 
**ItemWeight** | **decimal?** | If this is an item charge, the weight of the line item, if provided. | [optional] 
**ItemWeightUnit** | **string** | If this is an item charge, the weight measurement unit for the provided line item weight. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

