# P44SDK.V4.Model.AccessorialService
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | The code for the requested accessorial service. A list of accessorial service codes supported by project44 is provided in the API reference data section. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

