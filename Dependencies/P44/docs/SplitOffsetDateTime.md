# P44SDK.V4.Model.SplitOffsetDateTime
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **string** | The date portion of this this date time object. This field will always be populated if this object is not null. (format: yyyy-MM-dd) | [optional] 
**Time** | **string** | The time portion of this date time object. This field may be null if it is unknown. If this field is null, &#39;timeZoneOffset&#39; will also be null. (format: HH:mm) | [optional] 
**TimeZoneOffset** | **string** | The time zone offset of this date time object. This field may be null if it is unknown, in which case the date and time should be assumed local to the applicable location. UTC will be indicated by an offset of &#39;+0000&#39;. (format: +HHmm or -HHmm) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

