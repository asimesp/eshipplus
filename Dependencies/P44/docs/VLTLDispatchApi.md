# P44SDK.V4.Api.VLTLDispatchApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateShipment**](VLTLDispatchApi.md#createshipment) | **POST** /api/v4/vltl/dispatchedshipments | POST: Create a volume LTL shipment.


<a name="createshipment"></a>
# **CreateShipment**
> LtlDispatchedShipmentConfirmation CreateShipment (LtlDispatchedShipment shipment)

POST: Create a volume LTL shipment.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VLTLDispatchApi();
            var shipment = new LtlDispatchedShipment(); // LtlDispatchedShipment | shipment

            try
            {
                // POST: Create a volume LTL shipment.
                LtlDispatchedShipmentConfirmation result = apiInstance.CreateShipment(shipment);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VLTLDispatchApi.CreateShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipment** | [**LtlDispatchedShipment**](LtlDispatchedShipment.md)| shipment | 

### Return type

[**LtlDispatchedShipmentConfirmation**](LtlDispatchedShipmentConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

