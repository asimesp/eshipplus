# P44SDK.V4.Model.ShipmentNoteConfiguration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnableTruncation** | **bool?** | If set to &#39;true&#39;, project44 will truncate the final pickup note it constructs if it exceeds the maximum allowable length for the capacity provider. When &#39;false&#39;, an error will be returned and the shipment will be not placed for pickup with the capacity provider if the constructed pickup note exceeds the maximum allowable length for the capacity provider. (default: &#39;false&#39;) | [optional] 
**NoteSections** | [**List&lt;ShipmentNoteSection&gt;**](ShipmentNoteSection.md) | A list of sections, in order, to send through the capacity provider&#39;s pickup note API field. Brief descriptions of accessorial services will be used. Item dimensions are added in the format 00x00x00. If not provided, the default note sections, in order, are as follows: pickup note, priority accessorials, pickup accessorials, dimensions, delivery note, delivery accessorials, and other accessorials. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

