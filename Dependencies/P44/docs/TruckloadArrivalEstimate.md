# P44SDK.V4.Model.TruckloadArrivalEstimate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EstimatedArrivalWindow** | [**OffsetDateTimeWindow**](OffsetDateTimeWindow.md) | The approximate arrival time window, as estimated. | [optional] 
**LastCalculatedDateTime** | **string** | When this eta was last computed. (format: yyyy-MM-dd&#39;T&#39;HH:mm:ssZ) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

