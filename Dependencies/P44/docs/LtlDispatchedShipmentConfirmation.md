# P44SDK.V4.Model.LtlDispatchedShipmentConfirmation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long?** | The id of the dispatched shipment. | [optional] 
**ShipmentIdentifiers** | [**List&lt;LtlShipmentIdentifier&gt;**](LtlShipmentIdentifier.md) | A list of identifiers for the confirmed shipment. Nearly all capacity providers provide a pickup confirmation number, which will appear in this list with type &#39;PICKUP&#39;. A few capacity providers also provide a PRO number when a shipment is confirmed. Shipment identifiers provided by the customer will show up here, as well. | [optional] 
**CapacityProviderBolUrl** | **string** | URL pointing to a PDF document of the capacity provider&#39;s Bill of Lading, if available. | [optional] 
**PackingVisualizationUrl** | **string** | Customers who have enabled our Volume Visualizer product will find a URL to a PDF document illustrating the optimized item arrangement, for the shipment&#39;s total linear footage value. (Example: https://cloud.p-44.com/unauthenticated/linear-feet/pdf/{linearFeetVisualizationId}) | [optional] 
**PickupNote** | **string** | The final note that was sent through the capacity provider&#39;s pickup note API field, as constructed by project44 according to the requested shipment note configuration. | [optional] 
**PickupDateTime** | **DateTime?** | The pickup date and time as provided by the capacity provider in the timezone of origin location of the shipment. (format: yyyy-MM-dd&#39;T&#39;HH:mm:ss) | [optional] 
**InfoMessages** | [**List&lt;Message&gt;**](Message.md) | System messages and messages from the capacity provider with severity &#39;INFO&#39; or &#39;WARNING&#39;. No messages with severity &#39;ERROR&#39; will be returned here. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

