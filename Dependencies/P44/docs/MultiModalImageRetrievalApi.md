# P44SDK.V4.Api.MultiModalImageRetrievalApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**QueryImages**](MultiModalImageRetrievalApi.md#queryimages) | **POST** /api/v4/images/query | POST: Query for shipment images.


<a name="queryimages"></a>
# **QueryImages**
> MultiModalImageReference QueryImages (MultiModalImageQuery multiModalImageQuery)

POST: Query for shipment images.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class QueryImagesExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new MultiModalImageRetrievalApi();
            var multiModalImageQuery = new MultiModalImageQuery(); // MultiModalImageQuery | multiModalImageQuery

            try
            {
                // POST: Query for shipment images.
                MultiModalImageReference result = apiInstance.QueryImages(multiModalImageQuery);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MultiModalImageRetrievalApi.QueryImages: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **multiModalImageQuery** | [**MultiModalImageQuery**](MultiModalImageQuery.md)| multiModalImageQuery | 

### Return type

[**MultiModalImageReference**](MultiModalImageReference.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

