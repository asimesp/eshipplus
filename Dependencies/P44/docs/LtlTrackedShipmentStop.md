# P44SDK.V4.Model.LtlTrackedShipmentStop
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long?** | The id of this stop. This value is read only, it will be ignored on create and update. | [optional] 
**StopType** | **string** | The type of this stop. The user-defined stops will always have type &#39;ORIGIN&#39; and &#39;DESTINATION&#39;, which are customer locations. Stops of type &#39;TERMINAL&#39;, which are capacity provider redistribution centers, may be inserted dynamically as they are returned by the capacity provider.  Note that &#39;TERMINAL&#39; will be ignored if provided as part of a PUT. | 
**Location** | [**Location**](Location.md) | The location of this stop. A full address and contact should be provided by the user on shipment creation. Terminals may or may not have complete address and contact information. | 
**AppointmentWindow** | [**ZonedDateTimeWindow**](ZonedDateTimeWindow.md) | The appointment window of this stop - - i.e., the shipment&#39;s expected window of arrival at this stop. This may be used to report whether a shipment is on time to a stop. It may also be used to match and look up shipments with the capacity provider (e.g., by comparing pickup dates). The start and end times are required for &#39;appointmentWindow&#39; - - the default start time does not apply to this field. On responses, the time zone identifier (e.g., &#39;America/Chicago&#39;) for this date time window is determined by project44 when possible based on the stop&#39;s address. | 
**StopName** | **string** | The user-defined or capacity-provider-defined name of this stop. This is a freeform field. Project44 will default the name of each stop based on the stop number and type, if no other name is available. | [optional] 
**StopNumber** | **int?** | The stop number, where &#39;1&#39; is the origin and the destination has the largest number, with any stops inbetween ordered by appointment time. This field may not be provided when creating a shipment. The destination stop number will change dynamically as terminals are discovered along the shipment&#39;s route from origin to destination. Thus, the stop type is most useful for easily and reliably identifying the origin and destination. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

