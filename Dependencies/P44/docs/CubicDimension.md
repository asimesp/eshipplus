# P44SDK.V4.Model.CubicDimension
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Length** | **decimal?** | Length measurement. | 
**Width** | **decimal?** | Width measurement. | 
**Height** | **decimal?** | Height measurement. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

