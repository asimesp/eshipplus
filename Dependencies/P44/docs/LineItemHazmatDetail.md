# P44SDK.V4.Model.LineItemHazmatDetail
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IdentificationNumber** | **string** | The United Nations (UN) or North America (NA) number identifying the hazmat item. | 
**ProperShippingName** | **string** | The proper shipping name of the hazmat item. | 
**HazardClass** | **string** | The hazard class number, according to the classification system outlined by the Federal Motor Carrier Safety Administration (FMCSA). This is a one digit number or a two digit number separated by a decimal. | 
**PackingGroup** | **string** | The hazmat packing group for a line item, indicating the degree of danger. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

