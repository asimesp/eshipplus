# P44SDK.V4.Model.ParcelShipmentStatusReason
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | The code that further describes the status event. | [optional] 
**Description** | **string** | The description of the status event provided by the carrier. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

