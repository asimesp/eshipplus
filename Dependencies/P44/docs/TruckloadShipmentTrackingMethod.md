# P44SDK.V4.Model.TruckloadShipmentTrackingMethod
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ShipmentIdentifier** | [**TruckloadShipmentIdentifier**](TruckloadShipmentIdentifier.md) | A shipment identifier that can be used to track a shipment, containing the type required to use this tracking method and a dummy value. | [optional] 
**EquipmentIdentifier** | [**TruckloadEquipmentIdentifier**](TruckloadEquipmentIdentifier.md) | An equipment identifier that can be used to track a shipment, containing the type required to use this tracking method and a dummy value. | [optional] 
**CapacityProviderAccount** | [**CapacityProviderAccount**](CapacityProviderAccount.md) | A capacity provider account needed to authenticate with a tracking capacity provider, containing a dummy account code. | [optional] 
**InfoMessages** | [**List&lt;Message&gt;**](Message.md) | System messages and messages from the capacity provider with severity &#39;INFO&#39; or &#39;WARNING&#39;. No messages with severity &#39;ERROR&#39; will be returned here. These will provide more details about the shipment identifier and/or equipment identifier to provide. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

