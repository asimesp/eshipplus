# P44SDK.V4.Model.GeoCoordinates
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Latitude** | **float?** | The approximate geographic latitude of a location, using the WGS84 coordinate system. | [optional] 
**Longitude** | **float?** | The approximate geographic longitude of a location, using the WGS84 coordinate system. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

