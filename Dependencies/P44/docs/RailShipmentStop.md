# P44SDK.V4.Model.RailShipmentStop
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StopNumber** | **int?** | The user-defined stop number, where &#39;1&#39; is the origin and &#39;2&#39; is the destination. | 
**AppointmentWindow** | [**ZonedDateTimeWindow**](ZonedDateTimeWindow.md) | The user-defined appointment window of this stop - - i.e., the shipment&#39;s expected window of arrival at this stop. This is used to report whether a shipment is on time to a stop. The time zone identifier (e.g., &#39;America/Chicago&#39;) for this date time window is determined by project44 based on the stop&#39;s address. The time zone will be applied to the appointment window when making time comparisons. | [optional] 
**Location** | [**RailLocation**](RailLocation.md) | The location of this stop. | [optional] 
**StopName** | **string** | The optionally user-defined stop name of this stop. This is a freeform field. If no name is provided Project44 will default the name of each stop based on the address or stop number. | [optional] 
**GeoCoordinates** | [**GeoCoordinates**](GeoCoordinates.md) | The latitude and longitude of this stop. If not provided, this will be determined from the waybill. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

