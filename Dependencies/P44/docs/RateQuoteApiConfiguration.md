# P44SDK.V4.Model.RateQuoteApiConfiguration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Timeout** | **int?** | Number of seconds to wait for a capacity provider to return a rate quote. (default: &#39;10&#39;) | [optional] 
**EnableUnitConversion** | **bool?** | If set to &#39;true&#39;, weight and length values in this rate quote query will be converted when necessary to the capacity provider&#39;s supported units. When &#39;false&#39;, an error will be returned for each capacity provider that does not support the provided weight and length units. (default: &#39;false&#39;) | [optional] 
**AccessorialServiceConfiguration** | [**AccessorialServiceConfiguration**](AccessorialServiceConfiguration.md) | Configuration properties for accessorial services. | [optional] 
**FallBackToDefaultAccountGroup** | **bool?** | If set to &#39;true&#39; and the provided capacity provider account group code is invalid, the default capacity provider account group will be used. When &#39;false&#39;, an error will be returned if the provided capacity provider account group code is invalid. (default: &#39;false&#39;) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

