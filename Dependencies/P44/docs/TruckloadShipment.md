# P44SDK.V4.Model.TruckloadShipment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CarrierIdentifier** | [**CapacityProviderIdentifier**](CapacityProviderIdentifier.md) | The identifier of the carrier tendering the shipment. Type must be &#39;SCAC&#39;, &#39;DOT_NUMBER&#39;, or &#39;MC_NUMBER&#39;. | 
**ShipmentIdentifiers** | [**List&lt;TruckloadShipmentIdentifier&gt;**](TruckloadShipmentIdentifier.md) | A user-defined list of shipment identifiers that are unique to this shipment and carrier identifier. These may be used in subsequent API calls along with the carrier identifier to reference a shipment. They may also be used by project44 to track the shipment. Only one shipment identifier of each type may be provided. | 
**ShipmentStops** | [**List&lt;TruckloadShipmentStop&gt;**](TruckloadShipmentStop.md) | A user-defined list of all the shipment stops for the shipment, including stop locations and appointment windows. This list must contain the shipment&#39;s origin and destination and any stops of interest inbetween (other pickup or delivery locations). | 
**CapacityProviderAccountGroup** | [**CapacityProviderAccountGroup**](CapacityProviderAccountGroup.md) | Capacity provider account group, containing accounts to be used for authentication with the capacity providers&#39; truckload shipment APIs, if necessary. Not all tracking methods require a capacity provider account. Check the tracking methods API to see if your request requires a capacity provider account. The provided capacity provider account must be for the carrier specified by the &#39;carrierIdentifier&#39;. | [optional] 
**EquipmentIdentifiers** | [**List&lt;TruckloadEquipmentIdentifier&gt;**](TruckloadEquipmentIdentifier.md) | A user-defined list of equipment identifiers identifying truckload equipment that can be tracked, such as cell phones and electronic logging devices (ELDs). These need not be unique to this shipment. | [optional] 
**ShippingDetails** | [**TruckloadShippingDetails**](TruckloadShippingDetails.md) | Optional information about the truckload shipment that may adjust ETA and other tracking-related calculations. | [optional] 
**ApiConfiguration** | [**TruckloadShipmentApiConfiguration**](TruckloadShipmentApiConfiguration.md) | Fields for configuring the behavior of this API. | [optional] 
**Attributes** | [**List&lt;TruckloadShipmentAttribute&gt;**](TruckloadShipmentAttribute.md) | A list of up to 5 attributes (name/value pairs) to associate with this shipment. These values will be returned unchanged with each status response for the shipment and can also be used to group together related shipments in reporting scenarios, e.g. all shipments tracked for \&quot;division\&quot;:\&quot;Central\&quot;.  The attribute names have to be unique within the list. | [optional] 
**Id** | **long?** | The project44-generated id for the shipment. This may not be provided when creating a shipment, but will always be returned in the shipment confirmation. This id may be used in subsequent API calls to reference a shipment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

