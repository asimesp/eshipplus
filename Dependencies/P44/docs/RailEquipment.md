# P44SDK.V4.Model.RailEquipment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EquipmentIdentifier** | [**RailEquipmentIdentifier**](RailEquipmentIdentifier.md) | The equipment identifiers identifying the rail equipment to be tracked. Either a container id and rail car id | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

