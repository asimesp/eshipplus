# P44SDK.V4.Model.TruckloadShipmentActionConfirmation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InfoMessages** | [**List&lt;Message&gt;**](Message.md) | System messages and messages from the capacity provider with severity &#39;INFO&#39; or &#39;WARNING&#39;. No messages with severity &#39;ERROR&#39; will be returned here. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

