# P44SDK.V4.Model.TruckloadShipmentTruckDimensions
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Length** | **decimal?** | Indicates the length of the truck where excessive lengths may require the driver to choose alternative routes. | [optional] 
**Width** | **decimal?** | Indicates the width of the truck where excessive lengths may require the driver to choose alternative routes. | [optional] 
**Height** | **decimal?** | Indicates the height of the truck where excessive lengths may require the driver to choose alternative routes. | [optional] 
**UnitOfMeasure** | **string** | Length measurement units (default: &#39;FT&#39;). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

