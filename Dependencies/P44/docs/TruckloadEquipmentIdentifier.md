# P44SDK.V4.Model.TruckloadEquipmentIdentifier
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | The type of the equipment identifier. | 
**Value** | **string** | The value of the equipment identifier. Cell phone numbers will be obfuscated in project44 responses for privacy reasons. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

