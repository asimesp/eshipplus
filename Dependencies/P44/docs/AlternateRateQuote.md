# P44SDK.V4.Model.AlternateRateQuote
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CapacityProviderQuoteNumber** | **string** | The capacity provider&#39;s identifier for this alternate rate quote, if provided. | [optional] 
**ServiceLevel** | [**ServiceLevel**](ServiceLevel.md) | The service level (e.g., standard or guaranteed) of this alternate rate quote. | [optional] 
**DeliveryDateTime** | **DateTime?** | The delivery date and time for this alternate rate quote in the timezone of the destination location, if provided. (format: yyyy-MM-dd&#39;T&#39;HH:mm:ss) | [optional] 
**TransitDays** | **int?** | The number of service days to deliver the shipment being quoted after it is picked up, if provided. | [optional] 
**RateQuoteDetail** | [**RateQuoteDetail**](RateQuoteDetail.md) | Pricing details for this alternate rate quote, including the alternate total and subtotal. A list of charges is not returned for alternate rate quotes. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

