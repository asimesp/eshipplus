# P44SDK.V4.Api.ParcelWebhookApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateEndpoint**](ParcelWebhookApi.md#createendpoint) | **PUT** /api/v4/parcel/webhook/endpoint | PUT: Add or update webhook where shipment tracking updates are posted.
[**DeleteEndpoint**](ParcelWebhookApi.md#deleteendpoint) | **DELETE** /api/v4/parcel/webhook/endpoint | DELETE: Delete webhook endpoint - if one is defined.
[**GetEndpoint**](ParcelWebhookApi.md#getendpoint) | **GET** /api/v4/parcel/webhook/endpoint | GET: Get the webhook URL where shipment tracking updates are posted.


<a name="createendpoint"></a>
# **CreateEndpoint**
> ParcelWebhookEndpoint CreateEndpoint (ParcelWebhookEndpoint pushEndpoint)

PUT: Add or update webhook where shipment tracking updates are posted.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateEndpointExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new ParcelWebhookApi();
            var pushEndpoint = new ParcelWebhookEndpoint(); // ParcelWebhookEndpoint | pushEndpoint

            try
            {
                // PUT: Add or update webhook where shipment tracking updates are posted.
                ParcelWebhookEndpoint result = apiInstance.CreateEndpoint(pushEndpoint);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParcelWebhookApi.CreateEndpoint: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pushEndpoint** | [**ParcelWebhookEndpoint**](ParcelWebhookEndpoint.md)| pushEndpoint | 

### Return type

[**ParcelWebhookEndpoint**](ParcelWebhookEndpoint.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteendpoint"></a>
# **DeleteEndpoint**
> void DeleteEndpoint ()

DELETE: Delete webhook endpoint - if one is defined.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class DeleteEndpointExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new ParcelWebhookApi();

            try
            {
                // DELETE: Delete webhook endpoint - if one is defined.
                apiInstance.DeleteEndpoint();
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParcelWebhookApi.DeleteEndpoint: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getendpoint"></a>
# **GetEndpoint**
> ParcelWebhookEndpoint GetEndpoint ()

GET: Get the webhook URL where shipment tracking updates are posted.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GetEndpointExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new ParcelWebhookApi();

            try
            {
                // GET: Get the webhook URL where shipment tracking updates are posted.
                ParcelWebhookEndpoint result = apiInstance.GetEndpoint();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParcelWebhookApi.GetEndpoint: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ParcelWebhookEndpoint**](ParcelWebhookEndpoint.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

