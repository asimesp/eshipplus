# P44SDK.V4.Model.CapacityProviderAccountGroupInfo
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The user-provided name to be assigned to this group.  This field may be provided when creating an account group but is not required when creating or updating a set of capacity provider accounts in the capacity provider account management API. | 
**Code** | **string** | The unique, user-provided code used to identify this account group. Typically, an account group will correspond to a single customer or a single customer location. Account group codes are provided in shipment lifecycle APIs to identify a particular account. This field may be provided when creating an account group but is not required when creating or updating a set of capacity provider accounts in the capacity provider account management API. | 
**Id** | **long?** | The project44-generated id of the capacity provider account group. This id may not be provided when creating a capacity provider account group, but it is required when updating one or when creating or updating a capacity provider account set. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

