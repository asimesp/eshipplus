# P44SDK.V4.Api.LTLDispatchApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CancelShipment**](LTLDispatchApi.md#cancelshipment) | **POST** /api/v4/ltl/shipments/cancellations | POST: Cancel a dispatched shipment.
[**CreateShipment**](LTLDispatchApi.md#createshipment) | **POST** /api/v4/ltl/dispatchedshipments | POST: Create a shipment.
[**GenerateProNumber**](LTLDispatchApi.md#generatepronumber) | **POST** /api/v4/shipments/pronumbers/query | POST: Generate a ProNumber for specific vendor.


<a name="cancelshipment"></a>
# **CancelShipment**
> LtlShipmentCancellationConfirmation CancelShipment (LtlShipmentCancellation shipmentCancellation)

POST: Cancel a dispatched shipment.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CancelShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LTLDispatchApi();
            var shipmentCancellation = new LtlShipmentCancellation(); // LtlShipmentCancellation | shipmentCancellation

            try
            {
                // POST: Cancel a dispatched shipment.
                LtlShipmentCancellationConfirmation result = apiInstance.CancelShipment(shipmentCancellation);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LTLDispatchApi.CancelShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipmentCancellation** | [**LtlShipmentCancellation**](LtlShipmentCancellation.md)| shipmentCancellation | 

### Return type

[**LtlShipmentCancellationConfirmation**](LtlShipmentCancellationConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createshipment"></a>
# **CreateShipment**
> LtlDispatchedShipmentConfirmation CreateShipment (LtlDispatchedShipment shipment)

POST: Create a shipment.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LTLDispatchApi();
            var shipment = new LtlDispatchedShipment(); // LtlDispatchedShipment | shipment

            try
            {
                // POST: Create a shipment.
                LtlDispatchedShipmentConfirmation result = apiInstance.CreateShipment(shipment);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LTLDispatchApi.CreateShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipment** | [**LtlDispatchedShipment**](LtlDispatchedShipment.md)| shipment | 

### Return type

[**LtlDispatchedShipmentConfirmation**](LtlDispatchedShipmentConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="generatepronumber"></a>
# **GenerateProNumber**
> ProNumberIdentifier GenerateProNumber (ProNumberQuery proNumberQuery)

POST: Generate a ProNumber for specific vendor.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GenerateProNumberExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LTLDispatchApi();
            var proNumberQuery = new ProNumberQuery(); // ProNumberQuery | proNumberQuery

            try
            {
                // POST: Generate a ProNumber for specific vendor.
                ProNumberIdentifier result = apiInstance.GenerateProNumber(proNumberQuery);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LTLDispatchApi.GenerateProNumber: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **proNumberQuery** | [**ProNumberQuery**](ProNumberQuery.md)| proNumberQuery | 

### Return type

[**ProNumberIdentifier**](ProNumberIdentifier.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

