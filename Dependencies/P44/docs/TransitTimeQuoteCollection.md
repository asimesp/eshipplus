# P44SDK.V4.Model.TransitTimeQuoteCollection
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransitTimeQuotes** | [**List&lt;TransitTimeQuote&gt;**](TransitTimeQuote.md) | List of transit time quotes from all capacity providers requested. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

