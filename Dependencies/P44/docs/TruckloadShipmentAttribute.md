# P44SDK.V4.Model.TruckloadShipmentAttribute
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The name for an attribute. The name must be unique for this shipment. | 
**Value** | **string** | The value for an attribute. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

