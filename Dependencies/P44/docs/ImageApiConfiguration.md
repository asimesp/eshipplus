# P44SDK.V4.Model.ImageApiConfiguration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FallBackToDefaultAccountGroup** | **bool?** | If set to &#39;true&#39; and the provided capacity provider account group code is invalid, the default capacity provider account group will be used. When &#39;false&#39;, an error will be returned if the provided capacity provider account group code is invalid. (default: &#39;false&#39;) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

