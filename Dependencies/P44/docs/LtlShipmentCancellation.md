# P44SDK.V4.Model.LtlShipmentCancellation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CapacityProviderAccountGroup** | [**CapacityProviderAccountGroup**](CapacityProviderAccountGroup.md) | Capacity provider account group, containing all accounts to be used for authentication with the capacity provider&#39;s cancel dispatch API. | 
**OriginAddress** | [**Address**](Address.md) | The origin address for the shipment to be cancelled. | 
**ShipmentIdentifiers** | [**List&lt;ShipmentIdentifier&gt;**](ShipmentIdentifier.md) | A list of shipment identifiers of an item to cancel the dispatch of | 
**ApiConfiguration** | [**LtlShipmentCancellationApiConfiguration**](LtlShipmentCancellationApiConfiguration.md) | Fields for configuring the behavior of this API. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

