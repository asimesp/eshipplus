# P44SDK.V4.Model.RateQuoteQuery
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OriginAddress** | [**Address**](Address.md) | The origin address of the shipment to be quoted. City and state will be automatically looked up for American and Canadian postal codes if not provided. Note, however, some postal codes correspond to multiple cities. | 
**DestinationAddress** | [**Address**](Address.md) | The destination address of the shipment to be quoted. City and state will be automatically looked up for American and Canadian postal codes if not provided. Note, however, some postal codes correspond to multiple cities. | 
**LineItems** | [**List&lt;LineItem&gt;**](LineItem.md) | The line items composing the shipment to be quoted. A line item consists of one or more packages, all of the same package type and with the same dimensions, freight class, and NMFC code. Each package, however, may have a different number of pieces and a different weight. Note that each capacity provider has a different maximum number of line items that they can quote. | 
**CapacityProviderAccountGroup** | [**CapacityProviderAccountGroup**](CapacityProviderAccountGroup.md) | Capacity provider account group, containing all accounts to be used for authentication with the capacity providers&#39; rate quote APIs. | [optional] 
**AccessorialServices** | [**List&lt;AccessorialService&gt;**](AccessorialService.md) | List of accessorial services to be quoted for this shipment. | [optional] 
**PickupWindow** | [**LocalDateTimeWindow**](LocalDateTimeWindow.md) | The pickup date and time range in the timezone of the origin location of the shipment being quoted. (default: current date) | [optional] 
**DeliveryWindow** | [**LocalDateTimeWindow**](LocalDateTimeWindow.md) | The delivery date and time range in the timezone of the destination location of the shipment being quoted. Required by some capacity providers when requesting guaranteed or expedited services. | [optional] 
**TotalLinearFeet** | **int?** | The total linear feet that the shipment being quoted will take up in a trailer. Required only for volume LTL shipments. Customers who have enabled our Volume Visualizer product may submit rate requests without providing a value for totalLinearFeet. Linear footage will be automatically calculated, based on line item details, and submitted with your rate request. If a value is provided for totalLinearFeet, it will be submitted with the request and no calculation will be performed. | [optional] 
**WeightUnit** | **string** | Weight measurement unit for all weight values in this rate quote query. (default: &#39;LB&#39;) | [optional] 
**LengthUnit** | **string** | Length measurement unit for all length values in this rate quote query. (default: &#39;IN&#39;) | [optional] 
**PaymentTermsOverride** | **string** | An override of payment terms for all capacity provider accounts used by this request that have &#39;Enable API override of payment terms&#39; set as &#39;true&#39; in the project44 Self-Service Portal. This functionality is typically used in situations where both inbound and outbound shipments are common for a given capacity provider and account number. | [optional] 
**DirectionOverride** | **string** | An override of direction for all capacity provider accounts used by this request that have &#39;Enable API override of direction&#39; set as &#39;true&#39; in the project44 Self-Service Portal.This functionality is typically used in situations where both inbound and outbound shipments are common for a given capacity provider and account number. | [optional] 
**ApiConfiguration** | [**RateQuoteApiConfiguration**](RateQuoteApiConfiguration.md) | Fields for configuring the behavior of this API. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

