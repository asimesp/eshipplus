# P44SDK.V4.Model.TruckloadShipmentHazmatDetails
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HazardClasses** | **List&lt;string&gt;** | User-defined list of hazardous materials, if any that are included in the shipment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

