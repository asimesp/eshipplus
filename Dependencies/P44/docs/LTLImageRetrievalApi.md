# P44SDK.V4.Api.LTLImageRetrievalApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**QueryImages**](LTLImageRetrievalApi.md#queryimages) | **POST** /api/v4/ltl/images/query | POST: Query for LTL shipment images. 


<a name="queryimages"></a>
# **QueryImages**
> LtlImageReference QueryImages (LtlImageQuery ltlImageQuery)

POST: Query for LTL shipment images. 

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class QueryImagesExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LTLImageRetrievalApi();
            var ltlImageQuery = new LtlImageQuery(); // LtlImageQuery | ltlImageQuery

            try
            {
                // POST: Query for LTL shipment images. 
                LtlImageReference result = apiInstance.QueryImages(ltlImageQuery);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LTLImageRetrievalApi.QueryImages: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ltlImageQuery** | [**LtlImageQuery**](LtlImageQuery.md)| ltlImageQuery | 

### Return type

[**LtlImageReference**](LtlImageReference.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

