# P44SDK.V4.Api.VLTLQuotesApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**QueryRateQuotes**](VLTLQuotesApi.md#queryratequotes) | **POST** /api/v4/vltl/quotes/rates/query | POST: Query for volume LTL rate quotes.


<a name="queryratequotes"></a>
# **QueryRateQuotes**
> RateQuoteCollection QueryRateQuotes (RateQuoteQuery rateQuoteQuery)

POST: Query for volume LTL rate quotes.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class QueryRateQuotesExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VLTLQuotesApi();
            var rateQuoteQuery = new RateQuoteQuery(); // RateQuoteQuery | rateQuoteQuery

            try
            {
                // POST: Query for volume LTL rate quotes.
                RateQuoteCollection result = apiInstance.QueryRateQuotes(rateQuoteQuery);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VLTLQuotesApi.QueryRateQuotes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rateQuoteQuery** | [**RateQuoteQuery**](RateQuoteQuery.md)| rateQuoteQuery | 

### Return type

[**RateQuoteCollection**](RateQuoteCollection.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

