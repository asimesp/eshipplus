# P44SDK.V4.Api.TLTrackingApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateShipment**](TLTrackingApi.md#createshipment) | **POST** /api/v4/tl/shipments | POST: Initialize a truckload shipment for tracking.
[**CreateShipmentAction**](TLTrackingApi.md#createshipmentaction) | **POST** /api/v4/tl/shipments/{id}/actions | POST: Create an action for an existing truckload shipment identified by the unique system id.
[**DeleteShipment**](TLTrackingApi.md#deleteshipment) | **DELETE** /api/v4/tl/shipments/{id} | DELETE: Delete an existing truckload shipment using the unique system id.
[**GetShipment**](TLTrackingApi.md#getshipment) | **GET** /api/v4/tl/shipments/{id} | GET: Get a truckload shipment using the unique system id.
[**GetShipmentStatus**](TLTrackingApi.md#getshipmentstatus) | **GET** /api/v4/tl/shipments/statuses | GET: Get the status of a truckload shipment using the shipment identifier.
[**GetShipmentStatus_0**](TLTrackingApi.md#getshipmentstatus_0) | **GET** /api/v4/tl/shipments/{id}/statuses | GET: Get the status of a truckload shipment using the unique system id.
[**GetTrackingMethods**](TLTrackingApi.md#gettrackingmethods) | **GET** /api/v4/tl/shipments/trackingmethods | GET: Get available shipment tracking methods for a carrier.
[**RequestEquipmentApproval**](TLTrackingApi.md#requestequipmentapproval) | **POST** /api/v4/tl/shipments/{id}/equipmentapprovals/requests | POST: Request an ad hoc equipment approval from the tracking provider.
[**UpdateShipment**](TLTrackingApi.md#updateshipment) | **PUT** /api/v4/tl/shipments/{id} | PUT: Update a truckload shipment


<a name="createshipment"></a>
# **CreateShipment**
> TruckloadShipmentConfirmation CreateShipment (TruckloadShipment shipment)

POST: Initialize a truckload shipment for tracking.

If the shipment is initialized by using geoCoordinates instead of an address, it may be possible that a street address will not be returned in the response.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TLTrackingApi();
            var shipment = new TruckloadShipment(); // TruckloadShipment | shipment

            try
            {
                // POST: Initialize a truckload shipment for tracking.
                TruckloadShipmentConfirmation result = apiInstance.CreateShipment(shipment);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TLTrackingApi.CreateShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipment** | [**TruckloadShipment**](TruckloadShipment.md)| shipment | 

### Return type

[**TruckloadShipmentConfirmation**](TruckloadShipmentConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createshipmentaction"></a>
# **CreateShipmentAction**
> TruckloadShipmentActionConfirmation CreateShipmentAction (long? id, TruckloadShipmentAction shipmentAction)

POST: Create an action for an existing truckload shipment identified by the unique system id.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateShipmentActionExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TLTrackingApi();
            var id = 789;  // long? | The project44-generated id of the shipment to which this action applies.
            var shipmentAction = new TruckloadShipmentAction(); // TruckloadShipmentAction | shipmentAction

            try
            {
                // POST: Create an action for an existing truckload shipment identified by the unique system id.
                TruckloadShipmentActionConfirmation result = apiInstance.CreateShipmentAction(id, shipmentAction);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TLTrackingApi.CreateShipmentAction: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| The project44-generated id of the shipment to which this action applies. | 
 **shipmentAction** | [**TruckloadShipmentAction**](TruckloadShipmentAction.md)| shipmentAction | 

### Return type

[**TruckloadShipmentActionConfirmation**](TruckloadShipmentActionConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteshipment"></a>
# **DeleteShipment**
> void DeleteShipment (long? id)

DELETE: Delete an existing truckload shipment using the unique system id.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class DeleteShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TLTrackingApi();
            var id = 789;  // long? | The project44-generated id of the shipment being deleted.

            try
            {
                // DELETE: Delete an existing truckload shipment using the unique system id.
                apiInstance.DeleteShipment(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TLTrackingApi.DeleteShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| The project44-generated id of the shipment being deleted. | 

### Return type

void (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getshipment"></a>
# **GetShipment**
> TruckloadShipment GetShipment (long? id)

GET: Get a truckload shipment using the unique system id.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GetShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TLTrackingApi();
            var id = 789;  // long? | The project44-generated id of the shipment being queried.

            try
            {
                // GET: Get a truckload shipment using the unique system id.
                TruckloadShipment result = apiInstance.GetShipment(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TLTrackingApi.GetShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| The project44-generated id of the shipment being queried. | 

### Return type

[**TruckloadShipment**](TruckloadShipment.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getshipmentstatus"></a>
# **GetShipmentStatus**
> TruckloadShipmentStatus GetShipmentStatus (string carrierIdentifierType, string carrierIdentifierValue, string shipmentIdentifierType, string shipmentIdentifierValue, bool? includeStatusHistory = null, bool? includeMapUrl = null)

GET: Get the status of a truckload shipment using the shipment identifier.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GetShipmentStatusExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TLTrackingApi();
            var carrierIdentifierType = carrierIdentifierType_example;  // string | Capacity provider identifier type.
            var carrierIdentifierValue = carrierIdentifierValue_example;  // string | Capacity provider identifier value.
            var shipmentIdentifierType = shipmentIdentifierType_example;  // string | The type of the shipment identifier.
            var shipmentIdentifierValue = shipmentIdentifierValue_example;  // string | The value of the shipment identifier.
            var includeStatusHistory = true;  // bool? | Whether shipment status history ('statusUpdates') should be included in the response. (optional)  (default to false)
            var includeMapUrl = true;  // bool? | Whether a publicly accessible URL to the shipment details page should be included in the response. (optional)  (default to false)

            try
            {
                // GET: Get the status of a truckload shipment using the shipment identifier.
                TruckloadShipmentStatus result = apiInstance.GetShipmentStatus(carrierIdentifierType, carrierIdentifierValue, shipmentIdentifierType, shipmentIdentifierValue, includeStatusHistory, includeMapUrl);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TLTrackingApi.GetShipmentStatus: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **carrierIdentifierType** | **string**| Capacity provider identifier type. | 
 **carrierIdentifierValue** | **string**| Capacity provider identifier value. | 
 **shipmentIdentifierType** | **string**| The type of the shipment identifier. | 
 **shipmentIdentifierValue** | **string**| The value of the shipment identifier. | 
 **includeStatusHistory** | **bool?**| Whether shipment status history (&#39;statusUpdates&#39;) should be included in the response. | [optional] [default to false]
 **includeMapUrl** | **bool?**| Whether a publicly accessible URL to the shipment details page should be included in the response. | [optional] [default to false]

### Return type

[**TruckloadShipmentStatus**](TruckloadShipmentStatus.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getshipmentstatus_0"></a>
# **GetShipmentStatus_0**
> TruckloadShipmentStatus GetShipmentStatus_0 (long? id, bool? includeStatusHistory = null, bool? includeMapUrl = null)

GET: Get the status of a truckload shipment using the unique system id.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GetShipmentStatus_0Example
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TLTrackingApi();
            var id = 789;  // long? | The project44-generated id of the shipment being queried.
            var includeStatusHistory = true;  // bool? | Whether shipment status history ('statusUpdates') should be included in the response. (optional)  (default to false)
            var includeMapUrl = true;  // bool? | Whether a publicly accessible URL to the shipment details page should be included in the response. (optional)  (default to false)

            try
            {
                // GET: Get the status of a truckload shipment using the unique system id.
                TruckloadShipmentStatus result = apiInstance.GetShipmentStatus_0(id, includeStatusHistory, includeMapUrl);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TLTrackingApi.GetShipmentStatus_0: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| The project44-generated id of the shipment being queried. | 
 **includeStatusHistory** | **bool?**| Whether shipment status history (&#39;statusUpdates&#39;) should be included in the response. | [optional] [default to false]
 **includeMapUrl** | **bool?**| Whether a publicly accessible URL to the shipment details page should be included in the response. | [optional] [default to false]

### Return type

[**TruckloadShipmentStatus**](TruckloadShipmentStatus.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettrackingmethods"></a>
# **GetTrackingMethods**
> TruckloadShipmentTrackingMethods GetTrackingMethods (string carrierIdentifierType, string carrierIdentifierValue)

GET: Get available shipment tracking methods for a carrier.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GetTrackingMethodsExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TLTrackingApi();
            var carrierIdentifierType = carrierIdentifierType_example;  // string | Capacity provider identifier type.
            var carrierIdentifierValue = carrierIdentifierValue_example;  // string | Capacity provider identifier value.

            try
            {
                // GET: Get available shipment tracking methods for a carrier.
                TruckloadShipmentTrackingMethods result = apiInstance.GetTrackingMethods(carrierIdentifierType, carrierIdentifierValue);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TLTrackingApi.GetTrackingMethods: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **carrierIdentifierType** | **string**| Capacity provider identifier type. | 
 **carrierIdentifierValue** | **string**| Capacity provider identifier value. | 

### Return type

[**TruckloadShipmentTrackingMethods**](TruckloadShipmentTrackingMethods.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="requestequipmentapproval"></a>
# **RequestEquipmentApproval**
> void RequestEquipmentApproval (long? id)

POST: Request an ad hoc equipment approval from the tracking provider.

Only supported for the app-less tracking method once every 24h.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class RequestEquipmentApprovalExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TLTrackingApi();
            var id = 789;  // long? | The project44-generated id for the shipment.

            try
            {
                // POST: Request an ad hoc equipment approval from the tracking provider.
                apiInstance.RequestEquipmentApproval(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TLTrackingApi.RequestEquipmentApproval: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| The project44-generated id for the shipment. | 

### Return type

void (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateshipment"></a>
# **UpdateShipment**
> TruckloadShipmentConfirmation UpdateShipment (long? id, TruckloadShipment shipment)

PUT: Update a truckload shipment

*Tracking information updates are currently not supported. This includes shipmentIdentifiers, carrierIdentifiers, equipmentIdentifiers, capacityProviderAccountGroup, and apiConfiguration changes.* It is suggested to first get the current shipment object from the GET shipment endpoint, make the desired modifications, and then PUT the modified shipment to this endpoint. <ul><li> After the modified shipment is successfully submitted, the updated stops, attributes, shipping details etc. will be applied and used in tracking from then on. </li><li> If a shipment PUT request comes in with only geo-coordinates, it is possible that a street address will not be returned in the response. </li></ul>

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class UpdateShipmentExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TLTrackingApi();
            var id = 789;  // long? | The project44-generated id of the shipment being updated.
            var shipment = new TruckloadShipment(); // TruckloadShipment | shipment

            try
            {
                // PUT: Update a truckload shipment
                TruckloadShipmentConfirmation result = apiInstance.UpdateShipment(id, shipment);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TLTrackingApi.UpdateShipment: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **long?**| The project44-generated id of the shipment being updated. | 
 **shipment** | [**TruckloadShipment**](TruckloadShipment.md)| shipment | 

### Return type

[**TruckloadShipmentConfirmation**](TruckloadShipmentConfirmation.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

