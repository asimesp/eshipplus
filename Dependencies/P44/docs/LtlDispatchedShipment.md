# P44SDK.V4.Model.LtlDispatchedShipment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CapacityProviderAccountGroup** | [**CapacityProviderAccountGroup**](CapacityProviderAccountGroup.md) | Capacity provider account group, containing the account to be used for authentication with the capacity provider&#39;s shipment API. | 
**OriginLocation** | [**Location**](Location.md) | The origin address and contact for the shipment to be picked up. The origin contact will default to the requester, if not provided. | 
**DestinationLocation** | [**Location**](Location.md) | The destination address and contact for the requested shipment. | 
**RequesterLocation** | [**Location**](Location.md) | The address and contact of the agent or freight coordinator who is responsible for the order. Contact name, phone number, and email are required. | 
**LineItems** | [**List&lt;LineItem&gt;**](LineItem.md) | The line items to be shipped.A line item consists of one or more packages, all of the same package type and with the same dimensions, freight class, and NMFC code. Each package, however, may have a different number of pieces and a different weight. Note that each capacity provider has a different maximum number of line items that they can accept. | 
**PickupWindow** | [**LocalDateTimeWindow**](LocalDateTimeWindow.md) | The pickup date and time range in the timezone of the shipment&#39;s origin location. | 
**DeliveryWindow** | [**LocalDateTimeWindow**](LocalDateTimeWindow.md) | The delivery date and time range in the timezone of the shipment&#39;s destination location. Required by some capacity providers when requesting guaranteed or expedited services. | [optional] 
**CarrierCode** | **string** | SCAC of the carrier that is to pick up this shipment. Required only for capacity providers that support multiple SCACs. | [optional] 
**ShipmentIdentifiers** | [**List&lt;LtlShipmentIdentifier&gt;**](LtlShipmentIdentifier.md) | A list of identifiers or reference numbers for this shipment. Most capacity providers accept only identifiers of types &#39;BILL_OF_LADING&#39;, &#39;PURCHASE_ORDER&#39;, and &#39;CUSTOMER_REFERENCE&#39;. Only one identifier of each type may be provided. An identifier of type &#39;SYSTEM_GENERATED&#39; may not be provided. An identifier of type &#39;EXTERNAL&#39; may be provided and subsequently tracked with through project44 - - this identifier will not be communicated to the capacity provider. | [optional] 
**AccessorialServices** | [**List&lt;AccessorialService&gt;**](AccessorialService.md) | List of accessorial services to be requested for this shipment. Some capacity providers support accessorial services without providing a way of requesting them through their API. To handle this, project44 sends these accessorial services through the capacity provider&#39;s pickup note API field, according to the shipment note configuration. | [optional] 
**PickupNote** | **string** | Note that applies to the pickup of this shipment. The shipment note configuration determines the final pickup note that is sent through the capacity provider&#39;s API and whether or not part of it may be cut off. | [optional] 
**DeliveryNote** | **string** | Note that applies to the delivery of this shipment. Currently, since nearly all capacity provider APIs have only a pickup note field and not a delivery note field, this delivery note will be inserted into the capacity provider&#39;s pickup note API field, according to the shipment note configuration. | [optional] 
**EmergencyContact** | [**Contact**](Contact.md) | Emergency contact name and phone number are required when the shipment contains items marked as hazardous materials. | [optional] 
**CapacityProviderQuoteNumber** | **string** | The quote number for this shipment assigned by the capacity provider. Only a few LTL capacity providers accept a quote number when placing a shipment for pickup. Most volume LTL capacity providers, however, require a quote number. | [optional] 
**TotalLinearFeet** | **int?** | The total linear feet that the shipment being quoted will take up in a trailer. Only for volume LTL shipments. | [optional] 
**WeightUnit** | **string** | Weight measurement unit for all weight values in this shipment request. (default: &#39;LB&#39;) | [optional] 
**LengthUnit** | **string** | Length measurement unit for all length values in this shipment request. (default: &#39;IN&#39;) | [optional] 
**PaymentTermsOverride** | **string** | An override of payment terms for the capacity provider account used by this request, if it has &#39;Enable API override of payment terms&#39; set as &#39;true&#39; in the project44 Self-Service Portal. This functionality is typically used in situations where both inbound and outbound shipments are common for a given capacity provider and account number. | [optional] 
**DirectionOverride** | **string** | An override of direction for the capacity provider account used by this request, if it has &#39;Enable API override of direction&#39; set as &#39;true&#39; in the project44 Self-Service Portal.This functionality is typically used in situations where both inbound and outbound shipments are common for a given capacity provider and account number. | [optional] 
**ApiConfiguration** | [**ShipmentApiConfiguration**](ShipmentApiConfiguration.md) | Fields for configuring the behavior of this API. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

