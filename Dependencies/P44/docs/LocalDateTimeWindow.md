# P44SDK.V4.Model.LocalDateTimeWindow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **string** | Date for this time window in the timezone of the applicable location.(default: current date, format: yyyy-MM-dd) | [optional] 
**StartTime** | **string** | Start time of this window in the timezone of the applicable location. (format: HH:mm) | [optional] 
**EndTime** | **string** | End time of this window in the timezone of the applicable location. (format: HH:mm) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

