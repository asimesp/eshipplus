# P44SDK.V4.Api.LocationManagementApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateLocation**](LocationManagementApi.md#createlocation) | **POST** /api/v4/locations | POST: Create a new location. 
[**GetLocation**](LocationManagementApi.md#getlocation) | **GET** /api/v4/locations/{id} | GET: Get the location associated with the provided id.
[**ListLocations**](LocationManagementApi.md#listlocations) | **GET** /api/v4/locations | GET: Get the current list of all available locations.
[**UpdateLocation**](LocationManagementApi.md#updatelocation) | **PUT** /api/v4/locations/{id} | PUT: Update the specified location.


<a name="createlocation"></a>
# **CreateLocation**
> Location CreateLocation (Location location)

POST: Create a new location. 

A location consists of an address and an optional contact. Note that duplicate locations are allowed although each will be given a unique id which is returned here in the response.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class CreateLocationExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LocationManagementApi();
            var location = new Location(); // Location | location

            try
            {
                // POST: Create a new location. 
                Location result = apiInstance.CreateLocation(location);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LocationManagementApi.CreateLocation: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **location** | [**Location**](Location.md)| location | 

### Return type

[**Location**](Location.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getlocation"></a>
# **GetLocation**
> Location GetLocation (string id)

GET: Get the location associated with the provided id.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class GetLocationExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LocationManagementApi();
            var id = id_example;  // string | id

            try
            {
                // GET: Get the location associated with the provided id.
                Location result = apiInstance.GetLocation(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LocationManagementApi.GetLocation: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| id | 

### Return type

[**Location**](Location.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listlocations"></a>
# **ListLocations**
> LocationCollection ListLocations ()

GET: Get the current list of all available locations.

Each location returned in the response includes a unique id that can be used as reference in other APIs.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class ListLocationsExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LocationManagementApi();

            try
            {
                // GET: Get the current list of all available locations.
                LocationCollection result = apiInstance.ListLocations();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LocationManagementApi.ListLocations: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LocationCollection**](LocationCollection.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatelocation"></a>
# **UpdateLocation**
> Location UpdateLocation (string id, Location location)

PUT: Update the specified location.

The response reflects the changes made to the location.

### Example
```csharp
using System;
using System.Diagnostics;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;

namespace Example
{
    public class UpdateLocationExample
    {
        public void main()
        {
            
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new LocationManagementApi();
            var id = id_example;  // string | id
            var location = new Location(); // Location | location

            try
            {
                // PUT: Update the specified location.
                Location result = apiInstance.UpdateLocation(id, location);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LocationManagementApi.UpdateLocation: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| id | 
 **location** | [**Location**](Location.md)| location | 

### Return type

[**Location**](Location.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

