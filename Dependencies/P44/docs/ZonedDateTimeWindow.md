# P44SDK.V4.Model.ZonedDateTimeWindow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StartDateTime** | **DateTime?** | Start date and time of this window in the applicable time zone.(default: current date, format: yyyy-MM-dd&#39;T&#39;HH:mm:ss) | 
**EndDateTime** | **DateTime?** | End date and time of this window in the applicable time zone. (format: yyyy-MM-dd&#39;T&#39;HH:mm:ss) | 
**LocalTimeZoneIdentifier** | **string** | The time zone identifier (e.g., &#39;America/Chicago&#39;) for this date time window. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

