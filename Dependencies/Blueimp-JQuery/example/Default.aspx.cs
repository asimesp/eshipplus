﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    	ddlItems.DataSource = new List<ListItem>
    	                      	{
    	                      		new ListItem("Test 1", "0"),
    	                      		new ListItem("Test 2", "1"),
    	                      		new ListItem("Test 3", "2"),
    	                      	};
		ddlItems.DataBind();
    }
}
