﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System.Web;
using System.Web.Script.Serialization;

public class Handler : IHttpHandler
{

	public void ProcessRequest(HttpContext context)
	{
		context.Response.ContentType = "text/plain";//"application/json";
		var r = new System.Collections.Generic.List<ViewDataUploadFilesResult>();
		var js = new JavaScriptSerializer();
		foreach (string file in context.Request.Files)
		{
			var hpf = context.Request.Files[file];
			string fileName;
			if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
			{
				string[] files = hpf.FileName.Split(new[] { '\\' });
				fileName = files[files.Length - 1];
			}
			else
			{
				fileName = hpf.FileName;
			}
			if (hpf.ContentLength == 0)
				continue;
			string savedFileName = "C:\\Projects\\PacMan\\Dependencies\\Blueimp-JQuery\\example\\files\\" + fileName;
			hpf.SaveAs(savedFileName);

			r.Add(new ViewDataUploadFilesResult
					{
						Thumbnail_url = "files/" + fileName,
						Name = fileName,
						Length = hpf.ContentLength,
						Type = hpf.ContentType,
						Url = "files/" + fileName,
						AltText = context.Request.Form["ddlItems"]
					});
			
		}
		var uploadedFiles = new { files = r.ToArray() };
		var jsonObj = js.Serialize(uploadedFiles);
		//jsonObj.ContentEncoding = System.Text.Encoding.UTF8;
		//jsonObj.ContentType = "application/json;";
		context.Response.Write(jsonObj);
	}

	public bool IsReusable
	{
		get
		{
			return false;
		}
	}
}

public class ViewDataUploadFilesResult
{
	public string Thumbnail_url { get; set; }
	public string Name { get; set; }
	public int Length { get; set; }
	public string Type { get; set; }
	public string Url { get; set; }
	public string AltText { get; set; }
}