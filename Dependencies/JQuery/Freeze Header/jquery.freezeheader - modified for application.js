/* ------------------------------------------------------------------------
Class: freezeHeader
Use:freeze header row in html table
Example 1:  $('#tableid').freezeHeader();
Example 2:  $("#tableid").freezeHeader({ 'height': '300px' });
Example 3:  $("table").freezeHeader();
Example 4:  $(".table2").freezeHeader();
Author(s): Laerte Mercier Junior, Larry A. Hendrix
Version: 1.0.6

Modified: 
    Christopher Oyesiku     Mar 26 2015
    James Schradder         Mar 27 2015

Parameters:
    hhOffset                // header offset if not top 0 -- currently not used!
    ScrollContainerId       // scroll container if not window scroll
    ScrollOffsetControlId   // control id of object to base scroll off if not window scroll

-------------------------------------------------------------------------*/
(function ($) {
    var tableId = 0;
    $.fn.freezeHeader = function (params) {

        var copiedHeader = false;
        var resetCloneHeaderPosition = false;
        
        function freezeHeader(elem) {

            var settings = $.extend(true, {
                hhOffest: params && !jsHelper.IsNullOrEmpty(params.hhOffest) ? params.hhOffest : 180,  // page header offest
                fzIndex: params && !jsHelper.IsNullOrEmpty(params.HeaderZIndex) ? params.HeaderZIndex : "auto"    // bring forward to z-Index
            }, elem);
            
            var idObj = settings.attr('id') || ('tbl-' + (++tableId));
            if (settings.length > 0 && settings[0].tagName.toLowerCase() == "table") {
                
                var obj = {
                    id: idObj,
                    grid: settings,
                    container: null,
                    header: null,
                    divScroll: null,
                    openDivScroll: null,
                    closeDivScroll: null,
                    scroller: null,
                    DoNotFillHeader: params.DoNotFillHeaderBckg
                };
                
                obj.header = obj.grid.find('thead');

                obj.scroller = params && !jsHelper.IsNullOrEmpty(params.ScrollContainerId)
                    ? $(jsHelper.AddHashTag(params.ScrollContainerId))
                    : $(window);

                if ($('#hd' + obj.id).length == 0) {
                    obj.grid.before('<div id="hd' + obj.id + '"></div>');
                }

                obj.container = $('#hd' + obj.id);

                if (obj.header.offset() != null && limiteAlcancado(obj, params)) {
                    cloneHeaderRow(obj, settings);
                    copiedHeader = true;
                }

                obj.scroller.on('scroll', function () {
                    if (obj.header.offset() != null) {
                        if (limiteAlcancado(obj, params)) {
                            if (!copiedHeader) {
                                cloneHeaderRow(obj, settings);
                                copiedHeader = true;
                            } else {
                                obj.container.css("visibility", "visible");
                                
                                if (resetCloneHeaderPosition){
                                    setClonedHeaderRowPosition(obj);
                                    resetCloneHeaderPosition = false;
                                }
                            }
                        }
                        else {
                            if (($(document).scrollTop() > obj.header.offset().top - obj.grid.hhOffest)) {
                                obj.container.css("position", "absolute");
                                obj.container.css("top", (obj.grid.find("tr:last").offset().top - obj.header.height() - obj.grid.hhOffest) + "px");
                                resetCloneHeaderPosition = true;
                            }
                            else {
                                obj.container.css("visibility", "hidden");
                            }
                        }
                    }
                }); 
            }
        }

        function limiteAlcancado(obj, params1) {
            if (params1 && !jsHelper.IsNullOrEmpty(params1.ScrollContainerId)) {
                return (obj.header.offset().top < obj.scroller.offset().top);
            }
            else {
                return ($(document).scrollTop() > obj.header.offset().top - obj.grid.hhOffest && $(document).scrollTop() < (obj.grid.height() - obj.header.height() - obj.grid.find("tr:last").height()) + obj.header.offset().top - obj.grid.hhOffest);
            }
        }

        function cloneHeaderRow(obj) {
            obj.container.html('');
            obj.container.val('');
            var tabela = $('<table></table>');
            //var tabela = $('<table style="margin: 0 0;"></table>');
            var atributos = obj.grid.prop("attributes");

            $.each(atributos, function () {
                if (this.name != "id") {
                    tabela.attr(this.name, this.value);
                }
            });
            
            tabela.append('<thead>' + obj.header.html() + '</thead>');

            obj.container.append(tabela);
            obj.container.width(obj.header.width());
            obj.container.height(obj.header.height);
            obj.container.find('th').each(function (index) {
                var cellWidth = obj.grid.find('th').eq(index).width();
                $(this).css('width', cellWidth);
                if (!jsHelper.IsTrue(obj.DoNotFillHeader))
                    $(this).css('background-color', 'white'); // added as table headers are transparent in the application.
            });

            obj.container.css("visibility", "visible");

            setClonedHeaderRowPosition(obj);
            
            // logic to preserve checkbox states between clone and original header
            $(jsHelper.AddHashTag(obj.container.attr('id') + ' input:checkbox')).each(function() {
                var cloneCheckbox = $(this);
                var headerCheckbox = obj.header.find(jsHelper.AddHashTag(cloneCheckbox.attr('id')));
                if (headerCheckbox.prop('checked')) cloneCheckbox.prop('checked', 'checked');
            });
        }
        
        function setClonedHeaderRowPosition(obj) {
            if (params && !jsHelper.IsNullOrEmpty(params.ScrollContainerId)) {
                var offset = obj.scroller.offset().top - (params && !jsHelper.IsNullOrEmpty(params.ScrollOffsetControlId) ? $(jsHelper.AddHashTag(params.ScrollOffsetControlId)).offset().top + 2 : 0);
                obj.container.css("top", offset + "px");
                obj.container.css("position", "absolute");
                obj.container.css("z-index", obj.grid.fzIndex);
            } else {
                obj.container.css("top", obj.grid.hhOffest + "px");
                obj.container.css("z-index", obj.grid.fzIndex);
                obj.container.css("position", "fixed");
            }
        }

        return this.each(function (i, e) {
            freezeHeader($(e));
        });

    };
})(jQuery);
